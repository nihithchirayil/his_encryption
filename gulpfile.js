const {  src, dest , parallel } = require('gulp');
const uglify = require('gulp-terser');
const rename = require('gulp-rename');
const concat = require('gulp-concat');

function emrscript(cb) {
  return src('public/packages/extensionsvalley/emr/js/*.js')
    .pipe(concat('emr.app.js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('public/packages/extensionsvalley/emr/dist/'));
}

function css(cb) {
  // body omitted
  cb();
}

exports.emrscript = emrscript;
exports.css = css;
exports.build = parallel(emrscript, css);