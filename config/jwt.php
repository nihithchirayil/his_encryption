<?php

return [

    /*
    |--------------------------------------------------------------------------
    | JWT configurations
    |--------------------------------------------------------------------------
    |
    | Settings that are needed for jwt setup
    |
    */
    'issuer'        => env('JWT_ISS'),
    'audience'      => env('JWT_AUD'),
    'jti'           => env('JWT_JTI'),
    'secret'        => env('JWT_SECRET'),
    'expiration'    => env('JWT_EXPIRY'),
];