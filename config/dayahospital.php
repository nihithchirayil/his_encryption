<?php
return [
    'DashboardColors'=>[
        'seen_bg' => '#acf7d5',
        'seen_color' => '#000',
        'not_seen_bg' => '#594b7d',
        'not_seen_color' => '#ffff',
        'mlc_bg' => '#ffce49',
        'mlc_color' => '#000',
        'walkin_bg' => '#ff4995',
        'walkin_color' => '#fff',
        'follow_up_bg'=>'#30bbbb',
        'follow_up_color'=>'#ffff',
    ],

        'default_timezone_ist' =>  'Asia/Kolkata',
    'PayReceiptLedgerTypes'=>[
        '1' => 'Payment',
        '2' => 'Receipt',
    ],



 ];
?>
