<?php
return [
    'DashboardColors' => [
        'seen_bg' => '#acf7d5',
        'seen_color' => '#000',
        'not_seen_bg' => '#594b7d',
        'not_seen_color' => '#ffff',
        'mlc_bg' => '#ffce49',
        'mlc_color' => '#000',
        'walkin_bg' => '#ff4995',
        'walkin_color' => '#fff',
        'follow_up_bg' => '#30bbbb',
        'follow_up_color' => '#ffff',
    ],

    'search_code_expression' =>  '/[^\w\s-_\.]/gi',
    'datetime_format_web' => 'M-d-Y h:i A',
    'datetime_formt_web' => 'm-d-Y h:i A',
    'datetime_formt_web_new' => 'm-d-Y h:i A',
    'datetime_formt_web_booking' => 'm-d-Y h:i A',
    'datetime_format_web_insurance' => 'd-m-Y',
    'datetime_format_web_updated' => 'd-M-Y',
    'datetime_format_js' => 'MMM-DD-YYYY H:mm:ss',
    'datetime_format_mask' => 'aaa-99-9999 29:59:59',
    'datetime_format_placeholder' => 'MMM-DD-YYYY HH:mm:ss',
    'datetime_format_db' => 'Y-m-d H:i:s',
    'datetime_format_db_new' => 'Y-m-d H:i',
    'datetime_format_db_display' => 'Mon-dd-YYYY HH24:MI:SS',
    'datetime_format_db_display_web' => 'Mon-dd-YYYY HH24:MI',
    'datetime_format_db_display_new' =>  'Mon-dd-YYYY HH24:MI:SS',
    'datetime_format_db_display12' =>  'MM-DD-YYYY HH12:MI AM',
    'date_format_web' => 'M-d-Y',
    'date_format_web_new' => 'm-d-Y',
    'date_format_js' => 'MMM-DD-YYYY',
    'date_format_placeholder' => 'MMM-DD-YYYY',
    'date_format_placeholder_datepicker' =>  'MM-DD-YYYY',
    'date_format_mask' => 'aaa-99-9999',
    'date_format_mask_placeholder' =>  'MMM-DD-YYYY',
    'date_format_db' => 'Y-m-d',
    'date_format_db_new' => 'Y-m-d',
    'date_format_db_display' =>  'Mon-dd-YYYY',
    'date_frmt_web' =>  'd-m-Y',
    'datetime_frmt_web' =>  'd-m-Y H:i:s',

    'time_format_web' =>  'H:i:s',
    'time_format_web_am' => 'H:i a',
    'time_format_js' => 'HH:mm:ss',
    'time_format_mask' => '99:99:99',
    'time_format_placeholder' => 'HH:mm:ss',
    'time_format_db' => 'H:i:s',
    'date_format_age' =>  'm-d-Y',
    'day_format_web' => 'd',
    'day_format_JS' =>  'DD-MMM-YYYY',
    'day_format_JS1' => 'DD-MM-YYYY',
    'day_format_JSYear' => 'YYYY',
    'day_format_Year' => 'Y',
    'daytime_format_JS' =>  'DD-MMM-YYYY HH:mm:ss',
    'export_file_formats' =>  [
        'xls',
        'csv',
        'pdf'
    ],
    'default_timezone_ist' =>  'Asia/Kolkata',
    'Version' => '1:0',
   // 'copyright' => '<a href="http://codfoxx.in/wordpress/">Codfoxx Software Labs</a>',
   'copyright' => 'Grandis Business solutions Pvt Ltd',
    'BedStatusColors' => [
        'Admission Requested' => '#02075d', //11
        'Admitted But Not Occupied' => '#bb85b4', //5
        'Occupied' => '#c76000', //1
        'Transfer Requested' => '#767676', //10
        'Retain' => '#8d6911', //4
        'Marked for discharge' => '#F7EB00', //6
        'Sent for billing' => '#83943e', //8
        'Discharged but not vacated' => '#01ff70', //7
        'House Keeping' => '#81c042', //2
        'Vacant' => '#8c8c8c', //3
        'Blocked' => '#ae3a3a', //9
    ],
    'BedStatusTextColors' => [
        'Admission Requested' => '#001f3f', //11
        'Admitted But Not Occupied' => '#F59846', //5
        'Occupied' => '#E33C49', //1
        'Transfer Requested' => '#767676', //10
        'Retain' => '#AD00EE', //4
        'Marked for discharge' => '#F7EB00', //6
        'Sent for billing' => '#8C4800', //8
        'Discharged but not vacated' => '#A52A2A', //7
        'House Keeping' => '#3ACD8F', //2
        'Vacant' => '#4498F6', //3
        'Blocked' => '#BFBFBF', //9
    ],
    'op_no_label' => 'UHID',
    'admission_priority' => [
        '1'=>'ICU',
        '2'=>'NICU',
        '3'=>'WARD',
        '4'=>'NIV WITH 02',
    ],
    'reference_type' => [
        '1' => 'Immediately',
        '2' => 'Today Itself',
        '3' => 'Anytime',
    ],
    'PayReceiptLedgerTypes'=>[
        '1' => 'Payment',
        '2' => 'Receipt',
    ],
    'default_pharmacy' => '161',
    'private_note_visibility_default_checked' => 1,
    'private_note_visibility'=>1,
    'document_driver'=>0,//1-DB , 0-FILE
    'default_store' => 'ST001',
    'default_general_store' => 'LGST001',
    'default_return_store' => 'VS001',
    'iv_subcategory_id' => '384',
    'medication_without_approval' => 1,
    'medication_alert_duration' => 10,
    'nursing_notes_edit_max_time' => 86400,
];
