DROP TABLE IF EXISTS "item_type";
DROP SEQUENCE IF EXISTS item_type_id_seq;
CREATE SEQUENCE item_type_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."item_type" (
    "id" integer DEFAULT nextval('item_type_id_seq') NOT NULL,
    "name" character varying(50) NOT NULL,
    "group_code" character varying(50) NOT NULL,
    "status" smallint DEFAULT '1' NOT NULL
) WITH (oids = false);

INSERT INTO "item_type" ("id", "name", "group_code", "status") VALUES
(1,	'Inventory Items',	'INV01',	1);

CREATE TABLE "category" (
  "id" serial NOT NULL,
  "name" character varying(50) NOT NULL,
  "category_code" character varying(50) NOT NULL,
  "item_type" character varying(20) NULL,
  "status" smallint NOT NULL DEFAULT '1',
  "created_at" timestamp(0) NOT NULL,
  "created_by" integer NOT NULL DEFAULT '0',
  "updated_at" timestamp(0) NULL,
  "updated_by" integer NOT NULL DEFAULT '0',
  "deleted_at" integer NULL,
  "deleted_by" integer NOT NULL DEFAULT '0'
);