-- Adminer 4.7.7 PostgreSQL dump

DROP TABLE IF EXISTS "acl_permission";
DROP SEQUENCE IF EXISTS acl_permission_id_seq;
CREATE SEQUENCE acl_permission_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 12 CACHE 1;

CREATE TABLE "public"."acl_permission" (
    "id" integer DEFAULT nextval('acl_permission_id_seq') NOT NULL,
    "group_id" integer NOT NULL,
    "menu_text" character varying(50) NOT NULL,
    "link" character varying(255) NOT NULL,
    "icon" character varying(100),
    "acl_key" character varying(255) NOT NULL,
    "parent_menu" smallint DEFAULT '0' NOT NULL,
    "ordering" smallint NOT NULL,
    "adding" smallint DEFAULT '0' NOT NULL,
    "edit" smallint DEFAULT '0' NOT NULL,
    "view" smallint DEFAULT '0' NOT NULL,
    "trash" smallint DEFAULT '0' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "acl_permission_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "acl_permission" ("id", "group_id", "menu_text", "link", "icon", "acl_key", "parent_menu", "ordering", "adding", "edit", "view", "trash", "created_at", "updated_at") VALUES
(8,	1,	'User Panel',	'',	'<i class="fa fa-user"></i>',	'extensionsvalley.dashboard.userpanel',	0,	0,	0,	0,	1,	0,	'2020-11-09 11:29:16',	NULL),
(9,	1,	'Master',	'',	'<i class="fa fa-cogs"></i>',	'extensionsvalley.master.masters',	0,	-1,	0,	0,	1,	0,	'2020-11-09 11:29:16',	NULL),
(10,	1,	'Manage Users',	'http://localhost/inv_system/public/admin/ExtensionsValley/dashboard/list/users',	'',	'extensionsvalley.dashboard.users',	8,	0,	1,	1,	1,	1,	'2020-11-09 11:29:16',	NULL),
(11,	1,	'User Groups',	'http://localhost/inv_system/public/admin/ExtensionsValley/dashboard/list/groups',	'',	'extensionsvalley.dashboard.groups',	8,	0,	1,	1,	1,	1,	'2020-11-09 11:29:16',	NULL),
(12,	1,	'Manage Category',	'http://localhost/inv_system/public/master/listCategory',	'',	'extensionsvalley.master.listCategory',	9,	1,	1,	1,	1,	1,	'2020-11-09 11:29:16',	NULL);

DROP TABLE IF EXISTS "category";
DROP SEQUENCE IF EXISTS category_id_seq;
CREATE SEQUENCE category_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 12 CACHE 1;

CREATE TABLE "public"."category" (
    "id" integer DEFAULT nextval('category_id_seq') NOT NULL,
    "name" character varying(50) NOT NULL,
    "category_code" character varying(50) NOT NULL,
    "status" smallint DEFAULT '1' NOT NULL,
    "created_at" timestamp(0) NOT NULL,
    "created_by" integer DEFAULT '0' NOT NULL,
    "updated_at" timestamp(0),
    "updated_by" integer DEFAULT '0' NOT NULL,
    "deleted_at" integer,
    "deleted_by" integer DEFAULT '0' NOT NULL,
    "item_type" character varying(20)
) WITH (oids = false);

INSERT INTO "category" ("id", "name", "category_code", "status", "created_at", "created_by", "updated_at", "updated_by", "deleted_at", "deleted_by", "item_type") VALUES
(1,	'stationary',	'ST01',	1,	'2020-11-22 16:55:21',	0,	'2020-11-22 16:55:21',	0,	NULL,	0,	'INV01'),
(3,	'plastic',	'13',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(4,	'books',	'14',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(5,	'pen',	'15',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(6,	'table',	'16',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(7,	'sand',	'17',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(8,	'vegitables',	'18',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(9,	'balls',	'19',	1,	'2020-11-22 12:06:04',	0,	'2020-11-22 12:06:04',	0,	NULL,	0,	'INV01'),
(10,	'test1',	'20',	1,	'2020-11-23 02:13:02',	0,	'2020-11-23 02:13:02',	0,	NULL,	0,	'INV01'),
(11,	'test 2',	'21',	1,	'2020-11-23 02:17:11',	0,	'2020-11-23 02:17:11',	0,	NULL,	0,	'INV01'),
(12,	'test 3',	'22',	1,	'2020-11-23 02:19:13',	0,	'2020-11-23 02:19:13',	1,	NULL,	0,	'INV01'),
(2,	'ttttttttttttttt',	'12',	0,	'2020-11-22 12:06:04',	0,	'2020-11-23 02:42:18',	1,	NULL,	0,	'INV01');

DROP TABLE IF EXISTS "extension_manager";
DROP SEQUENCE IF EXISTS extension_manager_id_seq;
CREATE SEQUENCE extension_manager_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."extension_manager" (
    "id" integer DEFAULT nextval('extension_manager_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "vendor" character varying(255) NOT NULL,
    "description" character varying(255),
    "version" character varying(5) NOT NULL,
    "is_paid" smallint DEFAULT '0' NOT NULL,
    "status" smallint DEFAULT '1' NOT NULL,
    "package_type" character varying(50) NOT NULL,
    "icon" character varying(255),
    "update_url" character varying(255),
    "author" character varying(255),
    "website" character varying(255),
    "contact_email" character varying(255),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "extension_manager_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "extension_manager" ("id", "name", "vendor", "description", "version", "is_paid", "status", "package_type", "icon", "update_url", "author", "website", "contact_email", "created_at", "updated_at", "deleted_at") VALUES
(1,	'Dashboard',	'WalksWithMe',	'Core Package',	'2.0.0',	1,	1,	'wwmladmin-package',	'packages/extensionsvalley/dashboard/package_icons/icon.png',	'https://github.com/LaFlux/Laflux',	'Jobin <support@walkswithme.net>',	'http://www.walkswithme.net/contact-me',	'support@walkswithme.net',	'2020-11-09 10:10:50',	'2020-11-09 10:10:50',	NULL);

DROP TABLE IF EXISTS "failed_jobs";
DROP SEQUENCE IF EXISTS failed_jobs_id_seq;
CREATE SEQUENCE failed_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START  CACHE 1;

CREATE TABLE "public"."failed_jobs" (
    "id" bigint DEFAULT nextval('failed_jobs_id_seq') NOT NULL,
    "connection" text NOT NULL,
    "queue" text NOT NULL,
    "payload" text NOT NULL,
    "exception" text NOT NULL,
    "failed_at" timestamp(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "gen_settings";
DROP SEQUENCE IF EXISTS gen_settings_id_seq;
CREATE SEQUENCE gen_settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START  CACHE 1;

CREATE TABLE "public"."gen_settings" (
    "id" integer DEFAULT nextval('gen_settings_id_seq') NOT NULL,
    "settings_key" character varying(255) NOT NULL,
    "settings_value" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "gen_settings_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "gen_settings_settings_key_unique" UNIQUE ("settings_key")
) WITH (oids = false);


DROP TABLE IF EXISTS "groups";
DROP SEQUENCE IF EXISTS groups_id_seq;
CREATE SEQUENCE groups_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 6 CACHE 1;

CREATE TABLE "public"."groups" (
    "id" integer DEFAULT nextval('groups_id_seq') NOT NULL,
    "name" character varying(200) NOT NULL,
    "status" smallint DEFAULT '1' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "groups_name_unique" UNIQUE ("name"),
    CONSTRAINT "groups_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "groups" ("id", "name", "status", "created_at", "updated_at", "deleted_at") VALUES
(1,	'Super Admin',	1,	'2020-11-09 10:10:50',	NULL,	NULL),
(2,	'Admin',	1,	'2020-11-09 10:10:50',	NULL,	NULL),
(3,	'Manager',	1,	'2020-11-09 10:10:50',	NULL,	NULL),
(4,	'Editors',	1,	'2020-11-09 10:10:50',	NULL,	NULL),
(5,	'Writers',	1,	'2020-11-09 10:10:50',	NULL,	NULL),
(6,	'Registered Users',	1,	'2020-11-09 10:10:50',	NULL,	NULL);

DROP TABLE IF EXISTS "item_type";
DROP SEQUENCE IF EXISTS item_type_id_seq;
CREATE SEQUENCE item_type_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."item_type" (
    "id" integer DEFAULT nextval('item_type_id_seq') NOT NULL,
    "name" character varying(50) NOT NULL,
    "group_code" character varying(50) NOT NULL,
    "status" smallint DEFAULT '1' NOT NULL
) WITH (oids = false);

INSERT INTO "item_type" ("id", "name", "group_code", "status") VALUES
(1,	'Inventory Items',	'INV01',	1);

DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 9 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "migrations" ("id", "migration", "batch") VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2015_08_25_062200_Alter_Users_Table',	1),
(4,	'2015_08_25_062201_Create_Groups_Table',	1),
(5,	'2015_08_25_062202_Create_ACL_Table',	1),
(6,	'2015_08_25_062203_Create_Settings_Table',	1),
(7,	'2015_09_01_061101_Create_Users_Profile_Table',	1),
(8,	'2016_09_28_062203_Create_ExtensionManager_Table',	1),
(9,	'2019_08_19_000000_create_failed_jobs_table',	1);

DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "public"."password_resets" (
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "created_at" timestamp(0)
) WITH (oids = false);

CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");


DROP TABLE IF EXISTS "user_profile";
DROP SEQUENCE IF EXISTS user_profile_id_seq;
CREATE SEQUENCE user_profile_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START  CACHE 1;

CREATE TABLE "public"."user_profile" (
    "id" integer DEFAULT nextval('user_profile_id_seq') NOT NULL,
    "user_id" integer NOT NULL,
    "first_name" character varying(255) NOT NULL,
    "last_name" character varying(255) NOT NULL,
    "address" text NOT NULL,
    "street" text NOT NULL,
    "media" character varying(255),
    "city" character varying(255) NOT NULL,
    "state" character varying(255) NOT NULL,
    "zip" character varying(255) NOT NULL,
    "mobile" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "user_profile_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "user_profile_user_id_unique" UNIQUE ("user_id")
) WITH (oids = false);


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 2 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "email" character varying(255) NOT NULL,
    "email_verified_at" timestamp(0),
    "password" character varying(255) NOT NULL,
    "remember_token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "status" smallint DEFAULT '1' NOT NULL,
    "groups" smallint DEFAULT '3' NOT NULL,
    "deleted_at" timestamp(0),
    CONSTRAINT "users_email_unique" UNIQUE ("email"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "users" ("id", "name", "email", "email_verified_at", "password", "remember_token", "created_at", "updated_at", "status", "groups", "deleted_at") VALUES
(1,	'Admin',	'admin@demo.in',	NULL,	'$2y$10$YVBm3P6LfRolTLBtMjdlV.Hsp1f9Ms4jYBHbifnzwt6u3DYp.YA22',	NULL,	'2020-11-09 10:10:50',	'2020-11-09 10:10:50',	1,	1,	NULL),
(2,	'John',	'john@demo.in',	NULL,	'$2y$10$YVBm3P6LfRolTLBtMjdlV.Hsp1f9Ms4jYBHbifnzwt6u3DYp.YA22',	NULL,	'2020-11-09 10:10:50',	'2020-11-09 10:10:50',	1,	1,	NULL);

-- 2020-11-26 11:16:58.291733+05:30
