$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    searchGrnList();
});

var base_url = $('#base_url').val();
var token = $('#token').val();

function freeStockEditData(stock_id) {
    var main_uri = base_url + "/free_stock/addEditFreeStock";
    if (parseInt(stock_id) != 0) {
        main_uri = base_url + "/free_stock/addEditFreeStock/" + stock_id;
    }
    document.location.href = main_uri;
}

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

function searchGrnList() {
    var url = base_url + "/free_stock/searchFreeStockData";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var free_stock_id = $('#grn_id_hidden').val();
    var location = $('#to_location').val();
    var stock_status = $('#stock_status').val();
    var free_stock_no = $('#grn_no_search').val();

    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        free_stock_id: free_stock_id,
        location: location,
        stock_status: stock_status,
        free_stock_no: free_stock_no
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchGrnListData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $('#searchgrnlistBtn').attr('disabled', true);
            $('#searchgrnlistSpin').removeClass('fa fa-search');
            $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchGrnListData').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#searchGrnListData").LoadingOverlay("hide");
            $('#searchgrnlistBtn').attr('disabled', false);
            $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchgrnlistSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

//----------- Item Detail search------------
$('#item_desc').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('item_desc_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#item_desc_hidden').val() != "") {
            $('#item_desc_hidden').val('');
            $("#item_desc_AjaxDiv").hide();
        }
        var item_desc = $(this).val();
        if (item_desc == "") {
            $("#item_desc_AjaxDiv").html("").hide();
        } else {
            var url = base_url + "/grn/ProductNameSearch";

            $.ajax({
                type: "GET",
                url: url,
                data: "item_desc=" + item_desc,
                beforeSend: function () {
                    $("#item_desc_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#item_desc_AjaxDiv").html(html).show();
                    $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('item_desc_AjaxDiv', event);
    }
});


$('#grn_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxGrnNoSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var grn_number = $('#grn_no_search').val();
        if (grn_number == "") {
            $('#ajaxGrnNoSearchBox').hide();
            $("#grn_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    search_key_id: 'free_stock_no',
                    search_key: grn_number
                },
                beforeSend: function () {
                    $("#ajaxGrnNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxGrnNoSearchBox").html(html).show();
                    $("#ajaxGrnNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxGrnNoSearchBox', event);
    }
});


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function fillSearchDetials(grn_id, grn_number) {
    grn_number = htmlDecode(grn_number);
    $('#grn_no_search').val(grn_number);
    $('#grn_id_hidden').val(grn_id);
    $('#ajaxGrnNoSearchBox').hide();
}



function clearFilter() {
    var default_location = $('#default_location').val();
    $('#from_date').val('');
    $('#to_date').val('');
    $('#grn_id_hidden').val('');
    $('#to_location').val(default_location);
    $('#stock_status').val('');
    $('#grn_no_search').val('');
    searchGrnList();
}
