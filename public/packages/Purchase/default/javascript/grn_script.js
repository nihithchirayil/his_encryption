
$(document).ajaxStart(function () {
    //  $.LoadingOverlay("show",{ background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
});
$(document).ajaxStop(function () {
    //  $.LoadingOverlay("hide");
});
$(document).ajaxError(function () {
    //   $.LoadingOverlay("hide");
});
function batchEntry(obj, row_id) {
    batch = $('#batch_pop').val();
    $('#batch_row' + row_id).val(batch);
    item_arr[row_id]['batch_no'] = batch;
}
function ExpiryEntry(obj, row_id) {
    checkbatchStatus(4);
    expiry = $('#Expiry_date_pop').val();
    $('#expiry_date' + row_id).val(expiry);
    item_arr[row_id]['Expiry'] = expiry;
}

$(document).ready(function () {
    $(document).bind("keydown", function (event) {
        if (event.altKey && event.keyCode === 83) { // Alt + S to save
            event.preventDefault();
            $("#grn_save_btn").trigger('click');
        }
    });
    $(document).bind("keydown", function (event) {
        if (event.altKey && event.keyCode === 80) { // Alt + P to save and print
            event.preventDefault();
            $("#grn_save_print_btn").trigger('click');
        }
    });
})

var batch_validate = {};


window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

function checkPOPDataValidation() {
    setTimeout(function () {
        var batch_pop = $('#batch_pop').val();
        var Expiry_date_pop = $('#Expiry_date_pop').val();
        var grn_qty = parseFloat($('#grn_qty').val());
        var item_code = $('#batch_popup_itemcode').val();
        var rate = parseFloat($('#rate').val());
        var mrp = parseFloat($('#mrp').val());
        var unit_mrp = parseFloat($('#unit_mrp').val());
        var row_id = $('#row_id_hidden').val();
        var free_qty = $("#free_item_id").is(':checked');
        var selling_price = $('#selling_price').val();
        var batch_status = $('#batch_needed').val();
        if (!mrp) {
            mrp = 0;
        }
        if (!selling_price) {
            selling_price = 0;
        }
        $('#charge_percentage_1').focus();
        $('.validation').css('background', '#fff');
        if (batch_pop == '') {
            Command: toastr["warning"]("Batch is Empty !!");
            $('#batch_pop').focus();
            $('#batch_pop').css('background', '#f3bcbc69');
            return;
        }
        else if (Expiry_date_pop == '' && parseInt(batch_status) == 1) {
            Command: toastr["warning"]("Expiry date is Empty !!");
            $('#Expiry_date_pop').focus();
            $('#Expiry_date_pop').css('background', '#f3bcbc69');
            return;
        }
        else if (grn_qty == '') {
            Command: toastr["warning"]("GRN Qty is Empty !!");
            $('#grn_qty').focus();
            $('#grn_qty').css('background', '#f3bcbc69');
            return;
        } else if (rate == '' && !free_qty) {
            Command: toastr["warning"]("Rate is Empty !!");
            $('#rate').focus();
            $('#rate').css('background', '#f3bcbc69');
            return;
        } else if (parseFloat(mrp) == 0 && parseInt(batch_status) == 1) {
            Command: toastr["warning"]("MRP is Empty !!");
            $('#mrp').focus();
            $('#mrp').css('background', '#f3bcbc69');
            return;
        } else if (parseFloat(mrp) < parseFloat(rate) && parseInt(batch_status) == 1) {
            Command: toastr["warning"]("MRP is Less than Rate !!");
            $('#mrp').focus();
            $('#mrp').css('background', '#f3bcbc69');
            $('#rate').css('background', '#f3bcbc69');
            return;
        } else if ((parseFloat(unit_mrp) < parseFloat(selling_price)) && parseInt(batch_status) == 1) {
            Command: toastr["warning"]("Unit MRP is Less than Selling Price !!");
            $('#selling_price').focus();
            $('#selling_price').css('background', '#f3bcbc69');
            $('#unit_mrp').css('background', '#f3bcbc69');
            return;
        } else if (parseFloat(selling_price) == 0 && parseInt(batch_status) == 1) {
            Command: toastr["warning"]("Selling Price cannot be empty !!");
            $('#selling_price').focus();
            $('#selling_price').css('background', '#f3bcbc69');
            return;
        } else {
            var grn_id = $('#grn_id_hidden').val();
            var grn_status = $('#grn_status_hidden').val();
            if (parseInt(grn_id) != 0 && (parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) {
                var free_qty = $('#free_qty').val();
                var uom_val = $('#uom_val').val();
                var tot_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * parseFloat(uom_val);
                var row_id = $('#popup_rowid').val();
                var org_qty = $('#tot_qty_' + row_id).val();
                checkStockProductQty(tot_qty, row_id, 1, 1, 1);
            } else {
                $('#desc_amount_modal').modal('hide');
                setTimeout(function () {
                    BillDiscountcalculate();
                }, 1500);
            }
        }
        if (batch_pop != '' && parseInt(batch_status) == 1) {
            $('#batch_row' + row_id).val(batch_pop);
        } else if (parseInt(batch_status) == 0) {
            $('#batch_row' + row_id).val(batch_pop);
        }
    }, 1000);
}

function checkbatchStatus(from_type = 4) {
    var grn_id = $('#grn_id_hidden').val();
    var grn_status = $('#grn_status_hidden').val();
    if (parseInt(grn_id) != 0 && (parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) {
        var grn_qty = parseFloat($('#grn_qty').val());
        var free_qty = $('#free_qty').val();
        var uom_val = $('#uom_val').val();
        var tot_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * parseFloat(uom_val);
        var row_id = $('#popup_rowid').val();
        checkStockProductQty(tot_qty, row_id, 1, 1, from_type);
    }
}

function checkStockProductQty(tot_qty, row_id, cal_type, from_type, from_fn) {

    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
    if (bill_detail_id && bill_detail_id != 'undefined') {
        var obj = '';
        if (parseInt(from_fn) == 2) {
            obj = $('#grn_delete_check').val();
        } else {
            obj = $('#stock_data').val();
        }

        var data = JSON.parse(obj);

        var transfered_qty = parseInt(data.all_total_quantity) - parseInt(data.item_stock);
        if (parseInt(transfered_qty) < 0) {
            transfered_qty = parseInt(data.item_stock) - parseInt(data.all_total_quantity);
        }
        console.log(transfered_qty);
        console.log(data.all_total_quantity);
        console.log(data.item_stock);
        console.log(tot_qty);
        if (parseInt(from_fn) == 1) {
            if (parseFloat(data.all_total_quantity) <= parseFloat(data.item_stock)) {
                $('#desc_amount_modal').modal('hide');
                setTimeout(function () {
                    BillDiscountcalculate();
                }, 1500);
            } else if (parseFloat(tot_qty) < parseFloat(transfered_qty)) {
                $('#grn_qty').val(data.grn_qty ? data.grn_qty : 0);
                $('#free_qty').val(data.grn_free_qty ? data.grn_free_qty : 0);
                toastr.warning("Grn cannot be edited.Stock already transfered");
            } else {
                $('#desc_amount_modal').modal('hide');
                setTimeout(function () {
                    BillDiscountcalculate();
                }, 1500);
            }
        } else if (parseInt(from_fn) == 2) {
            if (parseFloat(data.all_total_quantity) == parseFloat(data.item_stock)) {
                if (charge_item.hasOwnProperty('TAX'))
                    delete charge_item['TAX'][row_id];
                if (charge_item.hasOwnProperty('DISCOUNT'))
                    delete charge_item['DISCOUNT'][row_id];
                if (charge_item.hasOwnProperty('FREE'))
                    delete charge_item['FREE'][row_id];
                if (charge_item.hasOwnProperty('OTHCH')) {
                    delete charge_item['OTHCH'][row_id];
                    delete charge_item['OTHCH'][row_id];
                }
                delete item_arr[row_id];
                BillDiscountcalculate();
                var detail_id = $('#bill_detail_id_hidden' + row_id).val();
                if (typeof (detail_id) != 'undefined') {
                    delete_array.push({
                        'detail_id': detail_id
                    });
                }
                $("#row_data_" + row_id).remove();
            } else if (parseFloat(tot_qty) > parseFloat(data.item_stock)) {
                toastr.warning("Grn cannot be edited.Stock already transfered");
            } else if (parseFloat(data.all_total_quantity) > parseFloat(tot_qty)) {
                toastr.warning("Grn cannot be edited.Stock already transfered");
            } else {
                if (charge_item.hasOwnProperty('TAX'))
                    delete charge_item['TAX'][row_id];
                if (charge_item.hasOwnProperty('DISCOUNT'))
                    delete charge_item['DISCOUNT'][row_id];
                if (charge_item.hasOwnProperty('FREE'))
                    delete charge_item['FREE'][row_id];
                if (charge_item.hasOwnProperty('OTHCH')) {
                    delete charge_item['OTHCH'][row_id];
                    delete charge_item['OTHCH'][row_id];
                }
                delete item_arr[row_id];
                BillDiscountcalculate();
                var detail_id = $('#bill_detail_id_hidden' + row_id).val();
                if (typeof (detail_id) != 'undefined') {
                    delete_array.push({
                        'detail_id': detail_id
                    });
                }
                $("#row_data_" + row_id).remove();
            }

        } else if (parseInt(from_fn) == 3) {
            if (parseFloat(data.all_total_quantity) <= parseFloat(data.item_stock)) {
                calculateFullData(row_id, cal_type, from_type, true);
            } else if (parseFloat(tot_qty) < parseFloat(transfered_qty)) {
                $('#grn_qty').val(data.grn_qty ? data.grn_qty : 0);
                $('#free_qty').val(data.grn_free_qty ? data.grn_free_qty : 0);
                toastr.warning("Grn cannot be edited.Stock already transfered");
                calculateFullData(row_id, cal_type, from_type, false);
            } else {
                calculateFullData(row_id, cal_type, from_type, true);
            }

        } else if (parseInt(from_fn) == 4) {
            if (parseFloat(data.all_total_quantity) != parseFloat(data.item_stock)) {
                $('#batch_pop').val(data.batch);
                $('#Expiry_date_pop').val(data.expiry);
            }
        }

    } else {
        if (parseInt(from_fn) == 1) {
            $('#desc_amount_modal').modal('hide');
            setTimeout(function () {
                BillDiscountcalculate();
            }, 1500);
        } else if (parseInt(from_fn) == 2) {
            if (charge_item.hasOwnProperty('TAX'))
                delete charge_item['TAX'][row_id];
            if (charge_item.hasOwnProperty('DISCOUNT'))
                delete charge_item['DISCOUNT'][row_id];
            if (charge_item.hasOwnProperty('FREE'))
                delete charge_item['FREE'][row_id];
            if (charge_item.hasOwnProperty('OTHCH')) {
                delete charge_item['OTHCH'][row_id];
                delete charge_item['OTHCH'][row_id];
            }
            delete item_arr[row_id];
            BillDiscountcalculate();
            var detail_id = $('#bill_detail_id_hidden' + row_id).val();
            if (typeof (detail_id) != 'undefined') {
                delete_array.push({
                    'detail_id': detail_id
                });
            }
            $("#row_data_" + row_id).remove();

        } else if (parseInt(from_fn) == 3) {
            calculateFullData(row_id, cal_type, from_type, true);
        }
    }
}


function removeRow(row_id) {
    if (item_arr.hasOwnProperty(row_id)) {
        bootbox.confirm({
            message: "Are you sure, you want delete ?",
            buttons: {
                'confirm': {
                    label: "Delete",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var grn_id = $('#grn_id_hidden').val();
                    var grn_status = $('#grn_status_hidden').val();
                    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
                    if (bill_detail_id && bill_detail_id != 'undefined' && parseInt(grn_id) != 0 && (parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) {
                        var item_code = $('#item_code_hidden' + row_id).val();
                        var tot_qty = $('#tot_qty_' + row_id).val();
                        var base_url = $('#base_url').val();
                        var url = base_url + "/purchase/checkItemProductStock";
                        var _token = $('#_token').val();
                        var location_code = $('#location').val();

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                _token: _token,
                                location_code: location_code,
                                bill_detail_id: bill_detail_id,
                                item_code: item_code,
                                item_code: item_code,
                            },
                            beforeSend: function () {
                                $("#row_body_data").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                            },
                            success: function (response) {
                                $('#grn_delete_check').val(JSON.stringify(response));
                            },
                            complete: function () {
                                $("#row_body_data").LoadingOverlay("hide");
                                checkStockProductQty(tot_qty, row_id, 1, 1, 2);
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });


                    } else {
                        if (charge_item.hasOwnProperty('TAX'))
                            delete charge_item['TAX'][row_id];
                        if (charge_item.hasOwnProperty('DISCOUNT'))
                            delete charge_item['DISCOUNT'][row_id];
                        if (charge_item.hasOwnProperty('FREE'))
                            delete charge_item['FREE'][row_id];
                        if (charge_item.hasOwnProperty('OTHCH')) {
                            delete charge_item['OTHCH'][row_id];
                            delete charge_item['OTHCH'][row_id];
                        }
                        delete item_arr[row_id];
                        BillDiscountcalculate();
                        var detail_id = $('#bill_detail_id_hidden' + row_id).val();
                        if (typeof (detail_id) != 'undefined') {
                            delete_array.push({
                                'detail_id': detail_id
                            });
                        }
                        $("#row_data_" + row_id).remove();
                    }

                }
            }
        });
    } else {
        $("#row_data_" + row_id).remove();
    }
    row_ct = 1;
    $(".row_class").each(function (i) {
        $(this).find('.row_count_class').text(row_ct); // row Count Re-arenge
        row_ct++;
    });

}


function updateDateNan() {
    $('#charge_amount_9').val(0.00);
    $('#charge_amount_10').val(0.00);
    $('#charge_amount_12').val(0.00);
    $('#charge_amount_13').val(0.00);
    $('#charges_tot_pop').val(0.00);
    $('#tot_rate_cal').val(0.00);
}


function validateText() {
    var flag = 1;
    var rate = $('#rate').val();
    if (!rate || rate == '0') {
        $('#rate').val(0);
        updateDateNan();
        flag = 0;
    }
    var grn_qty = $('#grn_qty').val();
    if (!grn_qty || grn_qty == '0') {
        $('#grn_qty').val(0);
        updateDateNan();
        flag = 0;
    }
    $(".number_class").each(function (index) {
        var val = $(this).val();
        if (isNaN(val)) {
            updateDateNan();
        } else if (val == 'Infinity') {
            updateDateNan();
        } else if (!val) {
            updateDateNan();
        } else if (val == 'NaN') {
            updateDateNan();
        }
        if ($(this).hasClass("calcu_percent")) {
            if (parseFloat(val) > 100) {
                updateDateNan();
            }
        }
    });
}

function fetchUOMData(item_id, row_id, uom_id) {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/uom_data";
    var _token = $('#_token').val();

    var item_code = $('#item_code_hidden' + row_id).val();
    var _token = $('#_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: _token,
            item_id: item_id,
            uom_id: uom_id,
            item_code: item_code,
            row_id: row_id,
            charge_item: charge_item,
            item_arr: item_arr
        },
        beforeSend: function () { },
        success: function (response) {
            response = JSON.parse(response);
            $('#uom_select_id_' + row_id).html(response.option);
            var item_ch_arr = response.item_arr;
            charges_dts = response.charges_dts;
            $.each(item_ch_arr, function (charge_status, charge) {
                if (typeof item_arr[row_id] == 'undefined')
                    item_arr[row_id] = {}
                item_arr[row_id]['uom_val'] = $('#uom_select_id_' + row_id + ' option').data(
                    "uom_value");
                item_arr[row_id]['grn_qty'] = charge.grn_qty;
                item_arr[row_id]['free_qty'] = charge.grn_free_qty;
                item_arr[row_id]['mrp'] = charge.grn_mrp;
                item_arr[row_id]['rate'] = charge.grn_rate;
                item_arr[row_id]['unit_check'] = charge.sales_rate_mrp_check;
                item_arr[row_id]['unit_mrp'] = charge.unit_mrp;
                item_arr[row_id]['grn_unit'] = charge.grn_unit;
                item_arr[row_id]['selling_price'] = charge.sales_rate;
            });
            if (item_ch_arr.length <= 0) {
                item_arr[row_id] = {}
                item_arr[row_id]['uom_val'] = $('#uom_select_id_' + row_id + ' option').data(
                    "uom_value");
                item_arr[row_id]['grn_qty'] = 0;
                item_arr[row_id]['free_qty'] = 0;
                item_arr[row_id]['mrp'] = 0;
                item_arr[row_id]['rate'] = 0;
                item_arr[row_id]['unit_check'] = 0;
                item_arr[row_id]['unit_mrp'] = 0;
                item_arr[row_id]['selling_price'] = 0;
            }
            if (charges_dts.length > 0) {
                $.each(charges_dts, function (charge_status, charge_val) {

                    if (charge_val.code == 'DISCOUNT') {
                        if (typeof charge_item['DISCOUNT'] == 'undefined')
                            charge_item['DISCOUNT'] = {}
                        if (typeof charge_item['DISCOUNT'][row_id] == 'undefined')
                            charge_item['DISCOUNT'][row_id] = {}
                        if (typeof charge_item['DISCOUNT'][row_id][charge_val.status_code] ==
                            'undefined')
                            charge_item['DISCOUNT'][row_id][charge_val.status_code] = {}
                    }
                    if (charge_val.code == 'TAX') {
                        if (typeof charge_item['TAX'] == 'undefined')
                            charge_item['TAX'] = {}
                        if (typeof charge_item['TAX'][row_id] == 'undefined')
                            charge_item['TAX'][row_id] = {}
                        if (typeof charge_item['TAX'][row_id][charge_val.status_code] ==
                            'undefined')
                            charge_item['TAX'][row_id][charge_val.status_code] = {}
                    }
                    if (charge_val.code == 'OTHCH') {
                        if (typeof charge_item['OTHCH'] == 'undefined')
                            charge_item['OTHCH'] = {}
                        if (typeof charge_item['OTHCH'][row_id] == 'undefined')
                            charge_item['OTHCH'][row_id] = {}
                        if (typeof charge_item['OTHCH'][row_id][charge_val.status_code] ==
                            'undefined')
                            charge_item['OTHCH'][row_id][charge_val.status_code] = {}
                    }
                    if (charge_val.code == 'FREE') {
                        if (typeof charge_item['FREE'] == 'undefined')
                            charge_item['FREE'] = {}
                        if (typeof charge_item['FREE'][row_id] == 'undefined')
                            charge_item['FREE'][row_id] = {}
                        if (typeof charge_item['FREE'][row_id][charge_val.status_code] ==
                            'undefined')
                            charge_item['FREE'][row_id][charge_val.status_code] = {}
                    }


                    if (charge_val.code == 'TAX') {
                        charge_item[charge_val.code][row_id][charge_val.status_code][
                            'table_id'
                        ] = charge_val.head_id;
                        charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                            charge_val.value;
                        charge_item[charge_val.code][row_id][charge_val.status_code]['tax'] =
                            charge_val.perc;
                    }
                    if (charge_val.code == 'DISCOUNT') {
                        if (charge_val.status_code == 'DAH') {

                            charge_item[charge_val.code][row_id][charge_val.status_code][
                                'table_id'
                            ] = charge_val.head_id;
                            charge_item[charge_val.code][row_id][charge_val.status_code][
                                'bill_dis_amt'
                            ] = charge_val.value;
                            charge_item[charge_val.code][row_id][charge_val.status_code][
                                'discount'
                            ] = charge_val.perc;
                        } else {

                            charge_item[charge_val.code][row_id][charge_val.status_code][
                                'table_id'
                            ] = charge_val.head_id;
                            charge_item[charge_val.code][row_id][charge_val.status_code][
                                'amount'
                            ] = charge_val.value;
                            charge_item[charge_val.code][row_id][charge_val.status_code][
                                'discount'
                            ] = charge_val.perc;
                        }
                    }
                    if (charge_val.code == 'FREE') {
                        charge_item[charge_val.code][row_id][charge_val.status_code][
                            'table_id'
                        ] = charge_val.head_id;
                        charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                            charge_val.value;
                        charge_item[charge_val.code][row_id][charge_val.status_code]['free'] =
                            charge_val.perc;
                    }
                    if (charge_val.code == 'OTHCH') {
                        charge_item[charge_val.code][row_id][charge_val.status_code][
                            'table_id'
                        ] = charge_val.head_id;
                        charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                            charge_val.value;
                        charge_item[charge_val.code][row_id][charge_val.status_code]['other'] =
                            charge_val.perc;
                    }
                });
            }
            chargesModel(row_id)
            BillDiscountcalculate();
        },
        complete: function () {
            if ($(".row_class:last").find("input[name='item_code_hidden[]']").val() != '') {
                getNewRowInserted();
            }
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function calcPercentage(obj, row_id, data_code, data_calc, data_charge, table_id, is_tax, is_disc_amount, item_oth_amt, charge_name, charge_code, cal_type) {
    validateText();
    if (!obj.value) {
        $('#' + obj.id).val(0);
    }
    $('#dataentryType').val(data_code);
    free_tax_amt = parseFloat($('#free_tax').val());
    // free_item = $('#free_item_id').val();
    grn_qty = parseFloat($('#grn_qty').val());
    free_qty = parseFloat($('#free_qty').val());
    rate = parseFloat($('#rate').val());
    mrp = parseFloat($('#mrp').val());
    unit_rate = parseFloat($('#unit_rate').val());
    unit_cost = parseFloat($('#unit_cost').val());
    unit_mrp = parseFloat($('#unit_mrp').val());

    uom_id = parseFloat($('#uom_select_id_' + row_id).val());
    uom_unit = parseFloat($('#uom_select_id_' + row_id + ' option').data("uom_value"));


    if (isNaN(rate)) { rate = 0; }
    if (isNaN(free_tax_amt)) { free_tax_amt = 0; }
    if (isNaN(grn_qty)) { grn_qty = 0; }
    if (isNaN(mrp)) { mrp = 0; }
    if (isNaN(unit_rate)) { unit_rate = 0; }
    if (isNaN(unit_cost)) { unit_cost = 0; }
    if (isNaN(unit_mrp)) { unit_mrp = 0; }
    if (isNaN(uom_unit)) { uom_unit = 0; }
    var total_qty = grn_qty * uom_unit;
    var free_qty = free_qty * uom_unit;
    var all_total_qty = total_qty + free_qty;
    var total_rate = rate * grn_qty;
    var total_mrp = mrp * grn_qty;
    if (isNaN(total_mrp)) { total_mrp = 0; }
    if (isNaN(total_rate)) { total_rate = 0; }
    if (isNaN(all_total_qty)) { all_total_qty = 0; }
    if (isNaN(free_qty)) { free_qty = 0; }
    if (isNaN(total_qty)) { total_qty = 0; }
    var tax_amt = 0;
    charge_value = obj.value;

    total_rate = grn_qty * rate;
    total_rate1 = grn_qty * rate;
    total_rate2 = total_rate1;
    dis_amount1 = 0;
    tem_arr = [];
    if (data_code == 'DISCOUNT') {
        if (typeof charge_item['DISCOUNT'] == 'undefined')
            charge_item['DISCOUNT'] = {}
        if (typeof charge_item['DISCOUNT'][row_id] == 'undefined')
            charge_item['DISCOUNT'][row_id] = {}
        if (typeof charge_item['DISCOUNT'][row_id][charge_code] == 'undefined')
            charge_item['DISCOUNT'][row_id][charge_code] = {}
        if (typeof charge_value != 'undefined') {
            if (data_code == 'DISCOUNT') {
                if (charge_code == 'SCHEMADISC') {
                    if ($('#charge_percentage_32')) {
                        charge_value = parseFloat($('#charge_percentage_32').val());
                        charge_item['DISCOUNT'][row_id][charge_code]['table_id'] = table_id;
                        charge_item['DISCOUNT'][row_id][charge_code]['discount'] = charge_value;
                    }
                } else {
                    charge_item['DISCOUNT'][row_id][charge_code]['table_id'] = table_id;
                    charge_item['DISCOUNT'][row_id][charge_code]['discount'] = charge_value;
                }

            }
        }
    }
    if (data_code == 'TAX') {
        if (typeof charge_item['TAX'] == 'undefined')
            charge_item['TAX'] = {}
        if (typeof charge_item['TAX'][row_id] == 'undefined')
            charge_item['TAX'][row_id] = {}
        if (typeof charge_item['TAX'][row_id][charge_code] == 'undefined')
            charge_item['TAX'][row_id][charge_code] = {}
        if (typeof charge_value != 'undefined') {
            if (data_code == 'TAX') {
                // tem_arr.push({'tax':charge_value});
                // tem_arr['tax']=charge_value;
                charge_item['TAX'][row_id][charge_code]['table_id'] = table_id;
                charge_item['TAX'][row_id][charge_code]['tax'] = charge_value;
            }
        }
    }
    if (data_code == 'OTHCH') {
        if (typeof charge_item['OTHCH'] == 'undefined')
            charge_item['OTHCH'] = {}
        if (typeof charge_item['OTHCH'][row_id] == 'undefined')
            charge_item['OTHCH'][row_id] = {}
        if (typeof charge_item['OTHCH'][row_id][charge_code] == 'undefined')
            charge_item['OTHCH'][row_id][charge_code] = {}
        if (typeof charge_item['OTHCH'][row_id][charge_code] != 'undefined') {
            charge_item['OTHCH'][row_id][charge_code]['table_id'] = table_id;
            charge_item['OTHCH'][row_id][charge_code]['other'] = charge_value;
        }
    }
    if (data_code == 'FREE') {
        if (typeof charge_item['FREE'] == 'undefined')
            charge_item['FREE'] = {}
        if (typeof charge_item['FREE'][row_id] == 'undefined')
            charge_item['FREE'][row_id] = {}
        if (typeof charge_item['FREE'][row_id][charge_code] == 'undefined')
            charge_item['FREE'][row_id][charge_code] = {}
        if (typeof charge_item['FREE'][row_id][charge_code] != 'undefined') {
            charge_item['FREE'][row_id][charge_code]['free'] = charge_value;
            charge_item['FREE'][row_id][charge_code]['table_id'] = table_id;
        }
    }
    priceCalculator(obj, row_id, cal_type);
}


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillItemValues(list, item_code, item_desc, item_id, uom_name, uom_id, uom_value) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#' + itemCodeTextId).val(htmlDecode(item_desc));
    $('#' + itemCodeTextId).attr('disabled', true);
    $('#' + itemCodeListDivId).hide();
    // alert(itemCodeListDivId+" -- "+itemCodeTextId ); return;

    $('#' + itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    $('#' + itemCodeTextId).closest("tr").find("input[name='item_id_hidden[]']").val(item_id);
    $('#' + itemCodeTextId).closest("tr").find("input[name='uom_name_hidden[]']").val(uom_name);
    $('#' + itemCodeTextId).closest("tr").find("input[name='uom_id_hidden[]']").val(uom_id);
    $('#' + itemCodeTextId).closest("tr").find("input[name='uom_val_hidden[]']").val(uom_value);
    var row_id = $('#grn_listrow_id').val();
    fetchUOMData(item_id, row_id, uom_id);
    item_history(row_id);

}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name) {

    vendor_name = htmlDecode(vendor_name);
    $("#inspected_by").attr("title", gst_vendor_code);
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#' + itemCodeTextId).val(vendor_name);
    $('#' + itemCodeListDivId).hide();

    $('#vendor_code_id').val(vendor_code);
    $('#vendor_id_js').val(id);
    checkGrnBillNo();

}

function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}

function searchvendorCode(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var location = $('#location').val();
        var base_url = $('#base_url').val();
        var url = base_url + "/purchase/vendor_search";
        var vendor_name = $('#' + id).val();
        if (vendor_name == "") {
            $("#" + ajax_div).html("");
        } else {
            setTimeout(function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        vendor_name: vendor_name
                    },
                    beforeSend: function () {
                        $("#" + ajax_div).html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () { },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }, 1000);
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}



function searchManufacturerCode(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var location = $('#location').val();
        var manufacturer_name = $('#' + id).val();
        var base_url = $('#base_url').val();
        var url = base_url + "/purchase/manufacturer_search";
        if (manufacturer_name == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    manufacturer_name: manufacturer_name
                },
                beforeSend: function () {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () { },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function fillManufacturerValues(list, id, manufacturer_code, manufacturer_name) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#' + itemCodeTextId).val(manufacturer_name);
    $('#' + itemCodeListDivId).hide();

    $('#manufacturer_code_id').val(manufacturer_code);
    $('#manufacturer_id_js').val(id);

}


function searchItemCode(id, event, row_id) { //alert(id);return;
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    $('#grn_listrow_id').val(row_id);
    var item_code = htmlDecode($('#' + id).val());
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/product_search";
    if (item_code) {
        if (value.match(keycheck)) {
            var location = $('#location').val();
            var list_status = 0;
            if ($("#getallgrnitems")) {
                list_status = $("#getallgrnitems").is(":checked");
            }

            var search_type = $('input[name="search_type_item"]:checked').val() ? $(
                'input[name="search_type_item"]:checked').val() : 'item_desc';
            var param = { item_code: item_code, location: location, search_type: search_type, list_status: list_status };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                    if (search_type && search_type == 'barcode') {
                        if ($("#" + ajax_div).find('li').length == 1) {
                            $("#" + ajax_div).find('li').first().click();
                        }
                    }
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    } else {
        $("#" + ajax_div).hide();
    }
}

function serach_po() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/Show_po";
    var _token = $('#_token').val();
    var po_department = $('#po_department').val();
    var po_date_from = $('#po_created_date_from').val();
    var po_date_to = $('#po_created_date_to').val();
    var po_id = $('#po_number_search_hidden').val();
    var vendor_code = $('#vendor_list_grn_hidden').val();
    var po_no = $('#po_number_search').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: _token,
            po_department: po_department,
            po_date_from: po_date_from,
            po_date_to: po_date_to,
            vendor_code: vendor_code,
            po_id: po_id,
            po_no: po_no
        },
        beforeSend: function () {
            $('#searchpobtn').attr('disabled', true);
            $('#posearchdataspin').removeClass('fa fa-serach');
            $('#posearchdataspin').addClass('fa fa-spinner fa-spin');
            $("#row_po_list").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            $('#po_list_model').modal('show');
            $('#row_po_list').html(html);
            $(".theadfix_wrapper").floatThead('reflow');
        },
        complete: function () {
            $('#searchpobtn').attr('disabled', false);
            $('#posearchdataspin').removeClass('fa fa-spinner fa-spin');
            $('#posearchdataspin').addClass('fa fa-serach');
            $("#row_po_list").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function priceCalculator(obj, row_id, cal_type = '') {

    free_tax_amt = parseFloat($('#free_tax').val());
    grn_qty = parseFloat($('#grn_qty').val());
    free_qty = parseFloat($('#free_qty').val());
    rate = parseFloat($('#rate').val());
    mrp = parseFloat($('#mrp').val());
    unit_rate = parseFloat($('#unit_rate').val());
    unit_cost = parseFloat($('#unit_cost').val());
    unit_mrp = parseFloat($('#unit_mrp').val());
    selling_price = parseFloat($('#selling_price').val());
    uom_id = parseFloat($('#uom_select_id_' + row_id).val());
    uom_unit = parseFloat($('#uom_select_id_' + row_id + ' option').data("uom_value"));
    unit_check = free_item = 0;
    if ($('#unit_check_box').prop("checked") == true) {
        unit_check = 1;
    }
    if ($('#free_item_id').prop("checked") == true) {
        free_item = 1;
    }
    if (typeof item_arr[row_id] == 'undefined')
        item_arr[row_id] = {}
    ExpiryEntry(obj, row_id)

    if (isNaN(rate)) { rate = 0; }
    if (isNaN(free_tax_amt)) { free_tax_amt = 0; }
    if (isNaN(grn_qty)) { grn_qty = 0; }
    if (isNaN(free_qty)) { free_qty = 0; }
    if (isNaN(mrp)) { mrp = 0; }
    if (isNaN(unit_rate)) { unit_rate = 0; }
    if (isNaN(unit_cost)) { unit_cost = 0; }
    if (isNaN(unit_mrp)) { unit_mrp = 0; }
    if (isNaN(selling_price)) { selling_price = 0; }
    if (isNaN(uom_unit)) { uom_unit = 0; }

    item_arr[row_id]['free_tax'] = free_tax_amt;
    item_arr[row_id]['grn_qty'] = grn_qty;
    item_arr[row_id]['free_qty'] = free_qty;
    item_arr[row_id]['rate'] = rate;
    item_arr[row_id]['mrp'] = mrp;
    item_arr[row_id]['unit_rate'] = unit_rate;
    item_arr[row_id]['unit_cost'] = unit_cost;
    item_arr[row_id]['unit_mrp'] = unit_mrp;
    item_arr[row_id]['selling_price'] = selling_price;
    item_arr[row_id]['unit_check'] = unit_check;
    item_arr[row_id]['is_free'] = free_item;
    item_arr[row_id]['tot_tax_amt'] = 0.0;
    item_arr[row_id]['tot_tax_perc'] = 0.0;

    calculateAll(row_id, cal_type, 1);
}

function removeWhiteSpace() {
    $(".number_class1").each(function (index) {
        var val = $(this).val();
        var res = val.replace(/\s+/g, "");
        $(this).val(res);
    });
}

function checkSellingPrice(row_id) {
    $('#selling_price_edited' + row_id).val(1);
}

function fetchUOMValue(row_id) {
    $('#uom_val').val($('#uom_select_pop_id_' + row_id + ' option:selected').data("uom_value"));
    $('#uom_select_id_' + row_id).val($('#uom_select_pop_id_' + row_id + ' option:selected').val());
    priceCalculator(this, row_id, '');
}
/*   calculate GRN price */
function calculateAll(row_id, cal_type = '', from_type) {
    var grn_id = $('#grn_id_hidden').val();
    var grn_status = $('#grn_status_hidden').val();
    if (parseInt(grn_id) != 0 && ((parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) && (parseInt(from_type) == 1)) {
        var free_qty = $('#free_qty').val();
        var batch_pop = $('#batch_pop').val();
        var Expiry_date_pop = $('#Expiry_date_pop').val();
        var grn_qty = parseFloat($('#grn_qty').val());
        var uom_val = $('#uom_val').val();
        var tot_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * parseFloat(uom_val);
        var org_qty = $('#tot_qty_' + row_id).val();
        var item_code = $('#batch_popup_itemcode').val();
        checkStockProductQty(tot_qty, row_id, cal_type, from_type, 3);
    } else {
        calculateFullData(row_id, cal_type, from_type, true);
    }

}

function calculateFullData(row_id_list, cal_type = '', from_type, flag) {
    setTimeout(function () {
        row_id = $('#grn_listrow_id').val();
        if (parseInt(row_id) == 0) {
            row_id = row_id_list;
        }
        uom_unit = parseFloat($('#uom_select_id_' + row_id + ' option:selected').data("uom_value"));
        if (item_arr.hasOwnProperty(row_id)) {
            free_tax_amt = parseFloat(item_arr[row_id]['free_tax']);
            grn_qty = parseFloat(item_arr[row_id]['grn_qty']);
            free_qty = parseFloat(item_arr[row_id]['free_qty']);
            rate = parseFloat(item_arr[row_id]['rate']);
            mrp = parseFloat(item_arr[row_id]['mrp']);
            unit_rate = parseFloat(item_arr[row_id]['unit_rate']);
            unit_cost = parseFloat(item_arr[row_id]['unit_cost']);
            if (isNaN(unit_cost)) { unit_cost = 0; }
            if (isNaN(mrp)) { mrp = 0; }
            unit_mrp = parseFloat(item_arr[row_id]['unit_mrp']);
            unit_check = parseFloat(item_arr[row_id]['unit_check']);
            selling_price = parseFloat(item_arr[row_id]['selling_price']);
            if (isNaN(selling_price)) { selling_price = 0; }
        } else {
            unit_check = 0;
            free_tax_amt = 0;
            grn_qty = 0;
            free_qty = 0;
            rate = 0;
            mrp = 0;
            unit_rate = 0;
            unit_cost = 0;
            unit_mrp = 0;
            unit_check = 0;
        }
        uom_id = parseFloat($('#uom_select_id_' + row_id).val());
        tot_dic_amt = parseFloat($('#tot_dic_amt_' + row_id).val());
        tot_tax_amt = parseFloat($('#tot_tax_amt_' + row_id).val());
        net_cost_cal = parseFloat($('#net_cost_' + row_id).val());
        if (isNaN(tot_dic_amt)) { tot_dic_amt = 0; }
        if (isNaN(tot_tax_amt)) { tot_tax_amt = 0; }
        if (isNaN(net_cost_cal)) { net_cost_cal = 0; }
        tot_unit_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * uom_unit;
        tot_grn_qty = parseFloat(grn_qty) * uom_unit;
        unit_rate = (rate * grn_qty) / tot_unit_qty;
        if (parseInt(from_type) == 1 && parseInt(unit_check) == 0) {
            var selling_edited = $('#selling_price_edited' + row_id).val();
            var selling_price = item_arr[row_id]['selling_price'] ? item_arr[row_id]['selling_price'] : 0;
            selling_price = (mrp * grn_qty) / tot_grn_qty;
            if (isNaN(selling_price)) {
                selling_price = 0;
            }
            $('#selling_price').val(selling_price.toFixed(parseInt(decimalConfiguration())));
        } else if (parseInt(from_type) != 1 && parseInt(unit_check) != 1) {
            selling_price = (mrp * grn_qty) / tot_grn_qty;
            if (isNaN(selling_price)) { selling_price = 0; }
            $('#selling_price').val(selling_price.toFixed(parseInt(decimalConfiguration())));
        } else if (parseInt(from_type) == 1 && parseInt(unit_check) == 1) {
            var selling_price = item_arr[row_id]['selling_price'] ? item_arr[row_id]['selling_price'] : 0;
            if (isNaN(selling_price)) {
                selling_price = 0;
            }
            if (parseInt(selling_price) == 0) {
                selling_price = (mrp * grn_qty) / tot_grn_qty;
                if (isNaN(selling_price)) { selling_price = 0; }
            }
            $('#selling_price').val(selling_price.toFixed(parseInt(decimalConfiguration())));
        }

        unit_mrp = (mrp * grn_qty) / tot_grn_qty;
        if (isNaN(unit_mrp)) { unit_mrp = 0; }
        $('#unit_mrp').val(unit_mrp.toFixed(parseInt(decimalConfiguration())));
        if (isNaN(unit_rate)) { unit_rate = 0; }

        $('#unit_rate').val(unit_rate.toFixed(parseInt(decimalConfiguration())));
        var total_qty = grn_qty * uom_unit;
        var tot_free_qty = free_qty * uom_unit;
        var all_total_qty = total_qty + tot_free_qty;
        var total_rate = rate * grn_qty;
        var total_free_rate = rate * free_qty;
        var total_mrp = mrp * grn_qty;
        if (isNaN(total_mrp)) { total_mrp = 0; }
        if (isNaN(total_rate)) { total_rate = 0; }
        if (isNaN(all_total_qty)) { all_total_qty = 0; }
        if (isNaN(total_qty)) { total_qty = 0; }
        if (isNaN(tot_free_qty)) { tot_free_qty = 0; }
        var tax_amt = 0;
        var tot_rate = 0;
        var tot_rate = grn_qty * rate;
        var tot_rate_pop = grn_qty * rate;
        var tot_mrp = grn_qty * mrp;
        var tot_dis_amt = 0;
        var data_code = $('#dataentryType').val();
        var data_code_main = $('#dataentryType_main').val();
        var discount_flag = 1;
        if (data_code == 'DISCOUNT' || data_code_main == 'EditSection') {
            if (typeof charge_item['DISCOUNT'] !== 'undefined') {
                $(charge_item['DISCOUNT'][row_id]).each(function (key, charge_vals) {
                    discount_flag = 0;
                    $.each(charge_vals, function (charge_status, charge_val1) {
                        $.each(charge_val1, function (charge_status1, charge_val) {
                            var tem_arr = [];
                            var cal_code = $("input[name='discount_" + charge_status + "_percentage[]']").data('calc');
                            var head_id = $("input[name='discount_" + charge_status + "_percentage[]']").data('id');
                            var dis_amount = 0;
                            if (charge_status1 == "discount" && cal_type == '2') {
                                if (charge_status == 'ITEMDISC') {
                                    total_rateget = total_rate;
                                    if (from_type == '1') {
                                        charge_val = $('#charge_amount_1').val();
                                    }
                                    total_rate = total_rate - charge_val;
                                    tot_percentage = (charge_val / total_rateget) * 100;

                                    charge_item['DISCOUNT'][row_id][charge_status]['amount'] = dis_amount.toFixed(parseInt(decimalConfiguration()));
                                    charge_item['DISCOUNT'][row_id][charge_status]['cal_type'] = 2;
                                    if (isNaN(tot_percentage.toFixed(2))) {
                                        $('#charge_percentage_1').val(0);
                                    } else {
                                        $('#charge_percentage_1').val(tot_percentage.toFixed(parseInt(decimalConfiguration())));
                                    }
                                } else if (charge_status == 'SCHEMADISC') {
                                    total_rateget = total_rate;
                                    if (from_type == '1') {
                                        charge_val = $('#charge_amount_32').val();
                                    }
                                    total_rate = total_rate - charge_val;
                                    tot_percentage = (charge_val / total_rateget) * 100;
                                    charge_item['DISCOUNT'][row_id][charge_status]['cal_type'] = 2;
                                    if (isNaN(tot_percentage.toFixed(2))) {
                                        $('#charge_percentage_32').val(0);
                                    } else {
                                        $('#charge_percentage_32').val(tot_percentage.toFixed(parseInt(decimalConfiguration())));
                                    }
                                }
                                tot_dis_amt = total_rate;

                            } else if (charge_status1 == "discount") {

                                if (charge_status == 'ITEMDISC') {
                                    if (from_type == '1') {
                                        charge_val = $('#charge_percentage_1').val();
                                    }
                                    dis_amount = total_rate * charge_val / 100;
                                    total_rate = total_rate - dis_amount;
                                    charge_item['DISCOUNT'][row_id][charge_status]['amount'] = dis_amount.toFixed(parseInt(decimalConfiguration()));
                                    charge_item['DISCOUNT'][row_id][charge_status]['cal_type'] = 1;
                                    if (isNaN(dis_amount.toFixed(2))) {
                                        $('#charge_amount_1').val(0);
                                    } else {
                                        $('#charge_amount_1').val(dis_amount.toFixed(parseInt(decimalConfiguration())));
                                    }
                                } else if (charge_status == 'SCHEMADISC') {
                                    if (from_type == '1') {
                                        charge_val = $('#charge_percentage_32').val();
                                    }
                                    dis_amount = total_rate * charge_val / 100;
                                    total_rate = total_rate - dis_amount;
                                    charge_item['DISCOUNT'][row_id][charge_status]['cal_type'] = 1;
                                    if (isNaN(dis_amount.toFixed(2))) {
                                        $('#charge_amount_32').val(0);
                                    } else {
                                        $('#charge_amount_32').val(dis_amount.toFixed(parseInt(decimalConfiguration())));
                                    }
                                }
                                var dis_amount_bill = 0;
                                if (charge_status == 'DAH') {
                                    dis_amount_bill = total_rate * charge_val / 100;
                                    total_rate = total_rate - dis_amount_bill;
                                    if (typeof charge_item['DISCOUNT'][row_id]['bill_dis_amt'] == 'undefined')
                                        charge_item['DISCOUNT'][row_id][charge_status]['bill_dis_amt'] = dis_amount_bill.toFixed(parseInt(decimalConfiguration()));
                                    charge_item['DISCOUNT'][row_id][charge_status]['cal_type'] = 1;
                                    if (isNaN(dis_amount_bill.toFixed(2))) {
                                        $('#charge_amount_1').val(0);
                                    } else {
                                        $('#charge_amount_1').val(dis_amount_bill.toFixed(parseInt(decimalConfiguration())));
                                    }

                                }
                                tot_dis_amt = tot_dis_amt + dis_amount + dis_amount_bill;
                            }

                            if (charge_status1 == "discount") {
                                if (charge_status == 'SCHEMADISC') {
                                    if ($('#charge_amount_32')) {
                                        var amt = parseFloat($('#charge_amount_32').val());
                                        charge_item['DISCOUNT'][row_id][charge_status]['amount'] = amt.toFixed(parseInt(decimalConfiguration()));
                                    }
                                }
                            }
                        });
                    });
                });
                /* +++++++++++++++++++++++++++++++++++++++++++++ */
            }
        }
        if (discount_flag == 1 && from_type == '1') {
            var total_rate = rate * grn_qty;
            var discount_per = $('#charge_percentage_1').val();
            discount_amt = (total_rate * discount_per) / 100;
            total_rate = total_rate - discount_amt;
        }
        var tax = tot_tax_amt = tot_tax_amt_pop = tot_tax_perc = 0;
        if (typeof charge_item['TAX'] !== 'undefined') {
            $(charge_item['TAX'][row_id]).each(function (key, charge_vals) {
                $.each(charge_vals, function (charge_status, charge_val1) {
                    $.each(charge_val1, function (charge_status1, charge_val) {
                        var tem_arr = [];
                        var cal_code = $("input[name='charge_" + charge_status + "_percentage[]']").data('calc');
                        var head_id = $("input[name='charge_" + charge_status + "_percentage[]']").data('id');

                        if (typeof charge_item['TAX'][row_id] != 'undefined') {
                            if (charge_status1 == 'tax') {

                                tax = (total_rate * parseFloat(charge_val)) / 100;
                                charge_item['TAX'][row_id][charge_status]['amount'] = tax.toFixed(parseInt(decimalConfiguration()));

                                if (isNaN(tax.toFixed(2))) {
                                    $('#charge_amount_' + head_id).val(0);
                                } else {
                                    $('#charge_amount_' + head_id).val(tax.toFixed(parseInt(decimalConfiguration())));
                                }

                                if (isNaN(tax.toFixed(2))) {
                                    $('#charge_percentage' + head_id).val(0);
                                } else {
                                    $('#charge_percentage' + head_id).val(tax.toFixed(parseInt(decimalConfiguration())));
                                }

                                tot_tax_amt += parseFloat(tax);
                                tot_tax_perc += parseFloat(charge_val);
                                tot_tax_amt_pop += tax;
                            }
                        }

                    });
                });
            });
            if (typeof item_arr[row_id] == 'undefined')
                item_arr[row_id] = {}
            item_arr[row_id]['tot_tax_amt'] = tot_tax_amt;
            item_arr[row_id]['tot_tax_perc'] = tot_tax_perc.toFixed(parseInt(decimalConfiguration()));
            total_rate = total_rate + tot_tax_amt;
            /* ====================================================== */
        }
        var free_tax = free_tot_tax_amt = 0;
        if (typeof charge_item['FREE'] !== 'undefined') {

            $(charge_item['FREE'][row_id]).each(function (key, charge_vals) {
                $.each(charge_vals, function (charge_status, charge_val1) {
                    $.each(charge_val1, function (charge_status1, charge_val) {
                        var tem_arr = [];
                        var cal_code = $("input[name='charge_" + charge_status + "_percentage[]']").data('calc');
                        var head_id = $("input[name='charge_" + charge_status + "_percentage[]']").data('id');

                        if (typeof charge_item['FREE'][row_id] != 'undefined') {
                            if (charge_status1 == 'free') {

                                free_tax = (total_free_rate * charge_val) / 100;
                                charge_item['FREE'][row_id][charge_status]['amount'] = free_tax.toFixed(parseInt(decimalConfiguration()));
                                if (isNaN(free_tax.toFixed(2))) {
                                    $('#charge_amount_' + head_id).val(0);
                                } else {
                                    $('#charge_amount_' + head_id).val(free_tax.toFixed(parseInt(decimalConfiguration())));

                                }

                            }
                        }

                    });
                });
            });
            /* ====================================================== */
        }
        var other = 0;
        if (typeof charge_item['OTHCH'] !== 'undefined') {
            $(charge_item['OTHCH'][row_id]).each(function (key, charge_vals) {
                $.each(charge_vals, function (charge_status, charge_val1) {
                    $.each(charge_val1, function (charge_status1, charge_val) {
                        // var  tem_arr =[];
                        var cal_code = $("input[name='charge_" + charge_status + "_percentage[]']").data('calc');
                        var head_id = $("input[name='charge_" + charge_status + "_percentage[]']").data('id');
                        if (charge_status1 == "other") {
                            other += parseFloat(charge_val);

                            charge_item['OTHCH'][row_id][charge_status]['amount'] = charge_val;
                        }
                    });
                });
            });
            total_rate += other;
        }

        if (flag == true) {
            unit_cost = (total_rate) / tot_unit_qty;
            var item_code1 = $("#item_code_hidden" + row_id).val();
            if (item_code1 != '') {
                $('#row_data_' + row_id).closest("tr").find("input[name='grn_qty[]']").val(grn_qty);
                $('#row_data_' + row_id).closest("tr").find("input[name='free_qty[]']").val(tot_free_qty);
                $('#row_data_' + row_id).closest("tr").find("input[name='tot_qty[]']").val(all_total_qty);
                $('#row_data_' + row_id).closest("tr").find("input[name='tot_rate[]']").val(tot_rate.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='tot_mrp[]']").val(tot_mrp.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='unit-cost[]']").val(unit_cost.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='unit-mrp[]']").val(unit_mrp.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='sales_price[]']").val(selling_price.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='unit_mrp_hidden[]']").val(unit_mrp.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='unit_cost_hidden[]']").val(unit_cost.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='unit_rate_hidden[]']").val(unit_rate.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='rate_hidden[]']").val(rate.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='mrp_hidden[]']").val(mrp.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='qty_hidden[]']").val(grn_qty);
                $('#row_data_' + row_id).closest("tr").find("input[name='tot_tax_amt[]']").val(tot_tax_amt.toFixed(parseInt(decimalConfiguration())));
                $('#row_data_' + row_id).closest("tr").find("input[name='free_qty_hidden[]']").val(free_qty);
                $('#tot_dic_amt_' + row_id).val(tot_dis_amt.toFixed(parseInt(decimalConfiguration())));
                $('#net_cost_' + row_id).val(total_rate.toFixed(parseInt(decimalConfiguration())));
            }

            if (isNaN(unit_cost)) { unit_cost = 0; }
            $('#unit_cost').val(unit_cost.toFixed(parseInt(decimalConfiguration())));
            var tax_tot = 0;
            var gross_tot = 0;
            var tot_net_amt = tot_net_amt_footer = 0;
            var tot_dicount_hd = 0;
            var tot_tax_hd = 0;
            item_array = new Array();
            var net_rate = 0;
            $('#row_body_data tr').each(function () {

                var item_code = $(this).find("input[name='item_code_hidden[]']").val();
                if (item_code != '') {
                    var row_id = $(this).find("input[name='row_id_hidden[]']").val();

                    var uom_val = $('#uom_select_id_' + row_id + ' option').data("uom_value");
                    var unit_cost = $(this).find("input[name='unit_cost_hidden[]']").val();
                    var unit_mrp = $(this).find("input[name='unit_mrp_hidden[]']").val();
                    var free_qty = $(this).find("input[name='free_qty_hidden[]']").val();

                    var free_qty = parseFloat($(this).find("input[name='free_qty[]']").val());
                    var tot_qty = parseFloat($(this).find("input[name='tot_qty[]']").val());
                    var tot_rate = parseFloat($(this).find("input[name='tot_rate[]']").val());
                    var tot_mrp = parseFloat($(this).find("input[name='tot_mrp[]']").val());
                    var tot_tax_amt = parseFloat($(this).find("input[name='tot_tax_amt[]']").val());
                    var tot_dic_amt = parseFloat($(this).find("input[name='tot_dic_amt[]']").val());
                    var net_cost = parseFloat($(this).find("input[name='net_cost[]']").val());
                    var unit_mrp = parseFloat($(this).find("input[name='unit_mrp_hidden[]']").val());
                    var unit_rate = parseFloat($(this).find("input[name='unit_rate_hidden[]']").val());
                    var rate = parseFloat($(this).find("input[name='rate_hidden[]']").val());
                    var mrp = parseFloat($(this).find("input[name='mrp_hidden[]']").val());
                    var grn_qty = parseFloat($(this).find("input[name='qty_hidden[]']").val());
                    var free_qty = parseFloat($(this).find("input[name='free_qty_hidden[]']").val());

                    if (isNaN(grn_qty)) { grn_qty = 0; }
                    if (isNaN(free_qty)) { free_qty = 0; }
                    if (isNaN(tot_qty)) { tot_qty = 0; }
                    if (isNaN(tot_mrp)) { tot_mrp = 0; }
                    if (isNaN(tot_rate)) { tot_rate = 0; }
                    if (isNaN(tot_dic_amt)) { tot_dic_amt = 0; }
                    if (isNaN(tot_tax_amt)) { tot_tax_amt = 0; }
                    if (isNaN(net_cost)) { net_cost = 0; }
                    if (isNaN(unit_mrp)) { unit_mrp = 0; }
                    if (isNaN(unit_cost)) { unit_cost = 0; }
                    if (isNaN(unit_rate)) { unit_rate = 0; }
                    if (isNaN(rate)) { rate = 0; }
                    if (isNaN(mrp)) { mrp = 0; }
                    if (isNaN(free_qty)) { free_qty = 0; }
                    if (isNaN(grn_qty)) { grn_qty = 0; }
                    tax_tot += tot_tax_amt;


                    gross_tot += (net_cost + tot_dic_amt) - tot_tax_amt;

                    tot_net_amt += net_cost;
                    tot_dicount_hd += tot_dic_amt;
                    tot_tax_hd += tot_tax_amt;
                }
            });
            net_rate = (tot_rate_pop - tot_dis_amt) + tot_tax_amt_pop + other;
            tot_rate = grn_qty * rate;
            $('#tot_rate_cal').val(tot_rate.toFixed(parseInt(decimalConfiguration())));
            $('#charges_tot_pop').val(total_rate.toFixed(parseInt(decimalConfiguration())));
            var round_off = parseFloat($('#round_off').val());
            if (isNaN(round_off)) { round_off = 0; }
            tot_net_amt_footer = tot_net_amt;
            tot_net_amt = tot_net_amt + round_off;
            $('#tot_net_amt_footer').text(tot_net_amt_footer.toFixed(parseInt(decimalConfiguration())));
            $('#tot_dicount_footer').text(tot_dicount_hd.toFixed(parseInt(decimalConfiguration())));
            $('#tot_tax_footer').text(tot_tax_hd.toFixed(parseInt(decimalConfiguration())));
            $('#net_amount_hd').val(tot_net_amt.toFixed(parseInt(decimalConfiguration())));
            $('#tot_tax_amt_hd').val(tax_tot.toFixed(parseInt(decimalConfiguration())));
            $('#discount_hd').val(tot_dicount_hd.toFixed(parseInt(decimalConfiguration())));
            $('#gross_amount_hd').val(gross_tot.toFixed(parseInt(decimalConfiguration())));

            $('.charges_class').text(0);
            $(charge_item).each(function (key, charge_vals) {
                $.each(charge_vals, function (charge_status, charge_val1) {
                    $.each(charge_val1, function (charge_status1, charge_val2) {
                        $.each(charge_val2, function (charge_status, charge_val3) {
                            cal_charge = 0;
                            var charge = parseFloat($('#' + charge_status + '_amot').text());
                            if (isNaN(charge)) { charge = 0; }
                            cal_charge = charge + parseFloat(charge_val3.amount);
                            $('#' + charge_status + '_amot').text(cal_charge.toFixed(parseInt(decimalConfiguration())));

                        });
                    });
                });
            });
        }
    }, 500);
}

function decimalConfiguration() {
    var decimalConfiguration = $('#decimal_configuration').val();
    return parseInt(decimalConfiguration);
}

function calRoundOff() {
    BillDiscountcalculate();
}
function unitMRPCh() {
    if ($("#unit_check_box").is(":checked")) {
        $('#selling_price').prop('readonly', false);
    } else {
        $('#selling_price').prop('readonly', true);
    }
}
function updateHsnCode(row_id) {
    if (typeof item_arr[row_id] == 'undefined')
        item_arr[row_id] = {}
    hsn_code = $('#hsn_code').val();
    item_arr[row_id]['hsn_code'] = hsn_code;
}

function BillDiscountcalculate() {
    $('#get_otherchargespopup').hide();
    var disc_type = $('#bill_discount_type').val();
    var disc_value = $('#bill_discount_value').val();
    var disc_mode = $('#disc_mode').val();
    if (disc_type == '') {
        toastr.warning("Choose a discount type");
        return;
    }
    if (disc_mode == '') {
        toastr.warning("Select discount mode");
        return;
    }
    if (disc_value == "") {
        $('#bill_discount_value').val(parseFloat(0.00));
    }
    if (isNaN(disc_value)) {
        toastr.warning("Discount value not valid");
        return;

    } else {
        if (disc_value < 0 || disc_value > 100) {
            if (disc_type == 1) {
                toastr.warning("Discount value not valid");
                return;
            }
        }

    }

    $("#row_body_data tr").each(function (index) {
        row_id = $(this).find("input[name='row_id_hidden[]']").val();
        calculateAll(row_id, '', 2);
    });

    setTimeout(function () {
        calculateBillwiseDiscount(disc_type, disc_value, disc_mode);
    }, 1500);
}


function calculateBillwiseDiscount(disc_type, disc_value, disc_mode) {
    var net_amount = parseFloat($('#net_amount_hd').val());
    var net_amount_fl = 0;
    var bill_dic = 0;
    var net_discount = parseFloat($('#discount_hd').val());

    if (isNaN(net_amount)) { net_amount = 0; }
    if (isNaN(net_discount)) { net_discount = 0; }
    if (disc_mode == 2) {
        if (disc_type == 1) {
            bill_dic = (net_amount * disc_value) / 100
            net_amount_fl = net_amount - bill_dic;
            $('.total_discount').text(bill_dic.toFixed(parseInt(decimalConfiguration())));
        } else {
            net_amount_fl = net_amount - disc_value;
            $('.total_discount').text(disc_value);
        }
    }
    tot_dic = bill_dic + net_discount;
    $('#discount_hd').val(tot_dic.toFixed(2))
    $('#net_amount_hd').val(net_amount_fl.toFixed(2))
}


function freeItem(obj, row_id) {
    $('#rate').val(0);
    if ($("#free_item_id").is(":checked") == true) {
        $('#rate').prop('readonly', true);
    } else {
        $('#rate').prop('readonly', false);
    }
    priceCalculator(obj, row_id, '');
}

function blockSpecialChar(e) {
    var k = e.keyCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
}

function checkGrnBillNo(post_type = '', print_type = '') {
    var bill_no = $('#bill_no').val();
    var bill_id = $('#bill_id_hidden').val();
    var vendor_code = $('#vendor_code_id').val();
    var duplication_check = $("#bill_duplication_check").is(':checked');
    if (!duplication_check) {
        if (bill_no && vendor_code) {
            var base_url = $('#base_url').val();
            var url = base_url + "/purchase/checkGrnBillNo";
            var param = { bill_no: bill_no, bill_id: bill_id, vendor_code: vendor_code };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                },
                success: function (data) {
                    if (parseInt(data) != 0) {
                        toastr.warning("Duplicate Bill Number: " + bill_no);
                        $('#bill_no').val('');
                    } else {
                        if (post_type) {
                            grn_savedata(post_type, print_type);
                        }
                    }
                },
                complete: function () {
                    $("body").LoadingOverlay("hide");
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            if (post_type) {
                grn_savedata(post_type, print_type);
            }
        }
    } else {
        if (post_type) {
            grn_savedata(post_type, print_type);
        }
    }
}


function checkGrnItems() {
    checkGrn = [];
    $('#row_body_data tr').each(function () {
        var item_code = $(this).find("input[name='item_code_hidden[]']").val();
        if (item_code != '') {
            checkGrn.push(item_code);
        }
    });
    return checkGrn;
}


function getPurchaseCategory() {

    var base_url = $('#base_url').val();
    var purchaseCategoryID = $('#purchaseCategoryIdhidden').val();
    var location = $('#location').val();
    var url = base_url + "/purchase/getPurchaseCategory";
    var param = { location: location, purchaseCategoryID: purchaseCategoryID };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#purchase_category_changediv').html(data);
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}


function AddPo(po_id, amend) {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/po_add_d";
    var push_state = base_url + "/purchase/grn";
    window.history.pushState('', '', push_state);
    var _token = $('#_token').val();
    var vendor_code = $('#vendor_list_grn_hidden').val();
    var rowCount = 0;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: _token,
            po_id: po_id,
            amend: amend
        },
        beforeSend: function () { },
        success: function (json_array) {
            $('#po_list_model').modal('hide');
            var data = JSON.parse(json_array);
            charge_item = data.charge_c_item;
            item_arr = data.item_c_array;
            batch_validate = data.batch_validate;
            rowCount = data.rowCount;
            $('#row_body_data').html(data.html);
            $(".expiry_date").datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $('#po_date').val(data.head.po_date);
            $('#po_no').val(data.head.po_no);
            $('#location option').filter(function () {
                return ($(this).val() == data.head.location); //To select Blue
            }).prop('selected', true);
            $('#location').prop('disabled', true);
            $('#inspected_by').val(data.vendor.vendor_name);
            $('#inspected_by').prop('disabled', true);
            $('#vendor_code_id').val(data.vendor.vendor_code);
            $('#vendor_id_js').val(data.vendor.id);
            $('#bill_discount_value').val(data.head.bill_discount_amount);
            $('#bill_discount_type').val(2);
        },
        complete: function () {
            BillDiscountcalculate();
            $('#row_count_id').val(rowCount);
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function addToGRN() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/po_add_d";
    var push_state = base_url + "/purchase/grn";
    window.history.pushState('', '', push_state);
    var _token = $('#_token').val();
    var vendor_code = $('#vendor_list_grn_hidden').val();
    var po_id = $('#pop_po_no').val();
    var amend = $('#iSammend').val();
    po_detail_id = new Array();
    $('.po_dl_id:checked').each(function (i, obj) {

        po_detail_id.push(this.value);
    });
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: _token,
            po_detail_id: po_detail_id,
            po_id: po_id,
            amend: amend
        },
        beforeSend: function () { },
        success: function (json_array) {
            $('#po_list_detail').modal('hide');
            var data = JSON.parse(json_array);
            charge_item = data.charge_c_item;
            item_arr = data.item_c_array;
            rowCount = data.rowCount;
            batch_validate = data.batch_validate;
            $('#row_body_data').html(data.html);
            $(".expiry_date").datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $('#po_date').val(data.head.po_date);
            $('#po_no').val(data.head.po_no);
            $('#po_no').val(data.head.po_no);
            $('#location option').filter(function () {
                return ($(this).val() == data.head.location); //To select Blue
            }).prop('selected', true);
            $('#location').prop('disabled', true);
            $('#inspected_by').val(data.vendor.vendor_name);
            $('#vendor_code_id').val(data.vendor.vendor_code);
            $('#vendor_id_js').val(data.vendor.id);
            BillDiscountcalculate();
        },
        complete: function () { },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

$('#poCheckBox').click(function () {
    if ($("#poCheckBox").is(":checked") == true) {
        $('#po_list_model').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#po_list_model').modal('show');
        // $('#po_list_model').modal({backdrop: 'static', keyboard: false})
    } else {
        var base_url = $('#base_url').val();
        var push_state = base_url + "/purchase/grn";
        window.history.pushState('', '', push_state);
        location.reload();
    }
});

function showBillNo(post_type, grn_no) {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/billList";
    var title = '';
    if (post_type == 1)
        title = "GRN Saved Successfuly";
    if (post_type == 2)
        title = "GRN Approved Successfuly";
    if (post_type == 3)
        title = "GRN Rejected Successfuly";
    if (post_type == 4)
        title = "GRN Closed Successfuly";
    bootbox.alert({
        title: title,
        message: "GRN No. :" + grn_no,
        callback: function () {
            window.location.href = url;
        }
    });
}

