$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    setLocalStorageData();
    setTimeout(function () {
        searchGrnList();
    }, 500);
});

var base_url = $('#base_url').val();
var token = $('#token').val();

function addBillNew() {
    var grnviewtype = $('#grnviewtype').val();
    var main_uri = base_url + "/purchase/grn";
    var insert_from = $('#insert_from').val();
    if (parseInt(insert_from) == 2) {
        main_uri = base_url + "/grn/addEditGeneralStoreGrn/";
    } else if (parseInt(grnviewtype) == 2) {
        main_uri = base_url + "/grn/addEditGrn";
    }
    document.location.href = main_uri;
}

function setLocalStorageData() {

    var default_location = $('#default_location').val();
    $('#from_date').val(localStorage.getItem('from_date') ? localStorage.getItem('from_date') : '');
    $('#to_date').val(localStorage.getItem('to_date') ? localStorage.getItem('to_date') : '');
    $('#grn_no_search').val(localStorage.getItem('grn_no') ? localStorage.getItem('grn_no') : '');
    $('#grn_id_hidden').val(localStorage.getItem('grn_id') ? localStorage.getItem('grn_id') : '');
    $('#bill_no_search').val(localStorage.getItem('bill_no') ? localStorage.getItem('bill_no') : '');
    $('#bill_id_hidden').val(localStorage.getItem('bill_id') ? localStorage.getItem('bill_id') : '');
    $('#vendor_name_search').val(localStorage.getItem('vendor_name') ? (localStorage.getItem('vendor_name')) : '');
    $('#vendoritem_id_hidden').val(localStorage.getItem('vendor_id') ? (localStorage.getItem('vendor_id')) : '');
    $('#item_desc').val(localStorage.getItem('item_desc') ? localStorage.getItem('item_desc') : '');
    $('#item_desc_hidden').val(localStorage.getItem('item_code') ? localStorage.getItem('item_code') : '');
    $('#to_location').val(localStorage.getItem('store_location') ? localStorage.getItem('store_location') : default_location);
    $('#status').val(localStorage.getItem('status') ? parseInt(localStorage.getItem('status')) : '');

}

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

function searchGrnList() {
    var url = base_url + "/eyegrn/searchEyeGrnList";
    $('#re_edit_modal').modal('hide');
    $('#grn_edit_details').html('');
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var grn_id = $('#grn_id_hidden').val();
    var bill_no = $('#bill_no_search').val();
    var vendor_id = $('#vendoritem_id_hidden').val();
    var item_desc = $('#item_desc').val();
    var item_code = $('#item_desc_hidden').val();
    var location = $('#to_location').val();
    var status = $('#status').val();
    var grn_no = $('#grn_no_search').val();
    var bill_id = $('#bill_id_hidden').val();
    var vendor_name = $('#vendor_name_search').val();
    var insert_from = $('#insert_from').val();

    localStorage.setItem("from_date", from_date);
    localStorage.setItem("to_date", to_date);
    localStorage.setItem("grn_no", grn_no);
    localStorage.setItem("grn_id", grn_id);
    localStorage.setItem("bill_no", bill_no);
    localStorage.setItem("bill_id", bill_id);
    localStorage.setItem("vendor_id", vendor_id);
    localStorage.setItem("vendor_name", vendor_name);
    localStorage.setItem("item_desc", item_desc);
    localStorage.setItem("item_code", item_code);
    localStorage.setItem("store_location", location);
    localStorage.setItem("status", status);

    var param = {
        _token: token,
        grn_id: grn_id,
        from_date: from_date,
        to_date: to_date,
        vendor_id: vendor_id,
        item_code: item_code,
        bill_no: bill_no,
        location: location,
        insert_from: insert_from,
        status: status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchGrnListData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $('#searchgrnlistBtn').attr('disabled', true);
            $('#searchgrnlistSpin').removeClass('fa fa-search');
            $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchGrnListData').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#searchGrnListData").LoadingOverlay("hide");
            $('#searchgrnlistBtn').attr('disabled', false);
            $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchgrnlistSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getGrnListPreview() {
    var url = base_url + "/purchase/getGrnFullList";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var grn_id = $('#grn_id_hidden').val();
    var bill_no = $('#bill_no_search').val();
    var vendor_id = $('#vendoritem_id_hidden').val();
    var item_code = $('#item_desc_hidden').val();
    var location = $('#to_location').val();
    var status = $('#status').val();
    var param = {
        _token: token,
        grn_id: grn_id,
        from_date: from_date,
        to_date: to_date,
        vendor_id: vendor_id,
        item_code: item_code,
        bill_no: bill_no,
        location: location,
        status: status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#grnlistpreviewBtn').attr('disabled', true);
            $('#grnlistpreviewSpin').removeClass('fa fa-eye');
            $('#grnlistpreviewSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#grnlist_div").html(data)
            $("#grnlist_model").modal({
                backdrop: 'static',
                keyboard: false
            });

        },
        complete: function () {
            $('#grnlistpreviewBtn').attr('disabled', false);
            $('#grnlistpreviewSpin').removeClass('fa fa-spinner fa-spin');
            $('#grnlistpreviewSpin').addClass('fa fa-eye');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function grn_listview(from_type, grn_id) {
    var url = base_url + "/purchase/listGrnItems";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var bill_no = $('#bill_no').val();
    var vendor_code = '';
    var flag = true;
    if (from_type == '2') {
        vendor_code = $('#search_vendor_code_hidden').val();
        if (!vendor_code) {
            flag = false;
        }
    }
    if (flag) {
        var param = {
            _token: token,
            grn_id: grn_id,
            vendor_code: vendor_code,
            from_date: from_date,
            to_date: to_date,
            from_type: from_type,
            bill_no: bill_no
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                if (from_type == '2') {
                    $('#Vendordatabtn').attr('disabled', true);
                    $('#Vendordataspin').removeClass('fa fa-industry');
                    $('#Vendordataspin').addClass('fa fa-spinner fa-spin');
                } else if (from_type == '1') {
                    $('#grn_listviewbtn' + grn_id).attr('disabled', true);
                    $('#grn_listviewspin' + grn_id).removeClass('fa fa-info');
                    $('#grn_listviewspin' + grn_id).addClass('fa fa-spinner fa-spin');
                }
            },
            success: function (data) {
                $("#grnlist_div").html(data)
                $("#grnlist_model").modal({
                    backdrop: 'static',
                    keyboard: false
                });

            },
            complete: function () {
                if (from_type == '1') {
                    $('#grn_listviewbtn' + grn_id).attr('disabled', false);
                    $('#grn_listviewspin' + grn_id).removeClass('fa fa-spinner fa-spin');
                    $('#grn_listviewspin' + grn_id).addClass('fa fa-info');
                } else if (from_type == '2') {
                    $('#Vendordatabtn').attr('disabled', false);
                    $('#Vendordataspin').removeClass('fa fa-spinner fa-spin');
                    $('#Vendordataspin').addClass('fa fa-industry');
                }
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select a Vendor");
    }
}

function billEditLoadData(grn_type, stock_id, insert_from) {
    var main_uri = base_url + "/purchase/grn/" + stock_id;
    if (parseInt(grn_type) == 1) {
        main_uri = base_url + "/eyegrn/addEditEyeLensGrn/" + stock_id;
    } else if (parseInt(grn_type) == 2) {
        main_uri = base_url + "/eyeframegrn/addEditEyeFrameGrn/" + stock_id;
    }
    document.location.href = main_uri;
}

//----------- Item Detail search------------
$('#item_desc').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('item_desc_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#item_desc_hidden').val() != "") {
            $('#item_desc_hidden').val('');
            $("#item_desc_AjaxDiv").hide();
        }
        var item_desc = $(this).val();
        if (item_desc == "") {
            $("#item_desc_AjaxDiv").html("").hide();
        } else {
            var url = base_url + "/grn/ProductNameSearch";

            $.ajax({
                type: "GET",
                url: url,
                data: "item_desc=" + item_desc,
                beforeSend: function () {
                    $("#item_desc_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#item_desc_AjaxDiv").html(html).show();
                    $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('item_desc_AjaxDiv', event);
    }
});


$('#vendor_name_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxVendorSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $('#vendor_name_search').val();
        if (vendor_name == "") {
            $('#ajaxVendorSearchBox').hide();
            $("#vendoritem_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    vendor_name: vendor_name,
                    row_id: 1
                },
                beforeSend: function () {
                    $("#ajaxVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxVendorSearchBox").html(html).show();
                    $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxVendorSearchBox', event);
    }
});


$('#grn_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxGrnNoSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var grn_number = $('#grn_no_search').val();
        if (grn_number == "") {
            $('#ajaxGrnNoSearchBox').hide();
            $("#grn_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    search_key_id: 'grn_number',
                    search_key: grn_number
                },
                beforeSend: function () {
                    $("#ajaxGrnNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxGrnNoSearchBox").html(html).show();
                    $("#ajaxGrnNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxGrnNoSearchBox', event);
    }
});


$('#bill_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxBillNoSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#bill_no_search').val();
        if (bill_no == "") {
            $('#ajaxBillNoSearchBox').hide();
            $("#bill_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    search_key_id: 'invoice_number',
                    search_key: bill_no
                },
                beforeSend: function () {
                    $("#ajaxBillNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxBillNoSearchBox").html(html).show();
                    $("#ajaxBillNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxBillNoSearchBox', event);
    }
});


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillitem_desc(item_code, item_desc) {
    item_desc = htmlDecode(item_desc);
    $('#item_desc_hidden').val(item_code);
    $('#item_desc').val(item_desc);
    $('#item_desc_AjaxDiv').hide();
}

function fillSearchDetials(grn_id, grn_number) {
    grn_number = htmlDecode(grn_number);
    $('#grn_no_search').val(grn_number);
    $('#grn_id_hidden').val(grn_id);
    $('#ajaxGrnNoSearchBox').hide();
}


function fillInvoice_no(grn_id, bill_no) {
    bill_no = htmlDecode(bill_no);
    $('#bill_no_search').val(bill_no);
    $('#bill_id_hidden').val(grn_id);
    $('#ajaxBillNoSearchBox').hide();
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#vendor_name_search').val(vendor_name);
    $('#vendoritem_id_hidden').val(id);
    $('#ajaxVendorSearchBox').hide();
}

function fetchGrnRequest(req_id, grn_no, from_type = 0) {
    var url = base_url + "/purchase/editRequestList";
    $('#req_id_modal').val(req_id);
    var param = {
        _token: token,
        request_id: req_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            reset_editables();
            if (from_type == 1) {
                $("#grn_edit_details").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });

            } else {
                $('#fetch_req_btn' + req_id).attr('disabled', true);
                $('#fetch_req_spin' + req_id).removeClass('fa fa-pencil');
                $('#fetch_req_spin' + req_id).addClass('fa fa-spinner fa-spin');
            }

        },
        success: function (data) {
            obj = JSON.parse(data);
            $('#grn_edit_details').html(obj.view_data);
            $('#current_grn_no').html(grn_no);
            $('#re_edit_modal').modal('show');
        },
        complete: function () {
            if (from_type == 1) {
                $("#grn_edit_details").LoadingOverlay("hide");

            } else {
                $('#fetch_req_btn' + req_id).attr('disabled', false);
                $('#fetch_req_spin' + req_id).removeClass('fa fa-spinner fa-spin');
                $('#fetch_req_spin' + req_id).addClass('fa fa-pencil');
            }

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

$(document).on('click', '.add_this_req', function () {

})

function setEditDetails(type = 0) {
    var url = base_url + "/purchase/addEditRequest";
    var request_id = $('#req_id_modal').val();
    var edit_id = $('#request_id').val();
    var grn_edit_remark = $('#grn_edit_remark').val();
    var grn_no = $('#current_grn_no').html().trim();
    var param = {
        _token: token,
        request_id: request_id,
        type: type,
        grn_no: grn_no,
        edit_id: edit_id,
        grn_edit_remark: grn_edit_remark
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#add_this_req_btn' + type).attr('disabled', true);
            if (type == 1) {
                $('#add_this_req_spin' + type).removeClass('fa fa-save');
            } else if (type == 2) {
                $('#add_this_req_spin' + type).removeClass('fa fa-thumbs-up');
            } else if (type == 3) {
                $('#add_this_req_spin' + type).removeClass('fa fa-thumbs-down');
            }

            $('#add_this_req_spin' + type).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data == 1) {
                toastr.success('Success.');
                var id = $('#req_id_modal').val();
                $('#fetch_req_btn' + request_id).removeClass('btn-info');
                $('#fetch_req_btn' + request_id).removeClass('btn-primary');
                $('#fetch_req_btn' + request_id).removeClass('btn-danger');
                $('#fetch_req_btn' + request_id).removeClass('btn-success');
                if (type == 1) {
                    $('#fetch_req_btn' + request_id).addClass('btn-info');
                    $('#provision_to_edit' + request_id).fadeOut();

                } else if (type == 2) {
                    $('#fetch_req_btn' + request_id).addClass('btn-primary');
                    $('#provision_to_edit' + request_id).fadeIn();
                } else if (type == 3) {
                    $('#fetch_req_btn' + request_id).addClass('btn-danger');
                    $('#provision_to_edit' + request_id).fadeOut();
                }
                fetchGrnRequest(id, grn_no, from_type = 1);
                reset_editables();
            } else if (data == 2) {
                toastr.warning('Existing request.');
            }

        },
        complete: function () {
            $('#add_this_req_btn' + type).attr('disabled', false);
            $('#add_this_req_spin' + type).removeClass('fa fa-spinner fa-spin');
            if (type == 1) {
                $('#add_this_req_spin' + type).addClass('fa fa-save');
            } else if (type == 2) {
                $('#add_this_req_spin' + type).addClass('fa fa-thumbs-up');
            } else if (type == 3) {
                $('#add_this_req_spin' + type).addClass('fa fa-thumbs-down');
            }

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function editRequest(rer_id, remarks, request_status, has_save) {
    $('#editable_grn').html('Update Request');
    $('#grn_edit_remark').val(atob(remarks));
    $('#request_id').val(rer_id);
    $('#app_btn').fadeIn();
    $('#app_rej_btn').fadeIn();
    var status = '';
    if (request_status == 1) {
        status = 'Send';
        $('#add_this_req_btn1').fadeIn();
    } else if (request_status == 2) {
        status = 'Approved';
        $('#add_this_req_btn1').fadeOut();
    }
    if (has_save == 1) {
        $('#add_this_req_btn1').fadeIn();

    } else {
        $('#add_this_req_btn1').fadeOut();
    }
    $('#request_status').val(status);
}

function reset_editables() {
    $('#request_status').val('');
    $('#grn_edit_remark').val('');
    $('#editable_grn').html('Add Request');
    $('#request_id').val('');
    $('#add_this_req_btn1').fadeIn();
    $('#app_btn').fadeOut();
    $('#app_rej_btn').fadeOut();
}

function clearFilter() {
    var default_location = $('#default_location').val();
    $('#from_date').val('');
    $('#to_date').val('');
    $('#grn_id_hidden').val('');
    $('#bill_no_search').val('');
    $('#vendoritem_id_hidden').val('');
    $('#item_desc').val('');
    $('#item_desc_hidden').val('');
    $('#to_location').val(default_location);
    $('#status').val('');
    $('#grn_no_search').val('');
    $('#bill_id_hidden').val('');
    $('#vendor_name_search').val('');

    localStorage.setItem("from_date", '');
    localStorage.setItem("to_date", '');
    localStorage.setItem("grn_no", '');
    localStorage.setItem("grn_id", '');
    localStorage.setItem("bill_no", '');
    localStorage.setItem("bill_id", '');
    localStorage.setItem("vendor_id", '');
    localStorage.setItem("vendor_name", '');
    localStorage.setItem("item_desc", '');
    localStorage.setItem("item_code", '');
    localStorage.setItem("store_location", default_location);
    localStorage.setItem("status", '');
    searchGrnList();
}
