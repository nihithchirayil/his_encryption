$("body").data("lastID", 0);

$(document).ajaxStart(function () {
    $.LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
});
$(document).ajaxStop(function () {
    $.LoadingOverlay("hide");
});
$(document).ajaxError(function () {
    $.LoadingOverlay("hide");
});

var purchase_remove_free_qty = $('.purchase_remove_free_qty').val();

purchase_selected_id = 0;
function purchaseAdjustAmount() {
    round_flag = 0;
    var purchase_adjust_amount = parseFloat($('#round_off').val());
    if (isNaN(purchase_adjust_amount)) {
        purchase_adjust_amount = 0;
    }
    var tot_purchase_amount = parseFloat($('#po_amount').val());
    tot_purchase_amount = tot_purchase_amount + purchase_adjust_amount - purchase_rund_val;
    purchase_rund_val = purchase_adjust_amount;
    tot_purchase_amount = tot_purchase_amount.toFixed(2);
    $('#po_amount').val(tot_purchase_amount);
    return 1;

}
function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function roundOff_validation() {
    if ($('#round_off').val() == '') {
        $('#round_off').val('0.00');
    }
    var adjust_amount = parseFloat($('#round_off').val());
    if (isNaN(adjust_amount)) {
        $('#round_off').css('background', '#ffc1c1');
        toastr.error('Not a valid number!!');
        return false;
    } else {
        $('#round_off').css('background', '#fff');
        return true;
    }
}
$(document).on('blur', '#bill_discount_value', function () {
    var disc_value = $('#bill_discount_value').val();
    if (disc_value == "") {
        $('#bill_discount_value').val(parseFloat(0.00));
    }
});
function BillDiscountcalculate(type = 0) {
    $('#get_otherchargespopup').hide();
    var disc_type = $('#bill_discount_type').val();
    var disc_value = parseFloat($('#bill_discount_value').val());
    var disc_mode = $('#disc_mode').val();
    if (isNaN(disc_value)) { disc_value = 0; }
    if (disc_type == '' && type == 0) {
        toastr.warning("Choose a discount type");
        return;
    }
    if (disc_mode == '') {
        toastr.warning("Select discount mode");
        return;
    }
    if (disc_value == "") {
        $('#bill_discount_value').val(parseFloat(0.00));
    }
    if (isNaN(disc_value)) {
        if (type == 0) {
            toastr.warning("Discount value not valid");
            return;
        }
    } else {
        if (disc_value < 0 || disc_value > 100) {
            if (disc_type == 1) {
                if (type == 0) {
                    toastr.warning("Discount value not valid");
                    return;
                }
            }
        }

    }
    calculateDiscountBillwise(disc_type, disc_value, disc_mode);
}
function calculateDiscountBillwise(disc_type, disc_value, disc_mode) {
    var gross_discount = 0;
    var disc_percentage = 0;
    if (disc_mode == '1') {
        if (disc_type == 2) {
            $.each(product_each_charge['total'], function (i, val) {
                $.each(product_each_charge['total'][i], function (i_code, i_gross) {
                    gross_discount += i_gross.gross;
                });
            });
            discountPer = (disc_value / gross_discount) * 100;
        } else {
            discountPer = disc_value;
        }
        var tot_dis_amount = 0;
        $('#added_new_list_table_product').find('td .inserted_row_id_each').each(function () {
            var item_id = $(this).attr('value');
            var item_code = $(this).attr('data-item-code');
            var d = new Object();
            d.value = discountPer;
            d.type = 'popup';
            $('#charge_percentage' + billDiscount).val(discountPer);

            calculationExtraCharges(d, 'percentage', item_id, rawurldecode(item_code), 'GA', 'SUB', billDiscount, 0, 1);
            if ($.type(product_each_charge['discount'][item_id]) != "undefined")
                tot_dis_amount += parseFloat(product_each_charge['discount'][item_id][item_code]['charge_percentage_' + billDiscount]);
        });
        $('.total_discount').html(tot_dis_amount);
    } else {
        var tot_amount = $('#po_amount').val();
        var amount_calc = 0;
        var disc_show = 0;
        if (tot_amount > 0) {
            reCalculateDiscountAmount();
            tot_amount = calculatePurchaseTotalAmount();

            if (disc_type == 2) {
                tot_amount = tot_amount - disc_value;
                disc_show = disc_value;
            } else {
                amount_calc = tot_amount * (disc_value / 100);
                tot_amount = tot_amount - amount_calc;
                disc_show = amount_calc;
            }

            $('.total_discount').html(disc_show);
            product_each_charge['bill_discount'] = disc_show;

            calculatePurchaseTotalAmount('1');
        }


    }
}
function calculatePurchaseTotalAmount(status = 0) {
    var po_total_mode = gross_total_mode = disc_amount_total_mode = tax_amount_total_mode = on_tax_amount_total_mode = tax_amount_free_total_mode = other_charges_mode = 0;
    $.each(product_each_charge['total'], function (i, val) {
        $.each(product_each_charge['total'][i], function (key, value) {

            $.each(product_each_charge['total'][i][key], function (keyNew, valueNew) {


                if (keyNew == 'base_rate') {
                    gross_total_mode += parseFloat(valueNew);
                } else if (keyNew == 'disc_amount')
                    disc_amount_total_mode += parseFloat(valueNew);
                else if (keyNew == 'tax_amount') {
                    tax_amount_total_mode += parseFloat(valueNew);
                } else if (keyNew == 'on_tax_amount')
                    on_tax_amount_total_mode += parseFloat(valueNew);
                else if (keyNew == 'tax_amount_free')
                    tax_amount_free_total_mode += parseFloat(valueNew);
                else if (keyNew == 'other_charges')
                    other_charges_mode += parseFloat(valueNew);
            });

        });
    });
    if (status != 0) {
        var bill_dis = product_each_charge['bill_discount'];
        po_total_mode = parseFloat(gross_total_mode) - parseFloat(disc_amount_total_mode) - parseFloat(bill_dis) + parseFloat(tax_amount_total_mode) + parseFloat(on_tax_amount_total_mode) + parseFloat(tax_amount_free_total_mode) + parseFloat(other_charges_mode);

        $('#item_value').val(gross_total_mode.toFixed(2));
        var payment_otr_mode = po_total_mode - gross_total_mode;
        $('#payment_other').val(payment_otr_mode.toFixed(2));
        $('#po_amount').val(po_total_mode.toFixed(2));
    } else {
        po_total_mode = parseFloat(gross_total_mode);

        return po_total_mode;
    }
}


function reCalculateDiscountAmount() {
    $('#added_new_list_table_product').find('td .inserted_row_id_each').each(function () {
        var item_id = $(this).attr('value');
        var item_code = $(this).attr('data-item-code');
        var d = new Object();
        d.value = parseFloat(0.0);
        d.type = 'popup';
        calculationExtraCharges(d, 'percentage', item_id, rawurldecode(item_code), 'GA', 'SUB', billDiscount, 0, 1);
    });
}

$("#added_new_list_table_product").on('click', function (e) {
    if ($.type($(this).find('input[name="edit_status[]"]'))) {
        var edit_status_id = $(e.target).parents('tr').find('input[name="edit_status[]"]').attr('id');
        $('#' + edit_status_id).val('1');
    }
});

$('.ajaxSearchBox').bind('DOMNodeInserted DOMNodeRemoved', function () {
    $('html').one('click', function () {
        $(".ajaxSearchBox").hide();
    });
});
$('#item_desc_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('item_desc_searchCodeAjaxDiv');
        return false;
    }
});

$('#item_requistion_no').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('requistion_no_searchCodeAjaxDiv');
        return false;
    }
});

$('#requested_by_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('requested_byCodeAjaxDiv');
        return false;
    }
});


$('#porequested_by_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxListKeyEnter('porequested_byCodeAjaxDiv');
        return false;
    }
});
$('#item_desc_search').keyup(delay(function (event) {

    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();

    if ($('#all_search').is(":checked")) {
        var check = 1;
    }
    else {
        var check = 0;
    }
    var loc = $('#search_from_department').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var product_ser = $(this).val();
        product_ser = product_ser.replace(hiddenCcode, '');
        product_ser = product_ser.trim();
        if (product_ser == "") {
            $("#item_desc_searchCodeAjaxDiv").html("");
            $("#item_desc_search_hidden").val("");
        } else {
            try {
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, item_desc: product_ser, loc: loc, check_value: check };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#item_desc_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#item_desc_searchCodeAjaxDiv").html(html).show();
                        $("#item_desc_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('item_desc_searchCodeAjaxDiv', event);
    }
}, 1000));
$('#manufacture_search').keyup(delay(function (event) {

    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();

    if ($('#all_search').is(":checked")) {
        var check = 1;
    }
    else {
        var check = 0;
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var manufacture = $(this).val();
        manufacture = manufacture.replace(hiddenCcode, '');
        manufacture = manufacture.trim();
        if (manufacture == "") {
            $("#manufacture_searchCodeAjaxDiv").html("");
            $("#manufacture_search_hidden").val("");
        } else {
            try {
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, manufacture_name: manufacture, check_value: check, manufacture_search: 1 };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#manufacture_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#manufacture_searchCodeAjaxDiv").html(html).show();
                        $("#manufacture_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('manufacture_searchCodeAjaxDiv', event);
    }
}, 1000));
function fillManufacture(list, manufacture_name, manu_code) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#manufacture_search').val(manufacture_name);
    $('#' + itemCodeListDivId).hide();
    $('#manufacture_search_hidden').val(manu_code);
}
$('#item_requistion_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();
    var department = $('#search_from_department').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var product_req_no = $(this).val();
        product_req_no = product_req_no.replace(hiddenCcode, '');
        product_req_no = product_req_no.trim();
        if (product_req_no == "") {
            $("#requistion_no_searchCodeAjaxDiv").html("");
            $("#item_requistion_nohidden").val("");
        } else {
            try {
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, item_req_no: product_req_no, department: department };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#requistion_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#requistion_no_searchCodeAjaxDiv").html(html).show();
                        $("#requistion_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('requistion_no_searchCodeAjaxDiv', event);
    }
});
$('#requested_by_search').keyup(delay(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var item_req_by = $(this).val();
        item_req_by = item_req_by.replace(hiddenCcode, '');
        item_req_by = item_req_by.trim();
        if (item_req_by == "") {
            $("#requested_byCodeAjaxDiv").html("");
            $("#requested_by_hidden").val("");
        } else {
            try {
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, item_req_by: item_req_by };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#requested_byCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#requested_byCodeAjaxDiv").html(html).show();
                        $("#requested_byCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('requested_byCodeAjaxDiv', event);
    }
}, 1000));
$('#po_number_search').keyup(delay(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var purchase_no = $(this).val();
        purchase_no = purchase_no.trim();
        if (purchase_no == "") {
            $("#po_number_searchCodeAjaxDiv").html("");
            $("#po_number_search_hidden").val("");
        } else {
            try {
                var status = $('#posearch_status').val();
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, po_no: purchase_no, status: status };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#po_number_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                        }
                        $("#po_number_searchCodeAjaxDiv").html(html).show();
                        $("#po_number_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('po_number_searchCodeAjaxDiv', event);
    }
}, 1000));
$('#po_number_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('po_number_searchCodeAjaxDiv');
        return false;
    }
});

/*popup po search*/
$('#popup_po_no_srch').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var purchase_no = $(this).val();
        purchase_no = purchase_no.replace(hiddenCcode, '');
        purchase_no = purchase_no.trim();
        if (purchase_no == "") {
            $("#popup_po_number_searchCodeAjaxDiv").html("");
            $("#popup_po_number_search_hidden").val("");
        } else {
            try {
                var status = 3;
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, po_no: purchase_no, status: status, type: 1 };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#popup_po_number_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                        }
                        $("#popup_po_number_searchCodeAjaxDiv").html(html).show();
                        $("#popup_po_number_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('popup_po_number_searchCodeAjaxDiv', event);
    }
});
$('#popup_po_no_srch').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('popup_po_number_searchCodeAjaxDiv');
        return false;
    }
});
/*popup po search*/
$('#porequested_by_search').keyup(delay(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var hiddenCcode = $('#hiddensearch_code_expression').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var product_requested = $(this).val();
        product_requested = product_requested.replace(hiddenCcode, '');
        product_requested = product_requested.trim();
        if (product_requested == "") {
            $("#porequested_byCodeAjaxDiv").html("");
            $("#porequested_by_hidden").val("");
        } else {
            try {
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, po_req_by: product_requested };
                $.ajax({
                    type: "POST",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#porequested_byCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#porequested_byCodeAjaxDiv").html(html).show();
                        $("#porequested_byCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('porequested_byCodeAjaxDiv', event);
    }
}, 1000));
$(document).keydown(function (event) {

    product_code2 = $("body").data("lastID");
    if (event.which == '113') {

        if (product_code2 != '0') {
            var file_token = $('#hidden_filetoken').val();
            var param = { _token: file_token, item_code_f2: product_code2 };
            $.ajax({
                type: "POST",
                url: '',
                data: param,
                beforeSend: function () {
                    $('#addProductSpin').removeClass('fa fa-plus');
                    $('#addProductSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if ($("#po_requisitionli").hasClass("active"))
                        $('#poplast_item_box_div_req').html(data).show();
                    else
                        $('#poplast_item_box_div').html(data).show();

                },
                complete: function () {
                    $('#addProductSpin').removeClass('fa fa-spinner fa-spin');
                    $('#addProductSpin').addClass('fa fa-plus');
                }
            });
        } else {
            toastr.warning("Please Select an Item !!!!");
        }
    } else if ((event.which === 78 && event.altKey) || (event.which === 110 && event.altKey)) {
        if (po_add_status == 1) {
            AddNewProduct(0, 1, 0, '');
        }
    } else if ((event.which === 67 && event.altKey) || (event.which === 99 && event.altKey)) {
        if (product_code2 != '0') {
            $('#itemadd_row' + product_code2).each(function () {
                if ($.type($(this).find('button[name="getProductCharges_btn[]"]'))) {
                    $(this).find('button[name="getProductCharges_btn[]"]').trigger('click');
                }
            });
        } else {
            toastr.warning("Please Select an Item !!!!")
        }
    }
});
$(document).ready(function () {
    serach_po();
    po_add_status = 0;
    item_array_new = {};
    stock_details_array = [];
    delete_array = [];
    total_qty_added = 0.0;
    item_id = 0;
    from_locationid = 0;
    stock_head_id = 0;
});
var coll = $('#scrollDownLoad');
window.offset = 0;
coll.scroll(function () {
    if (Math.trunc(coll.outerHeight()) >= Math.trunc((coll.get(0).scrollHeight - coll.scrollTop()) - 5)) {
        window.offset += 100;
        productList(window.offset);
    }
});
var col_po = $('#scrolldown_polist');
window.offset_po = 0;
col_po.scroll(function () {
    if (Math.trunc(col_po.outerHeight()) == Math.trunc((col_po.get(0).scrollHeight - col_po.scrollTop()))) {
        window.offset_po += 20;
        serachPurchaseOrder(0, window.offset_po);
    }
});
item_array = [];
annexture_data_text = '';
annexture_ref2_text = '';
annexture_ref1_text = '';
additionaltems_text = '';
function productList(offset) {
    var item_arrayString = JSON.stringify(item_manage_array);
    var purchase_req_no = $('#item_requistion_nohidden').val();
    var purchase_item_desc = $('#item_desc_search_hidden').val();
    var department = $('#search_from_department').val();
    var vendor_code = $('#vendor_list_po_hidden').val();
    var requested_by = $('#requested_by_hidden').val();
    var purchase_item_desc_char = $('#item_desc_search').val();
    var category_code = $('#from_category1').val();
    var sub_category_code = $('#get_subcatgeoryselect1').val();
    var manufacture_text = $('#manufacture_search').val();
    var manufacture_code = $('#manufacture_search_hidden').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var file_token = $('#hidden_filetoken').val();
    var param = {
        _token: file_token,
        department: department,
        item_arrayList: item_arrayString,
        offset: offset,
        from_date: from_date,
        to_date: to_date,
        purchase_req_no: purchase_req_no,
        purchase_item_desc: purchase_item_desc,
        purchase_item_desc_char: purchase_item_desc_char,
        category_code: category_code,
        sub_category_code: sub_category_code,
        manufacture_text: manufacture_text,
        manufacture_code: manufacture_code,
        requested_by: requested_by,
        vendor_code: vendor_code
    };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            $("#tbody_poitemdata").append(result);
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
        },
        complete: function () {
            $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
            $('#searchdataspin').addClass('fa fa-search');
        }
    });
}
function add_dataitemlist() {
    $('#itemts_getdiv').html('');
    $("#itemcode_searchModal").modal('toggle');
}

$('.nav-tabs a[href="#po_list"]').on('shown.bs.tab', function (e) {
    serachPurchaseOrder(1, 0);
});
function serachPurchaseOrder(post_type, offset) {
    $("#purchase_order_status").css('display', 'none');
    var po_no = $('#po_number_search_hidden').val();
    var req_by = $('#porequested_by_hidden').val();
    var location = $('#posearch_from_department').val();
    var created_at = $('#po_created_date').val();
    var created_at_to = $('#po_created_date_to').val();
    var file_token = $('#hidden_filetoken').val();
    var po_status = $('#posearch_status').val();
    var vendor_id = $('#vendor_list_po_hidden1').val();
    var date_filter = $('#date_filter').val();
    var user_filter = $('#user_filter').val();
    var mail_status = $('#mail_status').val();
    if (offset == 1) {
        offset = 0;
    }
    var param = { _token: file_token, user_filter: user_filter, date_filter: date_filter, created_at_to: created_at_to, vendor_id: vendor_id, po_status: po_status, purchase_selected_id: purchase_selected_id, created_at: created_at, location: location, req_by: req_by, po_no: po_no, search_polist: 1, mail_status: mail_status, offset: offset };
    var url = $('#purchase_order_serch_route').val();
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            if (post_type == '1') {
                $('#po_listlispin').removeClass('fa fa-list-alt');
                $('#po_listlispin').addClass('fa fa-spinner fa-spin');
            } else {
                $('#posearchdataspin').removeClass('fa fa-search');
                $('#posearchdataspin').addClass('fa fa-spinner fa-spin');
            }
        },
        success: function (result) {

            if (offset == 1) {
                offset = 0;
            }
            if (offset == '0') {
                $('#purchase_order_list_body').html('');
                $('#purchase_order_list_body').html(result);
                var myDiv = document.getElementById('scrolldown_polist');
                myDiv.scrollTop = 0;
                window.offset_po = 0;
            } else {
                $('#purchase_order_list_body').append(result);
            }

            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });

        },
        complete: function () {
            if (post_type == '1') {
                $('#po_listlispin').removeClass('fa fa-spinner fa-spin');
                $('#po_listlispin').addClass('fa fa-list-alt');
            } else {
                $('#posearchdataspin').removeClass('fa fa-spinner fa-spin');
                $('#posearchdataspin').addClass('fa fa-search');
            }
        }
    });
}
function close_last_itemlist() {
    $('#poplast_item_box_div').hide();
    $('#poplast_item_box_div_req').hide();
}
function popup_sync_close() {
    $('#pop_up_rate_sync').hide();
}

purchase_order_condition_array = [];
function purchaseOrderAddRemoveTerms(terms_id) {
    var productStatus = $('#purchase_order_terms_remove_add' + terms_id).is(":checked");
    terms_id = parseInt(terms_id);
    var position = purchase_order_condition_array.indexOf(terms_id);
    if (productStatus) {
        if (position) {
            purchase_order_condition_array.push(terms_id);
        }
    } else {
        if (~position) {
            purchase_order_condition_array.splice(position, 1);
        }
    }
}

function delete_itemlist(item_id) {
    var item_code = $('#item_codeid' + item_id).val();
    var item_detail_id = $('#item_detail_id' + item_id).val();

    if (confirm("Are you sure you want to delete Item?")) {
        if (product_each_charge['othch'].hasOwnProperty(item_id)) {
            $.each(product_each_charge['othch'][item_id][item_code]['percentage'], function (i, val) {
                product_each_charge['othch'][item_id][item_code]['charge_percentage_' + i] = 0;
                product_each_charge['othch'][item_id][item_code]['percentage'][i] = 0;
            });
        }
        $.each(purchase_requisition, function (detail_id, item_value) {
            if (item_code == item_value) {
                delete purchase_requisition[detail_id];
            }
        });
        $('#pur_qtyid' + item_id).val(0);
        $('#pur_qtyid' + item_id).trigger('onkeyup');
        $('#free_qtyid' + item_id).val(0);
        $('#free_qtyid' + item_id).trigger('onkeyup');


        if (item_array_new.hasOwnProperty(item_id)) {
            delete item_array_new[item_id];
        }
        if (product_each_charge['tax'].hasOwnProperty(item_id)) {
            delete product_each_charge['tax'][item_id];
        }
        if (product_each_charge['total'].hasOwnProperty(item_id)) {
            delete product_each_charge['total'][item_id];
        }
        if (product_each_charge['discount'].hasOwnProperty(item_id)) {
            delete product_each_charge['discount'][item_id];
        }
        if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
            delete product_each_charge['on_tax_amount'][item_id];
        }
        if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
            delete product_each_charge['tax_free'][item_id];
        }
        if (product_each_charge['othch'].hasOwnProperty(item_id)) {
            delete product_each_charge['othch'][item_id];
        }
        $('#pur_qtyid' + item_id).trigger('onkeyup');





        if (item_detail_id != '0') {
            var position = delete_array.indexOf(item_detail_id);
            if (position) {
                delete_array.push(item_detail_id);
            }
        }
        add_item--;
        delete item_array_new[item_id];
        var position = stock_details_array.indexOf(item_code);
        stock_details_array.splice(position, 1);
        if ($('#itemadd_row' + item_id)) {
            $('#itemadd_row' + item_id).remove();
        }

        locationEnable();
        $(".iterate_row").each(function (i) {
            $(this).find('.row_count_cls').text(i + 1);
        });
        row_count = $('#row_count').val();
        $('#row_count').val(--row_count);
    }
}


function purchaseEditList(po_id, amend, mode = 0) {
    var mode;
    $("#enable_mail").prop('checked', false);
    item_array_new = {};
    stock_details_array = [];
    delete_array = [];
    total_qty_added = 0.0;
    item_id = 0;
    from_locationid = 0;
    stock_head_id = 0;
    purchase_selected_id = 0;
    window.offset = 0;
    po_add_status = 0;
    item_array = [];
    annexture_data_text = '';
    annexture_ref2_text = '';
    annexture_ref1_text = '';
    additionaltems_text = '';
    add_item = 0;
    $("#purchase_order_status").css('display', 'block');
    purchase_selected_id = po_id;
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token, po_loadid: po_id, amend: amend };
    $.ajax({
        type: "POST",
        url: "poReq",
        data: param,
        beforeSend: function () {
            $('.po_listadata').removeClass('danger');
            $('#polistrow_id' + po_id).addClass('danger');
            $('#editpolistdata' + po_id).removeClass('fa fa-pencil-square-o');
            $('#editpolistdata' + po_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            if (result) {
                if (mode == 'new_po')
                    $('#vendor_list').prop('disabled', true);
                else
                    $('#vendor_list').prop('disabled', false);
                $("#print_preview").show();
                $("#added_new_list_table_product tr").remove();
                add_item = 0;
                item_array_new = {};
                purchase_order_condition_array = [];
                item_charge_array = {};
                product_each_charge['discount'] = {};
                product_each_charge['tax'] = {};
                product_each_charge['total'] = {};
                product_each_charge['on_tax_amount'] = {};
                product_each_charge['tax_free'] = {};
                product_each_charge['othch'] = {};
                product_each_charge['bill_discount'] = 0;
                delete_array = [];
                detail_id_array = {};
                var grn_detail_totl = 0;
                var po_detail_total = 0;
                var diff = 0;
                var po_no = '';
                var po_status = '';
                mode = mode;
                var po_approve_amt = JSON.parse(result).po_approve_amt;
                $('#po_approve_amt_id').val(po_approve_amt);
                $.each(JSON.parse(result).po_result, function (idx, obj) {
                    $('#is_po_status').val(obj.po_status);
                    $('#parent_po_status').val(obj.parent_po_status);
                    if (mode == 'new_po') {
                        obj.po_status = 'new_po';
                    } else {
                        po_status = obj.po_status;
                    }

                    if (obj.po_status == 'new_po') {
                        $("#po_post_btn_id").show();
                        $(".po_save_common_bttn").show();
                        $("#purchase_order_status,#print_preview").hide();
                    } else
                        if (obj.po_status == '1') {
                            $(".po_save_bttn").attr('onclick', "savePurchaseOrder(" + obj.po_status + ")");
                            $('#po_post_btn_id').show();
                            $('#po_verify_btn_id').show();
                            $('#po_close_btn_id').hide();
                            $('#po_cancel_btn_id').show();
                            $("#po_approve_btn_id").show();
                            $('#enable_mail_option').hide();
                            $("#po_status").html('Created');
                            $('#vendor_mail_btn_id').hide();
                            $('#vendor_pdf_print').hide();
                            $('#vendor_pdf_print_amned').hide();
                            $('#po_approve_revert_btn_id').hide();
                            $('#request_for_amend_btn_id').hide();
                            $('#download_pdf_js_id').hide();
                            $('#qutation_btn_id').show();
                        } else if (obj.po_status == '6') {
                            $(".po_save_bttn").attr('onclick', "savePurchaseOrder(" + obj.po_status + ")");
                            $('#po_post_btn_id').show();
                            if (parseFloat(obj.net_total) > parseFloat(po_approve_amt)) {
                                $('#po_approve_btn_id').hide();
                            } else if (po_approve_amt == 'null') {
                                $('#po_approve_btn_id').show();
                            } else {
                                $('#po_approve_btn_id').show();
                            }
                            $('#po_verify_btn_id').hide();
                            $('#po_close_btn_id').hide();
                            $('#po_cancel_btn_id').show();
                            $('#enable_mail_option').show();
                            $("#po_status").html('Verified');
                            $('#vendor_mail_btn_id').hide();
                            $('#download_pdf_js_id').hide();
                            $('#vendor_pdf_print').hide();
                            $('#po_approve_revert_btn_id').hide();
                            $('#request_for_amend_btn_id').hide();
                            $('#vendor_pdf_print_amned').hide();
                            $('#qutation_btn_id').show();
                        } else if (obj.po_status == '3') {
                            $('#po_verify_btn_id').hide();
                            $('#enable_mail_option').hide();
                            $('#po_post_btn_id').hide();
                            $('#po_approve_btn_id').hide();
                            $('#request_for_amend_btn_id').show();
                            $('#po_approve_revert_btn_id').show();
                            $('#vendor_mail_btn_id').show();
                            $('#vendor_pdf_print').show();
                            $('#vendor_pdf_print_amned').hide();
                            $('#download_pdf_js_id').show();
                            $('#po_close_btn_id').show();
                            $('#po_cancel_btn_id').hide();
                            $('#qutation_btn_id').show();
                            if (obj.grn_status == 0) {
                                $("#po_status").html('Approved');
                            } else if (obj.grn_status == 5) {
                                $("#po_status").html('GRN Partially Completed');
                            } else if (obj.grn_status == 10) {
                                $("#po_status").html('GRN Completed');
                            }

                        } else if (obj.po_status == 15) {
                            $('#po_verify_btn_id').hide();
                            $('#enable_mail_option').hide();
                            $('#po_post_btn_id').hide();
                            $('#po_approve_btn_id').hide();
                            $('#request_for_amend_btn_id').hide();
                            $('#vendor_mail_btn_id').hide();
                            $('#vendor_pdf_print').hide();
                            $('#vendor_pdf_print_amned').hide();
                            $('#download_pdf_js_id').hide();
                            $('#po_close_btn_id').hide();
                            $('#po_cancel_btn_id').hide();
                            $('#po_approve_revert_btn_id').hide();
                            $('#po_amend_btn_id').show();
                            $("#po_status").html('Request for Amend');
                            $('#qutation_btn_id').show();
                        } else if (obj.po_status == '2') {
                            $('#enable_mail_option').hide();
                            $('#po_post_btn_id').hide();
                            $('#po_close_btn_id').hide();
                            $('#po_cancel_btn_id').hide();
                            $('#po_approve_btn_id').hide();
                            $("#po_status").html('Close');
                            $('#vendor_mail_btn_id').hide();
                            $('#vendor_pdf_print').hide();
                            $('#vendor_pdf_print_amned').hide();
                            $('#qutation_btn_id').show();
                        } else if (obj.po_status == '4') {
                            $('#enable_mail_option').hide();
                            $('#vendor_list').prop('disabled', true);
                            $('#po_post_btn_id').hide();
                            $("#po_approve_btn_id").hide();
                            $("#po_amend_btn_id").hide();
                            $('#qutation_btn_id').show();
                            if (obj.approve_amend == 1) {
                                $("#aprove_amend_btn_id").show();
                                $("#request_for_amend_btn_id").show();
                            } else {
                                $("#aprove_amend_btn_id").hide();
                                $("#request_for_amend_btn_id").show();

                            }
                            $('#genarate_po .bg-blue-active').parent().hide();
                            $('#genarate_po .bg-maroon-active').parent().hide();
                            $('#genarate_po .bg-aqua-active').parent().hide();
                            $('#po_cancel_btn_id').hide();
                            $('#vendor_mail_btn_id').show();
                            $('#vendor_pdf_print').hide();
                            $('#download_pdf_js_id').show();
                            $('#po_verify_btn_id').hide();
                            if (obj.grn_status == 0) {
                                $("#po_status").html('Amend');
                            } else if (obj.grn_status == 5) {
                                $("#po_status").html('GRN Partially Completed');
                            } else if (obj.grn_status == 10) {
                                $("#po_status").html('GRN Completed');
                            }
                            if (obj.parent_po_status == '1' || obj.amended_po == "") {
                                $('#vendor_pdf_print_amned').show();
                                $('#po_close_btn_id').show();
                                $('#qutation_btn_id').show();
                            } else {
                                $('#po_close_btn_id').hide();
                                $('#vendor_pdf_print_amned').hide();
                                $('#vendor_mail_btn_id').hide();
                                $('#download_pdf_js_id').hide();
                                $('#po_approve_revert_btn_id').hide();
                                $('#qutation_btn_id').hide();
                            }

                        } else if (obj.po_status == '0') {
                            $('#enable_mail_option').hide();
                            $('#po_post_btn_id').hide();
                            $("#po_approve_btn_id").hide();
                            $('#po_close_btn_id').show();
                            $('#genarate_po .bg-blue-active').parent().hide();
                            $('#genarate_po .bg-maroon-active').parent().hide();
                            $('#genarate_po .bg-aqua-active').parent().hide();
                            $('#po_cancel_btn_id').hide();
                            $('#vendor_mail_btn_id').hide();
                            $("#po_status").html('Cancelled');
                        } else if (obj.po_status == '5') {
                            $('#enable_mail_option').hide();
                            $('#vendor_list').prop('disabled', true);
                            $('#po_post_btn_id').hide();
                            $('#po_approve_btn_id').hide();
                            $('#po_close_btn_id').show();
                            $('#po_cancel_btn_id').hide();
                            $('#vendor_pdf_print').hide();
                            $('#vendor_pdf_print_amned').hide();
                            $('#po_amend_btn_id').show();
                            $("#po_status").html('GRN Partially Generate');
                            $('#qutation_btn_id').show();

                        } else if (obj.po_status == '10') {
                            $('#enable_mail_option').hide();
                            $('#vendor_list').prop('disabled', true);
                            $('#po_post_btn_id').hide();
                            $('#po_approve_btn_id').hide();
                            $('#po_close_btn_id').show();
                            $('#po_cancel_btn_id').hide();
                            $('#vendor_pdf_print').hide();
                            $('#vendor_pdf_print_amned').hide();
                            $('#po_amend_btn_id').show();
                            $("#po_status").html('GRN Completed');
                            $('#qutation_btn_id').show();
                        }
                    $('#po_mail_div').show();
                    if (obj.mail_status) {
                        $('#po_mail').text('Mail Sent');
                    } else {
                        $('#po_mail').text('Mail Not Sent');
                    }
                    $('#hidden_status').val(obj.po_status);

                    if (obj.discount_calc_mode != '') {
                        $("#disc_mode").val(obj.discount_calc_mode);
                    }
                    if (obj.bill_discount_type != '') {
                        $("#bill_discount_type").val(obj.bill_discount_type);
                    }
                    if (obj.bill_discount_value != '') {
                        $("#bill_discount_value").val(obj.bill_discount_value);
                    }
                    if (obj.bill_discount_amount != '') {
                        $(".total_discount").html(obj.bill_discount_amount);
                        if (obj.discount_calc_mode == 2)
                            product_each_charge['bill_discount'] = obj.bill_discount_amount;

                    }
                    if (obj.disc_on_disc != '' && obj.disc_on_disc != 0) {
                        $("#from_discounted").prop('checked', true);
                    }
                    if (obj.adjust_amount != '') {
                        $("#round_off").val(obj.adjust_amount);
                    }

                    $('#po_status').val(obj.po_status);
                    if (mode == 'new_po') {
                        $('#po_added').val('');
                        $('#add_edit_status').val(0);
                    } else {
                        $('#po_added').val(obj.po_no);
                        $('#add_edit_status').val(obj.po_id);
                    }
                    $('#is_amended').val(amend);

                    $('#po_date').val(obj.po_date);

                    if ($("#vendor_list_po2").val() != "") {
                        $("#vendor_list_po2").val('');
                        $("#vendor_list_po_hidden2").val('');
                    }
                    $("#vendor_list_po2").val(obj.vendor_name);
                    $("#vendor_list_po2").attr('title', obj.contact_no);
                    $("#vendor_list_po_hidden2").val(obj.vendor_code);
                    $('#manufacturer_list').val(obj.manufacture_code);
                    $('#quotation_date').val(obj.quotation_date);
                    $('#charge_term').val(obj.charges_term);
                    $('#delivery_notes').val(obj.delivery_notes);
                    $('#payment_other').val(obj.others);
                    $('#requisition_no').val(obj.requisition_id);
                    $('#payment_date').val(obj.payment);
                    $('#payment_in').val(obj.payment_duration);
                    $('#payment_terms').val(obj.payment_terms);
                    $('#payment_remarks').val(obj.remarks);
                    $('#department').val(obj.for_department);
                    $('#payment_warranty').val(obj.warranty_terms);
                    $('#payment_dispatchmode').val(obj.dispatch_mode);
                    $('#search_from_department').val(obj.location);
                    $('#item_value').val(obj.total_purchase_cost);
                    $('#po_amount').val(obj.net_total);
                    $('#deprt_list').val(obj.location);
                    $('#round_off').val(obj.round_amount);
                    $('#delivery_id').val(obj.delivery_note);
                    annexture_ref1_text = obj.referance1;
                    annexture_ref2_text = obj.referance2
                    annexture_data_text = obj.annexture;
                    additionaltems_text = obj.additional_terms;
                });
                setTimeout(function () {
                    $.each(JSON.parse(result).po_terms, function (idx, obj) {
                        var terms_id = parseInt(obj.terms_id);
                        var position = purchase_order_condition_array.indexOf(terms_id);
                        if (position) {
                            purchase_order_condition_array.push(terms_id);
                        }
                    });
                    $.each(JSON.parse(result).po_items, function (idx, obj) {

                        var it_code = obj.item_code;
                        var purchase_unit = {};
                        if ($.type(JSON.parse(result).purchase_unit[it_code]) != "undefined")
                            purchase_unit = JSON.parse(result).purchase_unit[it_code];
                        purchase_unit = JSON.stringify(purchase_unit);
                        grn_detail_totl = obj.grn_totl;
                        po_detail_total = obj.po_totl;
                        po_no = obj.po_no;
                        if (grn_detail_totl > 0) {
                            diff = grn_detail_totl - po_detail_total;
                            diff = Math.abs(diff);

                        }

                        var approved_ammended_balance_zero = 0;
                        if ((obj.grn_status == '5') && (parseInt(obj.po_balance_qty) > '0')) {
                            var approved_ammended_balance_zero = 1;
                        }


                        if (mode == 'new_po')
                            item_id_added = fillData(obj.detail_id, 1, rawurlencode(obj.item_code), rawurlencode(obj.item_desc), rawurlencode(obj.pur_qnty), rawurlencode(obj.pur_unit), rawurlencode(obj.pur_rate), rawurlencode(obj.free_qty), rawurlencode(obj.mrp), 2, 2, rawurlencode(obj.total), rawurlencode(obj.is_free), rawurlencode(obj.narration), rawurlencode(obj.is_urgent), rawurlencode(obj.item_tooltip), purchase_unit, rawurlencode(obj.is_exclusive), rawurlencode(''), diff, po_no, '', '', '', approved_ammended_balance_zero, obj.pack_size, obj.selling_price, obj.selling_price_flag);
                        else
                            item_id_added = fillData(obj.detail_id, 0, rawurlencode(obj.item_code), rawurlencode(obj.item_desc), rawurlencode(obj.pur_qnty), rawurlencode(obj.pur_unit), rawurlencode(obj.pur_rate), rawurlencode(obj.free_qty), rawurlencode(obj.mrp), 2, 2, rawurlencode(obj.total), rawurlencode(obj.is_free), rawurlencode(obj.narration), rawurlencode(obj.is_urgent), rawurlencode(obj.item_tooltip), purchase_unit, rawurlencode(obj.is_exclusive), rawurlencode(''), diff, po_no, '', '', '', approved_ammended_balance_zero, obj.pack_size, obj.selling_price, obj.selling_price_flag);
                        item_id_added = item_id_added - 1;
                        item_array_new[item_id_added] = {};
                        item_array_new[item_id_added].itemdesc = rawurlencode(obj.item_desc);
                        detail_id_array[obj.detail_id] = {};
                        detail_id_array[obj.detail_id] = item_id_added;

                        var item_gross = parseFloat(obj.pur_qnty) * parseFloat(obj.pur_rate);
                        if (obj.is_free == '1')
                            item_gross = 0;
                        if (!product_each_charge['total'].hasOwnProperty(item_id_added) || $.type(product_each_charge['total'][item_id_added][obj.item_code]) == "undefined") {
                            product_each_charge['total'][item_id_added] = {};
                            product_each_charge['total'][item_id_added][obj.item_code] = {};
                            product_each_charge['total'][item_id_added][obj.item_code].gross = item_gross;
                            if (obj.is_exclusive == 1)
                                product_each_charge['total'][item_id_added][obj.item_code].base_rate = item_gross;
                            else
                                product_each_charge['total'][item_id_added][obj.item_code].base_rate = item_gross - (obj.total_tax_amount);

                            product_each_charge['total'][item_id_added][obj.item_code].disc_amount = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].tax_amount = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].on_tax_amount = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].tax_amount_free = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].other_charges = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].tot_tax_perc = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].tot_disc_perc = 0;
                            product_each_charge['total'][item_id_added][obj.item_code].tot_free_tax_perc = 0;

                        }
                    });
                    $.each(JSON.parse(result).qry_chg, function (idx, obj) {
                        item_id_added = detail_id_array[obj.po_detail_id];


                        if (!item_charge_array.hasOwnProperty(item_id_added) || $.type(item_charge_array[item_id_added][obj.item_code]) == "undefined") {
                            item_charge_array[item_id_added] = {};
                            item_charge_array[item_id_added][obj.item_code] = {};
                        }

                        item_charge_array[item_id_added][obj.item_code][obj.charge_head_id] = obj.perc + ':' + obj.value;
                        if (obj.calc_code == 'GA' && obj.charge_code == 'SUB') {
                            var item_code = obj.item_code;
                            var charge_id = obj.charge_head_id;
                            var perc_val = obj.perc;
                            var value = obj.value;
                            if (!product_each_charge['discount'].hasOwnProperty(item_id_added)) {
                                product_each_charge['discount'][item_id_added] = {};
                                product_each_charge['discount'][item_id_added][item_code] = {};
                                product_each_charge['discount'][item_id_added][item_code]['percentage'] = {};
                            }
                            product_each_charge['discount'][item_id_added][item_code]['percentage'][charge_id] = parseFloat(perc_val);
                            product_each_charge['total'][item_id_added][item_code].disc_amount += parseFloat(obj.value);
                            product_each_charge['total'][item_id_added][item_code].tot_disc_perc += parseFloat(perc_val);
                            product_each_charge['discount'][item_id_added][item_code]['charge_percentage_' + charge_id] = parseFloat(obj.value);

                        }

                        if (obj.calc_code == 'DA' && obj.charge_code == 'ADD' && obj.is_tax == '1') {
                            var item_code = obj.item_code;
                            var charge_id = obj.charge_head_id;
                            var perc_val = obj.perc;
                            var value = obj.value;
                            if (!product_each_charge['tax'].hasOwnProperty(item_id_added) || $.type(product_each_charge['tax'][item_id_added][item_code]) == "undefined") {
                                product_each_charge['tax'][item_id_added] = {};
                                product_each_charge['tax'][item_id_added][item_code] = {};
                                product_each_charge['tax'][item_id_added][item_code]['percentage'] = {};
                            }
                            product_each_charge['tax'][item_id_added][item_code]['percentage'][charge_id] = parseFloat(perc_val);

                            product_each_charge['total'][item_id_added][item_code].tax_amount += parseFloat(obj.value);
                            product_each_charge['total'][item_id_added][item_code].tot_tax_perc += parseFloat(perc_val);
                            product_each_charge['tax'][item_id_added][item_code]['charge_percentage_' + charge_id] = parseFloat(obj.value);

                        }

                        if (obj.calc_code == 'OTA') {
                            var item_code = obj.item_code;
                            var charge_id = obj.charge_head_id;
                            var perc_val = obj.perc;
                            var value = obj.value;
                            if (!product_each_charge['on_tax_amount'].hasOwnProperty(item_id_added)) {
                                product_each_charge['on_tax_amount'][item_id_added] = {};
                                product_each_charge['on_tax_amount'][item_id_added][item_code] = {};
                                product_each_charge['on_tax_amount'][item_id_added][item_code]['percentage'] = {};
                            }
                            product_each_charge['on_tax_amount'][item_id_added][item_code]['percentage'][charge_id] = parseFloat(perc_val);
                            product_each_charge['total'][item_id_added][item_code].on_tax_amount += parseFloat(obj.value);
                            product_each_charge['on_tax_amount'][item_id_added][item_code]['charge_percentage_' + charge_id] = parseFloat(obj.value);

                        }
                        if (obj.calc_code == 'FREE') {
                            var item_code = obj.item_code;
                            var charge_id = obj.charge_head_id;
                            var perc_val = obj.perc;
                            var value = obj.value;
                            if (!product_each_charge['tax_free'].hasOwnProperty(item_id_added)) {
                                product_each_charge['tax_free'][item_id_added] = {};
                                product_each_charge['tax_free'][item_id_added][item_code] = {};
                                product_each_charge['tax_free'][item_id_added][item_code]['percentage'] = {};
                            }
                            product_each_charge['tax_free'][item_id_added][item_code]['percentage'][charge_id] = perc_val;
                            product_each_charge['tax_free'][item_id_added][item_code]['charge_percentage_' + charge_id] = parseFloat(obj.value);
                            product_each_charge['total'][item_id_added][item_code].tax_amount_free += parseFloat(obj.value);
                            product_each_charge['total'][item_id_added][item_code].tot_free_tax_perc += parseFloat(perc_val);
                        }
                        if (obj.calc_code == 'OTHCH') {
                            var charge_id = obj.charge_head_id;
                            var perc_val = obj.perc;
                            var value = obj.value;
                            var item_code = obj.item_code;
                            if (!product_each_charge['othch'].hasOwnProperty(item_id_added)) {
                                product_each_charge['othch'][item_id_added] = {};
                                product_each_charge['othch'][item_id_added][item_code] = {};
                                product_each_charge['othch'][item_id_added][item_code]['percentage'] = {};
                            }
                            product_each_charge['othch'][item_id_added][item_code]['charge_percentage_' + charge_id] = parseFloat(obj.value);
                            product_each_charge['othch'][item_id_added][item_code]['percentage'][charge_id] = parseFloat(obj.value);

                            product_each_charge['total'][item_id_added][item_code].other_charges += parseFloat(obj.value);

                        }


                    });


                    if (JSON.parse(result).accessPerm.verification != 1)
                        $("#genarate_po #po_approve_btn_id").hide();
                    if (po_status != '5')
                        $("#genarate_po #po_amend_btn_id").hide();
                    if (JSON.parse(result).accessPerm.approval == 1 && (po_status == '4')) {
                        if (JSON.parse(result).po_result[0].parent_po_status == '1' || JSON.parse(result).po_result[0].amended_po == '') {
                            $("#genarate_po #po_amend_btn_id").show();
                        } else {
                            $("#genarate_po #po_amend_btn_id").hide();
                            $("#aprove_amend_btn_id").hide();
                            $("#request_for_amend_btn_id").hide();
                        }
                        if (parseFloat(JSON.parse(result).po_result[0].net_total) > parseFloat(po_approve_amt)) {
                            $('#po_amend_btn_id').hide();
                            $('#aprove_amend_btn_id').hide();
                        } else if (po_approve_amt == 'null') {
                            $('#po_amend_btn_id').show();
                            $('#aprove_amend_btn_id').show();
                        } else {
                            $('#aprove_amend_btn_id').show();
                            $('#po_amend_btn_id').show();
                        }
                        $("#po_close_btn_id").find('button').attr('onclick', 'savePurchaseOrder(2,4)');

                    } else if (JSON.parse(result).accessPerm.approval == 1 && (po_status == '3')) {
                        $("#genarate_po #po_amend_btn_id").show();
                        $("#po_close_btn_id").find('button').attr('onclick', 'savePurchaseOrder(2,3)');
                    }


                    $('#genarate_poli').show();
                    $('.nav-tabs a[href="#genarate_po"]').tab('show');
                    $('#po_requisitionli').hide();
                }, 1000);

            }
        },
        complete: function () {
            $('#editpolistdata' + po_id).removeClass('fa fa-spinner fa-spin');
            $('#editpolistdata' + po_id).addClass('fa fa-pencil-square-o');
        }
    });
}

function last_clicked(item_id) {
    var item_code = $('#item_codeid_prq' + item_id).val();

    $("body").data("lastID", item_code);

}
function last_clicked1(item_id) {
    var item_code = $('#item_codeid' + item_id).val();

    $("body").data("lastID", item_code);

}
function f3_clicked(item_code) {

    $("body").data("lastID", item_code);

}







function rawurlencode(str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
        replace(/\)/g, '%29').replace(/\*/g, '%2A');
}

function rawurldecode(str) {
    return decodeURIComponent((str + '')
        .replace(/%(?![\da-f]{2})/gi, function () {
            return '%25';
        }));
}

function newitem_manage(item_code, value = '', item_id = '') {

    var productStatus = $('#itemdetailselect' + item_code).is(":checked");
    item_code = item_code.toString();
    var from_department = $('#search_from_department').val();
    var color_code;
    var position = stock_details_array.indexOf(item_code);
    if (item_id == '')
        item_id = $('#itemdetailselect' + item_code).val();

    if (productStatus && value == '') {


        var approved_ammended_balance_zero = 0;

        var item_name = $('#item_descdata' + item_code).html();
        var item_name_short = $('#item_desc_short' + item_code).val().trim();
        var item_stock = $('#item_stockdata' + item_code).html();
        var item_unit = $('#item_unit_id' + item_code).val();
        var file_token = $('#hidden_filetoken').val();
        var product_code_id = 0;
        var pur_unit = {};
        $.ajax({
            type: "POST",
            url: '',
            data: { item_code_purchase: item_code, is_igst: is_igst, _token: file_token },
            beforeSend: function () {

            },
            success: function (result) {

                var pur_unit_defualt = 0;
                if (result.is_default != 0) {
                    item_unit = result.is_default;
                }
                if (!$.isEmptyObject(result.pack_size) && $.type(result.pack_size[0]) != "undefined" && $.type(result.pack_size[0].pack_size) != "undefined") {
                    pack_size = result.pack_size[0].pack_size;
                }
                product_code_id = fillData(0, 1, rawurlencode(item_code), rawurlencode(item_name), rawurlencode(0.00), rawurlencode(item_unit), rawurlencode(0.00), rawurlencode(0.00), rawurlencode(0.00), rawurlencode(''), rawurlencode(''), rawurlencode(0.00), rawurlencode(''), rawurlencode(''), rawurlencode(''), rawurlencode(item_name_short), result.unit_details, rawurlencode(''), result.grn_details, '', '', '', color_code, rawurlencode(pur_unit_defualt), approved_ammended_balance_zero, pack_size, 0, 0);
                product_code_id = product_code_id - 1;
                $('#itemdetailselect' + rawurlencode(item_code)).val(product_code_id);
                item_array_new[product_code_id] = {};
                item_array_new[product_code_id].itemdesc = rawurlencode(item_name);
                $("#appentitem_codediv").append("<span value='" + rawurldecode(product_code_id) + "' onclick='stockRemove(this);' id='ietmcodeitemdesc" + rawurldecode(product_code_id) + "' class='close_btnspan tag label-primary'>" + rawurldecode(item_code) + ':' + rawurldecode(item_name) + "<span class='item_code_close'><i class='fa fa-times'></i></span></span><div class='clearfix'></div>");
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });

                locationEnable();
            },
            complete: function () {

            },
            async: true
        })

    } else {

        var pur_qty = 0;
        var free_qty = 0;
        var item_id = item_id;

        if ($('#pur_qtyid' + item_id).length != 0)
            pur_qty = $('#pur_qtyid' + item_id).val();

        if ($('#free_qtyid' + item_id).length != 0) {
            free_qty = $('#free_qtyid' + item_id).val();

        }
        if (free_qty > 0 || pur_qty > 0) {
            var item_detail_id = $('#item_detail_id' + item_id).val();
            if (product_each_charge['othch'].hasOwnProperty(item_id)) {
                $.each(product_each_charge['othch'][item_id][item_code]['percentage'], function (i, val) {
                    product_each_charge['othch'][item_id][item_code]['charge_percentage_' + i] = 0;
                    product_each_charge['othch'][item_id][item_code]['percentage'][i] = 0;
                });
            }
            $('#pur_qtyid' + item_id).val(0);
            $('#pur_qtyid' + item_id).trigger('onkeyup');
            $('#free_qtyid' + item_id).val(0);
            $('#free_qtyid' + item_id).trigger('onkeyup');
            if (item_array_new.hasOwnProperty(item_id)) {
                delete item_array_new[item_id];
            }
            if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                delete product_each_charge['tax'][item_id][item_code];
            }
            if (product_each_charge['total'].hasOwnProperty(item_id)) {
                delete product_each_charge['total'][item_id][item_code];
            }
            if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                delete product_each_charge['discount'][item_id][item_code];
            }
            if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
                delete product_each_charge['on_tax_amount'][item_id][item_code];
            }
            if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
                delete product_each_charge['tax_free'][item_id][item_code];
            }
            if (product_each_charge['othch'].hasOwnProperty(item_id)) {
                delete product_each_charge['othch'][item_id][item_code];

            }

            if (item_detail_id != '0') {
                var position = delete_array.indexOf(item_detail_id);
                if (position) {
                    delete_array.push(item_detail_id);
                }
            }

        }


        if (~position) {
            stock_details_array.splice(position, 1);
        }
        delete item_array_new[item_id];
        if (document.getElementById('itemdetailselect' + item_code)) {
            $('#ietmcodeitemdesc' + item_id).remove();

        }
        stock_details_array.splice(position, 1);
        if (document.getElementById('itemadd_row' + item_id)) {
            $('#itemadd_row' + item_id).remove();
            add_item--;
        }
        if (document.getElementById('ietmcodeitemdesc' + item_id)) {
            $('#ietmcodeitemdesc' + item_id).remove();
        }
        if (document.getElementById('itemdetailselect' + item_code)) {
            document.getElementById('itemdetailselect' + item_code).checked = false;
        }

        locationEnable();
    }
}

function stockRemove(obj) {
    var item_id = $('#' + obj.id).attr('value');
    var item_code = $('#item_codeid' + item_id).attr('value');

    newitem_manage(item_code, 'remove', item_id);
}
function checkPurRateMrp() {
    var mrp = 0;
    var pur_rate = 0;
    var f = 0;
    $('input[name="pur_rate[]"]').each(function (key, val) {
        pur_rate = parseFloat($(this).val());
        mrp = parseFloat($(this).closest("tr").find("input[name='mrp_qty[]']").val());
        if (mrp < pur_rate) {
            $(this).addClass('text_boxalert');
            $(this).closest("tr").find("input[name='mrp_qty[]']").addClass('text_boxalert');
            f = 1;
        } else {
            $(this).removeClass('text_boxalert');
            $(this).closest("tr").find("input[name='mrp_qty[]']").removeClass('text_boxalert');
        }
    });
    if (f == 1) {
        toastr.warning('Purchase rate greater than MRP !!');
    }
    return f;
}
var round_flag = 1;
var param_data = new Array();

function po_confirm(post_type) {
    var msg = '';
    if (post_type == 4) {
        msg = "Do you want to Amend this Purchase Order ?"
    }
    if (post_type == 15) {
        msg = "Do you want to Amend this PO Request ? "
    }
    if (post_type == 17) {
        msg = "Do you want to Approve Amend ? "
    }
    if (confirm(msg)) {
        savePurchaseOrder(post_type);
    } else {

    }

}
function savePurchaseOrder(post_type, amend_type = '') {
    var cgst_found = igst_found = sgst_found = 0;
    BillDiscountcalculate(1);
    var disc_type = $('#bill_discount_type').val();
    var disc_mode = $('#disc_mode').val();
    var dis_value = $('#bill_discount_value').val();
    var disc_amnt = $('.total_discount').html();
    var from_disc = $('#from_discounted').is(":checked");
    var multi_select_list = new Array();
    var po_date = $('#po_date').val();
    var po_type = $('#add_edit_status').val();
    var file_token = $('#hidden_filetoken').val();
    var is_amended = $('#is_amended').val();
    var round_off = $('#round_off').val();
    var delivery_note = $('#delivery_id').val();
    var is_mail_enabled = $("#enable_mail").prop("checked");
    var val_res;
    var mail_body = "";
    if (is_mail_enabled == 1 && post_type == 3) {
        var text = tinymce.get("mail_template_text").getContent();
        mail_body = btoa(text);
    }

    var insert_array_length = Object.keys(item_array_new).length;
    if (insert_array_length != 0) {
        if (po_date) {
            var flag = true;
            var pur_qty = 0.0;
            var pur_unit = 0;
            var pur_rate = 0;
            var free_qty = 0;
            var item_code = '';
            var mrp_qty = 0.0;
            var vat_on = 0;
            var free_vat_on = 0;
            var tamount_qty = 0.0;
            var remarks_po = '';
            var free_item = 0;
            var item_urgent = '';
            var post_data_type = 0;
            var po_detail_id = 0;
            var is_exclusive = 0;
            var item_id = 0;
            var disc_perc_tot = 0;
            var tax_total_free_perc = 0;
            var tax_total_perc = 0;
            var tax_total = 0;
            var free_tax_total = 0;
            var disc_total = 0;
            var other_charges = 0;
            var selling_price = 0;
            var selling_price_check = 0;
            var unit_mrp = 0;

            is_valid = true;
            var po_stats = 0;
            $('#added_new_list_table_product tr').each(function () {

                po_stats = $('#po_status').val();
                if ($.type($(this).find('input[name="edit_status[]"]').val()) != "undefined") {
                    post_data_type = $(this).find('input[name="edit_status[]"]').val();
                }

                if ($.type($(this).find('input[name="item_code[]"]').val()) != "undefined") {
                    item_code = $(this).find('input[name="item_code[]"]').val();
                }

                if ($.type($(this).find('input[name="po_detail_id[]"]').val()) != "undefined") {
                    po_detail_id = $(this).find('input[name="po_detail_id[]"]').val();
                }

                if ($.type($(this).find('input[name="pur_qty[]"]').val()) != "undefined") {
                    pur_qty = $(this).find('input[name="pur_qty[]"]');
                    flag = validate_number(pur_qty, 2);
                    if (flag) {
                        pur_qty = $(this).find('input[name="pur_qty[]"]').val();
                        var is_free = $(this).find('select[name="free_item[]"]').val();
                        if (is_free != 1) {
                            if ((pur_qty == '' || pur_qty == 0)) {
                                toastr.error('Purchase quantity should not be zero!!');
                                is_valid = false;
                                return;
                            }
                        }

                    } else {
                        toastr.error('Not a valid number!!');
                        is_valid = false;
                        return;
                    }
                }
                if ($.type($(this).find('select[name="pur_unit[]"]').val()) != "undefined") {
                    pur_unit = $(this).find('select[name="pur_unit[]"]').val();
                }
                if ($.type($(this).find('input[name="pur_rate[]"]').val()) != "undefined") {
                    pur_rate = $(this).find('input[name="pur_rate[]"]');
                    flag = validate_number(pur_rate, 2);
                    if (flag) {
                        pur_rate = $(this).find('input[name="pur_rate[]"]').val();
                        var is_free = $(this).find('select[name="free_item[]"]').val();
                        if (is_free != 1) {
                            if (pur_rate == '' || pur_rate == 0) {
                                toastr.error('Purchase Rate should not be zero!!');
                                is_valid = false;
                                return;
                            }
                        }
                    } else {
                        toastr.error('Not a valid number!!');
                        is_valid = false;
                        return;
                    }
                }

                if ($.type($(this).find('input[name="free_qty[]"]').val()) != "undefined") {
                    free_qty = $(this).find('input[name="free_qty[]"]');
                    flag = validate_number(free_qty, 2);
                    if (flag) {
                        free_qty = $(this).find('input[name="free_qty[]"]').val();
                    } else {
                        toastr.error('Not a valid number!!');
                        is_valid = false;
                        return;
                    }
                }
                if ($.type($(this).find('input[name="mrp_qty[]"]').val()) != "undefined") {
                    mrp_qty = $(this).find('input[name="mrp_qty[]"]');
                    flag = validate_number(mrp_qty, 2);
                    if (flag) {
                        mrp_qty = $(this).find('input[name="mrp_qty[]"]').val();
                    } else {
                        toastr.error('Not a valid number!!');
                        is_valid = false;
                        return;
                    }
                }
                if ($.type($(this).find('input[name="selling_price_qty[]"]').val()) != "undefined") {
                    selling_price = $(this).find('input[name="selling_price_qty[]"]');
                    flag = validate_number(selling_price, 2);
                    if (flag) {
                        selling_price = $(this).find('input[name="selling_price_qty[]"]').val();
                    } else {
                        toastr.error('Not a valid number!!');
                        is_valid = false;
                        return;
                    }
                }

                if ($(this).find('input[name="selling_price_check[]"]').prop("checked") == true) {
                    selling_price_check = 1;
                } else {
                    selling_price_check = 0;
                }

                vat_on = 2;
                free_vat_on = 2;
                if ($.type($(this).find('select[name="vat_on[]"]').val()) != "undefined") {
                    vat_on = 2;
                }
                if ($.type($(this).find('select[name="free_vat_on[]"]').val()) != "undefined") {
                    free_vat_on = 2;
                }
                if ($.type($(this).find('select[name="free_item[]"]').val()) != "undefined") {
                    free_item = $(this).find('select[name="free_item[]"]').val();
                }
                if ($.type($(this).find('input[name="item_urgent[]"]').is(":checked")) != "undefined") {
                    item_urgent = $(this).find('input[name="item_urgent[]"]').is(":checked");
                }
                if ($.type($(this).find('input[name="remarks_po[]"]').val()) != "undefined") {
                    remarks_po = $(this).find('input[name="remarks_po[]"]').val();
                }
                if ($.type($(this).find('input[name="tamount_qty[]"]').val()) != "undefined") {
                    tamount_qty = $(this).find('input[name="tamount_qty[]"]');
                    flag = validate_number(tamount_qty, 2);
                    if (flag) {
                        tamount_qty = $(this).find('input[name="tamount_qty[]"]').val();
                    } else {
                        toastr.error('Not a valid number!!');
                        is_valid = false;
                        return;
                    }
                }
                if ($.type($(this).find('input[name="item_exclusive[]"]').is(":checked")) != "undefined") {
                    is_exclusive = $(this).find('input[name="item_exclusive[]"]').is(":checked");
                }
                if ($.type($(this).find('input[name="inserted_row_id[]"]').val()) != "undefined") {
                    item_id = $(this).find('input[name="inserted_row_id[]"]').val();
                }

                is_exclusive = 1;
                if (item_id) {
                    is_exclusive = $('#exclusive_checkbox' + item_id).is(":checked");
                }

                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].tot_disc_perc) != 'undefined')
                        disc_perc_tot = product_each_charge['total'][item_id][item_code].tot_disc_perc;
                }

                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].tot_free_tax_perc) != 'undefined')
                        tax_total_free_perc = product_each_charge['total'][item_id][item_code].tot_free_tax_perc;
                }

                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].tot_tax_perc) != 'undefined')
                        tax_total_perc = product_each_charge['total'][item_id][item_code].tot_tax_perc;
                }

                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].tax_amount) != 'undefined')
                        tax_total = product_each_charge['total'][item_id][item_code].tax_amount;
                }

                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].tax_amount_free) != 'undefined')
                        free_tax_total = product_each_charge['total'][item_id][item_code].tax_amount_free;

                }
                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].disc_amount) != 'undefined')
                        disc_total = product_each_charge['total'][item_id][item_code].disc_amount;
                }

                if (typeof (product_each_charge['total'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['total'][item_id][item_code].other_charges) != 'undefined')
                        other_charges = product_each_charge['total'][item_id][item_code].other_charges;
                }
                var charge_data = JSON.stringify(item_charge_array[item_id]);
                if (typeof (product_each_charge['tax'][item_id]) != 'undefined') {
                    if (typeof (product_each_charge['tax'][item_id][item_code]['percentage'][CGST_ID]) != 'undefined') {
                        if (product_each_charge['tax'][item_id][item_code]['percentage'][CGST_ID] != 0)
                            cgst_found = true;
                    }
                    if (typeof (product_each_charge['tax'][item_id][item_code]['percentage'][IGST_ID]) != 'undefined') {
                        if (product_each_charge['tax'][item_id][item_code]['percentage'][IGST_ID] != 0)
                            igst_found = true;
                    }
                    if (typeof (product_each_charge['tax'][item_id][item_code]['percentage'][SGST_ID]) != 'undefined') {
                        if (product_each_charge['tax'][item_id][item_code]['percentage'][SGST_ID] != 0)
                            sgst_found = true;
                    }
                }
                if ((cgst_found == true && igst_found == true) || (sgst_found == true && igst_found == true)) {
                    alert('The Bill Contain CGST & IGST Tax');
                    getProductCharges(item_id, item_code);
                    is_valid = false;
                    return false;
                }
                unit_mrp = parseFloat($(this).find('input[name="unit_mrp_hiden[]"]').val());
                unit_mrp = unit_mrp.toFixed(2);
                var sell_price = parseFloat($(this).find('input[name="selling_price_qty[]"]').val());
                sell_price = sell_price.toFixed(2);
                if (parseFloat(sell_price) > parseFloat(unit_mrp)) {
                    $(this).find('input[name="selling_price_qty[]"]').focus();
                    $(this).find('input[name="selling_price_qty[]"]').addClass('text_boxalert');
                    toastr.error("Selling Price Grater than Unit MRP !!");
                    is_valid = false;
                }

                else {
                    $(this).find('input[name="selling_price_qty[]"]').removeClass('text_boxalert');
                }

                multi_select_list.push({
                    'item_code': item_code
                    , 'po_detail_id': po_detail_id
                    , 'pur_qty': pur_qty
                    , 'pur_unit': pur_unit
                    , 'pur_rate': pur_rate
                    , 'free_qty': free_qty
                    , 'mrp_qty': mrp_qty
                    , 'vat_on': vat_on
                    , 'free_vat_on': free_vat_on
                    , 'tamount_qty': tamount_qty
                    , 'free_item': free_item
                    , 'remarks_po': remarks_po
                    , 'item_urgent': item_urgent ? item_urgent : 0
                    , 'is_exclusive': is_exclusive ? is_exclusive : 0
                    , 'charge_data': charge_data
                    , 'tot_disc_perc': disc_perc_tot
                    , 'tot_free_tax_perc': tax_total_free_perc
                    , 'tot_tax_perc': tax_total_perc
                    , 'tax_total': tax_total
                    , 'free_tax_total': free_tax_total
                    , 'disc_total': disc_total
                    , 'other_charges': other_charges
                    , 'selling_price': selling_price
                    , 'selling_price_check': selling_price_check
                    , 'unit_mrp': unit_mrp


                });
            });
            if ($('#deprt_list').val() == "" || $('#deprt_list').val() == null) {
                toastr.error('Please Select Location !!');
                is_valid = false;
                return;
            }
            if ($('#vendor_list_po2').val() == "" || $('#vendor_list_po_hidden2').val() == "") {
                toastr.error('Select Supplier!!');
                is_valid = false;
                return;
            }
            var adjust_amount = $('#round_off').val();
            if (isNaN(adjust_amount)) {
                $('#round_off').css('background', '#ffc1c1');
                toastr.error('Not a valid number!!');
                is_valid = false;
                return;
            } else {
                $('#round_off').css('background', '#fff');
            }

            var terms_condition = JSON.stringify(purchase_order_condition_array);
            var delete_array_send = JSON.stringify(delete_array);

            var pr_req_item = purchase_requisition;
            var po_req = JSON.stringify(item_manage_array);
            var payment_warranty = $('#payment_warranty').val();
            var payment_dispatchmode = $('#payment_dispatchmode').val();
            var item_value = $('#item_value').val();
            var payment_other = $('#payment_other').val();
            var location = $('#deprt_list').val();
            var payment_othercharges = $('#payment_othercharges').val();

            var po_added = $('#po_added').val();
            var vendor_list = $('#vendor_list_po_hidden2').val();

            var manufacturer_list = $('#manufacturer_list').val();
            var quotation_date = $('#quotation_date').val();
            var charge_term = $('#charge_term').val();
            var delivery_notes = $('#delivery_notes').val();
            var requisition_no = $('#requisition_no').val();
            var department = $('#department').val();
            var payment_date = $('#payment_date').val();
            var payment_in = $('#payment_in').val();
            var payment_terms = $('#payment_terms').val();
            var payment_remarks = $('#payment_remarks').val();
            var round_off = $('#round_off').val();
            var po_amount = $('#po_amount').val();
            if ($('#round_off').val() != '') {
                round_flag = 0;
                var po_amount = $('#po_amount').val();
                var adjust_amount = parseFloat($('#round_off').val());
                if (isNaN(adjust_amount)) {
                    adjust_amount = 0;
                }
                var tot_amount = parseFloat($('#po_amount').val());
                tot_amount = tot_amount + adjust_amount;
                rund_val = adjust_amount;
                tot_amount = tot_amount.toFixed(2);
                $('#po_amount').val(tot_amount);
                po_amount = tot_amount;
                round_flag = 1;
            }
            var param = {
                post_type: post_type
                , po_type: po_type
                , payment_warranty: payment_warranty
                , payment_dispatchmode: payment_dispatchmode
                , item_value: item_value
                , payment_other: payment_other
                , payment_othercharges: payment_othercharges
                , po_amount: po_amount
                , po_added: po_added
                , po_date: po_date
                , vendor_list: vendor_list
                , manufacturer_list: manufacturer_list
                , quotation_date: quotation_date
                , charge_term: charge_term
                , location: location
                , requisition_no: requisition_no
                , delivery_notes: delivery_notes
                , payment_date: payment_date
                , department: department
                , payment_in: payment_in
                , payment_terms: payment_terms
                , payment_remarks: payment_remarks
                , annexture_ref1_text: annexture_ref1_text
                , annexture_ref2_text: annexture_ref2_text
                , annexture_data_text: annexture_data_text
                , additionaltems_text: additionaltems_text
                , amend_type: amend_type
                , is_amended: is_amended
                , disc_type: disc_type
                , disc_mode: disc_mode
                , dis_value: dis_value
                , disc_amnt: disc_amnt
                , from_disc: from_disc
                , mail_body: mail_body
                , delivery_note: delivery_note
                , round_off: round_off
            };
            var po_data = JSONH.pack(multi_select_list);

            multi_select_list = new Array();
            if (is_valid && round_flag == 1) {
                $.ajax({
                    type: "POST",
                    url: '',
                    data: { po_req: po_req, pr_req_item: pr_req_item, delete_array: delete_array_send, terms_condition: terms_condition, param: param, po_data: po_data, po_savedata: 1, is_mail_enabled: is_mail_enabled, _token: file_token },

                    beforeSend: function () {
                        $(".po_list_bttn").prop('disabled', true);
                        if (post_type == '0') {
                            $('#po_cancel_spin').removeClass('fa fa-fast-backward');
                            $('#po_cancel_spin').addClass('fa fa-spinner fa-spin');
                        } else if (post_type == '1') {
                            $('#po_post_spin').removeClass('fa fa-paper-plane-o');
                            $('#po_post_spin').addClass('fa fa-spinner fa-spin');
                        } else if (post_type == '2') {
                            $('#po_close_spin').removeClass('fa fa-times-circle-o');
                            $('#po_close_spin').addClass('fa fa-spinner fa-spin');
                        } else if (post_type == '3') {
                            $('#po_approve_spin').removeClass('fa fa-buysellads');
                            $('#po_approve_spin').addClass('fa fa-spinner fa-spin');
                        }
                    },

                    success: function (result) {
                        result = JSON.parse(result);
                        if (result.success == 0) {
                            if (post_type == '16') {
                                alert(result.msg);
                                $(".po_list_bttn").prop('disabled', false);
                                return false;
                            } else {
                                toastr.success("PO Updation Failed..Try Again !!");
                            }
                        }
                        else if (result.success == 1 && result.type == 1) {
                            if (post_type == 2)
                                toastr.success("PO Closed Successfully PO NO :- (" + result.request_no + ")");
                            else if (post_type == 0)
                                toastr.success("PO Cancelled Successfully PO NO :- (" + result.request_no + ")");
                            else if (post_type == 4)
                                toastr.success("PO Amended Successfully PO NO :- (" + result.request_no + ")");
                            else if (post_type == 6)
                                toastr.success("PO Verified Successfully PO NO :- (" + result.request_no + ")");
                            else if (post_type == 15)
                                toastr.success("Send PO Amend Request of PO NO :- (" + result.request_no + ")");
                            else
                                toastr.success("PO Created Successfully PO NO :- (" + result.request_no + ")");
                        } else if (result.success == 1 && result.type == 2) {
                            if (post_type == 3)
                                toastr.success("PO Approved Successfully PO NO :- (" + result.request_no + ")");
                            else if (post_type == 6)
                                toastr.success("PO Verified Successfully PO NO :- (" + result.request_no + ")");
                            else if (post_type == '16')
                                toastr.success("PO :- (" + result.request_no + ") Revert to Verify status ");
                            else
                                toastr.success("PO Updated Successfully PO NO :- (" + result.request_no + ")");
                        } else if (result.success == 1 && result.type == 3) {
                            toastr.warning("Maximun Amend Number Exceded !!");
                        }


                        if (post_type == 3 && is_mail_enabled) {
                            if (result.mailResult == 'email-error') {
                                toastr.error("Vender Email Id Error Please Update!!");
                            } else {
                                toastr.info("Email will send to Vender !!");
                            }
                        }
                        if (result.success == 1) {
                            var location_url = $('#po_req_url').val();

                            window.location.href = location_url;

                        }

                    },
                    error: function (xhr) {

                    },
                    complete: function () {
                        if (post_type == '0') {
                            $('#po_cancel_spin').removeClass('fa fa-spinner fa-spin');
                            $('#po_cancel_spin').addClass('fa fa-fast-backward');
                        } else if (post_type == '1') {
                            $('#po_post_spin').removeClass('fa fa-spinner fa-spin');
                            $('#po_post_spin').addClass('fa fa-paper-plane-o');
                        } else if (post_type == '2') {
                            $('#po_close_spin').removeClass('fa fa-spinner fa-spin');
                            $('#po_close_spin').addClass('fa fa-times-circle-o');
                        } else if (post_type == '3') {
                            $('#po_approve_spin').removeClass('fa fa-spinner fa-spin');
                            $('#po_approve_spin').addClass('fa fa-buysellads');
                        }

                    }
                });
            }
        } else {
            toastr.warning("Please Enter PO Date");
        }
    } else {
        toastr.warning("Please Enter any items in List!!!");
    }

}

function validate_number(obj, post_type, vali_data) {
    var num = 0.0;
    if (post_type == '1') {
        num = obj.value;
    } else if (post_type == '2') {
        num = obj.val();
    }
    if (num) {
        if (isNaN(num)) {
            $('#' + obj.id).addClass('text_boxalert');
            return false;
        }
        else if (num.indexOf(' ') >= 0) {
            $('#' + obj.id).addClass('text_boxalert');
            $('#' + obj[0].id).addClass('text_boxalert');
            return false;
        }
        else {
            $('#' + obj.id).removeClass('text_boxalert');
            return true;
        }
    } else {
        if (vali_data == '1') {
            $('#' + obj.id).removeClass('text_boxalert');
            return true;
        } else {
            $('#' + obj.id).addClass('text_boxalert');
            return false;
        }
    }
}

function cal_typechange() {
    var cal_type = $('#calcuation_type').val();
    if (cal_type == '1') {
        $(".calcu_percent").attr('readonly', 'readonly');
        $(".calcu_amount").not(".data-entry-type").removeAttr('readonly');
    } else if (cal_type == '2') {
        $(".calcu_amount").attr('readonly', 'readonly');
        $(".calcu_percent").not(".data-entry-type").removeAttr('readonly');
    }
}
var product_each_charge = {};
product_each_charge['discount'] = {};
product_each_charge['tax'] = {};
product_each_charge['total'] = {};
product_each_charge['on_tax_amount'] = {};
product_each_charge['tax_free'] = {};
product_each_charge['othch'] = {};
product_each_charge['bill_discount'] = 0
function discount_calculation(type, obj, item_gross, item_code, charge_id, vat_on, item_gross_mrp, gross_free) {
    if (type == 'percentage') {
        perc_val = parseFloat(obj.value);
        amount = item_gross * (perc_val / 100);
        amount = amount.toFixed(2);
        $('#charge_amount' + charge_id).val(amount);
    } else if (type == 'amount') {
        amount = parseFloat(obj.value);
        perc_val = (amount * 100) / item_gross;
        perc_val = perc_val.toFixed(2);
        $('#charge_percentage' + charge_id).val(perc_val);
    }
    if (!product_each_charge['discount'].hasOwnProperty(item_code)) {
        product_each_charge['discount'][item_code] = {};
        product_each_charge['discount'][item_code]['percentage'] = {};
    }
    product_each_charge['discount'][item_code]['percentage'][charge_id] = perc_val;
    re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free);


}

function re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free, is_exclusive, type = '', item_id) {
    var item_gross = parseFloat(item_gross);
    var vat_on = parseFloat(vat_on);
    var item_gross_mrp = parseFloat(item_gross_mrp);
    var gross_free = parseFloat(gross_free);
    var disc_amount = 0;
    var tax_amount = 0;
    var on_tax_amount = 0;
    var tax_amount_free = 0;
    var disc_amount_total = 0;
    var tax_amount_total = 0;
    var tax_perc_total = 0;
    var fract = 1;
    var disc_perc_total = 0;
    var disc_amount_each = 0;
    var division_factor = 1;
    var tax_total_for_discount = 0;
    var from_disc = $('#from_discounted').is(":checked");

    if (product_each_charge['tax'].hasOwnProperty(item_id)) {
        $.each(product_each_charge['tax'][item_id][item_code], function (i, val) {
            if (i != 'percentage')
                tax_total_for_discount += parseFloat(val);
            else {
                $.each(product_each_charge['tax'][item_id][item_code]['percentage'], function (i, val) {
                    tax_perc_total += val;
                });
            }
        });
    }

    if (product_each_charge['discount'].hasOwnProperty(item_id)) {

        var disc_amount_except_bill = 0;
        if (product_each_charge['discount'].hasOwnProperty(item_id) && from_disc == true) {
            $.each(product_each_charge['discount'][item_id][item_code], function (i, val) {
                if (i != 'percentage') {
                    var disc_id_array = i.split('charge_percentage_');
                    var disc_id = disc_id_array[1];
                    if (disc_id != billDiscount)
                        disc_amount_except_bill += parseFloat(val);

                }

            });
        }


        $.each(product_each_charge['discount'][item_id][item_code]['percentage'], function (i, val) {
            var charge_id = parseFloat(i);
            var percentage = parseFloat(val);
            disc_perc_total += percentage;
            if (is_exclusive == 1) {
                if (i == billDiscount && from_disc == true) {

                    disc_amount = parseFloat(item_gross - disc_amount_except_bill) * (percentage / 100);
                } else {
                    disc_amount = parseFloat(item_gross) * (percentage / 100);
                }
                disc_amount_total += parseFloat(disc_amount);
            } else {
                if (tax_perc_total > 0)
                    fract = ((100 + parseFloat(tax_perc_total)) / 100);
                var total_rate_for_discount = parseFloat(item_gross) / fract;

                if (percentage == 100)
                    division_factor = 100;
                else
                    division_factor = 100 - percentage;

                var amount_after_discount_deduct = (total_rate_for_discount / (division_factor)) * 100;

                disc_amount = amount_after_discount_deduct * (percentage / 100);
                disc_amount = disc_amount.toFixed(2);
                disc_amount_total += parseFloat(disc_amount);


            }
            $('#charge_amount' + charge_id).val(disc_amount);
        });
    }
    if (product_each_charge['tax'].hasOwnProperty(item_id)) {
        $.each(product_each_charge['tax'][item_id][item_code]['percentage'], function (i, val) {
            var charge_id = parseFloat(i);
            var percentage = parseFloat(val);
            tax_amount = 0;
            if (vat_on == 2) {
                if (is_exclusive == 1)
                    tax_amount = (parseFloat(item_gross) - parseFloat(disc_amount_total)) * (percentage / 100);
                else {

                    var fract = ((100 + parseFloat(percentage)) / 100);
                    var rate_without_tax = (item_gross) / fract;

                    tax_amount = rate_without_tax * (percentage / 100);

                }
            } else if (vat_on == 1) {
                if (is_exclusive == 1)
                    tax_amount = (item_gross_mrp) * (percentage / 100);
                else {

                    var fract = ((100 + parseFloat(percentage)) / 100);
                    var rate_without_tax = (item_gross_mrp) / fract;
                    if (disc_perc_total > 0) {
                        disc_amount_each = rate_without_tax * (disc_perc_total / 100);
                    }
                    var rate_without_tax_minus_disc = rate_without_tax - disc_amount_each;
                    tax_amount = rate_without_tax_minus_disc * (percentage / 100);
                }
            }
            product_each_charge['tax'][item_id][item_code]['charge_percentage_' + charge_id] = parseFloat(tax_amount);
            tax_amount_total += parseFloat(tax_amount);
            tax_amount = parseFloat(tax_amount);
            $('#charge_amount' + charge_id).val(tax_amount.toFixed(2));
        });
    }
    var str = JSON.stringify(product_each_charge['on_tax_amount']);
    if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
        $.each(product_each_charge['on_tax_amount'][item_id][item_code]['percentage'], function (i, val) {
            var charge_id = parseFloat(i);
            var percentage = parseFloat(val);
            on_tax_amount = 0;
            if (is_exclusive == 1)
                on_tax_amount = (parseFloat(tax_amount_total)) * (parseFloat(percentage) / 100);
            else {
                var fract = ((100 + parseFloat(percentage)) / 100);
                on_tax_amount = (parseFloat(tax_amount_total)) / fract;
                on_tax_amount = (parseFloat(tax_amount_total)) - on_tax_amount;
            }
            product_each_charge['on_tax_amount'][item_id][item_code]['charge_percentage_' + charge_id] = on_tax_amount;
            $('#charge_amount' + charge_id).val(on_tax_amount.toFixed(2));
        });
    }
    if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
        $.each(product_each_charge['tax_free'][item_id][item_code]['percentage'], function (i, val) {
            var charge_id = parseFloat(i);
            var percentage = parseFloat(val);
            tax_amount_free = 0;
            if (is_exclusive == 1)
                tax_amount_free = (parseFloat(gross_free)) * (parseFloat(percentage) / 100);
            else {
                var fract = ((100 + parseFloat(percentage)) / 100);
                tax_amount_free = (parseFloat(gross_free)) / fract;
                tax_amount_free = (parseFloat(gross_free)) - tax_amount_free;
            }
            product_each_charge['tax_free'][item_id][item_code]['charge_percentage_' + charge_id] = parseFloat(tax_amount_free);
            $('#charge_amount' + charge_id).val(tax_amount_free.toFixed(2));
        });
    }
}
function calculationExtraCharges(obj, type, item_id, item_code, calc_code, charge_code, charge_id, is_tax, is_exclusive) {
    if (obj.type != 'popup') {
        if (obj.hasAttribute("readonly")) {
            if (!obj.hasAttribute("data-entry-type"))
                return;
        }
    }

    var other_charge = 0;
    var base_rate = 0;
    var each_total = 0;
    var amount = 0;
    var perc_val = 0;
    var pur_qty = parseFloat($('#pur_qtyid' + item_id).val());
    var par_rate = parseFloat($('#pur_rateid' + item_id).val());
    var free_qty = parseFloat($('#free_qtyid' + item_id).val());
    var mrp = parseFloat($('#mrp_qtyid' + item_id).val());
    var vat_on = parseFloat($('#vat_onselect' + item_id).val());
    var free_vat_on = parseFloat($('#free_vat_onselect' + item_id).val());
    var from_disc = $('#from_discounted').is(":checked");

    var calcuation_type = $('#calcuation_type').val();
    var free_item = parseFloat($('#free_item' + item_id).val());
    var item_gross = parseFloat(pur_qty) * parseFloat(par_rate);

    var item_gross_mrp = parseFloat(pur_qty) * parseFloat(mrp);
    var disc_amount = 0;
    var disc_amount_each = 0;
    var disc_amount_perc = 0;
    var tax_amount = 0;
    var tax_amount_free = 0;
    var gross_total = 0;
    var others_total = 0;
    var on_tax_amount = 0;
    var gross_free = 0;
    var tax_total_for_discount = 0;
    var tax_perc_total = 0;
    var division_factor = 1;
    var tot_tax_perc = 0
    var tot_disc_perc = 0;
    var tot_free_tax_perc = 0;


    var is_exclusive = true;

    if (free_vat_on == 2)
        gross_free = parseFloat(free_qty) * parseFloat(par_rate);
    if (free_vat_on == 1)
        gross_free = parseFloat(free_qty) * parseFloat(mrp);
    if (obj.value == '') {
        $('#' + obj.id).val(0);
    }
    var num = obj.value;
    if (num || num == 0) {
        num = parseFloat(num);
        if (isNaN(num)) {
            $('#' + obj.id).addClass('text_boxalert');
            $('#' + obj.id).val(0);

        } else {
            num = parseFloat(num);
            $('#' + obj.id).removeClass('text_boxalert');
            if (type == 'percentage' || type == 'amount') {
                perc_val = parseFloat(obj.value);
                if (calc_code == 'OTHCH') {
                    if (type == 'amount')
                        $('#charge_percentage' + charge_id).val(perc_val);
                    else
                        $('#charge_amount' + charge_id).val(perc_val);

                    if (!product_each_charge['othch'].hasOwnProperty(item_id)) {

                        product_each_charge['othch'][item_id] = {};
                        product_each_charge['othch'][item_id][item_code] = {};
                        product_each_charge['othch'][item_id][item_code]['percentage'] = {};
                    }
                    product_each_charge['othch'][item_id][item_code]['charge_percentage_' + charge_id] = perc_val;
                    product_each_charge['othch'][item_id][item_code]['percentage'][charge_id] = perc_val;

                }

                if (charge_code == 'SUB' && calc_code == 'GA' && item_gross > 0) {

                    if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                        $.each(product_each_charge['tax'][item_id][item_code], function (i, val) {
                            if (i != 'percentage')
                                tax_total_for_discount += parseFloat(val);
                            else {
                                $.each(product_each_charge['tax'][item_id][item_code]['percentage'], function (i, val) {
                                    tax_perc_total += val;
                                });
                            }

                        });
                    }

                    var disc_amount_except_bill = 0;
                    if (product_each_charge['discount'].hasOwnProperty(item_id) && from_disc == true) {
                        $.each(product_each_charge['discount'][item_id][item_code], function (i, val) {
                            if (i != 'percentage') {
                                var disc_id_array = i.split('charge_percentage_');
                                var disc_id = disc_id_array[1];
                                if (disc_id != billDiscount)
                                    disc_amount_except_bill += parseFloat(val);


                            }

                        });
                    }


                    if (is_exclusive == 1) {
                        if (type == 'percentage') {
                            perc_val = parseFloat(obj.value);
                            if (charge_id == billDiscount && from_disc == true) {
                                amount = (item_gross - disc_amount_except_bill) * (perc_val / 100);
                            } else {
                                amount = item_gross * (perc_val / 100);
                            }
                            amount = amount.toFixed(2);
                            $('#charge_amount' + charge_id).val(amount);
                        } else if (type == 'amount') {
                            amount = parseFloat(obj.value);
                            perc_val = (amount * 100) / item_gross;
                            perc_val = perc_val.toFixed(2);
                            $('#charge_percentage' + charge_id).val(perc_val);
                        }

                    } else {


                        if (type == 'percentage') {
                            perc_val = parseFloat(obj.value);

                            var fraction = 1;
                            if (tax_perc_total > 0)
                                fraction = ((100 + parseFloat(tax_perc_total)) / 100);
                            var total_rate_for_discount = item_gross / (fraction);

                            if (perc_val == 100)
                                division_factor = 100;
                            else
                                division_factor = 100 - perc_val;
                            var amount_after_discount_deduct = (total_rate_for_discount / (division_factor)) * 100;
                            amount = amount_after_discount_deduct * (perc_val / 100);


                            amount = amount.toFixed(2);
                            $('#charge_amount' + charge_id).val(amount);

                        } else {

                            var fraction = 1;
                            if (tax_perc_total > 0)
                                fraction = ((100 + parseFloat(tax_perc_total)) / 100);
                            var total_rate_for_discount = item_gross / (fraction);
                            amount = parseFloat(obj.value);
                            perc_val = (amount * 100) / (total_rate_for_discount);
                            perc_val = perc_val.toFixed(2);
                            $('#charge_percentage' + charge_id).val(perc_val);

                        }
                    }
                    if (!product_each_charge['discount'].hasOwnProperty(item_id)) {
                        product_each_charge['discount'][item_id] = {};
                        product_each_charge['discount'][item_id][item_code] = {};
                        product_each_charge['discount'][item_id][item_code]['percentage'] = {};
                    }
                    product_each_charge['discount'][item_id][item_code]['charge_percentage_' + charge_id] = amount;
                    product_each_charge['discount'][item_id][item_code]['percentage'][charge_id] = perc_val;
                    re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free, is_exclusive, type, item_id);
                }
                if (charge_code == 'ADD' && calc_code == 'DA' && is_tax == 1 && item_gross > 0 && calc_code != 'FREE') {

                    if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                        $.each(product_each_charge['discount'][item_id][item_code], function (i, val) {
                            if (i != 'percentage')
                                disc_amount += parseFloat(val);
                            else
                                $.each(product_each_charge['discount'][item_id][item_code]['percentage'], function (i, val) {
                                    disc_amount_perc += val;
                                });
                        });
                    }

                    if (vat_on == 2) {
                        if (type == 'percentage') {
                            if (is_exclusive == 1)
                                amount = (item_gross - disc_amount) * (perc_val / 100);
                            else {

                                var fract = ((100 + parseFloat(perc_val)) / 100);
                                var rate_without_tax = (item_gross) / fract;

                                amount = rate_without_tax * (perc_val / 100);
                            }
                            amount = amount.toFixed(2);
                            $('#charge_amount' + charge_id).val(amount);
                        } else if (type == 'amount') {
                            amount = parseFloat(obj.value);
                            if (is_exclusive == 1)
                                perc_val = (amount * 100) / (item_gross - disc_amount);
                            else {

                                var rate_without_tax = item_gross - amount;
                                var rate_without_tax_minus_disc = rate_without_tax - disc_amount;
                                perc_val = 100 * amount / (rate_without_tax_minus_disc);

                            }
                            perc_val = perc_val.toFixed(2);
                            $('#charge_percentage' + charge_id).val(perc_val);
                        }
                    } else if (vat_on == 1) {
                        if (type == 'percentage') {
                            if (is_exclusive == 1)
                                amount = (item_gross_mrp) * (perc_val / 100);
                            else {
                                var fract = ((100 + parseFloat(perc_val)) / 100);
                                var rate_without_tax = (item_gross_mrp) / fract;
                                if (disc_amount_perc > 0) {
                                    disc_amount_each = rate_without_tax * (disc_amount_perc / 100);
                                }
                                var rate_without_tax_minus_disc = rate_without_tax - disc_amount_each;
                                amount = rate_without_tax_minus_disc * (perc_val / 100);

                            }
                            amount = amount.toFixed(2);
                            $('#charge_amount' + charge_id).val(amount);
                        } else if (type == 'amount') {
                            amount = parseFloat(obj.value);
                            if (is_exclusive == 1)
                                perc_val = (amount * 100) / (item_gross_mrp);
                            else {

                                var rate_without_tax = item_gross_mrp - amount;
                                var rate_without_tax_minus_disc = rate_without_tax - disc_amount;
                                perc_val = 100 * amount / (rate_without_tax_minus_disc);
                            }
                            perc_val = perc_val.toFixed(2);
                            $('#charge_percentage' + charge_id).val(perc_val);
                        }
                    }

                    tax_amount += amount;
                    if (!product_each_charge['tax'].hasOwnProperty(item_id)) {
                        product_each_charge['tax'][item_id] = {};
                        product_each_charge['tax'][item_id][item_code] = {};
                        product_each_charge['tax'][item_id][item_code]['percentage'] = {};
                    }
                    product_each_charge['tax'][item_id][item_code]['charge_percentage_' + charge_id] = amount;
                    product_each_charge['tax'][item_id][item_code]['percentage'][charge_id] = perc_val;
                    re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free, is_exclusive, type, item_id);
                }

                if (charge_code == 'ADD' && calc_code == 'OTA' && item_gross > 0) {
                    var tax_total_ota = 0;
                    if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                        $.each(product_each_charge['tax'][item_id][item_code], function (i, val) {
                            if (i != 'percentage')
                                tax_total_ota += parseFloat(val);
                        });
                    }
                    if (tax_total_ota <= 0) {
                        $('#charge_amount' + charge_id).val(0);
                        $('#charge_percentage' + charge_id).val(0);
                        return;
                    }
                    if (type == 'percentage') {
                        if (is_exclusive == 1)
                            amount = (tax_total_ota) * (perc_val / 100);
                        else {
                            var fract = ((100 + parseFloat(perc_val)) / 100);
                            amount = (tax_total_ota) / fract;
                            amount = (tax_total_ota) - amount;
                        }


                        amount = amount.toFixed(2);
                        $('#charge_amount' + charge_id).val(amount);
                    } else if (type == 'amount') {
                        amount = parseFloat(obj.value);
                        if (is_exclusive == 1)
                            perc_val = (100 * amount) / (tax_total_ota);
                        else {
                            perc_val = (-1 * (tax_total_ota) * 100) / (amount - (tax_total_ota));
                            perc_val = perc_val - 100;

                        }


                        perc_val = perc_val.toFixed(2);
                        $('#charge_percentage' + charge_id).val(perc_val);
                    }
                    if (!product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
                        product_each_charge['on_tax_amount'][item_id] = {};
                        product_each_charge['on_tax_amount'][item_id][item_code] = {};
                        product_each_charge['on_tax_amount'][item_id][item_code]['percentage'] = {};
                    }
                    product_each_charge['on_tax_amount'][item_id][item_code]['charge_percentage_' + charge_id] = amount;
                    product_each_charge['on_tax_amount'][item_id][item_code]['percentage'][charge_id] = perc_val;
                    re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free, is_exclusive, type, item_id);
                }

                if (charge_code == 'ADD' && calc_code == 'FREE' && is_tax == 1 && gross_free > 0) {

                    if (type == 'percentage') {
                        if (is_exclusive == 1)
                            amount = (gross_free) * (perc_val / 100);
                        else {
                            var fract = ((100 + parseFloat(perc_val)) / 100);
                            amount = (gross_free) / fract;
                            amount = (gross_free) - amount;
                        }
                        amount = amount.toFixed(2);
                        $('#charge_amount' + charge_id).val(amount);
                    } else if (type == 'amount') {
                        amount = obj.value;
                        if (is_exclusive == 1)
                            perc_val = (100 * amount) / (gross_free);
                        else {
                            perc_val = (-1 * (tax_total_ota) * 100) / (amount - (tax_total_ota));
                            perc_val = perc_val - 100;
                        }
                        perc_val = perc_val.toFixed(2);
                        $('#charge_percentage' + charge_id).val(perc_val);
                    }
                    if (!product_each_charge['tax_free'].hasOwnProperty(item_id)) {
                        product_each_charge['tax_free'][item_id] = {};
                        product_each_charge['tax_free'][item_id][item_code] = {};
                        product_each_charge['tax_free'][item_id][item_code]['percentage'] = {};
                    }
                    product_each_charge['tax_free'][item_id][item_code]['charge_percentage_' + charge_id] = amount;
                    product_each_charge['tax_free'][item_id][item_code]['percentage'][charge_id] = perc_val;
                    re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free, is_exclusive, type, item_id);
                }

                on_tax_amount = 0;
                var on_tax_amount_calc = 0;
                if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['on_tax_amount'][item_id][item_code], function (i, val) {

                        if (i != 'percentage') {
                            on_tax_amount += parseFloat(val);
                            on_tax_amount_calc = parseFloat(val);
                            if (!item_charge_array.hasOwnProperty(item_id)) {
                                item_charge_array[item_id] = {};
                                item_charge_array[item_id][item_code] = {};
                            }

                            $.each(product_each_charge['on_tax_amount'][item_id][item_code]['percentage'], function (ch_id, per_val) {
                                item_charge_array[item_id][item_code][ch_id] = per_val + ':' + on_tax_amount_calc;
                            });

                        }

                    });

                }
                disc_amount = 0;
                var disc_amount_calc = 0;

                if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['discount'][item_id][item_code], function (i, val) {
                        if (i != 'percentage')
                            disc_amount += parseFloat(val);
                        else {

                            $.each(val, function (charge, percentage) {
                                tot_disc_perc += parseFloat(percentage);
                                if (!item_charge_array.hasOwnProperty(item_id)) {
                                    item_charge_array[item_id] = {};
                                    item_charge_array[item_id][item_code] = {};
                                }
                                disc_amount_calc = product_each_charge['discount'][item_id][item_code]['charge_percentage_' + charge];
                                item_charge_array[item_id][item_code][charge] = percentage + ':' + disc_amount_calc;

                            });

                        }
                    });
                }
                tax_amount = 0;
                var tax_amount_calc = 0;
                if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['tax'][item_id][item_code], function (i, val) {

                        if (i != 'percentage')
                            tax_amount += parseFloat(val);
                        else {
                            $.each(val, function (charge, percentage) {
                                tot_tax_perc += parseFloat(percentage);
                                if (!item_charge_array.hasOwnProperty(item_id)) {
                                    item_charge_array[item_id] = {};
                                    item_charge_array[item_id][item_code] = {};
                                }
                                tax_amount_calc = product_each_charge['tax'][item_id][item_code]['charge_percentage_' + charge];
                                item_charge_array[item_id][item_code][charge] = percentage + ':' + tax_amount_calc;
                            });

                        }
                    });
                }
                tax_amount_free = 0;
                var tax_amount_free_calc = 0;

                if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['tax_free'][item_id][item_code], function (i, val) {
                        if (i != 'percentage')
                            tax_amount_free += parseFloat(val);
                        else {

                            $.each(val, function (charge, percentage) {
                                tot_free_tax_perc += parseFloat(percentage);
                                if (!item_charge_array.hasOwnProperty(item_id)) {
                                    item_charge_array[item_id] = {};
                                    item_charge_array[item_id][item_code] = {};
                                }
                                tax_amount_free_calc = product_each_charge['tax_free'][item_id][item_code]['charge_percentage_' + charge];
                                item_charge_array[item_id][item_code][charge] = percentage + ':' + tax_amount_free_calc;
                            });
                        }

                    });
                }
                var other_charge_calc = 0;
                if (product_each_charge['othch'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['othch'][item_id][item_code], function (i, val) {
                        if (i != 'percentage')
                            other_charge += parseFloat(val);
                        else {
                            $.each(val, function (charge, percentage) {
                                if (!item_charge_array.hasOwnProperty(item_id)) {
                                    item_charge_array[item_id] = {};
                                    item_charge_array[item_id][item_code] = {};
                                }
                                other_charge_calc = product_each_charge['othch'][item_id][item_code]['charge_percentage_' + charge];
                                item_charge_array[item_id][item_code][charge] = percentage + ':' + other_charge_calc;
                            });
                        }
                    });
                }

                if (!product_each_charge['total'].hasOwnProperty(item_id)) {
                    product_each_charge['total'][item_id] = {};
                    product_each_charge['total'][item_id][item_code] = {};
                }

                if (is_exclusive == 1)
                    base_rate = item_gross
                else
                    base_rate = item_gross - tax_amount;

                if (free_item == 1) {
                    item_gross = 0;
                    base_rate = 0;
                }
                product_each_charge['total'][item_id][item_code].gross = item_gross;
                product_each_charge['total'][item_id][item_code].disc_amount = parseFloat(disc_amount);
                product_each_charge['total'][item_id][item_code].tax_amount = parseFloat(tax_amount);
                product_each_charge['total'][item_id][item_code].on_tax_amount = parseFloat(on_tax_amount);
                product_each_charge['total'][item_id][item_code].tax_amount_free = parseFloat(tax_amount_free);
                product_each_charge['total'][item_id][item_code].base_rate = base_rate;
                product_each_charge['total'][item_id][item_code].other_charges = other_charge;


                product_each_charge['total'][item_id][item_code].tot_tax_perc = tot_tax_perc;
                product_each_charge['total'][item_id][item_code].tot_disc_perc = tot_disc_perc;
                product_each_charge['total'][item_id][item_code].tot_free_tax_perc = tot_free_tax_perc;



                var gross_total = 0;
                var disc_amount_total = 0;
                var tax_amount_total = 0;
                var on_tax_amount_total = 0;
                var tax_amount_free_total = 0;
                var po_total = 0;
                var other_charges = 0;
                $.each(product_each_charge['total'], function (i, val) {
                    $.each(product_each_charge['total'][i], function (key, value) {

                        $.each(product_each_charge['total'][i][key], function (keyNew, valueNew) {


                            if (keyNew == 'base_rate') {
                                gross_total += parseFloat(valueNew);
                            } else if (keyNew == 'disc_amount')
                                disc_amount_total += parseFloat(valueNew);
                            else if (keyNew == 'tax_amount') {
                                tax_amount_total += parseFloat(valueNew);
                            } else if (keyNew == 'on_tax_amount')
                                on_tax_amount_total += parseFloat(valueNew);
                            else if (keyNew == 'tax_amount_free')
                                tax_amount_free_total += parseFloat(valueNew);
                            else if (keyNew == 'other_charges')
                                other_charges += parseFloat(valueNew);
                        });

                    });
                });


                po_total = parseFloat(gross_total) - parseFloat(disc_amount_total) + parseFloat(tax_amount_total) + parseFloat(on_tax_amount_total) + parseFloat(tax_amount_free_total) + parseFloat(other_charges);

                each_total = parseFloat(base_rate) - parseFloat(disc_amount) + parseFloat(on_tax_amount) + parseFloat(tax_amount_free) + parseFloat(tax_amount) + parseFloat(other_charge);

                $('#charge_name_text').val(each_total.toFixed(2));
                $('#tamountid' + item_id).val(each_total.toFixed(2));
                $('#item_value').val(gross_total.toFixed(2));
                var payment_otr = po_total - gross_total;
                $('#payment_other').val(disc_amount_total.toFixed(2));
                $('#payment_othercharges').val(tax_amount_total.toFixed(2));
                $('#po_amount').val(po_total.toFixed(2));

            }
        }
    } else {
        $('#' + obj.id).removeClass('text_boxalert');
    }
}
function amount_recalculate(obj, item_id, item_code, bypass = 0) {
    var free_item = $('#free_item' + item_id).val();
    var from_disc = $('#from_discounted').is(":checked");
    if (free_item == 1) {
        $('#pur_qtyid' + item_id).val(0);
        $('#pur_rateid' + item_id).val(0);
    }

    var item_code = $('#item_codeid' + item_id).val();
    var other_charge = 0;
    var base_rate = 0;
    var num = obj.value;
    var disc_amount = 0;
    var tax_amount = 0;
    var disc_amount_total = 0;
    var tax_amount_total = 0;
    var tax_amount_total_perc = 0;
    var tax_amount_free_total_perc = 0;
    var each_total = 0;
    var gross_total = 0;
    var others_total = 0;
    var grand_total_disc = 0;
    var grand_total_tax = 0;
    var grand_total_tax_on = 0;
    var grand_total_tax_free = 0;
    var on_tax_amount = 0;
    var on_tax_amount_total = 0;
    var gross_free = 0
    var tax_amount_free = 0;
    var tax_amount_free_total = 0;
    var item_gross_calc = 0;
    var tax_perc_total = 0;
    var disc_amount_perc = 0;
    var disc_amount_each = 0;
    var from_disc = $('#from_discounted').is(":checked");



    var is_exclusive = $('#exclusive_checkbox' + item_id).is(":checked");
    var division_factor = 1;
    if (num || bypass == 1) {
        if (isNaN(num) && bypass == 0) {
            $('#' + obj.id).addClass('text_boxalert');
        } else {
            $('#' + obj.id).removeClass('text_boxalert');
            var pur_qty = parseFloat($('#pur_qtyid' + item_id).val());
            if (pur_qty == '' || isNaN(pur_qty))
                pur_qty = 0;

            var par_rate = parseFloat($('#pur_rateid' + item_id).val());
            if (par_rate == '' || isNaN(par_rate))
                par_rate = 0;
            var mrp = parseFloat($('#mrp_qtyid' + item_id).val());
            if (mrp == '' || isNaN(mrp))
                mrp = 0;
            var vat_on = $('#vat_onselect' + item_id).val();
            var item_gross = parseFloat(pur_qty) * parseFloat(par_rate);
            var item_gross_mrp = parseFloat(pur_qty) * parseFloat(mrp);
            var free_vat_on = $('#free_vat_onselect' + item_id).val();
            var free_qty = $('#free_qtyid' + item_id).val();
            if (free_qty == '' || isNaN(free_qty))
                free_qty = 0;
            var free_item = $('#free_item' + item_id).val();
            if (free_vat_on == 2)
                gross_free = parseFloat(free_qty) * parseFloat(par_rate);
            if (free_vat_on == 1)
                gross_free = parseFloat(free_qty) * parseFloat(mrp);
            if (vat_on == 1)
                item_gross_calc = parseFloat(pur_qty) * parseFloat(mrp);
            else
                item_gross_calc = parseFloat(pur_qty) * parseFloat(par_rate);

            var tax_total_for_discount = 0;
            if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                $.each(product_each_charge['tax'][item_id][item_code], function (i, val) {
                    if (i != 'percentage')
                        tax_total_for_discount += parseFloat(val);
                    else {
                        $.each(product_each_charge['tax'][item_id][item_code]['percentage'], function (i, val) {
                            tax_perc_total += val;
                        });
                    }
                });
            }


            if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                var disc_amount_except_bill = 0;
                if (from_disc == true) {
                    $.each(product_each_charge['discount'][item_id][item_code], function (i, val) {
                        if (i != 'percentage') {
                            var disc_id_array = i.split('charge_percentage_');
                            var disc_id = disc_id_array[1];
                            if (disc_id != billDiscount)
                                disc_amount_except_bill += parseFloat(val);

                        }

                    });
                }

                $.each(product_each_charge['discount'][item_id][item_code]['percentage'], function (i, val) {
                    var charge_id = i;
                    var percentage = parseFloat(val);
                    if (is_exclusive == 1) {
                        if (i == billDiscount && from_disc == true) {
                            disc_amount = (item_gross - disc_amount_except_bill) * (percentage / 100);

                        } else {
                            disc_amount = item_gross * (percentage / 100);

                        }
                        disc_amount = disc_amount.toFixed(2);
                    } else {

                        var fraction = 1;
                        if (tax_perc_total > 0)
                            fraction = ((100 + parseFloat(tax_perc_total)) / 100);
                        var total_rate_for_discount = item_gross / (fraction);

                        if (percentage == 100)
                            division_factor = 100;
                        else
                            division_factor = 100 - percentage;

                        var amount_after_discount_deduct = (total_rate_for_discount / (division_factor)) * 100;
                        disc_amount = amount_after_discount_deduct * (percentage / 100);
                        disc_amount = disc_amount.toFixed(2);
                    }
                    product_each_charge['discount'][item_id][item_code]['charge_percentage_' + charge_id] = disc_amount;
                    product_each_charge['discount'][item_id][item_code]['percentage'][charge_id] = percentage;
                    if (!item_charge_array.hasOwnProperty(item_id)) {
                        item_charge_array[item_id] = {};
                        item_charge_array[item_id][item_code] = {};
                    }
                    item_charge_array[item_id][item_code][charge_id] = percentage + ':' + disc_amount;

                });
            }

            if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                $.each(product_each_charge['discount'][item_id][item_code], function (i, val) {
                    if (i != 'percentage')
                        disc_amount_total += parseFloat(val);
                    else
                        $.each(product_each_charge['discount'][item_id][item_code]['percentage'], function (i, val) {
                            disc_amount_perc += val;
                        });
                });
            }

            if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                $.each(product_each_charge['tax'][item_id][item_code]['percentage'], function (i, val) {
                    var charge_id = i;
                    var percentage = parseFloat(val);

                    if (vat_on == 2) {
                        if (is_exclusive == 1)
                            tax_amount = (item_gross - disc_amount_total) * (percentage / 100);
                        else {

                            var fract = ((100 + parseFloat(percentage)) / 100);
                            var rate_without_tax = (item_gross) / fract;

                            tax_amount = rate_without_tax * (percentage / 100);


                        }

                    } else if (vat_on == 1) {
                        if (is_exclusive == 1)
                            tax_amount = (item_gross_mrp) * (percentage / 100);
                        else {


                            var fract = ((100 + parseFloat(percentage)) / 100);
                            var rate_without_tax = (item_gross_mrp) / fract;
                            if (disc_amount_perc > 0) {
                                disc_amount_each = rate_without_tax * (disc_amount_perc / 100);
                            }
                            var rate_without_tax_minus_disc = rate_without_tax - disc_amount_each;
                            tax_amount = rate_without_tax_minus_disc * (percentage / 100);
                        }

                    }
                    tax_amount = tax_amount.toFixed(2);
                    product_each_charge['tax'][item_id][item_code]['charge_percentage_' + charge_id] = tax_amount;
                    product_each_charge['tax'][item_id][item_code]['percentage'][charge_id] = percentage;


                    tax_amount_total += parseFloat(tax_amount);
                    tax_amount_total_perc += parseFloat(percentage);
                    if (!item_charge_array.hasOwnProperty(item_id)) {
                        item_charge_array[item_id] = {};
                        item_charge_array[item_id][item_code] = {};
                    }


                    item_charge_array[item_id][item_code][charge_id] = percentage + ':' + tax_amount;

                });
            }

            if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
                $.each(product_each_charge['on_tax_amount'][item_id][item_code]['percentage'], function (i, val) {

                    var charge_id = i;
                    var percentage = parseFloat(val);
                    if (is_exclusive == 1)
                        on_tax_amount = (parseFloat(tax_amount_total)) * (percentage / 100);
                    else {
                        var fract = ((100 + parseFloat(percentage)) / 100);
                        on_tax_amount = (tax_amount_total) / fract;
                        on_tax_amount = (tax_amount_total) - on_tax_amount;

                    }
                    on_tax_amount = on_tax_amount.toFixed(2);
                    product_each_charge['on_tax_amount'][item_id][item_code]['charge_percentage_' + charge_id] = on_tax_amount;
                    product_each_charge['on_tax_amount'][item_id][item_code]['percentage'][charge_id] = parseFloat(percentage);
                    on_tax_amount_total += parseFloat(on_tax_amount);
                    if (!item_charge_array.hasOwnProperty(item_id)) {
                        item_charge_array[item_id] = {};
                        item_charge_array[item_id][item_code] = {};
                    }
                    item_charge_array[item_id][item_code][charge_id] = percentage + ':' + on_tax_amount;

                });
            }



            if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
                $.each(product_each_charge['tax_free'][item_id][item_code]['percentage'], function (i, val) {

                    var charge_id = i;
                    var percentage = parseFloat(val);
                    if (is_exclusive == 1)
                        tax_amount_free = (parseFloat(gross_free)) * (percentage / 100);
                    else {
                        var fract = ((100 + parseFloat(percentage)) / 100);
                        tax_amount_free = (gross_free) / fract;
                        tax_amount_free = (gross_free) - tax_amount_free;
                    }
                    tax_amount_free = tax_amount_free.toFixed(2);
                    product_each_charge['tax_free'][item_id][item_code]['charge_percentage_' + charge_id] = tax_amount_free;
                    tax_amount_free_total += parseFloat(tax_amount_free);
                    product_each_charge['tax_free'][item_id][item_code]['percentage'][charge_id] = percentage;
                    tax_amount_free_total_perc += parseFloat(percentage);
                    if (!item_charge_array.hasOwnProperty(item_id)) {
                        item_charge_array[item_id] = {};
                        item_charge_array[item_id][item_code] = {};
                    }
                    item_charge_array[item_id][item_code][charge_id] = percentage + ':' + tax_amount_free;

                });
            }
            if (product_each_charge['othch'].hasOwnProperty(item_id)) {
                if (!item_charge_array.hasOwnProperty(item_id)) {
                    item_charge_array[item_id] = {};
                    item_charge_array[item_id][item_code] = {};
                }

                if (typeof (product_each_charge['othch'][item_id][item_code]) !== "undefined") {
                    $.each(product_each_charge['othch'][item_id][item_code]['percentage'], function (i, val) {
                        other_charge += parseFloat(val);
                        item_charge_array[item_id][item_code][i] = val + ':' + val;
                    });
                }
            }

            if (is_exclusive == 1)
                base_rate = item_gross
            else
                base_rate = item_gross - tax_amount;

            if (free_item == 1) {
                item_gross = 0;
                base_rate = 0;
            }
            if (!product_each_charge['total'].hasOwnProperty(item_id)) {
                product_each_charge['total'][item_id] = {};
                product_each_charge['total'][item_id][item_code] = {};
            }

            product_each_charge['total'][item_id][item_code].gross = parseFloat(item_gross);
            product_each_charge['total'][item_id][item_code].disc_amount = parseFloat(disc_amount_total);
            product_each_charge['total'][item_id][item_code].tax_amount = parseFloat(tax_amount_total);
            product_each_charge['total'][item_id][item_code].on_tax_amount = parseFloat(on_tax_amount_total);
            product_each_charge['total'][item_id][item_code].tax_amount_free = parseFloat(tax_amount_free_total);
            product_each_charge['total'][item_id][item_code].base_rate = base_rate;
            product_each_charge['total'][item_id][item_code].other_charges = other_charge;

            product_each_charge['total'][item_id][item_code].tot_disc_perc = parseFloat(disc_amount_perc);
            product_each_charge['total'][item_id][item_code].tot_tax_perc = parseFloat(tax_amount_total_perc);
            product_each_charge['total'][item_id][item_code].tot_free_tax_perc = parseFloat(tax_amount_free_total_perc);

            var grand_total_oth_charges = 0;


            $.each(product_each_charge['total'], function (i, val) {
                $.each(product_each_charge['total'][i], function (key, value) {

                    $.each(product_each_charge['total'][i][key], function (keyNew, valueNew) {

                        if (keyNew == 'base_rate') {
                            gross_total += parseFloat(valueNew);
                        } else if (keyNew == 'disc_amount')
                            grand_total_disc += parseFloat(valueNew);
                        else if (keyNew == 'tax_amount') {
                            grand_total_tax += parseFloat(valueNew);
                        } else if (keyNew == 'on_tax_amount')
                            grand_total_tax_on += parseFloat(valueNew);
                        else if (keyNew == 'tax_amount_free') {
                            grand_total_tax_free += parseFloat(valueNew);
                        } else if (keyNew == 'other_charges')
                            grand_total_oth_charges += parseFloat(valueNew);
                    });

                });
            });


            each_total = parseFloat(base_rate) - parseFloat(disc_amount_total) + parseFloat(tax_amount_total) + parseFloat(on_tax_amount_total) + parseFloat(tax_amount_free_total) + parseFloat(other_charge);
            $('#tamountid' + item_id).val(each_total.toFixed(2));
            $('#item_value').val(gross_total.toFixed(2));
            var po_amnt = parseFloat(gross_total) - parseFloat(grand_total_disc) + parseFloat(grand_total_tax) + parseFloat(grand_total_tax_on) + parseFloat(grand_total_tax_free) + parseFloat(grand_total_oth_charges);
            others_total = po_amnt - gross_total;
            var po_approve_amt = $('#po_approve_amt_id').val();
            var is_po_status = $('#is_po_status').val();
            var parent_po_status = $('#parent_po_status').val();
            if (parseFloat(po_approve_amt) < parseFloat(po_amnt)) {
                $('#po_approve_btn_id').hide();
                $('#po_amend_btn_id').hide();
                $('#aprove_amend_btn_id').hide();
            } else if (po_approve_amt == 'null') {
                if (is_po_status == '6') {
                    $('#po_approve_btn_id').show();
                } else if (is_po_status == '4' && parent_po_status == '1') {
                    $('#po_amend_btn_id').show();
                    $('#aprove_amend_btn_id').show();
                }
            } else {
                if (is_po_status == '6') {
                    $('#po_approve_btn_id').show();
                } else if (is_po_status == '3') {
                    $('#po_amend_btn_id').show();
                } else if (is_po_status == '4' && parent_po_status == '1') {
                    $('#po_amend_btn_id').show();
                    $('#aprove_amend_btn_id').show();
                }
            }
            $('#payment_other').val(others_total.toFixed(2));
            $('#po_amount').val(po_amnt.toFixed(2));
        }
    } else {
        $('#' + obj.id).addClass('text_boxalert');
    }

}
function checkPurMrpvalidation(obj, item_id) {
    var pur_rate = parseFloat($('#pur_rateid' + item_id).val());
    var mrp = parseFloat($('#mrp_qtyid' + item_id).val());
    if (pur_rate > mrp) {
        $('#pur_rateid' + item_id).addClass('text_boxalert');
        $('#mrp_qtyid' + item_id).addClass('text_boxalert');
        toastr.warning('Purchase Rate Greater than MRP');
    } else if (pur_rate < mrp) {
        $('#pur_rateid' + item_id).removeClass('text_boxalert');
        $('#mrp_qtyid' + item_id).removeClass('text_boxalert');
    }
}
function calCuSellingPrice(obj, item_id) {
    var pur_qty = $('#pur_qtyid' + item_id).val();
    var mrp = $('#mrp_qtyid' + item_id).val();
    var pack_size = $('#pack_size' + item_id).val();
    if ($('#selling_price_check' + item_id).prop("checked") == false) {
        if ((pur_qty * mrp) > 0) {
            var selling_price = (pur_qty * mrp) / (pur_qty * pack_size);
        } else {
            selling_price = 0;
        }
        $('#selling_price_qty' + item_id).val(selling_price.toFixed(2));
        $('#unit_mrp_hiden' + item_id).val(selling_price.toFixed(2));

    } else {
        if ((pur_qty * mrp) > 0) {
            var selling_price = (pur_qty * mrp) / (pur_qty * pack_size);
        } else {
            selling_price = 0;
        }
        $('#selling_price_qty' + item_id).attr("disabled", false);
        $('#unit_mrp_hiden' + item_id).val(selling_price.toFixed(2));
    }
}
function enableDisableText(item_id) {
    if ($('#selling_price_check' + item_id).prop("checked") == false) {
        $('#selling_price_qty' + item_id).attr("disabled", true);
        var pur_qty = $('#pur_qtyid' + item_id).val();
        var mrp = $('#mrp_qtyid' + item_id).val();
        var pack_size = $('#pack_size' + item_id).val();
        var total_mrp = parseFloat(pur_qty) * parseFloat(mrp);
        if (total_mrp > 0) {
            selling_price = total_mrp / (pack_size * parseFloat(pur_qty));
        } else {
            selling_price = total_mrp;

        }
        $('#selling_price_qty' + item_id).val(selling_price.toFixed(2))
    } else {
        $('#selling_price_qty' + item_id).attr("disabled", false);
    }
}
function checkSellingPriceMrp(item_id) {
    var mrp = parseFloat($('#mrp_qtyid' + item_id).val());
    var selling_price = parseFloat($('#selling_price_qty' + item_id).val());
    var pack_size = parseFloat($('#pack_size' + item_id).val());
    unit_mrp = mrp / pack_size;
    if (unit_mrp < selling_price) {
        toastr.error("Selling Price Grater than Unit MRP !!");
        $('#selling_price_qty' + item_id).focus();
        $('#selling_price_qty' + item_id).addClass('text_boxalert');
        return false;
    }

    else {
        $('#selling_price_qty' + item_id).removeClass('text_boxalert');
        return true;
    }
}
add_item = 0;
function fillData(detail_id, post_type, item_code, ItemName, pur_qty = 0, pur_unit, pur_rate, free_qty, mrp, vat_on, free_vat_on, t_amount, free_item, remarks, urgent, item_desc_short, pur_unit_list, is_exclusive, grn_details = '', diff = 0, po_no = '', type = '', color_code = '', pur_unit_defualt, approved_ammended_balance_zero, pack_size = 1, selling_price = 0, selling_price_flag) {

    pur_unit_list = JSON.parse(pur_unit_list);
    // var table = document.getElementById("added_new_list_table_product");
    var config_data = $('#config_details_hidden').val();
    var unit_master = $('#hidden_unit_master').val();
    var obj = JSON.parse(config_data);
    var unit_obj = JSON.parse(unit_master);
    var unit_serachsting = '';
    var free_item_serachsting = '';
    var excl_checked = '';
    var sel = '';
    var vat_onsel = '';
    var free_vat_onsel = '';
    var free_itemsel = '';
    var unit_mrp = 0;
    row_count = $('#row_count').val();

    var tax_on_mrp = '';
    var tax_exclusive = '';

    $.each(pur_unit_list, function (key, val) {
        if (rawurldecode(pur_unit) == val.id) {
            sel = 'selected';
        }
        else if (rawurldecode(pur_unit_defualt) == val.id) {
            if (pur_unit.length === 0) {
                sel = 'selected';
            } else {
                sel = '';
            }
        }
        else {
            sel = '';
        }
        unit_serachsting += "<option " + sel + " value='" + val.id + "'>" + val.name + "</option>";
    });




    if (grn_details != '') {
        grn_dls = JSON.parse(grn_details);


        if (typeof (grn_dls[0]) !== "undefined")
            pur_rate = grn_dls[0].grn_rate;

        if (typeof (grn_dls[0]) !== "undefined")
            mrp = grn_dls[0].grn_mrp;

        if (typeof (grn_dls[0]) !== "undefined" && pur_qty == 0 && type != 'quick_add')
            pur_qty = grn_dls[0].grn_qty;

        if (typeof (grn_dls[0]) !== "undefined")
            free_qty = grn_dls[0].grn_free_qty;

        if (typeof (grn_dls[0]) !== "undefined") {
            tax_exclusive = grn_dls[0].tax_exclusive;
            if (tax_exclusive == '2')
                is_exclusive = '1';
        }
        if (typeof (grn_dls[0]) !== "undefined") {
            tax_on_mrp = grn_dls[0].tax_on_mrp;
            if (tax_on_mrp == '1') {
                vat_on = '1';
                free_vat_on = '1';

            } else {
                vat_on = '2';
                free_vat_on = '2';

            }

        }
    }

    var total_mrp = rawurldecode(parseFloat(pur_qty)) * rawurldecode(parseFloat(mrp));
    if (detail_id == 0 || detail_id == '' || detail_id == 'null') {
        if (total_mrp > 0) {
            selling_price = parseFloat(total_mrp / (pack_size * rawurldecode(parseFloat(pur_qty))));
        }
        selling_price = selling_price.toFixed(2);
    } else {
        selling_price = parseFloat(selling_price).toFixed(2)
    }
    if (total_mrp > 0) {
        unit_mrp = parseFloat(total_mrp / (pack_size * rawurldecode(parseFloat(pur_qty))));
        unit_mrp = unit_mrp.toFixed(2);
    }
    if (rawurldecode(is_exclusive) == '1') {
        excl_checked = 'checked';
    }
    $.each(obj, function (key, val) {
        if (val.head_name == 'vat_on') {
            if (rawurldecode(vat_on) == val.value) {
                vat_onsel = 'selected';
            } else {
                vat_onsel = '';
            }
            if (rawurldecode(free_vat_on) == val.value) {
                free_vat_onsel = 'selected';
            } else {
                free_vat_onsel = '';
            }
            vaton_serachsting += "<option " + vat_onsel + " value='" + val.value + "'>" + val.detail_name + "</option>";
            free_vaton_serachsting += "<option " + free_vat_onsel + " value='" + val.value + "'>" + val.detail_name + "</option>";
        } else if (val.head_name == 'yesNoState') {
            if (rawurldecode(free_item) == val.value) {
                free_itemsel = 'selected';
            } else {
                free_itemsel = '';
            }
            free_item_serachsting += "<option " + free_itemsel + " value='" + val.value + "'>" + val.detail_name + "</option>";

        }
    });

    free_item_serachsting += "<option  value='0'>No</option>";
    free_item_serachsting += "<option  value='1'>Yes</option>";

    lightbrown ='';
    if (approved_ammended_balance_zero == '1') {
        lightbrown = "lightbrown";
    }

    var readonlytext = '';
    if (purchase_remove_free_qty == 1) {
        var readonlytext = ' readonly ';
    }
    purchase_order_body = "<tr onclick='last_clicked1(" + item_id + ")' class='iterate_row bg-warning po_data' id='itemadd_row"+item_id+"'>";
    purchase_order_body += "<td><label class='row_count_cls' >" + row_count + "</label></td>";
    purchase_order_body += "<td class='"+lightbrown+"' style='text-align: left'><label data-toggle='tooltip' title='" + rawurldecode(item_code) + "'>" + rawurldecode(ItemName) + "</label><input type='hidden' name='item_code[]' id='item_codeid" + item_id + "' value='" + rawurldecode(item_code) + "'></td>";
    purchase_order_body += "<td><input  min='0' onkeypress='restrictMinusSign(event);'  onkeyup='amount_recalculate(this," + item_id + "," + 1 + ");calCuSellingPrice(this," + item_id + ");'  name='pur_qty[]' id='pur_qtyid" + rawurldecode(item_id) + "' value='" + rawurldecode(pur_qty) + "' class='form-control number_class'><input type='hidden' name='edit_status[]' id='edit_status" + item_id + "' value='" + post_type + "'></td>";

    purchase_order_body += "<td><select name='pur_unit[]' onchange=getProductPackSize('" + item_id + "') id='pur_unitselect" + item_id + "' class='form-control input_padding_sm'>" + unit_serachsting + "</select></td>";

    purchase_order_body += "<td><input placeholder='Pur Rate' min='0'   onkeypress='restrictMinusSign(event);' onkeyup='amount_recalculate(this," + item_id + "," + 1 + ");' name='pur_rate[]' id='pur_rateid" + item_id + "' value='" + rawurldecode(pur_rate) + "' class='form-control number_class'><input type='hidden' id='item_detail_id" + item_id + "' name='po_detail_id[]' value='" + detail_id + "'></td>";
    purchase_order_body += "<td><input " + readonlytext + " placeholder='Free Qty' min='0'  onkeypress='restrictMinusSign(event);' onkeyup='amount_recalculate(this," + item_id + "," + 1 + ")' name='free_qty[]' id='free_qtyid" + item_id + "' value='" + rawurldecode(free_qty) + "' class='form-control free_qty number_class " + rawurldecode(item_code) + "'></td>";
    purchase_order_body += "<td><input placeholder='MRP' min='0'   onkeypress='restrictMinusSign(event);' onkeyup='amount_recalculate(this," + item_id + "," + 1 + ");calCuSellingPrice(this," + item_id + ");' name='mrp_qty[]' id='mrp_qtyid" + item_id + "' value='" + rawurldecode(mrp) + "' class='form-control number_class'><input type='hidden' id='pack_size" + item_id + "' name='pack_size[]'value='" + pack_size + "' ></td>";
    purchase_order_body += "<td style='text-align: center' class='hide'><div class='checkbox checkbox-success'> <input " + excl_checked + " id='exclusive_checkbox" + item_id + "' name=item_exclusive[] class='styled' type='checkbox' checked='checked' disabled='disabled' onclick='amount_recalculate(this," + item_id + "," + rawurldecode(item_code) + ",1)' ><label for='exclusive_checkbox" + item_id + "'></label><input name='vat_on' type='hidden'  id='vat_onselect" + item_id + "' class='form-control input_padding_sm' value='2'><input name='free_vat_on' type='hidden'   id='free_vat_onselect" + item_id + "' class='form-control input_padding_sm' value='2'></div></td>";

    purchase_order_body += "<td><button class='btn btn-success' name='getProductCharges_btn[]' type='button' onclick='getProductCharges(" + item_id + ',' + detail_id + ")' style='padding:3px 3px'><i id='chargesbtnspin" + item_id + "' class='fa fa-calculator'></i></button></td>";
    purchase_order_body += "<td style='text-align: center'><input placeholder=' TAmount' type='text' onkeyup='validate_number(this,1)' readonly='' name='tamount_qty[]' id='tamountid" + item_id + "' value='" + rawurldecode(t_amount) + "' class='form-control number_class '></td>";
    purchase_order_body += "<td><select name='free_item[]' onchange='free_item_change(" + item_id + ",this)' id='free_item" + item_id + "' class='form-control input_padding_sm'>" + free_item_serachsting + "</select></td>";
    purchase_order_body += "<td><input type='text' placeholder='Remarks' name='remarks_po[]' id='remarks_poid" + item_id + "' value='" + rawurldecode(remarks) + "'  class='form-control'></td>";
    purchase_order_body += "<td style='text-align: center'><input type='hidden' data-item-code=" + rawurldecode(item_code) + " class='inserted_row_id_each' name='inserted_row_id[]' id='row_id" + rawurldecode(item_code) + "' value='" + item_id + "'>  <button id='getbatchdetails1' type='button' class='btn btn-success' onclick='getGlobalStock(\"" + item_id + "\")' style='padding: 3px 3px'><i id='globebtnspin' class='fa fa-globe'></i></button> <button class='btn btn-danger' type='button' onclick='delete_itemlist(\"" + item_id + "\")' style='padding:3px 3px;'><i class='fa fa-trash'></i></button></td>";
    purchase_order_body += "</tr>";
    $('#added_new_list_table_product').append(purchase_order_body);

    if (grn_details != '')
        $.each(grn_dls, function (key, val) {
            var d = new Object();
            d.value = val.perc;
            d.type = 'popup';

            calculationExtraCharges(d, 'percentage', item_id, rawurldecode(item_code), val.calc_code, val.charge_code, val.charge_head_id, val.is_tax, 1);
        });

    if (selling_price_flag == 1) {
        $('#selling_price_check' + item_id).attr('checked', true);
        $('#selling_price_qty' + item_id).attr('disabled', false);
    } else {
        $('#selling_price_check' + item_id).attr('checked', false);
        $('#selling_price_qty' + item_id).attr('disabled', true);
    }
    add_item++;
    item_id++;
    $('#row_count').val(++row_count);
    if ($("#save_itemlist #added_new_list_table_product tr").length > 0)
        $('#deprt_list').attr("disabled", true);
    return item_id;
}
function restrictMinusSign(e) {
    var inputKeyCode = e.keyCode ? e.keyCode : e.which;
    if (inputKeyCode != null) {
        if (inputKeyCode == 45)
            e.preventDefault();
        if (inputKeyCode == 43)
            e.preventDefault();
    }
}
function getProductPackSize(item_id) {
    var pack_id = $('#pur_unitselect' + item_id).val();
    var item_code = $('#item_codeid' + item_id).val();
    $.ajax({
        type: "POST",
        url: '',
        data: { pack_id: pack_id, item_code: item_code, change_pack_size: 1 },
        beforeSend: function () {

        },
        success: function (pack_size) {
            $('#pack_size' + item_id).val(pack_size);
            calCuSellingPrice(1, item_id);
        },
        complete: function () {
        }
    });
}

function getProductCharges(item_id, detail_id) {
    var item_code = $('#item_codeid' + item_id).val();
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token, item_id_charges: item_id, item_code_charges: item_code, detail_id: detail_id };
    $.ajax({
        type: "POST",
        url: '',
        data: param, beforeSend: function () {
            $('#chargesbtnspin' + item_id).removeClass('fa fa-calculator');
            $('#chargesbtnspin' + item_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#purchase_charges_div').html(data);
            $('#purchase_charge_modal').modal('show');

            $(function () {


            });
        },
        complete: function () {
            $('#chargesbtnspin' + item_id).removeClass('fa fa-spinner fa-spin');
            $('#chargesbtnspin' + item_id).addClass('fa fa-calculator');
        }
    });
}

function purchaseOrderList() {
    if ($('#po_added').val() != '') {
        $('#po_added').val('');
    }
    if (item_list_final.length) {
        for (var key in item_list_final) {
            item_manage(item_list_final[key], null, 'po_req');

        }

    }
    purchaseOrderNewList();
    item_list_final = [];
}
function purchaseOrderNewList() {
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token, terms_condition_load: 1 };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#generate_polistspin').removeClass('fa fa-forward');
            $('#generate_polistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                if (data.json_return != '') {
                    var jsonArray = JSON.parse(data.json_return);
                    $.each(jsonArray, function (index, jsonObject) {
                        var terms_id = parseInt(jsonObject.id);
                        var position = purchase_order_condition_array.indexOf(terms_id);
                        if (position) {
                            purchase_order_condition_array.push(terms_id);
                        }
                    });
                }
                if (data.accessPerm.add != 1)
                    $("#genarate_po #po_post_btn_id").hide();
            }
            po_add_status = 1;
            $('.req_listcheck').attr('onclick', 'return false');
            $('#genarate_poli').show();

            $('.nav-tabs a[href="#genarate_po"]').tab('show');
            $("#deprt_list").val($("#search_from_department").val());
            $("#vendor_list_po2").attr('title', $('#vendor_list_po').attr('title'));
            if ($('#vendor_list_po_hidden').val() != '' && $('#vendor_list_po_hidden').val() != 'undefined' && $('#vendor_list_po_hidden').val() != null) {
                $("#vendor_list_po2").val($('#vendor_list_po').val());
            }
            $("#vendor_list_po_hidden2").val($('#vendor_list_po_hidden').val());


        },
        complete: function () {
            $('#generate_polistspin').removeClass('fa fa-spinner fa-spin');
            $('#generate_polistspin').addClass('fa fa-forward');
        }
    });
}



function AddNewProduct(offset, post_type, tp = 0, item_code_sh) {

    var from_department = $('#deprt_list').val();
    var vendor_code = $('#vendor_list_po_hidden2').val();
    var catageory = 'All';
    var sub_catageory = 'All';
    var consume_type = [];
    var item_desc = 'All';
    var reorder_level = false;
    var search_all_item = false;
    var file_token = $('#hidden_filetoken').val();
    if (post_type == '2' || post_type == '3') {

        catageory = [];
        sub_catageory = [];
        $('#get_subcatgeoryselect option:selected').each(function (ind, el) {
            if ($(el).val() != '') {
                sub_catageory.push($(el).val());
            }
        });
        $('#from_category option:selected').each(function (ind, el) {
            if ($(el).val() != '') {
                catageory.push($(el).val());
            }
        });

        if (sub_catageory.length == 0)
            sub_catageory = 'All';
        if (catageory.length == 0)
            catageory = 'All';


        item_desc = $('#item_newdesc_listsearch').val();
        reorder_level = $('#reorderlevelcheck').is(":checked");
        search_all_item = $('#search_all_item').is(":checked");
    }
    var item_array_send = JSON.stringify(item_array_new);
    var param = {
        _token: file_token,
        from_department: from_department,
        vendor_code: vendor_code,
        item_desc_search: item_desc,
        post_type: post_type,
        sub_catageory_search: sub_catageory,
        catageory_search: catageory,
        offsetNewitem: offset,
        item_array_list: item_array_send,
        search_all_item: search_all_item,
        reorder_level: reorder_level,
    };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            if (post_type == '1') {
                $('#addProductSpin').removeClass('fa fa-plus');
                $('#addProductSpin').addClass('fa fa-spinner fa-spin');
            } else {
                $('#search_itemdataspin').removeClass('fa fa-search');
                $('#search_itemdataspin').addClass('fa fa-spinner fa-spin');
            }
            if (post_type == '2') {
                $('#search_data_getdiv').html('');
            }

        },
        success: function (result) {
            if (post_type == '1') {
                $('#itemts_getdiv').html(result);
                $("#itemcode_searchModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else if (post_type == '2') {
                $('#search_data_getdiv').html(result);
                $('#item_desc_search_new').val('');
                $('#item_newdesc_listsearch').val('');
            } else if (post_type == '3') {
                $("#tbody_itemdata").append(result);

            }
            if (tp == 1) {
                $('#itemdetailselect' + item_code_sh).trigger('click');
            }
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $("#all_chk_item_list_cls").prop("checked", false);
        },
        complete: function () {
            if (offset == '0' && post_type == '1') {
                $('#addProductSpin').removeClass('fa fa-spinner fa-spin');
                $('#addProductSpin').addClass('fa fa-plus');
            } else {
                $('#search_itemdataspin').removeClass('fa fa-spinner fa-spin');
                $('#search_itemdataspin').addClass('fa fa-search');
            }

            $('.multi_cat').multiselect({
                enableClickableOptGroups: true,
                enableCollapsibleOptGroups: true,
                enableFiltering: true,
                includeSelectAllOption: true,
                minWidth: 300,
                buttonWidth: '250px',
                maxHeight: 400,
                enableCaseInsensitiveFiltering: true,
            });
        }
    });
}

function getannexuredata() {
    var file_token = $('#hidden_filetoken').val();
    var annexure_id = 1;
    var param = { _token: file_token, annexure_id: annexure_id };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#annexure_pospin').removeClass('fa fa-anchor');
            $('#annexure_pospin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#annexture_poref_div').html(data);
            $("#annexture_poref_model").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#annexure_pospin').removeClass('fa fa-spinner fa-spin');
            $('#annexure_pospin').addClass('fa fa-anchor');
            $('#annexture_ref1_text').val(annexture_ref1_text);
            $('#annexture_ref2_text').val(annexture_ref2_text);
            $('#annexture_data_text').val(annexture_data_text);
        }
    });
}
function set_annexuretext() {
    annexture_ref1_text = $('#annexture_ref1_text').val();
    annexture_ref2_text = $('#annexture_ref2_text').val();
    annexture_data_text = $('#annexture_data_text').val();
    $('#annexture_poref_model').modal('toggle');
}
function termsAndConditions() {
    var file_token = $('#hidden_filetoken').val();
    var terms_condition = JSON.stringify(purchase_order_condition_array);
    var param = { _token: file_token, terms_condition: terms_condition, terms_cond: 1, po_id: $('#add_edit_status').val(), location_code: $("#deprt_list").val() };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#termsAndConditionspin').removeClass('fa fa-copyright');
            $('#termsAndConditionspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#terms_conndition_div').html(data);
            $("#terms_conndition_model").modal({
                backdrop: 'static',
                keyboard: false
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
        },
        complete: function () {
            $('#termsAndConditionspin').removeClass('fa fa-spinner fa-spin');
            $('#termsAndConditionspin').addClass('fa fa-copyright');
            $('#additionaltems_text').val(additionaltems_text);
        }
    });
}

function purchase_mail(mail_status) {
    var checkbox = $('#enable_mail:checked').length;
    var file_token = $('#hidden_filetoken').val();
    var po_mail_condent = "Get_po_mail_condent";
    var po_type_id = $('#add_edit_status').val();
    var vendor_code = $('#vendor_list_po_hidden2').val();
    $('.attachment_file_id_string').val('');
    $('.dropzone').html("<div class='dz-default dz-message'><span>Drop files here to upload</span></div>");
    $('.dz-message').show();


    if (mail_status == 1) {
        $('#enable_mail').attr('checked', false);
    }
    if (checkbox == 1 || mail_status == 1) {

        $.ajax({
            url: "",
            data: { po_mail_condent: po_mail_condent, po_type_id: po_type_id, vendor_code: vendor_code, _token: file_token },
            type: 'post',
            beforeSend: function () {
                $('#adddataspin').removeClass('fa fa-plus-square-o');
                $('#adddataspin').addClass('fa fa-spinner fa-spin');
            },
            success: function (result) {
                var obj = JSON.parse(result);
                var mail_body_data = '';
                if (obj.mail_body != '' && obj.mail_body != 'NULL' && obj.mail_body != null) {
                    mail_body_data = obj.mail_body;
                }
                $("#subject_id").val("Purchase Order");
                tinymce.init({
                    selector: '#mail_template_text',
                    toolbar: ' undo redo  | \n\
                                        bold italic underline | alignleft aligncenter alignright alignjustify | \n\
                                          | clearBtn ',
                    menubar: false,
                    forced_root_block: false,
                    setup: function (editor) {
                        editor.ui.registry.addButton('mybutton', {
                            icon: false,
                        });
                    },
                    init_instance_callback: function (editor) {
                        if (mail_body_data) {
                            editor.setContent(mail_body_data);
                        }
                    }
                });

                if (obj.ven_email != '' && obj.ven_email != 'NULL' && obj.ven_email != null) {
                    var arr_vendor_mail = [];
                    arr_vendor_mail = obj.ven_email.split(",", 2);
                    if (arr_vendor_mail.length == 1)
                        arr_vendor_mail = arr_vendor_mail[0].split(";", 2);
                    if (arr_vendor_mail.length == 1)
                        arr_vendor_mail = arr_vendor_mail[0].split(":", 2);
                    $.each(arr_vendor_mail, function (index, ven_email) {
                        $('.mailid_div:last-child').text(ven_email);
                        $(".mailid_div:last-child").append("<span class='mailid_close'>X</span>");
                    });
                    $(".mailid_close").click(function () {
                        $(this).parent().remove();
                        $('.mailid_div:last-child').text('');
                    });
                }
                if (obj.file_name) {
                    var url_path = $('#url_id').val();
                    var path = url_path + '/packages/uploads/Purchase/' + obj.file_name;
                }

                $("#pdf_download_popup").attr("href", path);
                popup();


                $("#mailBodyPopUpModel").modal({
                    backdrop: 'static',
                    keyboard: false
                  });
            },
            complite: function () {
                $('#adddataspin').removeClass('fa fa-spinner fa-spin');
                $('#adddataspin').addClass('fa fa-plus-square-o');
            }
        });


    } else {
        savePurchaseOrder(3);
    }
}
var to = [];
var cc = [];
function PO_mail_save() {
    var checkbox = $('#enable_mail:checked').length;
    var to_mail = $('#mailinput').val();
    var cc_mail = $('#ccinput').val();
    var flag = 1;
    var to_m = 1;
    var cc_m = 1;
    var cunt = 0;
    to = [];
    cc = [];
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    $(".mailid_div").each(function (i) {
        var str = $(this).text().trim();
        if (str != '') {
            var lastChar = str[str.length - 1];
            if (lastChar == 'X') {
                str = str.substring(0, str.length - 1);
            }
            if (str.trim() != '') {
                to[i] = str.trim();
                to[i] = to[i].toLowerCase();
                flag = re.test(to[i]);
                if (flag != 1) {
                    to_m = 0;
                }
            }
            cunt++;
        }
    });
    flag = re.test(to_mail);
    if (flag != 1 && to_mail != '') {
        to_m = 0;
    }
    if (to_mail != '') {
        if (cunt == 0) {
            to[cunt] = to_mail;
        } else {
            to[cunt + 1] = to_mail;
        }
    }
    to = to.filter(function (element) {
        return element !== undefined;
    });
    var uniqueNames = [];
    $.each(to, function (i, el) {
        if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
    });

    to = [];
    to = uniqueNames;
    cc_m = 1;
    cunt = 0;
    flag = re.test(cc_mail);
    cc = '';
    if (to_m == 1 && cc_m == 1) {
        if (checkbox == 1) {
        } else {
            po_pdf_mail();
        }
        $('#mailBodyPopUpModel').modal('hide');
    } else if (to_m == 0) {
        toastr.error("To Email Address Not Valid !!");
    } else if (cc_m == 0) {
        toastr.error("Cc Email address Not Valid!!");
    }
}

function department_change() {
    item_list_final = [];
    var from_department = $('#search_from_department').val();
    var insert_array_length = Object.keys(item_array_new).length;
    if (insert_array_length != 0) {

        if (confirm("To change store please reload the page ?")) {
            po_pagereload();


        } else {
            $('#search_from_department').val(from_locationid);
        }
    } else {
        serach_po();
        from_locationid = from_department;
    }

}

function getSellerAddress() {
    for (var j = 1; j <= $("#multi_addr").find("textarea").length; j++) {
        if (tinymce.get("multi_address_" + j)) {
            tinymce.remove('#multi_address_' + j);
        }

        tinymce.init({
            toolbar: 'undo redo',
            selector: '#multi_address_' + j,
            menubar: false,
            forced_root_block: false,
            setup: function (editor) {
                editor.ui.registry.addButton('mybutton', {
                    text: 'Template',
                    icon: false,
                });
            }
        });
    }
}

function add_addrs() {
    var txt_cnt = $("#multi_addr").find("textarea").length;
    txt_cnt = txt_cnt + 1;
    $("#multi_addr").append("<div class='col-md-6'><div class='col-md-2'><input type='radio' name='addr' data-loc_id='0' onclick='setDefault(this);' style='display: inline;width: 29%;'></div><div class='col-md-10'><div class='rm_close' onclick='remove_addr(this);'>X</div><textarea style='overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:98%; height: 100px' cols='50' id='multi_address_" + txt_cnt + "' name='multi_address_" + txt_cnt + "'> </textarea></div></div>");
    if (tinymce.get("multi_address_" + txt_cnt)) {
        tinymce.remove('#multi_address_' + txt_cnt);
    }

    tinymce.init({
        selector: '#multi_address_' + txt_cnt,
        toolbar: ' undo redo',

        menubar: false,
        forced_root_block: false,
        setup: function (editor) {
            editor.ui.registry.addButton('mybutton', {
                text: 'Template',
                icon: false,
            });
        }
    });
}

function setDefault(elm, id = '') {
    $("#multi_addr").find(".rm_label").remove();
    var po_id = $('#add_edit_status').val();

    if (id != '') {
        $.ajax({
            type: "get",
            data: { addr_po_id: po_id, location_buyer_id: id },
            url: '',
            beforeSend: function () {

            },
            success: function (data) {
                if (data) {
                    toastr.success("Set to default address !!");
                }
            },
            complete: function () {

            },
            error: function () {
                toastr.error("Error in Set to default address !!");
            }
        });

    } else {
        toastr.warning("Set to default address after click ok !!");
    }
    $(elm).parent().append("<label class='rm_label text-success'>Default</label>");

}

function remove_addr(el, id) {
    if ($(el).parent().parent().find("label").text() != "Default") {
        var po_id = $('#add_edit_status').val();
        if (id != '') {
            $.ajax({
                type: "get",
                data: { rmv_addr_po_id: po_id, rmv_location_buyer_id: id },
                url: '',
                beforeSend: function () {

                },
                success: function (data) {
                    console.log(data);
                    if (data == 1) {
                        toastr.success("Address delete successfully !!");
                        $(el).parent().parent().remove();
                    } else {
                        toastr.warning("You can't delete this address This address assign another po !!");
                    }
                },
                complete: function () {

                },
                error: function () {
                    toastr.error("Error in address deletion !!");
                }
            });

        } else {
            toastr.warning("Address delete successfully !!");
            $(el).parent().parent().remove();
        }

    } else {
        toastr.error("Default Address Can't Remove !!");
    }
}

function termsAndConditionsetbtn() {
    var tab_id = $("#termsAndConditions_tab .active").attr('id');
    if (tab_id == 'selleracceptanceli' || tab_id == 'footerli') {
        var content;
        var type;
        if (tab_id == 'selleracceptanceli') {
            content = tinymce.get('seller_acceptance').getContent();
            type = 1;
        }
        else if (tab_id == 'footerli') {
            content = tinymce.get('footer').getContent();
            type = 2;
        }

        if (confirm("Are you sure you want to Save?")) {
            var param = { save_seller_footer: 1, content: btoa(content), type: type };
            $.ajax({
                type: "GET",
                url: '',
                data: param,
                beforeSend: function () {
                },
                success: function (result) {
                    if (result == 1) {
                        toastr.success("Added Successfully");
                    }
                    else { toastr.error("Please Try Again"); }
                },
                complete: function () {
                }
            });
        }
    } else {
        if (tab_id == 'multiaddressli') {
            var addr_arr = []; var cnt_arr = [];
            for (var j = 1; j <= $("#multi_addr").find("textarea").length; j++) {
                cnt_arr = [];
                if ($("#multi_address_" + j).length != 0) {
                    if (tinymce.get("multi_address_" + j)) {

                        cnt_arr['content'] = tinymce.get("multi_address_" + j).getContent();
                        cnt_arr['location'] = $("#deprt_list").val();
                        if ($("#multi_address_" + j).parent().parent().find("label").text() == "Default") {
                            cnt_arr['is_default'] = "1";
                        } else {
                            cnt_arr['is_default'] = "0";
                        }

                        cnt_arr['loc_id'] = $("#multi_address_" + j).parent().parent().find("input").data().loc_id;
                        addr_arr.push(cnt_arr);
                    }
                }


            }


            if (confirm("Are you sure you want to Save?")) {
                addr_arr = JSONH.pack(addr_arr);
                var po_id = $('#add_edit_status').val();
                $.ajax({
                    type: "get",
                    url: '',
                    data: { add_address: addr_arr, po_id: po_id },
                    beforeSend: function () {
                    },
                    success: function (result) {
                        if (result == 1) {
                            toastr.success("Added Successfully");
                            $('#terms_conndition_model').modal('toggle');
                        }
                        else { toastr.error("Please Try Again"); }
                    },
                    complete: function () {
                    }
                });
            }

        } else {
            additionaltems_text = $('#additionaltems_text').val();
            $('#terms_conndition_model').modal('toggle');
        }
    }
}

item_manage_array = [];
item_list_final = [];
moving_po_req_list = [];
prq_item_manage_array = [];
purchase_requisition = {};

function item_list_manage_array(detail_id, obj) {
    var prq_details = [];
    var productStatus = $('#' + obj.id).is(":checked");
    var manage_position = item_list_final.indexOf(detail_id);
    var item_code = $('#item_code_req' + detail_id).text();
    var item_code = item_code.trim();
    var item_name = $('#item_desc_req' + detail_id).html().trim();
    var item_name_short = $('#item_desc_reqshort' + detail_id).val().trim();
    var pur_qty = $('#item_purchase_req' + detail_id).html().trim();
    var pur_unit = $('#reqitem_unit_id' + detail_id).val().trim();
    var item_urgent = $('#item_urgents' + detail_id).val().trim();
    var master_unit_id = $('#master_unit_id' + detail_id).val().trim();
    if ($('#vendor_list_po').val() != "") {
        if (productStatus) {

            prq_details['item_code'] = item_code;
            prq_details['item_name'] = item_name;
            prq_details['item_name_short'] = item_name_short;
            prq_details['pur_qty'] = pur_qty;
            prq_details['pur_unit'] = pur_unit;
            prq_details['item_urgent'] = item_urgent;
            prq_details['master_unit_id'] = master_unit_id;
            item_list_final.push(detail_id);
            prq_item_manage_array[detail_id] = prq_details;
            purchase_requisition[detail_id] = item_code;
        } else {
            if (~manage_position) {
                item_list_final.splice(manage_position, 1);
                delete purchase_requisition[detail_id];
            }
        }
    } else {
        $('#' + obj.id).prop("checked", false);
        toastr.error("Please Select Supplier !!");
    }
}


function item_manage(detail_id, obj, type = '') {
    var color_code;
    if (type == '') {
        var productStatus = $('#' + obj.id).is(":checked");
    } else {
        var productStatus = 1;
    }
    detail_id = parseInt(detail_id);
    var file_token = $('#hidden_filetoken').val();
    var manage_position = item_manage_array.indexOf(detail_id);
    if (productStatus) {
        if (manage_position) {
            item_manage_array.push(detail_id);
        }
    } else {
        if (~manage_position) {
            item_manage_array.splice(manage_position, 1);
        }
    }
    var item_code = prq_item_manage_array[detail_id]['item_code'];
    detail_id = Number(detail_id);
    var position = item_array.indexOf(detail_id);
    if (productStatus) {
        var item_name = prq_item_manage_array[detail_id]['item_name'];
        var item_name_short = prq_item_manage_array[detail_id]['item_name_short'];
        var pur_qty = prq_item_manage_array[detail_id]['pur_qty'];
        var pur_unit = prq_item_manage_array[detail_id]['pur_unit'];
        var item_urgent = prq_item_manage_array[detail_id]['item_urgent'];
        var pur_unit_multi = prq_item_manage_array[detail_id]['master_unit_id'];
        var product_code_id = 0;
        pur_qty = parseFloat(pur_qty);
        if (typeof (moving_po_req_list[item_code]) !== 'undefined') {
            var it_id = moving_po_req_list[item_code];
            if ($('#pur_qtyid' + it_id)) {
                var pur_getval = $('#pur_qtyid' + it_id).val();
                pur_getval = parseFloat(pur_getval);
                pur_getval += pur_qty;
                $('#pur_qtyid' + it_id).val(pur_getval);
                $('#pur_qtyid' + it_id).trigger('onkeyup');
            }
        } else {

            $.ajax({
                type: "POST",
                url: '',
                data: { item_code_purchase: item_code, pur_unit_multi: pur_unit_multi, _token: file_token },
                beforeSend: function () {

                },
                success: function (result) {
                    color_code = '';
                    var approved_ammended_balance_zero = 0;
                    var pack_size = 1;
                    var sect_vender = $('#vendor_list_po_hidden').val();
                    if (!$.isEmptyObject(result.pack_size) && $.type(result.pack_size[0]) != "undefined" && $.type(result.pack_size[0].pack_size) != "undefined") {
                        pack_size = result.pack_size[0].pack_size;
                    }
                    if (!$.isEmptyObject(result.intransit) && $.type(result.intransit[0]) != "undefined" && $.type(result.intransit[0].po_no) != "undefined") {
                        if (result.intransit[0].grn_status == '5' && (result.intransit[0].po_balance_qty > '0')) {
                            approved_ammended_balance_zero = 1;
                        }
                    } else if (!$.isEmptyObject(result.h1_item) && $.type(result.h1_item[0]) != "undefined" && $.type(result.h1_item[0].id) != "undefined") {

                    } else if (!$.isEmptyObject(result.prev_vendor) && $.type(result.prev_vendor[0]) != "undefined" && $.type(result.prev_vendor[0].vendor_code) != "undefined" && result.prev_vendor[0].vendor_code != sect_vender)

                        var pur_unit_defualt = 0;
                    if (result.is_default != 0) {
                        pur_unit_defualt = result.is_default;
                    }
                    if (result.unit_master_settings) {
                        var pur_unit = prq_item_manage_array[detail_id]['master_unit_id'];
                    }


                    product_code_id = fillData(0, 1, rawurlencode(item_code), rawurlencode(item_name), rawurlencode(pur_qty), rawurlencode(pur_unit), rawurlencode(0.00), rawurlencode(0.00), rawurlencode(0.00), rawurlencode(''), rawurlencode(''), rawurlencode(0.00), rawurlencode(''), rawurlencode(''), item_urgent, rawurlencode(item_name_short), result.unit_details, rawurlencode(''), result.grn_details, '', '', 'quick_add', color_code, rawurlencode(pur_unit_defualt), approved_ammended_balance_zero, pack_size, 0, 0);
                    product_code_id = product_code_id - 1;
                    item_array_new[product_code_id] = {};
                    item_array_new[product_code_id].itemdesc = rawurlencode(item_name);
                    moving_po_req_list[item_code] = product_code_id;

                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                    $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30,
                        suppressScrollX: true
                    });
                },
                complete: function () {

                }
            })
        }

    } else {

        var pur_qty = 0;
        var free_qty = 0;
        var item_id = 0;
        if ($('#pur_qtyid' + item_id).length != 0)
            pur_qty = $('#pur_qtyid' + item_id).val();
        if ($('#row_id' + item_code).length != 0) {
            item_id = $('#row_id' + item_code).val();
            free_qty = $('#free_qtyid' + item_id).val();

        }
        if (free_qty > 0 || pur_qty > 0) {

            var item_detail_id = $('#item_detail_id' + item_id).val();

            $('#pur_qtyid' + item_id).val(0);
            $('#pur_qtyid' + item_id).trigger('onkeyup');
            $('#free_qtyid' + item_id).val(0);
            $('#free_qtyid' + item_id).trigger('onkeyup');
            if (item_array_new.hasOwnProperty(item_id)) {
                delete item_array_new[item_id];
            }
            if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                delete product_each_charge['tax'][item_id][item_code];
            }
            if (product_each_charge['total'].hasOwnProperty(item_id)) {
                delete product_each_charge['total'][item_id][item_code];
            }
            if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                delete product_each_charge['discount'][item_id][item_code];
            }
            if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
                delete product_each_charge['on_tax_amount'][item_id][item_code];
            }
            if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
                delete product_each_charge['tax_free'][item_id][item_code];
            }
            if (product_each_charge['othch'].hasOwnProperty(item_id)) {
                delete product_each_charge['othch'][item_id][item_code];
            }

            if (item_detail_id != '0') {
                var position = delete_array.indexOf(item_detail_id);
                if (position) {
                    delete_array.push(item_detail_id);
                }
            }
        }


        if (~position) {
            stock_details_array.splice(position, 1);
        }
        delete item_array_new[item_id];
        if (document.getElementById('itemdetailselect' + item_code)) {
            document.getElementById('itemdetailselect' + item_code).checked = false;
            $('#ietmcodeitemdesc' + item_id).remove();
        }
        stock_details_array.splice(position, 1);
        if (document.getElementById('itemadd_row' + item_id)) {
            $('#itemadd_row' + item_id).remove();
            add_item--;
        }
        if (document.getElementById('ietmcodeitemdesc' + item_id)) {
            $('#ietmcodeitemdesc' + item_id).remove();
        }

    }

}
var item_charge_array = {};
function item_codecharge(item_code, detail_id, item_id) {
    item_charge_array[item_id] = {};
    item_charge_array[item_id][item_code] = {};
    var pur_amnt;
    pur_amnt = ($("#pur_qtyid" + item_id).val() * $("#pur_rateid" + item_id).val());
    if (pur_amnt <= 0) {
        toastr.warning("Tax Amount is applicable only when Purchase Amount greater than zero");
    }
    var flag = true;
    var charge_per = 0.0;
    var i = 0;
    var charge_type = 0;
    var perc_flag = true;
    $('#chargecap_tableid tr').each(function () {

        if ($.type($(this).find('input[name="charge_percentage[]"]').val()) != "undefined") {
            charge_per = $(this).find('input[name="charge_percentage[]"]');
            flag = validate_number(charge_per, 2, 1);
            var per_id = $(this).find('input[name="charge_percentage[]"]').attr('id');
            if ($.type(per_id) == 'undefined')
                var per_id = $(this).find('input[name="charge_amount[]"]').attr('id');


            var res = per_id.split('charge_percentage');
            var item_id_charge = res[1];
            var charge_amount = $("#charge_amount" + item_id_charge).val();

            if (flag) {
                charge_amountobj = $(this).find('input[name="charge_amount[]"]');
                flag = validate_number(charge_amountobj, 2, 1);
                if (flag) {
                    charge_per = $(this).find('input[name="charge_percentage[]"]').val();
                    flag = true;
                } else {
                    toastr.error("Not a valid Number !!");
                    $('#charge_amount' + item_id_charge).focus();
                    flag = false;
                    return false;
                }

            } else {
                toastr.error("Not a valid Number !!");
                $('#charge_percentage' + item_id_charge).focus();
                flag = false;
                return false;
            }
            if (detail_id == '0') {
                if ((charge_per != '0' && charge_per != '')) {
                    item_charge_array[item_id][item_code][item_id_charge] = charge_per + ':' + charge_amount;
                }
            } else {
                item_charge_array[item_id][item_code][item_id_charge] = charge_per + ':' + charge_amount;
            }
            charge_type = $("#charge_type" + item_id_charge).val();
            if (charge_type == 'DA' || charge_type == 'GA') {
                if (charge_per < 0 || charge_per > 100) {
                    flag = false;
                    perc_flag = false;
                    return false;
                }
            }


        }

    })


    if (flag) {
        $('#get_otherchargespopup').hide();
    }

    if (!perc_flag) {
        toastr.warning("Percentage Should be between 0 & 100");
    }
}

function close_charge_model() {
    $('#get_otherchargespopup').hide();
}

jQuery('body').on('click', '[data-toggle=dropdown]', function () {
    var opened = $(this).parent().hasClass("open");
    if (!opened) {
        $('.btn-group').addClass('open');
        $("button.multiselect").attr('aria-expanded', 'true');
    } else {
        $('.btn-group').removeClass('open');
        $("button.multiselect").attr('aria-expanded', 'false');
    }
});
function get_subcategory(obj) {
    var category = obj.value;
    if (!category) {
        category = 'All';
    }
    var cat_id = '#' + obj.id;
    var category = [];
    $(cat_id + ' option:selected').each(function (ind, el) {
        if ($(el).val() != '') {
            category.push($(el).val());
        }
    });
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token, category: category };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#getsubcategory_code_div').html('Please Wait!!!');
            $('#search_itemdataspin').removeClass('fa fa-search');
            $('#search_itemdataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            if (category.length != 0) {
                $('#getsubcategory_code_div_main').show();
                $('#getsubcategory_code_div').html(result);

                $('#get_subcatgeoryselect').multiselect({
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: true,
                    minWidth: 300,
                    buttonWidth: '250px',
                    maxHeight: 400,
                    enableCaseInsensitiveFiltering: true,
                });
            } else {
                $('#getsubcategory_code_div_main').hide();
                $('#getsubcategory_code_div').html('');
            }
        },
        complete: function () {
            $('#search_itemdataspin').removeClass('fa fa-spinner fa-spin');
            $('#search_itemdataspin').addClass('fa fa-search');
        }
    });
}
function get_subcategory1(obj) {
    var category = obj.value;
    if (!category) {
        category = 'All';
    }
    var cat_id = '#' + obj.id;
    var category = [];
    $(cat_id + ' option:selected').each(function (ind, el) {
        if ($(el).val() != '') {
            category.push($(el).val());
        }
    });
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token, category1: category };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#getsubcategory_code_div1').html('Please Wait!!!');
            $('#search_itemdataspin').removeClass('fa fa-search');
            $('#search_itemdataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            if (category.length != 0) {
                $('#getsubcategory_code_div_main1').show();
                $('#getsubcategory_code_div1').html(result);

                $('#get_subcatgeoryselect1').multiselect({
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: true,
                    minWidth: 300,
                    buttonWidth: '300px',
                    maxHeight: 400,
                    enableCaseInsensitiveFiltering: true,
                });
            } else {
                $('#getsubcategory_code_div_main1').hide();
                $('#getsubcategory_code_div1').html('');
            }
        },
        complete: function () {
            $('#search_itemdataspin').removeClass('fa fa-spinner fa-spin');
            $('#search_itemdataspin').addClass('fa fa-search');
        }
    });
}


function cancelbill() {
    $("#purchaselistModal").modal('toggle');
}

function fillitemsearchValues(list, item_name, item_code) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#item_desc_search').val(item_name + ' [ ' + item_code + ' ]');
    $('#' + itemCodeListDivId).hide();
    $('#item_desc_search_hidden').val(item_code);
    $('#item_desc_search').attr('onClick', 'f3_clicked(' + "'" + item_code + "'" + ')');
}
function fillitemdescValues2(list, item_name, item_code) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#request_no_search').val(item_name);
    $('#' + itemCodeListDivId).hide();
    $('#request_no_search_hidden').val(item_code);

}
function fillitemdescValues(list, item_name, item_code) {


    var itemCodeListDivId = $(list).parent().attr('id');
    $('#item_desc_search_new').val(item_name + ' [ ' + item_code + ' ]');
    $('#' + itemCodeListDivId).hide();
    $('#item_newdesc_listsearch').val(item_code);
    AddNewProduct(0, 2, 0, '');
}
function fillitemdescValues1(list, item_name, item_code, unit_name = '') {

    var itemCodeListDivId = $(list).parent().attr('id');
    $('#item_desc_search_new').val(item_name + ' [ ' + item_code + ' ]' + ' [ ' + unit_name + ' ]');
    $('#' + itemCodeListDivId).hide();
    $('#item_newdesc_listsearch').val(item_code);
    AddNewProduct(0, 2, 1, item_code);

}
function fillitemreq_no(list, req_no) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#item_requistion_no').val(req_no);
    $('#' + itemCodeListDivId).hide();
    $('#item_requistion_nohidden').val(req_no);
}
function fillitemreq_by(list, user_name, user_id) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#requested_by_search').val(user_name);
    $('#' + itemCodeListDivId).hide();
    $('#requested_by_hidden').val(user_id);
}

function close_batch_details(inc_id) {
    $('#batchwise_datadiv' + inc_id).html('');
}
function fillitempo_no(list, req_no, type = 0) {
    var itemCodeListDivId = $(list).parent().attr('id');
    if (type == '1') {
        $('#popup_po_no_srch').val(req_no);
    } else {
        $('#po_number_search').val(req_no);
    }
    $('#' + itemCodeListDivId).hide();
    if (type == '1') {
        $('#popup_po_number_search_hidden').val(req_no);
    } else {
        $('#po_number_search_hidden').val(req_no);
    }
}
function fillporeq_by(list, user_name, user_id) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#porequested_by_search').val(user_name);
    $('#' + itemCodeListDivId).hide();
    $('#porequested_by_hidden').val(user_id);
}
function serach_po() {
    var item_arrayString = JSON.stringify(item_manage_array);
    var item_list_Srting = JSON.stringify(item_list_final);
    var file_token = $('#hidden_filetoken').val();

    var purchase_item_desc = $('#item_desc_search_hidden').val();
    var purchase_item_desc_char = $('#item_desc_search').val();
    var requested_by = $('#requested_by_hidden').val();
    var department = $('#search_from_department').val();
    var purchase_req_no = $('#item_requistion_no').val();
    var category_code = $('#from_category1').val();
    var sub_category_code = $('#get_subcatgeoryselect1').val();
    var manufacture_text = $('#manufacture_search').val();
    var manufacture_code = $('#manufacture_search_hidden').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var department = $('#search_from_department').val();
    var vendor_code = $('#vendor_list_po_hidden').val();
    var param = {
        _token: file_token,
        item_arraySearch: item_arrayString,
        item_list_Srting: item_list_Srting,
        search: 1,
        department: department,
        from_date: from_date,
        to_date: to_date,
        purchase_req_no: purchase_req_no,
        purchase_item_desc: purchase_item_desc,
        purchase_item_desc_char: purchase_item_desc_char,
        requested_by: requested_by,
        vendor_code: vendor_code,
        category_code: category_code,
        sub_category_code: sub_category_code,
        manufacture_code: manufacture_code,
        manufacture_text: manufacture_text
    };
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            $("#tbody_poitemdata").html(result);
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
        },
        complete: function () {
            $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
            $('#searchdataspin').addClass('fa fa-search');
            $('.theadfix_wrapper').floatThead('reflow');
        }
    });
}

function remove_purchase_REQ(detail_id) {
    if (confirm('Are you sure wanted to delete ?')) {
        /* removing Check Purchase Req list  */
        $('#add_topurachase' + detail_id).prop("checked", false);
        var manage_position = item_list_final.indexOf(detail_id);
        var item_code = $('#item_code_req' + detail_id).text();
        if (~manage_position) {
            item_list_final.splice(manage_position, 1);
            delete purchase_requisition[detail_id];
        }

        $.ajax({
            type: "POST",
            url: '',
            data: { detail_id: detail_id, po_req_delete: 1 },
            beforeSend: function () {
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner fa-spin');
            },
            success: function (result) {
                if (result) {
                    $("#requisition_id_" + detail_id).remove();
                    toastr.success("Purchase Requisition item Removed Successfully !!!");
                }
            },
            complete: function () {
                $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
                $('#searchdataspin').addClass('fa fa-search');
            }
        });
    }
}

function addtopurchase() {
    var item_array_length = item_array.length;
    var file_token = $('#hidden_filetoken').val();
    if (item_array_length != 0) {
        var item_arrayString = JSON.stringify(item_array);
        var param = { _token: file_token, item_array: item_arrayString };
        $.ajax({
            type: "POST",
            url: '',
            data: param,
            beforeSend: function () {
                $('#adddataspin').removeClass('fa fa-plus-square-o');
                $('#adddataspin').addClass('fa fa-spinner fa-spin');
            },
            success: function (result) {
                $("#purchaselistDiv").html(result);

                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $("#purchaselistModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            complete: function () {
                $('#adddataspin').removeClass('fa fa-spinner fa-spin');
                $('#adddataspin').addClass('fa fa-plus-square-o');
            }
        });
    } else {
        toastr.warning("Please Select An Item!!!");
    }
}
function po_mail() {
    var po_type_id = $('#add_edit_status').val();
    var file_token = $('#hidden_filetoken').val();
    var text = tinymce.get("mail_template_text").getContent();
    var mail_body = btoa(text);
    $.ajax({
        url: '',
        data: { po_type_id: po_type_id, mail_body: mail_body, _token: file_token },
        type: 'post',
        beforeSend: function () {
            $('#adddataspin').removeClass('fa fa-plus-square-o');
            $('#adddataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            if (result == 'email-error') {
                toastr.error("Vender Email Id Error Please Update!!");
            } else if (result == 'po-error') {
                toastr.error("Purchase Order Number Error Try later!!");
            } else {
                toastr.info("Email will send to Vender !!");
            }
        },
        complite: function () {
            $('#adddataspin').removeClass('fa fa-spinner fa-spin');
            $('#adddataspin').addClass('fa fa-plus-square-o');
        }
    });
}
function po_pdf_mail() {
    var po_type_id = $('#add_edit_status').val();
    var attachment_file_id_string = $('.attachment_file_id_string').val();
    var file_token = $('#hidden_filetoken').val();
    var text = tinymce.get("mail_template_text").getContent();
    var sub = $('#subject_id').val();
    var po_status = $('#po_status').text().trim();
    var cc = $('#ccinput').val().toLowerCase().split(",");

    var mail_body = btoa(text);
    $.ajax({
        url: '',
        data: { po_type_id: po_type_id, mail_body: mail_body, _token: file_token, sub: sub, to: to, cc: cc, po_mail: 'po_mail', po_status: po_status, attachment_file_id_string: attachment_file_id_string },
        type: 'post',
        beforeSend: function () {
            $('#adddataspin').removeClass('fa fa-plus-square-o');
            $('#adddataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            console.log(result);
            if (result == '0') {
                toastr.error("Vender Email Id Error Please Update!!");
            } else if (result == 'po-error') {
                toastr.error("Purchase Order Number Error Try later!!");
            } else {
                toastr.info("Email will send to Vender !!");
            }
        },
        complite: function () {
            $('#adddataspin').removeClass('fa fa-spinner fa-spin');
            $('#adddataspin').addClass('fa fa-plus-square-o');
        }
    });
}
function po_print(post_type) {
    var po_h_id = $('#add_edit_status').val();
    var file_token = $('#hidden_filetoken').val();
    var po_print = 'po_print';
    $.ajax({
        url: '',
        data: { po_h_id: po_h_id, _token: file_token, post_type: post_type, po_print: po_print },
        type: 'post',
        beforeSend: function () {
            $('#adddataspin').removeClass('fa fa-plus-square-o');
            $('#adddataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            if (result) {
                var popupWin = window.open('', 'my div');
                popupWin.document.write(result);
                popupWin.document.close();
                popupWin.focus();
                popupWin.print();
                popupWin.close();
                return true;
            }
        },
        complite: function () {
            $('#adddataspin').removeClass('fa fa-spinner fa-spin');
            $('#adddataspin').addClass('fa fa-plus-square-o');
        }
    });
}
$("#genarate_poli").click(function () {
    if ($("#po_status").html() != "")
        $("#purchase_order_status").css('display', 'block');
});
function free_item_change(item_id, obj) {
    if (obj.value == 1) {
        $('#pur_qtyid' + item_id).val(0);
        $('#pur_rateid' + item_id).val(0);
        $('#pur_qtyid' + item_id).trigger('onkeyup');


    }
}

function locationEnable() {
    if ($("#save_itemlist #added_new_list_table_product tr").length == 0) {
        $('#deprt_list').attr("disabled", false);
    }
}


function CheckAllCheckbox() {
    var is_checked = $('#check_all').is(':checked');
    item_list_final = [];
    if ($("#tbody_poitemdata tr").length > 0) {
        if ($('#vendor_list_po').val() != "") {
            if (is_checked)
                $(".req_listcheck").prop('checked', false);
            else
                $(".req_listcheck").prop('checked', true);
            $(".req_listcheck").each(function () {
                $(this).trigger("click");
            });
        } else {
            $('#check_all').prop("checked", false);
            toastr.error("Please Select Supplier !!");
        }
    } else {
        $('#check_all').attr('checked', false);
        toastr.error("No Items Found");
    }
}

/*PO LIST*/
function add_po_request(offset, type = 'new', mode = 0) {
    var po_no = $("#popup_po_no_srch").val();
    var from_date = $("#popup_from_date").val();
    var to_date = $("#popup_to_date").val();
    $.ajax({
        type: "GET",
        url: '',
        data: { offset: offset, po_list: '1', po_no: po_no, from_date: from_date, to_date: to_date },
        beforeSend: function () {
            if (mode == 0) {
                $("#popup_po_no_srch,#popup_po_number_search_hidden").val('');
            }
            $('#po_list_popup').modal({ backdrop: 'static', keyboard: false });
        },
        success: function (result) {
            if (type == 'append') {
                $('#all_po_list').append(result);
            } else if (type == 'new') {
                $('#all_po_list').html(result);
            }
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead('reflow');
            }, 300);
        },
        complete: function () {
        }
    });
}

var col = $('#scrolldown_PO_list');
window.offset = 0;
col.scroll(function () {
    if (Math.trunc(col.outerHeight()) == Math.trunc((col.get(0).scrollHeight - col.scrollTop()))) {
        window.offset += 15;
        add_po_request(window.offset, 'append', 1);
    }
});

function add_new_po(head_id, amend) {
    purchaseEditList(head_id, amend, 'new_po');
    $('#po_list_popup').modal('toggle');
}

function popup() {
    if ($("#mailinput").val()) {

        $(".reci_div").hide();
        $(".enter_mailid_box").show();
    }


    $('.reci_button').click(function () {
        $('.enter_mailid_box').css('display', 'table-row');
        $('#mailinput').focus();
        $(this).parent().hide();
    });

    $(document).on('click', '.purchaseMailClass', function (e) {
        e.stopPropagation();
    });

    $('#mailinput').blur(function () {
        if (!$(this).val()) {
            $(document).on('click', function (e) {

                $('.enter_mailid_box').hide();
                $('.reci_div').show();
            });

        }

        if ($(this).val()) {

            $('.reci_div').hide();
            $('.enter_mailid_box').show();

            $(document).on('click', function (e) {
                $('.enter_mailid_box').show();
                $('.reci_div').hide();
                $('.cc_button').hide();
            });
        }

        if ($("#mailinput").val()) {
            $(document).on('click', function (e) {

                $('.enter_mailid_box').show();
                $('.reci_div').hide();

            });
        }
    });




    $('#mailBodyPopUpModel').on('shown.bs.modal', function () {


        $(document).on('click', '.mailid_close', function (e) {
            e.stopPropagation();
        });

        $(document).on('click', function (e) {

            if ($("#mailinput").val()) {
                $('.mailid_div:last-child').text($('#mailinput').val());

                $("#mailinput").val('');
                $(".mailid_div:last-child").append("<span class='mailid_close'>X</span>");
                $(".mailid_div:last-child").clone().insertAfter(".mailid_div:last-child");

                $(".mailid_close").click(function () {
                    $(this).parent().remove();
                    $('.mailid_div:last-child').text('');
                });
            }

        });

        $(document).on("keyup", '#mailinput', function (e) {

            if (e.keyCode == 188) {

                var str = $('#mailinput').val().split(',');


                $('.mailid_div:last-child').text($('#mailinput').val());
                $('.mailid_div:last-child').text(str[0]);

                $("#mailinput").val('');
                $(".mailid_div:last-child").append("<span class='mailid_close'>X</span>");
                $(".mailid_div:last-child").clone().insertAfter(".mailid_div:last-child");


                $(".mailid_close").click(function () {
                    $(this).parent().remove();
                    $('.mailid_div:last-child').text('');
                });
            }
        });


    })


    /* mail end*/
}

function addNewTermsConditionPopup() {

    $('#add_new_item_model').modal({ backdrop: 'static', keyboard: false });
    $("#add_new_item_model #new_terms_condition").val('');
}
function saveNewTermsCondition() {
    var text = JSON.stringify($('#addNewItemContent #new_terms_condition').val());
    var url = "";
    var file_token = $('#hidden_filetoken').val();
    var param = { terms_condition: text, AddPOTermsCondition: 1, _token: file_token };
    if ($('#addNewItemContent #new_terms_condition').val() != "") {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
            },
            success: function (result) {
                if (result != 0) {
                    toastr.success("Added Successfully");
                    $('#add_new_item_model').modal('toggle');
                    $("#default_terms_condition").append('<tr><td style="text-align: center;"><div class="checkbox checkbox-info inline"><input checked="" id="purchase_order_terms_remove_add' + result + '" onclick="purchaseOrderAddRemoveTerms(' + result + ');" name="purchase_order_terms_remove_add" class="styled" type="checkbox"><label for="purchase_order_terms_remove_add' + result + '"></label></div></td><td style="text-align: left">' + $('#addNewItemContent #new_terms_condition').val() + '</td></tr>');
                    purchaseOrderAddRemoveTerms(result);
                }
                else { toastr.error("Please Try Again"); }
            },
            complete: function () {
            }
        });
    } else {
        toastr.warning("Please Enter Terms and Condition");
    }
}
function close_terms_condition_popup() {
    $('#add_new_item_model').modal('toggle');
}

function getSellerFooterContent(type) {
    var selector_id;
    if (type == 1) { selector_id = 'seller_acceptance'; }
    else if (type == 2) { selector_id = 'footer'; }

    if (tinymce.get(selector_id)) {
        tinymce.remove('#' + selector_id);
    }
    tinymce.init({
        selector: '#' + selector_id,
        toolbar: ' undo redo | \n\
        bold italic underline | alignleft aligncenter alignright alignjustify | \n\
        | clearBtn ' ,
        menubar: false,
        forced_root_block: false,
        setup: function (editor) {
            editor.ui.registry.addButton('mybutton', {
                text: 'Template',
                icon: false,
            });
        }
    });
}

function close_conditionsetbtn() {
    $('#terms_conndition_model').modal('hide');
}


function modelClose() {
    $('#po_qutation').modal('hide');
}

$(document).on('click', '#all_chk_item_list_cls', function (e) {

    if ($("#all_chk_item_list_cls").is(":checked") == true) {
        $(".chk_item_list_cls").each(function () {
            $(this).prop("checked", true);
            var item_code = $(this).closest("tr").attr('class');
            newitem_manage(item_code);
            $(this).removeClass('chk_item_list_cls');
        });
    }
});

$(document).on('click', '#search_all_item', function (e) {
    if ($("#search_all_item").is(":checked")) {
        $("#reorderlevelcheck").prop("checked", false);
        $("#reorderlevelcheck").prop("disabled", true);
    } else {
        $("#reorderlevelcheck").prop("disabled", false);
    }
});

$(document).on('click', '#reorderlevelcheck', function (e) {
    if ($("#reorderlevelcheck").is(":checked")) {
        $("#reorderlevelcheck").prop("checked", true);
        $("#search_all_item").prop("checked", false);
    }
});


function getGlobalStock(item_id){
  var global_item_code = $('#item_codeid' + item_id).val();
   var file_token = $('#hidden_filetoken').val();
   var param = { _token: file_token, global_item_code: global_item_code };
    $.ajax({
        url: '',
        type: "POST",
        data: param,
        beforeSend: function() {
             $('#globebtnspin' + item_id).removeClass('fa fa-globe');
            $('#globebtnspin' + item_id).addClass('fa fa-spinner fa-spin');
        },
        success: function(res) {
            $('#global_stock_data').html(res);
            $('#global_stock_modal').modal('show');
        },
        complete: function() {

          $('#globebtnspin' + item_id).removeClass('fa fa-spinner fa-spin');
          $('#globebtnspin' + item_id).addClass('fa fa-globe');
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
              });

        }
    });
}



function location_list(global_item_code){
   var location_list = $( "#location_list option:selected" ).val();

   var file_token = $('#hidden_filetoken').val();
   var param = { _token: file_token, location_list: location_list,global_item_code: global_item_code };
    $.ajax({
        url: '',
        type: "POST",
        data: param,
        beforeSend: function() {

        },
        success: function(res) {
            $('#global_stock_data').html(res);
            $('#global_stock_modal').modal('show');

        },
        complete: function() {

         $("#location_list").val(location_list);
        }
    });
}
