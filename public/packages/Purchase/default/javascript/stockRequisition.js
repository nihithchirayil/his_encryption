$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    searchRequisitionData();
});


var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();

function searchRequisitionData() {
    var url = base_url + "/grn/searchRequisitionData";
    var request_id = $('#Requestid_hidden').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var status = $('#status').val();
    var param = {
        _token: token,
        request_id: request_id,
        from_date: from_date,
        to_date: to_date,
        from_location: from_location,
        to_location: to_location,
        status: status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchRequisitionDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#searchRequisitionBtn').attr('disabled', true);
            $('#searchRequisitionSpin').removeClass('fa fa-search');
            $('#searchRequisitionSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchRequisitionDataDiv').html(data.html);
            $('#total_cntspan').html(data.total_records);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#searchRequisitionDataDiv").LoadingOverlay("hide");
            $('#searchRequisitionBtn').attr('disabled', false);
            $('#searchRequisitionSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchRequisitionSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


$('#RequestNo_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('RequestNo_search_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var RequestNo_search = $(this).val();
        if (RequestNo_search == "") {
            $('#Requestid_hidden').val('');
            $("#RequestNo_search_AjaxDiv").hide();
        } else {
            var url = base_url + "/grn/RequestNo_search";
            var param = { RequestNo_search: RequestNo_search };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#RequestNo_search_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#RequestNo_search_AjaxDiv").html(html).show();
                    $("#RequestNo_search_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('RequestNo_search_AjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillitem_desc(id, request_no) {
    request_no = htmlDecode(request_no);
    $('#RequestNo_search').val(request_no);
    $('#Requestid_hidden').val(id);
    $('#RequestNo_search_AjaxDiv').hide();
}

function addStockRequestNew() {
    var main_uri = base_url + "/purchase/";
    var route_path = "addStockRequests";
    var action_url = main_uri + route_path;
    document.location.href = action_url;
}

function stockItemEditLoadData(list, stock_id) {
    var main_uri = base_url + "/purchase/";
    var route_path = "addStockRequests/" + stock_id;
    var action_url = main_uri + route_path;
    document.location.href = action_url;
}


function resetFilter() {
    var default_location = $('#default_location').val();
    $('#RequestNo_search').val();
    $('#Requestid_hidden').val('');
    $('#RequestNo_search_AjaxDiv').hide();
    $('#from_date').val('');
    $('#to_date').val('');
    $('#from_location').val('All');
    $('#from_location').select2();
    $('#to_location').val(default_location);
    $('#to_location').select2();
    $('#status').val('All');
    $('#status').select2();
    searchRequisitionData();
}
