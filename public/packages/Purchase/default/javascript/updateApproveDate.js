$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 10 });
    searchApproveGrn();
});

var base_url = $('#base_url').val();
var token = $('#token').val();


function searchApproveGrn() {
    var url = base_url + "/grn/searchApproveGrn";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var grn_id = $('#grn_id_hidden').val();
    var bill_no = $('#bill_id_hidden').val();
    var location = $('#to_location').val();

    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        grn_id: grn_id,
        bill_no: bill_no,
        location: location,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchGrnListData").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#searchgrnlistBtn').attr('disabled', true);
            $('#searchgrnlistSpin').removeClass('fa fa-search');
            $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchGrnListData').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#searchGrnListData").LoadingOverlay("hide");
            $('#searchgrnlistBtn').attr('disabled', false);
            $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchgrnlistSpin').addClass('fa fa-search');
            $('.datetime_picker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function updateGrnApproveDate(grn_id) {
    var url = base_url + "/grn/saveGrnApproveDate";
    var approve_date = $('#grnApproveDate' + grn_id).val();
    var approve_time = $('#grnApproveTime' + grn_id).val();
    var param = {
        _token: token,
        grn_id: grn_id,
        approve_date: approve_date,
        approve_time: approve_time
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#updateGrnApproveDateBtn' + grn_id).attr('disabled', true);
            $('#updateGrnApproveDateSpin' + grn_id).removeClass('fa fa-save');
            $('#updateGrnApproveDateSpin' + grn_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                toastr.success("Successfully Updated");
            }
        },
        complete: function () {
            $('#updateGrnApproveDateBtn' + grn_id).attr('disabled', false);
            $('#updateGrnApproveDateSpin' + grn_id).removeClass('fa fa-spinner fa-spin');
            $('#updateGrnApproveDateSpin' + grn_id).addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


$('#grn_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxGrnNoSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var grn_number = $('#grn_no_search').val();
        if (grn_number == "") {
            $('#ajaxGrnNoSearchBox').hide();
            $("#grn_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'grn_number', search_key: grn_number },
                beforeSend: function () {
                    $("#ajaxGrnNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxGrnNoSearchBox").html(html).show();
                    $("#ajaxGrnNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxGrnNoSearchBox', event);
    }
});


$('#bill_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxBillNoSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#bill_no_search').val();
        if (bill_no == "") {
            $('#ajaxBillNoSearchBox').hide();
            $("#bill_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'invoice_number', search_key: bill_no },
                beforeSend: function () {
                    $("#ajaxBillNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxBillNoSearchBox").html(html).show();
                    $("#ajaxBillNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxBillNoSearchBox', event);
    }
});


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function fillSearchDetials(grn_id, grn_number) {
    grn_number = htmlDecode(grn_number);
    $('#grn_no_search').val(grn_number);
    $('#grn_id_hidden').val(grn_id);
    $('#ajaxGrnNoSearchBox').hide();
}


function fillInvoice_no(grn_id, bill_no) {
    bill_no = htmlDecode(bill_no);
    $('#bill_no_search').val(bill_no);
    $('#bill_id_hidden').val(grn_id);
    $('#ajaxBillNoSearchBox').hide();
}


function clearFilter() {
    var default_location = $('#default_location').val();
    $('#from_date').val('');
    $('#to_date').val('');
    $('#grn_id_hidden').val('');
    $('#bill_no_search').val('');
    $('#grn_no_search').val('');
    $('#bill_id_hidden').val('');
    $('#to_location').val(default_location).select2();
    searchApproveGrn();
}
