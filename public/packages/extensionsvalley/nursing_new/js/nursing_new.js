var base_url = $('#base_url').val();
var token = $('#c_token').val();
var op_list_enabled = $('#nursing_op_list_enabled').val();
var is_casuality_user = $('#is_casuality_user').val();
var casuality_investigation_indent_type = $('#casuality_investigation_indent_type').val();
$(document).ready(function () {

    // $('.select2').select2();
    $(".select2").select2({
        dropdownParent: "#modal_add_to_do"
    });
    getToDoList();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.ns_search_casuality_date').hide();
    $('.ns_patient_search').hide();
    // $('.global_patient_search').hide();

    setInterval(function () {
        // fetchInvStatus(1);
    }, 20000);

    if (op_list_enabled == '1') {
        loadOpPatients();
    } else {
        if (is_casuality_user == '1') {
            loadCasulityPatients();
        }else {
        // changeNursingStation($('#default_nursing_station').val());
            var station_id = $('#ns_station_list').val();
            if (station_id != '' && station_id != undefined)
                changeNursingStation(station_id);
        }
    }
});


$(".btn-fetch-inv-intend-status").on("click", ".fa-search", function () {
    fetchInvStatus(0);
});

function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}
var allergy_cnt = 1;

function removePatientMedicneAllergy(row_cnt) {
    $('#patientMedicneAllergyRow' + row_cnt).remove();
    serialNoCalculation('allergy_class', 'allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}

function removePatientOtherAllergy(row_cnt) {
    $('#patientOtherAllergyRow' + row_cnt).remove();
    serialNoCalculation('other_allergy_class', 'other_allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}
var vital_edit_type = 0;

function getAllPatientAllergy(patient_allegies, from_type) {
    if (parseInt(from_type) == 1) {
        $.each(patient_allegies.generic_allergies, function (index, value) {
            addMedicneAllergy(1, value.generic_id, value.generic_name);
        });
        $.each(patient_allegies.medicine_allergies, function (index, value) {
            addMedicneAllergy(2, value.item_code, value.item_desc);
        });
        $.each(patient_allegies.other_allergies, function (index, value) {
            addOtherAllergy(value.allergy_id, value.allergy_name);
        });
    } else if (parseInt(from_type) == 2) {
        $(".allergy_list").html('');
        $.each(patient_allegies.medicine_allergies, function (index, value) {
            // allergy = '<span class="blue"> Brand : </span> ' + value.item_desc + '<br>';
            allergy = value.item_desc + '<br>';
            $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + value.item_code + '">' + allergy + '</span>');
        });
        $.each(patient_allegies.generic_allergies, function (index, value) {
            // allergy = '<span class="blue"> Generic : </span> ' + value.generic_name + '<br>';
            allergy = value.generic_name + '<br>';
            $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + value.generic_id + '">' + allergy + '</span>');
        });

        $.each(patient_allegies.other_allergies, function (index, value) {
            // allergy = '<span class="blue"> Other : </span> ' + value.allergy_name + '<br>';
            allergy = value.allergy_name + '<br>';
            $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + value.allergy_id + '">' + allergy + '</span>');
        });
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });
    }
}

function addMedicneAllergy(search_by = '', item_code = '', item_desc = '') {
    $search_brand = '';
    $search_generic = '';
    if (parseInt(search_by) == 1) {
        $search_generic = "selected=''";
    } else if (parseInt(search_by) == 2) {
        $search_brand = "selected=''";
    }
    $(".patientMedicneAllergytddata").append("<tr id='patientMedicneAllergyRow" + allergy_cnt + "' class='allergy_class'><td style='text-align: center; line-height: 25px;' class='allergy_count_class'></td><td><select class='form-control' id='genderic_brand" + allergy_cnt + "' name='genderic_brand'><option " + $search_generic + " value='1'>Generic</option><option " + $search_brand + " value='2'>Brand</option></select</td><td><input name='allergy_medicine_code' value='" + item_code + "' id='allergy_medicine_code" + allergy_cnt + "' type='hidden'><input  value='" + item_desc + "' onkeyup='searchPatientAllergyMedicne(this.id,event," + allergy_cnt + ")' type='text' autocomplete='off' name='allergy_name' id='allergy_name" + allergy_cnt + "' class='form-control bottom-border-text borderless_textbox allergy_name'/><div class='ajaxSearchBox dropdown_div' style='width: 66%' id='ajaxSearchBox" + allergy_cnt + "'></div></td><td class='common_td_rules deletePrescriptionItemBtn'><i  onclick='removePatientMedicneAllergy(" + allergy_cnt + ")' class='fa fa-times red'></i></td></tr>");
    $('#allergy_name' + allergy_cnt).focus();
    allergy_cnt++;
    serialNoCalculation('allergy_class', 'allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}


function addOtherAllergy(allergy_id = '', allergy_name = '') {
    $(".patientOtherAllergytddata").append("<tr id='patientOtherAllergyRow" + allergy_cnt + "' class='other_allergy_class'><td style='text-align: center; line-height: 25px;' class='other_allergy_count_class'></td><td><input name='other_allergy_code' id='other_allergy_code" + allergy_cnt + "' value='" + allergy_id + "' type='hidden'><input value='" + allergy_name + "' onkeyup='searchPatientOtherAllergy(this.id,event," + allergy_cnt + ")' type='text' autocomplete='off' name='other_allergy_name' id='other_allergy_name" + allergy_cnt + "' class='form-control bottom-border-text borderless_textbox allergy_name'/><div style='width: 90%;' class='ajaxSearchBox dropdown_div' id='ajaxSearchBox" + allergy_cnt + "'></div></td><td class='common_td_rules deletePrescriptionItemBtn'><i  onclick='removePatientOtherAllergy(" + allergy_cnt + ")' class='fa fa-times red'></i></td></tr>");
    $('#other_allergy_name' + allergy_cnt).focus();
    allergy_cnt++;
    serialNoCalculation('other_allergy_class', 'other_allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}

function getPatientMedicineAlergy(e, patient_id, visit_id) {
    var base_url = $("#base_url").val();
    var url = base_url + "/nursing_new/getPatientMedicineAllergy";
    var param = {
        patient_id: patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getPatientAllegryModelHeader').html("Allergy");
            $(e).attr('disabled', true);
            $(e).removeClass('fa fa-plus-circle');
            $(e).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $("#getPatientAllegryModelDiv").html(obj.data);
            getAllPatientAllergy(obj.patient_allegies, 1);
            $("#getPatientAllegryModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $(e).attr('disabled', false);
            $(e).removeClass('fa fa-spinner fa-spin');
            $(e).addClass('fa fa-plus-circle');
            $('#patientMedicneAllergytddata').html('');
            $('#patient_id').val(patient_id);
            addMedicneAllergy();
            addOtherAllergy();
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


function dischargeIntimation(visit_id, from_type, icon_name) {
    if (parseInt(from_type) == 1) {
        $('#dischargeintimationvisit_id').val(visit_id);
        var discharge_remarks = $('#dischargeIntimationRemarks' + visit_id).val();
        $('#discharge_remarks').val(discharge_remarks);
        $("#dischargeintimationMessageModel").modal({
            backdrop: 'static',
            keyboard: false
        });
    } else {
        var base_url = $("#base_url").val();
        var url = base_url + "/nursing_new/saveDischargeintimation";
        var param = {
            visit_id: visit_id,
            from_type: from_type
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#DischargeInitiateData' + from_type + visit_id).attr('disabled', true);
                $('#DischargeInitiateData' + from_type + visit_id).removeClass(icon_name);
                $('#DischargeInitiateData' + from_type + visit_id).addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (parseInt(data) == 1) {
                    $('.dischargeInitiateDiv' + visit_id).hide();
                    toastr.success("Successfully Updated");
                    if (parseInt(from_type) == 2) {
                        $('#DischargeInitiateDiv1' + visit_id).show();
                    } else if (parseInt(from_type) == 3) {
                        $('#DischargeInitiateDiv4' + visit_id).show();
                    } else if (parseInt(from_type) == 4) {
                        $('#DischargeInitiateDiv2' + visit_id).show();
                        $('#DischargeInitiateDiv3' + visit_id).show();
                    }

                    if (parseInt(from_type) == 3 || parseInt(from_type) == 4) {
                        var station_id = $('#ns_station_list').val();
                        // changeNursingStation($('#default_nursing_station').val());
                        if (station_id != '' && station_id != undefined)
                            changeNursingStation(station_id);
                    }
                } else {
                    if (parseInt(data) != 0 && data != '') {
                        $('#patientPendingIntendCountModalDiv').html(data);
                        $("#patientPendingIntendCountModal").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            },
            complete: function () {
                $('#DischargeInitiateData' + from_type + visit_id).attr('disabled', false);
                $('#DischargeInitiateData' + from_type + visit_id).removeClass('fa fa-spinner fa-spin');
                $('#DischargeInitiateData' + from_type + visit_id).addClass(icon_name);
            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    }
}



function saveDischargeintimation() {
    var visit_id = $("#dischargeintimationvisit_id").val();
    var discharge_remarks = $("#discharge_remarks").val();
    var base_url = $("#base_url").val();
    var url = base_url + "/nursing_new/saveDischargeintimation";
    var param = {
        visit_id: visit_id,
        from_type: 1,
        discharge_remarks: discharge_remarks
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#DischargeInitiateNotDoneBtn').attr('disabled', true);
            $('#saveDischargeintimationSpin').removeClass('fa fa-dashcube');
            $('#saveDischargeintimationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (parseInt(data) == 1) {
                toastr.success("Successfully Updated");
                $('.dischargeInitiateDiv' + visit_id).hide();
                $('#DischargeInitiateDiv2' + visit_id).show();
                $('#DischargeInitiateDiv3' + visit_id).show();
                $('#dischargeIntimationRemarks' + visit_id).val(discharge_remarks);
                $('#dischargeintimationMessageModel').modal('toggle');
            }
        },
        complete: function () {
            $('#saveDischargeintimationBtn').attr('disabled', false);
            $('#saveDischargeintimationSpin').removeClass('fa fa-spinner fa-spin');
            $('#saveDischargeintimationSpin').addClass('fa fa-dashcube');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}




function searchPatientAllergyMedicne(id, event, row_id) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox' + row_id);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var medicine_name = htmlDecode($('#' + id).val());
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/searchPatientAllergyMedicine";
        if (!medicine_name) {
            $('.ajaxSearchBox').hide();
            $("#allergy_medicine_code" + row_id).val('');
        } else {
            var genderic_brand = $('#genderic_brand' + row_id).val();
            var allergy_data = JSON.stringify(validateMedicineAllergy());
            var param = {
                medicine_name: medicine_name,
                allergy_data: allergy_data,
                genderic_brand: genderic_brand,
                row_id: row_id
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#ajaxSearchBox' + row_id).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $('#ajaxSearchBox' + row_id).html(html).show();
                    $('#ajaxSearchBox' + row_id).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxSearchBox' + row_id, event);
    }
}


function searchPatientOtherAllergy(id, event, row_id) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox' + row_id);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var allergy_name = htmlDecode($('#' + id).val());
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/searchPatientOtherAllergy";
        if (!allergy_name) {
            $('.ajaxSearchBox').hide();
            $("#other_allergy_code" + row_id).val('');
        } else {
            var allergy_data = JSON.stringify(validateMedicineAllergy());
            var param = {
                allergy_data: allergy_data,
                allergy_name: allergy_name,
                row_id: row_id
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#ajaxSearchBox' + row_id).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $('#ajaxSearchBox' + row_id).html(html).show();
                    $('#ajaxSearchBox' + row_id).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxSearchBox' + row_id, event);
    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillMedicineDetails(item_code, itemdesc, row_id) {
    $("#allergy_medicine_code" + row_id).val(item_code.trim());
    $("#allergy_name" + row_id).val(htmlDecode(itemdesc.trim()));
    $('.ajaxSearchBox').hide();
    if ($(".allergy_class:last").find("input[name='allergy_name']").val() != '') {
        addMedicneAllergy();
    }
}

function fillOtherAllergy(id, allergy, row_id) {
    $("#other_allergy_code" + row_id).val(id);
    $("#other_allergy_name" + row_id).val(htmlDecode(allergy.trim()));
    $('.ajaxSearchBox').hide();
    if ($(".other_allergy_class:last").find("input[name='other_allergy_name']").val() != '') {
        addOtherAllergy();
    }
}

function validateMedicineAllergy() {
    var patient_allergy = new Array();
    $('#patientMedicneAllergyTable tr').each(function () {
        var allergy_code = $(this).find("input[name='allergy_medicine_code']").val();
        var genderic_brand = $(this).find("select[name='genderic_brand']").val();
        if (parseInt(genderic_brand) == 1) {
            patient_allergy.push({
                'allergy_code': allergy_code,
                'allergy_type': 1,
            });
        } else if (parseInt(genderic_brand) == 2) {
            patient_allergy.push({
                'allergy_code': allergy_code,
                'allergy_type': 2,
            });
        }
    });
    $('#patientOtherAllergyTable tr').each(function () {
        var allergy_code = $(this).find("input[name='other_allergy_code']").val();
        if (allergy_code) {
            patient_allergy.push({
                'allergy_code': allergy_code,
                'allergy_type': 3,
            });
        }
    });
    return patient_allergy;
}

function savePatientAllergy() {
    var patient_id = $('#patient_id').val();
    var allergy_data = JSON.stringify(validateMedicineAllergy());
    var base_url = $("#base_url").val();
    var url = base_url + "/nursing_new/savePatientAllergy";
    var param = {
        patient_id: patient_id,
        allergy_data: allergy_data
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#savePatientAllergyBtn').attr('disabled', true);
            $('#savePatientAllergySpin').removeClass('fa fa-save');
            $('#savePatientAllergySpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (parseInt(obj.status) == 1) {
                getAllPatientAllergy(obj.patient_allegies, 2);
                toastr.success('Allergy successfully added');
                $('#getPatientAllegryModel').modal('toggle');
            } else {
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
            $('#savePatientAllergyBtn').attr('disabled', false);
            $('#savePatientAllergySpin').removeClass('fa fa-spinner fa-spin');
            $('#savePatientAllergySpin').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function changeNursingStation(station_id) {
    var url = base_url + "/nursing_new/get_dashboard_content";
    var dataparams = {
        'station_id': station_id,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            if (data != 0) {
                var obj = JSON.parse(data);
                $('.ns_main_patients_container').html('');
                $('.ns_main_patients_container').html(obj.patients_list);
                $('[data-toggle="popover"]').popover({
                    html: true,
                    animation: false,
                    placement: 'auto right'
                });

            } else {
                $('.ns_main_patients_container').html('<h5>No Patients Found!</h5>');
            }
            $('.ns_search_patient').show();
            $('.ns_search_room_type').show();
            $('.ns_search_casuality_date').hide();
            $('.ns_patient_search').hide();

            $('.btn_refresh_dashboard').removeClass('selected_btn');
            if ($('#is_casuality_user').val() == '1') {
                $('#load_ip_patient_btn').addClass('selected_btn');
            }
            // $('.global_patient_search').hide();
        },
        complete: function () {
            $('.BtnRefreshDashboard').removeClass('bg-green');
            $('.BtnRefreshDashboard').addClass('bg-blue');
            $('#btn_load_ip').removeClass('bg-blue');
            $('#btn_load_ip').addClass('bg-green');
            // $('.ns_main_patients_container').removeClass('ns_casuality_patient_container');
            $("body").LoadingOverlay("hide");
        }
    });
}

$('#ns_station_list').on('change', function () {
    var station_id = $(this).val();
    changeNursingStation(station_id);
});

//-----------global patient search---------------------------------
$('#patient_uhid').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if ($('#patient_uhid_hidden').val() != "") {
            $('#patient_uhid_hidden').val('');
            $("#patient_uhidAjaxDiv").html("").hide();
        }
        var patient_name_search = $(this).val();
        if (patient_name_search == "") {
            $("#patient_uhidAjaxDiv").html("").hide();
        } else {
            var url = '';
            $.ajax({
                type: "GET",
                url: url,
                data: "patient_name_search_main=" + patient_name_search,
                beforeSend: function () {
                    $("#patient_uhidAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#patient_uhidAjaxDiv").html(html).show();
                    $("#patient_uhidAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }
    } else {
        ajax_list_key_down('patient_uhidAjaxDiv', event);
    }
});

$("#patient_uhid").on('keydown', function (event) {
    if (event.keyCode == 13) {
        ajaxlistenter('patient_uhidAjaxDiv');
        return false;
    }
});
//------------------//
$('.global_patient_search').on('keyup', function () {
    var url = base_url + "/nursing_new/ns_global_patient_search";
    var data = {};
    data.patient_search_global = $("#patient_search_global").val();
    if($("#patient_search_global").val()==''){
        loadIpPatients();
        return false;
    }
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(".ns_main_patients_container").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $(".ns_main_patients_container").html(data);
            $('[data-toggle="popover"]').popover({
                html: true,
                animation: false,
                placement: 'auto right'
            });
            $('.ns_search_patient').hide();
            $('.ns_search_room_type').hide();
            // $('.ns_search_casuality_date').show();
            $('.ns_patient_search').show();
            $('.global_patient_search').show();
            $(".ns_main_patients_container").LoadingOverlay("hide");
            if ($('.BtnRefreshDashboard.bg-green').attr('id') == 'btn_load_ip') {
                $('.hide_for_ip_global_patient').hide();
            }

        }
    });


});


$('#patient_search_txt1').on('keyup', function () {
    var search_text = this.value;
    if (search_text == '') {
        $('.ns_patient_container_main').show();
    } else {
        $('.ns_patient_container_main').hide();
        $(".ns_patient_container_main[data-attr-patient*='" + search_text.toLowerCase() + "']").show();
        $(".ns_patient_container_main[data-attr-uhid*='" + search_text.toLowerCase() + "']").show();
    }
});


//----PATIENT list javascript search-----------------
$('#patient_search_txt').on('keyup', function () {
    var search_text = this.value;
    if (search_text == '') {
        $('.ns_patient_container_main').show();
    } else {
        $('.ns_patient_container_main').hide();
        $(".ns_patient_container_main[data-attr-patient*='" + search_text.toLowerCase() + "']").show();
        $(".ns_patient_container_main[data-attr-uhid*='" + search_text.toLowerCase() + "']").show();
    }
});

//---------room type filter--------------------------
$('#room_type').on('change', function () {
    var room_type = this.value;
    console.log(room_type);
    if (room_type == '') {
        $('.ns_patient_container_main').show();
    } else {
        $('.ns_patient_container_main').hide();
        $(".ns_patient_container_main[data-attr-room_type='" + room_type + "']").show();
    }
});

//---------To do list expand and collapse----------------
$('#expand_to_do_list').on('click', function () {
    $('.container1').hide();
    $('.container2').css('height', '610px');
    $('#expand_to_do_list').css('display', 'none');
    $('#collapse_to_do_list').css('display', 'block');
});

$('#collapse_to_do_list').on('click', function () {
    $('.container1').show();
    $('.container2').css('height', '298px');
    $('#expand_to_do_list').css('display', 'block');
    $('#collapse_to_do_list').css('display', 'none');
});

//-----add to do list content---------------------------
$('#add_new_to_do').on('click', function () {
    $('#modal_add_to_do').modal('show');
});


$('#to_do_patient_uhid').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if ($('#to_do_patient_uhid_hidden').val() != "") {
            $('#to_do_patient_uhid_hidden').val('');
            $('#to_do_patient_uhid_hidden').val('');
            $("#to_do_patient_uhidAjaxDiv").html("").hide();
        }
        var patient_name_search = $(this).val();
        if (patient_name_search == "") {
            $("#to_do_patient_uhidAjaxDiv").html("").hide();
        } else {
            var url = '';
            $.ajax({
                type: "GET",
                url: url,
                data: "patient_name_search=" + patient_name_search,
                beforeSend: function () {
                    $("#to_do_patient_uhidAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    // alert(html); return;
                    $("#to_do_patient_uhidAjaxDiv").html(html).show();
                    $("#to_do_patient_uhidAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }
    } else {
        ajax_list_key_down('to_do_patient_uhidAjaxDiv', event);
    }
});

$("#to_do_patient_uhid").on('keydown', function (event) {
    console.log(event.keyCode);
    if (event.keyCode == 13) {
        ajaxlistenter('to_do_patient_uhidAjaxDiv');
        return false;
    }
});

function fillPatientValues(uhid, patient_name) {
    $('#to_do_patient_uhid_hidden').val(uhid);
    $('#to_do_patient_uhid').val(patient_name);
    $('#to_do_patient_uhidAjaxDiv').hide();
}

function SaveToDo() {
    var nursing_station = $('#to_do_assigned_location').val();
    var assigned_to = $('#to_do_assigned_to').val();
    var description = $('#to_do_description').val();
    var patient_id = $('#to_do_patient_uhid_hidden').val();
    var task_id = $('#to_do_task_id').val();
    if (nursing_station == '') {
        toastr.warning('Please select nursing station!');
        $('#to_do_assigned_location').focus();
        return false;
    }
    if (nursing_station == '') {
        toastr.warning('Please select nurse!');
        $('#to_do_assigned_to').focus();
        return false;
    }
    if (description == '') {
        toastr.warning('Please enter description!');
        $('#to_do_description').focus();
        return false;
    }

    var dataparams = {
        '_token': token,
        'nursing_station': nursing_station,
        'assigned_to': assigned_to,
        'description': description,
        'patient_id': patient_id,
        'task_id': task_id
    };

    var url = base_url + "/nursing_new/save_to_do";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#modal_add_to_do_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == 1 || obj.status == 2) {
                toastr.success(obj.message);
                getToDoList();
            } else {
                toastr.error(obj.message);
            }
        },
        complete: function () {
            $("#modal_add_to_do_content").LoadingOverlay("hide");
            $('#modal_add_to_do').modal('show');
            getToDoList();
        }
    });
}

function getToDoList() {
    var url = base_url + "/nursing_new/get_to_do_list";
    $.ajax({
        type: "GET",
        url: url,
        data: '',
        beforeSend: function () {
            $("#to_do_list_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#to_do_list_content").html(data);
        },
        complete: function () {
            $("#to_do_list_content").LoadingOverlay("hide");
            $('#i_refresh_to_do').removeClass('fa-spin');

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
}

$('#refresh_to_do').on('click', function () {
    $('#i_refresh_to_do').addClass('fa-spin');
    getToDoList();
});

function updateToDoStatus(id) {
    var url = base_url + "/nursing_new/updateToDoStatus";
    var dataparams = {
        'task_id': id
    }
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#to_do_list_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
            $("#modal_history_to_do_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#to_do_content_list_" + id).remove();
            $("#to_do_history_list_" + id).remove();
        },
        complete: function () {
            $("#to_do_list_content").LoadingOverlay("hide");
            $("#modal_history_to_do_content").LoadingOverlay("hide");

        }
    });
}

function deleteTodo(id) {
    var url = base_url + "/nursing_new/deleteTodo";
    var dataparams = {
        'task_id': id
    }
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#to_do_list_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
            $("#modal_history_to_do_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#to_do_content_list_" + id).remove();
            $("#to_do_history_list_" + id).remove();
        },
        complete: function () {
            $("#to_do_list_content").LoadingOverlay("hide");
            $("#modal_history_to_do_content").LoadingOverlay("hide");

        }
    });
}

$('#history_to_do').on('click', function () {
    var url = base_url + "/nursing_new/historyToDo"
    $.ajax({
        type: "GET",
        url: url,
        data: '',
        beforeSend: function () {
            $('#modal_history_to_do').modal('show');
            $("#modal_history_to_do_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#modal_history_to_do_content").html(data);
        },
        complete: function () {
            $("#modal_history_to_do_content").LoadingOverlay("hide");
        }
    });
});

function editTodo(task_id, nursing_station, assigned_to, description, patient_id, patient_name) {
    $('#modal_history_to_do').modal('hide');
    $('#modal_add_to_do').modal('show');
    $('#to_do_assigned_location').val(nursing_station).select2();
    $('#to_do_task_id').val(task_id);
    $('#to_do_assigned_to').val(assigned_to).select2();
    $('#to_do_description').val(description);
    $('#to_do_patient_uhid').val(patient_name);
    $('#to_do_patient_uhid_hidden').val(patient_id);
}

function gotoNsPatientDashboard(patient_id) {
    // window.location.href = $('#base_url').val() + "/nursing_new/get_patient_dashboard/" + patient_id;
}

function loadCasulityPatients() {
    $('#casuality_date').attr('onblur', 'loadCasulityPatients()');
    $('#doctors_list').addClass('hidden');
    $('#casuality_doctors').removeClass('hidden');
    $('.btn_refresh_dashboard').removeClass('selected_btn');
    if ($('#is_casuality_user').val() == '1') {
        $('#load_casuality_patient_btn').addClass('selected_btn');
    }
    var casuality_station_location = $('#casuality_station_location').val();
    if (casuality_station_location != 0 || casuality_station_location != '') {
        $('#ns_station_list').val(casuality_station_location).select2();
    }
    // $('.ns_main_patients_container').addClass('ns_casuality_patient_container');
    $('#patient_search_global').val('');
    var url = base_url + "/nursing_new/loadCasulityPatients";
    var casuality_date = $('#casuality_date').val();
    var casuality_doctors = $('#casuality_doctors').val();
    var dataparams = {
        casuality_date: casuality_date,
        station_id: casuality_station_location,
        casuality_doctors: casuality_doctors
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $(".ns_main_patients_container").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $(".ns_main_patients_container").html(data);
            $('[data-toggle="popover"]').popover({
                html: true,
                animation: false,
                placement: 'auto right'
            });
            $('.ns_search_patient').hide();
            $('.ns_search_room_type').hide();
            $('.ns_search_casuality_date').show();
            $('.ns_patient_search').show();
            $('.global_patient_search').show();
        },
        complete: function () {
            $('.BtnRefreshDashboard').removeClass('bg-green');
            $('.BtnRefreshDashboard').addClass('bg-blue');
            $('#btn_load_casuality').removeClass('bg-blue');
            $('#btn_load_casuality').addClass('bg-green');
            $(".ns_main_patients_container").LoadingOverlay("hide");
        }
    });
}

function loadOpPatients() {
    $('#casuality_date').attr('onblur', 'loadOpPatients()');
    $('#doctors_list').removeClass('hidden');
    $('#casuality_doctors').addClass('hidden');
    var url = base_url + "/nursing_new/loadOpPatients";
    var casuality_date = $('#casuality_date').val();
    var doctors_list = $('#doctors_list').val();
    var dataparams = {
        casuality_date: casuality_date,
        doctors_list: doctors_list
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $(".ns_main_patients_container").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $(".ns_main_patients_container").html(data);
            $('[data-toggle="popover"]').popover({
                html: true,
                animation: false,
                placement: 'auto right'
            });
            $('.ns_search_patient').hide();
            $('.ns_search_room_type').hide();
            $('.ns_search_casuality_date').show();
            $('.ns_patient_search').show();
            $('.global_patient_search').show();
        },
        complete: function () {
            $('.BtnRefreshDashboard').removeClass('bg-green');
            $('.BtnRefreshDashboard').addClass('bg-blue');
            $('#btn_load_op').removeClass('bg-blue');
            $('#btn_load_op').addClass('bg-green');
            $(".ns_main_patients_container").LoadingOverlay("hide");
        }
    });
}

function loadIpPatients() {
    // $('.btn_refresh_dashboard').removeClass('selected_btn');
    // if ($('#is_casuality_user').val() == '1') {
    //     $('#load_ip_patient_btn').addClass('selected_btn');
    // }
    var station_id = $('#ns_station_list').val();
    if (station_id != '' && station_id != undefined)
        changeNursingStation(station_id);
    // changeNursingStation($('#default_nursing_station').val());
}



function ViewPatientInvestigation(obj, patient_id, visit_id, doctor_id) {
    var url = base_url + "/nursing_new/ViewPatientInvestigation";
    var header_name = '';
    if ($(obj).parent().attr('data-patient') != undefined) {
        var patient_name = ($(obj).parent().attr('data-patient')).trim();
        var patient_uhid = ($(obj).parent().attr('data-uhid')).trim();
        header_name = patient_name + ' - ' + patient_uhid;
        $('#patient_investigation_min .modal-title').html('<i class="fa fa fa-search"></i> Investigation (' + header_name + ')');
    } else {
        $('#patient_investigation_min .modal-title').html('<i class="fa fa fa-search"></i> Investigation');
    }

    var dataparams = {
        patient_id: patient_id,
        visit_id: visit_id,
        doctor_id: doctor_id,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#patient_investigation_min').modal('toggle');
            $("#patient_investigation_min_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#patient_investigation_min_content").html(data);
            if (is_casuality_user == '1' && parseInt(casuality_investigation_indent_type) > 0) {
                $('input[name="investigation_intend_type"][value="' + parseInt(casuality_investigation_indent_type) + '"]').prop('checked', true);
            }
            setTimeout(function () {
                $('.fav_investigation_box').hide();
                $('.show_inv_bookmarks_box').hide();
                $('#doctor_investigation').val(doctor_id);
            }, 1000);
        },
        complete: function () {
            $("#patient_investigation_min_content").LoadingOverlay("hide");

        }
    });
}

function fetchInvStatus(toggle_status) {
    var url = base_url + "/nursing_new/fetchInvStatus";
    var nursing_station = $('#ns_station_list').val();
    var inv_status_from_date = $('#inv_status_from_date').val();
    var inv_status_to_date = $('#inv_status_to_date').val();
    var dataparams = {
        nursing_station: nursing_station,
        inv_status_from_date: inv_status_from_date,
        inv_status_to_date: inv_status_to_date,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            if (toggle_status == 0) {
                $('#modalInvIntendsStatus').modal('show');
            }
            $("#InvIntendsStatus").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#InvIntendsStatus").html(data);
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        complete: function () {
            $("#InvIntendsStatus").LoadingOverlay("hide");

        }
    });
}

function getLabResultRtf(lab_results_id) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    $.ajax({
        type: "GET",
        url: url,
        data: "lab_results_id=" + lab_results_id,
        beforeSend: function () {
            $('#lab_results_rtf_modal').modal('show');
            $('#lab_results_rtf_data').html('');
        },
        success: function (data) {
            // console.log(data);
            $('#lab_results_rtf_data').html(data);
        },
        complete: function () {}
    });
}

function ViewPatientHistory(obj, patient_id, visit_id, doctor_id) {
    var url = $('#base_url').val() + "/emr_lite/fetchPatientClinicalHistory";
    // var patient_id = $("#patient_id").val();
    // var doctor_id = $('#doctor_id').val();
    var header_name = '';
    if ($(obj).parent().attr('data-patient') != undefined) {
        var patient_name = ($(obj).parent().attr('data-patient')).trim();
        var patient_uhid = ($(obj).parent().attr('data-uhid')).trim();
        header_name = patient_name + ' - ' + patient_uhid;
        $('#patientClinicalHistoryModal .modal-title').html('History (' + header_name + ')');
    } else {
        $('#patientClinicalHistoryModal .modal-title').html('History');
    }
    var param = {
        patient_id: patient_id,
        doctor_id: doctor_id
    };
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#patientClinicalHistoryModal").modal('show');
                $("#patientClinicalHistoryModalBody").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.6)",
                    imageColor: '#337AB7'
                });

            },
            success: function (data) {
                if (data.status == 1) {
                    $("#patientClinicalHistoryModalBody").html(data.patient_combined_history);
                    // $("#patientClinicalHistoryModal").modal('show');
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }
            },
            complete: function () {

                $("#patientClinicalHistoryModalBody").LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.printAssessmentBtn').hide();

            }
        });
    }
}

function patient_occupi(bed_id, visit_id, e) {
    var url = $('#base_url').val() + "/nursing_new/patientOccupi";
    $.ajax({
        type: "GET",
        url: url,
        data: 'bed_id=' + bed_id + '&visit_id=' + visit_id,
        beforeSend: function () {
            $('.ns_main_patients_container').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Patient is admitted!");
                $(e).hide();
            }
            else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {
            $('.ns_main_patients_container').LoadingOverlay("hide");
        }
    });
}

function regOrRenNewPatient() {
    $('#reg_or_renew_patient_modal').modal('show');
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('#new_reg_department').select2();
    $('#new_reg_doctor_id').select2();
    $('#new_ren_department').select2();
    $('#new_ren_doctor_id').select2();

    $('#new_reg_department').val(2).change();
    $('#new_ren_department').val(2).change();
}

function ViewPatientInvestigationResult(patient_id, visit_id, doctor_id) {
    $('#poc_patient_id').val(patient_id);
    $('#poc_visit_id').val(visit_id);
    $('#poc_doctor_id').val(doctor_id);
    fetchPatientInvResultEntryNursing();
}


function backToRetain(visit_id, current_bed_id, user_id) {
    var url = $('#base_url').val() + "/nursing/backToRetain";
    $.ajax({
        url: url,
        type: "GET",
        data: "visit_id=" + visit_id +
            '&current_bed_id=' + current_bed_id +
            '&user_id=' + user_id,
        beforeSend: function () {
            $('#nursing_modal').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            var data1 = data[0].back_to_retain;
            data1 = JSON.parse(data1);
            if (data1.Result == 1) {
                Command: toastr["success"]("Patient sent back to retianed bed!");
                window.location.reload();
            } else if(data1.Result == 2){
                Command: toastr["error"]("Something went wrong");
            }

        },
        complete: function () {

        }
    });
}

function fetchPatientLabResults(patient_id, visit_id,encounter_id='') {
    var url = $('#base_url').val() + "/emr_lite/labResultTrends";
    $("#patient_id").val(patient_id);
    $("#visit_id").val(visit_id);
    $("#encounter_id").val(encounter_id);
    var from_date = $('#investigation_fromdate').val();
    var to_date = $('#investigation_todate').val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "GET",
            url: url,
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id + "&from_date=" + from_date + "&to_date=" + to_date,
            beforeSend: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-flask');
            },
            success: function (data) {
                // console.log(data);
                $("#patientLabResultsModal").modal('show');
                $('#lab_restuls_data').html(data);
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
                $("#lab_result_entries").removeClass('headergroupbg');
                $("#lab_result_entries").addClass('headergrouplite');

            },
            complete: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa-flask').removeClass('fa-spinner fa-spin');
            }
        });
    }
}
function fetchPatientLabResultsView(patient_id, visit_id,encounter_id='') {
    var url = $('#base_url').val() + "/emr_lite/labResultTrends";
    $("#patient_id").val(patient_id);
    $("#visit_id").val(visit_id);
    $("#encounter_id").val(encounter_id);
    var from_date = $('#investigation_fromdate').val();
    var to_date = $('#investigation_todate').val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "GET",
            url: url,
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id + "&from_date=" + from_date + "&to_date=" + to_date,
            beforeSend: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-flask');
                $(".theadscroll").perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30,
                });
                setTimeout(function () {
                    $(".theadfix_wrapper").floatThead({
                        position: "absolute",
                        scrollContainer: true,
                    });
                }, 400);
            },
            success: function (data) {
                $("#patientLabResults").modal('show');
                $('#showResultviewtab1').html(data);
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
                $("#lab_result_entries").removeClass('headergroupbg');
                $("#lab_result_entries").addClass('headergrouplite');

            },
            complete: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa-flask').removeClass('fa-spinner fa-spin');
                $(".theadscroll").perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30,
                });
                setTimeout(function () {
                    $(".theadfix_wrapper").floatThead({
                        position: "absolute",
                        scrollContainer: true,
                    });
                }, 400);
            }
        });
    }
}
$(document).on("click", ".tab_list li", function (event) {
    
    $('.remove_active').removeClass('active');
    $(this).addClass('active');
    $('.tab-content1').hide();
    var activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn();
    setTimeout(function () {
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    }, 500);
   
    return false;
   
});
function LabResultsTrendView(patient_id, visit_id) {
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
    fetchPatientLabResultsView(patient_id, visit_id);
    $('#lab_result_patient').val('');
    $('#lab_result_patient').val(patient_id);
    var current_date=$('#current_date').val();
    $('#lab_result_fromdate_t').val(current_date);
    $('#lab_result_todate_t').val(current_date);
    
    showResult();
}
$(document).on('show','#patientLabResults', function() {
    $('.remove_active').removeClass('active');
    $(".tab_list li:first").addClass('active');
   })
function LabResultsTrend(patient_id, visit_id) {
    fetchPatientLabResults(patient_id, visit_id);
}

function getLabResultRtf(lab_results_id) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    $.ajax({
        type: "GET",
        url: url,
        data: "lab_results_id=" + lab_results_id,
        beforeSend: function () {
            $('#lab_results_rtf_modal').modal('show');
            $('#lab_results_rtf_data').html('');
        },
        success: function (data) {
            // console.log(data);
            $('#lab_results_rtf_data').html(data);
        },
        complete: function () {}
    });
}


//-----------graph view--------------------------------------------------

$(document).on('click', '.lab_graph_view', function (e) {
    e.preventDefault();

    var patient_id = $('#lab_result_patient_id').val();
    if (patient_id > 0) {
        var url = $('#base_url').val() + "/emr/get-graph-view-lab";
        $.ajax({
            url: url,
            type: "GET",
            data: "patient_id=" + patient_id,
            beforeSend: function () {
                $("#modal_lab_result_trends").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $('#lab_result_trends_data').html(data);
            },
            complete: function () {

            }
        });
    }
});






function BedTranferRequest(patient_id, visit_id, patient_name, uhid, bed) {
    $('#bedTransferRequestModal').modal('show');
    var dataparams = {
        patient_id: patient_id,
        visit_id: visit_id,
    };
    var url = $('#base_url').val() + "/nursing_new/BedTranferRequest";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#bedTransferequestModal_data').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (response) {
            $('#bedTransferequestModal_data').html(response);
            $('#bed_tranfer_patient_id').val(patient_id);
            $('#bed_tranfer_visit_id').val(visit_id);
            $('#bed_transfer_patient_name').html(patient_name);
            $('#bed_transfer_uhid').html(uhid);
            $('#bed_transfer_current_bed').html(bed);
        },
        complete: function () {
            $('#bedTransferequestModal_data').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            $('#bed_trasnfer_room_type').select2();
            $('#bed_trasnfer_nursing_station').select2();
            selectRoomRequestHistory();
        }
    });
}

function selectRoomTypeUsingLocation() {
    var location_id = $('#bed_trasnfer_nursing_station').val();
    var active_tab_val = $('input[name="bed_transferRadio"]:checked').val();
    var dataparams = {
        location_id: location_id,
        active_tab_val : active_tab_val
    };
    var url = $('#base_url').val() + "/nursing_new/selectRoomTypeUsingLocation";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {

        },
        success: function (response) {
            $('#bed_trasnfer_room_type').html(response).select2();
        },
        complete: function () {

        }
    });
}

function saveBedTransferRequest() {
    var room_type = $("#bed_trasnfer_room_type").val() ? $("#bed_trasnfer_room_type").val() : 0;
    var nursing_station = $("#bed_trasnfer_nursing_station").val();
    var retain_status = 0;
    var visit_id = $("#bed_tranfer_visit_id").val();
    var patient_id = $("#bed_tranfer_patient_id").val();
    var current_nursing_station = $('#ns_station_list').val();
    if ($("#retain_status").is(':checked')) {
        retain_status = 1;
    }

    if (nursing_station != '') {
        var url = $('#base_url').val() + "/nursing_new/saveBedTransferRequest";
        $.ajax({
            type: "GET",
            url: url,
            data: 'room_type=' + room_type +
                '&retain_status=' + retain_status +
                '&visit_id=' + visit_id +
                '&nursing_station=' + nursing_station +
                '&patient_id=' + patient_id,
            beforeSend: function () {
                $('#bedTransferRequestModal').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == 1) {
                    Command: toastr["success"](obj.message);
                    if (nursing_station == current_nursing_station) {
                        $('#bedTransferRequestModal').modal("hide");
                        $('#bedTransferRequestModal').LoadingOverlay("hide");
                        selectRoomRequestBed(obj.request_id, patient_id, visit_id, nursing_station, retain_status);
                    }
                }
                else if (obj.status == 2) {
                    Command: toastr["warning"](obj.message);
                }
                else {
                    Command: toastr["error"](obj.message);
                    return;
                }
                selectRoomRequestHistory();

            },
            complete: function () {
                $('#bedTransferRequestModal').LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr["warning"]("Please select nursing station.");
    }
}

function selectRoomRequestHistory() {
    var visit_id = $("#bed_tranfer_visit_id").val();
    var url = $('#base_url').val() + "/nursing_new/selectRoomRequestHistory";
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () {},
        success: function (data) {
            $("#request_history").html(data);
        },
        complete: function () {}
    });
}

function deleteRoomTransferReq(req_id, visit_id) {
    var url = $('#base_url').val() + "/nursing_new/deleteRoomTransferReq";
    var param = {
        visit_id: visit_id,
        req_id: req_id
    };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {},
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Deleted Successfully!");
            }
            else {
                Command: toastr["error"]("Someting went wrong!");
            }

            // location.reload();
        },
        complete: function () {
            selectRoomRequestHistory();
        }
    });
}


function selectRoomRequestBed(id, patient_id, visit_id, nursing_station, retain_status) {
    var url = $('#base_url').val() + "/nursing_new/selectRoomRequestBed";
    // var nursing_station = $('#default_nursing_station').val();
    $.ajax({
        type: "GET",
        url: url,
        data: "request_id=" + id +
            "&nursing_station=" + nursing_station +
            "&patient_id=" + patient_id +
            "&visit_id=" + visit_id +
            "&retain_status=" + retain_status,
        beforeSend: function () {
            $('#bedTransferRequestModal').modal("hide");
            $('#room_transfer_request_choose_bed').LoadingOverlay("hide");
            $('#room_transfer_request_choose_bed_modal').modal('show');
            $('#room_transfer_request_choose_bed_modal').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#room_transfer_request_choose_bed_modal').LoadingOverlay("hide");
            $('#room_transfer_request_choose_bed').html(html);
            $('#room_request_request_id').val(id);
            $('#room_request_visit_id').val(visit_id);
            $('#room_request_patient_id').val(patient_id);
            $('#room_request_retain_status').val(retain_status);
            $('.bedtheadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
        },
        complete: function () {},
    });
}


function admitRoomRequest() {
    var url = $('#base_url').val() + "/nursing_new/admitRoomRequest";
    var request_id = $('#room_request_request_id').val();
    var visit_id = $('#room_request_visit_id').val();
    var patient_id = $('#room_request_patient_id').val();
    var room_type = $('#room_request_room_type').val();
    var retain_status = $('#room_request_retain_status').val();
    var nursing_station = $('#default_nursing_station').val();
    var bed_id = $('input[name="room_transfer_bed"]:checked').val();
    if (!bed_id) {
        Command: toastr["error"]("Please select any bed.");
        return;
    }
    $.ajax({
        type: "GET",
        url: url,
        data: "request_id=" + request_id +
            "&patient_id=" + patient_id +
            "&visit_id=" + visit_id +
            "&room_type=" + room_type +
            "&nursing_station=" + nursing_station +
            "&bed_id=" + bed_id +
            "&retain_status=" + retain_status,
        beforeSend: function () {
            $('#room_transfer_request_choose_bed_modal').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#room_transfer_request_choose_bed_modal').LoadingOverlay("hide");
            var obj = JSON.parse(html);
            if (obj.status == 1) {
                Command: toastr["success"](obj.message);
                $('#room_transfer_request_choose_bed_modal').modal('hide');
                //openRoomRequestList();
                var station_id = $('#ns_station_list').val();
                changeNursingStation(station_id);
            }
            else {
                Command: toastr["error"](obj.message);
            }
        },
        complete: function () {
            $('#room_transfer_request_choose_bed_modal').modal('hide');
            var station_id = $('#ns_station_list').val();
            changeNursingStation(station_id);
        },
    });
}

function fetchBedCountInformations() {
    // alert(total_bed_count);
    $('.bed_status_count').each(function (e) {
        var bed_status = $(this).attr('data-attr-bed-status');
        var bed_status_count = $('.room_label[' + 'data-attr-bed-status' + '="' + bed_status + '"]').length;
        var total_bed_count = $('.room_label').length;
        $("#bed_status_" + bed_status).html(bed_status_count);
        // $('#show_all_beds').html(total_bed_count);
    });
}

$(document).on("click", ".filter_beds", function () {
    var bed_status = $(this).find('.bed_status_count').attr('data-attr-bed-status');
    $('.ns_patient_container_main').hide();
    $('.ns_patient_container_main[' + 'data-attr-bed-container-bed-status' + '="' + bed_status + '"]').show();
});

$(document).on("click", ".show_all_beds_container", function () {
    $('.ns_patient_container_main').show();
});

function getPatientVisitList(patient_id) {
    var dataparams = {
        patient_id: patient_id
    };
    var url = $('#base_url').val() + "/nursing_new/getPatientVisitList";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
        },
        success: function (response) {
            var data =  JSON.parse(response);
            $('#indent_history_visit_id').html(data.option_list);
            $('#indent_history_visit_id').val(data.current_visit_id);
            fetchIndentHistory();
        },
        complete: function () {

        }
    });
}

function getPatientIntendHistory(patient_id){
    $('#modal_indend_history').modal('toggle');
    $('#indent_history_result').html('');
    $('#indent_history_patient_id').val(patient_id);

    var current_location = $('#ns_station_list').val();
    $('#indent_history_location').val(current_location).select();
    // fetchIndentHistory();
    getPatientVisitList(patient_id);
}

function fetchIndentHistory(){
    var url = $('#base_url').val() + "/nursing_new/fetchIndentHistory";
    var patient_id=$('#indent_history_patient_id').val();
    var from_date = $('#indent_history_from_date').val();
    var to_date = $('#indent_history_to_date').val();
    var indent_type = $('#indent_history_type').val();
    var location = $('#indent_history_location').val();
    var visit_id = $('#indent_history_visit_id').val();
    var dataparams = {
        patient_id:patient_id,
        from_date:from_date,
        to_date:to_date,
        indent_type:indent_type,
        location:location,
        visit_id:visit_id,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#indent_history_result').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (obj) {
            $('#indent_history_result').LoadingOverlay("hide");
            $('#indent_history_result').html(obj);
        },
        complete: function () {
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);
        }

    });
}
function toggle_indent_history_tr(type, head_id) {
    $('.' + type + '_' + head_id).toggleClass('hidden');
}
function changeRadioBedTransfer(obj) {
    var type =  $(obj).val();
    var visit_id = $('#bed_tranfer_visit_id').val();
    var dataparams = {
        type: type,
        visit_id: visit_id
    };
    var url = $('#base_url').val() + "/nursing_new/selectNursingLocation";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {

        },
        success: function (response) {
            $('#bed_trasnfer_nursing_station').html(response).select2();
        },
        complete: function () {

        }
    });
}

function printTrendData(obj, bill_detail_id, sample_wise_print=0) {
    var url = $('#base_url').val() + "/lab_result/dowmloadLabResult";
    var dataparams = {
        bill_detail_id:bill_detail_id,
        sample_wise_print:sample_wise_print,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $(obj).find('i').removeClass('fa-file-pdf-o').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (obj) {
            var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write(obj + '<script>setTimeout(function(){window.print();window.close();  },1000)</script>');
        },
        complete: function () {
            $(obj).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-file-pdf-o');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },

    });
}

function viewPendingIntendData(visit_id, type) {
    var url = base_url + "/nursing_new/getPatientPendingIntendList";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            visit_id: visit_id,
            type: type
        },
        beforeSend: function () {
            $("#patientPendingIntendCountModalDiv").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (type == 'inv') {
                $('#patientPendingIntendListModalHeader').html('Pending Investigation Intends');
            } else if (type == 'med') {
                $('#patientPendingIntendListModalHeader').html('Pending Pharmacy Intends');
            } else {
                $('#patientPendingIntendListModalHeader').html('Pending Procedure Intends');
            }

            if (parseInt(obj.status) == 1) {
                $('#patientPendingIntendListModalDiv').html(obj.data);
                $("#patientPendingIntendListModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $(".theadfix_wrapper").floatThead({
                        position: "absolute",
                        scrollContainer: true,
                    });
                }, 400);
            } else {
                toastr.warning("Something went wrong");
            }
        },
        complete: function () {
            $("#patientPendingIntendCountModalDiv").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error(
                "Error Please Check Your Internet Connection and Try Again"
            );
        },
    });
}

function toggle_indent_pending_list_tr(type, head_id) {
    $('.' + type + '_' + head_id).toggleClass('hidden');
}

$(document).on('click', '#lab_result_view', function (e){
    $('#lab_result_patient').val('');
    var patient_id=$(this).attr('data-patient_id');
    $('#lab_result_patient').val(patient_id);
    var patient_name=$(this).attr('data-patient_name');
    $('#report_patient_name').html(patient_name);
    $('#lab_result_view_modal').modal('toggle');
    var current_date=$('#current_date').val();
    $('#lab_result_fromdate_t').val(current_date);
    $('#lab_result_todate_t').val(current_date);
    showResult()
})
function showResult(){
    var patient_id=$('#lab_result_patient').val();
    var lab_result_fromdate=$('#lab_result_fromdate_t').val();
    var lab_result_todate=$('#lab_result_todate_t').val();
    var type=$('#report_type').val();
    var uploaded_date = 0;
    if ($("#based_on_uploaded_date").is(":checked")) {
        uploaded_date = 1;
    }
    $('#lab_result_view_content').html('');
    if(patient_id){
        $.ajax({
            type: "POST",
            url: base_url + "/nursing_new/getLabResultView",
            data: {
                patient_id: patient_id,
                lab_result_fromdate: lab_result_fromdate,
                lab_result_todate: lab_result_todate,
                type: type,uploaded_date : uploaded_date,
            },
            beforeSend: function () {
              $('#lab_result_view_content').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
            },
            success: function (data) {
                $('#showResultviewtab3').html(data);
            },
            complete: function () {
                $('#lab_result_view_content').LoadingOverlay("hide");
            },
            error: function () {

            },
        });
    }

    return;
}
function viewFile(filePath,fileName) {
    var filePath=atob(filePath);
    var filePath=base_url+filePath;
    console.log(filePath);
    // filePath='https://www.africau.edu/images/default/01112023021057_910220.pdf';
    // fileName='01112023021057_910220.pdf;
    const fileExtension = fileName.split('.').pop().toLowerCase();
    if (fileExtension === 'pdf') {
        window.open(filePath, '_blank');
    } else if (fileExtension === 'doc' || fileExtension === 'docx') {
        window.open('https://docs.google.com/gview?url=' + filePath + '&embedded=true', '_blank');
    } else if (['jpg', 'jpeg', 'png', 'gif', 'bmp', 'tiff', 'webp'].includes(fileExtension)) {
        window.open(filePath, '_blank');
    } else {
        toastr.warning('File type not supported for viewing:'+ fileName);
    }
}
function btn_private_notes(patient_id,visit_id,e){
    console.log(visit_id)
    $('#new_nursing_note_modal').modal('toggle');
    var url = $('#base_url').val() + "/nursing_new/selectDoctorNotes";
    var patient_name = $(e).attr('data-patient');
    var uhid = $(e).attr('data-uhid');
    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id + '&visit_id=' + visit_id,
        beforeSend: function () {
            $('#new_nursing_note_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $("#new_nursing_note_modal_body").html(data);
        },
        complete: function () {
            $('#new_nursing_note_modal_body').LoadingOverlay("hide");
            $('#new_nursing_note_modal_title').html(patient_name + '(' + uhid +')');
        }
    });
}
