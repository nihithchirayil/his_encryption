var pub_patient_id = 0;
var pub_visit_id = 0;
var header_name = '';
var is_casuality_user = $('#is_casuality_user').val();
var casuality_prescription_indent_type = $('#casuality_prescription_indent_type').val();
var item_allergyArray = [];
var generic_allergyArray = [];

function ViewPatientMedicationMin(obj, patient_id, visit_id, tab = 'medication', flag = 0) {
    $("#patient_id").val(patient_id);
    if ($(obj).parent().attr('data-patient') != undefined) {
        var patient_name = ($(obj).parent().attr('data-patient')).trim();
        var patient_uhid = ($(obj).parent().attr('data-uhid')).trim();
        header_name = patient_name + ' - ' + patient_uhid;
        $('#patient_medication_min .modal-title').html('<i class="fa fa fa-medkit"></i> Medication/Administration (' + header_name + ')');
    } else {
        $('#patient_medication_min .modal-title').html('<i class="fa fa fa-medkit"></i> Medication/Administration');
    }
    pub_patient_id = patient_id;
    pub_visit_id = visit_id;
    $("#patient_medication_min").modal({
        backdrop: 'static',
        keyboard: false
    });
    if (tab == 'medication')
        $('#patient_medication_min_tab a[href="#tab_medication"]').trigger('click');
    else
        $('#patient_medication_min_tab a[href="#tab_administration"]').trigger('click');

    if (flag == 1) {
        var url = base_url + "/nursing_new/ViewPatientMedicationMin"
        var dataparams = {
            patient_id: patient_id,
            visit_id: visit_id,
        };
        $.ajax({
            type: "GET",
            url: url,
            data: dataparams,
            beforeSend: function () {
                // $('#patient_medication_min').modal('toggle');
                $("#patient_medication_min_content").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                $("#patient_medication_min_content").html(data);
            },
            complete: function () {
                $("#patient_medication_min_content").LoadingOverlay("hide");
                getNewRowInserted('add');
                $('input[name="selected_item_name[]"]').focus();

                $('#close_btn_med_search').click(function () {
                    $(".medicine-list-div-row-listing").hide();
                });
            }
        });
    }
}



//load prescription view
function loadPescriptionView(head_id) {
    if (head_id != '' && head_id != undefined) {
        $('#med_det_list_modal1').modal('show');
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/prescription-details",
            data: {
                head_id: head_id,
                _token: _token
            },
            beforeSend: function () {
                $('#med_det_list_modal_content1').html('<tr><td colspan="8" class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></td></tr>');
            },
            success: function (data) {
                //console.log(data);
                let res = JSON.parse(data);
                let response = '';
                let head = '';
                let is_iv_fluid = res[0].is_iv_fluid;

                if (is_iv_fluid != undefined && is_iv_fluid == 1) {

                    head = '<tr class="table_header_bg">\n\
                                <th>#</th>\n\
                                <th>Medicine</th>\n\
                                <th>Generic Name</th>\n\
                                <th>Quantity</th>\n\
                                <th>Rate</th>\n\
                                <th>Instructions</th>\n\
                            </tr>';
                } else {
                    head = '<tr class="table_header_bg">\n\
                                <th>#</th>\n\
                                <th><input type="checkbox" name="check_all_prescription" class="check_all_prescription" value="1" /></th>\n\
                                <th>Medicine</th>\n\
                                <th>Generic Name</th>\n\
                                <th>Dose</th>\n\
                                <th>Frequency</th>\n\
                                <th>Days</th>\n\
                                <th>Quantity</th>\n\
                                <th>Route</th>\n\
                                <th>Start At</th>\n\
                                <th>Instructions</th>\n\
                            </tr>';
                }

                if (res.length > 0) {
                    for (var i = 0; i < res.length; i++) {

                        let id = res[i].id;
                        let billconverted_status = res[i].billconverted_status;
                        let duration = res[i].duration;
                        let frequency = res[i].frequency;
                        let generic_name = res[i].generic_name;
                        let item_desc = res[i].item_desc;
                        let medicine_name = res[i].medicine_name;
                        let medicine_code = res[i].medicine_code;
                        let quantity = res[i].quantity;
                        let stop_flag = res[i].stop_flag;
                        let notes = res[i].notes;
                        let dose = res[i].dose;
                        let route = res[i].route;
                        let iv_rate = res[i].iv_rate;
                        let product_status = res[i].product_status;
                        let iv_start_at = res[i].iv_start_at;
                        iv_start_at = iv_start_at ? iv_start_at : 'NA';
                        let sl = parseInt(i) + 1;

                        let m_name = "";
                        if (item_desc != "") {
                            m_name = item_desc;
                        } else {
                            m_name = medicine_name;
                        }

                        var medicine_inactive_color = '';
                        if (parseInt(product_status) == 0) {
                            medicine_inactive_color = ' style="background-color:#ef9b9b;" ';
                        }

                        if (is_iv_fluid != undefined && is_iv_fluid == 1) {

                            response += '<tr ' + medicine_inactive_color + '><td width="5%">' + sl + '</td><td>' + m_name + '</td><td>' + generic_name + '</td><td>' + quantity + '</td><td>' + iv_rate + '</td><td>' + notes + '</td></tr>';

                        } else {

                            response += '<tr ' + medicine_inactive_color + '><td width="5%">' + sl + '</td><td width="5%"><input type="checkbox" class="patient_det_med_check" name="patient_det_med_check" value="' + id + '" /></td><td>' + m_name + '</td><td>' + generic_name + '</td><td>' + dose + '</td><td>' + frequency + '</td><td>' + duration + '</td><td>' + quantity + '</td><td>' + route + '</td><td>' + iv_start_at + '</td><td>' + notes + '</td></tr>';
                        }
                    }
                } else {
                    if (is_iv_fluid != undefined && is_iv_fluid == 1) {
                        response = '<tr><td colspan="5" class="text-center">No Data Found..!</td></tr>';
                    } else {
                        response = '<tr><td colspan="7" class="text-center">No Data Found..!</td></tr>';
                    }
                }

                $('#med_det_list_modal_head1').html(head);
                $('#med_det_list_modal_content1').html(response);
            },
            complete: function () {

            }
        });
    }
}


// to refresh the modal after closing a child modal
$('.modal').on('hidden.bs.modal', function () {
    if($('.modal:visible').length > 0){
        $('body').addClass('modal-open');
    }
});

$(document).on('click', '#med_view_clos', function () {
    $("#med_det_list_modal1").modal('hide');

})
$(document).on("click", ".check_all_prescription", function () {
    if ($(".check_all_prescription").prop('checked')) {
        $('.patient_det_med_check').prop("checked", true);
    } else {
        $('.patient_det_med_check').prop("checked", false);
    }

})

$(document).on("click", ".fav_presc_select_checkbox", function () {
    if ($(".fav_presc_select_checkbox").prop('checked')) {
        $('.fav_check').prop("checked", true);
    } else {
        $('.fav_check').prop("checked", false);
    }
})

$(document).on("click", ".applyPatientMed", function () {
    $('.selected_medicine_row').hide();
    var patient_medication_detail_ids = [];
    $("input:checkbox[name=patient_det_med_check]:checked").each(function () {
        patient_medication_detail_ids.push($(this).val());
    });

    //console.log(patient_medication_detail_ids);

    applyPatientMed(patient_medication_detail_ids);

})

function applyPatientMed(detail_id) {
    var url = $('#base_url').val() + "/emr/prescription-details-by-detail-id";
    let _token = $('#c_token').val();
    var itemCodesArray = []; 
    $.ajax({
        type: "POST",
        url: url,
        data: {
            detail_id: detail_id,
            _token: _token,
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(89, 89, 89, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            let res = JSON.parse(data);
            let response = '';

            if (res.length > 0) {
                // $('#medicine-listing-table tbody').html("");
                for (var i = 0; i < res.length; i++) {

                    let id = res[i].id;
                    let head_id = res[i].head_id;
                    let billconverted_status = res[i].billconverted_status;
                    let duration = res[i].duration;
                    let frequency = res[i].frequency;
                    let frequency_value = res[i].frequency_value;
                    let frequency_id = res[i].frequency_id;
                    let generic_name = res[i].generic_name;
                    let item_desc = res[i].item_desc;
                    let medicine_name = res[i].medicine_name;
                    let medicine_code = res[i].medicine_code;
                    let quantity = res[i].quantity;
                    let stop_flag = res[i].stop_flag;
                    let notes = res[i].notes;
                    let dose = res[i].dose;
                    let route = res[i].route;
                    let price = res[i].price;
                    let iv_start_at = res[i].iv_start_at;
                    let stock = res[i].stock;
                    let generic_id = res[i].generic_id;                    
                    let sl = parseInt(i) + 1;
                    itemCodesArray.push(medicine_code);

                    let data = {
                        med_code: medicine_code,
                        out_medicine_name: medicine_name,
                        med_name: item_desc,
                        duration: duration,
                        dose: dose,
                        route: route,
                        quantity: quantity,
                        freq_value: frequency_value,
                        freq_name: frequency,
                        freq_id: frequency_id,
                        remarks: notes,
                        price: price,
                        id: id,
                        head_id: head_id,
                        iv_start_at: iv_start_at,
                        stock: stock,
                        billconverted_status: billconverted_status,
                        generic_name: generic_name,
                        generic_id: generic_id,
                    };

                    getNewRowInserted('copy', data);

                }
                let intend_type = res[0].intend_type;
                $('input[name="p_search_type"][value="' + intend_type + '"]').prop('checked', true);
                $("#prescription_head_id").val(0);

                $("#med_det_list_modal1").modal('hide');
                checkAllergicMedicine(itemCodesArray);
            }

        },
        complete: function () {
            $("body").LoadingOverlay("hide");

            $('#prescription_wrapper .nav-tabs li a[href="#drug"]').closest('li').addClass('active');
            $('#prescription_wrapper .nav-tabs li a[href="#consumables"]').closest('li').removeClass('active');
            $('#prescription_wrapper .tab-content #drug').addClass('active in');
            $('#prescription_wrapper .tab-content #consumables').removeClass('active in');

        }
    });

}

//print prescription view
function printPescriptionView(head_id) {
    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            async: false,
            url: url + "/emr/print-prescription-details",
            data: {
                head_id: head_id,
                _token: _token
            },
            beforeSend: function () {},
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(
                    '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
                );
                winPrint.document.write(
                    '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
                winPrint.document.write(
                    '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
                );
                winPrint.document.write(
                    '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
                );
                winPrint.document.write(
                    '<style>.table-striped>tbody>tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} diaganosis_section{font-size:9px !important;}</style>'
                );

                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');




            },
            complete: function () {

            }
        });
    }
}

//medicine search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_medicine"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var patient_id = $('#patient_id').val();
    var search_type = $("input[name='m_search_type']:checked").val();
    var med_list = $('.medicine-list-div');

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(med_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('#MedicationTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    //alert(data);
                    let response = data.medicine_list;
                    // window.iv_sub_cat_id = data.iv_sub_category;
                    let res_data = "";


                    var search_list = $('#ListMedicineSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let subcategory_id = response[i].subcategory_id;
                            let stock = response[i].stock;
                            let route = response[i].route;
                            let frequency = response[i].frequency;
                            let duration = response[i].duration;
                            let directions = response[i].directions;
                            let category_name = response[i].category_name;
                            let subcategory_name = response[i].subcategory_name;
                            var nostock = '';
                            if (!stock) {
                                nostock = 'no-stock'
                            }
                            res_data += '<tr data-genreric-name="' + generic_name + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '"><td class="' + nostock + '">' + item_desc + '</td><td>' + generic_name + '</td><td>' + category_name + '</td><td>' + subcategory_name + '</td><td>' + stock + '</td><td>' + price + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="6">No Data Found..!</td></tr>';
                    }

                    if (response.length > 0) {
                        $('input[name="search_medicine"]').removeClass('outside-medicine');
                    } else {
                        $('input[name="search_medicine"]').addClass('outside-medicine');
                        $(med_list).hide();
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".presc_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});


$(document).on('keydown', function (event) {
    var entry = 0;
    $('.medicine-list-div-row-listing').each(function (key, val) {
        if ($(val).css('display') != 'none') {
            entry = 1;
        }
    });

    if (entry == 0) {
        return;
    }
    var row_length = $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr').length;
    if (row_length > 0) {
        if (event.which == 38) { // up
            if ($(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':first-child')) {
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr').last().addClass('active_row_item');
            } else {
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').prev().addClass('active_row_item');
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').last().removeClass('active_row_item');
            }
        } else if (event.which == 40) { // down
            if ($(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':last-child')) {
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr').first().addClass('active_row_item');
            } else {
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').next().addClass('active_row_item');
                $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').first().removeClass('active_row_item');
            }
        } else if (event.which == 13) { // enter
            $(".medicine-list-div-row-listing:visible").find(".list-medicine-search-data-row-listing").find('tr.active_row_item').trigger('click');
        } else if (event.which == 8) { // backspace
            $('.medicine-list-div-row-listing').hide();
            $(window.tr).find('.item_description').focus();
        } else if (event.which == 27) { // escape
            $('.medicine-list-div-row-listing').hide();
            $(window.tr).find('.item_description').focus();
        }
    }
});



//outside medicine colour code
$(document).on('blur', 'input[name="search_medicine"]', function (event) {
    let search_item_code_hidden = $('input[name="search_item_code_hidden"]').val();
    let search_medicine = $('input[name="search_medicine"]').val();
    if (search_medicine != "" && search_medicine != undefined && search_item_code_hidden == "") {
        $('input[name="search_medicine"]').addClass('outside-medicine');
    } else {
        $('input[name="search_medicine"]').removeClass('outside-medicine');
    }
});

//close medicine search
// $(document).on('click', '.medicine-list-div > .close_btn_med_search', function (event) {
//     event.preventDefault();
//     /* Act on the event */
// });



//when select medicine
$(document).on('dblclick', '#ListMedicineSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let generic_name = $(this).attr('data-genreric-name');
    let name = $(tr).find('input[name="list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="list_med_code_hid[]"]').val();
    let price = $(tr).find('input[name="list_med_price_hid[]"]').val();
    let subcategory_id = $(tr).find('input[name="list_med_sub_cat_id[]"]').val();
    let route = $(this).attr('data-route');
    let frequency = $(this).attr('data-frequency');
    let duration = $(this).attr('data-duration');
    let directions = $(this).attr('data-directions');
    let stock = $(this).attr('data-stock');
    if (route) {
        $("#search_route option[data-route-id='" + route + "']").prop('selected', true);
    }
    if (frequency) {
        $(".freq_id_" + frequency).trigger('dblclick');
    }
    if (duration) {
        $("#search_duration").val(duration);
    }
    if (directions) {
        $("#search_instructions").val(directions);
    }
    if (!stock) {
        $('input[name="search_medicine"]').css("color", "#ef0a1a");
    } else {
        $('input[name="search_medicine"]').css("color", "unset");
    }

    if (duration && frequency) {
        $('#search_duration').trigger('blur');
    }

    // if(subcategory_id.toString() == window.iv_sub_cat_id.toString()){
    //     $(tr).closest('.medicine-list-div').parents('tr').find(".normalMedication").find('input,select').attr("disabled", true);
    //     // $(".ivMedication").find('input,select').attr("disabled", false);
    //     $("input[name='search_quantity']").focus();
    // } else {
    //     $(tr).closest('.medicine-list-div').parents('tr').find(".normalMedication").find('input,select').attr("disabled", false);
    //     // $(".ivMedication").find('input,select').attr("disabled", true);
    //     $("input[name='search_dose']").focus();
    // }
    $("input[name='search_dose']").focus();

    if (name != '' && code != '') {
        $('input[name="search_medicine"]').val(name + ((generic_name) ? "(" + generic_name + ")" : ""));
        $('input[name="search_item_code_hidden"]').val(code);
        $('input[name="search_item_name_hidden"]').val(name);
        $('input[name="search_item_price_hidden"]').val(price);
        $('input[name="search_subcategory_id"]').val(subcategory_id);

        $(".medicine-list-div").hide();


        if ($("#allegy_details").find('tr#' + code).length > 0) {
            $('input[name="search_medicine"]').css("background", '#f4c4a6');
            bootbox.alert('The patient is allergic to this medicine.');
        } else {
            if (generic_name != "") {
                var allegic = false;
                $("#allegy_details").find('h4').each(function (key, val) {
                    var allegic_item = $(val).text().toUpperCase();
                    if (allegic_item.indexOf(generic_name.toUpperCase()) !== -1) {
                        allegic = true;
                    }
                });

                if (allegic == true) {
                    $('input[name="search_medicine"]').css("background", '#f4c4a6');
                    bootbox.alert('The patient is allergic to this medicine.');
                } else {
                    $('input[name="search_medicine"]').css("background", '#ffff');
                }
            }


        }
    }



});

//duration
$('#search_duration,#search_quantity,#consumable_search_quantity').on('keyup', function () {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9]/g, ""));
    if (cur_obj_value.length > 3) {
        $(cur_obj).val(Number(cur_obj_value.slice(0, 3)));
    }
});

//duration
$(document).on('keyup', 'input[name="selected_item_duration[]"]', function () {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9]/g, ""));
    if (cur_obj_value.length > 3) {
        $(cur_obj).val(Number(cur_obj_value.slice(0, 3)));
    }
});

//quantity
$(document).on('keyup', 'input[name="selected_item_quantity[]"]', function () {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9.]/g, ""));
    if (cur_obj_value.length > 4) {
        $(cur_obj).val(Number(cur_obj_value.slice(0, 3)));
    }
    var enable_ward_stock = $("#enable_ward_stock").val();
    if (parseInt(enable_ward_stock) == 1) {
        let search_w_stock = $(this).closest("tr").find('input[name="ward_stock[]"]').attr('data_w_stock');
        if (parseInt(search_w_stock) != 0) {
            if (parseInt(search_w_stock) < parseInt(cur_obj_value)) {
                $(this).closest("tr").find('input[name="ward_stock[]"]').prop("disabled", true);
                $(this).closest("tr").find('input[name="ward_stock[]"]').prop("checked", false);
            } else {
                $(this).closest("tr").find('input[name="ward_stock[]"]').prop("disabled", false);
            }
        }
    }
});

$('#search_duration,#search_frequency').on('blur change', function () {
    let search_duration = $(this).closest('tr').find('input[name="search_duration"]').val();
    let search_freq_value_hidden = $(this).closest('tr').find('input[name="search_freq_value_hidden"]').val();
    if (search_duration != "" && search_duration != undefined && search_freq_value_hidden != "" && search_freq_value_hidden != undefined) {
        let qty = parseInt(search_duration) * parseInt(Math.ceil(search_freq_value_hidden));
        $(this).closest('tr').find('input[name="search_quantity"]').val(qty);
    } else {
        $(this).closest('tr').find('input[name="search_quantity"]').val(1);
    }
});

$(document).on('blur focus', 'input[name="selected_item_duration[]"]', function () {
    let search_duration = $(this).closest('tr').find('input[name="selected_item_duration[]"]').val();
    let search_freq_value_hidden = $(this).closest('tr').find('input[name="selected_frequency_value[]"]').val();
    let calculate_quantity = $(this).closest('tr').find('input[name="selected_calculate_quantity[]"]').val();
    let search_w_stock = $(this).closest("tr").find('input[name="ward_stock[]"]').attr('data_w_stock');
    var enable_ward_stock = $("#enable_ward_stock").val();
    if (search_duration != "" && search_duration != undefined && search_freq_value_hidden != "" && search_freq_value_hidden != undefined && parseInt(calculate_quantity) > 0) {
        let qty = parseInt(search_duration) * parseInt(Math.ceil(search_freq_value_hidden));
        if (parseInt(enable_ward_stock) == 1) {
            if (parseInt(search_w_stock) != 0) {
                if (parseInt(search_w_stock) < parseInt(qty)) {
                    $(this).closest("tr").find('input[name="ward_stock[]"]').prop("disabled", true);
                    $(this).closest("tr").find('input[name="ward_stock[]"]').prop("checked", false);
                } else {
                    $(this).closest("tr").find('input[name="ward_stock[]"]').prop("disabled", false);
                }
            }
        }
        $(this).closest('tr').find('input[name="selected_item_quantity[]"]').val(qty);
    } else {
        $(this).closest('tr').find('input[name="selected_item_quantity[]"]').val(1);
    }

});

$(document).ready(function () {

    $('#medicine-listing-table tbody').sortable({
        stop: function (event, ui) {
            resetMedicineSerialNumbers();
        }
    });

    // fetchFrequencyList();

    $("#doctor_prescription").select2();
    $("#search_frequency").on("focus", function () {
        $(".frequency-list-div").show();
    });
    $("#search_duration, #search_medicine, #search_dose, #search_quantity, #search_route, #search_instructions").on("focus", function () {
        $(".frequency-list-div").hide();
    });

    $(document).on("focus", "input[name='selected_item_frequency[]']", function () {
        $(this).parents('tr').find('.frequency-list-div-row-listing').show()
    });

    $(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_name[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_remarks[]']", function () {
        $(this).parents('tr').find('.frequency-list-div-row-listing').hide();
    });

    $(document).on("focus", "input[name='selected_item_remarks[]']", function (event) {
        $(this).parents('tr').find('.instruction-list-div-row-listing').show();
        // $(event).keyup().change();
        if ($(event.currentTarget).parents('tr').find(".list-instruction-search-data-row-listing").find('tr').length == 0) {
            var esc = $.Event("keyup", {
                keyCode: 27
            });
            $(event.currentTarget).parents('tr').find("input[name='selected_item_remarks[]']").trigger(esc);
        }

    });

    $(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_name[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_frequency[]']", function () {
        $(this).parents('tr').find('.instruction-list-div-row-listing').hide();
    });

    $(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_frequency[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_remarks[]']", function () {
        $(this).parents('tr').find('.medicine-list-div-row-listing').hide();
        calculateTotalMedicineAmount();
    });

    $(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_frequency[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_name[]']", function () {
        $(this).parents('tr').find('.instruction-list-div-row-listing').hide();
    });


    $('#frequencypopup').on('hidden.bs.modal', function (e) {
        fetchFrequencyList();
    })


    setTimeout(function () {

        $('input[name="iv_selected_start_at[]"]').datetimepicker({
            format: 'DD-MMM-YYYY'
        })
    }, 1000);

    // if(sessionStorage.getItem('presc-data-form')){
    //     $('#medicine-listing-table tbody').html(sessionStorage.getItem('presc-data-form'));
    // }


    // setInterval(function(){
    //     var medication_table = '';
    //     $('#medicine-listing-table tbody > tr').each(function(key, val){
    //         if($(val).find('input[name="selected_item_name[]"]').val() && $(val).find('input[name="selected_item_name[]"]').val() != ""){
    //             medication_table = medication_table + val.outerHTML;
    //         }
    //     })
    //     if (medication_table != '') {
    //         sessionStorage.setItem('presc-data-form', medication_table);
    //     } else {
    //         sessionStorage.setItem('presc-data-form', "");
    //     }

    // }, 3000);

});


function fetchFrequencyList() {
    var url = $('#base_url').val() + "/emr/fetchFrequencyList";
    $.ajax({
        type: "GET",
        url: url,
        data: {

        },
        success: function (data) {
            var res_data = '';

            if (data.length > 0) {
                $("#ListFrequencySearchData").empty();
                $.each(data, function (key, val) {
                    // $(".search_frequency_select").append('<option value="'+val.id+'" data-frequency-value="'+val.frequency_value+'" >'+val.frequency+'</option>');
                    let frequency = val.frequency;
                    let frequency_value = val.frequency_value;
                    let frequency_id = val.id;

                    res_data += '<tr data-frequency-name="' + frequency + '" class="freq_id_' + frequency_id + '"><td>' + frequency + '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' + key + '" value="' + frequency_value + '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' + key + '" value="' + frequency + '"><input type="hidden" name="list_freq_id_hid[]" id="list_freq_id_hid-' + key + '" value="' + frequency_id + '"></tr>';
                });

            } else {
                res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
            }

            $("#ListFrequencySearchData").append(res_data);
            $(".list-frequency-search-data-row-listing").html($("#ListFrequencySearchData").html());

        },
        complete: function () {

            setTimeout(function () {
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 1000);



        }
    });
}

$(document).on("change", ".search_frequency_select", function (e) {
    let name = $('.search_frequency_select option:selected').text();
    let value = $('.search_frequency_select option:selected').attr('data-frequency-value');
    let id = $('.search_frequency_select option:selected').val();

    if (name != '' && value != '') {
        $('input[name="search_freq_value_hidden"]').val(value);
        $('input[name="search_freq_name_hidden"]').val(name);
        $('input[name="search_freq_id_hidden"]').val(id);

        $('input[name="search_duration"]').focus();
    }
})

//frequency search
var freq_timeout = null;
var freq_last_search_string = '';
// $('#search_frequency').on('keyup', function () {
//     event.preventDefault();
//     /* Act on the event */
//     var obj = $(this);

//     searchFrequency(obj);
// });

// $('#search_frequency').on('focus', function () {
//     event.preventDefault();
//     /* Act on the event */
//     var obj = $(this);

//     searchFrequency(obj,1);
// });



function searchFrequency(obj, all = 0) {
    var search_freq_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var freq_list = $('.frequency-list-div');
    if ((search_freq_string == "" || search_freq_string.length < 2) && all == 0) {
        freq_last_search_string = '';
        return false;
    } else {
        $(freq_list).show();
        clearTimeout(freq_timeout);
        freq_timeout = setTimeout(function () {
            if (search_freq_string == freq_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/frequency-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_freq_string: search_freq_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#FrequencyTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    let response = data;
                    let res_data = "";


                    var freq_search_list = $('#ListFrequencySearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let frequency = response[i].frequency;
                            let frequency_value = response[i].frequency_value;
                            let frequency_id = response[i].id;

                            res_data += '<tr><td>' + frequency + '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' + i + '" value="' + frequency_value + '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' + i + '" value="' + frequency + '"><input type="hidden" name="list_freq_id_hid[]" id="list_freq_id_hid-' + i + '" value="' + frequency_id + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(freq_search_list).html(res_data);
                    freq_last_search_string = search_freq_string;
                    $(".freq_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.freq_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}

//close frequency search
$(document).on('click', '.close_btn_freq_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".frequency-list-div").hide();
});

//when select frequency
$(document).on('dblclick', '#ListFrequencySearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
    let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();
    let id = $(tr).find('input[name="list_freq_id_hid[]"]').val();

    if (name != '' && value != '') {
        $('input[name="search_frequency"]').val(name);
        $('input[name="search_freq_value_hidden"]').val(value);
        $('input[name="search_freq_name_hidden"]').val(name);
        $('input[name="search_freq_id_hidden"]').val(id);

        $(".frequency-list-div").hide();
        $('input[name="search_duration"]').focus();
    }

});

function getFormattedTime(time) {
    var timeSplit = time.split(':'),
        hours,
        minutes,
        meridian;
    hours = timeSplit[0];
    minutes = timeSplit[1];
    if (hours > 12) {
        meridian = 'PM';
        hours -= 12;
    } else if (hours < 12) {
        meridian = 'AM';
        if (hours == 0) {
            hours = 12;
        }
    } else {
        meridian = 'PM';
    }
    return hours + ':' + minutes + ' ' + meridian;
}

//add medicine
function addNewMedicine() {
    // let status = validateRow();
    // if (status == false) {
    //     console.log("Error");
    //     return false;
    // }
    let mode = 'add';

    getNewRowInserted(mode);

    $('input[name="search_medicine"]').focus();
    $('input[name="search_medicine"]').css("background", 'initial');
}



$(document).on('click', '#med_clos', function () {
    $(".medicine-list-div-row-listing").hide();
})
//add row

function getNewRowInserted(mode = 'add', data = {}) {
    var enable_ward_stock = $("#enable_ward_stock").val();
    let code = "";
    if (mode == 'add') {
        code = $('input[name="search_item_code_hidden"]').val();
    } else {
        if (Object.keys(data).length > 0) {
            code = data.med_code;
        }
    }
    if (code != "") {}
    var administration_in_nurse_dashboard = $('#administration_in_nurse_dashboard').val();

    var table = document.getElementById("medicine-listing-table");
    let row_count = table.rows.length;
    let row_id = 0;

    var row = table.insertRow(row_count);

    row.setAttribute("row-id", row_id, 0);
    row.classList.add("selected_medicine_row");

    if (administration_in_nurse_dashboard == '1') {
        var cell10 = row.insertCell(0);
        cell10.width = '5%';
        cell10.classList.add("med_serial_no");

        var cell1 = row.insertCell(1);
        cell1.width = '22%';

        var cell12 = row.insertCell(2);
        cell12.width = '18%';

        var cell13 = row.insertCell(3);
        cell13.width = '7%';

        var cell4 = row.insertCell(4);
        cell4.width = '10%';

        var cell5 = row.insertCell(5);
        cell5.width = '5%';

        var cell6 = row.insertCell(6);
        cell6.width = '10%';

        var cell7 = row.insertCell(7);
        cell7.width = '13%';

        // var cell8 = row.insertCell(8);
        // cell8.classList.add("text-center");
        // cell8.width = '5%';
        if(enable_ward_stock == 1){
            var cell15 = row.insertCell(8);
            cell15.width = '5%';
            var cell8 = row.insertCell(9);
            cell8.classList.add("text-center");
            cell8.width = '5%';
        }else{
            var cell8 = row.insertCell(8);
            cell8.classList.add("text-center");
            cell8.width = "10%";
        }

    } else {
        var cell10 = row.insertCell(0);
        cell10.width = '5%';
        cell10.classList.add("med_serial_no");

        var cell1 = row.insertCell(1);
        cell1.width = '30%';

        var cell12 = row.insertCell(2);
        cell12.width = '25%';

        var cell6 = row.insertCell(3);
        cell6.width = '10%';

        var cell7 = row.insertCell(4);
        cell7.width = '20%';

        // var cell8 = row.insertCell(5);
        // cell8.classList.add("text-center");
        // cell8.width = '10%';

        if(enable_ward_stock == 1){
            var cell15 = row.insertCell(5);
            cell15.width = '4%';
            var cell8 = row.insertCell(6);
            cell8.classList.add("text-center");
            cell8.width = '1%';
        }else{
            var cell8 = row.insertCell(5);
            cell8.classList.add("text-center");
            cell8.width = "5%";
        }
    }



    // var cell2 = row.insertCell(4);
    // cell2.width = '7%';

    // var cell3 = row.insertCell(5);
    // cell3.width = '10%';

    // var cell4 = row.insertCell(6);
    // cell4.width = '5%';


    // var cell6 = row.insertCell(9);
    // cell6.width = '10%';



    // var cell9 = row.insertCell(8);
    // cell9.width = '10%';



    // var cell11 = row.insertCell(1);
    // cell11.width = '3%';



    let med_code = "";
    let med_name = "";
    let duration = "";
    let dose = "";
    let quantity = "";
    let route = "";
    let freq_value = "";
    let freq_name = "";
    let freq_id = 0;
    let remarks = "";
    let id = "";
    let price = 0;
    let iv_started_at = '';
    let medicine_color = '';
    let bill_editable_status = '';
    let subcategory_id = 0;
    let generic_name = '';
    let calculate_quantity = 0;
    let data_w_stock = 0;
    let ward_checked = '';
    let w_stock_disabled_flag = '';
    let data_generic_id = 0;

    if (mode == 'add') {

        med_code = $('input[name="search_item_code_hidden"]').val() ? $('input[name="search_item_code_hidden"]').val() : '';

        if (med_code == "") {
            med_name = $('input[name="search_medicine"]').val() ? $('input[name="search_medicine"]').val() : '';
            if (med_name != "") {
                cell1.classList.add("outside-medicine");
                $('input[name="search_medicine"]').removeClass('outside-medicine');
            } else {
                cell1.classList.remove("outside-medicine");
            }
        } else {
            med_name = $('input[name="search_item_name_hidden"]').val() ? $('input[name="search_item_name_hidden"]').val() : '';
            cell1.classList.remove("outside-medicine");
        }

        dose = $('input[name="search_dose"]').val() ? $('input[name="search_dose"]').val() : '';
        duration = $('input[name="search_duration"]').val() ? $('input[name="search_duration"]').val() : '';
        quantity = $('input[name="search_quantity"]').val() ? $('input[name="search_quantity"]').val() : '';

        freq_value = $('input[name="search_freq_value_hidden"]').val() ? $('input[name="search_freq_value_hidden"]').val() : '';
        freq_name = $('input[name="search_freq_name_hidden"]').val() ? $('input[name="search_freq_name_hidden"]').val() : '';
        freq_id = $('input[name="search_freq_id_hidden"]').val() ? $('input[name="search_freq_id_hidden"]').val() : '';


        route = $('select[name="search_route"]').val() ? $('select[name="search_route"]').val() : '';
        remarks = $('input[name="search_instructions"]').val() ? $('input[name="search_instructions"]').val() : '';

        price = $('input[name="search_item_price_hidden"]').val() ? $('input[name="search_item_price_hidden"]').val() : 0;
        if ($('input[name="iv_start_at"]').val()) {
            var iv_time = getFormattedTime($("#iv_timepicker").val());
            iv_started_at = $('input[name="iv_start_at"]').val() + ' ' + iv_time;
        }

        // }

        medicine_color = $('input[name="search_medicine"]').css("color") ? $('input[name="search_medicine"]').css("color") : '';
        subcategory_id = $('input[name="search_subcategory_id"]').val() ? $('input[name="search_subcategory_id"]').val() : 0;
        calculate_quantity = 0;


    } else {
        if (Object.keys(data).length > 0) {
            med_code = data.med_code;
            med_name = data.med_name;
            out_medicine_name = data.out_medicine_name;
            duration = data.duration;
            quantity = data.quantity;
            dose = data.dose;
            price = data.price;
            freq_value = data.freq_value;
            freq_name = data.freq_name;
            freq_id = data.freq_id;
            route = data.route;
            remarks = data.remarks;
            id = data.id;
            head_id = data.head_id;
            stock = data.stock;
            billconverted_status = data.billconverted_status;
            subcategory_id = data.subcategory_id;
            generic_name = data.generic_name ? data.generic_name : '';
            calculate_quantity = data.quantity ? data.quantity : 0;
            data_w_stock = data.w_stock ? data.w_stock : 0;
            data_generic_id = data.generic_id ? data.generic_id : 0;
            w_stock_disabled_flag = '';
            ward_checked = '';

            if (data.is_ward_stock == 1 ) {
                ward_checked = 'checked';
            }

            if (parseInt(data_w_stock) == 0) {
                w_stock_disabled_flag ='disabled';
                if (mode != "edit") {
                    ward_checked = '';
                }
            }


            if (!stock) {
                medicine_color = "#ef0a1a";
            }
            iv_started_at = data.iv_start_at ? moment(data.iv_start_at).format("DD-MMM-YYYY hh:mm A") : '';
            if (mode == 'copy') {
                id = "";
                head_id = "";
            } else {
                if (billconverted_status) {
                    bill_editable_status = "disabled='disabled'";
                }
            }

            if (med_code == "") {
                if (out_medicine_name != "") {
                    med_name = out_medicine_name;
                    cell1.classList.add("outside-medicine");
                } else {
                    cell1.classList.remove("outside-medicine");
                }
            } else {
                cell1.classList.remove("outside-medicine");
            }

            if (head_id != '' && head_id != 0) {
                $('#prescription_head_id').val(head_id);
            }
        }
    }
    
    let medicine_popup_search = '<div class="medicine-list-div-row-listing" style="display: none;">' +
        '<button type="button" class="med_clos" data-dismiss=""  id="med_clos" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '<div class=" presc_theadscroll" style="position: relative;"><table id="MedicationTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><thead><tr class="light_purple_bg" style="background: green !important; color: white !important;"><th>Medicine</th><th>Generic Name</th><th>Therapeutic Category</th><th>Therapeutic Sub Category</th><th>Stock</th><th>Price</th></tr></thead><tbody id="ListMedicineSearchDataRowListing-' + row_id + '" class="list-medicine-search-data-row-listing" ></tbody></table></div></div>';

    let current_freq_list = $("#ListFrequencySearchData").html();
    let medicine_freq_popup_search = '<div class="frequency-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_freq_search">X</a><div class=" freq_theadscroll" style="position: relative;"><table id="FrequencyTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><tbody id="ListFrequencySearchDataRowListing-' + row_id + '" class="list-frequency-search-data-row-listing FrequencyTableRowItem" >' + current_freq_list + '</tbody></table></div></div>';

    let medicine_instruction_popup_search = '<div class="instruction-list-div-row-listing" style="position: absolute;display: none;z-index: 99;"><a style="float: left;" class="close_btn_instruction_search">X</a><div class=" instruction_theadscroll" style="position: relative;"><table id="InstructionTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><tbody id="ListInstructionSearchDataRowListing-' + row_id + '" class="list-instruction-search-data-row-listing" ></tbody></table></div></div>';

    let route_options = "";
    let sel_search_route = document.getElementById('search_route');
    // route_options = sel_search_route.innerHTML;
    let color = '';
    if ($('input[name="search_medicine"]').css("background")) {
        color = "style='background:" + $('input[name="search_medicine"]').css("background") + "'";
    }

    if (medicine_color) {
        medicine_color = " style='color:" + medicine_color + "' ";
    }

    if (duration) {
        duration = "value='" + duration + "'";
    }
    if (quantity) {
        quantity = "value='" + quantity + "'";
    }

    cell1.innerHTML = "<span " + color + " class='med_name' width='50%'>" + med_name + "</span> <input type='hidden' name='selected_item_code[]' value='" + med_code + "'><input " + bill_editable_status + " type='hidden' class='form-control' name='selected_item_name[]' value='" + med_name + "' autocomplete='off' " + medicine_color + " data-generic-name='" + generic_name + "' item_code='" + med_code + "' data-generic-id='" + data_generic_id + "'><input type='hidden' name='selected_edit_id[]' value='" + id + "'><input type='hidden' name='selected_item_price[]' value='" + price + "'>" + medicine_popup_search;

    cell12.innerHTML = "<input type='text' disabled class='generic_name_input form-control' value=" + generic_name + " >";

    cell7.innerHTML = "<span class='remarks' title='" + remarks + "'>" + remarks + "</span> <input type='hidden' class='form-control' name='selected_item_remarks[]' value='" + remarks + "' autocomplete='off' " + bill_editable_status + ">" + medicine_instruction_popup_search;

    if (bill_editable_status == "") {
        cell8.innerHTML = "<button class='btn_sm delete_row del-presc-list-row'><i class='fa fa-trash'></i></button>";
    }

    cell6.innerHTML = "<span class='quantity'>" + quantity + "</span> <input type='hidden' class='form-control' name='selected_item_quantity[]' " + quantity + " " + bill_editable_status + " autocomplete='off'>";

    cell10.innerHTML = $("#medicine-listing-table tbody>tr").length;

    if(enable_ward_stock == 1){
        if (parseInt(calculate_quantity) > parseInt(data_w_stock)) {
            w_stock_disabled_flag = 'disabled';
        }
        if (mode == "edit" && data.is_ward_stock == 1) {
            w_stock_disabled_flag = '';
        }
        cell15.innerHTML = "<input type='checkbox' name='ward_stock[]'"+ ward_checked +" "+ w_stock_disabled_flag +" data_w_stock="+ data_w_stock +">";
    }

    if (administration_in_nurse_dashboard == '1') {
        cell13.innerHTML = "<span class='dose'>" + dose + "</span> <input " + bill_editable_status + " type='hidden' class='form-control' name='selected_item_dose[]' value='" + dose + "' autocomplete='off'> <input " + bill_editable_status + " type='hidden' class='form-control' name='selected_calculate_quantity[]' value='" + calculate_quantity + "' autocomplete='off' " + medicine_color + " >";
        cell13.classList.add('normalMedication');

        cell4.innerHTML = "<span class='frequency'>" + freq_name + "</span> <input type='hidden' class='form-control' name='selected_item_frequency[]' value='" + freq_name + "' autocomplete='off' " + bill_editable_status + "><input type='hidden' name='selected_frequency_value[]' value='" + freq_value + "'><input type='hidden' name='selected_frequency_id[]' value='" + freq_id + "'>" + medicine_freq_popup_search;
        cell4.classList.add('normalMedication');


        cell5.innerHTML = "<span class='duration'>" + duration + "</span> <input type='hidden' class='form-control' name='selected_item_duration[]' " + duration + " " + bill_editable_status + " autocomplete='off' >";
        cell5.classList.add('normalMedication');

    }



    // cell6.innerHTML = "<span class='route'>" + route + "</span> <select class='form-control' style='display: none;' name='selected_item_route[]' " + bill_editable_status + " >" + route_options + "</select>";
    // cell6.classList.add('normalMedication');



    // cell9.innerHTML = "<span class='iv_selected_start_at'>" + iv_started_at + "</span> <input type='hidden' class='form-control' name='iv_selected_start_at[]' value='" + iv_started_at + "' " + bill_editable_status + " autocomplete='off'>";



    // cell11.innerHTML = '<div class="fav_medicine" style="border: 1px solid #d6d6d6;padding: 1px 4px; cursor:pointer;"><i class="fa fa-star"></i></div>';



    // $(cell6).find('select').val(route);

    if (mode == 'add') {
        clearmedfields();
    }
    $(row).trigger('dblclick');
    calculateTotalMedicineAmount();
}
























//clear fields
function clearmedfields() {
    $('input[name="search_item_code_hidden"]').val('');
    $('input[name="search_item_name_hidden"]').val('');
    $('input[name="search_medicine"]').val('');
    $('input[name="search_item_price_hidden"]').val('');
    $('input[name="search_subcategory_id"]').val('');
    $('input[name="search_medicine"]').css("color", "unset");

    $('input[name="search_duration"]').val('');
    $('input[name="search_dose"]').val('');
    $('select[name="search_route"]').val('');
    $('input[name="search_quantity"]').val('');

    $('input[name="search_freq_value_hidden"]').val('');
    $('input[name="search_freq_name_hidden"]').val('');
    $('input[name="search_freq_id_hidden"]').val('');
    $('input[name="search_frequency"]').val('');

    $('input[name="search_instructions"]').val('');
    $('.search_frequency_select').val('');

    $('.medicine_search_head').find(".normalMedication").find('input,select').attr("disabled", false);
    // $(".ivMedication").find('input,select').attr("disabled", true);
    $("#iv_start_date").val('');
}

//validate top row
function validateRow(event) {

    event = $(event).parents('tr');
    let err = 0;

    // if($('input[name="search_subcategory_id"]').val() != window.iv_sub_cat_id){
    //     let itm_duration = $('input[name="search_duration"]').val();
    //     if (itm_duration == '' || itm_duration == undefined || itm_duration == 0) {
    //         Command: toastr["error"]("Enter Days.");
    //         $('#search_duration').focus();
    //         err = 1;
    //     }

    //     let itm_freq_val = $('input[name="search_freq_value_hidden"]').val();
    //     let itm_freq_name = $('input[name="search_freq_name_hidden"]').val();
    //     if (itm_freq_val == '' || itm_freq_val == undefined || itm_freq_name == '' || itm_freq_name == undefined) {
    //         Command: toastr["error"]("Enter Frequency.");
    //         $('#search_frequency').focus();
    //         err = 1;
    //     }
    // }

    let itm_name = $(event).find('input[name="selected_item_name[]"]').val();
    // let itm_code = $('input[name="search_item_code_hidden"]').val();
    if (itm_name == '' || itm_name == undefined) {
        Command: toastr["error"]("Enter Medicine.");
        $(event).find('input[name="selected_item_name[]"]').focus();
        err = 1;
    }



    let itm_quantity = $(event).find('input[name="selected_item_quantity[]"]').val();
    if (itm_quantity == '' || itm_quantity == undefined || itm_quantity == 0) {
        Command: toastr["error"]("Enter Quantity.");
        $(event).find('input[name="selected_item_quantity[]"]').focus();
        err = 1;
    }

    let duration = $(event).find('input[name="selected_item_duration[]"]').val();
    if (duration == '' || duration == undefined || duration == 0) {
        Command: toastr["error"]("Enter Days.");
        $(event).find('input[name="selected_item_duration[]"]').focus();
        err = 1;
    }

    let itm_freq_val = $(event).find('input[name="selected_frequency_value[]"]').val();
    let itm_freq_name = $(event).find('input[name="selected_item_frequency[]"]').val();
    if (itm_freq_val == '' || itm_freq_val == undefined || itm_freq_name == '' || itm_freq_name == undefined) {
        Command: toastr["error"]("Enter Frequency.");
        $(event).find('input[name="selected_item_frequency[]"]').focus();
        err = 1;
    }



    //same item already exists
    // let alredy_exist = $('input[name="selected_item_code[]"]').filter(function (ind, obj) {
    //     if ($(obj).val() == itm_code && itm_code != "") {
    //         return itm_code;
    //     }
    // });

    // if (alredy_exist.length > 0) {
    //     if($('input[name="iv_start_at"]').val()){
    //         var iv_time = getFormattedTime($("#iv_timepicker").val());
    //         iv_started_at = $('input[name="iv_start_at"]').val()+ ' '+ iv_time;
    //         iv_started_at = moment(iv_started_at).format("YYYY-MM-DD");

    //         var current_item_start_at = $("#medicine-listing-table").find('input[value="'+alredy_exist[0].value+'"]').parents('tr').first().find('.iv_selected_start_at').text();
    //         if(current_item_start_at != ""){
    //             current_item_start_at = moment(current_item_start_at).format("YYYY-MM-DD");
    //             if(moment(iv_started_at).diff(moment(current_item_start_at)) <= 0 ){
    //                 Command: toastr["error"]("Medicine already exist.");
    //                 err = 1;
    //             }
    //         }

    //     } else {
    //         Command: toastr["error"]("Medicine already exist.");
    //         err = 1;
    //     }

    // }

    if (err == 1) {
        return false;
    } else {
        return true;
    }
}


//check any changes occur in listing
var medicine_listing_table_changed = 0;
var medicine_saved_after_edit = 0;
var consumable_medicine_listing_table_changed = 0;
var consumable_medicine_saved_after_edit = 0;

var prescription_callback = function (mutationsList, observer) {
    for (var mutation of mutationsList) {
        if (mutation.type == 'childList') {
            // console.log('childList');
            medicine_listing_table_changed = 1;
        } else if (mutation.type == 'attributes') {
            // console.log('attributes');
            medicine_listing_table_changed = 1;
        }
    }
};

var consumable_callback = function (mutationsList, observer) {
    for (var mutation of mutationsList) {
        if (mutation.type == 'childList') {
            consumable_medicine_listing_table_changed = 1;
        } else if (mutation.type == 'attributes') {
            consumable_medicine_listing_table_changed = 1;
        }
    }
};

// // Create an observer instance linked to the callback function
// var prescriptionObserver = new MutationObserver(prescription_callback);
// var consumableObserver = new MutationObserver(consumable_callback);

// // Select the node that will be observed for mutations
// var targetNode = document.getElementById('medicine-listing-table');
// var targetNode2 = document.getElementById('consumable-medicine-listing-table');

// // Options for the observer (which mutations to observe)
// var config = { attributes: true, childList: true, subtree: true };

// // Start observing the target node for configured mutations
// prescriptionObserver.observe(targetNode, config);
// consumableObserver.observe(targetNode2, config);


//trigger when click outside
// window.addEventListener('click', function (e) {
//     var nursing_station = $('#nursing_station').val();
//     if (document.getElementById('prescription_wrapper').contains(e.target)) {
//         // Clicked in prescription_wrapper
//     } else {
//         // Clicked outside the prescription_wrapper
//         if (nursing_station == '') {

//             if (medicine_listing_table_changed == 1 && medicine_saved_after_edit == 0) {
//                 saveDoctorPrescriptions(1);
//             }
//             if (consumable_medicine_listing_table_changed == 1 && consumable_medicine_saved_after_edit == 0) {
//                 saveDoctorConsumablePrescriptions();
//             }
//         }

//     }
// });

var save_starts = 0;
//save prescription
function saveDoctorPrescriptions(save_status) {

    var enable_ward_stock = $("#enable_ward_stock").val();
    let validate = false;
    if ($('.selected_medicine_row').length == 1) {
        validate = validatePrescription();
    } else {
        // $('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]:not([value!=""])').parents('tr').remove();
        $('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]').each(function (key, val) {
            if ($(val).val() == '') {
                $(val).parents('tr').remove();
            }
        })
        validate = validatePrescription();
    }

    if (validate) {

        $("#save_and_print_prescription").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');

        let patient_id = $('#pres_patient_id').val();
        var visit_id = $('#pres_visit_id').val();
        let encounter_id = $('#pres_encounter_id').val();
        let admitting_doctor_id = $('#doctor_prescription').val();
        let prescription_head_id = $('#prescription_head_id').val();
        var p_type = $("input[name='p_search_type']:checked").val();
        var presc_form = [];
        var administration_in_nurse_dashboard = $('#administration_in_nurse_dashboard').val();

        $(".selected_medicine_row").each(function (key, value) {
            var presc_row = {};
            presc_row.selected_item_code = $(value).find('input[name="selected_item_code[]"]').val();
            presc_row.selected_item_name = $(value).find('input[name="selected_item_name[]"]').val();
            presc_row.selected_item_generic_name = $(value).find('input[name="selected_item_name[]"]').attr('data-generic-name');
            presc_row.selected_edit_id = $(value).find('input[name="selected_edit_id[]"]').val();
            if (administration_in_nurse_dashboard == '1') {
                presc_row.selected_item_duration = $(value).find('input[name="selected_item_duration[]"]').val();
                presc_row.selected_item_frequency = $(value).find('input[name="selected_item_frequency[]"]').val();
                presc_row.selected_frequency_value = $(value).find('input[name="selected_frequency_value[]"]').val();
                presc_row.selected_frequency_id = $(value).find('input[name="selected_frequency_id[]"]').val();
                presc_row.selected_item_dose = $(value).find('input[name="selected_item_dose[]"]').val();
            } else {
                presc_row.selected_item_duration = '';
                presc_row.selected_item_frequency = '';
                presc_row.selected_frequency_value = 1;
                presc_row.selected_frequency_id = '';
                presc_row.selected_item_dose = '';
            }
            presc_row.selected_item_route = '';
            presc_row.selected_item_remarks = $(value).find('input[name="selected_item_remarks[]"]').val();
            presc_row.selected_item_quantity = $(value).find('input[name="selected_item_quantity[]"]').val();
            presc_row.selected_item_start_at = '';

            if(enable_ward_stock==1){
                presc_row.selected_is_ward = $(value).find('input[name="ward_stock[]"]').is(':checked')?1:0;
            }else{
                presc_row.selected_is_ward = 0;
            }


            presc_form.push(presc_row);

        });

        // var presc_form = JSONH.pack(presc_form)
        var presc_form1 = JSON.stringify(presc_form);
        presc_form = escape(presc_form1);


        var summary_prescription = $('#summary_prescription').val();
        var prescription_nursing_station = 0;
        if ($('#prescription_nursing_station')) {
            prescription_nursing_station = $('#prescription_nursing_station').val();
        }


        // if(($('#medicine-listing-table').find('tr').length) > 0){
        //     if( $('#medicine-listing-table tbody > tr:not("[class*=iv_fluid]")').find('input[name="selected_item_name[]"]:not([value=""])').length != $('#medicine-listing-table tbody > tr:not("[class*=iv_fluid]")').find('input[name="selected_item_frequency[]"]:not([value=""])').length){
        //         Command: toastr["error"]("Please select frequency.");
        //         return;
        //     }
        // }


        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/emr/save-prescription-new";

        if (save_status == 1) {
            summary_prescription = 1;
        }

        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: "prescription_form=" + presc_form + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&prescription_head_id=' + prescription_head_id + '&intend_type=' + p_type + '&admitting_doctor_id=' + admitting_doctor_id + '&summary_prescription=' + summary_prescription + '&prescription_nursing_station=' + prescription_nursing_station,
            beforeSend: function () {
                $('#save_and_print_prescription1').attr('disabled', true);
                // $('#save_and_print_prescription1').find('i').removeClass('fa-save');
                // $('#save_and_print_prescription1').find('i').addClass('fa-spinner fa-spin');
                $('body').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });


                $('.save_btn_bg').prop({
                    disable: 'true'
                });
            },
            success: function (data) {

                $('#save_and_print_prescription1').attr('disabled', false);
                // $('#save_and_print_prescription1').find('i').removeClass('fa-spinner fa-spin');
                // $('#save_and_print_prescription1').find('i').addClass('fa-save');

                $("#save_and_print_prescription").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');

                $('patient_medication_min_content').LoadingOverlay("hide");

                if (data.Result == 1) {
                    window.atleastOneSavedRecords = true;
                    Command: toastr["success"]("Saved.");

                    if (summary_prescription == 1) {
                        fetchMedicationTosummary(data.prescription_head_id);
                    }
                    $('#medicine-listing-table tr').remove();

                }

                // updateSeenstatus(visit_id);

                if (window.save_encounter_status && window.save_encounter_status == "TRUE") {
                    window.save_encounter_status = "FALSE";
                    var investigation_head_id_c = $("#investigation_head_id").val();
                    var url = $('#base_url').val() + "/emr/save-doctors-encounter-datas";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&investigation_head_id=' + investigation_head_id_c,
                        beforeSend: function () {

                        },
                        success: function () {
                            $('.draft_mode_indication_p').empty()
                        },
                        complete: function () {

                        }
                    });

                } else {
                    $('.draft_mode_indication_p').html('<span class="bg-orange draft-badge"> Draft Mode <i class="fa fa-exclamation-circle" title="Clinical Notes saved as draft" aria-hidden="true"></i></span>');
                }

                if (save_status == 2) {
                    $("#prescription_head_id").val('');
                    // printPescriptionView(data.prescription_head_id);
                    if ($("#prescription_investigation_same_print_config").val() == 1) {
                        $("#prescription_print_config_modal").attr("head_id", data.prescription_head_id);
                        $("#prescription_print_config_modal").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else {
                        printSavedPrescription(data.print_data, presc_form1);
                    }


                }
                $('body').LoadingOverlay("hide");

            },
            complete: function () {
                medicine_listing_table_changed = 0;
                $("#save_and_print_prescription").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                $('.save_btn_bg').prop({
                    disable: 'false'
                });
                $('#prescription_changes_draft_mode').val(0);
                // sessionStorage.setItem('presc-data-form', "");
                addNewMedicine();



            }
        });


    } else {
        medicine_listing_table_changed = 0;
        $('.save_btn_bg').prop({
            disable: 'false'
        });
        return "FALSE";
    }


}

function printPatientPrescriptionWithInvestigation() {
    var print_investigation = ($("#print_investigation_check").prop('checked')) ? 1 : 0;
    var print_prescription = ($("#print_prescription_check").prop('checked')) ? 1 : 0;
    var print_assessment = ($("#print_assessment_check").prop('checked')) ? 1 : 0;
    var prescription_id = $("#prescription_print_config_modal").attr("head_id") ? $("#prescription_print_config_modal").attr("head_id") : '';
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/printPatientPrescriptionWithInvestigation";

    if (print_investigation == 0 && print_assessment == 0 && print_prescription == 1 && prescription_id != "") {
        printPescriptionView(prescription_id);
    } else if (print_investigation == 1 || print_assessment == 1 || print_prescription == 1) {
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: {
                _token: _token,
                patient_id: patient_id,
                visit_id: visit_id,
                prescription_id: prescription_id,
                print_investigation: print_investigation,
                print_prescription: print_prescription,
                print_assessment: print_assessment
            },
            beforeSend: function () {
                $('.save_btn_bg').prop({
                    disable: 'true'
                });
                $('#print_prescription_and_investigation_btn').find('i').LoadingOverlay("show", {
                    background: "rgba(89, 89, 89, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

                $("#prescription_print_config_modal").attr("head_id", '');
                $("#prescription_print_config_modal").modal('hide');
                $("#print_investigation_check").prop('checked', false);
            },
            complete: function () {
                $('.save_btn_bg').prop({
                    disable: 'false'
                });
                $('#print_prescription_and_investigation_btn').find('i').LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr['warning']('Please select atleast one print mode.!');
    }

}

// function printPatientPrescriptionWithInvestigation(){
//     var print_investigation = ($("#print_investigation_check").prop('checked'))? 1 : 0;
//     var print_prescription = ($("#print_prescription_check").prop('checked'))? 1 : 0;
//     var print_assessment = ($("#print_assessment_check").prop('checked'))? 1 : 0;
//     var prescription_id = $("#prescription_print_config_modal").attr("head_id");
//     var patient_id = $("#patient_id").val();
//     let _token = $('#c_token').val();
//     var url = $('#base_url').val() + "/emr/printPatientPrescriptionWithInvestigation";

//     if(print_investigation == 1 && print_prescription == 1){

//         $.ajax({
//             type: "POST",
//             async: false,
//             url: url,
//             data: "patient_id=" + patient_id + '&_token=' + _token + "&prescription_id=" + prescription_id,
//             beforeSend: function () {
//                 $('.save_btn_bg').prop({ disable: 'true' });
//                 $('#print_prescription_and_investigation_btn').find('i').LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
//             },
//             success: function (data) {
//                 let res = JSON.parse(data);
//                 var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
//                 winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

//                 $("#prescription_print_config_modal").attr("head_id", '');
//                 $("#prescription_print_config_modal").modal('hide');
//                 $("#print_investigation_check").prop('checked', false);
//             },
//             complete: function () {
//                 $('.save_btn_bg').prop({ disable: 'false' });
//                 $('#print_prescription_and_investigation_btn').find('i').LoadingOverlay("hide");
//             }
//         });

//     } else if(print_investigation == 0 && print_prescription ==1 ){
//         printPescriptionView(prescription_id);
//     } else if(print_investigation == 1 && print_prescription ==0){
//         $.ajax({
//             type: "POST",
//             async: false,
//             url: url,
//             data: "patient_id=" + patient_id + '&_token=' + _token + "&prescription_id=",
//             beforeSend: function () {
//                 $('.save_btn_bg').prop({ disable: 'true' });
//                 $('#print_prescription_and_investigation_btn').find('i').LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
//             },
//             success: function (data) {
//                 let res = JSON.parse(data);
//                 var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
//                 winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

//                 $("#prescription_print_config_modal").attr("head_id", '');
//                 $("#prescription_print_config_modal").modal('hide');
//             },
//             complete: function () {
//                 $('.save_btn_bg').prop({ disable: 'false' });
//                 $('#print_prescription_and_investigation_btn').find('i').LoadingOverlay("hide");
//             }
//         });
//     } else {
//         Command: toastr['warning']('Please select atleast one print mode.!');
//     }

// }

function printSavedPrescription(print_data, presc_form1) {
    var date = moment().format('MMM-DD-YYYY');
    var hospital_header = print_data.hospital_header;
    var medication_note = print_data.medication_note;
    var department_head = print_data.department_head;
    var patient_name = print_data.patient_details.patient_name;
    var patient_age = print_data.patient_details.age;
    var patient_gender = print_data.patient_details.gender ? print_data.patient_details.gender : '';
    var uhid = print_data.patient_details.uhid;
    var intend_type = getIntendType($("input[name='p_search_type']:checked").val());
    var review_on = $("#next_review_date").val();
    var medication_footer_text = print_data.medication_footer_text;
    var doctor_name = print_data.doctor_details.doctor_name ? print_data.doctor_details.doctor_name : '';
    var qualification = print_data.doctor_details.qualification ? print_data.doctor_details.qualification : '';
    var doctor_speciality = print_data.doctor_details.doctor_speciality ? print_data.doctor_details.doctor_speciality : '';
    var medicine_list = '';
    presc_form1 = JSON.parse(presc_form1);

    $.each(presc_form1, function (key, val) {
        var i = key + 1;
        var generic_name = val.selected_item_generic_name ? val.selected_item_generic_name : '';
        var selected_item_name = val.selected_item_name ? val.selected_item_name : '';
        var selected_item_dose = val.selected_item_dose ? val.selected_item_dose : '';
        var selected_item_frequency = val.selected_item_frequency ? val.selected_item_frequency : '';
        var selected_item_route = val.selected_item_route ? val.selected_item_route : '';
        var selected_item_duration = val.selected_item_duration ? val.selected_item_duration : '';
        var selected_item_quantity = val.selected_item_quantity ? val.selected_item_quantity : '';
        var selected_item_remarks = val.selected_item_remarks ? val.selected_item_remarks : '';
        var selected_item_start_at = val.selected_item_start_at ? val.selected_item_start_at : '';

        medicine_list = medicine_list + '<tr><td>' + i + '</td><td width="47%"><div>' + selected_item_name + '</div><div> Generic : ' + generic_name + '</div></td><td><center>' + selected_item_dose + '</center></td><td><center>' + selected_item_frequency + '</center> </td><td><center>' + selected_item_route + '</center> </td><td><center> ' + selected_item_duration + '</center> </td><td><center> ' + selected_item_quantity + '</center> </td><td width="17%"><center>' + selected_item_start_at + '</center></td><td><center>' + selected_item_remarks + '</center></td></tr>';
    });

    var reason_for_visit = '';
    if ($("#visit_reason").val()) {
        reason_for_visit = '<strong style="vertical-align:top; float: left;">Reason For Visit</strong><br><br>' + $("#visit_reason").val() + '<br>';
    }

    var special_notes = '';
    if ($("#pres_notes").val()) {
        special_notes = '<strong style="vertical-align:top; float: left;">Special Notes</strong><br><br>' + $("#pres_notes").val() + '<br>';
    }

    var diagnosis = '';
    if ($("#pres_history").val()) {
        diagnosis = '<strong> Diagnosis : </strong> <br>' + $("#pres_history").val();
    }

    if (review_on && review_on != "") {
        review_on = '<strong style="font-size: 11px;">REVIEW ON: </strong> &nbsp;' + review_on;
    }

    var print_html = '<div class="col-md-12" >' + hospital_header + '  <table border="1" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px;"><tbody>  <tr style="border-bottom: none"><td style="padding: 0px; vertical-align: middle; border-bottom: none" width="15%" align="center" colspan="2"><h3>' + department_head + '</h3></td></tr><tr><td style="padding: 0px; border-top: none; font-size: 11px;" width="15%" align="left" colspan="2"><table width="100%"><tbody><tr><td><strong>NAME:</strong></td><td>' + patient_name + '</td><td><strong>UHID:</strong></td><td>' + uhid + '</td></tr><tr><td><strong> GENDER/AGE:</strong></td><td>' + patient_gender + '/' + patient_age + '</td><td><strong>DATE :</strong></td><td>' + date + '</td></tr><tr><td colspan="5" style="font-size:11px;">' + diagnosis + '</td></tr></tbody></table></td></tr><tr><td width="20%" valign="top" style="padding: 20px 5px 5px 5px; font-size: 11px;"><span>' + medication_note + '</span></td><td width="80%" valign="top" style="padding: 20px 5px 5px 5px;"><span style="float:right">' + intend_type + '</span><br><br><table border="1" cellspacing="0" cellpadding="2" width="100%" style="border-collapse: collapse; font-size: 13px;"><thead><tr><th width="1%"><strong>SL#</strong></th><th width="47%"><strong>Drug/Generic Name</strong></th><th width="4%"><strong>Dose</strong></th><th width="8%"><strong>Frequency</strong></th><th width="8%"><strong>Route</strong></th><th width="5%"><strong>Days</strong></th><th width="5%"><strong>Qty</strong></th><th width="17%" ><strong>Start At</strong></th><th><strong>Remarks</strong></th></tr></thead><tbody>' + medicine_list + '</tbody></table><br><table width="100%" style="font-size:12px;"><tbody><tr><td width="49%">' + reason_for_visit + '</td><td width="49%">' + special_notes + '</td></tr></tbody></table><table width="100%" style="font-size:12px;"><tr><td width="49%"></td><tdwidth="49%"></td></tr> </table><table width="100%" style="font-size:12px;"><tr><td width="49%">' + review_on + '</td><tdwidth="49%"><span style="float: right; margin-right: 10px; font-size: 11px;">' + doctor_name + '<br/> ' + qualification + ' (' + doctor_speciality + ') <br/><br/><strong>SIGNATURE:</strong></span></td></tr></table><br><br></td></tr>' + medication_footer_text + '</tbody></table></div>';

    var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
    winPrint.document.write(print_html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
    // winPrint.document.write(print_html);

}


function getIntendType(intend_type_id) {
    switch (intend_type_id) {
        case "1":
            return 'Regular';
        case "2":
            return 'New Admission';
        case "3":
            return 'Emergency';
        case "4":
            return 'Discharge';
        case "5":
            return 'Own Medicine';
        default:
            return '';
    }

}

// var save_starts = 0;
// //save prescription
// function saveDoctorPrescriptions(save_status) {
//         let search_medicine = $("input[name=search_medicine]").val();
//         if(search_medicine != ''){
//             addNewMedicine();
//         }

//         let validate = validatePrescription();
//         let _token = $('#c_token').val();

//          $('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]:not([value!=""])').parents('tr').remove();

//         if (save_starts == 1) {
//             return false;
//         }

//         let patient_id = $('#patient_id').val();
//         var visit_id = $('#visit_id').val();
//         //alert(visit_id);return;

//         let encounter_id = $('#encounter_id').val();
//         let admitting_doctor_id = $('#doctor_prescription').val();
//         let prescription_head_id = $('#prescription_head_id').val();
//         let presc_form = $('#presc-data-form').serialize();
//         var p_type = $("input[name='p_search_type']:checked").val();

//         var summary_prescription = $('#summary_prescription').val();
//         var prescription_nursing_station = 0;
//          if($('#prescription_nursing_station')){
//             prescription_nursing_station = $('#prescription_nursing_station').val();
//         }
//         //console.log(presc_form);return;



//         if(($('#medicine-listing-table').find('tr').length) > 0){
//             var prescription_frequncy_check = false;
//             // $('input[name="selected_item_frequency[]"]').each(function(key, val){
//             //     if($(val).val() == ''){
//             //         prescription_frequncy_check = true;
//             //     }
//             // });

//             // if(prescription_frequncy_check == true){
//             //     Command: toastr["error"]("Please select frequency.");
//             //     return;

//             // }

//                 if( $('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]:not([value=""])').length != $('#medicine-listing-table tbody > tr').find('input[name="selected_item_frequency[]"]:not([value=""])').length){
//                 Command: toastr["error"]("Please select frequency.");
//                 return;

//             }
//         }

//         if (validate) {
//             var url = $('#base_url').val() + "/emr/save-prescription";
//             save_starts = 1;
//             $('.save_btn_bg').prop({ disable: 'true' });
//             $.ajax({
//                 type: "POST",
//                 async: false,
//                 url: url,
//                 data: presc_form + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&prescription_head_id=' + prescription_head_id + '&intend_type=' + p_type + '&admitting_doctor_id='+admitting_doctor_id + '&summary_prescription='+summary_prescription +'&prescription_nursing_station='+prescription_nursing_station,
//                 beforeSend: function () {

//                 },
//                 success: function (data) {
//                     //console.log(data);
//                     if (data.status == 1) {
//                         window.atleastOneSavedRecords = true;
//                         //Command: toastr["success"]("Save.");
//                         prescriptionEditMode(data.head_id, 'edit');
//                         medicine_saved_after_edit = 1;
//                         $('.draft_mode_indication_p').html('<span class="bg-orange draft-badge"> Draft Mode <i class="fa fa-exclamation-circle" title="Medicine saved as draft" aria-hidden="true"></i></span>');

//                         if(summary_prescription  == 1){
//                             fetchMedicationTosummary(data.head_id);
//                         }
//                         $('#medicine-listing-table tr').remove();

//                     }
//                     save_starts = 0;
//                     $('#prescription_changes_draft_mode').val(0);

//                     if(save_status == 2){
//                         $("#prescription_head_id").val('');
//                         printPescriptionView(data.head_id);
//                     }
//                     var visit_id = $('#visit_id').val();
//                     updateSeenstatus(visit_id);

//                 },
//                 complete: function () {
//                     medicine_listing_table_changed = 0;
//                     $('.save_btn_bg').prop({ disable: 'false' });
//                     //


//                 }
//             });

//         } else {
//             medicine_listing_table_changed = 0;
//             $('.save_btn_bg').prop({ disable: 'false' });
//         }




// }

function fetchMedicationTosummary(head_id) {
    var url = $('#base_url').val() + "/emr/fetchMedicationTosummary";
    $.ajax({
        type: "GET",
        url: url,
        data: '&head_id=' + head_id,
        beforeSend: function () {

        },
        success: function (data) {
            var innerHtml = data;
            tinyMCE.activeEditor.execCommand('mceInsertRawHTML', false, innerHtml);
        },
        complete: function () {

        }
    });
}

//validate row
function validatePrescription() {
    //check rows exist
    // if ($('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]:not([value=""])').length == 0) {
    //     Command: toastr["error"]("Select Medicine.");
    //     $('input[name="selected_item_name[]"]')[0].focus();
    //     return false;
    // }
    var medicine_check = [];
    $('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]').each(function (key, val) {
        if ($(val).val() != '') {
            medicine_check.push(val);
        }
    });

    if (medicine_check.length == 0) {
        Command: toastr["error"]("Select Medicine.");
        $('input[name="selected_item_name[]"]')[0].focus();
        return false;
    }



    var valid_quantity = [];
    $('input[name="selected_item_quantity[]"]').each(function (key, val) {
        if ($(val).val() != '' && $(val).val() != 0) {
            valid_quantity.push(val);
        }
    })

    if (medicine_check.length != valid_quantity.length) {
        Command: toastr["error"]("Please select quantity.");

        $('input[name="selected_item_quantity[]"]').each(function (key, val) {
            if ($(val).val() == '' || $(val).val() == 0) {
                $(val).focus();
                return false
            }
        })
        return false;
    }

    return true;
}

$(document).on('click', '.del-presc-list-row', function () {
    let tr = $(this).closest('tr');

    if ($(tr).find('input[name="selected_item_name[]"]').val() != "") {

        // if (confirm("Are you sure you want to delete.!")) {
        let edit_id = $(tr).find('input[name="selected_edit_id[]"]').val();
        if (edit_id != '' && edit_id != 0) {
            deleteRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
        if ($("#medicine-listing-table tbody").find('tr').length == 0) {
            $('#prescription_head_id').val('');
        }
        calculateTotalMedicineAmount();
        // }
    } else {

        $(tr).remove();
        if ($("#medicine-listing-table tbody").find('tr').length == 0) {
            $('#prescription_head_id').val('');
        }
        calculateTotalMedicineAmount();
    }

});

//delete single row from db
function deleteRowFromDb(id) {
    var url = $('#base_url').val() + "/emr/prescription-delete-item";
    var patient_id = $('#patient_id').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            patient_id: patient_id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}

$(document).on('click', '#prescription-history-data-list .page-link', function (e) {
    e.preventDefault();
    if ($(e.target).parent().hasClass('active') == false) {
        loadPrescriptionHistory($(this).attr('href').split('page=')[1], 0);
    }
});

//load history
function loadPrescriptionHistory(page, history_clicked) {

    var display_status = $("#prescriptionCollapse").css('display');

    if (display_status == "block" && history_clicked) {
        $(".visit_wise_filter_presc_div").hide();
        $("#visit_wise_filter_presc").val("ALL");
        return;
    }

    $(".visit_wise_filter_presc_div").show();

    var url = $('#base_url').val() + "/emr/prescription-history";
    var patient_id = $('#pres_patient_id').val();
    var visit_type = $('#visit_wise_filter_presc').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            prescription_history: 1,
            page: page,
            patient_id: patient_id,
            visit_type: visit_type,
            _token: _token,
        },
        beforeSend: function () {
            $('#prescription-history-data-list').empty();
            $(".presc_history_btn").find('i').removeClass('fa-history').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#prescription-history-data-list').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }
            $(".popoverData").popover({
                trigger: "hover",
                html: true
            });

        },
        complete: function () {
            $(".presc_history_btn").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-history');
        }
    });
}



$('#prescription-history-data-list').on('click', '.pagination a', function (e) {
    loadPrescriptionHistory($(this).attr('href').split('page=')[1]);
    $(".popoverData").popover({
        trigger: "hover",
        html: true
    });
    e.preventDefault();
});

$(document).on('click', '#med_his_clos', function () {
    $("#complete_medication_history_modal1").modal('hide');

})
//load history
function loadCompletePrescriptionHistory(page_no) {
    $("#complete_medication_history_modal1").modal('show');
    var patient_id = $('#pres_patient_id').val();
    var visit_id = $('#pres_visit_id').val();
    var search_key = $('#serchCompletePrescription').val();
    console.log(search_key)
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/completePrescriptionHistory";
    $.ajax({
        type: "GET",
        url: url,
        data: "&patient_id=" + patient_id + '&visit_id=' + visit_id + "&complete_prescription_history=1&page=" + page_no + '&search_key=' + search_key,
        beforeSend: function () {
            $("#complete_medication_history_data1").LoadingOverlay("show", {
                background: "rgba(89, 89, 89, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            // console.log(data);
            if (data != '' && data != undefined && data != 0) {
                $('#complete_medication_history_data1').html(data);

            }
        },
        complete: function () {
            $("#complete_medication_history_data1").LoadingOverlay("hide");
        }
    });
}

// function serachInCompleteHistory(){
//     table = $('#lab_result_table').DataTable( {
//         paging: false
//     } );
//     table.destroy();
//     loadLabData();
// }


$('#complete_medication_history_data').on('click', '.pagination a', function (e) {
    loadCompletePrescriptionHistory($(this).attr('href').split('page=')[1]);
    e.preventDefault();
});


//prescription pagination
$(document).on('click', '.prescription_pagination > .pagination li a', function (event) {
    event.preventDefault();
    $('li').removeClass('active');
    $(this).parent('li').addClass('active');
    var page = $(this).attr('href').split('page=')[1];
    loadPrescriptionHistory(page);
});

/*************************************** EDIT ROW *********************************************/

//edit when double click prescription
$(document).on('dblclick', '.selected_medicine_row:has(td)', function function_name(event) {
    let obj = $(this);
    prescriptionRowEdit(obj);
});

//set row editable
function prescriptionRowEdit(obj) {
    //medicine column editable

    if ($(obj).find('input[name="selected_item_name[]"]').attr("type") != "text") {
        $(obj).find('input[name="selected_item_name[]"]').attr('type', 'text');
        $(obj).find('span[class="med_name"]').hide();

        // if($(obj).find('span[class="iv_selected_start_at"]').html().trim() == ''){

        //dose column editable
        $(obj).find('input[name="selected_item_dose[]"]').attr('type', 'text');
        $(obj).find('span[class="dose"]').hide();

        //duration column editable
        $(obj).find('input[name="selected_item_duration[]"]').attr('type', 'text');
        $(obj).find('span[class="duration"]').hide();

        //frequency column editable
        $(obj).find('input[name="selected_item_frequency[]"]').attr('type', 'text');
        $(obj).find('span[class="frequency"]').hide();

        //route column editable
        $(obj).find('select[name="selected_item_route[]"]').show();
        $(obj).find('span[class="route"]').hide();

        // }

        //iv start_at column editable
        // if($(obj).find('span[class="iv_selected_start_at"]').html().trim() != ''){
        $(obj).find('input[name="iv_selected_start_at[]"]').attr('type', 'text');
        $(obj).find('span[class="iv_selected_start_at"]').hide();
        $(obj).find('input[name="iv_selected_start_at[]"]').datetimepicker({
            format: "DD-MMM-YYYY hh:mm A"
        });
        $(obj).find('input[name="iv_selected_start_at[]"]').val($(obj).find('span[class="iv_selected_start_at"]').text());
        // }

        //quantity column editable
        $(obj).find('input[name="selected_item_quantity[]"]').attr('type', 'text');
        $(obj).find('span[class="quantity"]').hide();




        //remarks column editable
        $(obj).find('input[name="selected_item_remarks[]"]').attr('type', 'text');
        $(obj).find('span[class="remarks"]').hide();

        medicine_saved_after_edit = 0;

    }


}


//load prescription different modes based on head_id
function prescriptionEditMode(head_id, mode = 'edit') {
    if (head_id != '' && mode != '') {
        var url = $('#base_url').val() + "/emr/prescription-details";
        let _token = $('#c_token').val();
        var itemCodesArray = []; 
        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: {
                head_id: head_id,
                _token: _token,
            },
            beforeSend: function () {
                $("body").LoadingOverlay("show", {
                    background: "rgba(89, 89, 89, 0.6)",
                    imageColor: '#337AB7'
                });
                $('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]:not([value!=""])').parents('tr').remove();
            },
            success: function (data) {
                let res = JSON.parse(data);
                let response = '';

                if (res.length > 0) {
                    if (mode == 'edit' || ($('#prescription_head_id').val() != '' && $('#prescription_head_id').val() != '0')) {
                        $('#medicine-listing-table tbody').html("");
                    }
                    for (var i = 0; i < res.length; i++) {

                        let id = res[i].id;
                        let billconverted_status = res[i].billconverted_status;
                        let duration = res[i].duration;
                        let frequency = res[i].frequency;
                        let frequency_value = res[i].frequency_value;
                        let frequency_id = res[i].frequency_id;
                        let generic_name = res[i].generic_name;
                        let item_desc = res[i].item_desc;
                        let medicine_name = res[i].medicine_name;
                        let medicine_code = res[i].medicine_code;
                        let quantity = res[i].quantity;
                        let stop_flag = res[i].stop_flag;
                        let notes = res[i].notes;
                        let dose = res[i].dose;
                        let route = res[i].route;
                        let price = res[i].price;
                        let iv_start_at = res[i].iv_start_at;
                        let stock = res[i].stock;
                        let subcategory_id = res[i].subcategory_id;
                        let generic_id = res[i].generic_id;
                        let sl = parseInt(i) + 1;
                        itemCodesArray.push(medicine_code);

                        let data = {
                            med_code: medicine_code,
                            out_medicine_name: medicine_name,
                            med_name: item_desc,
                            duration: duration,
                            dose: dose,
                            route: route,
                            quantity: quantity,
                            freq_value: frequency_value,
                            freq_name: frequency,
                            freq_id: frequency_id,
                            remarks: notes,
                            price: price,
                            id: id,
                            head_id: head_id,
                            iv_start_at: iv_start_at,
                            stock: stock,
                            billconverted_status: billconverted_status,
                            subcategory_id: subcategory_id,
                            generic_name: generic_name,
                            generic_id: generic_id,
                        };

                        console.log(data);

                        getNewRowInserted(mode, data);

                    }
                    let intend_type = res[0].intend_type;
                    $('input[name="p_search_type"][value="' + intend_type + '"]').prop('checked', true);

                    if (mode == 'copy') {
                        $("#prescription_head_id").val(0);
                        checkAllergicMedicine(itemCodesArray);
                    }
                }

            },
            complete: function () {
                $("body").LoadingOverlay("hide");

                $('#prescription_wrapper .nav-tabs li a[href="#drug"]').closest('li').addClass('active');
                $('#prescription_wrapper .nav-tabs li a[href="#consumables"]').closest('li').removeClass('active');
                $('#prescription_wrapper .tab-content #drug').addClass('active in');
                $('#prescription_wrapper .tab-content #consumables').removeClass('active in');

            }
        });
    }
}


//medicine search listing(edit row)
var edit_row_timeout = null;
var edit_row_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_name[]"]', function (event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        return false;
    }
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_string = $(this).val().trim();
    var patient_id = $('#patient_id').val();
    var search_type = $("input[name='m_search_type']:checked").val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_string == "") {
        $(tr).find('span[class="med_name"]').html("");
        $(tr).find('input[name="selected_item_code[]"]').val("");
        $(tr).find('input[name="selected_item_name[]"]').val("");
        $(tr).find('input[name="selected_item_name[]"]').attr('data-generic-name', "");
        $(tr).find('input[name="selected_item_name[]"]').attr('value', "");
        $(tr).find('input[name="selected_item_price[]"]').val("");
        $(tr).find('input[name="selected_calculate_quantity[]"]').val("");
        $(tr).find('.medicine-list-div-row-listing').hide();
        $(tr).find('.medicine-list-div-row-listing').find('.list-medicine-search-data-row-listing').empty();
    }

    if (edit_row_search_string == "" || edit_row_search_string.length < 3) {
        edit_row_last_search_string = '';
        return false;
    } else {
        var med_list = $(tr).find('.medicine-list-div-row-listing');
        $(med_list).show();
        clearTimeout(edit_row_timeout);
        edit_row_timeout = setTimeout(function () {
            if (edit_row_search_string == edit_row_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: edit_row_search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#MedicationTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="6"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data.medicine_list;
                    let res_data = "";
                    var disable_zero_stock_items_prescription = data.disable_zero_stock_items_prescription ? data.disable_zero_stock_items_prescription : 0;
                    var search_list = $(tr).find('#ListMedicineSearchDataRowListing-' + row_id);



                    console.log(response);
                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let subcategory_id = response[i].subcategory_id;
                            let stock = response[i].stock;
                            let route = response[i].route;
                            let frequency = response[i].frequency;
                            let duration = response[i].duration;
                            let directions = response[i].directions;
                            let category_name = response[i].category_name;
                            let subcategory_name = response[i].subcategory_name;
                            let calculate_quantity = response[i].calculate_quantity;
                            let generic_name_id = response[i].generic_name_id;

                            var nostock = '';
                            var color = '';
                            var color1 = '';
                            console.log(response[i].sound_a_like, response[i].look_a_like, item_code);
                            if (parseInt(response[i].lk_sd_prd) == 2) {
                                color = '#0080009e';
                                color1 = 'white';
                            } else if (parseInt(response[i].lk_sd_prd) == 1) {
                                color = '#ffff008f';
                                color1 = 'black';
                            }
                            if (response[i].sound_a_like == item_code) {
                                color = '#0080009e';
                                color1 = 'white';
                            } else if (response[i].look_a_like == item_code) {
                                color = '#ffff008f';
                                color1 = 'black';
                            }
                            if (!stock) {
                                nostock = 'no-stock'
                            }
                            if (disable_zero_stock_items_prescription == 0) {

                                res_data += '<tr style="background:' + color + ';color:' + color1 + '" data-genreric-name="' + generic_name + '" data-genreric-name-id="' + generic_name_id + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '" data-calculate-quantity="' + calculate_quantity + '"><td style="color:' + color1 + '" class="' + nostock + '">' + item_desc + '</td><td style="color:' + color1 + '">' + generic_name + '</td><td style="color:' + color1 + '">' + category_name + '</td><td style="color:' + color1 + '">' + subcategory_name + '</td><td style="color:' + color1 + '">' + stock + '</td><td style="color:' + color1 + '">' + price + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                            } else {
                                if (stock != 0 && stock != '' && stock != 0.00) {
                                    res_data += '<tr style="background:' + color + ';color:' + color1 + '" data-genreric-name="' + generic_name + '" data-genreric-name-id="' + generic_name_id + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '" data-calculate-quantity="' + calculate_quantity + '"><td style="color:' + color1 + '" class="' + nostock + '">' + item_desc + '</td><td style="color:' + color1 + '">' + generic_name + '</td><td style="color:' + color1 + '">' + category_name + '</td><td style="color:' + color1 + '">' + subcategory_name + '</td><td style="color:' + color1 + '">' + stock + '</td><td style="color:' + color1 + '">' + price + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                                }

                            }
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="6">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    edit_row_last_search_string = edit_row_search_string;
                    $(".presc_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});


$(".selected_medicine_row").each(function (index) {
    console.log(index + ": " + $(this).focus());
});

//when double click listing medicine search result (edit)
$(document).on('click', '.list-medicine-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let generic_name = $(this).attr('data-genreric-name');
    let generic_id = $(this).attr('data-genreric-name-id');
    let route = $(this).attr('data-route');
    let frequency = $(this).attr('data-frequency');
    let duration = $(this).attr('data-duration');
    let directions = $(this).attr('data-directions');
    let calculate_quantity = $(this).attr('data-calculate-quantity');
    let sel_row_td = $(tr).closest('.medicine-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');
    let name = $(tr).find('input[name="list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="list_med_code_hid[]"]').val();

    let price = $(tr).find('input[name="list_med_price_hid[]"]').val();
    let subcategory_id = $(tr).find('input[name="list_med_sub_cat_id[]"]').val();


    let cur_row_itm_code = $(sel_row_tr).find('input[name="selected_item_code[]"]').val();
    let stock = $(this).attr('data-stock');

    if (route) {
        $(sel_row_tr).find("select").val(route);
    }
    if (frequency) {
        $(sel_row_tr).find(".freq_id_" + frequency).trigger('dblclick');
    }
    if (duration) {
        $(sel_row_tr).find("input[name='selected_item_duration[]']").val(duration);
    }
    if (directions) {
        $(sel_row_tr).find("input[name='selected_item_remarks[]']").val(directions);
    }

    $(sel_row_tr).find("input[name='selected_item_quantity[]']").focus();

    if (!stock) {
        $('input[name="search_medicine"]').css("color", "#ef0a1a");
    } else {
        $('input[name="search_medicine"]').css("color", "unset");
    }

    $('input[name="selected_calculate_quantity[]"]').val(calculate_quantity);

    //same item already exists
    let alredy_exist = $('input[name="selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == code && $(obj).val() != cur_row_itm_code) {
            return code;
        }
    });

    if (alredy_exist.length > 0) {
        // Command: toastr["error"]("Medicine already exist.");
        var confirm = window.confirm("Medicine already exist. Do you want to continue ?");
        if (!confirm) {
            return false;
        }
    }

    // if(subcategory_id.toString() == window.iv_sub_cat_id.toString()){
    //     if(!$(tr).closest('.medicine-list-div-row-listing').parents('tr').hasClass('iv_fluid')){
    //         $(tr).closest('.medicine-list-div-row-listing').parents('tr').addClass('iv_fluid')
    //     }
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find('input,select').attr("disabled", true);
    //     // $(".ivMedication").find('input,select').attr("disabled", false);
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find("input[name='selected_item_quantity[]']").focus();
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find('input[name="selected_item_dose[]"]').val('');
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find('input[name="selected_item_frequency[]"]').val('');
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find('input[name="selected_item_duration[]"]').val('');
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find('select[name="selected_item_route[]"]').val('');
    // } else {
    //     if($(tr).closest('.medicine-list-div-row-listing').parents('tr').hasClass('iv_fluid')){
    //         $(tr).closest('.medicine-list-div-row-listing').parents('tr').removeClass('iv_fluid')
    //     }
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find('input,select').attr("disabled", false);
    //     // $(".ivMedication").find('input,select').attr("disabled", true);
    //     $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find("input[name='selected_item_dose[]']").focus();
    // }

    $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find("input[name='selected_item_dose[]']").focus();

    if (name != '' && code != '') {
        $(sel_row_td).removeClass('outside-medicine');
        $(sel_row_td).find('span[class="med_name"]').html(name);
        $(sel_row_td).find('input[name="selected_item_code[]"]').val(code);
        $(sel_row_td).find('input[name="selected_item_name[]"]').val(name);

        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('data-generic-name', generic_name);
        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('data-generic-id', generic_id);
        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('value', name);
        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('item_code', code);
        $(sel_row_td).find('input[name="selected_item_price[]"]').val(price);
        $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.generic_name_input').val(generic_name);

        $(sel_row_td).find(".medicine-list-div-row-listing").hide();

        if ($("#allegy_details").find('tr#' + code).length > 0) {
            $('input[name="search_medicine"]').css("background", '#f4c4a6');
            bootbox.alert('The patient is allergic to this medicine.');
        } else {
            if (generic_name != "") {
                var allegic = false;
                $("#allegy_details").find('h4').each(function (key, val) {
                    var allegic_item = $(val).text().toUpperCase();
                    if (allegic_item.indexOf(generic_name.toUpperCase()) !== -1) {
                        allegic = true;
                    }
                });

                if (allegic == true) {
                    $('input[name="search_medicine"]').css("background", '#f4c4a6');
                    bootbox.alert('The patient is allergic to this medicine.');
                } else {
                    $('input[name="search_medicine"]').css("background", '#ffff');
                }
            }


        }

        if ($('input[name="selected_item_name[]"]:not([value!=""])').length == 0) {
            addNewMedicine();
        }
    }
    checkSingleMedicineAllergy(code, generic_id);

});


$('.close_btn_med_search').on('click', function (event) {
    $(".medicine-list-div-row-listing").hide();
});


// //frequency search (edit row)
// var edit_row_freq_timeout = null;
// var edit_row_freq_last_search_string = '';
// $(document).on('keyup', 'input[name="selected_item_frequency[]"]', function (event) {
//     event.preventDefault();
//     /* Act on the event */
//     var obj = $(this);
//     var edit_row_search_freq_string = $(this).val();
//     var patient_id = $('#patient_id').val();
//     let tr = $(obj).closest('tr');
//     let row_id = $(obj).closest('tr').attr('row-id');

//     if (edit_row_search_freq_string == "" || edit_row_search_freq_string.length < 2) {
//         edit_row_freq_last_search_string = '';
//         return false;
//     } else {
//         var freq_list = $(tr).find('.frequency-list-div-row-listing');
//         $(freq_list).show();
//         clearTimeout(edit_row_freq_timeout);
//         edit_row_freq_timeout = setTimeout(function () {
//             if (edit_row_search_freq_string == edit_row_freq_last_search_string) {
//                 return false;
//             }
//             var url = $('#base_url').val() + "/emr/frequency-search";
//             $.ajax({
//                 type: "GET",
//                 url: url,
//                 data: {
//                     search_freq_string: edit_row_search_freq_string,
//                     patient_id: patient_id
//                 },
//                 beforeSend: function () {
//                     $(tr).find('#FrequencyTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
//                 },
//                 success: function (data) {

//                     let response = data;
//                     let res_data = "";

//                     var freq_search_list = $(tr).find('#ListFrequencySearchDataRowListing-' + row_id);



//                     if (response.length > 0) {
//                         for (var i = 0; i < response.length; i++) {

//                             let frequency = response[i].frequency;
//                             let frequency_value = response[i].frequency_value;

//                             res_data += '<tr><td>' + frequency + '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' + i + '" value="' + frequency_value + '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' + i + '" value="' + frequency + '"></tr>';
//                         }
//                     } else {
//                         res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
//                     }

//                     $(freq_search_list).html(res_data);
//                     edit_row_freq_last_search_string = edit_row_search_freq_string;
//                     $(".freq_theadscroll").animate({ scrollTop: 0 }, "slow");

//                 },
//                 complete: function () {
//                     $('.freq_theadfix_wrapper').floatThead("reflow");
//                 }
//             });
//         }, 500)

//     }
// });

//close frequency search
$(document).on('click', '.frequency-list-div-row-listing > .close_btn_freq_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".frequency-list-div-row-listing").hide();
});

//when select frequency
$(document).on('click', '.list-frequency-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.frequency-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
    let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();
    let id = $(tr).find('input[name="list_freq_id_hid[]"]').val();

    if (name != '' && value != '') {
        $(sel_row_td).find('span[class="frequency"]').html(name);
        $(sel_row_td).find('input[name="selected_frequency_value[]"]').val(value);
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').val(name);
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').attr('value', name);
        $(sel_row_td).find('input[name="selected_frequency_id[]"]').val(id);

        $(sel_row_td).find(".frequency-list-div-row-listing").hide();
        $(sel_row_tr).find('input[name="selected_item_duration[]"]').focus().val($(sel_row_tr).find('input[name="selected_item_duration[]"]').val());;
    }

});

//route search (edit row)
var edit_row_route_timeout = null;
var edit_row_route_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_route[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_route_string = $(this).val();
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_route_string == "" || edit_row_search_route_string.length < 2) {
        edit_row_route_last_search_string = '';
        return false;
    } else {
        var route_list = $(tr).find('.route-list-div-row-listing');
        $(route_list).show();
        clearTimeout(edit_row_route_timeout);
        edit_row_route_timeout = setTimeout(function () {
            if (edit_row_search_route_string == edit_row_route_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/route-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_route_string: edit_row_search_route_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#RouteTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var route_search_list = $(tr).find('#ListRouteSearchDataRowListing-' + row_id);

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let route = response[i].route;

                            res_data += '<tr><td>' + route + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(route_search_list).html(res_data);
                    edit_row_route_last_search_string = edit_row_search_route_string;
                    $(".route_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close route search
$(document).on('click', '.route-list-div-row-listing > .close_btn_route_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".route-list-div-row-listing").hide();
});

//when select route
$(document).on('dblclick', '.list-route-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.route-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let route = $(tr).text();

    if (route != '') {
        $(sel_row_td).find('input[name="selected_item_route[]"]').val(route);

        $(sel_row_td).find(".route-list-div-row-listing").hide();
        $(sel_row_td).find('input[name="selected_item_route[]"]').focus();
    }

});


//instruction search (edit row)
var edit_row_instruction_timeout = null;
var edit_row_instruction_last_search_string = '';
// $(document).on('keyup', 'input[name="selected_item_remarks[]"]', function (event) {
//     event.preventDefault();
//     /* Act on the event */
//     var obj = $(this);
//     var edit_row_search_instruction_string = $(this).val();
//     var patient_id = $('#patient_id').val();
//     let tr = $(obj).closest('tr');
//     let row_id = $(obj).closest('tr').attr('row-id');

//     if (edit_row_search_instruction_string == "" || edit_row_search_instruction_string.length < 2) {
//         edit_row_instruction_last_search_string = '';
//         return false;
//     } else {
//         var instruction_list = $(tr).find('.instruction-list-div-row-listing');
//         $(instruction_list).show();
//         clearTimeout(edit_row_instruction_timeout);
//         edit_row_instruction_timeout = setTimeout(function () {
//             if (edit_row_search_instruction_string == edit_row_instruction_last_search_string) {
//                 return false;
//             }
//             var url = $('#base_url').val() + "/emr/instruction-search";
//             $.ajax({
//                 type: "GET",
//                 url: url,
//                 data: {
//                     search_instruction_string: edit_row_search_instruction_string,
//                     patient_id: patient_id
//                 },
//                 beforeSend: function () {
//                     $(tr).find('#InstructionTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
//                 },
//                 success: function (data) {

//                     let response = data;
//                     let res_data = "";

//                     var instruction_search_list = $(tr).find('#ListInstructionSearchDataRowListing-' + row_id);

//                     if (response.length > 0) {
//                         for (var i = 0; i < response.length; i++) {

//                             let instruction = response[i].instruction;

//                             res_data += '<tr><td>' + instruction + '</td></tr>';
//                         }
//                     } else {
//                         res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
//                     }

//                     $(instruction_search_list).html(res_data);
//                     edit_row_instruction_last_search_string = edit_row_search_instruction_string;
//                     $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

//                 },
//                 complete: function () {
//                     $('.route_theadfix_wrapper').floatThead("reflow");
//                 }
//             });
//         }, 500)

//     }
// });

$(document).on('keyup', 'input[name="selected_item_remarks[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_instruction_string = $(this).val() ? $(this).val().trim() : "";
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    var instruction_list = $(tr).find('.instruction-list-div-row-listing');
    $(instruction_list).show();
    clearTimeout(edit_row_instruction_timeout);
    edit_row_instruction_timeout = setTimeout(function () {
        var url = $('#base_url').val() + "/emr/instruction-search";
        var req_data = {};
        req_data.search_instruction_string = edit_row_search_instruction_string;
        req_data.patient_id = patient_id;

        if (edit_row_search_instruction_string == "") {
            req_data.all = 1;
        }

        $.ajax({
            type: "GET",
            url: url,
            data: req_data,
            beforeSend: function () {
                $(tr).find('#InstructionTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
            },
            success: function (data) {

                let response = data;
                let res_data = "";

                var instruction_search_list = $(tr).find('#ListInstructionSearchDataRowListing-' + row_id);

                if (response.length > 0) {
                    for (var i = 0; i < response.length; i++) {

                        let instruction = response[i].instruction;

                        res_data += '<tr><td>' + instruction + '</td></tr>';
                    }
                } else {
                    res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                }

                $(instruction_search_list).html(res_data);
                $(".route_theadscroll").animate({
                    scrollTop: 0
                }, "slow");

            },
            complete: function () {
                $('.route_theadfix_wrapper').floatThead("reflow");
            }
        });
    }, 500)
});

//close instruction search
$(document).on('click', '.instruction-list-div-row-listing > .close_btn_instruction_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".instruction-list-div-row-listing").hide();
});

//when select route
$(document).on('click', '.list-instruction-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.instruction-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let instruction = $(tr).text();
    if (instruction == 'No Data Found..!') {
        $(sel_row_td).find(".instruction-list-div-row-listing").hide();
        return false;
    }

    if (instruction != '') {
        $(sel_row_td).find('input[name="selected_item_remarks[]"]').val(instruction);

        $(sel_row_td).find(".instruction-list-div-row-listing").hide();
    }

});


/*************************************** EDIT ROW *********************************************/



//Route search
var route_timeout = null;
var route_last_search_string = '';
$('#search_route').on('keyup', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchRoute(obj);

});

$('#search_route').on('focus', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchRoute(obj, 1);
});

function searchRoute(obj, all = 0) {
    var search_route_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var route_list = $('.route-list-div');

    if ((search_route_string == "" || search_route_string.length < 2) && all == 0) {
        route_last_search_string = '';
        return false;
    } else {
        var route_list = $('.route-list-div');
        $(route_list).show();
        clearTimeout(route_timeout);
        route_timeout = setTimeout(function () {
            if (search_route_string == route_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/route-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_route_string: search_route_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#RouteTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var route_search_list = $('#ListRouteSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let route = response[i].route;

                            res_data += '<tr><td>' + route + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(route_search_list).html(res_data);
                    route_last_search_string = search_route_string;
                    $(".route_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}

//close route search
$(document).on('click', '.close_btn_route_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".route-list-div").hide();
});


//when select route
$(document).on('dblclick', '#ListRouteSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let route = $(tr).text();

    if (route != '') {
        $('input[name="search_route"]').val(route);

        $(".route-list-div").hide();
        $('#search_instructions').focus();
    }

});

// enter to add medicine
// $(document).on('keyup', '.medicine_search_head input[type=text]', function (event) {
//     event.preventDefault();
//     /* Act on the event */

//     var medicine = $("#search_medicine").val();
//     if(event.keyCode == 13 && medicine){
//         addNewMedicine();
//     }

// });


//Instruction search
var instruction_timeout = null;
var instruction_last_search_string = '';
$('#search_instructions').on('keyup', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchInstructions(obj);

});

$('#search_instructions').on('focus', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchInstructions(obj, 1);
});

function searchInstructions(obj, all = 0) {
    var search_instruction_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var instruction_list = $('.instruction-list-div');

    if ((search_instruction_string == "" || search_instruction_string.length < 2) && all == 0) {
        instruction_last_search_string = '';
        return false;
    } else {
        var instruction_list = $('.instruction-list-div');
        $(instruction_list).show();
        clearTimeout(instruction_timeout);
        instruction_timeout = setTimeout(function () {
            if (search_instruction_string == instruction_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/instruction-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_instruction_string: search_instruction_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#InstructionTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var instruction_search_list = $('#ListInstructionSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let instruction = response[i].instruction;

                            res_data += '<tr><td>' + instruction + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(instruction_search_list).html(res_data);
                    instruction_last_search_string = search_instruction_string;
                    $(".route_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}


//when select instruction
$(document).on('dblclick', '#ListInstructionSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let instruction = $(tr).text();
    if (instruction == 'No Data Found..!') {
        $(".instruction-list-div").hide();
        return false;
    }

    if (instruction != '') {
        $('input[name="search_instructions"]').val(instruction);

        $(".instruction-list-div").hide();
    }

});

//close instruction search
$(document).on('click', '.close_btn_instruction_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".instruction-list-div").hide();
});


//calculate Final Total Amount;
function calculateTotalMedicineAmount() {
    var price = 0;
    var quantity = 0;
    var final_total = 0;
    $("input[name='selected_item_price[]']").each(function (input_index, input_val) {
        price = $('input[name="selected_item_price[]"]').eq(input_index).val();
        quantity = $('input[name="selected_item_quantity[]"]').eq(input_index).val();
        if (parseFloat(price) > 0) {
            if (parseFloat(quantity) > 0) {
                final_total = parseFloat(final_total) + (parseFloat(price) * parseFloat(quantity));
            } else {
                final_total = parseFloat(final_total) + parseFloat(price);
            }
        }

    });
    if (isNaN(final_total)) {
        final_total = 0;
    }
    $("#med_total_amnt").text(final_total.toFixed(2));
    $("#approx_amnt").val(final_total.toFixed(2));

    resetMedicineSerialNumbers();
}

function resetMedicineSerialNumbers() {
    $(".med_serial_no").each(function (key, val) {
        $(this).html(key + 1);
        if (!$(this).parents('tr').attr('row-id')) {
            $(this).parents('tr').attr('row-id', key + 1);
        }
    })
}

var p_footer_fields_timeout = null;

function update_prescription_footer_fields() {
    let next_review_date = $('#next_review_date').val();
    let reason_for_visit = $('#visit_reason').val();
    let diagnosis = $('#pres_history').val();
    let special_notes = $('#pres_notes').val();
    let encounter_id = $('#encounter_id').val();
    if (encounter_id != "" && encounter_id > 0 && encounter_id != undefined) {

        clearTimeout(p_footer_fields_timeout);
        p_footer_fields_timeout = setTimeout(function () {

            let url = $('#base_url').val();
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url + "/emr/prescription-footer-fields-save",
                data: {
                    encounter_id: encounter_id,
                    next_review_date: next_review_date,
                    reason_for_visit: reason_for_visit,
                    diagnosis: diagnosis,
                    special_notes: special_notes,
                    _token: _token
                },
                beforeSend: function () {

                },
                success: function (data) {

                },
                complete: function () {

                }
            });

        }, 500);

    } else {
        Command: toastr["warning"]("Error.! Encounter Not Found.");
    }
}

function prescriptionDeleteMode(obj, id) {
    if (id != "" && id > 0) {
        var result = confirm("Want to delete?");
        if (result) {

            let _token = $('#c_token').val();
            var url = $('#base_url').val() + "/emr/remove-patient-prescription";
            $.ajax({
                type: "POST",
                url: url,
                data: "removePrescrption=" + id + "&_token=" + _token,
                beforeSend: function () {},
                success: function (data) {
                    if (data.status == 1) {
                        $(obj).closest('tr').remove();
                        Command: toastr["success"]("Removed Successfully..!");
                    }
                },
                complete: function () {}
            });
        }
    }
}


$(document).on('click', '.close_btn_fav_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".favorite-medicine-list-popup").hide();
});

$(document).on('click', '.fav_dropdown_btn', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".favorite-medicine-list-popup").show();
    loadMedFavorites();
});

$(document).on('click', '.new_medication_bookmarks_btn', function (event) {
    event.preventDefault();

    $("#addItemToFavouriteNursingHeader").html('Medication Favourite');
    $("#addItemToFavouriteNursingModel").modal('toggle');
    loadMedFavorites(1);
});

function loadMedFavorites(new_bookmark = 0) {
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/medicine-favorite-groups";
    if (parseInt(new_bookmark) == 1) {
        loader_class = 'addItemToFavouriteNursingDiv';
    } else {
        loader_class = 'fav_groups';
    }
    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data: "load_groups=" + 1 + "&_token=" + _token + "&new_bookmark=" + new_bookmark,
        beforeSend: function () {
            $('#' + loader_class).html('<div class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></div>');
        },
        success: function (data) {
            if (data.status == 1) {
                if (parseInt(new_bookmark) == 1) {
                    $("#addItemToFavouriteNursingDiv").html(data.html);
                } else {
                    $("#fav_groups").html(data.html);
                }
            }
        },
        complete: function () {
            let group_length = $("#fav_groups li").length;
            if (group_length > 0) {
                $("#fav_groups input:first").prop("checked", true).trigger('click');
            }
        }
    });

}

var double_fav_click_prevent = 0;
var prescription_last_opened_group_id = 0;
$(document).on("click", "input[name=favgroups]", function () {
    id_ = $(this).val();

    if (prescription_last_opened_group_id == id_) {
        return false;
    }
    if (id_ == "" || id_ == undefined) {
        return false;
    }
    prescription_last_opened_group_id = id_;

    if (double_fav_click_prevent == 0) {
        double_fav_click_prevent = 1;

        $("body").LoadingOverlay("show", {
            background: "rgba(89, 89, 89, 0.6)",
            imageColor: '#337AB7'
        });
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/emr/medicine-favorite-list";
        $.ajax({
            type: "POST",
            url: url,
            data: 'load_group_items=1&group_id=' + id_ + '&_token=' + _token,
            success: function (response) {
                if (response.status == 1) {
                    $("#ListMedicineFavorites").html(response.html);
                    $(".fav_presc_select_checkbox").prop('checked', false);
                }
                double_fav_click_prevent = 0;
                $("body").LoadingOverlay("hide");
            },
            complete: function (e) {
                $("body").LoadingOverlay("hide");
            }
        });

    }

});

$(document).on("click", ".fav_apply_btn", function () {


    $("input[name='medicine[]']").each(function () {
        if ($(this).val() == '') {
            $(this).parent().parent().remove();
        }
    });
    var Rowcount = 1;
    if (($('#medicine-listing-table').find('tr').length) > 0) {
        var Rowcount = ($('#medicine-listing-table').find('tr').length) + 1;
    }
    let _token = $('#c_token').val();
    var enable_ward_stock = $("#enable_ward_stock").val();
    var url = $('#base_url').val() + "/emr/medicine-favorite-list";
    //Fav checked ids
    var favcheckedIDs = $(".fav_check:checked").map(function () {
        return $(this).val();
    }).get();
    if (favcheckedIDs.length > 0) {
        $("body").LoadingOverlay("show", {
            background: "rgba(89, 89, 89, 0.6)",
            imageColor: '#337AB7'
        });
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: "presc_fav_selected_items_add_to_list=" + 1 + "&favcheckedIDs=" + favcheckedIDs + "&rows=" + Rowcount +"&enable_ward_stock=" + enable_ward_stock+ "&_token=" + _token,
            beforeSend: function () {

            },
            success: function (medData) {
                if (medData.status == 1) {
                    $('#medicine-listing-table').append(medData.html);
                    $('.favorite-medicine-list-popup').hide();
                    calculateTotalMedicineAmount();
                    let current_freq_list = $("#ListFrequencySearchData").html();
                    $(".list-frequency-search-data-row-listing").html(current_freq_list);
                    $('#medicine-listing-table').find(".bookmark_row").trigger('dblclick');

                    let sel_search_route = document.getElementById('search_route');
                    route_options = sel_search_route.innerHTML;
                    $(".list-route-search-data-row-listing").html(route_options);

                    $(".list-route-search-data-row-listing").each(function () {
                        $(this).val($(this).attr('data-selected-route'));
                    })
                }
            },
            complete: function () {

                $(".fav_check").each(function () {
                    $(this).attr('checked', false);
                });
                $("body").LoadingOverlay("hide");
            }
        });
    }


    $('.fav_dropdown_close').trigger('click');
});

document.onkeydown = function (evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        $(".frequency-list-div").hide();
        $(".route-list-div").hide();
        $(".medicine-list-div").hide();
    }
};

function add_private_note() {

    var notes = $('#patient_notes').val();
    if (notes == "") {
        Command: toastr["warning"]('Enter Note.!');
        return false;
    }
    var patientId = $('#patient_id').val();

    if ($('#visiblility_status').prop("checked") == true) {
        var status = 1;
    } else if ($('#visiblility_status').prop("checked") == false) {
        var status = 0;
    }

    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/add-private-note";

    $.ajax({
        url: url,
        type: "POST",
        async: true,
        data: "private_note=" + notes + '&patient_id=' + patientId + '&visibility_status=' + status + '&_token=' + _token,
        beforeSend: function () {},
        success: function (data) {

            if (data.status == 1) {
                Command: toastr["success"]('Notes added successfully');
            }

        },
        complete: function () {}
    });

}



//search consumable
var consumable_timeout = null;
var consumable_last_search_string = '';
$(document).on('keyup', 'input[name="consumable_search_medicine"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var patient_id = $('#patient_id').val();
    var search_type = $("input[name='consumable_m_search_type']:checked").val();
    var med_list = $('.consumable-medicine-list-div');

    if (search_string == "" || search_string.length < 3) {
        consumable_last_search_string = '';
        return false;
    } else {
        $(med_list).show();
        clearTimeout(consumable_timeout);
        consumable_timeout = setTimeout(function () {
            if (search_string == consumable_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    is_consumable: 1,
                    search_key_string: search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('#ConsumableTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    //alert(data);
                    let response = data.medicine_list;
                    let res_data = "";


                    var search_list = $('#ConsumableListMedicineSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            // let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let stock = response[i].stock;

                            res_data += '<tr><td>' + item_desc + '</td><td>' + stock + '</td><td>' + price + '</td><input type="hidden" name="consumable_list_med_name_hid[]" id="consumable_list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="consumable_list_med_code_hid[]" id="consumable_list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="consumable_list_med_price_hid[]" id="consumable_list_med_price_hid-' + i + '" value="' + price + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }


                    $(search_list).html(res_data);
                    consumable_last_search_string = search_string;
                    $(".presc_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});


//close Consumable search
$(document).on('click', '.consumable-medicine-list-div > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".consumable-medicine-list-div").hide();
});

//when select Consumable
$(document).on('dblclick', '#ConsumableListMedicineSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let name = $(tr).find('input[name="consumable_list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="consumable_list_med_code_hid[]"]').val();
    let price = $(tr).find('input[name="consumable_list_med_price_hid[]"]').val();

    if (name != '' && code != '') {
        $('input[name="consumable_search_medicine"]').val(name);
        $('input[name="consumable_search_item_code_hidden"]').val(code);
        $('input[name="consumable_search_item_name_hidden"]').val(name);
        $('input[name="consumable_search_item_price_hidden"]').val(price);

        $(".consumable-medicine-list-div").hide();
        $("input[name='consumable_search_quantity']").focus();
    }

});


//add Consumable
function addNewConsumable() {
    let status = validateConsumableRow();
    if (status == false) {
        console.log("Error");
        return false;
    }

    let mode = 'add';

    getNewConsumableRowInserted(mode);

    $('input[name="consumable_search_medicine"]').focus();
}

//validate top Consumable row
function validateConsumableRow() {
    let err = 0;

    let itm_name = $('input[name="consumable_search_medicine"]').val();
    let itm_code = $('input[name="consumable_search_item_code_hidden"]').val();
    if (itm_name == '' || itm_name == undefined) {
        Command: toastr["error"]("Enter Medicine.");
        err = 1;
    }

    let consumable_search_quantity = $('input[name="consumable_search_quantity"]').val();
    if (consumable_search_quantity == '' || consumable_search_quantity == undefined) {
        Command: toastr["error"]("Enter Quantity.");
        err = 1;
    }

    //same item already exists
    let alredy_exist = $('input[name="consumable_selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == itm_code && itm_code != "") {
            return itm_code;
        }
    });

    if (alredy_exist.length > 0) {
        Command: toastr["error"]("Item already exist.");
        err = 1;
    }

    if (err == 1) {
        return false;
    } else {
        return true;
    }
}

//add Consumable row
function getNewConsumableRowInserted(mode = 'add', data = {}) {

    let code = "";
    if (mode == 'add') {
        code = $('input[name="consumable_search_item_code_hidden"]').val();
    } else {
        if (Object.keys(data).length > 0) {
            code = data.med_code;
        }
    }
    if (code != "") {
        //allergy_check(code);
    }

    var table = document.getElementById("consumable-medicine-listing-table");
    let row_count = table.rows.length;
    let row_id = row_count;
    if (parseInt(row_count) > 0) {
        let last_id = $('#consumable-medicine-listing-table tr:last').attr('row-id');
        if (last_id != undefined) {
            row_id = parseInt(last_id) + 1;
        }
    }

    var row = table.insertRow(row_count);

    row.setAttribute("row-id", row_id, 0);

    var cell1 = row.insertCell(0);
    cell1.width = '35%';
    var cell2 = row.insertCell(1);
    cell2.width = '10%';
    var cell3 = row.insertCell(2);
    cell3.width = '20%';
    var cell4 = row.insertCell(3);
    cell4.classList.add("text-center");
    cell4.width = '3%';

    let med_code = "";
    let med_name = "";
    let quantity = "";
    let rate = "";
    let remarks = "";
    let start_at = "";
    let id = "";
    let price = 0;

    if (mode == 'add') {


        med_code = $('input[name="consumable_search_item_code_hidden"]').val();

        if (med_code == "") {
            med_name = $('input[name="consumable_search_medicine"]').val();
        } else {
            med_name = $('input[name="consumable_search_item_name_hidden"]').val();
        }

        quantity = $('input[name="consumable_search_quantity"]').val();
        remarks = $('input[name="consumable_search_instructions"]').val();
        price = $('input[name="consumable_search_item_price_hidden"]').val();

    } else {
        //load bsed on prescription_id
        if (Object.keys(data).length > 0) {
            med_code = data.med_code;
            med_name = data.med_name;
            out_medicine_name = data.out_medicine_name;
            quantity = data.quantity;
            price = data.price;
            remarks = data.remarks;
            id = data.id;
            head_id = data.head_id;
            if (mode == 'copy') {
                id = "";
                head_id = "";
            }

            //in case of outside medicine
            if (med_code == "") {
                if (out_medicine_name != "") {
                    med_name = out_medicine_name;
                }
            }

            if (head_id != '' && head_id != 0) {
                $('#prescription_consumable_head_id').val(head_id);
            }
        }
    }

    let medicine_popup_search = '<div class="consumable-medicine-list-div-row-listing" style="display: none;"><a style="float: right;" class="close_btn_med_search"><i class="fa fa-times-circle" aria-hidden="true"></i></a><div class=" presc_theadscroll" style="position: relative;"><table id="ConsumableTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><thead><tr class="light_purple_bg"><th>Medicine</th><th>Stock</th><th>Price</th></tr></thead><tbody id="ConsumableListMedicineSearchDataRowListing-' + row_id + '" class="consumable-list-medicine-search-data-row-listing" ></tbody></table></div></div>';


    let medicine_instruction_popup_search = '';

    cell1.innerHTML = "<span class='consumable_name'>" + med_name + "</span> <input type='hidden' name='consumable_selected_item_code[]' id='consumable_selected_item_code-" + row_id + "' value='" + med_code + "'><input type='hidden' class='form-control' name='consumable_selected_item_name[]' id='consumable_selected_item_name-" + row_id + "' value='" + med_name + "' autocomplete='off'><input type='hidden' name='consumable_selected_edit_id[]' id='consumable_selected_edit_id-" + row_id + "' value='" + id + "'><input type='hidden' name='iv_selected_item_price[]' id='iv_selected_item_price-" + row_id + "' value='" + price + "'>" + medicine_popup_search;

    cell2.innerHTML = "<span class='quantity'>" + quantity + "</span> <input type='hidden' class='form-control' name='consumable_selected_item_quantity[]' id='consumable_selected_item_quantity-" + row_id + "' value='" + quantity + "' autocomplete='off'>";

    cell3.innerHTML = "<span class='consumable_remarks' title='" + remarks + "'>" + remarks + "</span> <input type='hidden' class='form-control' name='consumable_selected_item_remarks[]' id='consumable_selected_item_remarks-" + row_id + "' value='" + remarks + "'>" + medicine_instruction_popup_search;

    cell4.innerHTML = "<i class='fa fa-trash consumable-del-presc-list-row'></i>";

    if (mode == 'add') {
        consumableclearmedfields();
    }
    //calculateTotalMedicineAmount();
}

//clear fields consumable
function consumableclearmedfields() {
    $('input[name="consumable_search_item_code_hidden"]').val('');
    $('input[name="consumable_search_item_name_hidden"]').val('');
    $('input[name="consumable_search_medicine"]').val('');

    $('input[name="consumable_search_quantity"]').val('');

    $('input[name="consumable_search_instructions"]').val('');
}

//iv delete row
$(document).on('click', '.consumable-del-presc-list-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="consumable_selected_edit_id[]"]').val();
        if (edit_id != '' && edit_id != 0) {
            deleteRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});



//edit when double click consumable prescription
$(document).on('dblclick', '#consumable-medicine-listing-table tr:has(td)', function function_name(event) {
    let obj = $(this);
    consumablePrescriptionRowEdit(obj);
});

//set consumable row editable
function consumablePrescriptionRowEdit(obj) {
    //medicine column editable
    $(obj).find('input[name="consumable_selected_item_name[]"]').attr('type', 'text');
    $(obj).find('span[class="consumable_name"]').hide();

    //duration column editable
    $(obj).find('input[name="consumable_selected_item_quantity[]"]').attr('type', 'text');
    $(obj).find('span[class="quantity"]').hide();

    //remarks column editable
    $(obj).find('input[name="consumable_selected_item_remarks[]"]').attr('type', 'text');
    $(obj).find('span[class="consumable_remarks"]').hide();

    consumable_medicine_saved_after_edit = 0;
}

//consumable medicine search listing(edit row)
var consumable_edit_row_timeout = null;
var consumable_edit_row_last_search_string = '';
$(document).on('keyup', 'input[name="consumable_selected_item_name[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_string = $(this).val();
    var patient_id = $('#patient_id').val();
    var search_type = $("input[name='consumable_m_search_type']:checked").val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_string == "" || edit_row_search_string.length < 3) {
        consumable_edit_row_last_search_string = '';
        return false;
    } else {
        var med_list = $(tr).find('.consumable-medicine-list-div-row-listing');
        $(med_list).show();
        clearTimeout(consumable_edit_row_timeout);
        consumable_edit_row_timeout = setTimeout(function () {
            if (edit_row_search_string == edit_row_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    is_consumable: 1,
                    search_key_string: edit_row_search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#ConsumableTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data.medicine_list;
                    let res_data = "";

                    var search_list = $(tr).find('#ConsumableListMedicineSearchDataRowListing-' + row_id);




                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            // let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let stock = response[i].stock;

                            res_data += '<tr><td>' + item_desc + '</td><td>' + stock + '</td><td>' + price + '</td><input type="hidden" name="consumable_list_med_name_hid[]" id="consumable_list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="consumable_list_med_code_hid[]" id="consumable_list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="consumable_list_med_price_hid[]" id="consumable_list_med_price_hid-' + i + '" value="' + price + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    consumable_edit_row_last_search_string = edit_row_search_string;
                    $(".presc_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});

//when double click listing consumable medicine search result (edit)
$(document).on('dblclick', '.consumable-list-medicine-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let sel_row_td = $(tr).closest('.consumable-medicine-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let name = $(tr).find('input[name="consumable_list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="consumable_list_med_code_hid[]"]').val();

    let price = $(tr).find('input[name="consumable_list_med_price_hid[]"]').val();

    let cur_row_itm_code = $(sel_row_tr).find('input[name="consumable_selected_item_code[]"]').val();

    //same item already exists
    let alredy_exist = $('input[name="consumable_selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == code && $(obj).val() != cur_row_itm_code) {
            return code;
        }
    });

    if (alredy_exist.length > 0) {
        Command: toastr["error"]("Item already exist.");
        return false;
    }

    if (name != '' && code != '') {
        // $(sel_row_td).removeClass('outside-medicine');
        $(sel_row_td).find('span[class="consumable_name"]').html(name);
        $(sel_row_td).find('input[name="consumable_selected_item_code[]"]').val(code);
        $(sel_row_td).find('input[name="consumable_selected_item_name[]"]').val(name);
        $(sel_row_td).find('input[name="consumable_selected_item_name[]"]').attr('value', name);
        $(sel_row_td).find('input[name="iv_selected_item_price[]"]').val(price);

        $(sel_row_td).find(".consumable-medicine-list-div-row-listing").hide();
    }

});


//close medicine search listing (edit)
$(document).on('click', '.consumable-medicine-list-div-row-listing > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".consumable-medicine-list-div-row-listing").hide();
});

var consumable_save_starts = 0;
//save consumable prescription
function saveDoctorConsumablePrescriptions() {
    let validate = validateConsumablePrescription();
    let _token = $('#c_token').val();

    if (consumable_save_starts == 1) {
        return false;
    }

    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let prescription_consumable_head_id = $('#prescription_consumable_head_id').val();
    let presc_form = $('#consumable-presc-data-form').serialize();
    var p_type = $("input[name='consumable_p_search_type']:checked").val();

    if (validate) {
        var url = $('#base_url').val() + "/emr/save-consumable-prescription";
        consumable_save_starts = 1;
        $('.save_btn_bg').prop({
            disable: 'true'
        });
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: presc_form + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&prescription_head_id=' + prescription_consumable_head_id + '&intend_type=' + p_type,
            beforeSend: function () {
                $('body').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('body').LoadingOverlay("hide");
                if (data.status == 1) {
                    window.atleastOneSavedRecords = true;
                    // Command: toastr["success"]("Save.");
                    consumablePrescriptionEditMode(data.head_id, 'edit');
                    consumable_medicine_saved_after_edit = 1;
                    $('.draft_mode_indication_p_consumable').html('<span class="bg-orange draft-badge"> Draft Mode <i class="fa fa-exclamation-circle" title="Consumable saved as draft" aria-hidden="true"></i></span>');
                }
                consumable_save_starts = 0;
                $('#prescription_changes_draft_mode').val(0);
            },
            complete: function () {
                consumable_medicine_listing_table_changed = 0;
                $('.save_btn_bg').prop({
                    disable: 'false'
                });
            }
        });

    } else {
        consumable_medicine_listing_table_changed = 0;
        $('.save_btn_bg').prop({
            disable: 'false'
        });
    }
}

//validate consumable row
function validateConsumablePrescription() {
    //check rows exist
    if ($('#consumable-medicine-listing-table tbody > tr').find('input[name="consumable_selected_item_code[]"]').length == 0) {
        Command: toastr["error"]("Select Consumable.");
        $('#consumable_search_medicine').focus();
        return false;
    }

    return true;
}

//load consumable prescription different modes based on head_id
function consumablePrescriptionEditMode(head_id, mode = 'edit') {
    if (head_id != '' && mode != '') {
        var url = $('#base_url').val() + "/emr/prescription-details";
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: {
                head_id: head_id,
                _token: _token,
            },
            beforeSend: function () {
                $("body").LoadingOverlay("show", {
                    background: "rgba(89, 89, 89, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                let res = JSON.parse(data);
                let response = '';

                if (res.length > 0) {
                    $('#consumable-medicine-listing-table tbody').html("");
                    for (var i = 0; i < res.length; i++) {

                        let id = res[i].id;
                        let billconverted_status = res[i].billconverted_status;
                        let quantity = res[i].quantity;
                        let iv_rate = res[i].iv_rate;
                        let generic_name = res[i].generic_name;
                        let item_desc = res[i].item_desc;
                        let medicine_name = res[i].medicine_name;
                        let medicine_code = res[i].medicine_code;
                        let notes = res[i].notes;
                        let price = res[i].price;
                        let sl = parseInt(i) + 1;

                        let data = {
                            med_code: medicine_code,
                            out_medicine_name: medicine_name,
                            med_name: item_desc,
                            quantity: quantity,
                            rate: iv_rate,
                            remarks: notes,
                            price: price,
                            id: id,
                            head_id: head_id,
                        };

                        getNewConsumableRowInserted(mode, data);

                    }
                    let intend_type = res[0].intend_type;
                    $('input[name="consumable_p_search_type"][value="' + intend_type + '"]').prop('checked', true);

                    if (mode == 'copy') {
                        $("#prescription_consumable_head_id").val(0);
                    }
                }

            },
            complete: function () {
                $("body").LoadingOverlay("hide");

                $('#prescription_wrapper .nav-tabs li a[href="#drug"]').closest('li').removeClass('active');
                $('#prescription_wrapper .nav-tabs li a[href="#consumables"]').closest('li').addClass('active');
                $('#prescription_wrapper .tab-content #drug').removeClass('active in');
                $('#prescription_wrapper .tab-content #consumables').addClass('active in');
            }
        });
    }
}

function addFrequency() {
    $('#frequencypopup').modal('show');


    fetchFrequencyMasterDetails();

    $('#searchFreq').on('keyup', function () {
        var searchVal = $(this).val();

        if (searchVal != '') {
            $(".globalFreqList tr").hide();
            $('.globalFreqList tr[data-frequency-name^="' + searchVal.toLowerCase() + '"]').show();
            $('.globalFreqList tr[data-frequency-name^="' + searchVal.toUpperCase() + '"]').show();
        } else {
            $(".globalFreqList tr").show();
        }
    });


}

function fetchFrequencyMasterDetails() {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/fetchFrequencyMasterDetails",
        data: {
            _token: _token
        },
        beforeSend: function () {
            $(".globalFreqList").empty();
            $(".globalSelectedFreqList").empty();
            $(".globalFreqList").LoadingOverlay("show", {
                background: "rgba(89, 89, 89, 0.6)",
                imageColor: '#337AB7'
            });
            $(".globalSelectedFreqList").LoadingOverlay("show", {
                background: "rgba(89, 89, 89, 0.6)",
                imageColor: '#337AB7'
            });

        },
        success: function (data) {



            $.each(data.frequency_list, function (key, value) {

                var action = '';

                if (value.user_freq_id) {
                    action = '<input id="' + value.freq_id + '" type="checkbox" onclick="deleteuserFrequency(' + value.freq_id + ')" data-id="undefined"  checked></input> &nbsp; &nbsp; ';
                } else {
                    action = '<input id="' + value.freq_id + '" name="chk[]" class="' + value.frequency + '" type="checkbox" onclick="InsertFrequency(this)"  data-id="undefined" value="' + value.value + '"> &nbsp; &nbsp; ';
                }

                var global_row = "<tr id='" + value.frequency + "' style='padding:0px !important;' data-frequency-name='" + value.frequency + "' data-frequency-id='" + value.freq_id + "' data-frequency-value='" + value.value + "'><td style='text-align:left;padding:0px !important;'>" + action + "<label>" + value.frequency + "</label></td><td style='padding:0px !important;text-align:center' class='freq_row_val'>" + value.value + "</td></tr>";

                $(".globalFreqList").append(global_row);

                if (value.user_freq_id) {
                    var selected_row = '<tr id="' + value.freq_id + '" style="padding:0px !important;height:27px;" data-frequency-name="' + value.frequency + '" data-frequency-id="' + value.freq_id + '" data-frequency-value="' + value.value + '"> <td style="text-align: left;padding:0px !important;">    &nbsp;<label>' + value.frequency + '</label> </td> <td style="padding:0px !important;text-align: center;" class="freq_row_val">' + value.value + '</td> <td style="text-align: center;padding:0px !important;"> <input type="hidden"  name="frequency_id[]" value="' + value.freq_id + '"><button type="button" style="padding-top:3px;" class="btn btn-sm btn-danger delete-frequency-row"><i class="fa fa-trash"></i> Remove</button></td></tr>';

                    $(".globalSelectedFreqList").append(selected_row);
                }
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $(".globalFreqList").LoadingOverlay("hide");
            $(".globalSelectedFreqList").LoadingOverlay("hide");

        },
        complete: function () {

        }
    });

}

function addRoute(from_type) {

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "GET",
        url: url + "/emr/getDoctorRoute",
        data: {
            _token: _token
        },
        beforeSend: function () {
            $('#addDoctorRoutesBtn').attr('disabled', true);
            $('#addDoctorRoutesBtn').removeClass('fa fa-plus');
            $('#addDoctorRoutesBtn').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {

            $('#routtedatadiv').html(data);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            if (from_type != '1') {
                $("#routepopup").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }

        },
        complete: function () {
            $('#addDoctorRoutesBtn').attr('disabled', false);
            $('#addDoctorRoutesBtn').removeClass('fa fa-spinner fa-spin');
            $('#addDoctorRoutesBtn').addClass('fa fa-plus');
        }
    });

}

function save_route() {
    var route_name = $('#route_name').val();
    var url = $('#base_url').val() + "/emr/saveRoute";
    $.ajax({
        type: "GET",
        url: url,
        data: 'route_name=' + route_name,
        beforeSend: function () {
            $('#saveRouteBtn').attr('disabled', true);
            $('#saveRouteSpin').removeClass('fa fa-save');
            $('#saveRouteSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success("Successfully Added")
                addRoute(1);
                var obj = JSON.parse(data);
                $("select[name='selected_item_route[]']").append(obj.routedata);
            }
        },
        complete: function () {
            $('#saveRouteBtn').attr('disabled', false);
            $('#saveRouteSpin').removeClass('fa fa-spinner fa-spin');
            $('#saveRouteSpin').addClass('fa fa-save');
        }
    });
}



function delete_route(route_id, route_name) {
    route_name = atob(route_name);
    var url = $('#base_url').val() + "/emr/deleteRoute";
    var message_show = 'Are you sure you want to delete ' + route_name;
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'No',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var param = {
                    route_id: route_id
                };
                $.ajax({
                    type: "GET",
                    async: true,
                    url: url,
                    data: param,
                    cache: false,
                    beforeSend: function () {
                        $('#deleteprescriptionbtn' + route_id).attr('disabled', true);
                        $('#deleteprescriptionspin' + route_id).removeClass('fa fa-trash');
                        $('#deleteprescriptionspin' + route_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data == 1) {
                            toastr.success("Deleted Successfully");
                            $('#prescriptiondata' + route_id).remove();
                        } else {
                            toastr.error("Error Please Check Your Internet Connection");
                        }

                    },
                    complete: function () {
                        $('#deleteprescriptionbtn' + route_id).attr('disabled', false);
                        $('#deleteprescriptionspin' + route_id).removeClass('fa fa-spinner fa-spin');
                        $('#deleteprescriptionspin' + route_id).addClass('fa fa-trash');
                    }
                });
            }
        }
    });
}

function routeListSorting(table_id, search_field) {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(search_field);
    filter = input.value.toUpperCase();
    table = document.getElementById(table_id);
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().startsWith(filter)) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });
    $('.theadscroll').perfectScrollbar("update");
}

function InsertFrequency(e) {

    var value = e.value;
    //alert(frequency);
    var frequency_id = e.id;
    // alert(value);
    var get_freq_name = ($("#" + frequency_id).attr('class'));
    //alert(get_freq_name);

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $("body").LoadingOverlay("show", {
        background: "rgba(89, 89, 89, 0.6)",
        imageColor: '#337AB7'
    });
    $.ajax({
        type: "POST",
        async: false,
        url: url + "/emr/update_frequency",
        data: {

            frequency_id: frequency_id,

            _token: _token
        },
        beforeSend: function () {

        },
        success: function (data) {

            if (data.status == 1) {

                if ($('input[name="chk[]"]:checked').length > 0) {
                    $('#selected_freq_table').append("<tr id='" + data.frequency_id + "' style='padding:0px !important;height:27px;'><td style='text-align:left;padding:0px !important;'>&nbsp;&nbsp;" + get_freq_name + "</td><td style='padding:0px !important;text-align:center'>" + value + "</td><td  style='text-align: center;padding:0px !important;'><input type='hidden'  name='frequency_id[]' value=" + data.frequency_id + "><button type='button' style='padding-top:3px;' class='btn btn-sm btn-danger delete-frequency-row'><i class='fa fa-trash'></i> Remove</button></td></tr>");
                    $(".search_frequency_select").append('<option value="' + data.frequency_id + '" data-frequency-value="' + value + '" >' + get_freq_name + '</option>');
                    Command: toastr["success"]("Frequency Updated.");
                    $('#selected_freq_table #hiderow').hide();
                }

            } else {
                deleteuserFrequency(frequency_id);
            }

        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}


function save_frequency_details() {

    var frequency_name = $('#frequency_name').val();
    var frequency_id = $('#frequency_name').attr('frequency_id');
    var frequency_value = $('#frequency_value').val();

    let url = $('#base_url').val();
    let _token = $('#c_token').val();

    if (frequency_name == '' || frequency_value == '') {
        Command: toastr["error"]("Please Fill All Details")
    }
    else {

        $.ajax({
            type: "POST",
            async: false,
            url: url + "/emr/save_frequency",
            data: {
                frequency_name: frequency_name,
                frequency_value: frequency_value,
                frequency_id: frequency_id,
                _token: _token
            },
            beforeSend: function () {
                $("body").LoadingOverlay("show", {
                    background: "rgba(89, 89, 89, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (data.status == 1) {
                    $('#common_freq_table').append("<tr  style='padding:0px !important;height:27px;' id=" + frequency_name + "><td style='text-align:left;padding:0px !important;'>&nbsp; <input id=" + data.frequency_id + " name='chk[]' value=" + frequency_name + " type='checkbox' checked onclick='deleteuserFrequency(" + data.frequency_id + ")' data-id='undefined'>&nbsp;&nbsp; <label>" + frequency_name + "</label></td><td style='padding:0px !important;text-align:center;'>" + frequency_value + "</td></tr>");
                    $('#selected_freq_table').append("<tr id='" + data.frequency_id + "' style='padding:0px !important;height:27px;'><td style='text-align:left;padding:0px !important;'> &nbsp;" + frequency_name + "</td><td style='padding:0px !important;text-align:center'>" + frequency_value + "</td><td style='text-align: center;padding:0px !important;'><input type='hidden'  name='frequency_id[]' value=" + data.frequency_id + "><button type='button' style='padding-top:3px;' class='btn btn-sm btn-danger delete-frequency-row'><i class='fa fa-trash'></i> Remove</button></td></tr>");
                    $(".search_frequency_select").append('<option value="' + data.frequency_id + '" data-frequency-value="' + frequency_value + '" >' + frequency_name + '</option>');
                    Command: toastr["success"]("Frequency Saved.");
                    $('#frequency_name').val('');
                    $('#frequency_value').val('');
                    $('#selected_freq_table #hiderow').hide();

                } else if (data.status == 2) {
                    Command: toastr["success"]("Frequency Saved.");
                    $('#frequency_name').attr('frequency_id', '0');
                    $('#frequency_name').val('');
                    $('#frequency_value').val('');
                    $(".editRow").find('.freq_row_val').html(frequency_value);
                    $(".editRow").attr('data-frequency-name', frequency_name);
                    $(".editRow").attr('data-frequency-value', frequency_value);
                    $(".editRow").removeClass('editRow');
                }
                else {
                    Command: toastr["error"]("Frequency Already Exist.");
                }
            },
            complete: function () {
                $("body").LoadingOverlay("hide");
            }
        });
    }
}


$(document).on('click', '.delete-frequency-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="frequency_id[]"]').val();
        //alert(edit_id);
        deleteFrequencyRowFromDb(edit_id);
        $(tr).remove();

    }
});

//delete single row from db
function deleteFrequencyRowFromDb(id) {
    var url = $('#base_url').val() + "/emr/delete-frequency-row";
    let _token = $('#c_token').val();
    $("body").LoadingOverlay("show", {
        background: "rgba(89, 89, 89, 0.6)",
        imageColor: '#337AB7'
    });
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
                $('input:checkbox[id=' + id + ']').removeAttr('checked');
                $(".search_frequency_select option[value='" + id + "']").remove();
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}


function deleteuserFrequency(id) {
    var url = $('#base_url').val() + "/emr/delete-user-frequency";

    let _token = $('#c_token').val();
    $("body").LoadingOverlay("show", {
        background: "rgba(89, 89, 89, 0.6)",
        imageColor: '#337AB7'
    });
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#selected_freq_table tr#' + id + '').remove();
                Command: toastr["success"]("Deleted.");

                $(".search_frequency_select option[value='" + id + "']").remove();
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}

function takeNextAppointment() {
    var next_review_date = $('#next_review_date').val();
    var url = $('#base_url').val() + "/nursing/selectDrAppointments";
    var visit_id = $("#visit_id").val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id + '&next_review_date=' + next_review_date,
        beforeSend: function () {
            $('#appointment_modal').modal('show');
            $('#next_appointment_list_data').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#next_appointment_list_data').html(data);
            $('#next_appointment_list_data').LoadingOverlay("hide");
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });


        }
    });

}

// function create_appointment(slot_time){
//     $('#create_appointment_modal').modal('toggle');
//     $('#slot_time').val(slot_time);
//     myCalendarData();
// }


function create_appointment(slot_time, token_no, session_id) {
    var patient_name = $('#next_review_patient_name').val();
    var patient_id = $('#next_review_patient_id').val();
    var age = $('#next_review_age').val();
    var gender = $('#next_review_gender').val();
    var phone = $('#next_review_phone').val();
    var address = $('#next_review_address').val();
    var doctor_id = $('#next_review_doctor_id').val();
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/emr/create_new_appointment",
        data: 'patient_name=' + patient_name + '&patient_id=' + patient_id + '&age=' + age + '&gender=' + gender + '&phone=' + phone + '&slot_time=' + slot_time + '&address=' + address + '&doctor_id=' + doctor_id + '&token_no=' + token_no + '&session_id=' + session_id,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            takeNextAppointment();
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('#next_appointment_list_data').LoadingOverlay("hide");
        }
    });

}

$(document).on("click", ".fav_medicine", function () {
    var that = this;
    let status = validateRow(that);
    if (status == false) {
        return false;
    }

    getAllFavGroupsPrescription(that);
});

function getAllFavGroupsPrescription(event) {

    event = $(event).parents('tr');

    var med_code = $(event).find('input[name="selected_item_code[]"]').val();
    var med_name = $(event).find('input[name="selected_item_name[]"]').val();
    var dose = $(event).find('input[name="selected_item_dose[]"]').val();
    var duration = $(event).find('input[name="selected_item_duration[]"]').val();
    var quantity = $(event).find('input[name="selected_item_quantity[]"]').val();
    var freq_value = $(event).find('input[name="selected_frequency_value[]"]').val();
    var freq_name = $(event).find('input[name="selected_item_frequency[]"]').val();
    var freq_id = $(event).find('input[name="selected_frequency_id[]"]').val();
    var route = $(event).find('select[name="selected_item_route[]"]').val();
    var remarks = $(event).find('input[name="selected_item_remarks[]"]').val();

    var fav_med = {};
    fav_med.med_code = med_code;
    fav_med.med_name = med_name;
    fav_med.dose = dose;
    fav_med.duration = duration;
    fav_med.quantity = quantity;
    fav_med.freq_value = freq_value;
    fav_med.freq_name = freq_name;
    fav_med.freq_id = freq_id;
    fav_med.route = route;
    fav_med.remarks = remarks;

    var fav_med_json = JSON.stringify(fav_med);
    fav_med_json = btoa(fav_med_json);

    let url = $('#base_url').val();
    let _token = $('#c_token').val();

    $.ajax({
        type: "POST",
        url: url + "/emr/getAllFavGroupsPrescription",
        data: {
            medicine_details: fav_med,
            _token: _token
        },
        beforeSend: function () {
            $(event).find(".fav_medicine i").removeClass('fa-star').addClass('fa-spinner').addClass('fa-spin');
            $('.presc_add_fav_group_list').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });

        },
        success: function (data) {
            $(event).find(".fav_medicine i").removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-star');
            $('.presc_add_fav_group_list').LoadingOverlay("hide");
            $(".presc_add_fav_group_list").empty();
            if (data) {
                $.each(data, function (key, val) {
                    var checked = '';
                    if (val.favorite_status == 1) {
                        checked = "checked='checked'";
                    }
                    $(".presc_add_fav_group_list").append('<li onclick="updatePrescriptionToFavGroup(\'' + val.id + '\', \'' + fav_med_json + '\');" class="head_tab_' + val.id + '" data-id="' + val.id + '" style="display:flex;height: 30px; cursor:pointer;"><input style="margin-right: 5px;margin-left: 5px;"  class="styled" type="checkbox" name="fav_group_id" value="' + val.id + '" ' + checked + '> <a aria-expanded="true" style="width:93%; padding:0px;" onclick=""><h5 style="margin-top:2px;margin-left:5px;">' + val.group_name + '</h5></a></li>')
                })
                $(".fav_prescription_box").show();
            }
        },
        complete: function () {

        }
    });
}

function updatePrescriptionToFavGroup(medicine_fav_group_id, fav_med) {

    fav_med = JSON.parse(atob(fav_med));

    let url = $('#base_url').val();
    let _token = $('#c_token').val();

    $.ajax({
        type: "POST",
        url: url + "/emr/updateMedicineToFavGroup",
        data: {
            medicine_details: fav_med,
            group_id: medicine_fav_group_id,
            _token: _token
        },
        beforeSend: function () {
            $('.presc_add_fav_group_list').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });

        },
        success: function (data) {
            $('.presc_add_fav_group_list').LoadingOverlay("hide");
            if (data.status) {
                getAllFavGroupsPrescription();
                Command: toastr["success"]("Success");
            }
        },
        complete: function () {

        }
    });
}


//close presc_close
$(document).on('click', '.presc_close_fav', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".fav_prescription_box").hide();
});


$(document).on('click', '.presc_add_group_btn', function () {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    var group_name = $('.presc_fav_add_group_text').val();
    if (group_name.trim() != '') {
        $.ajax({
            type: "POST",
            url: url + "/emr/createNewMedicineGroup",
            data: {
                group_name: $('.presc_fav_add_group_text').val(),
                _token: _token
            },
            beforeSend: function () {
                $('.presc_add_group_icon').removeClass('fa-plus').addClass('fa-spinner').addClass('fa-spin');

            },
            success: function (data) {
                $('.presc_add_group_icon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-plus');
                $('.presc_fav_add_group_text').val('');
                if (data.status == 1) {
                    getAllFavGroupsPrescription();
                }

            },
            complete: function () {

            }
        });
    } else {
        Command: toastr["error"]("Enter Group Name");
    }

})


function complete_prescription_select_all() {
    if ($('#complete_prescription_select_all:checked').length > 0) {
        $('.complete_prescription').prop('checked', true);
    } else {
        $('.complete_prescription').prop('checked', false);
    }
}

function reorderMedication() {
    var reorderMedicationList = [];
    $("input:checkbox[class=complete_prescription]:checked").each(function () {
        reorderMedicationList.push($(this).val());
    });
    var visit_id = $("#visit_id").val();
    var encounter_id = $("#encounter_id").val();
    var patient_id = $("#patient_id").val();
    reorderMedicationList = JSON.stringify(reorderMedicationList);
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/emr/reorderMedication",
        data: 'reorderMedicationList=' + reorderMedicationList + '&visit_id=' + visit_id + '&patient_id=' + patient_id + '&encounter_id=' + encounter_id + '&ns_prescription=1',
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            console.log(html);
            var obj = JSON.parse(html);
            if (obj.status == 1) {
                Command: toastr["success"](obj.message);
            }
            else {
                Command: toastr["error"](obj.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");

        }
    });
}

function addToMedication() {
    var reorderMedicationList = [];
    var itemCodesArray = []; 
    $("input:checkbox[class=complete_prescription]:checked").each(function () {
        reorderMedicationList.push($(this).val());
    });
    reorderMedicationList = JSON.stringify(reorderMedicationList);

    if (jQuery("input:checkbox[class=complete_prescription]:checked").length == 0) {
        Command: toastr["warning"]("Select Medicine!");
        return;
    }

    // alert(reorderMedicationList.length);
    // return;

    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/emr/addToMedication",
        data: 'reorderMedicationList=' + reorderMedicationList,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            console.log(data);

            let res = JSON.parse(data);
            let response = '';
            let mode = 'copy';

            if (res.length > 0) {
                $('#medicine-listing-table tbody').html("");
                for (var i = 0; i < res.length; i++) {

                    let id = res[i].id;
                    let billconverted_status = res[i].billconverted_status;
                    let duration = res[i].duration;
                    let frequency = res[i].frequency;
                    let frequency_value = res[i].frequency_value;
                    let frequency_id = res[i].frequency_id;
                    let generic_name = res[i].generic_name;
                    let item_desc = res[i].item_desc;
                    let medicine_name = res[i].medicine_name;
                    let medicine_code = res[i].medicine_code;
                    let quantity = res[i].quantity;
                    let stop_flag = res[i].stop_flag;
                    let notes = res[i].notes;
                    let dose = res[i].dose;
                    let route = res[i].route;
                    let price = res[i].price;
                    let iv_start_at = res[i].iv_start_at;
                    let generic_id = res[i].generic_id;                    
                    let sl = parseInt(i) + 1;
                    itemCodesArray.push(medicine_code);

                    let head_id = res[i].head_id;
                    let mode = res[i].mode;

                    let data = {
                        med_code: medicine_code,
                        out_medicine_name: medicine_name,
                        med_name: item_desc,
                        duration: duration,
                        dose: dose,
                        route: route,
                        quantity: quantity,
                        freq_value: frequency_value,
                        freq_name: frequency,
                        freq_id: frequency_id,
                        remarks: notes,
                        price: price,
                        id: id,
                        head_id: head_id,
                        iv_start_at: iv_start_at,
                        generic_id: generic_id
                    };

                    getNewRowInserted(mode, data);

                }
                let intend_type = res[0].intend_type;
                $('input[name="p_search_type"][value="' + intend_type + '"]').prop('checked', true);

                if (mode == 'copy') {
                    $("#prescription_head_id").val(0);
                    checkAllergicMedicine(itemCodesArray);
                }
            }

        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('#complete_medication_history_modal').modal('hide');
        }
    });

}

$('#search_frequency').on('keyup', function () {
    var searchVal = $(this).val();

    if (searchVal != '') {
        $("#ListFrequencySearchData tr").hide();
        $('#ListFrequencySearchData tr[data-frequency-name^="' + searchVal.toLowerCase() + '"]').show();
        $('#ListFrequencySearchData tr[data-frequency-name^="' + searchVal.toUpperCase() + '"]').show();
    } else {
        $("#ListFrequencySearchData tr").show();
    }
});


$(document).on('keyup', 'input[name="selected_item_frequency[]"]', function () {
    var searchVal = $(this).val();

    if (searchVal != '') {
        $(this).parents('tr').find('.FrequencyTableRowItem').find('tr').hide();
        $(this).parents('tr').find('.FrequencyTableRowItem tr[data-frequency-name^="' + searchVal.toLowerCase() + '"]').show();
        $(this).parents('tr').find('.FrequencyTableRowItem tr[data-frequency-name^="' + searchVal.toUpperCase() + '"]').show();
    } else {
        $(this).parents('tr').find(".FrequencyTableRowItem tr").show();
    }
});



$('#searchFreq').on('keyup', function () {
    var searchVal = $(this).val();

    if (searchVal != '') {
        $(".globalFreqList tr").hide();
        $('.globalFreqList tr[data-frequency-name^="' + searchVal.toLowerCase() + '"]').show();
        $('.globalFreqList tr[data-frequency-name^="' + searchVal.toUpperCase() + '"]').show();
    } else {
        $(".globalFreqList tr").show();
    }
});

$(document).on('dblclick', '.globalFreqList tr', function (event) {
    var frequency_name = $(this).attr('data-frequency-name');
    var frequency_id = $(this).attr('data-frequency-id');
    var frequency_value = $(this).attr('data-frequency-value');

    $("#frequency_name").val(frequency_name);
    $("#frequency_value").val(frequency_value);
    $("#frequency_name").attr("frequency_id", frequency_id);
    $(".globalSelectedFreqList tr").removeClass('editRow');
    $(this).addClass('editRow');
});

$(document).on('dblclick', '.globalSelectedFreqList tr', function (event) {
    var frequency_name = $(this).attr('data-frequency-name');
    var frequency_id = $(this).attr('data-frequency-id');
    var frequency_value = $(this).attr('data-frequency-value');

    $("#frequency_name").val(frequency_name);
    $("#frequency_value").val(frequency_value);
    $("#frequency_name").attr("frequency_id", frequency_id);
    $(".globalFreqList tr").removeClass('editRow');
    $(this).addClass('editRow');
});

$('#searchSelectedFreq').on('keyup', function () {
    var searchVal = $(this).val();

    if (searchVal != '') {
        $(".globalSelectedFreqList tr").hide();
        $('.globalSelectedFreqList tr[data-frequency-name^="' + searchVal.toLowerCase() + '"]').show();
        $('.globalSelectedFreqList tr[data-frequency-name^="' + searchVal.toUpperCase() + '"]').show();
    } else {
        $(".globalSelectedFreqList tr").show();
    }
});


function removeMedicineFromBookmark(bookmark_id, that) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/removeMedicineFromBookmark",
        data: {
            bookmark_id: bookmark_id,
            _token: _token
        },
        beforeSend: function () {
            $(that).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
            $(that).parents('tr').remove();
        },
        complete: function () {
            Command: toastr["success"]("Removed.");
        }
    });

}

function delete_prescription_group(id) {
    let url = $('#base_url').val();
    $.ajax({
        type: "GET",
        url: url + "/emr/deleteMedicationGroup",
        data: {
            group_id: id,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Removed.");
                $('#favourite_group_' + id).remove();
            }
            else {
                Command: toastr["error"]("Error.");
            }
        },
        complete: function () {}
    });
}

function close_med_search() {
    $(".medicine-list-div-row-listing").hide();
}

function loadMedicationData() {
    var url = base_url + "/nursing_new/ViewPatientMedicationMin"
    var dataparams = {
        patient_id: pub_patient_id,
        visit_id: pub_visit_id,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            // $('#patient_medication_min').modal('toggle');
            $("#patient_medication_min_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#patient_medication_min_content").html(data);
            if (is_casuality_user == '1' && parseInt(casuality_prescription_indent_type) > 0) {
                $('input[name="p_search_type"][value="' + parseInt(casuality_prescription_indent_type) + '"]').prop('checked', true);
            }
        },
        complete: function () {
            $("#patient_medication_min_content").LoadingOverlay("hide");
            getNewRowInserted('add');
            fetchFrequencyList();
            // $('input[name="selected_item_name"]').focus();
            var patient_allergy_list = $('#hiddenAllergy_medcation_list').val();
            patient_allergy_list =atob(patient_allergy_list);
            patient_allergy_list =JSON.parse(patient_allergy_list);
            item_allergyArray = [];
            generic_allergyArray = [];
            if (typeof (patient_allergy_list) != 'undefined') {
                if (patient_allergy_list.length > 0) {
                    $.each(patient_allergy_list, function (key, val) {
                        console.log(val);
                        if (val.allergy_type == 'B') {
                            item_allergyArray.push(val.type_id);
                        }else if (val.allergy_type == 'G') {
                            generic_allergyArray.push(parseInt(val.type_id));
                        }
                    })
                }
            }


            $('#close_btn_med_search').click(function () {
                $(".medicine-list-div-row-listing").hide();
            });
            $("#medicine-listing-table tr").find('input[name="selected_item_name[]"]:first').focus();
        }
    });
}

function showMedicationChart() {
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/showMedicationChart",
        data: "visit_id=" + pub_visit_id,
        beforeSend: function () {
            $('#medication_chart_content').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#medication_chart_content').LoadingOverlay("hide");
            $('#medication_chart_content').html(html);
            if ($('#hide_ns_medication_chart').val() == 1) {
                $('#ns_medication_chart').hide();
            } else {
                $('#ns_medication_chart').show();
            }
        },
        complete: function () {
            $('.datepicker').datetimepicker({
                format: 'MMMM-DD-YYYY'
            });

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });

            $("[data-toggle='popover']").popover('toggle');
            $("[data-toggle='popover']").popover('hide');
        }
    });
}

var add_med = 0;

function addMedicineGiven(medication_detail_id, frequency_value, frequency = 0) {
    if (add_med == 0) {
        add_med = 1;
        var url = $('#base_url').val() + "/nursing/addMedicineGiven";
        var visit_id = pub_visit_id;
        var patient_id = pub_patient_id;
        var checkStatus = 0;
        var giventime = '';
        var expected_time = '';


        if ($('#check_' + medication_detail_id).is(':checked')) {
            checkStatus = 1;
        } else {
            checkStatus = 0;
        }

        givendate = $('#given_date_' + medication_detail_id).val();
        giventime = $('#given_time_' + medication_detail_id).val();
        $('#btn_medicine_given_' + medication_detail_id).attr('disabled', true);
        $.ajax({
            type: "POST",
            url: url,
            async: true,
            data: 'medication_detail_id=' + medication_detail_id +
                '&visit_id=' + visit_id +
                '&patient_id=' + patient_id +
                '&given_time=' + giventime +
                '&given_date=' + givendate +
                '&expected_time=' + expected_time +
                '&frequency_value=' + frequency_value +
                '&checkStatus=' + checkStatus,
            beforeSend: function () {
                $('#medication_given_container_' + medication_detail_id).LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });


                $('#fa-medicine_given_' + medication_detail_id).removeClass('fa-medkit');
                $('#fa-medicine_given_' + medication_detail_id).addClass('fa-spinner fa-spin');


            },
            success: function (data) {

                $('#medication_given_container_' + medication_detail_id).LoadingOverlay("hide");
                $('#fa-medicine_given_' + medication_detail_id).removeClass('fa-spinner fa-spin');
                $('#fa-medicine_given_' + medication_detail_id).addClass('fa-medkit');
                //console.log(data);
                if (data == 0) {
                    Command: toastr["error"]("Error.! Something went wrong!.");
                }
                else if (data == 3) {
                    Command: toastr["warning"]("Course completed !");
                }
                else {
                    var obj = JSON.parse(data);

                    // $('#given_time_' + medication_detail_id).css('color', '#a8a8a8 !important');
                    // $('#given_date_' + medication_detail_id).css('color', '#a8a8a8 !important');

                    $('#given_time_' + medication_detail_id).val(obj.increment_time);
                    $('#given_date_' + medication_detail_id).val(obj.increment_date);

                    if (frequency != 'STAT') {
                        Command: toastr["info"]("Note ! Current Medication is given at " + obj.inserted_time + ".\n Next course should be given at " + obj.next_expected_time);
                    }

                    $('#medication_given_container_' + medication_detail_id).append(obj.medicine_given_view);

                    if (frequency == 'STAT') {
                        stopMedicine(medication_detail_id);
                    }
                }
            },
            complete: function () {
                $('#btn_medicine_given_' + medication_detail_id).attr('disabled', false);
                add_med = 0;
            }
        });
    }
}

function stopMedicine(medicine_detail_id) {

    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/stopMedicine",
        data: "medicine_detail_id=" + medicine_detail_id,
        beforeSend: function () {
            $('body').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            // console.log(html);
            $('body').LoadingOverlay("hide");
            if (html == 1) {
                Command: toastr["success"]("Medication stopped!");
            }
            else {
                Command: toastr["warning"]("Something went wrong!");
            }
        },
        complete: function () {
            showMedicationChart();
        }
    });
}

function editGivenMedication(medication_chart_id, change_date, change_time) {
    $('#modalChangeMedicationTime').modal('toggle');
    $('#med_chart_change_date').val(change_date);
    $('#med_chart_change_time').val(change_time);
    $('#change_med_chart_id').val(medication_chart_id);
}

function editGivenMedicationSave() {

    var med_chart_change_date = $('#med_chart_change_date').val();
    var med_chart_change_time = $('#med_chart_change_time').val();
    var medication_chart_id = $('#change_med_chart_id').val();

    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/editGivenMedication",
        data: "medication_chart_id=" + medication_chart_id + '&med_chart_change_date=' + med_chart_change_date + '&med_chart_change_time=' + med_chart_change_time,
        beforeSend: function () {

            $('#modalChangeMedicationTime').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });

        },
        success: function (html) {
            //console.log(html);return;
            $('#modalChangeMedicationTime').LoadingOverlay("hide");
            if (html != 0) {
                Command: toastr["success"]("Updated Successfully!");
                $('#modalChangeMedicationTime').modal('hide');
                //$( "#chart_id_"+medication_chart_id ).remove();
                obj = JSON.parse(html);
                var medication_detail_id = obj.medication_detail_id;
                $("#chart_id_" + medication_chart_id).html('');
                $("#chart_id_" + medication_chart_id).html(obj.medicine_given_view);

            }
            else {
                Command: toastr["warning"]("Something went wrong!");
            }
        },
        complete: function () {

        }
    });
}

function deleteGivenMedication(medication_chart_id) {
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/deleteGivenMedication",
        data: "medication_chart_id=" + medication_chart_id,
        beforeSend: function () {

            $('body').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });

        },
        success: function (html) {
            $('body').LoadingOverlay("hide");
            if (html == 1) {
                Command: toastr["success"]("Deleted!");
                $("#chart_id_" + medication_chart_id).remove();

            }
            else {
                Command: toastr["warning"]("Something went wrong!");
            }
        },
        complete: function () {

        }
    });
}

function gotoMedicationChart() {
    var patient_id = pub_patient_id;
    var url = $('#base_url').val() + "/nursing/MedicationChartReport/?patient_id=" + patient_id + "&from_op_list=1";
    window.open(url, '_blank');
}

function saveNewGroup(group_name, e) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    if (group_name.trim() != '') {
        $.ajax({
            type: "POST",
            url: url + "/emr/createNewMedicineGroup",
            data: {
                group_name: group_name,
                _token: _token
            },
            beforeSend: function () {
                $('#addGrpBtnIcon').removeClass('fa-plus').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                $('#addGrpBtnIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-plus');
                $('#addNewGrp').val('');
                if (data.status == 1) {
                    loadMedFavorites(1);
                }

            },
            complete: function () {

            }
        });
    } else {
        Command: toastr["error"]("Enter Group Name");
    }
}

function addNewMediceneGrp() {
    if ($('.activeTr').hasClass('bg-green')) {
        $(".prescription_list_table_body_grp").append("<tr class='row_class_addGrp'><td></td> <td style='text-align: center; line-height: 25px;' class='row_count_class_addGrp'></td><td><input type='text' autocomplete='off' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' data-item-code='' style='border-bottom: 1px solid #21a4f1 !important;'/></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' style='border-bottom: 1px solid #21a4f1 !important;'/></td><td class='text-center'><i  onclick='removeAddGrpRow(this)' class='fa fa-times red removeAddGrpRowBtn'></i></td></tr>");
        serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
    } else {
        toastr.warning('Please make sure you selected a group.');
    }
}

function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}

function itemListGrpWise(e, group_id) {
    $('.activeTr').removeClass('bg-green');
    $(e).closest('.activeTr').addClass('bg-green');
    if (group_id) {
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/emr/medicine-favorite-list";
        $.ajax({
            type: "POST",
            url: url,
            data: 'load_group_items=1&group_id=' + group_id + '&_token=' + _token + "&new_bookmark=1",
            beforeSend: function () {
                $('#prescription_list_table_grp').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (response) {
                $('#prescription_list_table_grp').html(response.html);
                addNewMediceneGrp();
            },
            complete: function (e) {
                $('#prescription_list_table_grp').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            }
        });
    }
}

function saveChangedGrpName(e, group_id) {
    if (group_id && ($(e).parents('tr').find('.editGrpName').attr('data-oldGrpName') != $(e).parents('tr').find('.editGrpName').val())) {
        var changed_name = $(e).parents('tr').find('.editGrpName').val();
        var old_name = $(e).parents('tr').find('.editGrpName').attr('data-oldGrpName');
        bootbox.confirm({
            title: 'Change Group Name',
            size: 'medium',
            message: 'Are you sure, you want change this group name ?',
            buttons: {
                'cancel': {
                    label: 'No',
                    style: 'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function (result) {
                if (result) {
                    var base_url = $("#base_url").val();
                    var url = base_url + "/emr_lite/saveChangedItemName";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            group_id: group_id,
                            changed_name: changed_name
                        },
                        beforeSend: function () {
                            $(e).parents('tr').find('.grpEditBtn').attr('disabled', true);
                            $(e).find('i').removeClass('fa fa-save');
                            $(e).find('i').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            if (data.status == 200) {
                                toastr.success('Success');
                            } else if (data.status == 101) {
                                toastr.warning('Named Group already Exist.');
                                $(e).parents('tr').find('.editGrpName').val(old_name);
                            } else {
                                toastr.warning('Sorry,Something went wrong.');
                                $(e).parents('tr').find('.editGrpName').val(old_name);
                            }

                        },
                        complete: function () {
                            $(e).parents('tr').find('.grpEditBtn').attr('disabled', false);
                            $(e).find('i').removeClass('fa fa-spinner fa-spin');
                            $(e).find('i').addClass('fa fa-save');

                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                            $(e).parents('tr').find('.editGrpName').val(old_name);
                        },
                    });
                } else {
                    $(e).parents('tr').find('.editGrpName').val(old_name);
                }
            }
        });
    } else {
        toastr.warning('No change found.');
    }
}

function deleteGrpName(e, group_id, from_type = 0) {
    if (group_id) {
        bootbox.confirm({
            title: 'Delete Group',
            size: 'medium',
            message: 'Are you sure, you want to delete this group ?',
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    style: 'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function (result) {
                if (result) {
                    var base_url = $("#base_url").val();
                    var url = base_url + "/emr_lite/deleteGrpName";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            group_id: group_id
                        },
                        beforeSend: function () {
                            if (from_type == 1) {
                                $(e).attr('disabled', true);

                            } else {
                                $(e).parents('tr').find('.grpdeleteBtn').attr('disabled', true);

                            }
                            $(e).find('i').removeClass('fa fa-trash');
                            $(e).find('i').addClass('fa fa-spinner fa-spin');


                        },
                        success: function (data) {
                            if (data.status == 200) {
                                toastr.success('Deleted.');
                                if (from_type == 1) {
                                    $('#prescription_bookmark_item' + group_id).fadeOut();
                                } else {
                                    $(e).parents('tr').remove();
                                    serialNoCalculation('row_class_newGrp', 'row_count_class_newGrp');
                                }
                                $('#prescription_bookmark_item' + group_id).fadeOut();
                            } else {
                                toastr.warning('Sorry,Something went wrong.');
                            }

                        },
                        complete: function () {
                            if (from_type == 1) {
                                $(e).attr('disabled', false);

                            } else {
                                $(e).parents('tr').find('.grpdeleteBtn').attr('disabled', false);
                            }
                            $(e).find('i').removeClass('fa fa-spinner fa-spin');
                            $(e).find('i').addClass('fa fa-trash');


                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                        },
                    });
                }
            }
        });
    }
}

$(document).on('keyup', 'input[name="medicine_name"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var patient_id;
    // var search_type = $("input[name='medicine_search_type']:checked").val();
    var search_type = 'both';
    var med_list = $('.medicine-list-div-row-listing-new');
    if ($('#addItemToFavouriteNursingModel').is(':visible')) {
        patient_id = 1;
    } else {
        patient_id = $('#patient_id').val();
    }

    let tr = $(obj).closest('tr');
    window.tr = tr;

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(med_list).show();
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr_lite/medicine-search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    search_key_string: search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('.medicine-list-div-row-listing-new').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    // $('.item_description').blur();
                },
                success: function (data) {
                    let response = data.medicine_list;
                    var disable_zero_stock_items_prescription = data.disable_zero_stock_items_prescription ? data.disable_zero_stock_items_prescription : 0;
                    let res_data = "";
                    var search_list = $('.list-medicine-search-data-row-listing-new');
                    var show_price = $('#show_price_prescription').val();
                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {
                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let med_price = response[i].med_price;
                            let subcategory_id = response[i].subcategory_id;
                            let stock = response[i].stock;
                            let route = response[i].route;
                            let frequency = response[i].frequency;
                            let duration = response[i].duration;
                            let directions = response[i].directions;
                            let category_name = response[i].category_name;
                            let subcategory_name = response[i].subcategory_name;
                            let calculate_quantity = response[i].calculate_quantity;
                            var nostock = '';

                            if (!stock) {
                                nostock = 'no-stock'
                            }
                            var medic_price = '';
                            if (show_price == 1) {
                                medic_price = "<td>" + med_price + "</td>";
                            }
                            // <td>' + price + '</td>
                            if (disable_zero_stock_items_prescription == 0) {
                                res_data += '<tr data-item-code="' + item_code + '" data-calculate-quantity="' + calculate_quantity + '" data-item-desc="' + item_desc + '" data-genreric-name="' + generic_name + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '"><td class="' + nostock + '">' + item_desc + '</td><td>' + generic_name + '</td><td>' + category_name + '</td><td>' + subcategory_name + '</td><td>' + stock + '</td>' + medic_price + '<input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                            } else {
                                if (stock != 0 && stock != '' && stock != 0.00) {

                                    res_data += '<tr data-item-code="' + item_code + '" data-calculate-quantity="' + calculate_quantity + '" data-item-desc="' + item_desc + '" data-genreric-name="' + generic_name + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '"><td class="' + nostock + '">' + item_desc + '</td><td>' + generic_name + '</td><td>' + category_name + '</td><td>' + subcategory_name + '</td><td>' + stock + '</td>' + medic_price + '<input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                                }
                            }

                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="6">No Data Found..!</td></tr>';
                    }

                    if (response.length > 0) {
                        $('input[name="medicine_name"]').removeClass('outside-medicine');
                    } else {
                        $('input[name="medicine_name"]').addClass('outside-medicine');
                        $(med_list).hide();
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    // $(".theadscroll").animate({ scrollTop: 0 }, "slow");
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });

                },
                complete: function () {
                    $('.theadfix_wrapper').floatThead("reflow");
                    $('.medicine-list-div-row-listing-new').LoadingOverlay("hide");

                }
            });
        }, 500)

    }

});

$(document).on('click', '.close_btn_med_search-new', function () {
    $(".medicine-list-div-row-listing-new").hide();
});

$(document).on('click', '.list-medicine-search-data-row-listing-new > tr', function (event) {
    console.log("click triggered")
    event.preventDefault();
    var item_code = $(event.currentTarget).attr('data-item-code');
    var calculate_quantity = $(event.currentTarget).attr('data-calculate-quantity');
    var stock = $(event.currentTarget).attr('data-stock');

    if ($(".item_description[data-item-code='" + item_code + "']").length > 0) {
        var confirm = window.confirm("Medicine already exist. Do you want to continue ?");
        if (!confirm) {
            return false;
        }
    }

    $(window.tr).find('.item_description').val($(event.currentTarget).attr('data-item-desc'));
    $(window.tr).find('.item_description').attr('title', $(event.currentTarget).attr('data-item-desc'));
    $(window.tr).find('.item_description').attr('data-item-code', item_code);
    $(window.tr).find('.frequency').attr('data-calculate-quantity', calculate_quantity);
    $(window.tr).find('.quantity').focus();
    $(".medicine-list-div-row-listing-new").hide();
    addNewMediceneGrp();

    if (parseInt(stock) <= 0) {
        $(window.tr).find('.item_description').css('color', 'red');
    }
});

function removeAddGrpRow(e) {
    if ($(e).parents('tr').find('.item_description').val()) {
        bootbox.confirm({
            title: 'Remove Medicine',
            size: 'medium',
            message: 'Are you sure, you want to remove this Medicine ?',
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    style: 'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function (result) {
                if (result) {
                    $(e).parents('tr').remove();
                    serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
                }
            }
        });
    } else {
        $(e).parents('tr').remove();
        serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
    }

}

function saveNewItemsToGroup(e) {
    var groupItems = new Array();
    var grp_id = $('#addItemTogrp_grpId').val();

    $('.prescription_list_table_body_grp tr').each(function () {
        if ($(this).find('.item_description').val()) {
            var dosage = $(this).find('.dosage').val();
            dosage = (dosage != undefined) ? dosage : '';
            var dosage_unit = $(this).find('.dosage_unit').val();
            dosage_unit = (dosage_unit != undefined) ? dosage_unit : 0;
            groupItems.push({
                'medicine_name': $(this).find('.item_description').val(),
                'medicine_code': $(this).find('.item_description').data('item-code'),
                'frequency': $(this).find('.frequency').val(),
                'duration': $(this).find('.duration').val(),
                'duration_unit': $(this).find('.duration_unit').val(),
                'quantity': $(this).find('.quantity').val(),
                'direction': $(this).find('.direction').val(),
                'frequency_value': $(this).find('.frequency').attr('data-frequency-value'),
                'frequency_id': $(this).find('.frequency').attr('data-frequency-id'),
                'dosage': dosage,
                'dosage_unit': dosage_unit,
            })
        }
    })
    if (grp_id && groupItems.length > 0) {
        var base_url = $("#base_url").val();
        var stringGrpItems = JSON.stringify(groupItems);
        var url = base_url + "/emr_lite/saveItemsToGroup";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                group_id: grp_id,
                stringGrpItems: stringGrpItems
            },
            beforeSend: function () {
                $('#saveNewItemsToGroupBtn').attr('disabled', true);
                $(e).find('i').removeClass('fa fa-plus');
                $(e).find('i').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (data.status == 200) {
                    toastr.success('Success');
                    $("#prescription_list_table_grp").html('');
                    $(".row_count_class_newGrp[data-grp-id='" + grp_id + "']").click();
                } else {
                    toastr.warning('Sorry,Something went wrong.');
                }

            },
            complete: function () {
                $('#saveNewItemsToGroupBtn').attr('disabled', false);
                $(e).find('i').removeClass('fa fa-spinner fa-spin');
                $(e).find('i').addClass('fa fa-plus');

            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    }

}

function addFavouriteToPrescription() {
    var checkedRows = [];
    var checkedRowId = [];
    var itemCodesArray = []; 
    $(".prescription_list_table_body_grp input.prescription_bookmark_item_checkbox:checked").each(function () {
        var row = $(this).closest("tr");
        var item_code = row.find(".item_description").attr("data-item-code");
        var row_id = row.find(".prescription_bookmark_item_checkbox").attr("data-row-id");
        if (item_code.trim() != '') {
            var attributes = {
                item_code: item_code,
                item_desc: row.find(".item_description").val(),
                quantity: row.find(".quantity").val(),
                quantity: row.find(".quantity").val(),
            };
            itemCodesArray.push(item_code);
            checkedRows.push(attributes);
            checkedRowId.push(row_id);
        }
    });
    if (checkedRows.length > 0) {
        // console.log(checkedRowId, checkedRows);
        removeEmptyMedicineRows();
        var administration_in_nurse_dashboard = $('#administration_in_nurse_dashboard').val();
        var favcheckedIDs = checkedRowId.join(',');

        var Rowcount = 1;
        if ($("#medicine-listing-table tr.selected_medicine_row").length > 0) {
            var Rowcount = $("#medicine-listing-table tr.selected_medicine_row").length + 1;
        }
        let _token = $('#c_token').val();

        var url = $('#base_url').val() + "/emr/medicine-favorite-list";
        $("body").LoadingOverlay("show", {
            background: "rgba(89, 89, 89, 0.6)",
            imageColor: '#337AB7'
        });
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: "presc_fav_selected_items_add_to_list=" + 1 + "&favcheckedIDs=" + favcheckedIDs + "&rows=" + Rowcount + "&_token=" + _token + "&administration_in_nurse_dashboard=" + administration_in_nurse_dashboard + "&new_bookmark=1",
            beforeSend: function () {

            },
            success: function (medData) {
                if (medData.status == 1) {
                    $('#medicine-listing-table').append(medData.html);
                    $('.favorite-medicine-list-popup').hide();
                    calculateTotalMedicineAmount();
                    let current_freq_list = $("#ListFrequencySearchData").html();
                    $(".list-frequency-search-data-row-listing").html(current_freq_list);
                    $('#medicine-listing-table').find(".bookmark_row").trigger('dblclick');

                    $("#addItemToFavouriteNursingModel").modal('toggle');
                    // let sel_search_route = document.getElementById('search_route');
                    // route_options = sel_search_route.innerHTML;
                    // $(".list-route-search-data-row-listing").html(route_options);

                    // $(".list-route-search-data-row-listing").each(function () {
                    //     $(this).val($(this).attr('data-selected-route'));
                    // })
                }
            },
            complete: function () {
                checkAllergicMedicine(itemCodesArray);
                // $(".fav_check").each(function () {
                //     $(this).attr('checked', false);
                // });
                $("body").LoadingOverlay("hide");
            }
        });

    } else {
        toastr.warning('Please select medicines');
    }
}

function removeEmptyMedicineRows() {
    $('#medicine-listing-table tbody > tr').each(function () {
        var inputField = $(this).find('input[name="selected_item_name[]"]');
        if (inputField.val() === '') {
            $(this).remove();
        }
    });
}

function cloneItemListGrpWisae(e, group_id) {
    if (group_id) {
        bootbox.prompt({
            title: "Clone Group",
            message: 'Are you sure, you want to clone this group ?',
            inputType: 'text',
            placeholder: 'Enter group name',
            className: 'clone_new_group',

            buttons: {
                confirm: {
                    label: "Save",
                    className: 'btn-primary',
                    callback: function () {}
                },
                cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function () {}
                },
            },
            callback: function (result) {
                if (result != "" && result != undefined) {
                    var base_url = $("#base_url").val();
                    var url = base_url + "/emr_lite/cloneGrpName";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            group_id: group_id,
                            group_name: result,
                        },
                        beforeSend: function () {
                            $(e).find('.grpCloneBtn').attr('disabled', true);
                            $(e).find('i').removeClass('fa fa-clone');
                            $(e).find('i').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                toastr.success('Cloned successfully.');
                                loadMedFavorites(1);
                                serialNoCalculation('row_class_newGrp', 'row_count_class_newGrp');
                            } else {
                                toastr.warning('Sorry,Something went wrong.');
                            }

                        },
                        complete: function () {
                            $(e).find('.grpCloneBtn').attr('disabled', false);
                            $(e).find('i').removeClass('fa fa-spinner fa-spin');
                            $(e).find('i').addClass('fa fa-clone');
                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                        },
                    });
                }
            }
        });
    }
}

$(document).on('click', '.prescription_bookmark_item_checkbox_all', function () {
    var isChecked = $(this).prop('checked');
    var index = $(this).closest('th').index();
    $('td:nth-child(' + (index + 1) + ') input[type="checkbox"].prescription_bookmark_item_checkbox').prop('checked', isChecked);
});

// $('input[name=selected_item_quantity[]]').key('presss', function(e) {
//     if (event.which == 9) {
//         console.log(event.which);
//         var focus_element = $(':focus');
//         if(focus_element.attr('name')=='selected_item_quantity[]' || focus_element.attr('name')=='selected_item_remarks[]'){
//             $(focus_element).parents('tr.selected_medicine_row').next().find('input[name="selected_item_name[]"]').focus();
//         }

//     }
// });
// $('input[name="selected_item_quantity\\[\\]"]').on('keydown', function(event) {
//     if (event.which === 9) {
//         $(this).parents('tr.selected_medicine_row').next().find('input[name="selected_item_name[]"]').val('ffff');

//     }
// });

$(document).on('keydown', 'input[name="selected_item_quantity[]"]', function (event) {
    if (event.which == 9) {
        event.preventDefault();
        var focus_element = $(':focus');
        if($(focus_element).parents('tr.selected_medicine_row').next()){
            $(focus_element).parents('tr.selected_medicine_row').next().find('input[name="selected_item_name[]"]').focus();
        } else {
            $(focus_element).parents('tr.selected_medicine_row').find('input[name="selected_item_remarks[]"]').focus();
        }

    }
});

$(document).on('keydown', function (event) {
    if ($('.medicine-list-div-row-listing-new').css('display') == 'block') {
        var row_length = $(".list-medicine-search-data-row-listing-new").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                if ($(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').is(':first-child')) {
                    $(".list-medicine-search-data-row-listing-new").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing-new").find('tr').last().addClass('active_row_item');
                } else {
                    $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').prev().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').last().removeClass('active_row_item');
                }
            } else if (event.which == 40) { // down
                if ($(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').is(':last-child')) {
                    $(".list-medicine-search-data-row-listing-new").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing-new").find('tr').first().addClass('active_row_item');
                } else {
                    $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').next().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').first().removeClass('active_row_item');
                }
            } else if (event.which == 13) { // enter
                $(".list-medicine-search-data-row-listing-new").find('tr.active_row_item').trigger('click');
            } else if (event.which == 8) { // backspace
                $('.medicine-list-div-row-listing-new').hide();
                $(window.tr).find('.item_description').focus();
            } else if (event.which == 27) { // escape
                $('.medicine-list-div-row-listing-new').hide();
                $(window.tr).find('.item_description').focus();
            }
        }
    }
})


function checkAllergicMedicine(item_array =Array()) {
    var patient_allergic_data = []; 
    var allergy_row_data = ''  
    var count = 1;   
    $.each(item_array, function (key, val) {
        var item_code = val; 
        var item_name = $(".selected_medicine_row").find("input[item_code='" + item_code + "']").val();              
        var index_item = item_allergyArray.indexOf(item_code);
        if (index_item !== -1) {
            patient_allergic_data.push(item_code + '::' + item_name + '::' + 0 + '::' + 'Brand');
        } else {        
            var generic_id = $(".selected_medicine_row").find("input[item_code='" + item_code + "']").attr('data-generic-id');
            var generic_name = $(".selected_medicine_row").find("input[item_code='" + item_code + "']").attr('data-generic-name');
            var index_alergic = generic_allergyArray.indexOf(parseInt(generic_id));
            if (index_alergic !== -1) {
                var generic_string = "Generic (" + generic_name + ")";
                patient_allergic_data.push(item_code + '::' + item_name + '::' + generic_id  + '::' + generic_string);
            }
        }
    });
    if (patient_allergic_data.length != 0) {
        if (patient_allergic_data.length > 1) {
            $.each(patient_allergic_data, function (key, val) {
                var split_data = val.split('::');
                var data_item_code = split_data[0];
                var data_item_desc = split_data[1];
                var data_string = split_data[3];
    
                allergy_row_data += "<tr>" +               
                "<td>" +
                "<div class='checkbox checkbox-info inline no-margin' style='margin-top:4px !important'>" +
                "<input type='checkbox' id='check_remove_medicine_"+ count +"' value='"+ data_item_code +"' class='check_remove_medicine'>" +
                "<label style='padding-left: 2px;' for='check_remove_medicine_" + count +"'>" +
                "</label></div></td>" + 
                "<td style='float:left'>" + data_item_desc + "</td>" +
                "<td>" + data_string +"</td></tr>";          
                count++;      
            });
            $('#check_all_medicine').prop('checked', false);
            $('#presc_comman_allergy_validate_modal_body').html(allergy_row_data);  
            $('#presc_comman_validate_modal_header').html('These medicines may cause an allergic reaction. Do you want to continue? ');  
            $("#presc_allergy_comman_validate_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            setTimeout(function () {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');  
            }, 200); 
        } else {
            var selected_item_code = patient_allergic_data[0] ? patient_allergic_data[0] : '';  
            var split_data = selected_item_code.split('::');
            var data_item_code = split_data[0];
            var data_generic_id = split_data[2];
            checkSingleMedicineAllergy(data_item_code, data_generic_id);
        }
    }  
}
function checkSingleMedicineAllergy(item_code ='', generic_id ='') {
    var allergy_itemwise_flag = 0;
    var allergy_genericwise_flag = 0;
    var allergy_itemwise_string = '';
    var index_item = item_allergyArray.indexOf(item_code);
    var input_element =  $(".selected_medicine_row").find("input[item_code='" + item_code + "']");
    var tr_element = input_element.closest('tr');
    if (index_item !== -1) {
        allergy_itemwise_flag = 1;
        var item_name = $(".selected_medicine_row").find("input[item_code='" + item_code + "']").val();
        allergy_itemwise_string = item_name + ' medicine may have the possibility of causing an allergic reaction in the patient. Do you want to continue with it?';
    } else {        
        var index_alergic = generic_allergyArray.indexOf(parseInt(generic_id));
        if (index_alergic !== -1) {
            allergy_genericwise_flag = 1;
            var generic_name = $(".selected_medicine_row").find("input[item_code='" + item_code + "']").attr('data-generic-name');
            allergy_itemwise_string = generic_name + ' generic may have the possibility of causing an allergic reaction in the patient. Do you want to continue with it?';
        }
    }
    if (parseInt(allergy_itemwise_flag) == 1 || parseInt(allergy_genericwise_flag) == 1) {
        bootbox.confirm({
            message: allergy_itemwise_string,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (!result) {                 
                    $(tr_element).find('.del-presc-list-row').click();
                } 
            }
        });
    }
}

$(document).on('change', '#check_all_medicine', function () {
    var is_checked = $(this).prop('checked');
    $('.check_remove_medicine').prop('checked', is_checked);
});

function addAllergy_medicine_save() {
    $('.check_remove_medicine').each(function () {
        if (!$(this).prop('checked')) {
            var input_element = $(".selected_medicine_row").find("input[item_code='" + $(this).val() + "']");
            var tr_element = input_element.closest('tr');
            $(tr_element).find('.del-presc-list-row').click();
        }
    });
    $('#presc_allergy_comman_validate_modal').modal('hide');
    $(window.tr).find('.dosage').focus();  
}


$(document).on('change', '.check_remove_medicine', function () {
    var totalCheckboxes = $('.check_remove_medicine').length;
    var totalChecked    = $('.check_remove_medicine:checked').length;
    if (totalCheckboxes == totalChecked) {        
        $('#check_all_medicine').prop('checked', true);
    } else {
        $('#check_all_medicine').prop('checked', false);
    }
});
