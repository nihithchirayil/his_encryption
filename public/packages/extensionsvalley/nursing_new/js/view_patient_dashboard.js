$(document).ready(function(){
   $("#poplink").popover({
        html: true,
        placement: "right",
        trigger: "hover",
        title: function () {
            return $(".pop-title").html();
        },
        content: function () {
            return $(".pop-content").html();
        }
    });

    var patient_id = $('#patient-id').val();
    fetchPatientDetails(patient_id);
});

$(document).on('click', '.patient_special_notes_btn', function (e) {
    e.preventDefault();
    fetchSpecialNotes();
});

$(document).on('click', '.patient_lab_results_btn', function (e) {
    e.preventDefault();
    fetchPatientLabResults();
});

$(document).on('click', '.patient_radiology_results_btn', function (e) {
    e.preventDefault();
    fetchRadiologyResults();
});

$(document).on('click', '.patient_discharge_summary_list_btn', function (e) {
    e.preventDefault();
    fetchDischargeSummaryList();
});

$(document).on("click",".radilogy_results_pagination_div nav ul li a.page-link", function(e) {
    $(this).parent('li').addClass('active');
    var page = $(this).attr('href').split('page=')[1];
    fetchRadiologyResults(page);
});

$(document).on("click",".patient_discharge_summary_create_btn", function(e) {
    e.preventDefault();
    var visit_id = $("#visit_id").val();
    var visit_type = $("#visit_type").val();
    if(visit_id && visit_type == 'IP'){
        editDischargeSummary(visit_id);
    } else {
        Command: toastr["warning"]("Please select any IP patient to continue.");
    }
});

var investigation_future_date = 0;

function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}
function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function showPatientAllergies(patient_allergies, patient_other_allergy) {
    $(".allergy_list").html('');
    // console.log(patient_allergies);return;
    if(typeof patient_allergies !== 'undefined'){
        if (patient_allergies.length > 0) {
            $.each(patient_allergies, function (key, val) {
                var allergy = '';
                if (val.allergy_type == 'B') {
                    // allergy = '<span class="blue"> Brand : </span> ' + val.allergy_name + '<br>';
                    allergy = val.allergy_name + '<br>';
                } else if (val.allergy_type == 'G') {
                    // allergy = '<span class="blue"> Generic : </span> ' + val.allergy_name + '<br>';
                    allergy = val.allergy_name + '<br>';
                }
                $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + val.allergy_id + '">' + allergy + '</span>');
            })

        }
    }

   if(typeof patient_other_allergy !== 'undefined'){
        if (patient_other_allergy.length > 0) {
            $.each(patient_other_allergy, function (key, val) {
                if(val.allergy){
                    // var allergy = '<span class="blue"> Other : </span> ' + val.allergy + '<br>';
                    var allergy = val.allergy + '<br>';
                    $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + val.allergy_id + '">' + allergy + '</span>');
                }
            })
        }
   }

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
}


function fetchPatientDetails(patient_id) {
    var patient_id;
    if (!patient_id) {
        patient_id = $('#patient_id').val();
    }
    var doctor_id = $('#doctor_id').val();
    var token = $('#c_token').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/nursing_new/fetchNsPatientDetails";
    var params = {'doctor_id':doctor_id,
                  'patient_id':patient_id,
                  '_token':token
                };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".todays_visit_details_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".patient_combined_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".investigation_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".prescription_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var patient_details = data.patient_details;
            var patient_basic_details = patient_details.patient_details;
            var patient_allergies = patient_details.patient_allergies;
            var patient_other_allergy = patient_details.patient_other_allergy;
            var personal_notes = patient_details.patient_private_notes;
            showPatientDetails(patient_basic_details);

            var formdata = patient_details.formdata;
            showPatientFormData(formdata);
            showPatientAllergies(patient_allergies, patient_other_allergy);
            $(".private_notes_textarea").text(personal_notes);
            var patient_vitals = patient_details.patient_vitals;
            // showLatestPatientVitals(patient_vitals[0]);
            var patient_medications = patient_details.patient_medication;
            showPatientMedicationHistory(patient_medications);
            var patient_investigations = patient_details.patient_investigation;
            // showPatientInvestigationHistory(patient_investigations);
            $(".investigation_history_div").html(patient_details.investigation_details);
            var patient_combined_history = patient_details.patient_combined_history;
            showPatientCombinedHistory(patient_combined_history);
            var patient_todays_visit_details = patient_details.patient_todays_visit_details;
            $(".todays_visit_details_div").html(patient_todays_visit_details);
            $(".chief_complaint").html('');
            $(".investigation_item_search_textbox").val('');
            $(".assessmentFavButton").show();

        },
        complete: function () {
            $(".todays_visit_details_div").LoadingOverlay("hide");
            $(".patient_combined_history_div").LoadingOverlay("hide");
            $(".investigation_history_div").LoadingOverlay("hide");
            $(".prescription_history_div").LoadingOverlay("hide");
            fetchPatientVisit();
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function showPatientDetails(patient_basic_details) {
    // $("#visit_id").val(patient_basic_details.visit_id ? patient_basic_details.visit_id : 0);
    // $("#encounter_id").val(patient_basic_details.encounter_id);

}

function showPatientFormData(formdata) {
    $(".notes_chief_complaint_container").html(formdata.response);
}

function showPatientMedicationHistory(patient_medications) {
    $(".prescription_history_div").empty();

    $.each(patient_medications, function (key, val) {

        var presc_date = moment(val.created_at).format('DD MMM YYYY hh:mm A');
        var doctor_name = capitalizeFirstLetter(val.doctor_name);

        var bill_converted_class = '';
        if (val.billconverted_status == 1) {
            bill_converted_class = ' hidden ';
        }
        var prescription_head_id = val.head_id;
        var presc_div = '';
        presc_div = '<div class="prescription_history_item" data-head-id="' + prescription_head_id + '" data-bill-converted-head-status="' + val.billconverted_status + '"><div class="presc_history_head" style="display:flex;"><div class="presc_history_head_left"><div><i class="fa fa-calendar"></i> ' + presc_date + '</div><div class="overflow_text"><i class="fa fa-user-md"></i> ' + doctor_name + '</div> </div><div class="presc_history_head_right"><i class="fa fa-print printBtn printPrescriptionButton"></i><i class="fa fa-edit editBtn ' + bill_converted_class + ' editPrescriptionButton"></i><i class="fa fa-copy copyBtn copyPrescriptionButton"></i><i class="fa fa-trash deleteBtn ' + bill_converted_class + ' deletePrescriptionButton"></i></div></div> <div class="prescription_list">';

        $.each(val.medicine_list, function (key1, val1) {

            presc_div = presc_div + '<span class="checkbox_serial"><input type="checkbox" class="prescription_history_item_checkbox" data-detail-id="' + val1.detail_id + '" data-frequency-name="' + val1.frequency + '" data-frequency-value="' + val1.frequency_value + '" data-frequency-id="' + val1.frequency_id + '" data-item-quantity="' + val1.quantity + '" data-item-duration="' + val1.duration + '" data-item-direction="' + val1.notes + '" data-item-desc="' + val1.item_desc + '" data-item-code="' + val1.medicine_code + '" data-bill-converted-status="' + val1.bill_converted_status + '" /></span> <span class="presc_history_item_desc">' + val1.item_desc + '</span><span class="presc_history_frequency"> | ' + val1.frequency + ' | </span> <span class="presc_history_duration"> x ' + val1.duration + ' days</span> <div class="clearfix border_dashed_bottom"></div> ';

        });

        presc_div = presc_div + '</div> </div> ';

        $(".prescription_history_div").append(presc_div);



    });

}


function showPatientInvestigationHistory(patient_investigations) {
    $(".investigation_history_div").empty();

    $.each(patient_investigations, function (key, val) {

        var invest_date = moment(val.created_at).format('DD MMM YYYY hh:mm A');
        var doctor_name = val.doctor_name ? capitalizeFirstLetter(val.doctor_name) : '';

        var bill_converted_class = '';
        if (val.bill_converted_status == 1) {
            bill_converted_class = ' hidden ';
        }
        var invest_div = '';
        invest_div = '<div class="investigation_history_item" data-investigation-head-id="' + val.head_id + '"><div class="invest_history_head" style="display:flex;"><div class="invest_history_head_left"><div><i class="fa fa-calendar"></i> ' + invest_date + '</div><div><i class="fa fa-user-md"></i> ' + doctor_name + '</div> </div><div class="invest_history_head_right"><i class="fa fa-edit editBtn editInvestigationBtn ' + bill_converted_class + '"></i><i class="fa fa-copy copyBtn copyInvestigationBtn"></i><i class="fa fa-trash deleteBtn deleteInvestigationBtn ' + bill_converted_class + '"></i></div></div> <div class="investigation_list">';

        $.each(val.investigation_list, function (key1, val1) {

            var result = '';
            if (val1.sub_test_list.length == 0 && val1.investigation_type == 'LAB') {
                result = ' - ' + val1.actual_result;
            }

            var invest_sub_list = '';
            $.each(val1.sub_test_list, function (key2, val2) {
                invest_sub_list = invest_sub_list + '<div class="investigation_subtest_list"><span class="checkbox_serial"><i class="fa fa-bullseye"></i></span> <span class="invest_history_item_desc">' + val2.service_desc + '</span><span class="invest_history_result"> - ' + val2.actual_result + '</span><div class="clearfix border_dashed_bottom"></div></div>';
            });
            invest_div = invest_div + '<div class="investigation_list_item"><span class="checkbox_serial">' + (key1 + 1) + '. </span> <span class="invest_history_item_desc">' + val1.investigation_type + '-' + val1.service_desc + '</span><span class="invest_history_result">' + result + '</span><div class="clearfix border_dashed_bottom"></div> ' + invest_sub_list + '</div>';

        });


        invest_div = invest_div + '</div> </div> ';

        $(".investigation_history_div").append(invest_div);



    });

}

function showPatientCombinedHistory(patient_combined_history) {
    $(".patient_combined_history_div").html(patient_combined_history);
}

var bp_sys_vital_master_id = "5";
var bp_dia_vital_master_id = "6";

function showLatestPatientVitals(patient_vitals) {

    if (patient_vitals) {
        var vital_details = patient_vitals.vital_details;
        $(".vital_list_table_body").empty();
        var bp_value = '';
        $.each(vital_details, function (key, val) {
            var vital_icon = val.icon_class ? val.icon_class : '';
            var vital_name = val.display_name ? val.display_name : val.vital_name;
            var vital_value = val.vital_value;
            var string_class = '';
            if (vital_value > parseFloat(val.max_value)) {
                string_class = " <i class='fa fa-arrow-up blink_me'></i> ";
            }
            if (vital_value < parseFloat(val.min_value)) {
                string_class = " <i class='fa fa-arrow-down blink_me'></i> ";
            }


            if (val.vital_master_id == bp_dia_vital_master_id) {
                if($(".vital_master_" + bp_sys_vital_master_id).length > 0){
                    var bp_sys = $(".vital_master_" + bp_sys_vital_master_id).find('.patient_vital_value').html();
                    $(".vital_master_" + bp_sys_vital_master_id).find('.patient_vital_value').html(bp_sys + '/' + vital_value + '' + string_class);
                } else {
                    vital_name = 'BP';
                    $(".vital_list_table_body").append('<tr data-vital-master-id="'+val.vital_master_id+'" class="vital_item vital_master_' + val.vital_master_id + '"><td class="vital_icon"><i class="' + vital_icon + '"></i></td><td>' + vital_name + '</td><td><b class="patient_vital_value ">' + vital_value + '</b></td></tr>');
                }

            } else if(val.vital_master_id == bp_sys_vital_master_id) {
                if($(".vital_master_" + bp_dia_vital_master_id).length > 0){
                    var bp_dia = $(".vital_master_" + bp_dia_vital_master_id).find('.patient_vital_value').html();
                    $(".vital_master_" + bp_dia_vital_master_id).find('.patient_vital_value').html(vital_value + '/' + bp_dia + '' + string_class);
                } else {
                    vital_name = 'BP';
                    $(".vital_list_table_body").append('<tr data-vital-master-id="'+val.vital_master_id+'" class="vital_item vital_master_' + val.vital_master_id + '"><td class="vital_icon"><i class="' + vital_icon + '"></i></td><td>' + vital_name + '</td><td><b class="patient_vital_value ">' + vital_value + '</b></td></tr>');
                }

            } else {
                $(".vital_list_table_body").append('<tr data-vital-master-id="'+val.vital_master_id+'" class="vital_item vital_master_' + val.vital_master_id + '"><td class="vital_icon"><i class="' + vital_icon + '"></i></td><td>' + vital_name + '</td><td><b class="patient_vital_value ">' + vital_value + '</b>'+string_class+'</td></tr>');
            }
        })
    }
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
}

$('.private_notes_area').on('blur', function(){
    savePatientPersonalNotes();
});
function savePatientPersonalNotes(){
    var personal_notes = $(".private_notes_area").text();
    var patient_id = $("#patient_id").val();
    var visibility_status = 0;

    if(patient_id && personal_notes){
        var url = $('#base_url').val() + "/emr_lite/savePatientPersonalNotes";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                personal_notes:personal_notes,
                patient_id:patient_id,
                visibility_status: visibility_status
            },
            beforeSend: function () {
                $(".savePatientPersonalNotesBtn").find('i').removeClass('fa-save').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                if(data.status == 1){
                    $("#patientPersonalNotesModal").modal('hide');
                    Command: toastr["success"]('Successfully saved.');
                } else {
                    Command: toastr["error"]('Save failed.');
                }
            },
            error: function () {
                Command: toastr["error"]('Error.!');
            },
            complete: function () {
                $(".savePatientPersonalNotesBtn").find('i').removeClass('fa-spinner fa-spin').addClass('fa-save');
            }
        });
    } else {
        Command: toastr["error"]('Invalid details');
    }

}

function fetchSpecialNotes() {
    var url = $('#base_url').val() + "/emr_lite/fetchSpecialNotes";
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                visit_id:visit_id
            },
            beforeSend: function () {
                $('.patient_special_notes_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-book');
            },
            success: function (data) {
                if(data != ''){
                    $('#special_notes_modal_body').html(data);
                    $("#special_notes_modal").modal('show');
                } else {
                    toastr.success("No special notes added yet.");
                }

            },
            complete: function () {
                $('.patient_special_notes_btn').find('i').addClass('fa-book').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchPatientLabResults() {
    var url = $('#base_url').val() + "/emr_lite/labResultTrends";
    var patient_id = $("#patient_id").val();
    var encounter_id = $('#encounter_id').val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "GET",
            url: url,
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id,
            beforeSend: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-flask');
            },
            success: function (data) {
                console.log(data);
                $('#lab_restuls_data').html(data);
                $("#patientLabResultsModal").modal('show');
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            },
            complete: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa-flask').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchRadiologyResults(page = 1) {
    var url = $('#base_url').val() + "/emr_lite/fetchRadiologyResults";
    var patient_id = $("#patient_id").val();
    var search_service_id = $('#search_service_id').val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                search_service_id:search_service_id,
                page: page
            },
            beforeSend: function () {
                $('.patient_radiology_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-camera');
            },
            success: function (data) {
                $('#radiology_results_data').html(data.html);
                $("#search_service_id").empty();
                $("#search_service_id").append('<option value="">Select Service</option>');
                $.each(data.service_list, function(key, val){
                    $("#search_service_id").append('<option value="'+val.id+'">'+val.service_desc+'</option>');
                })

                if(search_service_id){
                    $("#search_service_id").val(search_service_id);
                }
                $("#radiology_results_modal").modal('show');
            },
            complete: function () {
                $('.patient_radiology_results_btn').find('i').addClass('fa-camera').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function editDischargeSummary(visit_id) {
    window.location = $("#base_url").val() + '/summary/dischargesummary/' + visit_id;
}

function fetchDischargeSummaryList() {
    var url = $('#base_url').val() + "/emr_lite/fetchDischargeSummaryList";
    var patient_id = $("#patient_id").val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id
            },
            beforeSend: function () {
                $('.patient_discharge_summary_list_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-list');
            },
            success: function (data) {
                $('#discharge_summary_list_modal_body').html(data);
                $("#discharge_summary_list_modal").modal('show');
            },
            complete: function () {
                $('.patient_discharge_summary_list_btn').find('i').addClass('fa-list').removeClass('fa-spinner fa-spin');
            }
        });
    }
}
