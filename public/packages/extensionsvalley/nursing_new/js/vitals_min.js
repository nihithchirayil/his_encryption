$(document).ready(function(){
    $("#time_taken").datetimepicker({
        format: 'MMM-DD-YYYY hh:mm a',
        maxDate: new Date()
        // .setHours(23, 59, 59),
        // widgetPositioning:{
        //     horizontal: 'auto',
        //     vertical: 'bottom'
        // }
    });
});
function saveVitals() {
    var vital_value = {};
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    let patient_id = $("#selected_patient_id").val();
    let visit_id = $("#patient_visit_id").val();
    let remark_id = $("#editvitalModal").attr('remark_id');
    // let remarks = $("#editvitalModal").attr('remarks');
    let encounter_id = $("#patient_encounter_id").val();
    let time_taken = $('#time_taken').val();
    let remarks = $('#remarks').val();
    let vital_batch = $('#vital_batch').val();

    if ($("#weight").val() == 1) {
        if ($("#weight_value").val() != '') {
            vital_value["1"] = $("#weight_value").val();
            vital_value["2"] = (($("#weight_value").val()) / 2.205).toFixed(2);
        }

    }
    if ($("#weight").val() == 2) {
        if ($("#weight_value").val() != '') {
            vital_value["2"] = $("#weight_value").val();
            vital_value["1"] = (($("#weight_value").val()) * 2.205).toFixed(2);
        }

    }



    if ($("#temperature").val() == 9) {
        if ($("#temperature_value_f").val() != '') {
            vital_value["9"] = $("#temperature_value_f").val();
            vital_value["10"] = (($("#temperature_value_f").val() - 32) * (5 / 9)).toFixed(2);
        }

    }
    if ($("#temperature").val() == 10) {
        if ($("#temperature_value").val() != '') {
            vital_value["10"] = $("#temperature_value").val();
            vital_value["9"] = (($("#temperature_value").val() * (9 / 5)) + 32).toFixed(2);
            console.log(vital_value);
        }

    }


    if ($("#height").val() == 3) {
        if ($("#height_value").val() != '') {
            vital_value["3"] = $("#height_value").val();
            vital_value["4"] = (($("#height_value").val()) * (30.48)).toFixed(2);
        }

    }
    if ($("#height").val() == 4) {
        if ($("#height_value").val() != '') {
            vital_value["4"] = $("#height_value").val();
            vital_value["3"] = ((($("#height_value").val()) / 30.48).toFixed(2));
            ;
        }

    }
    if ($("#bp_systolic").val() != '') {
        vital_value["5"] = $("#bp_systolic").val();
    }
    if ($("#bp_diastolic").val() != '') {
        vital_value["6"] = $("#bp_diastolic").val();
    }
    if ($("#pulse").val() != '') {
        vital_value["7"] = $("#pulse").val();
    }
    if ($("#respiration").val() != '') {
        vital_value["8"] = $("#respiration").val();
    }
    if ($("#temperature_location").val() != '') {
        vital_value["11"] = $("#temperature_location").val();
    }

    if ($("#oxygen").val() != '') {
        vital_value["12"] = $("#oxygen").val();
    }
    if ($("#head").val() != '') {
        vital_value["13"] = $("#head").val();
    }
    if ($("#bmi").val() != '') {
        vital_value["14"] = $("#bmi").val();
    }
    if ($("#waist").val() != '') {
        vital_value["20"] = $("#waist").val();
    }
    if ($("#hip").val() != '') {
        vital_value["21"] = $("#hip").val();
    }
    if ($("#spirometry").val() != '') {
        vital_value["20"] = $("#spirometry").val();
    }
    if ($("input[name='visibility_status']:checked").val()==1) {
        if($("#blood_sug").val()){
            vital_value["22"] = $("#blood_sug").val();
        }

    }else if($("input[name='visibility_status']:checked").val()==2){
        if($("#blood_sug").val()){
            vital_value["23"] = $("#blood_sug").val();
        }
    }


    var vitals_length = Object.keys(vital_value).length;
    if(vitals_length < 2){
        toastr.info('Enter patient vitals')
        return;
    }

    var dataparams = {
        vital_array: vital_value,
        patient_id: patient_id,
        visit_id: visit_id,
        remark_id: remark_id,
        encounter_id: encounter_id,
        time_taken: time_taken,
        remarks: remarks,
        vital_batch: vital_batch,
        _token: _token
    };

    console.log(dataparams);
    $.ajax({
        type: "POST",
        url: url + "/emr/add_vital",
        data: dataparams,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            console.log(data);
            $('body').LoadingOverlay("hide");
            if(data){
                Command: toastr["success"]("Success");
            $("#editvitalModal").modal('hide');
            $('#vital_batch').val(data);
                // fetchScreeningListData();
            }
        },
        complete: function () {
          //  $('#editvitalModal').modal('toggle');
        }
    });
}

$('#weight_value,#height_value,#height_value').on('blur',function(){
    let w_val = '';
    let w_type = '';
    let h_val = '';
    let h_type = '';

    w_val = $('#weight_value').val();
    h_val = $('#height_value').val();
    if(h_val == ""){
        h_val = $('#height_value').val();
    }
    w_type = $('#weight').val();
    h_type = $('#height').val();

    if(w_val != '' && h_val != '' && w_type != ''){
        calculateBmi(w_val,w_type,h_val,h_type);
    }
});

function calculateBmi(weight,wtype,height,htype){

    //convert weight to LBS to KG (weight in KG)
    let weight_type = "";
    let weight_kg = "";
    weight_type = wtype;
    //1 LBS 2 KG
    if(weight_type == "1"){
        weight_kg = Math.round((parseFloat(weight)/2.2046));
    }else{
        weight_kg = Math.round(parseFloat(weight));
    }

    //convert height to (F/I) to M
    //3 INCH 4 CM
    let height_type = "";
    let height_cm = "";
    let height_m = "";
    height_type = htype;
    if(height_type == "3"){
      let vital_value_feet = height;

       //inch to meter
       height_m = parseFloat(vital_value_feet)/3.2808;

    }else{
        height_cm = parseFloat(height);
        height_m = parseFloat(height_cm/100);//convert to meter
    }

    if(weight_kg != '' && height_m != ''){
        let bmi_val = '';
        bmi_val = parseFloat(weight_kg/(height_m*height_m));
        $('input[name="bmi"]').val(bmi_val.toFixed(2));
    }

}

$(document).on("blur", ".vital_form_container input[type='text']", function () {
    var vital = $(this).attr('id');
    var value = $("#"+vital).val();
    var vital_values = $('#vital_values').val();
    vital_values = $.parseJSON(vital_values);

    if (value != '') {
        value = parseFloat(value);
        var vital_val = '';
        var min_val = '';
        var max_val = '';

        if (vital == 'weight_value') {
            vital_val = vital_values.weight_kg.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
                $("#bmi").val('');
            }
        } else if (vital == 'height_value') {
            vital_val = vital_values.height_cm.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
                $("#bmi").val('');
            }
        } else if (vital == 'oxygen') {
            vital_val = vital_values.oxygen.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else if (vital == 'bp_systolic') {
            vital_val = vital_values.bp_systolic.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else if (vital == 'bp_diastolic') {
            vital_val = vital_values.bp_diastolic.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else if (vital == 'temperature_value') {
            vital_val = vital_values.temperature_c.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
                $("#temperature_value_f").val('');
            }
        } else if (vital == 'temperature_value_f') {
            vital_val = vital_values.temperature_f.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
                $("#temperature_value").val('');
            }
        } else if (vital == 'head') {
            vital_val = vital_values.head_circ.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else if (vital == 'waist') {
            vital_val = vital_values.waist_circ.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
                $("#waist_hip_ratio").val('');
            }
        } else if (vital == 'hip') {
            vital_val = vital_values.hip_circ.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
                $("#waist_hip_ratio").val('');
            }
        } else if (vital == 'pulse') {
            vital_val = vital_values.pulse.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else if (vital == 'respiration') {
            vital_val = vital_values.respiration.split('::');
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else if (vital == 'blood_sug') {
            if ($("input[name='visibility_status']:checked").val() == '1') {
                vital_val = vital_values.blood_sugar_mmol_dL.split('::');
            } else {
                vital_val = vital_values.blood_sugar_mg_dL.split('::');
            }
            min_val = parseFloat(vital_val[0]);
            max_val = parseFloat(vital_val[1]);
            if ((value < min_val) || (value > max_val)) {
                toastr.warning("Value between "+min_val+ " and " + max_val);
                $("#"+vital).val('');
            }
        } else {
        }
    }
});


function getYellowSheetServices(visit_id) {
    window.open($('#base_url').val() + "/master/mapYellowSheetPatient/" + visit_id, '_self');
}


function clear_vital_vlues(obj,patient_id,visit_id,doctor_id) {
    $("#vital_batch").val('0');
    $("#weight_value").val('');
    $("#temperature_value").val('');
    $("#temperature_value_f").val('');
    $("#height_value").val('');
    $("#bp_systolic").val('');
    $("#bp_diastolic").val('');
    $("#pulse").val('');
    $("#respiration").val('');
    $("#head").val('');
    $("#oxygen").val('');
    $("#bmi").val('');
    $("#waist").val('');
    $("#hip").val('');
    $("#blood_sug").val('');
    $("#spirometry").val('');
    $("#waist_hip_ratio").val('');
    $("#remarks").val('');
    $("#time_taken").val(moment().format('MMM-DD-YYYY hh:mm A'));
    $("#update_vital").text('Save');
    //createOrSelectEncounter(patient_id,visit_id,doctor_id);
    $("#editvitalModal").modal('show');
    $('#patient_visit_id').val(visit_id);
    $('#selected_patient_id').val(patient_id);
    var header_name = '';
    if ($(obj).parent().attr('data-patient') != undefined) {
        var patient_name = ($(obj).parent().attr('data-patient')).trim();
        var patient_uhid = ($(obj).parent().attr('data-uhid')).trim();
        header_name = patient_name + ' - ' + patient_uhid;
        $('#editvitalModal .modal-title').html('Patient Vitals (' + header_name + ')');
    } else {
        $('#editvitalModal .modal-title').html('Patient Vitals');
    }
    setTimeout(function() {$("#weight_value").focus();}, 500);



}


$(document).on("keyup", "#temperature_value", function(){
    vital_value_c = $("#temperature_value").val();
    if(vital_value_c != ''){
        vital_value_f = ((vital_value_c * (9 / 5)) + 32).toFixed(2);
        $('#temperature_value_f').val(vital_value_f);
    }else{
        $('#temperature_value_f').val('');
    }
});


$(document).on("keyup", "#temperature_value_f", function(){
    vital_value_f = $("#temperature_value_f").val();
    if(vital_value_f != ''){
        vital_value_c = ((vital_value_f - 32) * (5 / 9)).toFixed(2);
        $('#temperature_value').val(vital_value_c);
    }else{
        $('#temperature_value').val('');
    }
});

$(document).on('keyup','#waist',function(){
    autoCalRatio();
});
$(document).on('keyup','#hip',function(){
    autoCalRatio();
});

function autoCalRatio(){
 var waist=$('#waist').val().trim();
 var hip=$('#hip').val().trim();
 console.log(waist,hip)
 if(waist!='' && hip!=''){
    var waist_hip_ratio=parseFloat(waist/hip).toFixed(2);
    $('#waist_hip_ratio').val(waist_hip_ratio);
 }else{
    $('#waist_hip_ratio').val('');
 }
}


function show_vital_history(obj, patient_id,visit_id,doctor_id){
    if (patient_id != undefined) {
        $("#selected_patient_id").val(patient_id);
    } else {
        patient_id = $("#selected_patient_id").val();
    }
    var header_name = '';
    if ($(obj).parent().attr('data-patient') != undefined) {
        var patient_name = ($(obj).parent().attr('data-patient')).trim();
        var patient_uhid = ($(obj).parent().attr('data-uhid')).trim();
        header_name = patient_name + ' - ' + patient_uhid;
        $('#vital_graph_modal .modal-title').html('Vital and Progress (' + header_name + ')');
    } else {
        $('#vital_graph_modal .modal-title').html('Vital and Progress');
    }
    let lstVitalItems = $("#lstVitalItems").val();
    let from_date = $("#vital_graph_from_date").val();
    let to_date = $("#vital_graph_to_date").val();
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
        $.ajax({
        type: "POST",
        url: url + "/emr/show_vital_history",
        dataType: "json",
        data: {
            patient_id: patient_id,
            vital_mster_id: lstVitalItems,
            from_date:from_date,
            to_date:to_date,
            _token: _token
        },
        beforeSend: function () {
        $('#vital_his_table').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
        $('#vital_graph').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
        },
        success: function (data) {

        var datas = data;
        var chart = Highcharts.chart('vital_graph', {
            chart: {
            type: 'spline'
            },
            title: {
            text: 'Patient Vitals Chart'
             },

            xAxis: {
            type: 'datetime',
             categories: datas[9].time_taken,


            },
            yAxis: {
            title: {
            text: 'Vital Values'
            },
            labels: {
            formatter: function () {
            return this.value;
            }
            }
            },
            tooltip: {
            split: true,
            },

            plotOptions: {
            spline: {
            marker: {
            enabled: true
            },
            dataLabels: {
            enabled: true
            },
            enableMouseTracking: true
            }
            },
            series: data
    });

    vital_history_table(patient_id,lstVitalItems,from_date,to_date);
    $("#vital_graph_modal").modal('show');
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}

function vital_history_table(patient_id,lstVitalItems,from_date,to_date) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/show_vital_history_table",
        data: {
            patient_id:patient_id,
            history_table:'table',
            vital_mster_id: lstVitalItems,
            from_date:from_date,
            to_date:to_date,
            _token: _token
        },
        beforeSend: function () {
        },
        success: function (data) {
                $('#vital_his_table').html(data);
        },
    });
}
