var ret_patient_id = 0;
var ret_visit_id = 0;
function viewPatientMedicationReturn(obj, patient_id, visit_id) {
    $("#patient_id").val(patient_id);
    if ($(obj).parent().attr('data-patient') != undefined) {
        var patient_name = ($(obj).parent().attr('data-patient')).trim();
        var patient_uhid = ($(obj).parent().attr('data-uhid')).trim();
        header_name = patient_name + ' - ' + patient_uhid;
        $('#patient_medication_return_modal .modal-title').html('<i class="fa fa fa-medkit"></i> Medication Return (' + header_name + ')');
    } else {
        $('#patient_medication_return_modal .modal-title').html('<i class="fa fa fa-medkit"></i> Medication Return');
    }
    ret_patient_id = patient_id;
    ret_visit_id = visit_id;
    $("#patient_medication_return_modal").modal({
        backdrop: 'static',
        keyboard: false
    });

    var url = base_url + "/nursing_new/viewPatientMedicationReturn"
    var dataparams = {
        patient_id: patient_id,
        visit_id: visit_id,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#patient_medication_return_modal_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#patient_medication_return_modal_content").html(data);
            patientMedicationReturnDetails();
        },
        complete: function () {
            $("#patient_medication_return_modal_content").LoadingOverlay("hide");
        }
    });
}

function patientMedicationReturnDetails() {
    if ($(".search-medicine-btn").hasClass("fa-close")) {
        $(".search-medicine-btn").trigger('click');
    }
    var url = base_url + "/nursing_new/patientMedicationReturnDetails"
    var dataparams = {
        visit_id: ret_visit_id,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#medication_item_list_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#medication_item_list").html(data);
            $('#return_medicine_items').html('');
            $('#return_remarks').val('');
            $("#edit_return_id").val(0);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $("#medication_item_list_div").LoadingOverlay("hide");
        }
    });
}

function loadMedicationReturnHistory(obj) {
    if ($(obj).attr('aria-expanded') === 'false' || $(obj).attr('aria-expanded') === undefined) {
        $('#patient_medication_return_modal .modal-content').css({ 'height': '125vh', 'width': '94vw' });

        loadHistory(1);
    } else {
        $('#patient_medication_return_modal .modal-content').css({ 'height': '100vh', 'width': '95vw' });
    }
}

function addMedicineToReturn(obj) {
    var item_desc = $(obj).attr('data-item-desc');
    var item_id = $(obj).attr('data-item-id');
    var bill_id = $(obj).attr('data-bill-detail-id');
    var medicine_id = $(obj).attr('data-med-detail-id');
    var qty = $(obj).attr('data-qty');
    var available_qty = $(obj).attr('data-available-qty');
    var bill_head_id = $(obj).attr('data-bill-head-id');

    if (!$('#return_medicine_items .clip-row-' + bill_id).length) {
        var str = '<div class="col-md-12 no-padding med_clip clip-row-' + bill_id + '"> <span class="col-md-7 medicine_name" id="medicine_name_' + bill_id + '" attr-bill-id="' + bill_id + '" attr-bill-head-id="' + bill_head_id + '">' + item_desc + '</span> <span class="col-md-4"> <input type="number" class="form-control medicine_return_qty" placeholder="Return Quantity" attr-qty="' + qty + '" attr-available-qty="' + available_qty + '" attr-item-id="' + item_id + '" style="height: 25px !important;"> </span> <span class="col-md-1" onclick="trashClipboard(this, ' + bill_id + ')" attr-medicine-id="' + medicine_id + '"><i class="fa fa-times-circle remove_icon"></i></span> </div>';

        $('#return_medicine_items').append(str);
        $(obj).addClass("selected");
    } else {
        $('#return_medicine_items .clip-row-' + bill_id).remove();
        $(obj).removeClass("selected");
    }
}

function trashClipboard(obj, bill_id) {
    // let id = $(obj).attr('attr   -medicine-id');
    $(obj).parent().remove();
    $('#medication_item_list tr[data-bill-detail-id="' + bill_id + '"]').removeClass('selected');
}

function saveMedicationReturn(obj) {
    var validate = validateData();
    $(obj).prop('disabled', true);
    var medicine_return = new Array();
    if (validate) {
        $('#medication_return_items .med_clip').each(function () {
            medicine_return.push({
                'bill_detail_id': $(this).find('.medicine_name').attr('attr-bill-id'),
                'bill_head_id': $(this).find('.medicine_name').attr('attr-bill-head-id'),
                'item_id': $(this).find('.medicine_return_qty').attr('attr-item-id'),
                // 'qty': $(this).find('.medicine_return_qty').attr('attr-qty'),
                'quantity': $(this).find('.medicine_return_qty').attr('attr-available-qty'),
                'return_qty': $(this).find('.medicine_return_qty').val(),
                'medication_detail_id': $(this).find('.remove_icon').closest('span').attr('attr-medicine-id'),
            });
        });
        var head_id = $("#edit_return_id").val();
        head_id = parseInt(head_id) > 0 ? head_id : 0;

        var url = base_url + "/nursing_new/SaveMedicationReturn";
        let _token = $('#c_token').val();
        var dataparams = {
            _token: _token,
            visit_id: ret_visit_id,
            patient_id: ret_patient_id,
            remarks: $('#return_remarks').val(),
            medicine_list: JSON.stringify(medicine_return),
            location_id: 0,
            return_id: head_id,
        };
        $.ajax({
            type: "POST",
            url: url,
            data: dataparams,
            beforeSend: function () {
                $("#medication_return_items").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (data == 1) {
                    Command: toastr['success']('Saved Successfully');
                    $("#edit_return_id").val(0);
                    patientMedicationReturnDetails();
                } else {
                    Command: toastr["error"]("Something went wrong!");
                }
            },
            complete: function () {
                $(obj).prop('disabled', false);
                $("#medication_return_items").LoadingOverlay("hide");
            }
        });
    } else {
        $(obj).prop('disabled', false);
    }
}

function validateData() {
    if ($("#medication_return_items .med_clip").length > 0) {
        var flag = 0;
        $('#medication_return_items .med_clip').each(function () {
            if ($(this).find('.medicine_return_qty').val() == '') {
                Command: toastr['warning']('Please add return quantity');
                $(this).find('.medicine_return_qty').focus();
                flag = 1;
                return false;
            }
        });
        if (flag == 1) {
            return false;
        } else {
            return true;
        }
    } else {
        Command: toastr['warning']('Please select medicines');
        return false;
    }
}

$(document).on('change', '#medication_return_items .medicine_return_qty', function () {
    var current_qty = $(this).val();
    var remaining_qty = $(this).attr('attr-available-qty');
    if ((parseInt(current_qty) > parseInt(remaining_qty)) || (parseInt(current_qty) <= 0)) {
        Command: toastr["error"]("Return quantity should not greater than available quantity!");
        $(this).val('');
        $(this).focus();
    }
});

$(document).on('click', '.search-medicine-btn', function () {
    var th = $(this).closest('th');
    var inputBox = th.find('.search-input-box');

    if (inputBox.is(':visible')) {
        $('.search-medicine-btn').removeClass('fa-close btn btn-danger');
        $('.search-medicine-btn').addClass('fa-search btn btn-success');
        inputBox.hide();
        inputBox.val('');
        $('.search-input-box').trigger('keyup');
    } else {
        $('.search-medicine-btn').removeClass('fa-search btn btn-success');
        $('.search-medicine-btn').addClass('fa-close btn btn-danger');
        inputBox.show();
        inputBox.focus();
    }
});

$(document).on('keyup', '.search-input-box', function () {
    var search_key = $(this).val();
    if (search_key == '') {
        $('#medication_item_list tr').show();
    } else {
        $('#medication_item_list tr').hide();
        $("#medication_item_list tr[data-item-desc-search*='" + search_key.toLowerCase() + "']").show();
    }
});

function loadHistory(page = 1) {
    var url = base_url + "/nursing_new/getMedicationReturnHistory";
    var dataparams = {
        visit_id: ret_visit_id,
        page: page,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#return-history-data-list").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#return-history-data-list").html(data);
        },
        complete: function () {
            $("#return-history-data-list").LoadingOverlay("hide");
        }
    });
}

$(document).on('click', '.return_history_pages .page-item a.page-link', function (event) {
    event.preventDefault();
    var hrefValue = $(this).attr('href');
    var page = hrefValue.match(/\?page=(\d+)/);

    if (page && page[1]) {
        page = page[1];
        loadHistory(page);
    }
});

function loadReturnView(head_id) {
    if (head_id != '' && head_id != undefined) {
        $('#med_return_list_modal').modal('show');
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/nursing_new/getMedicationReturnHistoryDetails",
            data: {
                head_id: head_id,
                from_type: 'history_view',
                _token: _token
            },
            beforeSend: function () {
                $('#med_return_list_modal_content').html('<tr><td colspan="5" class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></td></tr>');
            },
            success: function (data) {
                $('#med_return_list_modal_content').html(data);
            },
            complete: function () {

            }
        });
    }
}

function editReturnView(head_id) {
    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/nursing_new/getMedicationReturnHistoryDetails",
            data: {
                head_id: head_id,
                from_type: 'history_edit',
                _token: _token
            },
            beforeSend: function () {
                $('#return_medicine_items').html('');
                $("#medication_return_items").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (data.length > 0) {
                    $("#edit_return_id").val(head_id);
                    var remarks = data[0].remarks;
                    $('#return_remarks').val(remarks);
                    $.each(data, function (key, value) {
                        var item_desc =value.item_desc;
                        var item_id =value.item_id;
                        var bill_id =value.bill_detail_id;
                        var medicine_id =value.patient_medication_detail_id;
                        var qty =value.billed_qty;
                        var available_qty =(value.available_qty - value.total_req_qty);
                        var bill_head_id =value.bill_head_id;
                        var return_qty =value.requested_qty;

                        if (!$('#return_medicine_items .clip-row-' + bill_id).length) {
                            var str = '<div class="col-md-12 no-padding med_clip clip-row-' + bill_id + '"> <span class="col-md-7 medicine_name" id="medicine_name_' + bill_id + '" attr-bill-id="' + bill_id + '" attr-bill-head-id="' + bill_head_id + '">' + item_desc + '</span> <span class="col-md-4"> <input type="number" class="form-control medicine_return_qty" placeholder="Return Quantity" attr-qty="' + qty + '" attr-available-qty="' + available_qty + '" attr-item-id="' + item_id + '" value="' + return_qty + '" style="height: 25px !important;"> </span> <span class="col-md-1" onclick="trashClipboard(this, ' + bill_id + ')" attr-medicine-id="' + medicine_id + '"><i class="fa fa-times-circle remove_icon"></i></span> </div>';

                            $('#return_medicine_items').append(str);
                            $('#medication_item_list tr[data-bill-detail-id="' + bill_id + '"]').addClass('selected');
                        }
                    });
                }
                // $('#med_return_list_modal_content').html(data);
            },
            complete: function () {
                $("#medication_return_items").LoadingOverlay("hide");
            }
        });
    }
}

function deleteReturnData(obj, head_id) {
    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/nursing_new/deleteMedicationReturn",
            data: {
                head_id: head_id,
                _token: _token
            },
            beforeSend: function () {
                $(obj).find(".delete_return_icon").removeClass("fa-trash").addClass("fa-spinner fa-spin");
            },
            success: function (data) {
                if (data == 1) {
                    Command: toastr['success']('Deleted Successfully');
                    loadHistory(1);
                } else {
                    Command: toastr["error"]("Something went wrong!");
                }
            },
            complete: function () {
                $(obj).find(".delete_return_icon").addClass("fa-trash").removeClass("fa-spinner fa-spin");
            }
        });
    }
}