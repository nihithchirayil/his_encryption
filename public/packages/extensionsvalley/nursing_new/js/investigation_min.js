/**
 * Function for loading the favaraoute group items by clcikig each tab
 * group_head_id is the head id of favarate group items tab
 *
 */
$(document).ready(function () {
    setTimeout(function () {
        insertInvHistoryToClipboard();

        $('#inlineRadio22').trigger('click');
        $('#inlineRadio11').trigger('click');
    },2000);
});

function isNumber(txt, evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46) {
      //Check if the text already contains the . character
      if (txt.value.indexOf('.') === -1) {
        return true;
      } else {
        return false;
      }
    } else {
      if (charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return false;
    }
    return true;
  }
function insertInvHistoryToClipboard(){
    $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('.checkbox').find('.fav_inv').each(function(){

        var id = $(this).attr('id');
        if($('#'+id).prop('checked') == true){

            var service_code = $('#'+id).val();
            var service_name = $('#'+id).next().text();
            second_class = '';
            var det_id = '';
            var checked = $("#"+id).is(":checked");
            var service_items = [];

            $('#investigation_clipboard').find('.col-md-10').each(function(){
                service_items.push($(this).attr('id'));
            });
            let isInArray = service_items.includes(service_code);

            if(checked == false) {
                $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
                return;
            }
            if(isInArray ==false){

                if ($('#' + service_code).prop("checked") == true) {
                    $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_name + "</span></div>");
                } else {
                    $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
                }
            }else{
                // Command: toastr["warning"]("Item already added in the list");
                // $("#" + e.id).prop('checked', false);
            }

        }else{
            $("#investigation_clipboard").find('.clip-row-' + id).remove();
        }

    });
}









function loadFavoriteItemsTab(group_head_id, force_load = 0) {
    $('.investigation_tab_content').find('.tab-panel').removeClass('active');
}
// function loadFavoriteItemsTab(group_head_id, force_load = 0) {

//     $("#investigation_tab_content").html('<i style="font-size: 28px;margin-left: 175px;margin-top: 70px;color:#0c9e61 !important;" class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');


//     var check_value = $('input[name="radioInline11"]:checked').val();// currently cheked radio button value
//     var investigation_head_id = $("#investigation_head_id").val();// id from patient_investigation_head
//     var service_name = '';
//     if (investigation_head_id == '') {
//         investigation_head_id = 0;
//     }
//     if ($('#invs_' + group_head_id).find(".chekbox_table_" + check_value).length < 1 || force_load == 1) {
//         setTimeout(function () {
//             common_ajax_loading(group_head_id, investigation_head_id, check_value, service_name);
//         },2000);
//     }

//     common_check_all_tab(group_head_id)
// }
/**
 *
 * Function called when the type is changed (Clicking the radio button)
 *
 */

function changeRadio(e) {
    $(".fav_group_check_box").prop("checked", false);
    var check_value = e.value;
    if(check_value == 'lab'){
        $('.investigation_fav_tabs_list li').hide();
        $('.investigation_fav_tabs_list li').removeClass('active');
        $('.investigation_fav_tabs_list').find(`[data-has-lab='1']`).show();
        $('#investigation_tab_content .tab-pane').removeClass('active');
        $('.investigation_fav_tabs_list li[data-has-lab="1"]:first a').click();

        $('#investigation_item_search_box').attr('placeholder', 'Search Lab');

    }
    if(check_value == 'radiology'){
        $('.investigation_fav_tabs_list li').hide();
        $('.investigation_fav_tabs_list li').removeClass('active');
        $('.investigation_fav_tabs_list').find(`[data-has-radiology='1']`).show();
        $('#investigation_tab_content .tab-pane').removeClass('active');
        $('.investigation_fav_tabs_list li[data-has-radiology="1"]:first a').click();

        $('#investigation_item_search_box').attr('placeholder', 'Search Radiology');

    }
    if(check_value == 'procedure'){
        $('.investigation_fav_tabs_list li').hide();
        $('.investigation_fav_tabs_list li').removeClass('active');
        $('.investigation_fav_tabs_list').find(`[data-has-procedure='1']`).show();
        $('#investigation_tab_content .tab-pane').removeClass('active');
        $('.investigation_fav_tabs_list li[data-has-procedure="1"]:first a').click();

        $('#investigation_item_search_box').attr('placeholder', 'Search Procedure');

    }
}

function InsertInvestigation(e){
    var service_code = e.value;
    console.log(service_code)
    var service_name = $('.' + service_code).html();
    // var second_class = ($("#" + service_code).attr('class').split(' ')[1]);
    second_class = '';
    var det_id = '';
    var enable_edit_price =  $('#enable_edit_price').val();
    var is_price_editable =  $(e).data('price_editable');
    //var checked = $("#" + e.id).is(":checked");
    var checked = $('#investigation_tab_content').find('.active').find('.ggg').find(e).is(":checked");

    var total_count =  $('#investigation_tab_content').find('.active').find('.ggg').find('input:checkbox').length;
    var checked_count = $('#investigation_tab_content').find('.active').find('.ggg').find('input:checked').length;
    if(total_count == checked_count){
        $('.investigation_fav_tabs_list').find('.active').find('input:checkbox').prop('checked',true);
    }else{
        $('.investigation_fav_tabs_list').find('.active').find('input:checkbox').prop('checked',false);
    }
    var service_items = [];
    $('#investigation_clipboard').find('.col-md-10').each(function(){
        service_items.push($(this).attr('id'));
    });
    let isInArray = service_items.includes(service_code);


    if(checked == false) {
        $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
        $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", false);
        return;
    }
    var exists = true;
    // if($('#investigation_clipboard').hasClass('common_service')){
    $('.common_service').each(function(){
        var service_code_inve=$(this).attr('id');
     if(service_code_inve==service_code){

        exists = false;
     }else{
        exists = true;
     }
    });


    if(isInArray ==false && checked == true && exists){

        if ($(e).prop("checked") == true) {
            if(is_price_editable==1 && enable_edit_price==1){
            $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code
            + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
            + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6 common_service' style='margin-left:-35px;' id='"
            + service_code + "'>" + service_name + "</span><span class='col-md-4'><input type='text' class='invest_price' onkeypress='return isNumber(this,event)' id='invest_price"+ service_code + "'name='invest_price[]' style='margin-left:50px;width: 93px !important;'></span></div>");
            $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", true);
            }else{

                $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code
                + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6 common_service' id='"
                + service_code + "' style='margin-left:-35px;'>" + service_name + "</span></div>");
                $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", true);
                }


        } else {
            $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
            $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", false);
        }
    // }
}else{
        Command: toastr["warning"]("Item already added in the list");
        $("#" + e.id).prop('checked', false);
        service_code.prop('checked', false);

    }

}



function InsertBookmarkInvestigation(service_id,service_code,service_name,is_price_editable){
    // var service_code = e.value;
    // var service_name = $('.' + service_code).html();
    var tr=$(this);
    second_class = '';
    var det_id = '';
    var checked = $('#inv_favorite_group_item_id_'+service_id).is(":checked");
    //var checked = $('#investigation_tab_content').find('.active').find('.ggg').find(e).is(":checked");
    // var is_price_editable =  tr.data('price_editable');
    console.log(is_price_editable)
    var enable_edit_price =  $('#enable_edit_price').val();
    console.log(enable_edit_price)

    var total_count =  $('#investigation_tab_content').find('.active').find('.ggg').find('input:checkbox').length;
    var checked_count = $('#investigation_tab_content').find('.active').find('.ggg').find('input:checked').length;
    if(total_count == checked_count){
        $('.investigation_fav_tabs_list').find('.active').find('input:checkbox').prop('checked',true);
    }else{
        $('.investigation_fav_tabs_list').find('.active').find('input:checkbox').prop('checked',false);
    }


    var service_items = [];
    $('#investigation_clipboard').find('.col-md-10').each(function(){
        service_items.push($(this).attr('id'));
    });
    let isInArray = service_items.includes(service_code);


    if(checked == false) {
        $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
        $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", false);
        return;
    }

    if(isInArray ==false && checked == true){

        if (checked == true) {

            if(is_price_editable && enable_edit_price==1){
                $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' id='"
                + service_code + "' style='margin-left: -35px;'>" + service_name + " </span><span class='col-md-4'><input type='text' class='invest_price' onkeypress='return isNumber(this,event)' id='invest_price"+ service_code + "'name='invest_price[]' style='margin-left:50px;width: 93px !important;'></span></div>");
                $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", true);

                }else{
                    $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                    + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' id='"
                    + service_code + "' style='margin-left: -35px;'>" + service_name + "</span></div>");
                    $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", true);
                }
        } else {
            $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
            $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", false);
        }
    }else{
        Command: toastr["warning"]("Item already added in the list");
        $("#inv_favorite_group_item_id_"+service_id).prop('checked', false);

    }
}




function save_draft_investigation(save_status = 0,print_status = 0){
    var service_items = [];
    var service_codes = [];

    $('#investigation_clipboard').find('.col-md-6').each(function(){
        var temp = {};
        var data = {};
        temp.service_code = $(this).attr('id');

        temp.invest_price = $('#invest_price'+temp.service_code).val();
        // invest_price.push(data);

        temp.service_name = $(this).html();
        service_items.push(temp);
        if($(this).attr('id')){
            service_codes.push($(this).attr('id'));
        }
    });

    if(service_items.length <= 0 ){
        toastr.warning('Please select atleast one item');
        return false;
    }

    var service_restriction_flag = 0;
    // if ($('.' + second_class + ':checked').length == $('.' + second_class).length) {
    //     $("input[name=" + second_class + "]").prop('checked', true);
    // } else {
    //     $("input[name=" + second_class + "]").prop('checked', false);
    // }
    var inv_remarks = $("#inv_remarks").val();
    var patient_id = $("#investigation_patient_id").val();
    var visit_id = $('#investigation_visit_id').val();
    // var invest_price = $('.invest_price').val();
    // const propertyValues=Object.values(temp['invest_price']);
    // var invest_price= $('#invest_price').val(propertyValues).change();

    var investigation_intend_type = $("input[name='investigation_intend_type']:checked").val();
    var admitting_doctor_id = $("#doctor_investigation").val();
    var inv_clinical_history = $("#inv_clinical_history").val();
    var is_ns = 1;
    var item_id = '';
    // var item_id = $(e).data('id');
    // if (item_id == 'undefined') {
    //     item_id = 0;
    // }
    var head_id = $("#investigation_head_id").val();
    var investigation_type = $('input[name="radioInline11"]:checked').val();
    let url = $('#base_url').val();
    url = url + "/emr/update_investigation_details";
    let _token = $('#c_token').val();
    service_restriction_flag = 0;
    // service_restriction_flag = checkServiceRestriction(patient_id,service_codes);
    if(service_restriction_flag == 1){
        return false;
    }

    var location = $('#ns_station_list').val();


    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: {
            head_id: head_id,
            service_items: service_items,
            patient_id: patient_id,
            inv_remarks: inv_remarks,
            investigation_intend_type: investigation_intend_type,
            is_ns: is_ns,
            type: investigation_type,
            item_id: item_id,
            doctor_id: admitting_doctor_id,
            save_status:save_status,
            // invest_price:invest_price,
            inv_clinical_history: inv_clinical_history,
            location: location,
            _token: _token
        },
        beforeSend: function () {
            $('#investigation_tab_content').parent().LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (data) {
            var detail_id_list = data.detail_id;
            var intent_converted_status = data.intent_converted_status;
            $('#investigation_tab_content').parent().LoadingOverlay("hide");
            let det_id = (data.detail_id != "" && data.detail_id != undefined) ? data.detail_id : 0;
            if (data.head_id > 0 && intent_converted_status == 0) {
                $("#investigation_head_id").val(data.head_id);
                editInvestigationHistoryList(data.head_id);

            }
            window.atleastOneSavedRecords = true;
            if(save_status == 0){
                $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge" style="margin-top:3px;"> Draft Mode <i class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');
            }else{
                $('.draft_mode_indication_inv').html('');
            }

            for (var key in detail_id_list) {
                var value = detail_id_list[key];
                console.log(key + "=" + value);
                $('.clip-row-' + key).find('.iv_trash_btn_span').attr('attr-id', value);
            }
            $('.inv_clip').remove();
            $('#investigation_item_search_box').val('');
            $('.investigation-list-div').hide();
            $("#inv_remarks").val('');
            $("#inv_clinical_history").val('');
            $('input[type="checkbox"]').prop('checked', false);
        },
        complete: function () {
            // updateSeenstatus(visit_id);
            loadInvestigationHistory(1);
        }
    });

    if(print_status == 1){
        let head_id = $("#investigation_head_id").val();
        printInvestigationHistoryList(head_id);
    }else{
        let head_id = $("#investigation_head_id").val();
        if(head_id !=''){
            Command: toastr["success"]("Successfully saved!");
        }
    }

}

// function InsertInvestigation(e) {
//     var service_code = e.value;
//     var service_name = $('.' + service_code).html();
//     var second_class = ($("#" + service_code).attr('class').split(' ')[1]);
//     var service_restriction_flag = 0;

//     if ($('.' + second_class + ':checked').length == $('.' + second_class).length) {
//         $("input[name=" + second_class + "]").prop('checked', true);
//     } else {
//         $("input[name=" + second_class + "]").prop('checked', false);
//     }
//     var inv_remarks = $("#inv_remarks").val();
//     var patient_id = $("#investigation_patient_id").val();
//     var visit_id = $('#investigation_visit_id').val();
//     var investigation_intend_type = $("input[name='investigation_intend_type']:checked").val();
//     var admitting_doctor_id = $("#doctor_investigation").val();
//     var is_ns = 1;
//     var item_id = $(e).data('id');
//     if (item_id == 'undefined') {
//         item_id = 0;
//     }
//     var det_id = 0;
//     var head_id = $("#investigation_head_id").val();
//     var investigation_type = $('input[name="radioInline11"]:checked').val();
//     let url = $('#base_url').val();
//     url = url + "/emr/update_investigation_details";
//     let _token = $('#c_token').val();
//     service_restriction_flag = checkServiceRestriction(patient_id,service_code);
//     if(service_restriction_flag == 1){
//         return false;
//     }

//     $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge" style="margin-top:3px;"> Draft Mode <i class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');

//     if ($('#' + service_code).prop("checked") == true) {
//         $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "'><i class='fa fa-trash' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_name + "</span></div>");
//     } else {
//         $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
//     }

//     return;


//     $.ajax({
//         type: "POST",
//         async: true,
//         url: url,
//         data: {
//             head_id: head_id,
//             service_code: service_code,
//             patient_id: patient_id,
//             inv_remarks: inv_remarks,
//             investigation_intend_type: investigation_intend_type,
//             is_ns: is_ns,
//             type: investigation_type,
//             item_id: item_id,
//             doctor_id: admitting_doctor_id,
//             _token: _token
//         },
//         beforeSend: function () {
//             // $('.tab-pane').append('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
//             $('#investigation_tab_content').parent().LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

//         },
//         success: function (data) {
//             // console.log(data);return;
//             //  $(".fa-spinner").css('display','none');
//             $('#investigation_tab_content').parent().LoadingOverlay("hide");
//             let det_id = (data.detail_id != "" && data.detail_id != undefined) ? data.detail_id : 0;
//             if (data.head_id > 0) {
//                 $("#investigation_head_id").val(data.head_id);
//             }
//             window.atleastOneSavedRecords = true;
//             //  Command: toastr["success"]("Successfully Updated");
//             $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge" style="margin-top:3px;"> Draft Mode <i class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');

//             if ($('#' + service_code).prop("checked") == true) {
//                 $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-3'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "'><i class='fa fa-trash' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_name + "</span></div>");
//             } else {
//                 $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
//             }

//         },
//         complete: function () {
//             updateSeenstatus(visit_id);
//             loadInvestigationHistory(1);
//         }
//     });
// }

function checkServiceRestriction(patient_id,service_code){
    let url = $('#base_url').val();
    url = url + "/emr/check_service_restriction";
    var returnVal = 0;
    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: {
            patient_id: patient_id,
            service_code: service_code,
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var service_restriction = obj.service_restriction;
            if(service_restriction == 1){
                var restrict_duration_type = obj.restrict_duration_type;
                var time_difference_minutes = obj.time_difference_minutes;
                if(restrict_duration_type == 1){
                    toastr.warning("Warning.! This Investigation Already entered "+time_difference_minutes+' minutes ago');
                    returnVal= 0;

                }else if(restrict_duration_type == 2){
                    toastr.error("Warning.! This Investigation Already entered "+time_difference_minutes+' minutes ago');
                    returnVal= 1;

                }
            }else{
                returnVal= 0;
            }

        }
    });
    return returnVal;
}

function showInvestigationHistoryList(id) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/get_investigation_history",
        data: {
            investigation_head_id: id,
            _token: _token
        },
        beforeSend: function () {
        },
        success: function (data) {
            let res = data;
            let response = '';
            var j = 1;
            if (res.length > 0) {
                let current_cat = 0;
                let last_cat = 0;
                for (var i = 0; i < res.length; i++) {
                    let id = res[i].id;
                    let image_viewer_button = '';
                    let service_code = res[i].service_code;
                    let type = res[i].investigation_type;
                    let service_desc = res[i].service_desc;
                    let sub_dept_name = res[i].sub_dept_name;
                    let accession_no = res[i].accession_no;
                    if (type.toUpperCase() == 'RADIOLOGY' || type.toUpperCase() == 'PROCEDURE') {
                        // service_desc = service_desc + " (" + sub_dept_name + ")";
                        if(accession_no != ''){
                            image_viewer_button = '<td><button type="button" class="btn btn-sm btn-primary pacsViewerBtn"  data-accession-no="'+accession_no+'" ><i class="fa fa-eye"></i> View Image</button><button type="button" class="btn btn-sm btn-primary pacsViewReportBtn" data-accession-no="'+accession_no+'"><i class="fa fa-file"></i> View Report</button></td>';
                        }

                    }

                    if (current_cat == 0) {
                        // Write the first row of the table - Category Title
                        response += '<tr class="table_header_bg"><th colspan="3" style="text-align:center">' + type.toUpperCase(); +'</th></tr>';
                    }
                    current_cat = type;
                    if (last_cat != 0) {
                        if (current_cat != last_cat) {
                            response += '<tr class="table_header_bg"><th colspan="3" style="text-align:center">' + type.toUpperCase(); +'</th></tr>';
                        }
                    }
                    response += '<tr><td>' + j + '</td><td>' + service_desc + '</td>'+image_viewer_button+'</tr>';
                    j++;
                    last_cat = current_cat;
                }
            } else {
                response = '<tr><td colspan="2" class="text-center">No Data Found..!</td></tr>';
            }

            $('#investigation_det_list_modal_content').html(response);
        },
        complete: function () {
            $("#investigation_det_list_modal").modal('show');
        }
    });
}

    $(document).on('click','#invest_view_clos',function () {
        $("#investigation_det_list_modal").modal('hide');

})
$(document).on('click','#bookmark_close',function () {
    $("#inv_bookmarks_box").modal('hide');

})
function printInvestigationHistoryList(id) {
    if (id != '' && id != undefined) {
        let url = $('#base_url').val();

        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/print_investigation_history",
            data: {
                investigation_head_id: id,
                _token: _token
            },
            beforeSend: function () {
            },
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html);
                winPrint.document.close();
                winPrint.focus();
                winPrint.print();
            },
            complete: function () {
                $('.draft_mode_indication_inv').html('');
            }
        });
    }
}


function check_all_investigation(obj) {
    var id = $(obj).attr('id');
    $('#'+id).next().trigger('click');
    setTimeout(function () {
        if ($('#'+id).prop("checked") == true) {
            $('#investigation_tab_content').find('.active').find('.ggg').find('.checkbox').find('.fav_inv').prop('checked', true);
            } else {
                $('#investigation_tab_content').find('.active').find('.ggg').find('.checkbox').find('.fav_inv').prop('checked', false);
            }
            insertOrRemoveInvToClipboard();
    },500);


}

function insertOrRemoveInvToClipboard(){
    $('#investigation_tab_content').find('.active').find('.ggg').find('.checkbox').find('.fav_inv').each(function(){
        var id = $(this).attr('id');
       // console.log(id);
        if($('#investigation_tab_content').find('.active').find('.ggg').find('.checkbox').find('#'+id).prop('checked') == true){
            var service_code = $('#'+id).val();
            var service_name = $('#'+id).next().text();
            var enable_edit_price =  $('#enable_edit_price').val();
            var is_price_editable =  $(this).data('price_editable');
            second_class = '';
            var det_id = '';
            var checked = $("#"+id).is(":checked");
            var service_items = [];
         //   console.log(service_name);
         //   console.log(service_code);
            $('#investigation_clipboard').find('.col-md-6').each(function(){
                service_items.push($(this).attr('id'));
            });
            let isInArray = service_items.includes(service_code);

            if(checked == false) {
                $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
                //return;
            }
            if(isInArray ==false){

                if ($('#investigation_tab_content').find('.active').find('.ggg').find('.checkbox').find('#' + service_code).prop("checked") == true) {
                    if(is_price_editable==1 && enable_edit_price==1){
                        $('#investigation_clipboard').append("<div class='inv_clip clip-row-"
                        + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                        + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' id='"
                        + service_code + "' style='margin-left: -35px;'>" + service_name + " </span><span class='col-md-4'><input type='text' class='invest_price' onkeypress='return isNumber(this,event)' id='invest_price"+ service_code + "'name='invest_price[]' style='margin-left:50px;width: 93px !important;'></span></div>");
                    }else{
                        $('#investigation_clipboard').append("<div class='inv_clip clip-row-"
                        + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                        + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' id='"
                        + service_code + "' style='margin-left: -35px;'>" + service_name + " </span></div>");
                    }

                } else {
                    $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
                }
            }else{
                // Command: toastr["warning"]("Item already added in the list");
                // $("#" + e.id).prop('checked', false);
            }

        }else{
            $("#investigation_clipboard").find('.clip-row-' + id).remove();
        }

    });
}


// $('.fav_inv').on('change', function () {
//     var id = $(this).attr('id');
//     if($('#'+id).prop('checked') == true){

//         var service_code = $('#'+id).val();
//         var service_name = $('#'+id).html();
//         second_class = '';
//         var det_id = '';
//         var checked = $("#" + e.id).is(":checked");
//         var service_items = [];

//         $('#investigation_clipboard').find('.col-md-10').each(function(){
//             service_items.push($(this).attr('id'));
//         });
//         let isInArray = service_items.includes(service_code);

//         if(checked == false) {
//             $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
//             return;
//         }
//         if(isInArray ==false){

//             if ($('#' + service_code).prop("checked") == true) {
//                 $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_name + "</span></div>");
//             } else {
//                 $("#investigation_clipboard").find('.clip-row-' + service_code).remove();
//             }
//         }else{
//             Command: toastr["warning"]("Item already added in the list");
//             $("#" + e.id).prop('checked', false);
//         }

//     }else{
//         $("#investigation_clipboard").find('.clip-row-' + id).remove();
//     }
// });



// function check_all_investigation(e) {
//     var del = '0';
//     var patient_id = $("#investigation_patient_id").val();
//     var admitting_doctor_id = $("#doctor_investigation").val();
//     var head_id = $("#investigation_head_id").val();
//     var encounter_id = $("#encounter_id").val();
//     var investigation_type = $('input[name="radioInline11"]:checked').val();
//     var check_value = [];
//     var check_all_id = e.value;
//     var checked = $("input[name=" + check_all_id + "]").is(':checked');
//     let url = $('#base_url').val();
//     let _token = $('#c_token').val();

//     $('#' + check_all_id).find('a').trigger('click');

//     if (checked) {
//         $("." + check_all_id).each(function () {
//             $(this).prop("checked", true);
//             check_value.push($(this).val());
//         });
//         var del = 0;
//     } else {
//         $("." + check_all_id).each(function () {
//             $(this).prop("checked", false);
//             check_value.push($(this).val());
//         });
//         var del = 1;
//     }

//     // console.log(check_value);
//     $.ajax({
//         type: "POST",
//         async: false,
//         url: url + "/emr/update_all_investigation_details",
//         data: {
//             head_id: head_id,
//             group_id: $(e).val(),
//             patient_id: patient_id,
//             encounter_id: encounter_id,
//             type: investigation_type,
//             del: del,
//             doctor_id: admitting_doctor_id,
//             _token: _token
//         },
//         beforeSend: function () {
//         },
//         success: function (data) {
//             if (data != '') {
//                 $("#investigation_head_id").val(data);
//                 $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge" style="margin-top:3px;"> Draft Mode <i class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');
//             }
//         },
//         complete: function () {
//             let h_id = $("#investigation_head_id").val();
//             if (h_id > 0 && h_id != "" && h_id != undefined) {
//                 investigationClipboard(h_id);
//             }
//         }
//     });

// }
function common_check_all_tab(group_head_id) {
    if ($('.' + group_head_id + ':checked').length == $('.' + group_head_id).length && $('.' + group_head_id + ':checked').length != 0) {
        $("input[name=" + group_head_id + "]").prop('checked', true);
    } else {
        $("input[name=" + group_head_id + "]").prop('checked', false);
    }
}


function common_ajax_loading(group_head_id, investigation_head_id, check_value, service_name) {
    // alert('ddd');return;
    if (group_head_id != '' && group_head_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        let edit_price = $('#edit_price').val();
        $.ajax({
            type: "POST",
            async: false,
            url: url + "/emr/get_all_items",
            data: {
                group_head_id: group_head_id,
                investigation_head_id: investigation_head_id,
                type: check_value,
                service_name: service_name,
                _token: _token
            },
            beforeSend: function () {

            },
            success: function (data) {
                let res = data.result;
                let remarks = data.remarks;
                let clinical_history = data.clinical_history;
                let response = '';
                var j = 0;
                if (res.length > 0) {
                    response += '<div class="tab-pane fade active in" id="invs_' + group_head_id + '" >';
                    response += '<div class="ggg chekbox_table_' + check_value + '">';
                    for (var i = 0; i < res.length; i++) {
                        if ($("." + res[i].service_code).length < 1) {
                            let id = res[i].id;
                            let service_code = res[i].service_code;
                            let checked_status = res[i].checked_status;
                            let patient_iv_id = res[i].patient_iv_id;
                            let service_desc = res[i].service_desc;
                            let sub_dept_name = res[i].sub_dept_name;

                            service_desc = service_desc.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                return letter.toUpperCase();
                            });

                            if (checked_status == 'checked_true') {
                                var check_box = 'checked=""';
                            } else {
                                var check_box = "";
                            }

                            if (check_value == 'radiology' || check_value == 'procedure') {
                                service_desc = service_desc + "(" + sub_dept_name + ")";
                            }
                            response += '<div class="checkbox checkbox-warning">\n\
                          <input id="'+ service_code + '" class="fav_inv styled ' + group_head_id + '" type="checkbox" data-service="'+ service_code + '" data-id="' + patient_iv_id + '"onclick="InsertInvestigation(this)" value="' + service_code + '" ' + check_box + ' >\n\
                              <label for="'+ service_code + '" class="' + service_code + '">' + service_desc + '</label>\n\
                      </div>\n\
                      ';
                        }
                    }
                } else {
                    response += '<div class="checkbox checkbox-warning">No Data Found..!</div>';
                }

                if (remarks != "" && remarks != undefined) {
                    $("#inv_remarks").val(remarks);
                }
                if (clinical_history != "" && clinical_history != undefined) {
                    $("#inv_clinical_history").val(clinical_history);
                }

                response += '</div></div>';
                $('#investigation_tab_content').html(response);
            },
            complete: function () {
                $("#investigation_tab_content").LoadingOverlay("hide", true);
                common_check_all_tab(group_head_id);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }
        });
    }
}

//investigation search
var inv_timeout = null;
var inv_last_search_string = '';
$('#investigation_item_search_box').on('keyup', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_inv_string = $(this).val();
    var patient_id = $('#investigation_patient_id').val();
    if (search_inv_string == "" || search_inv_string.length < 2) {
        inv_last_search_string = '';
        return false;
    } else {
        var type = $('input[name="radioInline11"]:checked').val();

        clearTimeout(inv_timeout);
        inv_timeout = setTimeout(function () {
            if (search_inv_string == inv_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/investigation-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_inv_string: search_inv_string,
                    type: type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('#InvestigationTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    let response = data;

                    let res_data = "";

                    var inv_list = $('.investigation-list-div');
                    var inv_search_list = $('#ListInvestigationSearchData');

                    $(inv_list).show();

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let service_desc = response[i].service_desc;
                            let service_code = response[i].service_code;
                            let sub_department = response[i].sub_department;
                            let price_editable = response[i].is_price_editable ? 1 : 0;

                            res_data += '<tr onclick="ListInvestigationSearch(this)" data-price_editable= '+price_editable+'><td style="text-align:center; padding:5px !important;"><i class="fa fa-star fav-' + service_code + '"  style="cursor:pointer;" id="InvestigationSearchData' + service_code + '"  onclick="favouriteInvestigation(\'' + service_code + '\', this)"></i></td><td style="cursor:pointer; padding:5px !important;">' + service_desc + '</td><input type="hidden" name="list_service_code_value_hid[]" id="list_service_code_value_hid-' + i + '" value="' + service_code + '"><td style="padding:5px !important; cursor:pointer;">' + sub_department + '</td></tr>';
                        }
                    } else {
                        price_editable = 1;
                        service_code='nocode-'+search_inv_string;
                        res_data = '<tr onclick="ListInvestigationSearch(this)" data-price_editable= '+price_editable+' class="text-center"><td style="width:5%;"><i class="fa fa-star fav-'+search_inv_string+'"  onclick="favouriteInvestigation(\''+ service_code + '\', this)"></i></td><td style="width: 70%; ">No Data Found..!</td><td><button type="button" class="btn btn-sm btn-block btn-success" style=" margin: auto; " onclick="addOutsideInvestigation();"><i class="fa fa-plus"></i> Add Outside Investigation</button></td></tr>';

                    }

                    $(inv_search_list).html(res_data);
                    inv_last_search_string = search_inv_string;
                    $(".inv_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.inv_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

function addOutsideInvestigation() {
    var service_desc = $("#investigation_item_search_box").val();
    console.log(service_desc);
        let service_code = '';
        det_id = '';
    if (service_desc != '' && service_desc != undefined) {

        $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_desc + "</span></div>");


        $('#investigation_item_search_box').val('');
    }
}



// function addOutsideInvestigation() {
//     var service_desc = $("#investigation_item_search_box").val();
//     let service_code = '';

//     if (service_desc != '' && service_desc != undefined) {
//         var patient_id = $("#investigation_patient_id").val();

//         var head_id = $("#investigation_head_id").val();
//         var investigation_type = $('input[name="radioInline11"]:checked').val();
//         var admitting_doctor_id = $("#doctor_investigation").val();
//         let url = $('#base_url').val();
//         let _token = $('#c_token').val();
//         $.ajax({
//             type: "POST",
//             url: url + "/emr/update_investigation_details",
//             data: {
//                 head_id: head_id,
//                 service_code: service_code,
//                 service_desc: service_desc,
//                 patient_id: patient_id,
//                 type: investigation_type,
//                 item_id: 0,
//                 doctor_id: admitting_doctor_id,
//                 _token: _token
//             },
//             beforeSend: function () {
//                 $('.tab-pane').append('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
//             },
//             success: function (data) {
//                 $('.investigation-list-div').hide();
//                 $(".fa-spinner").css('display', 'none');
//                 let det_id = (data.detail_id != "" && data.detail_id != undefined) ? data.detail_id : 0;
//                 if (data.head_id > 0) {
//                     $("#investigation_head_id").val(data.head_id);
//                 }
//                 window.atleastOneSavedRecords = true;
//                 //  Command: toastr["success"]("Successfully Updated");
//                 $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge" style="margin-top:3px;"> Draft Mode <i class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');

//                 $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-3'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "' ><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_desc + "</span></div>");

//                 $('#investigation_item_search_box').val('');

//             },
//             complete: function () {

//             }
//         });
//     }
// }

//close frequency search
$(document).on('click', '.close_btn_inv_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".investigation-list-div").hide();
});


//when select Investigation Search
function ListInvestigationSearch(event) {
    let tr = $(event);
    let det_id = '';
    let td = $(tr).find('td');
    var enable_edit_price =  $('#enable_edit_price').val();
    var is_price_editable = $(event).data('price_editable');
    let service_code = $(tr).find('input[name="list_service_code_value_hid[]"]').val();
    let service_name = $(tr).find(td).text();
    var service_items = [];
    if(service_code != undefined){
        service_items.splice(0, service_items.length);
        $('#investigation_clipboard').find('.col-md-6').each(function(){
            service_items.push($(this).attr('id'));
        });
        console.log(service_items);
        let isInArray = service_items.includes(service_code);
        // var exists = true;
        // // if($('#investigation_clipboard').hasClass('common_service')){
        // $('.common_service').each(function(){
        //     var service_code_inve=$(this).attr('attr-id');
        //  if(service_code_inve==service_code){

        //     exists = false;
        //  }else{
        //     exists = true;
        //  }
        // });
        if(isInArray ==false){

            if(is_price_editable==1 && enable_edit_price==1){
            $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code
            + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id
            + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' id='"
            + service_code + "' ' style='margin-left:-35px;'>" + service_name + "</span><span class='col-md-4'><input type='text' onkeypress='return isNumber(this,event)' class='invest_price' id='invest_price"+ service_code + "'name='invest_price[]' style='margin-left:49px;width: 93px !important;'></span></div>");
            }else{
                $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code
                + " col-md-12'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id
                + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' id='"
                + service_code + "' style='margin-left:-34px;color: black !important;'>" + service_name + "</span></div>");
            }

        }else{
            Command: toastr["warning"]("Already Exists");
        }
    }

}

// $(document).on('dblclick', '#ListInvestigationSearchData tr', function (event) {
//     event.preventDefault();
//     /* Act on the event */

//     let tr = $(this);

//     let td = $(tr).find('td');

//     let service_code = $(tr).find('input[name="list_service_code_value_hid[]"]').val();
//     let service_desc = $(tr).find(td).text();
//     var service_restriction_flag = 0;
//     if (service_code != '' && service_code != undefined) {
//         var patient_id = $("#investigation_patient_id").val();

//         var head_id = $("#investigation_head_id").val();
//         var investigation_type = $('input[name="radioInline11"]:checked').val();
//         var investigation_intend_type = $("input[name='investigation_intend_type']:checked").val();
//         var admitting_doctor_id = $("#doctor_investigation").val();
//         let url = $('#base_url').val();
//         let _token = $('#c_token').val();
//         service_restriction_flag = checkServiceRestriction(patient_id,service_code);
//         if(service_restriction_flag == 1){
//             return false;
//         }

//         $.ajax({
//             type: "POST",
//             url: url + "/emr/update_investigation_details",
//             data: {
//                 head_id: head_id,
//                 service_code: service_code,
//                 patient_id: patient_id,
//                 type: investigation_type,
//                 investigation_intend_type: investigation_intend_type,
//                 item_id: 0,
//                 doctor_id: admitting_doctor_id,
//                 _token: _token
//             },
//             beforeSend: function () {
//                 $('.tab-pane').append('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
//             },
//             success: function (data) {
//                 $('.investigation-list-div').hide();
//                 $(".fa-spinner").css('display', 'none');
//                 let det_id = (data.detail_id != "" && data.detail_id != undefined) ? data.detail_id : 0;
//                 if (data.head_id > 0) {
//                     $("#investigation_head_id").val(data.head_id);
//                 }
//                 window.atleastOneSavedRecords = true;
//                 //  Command: toastr["success"]("Successfully Updated");
//                 $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge" style="margin-top:3px;"> Draft Mode <i class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');

//                 $('#investigation_clipboard').append("<div class='inv_clip clip-row-" + service_code + " col-md-3'><span class='iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='" + det_id + "' attr-service='" + service_code + "' ><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-10' id='" + service_code + "'>" + service_desc + "</span></div>");

//                 $('#investigation_item_search_box').val('');

//             },
//             complete: function () {

//             }
//         });
//     }

// });

function investigationClipboard(head_id) {

    var type = $('input[name="radioInline11"]:checked').val();
    var enable_edit_price = $('#enable_edit_price').val();
    console.log(enable_edit_price)
    var is_price_editable = $('#invest_price').val();
    if (head_id != undefined && head_id != "") {
        var patient_id = $("#investigation_patient_id").val();
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/investigation_clipboard_list",
            data: {
                head_id: head_id,
                type: type,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $('#investigation_clipboard').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('.inv_clip').remove();
                $('#investigation_clipboard').LoadingOverlay("hide");
                if (data.length > 0) {
                    $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('.fav_inv').prop("checked", false);

                    $(data).each(function (ind, dat) {
                        let service_code = dat.service_code;
                        let service_desc = dat.service_desc;
                        let sub_dept_name = dat.sub_dept_name;
                        let price = dat.price;
                        if(price==0.00){
                            price='';
                        }
                        let is_price_editable = dat.is_price_editable;
                        let id = dat.id;
                        if (type == 'radiology' || type == 'procedure') {
                            service_desc = service_desc + "(" + sub_dept_name + ")";
                        }

                       if(is_price_editable==1 && enable_edit_price==1){
                        $('#investigation_clipboard').append("<div class='inv_clip clip-row-"
                        +service_code + " col-md-12'><span class=' iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                        + id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' style='margin-left:-35px;' id='"
                        + service_code + "'>" + service_desc + "</span><span class='col-md-4'><input type='text' onkeypress='return isNumber(this,event)' class='invest_price' id='invest_price"
                        + service_code + "' value='"+price+"' style=' margin-left:50px;width: 93px !important;'></span></div>");
                       }else{
                        $('#investigation_clipboard').append("<div class='inv_clip clip-row-"
                        +service_code + " col-md-12'><span class=' iv_trash_btn_span col-md-2' style='' onclick='trashClipboard(this)' attr-id='"
                        + id + "' attr-service='" + service_code + "'><i class='fa fa-times-circle' style='font-size:14px;float:left;'></i></span><span class='col-md-6' style='margin-left:-35px;' id='"
                        + service_code + "'>" + service_desc + "</span></div>");
                       }
                        $('#investigation_tab_content').find('.tab-pane').find('.ggg').find('#'+service_code).prop("checked", true);

                    });


                } else {
                    $('#investigation_clipboard').html("");
                }
            },
            complete: function () {

            }
        });
    }
}

function favouriteInvestigation(service_code = '', element = '') {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    if (service_code) {
        $(".fav_service_code").val(service_code);
    }
    $.ajax({
        type: "POST",
        url: url + "/emr/getAllFavGroups",
        data: {
            service_code: $(".fav_service_code").val(),
            _token: _token

        },
        beforeSend: function () {
            if (element) {
                $(element).removeClass('fa-star').addClass('fa-spinner').addClass('fa-spin');
            }
            $('.add_fav_group_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (data) {
            if (element) {
                $(element).removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-star');
            }
            var item_code = $(".fav_service_code").val();
            $('.add_fav_group_list').LoadingOverlay("hide");
            $(".add_fav_group_list").empty();
            if (data) {
                $.each(data, function (key, val) {
                    var checked = '';
                    if (val.favorite_status == 1) {
                        checked = "checked='checked'";
                    }
                    $(".add_fav_group_list").append('<li onclick="updateInvestigationToFavGroup(\'' + item_code + '\', \'' + val.id + '\');" class="head_tab_' + val.id + '" data-id="' + val.id + '" style="display:flex;height: 30px; cursor:pointer;"><input style="margin-right: 5px;margin-left: 5px;"  class="styled" type="checkbox" name="fav_group_id" value="' + val.id + '" ' + checked + '> <a aria-expanded="true" style="width:93%;padding:0px;background-color:white !important;color:black !important;" onclick=""><h5 style="margin-top:8px;margin-left:5px;">' + val.group_name + '</h5></a></li>')
                })
                $(".fav_investigation_box").show();
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }
        },
        complete: function () {

        }
    });
}

function updateInvestigationToFavGroup(service_code, group_id) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    let type=$('.gen_exam_acne .active input').val();

    $.ajax({
        type: "POST",
        url: url + "/emr/updateInvestigationToFavGroup",
        data: {
            service_code: service_code,
            group_id: group_id,
            _token: _token,
            type:type
        },
        beforeSend: function () {
            $('.add_fav_group_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (data) {
            $('.add_fav_group_list').LoadingOverlay("hide");
            if (data.status == 1) {
                favouriteInvestigation();
                changeRadio($('input[name="radioInline11"]:checked').get(0))
                if(service_code.split('-')[0] =='nocode'){
                    $('#invs_'+group_id).find('.ggg').append('<div class="checkbox checkbox-primary chekbox_table_lab"><input id="'+service_code+'" data-service="'+ service_code + '" class="fav_inv styled '+group_id+'" type="checkbox"  onclick="InsertInvestigation(this)" data-id="undefined" value="'+service_code+'"> <label for="'+service_code+'"class="'+service_code+'">'+service_code.split('-')[1]+'</label></div>');
                }

                Command: toastr["success"]("Success");
            }
        },
        complete: function () {

        }
    });

}

$(document).on('click', '.invst_add_group_btn', function () {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    var group_name = $('.invst_fav_add_group_text').val();
    if (group_name.trim() != '') {
        $.ajax({
            type: "POST",
            url: url + "/emr/createNewInvestigationGroup",
            data: {
                group_name: $('.invst_fav_add_group_text').val(),
                _token: _token
            },
            beforeSend: function () {
                $('.add_group_icon').removeClass('fa-plus').addClass('fa-spinner').addClass('fa-spin');

            },
            success: function (data) {
                $('.add_group_icon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-plus');
                $('.invst_fav_add_group_text').val('');
                if (data.status == 1) {
                    favouriteInvestigation();
                }

            },
            complete: function () {

            }
        });
    } else {
        Command: toastr["error"]("Enter Group Name");
    }

})


//close invst_close
$(document).on('click', '.invst_close_fav', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".fav_investigation_box").hide();
});

//close invst_close
$(document).on('click', '.show_inv_close_bookmark', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".show_inv_bookmarks_box").hide();
});


function trashClipboard(obj) {
    let id = $(obj).attr('attr-id');
    let service_code = $(obj).attr('attr-service');    
    if (id != '' && service_code != '') {
        deleteInvfromDb(id, obj);
        $('input[type="checkbox"][id="' + service_code + '"]').prop('checked', false);
    } else {
        $('#' + service_code).trigger('click');
        $(obj).closest('.clip-row-' + service_code + '').remove();
    }
}
function deleteInvfromDb(id, obj ='') {
    if (id > 0 && id != "" && id != undefined) {
        var patient_id = $("#investigation_patient_id").val();
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        let service_code = $(obj).attr('attr-service');
        $.ajax({
            type: "POST",
            url: url + "/emr/investigation-item-delete",
            data: {
                detail_id: id,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $(obj).find('i').removeClass('fa-times-circle').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (parseInt(data.status) == 0) {
                    toastr.warning("Item already converted..!");
                } else {
                    $(obj).closest('.clip-row-' + service_code + '').remove();
                }
            },
            complete: function () {
                $(obj).find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-times-circle');
            }
        });
    }
}

function saveInvRemarks() {
    let inv_remarks = $('#inv_remarks').val();
    let head_id = $('#investigation_head_id').val();
    if (head_id != "" && head_id > 0) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/investigation-remarks-save",
            data: {
                head_id: head_id,
                inv_remarks: inv_remarks,
                _token: _token
            },
            beforeSend: function () {

            },
            success: function (data) {

            },
            complete: function () {

            }
        });
    } else {
        // Command: toastr["warning"]("Error.! Select Investigation.");
    }
}

function saveClinicalHistory() {
    let inv_clinical_history = $('#inv_clinical_history').val();
    let head_id = $('#investigation_head_id').val();
    if (head_id != "" && head_id > 0) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/investigation-clinical-history-save",
            data: {
                head_id: head_id,
                inv_clinical_history: inv_clinical_history,
                _token: _token
            },
            beforeSend: function () {

            },
            success: function (data) {

            },
            complete: function () {

            }
        });
    } else {
        // Command: toastr["warning"]("Error.! Select Investigation.");
    }
}
function editInvestigationHistoryList(head_id) {

    if (head_id != "" && head_id > 0) {
        $('#investigation_head_id').val(head_id);
        //select 1st tab investigation
        let group_id = $('.investigation_fav_tabs_list li:first').attr('data-id');
        if (group_id != "" && group_id != undefined) {
            loadFavoriteItemsTab(group_id, 1);
        }
        investigationClipboard(head_id);
    }
}
function deleteInvestigationHistoryList(obj, id) {
    if (id != "" && id > 0) {
        var result = confirm("Want to delete?");
        if (result) {

            let _token = $('#c_token').val();
            var url = $('#base_url').val() + "/emr/remove-patient-investigation";
            $.ajax({
                type: "POST",
                url: url,
                data: "removeInvestigation=" + id + "&_token=" + _token,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status == 1) {
                        $(obj).closest('tr').remove();
                        Command: toastr["success"]("Removed Successfully..!");
                    }else if (data.status == 2) {
                        Command: toastr["warning"]("Item already converted..!");
                    }
                },
                complete: function () {
                }
            });
        }
    }
}

function getLabResultRtf(lab_results_id) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    $.ajax({
        type: "GET",
        url: url,
        data: "lab_results_id=" + lab_results_id,
        beforeSend: function () {
            $('#lab_results_rtf_modal').modal('show');
        },
        success: function (data) {
            // console.log(data);
            $('#lab_results_rtf_data').html(data);
        },
        complete: function () {
        }
    });
}



//load invest history
function loadInvestigationHistory(page, history_clicked) {

    var display_status = $("#investigationCollapse").css('display');

    if (display_status == "block" && history_clicked) {
        $(".visit_wise_filter_inv_div").hide();
        $("#visit_wise_filter_inv").val("ALL");
        return;
    }

    $(".visit_wise_filter_inv_div").show();

    var url = $('#base_url').val() + "/emr/get_investigation_history_heads";
    var patient_id = $('#investigation_patient_id').val();
    var visit_type = $('#visit_wise_filter_inv').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        async: true,
        data: {
            investigation_history: 1,
            page: page,
            patient_id: patient_id,
            visit_type: visit_type,
            _token: _token,
        },
        beforeSend: function () {
            $('#investigation-history-data-list').empty();
            $(".inv_history_btn").find('i').removeClass('fa-history').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#investigation-history-data-list').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $(".popoverData").popover({ trigger: "hover", html: true });
            }
        },
        complete: function () {
            $(".inv_history_btn").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-history');
            $(".theadfix_wrapper").floatThead('reflow');
            $('.theadscroll').perfectScrollbar("update");
        }
    });
}


$('#investigation-history-data-list').on('click', '.pagination a', function (e) {
    loadInvestigationHistory($(this).attr('href').split('page=')[1]);
    $(".popoverData").popover({ trigger: "hover", html: true });
    e.preventDefault();
});


$(document).on('ready', function () {
    $("#doctor_investigation").select2();
})

function loadOutsideInvestigation() {
    $('#outsideInvestigationModal').modal('toggle');
}

function outsideInvestigationRow() {

    var table = document.getElementById("outsideInvestigationTable").getElementsByTagName('tbody')[0];
    var countRow = document.getElementById("outsideInvestigationTable").rows.length;
    countRow = countRow + 1;
    if (countRow == 0) {
        countRow = 1;
    }

    var row = table.insertRow(table.rows.length);
    var cell0 = row.insertCell(0);
    var cell1 = row.insertCell(1);
    cell0.setAttribute("width", "90%;");

    cell0.innerHTML = "<input type='text' class='form-control bottom-border-text' value='' onfocus='PreviewDescription(this)' autocomplete='off' placeholder='Outside Investigation' data-med='0' name='outsideInvestigation[]' id='outsideInvestigation-" + countRow + "'>";

    // cell0.innerHTML = "<input type='text' class='form-control btn-without-border saveMedicationAllergyNewItem' placeholder='NKDA' value='' autocomplete='off' " +
    // " onKeyup='searchMedicine2(this.id,event)' name='allergymedicine[]' id='allergymedicine-"+countRow+"'>";

    cell1.innerHTML = "<button type='button' class='btn btn-danger' style='' value=''onclick='deleteoutsideInvestigation(this)' id='delete-" + countRow + "'><i class='fa fa-times-circle'></i></button>";

    $('#outsideInvestigationTable' + countRow).focus();

    $('[data-toggle="popover"]').popover();
    $("#outsideInvestigationModal").animate({ scrollTop: $('#outsideInvestigationModal').prop("scrollHeight") }, 1000);

}

function deleteoutsideInvestigation(x) {

    if (confirm("Are you sure you want to delete this?")) {

        var DelIndex = x.closest('tr').rowIndex;
        var table = document.getElementById("outsideInvestigationTable");
        table.deleteRow(DelIndex);

    }

}

function print_patient_investigation() {
    save_draft_investigation(1,1);

}

function saveInvestigation() {
    save_draft_investigation(1,0);
}
function save_investigation() {
    save_draft_investigation(2,0);
}

function showInvestigationBookmarks() {
    $('#inv_bookmarks_box').modal('show');

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/getAllFavGroups",
        data: {
            _token: _token
        },
        beforeSend: function () {
            $("#showInvestigationBookmarks").find('i').removeClass('fa-star').addClass('fa-spinner').addClass('fa-spin');
            $('.add_investigation_group_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (data) {
            $("#showInvestigationBookmarks").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-star');
            $('.add_investigation_group_list').LoadingOverlay("hide");
            $(".add_investigation_group_list").empty();
            if (data) {
                $.each(data, function (key, val) {
                    $(".add_investigation_group_list").append('<li class="head_tab_' + val.id + '" data-id="' + val.id + '" style="height:35px;"><div class="col-md-12 padding_sm"><div class="col-md-11 padding_sm" data-group="'+val.group_name+'" id="group_name_add"><div style="float:left;" class="checkbox checkbox-success inline no-margin"><input onclick="fetchFavoriteGroupItems(' + val.id + ');" type="checkbox" class="FavoriteGroups" id="group_namedata' + val.id + '" name="detail" value="' + val.id + '"><label style="padding-left: 2px;" for="group_namedata' + val.id + '">' + val.group_name + '</label></div></div><div class="col-md-1 padding_sm"><i style="color:red;font-size:19px;" onclick="deleteFavoriteGroup(' + val.id + ');" class="fa fa-times-circle"></i></div></div></li>')
                })
                $(".show_inv_bookmarks_box").show();
            }
        },
        complete: function () {

        }
    });
}

function deleteFavoriteGroup(group_id) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/deleteFavoriteGroup",
        data: {
            group_id: group_id,
            _token: _token
        },
        beforeSend: function () {
            $(this).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $("#showInvestigationBookmarks").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
            Command: toastr["success"]("Successfully removed");
            showInvestigationBookmarks();
            changeRadio($('input[name="radioInline11"]:checked').get(0))
        },
        complete: function () {

        }
    });

}

function fetchFavoriteGroupItems(group_id) {
    var group_id_array = [];
    $('.FavoriteGroups').each(function () {
        if ($(this).is(':checked')) {
            group_id_array.push($(this).val());
        }
    });
    // console.log(group_id_array);
    // return;
    window.favoriteGroupId = group_id;
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/fetchFavoriteGroupItems",
        data: {
            group_id: group_id_array,
            _token: _token
        },
        beforeSend: function () {
            $(this).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
            $('.investigation_group_items').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('.investigation_group_items').LoadingOverlay("hide");
            $(".investigation_group_items").empty();
            if (data.favorite_items.length > 0) {
                $.each(data.favorite_items, function (key, val) {
                    $(".investigation_group_items").append('<li data-fav-item-name="' + val.service_desc + '" data-id="' + val.id + '" style="height: 30px; cursor:pointer;"><div class="col-md-12 no-padding"><div class="col-md-1 no-padding"><input style="margin-right: 5px;margin-left: 5px;" onclick="InsertBookmarkInvestigation(\''+val.id+'\',\''+val.service_code+'\',\''+val.service_desc+'\',\''+val.is_price_editable+'\');"  class="styled" type="checkbox" id="inv_favorite_group_item_id_'+val.id+'" name="favorite_group_item_id" value="' + val.id + '"/></div><div class="col-md-10 no-padding"><h6 style="margin-top:2px;margin-left:5px;float:left;">' + val.service_desc + ' - ' + val.type.toUpperCase() + ' </h6></div><div class="col-md-1 no-padding"><i style="color:red;font-size:19px;" onclick="deleteFavoriteGroupItem(' + group_id + ',' + val.id + ');" class="fa fa-times-circle"></i></div></div> </li>');
                });
                $(".checkAllFavItemsBtn").show();
                $(".unCheckAllFavItemsBtn").hide();
                $(".deleteAllFavItemsBtn").show();
                $(".investigation_fav_search_div").show();
            } else {
                $(".investigation_group_items").append('<li> No favorite items </li>');
                $(".checkAllFavItemsBtn").hide();
                $(".unCheckAllFavItemsBtn").hide();
                $(".deleteAllFavItemsBtn").hide();
                $(".investigation_fav_search_div").hide();
            }
        },
        complete: function () {

        }
    });

}


function deleteFavoriteGroupItem(group_id, item_id) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    var that = this;
    var item_ids = [];
    item_ids.push(item_id);
    $.ajax({
        type: "POST",
        url: url + "/emr/deleteFavoriteGroupItem",
        data: {
            item_id: item_ids,
            _token: _token
        },
        beforeSend: function () {
            $(that).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
            Command: toastr["success"]("Successfully removed");
            fetchFavoriteGroupItems(group_id);
            changeRadio($('input[name="radioInline11"]:checked').get(0))
        },
        complete: function () {

        }
    });

}


function checkAllFavItemsBtn() {
    $(".checkAllFavItemsBtn").hide();
    $(".unCheckAllFavItemsBtn").show();
    $('input[name="favorite_group_item_id"]').prop("checked", true);
    $('.checkAllFavItemsBtn').find('input[type="checkbox"]').prop("checked", false);

}


function unCheckAllFavItemsBtn() {
    $(".checkAllFavItemsBtn").show();
    $(".unCheckAllFavItemsBtn").hide();
    $('input[name="favorite_group_item_id"]').prop("checked", false);
}

function deleteAllFavItemsBtn() {
    $(".checkAllFavItemsBtn").show();
    $(".unCheckAllFavItemsBtn").hide();
    var item_ids = [];
    $('input[name="favorite_group_item_id"]:checked').each(function (key, val) {
        item_ids.push($(val).val());
    });

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    var that = this;
    $.ajax({
        type: "POST",
        url: url + "/emr/deleteFavoriteGroupItem",
        data: {
            item_id: item_ids,
            _token: _token
        },
        beforeSend: function () {
            $(that).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
            Command: toastr["success"]("Successfully removed");
            if (window.favoriteGroupId) {
                fetchFavoriteGroupItems(window.favoriteGroupId);
            }
            $(".investigation_fav_search_text").val('');

        },
        complete: function () {

        }
    });



}

function investigationFavGroupAdd() {

    var group_name = $('.invstigation_fav_add_group_text').val();
    // alert(group_name)
    var that = this;
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    var exists = true;
    // if($('#investigation_clipboard').hasClass('common_service')){
    $('#group_name_add').each(function(){
        var group_name_add=$('#group_name_add').attr('data-group');
     if(group_name_add==group_name){
        exists = false;
     }else{
        exists = true;
     }
    });
    if (group_name.trim() != '' && exists) {
        $.ajax({
            type: "POST",
            url: url + "/emr/createNewInvestigationGroup",
            data: {
                group_name: $('.invstigation_fav_add_group_text').val(),
                _token: _token
            },
            beforeSend: function () {
                $(that).find('i').removeClass('fa-plus').addClass('fa-spinner').addClass('fa-spin');

            },
            success: function (data) {
                $(that).find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-plus');
                $('.invstigation_fav_add_group_text').val('');
                if (data.status == 1) {

                    showInvestigationBookmarks();
                    changeRadio($('input[name="radioInline11"]:checked').get(0))

                }

            },
            complete: function () {

            }
        });
    }else{
              Command: toastr["warning"]("Item already added in the list");

    }
}


$(document).on('keyup', '.investigation_fav_search_text', function () {
    var searchVal = $(this).val();

    if (searchVal != '') {
        $(".investigation_group_items li").hide();
        $('.investigation_group_items li[data-fav-item-name^="' + searchVal.toLowerCase() + '"]').show();
        $('.investigation_group_items li[data-fav-item-name^="' + searchVal.toUpperCase() + '"]').show();
    } else {
        $(".investigation_group_items li").show();
    }
});


$('#toggle_fav_group_add').on('click', function() {
    $('.invstigation_fav_add_group_text').toggle();
});

$('.invstigation_fav_add_group_text').on('keyup', function(e) {
    if(e.keyCode == 13){
        investigationFavGroupAdd();
    }
});
$('.invstigation_fav_add_group_text').on('blur', function(e) {
    investigationFavGroupAdd();
});


