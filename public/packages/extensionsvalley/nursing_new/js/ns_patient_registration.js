// $(document).on("blur", "#new_reg_age", function(){

//     var age = $(this).val();
//     var regex = /^(?:[0-9]|[1-9][0-9]|1[01][0-9]|110)$/;
//     if (!regex.test(age)) {
//       toastr.warning('Please enter age between 0 and 110!');
//       document.getElementById("new_reg_age").value = "";
//       $('#new_reg_dob').val('');
//       return false;
//     }else{
//         var today = new Date();
//         var dob  = new Date(today.getFullYear() - age, today.getMonth(), today.getDate());
//         var dob = moment(dob);
//         var dob = dob.format("MMM-DD-YYYY");
//         $('#new_reg_dob').val(dob);
//     }
// });

function calculateDOB() {
    // Get values from age input fields
    var years = parseInt($('#new_reg_age_y').val()) || 0;
    var months = parseInt($('#new_reg_age_m').val()) || 0;
    var days = parseInt($('#new_reg_age_d').val()) || 0;

    var y_regex = /^(?:[0-9]|[1-9][0-9]|1[01][0-9]|110)$/;
    if (!y_regex.test(years)) {
        toastr.warning('Please enter age between 0 and 110!');
        document.getElementById("new_reg_age_y").value = "";
        $('#new_reg_dob').val('');
        return false;
    }

    // Use RegExp constructor for months regex
    var m_regex = /^(0?[1-9]|1[012])$/;
    if (!m_regex.test(months)) {
        toastr.warning('Please enter Month between 0 and 12!' + months);
        document.getElementById("new_reg_age_m").value = "";
        $('#new_reg_dob').val('');
        return false;
    }

    // Get current date
    var currentDate = new Date();

    // Calculate DOB
    var dob = new Date(
        currentDate.getFullYear() - years,
        currentDate.getMonth() - months,
        currentDate.getDate() - days
    );

    // Format DOB and set the value to the date input field
    var formattedDOB = moment(dob).format("MMM-DD-YYYY");
    $('#new_reg_dob').val(formattedDOB);
}


$('#new_reg_age_y, #new_reg_age_m, #new_reg_age_d').on('input', calculateDOB);


$(document).on("blur", "#new_reg_dob", function(){
    var dob = $(this).val();

    const inputDate = dob;
    const formattedDate = moment(inputDate, 'MMM-DD-YYYY').format('DD/MM/YYYY');

    var dob = moment(dob);
    var dob = dob.format("DD/MM/YYYY");
    var dateArray = getAge(dob);
    $('#new_reg_age_y').val(dateArray['years']);
    $('#new_reg_age_m').val(dateArray['months']);
    $('#new_reg_age_d').val(dateArray['days']);

});

function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    var dob = new Date(
      dateString.substring(6, 10),
      dateString.substring(3, 5) - 1,
      dateString.substring(0, 2)
    );

    var yearAge = now.getFullYear() - dob.getFullYear();
    var monthAge = now.getMonth() - dob.getMonth();
    var dateAge = now.getDate() - dob.getDate();

    if (dateAge < 0) {
      monthAge--;
      dateAge += daysInMonth(now.getMonth(), now.getFullYear());
    }

    if (monthAge < 0) {
      yearAge--;
      monthAge += 12;
    }

    var age = {
      years: yearAge,
      months: monthAge,
      days: dateAge,
    };

    var yearString = age.years > 1 ? " years" : " year";
    var monthString = age.months > 1 ? " months" : " month";
    var dayString = age.days > 1 ? " days" : " day";

    var ageString = "";

    if (age.years > 0) {
      ageString += age.years + yearString;

      if (age.months > 0 && age.days > 0) {
        ageString += ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
      } else if (age.months > 0) {
        ageString += " and " + age.months + monthString + " old.";
      } else if (age.days > 0) {
        ageString += " and " + age.days + dayString + " old.";
      } else {
        ageString += " old. Happy Birthday!!";
      }
    } else if (age.months > 0) {
      ageString += age.months + monthString;

      if (age.days > 0) {
        ageString += " and " + age.days + dayString + " old.";
      } else {
        ageString += " old.";
      }
    } else if (age.days > 0) {
      ageString += age.days + dayString + " old!";
    } else {
      ageString = "Oops! Could not calculate age!";
    }

    var returndata = {
        'years': age.years,
        'months': age.months,
        'days': age.days
    };

    return returndata;
}






var token = $('#c_token').val();
var base_url = $('#base_url').val();

//---patient registration-------------------------
function nsRegisterNewPatient(){
    var title = $('#new_reg_title').val();
    var patient_name = $('#new_reg_patient_name').val();
    var age = $('#new_reg_age_y').val();
    var dob = $('#new_reg_dob').val();
    var phone = $('#new_reg_phone').val();
    var gender = $('#new_reg_gender').val();
    var doctor_id = $('#new_reg_doctor_id').val();
    var department = $('#new_reg_department').val();
    var dobnotknown = $('#dobnotknown').val();
    if(title ==''){
        toastr.warning('Please enter patient title!');
        return false;
    }
    if(patient_name ==''){
        toastr.warning('Please enter patient name!');
        return false;
    }
    if(age ==''){
        toastr.warning('Please enter patient age!');
        return false;
    }
    if(dob ==''){
        toastr.warning('Please enter patient date of birth!');
        return false;
    }
    if(phone ==''){
        toastr.warning('Please enter patient phone number!');
        return false;
    }
    if(gender ==''){
        toastr.warning('Please select patient gender!');
        return false;
    }
    if(doctor_id ==''){
        toastr.warning('Please select doctor!');
        return false;
    }
    if(department ==''){
        toastr.warning('Please select department!');
        return false;
    }

    var dataparams = {
        '_token':token,
        'title':title,
        'patient_name':patient_name,
        'age':age,
        'dob':dob,
        'phone':phone,
        'gender':gender,
        'doctor_id':doctor_id,
        'department':department,
        'dobnotknown':dobnotknown,
    };

    var url = base_url + "/nursing_new/ns_patient_registration";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("#tab_registration").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $("#tab_registration").LoadingOverlay("hide");
            var obj = JSON.parse(data);
            if(obj.status == 1){
                toastr.success(obj.message);
                bootbox.alert({
                    message: 'Patient registered successfully!<br><table><tr><td>Patient:</td><td>'+patient_name+'</td><br></tr><tr><td>Uhid:</td><td>'+obj.uhid+'</td></tr><br><tr><td>Bill no:</td><td>'+obj.bill_no+'</td></tr></table>',
                    backdrop: true,
                });

                $('#reset_patient_registration').trigger('click');
                $('#new_reg_department').val(null).trigger('change');
                $('#new_reg_doctor_id').val(null).trigger('change');

            }else{
                toastr.error(obj.message);
            }
        },
        complete: function () {
            $("#tab_registration").LoadingOverlay("hide");
            $('#reset_patient_registration').trigger('click');
        }
    });
}

function selectDoctorsList() {
    var url = base_url + "/appointment_api/select_doctors_using_speciality";
    var speciality = $('#new_reg_department').val();
    var param = {speciality: speciality};
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#new_reg_doctor_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#new_reg_doctor_id").select2("val", "");
            // $('#new_reg_doctor_id').val('');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#new_reg_doctor_id').LoadingOverlay("hide");
            $('#new_reg_doctor_id').empty().trigger("change");
            var doctors_list = obj.response;
            doctors_list = $.parseJSON(doctors_list);
            $("#new_reg_doctor_id").append('<option value="" selected>Select</option>');
            if(doctors_list) {
                $.each(doctors_list, function (key, value) {
                    $("#new_reg_doctor_id").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
            }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function selectDoctorsList1() {
    var url = base_url + "/appointment_api/select_doctors_using_speciality";
    var speciality = $('#new_ren_department').val();
    var param = {speciality: speciality};
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#new_ren_doctor_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#new_ren_doctor_id").select2("val", "");
            // $('#new_ren_doctor_id').val('');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#new_ren_doctor_id').LoadingOverlay("hide");
            $('#new_ren_doctor_id').empty().trigger("change");
            var doctors_list = obj.response;
            doctors_list = $.parseJSON(doctors_list);
            $("#new_ren_doctor_id").append('<option value="" selected>Select</option>');
            if(doctors_list) {
                $.each(doctors_list, function (key, value) {
                    $("#new_ren_doctor_id").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
            }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


//-----------uhid patient search---------------------------------
$(document).on("keyup", "#ren_patient_uhid", function (e) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if ($('#ren_patient_uhid_hidden').val() != "") {
            $('#ren_patient_uhid_hidden').val('');
            $("#ren_patient_uhidAjaxDiv").html("").hide();
        }
        var patient_name_search = $(this).val();
        if (patient_name_search == "") {
            $("#ren_patient_uhidAjaxDiv").html("").hide();
        }else{
            var url = base_url + "/nursing_new/uhid_patient_search";
            $.ajax({
                type: "GET",
                url: url,
                data: "patient_name_search_main=" + patient_name_search,
                beforeSend: function () {
                    $("#ren_patient_uhidAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ren_patient_uhidAjaxDiv").html(html).show();
                    $("#ren_patient_uhidAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }
    }else{
        ajax_list_key_down('ren_patient_uhidAjaxDiv',event);
    }
});

$("#ren_patient_uhid").on('keydown',function(event){
    if(event.keyCode == 13){
        ajaxlistenter('ren_patient_uhidAjaxDiv');
        return false;
    }
});

//------------------//-----------------------

$(document).on("change", "#new_reg_age", function (e) {
    $('#dobnotknown').val(0);
});
$(document).on("blur", "#new_reg_dob", function (e) {
    $('#dobnotknown').val(1);
});

$(document).on("click", "#reset_patient_registration", function(){
    $('#new_reg_department').val(null).trigger('change');
    $('#new_reg_doctor_id').val(null).trigger('change');
});
$(document).on("click", "#reset_patient_renewal", function(){
    $('#new_ren_department').val(null).trigger('change');
    $('#new_ren_doctor_id').val(null).trigger('change');
});

$(document).on("click", "#reset_ext_patient_registration", function(){
    $('#externel_reg_department').val(null).trigger('change');
    $('#externel_reg_doctor_id').val(null).trigger('change');
});

function fillRenewalPatientValues(id,patient_name,title,uhid,age,dob,gender,phone){
    $("#ren_patient_uhidAjaxDiv").html("").hide();
    $('#new_ren_title').val(atob(title));
    $('#ren_patient_uhid_hidden').val(uhid);
    $('#ren_patient_id_hidden').val(id);
    $('#new_ren_patient_name').val(atob(patient_name));
    $('#new_ren_age').val(age);
    var dob = moment(dob).format('MMM-DD-YYYY');
    $('#new_ren_dob').val(dob);
    $('#new_ren_gender').val(gender);
    $('#new_ren_phone').val(phone);
}

//-----patient renewal----------------------------------------

function nsRenewalNewPatient(){
    var title = $('#new_ren_title').val();
    var patient_name = $('#new_ren_patient_name').val();
    var patient_id = $('#ren_patient_id_hidden').val();
    var uhid = $('#ren_patient_uhid_hidden').val();
    var age = $('#new_ren_age').val();
    var dob = $('#new_ren_dob').val();
    var phone = $('#new_ren_phone').val();
    var gender = $('#new_ren_gender').val();
    var doctor_id = $('#new_ren_doctor_id').val();
    var department = $('#new_ren_department').val();
    if(patient_name ==''){
        toastr.warning('Please select patient!');
        return false;
    }
    if(title ==''){
        toastr.warning('Please enter patient title!');
        return false;
    }
    if(age ==''){
        toastr.warning('Please enter patient age!');
        return false;
    }
    if(dob ==''){
        toastr.warning('Please enter patient date of birth!');
        return false;
    }
    if(phone ==''){
        toastr.warning('Please enter patient phone number!');
        return false;
    }
    if(gender ==''){
        toastr.warning('Please select patient gender!');
        return false;
    }
    if(doctor_id ==''){
        toastr.warning('Please select doctor!');
        return false;
    }
    if(department ==''){
        toastr.warning('Please select department!');
        return false;
    }

    var dataparams = {
        '_token':token,
        'title':title,
        'patient_id':patient_id,
        'uhid':uhid,
        'patient_name':patient_name,
        'age':age,
        'dob':dob,
        'phone':phone,
        'gender':gender,
        'doctor_id':doctor_id,
        'department':department,
    };

    var url = base_url + "/nursing_new/ns_patient_renewal";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("#tab_renewal").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $("#tab_renewal").LoadingOverlay("hide");
            var obj = JSON.parse(data);
            if(obj.status == 1){
                toastr.success(obj.message);
                bootbox.alert({
                    message: 'Patient renewal completed successfully!<br><table><tr><td>Patient:</td><td>'+patient_name+'</td><br></tr><br><tr><td>Bill no:</td><td>'+obj.bill_no+'</td></tr></table>',
                    backdrop: true,
                });

                $('#reset_patient_renewal').trigger('click');
                $('#new_ren_department').val(null).trigger('change');
                $('#new_ren_doctor_id').val(null).trigger('change');

            }else{
                toastr.error(obj.message);
            }
        },
        complete: function () {
            $("#tab_renewal").LoadingOverlay("hide");
            $('#reset_patient_renewal').trigger('click');
        }
    });
}
