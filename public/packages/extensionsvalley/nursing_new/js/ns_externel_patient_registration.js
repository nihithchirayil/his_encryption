$(document).on("blur", "#externel_reg_age", function(){

    var age = $(this).val();
    var regex = /^(?:[0-9]|[1-9][0-9]|1[01][0-9]|110)$/;
    if (!regex.test(age)) {
      toastr.warning('Please enter age between 0 and 110!');
      document.getElementById("externel_reg_age").value = "";
      $('#externel_reg_dob').val('');
      return false;
    }else{
        var today = new Date();
        var dob  = new Date(today.getFullYear() - age, today.getMonth(), today.getDate());
        var dob = moment(dob);
        var dob = dob.format("MMM-DD-YYYY");
        $('#externel_reg_dob').val(dob);
    }
});

$(document).on("blur", "#externel_reg_dob", function(){
    var dob = $(this).val();
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    $('#externel_reg_age').val(age);
});

var token = $('#c_token').val();
var base_url = $('#base_url').val();

//---patient registration-------------------------
function nsRegisterExternelPatient(){
    var title = $('#externel_reg_title').val();
    var patient_name = $('#externel_reg_patient_name').val();
    var age = $('#externel_reg_age').val();
    var dob = $('#externel_reg_dob').val();
    var phone = $('#externel_reg_phone').val();
    var gender = $('#externel_reg_gender').val();
    var doctor_id = $('#externel_reg_doctor_id').val();
    var department = $('#externel_reg_department').val();
    var dobnotknown = $('#ext_dobnotknown').val();
    if(patient_name ==''){
        toastr.warning('Please enter patient name!');
        return false;
    }
    if(title ==''){
        toastr.warning('Please enter patient title!');
        return false;
    }
    if(age ==''){
        toastr.warning('Please enter patient age!');
        return false;
    }
    if(dob ==''){
        toastr.warning('Please enter patient date of birth!');
        return false;
    }
    if(phone ==''){
        toastr.warning('Please enter patient phone number!');
        return false;
    }
    if(gender ==''){
        toastr.warning('Please select patient gender!');
        return false;
    }
    if(doctor_id ==''){
        toastr.warning('Please select doctor!');
        return false;
    }
    if(department ==''){
        toastr.warning('Please select department!');
        return false;
    }

    var dataparams = {
        '_token':token,
        'title':title,
        'patient_name':patient_name,
        'age':age,
        'dob':dob,
        'phone':phone,
        'gender':gender,
        'doctor_id':doctor_id,
        'department':department,
        'dobnotknown':dobnotknown,
    };

    var url = base_url + "/nursing_new/ns_externel_patient_registration";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("#tab_externel_patient_registration").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $("#tab_externel_patient_registration").LoadingOverlay("hide");
            var obj = JSON.parse(data);

            if(obj.status == 1){
                toastr.success(obj.message);
                bootbox.alert({
                    message: 'Patient registered successfully!<br><table><tr><td>Patient:</td><td>'+patient_name+'</td><br></tr><tr><td>Uhid:</td><td>'+obj.uhid+'</td></tr><br></table>',
                    backdrop: true,
                });

                loadCasulityPatients();

                $('#reset_ext_patient_registration').trigger('click');
                $('#externel_reg_department').val(null).trigger('change');
                $('#externel_reg_doctor_id').val(null).trigger('change');

            }else{
                toastr.error(obj.message);
            }
        },
        complete: function () {
            $("#tab_externel_patient_registration").LoadingOverlay("hide");
            $('#reset_ext_patient_registration').trigger('click');
        }
    });
}

function selectExtDoctorsList() {
    var url = base_url + "/appointment_api/select_doctors_using_speciality";
    var speciality = $('#externel_reg_department').val();
    var param = {speciality: speciality};
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#externel_reg_doctor_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#externel_reg_doctor_id").select2("val", "");
            // $('#externel_reg_doctor_id').val('');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#externel_reg_doctor_id').LoadingOverlay("hide");
            $('#externel_reg_doctor_id').empty().trigger("change");
            var doctors_list = obj.response;
            doctors_list = $.parseJSON(doctors_list);
            $("#externel_reg_doctor_id").append('<option value="" selected>Select</option>');
            if(doctors_list) {
                $.each(doctors_list, function (key, value) {
                    $("#externel_reg_doctor_id").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
            }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function selectDoctorsList1() {
    var url = base_url + "/appointment_api/select_doctors_using_speciality";
    var speciality = $('#externel_ren_department').val();
    var param = {speciality: speciality};
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#externel_ren_doctor_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#externel_ren_doctor_id").select2("val", "");
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#externel_ren_doctor_id').LoadingOverlay("hide");
            $('#externel_ren_doctor_id').empty().trigger("change");
            var doctors_list = obj.response;
            doctors_list = $.parseJSON(doctors_list);
            $("#externel_ren_doctor_id").append('<option value="" selected>Select</option>');
            if(doctors_list) {
                $.each(doctors_list, function (key, value) {
                    $("#externel_ren_doctor_id").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
            }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

//------------------//-----------------------

$(document).on("change", "#new_reg_age", function (e) {
    $('#dobnotknown').val(0);
});
$(document).on("blur", "#new_reg_dob", function (e) {
    $('#dobnotknown').val(1);
});

$(document).on("click", "#reset_ext_patient_registration", function(){
    $('#externel_reg_department').val(null).trigger('change');
    $('#externel_reg_doctor_id').val(null).trigger('change');
});



