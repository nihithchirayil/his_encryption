$(document).ready(function() {
  
    $(".datepickers").datetimepicker({
        format: "MMM-DD-YYYY",
        minDate: new Date()
    });
    searchBookingData();
    $('[data-toggle="popover"]').popover({ html: true, sanitize: false,trigger: "hover"});
});

function saveMachineBook(e, booking_slot, machine_id, enrol_id) {
    var url = $("#base_url").val();
    var patient_id = $("#search_patient_name_id").val();
    var enrol_id = $("#enrol_id").val();
    var search_patient_name = $("#search_patient_name").val();
    var booking_date = $("#booking_date").val();
    if (search_patient_name == "") {
        toastr.warning("Please select patient");
        return false;
    }
    if (booking_date == "") {
        toastr.warning("Please select Date");
        return false;
    }
    var param = {
        patient_id: patient_id,
        booking_date: booking_date,
        booking_slot: booking_slot,
        machine_id: machine_id,
        enrol_id: enrol_id
    };
    $.ajax({
        type: "POST",
        url: url + "/dialysis/saveMachineBook",
        data: param,
        beforeSend: function() {
            $(
                "#img_cls" + booking_slot + "" + machine_id
            ).LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(html) {
            $("#img_cls" + booking_slot + "" + machine_id).LoadingOverlay(
                "hide"
            );
            if (html.success == 1) {
                toastr.success(html.message);
                if (window.location.href.indexOf("patient_id") > -1) {
                    // window.location.href = url + "/dialysis/dialysisMachineBooking";
                    window.location.reload();
                  }else{
                    searchBookingData();
                  }

            } else if (html.success == 5) {
                setTimeout(function() {
                    reschudele(booking_slot, machine_id);
                }, 300);
            } else {
                toastr.error(html.message);
            }
        },
        complete: function() {}
    });
}
function select_booked_patient(e, event, from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (from_type == "booked") {
        var ajax_div = "search_patient_name_ajax";
        var patient_name = $("#search_patient_name").val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if (patient_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data:
                    "patient_name=" +
                    patient_name +
                    "&search_patient_add=1&from_type=" +
                    from_type,
                beforeSend: function() {
                    $("#" + ajax_div)
                        .html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        )
                        .show();
                },
                success: function(html) {
                    $("#" + ajax_div)
                        .html(html)
                        .show();
                    $("#" + ajax_div)
                        .find("li")
                        .first()
                        .addClass("liHover");
                },
                complete: function() {
                    $(".theadscroll").perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead("reflow");
                }
            });
        }
    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillEnrolPatient(e, id, enrol_id, patient_name,uhid,serial_no) {
    $("#search_patient_name").val(patient_name.replace(/&amp;/g, "&"));
    $("#search_patient_name_id").val(id);
    $("#enrol_id").val(enrol_id);
    searchBookingData();
}
function searchBookingData() {
    var url = $("#base_url").val();
    var booking_date = $("#booking_date").val();
    var patient_id = $("#search_patient_name_id").val();
    if (booking_date == "") {
        toastr.warning("Please select Date");
        return false;
    }
    var param = { patient_id: patient_id, booking_date: booking_date };
    $.ajax({
        type: "POST",
        url: url + "/dialysis/searchMachineBook",
        data: param,
        beforeSend: function() {
            $("#machineBookDiv").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(html) {
            $("#machineBookDiv").LoadingOverlay("hide");
            $("#machineBookDiv").html(html);
            $('[data-toggle="popover"]').popover({
                html: true,
                sanitize: false,trigger: "hover"
            });
        },
        complete: function() {}
    });
}
function swap_booking(e, id, booking_slot, machine_id,incriment_id,patient_id){
    var url = $("#base_url").val();
    var patient_booking_id = $('#booking_id').val();
    console.log(id)
    console.log(patient_booking_id)
    var param = { booking_id: id ,patient_booking_id:patient_booking_id};
    $.ajax({
        type: "POST",
        url: url + "/dialysis/swap_booking",
        data: param,
        beforeSend: function() {
           
        },
        success: function(datas) {
            console.log(data)
            var data = JSON.parse(datas);
            if (data.status == '1') {
                toastr.success(data.message);
                window.location.reload()
            }else{
                toastr.success(data.message);
            }
        },
        complete: function() {}
    });
}

function delete_booking(e, id, booking_slot, machine_id,incriment_id,patient_id){
    var url = $("#base_url").val();
    var param = { id: id };
    $.ajax({
        type: "POST",
        url: url + "/dialysis/deleteMachineBook",
        data: param,
        beforeSend: function() {
            $(
                "#img_cls" + booking_slot + "" + machine_id
            ).LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(data) {
            $(
                "#img_cls" + booking_slot + "" + machine_id
            ).LoadingOverlay("hide");
            if (data == 1) {
                toastr.success("Successfully deleted");
                searchBookingData();
            } else {
                toastr.error("Error occured please try again");
            }
        },
        complete: function() {}
    });
}
function deleteMachineBook(e, id, booking_slot, machine_id,incriment_id,booking_patient_id) {
    var url = $("#base_url").val();
    var patient_id = $('#search_patient_name_id').val();
    var exist = false;
    console.log(patient_id)
    var book_incr_id = $('.ns_patient_container').attr('data-booking_incr');
   
        if(patient_id == booking_patient_id){
            exist = true;
        }
        console.log(booking_patient_id)

    if(exist)
    {
        bootbox.confirm({
            message: "Are you sure, you want to Cancel ?",
            buttons: {
                confirm: {
                    label: "Yes",
                    className: "btn-success",
                    default: "true"
                },
                cancel: {
                    label: "No",
                    className: "btn-warning"
                }
            },
            callback: function(result) {
                if (result) {
                    delete_booking(e, id, booking_slot, machine_id,incriment_id,patient_id);
                }
            }
        });
    }else{
        bootbox.dialog({
            message: "Do you want to cancel / swap this booking?",
            buttons: {
                cancel: {
                    label: "CancelBooking",
                    className: "btn-secondary",
                    callback: function(result) {
                        if (result) {
                            delete_booking(e, id, booking_slot, machine_id,incriment_id,booking_patient_id);
                        }
                    }
                  },
                  confirm: {
                    label: "SwapBooking",
                    className: "btn-success",
                    callback: function() {
                        swap_booking(e, id, booking_slot, machine_id,incriment_id,patient_id);
                    }
                  }
                }
        });
    }
 
  


}
function reschudele(booking_slot, machine_id) {
    bootbox.confirm({
        message: "Booking allready exist do you want to reschedule ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success",
                default: "true"
            },
            cancel: {
                label: "No",
                className: "btn-warning"
            }
        },
        callback: function(result) {
            if (result) {
                var url = $("#base_url").val();
                var patient_id = $("#search_patient_name_id").val();
                var enrol_id = $("#enrol_id").val();
                var search_patient_name = $("#search_patient_name").val();
                var booking_date = $("#booking_date").val();
                if (search_patient_name == "") {
                    toastr.warning("Please select patient");
                    return false;
                }
                if (booking_date == "") {
                    toastr.warning("Please select Date");
                    return false;
                }
                var param = {
                    patient_id: patient_id,
                    booking_date: booking_date,
                    booking_slot: booking_slot,
                    machine_id: machine_id,
                    enrol_id: enrol_id
                };
                $.ajax({
                    type: "POST",
                    url: url + "/dialysis/rescheduleMachineBook",
                    data: param,
                    beforeSend: function() {
                        $(
                            "#img_cls" + booking_slot + "" + machine_id
                        ).LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#009869"
                        });
                    },
                    success: function(html) {
                        $(
                            "#img_cls" + booking_slot + "" + machine_id
                        ).LoadingOverlay("hide");
                        if (html.success == 1) {
                            toastr.success(html.message);
                            if (window.location.href.indexOf("patient_id") > -1) {
                                // window.location.href = url + "/dialysis/dialysisMachineBooking";
                                window.location.reload();
                              }else{
                                searchBookingData();
                              }

                        } else {
                            toastr.error(html.message);
                        }
                    },
                    complete: function() {}
                });
            }
        }
    });
}
