$(document).ready(function() {

    $('.datepickers').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.closedatepickers').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A',
    });
    $('.time_picker').datetimepicker({
        format: 'hh:mm A',
    });
    tinymce.init({
        selector: 'textarea.tiny_text',
        height: 80,
        min_height: 80,
        menubar: false,
        toolbar: false,
        branding: false,
        forced_root_block : "",
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
      });
      tinymce.init({
        selector: 'textarea.tiny_text_tbl',
        height: 40,
        min_height: 40,
        menubar: false,
        toolbar: false,
        branding: false,
        forced_root_block : "",
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
      });

});
function select_booked_patient(e,event,from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if(from_type == 'booked'){
        var ajax_div = 'search_patient_name_ajax';
        var patient_name = $('#search_patient_name').val();
    }


    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (patient_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data: 'patient_name=' + patient_name+'&search_patient_add=1&from_type='+from_type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillEnrolPatient(e,id, enrol_id,patient_name,uhid,serial_no) {

    $("#search_patient_name").val(patient_name.replace(/&amp;/g, '&'));
    $("#search_patient_name_id").val(id);
    $("#enrol_id").val(enrol_id);
    $("#uhid").val(uhid);
    var lastFourUhid = uhid.substr(uhid.length - 4);
    var lastFourName = patient_name.substr(0,3);
    lastFourName = lastFourName.toUpperCase();
    if(serial_no == 'n'){
        genertedSerial_no = lastFourName+''+lastFourUhid+'-1';
    }else{
        var genSerailNos = serial_no.split('-');
        genSerailNo = parseFloat(genSerailNos['1'])+1;
        genertedSerial_no = lastFourName+''+lastFourUhid+'-'+genSerailNo;
    }
    $("#serail_no_label").html('<b>'+ genertedSerial_no +'</b>');
    $("#dialysis_serial_no").val(genertedSerial_no);
    $("#dsn").show();
}
function saveDialysisFormData() {
    var url = $("#base_url").val();
    selected_item_list = new Array();
    return_data_array = {};
if($("#search_patient_name").val() == ''){
    toastr.warning("Please select patient");
    return false;
}
    var patient_id = $("#search_patient_name_id").val();
    var dialysis_serail_no = $("#dialysis_serial_no").val();
    var date = $("#date").val();
    var enrol_id = $("#enrol_id").val();
    var dry_weight = $('#dry_weight').val();
    var machine = $('#machine').val();
    var av_fistula = $('#av_fistula').val();
    var femoral = $('#femoral').val();
    var no_of_use  = $('#no_of_use').val();
    var blood_tubing = $('#blood_tubing').val();
    var dializer = $('#dializer').val();
//===================== TABLE DATA =============================

$('#dialysis_tblform_body tr').each(function () {
    if ($.type($(this).find('input[name="time[]"]').val()) != "undefined") {
        time = $(this).find('input[name="time[]"]').val();
    }

    if ($.type($(this).find('input[name="bp_sys[]"]').val()) != "undefined") {
        bp_sys = $(this).find('input[name="bp_sys[]"]').val();
    }
    if ($.type($(this).find('input[name="bp_dia[]"]').val()) != "undefined") {
        bp_dia = $(this).find('input[name="bp_dia[]"]').val();
    }

    if ($.type($(this).find('input[name="pulse_tbl[]"]').val()) != "undefined") {
        pulse_tbl = $(this).find('input[name="pulse_tbl[]"]').val();
    }
    if ($.type($(this).find('input[name="temp[]"]').val()) != "undefined") {
        temp = $(this).find('input[name="temp[]"]').val();
    }
    if ($.type($(this).find('input[name="hep[]"]').val()) != "undefined") {
        hep = $(this).find('input[name="hep[]"]').val();
    }
    if ($.type($(this).find('input[name="bf[]"]').val()) != "undefined") {
        bf = $(this).find('input[name="bf[]"]').val();
    }
    if ($.type($(this).find('input[name="vp[]"]').val()) != "undefined") {
        vp = $(this).find('input[name="vp[]"]').val();
    }
    if ($.type($(this).find('input[name="np[]"]').val()) != "undefined") {
        np = $(this).find('input[name="np[]"]').val();
    }
    if ($.type($(this).find('input[name="complication[]"]').val()) != "undefined") {
        complication = $(this).find('input[name="complication[]"]').val();
    }

    if ($.type($(this).find('input[name="medication[]"]').val()) != "undefined") {
        medication = $(this).find('input[name="medication[]"]').val();
    }

    selected_item_list.push({
          'time': time
        , 'bp_sys': bp_sys
        , 'bp_dia': bp_dia
        , 'pulse_tbl': pulse_tbl
        , 'temp': temp
        , 'hep': hep
        , 'bf': bf
        , 'vp': vp
        , 'np': np
        , 'complication': complication
        , 'medication': medication
});
});
return_data_array['table_array'] = selected_item_list;
//==============================================================



    var condition_of_start = tinyMCE.get('condition_of_start').getContent(); //$("#condition_of_start").val();
    var pre_bp = $("#pre_bp").val();
    var pre_weight = $("#pre_weight").val();
    var post_weight = $("#post_weight").val();
    var pulse = $("#pulse").val();
    var resp = $("#resp").val();
    var temperature = $("#temperature").val();
    var inter_dialysis_weight_gain = $("#inter_dialysis_weight_gain").val();
    var weight_gain_from_dry_weight = $("#weight_gain_from_dry_weight").val();
    var bolus = $("#bolus").val();
    var infusion = $("#infusion").val();
    var duration = $("#duration").val();
    var blood_flow = $("#blood_flow").val();
    var dialysis_flow = $("#dialysis_flow").val();





    var post_dialysis_medication = tinyMCE.get('post_dialysis_medication').getContent();//$("#post_dialysis_medication").val();
    var doctors_order = tinyMCE.get('doctors_order').getContent(); //$("#doctors_order").val();
    var closing_time = $("#closing_time").val();
    var close_post_bp = $("#close_post_bp").val();
    var close_post_weight = $("#close_post_weight").val();
    var close_weight_loss = $("#close_weight_loss").val();
    var lab_investigation = tinyMCE.get('lab_investigation').getContent();//$("#lab_investigation").val();
    var close_medication = tinyMCE.get('close_medication').getContent(); //$("#close_medication").val();
    var started_by = $("#started_by").val();
    var assisted_by = $("#assisted_by").val();
    var started_by_id = $("#search_started_by").val();
    var assisted_by_id = $("#search_assisted_by").val();
    var dializer_fbv = $("#dializer_fbv").val();
    var blood_transfusion = $("input[name='blood_transfusion']:checked").val();

    var param = {patient_id:patient_id,enrol_id:enrol_id,date:date,
        dry_weight:dry_weight,
        machine:machine,
        av_fistula:av_fistula
        ,femoral:femoral,
        no_of_use:no_of_use
        ,blood_tubing:blood_tubing
        ,dializer:dializer
        ,condition_of_start:condition_of_start,
        pre_bp:pre_bp
        ,pre_weight:pre_weight
        ,post_weight:post_weight,pulse:pulse
        ,resp:resp
        ,temperature:temperature
        ,inter_dialysis_weight_gain:inter_dialysis_weight_gain
        ,weight_gain_from_dry_weight:weight_gain_from_dry_weight
        ,bolus:bolus
        ,infusion:infusion
        ,duration:duration
        ,blood_flow:blood_flow,dialysis_flow:dialysis_flow,
        post_dialysis_medication:post_dialysis_medication,
        doctors_order:doctors_order,
        closing_time:closing_time,
        close_post_bp:close_post_bp,
        close_post_weight:close_post_weight,
        close_weight_loss:close_weight_loss,
        lab_investigation:lab_investigation,
        close_medication:close_medication,
        started_by:started_by,
        assisted_by:assisted_by,
        started_by_id:started_by_id,
        assisted_by_id:assisted_by_id,
        blood_transfusion:blood_transfusion,
        dializer_fbv:dializer_fbv,
        selected_item_list:return_data_array,
        dialysis_serail_no:dialysis_serail_no}
            $.ajax({
                type: "POST",
                url: url + "/dialysis/saveDialysisFormData",
                data: param,
                beforeSend: function () {
                  $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                },
                success: function (data) {
                    if(data.success=='1'){
                        toastr.success(data.message);
                        setTimeout(function () {
                        window.location.href = url + "/dialysis/dialysisForm";
                        },300);
                    }else{
                        toastr.error(data.message);
                    }

                    $.LoadingOverlay("hide")
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });

}
function appendTableRaw(e){
    var now = new Date();
var dt = [
  now.getHours(),
  ':',
  now.getMinutes(),
].join('');
    $html = '<tr>'+
    '<td><input type="text" name="time[]" value="'+ dt +'"class="form-control time_picker" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="bp_sys[]" class="form-control" onkeyup="number_validation(this)" onblur="appendTableRaw(this)"></td>'+
    '<td><input type="text" name="bp_dia[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="pulse_tbl[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="temp[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="hep[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="bf[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="vp[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="np[]" class="form-control" onkeyup="number_validation(this)"></td>'+
    '<td><input type="text" name="complication[]" class="form-control"></td>'+
    '<td><input type="text" name="medication[]" class="form-control">'+
    '<td onclick="removeCurrentRaw(this)"><i class = "fa fa-trash" style="color:red;cursor:pointer"></i></td>'+
'</tr>';
$("#dialysis_tblform_body").append($html);
$('.time_picker').datetimepicker({
    format: 'hh:mm A',
});
  tinymce.init({
    selector: 'textarea.tiny_text_tbl',
    height: 40,
    min_height: 40,
    menubar: false,
    toolbar: false,
    branding: false,
    forced_root_block : "",
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
  });
setTimeout(function(){

    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table){
            return $table.closest('.theadscroll');
        }
    });

},3000);
}
function removeCurrentRaw(e){
    $(e).parent("tr").remove();
}
function select_user(e,event,from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if(from_type == 'started_by'){
        var ajax_div = 'search_started_by_ajax';
        var user_name = $('#started_by').val();
    }else{
        var ajax_div = 'search_assisted_by_ajax';
        var user_name = $('#assisted_by').val();
    }


    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (user_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data: 'user_name=' + user_name+'&user_search=1&from_type='+from_type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function filluserData(e,id, name,from_type) {

     if(from_type == 'started_by'){
    $('#started_by').val(name);
    $('#search_started_by').val(id);
    }else{
        $('#assisted_by').val(name);
        $('#search_assisted_by').val(id);
    }
}
function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function calculateWeightLoss(e){
    var postWeight = $(e).val();
    var dryWeight = $("#dry_weight").val();
    if(dryWeight != '' && postWeight != '' && ($.isNumeric($("#dry_weight").val()) )){
        var calulatedWeight = parseFloat(dryWeight) - parseFloat(postWeight);
        $("#close_weight_loss").val(calulatedWeight);
    }
}
