$(document).ready(function() {

    $('.datepickers').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});


function fetchDialysisLogData(){
    var url = $("#base_url").val();
    var patient_id = $("#search_patient_name_id").val();
    var search_patient_name = $("#search_patient_name").val();
    var param = {search_patient_id:patient_id,search_patient_name:search_patient_name};
    $.ajax({
        type: "POST",
        url: url + "/dialysis/fetchDialysisLogData",
        data: param,
        beforeSend: function () {
          $("#dialysis_log_div").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (html) {
            $("#dialysis_log_div").LoadingOverlay("hide");
            $("#dialysis_log_div").html(html);
            $('.datepickers').datetimepicker({
                format: 'MMM-DD-YYYY',
            });
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}
function select_booked_patient(e,event,from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if(from_type == 'booked'){
        var ajax_div = 'search_patient_name_ajax';
        var patient_name = $('#search_patient_name').val();
    }


    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (patient_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data: 'patient_name=' + patient_name+'&search_patient_add=1&from_type='+from_type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillEnrolPatient(e,id, enrol_id,patient_name,uhid,serial_no) {

      $("#search_patient_name").val(patient_name.replace(/&amp;/g, '&'));
      $("#search_patient_name_id").val(id);
      $("#enrol_id").val(enrol_id);
      fetchDialysisLogData();
}
function updateNextAppointment(e,id){
    var next_app_dt = $(e).val();
    var url = $("#base_url").val();
     $.ajax({
                type: "GET",
                url: url + "/dialysis/updateNextAppointment",
                data: 'next_app_dt=' + next_app_dt+'&update_id='+id,
                beforeSend: function () {
                    $("#dialysis_log_div").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                },
                success: function (html) {
                    $("#dialysis_log_div").LoadingOverlay("hide");
                    if(html == 1){
                        toastr.success("Updated successfully");
                    }else{
                        toastr.error("Eoor occured please try again");
                    }
                },
                complete: function () {

                }
            });
}
