$(document).ready(function() {

    $('.datepickers').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.datepickers').val(moment().format('MMM-DD-YYYY'));
    fetchDialysisListData();
});

function fetchDialysisListData(){
    var url = $("#base_url").val();
    var patient_id = $("#search_patient_name_id").val();
    var search_patient_name = $("#search_patient_name").val();
    var search_date = $("#search_date").val();

    var param = {search_patient_id:patient_id,search_patient_name:search_patient_name,search_date:search_date};
    $.ajax({
        type: "POST",
        url: url + "/dialysis/dialysisEnrolPatientListData",
        data: param,
        beforeSend: function () {
          $("#fetch_dialysis_data").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (html) {
            $("#fetch_dialysis_data").LoadingOverlay("hide");
            $("#fetch_dialysis_data").html(html);
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}



function select_booked_patient(e,event,from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if(from_type == 'booked'){
        var ajax_div = 'search_patient_name_ajax';
        var patient_name = $('#search_patient_name').val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if (patient_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data: 'patient_name=' + encodeURIComponent(patient_name)+'&search_patient_add=1&from_type='+from_type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }
    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillEnrolPatient(e,id, enrol_id,patient_name,uhid,serial_no) {

      $("#search_patient_name").val(patient_name.replace(/&amp;/g, '&'));
      $("#search_patient_name_id").val(id);
      $("#enrol_id").val(enrol_id);
}
function printDialysis(id,patient_id){
    var param ={search_patient_id:patient_id};
    var url = $("#base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/dialysis/fetchDialysisLogData",
        data: param,
        beforeSend: function() {
            $("#print_dialysis_btn_" + id).attr("disabled", true);
            $("#print_dialysis_btn_spin_" + id).removeClass("fa fa-print");
            $("#print_dialysis_btn_spin_" + id).addClass(
                "fa fa-spinner fa-spin"
            );
        },
        success: function(data) {
            if (data) {
                $("#ResultsViewArea").html(data);
                $('#print_config_modal').modal('show');
            }else{
                toastr.error("ss");
            }
        },
        complete: function() {
            $("#print_dialysis_btn_" + id).attr("disabled", false);
            $("#print_dialysis_btn_spin_" + id).removeClass(
                "fa fa-spinner fa-spin"
            );
            $("#print_dialysis_btn_spin_" + id).addClass(
                "fa fa-print"
            );
        }
    });
}
function print_generate_log_Data(printData) {

    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }
    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }
    mywindow.document.write('<style>.table.no-border, .table.no-border td, .table.no-border th { border: 0; } .table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } .table_sm td span { line-height: 18px !important; } .table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } .table_head.tth { font-size: 12px !important; padding: 1px !important; } .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } .table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, .table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, .table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } .headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} th { background-color:#d9dfe0 !important;}.common_td_rules{ text-align: left !important; overflow: hidden !important;}.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; }</style>');
    mywindow.document.write(showw + '<script>setTimeout(function(){window.print();window.close();},1000)</script>');
    return true;
}
