$(document).ready(function() {

    $('.datepickers').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    fetchDialysisListData();
});
function select_patient_data(e,event,from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if(from_type == 'search'){
        var ajax_div = 'search_patient_name_ajax';
        var patient_name = $('#search_patient_name').val();
    }else{
        var ajax_div = 'patient_name_ajax';
        var patient_name = $('#patient_name').val();
    }
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if (patient_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data: 'patient_name=' + encodeURIComponent(patient_name)+'&search_patient_add=1&from_type='+from_type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function filldaialysisData(e,id, patient_name, uhid,age,gender,address,blood_group) {
      $("#patient_name").val(patient_name.replace(/&amp;/g, '&'));
      $("#patient_name_id").val(id);
      $("#uhid").html(uhid);
      $("#patient_age").html(age);
      $("#patient_sex").html(gender);
      $("#patient_address").val(address);
      $("#blood_group").val(blood_group);
}
function filldaialysisDataSearch(e,id, patient_name, uhid,age,gender,address,blood_group) {

    $("#search_patient_name").val(patient_name.replace(/&amp;/g, '&'));
    $("#search_patient_name_id").val(id);
}
function saveDialysisData() {
    var url = $("#base_url").val();
if($("#patient_name").val() == ''){
    toastr.warning("Please select patient");
    return false;
}
    var patient_id = $("#patient_name_id").val();
    var intimation_date = $("#intimation_date").val();
    var vascular_access = $("#vascular_access").val();
    var needle_types = $("#needle_types").val();
    var duration_of_hd = $("#duration_of_hd").val();
    var bolus = $("#bolus").val();
    var infusion = $("#infusion").val();
    var dialysis_types = $("#dialysis_types").val();
    var blood_flow_rate = $("#blood_flow_rate").val();
    var hiv = $("#hiv").val();
    var hbag = $("#hbag").val();
    var hcv = $("#hcv").val();
    var dialysis_id = $("#dialysis_id").val();
    var dializer_types = $("#dializer_types").val();
    var blood_group = $("#blood_group").val();
    var patient_diagnosis = $("#patient_diagnosis").val();
var param = {patient_id:patient_id,blood_group:blood_group,patient_diagnosis:patient_diagnosis,intimation_date:intimation_date,vascular_access:vascular_access,
    needle_types:needle_types,duration_of_hd:duration_of_hd,bolus:bolus,infusion:infusion,
    dialysis_types:dialysis_types,blood_flow_rate:blood_flow_rate,hiv:hiv,hbag:hbag,hcv:hcv,dializer_types:dializer_types,dialysis_id:dialysis_id}
            $.ajax({
                type: "POST",
                url: url + "/dialysis/saveDialysisData",
                data: param,
                beforeSend: function () {
                  $("#savedatadiv").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                },
                success: function (data) {
                    if(data.success=='1'){
                        toastr.success(data.message);
                        setTimeout(function () {
                            window.location.reload();
                        },300);
                    }else{
                        toastr.error(data.message);
                    }

                    $("#savedatadiv").LoadingOverlay("hide")
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });

}
function fetchDialysisListData(){
    var url = $("#base_url").val();
    var patient_id = $("#search_patient_name_id").val();
    var search_patient_name = $("#search_patient_name").val();
    var search_date = $("#search_date").val();

    var param = {search_patient_id:patient_id,search_patient_name:search_patient_name,search_date:search_date};
    $.ajax({
        type: "POST",
        url: url + "/dialysis/fetchDialysisListData",
        data: param,
        beforeSend: function () {
          $("#fetch_dialysis_data").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (html) {
            $("#fetch_dialysis_data").LoadingOverlay("hide");
            $("#fetch_dialysis_data").html(html);
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}
function editDialysis(id,patient_name,pid,uhid,age,gender,address,blood_group,intimation_date,vascular_access,
    needle_type,duration_of_hd,bolus,infusion,dialysis_types,blood_flow_rate,hiv,hbag,hcv,dializer_types,blood_group,diagnosis){
        $("#dialysis_id").val(id);
        $("#patient_name").val(patient_name);
        $("#patient_name_id").val(pid);
        $("#uhid").html(uhid);
        $("#patient_age").html(age);
        $("#patient_sex").html(gender);
        $("#patient_address").html(address);
        $("#blood_grpup").html(blood_group);
        $("#intimation_date").val(intimation_date);
        $("#vascular_access").val(vascular_access);
        $("#needle_types").val(needle_type);
        $("#duration_of_hd").val(duration_of_hd);
        $("#bolus").val(bolus);
        $("#infusion").val(infusion);
        $("#dialysis_types").val(dialysis_types);
        $("#blood_flow_rate").val(blood_flow_rate);
        $("#hiv").val(hiv);
        $("#hbag").val(hbag);
        $("#hcv").val(hcv);
        $("#dializer_types").val(dializer_types);
        $("#patient_diagnosis").val(diagnosis);
        $("#blood_group").val(blood_group);
        $("#save_btn_id").html('<i class="fa fa-save" id="add_new_ledger_spin"></i> Update')

}
function deleteDialysis(id){
    var url = $("#base_url").val();
    var param = {id:id};
    bootbox.confirm({
        message: "Are you sure, you want to Delete ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success",
                default: "true"
            },
            cancel: {
                label: "No",
                className: "btn-warning"
            }
        },
        callback: function(result) {
            if (result) {

                $.ajax({
                    type: "POST",
                    url: url + "/dialysis/deleteDialysisListData",
                    data: param,
                    beforeSend: function() {
                        $("#delete_dialysis_btn_" + id).attr("disabled", true);
                        $("#delete_dialysis_btn_spin_" + id).removeClass("fa fa-trash");
                        $("#delete_dialysis_btn_spin_" + id).addClass(
                            "fa fa-spinner fa-spin"
                        );
                    },
                    success: function(data) {
                        if (data == 1) {
                            toastr.success("Successfully deleted");
                            $("#tr_"+id).remove();
                        }else{
                            toastr.error("Error occured please try again");
                        }
                    },
                    complete: function() {
                        $("#delete_dialysis_btn_" + id).attr("disabled", false);
                        $("#delete_dialysis_btn_spin_" + id).removeClass(
                            "fa fa-spinner fa-spin"
                        );
                        $("#delete_dialysis_btn_spin_" + id).addClass(
                            "fa fa-trash"
                        );
                    }
                });
            }
        }
    });
}

function select_booked_patient(e,event,from_type) {
    var url = $("#base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if(from_type == 'booked'){
        var ajax_div = 'search_patient_name_ajax';
        var patient_name = $('#search_patient_name').val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if (patient_name == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
        } else {
            $.ajax({
                type: "GET",
                url: url + "/dialysis/searchInput",
                data: 'patient_name=' + encodeURIComponent(patient_name)+'&search_patient_add=1&from_type='+from_type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillEnrolPatient(e,id, enrol_id,patient_name,uhid,serial_no) {

      $("#search_patient_name").val(patient_name.replace(/&amp;/g, '&'));
      $("#search_patient_name_id").val(id);
      $("#enrol_id").val(enrol_id);
}

