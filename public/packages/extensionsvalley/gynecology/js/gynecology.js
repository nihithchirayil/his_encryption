$(document).ready(function (){
    $(".datepicker").datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    // $("#next_review_date").val('');
    $(".first_tab").champ();

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

    $(document).on("click", ".tab_list li", function (event) {
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    var show_shiftwise_patient = $("#show_shiftwise_patient").val() ? $("#show_shiftwise_patient").val() : 0;
    if(show_shiftwise_patient == 1){
        getShiftPatient();
    }
    setTimeout(function(){
        getOpPatients(1);
    },2000);
    fetchFrequencyList();
    fetchDirectionList();

    $(document).on("keyup","#gynec_weight", function(){
        var weight = $('#gynec_weight').val();
        var height = $('#gynec_height').val();
        var bmi = '';
        if(weight != '' && height != ''){
            bmi = calculateBmi(weight, height);
            $('#gynec_bmi').val(bmi);
        }else{
            $('#gynec_bmi').val('');
        }
    });

    $(document).on("keyup","#gynec_height", function(){
        var weight = $('#gynec_weight').val();
        var height = $('#gynec_height').val();
        var bmi = '';
        if(weight != '' && height != ''){
            bmi = calculateBmi(weight, height);
            $('#gynec_bmi').val(bmi);
        }else{
            $('#gynec_bmi').val('');
        }
    });

    function calculateBmi(weight, height){
        console.log(weight+'#'+height);
        height = height/100;
        height = height*height;
        bmi = weight/height;
        bmi = bmi.toFixed(2);
        return bmi;
    }

    $('.number_input').on('keydown keyup', function(e) {
        var regExp = /[0-9\.\,]/;
        var value = String.fromCharCode(e.which) || e.key;
        // Only numbers, dots and commas
        // if (!regExp.test(value)
        //   && e.which != 190 // .
        //   && e.which != 8   // backspace
        //   && e.which != 46  // delete
        //   && (e.which < 37  || e.which > 40)) {
        //       e.preventDefault();
        //       return false;
        // }

        // (evt.which < 48 || evt.which > 57) && (evt.which < 105 || evt.which > 96) && (evt.which != 190)

        if (!regExp.test(value)
          && e.which != 190 // .
          && e.which != 8   // backspace
          && e.which != 46  // delete
          && e.which != 96
          && e.which != 97
          && e.which != 98
          && e.which != 99
          && e.which != 100
          && e.which != 101
          && e.which != 102
          && e.which != 103
          && e.which != 104
          && e.which != 105
          && (e.which < 37  || e.which > 40))

          {
              e.preventDefault();
              return false;
        }
    });

    getDoctorPrescription();
    getDoctorBookmarkedNotes();

});
$(document).on("keydown", "textarea,input[type='text']", function () {
    addBeforeUnloadCallbak();
});

var investigation_future_date = 0;

$(document).on("click", "input[type='checkbox'],input[type='radio']", function () {
    addBeforeUnloadCallbak();
});


function fetchGynecHistory(patient_id){
    var url = $('#base_url').val() + "/gynecology_lite/fetchClinicalData";
    var dataparams = {'patient_id':patient_id};
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#allergic_history_data').val('');
            $('#obstretic_history_data').val('');
            $('#family_history_data').val('');
            $('#medical_and_surgical_history_data').val('');
            $('#nurtitional_and_screening_data').val('');
        },
        success: function (data) {
            if(data!=0){
                var obj = JSON.parse(data);
                $('#allergic_history_data').val(obj.allergic_history);
                $('#obstretic_history_data').val(obj.obstretic_history);
                $('#family_history_data').val(obj.family_history);
                $('#nurtitional_and_screening_data').val(obj.nurtitional_and_screening);
                $('#medical_and_surgical_history_data').val(obj.medical_and_surgical_history);

            }else{

                $('#allergic_history_data').val('');
                $('#obstretic_history_data').val('');
                $('#family_history_data').val('');
                $('#nurtitional_and_screening_data').val('');
                $('#medical_and_surgical_history_data').val('');

            }
        },
        error: function () {
            Command: toastr["warning"]('Error.!');
        },
        complete: function () {

        }
    });
}

function addBeforeUnloadCallbak() {
    var patient_id = $("#patient_id").val();
    if (patient_id && window.onbeforeunload == null) {
        window.onbeforeunload = function () {
            return "You have unsaved changes. Are you sure want to continue ?";
        };
    }

}
$(document).on("click", '.toggleIpOpListBtn', function(){
    if($(this).hasClass('leftArrow')){
        $(this).removeClass('leftArrow');
        $(this).find('i').removeClass('fa-arrow-left').addClass('fa-arrow-right');
        $(".patient_details_inner_div").show();
        $(".ip_op_list_div").hide("slide", { direction: "left" }, 500);
        $('.patient_data_list_div').removeClass('col-md-10');
        $('.patient_data_list_div').addClass('col-md-12');
    } else {
        $(this).addClass('leftArrow');
        $(this).find('i').removeClass('fa-arrow-right').addClass('fa-arrow-left');
        $(".patient_details_inner_div").hide();
        $('.patient_data_list_div').removeClass('col-md-12');
        $('.patient_data_list_div').addClass('col-md-10');
        $(".ip_op_list_div").show("slide", { direction: "left" }, 500);
    }
    resetBootstrapTable();
})

function showHidePatientFilters(from_type) {
    if (parseInt(from_type) == 1) {
        $("#showHidePatientFiltersAjaxDiv").slideDown();
        $('#showHidePatientFiltersAjaxDiv').show();
    } else if (parseInt(from_type) == 2) {
        $("#showHidePatientFiltersAjaxDiv").slideUp();
        $('#showHidePatientFiltersAjaxDiv').hide();
    }
}
function getShiftPatient()
{
    var base_url = $("#base_url").val();
    var shift_search = $("#shift_search").val();
    var url = base_url + "/gynecology_lite/getShiftPatient";
    var params = { shift_search: shift_search };

    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {

        },
        success: function (data) {

            if(data.shift_search != 0){
                    shift = data.shiftpatients[0].id;
                    console.log(shift)
                    $("#shift_search").val(shift);
            }else{
                $("#shift_search").val('All');
            }

            setTimeout(function(){
                getOpPatients();
                getOpPatients(1);
            },2000);
        },
        complete: function () {
        },

    });
}


function getOpPatients(seen_type) {
    // seen_type = 3;
    var base_url = $("#base_url").val();
    var search_date = $("#op_patient_search_date").val();
    var group_doctor = $("#group_doctor").val();
    var shift_search = $("#shift_search").val();
    console.log(shift_search);
    var url = base_url + "/gynecology_lite/showOPEmrPatient";
    var params = { search_date: search_date, group_doctor: group_doctor, seen_type:seen_type, shift_search: shift_search };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            showHidePatientFilters(2);
            $("#tab1").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#patient_total_cnt').html("Total : " + obj.resultcount);
            if(seen_type == 1){
                $("#tab1").html(obj.data);
            } else {
                $("#tab2").html(obj.data);
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

        },
        complete: function () {
            $("#tab1").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

$(document).on("click", ".ip_op_list_item", function(){
    var patient_id = $(this).attr('data-patient-id');
    var booking_id = $(this).attr('data-booking-id');
    if ($('#patient_id').val() && window.onbeforeunload != null) {
        var confim_change = window.confirm("You have unsaved changes. Are you sure want to continue ?");
        if (!confim_change) {
            return;
        }
    }

    $('#patient_id').val(patient_id);
    $('#booking_id').val(booking_id);
    $(".ip_op_list_item").removeClass('active_row');
    $(".list_item_age").hide();
    $(".list_item_tokenunseen_div").removeClass('active_item');
    $(this).addClass('active_row');
    $(this).find('.list_item_age').show();
    $(this).find('.list_item_tokenunseen_div').addClass('active_item');
    $('#usg_res_entry').trigger("reset");
    $('#private_notes_textarea').val();
    fetchPatientDetails(patient_id);
    fetchGynecologyDetails(patient_id);
    fetchGynecologycurrentVisitDetails(booking_id);
    fetchGynecHistory(patient_id);
    fetchPregnancyDetails(patient_id);
    fetchUsgResultsEntry(patient_id);
    fetchUsgCurrentVisitResultsEntry(booking_id);

    $('#dynamic_template_data').html('');
});


$('.tabs-nav1 li:first-child').addClass('active');
$('.tab-content1').hide();
$('.tab-content1:first').show();

// Click function
$('#tabs-nav li').click(function () {
    $('#tabs-nav li').removeClass('active');
    $(this).addClass('active');
    $('.tab-content1').hide();

    var activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn();
    return false;
});






function fetchPatientDetails(patient_id) {
    var patient_id;
    if (!patient_id) {
        patient_id = $('#patient_id').val();
    }

    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/fetchPatientDetails";
    var params = {};
    params.patient_id = patient_id;
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            // $(".todays_visit_details_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            // $(".patient_combined_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            // $(".investigation_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            // $(".prescription_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });

            // $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".clinical_data_container").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            clearvitalsArea();
        },
        success: function (data) {
            var patient_details = data.patient_details;
            var patient_basic_details = patient_details.patient_details;
            var patient_allergies = patient_details.patient_allergies;
            var patient_other_allergy = patient_details.patient_other_allergy;
            var personal_notes = patient_details.patient_private_notes;
            showPatientDetails(patient_basic_details);
            $(".patient_details_patient_place").html(patient_details.area);
            var formdata = patient_details.formdata;
            showPatientFormData(formdata);
            showPatientAllergies(patient_allergies, patient_other_allergy);
            $(".private_notes_textarea").text(personal_notes);
            var patient_vitals = patient_details.patient_vitals;
            // showLatestPatientVitals(patient_vitals[0]);
            var patient_medications = patient_details.patient_medication;
            var patient_medications_old = patient_details.patient_medications_old;
            showPatientMedicationHistory(patient_medications, patient_medications_old);
            var patient_investigations = patient_details.patient_investigation;
            // showPatientInvestigationHistory(patient_investigations);
            $(".investigation_history_div").html(patient_details.investigation_details);
            var patient_combined_history = patient_details.patient_combined_history;
            showPatientCombinedHistory(patient_combined_history);
            var patient_todays_visit_details = patient_details.patient_todays_visit_details;
            $(".todays_visit_details_div").html(patient_todays_visit_details);
            $(".chief_complaint").html('');
            $(".investigation_item_search_textbox").val('');
            $(".assessmentFavButton").show();

            $('#gynec_vital_details').html(patient_vitals);

        },
        complete: function () {
            // $(".todays_visit_details_div").LoadingOverlay("hide");
            // $(".patient_combined_history_div").LoadingOverlay("hide");
            // $(".investigation_history_div").LoadingOverlay("hide");
            // $(".prescription_history_div").LoadingOverlay("hide");
            // $("body").LoadingOverlay("hide");
            $(".clinical_data_container").LoadingOverlay("hide");
            $('#add_new_vital_batch').val(0);
            setTimeout(function () {
                fetchPatientVisit()},500);
            resetPrescriptionList();
            resetSelectedInvestigations();
            fetchPatientInvResultEntry();
            clearData();
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function clearData() {
    $('#prescription_head_id').val('');
    $('#investigation_head_id').val('');
    $(".saveButtonDiv").show();
    $(".updatePrescriptionBtnDiv").hide();
    $(".updateInvestigationBtnDiv").hide();
}
function showPatientCombinedHistory(patient_combined_history) {
    $(".patient_combined_history_div").html(patient_combined_history);
}

function showPatientDetails(patient_basic_details) {
    $("#visit_id").val(patient_basic_details.visit_id ? patient_basic_details.visit_id : 0);
    $("#encounter_id").val(patient_basic_details.encounter_id);
    $("#visit_type").val(patient_basic_details.visit_status ? patient_basic_details.visit_status : '');
    $(".patient_details_patient_name").html(patient_basic_details.patient_name);
    $(".patient_details_patient_uhid").html(patient_basic_details.uhid);
    $(".patient_details_patient_age_gender").html(patient_basic_details.patient_age + '/' + patient_basic_details.patient_gender);
    $("#next_review_date").val(patient_basic_details.next_review_date ? patient_basic_details.next_review_date : '');
}

function showPatientFormData(formdata) {
    $(".notes_chief_complaint_container").html(formdata.response);
}

function showPatientAllergies(patient_allergies, patient_other_allergy) {
    $(".allergy_list").html('');
    if(patient_allergies !== undefined){
    if (patient_allergies.length > 0 ) {
        $.each(patient_allergies, function (key, val) {
            var allergy = '';
            if (val.allergy_type == 'B') {
                // allergy = '<span class="blue"> Brand : </span> ' + val.allergy_name + '<br>';
                allergy = val.allergy_name + '<br>';
            } else if (val.allergy_type == 'G') {
                // allergy = '<span class="blue"> Generic : </span> ' + val.allergy_name + '<br>';
                allergy = val.allergy_name + '<br>';
            }
            $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + val.allergy_id + '">' + allergy + '</span>');
        })

    }
}
if(patient_other_allergy !== undefined){
    if (patient_other_allergy.length > 0) {
        $.each(patient_other_allergy, function (key, val) {
            if(val.allergy){
                // var allergy = '<span class="blue"> Other : </span> ' + val.allergy + '<br>';
                var allergy = val.allergy + '<br>';
                $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + val.allergy_id + '">' + allergy + '</span>');
            }
        })
    }
}
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
}

function showPatientMedicationHistory(patient_medications, patient_medications_old) {
    $(".prescription_history_div").empty();

    $.each(patient_medications, function (key, val) {

        var presc_date = moment(val.created_at).format('DD MMM YYYY hh:mm A');
        var doctor_name = capitalizeFirstLetter(val.doctor_name);

        var bill_converted_class = '';
        if (val.billconverted_status == 1) {
            bill_converted_class = ' hidden ';
        }
        var prescription_head_id = val.head_id;
        var advice_given = val.advice_given ? val.advice_given :'';
        var next_review_remarks = val.next_review_remarks ? val.next_review_remarks :'';
        var presc_div = '';
        presc_div = '<div class="prescription_history_item" data-head-id="' + prescription_head_id + '" data-bill-converted-head-status="' + val.billconverted_status + '" data-advice_given="' + advice_given + '" data-next_review_remarks="'+next_review_remarks+'"><div class="presc_history_head" style="display:flex;"><div class="presc_history_head_left"><div><i class="fa fa-calendar"></i> ' + presc_date + '</div><div class="overflow_text"><i class="fa fa-user-md"></i> ' + doctor_name + '</div> </div><div class="presc_history_head_right"><i class="fa fa-print printBtn printPrescriptionButton"></i><i class="fa fa-edit editBtn ' + bill_converted_class + ' editPrescriptionButton"></i><i class="fa fa-copy copyBtn copyPrescriptionButton"></i><i class="fa fa-trash deleteBtn ' + bill_converted_class + ' deletePrescriptionButton"></i></div></div> <div class="prescription_list">';

        $.each(val.medicine_list, function (key1, val1) {

            presc_div = presc_div + '<span class="checkbox_serial"><input type="checkbox" class="prescription_history_item_checkbox" data-detail-id="' + val1.detail_id + '" data-frequency-name="' + val1.frequency + '" data-frequency-value="' + val1.frequency_value + '" data-frequency-id="' + val1.frequency_id + '" data-item-quantity="' + val1.quantity + '" data-item-duration="' + val1.duration + '" data-item-direction="' + val1.notes + '" data-item-desc="' + val1.item_desc + '" data-item-code="' + val1.medicine_code + '" data-bill-converted-status="' + val1.bill_converted_status + '" /></span> <span class="presc_history_item_desc">' + val1.item_desc + '</span><span class="presc_history_frequency"> | ' + val1.frequency + ' | </span> <span class="presc_history_duration"> x ' + val1.duration + ' days</span> <div class="clearfix border_dashed_bottom"></div> ';

        });

        presc_div = presc_div + '</div> </div> ';

        $(".prescription_history_div").append(presc_div);

    });

    $(".prescription_history_div").append(patient_medications_old);

}

function capitalizeFirstLetter(string) {
    string = string.toLowerCase();
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// function fetchPatientVisit() {
//     var visit_id = $("#visit_id").val();
//     var patient_id = $('#patient_id').val();
//     var base_url = $("#base_url").val();
//     var url = base_url + "/emr_lite/fetchPatientVitals";
//     var params = { visit_id: visit_id, patient_id: patient_id };
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: params,
//         beforeSend: function () {
//             $(".vital_list").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
//         },
//         success: function (data) {
//             showLatestPatientVitals(data);
//             $('#vital_batch_no').val(data.batch_no ? data.batch_no : '');
//             $('#encounter_id').val(data.encounter_id ? data.encounter_id : 0);
//         },
//         complete: function () {
//             $(".vital_list").LoadingOverlay("hide");
//         },
//         error: function () {
//             toastr.error("Error please check your internet connection");
//         },
//     });
// }

$(document).on("click", ".add_new_batch_vitals", function () {
    $(".vital_form_container").find('input[type="text"]').val('');
    $('.gynec_height_range').html('');
    $('.gynec_weight_range').html('');
    $('.gynec_bmi_range').html('');
    $('.gynec_waist_circ_range').html('');
    $('.gynec_bp_sys_range').html('');
    $('.gynec_bp_dia_range').html('');
    $('.gynec_pallor_range').html('');
    $('.gynec_edema_range').html('');
    $('.gynec_cvs_range').html('');
    $('.gynec_rs_range').html('');
    $('#add_new_vital_batch').val(1);
});

function showLatestPatientVitals(patient_vitals) {
    $('#vital_batch_no').val('');
    if (patient_vitals) {
        var batch_no = patient_vitals.batch_no ? patient_vitals.batch_no : '';
        $('#vital_batch_no').val(batch_no);
        $('.add_new_batch_vitals').addClass('hidden');
        if (batch_no) { 
            $('.add_new_batch_vitals').removeClass('hidden');
        }
        
        var vital_details = patient_vitals.vital_details;
        $(".vital_list_table_body").empty();
        var bp_value = '';
        if(vital_details.length>0){

            $.each(vital_details, function (key, val) {
                var vital_icon = val.icon_class ? val.icon_class : '';
                var vital_name = val.display_name ? val.display_name : val.vital_name;
                var vital_value = val.vital_value;
                var string_class = '';
                if ((vital_value != null) && (vital_value > parseFloat(val.max_value))) {
                    string_class = " <i title='Min:"+val.min_value+" - Max:"+val.max_value+"' class='fa fa-arrow-up blink_me'></i> ";
                }
                if ((vital_value != null) && (vital_value < parseFloat(val.min_value))) {
                    string_class = " <i title='Min:"+val.min_value+" - Max:"+val.max_value+"' class='fa fa-arrow-down blink_me'></i> ";
                }

                if(val.vital_master_id == 4){
                    if(vital_value !=''){

                        $('#gynec_height').val(vital_value);
                        $('.gynec_height_range').html(string_class);
                    }else{
                        $('#gynec_height').val('');
                        $('.gynec_height_range').html('');
                    }
                }
                if(val.vital_master_id == 2){
                    if(vital_value !=''){
                        $('#gynec_weight').val(vital_value);
                        $('.gynec_weight_range').html(string_class);
                    }else{
                        $('#gynec_weight').val('');
                        $('.gynec_weight_range').html('');
                    }
                }
                if(val.vital_master_id == 14){
                    if(vital_value !=''){
                        $('#gynec_bmi').val(vital_value);
                        $('.gynec_bmi_range').html(string_class);
                    }else{
                        $('#gynec_bmi').val('');
                        $('.gynec_bmi_range').html('');
                    }
                }
                if(val.vital_master_id == 15){
                    if(vital_value !=''){
                        $('#gynec_waist_circ').val(vital_value);
                        $('.gynec_waist_circ_range').html(string_class);
                    }else{
                        $('#gynec_waist_circ').val('');
                        $('.gynec_waist_circ_range').html('');
                    }
                }
                if(val.vital_master_id == 16){
                    if(vital_value !=''){
                        $('#gynec_pr').val(vital_value);
                        $('.gynec_pr_range').html(string_class);
                    }else{
                        $('#gynec_pr').val('');
                        $('.gynec_pr_range').html('');
                    }
                }
                if(val.vital_master_id == 5){
                    if(vital_value !=''){
                        $('#gynec_bp_sys').val(vital_value);
                        $('.gynec_bp_sys_range').html(string_class);
                    }else{
                        $('#gynec_bp_sys').val('');
                        $('.gynec_bp_sys_range').html('');
                    }
                }
                if(val.vital_master_id == 6){
                    if(vital_value !=''){
                        $('#gynec_bp_dia').val(vital_value);
                        $('.gynec_bp_dia_range').html(string_class);
                    }else{
                        $('#gynec_bp_dia').val('');
                        $('.gynec_bp_dia_range').html('');
                    }
                }
                if(val.vital_master_id == 17){
                    if(vital_value !=''){
                        $('#gynec_pallor').val(vital_value);
                        $('.gynec_pallor_range').html(string_class);
                    }else{
                        $('#gynec_pallor').val('');
                        $('.gynec_pallor_range').html('');
                    }
                }
                if(val.vital_master_id == 18){
                    if(vital_value !=''){
                        $('#gynec_edema').val(vital_value);
                        $('.gynec_edema_range').html(string_class);
                    }else{
                        $('#gynec_edema').val('');
                        $('.gynec_edema_range').html('');
                    }
                }
                if(val.vital_master_id == 19){
                    if(vital_value !=''){
                        $('#gynec_cvs').val(vital_value);
                        $('.gynec_cvs_range').html(string_class);
                    }else{
                        $('#gynec_cvs').val('');
                        $('.gynec_cvs_range').html('');
                    }
                }
                if(val.vital_master_id == 20){
                    if(vital_value !=''){
                        $('#gynec_rs').val(vital_value);
                        $('.gynec_rs_range').html(string_class);
                    }else{
                        $('#gynec_rs').val('');
                        $('.gynec_rs_range').html('');
                    }
                }

            })

        }else{
            $('#gynec_height').val('');
            $('.gynec_height_range').html('');
            $('#gynec_weight').val('');
            $('.gynec_weight_range').html('');
            $('#gynec_bmi').val('');
            $('.gynec_bmi_range').html('');
            $('#gynec_waist_circ').val('');
            $('.gynec_waist_circ_range').html('');
            $('#gynec_pr').val('');
            $('.gynec_pr_range').html('');
            $('#gynec_bp_sys').val('');
            $('.gynec_bp_sys_range').html('');
            $('#gynec_bp_dia').val('');
            $('.gynec_bp_dia_range').html('');
            $('#gynec_pallor').val('');
            $('.gynec_pallor_range').html('');
            $('#gynec_edema').val('');
            $('.gynec_edema_range').html('');
            $('#gynec_rs').val('');
            $('.gynec_rs_range').html('');
            $('#gynec_cvs').val('');
            $('.gynec_cvs_range').html('');
        }

    }
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
}

$('.clinical_data_area').on('click', function(){
    var patient_id = $("#patient_id").val();
    if(patient_id == ''){
        toastr.warning("select patient!");
        return false;
    }
});

// $('.clinical_data_area').on('blur', function(){
//     var select = $(this);
//     if(patient_id == ''){
//         toastr.warning("select patient!");
//         return false;
//     }
//     var data_text= select.text();
//         data_text = btoa(data_text);
//         console.log(data_text);
//     var colum_name =select.attr('data-attr-label');
//     var base_url = $("#base_url").val();
//     var token = $("#c_token").val();
//     var patient_id = $("#patient_id").val();
//     var params = {
//         '_token':token,
//         'data_text':data_text,
//         'patient_id':patient_id,
//         'colum_name':colum_name
//     };
//     var url = base_url + "/gynecology_lite/savePatientClinicalData";
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: params,
//         beforeSend: function () {
//             select.LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
//         },
//         success: function (data) {
//             if(data == 1){
//                 toastr.success("Saved successfully!");
//             }else{
//                 toastr.error("Error please check your internet connection");
//             }
//         },
//         complete: function () {
//             select.LoadingOverlay("hide");
//         },
//         error: function () {
//             toastr.error("Error please check your internet connection");
//         },
//     });

// });

function save_gynec(){
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var doctor_id = $('#doctor_id').val();
    var encounter_id = $('#encounter_id').val();
    var pregnancy_id = 0;
    var lmp = $('#lmp').val();
    var pmp = $('#pmp').val();
    var edd = $('#edd').val();
    var presenting_complaints = $('#presenting_complaints').val();
    var local_examinations  = $('#local_examinations').val();
    var usg  = $('#usg').val();
    var provisional_diagnosis  = $('#provisional_diagnosis').val();
    var plan_of_care  = $('#plan_of_care').val();
    var gynecology_lite_id = $('#gynecology_lite_id').val();
    var follow_up_care  = $('#follow_up_care').val();
    var special_care  = $('#special_care').val();
    var token = $('#c_token').val();
    var base_url = $("#base_url").val();
    var clinical_data_string = '';
    var clinical_data = new Array();
    var height = $('#gynec_height').val();
    var weight = $('#gynec_weight').val();
    var bmi = $('#gynec_bmi').val();
    var waist_circ = $('#gynec_waist_circ').val();
    var pr = $('#gynec_pr').val();
    var bp_sys = $('#gynec_bp_sys').val();
    var bp_dia = $('#gynec_bp_dia').val();
    var pallor = $('#gynec_pallor').val();
    var edema = $('#gynec_edema').val();
    var cvs = $('#gynec_cvs').val();
    var rs = $('#gynec_rs').val();
    var periods = $('#periods').val();
    var dysmenorrhea = $('#dysmenorrhea').val();
    var dynamic_template_enable_emr_lite = $('#dynamic_template_enable_emr_lite').val();
    var add_new_vital_batch = $('#add_new_vital_batch').val();

    var vital_batch = '';
    if (parseInt(add_new_vital_batch) == 0) {
        vital_batch = $('#vital_batch_no').val();
    }

    if(patient_id == ''){
        toastr.warning("Select patient!");
        return false;
    }
    // if(lmp !=''){
    //     mark_pregnant(0);
    // }

    // if(presenting_complaints == ''){
    //     toastr.warning("Enter presenting complaints!");
    //     return false;
    // }
    // if(local_examinations == ''){
    //     toastr.warning("Enter Local examinations!");
    //     return false;
    // }
    $('.clinical_data_area').each(function(){
        var select = $(this);
        var data_text= select.val();
        data_text = data_text.trim();
        data_text = data_text.replace(/\s\s+/g, ' ');
        data_text = btoa(unescape(encodeURIComponent(data_text)))
        var colum_name =select.attr('data-attr-label');
        clinical_data.push({
            colum_name:colum_name,
            colum_value:data_text,
        });
    });

    clinical_data_string = JSON.stringify(clinical_data);

    var dataparams = {
        'token':token,
        'gynecology_lite_id':gynecology_lite_id,
        'patient_id':patient_id,
        'visit_id':visit_id,
        'doctor_id':doctor_id,
        'encounter_id':encounter_id,
        'pregnancy_id':pregnancy_id,
        'lmp':lmp,
        'pmp':pmp,
        'edd':edd,
        'presenting_complaints':presenting_complaints,
        'local_examinations':local_examinations,
        'usg':usg,
        'provisional_diagnosis':provisional_diagnosis,
        'plan_of_care':plan_of_care,
        'follow_up_care':follow_up_care,
        'special_care':special_care,
        'clinical_data_string':clinical_data_string,
        'height':height,
        'weight':weight,
        'bmi':bmi,
        'waist_circ':waist_circ,
        'pr':pr,
        'bp_sys':bp_sys,
        'bp_dia':bp_dia,
        'pallor':pallor,
        'edema':edema,
        'cvs':cvs,
        'rs':rs,
        'dysmenorrhea':dysmenorrhea,
        'periods':periods,
        'vital_batch' :vital_batch,
    };

    var url = base_url + "/gynecology_lite/savegynecologyLite";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (dynamic_template_enable_emr_lite == 1) {
                saveDynamicTemplate();
            }
            if(data == 1){
                // toastr.success("Saved successfully!");
            }else{
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            fetchGynecologyDetails(patient_id);

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}

$(document).on('click', '.patient_clinical_history_btn', function (e) {
    e.preventDefault();
    fetchPatientClinicalHistory();
});

$(document).on('click', '.patient_lab_results_btn', function (e) {
    e.preventDefault();
    fetchPatientLabResults();
});

$(document).on('click', '.patient_personal_notes_btn', function (e) {
    e.preventDefault();
    fetchPatientPersonalNotes();
});

$(document).on('click', '.patient_discharge_summary_list_btn', function (e) {
    e.preventDefault();
    fetchDischargeSummaryList();
});

$(document).on('click', '.patient_special_notes_btn', function (e) {
    e.preventDefault();
    fetchSpecialNotes();
});

$(document).on('click', '.patient_documents_btn', function (e) {
    e.preventDefault();
    var patient_id = $("#patient_id").val();
    if(patient_id){
        manageDocs();
    }
});

$(document).on('click', '.patient_radiology_results_btn', function (e) {
    e.preventDefault();
    fetchRadiologyResults();
});

$(document).on("change", "#search_service_id", function(){
    fetchRadiologyResults();
});

$(document).on("click",".radilogy_results_pagination_div nav ul li a.page-link", function(e) {
    $(this).parent('li').addClass('active');
    var page = $(this).attr('href').split('page=')[1];
    fetchRadiologyResults(page);
});

$(document).on("click",".patient_discharge_summary_create_btn", function(e) {
    e.preventDefault();
    var visit_id = $("#visit_id").val();
    var visit_type = $("#visit_type").val();
    if(visit_id && visit_type == 'IP'){
        editDischargeSummary(visit_id);
    } else {
        Command: toastr["warning"]("Please select any IP patient to continue.");
    }
});


$(document).on("click",".patient_refer_btn", function(e) {
    e.preventDefault();
    referPatient();
});

$(document).on("click",".patient_transfer_btn", function(e) {
    e.preventDefault();
    transferPatient();
});


function fetchPatientClinicalHistory() {
    var url = $('#base_url').val() + "/emr_lite/fetchPatientClinicalHistory";
    var patient_id = $("#patient_id").val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id
            },
            beforeSend: function () {
                $('.patient_clinical_history_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-history');
            },
            success: function (data) {

                if (data.status == 1) {
                    $("#patientClinicalHistoryModalBody").html(data.patient_combined_history);
                    $("#patientClinicalHistoryModal").modal('show');
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }
            },
            complete: function () {
                $('.patient_clinical_history_btn').find('i').addClass('fa-history').removeClass('fa-spinner fa-spin');

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });
            }
        });
    }
}

function fetchSpecialNotes() {
    var url = $('#base_url').val() + "/emr_lite/fetchSpecialNotes";
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                visit_id:visit_id
            },
            beforeSend: function () {
                $('.patient_special_notes_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-book');
            },
            success: function (data) {
                if(data != ''){
                    $('#special_notes_modal_body').html(data);
                    $("#special_notes_modal").modal('show');
                } else {
                    toastr.success("No special notes added yet.");
                }

            },
            complete: function () {
                $('.patient_special_notes_btn').find('i').addClass('fa-book').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchPatientPersonalNotes() {
    var url = $('#base_url').val() + "/emr_lite/fetchPatientPersonalNotes";
    var patient_id = $("#patient_id").val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id
            },
            beforeSend: function () {
                $('.patient_personal_notes_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-commenting-o');
            },
            success: function (data) {
                console.log(data);
                if (data.status == 1) {
                    $(".private_notes_textarea").text(data.personal_notes);
                    $("#patientPersonalNotesModal").modal('show');
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }
            },
            complete: function () {
                $('.patient_personal_notes_btn').find('i').addClass('fa-commenting-o').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchPatientLabResults() {
    var url = $('#base_url').val() + "/emr_lite/labResultTrends";
    var patient_id = $("#patient_id").val();
    var encounter_id = $('#encounter_id').val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "GET",
            url: url,
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id,
            beforeSend: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-flask');
            },
            success: function (data) {
                console.log(data);
                $('#lab_restuls_data').html(data);
                $("#patientLabResultsModal").modal('show');
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            },
            complete: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa-flask').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchRadiologyResults(page = 1) {
    var url = $('#base_url').val() + "/emr_lite/fetchRadiologyResults";
    var patient_id = $("#patient_id").val();
    var search_service_id = $('#search_service_id').val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                search_service_id:search_service_id,
                page: page
            },
            beforeSend: function () {
                $('.patient_radiology_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-camera');
            },
            success: function (data) {
                $('#radiology_results_data').html(data.html);
                $("#search_service_id").empty();
                $("#search_service_id").append('<option value="">Select Service</option>');
                $.each(data.service_list, function(key, val){
                    $("#search_service_id").append('<option value="'+val.id+'">'+val.service_desc+'</option>');
                })

                if(search_service_id){
                    $("#search_service_id").val(search_service_id);
                }
                $("#radiology_results_modal").modal('show');
            },
            complete: function () {
                $('.patient_radiology_results_btn').find('i').addClass('fa-camera').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchDischargeSummaryList() {
    var url = $('#base_url').val() + "/emr_lite/fetchDischargeSummaryList";
    var patient_id = $("#patient_id").val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id
            },
            beforeSend: function () {
                $('.patient_discharge_summary_list_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-list');
            },
            success: function (data) {
                $('#discharge_summary_list_modal_body').html(data);
                $("#discharge_summary_list_modal").modal('show');
            },
            complete: function () {
                $('.patient_discharge_summary_list_btn').find('i').addClass('fa-list').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function showSummary(summary_id) {
    var url = $('#base_url').val() + "/emr_lite/showDischargeSummary";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            discharge_summary_id: summary_id
        },
        beforeSend: function () {
            $('#discharge_summary_view_modal').modal('show');
            $('#discharge_summary_view_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#discharge_summary_view_modal_body').LoadingOverlay("hide");
            $('#discharge_summary_view_modal_body').html('<div class="" style="height:400px;overflow-y:scroll">' + data.summary + '</div>');

            if (data.final_status == 0) {
                $('#discharge_summary_view_modal_body').append('<button style="margin-top: 20px;" title="Mark as Finalized" onclick="finalizeDischargeSummary(' + summary_id + ');" class="btn btn-success"><i class="fa fa-check"></i>Finalize Summary</button><button style="margin-top: 20px;" title="Edit Summary" onclick="editDischargeSummary(' + data.visit_id + ');" class="btn btn-success"><i class="fa fa-edit"></i> Edit Summary</button>');
            }
        },
        complete: function () {

        }
    });
}

function finalizeDischargeSummary(summary_id) {
    var url = $('#base_url').val() + "/emr/finalizeDischargeSummary";
    $.ajax({
        url: url,
        type: "GET",
        data: "summary_id=" + summary_id,
        beforeSend: function () {
            $('#summary_view_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            if ($('#summary_view_modal').hasClass('in')) {
                $('#summary_view_modal').modal('hide');
            } else {
                showDischargeSummary();
            }
            if (data.status == 1) {
                toastr.success("Discharge Summary Finalized..!");
            }

        },
        complete: function () {

        }
    });
}



function editDischargeSummary(visit_id) {
    window.location = $("#base_url").val() + '/summary/dischargesummary/' + visit_id;
}


function referPatient(){
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/emr_lite/referPatient";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            req_type:"show",
            patient_id:patient_id,
            visit_id:visit_id,
        },
        beforeSend: function () {
            $('.patient_refer_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-user-plus');
        },
        success: function (response) {
            $('#referDoctor .modal-body').html(response.html);
            $('#referDoctor').modal('show');
        },
        complete: function () {
            $('.patient_refer_btn').find('i').addClass('fa-user-plus').removeClass('fa-spinner fa-spin');
        }
    });
}

function saveReferDoctor() {

    if ($('#doctor_ref_loaded').length > 0) {

            $('#refer_save_btn').prop('disabled', true);

            let patient_id = $('#patient_id').val();
            let visit_id = $('#visit_id').val();
            let encounter_id = $('#encounter_id').val();
            let _token = $('#c_token').val();
            let data_params = $('#referDoctorForm').serialize();
            let payment_status = $("input[name='payment_status']:checked").val();
            let reference_type = $('#reference_type').val();

            data_params += '&patient_id=' + patient_id;
            data_params += '&visit_id=' + visit_id;
            data_params += '&encounter_id=' + encounter_id;
            data_params += '&payment_status=' + payment_status;
            data_params += '&reference_type=' + reference_type;
            data_params += '&req_type=' + 'save';
            data_params += '&_token=' + _token;


            var url = $('#base_url').val() + "/emr_lite/referPatient";
            $.ajax({
                url: url,
                type: "POST",
                data: data_params,
                beforeSend: function () {
                    $("#referDoctor .modal-body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                },
                success: function (data) {
                    if (parseInt(data) > 0) {
                        //when data set to 1 saved successfully
                        //when data set to 2 saved partialy some already reffered [unit wise refferal]
                        //when data set to 3 already reffered [single doctor refferal]

                        if (parseInt(data) == 1) {
                            Command: toastr["success"]('Doctor Referred Successfully');
                            $('#referDoctor').modal('hide');
                        } else if (parseInt(data) == 2) {
                            Command: toastr["success"]('Some Doctors Already Referred');
                        } else if (parseInt(data) == 3) {
                            Command: toastr["warning"]('Already Referred');
                        }

                        $('#doctor_ref').val('').trigger('change');
                        $('textarea[name="refer_notes"]').val('');
                    } else {
                        Command: toastr["error"]('Insertion Failed.');
                    }
                },
                error: function () {
                    Command: toastr["warning"]('Error.!');
                },
                complete: function () {
                    $('#refer_save_btn').prop('disabled', false);
                    $("#referDoctor .modal-body").LoadingOverlay("hide");
                }
            });

    }

}


function transferPatient(){
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/emr_lite/transferPatient";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            req_type:"show",
            patient_id:patient_id,
            visit_id:visit_id,
        },
        beforeSend: function () {
            $('.patient_transfer_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-exchange');
        },
        success: function (response) {
            if(response.status == 1){
                $('#transferDoctor .modal-body').html(response.html);
                $('#transferDoctor').modal('show');
            } else {
                Command: toastr["error"]('Please select any patient.');
            }

        },
        complete: function () {
            $('.patient_transfer_btn').find('i').addClass('fa-exchange').removeClass('fa-spinner fa-spin');
        }
    });
}

function saveTransferPatient() {
    if ($('#doctor_ref_loaded').length > 0) {

        $('#transfer_save_btn').prop('disabled', true);
        let booking_id = $('#booking_id').val();
        let patient_id = $('#patient_id').val();
        let visit_id = $('#visit_id').val();
        let encounter_id = $('#encounter_id').val();
        let _token = $('#c_token').val();
        let data_params = $('#transferDoctorForm').serialize();
        let payment_status = $("input[name='payment_status']:checked").val();
        let reference_type = $('#reference_type').val();


        data_params += '&patient_id=' + patient_id;
        data_params += '&visit_id=' + visit_id;
        data_params += '&encounter_id=' + encounter_id;
        data_params += '&payment_status=' + payment_status;
        data_params += '&reference_type=' + reference_type;
        data_params += '&req_type=' + 'save';
        data_params += '&_token=' + _token;
        data_params += '&booking_id=' + booking_id;


        var url = $('#base_url').val() + "/emr_lite/transferPatient";
        $.ajax({
            url: url,
            type: "POST",
            data: data_params,
            beforeSend: function () {
                $("#transferDoctor .modal-body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                if (parseInt(data) > 0) {
                    //when data set to 1 saved successfully
                    //when data set to 2 saved partialy some already reffered [unit wise refferal]
                    //when data set to 3 already reffered [single doctor refferal]

                    if (parseInt(data) == 1) {
                        Command: toastr["success"]('Doctor Referred Successfully');
                        $('#transferDoctor').modal('hide');
                        removeBeforeUnloadCallbak();
                        location.reload();
                    } else if (parseInt(data) == 2) {
                        Command: toastr["success"]('Some Doctors Already Referred');
                    } else if (parseInt(data) == 3) {
                        Command: toastr["warning"]('Already Referred');
                    }

                    $('#doctor_transfer').val('').trigger('change');
                    $('textarea[name="transfer_notes"]').val('');
                } else {
                    Command: toastr["error"]('Insertion Failed.');
                }
            },
            error: function () {
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $('#transfer_save_btn').prop('disabled', false);
                $("#transferDoctor .modal-body").LoadingOverlay("hide");
            }
        });

    }

}



$(document).on("click", ".pacsViewerBtn", function(){
    let pacs_viewer_prefix = $('#pacs_viewer_prefix').val();
    var accession_no = $(this).attr('data-accession-no');
    if (pacs_viewer_prefix && accession_no) {
        $("#pacs_viewer_iframe").attr('src', pacs_viewer_prefix+accession_no);
    }
    $("#radiology_pacs_viewer_iframe_modal").modal('show');
});

$(document).on("click", ".pacsViewReportBtn", function(){
    let pacs_report_prefix = $('#pacs_report_prefix').val();
    var accession_no = $(this).attr('data-accession-no');
    window.open(pacs_report_prefix+accession_no, '_blank');
});


// $('.number_input').keydown(function(evt) {
//     if ((evt.which < 48 || evt.which > 57) && (evt.which < 105 || evt.which > 96) && (evt.which != 190)) {
//         evt.preventDefault();
//     }
// });​



function fetchGynecologyDetails(patient_id){
    var url = $('#base_url').val() + "/gynecology_lite/fetchGynecologyDetails";
    var dataparams = {'patient_id':patient_id};
        $.ajax({
            url: url,
            type: "POST",
            data: dataparams,
            beforeSend: function () {
                $("#gynec_history_container").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $('#gynec_history_container').html(data);
            },
            error: function () {
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $("#gynec_history_container").LoadingOverlay("hide");
            }
        });
}
function fetchGynecologycurrentVisitDetails(booking_id){
    var url = $('#base_url').val() + "/gynecology_lite/fetchGynecologycurrentVisitDetails";
    var dataparams = {'booking_id':booking_id};
        $.ajax({
            url: url,
            type: "POST",
            data: dataparams,
            beforeSend: function () {
                $('#presenting_complaints').val('');
                    $('.chief_complaint_div').html('');
                    $('#local_examinations').val('');
                    $('#usg').val('');
                    $('#provisional_diagnosis').val('');
                    $('#plan_of_care').val('');
                    $('#follow_up_care').val('');
                    $('#special_care').val('');
                    // $('#periods').val('');
                    // $('#dysmenorrhea').val('');
            },
            success: function (data) {
                if(data !=0){
                    var obj = JSON.parse(data);
                    $('#presenting_complaints').val(obj.presenting_complaints);
                    $('.chief_complaint_div').html(obj.presenting_complaints);
                    $('#local_examinations').val(obj.local_examinations);
                    $('#usg').val(obj.usg);
                    $('#provisional_diagnosis').val(obj.provisional_diagnosis);
                    $('#plan_of_care').val(obj.plan_of_care);
                    $('#follow_up_care').val(obj.follow_up_care);
                    $('#special_care').val(obj.special_care);
                    // $('#periods').val(obj.periods);
                    // $('#dysmenorrhea').val(obj.dysmenorrhea);
                }else{
                    $('#presenting_complaints').val('');
                    $('.chief_complaint_div').html('');
                    $('#local_examinations').val('');
                    $('#usg').val('');
                    $('#provisional_diagnosis').val('');
                    $('#plan_of_care').val('');
                    $('#follow_up_care').val('');
                    $('#special_care').val('');
                    // $('#periods').val('');
                    // $('#dysmenorrhea').val('');
                }
            },
            error: function () {

            },
            complete: function () {

            }
        });
}

function showPatientCombinedHistory(patient_combined_history) {
    $(".patient_combined_history_div").html(patient_combined_history);
}

$(document).on("click", ".saveClinicalDataButton", function () {
    saveClinicalData();
});

function saveClinicalData() {

    $('.saveClinicalDataButton').prop({ disable: 'true' });
    var patient_clinical_data = {};

    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let ca_head_id = $('#ca_head_id').val();
    let form_id = $("#default_note_form_id").val();
    let tableList = $('#table_names').val();
    let gcheckboxNamesArr = $('#gcheckbox_names').val();
    let ca_form = $('#ca-data-form').serializeArray();
    let psearchSelectedArr = [];
    let dataValueArr = [];

    var tinyList = '';
    if ($.trim($('#tinymce_names').val()) != "") {
        tinyList = JSON.parse($('#tinymce_names').val());
    }

    if(patient_id ==''){
        return false;
    }

    //table strucure check
    //all table field name array
    var tableNames = "";
    if ($.trim(tableList) != "" && $.trim(tableList) != undefined) {
        tableNames = JSON.parse(tableList);
    }
    var tableFieldNames = [];
    var arrTableRowColData = [];
    if ((tableNames.length) > 0) {

        for (var j = 0; j < tableNames.length; j++) {
            colsCount = $('input[name="' + tableNames[j] + '_#colsCount"]').val();
            rowsCount = $('input[name="' + tableNames[j] + '_#rowsCount"]').val();
            var fieldName = tableNames[j];
            tableFieldNames.push(fieldName);
            arrTableRowColData.push({ 'cols': colsCount, 'rows': rowsCount });
        }

    }

    //group checkbox check
    //all group checkbox field name array
    var gcheckboxNames = gcheckboxNamesArr;

    var psearchNames = '';
    //all Progress Search field name array
    if ($.trim($('#psearch_names').val()) != "") {
        psearchNames = JSON.parse($('#psearch_names').val());
    }


    var notes_data = {};
    //loop through all fields and create json
    $(ca_form).each(function (index, obj) {

        //tinymce data setup
        if ((tinyList.length) > 0) {
            //check its a tinymce field
            if (tinyList.indexOf(obj.name) >= 0) {
                let tinycontent = '';
                tinycontent = tinyMCE.get(obj.name).getContent();
                ca_form[index].value = tinycontent;
                notes_data[obj.name] = tinycontent;
                return;
            }
        }

        //progress search data setup [multiselect]
        if ((psearchNames.length) > 0) {
            //check its a progress search field
            if (psearchNames.indexOf(obj.name) >= 0) {
                notes_data[obj.name + '_psearch_' + obj.value] = obj.value;
                psearchSelectedArr.push(obj.name + '_psearch_' + obj.value);
            }
        }

        notes_data[obj.name] = obj.value;

        if (obj.value != '' && obj.value != undefined) {
            dataValueArr.push(obj.value);
        }

    });

    //remove progress old keys
    if ((psearchNames.length) > 0) {
        $.each(psearchNames, function (index, psdata) {
            if (psdata != "" && psdata != undefined) {
                delete notes_data[psdata];
            }
        });
    }

    let validate = validateAssessment(dataValueArr);

    // if (form_id == "") {
    //     Command: toastr["warning"]("Error.! Select Form.");
    //     return false;
    // }

    if (dataValueArr.length > 0) {
        notes_data = JSON.stringify(notes_data);
        notes_data = encodeURIComponent(notes_data)
    } else {
        notes_data = "";
    }

    patient_clinical_data.patient_id = patient_id;
    patient_clinical_data.visit_id = visit_id;
    patient_clinical_data.encounter_id = encounter_id;
    patient_clinical_data.ca_head_id = ca_head_id;
    patient_clinical_data.form_id = form_id;
    patient_clinical_data.arrTableRowColData = JSON.stringify(arrTableRowColData);
    patient_clinical_data.tableFieldNames = JSON.stringify(tableFieldNames);
    patient_clinical_data.gcheckboxNames = gcheckboxNames;
    patient_clinical_data.psearchNames = JSON.stringify(psearchSelectedArr);
    if(notes_data != ""){
        patient_clinical_data.notes_data = JSON.stringify(notes_data);
    } else {
        patient_clinical_data.notes_data = notes_data;
    }


    // investigation details

    var investigations = {};
    investigations.investigation_list = [];

    $(".selected_investigation_table_body").find('tr').each(function (key, val) {
        var investigation = {};
        investigation.detail_id = $(val).attr('data-detail-id') ? $(val).attr('data-detail-id') : '';
        investigation.service_name = $(val).find('.selected_investigation_service_name').html();
        investigation.service_code = $(val).attr('data-service-code');
        investigations.investigation_list.push(investigation);
    })

    investigations.remark = $(".investigation_remarks_textarea").text();
    investigations.clinical_history = $(".investigation_clinical_history_textarea").text();

    patient_clinical_data.investigations = investigations;
    patient_clinical_data.investigation_head_id = $('#investigation_head_id').val();

    // end of investigation


    // prescription details


    let validatePresc = validatePrescription();
    if (validatePresc) {

        var medicine_check = [];
        $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
            if ($(val).val() != '') {
                medicine_check.push(val);
            }
        });

        if (medicine_check.length > 0) {
            let prescription_head_id = $('#prescription_head_id').val();
            // var p_type = $("input[name='p_search_type']:checked").val();
            var presc_form = [];

            $(".prescription_list_table_body").find('tr.row_class').each(function (key, value) {
                var presc_row = {};
                presc_row.selected_item_code = $(value).find('input[name="medicine_name"]').attr('data-item-code');
                presc_row.selected_item_name = $(value).find('input[name="medicine_name"]').val();
                presc_row.selected_item_frequency = $(value).find('.frequency').val();
                presc_row.selected_frequency_value = $(value).find('.frequency').attr('data-frequency-value');
                presc_row.selected_frequency_id = $(value).find('.frequency').attr('data-frequency-id');
                presc_row.selected_item_duration = $(value).find('.duration').val();
                presc_row.selected_item_quantity = $(value).find('.quantity').val();
                presc_row.selected_item_direction = $(value).find('.direction').val() ? $(value).find('.direction').val() : '';
                presc_row.selected_detail_id = $(value).attr('data-detail-id') ? $(value).attr('data-detail-id') : '';
                // presc_row.selected_item_generic_name = $(value).find('input[name="selected_item_name[]"]').attr('data-generic-name');
                // presc_row.selected_item_remarks = $(value).find('input[name="selected_item_remarks[]"]').val();
                // presc_row.selected_item_dose = $(value).find('input[name="selected_item_dose[]"]').val();
                // presc_row.selected_item_start_at = $(value).find('input[name="iv_selected_start_at[]"]').val() ? $(value).find('input[name="iv_selected_start_at[]"]').val() : '';

                presc_form.push(presc_row);

            });

            var presc_form1 = JSON.stringify(presc_form);
            presc_form = escape(presc_form1);

            patient_clinical_data.prescriptions = presc_form;
            patient_clinical_data.prescription_head_id = prescription_head_id;
            patient_clinical_data.presc_advice = $(".prescription_advice_textarea").val();
            patient_clinical_data.next_review_date = $("#next_review_date").val();
            patient_clinical_data.next_review_remarks = $("#next_review_remarks").val();
        } else {
            patient_clinical_data.prescriptions = "";
            patient_clinical_data.prescription_head_id = "";
            patient_clinical_data.presc_advice = $(".prescription_advice_textarea").val();
            patient_clinical_data.next_review_date = $("#next_review_date").val();
            patient_clinical_data.next_review_remarks = $("#next_review_remarks").val();
        }

    } else {
        $('.saveClinicalDataButton').prop({ disable: 'false' });
        return;
    }
    var booking_id = $("#booking_id").val()
    patient_clinical_data.booking_id = booking_id;

    if (validate && validatePresc) {
        var url = $('#base_url').val() + "/emr_lite/saveClinicalData";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_clinical_data: patient_clinical_data
            },
            beforeSend: function () {
                $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                console.log(data);
                if (data.status == 1) {
                    fetchPatientDetails();
                    $(".selected_investigation_table_body").empty();
                    $(".prescription_list_table_body").empty();
                    $("#ca-data-form").find('textarea').val('');
                    $("#ca-data-form").find('input:text').val('');
                    $("#ca-data-form").find('input:radio').prop('checked', false);
                    $("#ca-data-form").find('input:checkbox').prop('checked', false);
                    $("#ca-data-form").find('li').removeClass('active');
                    $("#ca-data-form").find('textarea').val('');
                    $("#ca-data-form").find('input:text').val('');
                    $("#ca-data-form").find('input:radio').prop('checked', false);
                    $("#ca-data-form").find('input:checkbox').prop('checked', false);
                    $("#ca-data-form").find('li').removeClass('active');
                    $("#ca-data-form").find('input:hidden').val('');

                    $(".investigation_tree_area").find('input[type="checkbox"]').prop('checked', false)
                    $(".prescription_history_div").find('input[type="checkbox"]').prop('checked', false)
                    $(".prescription_bookmark_div").find('input[type="checkbox"]').prop('checked', false)

                    Command: toastr["success"]("Success");

                    if($("#print_dialog_config").val().toString() == '1' & data.prescription_head_id > 0 && data.investigation_head_id > 0){
                        $("#prescription_print_config_modal").attr("presc-head-id", data.prescription_head_id);
                        $("#prescription_print_config_modal").attr("invest-head-id", data.investigation_head_id);
                        $("#prescription_print_config_modal").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if(data.prescription_head_id > 0 && data.investigation_head_id == 0){
                        printPrescription(data.prescription_head_id);
                    } else if(data.prescription_head_id == 0 && data.investigation_head_id > 0){
                        printInvestigation(data.investigation_head_id);
                    }
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }

            },
            complete: function () {
                $(".prescription_advice_textarea").val('');
                $("#next_review_remarks").val('');
                $('body').LoadingOverlay("hide");
                saveInvestigationResultEntry();
                saveUsgResultsEntry();
            }
        });

    } else {
        $('.saveClinicalDataButton').prop({ disable: 'false' });
    }
}

// validate notes
function validateAssessment(dataValueArr) {
    if (Array.isArray(dataValueArr)) {
        return true;
    }
    // if (Array.isArray(dataValueArr) && dataValueArr.length) {
    //     return true;
    // }
    return false;
}


//validate prescription
function validatePrescription() {

    $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
        if ($(val).val() == '') {
            $(val).parents('tr').remove();
        }
    })

    var medicine_check = [];
    $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
        if ($(val).val() != '') {
            medicine_check.push(val);
        }
    });

    // if (medicine_check.length == 0) {
    //     Command: toastr["error"]("Select Medicine.");
    //     $('input[name="medicine_name"]')[0].focus();
    //     return false;
    // }

    if (medicine_check.length > 0 && medicine_check.length != $('.prescription_list_table_body > tr').find('.frequency:not([value=""])').length) {
        Command: toastr["error"]("Please select frequency.");
        $('input[name="selected_item_frequency[]"]').each(function (key, val) {
            if ($(val).parent().find('input[name="selected_frequency_value[]"]').val() == '') {
                $(val).focus();
                return false;
            }
        })
        return false;
    }

    var valid_duration = [];
    $('.duration').each(function (key, val) {
        if ($(val).val() != '') {
            valid_duration.push(val);
        }
    })

    if (medicine_check.length != valid_duration.length) {
        Command: toastr["error"]("Please select Days.");
        $('input[name="selected_item_duration[]"]').each(function (key, val) {
            if ($(val).val() == '') {
                $(val).focus();
                return false
            }
        })

        return false;
    }

    var valid_quantity = [];
    $('.quantity').each(function (key, val) {
        if ($(val).val() != '') {
            valid_quantity.push(val);
        }
    })

    if (medicine_check.length != valid_quantity.length) {
        Command: toastr["error"]("Please select quantity.");

        $('.quantity').each(function (key, val) {
            if ($(val).val() == '') {
                $(val).focus();
                return false
            }
        })
        return false;
    }

    return true;
}

function mark_pregnant(success_status){
    var lmp = $('#lmp').val();
    var pmp = $('#pmp').val();
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();

    if(patient_id == ''){
        Command: toastr["warning"]("Please select patient");
        return false;
    }

    if(lmp ==''){
        Command: toastr["warning"]("Please enter LMP");
        $('#lmp').focus();
        return false;
    }

    setGestationalAge();

    var edd = $('#edd').val();
    dataparams = {
        'lmp':lmp,
        'pmp':pmp,
        'edd':edd,
        'patient_id':patient_id,
        'visit_id':visit_id,
        'success_status':success_status,
    }

    var url = $('#base_url').val() + "/gynecology_lite/markPregnant";
        $.ajax({
            type: "POST",
            url: url,
            data:dataparams,
            beforeSend: function () {
                $('.mark_pregnancy_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-check-circle');
            },
            success: function (data) {
                if(data ==1){
                    if(success_status == 1){
                        Command: toastr["success"]("Saved successfully!");
                    }

                    $('#mark_pregnancy_btn').addClass('add_bg-green');
                    $('#i_pregnancy_btn').addClass('add_color-white');
                    $('#span_pregnancy_btn').addClass('add_color-white');
                    $('#pregnant_status').css('display','block');


                }else if(data ==2){
                    Command: toastr["success"]("Updated successfully!");
                    $('#mark_pregnancy_btn').removeClass('add_bg-green');
                    $('#i_pregnancy_btn').removeClass('add_color-white');
                    $('#span_pregnancy_btn').removeClass('add_color-white');
                    $('#pregnant_status').css('display','block');

                    $('#lmp').val('');
                    $('#edd').val('');
                    $('#pmp').val('');
                    $('#gestational_age').val('');
                    $('#pregnant_status').css('display','none');

                }else if(data ==3){
                    Command: toastr["warning"]("Patient is not female!");
                    $('#lmp').val('');
                    $('#edd').val('');
                    $('#pmp').val('');
                    $('#gestational_age').val('');
                    $('#mark_pregnancy_btn').removeClass('add_bg-green');
                    $('#i_pregnancy_btn').removeClass('add_color-white');
                    $('#span_pregnancy_btn').removeClass('add_color-white');
                    $('#pregnant_status').css('display','none');
                }else{
                    Command: toastr["error"]("Something went wrong!");
                    $('#mark_pregnancy_btn').removeClass('add_bg-green');
                    $('#i_pregnancy_btn').removeClass('add_color-white');
                    $('#span_pregnancy_btn').removeClass('add_color-white');
                    $('#pregnant_status').css('display','none');
                }
            },
            complete: function () {
                $('.mark_pregnancy_btn').find('i').addClass('fa-check-circle').removeClass('fa-spinner fa-spin');
            }
        });

}

$('#presenting_complaints').on('blur',function(){
    var chief_complaint = $('#presenting_complaints').html();
    $('.chief_complaint_div').html(chief_complaint);
});

function setGestationalAge(){
    var lmp = $('#lmp').val();
    if(lmp == ''){
        $('#gestational_age').val('');
        return;
    }
    const date = new Date(lmp);
    const lmpdate = new Date(lmp);
    date.setDate(date.getDate() + 280);
    $('#edd').val(moment(date).format('MMM-DD-YYYY'));

    let date_1 = new Date(moment(lmpdate).format('MM-DD-YYYY'));
    let date_2 = new Date();
    let difference = date_2. getTime() - date_1. getTime();
    let TotalDays = Math. ceil(difference / (1000 * 3600 * 24));
    //console.log(date_1);
    let weeks = 0
    let days = 0;
    if(TotalDays > 7){
        weeks =  Math.floor(TotalDays / 7),
        days = TotalDays % 7;
        $('#gestational_age').val(weeks+'wk - '+days+'d');
    }else{
        $('#gestational_age').val(TotalDays+' days');
    }
}

function fetchPregnancyDetails(patient_id){
    var url = $('#base_url').val() + "/gynecology_lite/fetchPregnancyDetails";
    var dataparams = {'patient_id':patient_id};
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
        },
        success: function (data) {
            if(data!=0){
                var obj = JSON.parse(data);
                var is_pregnant = obj.is_pregnant;
                $('#lmp').val(obj.lmp);
                $('#edd').val(obj.edd);
                $('#pmp').val(obj.pmp);
                $('#dysmenorrhea').val(obj.dysmenorrhea);
                $('#periods').val(obj.periods);

                if(is_pregnant == 1){
                    if(obj.diff > 7){
                        weeks =  Math.floor(obj.diff / 7),
                        days = obj.diff % 7;
                        $('#gestational_age').val(weeks+'wk - '+days+'d');
                    }else{
                        $('#gestational_age').val(obj.diff+' days');
                    }
                    $('#mark_pregnancy_btn').addClass('add_bg-green');
                    $('#i_pregnancy_btn').addClass('add_color-white');
                    $('#span_pregnancy_btn').addClass('add_color-white');
                    $('#pregnant_status').css('display','block');
                }

            }else{
                $('#lmp').val('');
                $('#edd').val('');
                $('#pmp').val('');
                $('#gestational_age').val('');
                $('#dysmenorrhea').val('');
                $('#periods').val('');

                $('#mark_pregnancy_btn').removeClass('add_bg-green');
                $('#i_pregnancy_btn').removeClass('add_color-white');
                $('#span_pregnancy_btn').removeClass('add_color-white');
                $('#pregnant_status').css('display','none');
            }
        },
        complete: function () {

        }
    });
}



//-----bmi------------------------------------------
$(document).on("keyup","#gynec_weight", function(){
    var weight = $('#gynec_weight').val();
    var height = $('#gynec_height').val();
    var bmi = '';
    if(weight != '' && height != ''){
        bmi = calculateBmi(weight, height);
        $('#gynec_bmi').val(bmi);
    }else{
        $('#gynec_bmi').val('');
    }
});


$(document).on("keyup","#gynec_height", function(){
    var weight = $('#gynec_weight').val();
    var height = $('#gynec_height').val();
    var bmi = '';
    if(weight != '' && height != ''){
        bmi = calculateBmi(weight, height);
        $('#gynec_bmi').val(bmi);
    }else{
        $('#gynec_bmi').val('');
    }
});

function savePatientPersonalNotes(){
    var personal_notes = $(".private_notes_textarea").val();
    var patient_id = $("#patient_id").val();
    var visibility_status = 0;

    if(patient_id && personal_notes){
        var url = $('#base_url').val() + "/emr_lite/savePatientPersonalNotes";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                personal_notes:personal_notes,
                patient_id:patient_id,
                visibility_status: visibility_status
            },
            beforeSend: function () {
                $(".savePatientPersonalNotesBtn").find('i').removeClass('fa-save').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                if(data.status == 1){
                    $("#patientPersonalNotesModal").modal('hide');
                    Command: toastr["success"]('Successfully saved.');
                } else {
                    Command: toastr["error"]('Save failed.');
                }
            },
            error: function () {
                Command: toastr["error"]('Error.!');
            },
            complete: function () {
                $(".savePatientPersonalNotesBtn").find('i').removeClass('fa-spinner fa-spin').addClass('fa-save');
            }
        });
    } else {
        Command: toastr["error"]('Invalid details');
    }

}

function getDoctorBookmarkedNotes() {
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/getDoctorBookmarkedNotes";
    $.ajax({
        type: "POST",
        url: url,
        beforeSend: function () {
            $(".notes_bookmarks_area").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $(".notes_bookmarks_area").html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $(".notes_bookmarks_area").LoadingOverlay("hide");

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function chekMinMax(obj,master_id){
    var vital_value = $(obj).val();
    var vital_min_array = $('#vital_min_data').val();
    var vital_max_array = $('#vital_max_data').val();
    var vital_min_value = '';
    var vital_max_value = '';
    vital_min_array = JSON.parse(vital_min_array);
    vital_max_array = JSON.parse(vital_max_array);

    vital_min_value = vital_min_array[master_id];
    vital_max_value = vital_max_array[master_id];

    var range_container ={};
    range_container[4] = 'gynec_height_range';
    range_container[2] = 'gynec_weight_range';
    range_container[14] = 'gynec_bmi_range';
    range_container[15] = 'gynec_waist_circ_range';
    range_container[16] = 'gynec_pr_range';
    range_container[5] = 'gynec_bp_sys_range';
    range_container[6] = 'gynec_bp_dia_range';
    range_container[17] = 'gynec_pallor_range';
    range_container[18] = 'gynec_edema_range';
    range_container[19] = 'gynec_cvs_range';
    range_container[20] = 'gynec_rs_range';


    var string_class = '';
    if ((vital_value != null) && (vital_max_value !='') && (vital_value > parseFloat(vital_max_value))) {
        string_class = " <i title='Min:"+vital_min_value+" - Max:"+vital_max_value+"' class='fa fa-arrow-up blink_me'></i> ";
    }
    if ((vital_value != null) && (vital_min_value !='')  &&(vital_value < parseFloat(vital_min_value))) {
        string_class = " <i title='Min:"+vital_min_value+" - Max:"+vital_max_value+"' class='fa fa-arrow-down blink_me'></i> ";
    }

    if(string_class !=''){
        $('.'+range_container[master_id]).html(string_class);
    }else{
        $('.'+range_container[master_id]).html('');
    }
    if(vital_value ==''){
        $('.'+range_container[master_id]).html('');
    }

}

function clearvitalsArea(){
    $('#gynec_height').val('');
    $('.gynec_height_range').html('');
    $('#gynec_weight').val('');
    $('.gynec_weight_range').html('');
    $('#gynec_bmi').val('');
    $('.gynec_bmi_range').html('');
    $('#gynec_waist_circ').val('');
    $('.gynec_waist_circ_range').html('');
    $('#gynec_pr').val('');
    $('.gynec_pr_range').html('');
    $('#gynec_bp_sys').val('');
    $('.gynec_bp_sys_range').html('');
    $('#gynec_bp_dia').val('');
    $('.gynec_bp_dia_range').html('');
    $('#gynec_pallor').val('');
    $('.gynec_pallor_range').html('');
    $('#gynec_edema').val('');
    $('.gynec_edema_range').html('');
    $('#gynec_rs').val('');
    $('.gynec_rs_range').html('');
    $('#gynec_cvs').val('');
    $('.gynec_cvs_range').html('');
}

function saveUsgResultsEntry(){
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var doctor_id = $('#doctor_id').val();
    var entry_status = 0;


    $('#usg_res_entry').find('input[type="textbox"]').each(function(){
        console.log($(this).val());
        if($(this).val() != ''){
            entry_status = 1;
        }
    });

    if(entry_status == 0){
        return false;
    }

    if(patient_id == ''){
        toastr.warning("select patient!");
        return false;
    }

    var usg_results_entry_date = $('#usg_results_entry_date').val();

    var pog_wk = $('#pog_wk').val();
    var pog_d = $('#pog_d').val();
    var siugs = 0;
    var sliug = 0;
    var slf = 0;
    var slf_wk = $('#slf_wk').val();
    var slf_d = $('#slf_d').val();
    var ceph = 0;
    var breech = 0;
    var transverse_lie = 0;
    var afi = $('#afi').val();
    var efw = $('#efw').val();
    var edd = $('#usg_edd').val();
    var fhr = $('#fhr').val();
    var placenta = $('#placenta').val();
    var nt = $('#nt').val();
    var nb = $('#nb').val();

    if($("#siugs").prop('checked') == true){
        siugs = 1;
    }else{
        siugs = 0;
    }

    if($("#sliug").prop('checked') == true){
        sliug = 1;
    }else{
        sliug = 0;
    }

    if($("#slf").prop('checked') == true){
        slf = 1;
    }else{
        slf = 0;
    }

    if($("#ceph").prop('checked') == true){
        ceph = 1;
    }else{
        ceph = 0;
    }

    if($("#breech").prop('checked') == true){
        breech = 1;
    }else{
        breech = 0;
    }

    if($("#transverse_lie").prop('checked') == true){
        transverse_lie = 1;
    }else{
        transverse_lie = 0;
    }

    var dataparams = {
        patient_id:patient_id,
        visit_id:visit_id,
        doctor_id:doctor_id,
        usg_results_entry_date:usg_results_entry_date,
        pog_wk:pog_wk,
        pog_d:pog_d,
        siugs:siugs,
        sliug:sliug,
        slf:slf,
        slf_wk:slf_wk,
        slf_d:slf_d,
        ceph:ceph,
        fhr:fhr,
        breech:breech,
        transverse_lie:transverse_lie,
        afi:afi,
        efw:efw,
        edd:edd,
        placenta:placenta,
        nt:nt,
        nb:nb,
    };

    var base_url = $("#base_url").val();
    var url = base_url + "/gynecology_lite/saveUsgResultsEntry";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {

        },
        success: function (data) {

        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function fetchUsgResultsEntry(patient_id){
    var base_url = $("#base_url").val();
    var url = base_url + "/gynecology_lite/fetchUsgResultsEntry";
    var dataparams = {patient_id:patient_id};
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            if(data !=0){
                $('#usg_entry_history_container').html(data);
            }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function add_presenting_complaints_fav(){
    var txt = $('#presenting_complaints').val();
    txt = txt.trim();
    if(txt == ''){
        return false;
    }
    txt = btoa(txt);
    var base_url = $("#base_url").val();
    var url = base_url + "/gynecology_lite/addPresentingComplaintsFav";
    var dataparams = {txt:txt};
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
        },
        success: function (data) {
            if(data ==1){
                toastr.success("Added to favorite list!");
            }else{
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}

function list_presenting_complaints_fav(){
    var base_url = $("#base_url").val();
    var url = base_url + "/gynecology_lite/listPresentingComplaintsFav";
    $.ajax({
        type: "POST",
        url: url,
        data:'',
        beforeSend: function () {
            $('#presenting_complaints_fav_modal').modal('toggle');
            $("#presenting_complaints_modal_data").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#presenting_complaints_modal_data').html(data);
        },
        complete: function () {
            $("#presenting_complaints_modal_data").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}

function applyGynecPresentingFav(fav_text){
    var current = $('#presenting_complaints').val();
    var new_txt = current+' '+fav_text;
    $('#presenting_complaints').val(new_txt);
    $('#presenting_complaints_fav_modal').modal('hide');
}

function deleteGynecPresentingFav(id){
    var base_url = $("#base_url").val();
    var url = base_url + "/gynecology_lite/deleteGynecPresentingFav";
    var dataparams = {
        favid:id
    };
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {

        },
        success: function (data) {
           if(data == 1){
            toastr.success("fav list item deleted!");
            $('#prenting_cmp_fav_row_'+id).remove();
           }else{
            toastr.error("Error please check your internet connection");
           }
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}


function fetchUsgCurrentVisitResultsEntry(booking_id){
    var base_url = $("#base_url").val();
    var url = base_url + "/gynecology_lite/fetchUsgCurrentVisitResultsEntry";
    var dataparams = {booking_id:booking_id};
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $('#usg_entry_block').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if(data !=0){
                var obj = JSON.parse(data);

                if(obj.usg_results_entry_date !=''){
                    var usg_results_entry_date = moment(obj.usg_results_entry_date).format('DD MMM YYYY');
                    $('#usg_results_entry_date').val(usg_results_entry_date);
                }

                $('#pog_wk').val(obj.pog_wk);
                $('#pog_d').val(obj.pog_d);

                if(obj.siugs == 1){
                    $('#siugs').prop('checked', 'checked');
                }
                if(obj.sliug == 1){
                    $('#sliug').prop('checked', 'checked');
                }
                if(obj.slf == 1){
                    $('#slf').prop('checked', 'checked');
                }

                $('#slf_wk').val(obj.slf_wk);
                $('#slf_d').val(obj.slf_d);

                if(obj.ceph == 1){
                    $('#ceph').prop('checked', 'checked');
                }
                if(obj.breech == 1){
                    $('#breech').prop('checked', 'checked');
                }
                if(obj.transverse_lie == 1){
                    $('#transverse_lie').prop('checked', 'checked');
                }
                $('#afi').val(obj.afi);
                $('#efw').val(obj.efw);
                $('#usg_edd').val(obj.edd);
                $('#placenta').val(obj.placenta);
                $('#nt').val(obj.nt);
                $('#fhr').val(obj.fhr);
                $('#nb').val(obj.nb);

            }
        },
        complete: function () {
            $('#usg_entry_block').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}
function removeBeforeUnloadCallbak(){
    if(window.onbeforeunload != null){
        window.onbeforeunload = null;
    }
}
function saveDynamicTemplate(){
    console.log('in');
}

function fetchPatientLocalDetails(patient_id) {
    var patient_id;
    if (!patient_id) {
        patient_id = $('#patient_id').val();
    }

    var base_url = $("#base_url").val();
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/fetchPatientDetails";
    var params = { patient_id: patient_id, doctor_id: doctor_id };
    $('.dynamic_chief_complaints_textarea').val('');
    $.ajax({
        type: "POST",
        url: url,
        async:false,
        data: params,
        beforeSend: function () {
            $(".todays_visit_details_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".patient_combined_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".investigation_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $(".prescription_history_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            // console.log(data);
            var patient_details = data.patient_details;
            var history_editable = data.history_editable;

            var patient_basic_details = patient_details.patient_details;
            var patient_allergies = patient_details.patient_allergies;
            var patient_other_allergy = patient_details.patient_other_allergy;
            var personal_notes = patient_details.patient_private_notes;
            showPatientDetails(patient_basic_details);
            var show_patient_reference = data.show_patient_reference;
            if (patient_details.patient_info.patient_image_url) {
                $("#patient_image_url").attr('src', patient_details.patient_info.patient_image_url)
            }
            $(".patient_details_patient_place").html("Area : " + patient_details.area);
            $(".patient_details_patient_address").html("Address : " + patient_details.patient_info.address);
            $(".patient_details_patient_district").html("District : " + patient_details.patient_info.district);
            $(".patient_details_patient_company").html("Company : " + patient_details.patient_info.company_name);
            $(".patient_details_patient_pricing").html("Pricing : " + patient_details.patient_info.pricing_name);
            $(".patient_details_patient_phone").html("Mobile : " + patient_details.patient_info.phone);
            if (show_patient_reference == '1') {
                $(".patient_details_co_type").html("C/O Type : " + patient_details.patient_info.co_type);
                $(".patient_details_co_name").html("C/O Name : " + patient_details.patient_info.co_name);
                $(".patient_details_co_mobile").html("C/O Mobile : " + patient_details.patient_info.co_mobile);
            } else {
                $(".patient_details_patient_covid_history").html("Covid History : " + patient_details.patient_info.covid_history);
                $(".patient_details_patient_vaccine_type").html("Type of vaccine : " + patient_details.patient_info.vaccine_name);
            }
            var formdata = patient_details.formdata;
            showPatientFormData(formdata);
            showPatientAllergies(patient_allergies, patient_other_allergy);
            $(".private_notes_textarea").text(personal_notes);
            var patient_vitals = patient_details.patient_vitals;
            // showLatestPatientVitals(patient_vitals[0]);
            var patient_medications = patient_details.patient_medication;
            var patient_medications_old = patient_details.patient_medications_old;
            showPatientMedicationHistory(patient_medications, patient_medications_old, history_editable);
            var patient_investigations = patient_details.patient_investigation;
            // showPatientInvestigationHistory(patient_investigations);
            $(".investigation_history_div").html(patient_details.investigation_details);
            var patient_combined_history = patient_details.patient_combined_history ? patient_details.patient_combined_history : "";
            showPatientCombinedHistory(patient_combined_history);
            var patient_todays_visit_details = patient_details.patient_todays_visit_details;
            $(".todays_visit_details_div").html(patient_todays_visit_details);
            $(".chief_complaint").html('');
            $(".investigation_item_search_textbox").val('');
            $(".assessmentFavButton").show();
            $('#chief_com').focus();


            if (patient_details.clinical_data_change == 1) {
                $("li[rel='tab_1']").click(); // navigating to notes tab
            }

        },
        complete: function () {
            $(".todays_visit_details_div").LoadingOverlay("hide");
            $(".patient_combined_history_div").LoadingOverlay("hide");
            $(".investigation_history_div").LoadingOverlay("hide");
            $(".prescription_history_div").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


// FOR ASSESSMENT
$(document).on("click", ".printGynecAssessmentButton", function () {
    var that = this;
    var assesment_head_id = $(this).parents('.gynec_assessment_history_item').attr('gynec-data-assessment-head-id');
    printGynecAssessment(assesment_head_id, that);
});

function printGynecAssessment(assesment_head_id, obj) {

    if (assesment_head_id != '' && assesment_head_id != undefined) {
        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        $.ajax({
            type: "POST",
            url: url + "/gynecology_lite/printGynecAssessment",
            data: {
                assesment_head_id: assesment_head_id,
                patient_id: patient_id,
            },
            beforeSend: function () {
                $(obj).removeClass('fa-print printBtn').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                var response = '';
                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }
                var winCaPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winCaPrint.document.write(response + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

            },
            complete: function () {
                $(obj).removeClass('fa-spinner fa-spin').addClass('fa-print printBtn');
            }
        });
    }
}

$(document).on('click', '.deleteGynecAssessmentButtonBtn', function () {
    var that = this;
    var patient_id = $("#patient_id").val();
    var assesment_head_id = $(this).parents('.gynec_assessment_history_item').attr('gynec-data-assessment-head-id');
    bootbox.confirm({
        message: "Are you sure want to delete the assessment ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/gynecology_lite/deleteGynTemplateEntryData";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        assesment_head_id: assesment_head_id,
                        patient_id: patient_id
                    },
                    beforeSend: function () {
                        $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Command: toastr["success"]("Success");
                            $(that).parents('.gynec_assessment_history_item').remove();
                        } else {
                            Command: toastr["error"]("Error please check your internet connection");
                        }

                    },
                    complete: function () {
                        $('body').LoadingOverlay("hide");
                    }
                });
            }
        }
    });
});

$(document).on('click', '.copyGynecAssessmentButtonBtn', function () {
    var that = this;
    var patient_id = $("#patient_id").val();
    var assesment_head_id = $(this).parents('.gynec_assessment_history_item').attr('gynec-data-assessment-head-id');
    fetchGynecologyAssessmentDetails(that, assesment_head_id);
});

$(document).on('click', '.editGynecAssessmentButtonBtn', function () {
    var that = this;
    var patient_id = $("#patient_id").val();
    var assesment_head_id = $(this).parents('.gynec_assessment_history_item').attr('gynec-data-assessment-head-id');
    fetchGynecologyAssessmentDetails(that, assesment_head_id, 1);
    $('#assessment_head_id').val(assesment_head_id);
});

$(document).on('click', '.canceGynecAssessmentEditBtn', function () {
    $('.updateGynecAssessmentBtnDiv').hide();
    $('#presenting_complaints').val('');
    $('.chief_complaint_div').html('');
    $('#local_examinations').val('');
    $('#usg').val('');
    $('#provisional_diagnosis').val('');
    $('#plan_of_care').val('');
    $('#follow_up_care').val('');
    $('#special_care').val('');
    $('#assessment_head_id').val('');
    $(".saveButtonDiv").show();
});

function fetchGynecologyAssessmentDetails(obj, assesment_head_id, btn_type = 0) {
    // btn_type 0 for copy; 1: for edit
    var url = $('#base_url').val() + "/gynecology_lite/editGynecologycurrentVisitDetails";
    var dataparams = {assesment_head_id: assesment_head_id};
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            if (btn_type == 1) {
                $(obj).removeClass('fa-edit editBtn').addClass('fa-spinner fa-spin');
            } else {
                $(obj).removeClass('fa-copy copyBtn').addClass('fa-spinner fa-spin');
            }
            $('#presenting_complaints').val('');
            $('.chief_complaint_div').html('');
            $('#local_examinations').val('');
            $('#usg').val('');
            $('#provisional_diagnosis').val('');
            $('#plan_of_care').val('');
            $('#follow_up_care').val('');
            $('#special_care').val('');
        },
        success: function (data) {
            if (data !=0) {
                var obj = JSON.parse(data);
                $('#presenting_complaints').val(obj.presenting_complaints);
                $('.chief_complaint_div').html(obj.presenting_complaints);
                $('#local_examinations').val(obj.local_examinations);
                $('#usg').val(obj.usg);
                $('#provisional_diagnosis').val(obj.provisional_diagnosis);
                $('#plan_of_care').val(obj.plan_of_care);
                $('#follow_up_care').val(obj.follow_up_care);
                $('#special_care').val(obj.special_care);
            } else {
                $('#presenting_complaints').val('');
                $('.chief_complaint_div').html('');
                $('#local_examinations').val('');
                $('#usg').val('');
                $('#provisional_diagnosis').val('');
                $('#plan_of_care').val('');
                $('#follow_up_care').val('');
                $('#special_care').val('');
            }
        },
        error: function () {

        },
        complete: function () {
            if (btn_type == 1) {
                $(obj).removeClass('fa-spinner fa-spin').addClass('fa-edit editBtn');
                $('.updateGynecAssessmentBtnDiv').show();
                $(".saveButtonDiv").hide();
            } else {
                $(obj).removeClass('fa-spinner fa-spin').addClass('fa-copy copyBtn');
                $('.updateGynecAssessmentBtnDiv').hide();
                $(".saveButtonDiv").show();
            }
            $('.nav a[href="#assesment_block"]').tab('show');
        }
    });
}

$(document).on("click", '.updateGynecAssessmentBtn', function () {
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var doctor_id = $('#doctor_id').val();
    var encounter_id = $('#encounter_id').val();
    var presenting_complaints = $('#presenting_complaints').val();
    var local_examinations = $('#local_examinations').val();
    var usg = $('#usg').val();
    var provisional_diagnosis = $('#provisional_diagnosis').val();
    var plan_of_care = $('#plan_of_care').val();
    var follow_up_care = $('#follow_up_care').val();
    var special_care = $('#special_care').val();
    var lmp = $('#lmp').val();
    var pmp = $('#pmp').val();
    var edd = $('#edd').val();
    var assesment_head_id = $('#assessment_head_id').val();

    if (patient_id == '') {
        toastr.warning("Select patient!");
        return false;
    }

    var dataparams = {
        'patient_id':patient_id,
        'visit_id':visit_id,
        'doctor_id':doctor_id,
        'encounter_id':encounter_id,
        'presenting_complaints':presenting_complaints,
        'local_examinations':local_examinations,
        'usg':usg,
        'provisional_diagnosis':provisional_diagnosis,
        'plan_of_care':plan_of_care,
        'follow_up_care':follow_up_care,
        'special_care':special_care,
        'lmp':lmp,
        'pmp':pmp,
        'edd':edd,
        'assesment_head_id' : assesment_head_id,
    };

    var url = base_url + "/gynecology_lite/updategynecologyLiteAssessment";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data == 1) {
                toastr.success("Updated successfully!");
                $('.updateGynecAssessmentBtnDiv').hide();
                $('#assessment_head_id').val('');
                $(".saveButtonDiv").show();
            } else {
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            fetchGynecologyDetails(patient_id);
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
})
$(document).on("click", ".patient_visit_history_list_btn", function (e) {
    e.preventDefault();
    var patient_id = $("#patient_id").val();
    if(!patient_id){
        Command: toastr["warning"]("Please select patient!");
        return false;
    }
    fetchPatientVisitHistory();

});

$(document).on("click", ".visit_check_all", function (e) {
    if($(this).prop('checked') ) {
        $(".visit_history_data_table").find('.visit_check_item').prop('checked', true);
        $(".print_all_visit_history_btn").prop('disabled', false);
    } else {
        $(".visit_history_data_table").find('.visit_check_item').prop('checked', false);
        $(".print_all_visit_history_btn").prop('disabled', true);
    }
});

$(document).on("click", ".visit_check_item", function (e) {
    e.stopPropagation();
    var checked = 0;
    $('.visit_check_item').each(function (key, val) {
        if ($(val).is(":checked") == true) {
            checked = 1;
            return false;
        }
    })
    if (checked) {
        $(".print_all_visit_history_btn").prop('disabled', false);
    } else {
        $(".print_all_visit_history_btn").prop('disabled', true);
    }
});

$(document).on("click", ".visit_item_clinical_check", function (e) {
    e.stopPropagation();
});

$(document).on("click", ".visit_item_vital_check", function (e) {
    e.stopPropagation();
});

$(document).on("click", ".visit_item_investigation_check", function (e) {
    e.stopPropagation();
});

$(document).on("click", ".visit_item_prescription_check", function (e) {
    e.stopPropagation();
});

$(document).on("click", ".visit_history_parent_content", function (e) {
    if($(this).next('tr.visit_history_child_content').css('display') == 'none') {
        $(this).next('tr.visit_history_child_content').show();
    } else {
        $(this).next('tr.visit_history_child_content').hide();
    }
});

$(document).on("click", ".print_all_visit_history_btn", function (e) {
    e.stopPropagation();
    var patient_id = $("#patient_id").val();
    var print_items = [];
    $('.visit_history_parent_content').each(function () {
        var selected_items = {};
        if ($(this).find('.visit_check_item').is(':checked')) {
            var visit_id = $(this).find(".print_visit_history_btn").attr("data-visit-id");
            var vital_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_vital_check').is(':checked');
            var clinical_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_clinical_check').is(':checked');
            var investigation_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_investigation_check').is(':checked');
            var prescription_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_prescription_check').is(':checked');
            selected_items.visit_id = visit_id;
            selected_items.vital_check = (vital_check == true) ? 1 : 0;
            selected_items.clinical_check = (clinical_check == true) ? 1 : 0;
            selected_items.investigation_check = (investigation_check == true) ? 1 : 0;
            selected_items.prescription_check = (prescription_check == true) ? 1 : 0;
            print_items.push(selected_items);
        }
    });

    printPatientVisitHistory(patient_id, print_items);
});

$(document).on("click", ".print_visit_history_btn", function (e) {
    e.stopPropagation();
    var patient_id = $(this).attr("data-patient-id");
    var visit_id = $(this).attr("data-visit-id");
    var print_items = [];
    var selected_items = {};
    var vital_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_vital_check').is(':checked');
    var clinical_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_clinical_check').is(':checked');
    var investigation_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_investigation_check').is(':checked');
    var prescription_check = $('#visit_history_child_content_'+visit_id).find('.visit_item_prescription_check').is(':checked');
    selected_items.visit_id = visit_id;
    selected_items.vital_check = (vital_check == true) ? 1 : 0;
    selected_items.clinical_check = (clinical_check == true) ? 1 : 0;
    selected_items.investigation_check = (investigation_check == true) ? 1 : 0;
    selected_items.prescription_check = (prescription_check == true) ? 1 : 0;
    print_items.push(selected_items);
    // printPatientVisitHistory(patient_id, visit_id);
    printPatientVisitHistory(patient_id, print_items);
});

$(document).on("click", ".visit_history_child_div", function (e) {
    if($(this).find('input[name="visit_history_child_div_check"]').prop('checked') ) {
        $(this).find('input[name="visit_history_child_div_check"]').prop('checked', false);
    } else {
        $(this).find('input[name="visit_history_child_div_check"]').prop('checked', true);
    }
});

// function printPatientVisitHistory(patient_id, visit_id){
function printPatientVisitHistory(patient_id, print_items){
    var url = $('#base_url').val() + "/emr_lite/printPatientVisitHistory";
    let _token = $('#c_token').val();
    $(".print_visit_history_btn").attr('disabled', true);
    var request_data = {};
    request_data.patient_id = patient_id;
    // request_data.visit_id = visit_id;
    request_data._token = _token;
    request_data.print_items = JSON.stringify(print_items);
    $.ajax({
        type: "POST",
        url: url,
        data: request_data,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            var mywindow = window.open('', 'Print Visit Report', 'height=3508,width=2480');
            mywindow.document.write("<style>.blank-row { background: #000 !important; }</style>" + html);
            mywindow.document.write('<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
            $(".print_visit_history_btn").attr('disabled', false);
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }
    });
}


    function fetchPatientVisitHistory(patient_id){
      var patient_id = $("#patient_id").val();
      var visit_date = $("#visit_date_emr_history").val();
      var doctor_id = $("#visit_history_doctor_id").val();
      var url = $('#base_url').val() + "/emr_lite/fetchPatientVisitHistory";
      var param = { patient_id: patient_id, visit_date: visit_date, doctor_id:doctor_id };
      if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                 $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
            },
                 success: function (data) {
                 console.log(data);
                 if (data != '') {
                        $("#patient_visit_history_modal_content").html(data);
                        $("#patient_visit_history_modal").modal('show');
                } else {
                       Command: toastr["warning"]("No Data Found");
                }
                  $('.theadscroll').perfectScrollbar({
                      wheelPropagation: true,
                       minScrollbarLength: 30
                });
                setTimeout(function () {
                  $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                 });
                 }, 400);
                   },
                   complete: function () {
                     $('body').LoadingOverlay("hide");
                 }
                 });
               }
         }


         $('.global_patient_search_textbox').keyup(function (event) {
            var keycheck = /[a-zA-Z0-9 ]/;
            var value = event.key;
            var current;
            if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                var op_no_search = $(this).val();
                op_search = op_no_search.trim();
                if (op_search == "") {
                    $("#AjaxDiv").html("");
                    $("#patient_name_hidden").val("");
                } else {
                    try {
                        var url = $('#base_url').val() + "/emr_lite/ajaxSearchData";
                        var param = {
                            op_no_search: op_no_search,
                            op_no_search_prog: 1
                        };
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: param,
                            beforeSend: function () {
                                $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                            },
                            success: function (html) {
                                $("#AjaxDiv").html(html).show();
                                $("#AjaxDiv").find('li').first().addClass('liHover');
                            },
                            complete: function () {}
                        });
                    } catch (err) {
                        document.getElementById("demo").innerHTML = err.message;
                    }
                }
            } else {
                ajaxProgressiveKeyUpDown('AjaxDiv', event);
            }
        });

        function fillGlobalPatientData(id, uhid, patient_name, input_id) {
            $('#AjaxDiv').hide();
            $(".op_patients_list_table_body").empty();
            $(".global_patient_search_textbox").val('');
            $(".op_patients_list_table_body").append('<tr class="ip_op_list_item col-md-12 no-padding" data-patient-id="' + id + '" data-booking-id="0"><td><div class="col-md-10 padding_sm"><div class="list_item_name">' + patient_name + '</div><div class="list_item_uhid">' + uhid + '</div></div></td><td></td></tr>');
            showHidePatientFilters(2);
            $(".op_patients_list_table_body tr:first").trigger('click');
        }
        function getgynechistory(h_type) {
            var base_url = $("#base_url").val();
            var url = base_url + "/gynecology_lite/getGynecHistory";
            var patient_id = $('#patient_id').val();
            if (patient_id == '') {
                toastr.warning("Select patient!");
                return false;
            }
            var dataparams = {
                patient_id: patient_id,
                h_type: h_type,
            }
            $.ajax({
                type: "POST",
                url: url,
                data: dataparams,
                beforeSend: function () {
                    $('#gynecology_history_modal_data').html('');
                    $("#gynecology_history_modal_data").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    $("#gynecology_history_modal").modal("show");
                },
                success: function (data) {
                    var $table = $('table.theadfix_wrapper');

                    $table.floatThead({
                        scrollContainer: function ($table) {
                            return $table.closest('.theadscroll');
                        }

                    });

                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                    if (data != '') {
                        $('#gynecology_history_modal_data').html(data);
                    }
                },
                complete: function () {
                    $("#gynecology_history_modal_data").LoadingOverlay("hide");
        
                },
                error: function () {
                    toastr.error("Error please check your internet connection");
                },
            });
        
        }
        function getDoctorNotesModel() {
            var patient_id = $("#patient_id").val();
            var doctor_id = $("#doctor_id").val();
            var visit_id = $("#visit_id").val();
             $('#nursingnotes_patientid').val(patient_id);
            $('#nursingnotes_doctorid').val(doctor_id);
            $('#nursingnotes_visitid').val(visit_id);
            getDoctorNotes(patient_id, doctor_id, 1);
        }
