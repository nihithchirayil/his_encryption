$(document).on("click", ".addAllergies", function () {
    getPatientMedicineAlergy()
});
$(document).on("click", ".addVitals", function () {
    addPatientVitals(1);
});
$(document).on("click", ".editVitals", function () {
    addPatientVitals(2);
});
$(document).on("click", ".historyVitals", function () {
    show_vital_history();
});
$(document).on("click", ".addNewBatchBtn", function () {
    $('.getPatientVitalModelHeader').html("Add Vitals");
    $(".vital_form_container").find('input[type="text"]').val('');
    $('#add_new_vital_batch').val(1);
    $("#time_taken").val(moment().format('MMM-DD-YYYY hh:mm a'));
});

$('#getPatientVitalModel').on('hidden.bs.modal', function () {
    $('#add_new_vital_batch').val(0);
})

var vital_edit_type = 0;
function getAllPatientAllergy(patient_allegies, from_type) {
    if (parseInt(from_type) == 1) {
        $.each(patient_allegies.generic_allergies, function (index, value) {
            addMedicneAllergy(1, value.generic_id, value.generic_name);
        });
        $.each(patient_allegies.medicine_allergies, function (index, value) {
            addMedicneAllergy(2, value.item_code, value.item_desc);
        });
        $.each(patient_allegies.other_allergies, function (index, value) {
            addOtherAllergy(value.allergy_id, value.allergy_name);
        });
    } else if (parseInt(from_type) == 2) {
        item_allergyArray = [];
        generic_allergyArray = [];
        $(".allergy_list").html('');
        $.each(patient_allegies.medicine_allergies, function (index, value) {
            // allergy = '<span class="blue"> Brand : </span> ' + value.item_desc + '<br>';
            allergy = value.item_desc + '<br>';
            item_allergyArray.push(value.item_code);
            $(".allergy_list").append('<span class="allergic_item" data-allergic-generic-id="' + value.item_code + '" data-allergic-id="' + value.item_code + '" data-allergic-name="' + value.item_desc + '">' + allergy + '</span>');
        });
        $.each(patient_allegies.generic_allergies, function (index, value) {
            // allergy = '<span class="blue"> Generic : </span> ' + value.generic_name + '<br>';
            allergy = value.generic_name + '<br>';
            generic_allergyArray.push(parseInt(value.generic_id));
            $(".allergy_list").append('<span class="allergic_item" data-allergic-generic-id="' + value.generic_id + '" data-allergic-id="' + value.generic_id + '" data-allergic-name="' + value.generic_name + '">' + allergy + '</span>');
        });
        
        $.each(patient_allegies.other_allergies, function (index, value) {
            // allergy = '<span class="blue"> Other : </span> ' + value.allergy_name + '<br>';
            allergy = value.allergy_name + '<br>';
            $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + value.allergy_id + '">' + allergy + '</span>');
        });
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });
    }
}


function getPatientMedicineAlergy() {
    var patient_id = $('#patient_id').val();
    if (patient_id) {
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/getPatientMedicineAllergy";
        var param = { patient_id: patient_id };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#getPatientAllegryModelHeader').html("Allergy");
                $('.addAllergies').attr('disabled', true);
                $('.addAllergies').removeClass('fa fa-plus-circle');
                $('.addAllergies').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                $("#getPatientAllegryModelDiv").html(obj.data);
                getAllPatientAllergy(obj.patient_allegies, 1);
                $("#getPatientAllegryModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            complete: function () {
                $('.addAllergies').attr('disabled', false);
                $('.addAllergies').removeClass('fa fa-spinner fa-spin');
                $('.addAllergies').addClass('fa fa-plus-circle');
                $('#patientMedicneAllergytddata').html('');
                addMedicneAllergy();
                addOtherAllergy();
            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    } else {
        toastr.warning("Please select a patient");
    }
}

function addPatientVitals(from_type) {
    var patient_id = $('#patient_id').val();
    if (patient_id) {
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/addPatientVitals";
        var param = { patient_id: patient_id, from_type: from_type };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                if (parseInt(from_type) == 1) {
                    $('.addVitals').attr('disabled', true);
                    $('.addVitals').removeClass('fa fa-plus-circle');
                    $('.addVitals').addClass('fa fa-spinner fa-spin');
                } else if (parseInt(from_type) == 2) {
                    $('.editVitals').attr('disabled', true);
                    $('.editVitals').removeClass('fa fa-pencil-square');
                    $('.editVitals').addClass('fa fa-spinner fa-spin');
                }
            },
            success: function (data) {
                $("#getPatientVitalModelDiv").html(data);
                $("#getPatientVitalModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                $("#time_taken").datetimepicker({
                    format: 'MMM-DD-YYYY hh:mm a',
                    maxDate: new Date(),
                });

                if($("#getPatientVitalModelDiv").find('input[type="text"]:not([value=""])').length > 1){
                    $('.getPatientVitalModelHeader').html("Edit Vitals");
                    vital_edit_type = 2;
                } else {
                    vital_edit_type = 1;
                    $('.getPatientVitalModelHeader').html("Add Vitals");
                }
            },
            complete: function () {
                if (parseInt(from_type) == 1) {
                    $('.addVitals').attr('disabled', false);
                    $('.addVitals').removeClass('fa fa-spinner fa-spin');
                    $('.addVitals').addClass('fa fa-plus-circle');
                } else if (parseInt(from_type) == 2) {
                    $('.editVitals').attr('disabled', false);
                    $('.editVitals').removeClass('fa fa-spinner fa-spin');
                    $('.editVitals').addClass('fa fa-pencil-square');
                }
            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    } else {
        toastr.warning("Please select a patient");
    }
}

function calculateTemperature(from_type) {
    if (parseInt(from_type) == 1) {
        vital_value_c = $("#temperature_value").val();
        if (vital_value_c != '') {
            vital_value_f = ((vital_value_c * (9 / 5)) + 32).toFixed(2);
            $('#temperature_value_f').val(vital_value_f);
        } else {
            $('#temperature_value_f').val('');
        }
    } else if (parseInt(from_type) == 2) {
        vital_value_f = $("#temperature_value_f").val();
        if (vital_value_f != '') {
            vital_value_c = ((vital_value_f - 32) * (5 / 9)).toFixed(2);
            $('#temperature_value').val(vital_value_c);
        } else {
            $('#temperature_value').val('');
        }
    }
}

$(document).on("keyup", "#weight_value,#height_value", function () {
    let w_val = '';
    let w_type = '';
    let h_val = '';
    let h_type = '';

    w_val = $('#weight_value').val();
    h_val = $('#height_value').val();
    if (h_val == "") {
        h_val = $('#height_value').val();
    }
    w_type = $('#weight').val();
    h_type = $('#height').val();

    if (w_val != '' && h_val != '' && w_type != '') {
        calculateBmi(w_val, w_type, h_val, h_type);
    }
});

function calculateBmi(weight, wtype, height, htype) {

    let weight_type = "";
    let weight_kg = "";
    weight_type = wtype;
    if (weight_type == "1") {
        weight_kg = Math.round((parseFloat(weight) / 2.2046));
    } else {
        weight_kg = Math.round(parseFloat(weight));
    }

    let height_type = "";
    let height_cm = "";
    let height_m = "";
    height_type = htype;
    if (height_type == "3") {
        let vital_value_feet = height;

        height_m = parseFloat(vital_value_feet) / 3.2808;

    } else {
        height_cm = parseFloat(height);
        height_m = parseFloat(height_cm / 100);//convert to meter
    }

    if (weight_kg != '' && height_m != '') {
        let bmi_val = '';
        bmi_val = parseFloat(weight_kg / (height_m * height_m));
        $('input[name="bmi"]').val(bmi_val.toFixed(2));
    }

}

var allergy_cnt = 1;

function removePatientMedicneAllergy(row_cnt) {
    $('#patientMedicneAllergyRow' + row_cnt).remove();
    serialNoCalculation('allergy_class', 'allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}

function removePatientOtherAllergy(row_cnt) {
    $('#patientOtherAllergyRow' + row_cnt).remove();
    serialNoCalculation('other_allergy_class', 'other_allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}

function fetchPatientVisit() {
    var visit_id = $("#visit_id").val();
    var patient_id = $('#patient_id').val();
    // alert(patient_id);
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/fetchPatientVitals";
    var params = { visit_id: visit_id, patient_id: patient_id };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".vital_list").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            showLatestPatientVitals(data);
            $('#vital_batch_no').val(data.batch_no ? data.batch_no : '');
            $('#encounter_id').val(data.encounter_id ? data.encounter_id : 0);
        },
        complete: function () {
            $(".vital_list").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


function addMedicneAllergy(search_by = '', item_code = '', item_desc = '') {
    $search_brand = '';
    $search_generic = '';
    if (parseInt(search_by) == 1) {
        $search_generic = "selected=''";
    } else if (parseInt(search_by) == 2) {
        $search_brand = "selected=''";
    }
    $(".patientMedicneAllergytddata").append("<tr id='patientMedicneAllergyRow" + allergy_cnt + "' class='allergy_class'><td style='text-align: center; line-height: 25px;' class='allergy_count_class'></td><td><select class='form-control' id='genderic_brand" + allergy_cnt + "' name='genderic_brand'><option " + $search_generic + " value='1'>Generic</option><option " + $search_brand + " value='2'>Brand</option></select</td><td><input name='allergy_medicine_code' value='" + item_code + "' id='allergy_medicine_code" + allergy_cnt + "' type='hidden'><input  value='" + item_desc + "' onkeyup='searchPatientAllergyMedicne(this.id,event," + allergy_cnt + ")' type='text' autocomplete='off' name='allergy_name' id='allergy_name" + allergy_cnt + "' class='form-control bottom-border-text borderless_textbox allergy_name'/><div class='ajaxSearchBox dropdown_div' style='width: 66%' id='ajaxSearchBox" + allergy_cnt + "'></div></td><td class='common_td_rules deletePrescriptionItemBtn'><i  onclick='removePatientMedicneAllergy(" + allergy_cnt + ")' class='fa fa-times red'></i></td></tr>");
    $('#allergy_name' + allergy_cnt).focus();
    allergy_cnt++;
    serialNoCalculation('allergy_class', 'allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}


function addOtherAllergy(allergy_id = '', allergy_name = '') {
    $(".patientOtherAllergytddata").append("<tr id='patientOtherAllergyRow" + allergy_cnt + "' class='other_allergy_class'><td style='text-align: center; line-height: 25px;' class='other_allergy_count_class'></td><td><input name='other_allergy_code' id='other_allergy_code" + allergy_cnt + "' value='" + allergy_id + "' type='hidden'><input value='" + allergy_name + "' onkeyup='searchPatientOtherAllergy(this.id,event," + allergy_cnt + ")' type='text' autocomplete='off' name='other_allergy_name' id='other_allergy_name" + allergy_cnt + "' class='form-control bottom-border-text borderless_textbox allergy_name'/><div style='width: 90%;' class='ajaxSearchBox dropdown_div' id='ajaxSearchBox" + allergy_cnt + "'></div></td><td class='common_td_rules deletePrescriptionItemBtn'><i  onclick='removePatientOtherAllergy(" + allergy_cnt + ")' class='fa fa-times red'></i></td></tr>");
    $('#other_allergy_name' + allergy_cnt).focus();
    allergy_cnt++;
    serialNoCalculation('other_allergy_class', 'other_allergy_count_class');
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
    setTimeout(function () {
        $(".theadfix_wrapper").floatThead({
            position: "absolute",
            scrollContainer: true,
        });
    }, 400);
}

function validateMedicineAllergy() {
    var patient_allergy = new Array();
    $('#patientMedicneAllergyTable tr').each(function () {
        var allergy_code = $(this).find("input[name='allergy_medicine_code']").val();
        var genderic_brand = $(this).find("select[name='genderic_brand']").val();
        if (parseInt(genderic_brand) == 1) {
            patient_allergy.push({
                'allergy_code': allergy_code,
                'allergy_type': 1,
            });
        } else if (parseInt(genderic_brand) == 2) {
            patient_allergy.push({
                'allergy_code': allergy_code,
                'allergy_type': 2,
            });
        }
    });
    $('#patientOtherAllergyTable tr').each(function () {
        var allergy_code = $(this).find("input[name='other_allergy_code']").val();
        if (allergy_code) {
            patient_allergy.push({
                'allergy_code': allergy_code,
                'allergy_type': 3,
            });
        }
    });
    return patient_allergy;
}

function savePatientAllergy() {
    var patient_id = $('#patient_id').val();
    var allergy_data = JSON.stringify(validateMedicineAllergy());
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/savePatientAllergy";
    var param = { patient_id: patient_id, allergy_data: allergy_data };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#savePatientAllergyBtn').attr('disabled', true);
            $('#savePatientAllergySpin').removeClass('fa fa-save');
            $('#savePatientAllergySpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (parseInt(obj.status) == 1) {
                getAllPatientAllergy(obj.patient_allegies, 2);
                toastr.success('Allergy successfully added');
                $('#getPatientAllegryModel').modal('toggle');
            } else {
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
            $('#savePatientAllergyBtn').attr('disabled', false);
            $('#savePatientAllergySpin').removeClass('fa fa-spinner fa-spin');
            $('#savePatientAllergySpin').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


function show_vital_history() {
    let patient_id = $('#patient_id').val();
    let lstVitalItems = $("#lstVitalItems").val();
    let from_date = $("#vital_graph_from_date").val();
    let to_date = $("#vital_graph_to_date").val();
    var base_url = $("#base_url").val();
    var url = base_url + "/emr/show_vital_history";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            patient_id: patient_id,
            vital_mster_id: lstVitalItems,
            from_date: from_date,
            to_date: to_date,
        },
        beforeSend: function () {
            $('.historyVitals').attr('disabled', true);
            $('.historyVitals').removeClass('fa fa-history');
            $('.historyVitals').addClass('fa fa-spinner fa-spin');
            $('#vital_his_table').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
            $('#vital_graph').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
        },
        success: function (data) {
            //console.log(data);
            // vital graph chart starts here
            var time_taken = data[9].time_taken;
            data.pop();
            // var datas = data;
            var chart = Highcharts.chart('vital_graph', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Patient Vitals Chart'
                },

                xAxis: {
                    type: 'datetime',
                    categories: time_taken,


                },
                yAxis: {
                    title: {
                        text: 'Vital Values'
                    },
                    labels: {
                        formatter: function () {
                            return this.value;
                        }
                    }
                },
                tooltip: {
                    split: true,
                },

                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        },
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: data
            });

            vital_history_table(patient_id, lstVitalItems, from_date, to_date);
            $("#vital_graph_modal").modal('show');
        },
        complete: function () {
            $('.historyVitals').attr('disabled', false);
            $('.historyVitals').removeClass('fa fa-spinner fa-spin');
            $('.historyVitals').addClass('fa fa-history');
        }
    });
}

function vital_history_table(patient_id, lstVitalItems, from_date, to_date) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/emr/show_vital_history_table",
        data: {
            patient_id: patient_id,
            history_table: 'table',
            vital_mster_id: lstVitalItems,
            from_date: from_date,
            to_date: to_date,
            _token: _token
        },
        beforeSend: function () {
        },
        success: function (data) {
            $('#vital_his_table').html(data);
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        },
    });
}



function savePatientVitals() {
    var patient_id = $('#patient_id').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/savePatientVitals";

    var vital_value = {};
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let time_taken = $('#time_taken').val();
    let add_new_vital_batch = $('#add_new_vital_batch').val();
    var vital_batch = '';
    if (parseInt(vital_edit_type) == 2 && add_new_vital_batch == 0) {
        vital_batch = $('#vital_batch_no').val();
    }

    if(add_new_vital_batch != 0){
        $('#add_new_vital_batch').val(0);
    }

    if ($("#weight").val() == 1) {
        if ($("#weight_value").val() != '') {
            vital_value["1"] = $("#weight_value").val();
            vital_value["2"] = (($("#weight_value").val()) / 2.205).toFixed(2);
        }
    }
    if ($("#weight").val() == 2) {
        if ($("#weight_value").val() != '') {
            vital_value["2"] = $("#weight_value").val();
            vital_value["1"] = (($("#weight_value").val()) * 2.205).toFixed(2);
        }
    }

    if ($("#temperature").val() == 9) {
        if ($("#temperature_value_f").val() != '') {
            vital_value["9"] = $("#temperature_value_f").val();
            vital_value["10"] = (($("#temperature_value_f").val() - 32) * (5 / 9)).toFixed(2);
        }
    }

    if ($("#temperature").val() == 10) {
        if ($("#temperature_value").val() != '') {
            vital_value["10"] = $("#temperature_value").val();
            vital_value["9"] = (($("#temperature_value").val() * (9 / 5)) + 32).toFixed(2);
        }
    }

    if ($("#height").val() == 3) {
        if ($("#height_value").val() != '') {
            vital_value["3"] = $("#height_value").val();
            vital_value["4"] = (($("#height_value").val()) * (30.48)).toFixed(2);
        }

    }
    if ($("#height").val() == 4) {
        if ($("#height_value").val() != '') {
            vital_value["4"] = $("#height_value").val();
            vital_value["3"] = ((($("#height_value").val()) / 30.48).toFixed(2));
            ;
        }

    }
    if ($("#bp_systolic").val() != '') {
        vital_value["5"] = $("#bp_systolic").val();
    }
    if ($("#bp_diastolic").val() != '') {
        vital_value["6"] = $("#bp_diastolic").val();
    }
    if ($("#pulse").val() != '') {
        vital_value["7"] = $("#pulse").val();
    }
    if ($("#respiration").val() != '') {
        vital_value["8"] = $("#respiration").val();
    }
    if ($("#temperature_location").val() != '') {
        vital_value["11"] = $("#temperature_location").val();
    }

    if ($("#oxygen").val() != '') {
        vital_value["12"] = $("#oxygen").val();
    }
    if ($("#head").val() != '') {
        vital_value["13"] = $("#head").val();
    }
    if ($("#bmi").val() != '') {
        vital_value["14"] = $("#bmi").val();
    }
    if ($("#spirometry").val() != '') {
        vital_value["20"] = $("#spirometry").val();
    }


    var vitals_length = Object.keys(vital_value).length;
    if (vitals_length < 2) {
        toastr.info('Enter patient vitals')
        return;
    }
    var param = {
        vital_array: vital_value,
        patient_id: patient_id,
        visit_id: visit_id,
        encounter_id: encounter_id,
        time_taken: time_taken,
        vital_batch: vital_batch
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#savePatientVitalsBtn').attr('disabled', true);
            $('#savePatientVitalsSpin').removeClass('fa fa-save');
            $('#savePatientVitalsSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (parseInt(obj.status) == 1) {
                showLatestPatientVitals(obj.patient_visits);
                $('#vital_batch_no').val(obj.patient_visits.batch_no ? obj.patient_visits.batch_no : '');
                toastr.success('Vitals successfully updated');
                $('#getPatientVitalModel').modal('toggle');
            }

        },
        complete: function () {
            $('#savePatientVitalsBtn').attr('disabled', false);
            $('#savePatientVitalsSpin').removeClass('fa fa-spinner fa-spin');
            $('#savePatientVitalsSpin').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


function searchPatientAllergyMedicne(id, event, row_id) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox' + row_id);
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var medicine_name = htmlDecode($('#' + id).val());
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/searchPatientAllergyMedicine";
        if (!medicine_name) {
            $('.ajaxSearchBox').hide();
            $("#allergy_medicine_code" + row_id).val('');
        } else {
            var genderic_brand = $('#genderic_brand' + row_id).val();
            var allergy_data = JSON.stringify(validateMedicineAllergy());
            var param = { medicine_name: medicine_name, allergy_data: allergy_data, genderic_brand: genderic_brand, row_id: row_id };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#ajaxSearchBox' + row_id).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $('#ajaxSearchBox' + row_id).html(html).show();
                    $('#ajaxSearchBox' + row_id).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxSearchBox' + row_id, event);
    }
}


function searchPatientOtherAllergy(id, event, row_id) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox' + row_id);
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var allergy_name = htmlDecode($('#' + id).val());
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/searchPatientOtherAllergy";
        if (!allergy_name) {
            $('.ajaxSearchBox').hide();
            $("#other_allergy_code" + row_id).val('');
        } else {
            var allergy_data = JSON.stringify(validateMedicineAllergy());
            var param = { allergy_data: allergy_data, allergy_name: allergy_name, row_id: row_id };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#ajaxSearchBox' + row_id).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $('#ajaxSearchBox' + row_id).html(html).show();
                    $('#ajaxSearchBox' + row_id).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxSearchBox' + row_id, event);
    }
}

function fillMedicineDetails(item_code, itemdesc, row_id) {
    $("#allergy_medicine_code" + row_id).val(item_code.trim());
    $("#allergy_name" + row_id).val(htmlDecode(itemdesc.trim()));
    $('.ajaxSearchBox').hide();
    if ($(".allergy_class:last").find("input[name='allergy_name']").val() != '') {
        addMedicneAllergy();
    }
}

function fillOtherAllergy(id, allergy, row_id) {
    $("#other_allergy_code" + row_id).val(id);
    $("#other_allergy_name" + row_id).val(htmlDecode(allergy.trim()));
    $('.ajaxSearchBox').hide();
    if ($(".other_allergy_class:last").find("input[name='other_allergy_name']").val() != '') {
        addOtherAllergy();
    }
}




function show_vital_history_by_vital_master(vital_master_id) {
    let patient_id = $('#patient_id').val();
    let lstVitalItems = $("#lstVitalItems").val();
    let from_date = $("#vital_graph_from_date").val();
    let to_date = $("#vital_graph_to_date").val();
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/showVitalHistory";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            patient_id: patient_id,
            vital_mster_id: vital_master_id,
            from_date: from_date,
            to_date: to_date,
        },
        beforeSend: function () {
            $('#vital_graph_individual').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
        },
        success: function (data) {
            //console.log(data);
            // vital graph chart starts here
            var time_taken = data[9].time_taken;
            data.pop();
            // var datas = data;
            var chart = Highcharts.chart('vital_graph_individual', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Patient Vitals Chart'
                },

                xAxis: {
                    type: 'datetime',
                    categories: time_taken,


                },
                yAxis: {
                    title: {
                        text: 'Vital Values'
                    },
                    labels: {
                        formatter: function () {
                            return this.value;
                        }
                    }
                },
                tooltip: {
                    split: true,
                },

                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        },
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: data
            });

            // $("#vital_graph_modal").modal('show');
        },
        complete: function () {
            // $('.historyVitals').attr('disabled', false);
            // $('.historyVitals').removeClass('fa fa-spinner fa-spin');
            // $('.historyVitals').addClass('fa fa-history');
        }
    });
}

$(document).on("click", ".addOtherAllergyBtn", function(){
    var row_id = $(this).attr('data-row-id');
    var allergy_name = $(this).parents('td').find('input[name="other_allergy_name"]').val();
    $(".other_allergy_description").html(allergy_name);
    $("#other_allergy_save_modal").modal('show');
    $("#other_allergy_save_modal").attr('data-row-id', row_id);
});

function saveOtherAlleryMaster(){
    var allergy_name = $(".other_allergy_description").html();
    var allergy_type = $('select[name="other_allergy_type"]').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/saveOtherAlleryMaster";

    var param = { allergy_name: allergy_name, allergy_type: allergy_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('.saveOtherAlleryMasterBtn').find('i').removeClass('fa-save').addClass('fa-spinner fa-spin');
        },
        success: function (data) {
            if(data.status == 1){
                $("#other_allergy_save_modal").modal('hide');
                var row_id = $("#other_allergy_save_modal").attr('data-row-id');
                $("#ajaxSearchBox"+row_id).hide();
                $("#other_allergy_code"+row_id).val(data.allergy_id);
                addOtherAllergy();
            } else {
                toastr.error("Error Please Check Your Internet Connection");
            }
        },
        complete: function () {
            $('.saveOtherAlleryMasterBtn').find('i').removeClass('fa-spinner fa-spin').addClass('fa-save');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}
