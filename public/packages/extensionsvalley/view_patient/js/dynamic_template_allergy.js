function fillAllergyItemValues(list, item_code, item_desc, chemical_name, stock, alergy_type) {
    alergy_type = $('input[name="sel_t"]:checked').val();
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    console.log(itemCodeTextId);
    $('#' + itemCodeTextId).val('(' + $.trim(chemical_name) + ') ' + item_desc);
    $('#' + itemCodeListDivId).hide();
    $('#' + itemCodeTextId).closest("tr").find("input[name='alergylist_medicine_code_hidden[]']").val(item_code);
    $('#' + itemCodeTextId).closest("tr").find("input[name='alrg_type[]']").val(alergy_type);
    $(list).parents('tr').next().find('input[type=text]').focus();
}

function searchMedicine2(id, event) {
    // alert('fg');
    var curr_meds = [];

    var alrgy_type = $("input:radio[name='sel_t']:checked").val();


    $('input[name="alergylist_medicine_code_hidden[]"]').each(function () {
        if ($(this).val() != '') {
            curr_meds.push($(this).val());
        }
    });

    $('input[name="alrg_type[]"]').each(function () {
        if ($(this).val() != '') {
            curr_meds.push($(this).val());
        }
    });

    var myJsonMeds = JSON.stringify(curr_meds);
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    var ajaxHiddenId = $('#' + id).next().next().attr('id');
    var data = $('#' + id).val();
    if (data == "") {
        $('#' + ajaxHiddenId).val('');
    }
    if (value.match(keycheck)) {

        var medicine_code = $('#' + id).val();
        medicine_code = medicine_code.trim();
        //alert(ajax_div); return;
        if (medicine_code == "") {
            $("#" + ajax_div).html("");
            $("#" + ajaxHiddenId).val("");
        } else {
            var url = $('#base_url').val() + "/emr/getallergyitems";
            $.ajax({
                type: "GET",
                url: url,
                data: { search_key: medicine_code, curr_med: myJsonMeds, alrgy_type: alrgy_type },
                beforeSend: function () {
                    //$("#inbox_area").html("");
                    // $('#loading_image').show();
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    //alert(html); return;
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    //  $('#loading_image').hide();
                }
            });
        }

    } else {
        ajax_list_key_down(ajax_div, event);
    }

    /* setting for enter key press in ajaxDiv listing */
    $("#" + id).on('keydown', function (event) {
        if (event.keyCode === 13) {
            if ($("#" + ajax_div).html() != "") {
                $("#" + ajax_div).show();
                ajaxlistenter(ajax_div);
                return false;
            } else {
                ajaxlistenter(ajax_div);
                return false;
            }
        }
    });
}

function addRow() {

    var table = document.getElementById("allergymedicTable").getElementsByTagName('tbody')[0];
    var countRow = document.getElementById("allergymedicTable").rows.length;
    countRow = countRow + 1;
    if (countRow == 0) {
        countRow = 1;
    }

    var row = table.insertRow(table.rows.length);
    var cell0 = row.insertCell(0);
    var cell1 = row.insertCell(1);
    cell0.setAttribute("width", "90%;");

    cell0.innerHTML = "<input type='text' class='form-control bottom-border-text searchMedicine2 btn-without-border saveMedicationAllergyNewItem' placeholder='' value='' autocomplete='off' " +
        " onKeyup='searchMedicine2(this.id,event)' name='allergymedicine[]' id='allergymedicine-" + countRow + "'>" +

        "<div class='ajaxSearchBox' id='alergymedicineAjaxList-" + countRow + "' style='width:100%;max-height: 515px; display:none;'>" +
        "</div><input type='hidden' id='AllergymedCodeId-" + countRow + "' class='allergyNewItemHidden' name='alergylist_medicine_code_hidden[]' >" +
        " <input type='hidden' name='alrg_type[]' id='alrg_type-" + countRow + "'> ";

    // cell1.innerHTML = "<div style='text-align:left;margin-right: 3px;'><a tabindex='-1' " +
    //     "id='delete-" + countRow + "' class='btn btn-danger'  data-toggle='tooltip' data-placement='left'" +
    //     "  onclick='deleteNews(this);' ><i class='fa fa-times-circle' aria-hidden='true'></i></a></div>";
    cell1.innerHTML = "<i class='fa fa-times-circle' aria-hidden='true' style='/*color:#FFFF;*/font-size: 23px; margin: 7px;' onclick='deleteNews(this)' id='delete-" + countRow + "'></i>";
    $('#allergymedicine-' + countRow).focus();

    $('[data-toggle="popover"]').popover();
    $("#allergy_Scroll").animate({ scrollTop: $('#allergy_Scroll').prop("scrollHeight") }, 1000);
}

function deleteNews(x) {

    var DelIndex_code = $(x).closest('tr').find('input[name="alergylist_medicine_code_hidden[]"]').val();
    var alerg_type = $(x).closest('tr').find('input[name="alrg_type[]"]').val();
    var id = $(x).closest('tr').find('input[name="saved_hidden_id[]"]').val();

    if (id != '') {
        if (confirm("Are you sure you want to delete this?")) {

            var DelIndex = x.closest('tr').rowIndex;
            var table = document.getElementById("allergymedicTable");
            table.deleteRow(DelIndex);

            var patient_id = $('#patient_id').val();
            var url = $('#base_url').val() + "/emr/saveallergyprescription";
            var token = $('#formallergyprescription').find('input[name="_token"]').val();
            $.ajax({
                url: url,
                data: '_token=' + token + '&alerg_type=' + alerg_type + '&patient_id=' + patient_id + '&id=' + id + '&delIndex_code=' + DelIndex_code,
                async: true,
                type: 'post',
                //dataType: "json",
                success: function (html) {
                    if (html >= 1) {
                        Command: toastr["success"]("Deleted Successfully");
                        fetch_all_allergy(patient_id);
                    }
                }
            });
        }

    } else {
        if (confirm("Are you sure you want to delete this?")) {

            //var DelIndex = x.parentNode.parentNode.parentNode.rowIndex;
            var DelIndex = x.closest('tr').rowIndex;
            var table = document.getElementById("allergymedicTable");
            table.deleteRow(DelIndex);

        }
    }
}