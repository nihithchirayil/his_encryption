function addNewInvestigation(row) {

    var that = row;
    var investigation_type = $(".investigation_type").val();
    var base_url = $("#base_url").val();
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/addNewInvestigationGroups";


        var params = { investigation_type:investigation_type, doctor_id: doctor_id };
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            beforeSend: function () {

                    $('#addNewInvestigation').attr('disabled', true);
                    $(that).find('i').removeClass('fa fa-star');
                    $(that).find('i').addClass('fa fa-spinner fa-spin');
                    $('#addItemtoFavouriteModelHeader').html('Investigation Favourites');

            },
            success: function (data) {
                $('#addItemtoFavouriteModelDiv').html(data);
                $('#addfovouritesmodal').css('max-width','665px');
                $("#addItemtoFavouriteModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            complete: function () {
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                    $('#addNewInvestigation').attr('disabled', false);
                    $(that).find('i').removeClass('fa fa-spinner fa-spin');
                    $(that).find('i').addClass('fa fa-star');


            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });


}
function saveNewInvGroup(grp_name,e){
   if(grp_name !=''){
     var base_url = $("#base_url").val();
     var doctor_id = $('#doctor_id').val();
     var url = base_url + "/emr_lite/saveNewInvGroup";

     $.ajax({
         type: "POST",
         url: url,
         data: {
             grp_name:grp_name, doctor_id: doctor_id
         },
         beforeSend: function () {
             $('#addInvBtn').attr('disabled', true);
             $(e).find('i').removeClass('fa fa-plus');
             $(e).find('i').addClass('fa fa-spinner fa-spin');
         },
         success: function (data) {
             console.log(data);
            if(data.status==200){
             toastr.success('Success');
             if(data.prependData){
                 $('#addInvTableBody').prepend(data.prependData);
             }
             serialNoCalculation('row_class_newInv', 'row_count_class_newInv');
            }else if(data.status==102){
             toastr.warning('Already existing Group Name.');
            }else{
             toastr.warning('Sorry,Something went wrong.');
            }
            $('#addNewInv').val(' ');

         },
         complete: function () {
             $('#addInvBtn').attr('disabled', false);
             $(e).find('i').removeClass('fa fa-spinner fa-spin');
             $(e).find('i').addClass('fa fa-plus');
         },
         error: function () {
             toastr.error("Error please check your internet connection");
             $('#addNewInv').val(' ');

         },
     });
   }
 }
 function itemListInvWise(e,group_id){
    $('.activeTr').removeClass('bg-green');
    $(e).closest('.activeTr').addClass('bg-green');
    if(group_id){
     var investigation_type = $(".investigation_type").val();
     var base_url = $("#base_url").val();
     var doctor_id = $('#doctor_id').val();
     var url = base_url + "/emr_lite/itemListInvWise";

     $.ajax({
         type: "POST",
         url: url,
         data: {
             group_id:group_id,investigation_type:investigation_type, doctor_id: doctor_id
         },
         beforeSend: function () {
             $('#prescription_list_table_inv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
         },
         success: function (data) {
            $('#prescription_list_table_inv').html(data)

         },
         complete: function () {
             $('#prescription_list_table_inv').LoadingOverlay("hide");
             $('.theadscroll').perfectScrollbar({
                 wheelPropagation: true,
                 minScrollbarLength: 30
             });
             setTimeout(function () {
                 $('.theadfix_wrapper').floatThead({
                     position: 'absolute',
                     scrollContainer: true
                 });
             }, 400);


         },
         error: function () {
             toastr.error("Error please check your internet connection");
         },
     });
   }
 }

function saveChangedInvName(e,group_id){
    if(group_id && ($(e).parents('tr').find('.editInvName').attr('data-oldInvName') != $(e).parents('tr').find('.editInvName').val())){
      var changed_name=$(e).parents('tr').find('.editInvName').val();
      var old_name=$(e).parents('tr').find('.editInvName').attr('data-oldInvName');
      bootbox.confirm({
          title: 'Change Group Name',
          size: 'medium',
          message: 'Are you sure, you want change this group name ?',
          buttons: {
              'cancel': {
                  label: 'No',
                  style:'margin-left: 5px;'
              },
              'confirm': {
                  label: 'Yes',
              }
          },

          callback: function(result) {
              if (result) {
                  var base_url = $("#base_url").val();
                  var doctor_id = $('#doctor_id').val();
                  var url = base_url + "/emr_lite/saveChangedInvName";

                  $.ajax({
                      type: "POST",
                      url: url,
                      data: {
                          group_id:group_id,changed_name:changed_name, doctor_id: doctor_id
                      },
                      beforeSend: function () {
                          $(e).parents('tr').find('.grpEditBtn').attr('disabled', true);
                          $(e).find('i').removeClass('fa fa-save');
                          $(e).find('i').addClass('fa fa-spinner fa-spin');
                      },
                      success: function (data) {
                          if(data.status==200){
                              toastr.success('Success');
                          }else if(data.status==101){
                              toastr.warning('Named Group already Exist.');
                              $(e).parents('tr').find('.editGrpName').val(old_name);
                          }else{
                              toastr.warning('Sorry,Something went wrong.');
                              $(e).parents('tr').find('.editGrpName').val(old_name);
                          }

                      },
                      complete: function () {
                          $(e).parents('tr').find('.grpEditBtn').attr('disabled', false);
                          $(e).find('i').removeClass('fa fa-spinner fa-spin');
                          $(e).find('i').addClass('fa fa-save');

                      },
                      error: function () {
                          toastr.error("Error please check your internet connection");
                          $(e).parents('tr').find('.editGrpName').val(old_name);
                      },
                  });
              }else{
                  $(e).parents('tr').find('.editGrpName').val(old_name);
              }
          }
      });
    }else{
     toastr.warning('No change found.');
    }
  }
  function saveNewInvToGroup(e){
    var groupItems= new Array();
     var grp_id= $('#addItemToInv_grpId').val();
     var investigation_type = $(".investigation_type").val();
     var doctor_id = $('#doctor_id').val();

    $('.service_list_table_body_grp tr').each(function(){
      if($(this).find('.add_investigation_service_name').html()){
          groupItems.push({
              'service_name' : $(this).find('.add_investigation_service_name').html(),
              'service_code' : $(this).data('service-code'),

          })
      }
    })

   if(grp_id && groupItems.length > 0 ){
      var base_url = $("#base_url").val();
      var stringGrpItems=JSON.stringify(groupItems);
      var url = base_url + "/emr_lite/saveNewInvToGroup";

      $.ajax({
          type: "POST",
          url: url,
          data: {
              group_id:grp_id,stringGrpItems:stringGrpItems,investigation_type:investigation_type, doctor_id: doctor_id
          },
          beforeSend: function () {
              $('#saveNewItemsToInvBtn').attr('disabled', true);
              $(e).find('i').removeClass('fa fa-plus');
              $(e).find('i').addClass('fa fa-spinner fa-spin');
          },
          success: function (data) {
              if(data.status==200){
                  toastr.success('Success');
                  fetchInvestigationGroups();
              }else{
                  toastr.warning('Sorry,Something went wrong.');
              }

          },
          complete: function () {
              $('#saveNewItemsToInvBtn').attr('disabled', false);
              $(e).find('i').removeClass('fa fa-spinner fa-spin');
              $(e).find('i').addClass('fa fa-plus');

          },
          error: function () {
              toastr.error("Error please check your internet connection");
          },
      });
    }

  }
  function deleteInvName(e,group_id,from_type=0){
    if(group_id){
        bootbox.confirm({
            title: 'Delete Group',
            size: 'medium',
            message: 'Are you sure, you want to delete this group ?',
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    style:'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function(result) {
                if (result) {
                    var base_url = $("#base_url").val();
                    var url = base_url + "/emr_lite/deleteInvName";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            group_id:group_id
                        },
                        beforeSend: function () {
                            if(from_type==1){
                                $(e).attr('disabled', true);

                            }else{
                                $(e).parents('tr').find('.InvdeleteBtn').attr('disabled', true);

                            }
                            $(e).find('i').removeClass('fa fa-trash');
                            $(e).find('i').addClass('fa fa-spinner fa-spin');


                        },
                        success: function (data) {
                            if(data.status==200){
                                toastr.success('Deleted.');
                              if(from_type==1){
                                $('#prescription_bookmark_item'+group_id).fadeOut();
                              }else{
                                $(e).parents('tr').remove();
                                serialNoCalculation('row_class_newInv', 'row_count_class_newInv');

                              }
                              $('#prescription_bookmark_item'+group_id).fadeOut();
                            }else{
                                toastr.warning('Sorry,Something went wrong.');
                            }

                        },
                        complete: function () {
                            if(from_type==1){
                                $(e).attr('disabled', false);

                            }else{
                                $(e).parents('tr').find('.InvdeleteBtn').attr('disabled', false);
                            }
                            $(e).find('i').removeClass('fa fa-spinner fa-spin');
                                $(e).find('i').addClass('fa fa-trash');


                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                        },
                    });
                }
            }
        });
    }
}
