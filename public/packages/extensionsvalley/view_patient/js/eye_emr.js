$(document).ready(function () {

    $(".datepicker").datetimepicker({
        format: 'MMM-DD-YYYY'
    });

    $(".first_tab").champ();

    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    // getOpPatients(1);
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $(".select2").select2({
        placeholder: "",
        maximumSelectionSize: 6
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

    $(document).on("click", ".tab_list li", function (event) {
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    });

    setTimeout(function () {
        changeDoctor();
    }, 1000);


    $(".tab_content").removeAttr('title');
    if ($(".dynamic_notes").find('button').length == 1) {
        $(".dynamic_notes").hide();
        $(".tabcontent").css('width', '100%');
        $(".tabcontent").css('margin-top', '0px');
        $(".tabcontent").css('padding', '0px');
        $(".dynamic_single_head_text").html($(".dynamic_notes").find('button:first').html());
        $(".dynamic_single_head_text").show();
    }
});

var base_url = $("#base_url").val();
var investigation_future_date = 0;

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

$(document).on("keydown", "textarea,input[type='text']", function () {
    addBeforeUnloadCallbak();
});

$(document).on("click", "input[type='checkbox'],input[type='radio']", function () {
    addBeforeUnloadCallbak();
});

var emr_changes_array = $('#emr_changes').val();
emr_changes_array_parse = JSON.parse(emr_changes_array);

var investigation_future_date = emr_changes_array_parse.investigation_future_date ? emr_changes_array_parse.investigation_future_date : 0;
var current_date = $('#current_date').val();
var invest_date_cnt = 1;


function addBeforeUnloadCallbak() {
    var patient_id = $("#patient_id").val();
    if (patient_id && window.onbeforeunload == null) {
        window.onbeforeunload = function () {
            return "You have unsaved changes. Are you sure want to continue ?";
        };
    }

}

function removeBeforeUnloadCallbak() {
    if (window.onbeforeunload != null) {
        window.onbeforeunload = null;
    }
}

$(document).on("click", ".qms_call_button", function (e) {
    e.stopPropagation();
    var qms_token_no = $(this).parents('tr').attr('data-qms-token-no');
    var qms_appointment_id = $(this).parents('tr').attr('data-qms-appointment-id');
    var doctor_id = $("#doctor_id").val();
    var doctor_location = $("#doctor_location").val();
    var doctor_room = $("#doctor_room").val();
    var url = $("#queue_api_url").val();
    var patient_name = $(this).parents('tr').find('.list_item_name').html();
    var patient_uhid = $(this).parents('tr').attr('title');

    var params = {};
    params.doc_id = doctor_id;
    params.current_appointment_id = qms_appointment_id;
    params.location_id = doctor_location;
    params.appointment_no = qms_token_no;
    params.counter_name = doctor_room;
    params.current_patient = patient_name;
    params.current_patient_uhid = patient_uhid;
    params.from_type = "DOCTOR";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {

        },
        success: function (data) {
            toastr.success("Success");
        },
        complete: function () {

        },
        error: function () {

        },
    });

});

$(document).on("click", ".allergy_vital_show_hide_button", function () {
    if ($(".patient_demographics_container__parent").css('display') == 'block') {
        $(".patient_demographics_container__parent").hide();
        $(".patient_details_div").removeClass('col-md-3').addClass('col-md-1');
        $(".visit_details_div").removeClass('col-md-9').addClass('col-md-11');
        $(".left_side_tools_container__parent").removeClass('col-md-2').addClass('col-md-6');
    } else {
        $(".patient_details_div").removeClass('col-md-1').addClass('col-md-3');
        $(".visit_details_div").removeClass('col-md-11').addClass('col-md-9');
        $(".left_side_tools_container__parent").removeClass('col-md-6').addClass('col-md-2');
        $(".patient_demographics_container__parent").show();
    }
});

function configureAllergyVitalsDiv() {
    var emr_lite_allergy_vital_config = $("#emr_lite_allergy_vital_config").val();
    if (emr_lite_allergy_vital_config == 0) {
        $(".patient_demographics_container__parent").hide();
        $(".patient_details_div").removeClass('col-md-3').addClass('col-md-1');
        $(".visit_details_div").removeClass('col-md-9').addClass('col-md-11');
        $(".left_side_tools_container__parent").removeClass('col-md-2').addClass('col-md-6');
    }
}
var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = 0;
var patient_consultation_timer = null;
var timer2 = "0:15";

function setTime() {
    ++totalSeconds;
    secondsLabel.innerHTML = pad(totalSeconds % 60);
    minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
}

function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}


// var interval = setInterval(function () {

//     var timer = timer2.split(':');
//     var minutes = parseInt(timer[0], 10);
//     var seconds = parseInt(timer[1], 10);
//     --seconds;
//     minutes = (seconds < 0) ? --minutes : minutes;
//     if (minutes < 0) clearInterval(interval);
//     seconds = (seconds < 0) ? 59 : seconds;
//     seconds = (seconds < 10) ? '0' + seconds : seconds;
//     //minutes = (minutes < 10) ?  minutes : minutes;
//     $('.countdown_timer_oplist_refresh').html(minutes + ':' + seconds);
//     timer2 = minutes + ':' + seconds;
//     if (seconds == 00) {
//         getOpPatients(1);
//         timer2 = "0:15";
//     }
// }, 1000);

var prescription_id = 1;

function getOpPatients(seen_type) {
    if (seen_type) {
        window.seen_type = seen_type;
    } else {
        seen_type = window.seen_type;
    }
    var doctor_id = $('#doctor_id').val();
    var search_date = $("#op_patient_search_date").val();
    var group_doctor = $("#group_doctor").val();
    var url = base_url + "/emr_lite/showOPEmrPatient";
    var params = {
        search_date: search_date,
        doctor_id: doctor_id,
        group_doctor: group_doctor,
        seen_type: seen_type
    };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            showHidePatientFilters(2);
            $("#tab1").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $(".getPatientRefreshIcon").removeClass('fa-refresh').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#patient_total_cnt').html("Total : " + obj.resultcount);
            $('#unseen_cnt').html(obj.unseen_patients_count);
            $('#seen_cnt').html(obj.seen_patients_count);
            if (seen_type == 1) {
                $("#tab1").html(obj.data).show();
                $("#tab2").hide();
            } else {
                $("#tab2").html(obj.data).show();
                $("#tab1").hide();
            }
            if ($("#patient_id").val() != "") {
                $("tr[data-patient-id='" + $("#patient_id").val() + "']").addClass('active_row');
                $("tr[data-patient-id='" + $("#patient_id").val() + "']").find('.list_item_age').show();
                $("tr[data-patient-id='" + $("#patient_id").val() + "']").find('.list_item_tokenunseen_div').addClass('active_item');
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

        },
        complete: function () {
            $("#tab1").LoadingOverlay("hide");
            $(".getPatientRefreshIcon").removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-refresh');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function showHidePatientList(from_type) {
    if (parseInt(from_type) == 1) {
        $(".ip_op_list_div").hide("slide", {
            direction: "left"
        }, 500);
        $('.patient_data_list_div').removeClass('col-md-10');
        $('.patient_data_list_div').addClass('col-md-12');
        $('#patient_data_list_btn').show();
    }
    if (parseInt(from_type) == 2) {
        $('#patient_data_list_btn').hide();
        $('.patient_data_list_div').removeClass('col-md-12');
        $('.patient_data_list_div').addClass('col-md-10');
        $(".ip_op_list_div").show("slide", {
            direction: "left"
        }, 500);
    }
    resetBootstrapTable();
}

function showHidePatientFilters(from_type) {
    if (parseInt(from_type) == 1) {
        $("#showHidePatientFiltersAjaxDiv").slideDown();
        $('#showHidePatientFiltersAjaxDiv').show();
    } else if (parseInt(from_type) == 2) {
        $("#showHidePatientFiltersAjaxDiv").slideUp();
        $('#showHidePatientFiltersAjaxDiv').hide();
    }
}

$(document).on("click", ".ip_op_list_item", function () {
    var patient_id = $(this).attr('data-patient-id');
    var booking_id = $(this).attr('data-booking-id');
    var qms_auto_call_enable = $(this).attr('data-qms_auto_call_enable');
    var qms_queue_enable = $(this).attr('data-qms-queue_enable');
    var data_seen_status = $(this).attr('data-seen-status');
    var is_video_consultation = $(this).attr('data-video-consultation');

    if ($('#patient_id').val() && window.onbeforeunload != null) {
        var confim_change = window.confirm("You have unsaved changes. Are you sure want to continue ?");
        if (!confim_change) {
            return;
        }
    }

    removeBeforeUnloadCallbak();

    if (qms_auto_call_enable == 1 && qms_queue_enable == 1 && data_seen_status == 0) {
        $(this).find('.qms_call_button').trigger('click');
    }

    $('#patient_id').val(patient_id);
    $('#booking_id').val(booking_id);
    $(".ip_op_list_item").removeClass('active_row');
    $(".list_item_age").hide();
    $(".list_item_tokenunseen_div").removeClass('active_item');
    $(this).addClass('active_row');
    $(this).find('.list_item_age').show();
    $(this).find('.list_item_tokenunseen_div').addClass('active_item');
    $(".toggleIpOpListBtn").trigger('click');
    fetchPatientDetails(patient_id);
    if (patient_consultation_timer) {
        clearInterval(patient_consultation_timer);
        secondsLabel.innerHTML = '00';
        minutesLabel.innerHTML = '00';
        totalSeconds = 0;
    }
    patient_consultation_timer = setInterval(setTime, 1000);

    if (is_video_consultation == '1') {
        $(".video_consultation_div").removeClass('hidden');
    } else {
        $(".video_consultation_div").addClass('hidden');
    }

});


function fetchPatientDetails(patient_id) {
    var patient_id;
    if (!patient_id) {
        patient_id = $('#patient_id').val();
    }

    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/fetchPatientDetails";
    var params = {
        patient_id: patient_id,
        doctor_id: doctor_id
    };
    var patient_basic_details = [];
    $('.dynamic_chief_complaints_textarea').val('');
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".todays_visit_details_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $(".patient_combined_history_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $(".investigation_history_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $(".prescription_history_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var patient_details = data.patient_details;
            var history_editable = data.history_editable;
            patient_basic_details = patient_details.patient_details;
            var patient_allergies = patient_details.patient_allergies;
            var patient_other_allergy = patient_details.patient_other_allergy;
            var personal_notes = patient_details.patient_private_notes;
            var show_patient_reference = data.show_patient_reference;
            if (patient_details.patient_info.patient_image_url) {
                $("#patient_image_url").attr('src', patient_details.patient_info.patient_image_url)
            }
            $(".patient_details_patient_place").html("Area : " + patient_details.area);
            $(".patient_details_patient_address").html("Address : " + patient_details.patient_info.address);
            $(".patient_details_patient_district").html("District : " + patient_details.patient_info.district);
            $(".patient_details_patient_company").html("Company : " + patient_details.patient_info.company_name);
            $(".patient_details_patient_pricing").html("Pricing : " + patient_details.patient_info.pricing_name);
            $(".patient_details_patient_phone").html("Mobile : " + patient_details.patient_info.phone);
            if (show_patient_reference == '1') {
                $(".patient_details_co_type").html("C/O Type : " + patient_details.patient_info.co_type);
                $(".patient_details_co_name").html("C/O Name : " + patient_details.patient_info.co_name);
                $(".patient_details_co_mobile").html("C/O Mobile : " + patient_details.patient_info.co_mobile);
            } else {
                $(".patient_details_patient_covid_history").html("Covid History : " + patient_details.patient_info.covid_history);
                $(".patient_details_patient_vaccine_type").html("Type of vaccine : " + patient_details.patient_info.vaccine_name);
            }
            var formdata = patient_details.formdata;
            showPatientFormData(formdata);
            showPatientAllergies(patient_allergies, patient_other_allergy);
            $(".private_notes_textarea").text(personal_notes);
            var patient_vitals = patient_details.patient_vitals;
            // showLatestPatientVitals(patient_vitals[0]);
            var patient_medications = patient_details.patient_medication;
            var patient_medications_old = patient_details.patient_medications_old;
            showPatientMedicationHistory(patient_medications, patient_medications_old, history_editable);
            var patient_investigations = patient_details.patient_investigation;
            // showPatientInvestigationHistory(patient_investigations);
            $(".investigation_history_div").html(patient_details.investigation_details);
            var patient_combined_history = patient_details.patient_combined_history ? patient_details.patient_combined_history : "";
            showPatientCombinedHistory(patient_combined_history);
            var patient_todays_visit_details = patient_details.patient_todays_visit_details;
            $(".todays_visit_details_div").html(patient_todays_visit_details);
            $(".chief_complaint").html('');
            $(".investigation_item_search_textbox").val('');
            $(".assessmentFavButton").show();
            $('#chief_com').focus();
            resetPrescriptionList();
            resetSelectedInvestigations();
            fetchPatientInvResultEntry();

            if (patient_details.clinical_data_change == 1) {
                $("li[rel='tab_1']").click(); // navigating to notes tab
            }

        },
        complete: function () {
            $(".todays_visit_details_div").LoadingOverlay("hide");
            $(".patient_combined_history_div").LoadingOverlay("hide");
            $(".investigation_history_div").LoadingOverlay("hide");
            $(".prescription_history_div").LoadingOverlay("hide");
            fetchPatientVisit();
            setTimeout(function () {
                showPatientDetails(patient_basic_details);

            }, 1000);

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


function showPatientDetails(patient_basic_details) {
    $("#visit_id").val(patient_basic_details.visit_id ? patient_basic_details.visit_id : 0);
    $("#encounter_id").val(patient_basic_details.encounter_id);
    $("#visit_type").val(patient_basic_details.visit_status ? patient_basic_details.visit_status : '');
    $(".patient_details_patient_name").html(patient_basic_details.patient_name);
    $(".patient_details_patient_uhid").html(patient_basic_details.uhid);
    $(".patient_details_patient_age_gender").html(patient_basic_details.patient_age + '/' + patient_basic_details.patient_gender);
    viewDynamicNotesFields();
    getEyeRefractionHistory();
}


function showPatientAllergies(patient_allergies, patient_other_allergy) {
    $(".allergy_list").html('');
    if (typeof (patient_allergies) != 'undefined') {
        if (patient_allergies.length > 0) {
            $.each(patient_allergies, function (key, val) {
                var allergy = '';
                if (val.allergy_type == 'B') {
                    // allergy = '<span class="blue"> Brand : </span> ' + val.allergy_name + '<br>';
                    allergy = val.allergy_name + '<br>';
                } else if (val.allergy_type == 'G') {
                    // allergy = '<span class="blue"> Generic : </span> ' + val.allergy_name + '<br>';
                    allergy = val.allergy_name + '<br>';
                }
                $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + val.allergy_id + '">' + allergy + '</span>');
            });
        }
    }

    if (typeof (patient_other_allergy) != 'undefined') {
        if (patient_other_allergy.length > 0) {
            $.each(patient_other_allergy, function (key, val) {
                if (val.allergy_name) {
                    // var allergy = '<span class="blue"> Other : </span> ' + val.allergy + '<br>';
                    var allergy = val.allergy_name + '<br>';
                    $(".allergy_list").append('<span class="allergic_item" data-allergic-id="' + val.allergy_id + '">' + allergy + '</span>');
                }
            })
        }
    }

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
}

var bp_sys_vital_master_id = "5";
var bp_dia_vital_master_id = "6";

function showLatestPatientVitals(patient_vitals) {
    if (patient_vitals) {
        var vital_details = patient_vitals.vital_details;
        $(".vital_list_table_body").empty();
        var bp_value = '';
        $.each(vital_details, function (key, val) {
            var vital_icon = val.icon_class ? val.icon_class : '';
            var vital_name = val.display_name ? val.display_name : val.vital_name;
            var vital_value = val.vital_value;
            var string_class = '';
            if (vital_value > parseFloat(val.max_value)) {
                string_class = " <i class='fa fa-arrow-up blink_me'></i> ";
            }
            if (vital_value < parseFloat(val.min_value)) {
                string_class = " <i class='fa fa-arrow-down blink_me'></i> ";
            }


            if (val.vital_master_id == bp_dia_vital_master_id) {
                if ($(".vital_master_" + bp_sys_vital_master_id).length > 0) {
                    var bp_sys = $(".vital_master_" + bp_sys_vital_master_id).find('.patient_vital_value').html();
                    $(".vital_master_" + bp_sys_vital_master_id).find('.patient_vital_value').html(bp_sys + '/' + vital_value + '' + string_class);
                } else {
                    vital_name = 'BP';
                    $(".vital_list_table_body").append('<tr data-vital-master-id="' + val.vital_master_id + '" class="vital_item vital_master_' + val.vital_master_id + '"><td class="vital_icon"><i class="' + vital_icon + '"></i></td><td>' + vital_name + '</td><td><b class="patient_vital_value ">' + vital_value + '</b></td></tr>');
                }

            } else if (val.vital_master_id == bp_sys_vital_master_id) {
                if ($(".vital_master_" + bp_dia_vital_master_id).length > 0) {
                    var bp_dia = $(".vital_master_" + bp_dia_vital_master_id).find('.patient_vital_value').html();
                    $(".vital_master_" + bp_dia_vital_master_id).find('.patient_vital_value').html(vital_value + '/' + bp_dia + '' + string_class);
                } else {
                    vital_name = 'BP';
                    $(".vital_list_table_body").append('<tr data-vital-master-id="' + val.vital_master_id + '" class="vital_item vital_master_' + val.vital_master_id + '"><td class="vital_icon"><i class="' + vital_icon + '"></i></td><td>' + vital_name + '</td><td><b class="patient_vital_value ">' + vital_value + '</b></td></tr>');
                }

            } else {
                $(".vital_list_table_body").append('<tr data-vital-master-id="' + val.vital_master_id + '" class="vital_item vital_master_' + val.vital_master_id + '"><td class="vital_icon"><i class="' + vital_icon + '"></i></td><td>' + vital_name + '</td><td><b class="patient_vital_value ">' + vital_value + '</b>' + string_class + '</td></tr>');
            }
        })
    }
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    });
}

function showPatientNotesHistory(patient_notes) {

    $(".notes_history_div").empty();

    $.each(patient_notes, function (key, val) {

        var notes_date = moment(val.created_at).format('DD MMM YYYY hh:mm A');
        var doctor_name = capitalizeFirstLetter(val.doctor_name);

        var notes_div = '';
        notes_div = '<div class="notes_history_item"><div class="notes_history_head" style="display:flex;"><div class="notes_history_head_left"><div><i class="fa fa-calendar"></i> ' + notes_date + '</div><div><i class="fa fa-user-md"></i> ' + doctor_name + '</div> </div><div class="notes_history_head_right"><i class="fa fa-edit editBtn"></i><i class="fa fa-copy copyBtn"></i><i class="fa fa-trash deleteBtn"></i></div></div> <div class="notes_list">';

        $.each(val.notes_list, function (key1, val1) {

            notes_div = notes_div + ' <span class="notes_history_item_desc">' + val1.notes_html + '</span><div class="clearfix border_dashed_bottom"></div> ';

        });

        notes_div = notes_div + '</div> </div> ';

        $(".notes_history_div").append(notes_div);



    });



}

function showDynamicAssessmentTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabName).style.display = "block";
    $('#' + tabName).find('.' + tabName + '_textarea').focus();
    evt.currentTarget.className += " active";

    var dynamic_head_id = $(evt.currentTarget).attr('data-head-id');
    if (parseInt(dynamic_head_id) > 0) {
        getDoctorBookmarkedNotes(parseInt(dynamic_head_id));
    }
}

function showPatientFormData(formdata) {
    // $(".notes_chief_complaint_container").html(formdata.response);
}

function capitalizeFirstLetter(string) {
    string = string.toLowerCase();
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function getDoctorBookmarkedNotes(head_id = 1) {
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/getDoctorBookmarkedNotes";
    var params = {
        doctor_id: doctor_id,
        head_id: head_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".notes_bookmarks_area").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $(".notes_bookmarks_area").html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $(".notes_bookmarks_area").LoadingOverlay("hide");

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function showPatientMedicationHistory(patient_medications, patient_medications_old, history_editable) {

    $(".prescription_history_div").empty();
    var doctor_id = $('#doctor_id').val();
    $.each(patient_medications, function (key, val) {
        var presc_date = moment(val.created_at).format('DD MMM YYYY hh:mm A');
        var doctor_name = capitalizeFirstLetter(val.doctor_name);
        var created_doctor_id = val.doctor_id;
        var date1 = new Date();
        var date2 = new Date(val.created_at);
        var diffTime = Math.abs(date1 - date2);
        var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        var bill_converted_class = '';
        if (val.billconverted_status == 1 && parseInt(diffDays) >= parseInt(history_editable)) {
            bill_converted_class = ' hidden ';
        }

        if (created_doctor_id != undefined && created_doctor_id != doctor_id) {
            bill_converted_class = ' hidden ';
        }
        var prescription_head_id = val.head_id;
        var presc_div = '';
        presc_div = '<div class="prescription_history_item" data-head-id="' + prescription_head_id + '" data-bill-converted-head-status="' + val.billconverted_status + '"><div class="presc_history_head" style="display:flex;"><div class="presc_history_head_left"><div><i class="fa fa-calendar"></i> ' + presc_date + '</div><div class="overflow_text"><i class="fa fa-user-md"></i> ' + doctor_name + '</div> </div><div class="presc_history_head_right"><i class="fa fa-print printBtn printPrescriptionButton"></i><i class="fa fa-edit editBtn ' + bill_converted_class + ' editPrescriptionButton"></i><i class="fa fa-copy copyBtn copyPrescriptionButton"></i><i class="fa fa-trash deleteBtn ' + bill_converted_class + ' deletePrescriptionButton"></i></div></div> <div class="prescription_list">';
        $.each(val.medicine_list, function (key1, val1) {
            duration_unit_id = val1.duration_unit_id;

            if (duration_unit_id == 1) {
                duration_unit = 'Days';

            } else if (duration_unit_id == 2) {
                duration_unit = 'Week';
            } else if (duration_unit_id == 3) {
                duration_unit = 'Month';
            } else {
                duration_unit = 'Days';
            }
            presc_div = presc_div + '<span class="checkbox_serial"><input type="checkbox" class="prescription_history_item_checkbox" data-detail-id="' + val1.detail_id + '" data-frequency-name="' + val1.frequency + '" data-frequency-value="' + val1.frequency_value + '" data-frequency-id="' + val1.frequency_id + '" data-item-quantity="' + val1.quantity + '" data-item-duration="' + val1.duration + '" data-item-duration_unit="' + duration_unit_id + '" data-item-direction="' + val1.notes + '" data-item-desc="' + val1.item_desc + '" data-item-code="' + val1.medicine_code + '" data-bill-converted-status="' + val1.bill_converted_status + '" /></span> <span class="presc_history_item_desc">' + val1.item_desc + '</span><span class="presc_history_frequency"> | ' + val1.frequency + ' | </span> <span class="presc_history_duration"> x ' + val1.duration + ' ' + duration_unit + '</span> <div class="clearfix border_dashed_bottom"></div> ';

        });

        presc_div = presc_div + '</div> </div> ';

        $(".prescription_history_div").append(presc_div);

    });

    $(".prescription_history_div").append(patient_medications_old);

}


function showPatientInvestigationHistory(patient_investigations) {
    $(".investigation_history_div").empty();
    $.each(patient_investigations, function (key, val) {

        var invest_date = '';
        if (val.investigation_list.length > 0) {
            invest_date = moment(val.investigation_list[0].created_at).format('DD MMM YYYY hh:mm A');
        } else {
            invest_date = moment(val.created_at).format('DD MMM YYYY hh:mm A');
        }

        var doctor_name = val.doctor_name ? capitalizeFirstLetter(val.doctor_name) : '';

        var bill_converted_class = '';
        if (val.bill_converted_status == 1) {
            bill_converted_class = ' hidden ';
        }
        var invest_div = '';
        invest_div = '<div class="investigation_history_item" data-investigation-head-id="' + val.head_id + '"><div class="invest_history_head" style="display:flex;"><div class="invest_history_head_left"><div><i class="fa fa-calendar"></i> ' + invest_date + '</div><div><i class="fa fa-user-md"></i> ' + doctor_name + '</div> </div><div class="invest_history_head_right"><i class="fa fa-edit editBtn editInvestigationBtn ' + bill_converted_class + '"></i><i class="fa fa-copy copyBtn copyInvestigationBtn"></i><i class="fa fa-trash deleteBtn deleteInvestigationBtn ' + bill_converted_class + '"></i></div></div> <div class="investigation_list">';

        $.each(val.investigation_list, function (key1, val1) {

            var result = '';
            if (val1.sub_test_list.length == 0 && val1.investigation_type == 'LAB') {
                result = ' - ' + val1.actual_result;
            }

            var invest_sub_list = '';
            $.each(val1.sub_test_list, function (key2, val2) {
                invest_sub_list = invest_sub_list + '<div class="investigation_subtest_list"><span class="checkbox_serial"><i class="fa fa-bullseye"></i></span> <span class="invest_history_item_desc">' + val2.service_desc + '</span><span class="invest_history_result"> - ' + val2.actual_result + '</span><div class="clearfix border_dashed_bottom"></div></div>';
            });
            invest_div = invest_div + '<div class="investigation_list_item"><span class="checkbox_serial">' + (key1 + 1) + '. </span> <span class="invest_history_item_desc">' + val1.investigation_type + '-' + val1.service_desc + '</span><span class="invest_history_result">' + result + '</span><div class="clearfix border_dashed_bottom"></div> ' + invest_sub_list + '</div>';

        });


        invest_div = invest_div + '</div> </div> ';

        $(".investigation_history_div").append(invest_div);



    });

}

function showPatientCombinedHistory(patient_combined_history) {
    $(".patient_combined_history_div").html(patient_combined_history);
}

$(document).on("click", ".saveClinicalDataButton", function () {
    saveClinicalData(1);
});

$(document).on("click", ".saveAndPrintClinicalDataButton", function () {
    saveClinicalData(2);
});



function saveClinicalData(save_status) {

    $('.saveClinicalDataButton').prop({
        disable: 'true'
    });
    var patient_clinical_data = {};

    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let doctor_id = $('#doctor_id').val();
    patient_clinical_data.patient_id = patient_id;
    patient_clinical_data.visit_id = visit_id;
    patient_clinical_data.encounter_id = encounter_id;
    patient_clinical_data.doctor_id = doctor_id;
    var investigations = {};
    investigations.investigation_list = [];

    $(".selected_investigation_table_body").find('tr').each(function (key, val) {
        var investigation = {};
        investigation.detail_id = $(val).attr('data-detail-id') ? $(val).attr('data-detail-id') : '';
        investigation.service_name = $(val).find('.selected_investigation_service_name').html();
        investigation.service_code = $(val).attr('data-service-code');
        investigations.investigation_list.push(investigation);
    });

    investigations.remark = $(".investigation_remarks_textarea").val();
    investigations.clinical_history = $(".investigation_clinical_history_textarea").val();

    patient_clinical_data.investigations = investigations;
    patient_clinical_data.investigation_head_id = $('#investigation_head_id').val();
    patient_clinical_data.glassPrescription = getGlassPrescription();
    patient_clinical_data.glass_prescription_id = $('#glass_prescription_id').val();

    let validatePresc = validatePrescription();
    if (validatePresc) {

        var medicine_check = [];
        $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
            if ($(val).val() != '') {
                medicine_check.push(val);
            }
        });

        if (medicine_check.length > 0) {
            let prescription_head_id = $('#prescription_head_id').val();
            var presc_form = [];

            $(".prescription_list_table_body").find('tr.row_class').each(function (key, value) {
                var presc_row = {};
                presc_row.selected_item_code = $(value).find('input[name="medicine_name"]').attr('data-item-code');
                presc_row.selected_item_name = $(value).find('input[name="medicine_name"]').val();
                presc_row.selected_item_frequency = $(value).find('.frequency').val();
                presc_row.selected_frequency_value = $(value).find('.frequency').attr('data-frequency-value');
                presc_row.selected_frequency_id = $(value).find('.frequency').attr('data-frequency-id');
                presc_row.selected_item_duration = $(value).find('.duration').val();
                presc_row.duration_unit = $(value).find('.duration_unit').val();
                presc_row.selected_item_quantity = $(value).find('.quantity').val();
                presc_row.selected_item_direction = $(value).find('.direction').val() ? $(value).find('.direction').val() : '';
                presc_row.selected_detail_id = $(value).attr('data-detail-id') ? $(value).attr('data-detail-id') : '';

                presc_form.push(presc_row);

            });

            var presc_form1 = JSON.stringify(presc_form);
            presc_form = escape(presc_form1);
            patient_clinical_data.prescriptions = presc_form;
            patient_clinical_data.prescription_head_id = prescription_head_id;
            patient_clinical_data.presc_advice = $(".prescription_advice_textarea").val();
        } else {
            patient_clinical_data.prescriptions = "";
            patient_clinical_data.prescription_head_id = "";
            patient_clinical_data.presc_advice = $(".prescription_advice_textarea").val();
        }

    } else {
        $('.saveClinicalDataButton').prop({
            disable: 'false'
        });
        return;
    }
    patient_clinical_data.eye_image_editor = canvas.toDataURL();
    var dynamic_notes_data = {};
    try {
        $('.notes_templates').each(function (index) {
            var element = $(this).attr('attr-dataid');
            dynamic_notes_data[element] = $(this).val();
        });
        dynamic_notes_data = JSON.stringify(dynamic_notes_data);
        dynamic_notes_data = escape(dynamic_notes_data);
    } catch (e) {
        dynamic_notes_data = '';
    }

    patient_clinical_data.dynamic_notes_data = dynamic_notes_data;
    var booking_id = $("#booking_id").val()
    patient_clinical_data.booking_id = booking_id;

    if (validatePresc) {
        var url = base_url + "/eye_emr/saveEyeEmrData";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_clinical_data: patient_clinical_data
            },
            beforeSend: function () {
                $('body').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                if (data.status == 1) {
                    fetchPatientTodaysDetails();
                    $(".selected_investigation_table_body").empty();
                    $(".prescription_list_table_body").empty();
                    $("#ca-data-form").find('textarea').val('');
                    $("#ca-data-form").find('input:text').val('');
                    $("#ca-data-form").find('input:radio').prop('checked', false);
                    $("#ca-data-form").find('input:checkbox').prop('checked', false);
                    $("#ca-data-form").find('li').removeClass('active');
                    $("#ca-data-form").find('textarea').val('');
                    $("#ca-data-form").find('input:text').val('');
                    $("#ca-data-form").find('input:radio').prop('checked', false);
                    $("#ca-data-form").find('input:checkbox').prop('checked', false);
                    $("#ca-data-form").find('li').removeClass('active');
                    $("#ca-data-form").find('input:hidden').val('');
                    $("#glass_prescription_id").val(data.glassPrescription_id);

                    $(".investigation_tree_area").find('input[type="checkbox"]').prop('checked', false)
                    $(".prescription_history_div").find('input[type="checkbox"]').prop('checked', false)
                    $(".prescription_bookmark_div").find('input[type="checkbox"]').prop('checked', false)

                    Command: toastr["success"]("Success");

                    if ($("tr[data-patient-id='" + patient_id + "']").attr('data-seen-status') != "1") {
                        $("tr[data-patient-id='" + patient_id + "']").remove();
                    }
                    removeBeforeUnloadCallbak();

                    if (save_status == 2) {
                        if ($("#print_dialog_config").val().toString() == '1' & data.prescription_head_id > 0 && data.investigation_head_id > 0) {
                            $("#prescription_print_config_modal").attr("presc-head-id", data.prescription_head_id);
                            $("#prescription_print_config_modal").attr("invest-head-id", data.investigation_head_id);
                            $("#prescription_print_config_modal").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        } else if (data.prescription_head_id > 0 && data.investigation_head_id == 0) {
                            printPrescription(data.prescription_head_id);
                        } else if (data.prescription_head_id == 0 && data.investigation_head_id > 0) {
                            printInvestigation(data.investigation_head_id);
                        }
                        if (parseInt(data.glassPrescription_id) != 0) {
                            printGlassPrescription(data.glassPrescription_id);
                        }
                    }
                    canvas.clear();

                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }

            },
            complete: function () {
                $('body').LoadingOverlay("hide");
                getEyeRefractionHistory();
                // saveInvestigationResultEntry();
                getOpPatients(1);
            }
        });

    } else {
        $('.saveClinicalDataButton').prop({
            disable: 'false'
        });
    }
}


function fetchPatientTodaysDetails() {
    var patient_id = $('#patient_id').val();
    var url = base_url + "/emr_lite/fetchPatientTodaysDetails";
    var doctor_id = $('#doctor_id').val();
    var params = {
        patient_id: patient_id,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".todays_visit_details_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var patient_details = data.patient_details;
            var patient_todays_visit_details = patient_details.patient_todays_visit_details;
            $(".todays_visit_details_div").html(patient_todays_visit_details);
            $(".chief_complaint").html('');
            $(".investigation_item_search_textbox").val('');
            $(".assessmentFavButton").show();
            $('#chief_com').focus();
            resetPrescriptionList();
            resetSelectedInvestigations();

            if (patient_details.clinical_data_change == 1) {
                $("li[rel='tab_1']").click(); // navigating to notes tab
            }

        },
        complete: function () {
            $(".todays_visit_details_div").LoadingOverlay("hide");
            fetchPatientVisit();
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


// validate notes
function validateAssessment(dataValueArr) {
    if (Array.isArray(dataValueArr)) {
        return true;
    }
    // if (Array.isArray(dataValueArr) && dataValueArr.length) {
    //     return true;
    // }
    return false;
}


//validate prescription
function validatePrescription() {

    $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
        if ($(val).val() == '') {
            $(val).parents('tr').remove();
        }
    })

    var medicine_check = [];
    var frequency_check = [];
    $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
        if ($(val).val() != '') {
            medicine_check.push(val);
        }
        if ($(val).parents('tr').find('.frequency').val() != '') {
            frequency_check.push(val);
        }
    });

    // if (medicine_check.length == 0) {
    //     Command: toastr["error"]("Select Medicine.");
    //     $('input[name="medicine_name"]')[0].focus();
    //     return false;
    // }

    if (medicine_check.length > 0 && medicine_check.length != frequency_check.length) {
        Command: toastr["error"]("Please select frequency.");
        $('input[name="selected_item_frequency[]"]').each(function (key, val) {
            if ($(val).parent().find('input[name="selected_frequency_value[]"]').val() == '') {
                $(val).focus();
                return false;
            }
        })
        return false;
    }

    var valid_duration = [];
    $('.duration').each(function (key, val) {
        if ($(val).val() != '') {
            valid_duration.push(val);
        }
    })

    if (medicine_check.length != valid_duration.length) {
        Command: toastr["error"]("Please select Days.");
        $('input[name="selected_item_duration[]"]').each(function (key, val) {
            if ($(val).val() == '') {
                $(val).focus();
                return false
            }
        })

        return false;
    }

    var valid_quantity = [];
    $('.quantity').each(function (key, val) {
        if ($(val).val() != '') {
            valid_quantity.push(val);
        }
    })

    if (medicine_check.length != valid_quantity.length) {
        Command: toastr["error"]("Please select quantity.");

        $('.quantity').each(function (key, val) {
            if ($(val).val() == '') {
                $(val).focus();
                return false
            }
        })
        return false;
    }

    return true;
}

$(document).on('click', '.patient_clinical_history_btn', function (e) {
    e.preventDefault();
    fetchPatientClinicalHistory();
});

$(document).on('click', '.patient_lab_results_btn', function (e) {
    e.preventDefault();
    fetchPatientLabResults();
});

$(document).on('click', '.patient_personal_notes_btn', function (e) {
    e.preventDefault();
    fetchPatientPersonalNotes();
});

$(document).on('click', '.patient_discharge_summary_list_btn', function (e) {
    e.preventDefault();
    fetchDischargeSummaryList();
});

$(document).on('click', '.patient_special_notes_btn', function (e) {
    e.preventDefault();
    fetchSpecialNotes();
});

$(document).on('click', '.patient_documents_btn', function (e) {
    e.preventDefault();
    var patient_id = $("#patient_id").val();
    if (patient_id) {
        manageDocs();
    }
});

$(document).on('click', '.patient_radiology_results_btn', function (e) {
    e.preventDefault();
    fetchRadiologyResults();
});

$(document).on("change", "#search_service_id", function () {
    fetchRadiologyResults();
});

$(document).on("click", ".radilogy_results_pagination_div nav ul li a.page-link", function (e) {
    $(this).parent('li').addClass('active');
    var page = $(this).attr('href').split('page=')[1];
    fetchRadiologyResults(page);
});

$(document).on("click", ".patient_discharge_summary_create_btn", function (e) {
    e.preventDefault();
    var visit_id = $("#visit_id").val();
    var visit_type = $("#visit_type").val();
    if (visit_id && visit_type == 'IP') {
        editDischargeSummary(visit_id);
    } else {
        Command: toastr["warning"]("Please select any IP patient to continue.");
    }
});


$(document).on("click", ".patient_refer_btn", function (e) {
    e.preventDefault();
    referPatient();
});

$(document).on("click", ".patient_transfer_btn", function (e) {
    e.preventDefault();
    transferPatient();
});




function fetchPatientClinicalHistory() {
    var url = base_url + "/emr_lite/fetchPatientClinicalHistory";
    var patient_id = $("#patient_id").val();
    var doctor_id = $('#doctor_id').val();
    var param = {
        patient_id: patient_id,
        doctor_id: doctor_id
    };
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('.patient_clinical_history_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-history');
            },
            success: function (data) {
                if (data.status == 1) {
                    $("#patientClinicalHistoryModalBody").html(data.patient_combined_history);
                    $("#patientClinicalHistoryModal").modal('show');
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }
            },
            complete: function () {
                $('.patient_clinical_history_btn').find('i').addClass('fa-history').removeClass('fa-spinner fa-spin');
            }
        });
    }

}


function fetchPatientLabResults() {
    var url = base_url + "/emr_lite/labResultTrends";
    var patient_id = $("#patient_id").val();
    var encounter_id = $('#encounter_id').val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "GET",
            url: url,
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id,
            beforeSend: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-flask');
            },
            success: function (data) {
                $('#lab_restuls_data').html(data);
                $("#patientLabResultsModal").modal('show');
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            },
            complete: function () {
                $('.patient_lab_results_btn').find('i').addClass('fa-flask').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchPatientPersonalNotes() {
    var url = base_url + "/emr_lite/fetchPatientPersonalNotes";
    var patient_id = $("#patient_id").val();
    var doctor_id = $('#doctor_id').val();
    var that = this;
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                doctor_id: doctor_id
            },
            beforeSend: function () {
                $('.patient_personal_notes_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-commenting-o');
            },
            success: function (data) {
                if (data.status == 1) {
                    $(".private_notes_textarea").val(data.personal_notes);
                    $("#patientPersonalNotesModal").modal('show');
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }
            },
            complete: function () {
                $('.patient_personal_notes_btn').find('i').addClass('fa-commenting-o').removeClass('fa-spinner fa-spin');
            }
        });
    }
}


function fetchDischargeSummaryList() {
    var url = base_url + "/emr_lite/fetchDischargeSummaryList";
    var patient_id = $("#patient_id").val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id
            },
            beforeSend: function () {
                $('.patient_discharge_summary_list_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-list');
            },
            success: function (data) {
                $('#discharge_summary_list_modal_body').html(data);
                $("#discharge_summary_list_modal").modal('show');
            },
            complete: function () {
                $('.patient_discharge_summary_list_btn').find('i').addClass('fa-list').removeClass('fa-spinner fa-spin');
            }
        });
    }
}


function showSummary(summary_id) {
    var url = base_url + "/emr_lite/showDischargeSummary";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            discharge_summary_id: summary_id
        },
        beforeSend: function () {
            $('#discharge_summary_view_modal').modal('show');
            $('#discharge_summary_view_modal_body').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#discharge_summary_view_modal_body').LoadingOverlay("hide");
            $('#discharge_summary_view_modal_body').html('<div class="" style="height:400px;overflow-y:scroll">' + data.summary + '</div>');

            if (data.final_status == 0) {
                $('#discharge_summary_view_modal_body').append('<button style="margin-top: 20px;" title="Mark as Finalized" onclick="finalizeDischargeSummary(' + summary_id + ');" class="btn btn-success"><i class="fa fa-check"></i>Finalize Summary</button><button style="margin-top: 20px;" title="Edit Summary" onclick="editDischargeSummary(' + data.visit_id + ');" class="btn btn-success"><i class="fa fa-edit"></i> Edit Summary</button>');
            }
        },
        complete: function () {

        }
    });
}

function finalizeDischargeSummary(summary_id) {
    var url = base_url + "/emr/finalizeDischargeSummary";
    var doctor_id = $('#doctor_id').val();
    var param = {
        summary_id: summary_id,
        doctor_id: doctor_id
    };
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            $('#summary_view_data').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            if ($('#summary_view_modal').hasClass('in')) {
                $('#summary_view_modal').modal('hide');
            } else {
                showDischargeSummary();
            }
            if (data.status == 1) {
                toastr.success("Discharge Summary Finalized..!");
            }

        },
        complete: function () {

        }
    });
}



function editDischargeSummary(visit_id) {
    window.location = base_url + '/summary/dischargesummary/' + visit_id;
}


function fetchSpecialNotes() {
    var url = base_url + "/emr_lite/fetchSpecialNotes";
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                visit_id: visit_id
            },
            beforeSend: function () {
                $('.patient_special_notes_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-book');
            },
            success: function (data) {
                if (data != '') {
                    $('#special_notes_modal_body').html(data);
                    $("#special_notes_modal").modal('show');
                } else {
                    toastr.success("No special notes added yet.");
                }

            },
            complete: function () {
                $('.patient_special_notes_btn').find('i').addClass('fa-book').removeClass('fa-spinner fa-spin');
            }
        });
    }
}

function fetchRadiologyResults(page = 1) {
    var url = base_url + "/emr_lite/fetchRadiologyResults";
    var patient_id = $("#patient_id").val();
    var search_service_id = $('#search_service_id').val();
    if (patient_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                search_service_id: search_service_id,
                page: page
            },
            beforeSend: function () {
                $('.patient_radiology_results_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-camera');
            },
            success: function (data) {
                $('#radiology_results_data').html(data.html);
                $("#search_service_id").empty();
                $("#search_service_id").append('<option value="">Select Service</option>');
                $.each(data.service_list, function (key, val) {
                    $("#search_service_id").append('<option value="' + val.id + '">' + val.service_desc + '</option>');
                })

                if (search_service_id) {
                    $("#search_service_id").val(search_service_id);
                }
                $("#radiology_results_modal").modal('show');
            },
            complete: function () {
                $('.patient_radiology_results_btn').find('i').addClass('fa-camera').removeClass('fa-spinner fa-spin');
            }
        });
    }
}


$(document).on("click", ".pacsViewerBtn", function () {
    let pacs_viewer_prefix = $('#pacs_viewer_prefix').val();
    var accession_no = $(this).attr('data-accession-no');
    if (pacs_viewer_prefix && accession_no) {
        $("#pacs_viewer_iframe").attr('src', pacs_viewer_prefix + accession_no);
    }
    $("#radiology_pacs_viewer_iframe_modal").modal('show');
});

$(document).on("click", ".pacsViewReportBtn", function () {
    let pacs_report_prefix = $('#pacs_report_prefix').val();
    var accession_no = $(this).attr('data-accession-no');
    window.open(pacs_report_prefix + accession_no, '_blank');
});




function referPatient() {
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    var url = base_url + "/emr_lite/referPatient";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            req_type: "show",
            patient_id: patient_id,
            visit_id: visit_id,
        },
        beforeSend: function () {
            $('.patient_refer_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-user-plus');
        },
        success: function (response) {
            $('#referDoctor .modal-body').html(response.html);
            $('#referDoctor').modal('show');
        },
        complete: function () {
            $('.patient_refer_btn').find('i').addClass('fa-user-plus').removeClass('fa-spinner fa-spin');
        }
    });
}

function saveReferDoctor() {

    if ($('#doctor_ref_loaded').length > 0) {

        $('#refer_save_btn').prop('disabled', true);

        let patient_id = $('#patient_id').val();
        let visit_id = $('#visit_id').val();
        let encounter_id = $('#encounter_id').val();
        let _token = $('#c_token').val();
        let data_params = $('#referDoctorForm').serialize();
        let payment_status = $("input[name='payment_status']:checked").val();
        let reference_type = $('#reference_type').val();
        let booking_id = $('#booking_id').val()

        data_params += '&patient_id=' + patient_id;
        data_params += '&visit_id=' + visit_id;
        data_params += '&encounter_id=' + encounter_id;
        data_params += '&payment_status=' + payment_status;
        data_params += '&reference_type=' + reference_type;
        data_params += '&booking_id=' + booking_id;
        data_params += '&req_type=' + 'save';
        data_params += '&_token=' + _token;


        var url = base_url + "/emr_lite/referPatient";
        $.ajax({
            url: url,
            type: "POST",
            data: data_params,
            beforeSend: function () {
                $("#referDoctor .modal-body").LoadingOverlay("show", {
                    background: "rgba(89, 89, 89, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (parseInt(data) > 0) {
                    //when data set to 1 saved successfully
                    //when data set to 2 saved partialy some already reffered [unit wise refferal]
                    //when data set to 3 already reffered [single doctor refferal]

                    if (parseInt(data) == 1) {
                        Command: toastr["success"]('Doctor Referred Successfully');
                        $('#referDoctor').modal('hide');
                    }
                    else if (parseInt(data) == 2) {
                        Command: toastr["success"]('Some Doctors Already Referred');
                    }
                    else if (parseInt(data) == 3) {
                        Command: toastr["warning"]('Already Referred');
                    }

                    $('#doctor_ref').val('').trigger('change');
                    $('textarea[name="refer_notes"]').val('');
                } else {
                    Command: toastr["error"]('Insertion Failed.');
                }
            },
            error: function () {
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $('#refer_save_btn').prop('disabled', false);
                $("#referDoctor .modal-body").LoadingOverlay("hide");
            }
        });

    }

}


function transferPatient() {
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let booking_id = $('#booking_id').val()
    var url = base_url + "/emr_lite/transferPatient";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            req_type: "show",
            patient_id: patient_id,
            visit_id: visit_id,
            booking_id: booking_id
        },
        beforeSend: function () {
            $('.patient_transfer_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-exchange');
        },
        success: function (response) {
            if (response.status == 1) {
                $('#transferDoctor .modal-body').html(response.html);
                $('#transferDoctor').modal('show');
            } else {
                Command: toastr["error"]('Please select any patient.');
            }

        },
        complete: function () {
            $('.patient_transfer_btn').find('i').addClass('fa-exchange').removeClass('fa-spinner fa-spin');
        }
    });
}

function saveTransferPatient() {

    if ($('#doctor_ref_loaded').length > 0) {

        $('#transfer_save_btn').prop('disabled', true);
        let booking_id = $('#booking_id').val()

        let patient_id = $('#patient_id').val();
        let visit_id = $('#visit_id').val();
        let encounter_id = $('#encounter_id').val();
        let _token = $('#c_token').val();
        let data_params = $('#transferDoctorForm').serialize();
        let payment_status = $("input[name='payment_status']:checked").val();
        let reference_type = $('#reference_type').val();

        data_params += '&patient_id=' + patient_id;
        data_params += '&visit_id=' + visit_id;
        data_params += '&encounter_id=' + encounter_id;
        data_params += '&payment_status=' + payment_status;
        data_params += '&reference_type=' + reference_type;
        data_params += '&req_type=' + 'save';
        data_params += '&_token=' + _token;
        data_params += '&booking_id=' + booking_id;


        var url = base_url + "/emr_lite/transferPatient";
        $.ajax({
            url: url,
            type: "POST",
            data: data_params,
            beforeSend: function () {
                $("#transferDoctor .modal-body").LoadingOverlay("show", {
                    background: "rgba(89, 89, 89, 0.6)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (parseInt(data) > 0) {
                    //when data set to 1 saved successfully
                    //when data set to 2 saved partialy some already reffered [unit wise refferal]
                    //when data set to 3 already reffered [single doctor refferal]

                    if (parseInt(data) == 1) {
                        Command: toastr["success"]('Doctor Transfered Successfully');
                        $('#transferDoctor').modal('hide');
                        removeBeforeUnloadCallbak();
                        location.reload();

                    }
                    else if (parseInt(data) == 2) {
                        Command: toastr["success"]('Some Doctors Already Transfered');
                    }
                    else if (parseInt(data) == 3) {
                        Command: toastr["warning"]('Already Transfered');
                    }
                    else if (parseInt(data) == 4) {
                        Command: toastr["warning"]('Already Transfered');
                    }

                    $('#doctor_transfer').val('').trigger('change');
                    $('textarea[name="transfer_notes"]').val('');
                } else {
                    Command: toastr["error"]('Insertion Failed.');
                }
            },
            error: function () {
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $('#transfer_save_btn').prop('disabled', false);
                $("#transferDoctor .modal-body").LoadingOverlay("hide");
            }
        });

    }

}

$('.global_patient_search_textbox').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#AjaxDiv").html("");
            $("#patient_name_hidden").val("");
        } else {
            try {
                var url = base_url + "/emr_lite/ajaxSearchData";
                var param = {
                    op_no_search: op_no_search,
                    op_no_search_prog: 1
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#AjaxDiv").html(html).show();
                        $("#AjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {}
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('AjaxDiv', event);
    }
});

function fillGlobalPatientData(id, uhid, patient_name, input_id) {
    $('#AjaxDiv').hide();
    $(".op_patients_list_table_body").empty();
    $(".global_patient_search_textbox").val('');
    $(".op_patients_list_table_body").append('<tr class="ip_op_list_item col-md-12 no-padding" data-patient-id="' + id + '" data-booking-id="0"><td><div class="col-md-10 padding_sm"><div class="list_item_name">' + patient_name + '</div><div class="list_item_uhid">' + uhid + '</div></div></td><td></td></tr>');
    showHidePatientFilters(2);
    $(".op_patients_list_table_body tr:first").trigger('click');
}


function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}

function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}


$(document).on('click', ".vital_item", function () {
    var vital_master_id = $(this).attr('data-vital-master-id');
    $("#vitalGraphIndividualModal").modal('show');

    show_vital_history_by_vital_master(vital_master_id);
});

$('#prescription_print_config_modal').on('hidden.bs.modal', function () {
    // do something…

    $("#prescription_print_config_modal").attr("presc-head-id", '');
    $("#prescription_print_config_modal").attr("invest-head-id", '');
    $("#print_investigation_check").prop('checked', false);
});

function savePatientPersonalNotes() {
    var personal_notes = $(".private_notes_textarea").val();
    var patient_id = $("#patient_id").val();
    var visibility_status = 0;

    if (patient_id && personal_notes) {
        var url = base_url + "/emr_lite/savePatientPersonalNotes";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                personal_notes: personal_notes,
                patient_id: patient_id,
                visibility_status: visibility_status
            },
            beforeSend: function () {
                $(".savePatientPersonalNotesBtn").find('i').removeClass('fa-save').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {
                    $("#patientPersonalNotesModal").modal('hide');
                    Command: toastr["success"]('Successfully saved.');
                } else {
                    Command: toastr["error"]('Save failed.');
                }
            },
            error: function () {
                Command: toastr["error"]('Error.!');
            },
            complete: function () {
                $(".savePatientPersonalNotesBtn").find('i').removeClass('fa-spinner fa-spin').addClass('fa-save');
            }
        });
    } else {
        Command: toastr["error"]('Invalid details');
    }
}

$(document).on("click", '.toggleIpOpListBtn', function () {
    if ($(this).hasClass('leftArrow')) {
        $(this).removeClass('leftArrow');
        $(this).find('i').removeClass('fa-arrow-left').addClass('fa-arrow-right');
        $(".patient_details_inner_div").show();
        $(".ip_op_list_div").hide("slide", {
            direction: "left"
        }, 500);
        $('.patient_data_list_div').removeClass('col-md-9');
        $('.patient_data_list_div').addClass('col-md-12');
    } else {
        $(this).addClass('leftArrow');
        $(this).find('i').removeClass('fa-arrow-right').addClass('fa-arrow-left');
        $(".patient_details_inner_div").hide();
        $('.patient_data_list_div').removeClass('col-md-12');
        $('.patient_data_list_div').addClass('col-md-9');
        $(".ip_op_list_div").show("slide", {
            direction: "left"
        }, 500);
    }

    resetBootstrapTable();
})



function getDoctorSettings() {
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/getDoctorSettings";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            doctor_id: doctor_id
        },
        beforeSend: function () {},
        success: function (data) {
            $("#emr_lite_allergy_vital_config").val(data.emr_lite_allergy_vital_config);
            $("#quantity_auto_calculation_config").val(data.quantity_auto_calculation_config);
        },
        error: function () {
            Command: toastr["error"]('Error.!');
        },
        complete: function () {}
    });
}

function changeDoctor() {
    if ($('#change_doctor_id')) {
        var doctor_id = $('#change_doctor_id').val();
        if (doctor_id) {
            $('#doctor_id').val(doctor_id);
        }
    }
    getDoctorSettings();
    resetEmrLiteForChangeDoctor();
    viewDynamicNotesFields();
    getOpPatients(1);
    getDoctorPrescription();
    configureAllergyVitalsDiv();
    getDoctorBookmarkedNotes(1);
    fetchInvestigationGroups();
    addNewPrescriptionRow();
    fetchFrequencyList();
    fetchDirectionList();
}


function resetEmrLiteForChangeDoctor() {
    $('#patient_id').val('');
    $(".todays_visit_details_div").html('');
    $(".investigation_history_div").html('');
    $(".prescription_history_div").empty();
}

function viewDynamicNotesFields() {

    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    if (patient_id) {
        $('.showEyeMarkerDetails').show();
    }
    var visit_id = $("#visit_id").val();
    var encounter_id = $('#encounter_id').val();
    var url = base_url + "/eye_emr/getEyeEmrDynamicNotes";
    var html_string = '';
    var dataparams = {
        doctor_id: doctor_id,
        patient_id: patient_id,
        visit_id: visit_id,
        encounter_id: encounter_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#dynamic_notes_template').html('');
        },
        success: function (data) {
            $('#dynamic_notes_template').html(data.html);
            $(data.document_details).each(function (index, element) {
                html_string += "<div class='upperslider'><img class='upperslider_img' width='75' height='60' onclick='getImageUrl(this)' src='" + element + "'><div class='clearfix'></div></div>";
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            $('#editedPatientImageData').html(html_string);
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function fetchPatientLocalDetails() {

}
