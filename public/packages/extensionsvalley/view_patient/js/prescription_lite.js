var minimun_searchable_alphabet=$('#minimun_searchable_alphabet').val();
$(document).on('ready', function(){
    addNewPrescriptionRow();
    fetchFrequencyList();
    fetchDirectionList();

    $('#next_review_date').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate: new Date(),        
    }).on('dp.change', function (e) {
        takeNextAppointment();
    });
    item_allergyArray = [];
    generic_allergyArray = [];
});

$(document).on('change', '#check_all_medicine', function () {
    var is_checked = $(this).prop('checked');
    $('.check_remove_medicine').prop('checked', is_checked);
});

$(document).on('change', '.check_remove_medicine', function () {
    var totalCheckboxes = $('.check_remove_medicine').length;
    var totalChecked    = $('.check_remove_medicine:checked').length;
    if (totalCheckboxes == totalChecked) {        
        $('#check_all_medicine').prop('checked', true);
    } else {
        $('#check_all_medicine').prop('checked', false);
    }
});

dragElement(document.getElementById("medicine-list-div-row-listing"));
function dragElement(elemnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  elemnt.onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    elemnt.style.top = (elemnt.offsetTop - pos2) + "px";
    elemnt.style.left = (elemnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }
}


if($('#duration_unit_enable').val()==1){
    var enable_duration_unit=1;
}else{
   var enable_duration_unit=0;
}
var addItemGlobal=new Array;
// medicine search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="medicine_name"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var patient_id;
    var search_type = $("input[name='medicine_search_type']:checked").val();
    var med_list = $('.medicine-list-div-row-listing');
    if ($('#addItemtoFavouriteModel').is(':visible')) {
        patient_id = 1;
    } else {
        patient_id = $('#patient_id').val();
    }
    var allow_outside_medicine_config = $('#allow_outside_medicine_config').val();
    let tr = $(obj).closest('tr');
    window.tr = tr;

    if (search_string == "" || search_string.length < minimun_searchable_alphabet) {
        last_search_string = '';
        // console.log(minimun_searchable_alphabet);
        return false;
    } else {
        $(med_list).show();
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr_lite/medicine-search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    search_key_string: search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('.medicine-list-div-row-listing').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    // $('.item_description').blur();
                },
                success: function (data) {
                    let response = data.medicine_list;
                    var disable_zero_stock_items_prescription = data.disable_zero_stock_items_prescription ? data.disable_zero_stock_items_prescription : 0;
                    let res_data = "";
                    var search_list = $('.list-medicine-search-data-row-listing');
                    var show_price = $('#show_price_prescription').val();
            
                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {
                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let med_price = response[i].med_price;
                            let subcategory_id = response[i].subcategory_id;
                            let stock = response[i].stock;
                            let route = response[i].route;
                            let frequency = response[i].frequency;
                            let duration = response[i].duration;
                            let directions = response[i].directions;
                            let category_name = response[i].category_name;
                            let subcategory_name = response[i].subcategory_name;
                            let calculate_quantity = response[i].calculate_quantity;
                            let generic_id = response[i].generic_id;
                            var nostock = '';
                           
                            if (!stock) {
                                nostock = 'no-stock'
                            }
                            var medic_price = '';
                            if(show_price == 1){
                                 medic_price = "<td>"+ med_price +"</td>";
                            }
                            // <td>' + price + '</td>
                            if(disable_zero_stock_items_prescription == 0){
                                    res_data += '<tr data-item-code="' + item_code + '" data-calculate-quantity="' + calculate_quantity + '" data-generic-id="' + generic_id + '" data-item-desc="' + item_desc + '" data-genreric-name="' + generic_name + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '"><td class="' + nostock + '">' + item_desc + '</td><td>' + generic_name + '</td><td>' + category_name + '</td><td>' + subcategory_name + '</td><td>' + stock + '</td>' + medic_price +'<input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                            }else{
                                if(stock != 0 && stock != '' && stock != 0.00){

                                res_data += '<tr data-item-code="' + item_code + '" data-calculate-quantity="' + calculate_quantity + '" data-generic-id="' + generic_id + '" data-item-desc="' + item_desc + '" data-genreric-name="' + generic_name + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '"><td class="' + nostock + '">' + item_desc + '</td><td>' + generic_name + '</td><td>' + category_name + '</td><td>' + subcategory_name + '</td><td>' + stock + '</td>' + medic_price +'<input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                                }
                            }
                           
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="6">No Data Found..!</td></tr>';
                    }

                    if (response.length > 0) {
                        $('input[name="medicine_name"]').removeClass('outside-medicine');
                        $(window.tr).find('.item_description').css('color', '');
                    } else if(allow_outside_medicine_config == 0) {
                        $(window.tr).find('.item_description').css('color', 'red');
                        $(med_list).hide();
                        toastr.warning("Outside medicine can't be allowed");
                        $('input[name="medicine_name"]').addClass('outside-medicine');
                    }else{
                        $('input[name="medicine_name"]').addClass('outside-medicine');
                        $(window.tr).find('.item_description').css('color', '');
                        $(med_list).hide();
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.theadfix_wrapper').floatThead("reflow");
                    $('.medicine-list-div-row-listing').LoadingOverlay("hide");

                }
            });
        }, 500)

    }

});


$(document).on('click', '.list-medicine-search-data-row-listing > tr', function (event) {
    event.preventDefault();
    var item_code = $(event.currentTarget).attr('data-item-code');
    var calculate_quantity = $(event.currentTarget).attr('data-calculate-quantity');
    var stock = $(event.currentTarget).attr('data-stock');
    var generic_id = $(event.currentTarget).attr('data-generic-id');


    if ($(".item_description[data-item-code='" + item_code + "']").length > 0) {
        var confirm = window.confirm("Medicine already exist. Do you want to continue ?");
        if (!confirm) {
            return false;
        }
    }
    $(window.tr).find('.item_description').val($(event.currentTarget).attr('data-item-desc'));
    $(window.tr).find('.item_description').attr('title', $(event.currentTarget).attr('data-item-desc'));
    $(window.tr).find('.item_description').attr('data-item-code', item_code);
    $(window.tr).find('.frequency').attr('data-calculate-quantity', calculate_quantity);
    $(".medicine-list-div-row-listing").hide();
    addNewPrescriptionRow();
    if (enable_duration_unit == 1) {
        $(window.tr).find('.duration').focus();
        $(window.tr).find('.dosage').focus();
    } else {
        $(window.tr).find('.frequency').focus();
    }
    
    if (parseInt(stock) <= 0) {
        $(window.tr).find('.item_description').css('color', 'red');
    }
    if (!$("#addfovouritesmodal").is(":visible")) {
        checkSingleMedicineAllergy(item_code, generic_id);
    }

});

function checkAllergicMedicine(obj= '', type= 1) {
    var patient_allergic_data = []; 
    var allergy_row_data = ''  
    var count = 1;
    var parent_class = 'prescription_history_item';
    var find_class = 'prescription_history_item_checkbox';
    if (parseInt(type) == 2) {
        parent_class = 'prescription_bookmark_item';
        find_class = 'bookmarks_list_item';
    } 
    $(obj).parents('.' + parent_class).find('.' + find_class).each(function (key, value) {
        if (parseInt(type) == 1) {
            var generic_id = parseInt($(value).attr("data-generic-id"));
            var item_code = $(value).attr("data-item-code"); 
        } else {
            var generic_id = parseInt(atob($(value).attr("data-generic-id")));
            var item_code = atob($(value).attr("data-item-code")); 
        }
        var item_name = $(".item_description[data-item-code='" + item_code + "']").val(); 
        var index_item = item_allergyArray.indexOf(item_code);
        if (index_item !== -1) {
            patient_allergic_data.push(item_code + '::' + item_name + '::' + 0 + '::' + 'Brand');
        } else {
            var generic_name = $.trim($('.allergic_item[data-allergic-generic-id="' + generic_id + '"]').attr('data-allergic-name'));
            var index_alergic = generic_allergyArray.indexOf(generic_id);
            if (index_alergic !== -1) {
                var generic_string = "Generic (" + generic_name + ")";
                patient_allergic_data.push(item_code + '::' + item_name + '::' + generic_id + '::' + generic_string);
            }
        }
    });
    if (patient_allergic_data.length != 0) {
        if (patient_allergic_data.length > 1) {
            $.each(patient_allergic_data, function (key, val) {
                var split_data = val.split('::');
                var data_item_code = split_data[0];
                var data_item_desc = split_data[1];
                var data_string = split_data[3];
    
                allergy_row_data += "<tr>" +               
                "<td>" +
                "<div class='checkbox checkbox-info inline no-margin' style='margin-top:4px !important'>" +
                "<input type='checkbox' id='check_remove_medicine_"+ count +"' value='"+ data_item_code +"' class='check_remove_medicine'>" +
                "<label style='padding-left: 2px;' for='check_remove_medicine_" + count +"'>" +
                "</label></div></td>" + 
                "<td style='float:left'>" + data_item_desc + "</td>" +
                "<td>" + data_string +"</td></tr>";          
                count++;      
            });     
            $('#check_all_medicine').prop('checked', false);      
            $('#presc_comman_validate_modal_body').html(allergy_row_data);  
            $('#presc_comman_validate_modal_header').html('These medicines may cause an allergic reaction. Do you want to continue? ');  
            $("#presc_comman_validate_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            setTimeout(function () {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');  
            }, 200); 
        } else {
            var selected_item_code = patient_allergic_data[0] ? patient_allergic_data[0] : '';  
            var split_data = selected_item_code.split('::');
            var data_item_code = split_data[0];
            var data_generic_id = split_data[2];
            checkSingleMedicineAllergy(data_item_code, data_generic_id);
        }
    }  
}

function checkSingleMedicineAllergy(item_code ='', generic_id ='') {
    var allergy_itemwise_flag = 0;
    var allergy_genericwise_flag = 0;
    var allergy_itemwise_string = ''
    var index_item = item_allergyArray.indexOf(item_code);
    var input_element = $('.item_description[data-item-code="' + item_code + '"]');
    var tr_element = input_element.closest('tr');
    if (index_item !== -1) {
        allergy_itemwise_flag = 1;
        var item_name = $(".item_description[data-item-code='" + item_code + "']").val();
        allergy_itemwise_string = item_name + ' medicine may have the possibility of causing an allergic reaction in the patient. Do you want to continue with it?';
    } else {        
        generic_id = parseInt(generic_id);
        var index_alergic = generic_allergyArray.indexOf(generic_id);

        if (index_alergic !== -1) {
            allergy_genericwise_flag = 1;
            var generic_name = $.trim($('.allergic_item[data-allergic-generic-id="' + generic_id + '"]').attr('data-allergic-name'));
            allergy_itemwise_string = generic_name + ' generic may have the possibility of causing an allergic reaction in the patient. Do you want to continue with it?';
        }
    }
    if (parseInt(allergy_itemwise_flag) == 1 || parseInt(allergy_genericwise_flag) == 1) {
        bootbox.confirm({
            message: allergy_itemwise_string,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (!result) {                 
                    $(tr_element).find('.deletePrescriptionItemBtn').click();
                } 
            }
        });
    }
}

function addAllergy_medicine_save() {
    $('.check_remove_medicine').each(function () {
        if (!$(this).prop('checked')) {
            var input_element = $('.item_description[data-item-code="' + $(this).val() + '"]');
            var tr_element = input_element.closest('tr');
            $(tr_element).find('.deletePrescriptionItemBtn').click();
        }
    });
    $('#presc_comman_validate_modal').modal('hide');
    $(window.tr).find('.dosage').focus();
    setTimeout(function() {           
        if ($('.prescription_list_table_body tr').length == 0) {
            addNewPrescriptionRowWithFocus();
        }
    }, 100);
}
$(document).on('keydown', function (event) {
    if ($('.medicine-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-medicine-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                if ($(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':first-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr').last().addClass('active_row_item');
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').prev().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').last().removeClass('active_row_item');
                }
            } else if (event.which == 40) { // down
                if ($(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':last-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr').first().addClass('active_row_item');
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').next().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').first().removeClass('active_row_item');
                }
            } else if (event.which == 13) { // enter
                $(".list-medicine-search-data-row-listing").find('tr.active_row_item').trigger('click');
            } else if (event.which == 8) { // backspace
                $('.medicine-list-div-row-listing').hide();
                $(window.tr).find('.item_description').focus();
            } else if (event.which == 27) { // escape
                $('.medicine-list-div-row-listing').hide();
                $(window.tr).find('.item_description').focus();
            }
        }
    } else if ($('.frequency-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-frequency-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                if ($(".list-frequency-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-frequency-search-data-row-listing").find('tr.active_row_item').is(':first-child')) {
                    $(".list-frequency-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-frequency-search-data-row-listing").find('tr').last().addClass('active_row_item');
                } else {
                    $(".list-frequency-search-data-row-listing").find('tr.active_row_item').prev().addClass('active_row_item');
                    $(".list-frequency-search-data-row-listing").find('tr.active_row_item').last().removeClass('active_row_item');
                }
            } else if (event.which == 40) { // down
                if ($(".list-frequency-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-frequency-search-data-row-listing").find('tr.active_row_item').is(':last-child')) {
                    $(".list-frequency-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-frequency-search-data-row-listing").find('tr').first().addClass('active_row_item');
                } else {
                    $(".list-frequency-search-data-row-listing").find('tr.active_row_item').next().addClass('active_row_item');
                    $(".list-frequency-search-data-row-listing").find('tr.active_row_item').first().removeClass('active_row_item');
                }
            } else if (event.which == 13) {  // enter
                $(".list-frequency-search-data-row-listing").find('tr.active_row_item').trigger('click');
            } else if (event.which == 8) { // backspace
                // $('.frequency-list-div-row-listing').hide();
                // $(window.tr).find('.frequency').focus();
            } else if (event.which == 27) { // escape
                $('.frequency-list-div-row-listing').hide();
                // $(window.tr).find('.frequency').focus();
            }
        }
    } else if ($('.direction-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-direction-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                if ($(".list-direction-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-direction-search-data-row-listing").find('tr.active_row_item').is(':first-child')) {
                    $(".list-direction-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-direction-search-data-row-listing").find('tr').last().addClass('active_row_item');
                } else {
                    $(".list-direction-search-data-row-listing").find('tr.active_row_item').prev().addClass('active_row_item');
                    $(".list-direction-search-data-row-listing").find('tr.active_row_item').last().removeClass('active_row_item');
                }
            } else if (event.which == 40) { // down
                if ($(".list-direction-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-direction-search-data-row-listing").find('tr.active_row_item').is(':last-child')) {
                    $(".list-direction-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-direction-search-data-row-listing").find('tr').first().addClass('active_row_item');
                } else {
                    $(".list-direction-search-data-row-listing").find('tr.active_row_item').next().addClass('active_row_item');
                    $(".list-direction-search-data-row-listing").find('tr.active_row_item').first().removeClass('active_row_item');
                }
            } else if (event.which == 13) { // enter
                $(".list-direction-search-data-row-listing").find('tr.active_row_item').trigger('click');
            } else if (event.which == 8) { // backspace
                // $('.direction-list-div-row-listing').hide();
                // $(window.tr).find('.duration').focus();
            } else if (event.which == 27) { // escape
                $('.direction-list-div-row-listing').hide();
                // $(window.tr).find('.duration').focus();
            }
        }
    } else if ($('#op_noAjaxDiv').css('display') == 'block') {
        if (event.which == 13) {
            $("#op_noAjaxDiv").find('li.liHover').trigger('click');
        }
    }
})


function fetchFrequencyList(searchVal = '') {
    var base_url = $("#base_url").val();
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/fetchFrequencyList";
    var params = { searchVal: searchVal, doctor_id: doctor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".frequency-list-div-row-listing").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var row_data = '';
            // $(".frequency-list-div-row-listing").show();
            if (data.length > 0) {
                $.each(data, function (key, val) {
                    row_data += '<tr data-frequency-name="' + val.frequency + '" data-frequency-value="' + val.frequency_value + '" data-frequency-id="' + val.id + '"><td>' + val.frequency + '</td></tr>';
                });
                $(".list-frequency-search-data-row-listing").html(row_data);
                if (window.tr && $(window.tr).find('.item_description').val() != '') {

                }
            } else {
                row_data += '<tr class="new_record_to_add"><td><b>Frequency</b></td><td><b>Value</b></td><td><b>Action</b></td></tr><tr class="new_record_to_add freq_new_record"><td class="freq_name_new" >' + searchVal + '</td><td><input type="number" class="freq_value_new form-control" /></td><td><button type="button" class="btn btn-blue addNewFrequecnyBtn">Add</button></td></tr>';
                $(".list-frequency-search-data-row-listing").html(row_data);
            }






        },
        complete: function () {
            $(".frequency-list-div-row-listing").LoadingOverlay("hide");


        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

$(document).on('click', '.addNewFrequecnyBtn', function () {
    var freq_val = $(this).parents('tr').find('.freq_value_new').val();
    var freq_name = $(this).parents('tr').find('.freq_name_new').html();


    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/addNewUserFrequency";
    if(freq_val==""){
        Command: toastr["warning"]("Please enter value");
        return;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: {
            freq_val: freq_val,
            freq_name: freq_name
        },
        beforeSend: function () {
            $(".frequency-list-div-row-listing").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            console.log('succeess', data);
            if (data.status == 1) {
                toastr.success("Saved Succesfully.");
                $(".list-frequency-search-data-row-listing").html('');
                $(".frequency-list-div-row-listing").hide();
                $(window.tr).find('.frequency').val(freq_name);
                $(window.tr).find('.frequency').attr('data-frequency-value', freq_val);
                $(window.tr).find('.frequency').attr('data-frequency-id', data.frequency_id);
                $(window.tr).find('.duration').focus();

            } else if (data.status == 2) {
                toastr.error("Something went wrong.");
            }
        },
        complete: function () {
            $(".frequency-list-div-row-listing").LoadingOverlay("hide");


        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

    console.log()
})


function fetchDirectionList(search_instruction_string) {
    var base_url = $("#base_url").val();
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/fetchDirectionList";
    console.log(search_instruction_string);
    var params = { search_instruction_string: search_instruction_string, doctor_id: doctor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $("#directionSearchDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {

            var row_data = '';
            $.each(data, function (key, val) {
                row_data += '<tr data-instruction-name="' + val.instruction + '" ><td>' + val.instruction + '</td></tr>';
            });

            $(".list-direction-search-data-row-listing").html(row_data);


        },
        complete: function () {
            $("#directionSearchDiv").LoadingOverlay("hide");


        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function addNewPrescriptionRowWithFocus() {
    addNewPrescriptionRow();
    $(".prescription_list_table_body").find("tr:last").find('.item_description').focus();
}

function addNewPrescriptionRow() {
    if(enable_duration_unit==1){
        var duration_unit=$('#duration_unit').val();
        var duration_unit=JSON.parse(duration_unit);
        var dosage_unit=$('#dosage_unit').val();
        var dosage_unit=JSON.parse(dosage_unit);
        
        var option='<option id="" > Unit </option>';
        var selected='';
        var title='';     
        
        // $.each(duration_unit, function(key, val){
        //     if(val.name=='DAYS'){
        //         selected='selected';
        //         title=val.name;
    
        //     }else{
        //         selected=''; 
        //     }
        //     option += '<option value="'+val.id+'" data-unit-val="'+val.value+'"  title="'+ val.name +'" '+selected+'>'+ val.name +'</option>';
        // }); 
        var dosage_option = dosageUnitPopup(dosage_unit);
        var dosage_attrs = "data-dosage-val='' data-dosage-name=''";

        var duration_option = durationUnitPopup(duration_unit);
        var duration_attrs = "data-duration-id='' data-duration-val='' data-duration-name=''";

        $(".prescription_list_table_body").append("<tr class='row_class'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td><input  type='text' autocomplete='off' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' data-item-code='' /></td><td><input style='width: 65%;float: left;' type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' "+dosage_attrs+" />"+dosage_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 65%;float: left;' "+duration_attrs+"/>"+duration_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' /></td><td><input type='text'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction' /></td><td class='text-center'><i  onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i  onclick='removePrescriptionRow(this)' class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");

        // $(".prescription_list_table_body").append("<tr class='row_class'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td><input type='text' autocomplete='off' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' data-item-code='' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' / style='width: 57%;float: left;'><select title='"+title+"'  onchange='showTitle(this)' name='duration_unit' style='width: 21px;height: 23px;border:1px solid #21a4f1 !important' class='form-controlbottom-border-text borderless_textbox duration_unit'>"+option+"</select></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' /></td><td><input type='text'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction' /></td><td class='text-center'><i  onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i  onclick='removePrescriptionRow(this)' class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");
    }else{ 
        $(".prescription_list_table_body").append("<tr class='row_class'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td><input type='text' autocomplete='off' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' data-item-code='' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' /></td><td><input type='text'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction' /></td><td class='text-center'><i  onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i  onclick='removePrescriptionRow(this)' class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");

    }
   
    serialNoCalculation('row_class', 'row_count_class');
}
////new

$(document).on('click', '.dosage-list-group > li', function (event) {
    event.preventDefault();
    $(".dosage-list-group li.active").removeClass("active");
    $(this).addClass("active");
    var currentRow = $(this).closest('tr');
    var data_dose_val = $(this).attr('data-dose-val');
    var data_dose_name = $(this).attr('data-dose-name');
    $(currentRow).find(".dosage").attr('data-dosage-val', data_dose_val);
    $(currentRow).find(".dosage").attr('data-dosage-name', data_dose_name);
    var sub_str = data_dose_name.substr(0, 2);
    $(currentRow).find(".dosage-fa-list").removeClass('fa-arrow-circle-down');
    $(currentRow).find(".dosage-fa-list").html(sub_str.toLowerCase());
    $(currentRow).find(".btn_dosage_settings").attr('title', data_dose_name);
    $('.popDiv').hide();
});

function displayDoseSettings(obj) {
    $('.popDiv').hide();
    var currentRow = $(obj).closest("tr");
    $('.dosage-list-group > li').removeClass('active');

    if ($(currentRow).find('.popDiv').is(":visible")) {
        $(currentRow).find('.popDiv').hide();
    } else {
        var dosage_val = $(currentRow).find('.dosage').attr("data-dosage-val");
        $('.dosage-list-group > li[data-dose-val="'+dosage_val+'"]').addClass('active');
        $(currentRow).find('.popDiv').show();
    }
    $(currentRow).find('.close').click(function () {
        $(currentRow).find('.popDiv').hide();
    });
}

$(document).on('click', '.duration-list-group > li', function (event) {
    event.preventDefault();
    $(".duration-list-group li.active").removeClass("active");
    $(this).addClass("active");
    var currentRow = $(this).closest('tr');
    var data_unit_val = $(this).attr('data-unit-val');
    var data_unit_name = $(this).attr('data-unit-name');
    var data_unit_id = $(this).attr('data-unit-id');
    $(currentRow).find(".duration").attr('data-duration-val', data_unit_val);
    $(currentRow).find(".duration").attr('data-duration-id', data_unit_id);
    $(currentRow).find(".duration").attr('data-duration-name', data_unit_name);
    var sub_str = data_unit_name.substr(0, 1);
    $(currentRow).find(".duration-fa-list").removeClass('fa-arrow-circle-down');
    $(currentRow).find(".duration-fa-list").html(sub_str.toUpperCase());
    $(currentRow).find(".btn_duration_settings").attr('title', data_unit_name);
    $('.popDurationDiv').hide();
    $(currentRow).find('.duration').focus();
});

function displayDurationSettings(obj) {
    $('.popDurationDiv').hide();
    var currentRow = $(obj).closest("tr");
    $('.duration-list-group > li').removeClass('active');

    if ($(currentRow).find('.popDurationDiv').is(":visible")) {
        $(currentRow).find('.popDurationDiv').hide();
    } else {
        var duration_val = $(currentRow).find('.duration').attr("data-duration-id");
        $('.duration-list-group > li[data-unit-id="'+duration_val+'"]').addClass('active');
        $(currentRow).find('.popDurationDiv').show();
    }
    $(currentRow).find('.close').click(function () {
        $(currentRow).find('.popDurationDiv').hide();
    });
}
function dosageUnitPopup(dosage_unit, title='') {
    var pop_li = '';
    $.each(dosage_unit, function(key, val){
        pop_li += '<li class="list-group-item" data-dose-val="'+ val.id +'" data-dose-name="'+ val.name +'"><b>'+ val.name +'</b></li>';
    });
    var style = 'fa-arrow-circle-down';
    var short_title = '';
    var tooltip = '';
    if (title != '') {
        style = '';
        short_title = title.substr(0, 2);
        short_title = short_title.toLowerCase();
        tooltip = 'title='+title;
    }

    var popupDiv ='<button type="button" class="no-padding btn_dosage_settings" onclick="displayDoseSettings(this);" style="width: 25px; height: 23px; border: none;margin: 0px -6px 0px 0px;font-size: 15px;" '+tooltip+'><i class="fa '+style+' dosage-fa-list">'+short_title+'</i></button> <div class="popupDiv"><div class="popDiv" style="display: none;"> <div class="col-md-1 pull-right"><button type=" button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: #fff;font-weight: 100;font-size: 12px;" aria-hidden="true">X</span></button></div> <div class="col-md-12" style="top: -25px;"><ul class="list-group dosage-list-group" style="cursor: pointer">'+ pop_li +'</ul></div></div></div>';

    return popupDiv;
}

function durationUnitPopup(duration_unit, title='') {
    var pop_li = '';
    $.each(duration_unit, function(key, val){
        pop_li += '<li class="list-group-item" data-unit-id="'+ val.id +'" data-unit-val="'+ val.value +'" data-unit-name="'+ val.name +'"><b>'+ val.name +'</b></li>';
    });
    var style = 'fa-arrow-circle-down';
    var short_title = '';
    var tooltip = '';
    if (title != '') {
        style = '';
        short_title = title.substr(0, 1);
        short_title = short_title.toUpperCase();
        tooltip = 'title='+title;
    }

    var popupDiv ='<button type="button" class="no-padding btn_duration_settings" onclick="displayDurationSettings(this);" style="width: 25px; height: 23px; border: none;margin: 0px -6px 0px 0px;font-size: 15px;" '+tooltip+'><i class="fa '+style+' duration-fa-list">'+short_title+'</i></button> <div class="popupDurationDiv"><div class="popDurationDiv" style="display: none;"> <div class="col-md-1 pull-right"><button type=" button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: #fff;font-weight: 100;font-size: 12px;" aria-hidden="true">X</span></button></div> <div class="col-md-12" style="top: -25px;"><ul class="list-group duration-list-group" style="cursor: pointer">'+ pop_li +'</ul></div></div></div>';

    return popupDiv;
}
//end

function resetPrescriptionList() {
    $(".prescription_list_table_body").find('tr').remove();
    addNewPrescriptionRow();
}

function showTitle(e){
   var title=$(e).children("option:selected").text();
   $(e).prop('title',title);
}

function removePrescriptionRow(row) {
    var detail_id = $(row).parents('tr').attr('data-detail-id') ? $(row).parents('tr').attr('data-detail-id') : '';
    var item_code = $(row).parents('tr').find('.item_description').attr('data-item-code');
    if (detail_id != "") {
        $('.prescription_history_item_checkbox[data-detail-id="' + detail_id + '"]').prop('checked', false);
    } else if (item_code != "") {
        $('.item_code_prescription' + item_code).prop('checked', false);
    }
    $(row).parents('tr').remove();

    serialNoCalculation('row_class', 'row_count_class');
}


function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}


function addDoctorPrescription(medicine_name, frequency, duration,duration_unit_id, quantity, notes, item_code, obj, dose = '',dosage_unit_id = '', generic_id = '') {
    if ($(".item_description[data-item-code='" + item_code + "']").length > 0) {
        var confirm = window.confirm("Medicine already exist. Do you want to continue ?");
        if (!confirm) {
            return false;
        }
    }

    if ($(".row_class:last").find("input[name='medicine_name']").val() == '') {
        $(".row_class:last").remove();
    }
   
    var status = $(obj).is(":checked");
    item_code = atob(item_code);
    if (status) {
        $('.item_code_prescription' + item_code).prop('checked', true);
        medicine_name = atob(medicine_name);
        frequency = atob(frequency);
        notes = atob(notes);
        dose = atob(dose);
        dosage_unit_id = atob(dosage_unit_id);
        generic_id = atob(generic_id);
        var frequency_id = atob($(obj).parents('.bookmarks_list_item').attr('data-frequency-id'));
        var frequency_value = atob($(obj).parents('.bookmarks_list_item').attr('data-frequency-value'));
       if (enable_duration_unit == 1) {
        var duration_unit=$('#duration_unit').val();
        var duration_unit=JSON.parse(duration_unit);
        var dosage_unit = $('#dosage_unit').val();
        var dosage_unit = JSON.parse(dosage_unit);

        var option='<option id="" > Unit </option>';
        var selected='';
        var title = '';
        var title1 = '';
        // console.log(duration_unit);
        // $.each(duration_unit, function(key, val){
        //     if(val.id==duration_unit_id){
        //         selected='selected';
        //         title=val.name;
    
        //     }else{
        //         selected=''; 
        //     }
        //     option += '<option value="'+val.id+'" data-unit-val="'+val.value+'"   title="'+ val.name +'" '+selected+'>'+ val.name +'</option>';
        // });

        $.each(duration_unit, function(key, val){
            if (val.id == duration_unit_id) {
                title = val.name;
                dur_val = val.value;
            }
        });
        $.each(dosage_unit, function(key, val){
            if (val.id == dosage_unit_id) {
                title1 = val.name;
            }
        });
        var dosage_option = dosageUnitPopup(dosage_unit, title1);
        var dosage_attrs = "data-dosage-val='"+dosage_unit_id+"' data-dosage-name='"+title1+"'";

        var duration_option = durationUnitPopup(duration_unit, title);
        var duration_attrs = "data-duration-id='"+duration_unit_id+"' data-duration-val='"+dur_val+"' data-duration-name='"+title+"'";

        $(".prescription_list_table_body").append("<tr class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td><input type='text' autocomplete='off' title='" + medicine_name + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + medicine_name + "' data-item-code='"+item_code+"' /></td><td><input style='width: 65%;float: left;' type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' value='" + dose + "' "+dosage_attrs+" />"+dosage_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 65%;float: left;' value='" + duration + "' "+duration_attrs+" />"+duration_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "'  /></td><td><input type='text'  title='" + notes + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + notes + "'  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");
       }else{
        $(".prescription_list_table_body").append("<tr class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td><input type='text' autocomplete='off' title='" + medicine_name + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + medicine_name + "' data-item-code='"+item_code+"' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration'  value='" + duration + "' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "'  /></td><td><input type='text'  title='" + notes + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + notes + "'  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");

       }
       checkSingleMedicineAllergy(item_code, generic_id);
    } else {
        $('.item_code_prescription_list' + item_code).remove();
        $('.item_code_prescription' + item_code).prop('checked', false);
    }
    serialNoCalculation('row_class', 'row_count_class');
}
$(document).on('focus', '.frequency', function () {
    var quantity_auto_calculation_config = $("#quantity_auto_calculation_config").val();
    if (quantity_auto_calculation_config == 1) {       
        $(".frequency-list-div-row-listing").show();
        $(".direction-list-div-row-listing").hide();        
        var obj = $(this);
        let tr = $(obj).closest('tr');
        window.tr = tr;
        fetchDirectionList('a');
        if ($("#addfovouritesmodal").is(":visible")) {
            $(".frequency-list-div-row-listing").css("right", "22%");
            $(".frequency-list-div-row-listing").css("top", "0%");
        } else {
            $(".frequency-list-div-row-listing").css("right", "32%");
            $(".frequency-list-div-row-listing").css("top", "15%");
        }

    }
});

$(document).on('focus', '.direction', function (e) {
    $(".direction-list-div-row-listing").show();
    $(".frequency-list-div-row-listing").hide();
    $(".list-direction-search-data-row-listing").html('<span>Search Related Directions.</span>');
    
    var obj = $(this);
    let tr = $(obj).closest('tr');
    console.log(window);
    window.tr = tr;
    var searchVal = $(this).val();
    if (e.which != 8 || searchVal == '') {
            fetchDirectionList('a');
    } 
    if ($("#addfovouritesmodal").is(":visible")) {
        $(".direction-list-div-row-listing").css("right", "0%");
        $(".direction-list-div-row-listing").css("top", "0%");
    } else {
        $(".direction-list-div-row-listing").css("right", "10%");
        $(".direction-list-div-row-listing").css("top", "15%");
    }
});
$(document).on('click', '.presc_bookmark_head_left', function () {
    var book_mark = $(this).parents('.prescription_bookmark_item');
    if (book_mark.find('.bookmarks_list').is(':visible')) {
        book_mark.find('.bookmarks_list').hide();
    } else {
        book_mark.find('.bookmarks_list').show();
    }

})
function getDoctorPrescription() {
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/fetchDoctorPrescription";
    var doctor_id = $('#doctor_id').val();
    var params = { doctor_id: doctor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".prescription_bookmark_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) { 
            $(".prescription_bookmark_div").html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $(".prescription_bookmark_div").LoadingOverlay("hide");

        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function addtoFavorite(row, from_type = 0) {

    var that = row;
    addItemGlobal = [];
    var base_url = $("#base_url").val();
    var doctor_id = $('#doctor_id').val();
    var url = base_url + "/emr_lite/addtoPrescriptionFavorite";
    if (from_type != 1) {
        // var item_code = $(row).parents('tr').find('.item_description').attr('data-item-code');
        // var item_desc = $(row).parents('tr').find('.item_description').val();
        // var frequency = $(row).parents('tr').find('.frequency').val();
        // var frequency_val = $(row).parents('tr').find('.frequency').attr('data-frequency-value');
        // var duration = $(row).parents('tr').find('.duration').val();
        // var duration_unit = $(row).parents('tr').find('.duration_unit').val();
        // console.log(duration_unit);
        // var quantity = $(row).parents('tr').find('.quantity').val();
        // var direction = $(row).parents('tr').find('.direction').val();
        var item_code = $(row).parents('tr').find('.item_description').attr('data-item-code');
        var item_desc = $(row).parents('tr').find('.item_description').val();   
        var frequency = $(row).parents('tr').find('.frequency').val();
        var frequency_val = $(row).parents('tr').find('.frequency').attr('data-frequency-value');
        var frequency_id = $(row).parents('tr').find('.frequency').attr('data-frequency-id');
        var duration = $(row).parents('tr').find('.duration').val();
        var duration_unit = $(row).parents('tr').find('.duration').attr('data-duration-id');
        duration_unit = (duration_unit != undefined) ? duration_unit : 1;
        var dose = $(row).parents('tr').find('.dosage').val();
        dose = (dose != undefined) ? dose : '';
        var dose_unit = $(row).parents('tr').find('.dosage').attr('data-dosage-val');
        dose_unit = (dose_unit != undefined) ? dose_unit : 0;
        var quantity = $(row).parents('tr').find('.quantity').val();
        var direction = $(row).parents('tr').find('.direction').val();

        addItemGlobal.push({
            'item_code': item_code,
            'medicine_name': item_desc,
            'frequency': frequency,
            'frequency_value': frequency_val,
            'frequency_id': frequency_id,
            'duration': duration,
            'quantity': quantity,
            'notes': direction,
            'dose': dose,
            'dose_unit': dose_unit,
            'duration_unit': duration_unit ?? 1,
        })

    } else {
        item_code = null;
        item_desc = 'Favourites';
    }
    if (item_code || from_type == 1) {
        var params = { item_code: item_code, from_type: from_type, doctor_id: doctor_id };
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            beforeSend: function () {
                if (from_type == 1) {
                    $('#favMedOrGrpWithoutItem').attr('disabled', true);
                    $(that).find('i').removeClass('fa fa-star');
                    $(that).find('i').addClass('fa fa-spinner fa-spin');
                    $('#addItemtoFavouriteModelHeader').html(htmlDecode(item_desc));
                } else {
                    $(that).attr('disabled', true);
                    $(that).removeClass('fa fa-star');
                    $(that).addClass('fa fa-spinner fa-spin');
                    $('#addItemtoFavouriteModelHeader').html(htmlDecode(item_desc));
                }


            },
            success: function (data) {
                $('#addItemtoFavouriteModelDiv').html(data);
                $('#addfovouritesmodal').css('max-width', '1000px');
                $("#addItemtoFavouriteModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            complete: function () {
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                if (from_type == 1) {
                    $('#favMedOrGrpWithoutItem').attr('disabled', false);
                    $(that).find('i').removeClass('fa fa-spinner fa-spin');
                    $(that).find('i').addClass('fa fa-star');
                } else {
                    $(that).attr('disabled', false);
                    $(that).removeClass('fa fa-spinner fa-spin');
                    $(that).addClass('fa fa-star');
                }

            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    } else {
        toastr.warning("Please select any item");
    }

}


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}



$(document).on('click', '.list-frequency-search-data-row-listing > tr', function (event) {
    event.preventDefault();
    if ($(this).hasClass('new_record_to_add')) {
        return;
    }
    var frequency_name = $(event.currentTarget).attr('data-frequency-name');
    var frequency_value = $(event.currentTarget).attr('data-frequency-value');
    var frequency_id = $(event.currentTarget).attr('data-frequency-id');

    $(window.tr).find('.frequency').val(frequency_name);
    $(window.tr).find('.frequency').attr('data-frequency-value', frequency_value);
    $(window.tr).find('.frequency').attr('data-frequency-id', frequency_id);
    $(window.tr).find('.duration').focus();
    $(".frequency-list-div-row-listing").hide();

});


$(document).on('click', '.list-direction-search-data-row-listing > tr', function (event) {
    event.preventDefault();
    var instruction_name = $(event.currentTarget).attr('data-instruction-name');

    $(window.tr).find('.direction').val(instruction_name);
    $(".direction-list-div-row-listing").hide();

});


//duration
$(document).on('keyup', '.duration', function () {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9]/g, ""));
    if (cur_obj_value.length > 3) {
        $(cur_obj).val(Number(cur_obj_value.slice(0, 3)));
    }
});

//quantity
$(document).on('keyup', '.quantity', function () {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9.]/g, ""));
    if (cur_obj_value.length > 4) {
        $(cur_obj).val(Number(cur_obj_value.slice(0, 3)));
    }
});


$(document).on('blur focus', '.duration', function () {
    let search_duration = $(this).closest('tr').find('.duration').val();
    // let duration_unit = $(this).closest('tr').find('.duration_unit').val();
    let duration_unit = $(this).closest('tr').find('.duration').attr('data-duration-id');
    let item_stock = $(this).closest('tr').find('.item_description').attr('data-item-stock');
    let search_freq_value_hidden = $(this).closest('tr').find('.frequency').attr('data-frequency-value');
    let calculate_quantity = $(this).closest('tr').find('.frequency').attr('data-calculate-quantity') ? $(this).closest('tr').find('.frequency').attr('data-calculate-quantity') : 1;
    var quantity_auto_calculation_config = $("#quantity_auto_calculation_config").val();
    if (search_duration != "" && search_duration != undefined && search_freq_value_hidden != "" && search_freq_value_hidden != undefined && parseInt(calculate_quantity) > 0 && quantity_auto_calculation_config == 1) {
       if(enable_duration_unit==1 &&(duration_unit != "" && duration_unit != undefined)){
        if (duration_unit==3) {
            unit_value=30;
        } else if(duration_unit==2) {
            unit_value=7;
        }else if(duration_unit==1){
            unit_value=1;
        }else{
            unit_value=1;
        }
        var qty = parseFloat(search_duration) * parseFloat(search_freq_value_hidden)*parseFloat(unit_value);

       }else{
        var qty = parseFloat(search_duration) * parseFloat(search_freq_value_hidden);

       }      
       $(this).closest('tr').find('.quantity').val(qty);
    }

});
$(document).on('change', '.duration_unit', function () {
    let search_duration = $(this).closest('tr').find('.duration').val();
    let duration_unit = $(this).closest('tr').find('.duration_unit').val();
    let search_freq_value_hidden = $(this).closest('tr').find('.frequency').attr('data-frequency-value');
    let calculate_quantity = $(this).closest('tr').find('.frequency').attr('data-calculate-quantity') ? $(this).closest('tr').find('.frequency').attr('data-calculate-quantity'): 1;
    var quantity_auto_calculation_config = $("#quantity_auto_calculation_config").val();
    if (search_duration != "" && search_duration != undefined && search_freq_value_hidden != "" && search_freq_value_hidden != undefined && parseInt(calculate_quantity) > 0 && quantity_auto_calculation_config == 1) {
       if(enable_duration_unit==1){
        if (duration_unit==3) {
            unit_value=30;
        } else if(duration_unit==2) {
            unit_value=7;
        }else if(duration_unit==1){
            unit_value=1;
        }else{
            unit_value=1;
        }
        var qty = parseFloat(search_duration) * parseFloat(search_freq_value_hidden)*parseFloat(unit_value);

       }else{
        var qty = parseFloat(search_duration) * parseFloat(search_freq_value_hidden);

       }
        $(this).closest('tr').find('.quantity').val(qty);
    }
})


$(document).on("click", '.prescription_history_item_checkbox', function () {



    var detail_id = $(this).attr('data-detail-id');
    var item_code = $(this).attr('data-item-code');
    var frequency_name = $(this).attr('data-frequency-name');
    var frequency_value = $(this).attr('data-frequency-value');
    var frequency_id = $(this).attr('data-frequency-id');
    var direction = $(this).attr('data-item-direction');
    var quantity = $(this).attr('data-item-quantity');
    var item_desc = $(this).attr('data-item-desc');
    var duration = $(this).attr('data-item-duration');
    var duration_unit_id = $(this).attr('data-item-duration_unit');
    var duration = $(this).attr('data-item-duration');
    var bill_converted_status = $(this).attr('data-bill-converted-status');
    var item_dosage = $(this).attr('data-item-dosage') ? $(this).attr('data-item-dosage') : '';
    var item_dosage_unit = $(this).attr('data-item-dosage-unit') ? $(this).attr('data-item-dosage-unit') : '';
    var data_generic_id = $(this).attr('data-generic-id') ? $(this).attr('data-generic-id') : '';
   

    if ($(this).prop("checked")) {

        if ($(".item_description[data-item-code='" + item_code + "']").length > 0) {
            var confirm = window.confirm("Medicine already exist. Do you want to continue ?");
            if (!confirm) {
                return false;
            }
        }

        if ($(".row_class:last").find("input[name='medicine_name']").val() == '') {
            $(".row_class:last").remove();
        }
       if (enable_duration_unit == 1){
        var duration_unit=$('#duration_unit').val();
        var duration_unit=JSON.parse(duration_unit);
        var dosage_unit = $('#dosage_unit').val();
        var dosage_unit = JSON.parse(dosage_unit);

        var option='<option id="" > Unit </option>';
        var selected='';
        var title='';
        var title_d='';
        var dur_val = '';

        // $.each(duration_unit, function(key, val){
        //     if(val.id==duration_unit_id){
        //         selected='selected';
        //         title=val.name;
    
        //     }else{
        //         selected=''; 
        //     }
        //     option += '<option value="'+val.id+'" data-unit-val="'+val.value+'"  title="'+ val.name +'" '+selected+'>'+ val.name +'</option>';
        // });

        $.each(duration_unit, function(key, val){
            if (val.id == duration_unit_id) {
                title = val.name;
                dur_val = val.value;
            }
        });
        $.each(dosage_unit, function(key, val){
            if (val.id == item_dosage_unit) {
                title_d = val.name;
            }
        });

        var dosage_option = dosageUnitPopup(dosage_unit, title_d);
        var dosage_attrs = "data-dosage-val='"+item_dosage_unit+"' data-dosage-name='"+title_d+"'";

        var duration_option = durationUnitPopup(duration_unit, title);
        var duration_attrs = "data-duration-id='"+duration_unit_id+"' data-duration-val='"+dur_val+"' data-duration-name='"+title+"'";        

        $(".prescription_list_table_body").append("<tr data-detail-id='"+detail_id+"' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' /></td><td><input style='width: 65%;float: left;' type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' value='" + item_dosage + "' "+dosage_attrs+"/>"+dosage_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 65%;float: left;' value='" + duration + "' "+duration_attrs+"/>"+duration_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "'  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "'  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");
       }else{
        $(".prescription_list_table_body").append("<tr data-detail-id='"+detail_id+"' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration'  value='" + duration + "' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "'  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "'  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'><i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i></td></tr>");

       }
       checkSingleMedicineAllergy(item_code, data_generic_id);
    } else {
        if (item_code != "") {
            $(".prescription_list_table_body").find('.item_description[data-item-code="' + item_code + '"]').parents('tr').remove();
        }
    }

    serialNoCalculation('row_class', 'row_count_class');



});

$(document).on("click", '.close_btn_med_search', function () {
    $(this).parent().hide();
})
$(document).on("click", '.close_btn_frequency_search', function () {
    $(this).parent().hide();
})
$(document).on("click", '.close_btn_direction_search', function () {
    $(this).parent().hide();
})
$(document).on("click", '.copyPrescriptionButton', function () {
    // $(".prescription_list_table_body").empty();
    $("#prescription_head_id").val('');

    $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
        if ($(val).val() == '') {
            $(val).parents('tr').remove();
        }
    });

    $(this).parents('.prescription_history_item').find('.prescription_history_item_checkbox').each(function (key, value) {
        // var detail_id = $(value).attr("data-detail-id");
        var detail_id = '';
        var frequency_name = $(value).attr("data-frequency-name");
        var frequency_value = $(value).attr("data-frequency-value");
        var frequency_id = $(value).attr("data-frequency-id");
        var quantity = $(value).attr("data-item-quantity");
        var duration = $(value).attr("data-item-duration");
        var duration_unit_id = $(value).attr("data-item-duration_unit");
        var direction = $(value).attr("data-item-direction");
        var item_desc = $(value).attr("data-item-desc");
        var item_code = $(value).attr("data-item-code");
        var bill_converted_status = $(value).attr("data-bill-converted-status");
        var item_dosage = $(this).attr('data-item-dosage') ? $(this).attr('data-item-dosage') : '';
        var item_dosage_unit = $(this).attr('data-item-dosage-unit') ? $(this).attr('data-item-dosage-unit') : '';

       if (enable_duration_unit == 1) {
        var duration_unit=$('#duration_unit').val();
        var duration_unit=JSON.parse(duration_unit);
        var dosage_unit=$('#dosage_unit').val();
        var dosage_unit=JSON.parse(dosage_unit);
        
        var option='<option id="" > Unit </option>';
        var selected='';
        var title ='';
        var title_d ='';
   
        // $.each(duration_unit, function(key, val){
        //     if(val.id==duration_unit_id){
        //         selected='selected';
        //         title=val.name;
    
        //     }else{
        //         selected=''; 
        //     }
        //     option += '<option value="'+val.id+'" data-unit-val="'+val.value+'"  title="'+ val.name +'" '+selected+'>'+ val.name +'</option>';
        // });

        var remove_presc_btn = "<i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i>";
        var disabled = '';

        
        var dur_val = '';
        $.each(duration_unit, function(key, val){
            if (val.id == duration_unit_id) {
                title = val.name;
                dur_val = val.value;
            }
        });
        $.each(dosage_unit, function(key, val){
            if (val.id == item_dosage_unit) {
                title_d = val.name;
            }
        });

        var dosage_option = dosageUnitPopup(dosage_unit, title_d);
        var dosage_attrs = "data-dosage-val='"+item_dosage_unit+"' data-dosage-name='"+title_d+"'";

        var duration_option = durationUnitPopup(duration_unit, title);
        var duration_attrs = "data-duration-id='"+duration_unit_id+"' data-duration-val='"+dur_val+"' data-duration-name='"+title+"'";        

        $(".prescription_list_table_body").append("<tr data-detail-id='"+detail_id+"' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' "+disabled+" /></td><td><input style='width: 65%;float: left;' type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' value='" + item_dosage + "' "+dosage_attrs+"/>"+dosage_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 65%;float: left;' value='" + duration + "' "+duration_attrs+"/>"+duration_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "' "+disabled+"  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "' "+disabled+"  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown prescription_history_item_checkbox'></i></td><td class='text-center'>"+remove_presc_btn+"</td></tr>");
       } else {
        var remove_presc_btn = "<i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i>";
        var disabled = '';

        $(".prescription_list_table_body").append("<tr data-detail-id='"+detail_id+"' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration'  value='" + duration + "' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "' "+disabled+"  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "' "+disabled+"  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'>"+remove_presc_btn+"</td></tr>");
       }
    });

    serialNoCalculation('row_class', 'row_count_class');

    // $(".prescription_history_item_checkbox").attr("disabled", true);
    $(".updatePrescriptionBtnDiv").hide();
    if ($("#investigation_head_id").val() == "" && $("#ca_head_id").val() == "") {
        $(".saveButtonDiv").show();
    }
    checkAllergicMedicine(this, 1);
})

$(document).on("click", '.editPrescriptionButton', function () {
    $(".prescription_list_table_body").empty();
    var prescription_id = $(this).parents('.prescription_history_item').attr('data-head-id');
    var advice_given = $(this).parents('.prescription_history_item').attr('data-advice_given');
    var next_review_remarks = $(this).parents('.prescription_history_item').attr('data-next_review_remarks');
    $("#prescription_head_id").val(prescription_id);
    $(".prescription_advice_textarea").val(advice_given);
    $("#next_review_remarks").val(next_review_remarks);
    $(this).parents('.prescription_history_item').find('.prescription_history_item_checkbox').each(function (key, value) {
        var detail_id = $(value).attr("data-detail-id");
        var frequency_name = $(value).attr("data-frequency-name");
        var frequency_value = $(value).attr("data-frequency-value");
        var frequency_id = $(value).attr("data-frequency-id");
        var quantity = $(value).attr("data-item-quantity");
        var duration = $(value).attr("data-item-duration");
        var duration_unit_id = $(value).attr("data-item-duration_unit");
        var direction = $(value).attr("data-item-direction");
        var item_desc = $(value).attr("data-item-desc");
        var item_code = $(value).attr("data-item-code");
        var bill_converted_status = $(value).attr("data-bill-converted-status");
        var item_dosage = $(value).attr('data-item-dosage') ? $(value).attr('data-item-dosage') : '';
        var item_dosage_unit = $(value).attr('data-item-dosage-unit') ? $(value).attr('data-item-dosage-unit') : '';

        var remove_presc_btn = '';
        var disabled = '';
        if (bill_converted_status == "0") {
            remove_presc_btn = "<i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i>";

        } else {
            disabled = "disabled='disabled' readonly";
        }
       if (enable_duration_unit == 1){
        var duration_unit=$('#duration_unit').val();
        var duration_unit=JSON.parse(duration_unit);
        var dosage_unit = $('#dosage_unit').val();
        var dosage_unit = JSON.parse(dosage_unit);

        var option='<option id="" > Unit </option>';
        var selected='';
        var title='';
        var title_d='';
        var dur_val='';
        console.log(duration_unit);
        // $.each(duration_unit, function(key, val){
        //     if(val.id==duration_unit_id){
        //         selected='selected';
        //         title=val.name;
    
        //     }else{
        //         selected=''; 
        //     }
        //     option += '<option value="'+val.id+'" data-unit-val="'+val.value+'"  title="'+ val.name +'" '+selected+'>'+ val.name +'</option>';
        // });

        $.each(duration_unit, function(key, val){
            if (val.id == duration_unit_id) {
                title = val.name;
                dur_val = val.value;
            }
        });
        $.each(dosage_unit, function(key, val){
            if (val.id == item_dosage_unit) {
                title_d = val.name;
            }
        });
        
        var dosage_option = dosageUnitPopup(dosage_unit, title_d);
        var dosage_attrs = "data-dosage-val='"+item_dosage_unit+"' data-dosage-name='"+title_d+"'";

        var duration_option = durationUnitPopup(duration_unit, title);
        var duration_attrs = "data-duration-id='"+duration_unit_id+"' data-duration-val='"+dur_val+"' data-duration-name='"+title+"'";    

        $(".prescription_list_table_body").append("<tr data-detail-id='"+detail_id+"' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' "+disabled+" /></td><td><input style='width: 65%;float: left;' type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' value='" + item_dosage + "' "+dosage_attrs+"/>"+dosage_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 65%;float: left;' value='" + duration + "' "+duration_attrs+"/>"+duration_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "' "+disabled+"  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "' "+disabled+"  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'>"+remove_presc_btn+"</td></tr>");
       }else{
        $(".prescription_list_table_body").append("<tr data-detail-id='"+detail_id+"' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration'  value='" + duration + "' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "' "+disabled+"  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "' "+disabled+"  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'>"+remove_presc_btn+"</td></tr>");

       }
    });

    serialNoCalculation('row_class', 'row_count_class');

    $(".prescription_history_item_checkbox").attr("disabled", true);
    $(".saveButtonDiv").hide();
    $(".updatePrescriptionBtnDiv").show();
})


$(document).on("click", '.applyPrescriptionGroupBtn', function () {
    if ($(".row_class").find("input[name='medicine_name']").val() == '') {
        $(".row_class").remove();
    }

    $(this).parents('.prescription_bookmark_item').find('.bookmarks_list_item').each(function (key, value) {
        var item_code = atob($(value).attr('data-item-code'));
        var item_desc = atob($(value).attr('data-medicine-name'));
        var frequency_name = atob($(value).attr('data-frequency-name'));
        var frequency_value = atob($(value).attr('data-frequency-value'));
        var frequency_id = atob($(value).attr('data-frequency-id'));
        var duration = atob($(value).attr('data-item-duration'));
        var frequency_id = atob($(value).attr('data-frequency-id'));
        var duration_unit_id = atob($(value).attr('data-item-duration_unit'));
        var quantity = atob($(value).attr('data-item-quantity'));
        var direction = atob($(value).attr('data-item-direction'));
        var dose = atob($(value).attr('data-item-dose'));
        var dosage_unit_id = atob($(value).attr('data-item-dosage_unit'));
        
        var remove_presc_btn = "<i onclick=\"removePrescriptionRow(this)\" class='fa fa-times red deletePrescriptionItemBtn'></i>";
        var disabled = '';
        if(enable_duration_unit==1){
            var duration_unit=$('#duration_unit').val();
            var duration_unit=JSON.parse(duration_unit);
            var dosage_unit = $('#dosage_unit').val();
            var dosage_unit = JSON.parse(dosage_unit);
            var option='<option id="" > Unit </option>';
            var selected='';
            var title='';
            var title1 = '';
            // console.log(duration_unit);
            // $.each(duration_unit, function(key, val){
            //     if(val.id==duration_unit_id){
            //         selected='selected';
            //         title=val.name;
        
            //     }else{
            //         selected=''; 
            //     }
            //     option += '<option value="'+val.id+'" data-unit-val="'+val.value+'"  title="'+ val.name +'" '+selected+'>'+ val.name +'</option>';
            // });

            var dur_val = '';
            $.each(duration_unit, function(key, val){
                if (val.id == duration_unit_id) {
                        title = val.name;
                        dur_val = val.value;
                }
            });
            $.each(dosage_unit, function(key, val){
                if (val.id == dosage_unit_id) {
                        title1 = val.name;
                }
            });
            var dosage_option = dosageUnitPopup(dosage_unit, title1);
            var dosage_attrs = "data-dosage-val='"+dosage_unit_id+"' data-dosage-name='"+title1+"'";
           
            var duration_option = durationUnitPopup(duration_unit, title);
            var duration_attrs = "data-duration-id='"+duration_unit_id+"' data-duration-val='"+dur_val+"' data-duration-name='"+title+"'";

            $(".prescription_list_table_body").append("<tr data-detail-id='' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' "+disabled+" /></td><td><input style='width: 65%;float: left;' type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' value='" + dose + "' "+dosage_attrs+"/>"+dosage_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 65%;float: left;' value='" + duration + "' "+duration_attrs+"/>"+duration_option+"</td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "' "+disabled+"  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "' "+disabled+"  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'>"+remove_presc_btn+"</td></tr>");
        }else{
            $(".prescription_list_table_body").append("<tr data-detail-id='' class='row_class item_code_prescription_list" + item_code + "'><td style='text-align: center; line-height: 25px;' class='row_count_class'></td><td title='"+item_desc+"'><input type='text' autocomplete='off' title='" + item_desc + "' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' value='" + item_desc + "' data-item-code='"+item_code+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency'  value='" + frequency_name + "' data-frequency-value='"+frequency_value+"' data-frequency-id='"+frequency_id+"' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration'  value='" + duration + "' "+disabled+" /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' value='" + quantity + "' "+disabled+"  /></td><td><input type='text'  title='" + direction + "'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction'  value='" + direction + "' "+disabled+"  /></td><td class='text-center'><i onclick='addtoFavorite(this)' class='fa fa-star brown addPrescriptionFavItemBtn'></i></td><td class='text-center'>"+remove_presc_btn+"</td></tr>");

        }

    })
    serialNoCalculation('row_class', 'row_count_class');
    checkAllergicMedicine(this, 2);
})

$(document).on("click", '.deletePrescriptionButton', function () {


    var that = this;
    var prescription_head_id = $(this).parents('.prescription_history_item').attr('data-head-id');

    bootbox.confirm({
        message: "Are you sure want to delete the prescription ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $("#base_url").val();
                var url = base_url + "/emr_lite/deletePatientPrescription";

                if (prescription_head_id) {
                    var params = { prescription_head_id: prescription_head_id };
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: params,
                        beforeSend: function () {
                            $(that).attr('disabled', true);
                            $(that).removeClass('fa-trash deleteBtn');
                            $(that).addClass('fa-spinner fa-spin');
                        },
                        success: function (data) {
                            toastr.success("Successfully removed.");
                            $(that).parents('.prescription_history_item').remove();
                        },
                        complete: function () {
                            $(that).attr('disabled', false);
                            $(that).removeClass('fa-spinner fa-spin');
                            $(that).addClass('fa-trash deleteBtn');
                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                        },
                    });

                } else {
                    toastr.error("Invalid prescription");
                }
            }
        }
    });




})

$(document).on("click", '.updatePrescriptionBtn', function () {

    var patient_clinical_data = {};
    patient_clinical_data.patient_id = $('#patient_id').val();
    patient_clinical_data.visit_id = $('#visit_id').val();
    patient_clinical_data.encounter_id = $('#encounter_id').val();
    patient_clinical_data.doctor_id = $('#doctor_id').val();
    $('.updatePrescriptionBtn').prop({ disable: 'true' });

    var medicine_check = [];
    $('.prescription_list_table_body > tr').find('input[name="medicine_name"]').each(function (key, val) {
        if ($(val).val() != '') {
            medicine_check.push(val);
        }
    });

    if (medicine_check.length == 0) {
        Command: toastr["error"]("Select Medicine.");
        return false;
    }

    let validatePresc = validatePrescription();
    if (validatePresc) {

        let prescription_head_id = $('#prescription_head_id').val();
        // var p_type = $("input[name='p_search_type']:checked").val();
        var presc_form = [];

        $(".prescription_list_table_body").find('tr.row_class').each(function (key, value) {
            var presc_row = {};
            presc_row.selected_item_code = $(value).find('input[name="medicine_name"]').attr('data-item-code');
            presc_row.selected_item_name = $(value).find('input[name="medicine_name"]').val();
            presc_row.selected_item_frequency = $(value).find('.frequency').val();
            presc_row.selected_frequency_value = $(value).find('.frequency').attr('data-frequency-value');
            presc_row.selected_frequency_id = $(value).find('.frequency').attr('data-frequency-id');
            presc_row.selected_item_duration = $(value).find('.duration').val();
            presc_row.duration_unit = $(value).find('.duration_unit').val() ? $(value).find('.duration_unit').val() : 0;
            presc_row.selected_item_quantity = $(value).find('.quantity').val();
            presc_row.selected_item_direction = $(value).find('.direction').val() ? $(value).find('.direction').val() : '';
            presc_row.selected_detail_id = $(value).attr('data-detail-id') ? $(value).attr('data-detail-id') : '';
            // presc_row.selected_item_generic_name = $(value).find('input[name="selected_item_name[]"]').attr('data-generic-name');
            // presc_row.selected_item_remarks = $(value).find('input[name="selected_item_remarks[]"]').val();
            // presc_row.selected_item_dose = $(value).find('input[name="selected_item_dose[]"]').val();
            // presc_row.selected_item_start_at = $(value).find('input[name="iv_selected_start_at[]"]').val() ? $(value).find('input[name="iv_selected_start_at[]"]').val() : '';

            presc_form.push(presc_row);

        });

        var presc_form1 = JSON.stringify(presc_form);
        presc_form = escape(presc_form1);


        patient_clinical_data.prescriptions = presc_form;
        patient_clinical_data.prescription_head_id = prescription_head_id;
        patient_clinical_data.presc_advice = $(".prescription_advice_textarea").val();
        patient_clinical_data.next_review_remarks = $("#next_review_remarks").val();



    } else {
        $('.updatePrescriptionBtn').prop({ disable: 'false' });
        return;
    }


    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/updatePatientPrescription";
    var that = this;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_clinical_data: patient_clinical_data
        },
        beforeSend: function () {
            $(that).find('i').attr('disabled', true);
            $(that).find('i').removeClass('fa fa-save');
            $(that).find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            toastr.success("Success");
            $("#prescription_head_id").val('');
            $(".prescription_advice_textarea").val('');
            $("#next_review_remarks").val('');
            $(".prescription_list_table_body").empty();
            $(".saveButtonDiv").show();
            $(".updatePrescriptionBtnDiv").hide();
            $(".prescription_history_item_checkbox").attr("disabled", false);
            fetchPatientDetails();
        },
        complete: function () {
            $(that).find('i').attr('disabled', false);
            $(that).find('i').removeClass('fa fa-spinner fa-spin');
            $(that).find('i').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
})



$(document).on('click', '.emr_lite_prescription_tab', function () {
    $(".item_description").last().focus();
});
$(document).mousemove(function (event) {
    timer2 = "0:15";
});

$(document).on('keyup', '.frequency', function () {
    var searchVal = $(this).val();

    // if (searchVal != '') {
    //     $('.frequency-list-div-row-listing').find('.list-frequency-search-data-row-listing').find('tr').hide();
    //     $('.frequency-list-div-row-listing').find('.list-frequency-search-data-row-listing tr[data-frequency-name^="' + searchVal.toLowerCase() + '"]').show();
    //     $('.frequency-list-div-row-listing').find('.list-frequency-search-data-row-listing tr[data-frequency-name^="' + searchVal.toUpperCase() + '"]').show();
    // } else {
    //     $('.frequency-list-div-row-listing').show();
    //     $('.frequency-list-div-row-listing').find(".list-frequency-search-data-row-listing tr").show();
    // }

    var quantity_auto_calculation_config = $("#quantity_auto_calculation_config").val();
    if (quantity_auto_calculation_config == 1) {
        fetchFrequencyList(searchVal);
    } else {
        $(window.tr).find('.frequency').attr('data-frequency-value', 1);
        $(window.tr).find('.frequency').attr('data-frequency-id', '');
    }


});

var direction_fetch_timer = null;
var last_searched_direction = '';
$(document).on('keyup', '.direction', function (e) {
    var searchVal = $(this).val();

    // if (searchVal != '') {
    //     $('.direction-list-div-row-listing').find('.list-direction-search-data-row-listing').find('tr').hide();
    //     $('.direction-list-div-row-listing').find('.list-direction-search-data-row-listing tr[data-instruction-name^="' + searchVal.toLowerCase() + '"]').show();
    //     $('.direction-list-div-row-listing').find('.list-direction-search-data-row-listing tr[data-instruction-name^="' + searchVal.toUpperCase() + '"]').show();
    // } else {
    //     $('.direction-list-div-row-listing').show();
    //     $('.direction-list-div-row-listing').find(".list-direction-search-data-row-listing tr").show();
    // }

    if (direction_fetch_timer != null) {
        clearTimeout(direction_fetch_timer);
    }
    if (e.which != 8 || searchVal != '') {
        direction_fetch_timer = setTimeout(function () {
            fetchDirectionList(searchVal);
            last_searched_direction = searchVal;
        }, 1000)
    } else  {
        fetchDirectionList('a');
        last_searched_direction = '';
    }

//if (last_searched_direction != '')
});


function printPrescriptionWithInvestigation() {
    var print_investigation = ($("#print_investigation_check").prop('checked')) ? 1 : 0;
    var print_prescription = ($("#print_prescription_check").prop('checked')) ? 1 : 0;
    var print_assessment = ($("#print_assessment_check").prop('checked')) ? 1 : 0;
    var print_vital = ($("#print_vital_check").prop('checked')) ? 1 : 0;
    var prescription_id = $("#prescription_print_config_modal").attr("presc-head-id");
    var investigation_id = $("#prescription_print_config_modal").attr("invest-head-id");
    var assessment_head_id = $("#prescription_print_config_modal").attr("assessment_head_id");
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr_lite/printPatientPrescriptionWithInvestigationLite";

    if ((print_investigation == 1 && print_prescription == 1) || (print_prescription > 0 && print_assessment > 0) || (print_assessment > 0 && print_investigation > 0) || print_vital == 1) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                visit_id: visit_id,
                prescription_id: prescription_id,
                investigation_id: investigation_id,
                print_investigation: print_investigation,
                print_prescription: print_prescription,
                assessment_head_id: assessment_head_id,
                print_assessment: print_assessment,
                print_vital: print_vital,
            },
            beforeSend: function () {
                $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-print').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
            },
            complete: function () {
                $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-spinner fa-spin').addClass('fa-print');
            }
        });


    } else if (print_investigation == 0 && print_prescription == 1 && print_assessment == 0) {
        printPrescription(prescription_id);
    } else if (print_investigation == 1 && print_prescription == 0 && print_assessment == 0) {
        printInvestigation(investigation_id);
    }  else if (print_investigation == 0 && print_prescription == 0 && print_assessment == 1) {
        printAssessment(assessment_head_id);
    } else {
        Command: toastr['warning']('Please select atleast one print mode.!');
    }

}


//print prescription view
function printPrescription(head_id, obj) {
    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr_lite/printPrescription",
            data: {
                head_id: head_id,
                _token: _token
            },
            beforeSend: function () {
                if (obj) {
                    $(obj).removeClass('fa-print printBtn').addClass('fa-spinner fa-spin');
                } else {
                    $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-print').addClass('fa-spinner fa-spin');
                }
            },
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write(res.html + '<script>setTimeout(function(){window.print();window.close();  },1000)</script>');
            },
        
            complete: function () {
                if (obj) {
                    $(obj).removeClass('fa-spinner fa-spin').addClass('fa-print printBtn');
                } else {
                    $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-spinner fa-spin').addClass('fa-print');
                }
            }
        });
    }
}

$(document).on("click", '.printPrescriptionButton', function () {
    var that = this;
    var prescription_head_id = $(this).parents('.prescription_history_item').attr('data-head-id');
    printPrescription(prescription_head_id, that);
});

$(document).on("click", '.cancePrescriptionEditBtn', function () {
    $(".updatePrescriptionBtnDiv").hide();
    $(".prescription_advice_textarea").val('');
    $("#next_review_remarks").val('');
    $(".prescription_history_item_checkbox").attr("disabled", false);
    $(".prescription_list_table_body").empty();
    $("#prescription_head_id").val('');
    addNewPrescriptionRow();

    if ($("#investigation_head_id").val() == "" && $("#ca_head_id").val() == "") {
        $(".saveButtonDiv").show();
    }
});

function saveNewGroup(grp_name, e) {
    var item_details = JSON.stringify(addItemGlobal);
    if (grp_name != '') {
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/saveNewGroup";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                grp_name: grp_name, item_details: item_details
            },
            beforeSend: function () {
                $('#addGrpBtn').attr('disabled', true);
                $(e).find('i').removeClass('fa fa-plus');
                $(e).find('i').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                console.log(data);
                if (data.status == 200) {
                    toastr.success('Success');
                    getDoctorPrescription();
                    
                    if (data.prependData) {
                        $('#addGrpTableBody').prepend(data.prependData);
                    }
                    serialNoCalculation('row_class_newGrp', 'row_count_class_newGrp');
                } else if (data.status == 102) {
                    toastr.warning('Already existing Group Name.');
                } else {
                    toastr.warning('Sorry,Something went wrong.');
                }
                $('#addNewGrp').val(' ');

            },
            complete: function () {
                $('#addGrpBtn').attr('disabled', false);
                $(e).find('i').removeClass('fa fa-spinner fa-spin');
                $(e).find('i').addClass('fa fa-plus');
            },
            error: function () {
                toastr.error("Error please check your internet connection");
                $('#addNewGrp').val(' ');

            },
        });
    }
}

function itemListGrpWise(e, group_id) {
    $('.activeTr').removeClass('bg-green');
    $(e).closest('.activeTr').addClass('bg-green');
    if (group_id) {
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/listItemGrpWise";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                group_id: group_id
            },
            beforeSend: function () {
                $('#prescription_list_table_grp').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $('#prescription_list_table_grp').html(data)

            },
            complete: function () {
                $('#prescription_list_table_grp').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);


            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    }
}

function addNewMediceneGrp() {
    if ($('.activeTr').hasClass('bg-green')) {
        if (enable_duration_unit == 1) {
            var duration_unit=$('#duration_unit').val();
            var duration_unit=JSON.parse(duration_unit);
            var dosage_unit=$('#dosage_unit').val();
            var dosage_unit=JSON.parse(dosage_unit);
            var option='<option value="" > Unit </option>';
            var option_d='<option value="" > Dose </option>';
            var selected='';
            var title='';
            var title1='';

            $.each(duration_unit, function(key, val){
                if (val.name=='DAYS') {
                    selected='selected';
                    title=val.name;
                }else {
                    selected='';
                }
                option += '<option value="'+val.id+'" data-unit-val="'+val.value+'" '+selected+'>'+ val.name +'</option>';
            });
            $.each(dosage_unit, function(key, val) {
                if (val.name=='mg') {
                    selected='selected';
                    title1=val.name;
                }else {
                    selected='';
                }
               option_d += '<option value="'+val.id+'" '+selected+'>'+ val.name +'</option>';
            });

            $(".prescription_list_table_body_grp").append("<tr class='row_class_addGrp'><td style='text-align: center; line-height: 25px;' class='row_count_class_addGrp'></td><td><input type='text' autocomplete='off' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' data-item-code='' style='float: left;' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox dosage' style='width: 60%;float: left;'/><select onchange='showTitle(this)' name='dosage_unit' style='width: 18px;height: 18px; margin-top: 2px; margin-left: 3px; border:1px solid #21a4f1 !important' class='form-control bottom-border-text borderless_textbox dosage_unit'>"+option_d+"</select></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' style='width: 60%;float: left;'/><select onchange='showTitle(this)' name='duration_unit' class='form-control bottom-border-text borderless_textbox duration_unit' style='width: 18px;height: 18px; margin-top: 2px; margin-left: 3px; border:1px solid #21a4f1 !important'>"+option+"</select></td> <td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' /></td><td><input type='text'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction' /></td><td class='text-center'><i  onclick='removeAddGrpRow(this)' class='fa fa-times red removeAddGrpRowBtn'></i></td></tr>");
            serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
        }else {
        $(".prescription_list_table_body_grp").append("<tr class='row_class_addGrp'><td style='text-align: center; line-height: 25px;' class='row_count_class_addGrp'></td><td><input type='text' autocomplete='off' name='medicine_name' class='form-control bottom-border-text borderless_textbox item_description' data-item-code='' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox frequency' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox duration' /></td><td><input type='text' autocomplete='off' class='form-control bottom-border-text borderless_textbox quantity' /></td><td><input type='text'  autocomplete='off' class='form-control bottom-border-text borderless_textbox direction' /></td><td class='text-center'><i  onclick='removeAddGrpRow(this)' class='fa fa-times red removeAddGrpRowBtn'></i></td></tr>");
        serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
        }
    }else {
        toastr.warning('Please make sure you selected a group.');
    }
}
function removeAddGrpRow(e) {
    if ($(e).parents('tr').find('.item_description').val()) {
        bootbox.confirm({
            title: 'Remove Medicine',
            size: 'medium',
            message: 'Are you sure, you want to remove this Medicine ?',
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    style: 'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function (result) {
                if (result) {
                    $(e).parents('tr').remove();
                    serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
                }
            }
        });
    } else {
        $(e).parents('tr').remove();
        serialNoCalculation('row_class_addGrp', 'row_count_class_addGrp');
    }

}
$('#addItemtoFavouriteModel').on('hidden.bs.modal', function (e) {
    $("#prescription_list_table_grp").html('');

})
function saveNewItemsToGroup(e) {
    var groupItems = new Array();
    var grp_id = $('#addItemTogrp_grpId').val();

    $('.prescription_list_table_body_grp tr').each(function () {
        if ($(this).find('.item_description').val()) {
            var dosage = $(this).find('.dosage').val();
            dosage = (dosage != undefined) ? dosage : '';
            var dosage_unit = $(this).find('.dosage_unit').val();
            dosage_unit = (dosage_unit != undefined) ? dosage_unit : 0;
            groupItems.push({
                'medicine_name': $(this).find('.item_description').val(),
                'medicine_code': $(this).find('.item_description').data('item-code'),
                'frequency': $(this).find('.frequency').val(),
                'duration': $(this).find('.duration').val(),
                'duration_unit': $(this).find('.duration_unit').val(),
                'quantity': $(this).find('.quantity').val(),
                'direction': $(this).find('.direction').val(),
                'frequency_value': $(this).find('.frequency').attr('data-frequency-value'),
                'frequency_id': $(this).find('.frequency').attr('data-frequency-id'),
                'dosage': dosage,
                'dosage_unit': dosage_unit,
            })
        }
    })
    if (grp_id && groupItems.length > 0) {
        var base_url = $("#base_url").val();
        var stringGrpItems = JSON.stringify(groupItems);
        var url = base_url + "/emr_lite/saveItemsToGroup";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                group_id: grp_id, stringGrpItems: stringGrpItems
            },
            beforeSend: function () {
                $('#saveNewItemsToGroupBtn').attr('disabled', true);
                $(e).find('i').removeClass('fa fa-plus');
                $(e).find('i').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (data.status == 200) {
                    toastr.success('Success');
                    $("#prescription_list_table_grp").html('');
                    getDoctorPrescription();

                } else {
                    toastr.warning('Sorry,Something went wrong.');
                }

            },
            complete: function () {
                $('#saveNewItemsToGroupBtn').attr('disabled', false);
                $(e).find('i').removeClass('fa fa-spinner fa-spin');
                $(e).find('i').addClass('fa fa-plus');

            },
            error: function () {
                toastr.error("Error please check your internet connection");
            },
        });
    }

}
function saveChangedGrpName(e, group_id) {
    if (group_id && ($(e).parents('tr').find('.editGrpName').attr('data-oldGrpName') != $(e).parents('tr').find('.editGrpName').val())) {
        var changed_name = $(e).parents('tr').find('.editGrpName').val();
        var old_name = $(e).parents('tr').find('.editGrpName').attr('data-oldGrpName');
        bootbox.confirm({
            title: 'Change Group Name',
            size: 'medium',
            message: 'Are you sure, you want change this group name ?',
            buttons: {
                'cancel': {
                    label: 'No',
                    style: 'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function (result) {
                if (result) {
                    var base_url = $("#base_url").val();
                    var url = base_url + "/emr_lite/saveChangedItemName";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            group_id: group_id, changed_name: changed_name
                        },
                        beforeSend: function () {
                            $(e).parents('tr').find('.grpEditBtn').attr('disabled', true);
                            $(e).find('i').removeClass('fa fa-save');
                            $(e).find('i').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            if (data.status == 200) {
                                toastr.success('Success');
                            } else if (data.status == 101) {
                                toastr.warning('Named Group already Exist.');
                                $(e).parents('tr').find('.editGrpName').val(old_name);
                            } else {
                                toastr.warning('Sorry,Something went wrong.');
                                $(e).parents('tr').find('.editGrpName').val(old_name);
                            }

                        },
                        complete: function () {
                            $(e).parents('tr').find('.grpEditBtn').attr('disabled', false);
                            $(e).find('i').removeClass('fa fa-spinner fa-spin');
                            $(e).find('i').addClass('fa fa-save');

                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                            $(e).parents('tr').find('.editGrpName').val(old_name);
                        },
                    });
                } else {
                    $(e).parents('tr').find('.editGrpName').val(old_name);
                }
            }
        });
    } else {
        toastr.warning('No change found.');
    }
}

function deleteGrpName(e, group_id, from_type = 0) {
    if (group_id) {
        bootbox.confirm({
            title: 'Delete Group',
            size: 'medium',
            message: 'Are you sure, you want to delete this group ?',
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    style: 'margin-left: 5px;'
                },
                'confirm': {
                    label: 'Yes',
                }
            },

            callback: function (result) {
                if (result) {
                    var base_url = $("#base_url").val();
                    var url = base_url + "/emr_lite/deleteGrpName";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            group_id: group_id
                        },
                        beforeSend: function () {
                            if (from_type == 1) {
                                $(e).attr('disabled', true);

                            } else {
                                $(e).parents('tr').find('.grpdeleteBtn').attr('disabled', true);

                            }
                            $(e).find('i').removeClass('fa fa-trash');
                            $(e).find('i').addClass('fa fa-spinner fa-spin');


                        },
                        success: function (data) {
                            if (data.status == 200) {
                                toastr.success('Deleted.');
                                if (from_type == 1) {
                                    $('#prescription_bookmark_item' + group_id).fadeOut();
                                } else {
                                    $(e).parents('tr').remove();
                                    serialNoCalculation('row_class_newGrp', 'row_count_class_newGrp');
                                }
                                $('#prescription_bookmark_item' + group_id).fadeOut();
                            } else {
                                toastr.warning('Sorry,Something went wrong.');
                            }

                        },
                        complete: function () {
                            if (from_type == 1) {
                                $(e).attr('disabled', false);

                            } else {
                                $(e).parents('tr').find('.grpdeleteBtn').attr('disabled', false);
                            }
                            $(e).find('i').removeClass('fa fa-spinner fa-spin');
                            $(e).find('i').addClass('fa fa-trash');


                        },
                        error: function () {
                            toastr.error("Error please check your internet connection");
                        },
                    });
                }
            }
        });
    }
}

function addRemoveItemFrmGrp(e, item_code, group_id) {

    if (item_code && group_id) {
        var add;
        if ($(e).is(":checked")) {
            add = 1;
        } else {
            add = 2;
        }
        var stringGrpItems = JSON.stringify(addItemGlobal);
        var base_url = $("#base_url").val();
        var url = base_url + "/emr_lite/addRemoveItemFrmGrp";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                group_id: group_id, item_code: item_code, stringGrpItems: stringGrpItems, add: add,
            },
            beforeSend: function () {
                $('#itemRelativeGrps').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                if (data.status == 200) {
                    toastr.success('success.');
                    itemListGrpWise(e, group_id);                                            
                } else {
                    toastr.warning('Sorry,Something went wrong.');
                    if (add == 1) {
                        $(e).prop('checked', true);
                    } else {
                        $(e).prop('checked', false);
                    }
                }

            },
            complete: function () {
                $('#itemRelativeGrps').LoadingOverlay("hide");
                getDoctorPrescription();
            },
            error: function () {
                toastr.error("Error please check your internet connection");
                if (add == 1) {
                    $(e).prop('checked', true);
                } else {
                    $(e).prop('checked', false);
                }
            },
        });
    }
}

$(document).on('click','.viewPrescriptionGroupBtn', function() {
    var book_mark=$(this).parents('.prescription_bookmark_item');
    if (book_mark.find('.bookmarks_list').is(':visible')) {
        book_mark.find('.bookmarks_list').hide();
    } else {
        book_mark.find('.bookmarks_list').show();
    }
  
})


function takeNextAppointment() {
    var next_review_date = $('#next_review_date').val();
    var url = $('#base_url').val() + "/emr_lite/selectDoctorAppointments";
    var visit_id = $("#visit_id").val();
    var doctor_id = $("#doctor_id").val();    
    var patient_id = $('#patient_id').val();
    if(patient_id == ''){
        toastr.warning("select patient!");
        return false;
    }
    $.ajax({
        type: "POST",
        url: url,
        data: 'visit_id=' + visit_id + '&next_review_date=' + next_review_date + '&doctor_id=' + doctor_id,
        beforeSend: function () {
            $("#appointment_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#next_appointment_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#next_appointment_list_data').html(data);
            $('#next_appointment_list_data').LoadingOverlay("hide");
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });


        }
    });
}
function create_appointment(slot_time, token_no, session_id) {
    var patient_name = $('#next_review_patient_name').val();
    var patient_id = $('#next_review_patient_id').val();
    var age = $('#next_review_age').val();
    var gender = $('#next_review_gender').val();
    var phone = $('#next_review_phone').val();
    var address = $('#next_review_address').val();
    var doctor_id = $('#next_review_doctor_id').val();
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/emr/create_new_appointment",
        data: 'patient_name=' + patient_name + '&patient_id=' + patient_id + '&age=' + age + '&gender=' + gender + '&phone=' + phone + '&slot_time=' + slot_time + '&address=' + address + '&doctor_id=' + doctor_id + '&token_no=' + token_no + '&session_id=' + session_id,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            takeNextAppointment();
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('#next_appointment_list_data').LoadingOverlay("hide");
        }
    });
}
