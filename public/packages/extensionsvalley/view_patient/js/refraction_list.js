$(document).ready(function () {
    $("input[data-attr='date']").datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    fetchScreeningListData();

    $("#global_patient_check").click(function () {
        if ($(this).is(":checked")) {
            $("#patient_general_search_txt").attr("readonly", false);
            $("#group_doctor").attr("readonly", true);
            $("#search_date").attr("readonly", true);
            $("#patient_search").attr("readonly", true);
        } else {
            $("#patient_general_search_txt").attr("readonly", true);
            $("#group_doctor").attr("readonly", false);
            $("#search_date").attr("readonly", false);
            $("#patient_search").attr("readonly", false);
            $("#patient_general_search_txt").val('');
        }
        fetchScreeningListData();
    });
    $(".select2").select2({
        placeholder: "",
        maximumSelectionSize: 6
    });

});

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

var base_url = $('#base_url').val();

function fetchScreeningListData() {
    var doctor_id = $('#group_doctor').val() ? $('#group_doctor').val() : 0;
    var search_date = $('#search_date').val();
    var global_search = $("#global_patient_check").is(":checked") ? 1 : 0;
    var patient_general_search_txt = $("#patient_general_search_txt").val();
    var data = {
        doctor_id: doctor_id,
        search_date: search_date,
        global_search: global_search,
        patient_general_search_txt: patient_general_search_txt
    }

    if (global_search == 1 && patient_general_search_txt == "") {
        return;
    }
    if (doctor_id && search_date) {
        var url = base_url + "/eye_emr/fetchEyeListData";
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            beforeSend: function () {
                $('#fetchScreeningListDataBtn').attr('disabled', true);
                $('#fetchScreeningListDataSpin').removeClass('fa fa-search');
                $('#fetchScreeningListDataSpin').addClass('fa fa-spinner fa-spin');
                $('.screening_list_table_container').html("Please Wait!!!");
            },
            success: function (res) {
                $('.screening_list_table_container').html(res);
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }

                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            },
            complete: function () {
                $('#fetchScreeningListDataBtn').attr('disabled', false);
                $('#fetchScreeningListDataSpin').removeClass('fa fa-spinner fa-spin');
                $('#fetchScreeningListDataSpin').addClass('fa fa-search');
            }
        });
    }
}

function getRefractionDilation(patient_id, doctor_id, appointment_id, visit_id) {
    var url = base_url + "/eye_emr/getPatientRefraction";
    var patient_name = $('#patientNameString' + patient_id).html();
    $('#patient_id').val(patient_id);
    $('#doctor_id').val(doctor_id);
    $('#visit_id').val(visit_id);
    var patient_pg_power_string = '';
    var param = {
        patient_id: patient_id,
        doctor_id: doctor_id,
        appointment_id: appointment_id,
        visit_id: visit_id
    }
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            $("#refractionDilationModelHeader").html(patient_name);
            $('.getRefractionDilation').attr('disabled', true);
            $('#getRefractionDilationSpin' + patient_id).removeClass('fa fa-eye-slash');
            $('#getRefractionDilationSpin' + patient_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#getEyeRefractionModelDiv").html(data.html);
            $("#patient_refraction_id").val(data.patient_refraction_id);
            patient_pg_power_string = data.patient_pg_power;
            $("#getEyeRefractionModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('.getRefractionDilation').attr('disabled', false);
            $('#getRefractionDilationSpin' + patient_id).removeClass('fa fa-spinner fa-spin');
            $('#getRefractionDilationSpin' + patient_id).addClass('fa fa-eye-slash');
            addNewPgPower(patient_pg_power_string);
        }
    });
}


function addNewPgPower(patient_refraction_pg_power) {
    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    var url = base_url + "/eye_emr/addNewPgPower";
    if (doctor_id) {
        if (patient_id) {
            var param = {
                doctor_id: doctor_id,
                patient_id: patient_id,
                patient_refraction_pg_power: patient_refraction_pg_power
            };
            $.ajax({
                url: url,
                type: "POST",
                data: param,
                beforeSend: function () {
                    $('#addNewPgBtn').attr('disable', true);
                    $('#addNewPgSpin').removeClass('fa fa-plus-circle');
                    $('#addNewPgSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    $("#addNewPgBtnDiv").append(data);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                },
                error: function () {
                    Command: toastr["error"]('Error.!');
                },
                complete: function () {
                    $('#addNewPgBtn').attr('disable', false);
                    $('#addNewPgSpin').removeClass('fa fa-spinner fa-spin');
                    $('#addNewPgSpin').addClass('fa fa-plus-circle');
                }
            });
        } else {
            toastr.warning("Please select any patient");
        }
    } else {
        toastr.warning("Please select any doctor");
    }
}


function getEyeVitals() {
    eye_data = {};
    snellen_chart_right = {};
    snellen_chart_left = {};
    eye_right_details = {};
    eye_left_details = {};
    pg_power = {};

    $('.snellen_right').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined') {
            snellen_chart_right[attr_name] = attr_val;
        }

    });
    $('.snellen_left').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined') {
            snellen_chart_left[attr_name] = attr_val;
        }

    });
    $('.ref_right').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined') {
            eye_right_details[attr_name] = attr_val;
        }

    });
    $('.ref_left').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined') {
            eye_left_details[attr_name] = attr_val;
        }

    });
    var i = 0;
    $('.pg_power_data').each(function (index) {
        each_array = {};
        var right_glass_used_for = $(this).find("input[name='right_glass_used_for']").val();
        $(this).find(".right_power").each(function () {
            var RDvSPH = $(this).find("input[name='RDvSPH']").val();
            var RDvCyl = $(this).find("input[name='RDvCyl']").val();
            var RDvAxix = $(this).find("input[name='RDvAxix']").val();
            var RNvSPH = $(this).find("input[name='RNvSPH']").val();
            var RNvCyl = $(this).find("input[name='RNvCyl']").val();
            var RNvAxix = $(this).find("input[name='RNvAxix']").val();
            each_array['right'] = {
                'RGUF': right_glass_used_for,
                'RDvSPH': RDvSPH,
                'RDvCyl': RDvCyl,
                'RDvAxix': RDvAxix,
                'RNvSPH': RNvSPH,
                'RNvCyl': RNvCyl,
                'RNvAxix': RNvAxix,
            }
        });
        $(this).find(".left_power").each(function () {
            var LDvSPH = $(this).find("input[name='LDvSPH']").val();
            var LDvCyl = $(this).find("input[name='LDvCyl']").val();
            var LDvAxix = $(this).find("input[name='LDvAxix']").val();
            var LNvSPH = $(this).find("input[name='LNvSPH']").val();
            var LNvCyl = $(this).find("input[name='LNvCyl']").val();
            var LNvAxix = $(this).find("input[name='LNvAxix']").val();
            each_array['left'] = {
                'LDvSPH': LDvSPH,
                'LDvCyl': LDvCyl,
                'LDvAxix': LDvAxix,
                'LNvSPH': LNvSPH,
                'LNvCyl': LNvCyl,
                'LNvAxix': LNvAxix,
            }
        });
        pg_power[i] = each_array;
        i++;
    });

    eye_data['refraction'] = {
        'snellen_right': snellen_chart_right,
        'snellen_left': snellen_chart_left,
        'right': eye_right_details,
        'left': eye_left_details,
        'pg_power': pg_power,
    }
    eye_data['head_details'] = {
        'presenting_complaints': $('#eye_presenting_complaints').val(),
        'past_history': $('#eye_presenting_history').val(),
    }

    return JSON.stringify(eye_data);
}

function deletePGPower(obj) {
    obj.closest('.pg_power_data').remove();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
}

function savePatientRefraction() {
    var patient_refraction_id = $('#patient_refraction_id').val();
    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    var visit_id = $("#visit_id").val();
    var encounter_id = $('#encounter_id').val();
    var getEyeVitals_string = getEyeVitals();
    var url = base_url + "/eye_emr/savePatientRefraction";
    if (patient_id) {
        var param = {
            patient_refraction_id: patient_refraction_id,
            patient_id: patient_id,
            visit_id: visit_id,
            encounter_id: encounter_id,
            doctor_id: doctor_id,
            getEyeVitals_string: getEyeVitals_string
        };
        $.ajax({
            url: url,
            type: "POST",
            data: param,
            beforeSend: function () {
                $('#refractionDilationModelDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
                $('#savePatientRefractionBtn').attr('disabled', true);
                $('#savePatientRefractionSpin').removeClass('fa fa-save');
                $('#savePatientRefractionSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (res) {
                if (parseInt(res.status) == 100) {
                    toastr.success(res.message);
                    $("#getEyeRefractionModel").modal('toggle');
                } else {
                    toastr.warning(res.message);
                }
            },
            complete: function () {
                $("#refractionDilationModelDiv").LoadingOverlay("hide");
                $('#savePatientRefractionBtn').attr('disabled', false);
                $('#savePatientRefractionSpin').removeClass('fa fa-spinner fa-spin');
                $('#savePatientRefractionSpin').addClass('fa fa-save');

            }
        });
    } else {
        toastr.warning("Please select any select");
    }
}

function calculate_count(from_type) {
    var count_val = 0.0;
    $('.' + from_type + '_claculate_count').each(function (index) {
        if (parseInt($(this).val())) {
            count_val += parseInt($(this).val());
        }
    });

    var calculated_string = count_val + '/6-';
    console.log(calculated_string);
    $('#' + from_type + '_count_total').val(calculated_string)
}



function search_patient() {
    var keytype = $('input[name="search_type"]:checked').val();
    var search_key = $("#patient_search").val();
    search_key = search_key.toUpperCase();
    $('.patient_det_widget_screening').hide();
    if (keytype == 1) {
        $(".patient_det_widget_screening[data-patientname^='" + search_key + "']").show();
    } else if (keytype == 2) {
        $(".patient_det_widget_screening[data-uhid^='" + search_key + "']").show();
    } else {
        $(".patient_det_widget_screening[data-phone^='" + search_key + "']").show();
    }
    if (search_key == '') {
        $('.patient_det_widget_screening').show();
    }
}


var search_timeout = null;

function patient_general_search(txt) {
    var last_search_key = '';
    var request_flag = '';
    if (txt.length >= 2 && $.trim(txt) != '') {

        clearTimeout(search_timeout);
        search_timeout = setTimeout(function () {

            if (last_search_key == txt || request_flag == 1) {
                return false;
            }
            last_search_key = txt;
            var url = base_url + "/eye_emr/generalPatientSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'txt=' + txt,
                beforeSend: function () {
                    $("#completepatientbox").empty();
                    $("#completepatientbox").show();
                    $(".input_rel .input_spinner").removeClass('hide');
                    $("#completepatientbox").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    request_flag = 1
                },
                success: function (data) {

                    $("#completepatientbox").LoadingOverlay("hide");
                    $("#completepatientbox").html(data);
                    $(".input_rel .input_spinner").addClass('hide');
                    request_flag = 0;
                }
            });
        });
    } else if (txt.length === 0) {
        $("#completepatientbox").hide();
        $("#completepatientbox").LoadingOverlay("hide");
    }
}
