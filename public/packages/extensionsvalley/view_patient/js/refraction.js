function getRefractionDilation(patient_id, doctor_id, appointment_id, visit_id) {
    var url = base_url + "/eye_emr/getPatientRefraction";
    $('#patient_id').val(patient_id);
    $('#doctor_id').val(doctor_id);
    $('#visit_id').val(visit_id);
    $.ajax({
        url: url,
        type: "POST",
        data: {},
        beforeSend: function () {
            $('#patient_refraction_id').val(0);
            $('.getRefractionDilation').attr('disabled', true);
            $('#getRefractionDilationSpin' + patient_id).removeClass('fa fa fa-ravelry');
            $('#getRefractionDilationSpin' + patient_id).addClass('fa fa-spinner fa-spin');
            $('#AddNewRefractionBtn').hide();
        },
        success: function (data) {
            $("#getEyeRefractionModelDiv").html(data.html);
            $("#getEyeRefractionModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('.getRefractionDilation').attr('disabled', false);
            $('#getRefractionDilationSpin' + patient_id).removeClass('fa fa-spinner fa-spin');
            $('#getRefractionDilationSpin' + patient_id).addClass('fa fa fa-ravelry');
            getRefractionList();
            addNewPgPower('');
        }
    });
}



function addRefraction() {
    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    var visit_id = $("#visit_id").val();
    var encounter_id = $('#encounter_id').val();
    $('#AddNewRefractionBtn').hide();
    var url = base_url + "/eye_emr/getPatientRefraction";
    if (doctor_id) {
        if (patient_id) {
            var param = {
                doctor_id: doctor_id,
                patient_id: patient_id,
                visit_id: visit_id,
                encounter_id: encounter_id
            };
            $.ajax({
                url: url,
                type: "POST",
                data: param,
                beforeSend: function () {
                    $('#addRefractionSpin').attr('disable', true);
                    $('#addRefractionSpin').removeClass('fa fa-plus-circle');
                    $('#addRefractionSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    $("#getEyeRefractionModelDiv").html(data.html);
                    $("#getEyeRefractionModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                },
                error: function () {
                    Command: toastr["error"]('Error.!');
                },
                complete: function () {
                    $('#addRefractionSpin').attr('disable', false);
                    $('#addRefractionSpin').removeClass('fa fa-spinner fa-spin');
                    $('#addRefractionSpin').addClass('fa fa-plus-circle');
                    getRefractionList();
                    addNewPgPower('');
                }
            });
        } else {
            toastr.warning("Please select any patient");
        }
    } else {
        toastr.warning("Please select any doctor");
    }
}

function addNewRefraction() {
    var url = base_url + "/eye_emr/getPatientRefraction";
    var visit_id = $("#visit_id").val();
    $.ajax({
        url: url,
        type: "POST",
        data: {
            visit_id: visit_id
        },
        beforeSend: function () {
            $('#patient_refraction_id').val(0);
            $('.eye_refraction_data').removeClass('bg-orange');
            $('#AddNewRefractionBtn').attr('disabled', true);
            $('#AddNewRefractionSpin').removeClass('fa fa-plus');
            $('#AddNewRefractionSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#getEyeRefractionModelDiv").html(data.html);
        },
        complete: function () {
            $('#AddNewRefractionBtn').attr('disabled', false);
            $('#AddNewRefractionSpin').removeClass('fa fa-spinner fa-spin');
            $('#AddNewRefractionSpin').addClass('fa fa-plus');
            $('#AddNewRefractionBtn').hide();
            addNewPgPower('');
        }
    });
}


function getRefractionList() {
    var url = base_url + "/eye_emr/getRefractionList";
    var patient_id = $('#patient_id').val();
    var doctor_id = $('#doctor_id').val();
    var visit_id = $('#visit_id').val();
    var param = {
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id
    };
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            $('#refractionDataList').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#refractionDataList").html(data.html);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $("#refractionDataList").LoadingOverlay("hide");
        }
    });
}

function getRefractionData(refraction_id) {
    var url = base_url + "/eye_emr/getRefractionData";
    var patient_pg_power_string = '';
    var param = {
        refraction_id: refraction_id,
    };
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            $('.eye_refraction_data').removeClass('bg-orange');
            $('#getEyeRefractionModelDiv').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#getEyeRefractionModelDiv").html(data.html);
            patient_pg_power_string = data.patient_pg_power;
            $('#patient_refraction_id').val(refraction_id);
        },
        complete: function () {
            $('#getEyeRefractionModelDiv').LoadingOverlay("hide");
            $('#refraction_datarow' + refraction_id).addClass('bg-orange');
            addNewPgPower(patient_pg_power_string);
            $('#AddNewRefractionBtn').show();
        }
    });
}

function changeDilationData() {
    var status = $('#refraction_dilation').is(":checked");
    if (status) {
        $('.near_vision').prop('readonly', true);
    } else {
        $('.near_vision').prop('readonly', false);
    }
}

function addNewPgPower(patient_refraction_pg_power) {
    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    var url = base_url + "/eye_emr/addNewPgPower";
    if (doctor_id) {
        if (patient_id) {
            var param = {
                doctor_id: doctor_id,
                patient_id: patient_id,
                patient_refraction_pg_power: patient_refraction_pg_power
            };
            $.ajax({
                url: url,
                type: "POST",
                data: param,
                beforeSend: function () {
                    $('#addNewPgBtn').attr('disable', true);
                    $('#addNewPgSpin').removeClass('fa fa-plus-circle');
                    $('#addNewPgSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    $("#addNewPgBtnDiv").append(data);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                },
                error: function () {
                    Command: toastr["error"]('Error.!');
                },
                complete: function () {
                    $('#addNewPgBtn').attr('disable', false);
                    $('#addNewPgSpin').removeClass('fa fa-spinner fa-spin');
                    $('#addNewPgSpin').addClass('fa fa-plus-circle');
                }
            });
        } else {
            toastr.warning("Please select any patient");
        }
    } else {
        toastr.warning("Please select any doctor");
    }
}

function getGlassPrescription() {
    glass_prescription = {};
    glass_prescription_right = {};
    glass_prescription_left = {};

    var pres_data = '';
    $('.glass_prescription').each(function (index) {
        if (!pres_data) {
            pres_data = $(this).val();
        }
    });

    if (pres_data) {
        $('.glass_prescription_right').each(function (index) {
            var attr_name = $(this).attr('attr-code');
            var attr_val = $(this).val();
            if (attr_val != 'undefined' && attr_val) {
                glass_prescription_right[attr_name] = attr_val;
            }
        });
        $('.glass_prescription_left').each(function (index) {
            var attr_name = $(this).attr('attr-code');
            var attr_val = $(this).val();
            if (attr_val != 'undefined' && attr_val) {
                glass_prescription_left[attr_name] = attr_val;
            }
        });

        glass_prescription = {
            'glass_prescription_right': glass_prescription_right,
            'glass_prescription_left': glass_prescription_left,
        }
    }
    return JSON.stringify(glass_prescription);
}

function getEyeVitals() {
    eye_data = {};
    snellen_chart_right = {};
    snellen_chart_left = {};
    eye_right_details = {};
    eye_left_details = {};
    ketometer_right = {};
    ketometer_left = {};
    subrefraction_right = {};
    subrefraction_left = {};
    prism_right = {};
    prism_left = {};
    pg_power = {};

    $('.snellen_right').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            snellen_chart_right[attr_name] = attr_val;
        }

    });
    $('.snellen_left').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            snellen_chart_left[attr_name] = attr_val;
        }

    });
    $('.ref_right').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            eye_right_details[attr_name] = attr_val;
        }

    });
    $('.ref_left').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            eye_left_details[attr_name] = attr_val;
        }

    });
    $('.ketometer_right').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            ketometer_right[attr_name] = attr_val;
        }
    });
    $('.ketometer_left').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            ketometer_left[attr_name] = attr_val;
        }
    });
    $('.sub_rightrefraction').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            subrefraction_right[attr_name] = attr_val;
        }
    });
    $('.sub_leftrefraction').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            subrefraction_left[attr_name] = attr_val;
        }
    });
    $('.prism_right').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            prism_right[attr_name] = attr_val;
        }
    });
    $('.prism_left').each(function (index) {
        var attr_name = $(this).attr('attr-code');
        var attr_val = $(this).val();
        if (attr_val != 'undefined' && attr_val) {
            prism_left[attr_name] = attr_val;
        }
    });

    var i = 0;
    $('.pg_power_data').each(function (index) {
        each_array = {};
        var right_glass_used_for = $(this).find("input[name='right_glass_used_for']").val();
        $(this).find(".right_power").each(function () {
            var RDvSPH = $(this).find("input[name='RDvSPH']").val();
            var RDvCyl = $(this).find("input[name='RDvCyl']").val();
            var RDvAxix = $(this).find("input[name='RDvAxix']").val();
            var RNvSPH = $(this).find("input[name='RNvSPH']").val();
            var RNvCyl = $(this).find("input[name='RNvCyl']").val();
            var RNvAxix = $(this).find("input[name='RNvAxix']").val();
            each_array['right'] = {
                'RGUF': right_glass_used_for,
                'RDvSPH': RDvSPH,
                'RDvCyl': RDvCyl,
                'RDvAxix': RDvAxix,
                'RNvSPH': RNvSPH,
                'RNvCyl': RNvCyl,
                'RNvAxix': RNvAxix,
            }
        });
        $(this).find(".left_power").each(function () {
            var LDvSPH = $(this).find("input[name='LDvSPH']").val();
            var LDvCyl = $(this).find("input[name='LDvCyl']").val();
            var LDvAxix = $(this).find("input[name='LDvAxix']").val();
            var LNvSPH = $(this).find("input[name='LNvSPH']").val();
            var LNvCyl = $(this).find("input[name='LNvCyl']").val();
            var LNvAxix = $(this).find("input[name='LNvAxix']").val();
            each_array['left'] = {
                'LDvSPH': LDvSPH,
                'LDvCyl': LDvCyl,
                'LDvAxix': LDvAxix,
                'LNvSPH': LNvSPH,
                'LNvCyl': LNvCyl,
                'LNvAxix': LNvAxix,
            }
        });
        pg_power[i] = each_array;
        i++;
    });

    eye_data['refraction'] = {
        'snellen_right': snellen_chart_right,
        'snellen_left': snellen_chart_left,
        'right': eye_right_details,
        'left': eye_left_details,
        'ketometer_right': ketometer_right,
        'ketometer_left': ketometer_left,
        'subrefraction_right': subrefraction_right,
        'subrefraction_left': subrefraction_left,
        'prism_right': prism_right,
        'prism_left': prism_left,
        'pg_power': pg_power,
    }
    eye_data['head_details'] = {
        'refraction_notes': $('#eye_refraction_notes').val(),
        'presenting_complaints': $('#eye_presenting_complaints').val(),
        'past_history': $('#eye_presenting_history').val(),
    }
    eye_data['prescription_details'] = {
        'glass_prescription_right': eye_right_details,
        'glass_prescription_left': eye_left_details,
    }
    return JSON.stringify(eye_data);
}

function deletePGPower(obj) {
    obj.closest('.pg_power_data').remove();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
}

function savePatientRefraction() {
    var patient_refraction_id = $('#patient_refraction_id').val();
    var dilation_status = $('#refraction_dilation').is(":checked");
    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    var visit_id = $("#visit_id").val();
    var encounter_id = $('#encounter_id').val();
    var from_list = $('#from_list').val();
    var getEyeVitals_string = getEyeVitals();
    var url = base_url + "/eye_emr/savePatientRefraction";
    var flag = 0;
    if (patient_id) {
        var param = {
            patient_refraction_id: patient_refraction_id,
            patient_id: patient_id,
            visit_id: visit_id,
            encounter_id: encounter_id,
            doctor_id: doctor_id,
            dilation_status: dilation_status,
            getEyeVitals_string: getEyeVitals_string
        };
        $.ajax({
            url: url,
            type: "POST",
            data: param,
            beforeSend: function () {
                $('#refractionDilationModelDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
                $('#savePatientRefractionBtn').attr('disabled', true);
                $('#savePatientRefractionSpin').removeClass('fa fa-save');
                $('#savePatientRefractionSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (res) {
                if (parseInt(res.status) == 100) {
                    toastr.success(res.message);
                    flag = 1;
                    getRefractionList();
                } else {
                    toastr.warning(res.message);
                }
            },
            complete: function () {
                $("#refractionDilationModelDiv").LoadingOverlay("hide");
                $('#savePatientRefractionBtn').attr('disabled', false);
                $('#savePatientRefractionSpin').removeClass('fa fa-spinner fa-spin');
                $('#savePatientRefractionSpin').addClass('fa fa-save');
                if (flag == 1 && parseInt(from_list) == 2) {
                    getEyeRefractionHistory();
                }

            }
        });
    } else {
        toastr.warning("Please select any select");
    }
}


function getEyeRefractionHistory() {
    var doctor_id = $('#doctor_id').val();
    var patient_id = $('#patient_id').val();
    var search_date = $('#op_patient_search_date').val();
    var visit_id = $("#visit_id").val();
    var encounter_id = $('#encounter_id').val();
    var url = base_url + "/eye_emr/getEyeRefractionHistory";
    if (patient_id) {
        var param = {
            patient_id: patient_id,
            visit_id: visit_id,
            encounter_id: encounter_id,
            doctor_id: doctor_id,
            search_date: search_date
        };
        $.ajax({
            url: url,
            type: "POST",
            data: param,
            beforeSend: function () {
                $('.eye_history_list').show();
                $('#eye_refraction_present').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (res) {
                $('#refraction_notes_div').html(res.presenting_complaints);
                $('#refraction_notes_div').html(res.refraction_notes);
                $('#show_presenting_complaints_div').html(res.presenting_complaints);
                $('#show_eye_ast_history_div').html(res.past_history);
                $('#notes_history').html(res.notes_history);
                $('#glass_prescription_history').html(res.glass_prescription);
                $('#eye_refraction_present').html(res.today_visit);
                $('#eye_refraction_history').html(res.full_visit);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                $("#eye_refraction_present").LoadingOverlay("hide");
            }
        });
    } else {
        toastr.warning("Please select any select");
    }
}

function editGlassPrescription(glass_presc) {
    var url = base_url + "/eye_emr/editGlassPrescription";
    var param = {
        glass_presc: glass_presc
    };
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            if (parseInt(glass_presc) != 0) {
                $('#glassPrescBtn' + glass_presc).attr('disabled', true);
                $('#glassPrescSpin' + glass_presc).removeClass('fa fa-edit');
                $('#glassPrescSpin' + glass_presc).addClass('fa fa-spinner fa-spin');
                $('#GlassPrescriptionHistoryBtn').show();
            } else {
                $('#GlassPrescriptionHistoryBtn').attr('disabled', true);
                $('#GlassPrescriptionHistoryBtn').removeClass('fa fa-plus');
                $('#GlassPrescriptionHistoryBtn').addClass('fa fa-spinner fa-spin');
            }
            $('#glass_prescription_id').val(glass_presc);
        },
        success: function (res) {
            $('#glass_prescription_data_list').html(res.glass_prescription);
        },
        complete: function () {
            if (parseInt(glass_presc) != 0) {
                $('#glassPrescBtn' + glass_presc).attr('disabled', false);
                $('#glassPrescSpin' + glass_presc).removeClass('fa fa-spinner fa-spin');
                $('#glassPrescSpin' + glass_presc).addClass('fa fa-edit');
            } else {
                $('#GlassPrescriptionHistoryBtn').attr('disabled', false);
                $('#GlassPrescriptionHistoryBtn').removeClass('fa fa-spinner fa-spin');
                $('#GlassPrescriptionHistoryBtn').addClass('fa fa-plus');
                $('#GlassPrescriptionHistoryBtn').hide();
            }
        }
    });
}

function printGlassPrescription(glassPrescription_id, obj) {
    if (glassPrescription_id != '' && glassPrescription_id != undefined) {
        var url = base_url + "/eye_emr/printGlassPrescription";
        var doctor_id = $('#doctor_id').val();
        var patient_id = $('#patient_id').val();
        $.ajax({
            type: "POST",
            url: url,
            data: {
                glassPrescription_id: glassPrescription_id,
                doctor_id: doctor_id,
                patient_id: patient_id
            },
            beforeSend: function () {
                if (obj) {
                    $(obj).removeClass('fa-print printBtn').addClass('fa-spinner fa-spin');
                } else {
                    $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-print').addClass('fa-spinner fa-spin');
                }
            },
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
            },
            complete: function () {
                if (obj) {
                    $(obj).removeClass('fa-spinner fa-spin').addClass('fa-print printBtn');
                } else {
                    $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-spinner fa-spin').addClass('fa-print');
                }
            }
        });
    }
}
