var minimun_searchable_alphabet = $('#minimun_searchable_alphabet').val();
var invest_date_cnt = 1;

function refreshInvestigationGroupTree() {
    // Hide all lists except the outermost.
    $('ul.investigation_group_tree ul').hide();

    $('.investigation_group_tree li > ul').each(function (i) {
        var $subUl = $(this);
        var $parentLi = $subUl.parent('li');
        var $toggleIcon = '<i class="js-toggle-icon">+</i>';

        $parentLi.addClass('has-children');

        $parentLi.prepend($toggleIcon).find('.js-toggle-icon').on('click', function () {
            $(this).text($(this).text() == '+' ? '-' : '+');
            $subUl.slideToggle('fast');
        });
    });
}

$(document).ready(function () {
    fetchInvestigationGroups();
});

function fetchInvestigationGroups() {
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/fetchInvestigationGroups";
    var params = {};
    var patient_id = $('#patient_id').val();
    var doctor_id = $('#doctor_id').val();
    var investigation_type = $(".investigation_type").val();
    params.patient_id = patient_id;
    params.doctor_id = doctor_id;
    params.investigation_type = investigation_type;
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $(".investigation_tree_area").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $(".investigation_tree_area").html(data);
            refreshInvestigationGroupTree();
            markSelectedInvestigations();

        },
        complete: function () {
            $(".investigation_tree_area").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}


function addInvestigationDate(invest_cnt) {
    $('#invest_cnt_hiddden').val(invest_cnt);
    var investigation_name = $('#investigation_name' + invest_cnt).html();
    var investigation_date = $('#investigation_date' + invest_cnt).val();
    $("#investigationDateTimePickerModelHeader").html(investigation_name);
    $("#investigationServiceDate").val(investigation_date);
    setTimeout('$("#investigationServiceDate").focus();', 500);
    $("#investigationDateTimePickerModel").modal({
        backdrop: 'static',
        keyboard: false
    });
}


$("#investigationServiceDate").on("blur", function () {
    editServiceDate();
});


function editServiceDate() {
    invest_cnt_id = $('#invest_cnt_hiddden').val();
    investigationServiceDate = $('#investigationServiceDate').val();
    if (investigationServiceDate) {
        $('#investigation_date' + invest_cnt_id).val(investigationServiceDate);
        $("#investigationDateTimePickerModel").modal('hide');
    }
}

$(document).on('click', '.parent_group_item', function () {
    var that = this;
    $(that).parents('li').find('.child_group_item').each(function (key, val) {
        var service_name = $(val).attr('data-service-name');
        var sub_dept_name = $(val).attr('data-sub-dept-name');
        var service_code = $(val).val();
        var group_id = $(val).parents('li').find('.parent_group_item').val();
        if ($(that).prop('checked')) {
            if (parseInt(investigation_future_date) == 1) {
                if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                    $(that).parents('li').find('.child_group_item').prop('checked', true);
                    $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id="' + group_id + '"><td class="selected_investigation_service_name" id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;font-size: 13px !important;">' + service_name + ' ( ' + sub_dept_name + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + current_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                    invest_date_cnt++;
                }
            } else {
                if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                    $(that).parents('li').find('.child_group_item').prop('checked', true);
                    $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id="' + group_id + '"><td class="selected_investigation_service_name" style="width: 90% !important;font-size: 13px !important;">' + service_name + ' ( ' + sub_dept_name + ' )</td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                }
            }

        } else {
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length > 0) {
                $(that).parents('li').find('.child_group_item').prop('checked', false);
                $(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').remove();
            }
        }
    })
    if ($(that).prop('checked')) {
        toastr.success("Items added");
    } else {
        toastr.success("Items removed");
    }
});


$(document).on('click', '.child_group_item', function () {
    var service_name = $(this).attr('data-service-name');
    var sub_dept_name = $(this).attr('data-sub-dept-name');
    var service_code = $(this).val();
    var group_id = $(this).parents('li').find('.parent_group_item').val();
    if ($(this).prop('checked')) {
        if (parseInt(investigation_future_date) == 1) {
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id="' + group_id + '"><td class="selected_investigation_service_name" id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;font-size: 13px !important;">' + service_name + ' ( ' + sub_dept_name + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + current_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                invest_date_cnt++;
                toastr.success("Item added");
            }
        } else {
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id="' + group_id + '"><td class="selected_investigation_service_name" style="font-size: 13px !important;">' + service_name + ' ( ' + sub_dept_name + ' )</td><td class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                toastr.success("Item added");
            }
        }
    } else {
        if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length > 0) {
            $(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').remove();
            toastr.success("Item removed");
        }
    }
});

$(document).on('click', '.selected_investigation_delete_btn i', function () {
    var service_code = $(this).parents('tr').attr('data-service-code');
    $('.child_group_item[value="' + service_code + '"]').prop('checked', false);
    $(this).parents('tr').remove();
    toastr.success("Item removed");
})
$(document).on('click', '.add_investigation_delete_btn i', function () {
    // var service_code = $(this).parents('tr').attr('data-service-code');
    // $('.child_group_item[value="'+service_code+'"]').prop('checked', false);
    $(this).parents('tr').remove();
})

$(document).on('click', '.investigation_type_btn', function () {
    var investigation_type = $(this).find('.options').val();
    $(".investigation_type").val(investigation_type)
    fetchInvestigationGroups();
})

function markSelectedInvestigations() {
    $(".selected_investigation_table_body tr").each(function (key, val) {
        var service_code = $(val).attr('data-service-code');
        var group_id = $(val).attr('data-group-id');
        $(".investigation_group_tree li").each(function (key1, val1) {
            $(val1).find('input[value="' + service_code + '"]').prop('checked', true);
        });

    })
}

function resetSelectedInvestigations() {
    $(".selected_investigation_table_body").empty();
    $(".parent_group_item").prop('checked', false);
    $(".child_group_item").prop('checked', false);
    $(".investigation_item_search_textbox").val('');

}

function startLoader(class_name) {

    $("." + class_name).append('<div class="ecg_loader_wrapper"><div class="ecg_loader_blockcont"><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div><div class="ecg_loader_block"></div></div></div>');

}

function stopLoader(class_name) {
    $("." + class_name).find('.ecg_loader_wrapper').remove();
}

//investigation search
var inv_timeout = null;
var inv_last_search_string = '';
$(document).on('keyup', '.investigation_item_search_textbox', function (event) {
    //  event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_inv_string = $(this).val();
    var patient_id = $('#patient_id').val();
    if ($('#addItemtoFavouriteModel').is(':visible')) {
        patient_id = 1;
    } else {
        patient_id = $('#patient_id').val();
    }

    if (search_inv_string == "" || search_inv_string.length < minimun_searchable_alphabet) {
        inv_last_search_string = '';
        // console.log(minimun_searchable_alphabet);
        return false;
    } else {
        var type = $('.investigation_type_btn.active').find('input').val();


        clearTimeout(inv_timeout);
        inv_timeout = setTimeout(function () {
            if (search_inv_string == inv_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr_lite/getInvestigationSearch";
            var doctor_id = $('#doctor_id').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    search_inv_string: search_inv_string,
                    type: type,
                    patient_id: patient_id,
                    doctor_id: doctor_id
                },
                beforeSend: function () {
                    $('#InvestigationTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";
                    var inv_list = $('.investigation-list-div');
                    var inv_search_list = $('#ListInvestigationSearchData');


                    $(inv_list).show();
                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let service_desc = response[i].service_desc;
                            let service_code = response[i].service_code;
                            let sub_department = response[i].sub_department;

                            res_data += '<tr><td style="cursor:pointer; padding-left:5px !important;font-size: 13px !important;" class="item_service_desc">' + service_desc + '</td><input type="hidden" name="list_service_code_value_hid[]" id="list_service_code_value_hid-' + i + '" value="' + service_code + '"><td style="padding-left:5px !important; cursor:pointer;font-size: 13px !important;" class="item_sub_dept">' + sub_department + '</td></tr>';
                            // res_data += '<tr><td style="text-align:center; padding-left:5px !important;"><i class="fa fa-star fav-' + service_code + '" style="cursor:pointer;" onclick="favouriteInvestigation(\'' + service_code + '\', this)"></i></td><td style="cursor:pointer; padding-left:5px !important;" class="item_service_desc">' + service_desc + '</td><input type="hidden" name="list_service_code_value_hid[]" id="list_service_code_value_hid-' + i + '" value="' + service_code + '"><td style="padding-left:5px !important; cursor:pointer;" class="item_sub_dept">' + sub_department + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td style="width: 75%; ">No Data Found..!</td><td><button type="button" class="btn btn-sm btn-block addOutsideInvestigationBtn" style=" margin: auto; " onclick="addOutsideInvestigation();"><i class="fa fa-plus"></i> Add Outside Investigation</button></td></tr>';

                    }

                    $(inv_search_list).html(res_data);
                    inv_last_search_string = search_inv_string;
                    $(".inv_theadscroll").animate({
                        scrollTop: 0
                    }, "slow");

                },
                complete: function () {
                    $('.inv_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

function addOutsideInvestigation() {
    var service_name = "";
    if ($('#addItemtoFavouriteModel').is(':visible')) {
        service_name = $("#addItemtoFavouriteModel").find(".investigation_item_search_textbox").val();
    } else {
        service_name = $('.investigation_search_area').find(".investigation_item_search_textbox").val();
    }
    var service_code = '';
    var group_id = '';
    var type = $('.investigation_type_btn.active').find('input').val();

    if (service_name != '' && service_name != undefined) {
        if (parseInt(investigation_future_date) == 1) {
            $(".selected_investigation_table_body").append('<tr data-invest-type="' + type.toUpperCase() + '" data-service-code="' + service_code + '" data-group-id="' + group_id + '"><td class="selected_investigation_service_name" id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;font-size: 13px !important;">' + service_name + ' ( ' + sub_dept_name + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + current_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
            invest_date_cnt++;
        } else {
            $(".selected_investigation_table_body").append('<tr data-invest-type="' + type.toUpperCase() + '" data-service-code="' + service_code + '" data-group-id="' + group_id + '"><td class="selected_investigation_service_name" style="font-size: 13px !important;">' + service_name + ' ( ' + type.toUpperCase() + ' )</td><td class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
        }
        $(".close_btn_inv_search").trigger('click');
        $(".investigation_item_search_textbox").val('');
    }
}


//ListInvestigationSearchData
//when select Investigation Search
$(document).on('click', '#ListInvestigationSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */
    // $('.investigation-list-div').hide();
    $('.investigation_item_search_textbox').val('');
    let tr = $(this);

    let td = $(tr).find('td');

    let service_code = $(tr).find('input[name="list_service_code_value_hid[]"]').val();
    let service_desc = $(tr).find('.item_service_desc').text();
    let item_sub_dept = $(tr).find('.item_sub_dept').text();
    var service_restriction_flag = 0;
    var append_body;
    if (service_code != '' && service_code != undefined) {
        if ($('#addItemtoFavouriteModel').is(':visible')) {
            append_body = 'service_list_table_body_grp';

            if (parseInt(investigation_future_date) == 1) {
                t_body_tr = '<tr data-service-code="' + service_code + '" data-group-id=""><td id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;text-align:left" class="add_investigation_service_name">' + service_desc + ' ( ' + item_sub_dept + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + current_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="add_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>';
            } else {
                t_body_tr = '<tr data-service-code="' + service_code + '" data-group-id=""><td class="add_investigation_service_name">' + service_desc + ' ( ' + item_sub_dept + ' )</td><td class="add_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>';
            }

        } else {
            append_body = 'selected_investigation_table_body';
            if (parseInt(investigation_future_date) == 1) {
                t_body_tr = '<tr data-service-code="' + service_code + '" data-group-id=""><td id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;text-align:left" class="selected_investigation_service_name" style="font-size: 13px !important;">' + service_desc + ' ( ' + item_sub_dept + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + current_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>';
                invest_date_cnt++;
            } else {
                t_body_tr = '<tr data-service-code="' + service_code + '" data-group-id=""><td class="selected_investigation_service_name" style="font-size: 13px !important;">' + service_desc + ' ( ' + item_sub_dept + ' )</td><td class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>';
            }
        }


        if ($("." + append_body).find('tr[data-service-code="' + service_code + '"]').length == 0 || service_code == "") {
            $("." + append_body).append('' + t_body_tr + '');
            toastr.success("Item added");
        } else {
            bootbox.alert('Item already exists.');
        }

    }

});



function checkServiceRestriction(patient_id, service_code) {
    let url = $('#base_url').val();
    url = url + "/emr/check_service_restriction";
    var returnVal = 0;
    $.ajax({
        type: "GET",
        url: url,
        data: {
            patient_id: patient_id,
            service_code: service_code,
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var service_restriction = obj.service_restriction;
            if (service_restriction == 1) {
                var restrict_duration_type = obj.restrict_duration_type;
                var time_difference_minutes = obj.time_difference_minutes;
                if (restrict_duration_type == 1) {
                    toastr.warning("Warning.! This Investigation Already entered " + time_difference_minutes + ' minutes ago');
                    returnVal = 0;

                } else if (restrict_duration_type == 2) {
                    toastr.error("Warning.! This Investigation Already entered " + time_difference_minutes + ' minutes ago');
                    returnVal = 1;

                }
            } else {
                returnVal = 0;
            }

        }
    });
    return returnVal;
}


$(document).on('click', '.close_btn_inv_search', function () {
    $('.investigation-list-div').hide();
})

$(document).on("click", '.copyInvestigationBtn', function () {
    $(this).parents('.investigation_history_item').find('.investigation_list_item').each(function (key, value) {
        var service_code = $(value).attr('data-service-code') ? $(value).attr('data-service-code') : '';

        var service_desc = $(value).find('.invest_history_item_desc').html();
        if (parseInt(investigation_future_date) == 1) {
            var service_date = $(value).attr('data-service-date') ? $(value).attr('data-service-date') : '';
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id=""><td class="selected_investigation_service_name" id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;font-size: 13px !important;">' + service_desc + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + service_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                invest_date_cnt++;
            }
        } else {
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id=""><td class="selected_investigation_service_name" style="font-size: 13px !important;">' + service_desc + ' )</td><td class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                invest_date_cnt++;
            }
        }
    });

    $("#investigation_head_id").val('');
    $(".updateInvestigationBtnDiv").hide();
    if ($("#prescription_head_id").val() == "" && $("#ca_head_id").val() == "") {
        $(".saveButtonDiv").show();
    }
})

$(document).on("click", '.editInvestigationBtn', function () {
    $(".selected_investigation_table_body").empty();
    var investigation_id = $(this).parents('.investigation_history_item').attr('data-investigation-head-id');
    $("#investigation_head_id").val(investigation_id);
    $(this).parents('.investigation_history_item').find('.investigation_list_item').each(function (key, value) {
        var service_code = $(value).attr('data-service-code') ? $(value).attr('data-service-code') : '';
        var service_desc = $(value).find('.invest_history_item_desc').html();
        if (parseInt(investigation_future_date) == 1) {
            var service_date = $(value).attr('data-service-date') ? $(value).attr('data-service-date') : '';
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id=""><td class="selected_investigation_service_name" id="investigation_name' + invest_date_cnt + '" style="width: 60% !important;font-size: 13px !important;">' + service_desc + ' )</td><td style="width: 30% !important;"><input style="cursor: pointer;" onclick="addInvestigationDate(' + invest_date_cnt + ')" value="' + service_date + '" type="text" readonly id="investigation_date' + invest_date_cnt + '" class="form-control selected_investigation_currentdate"></td><td style="width: 10% !important;" class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                invest_date_cnt++;
            }
        } else {
            if ($(".selected_investigation_table_body").find('tr[data-service-code="' + service_code + '"]').length == 0) {
                $(".selected_investigation_table_body").append('<tr data-service-code="' + service_code + '" data-group-id=""><td class="selected_investigation_service_name" style="font-size: 13px !important;">' + service_desc + ' )</td><td style="width: 30% !important;"><td class="selected_investigation_delete_btn"><i class="fa fa-times red"></i></td></tr>');
                invest_date_cnt++;
            }
        }

    });

    $(".saveButtonDiv").hide();
    $(".updateInvestigationBtnDiv").show();
})


$(document).on("click", '.updateInvestigationBtn', function () {
    // investigation details

    var patient_clinical_data = {};
    patient_clinical_data.patient_id = $('#patient_id').val();
    patient_clinical_data.visit_id = $('#visit_id').val();
    patient_clinical_data.encounter_id = $('#encounter_id').val();
    patient_clinical_data.doctor_id = $('#doctor_id').val();
    var investigations = {};
    investigations.investigation_list = [];

    $(".selected_investigation_table_body").find('tr').each(function (key, val) {
        var investigation = {};
        investigation.detail_id = $(val).attr('data-detail-id') ? $(val).attr('data-detail-id') : '';
        investigation.service_name = $(val).find('.selected_investigation_service_name').html();
        investigation.service_code = $(val).attr('data-service-code');
        investigation.invest_type = $(val).attr('data-invest-type') ? $(val).attr('data-invest-type') : '';
        investigation.service_date = $(val).find('.selected_investigation_currentdate').val();
        investigations.investigation_list.push(investigation);
    })

    investigations.remark = $(".investigation_remarks_textarea").val();
    investigations.clinical_history = $(".investigation_clinical_history_textarea").val();

    patient_clinical_data.investigations = investigations;
    patient_clinical_data.investigation_head_id = $('#investigation_head_id').val();

    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/updatePatientInvestigation";
    var that = this;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_clinical_data: patient_clinical_data
        },
        beforeSend: function () {
            $(that).find('i').attr('disabled', true);
            $(that).find('i').removeClass('fa fa-save');
            $(that).find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            toastr.success("Success");
            $("#investigation_head_id").val('');
            $(".selected_investigation_table_body").empty();
            $(".saveButtonDiv").show();
            $(".updateInvestigationBtnDiv").hide();
            $('.parent_group_item').prop('checked', false);
            $('.child_group_item').prop('checked', false);
            fetchPatientDetails();
        },
        complete: function () {
            $(that).find('i').attr('disabled', false);
            $(that).find('i').removeClass('fa fa-spinner fa-spin');
            $(that).find('i').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

    // end of investigation

})

$(document).on("click", ".printInvestigationButton", function () {
    var that = this;
    var investigation_head_id = $(this).parents('.investigation_history_item').attr('data-investigation-head-id');
    printInvestigation(investigation_head_id, that);
});


function printInvestigation(investigation_head_id, obj) {
    if (investigation_head_id != '' && investigation_head_id != undefined) {
        let url = $('#base_url').val() + "/emr_lite/printInvestigation";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                investigation_head_id: investigation_head_id,
            },
            beforeSend: function () {
                if (obj) {
                    $(obj).removeClass('fa-print printBtn').addClass('fa-spinner fa-spin');
                } else {
                    $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-print').addClass('fa-spinner fa-spin');
                }
            },
            success: function (data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print();window.close();  },1000)</script>');
            },

            complete: function () {
                if (obj) {
                    $(obj).removeClass('fa-spinner fa-spin').addClass('fa-print printBtn');
                } else {
                    $('#print_prescription_and_investigation_btn').find('i').removeClass('fa-spinner fa-spin').addClass('fa-print');
                }
            }
        });
    }
}



$(document).on("click", ".deleteInvestigationBtn", function () {
    var that = this;
    var investigation_head_id = $(this).parents('.investigation_history_item').attr('data-investigation-head-id');
    bootbox.confirm({
        message: "Are you sure want to delete the investigation ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/emr_lite/deleteInvestigationHead";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        investigation_head_id: investigation_head_id
                    },
                    beforeSend: function () {},
                    success: function (data) {
                        if (data.status == 1) {
                            $(that).parents('.investigation_history_item').remove();
                            Command: toastr["success"]("Removed Successfully..!");
                        }
                    },
                    complete: function () {}
                });
            }
        }
    });
});


$(document).on("click", '.canceInvestigationEditBtn', function () {
    $(".updateInvestigationBtnDiv").hide();
    $(".selected_investigation_table_body").empty();
    $("#investigation_head_id").val('');
    $('.parent_group_item').prop('checked', false);
    $('.child_group_item').prop('checked', false);

    if ($("#prescription_head_id").val() == "" && $("#ca_head_id").val() == "") {
        $(".saveButtonDiv").show();
    }
});

function getLabResultRtf(lab_results_id) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    $.ajax({
        type: "GET",
        url: url,
        data: "lab_results_id=" + lab_results_id,
        beforeSend: function () {
            $('#lab_results_rtf_modal').modal('show');
        },
        success: function (data) {
            // console.log(data);
            $('#lab_results_rtf_data').html(data);
        },
        complete: function () {}
    });
}

function printTrendData(obj, bill_detail_id, sample_wise_print=0) {
    var url = $('#base_url').val() + "/lab_result/dowmloadLabResult";
    var dataparams = {
        bill_detail_id:bill_detail_id,
        sample_wise_print:sample_wise_print,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $(obj).find('i').removeClass('fa-file-pdf-o').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (obj) {
            var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write(obj + '<script>setTimeout(function(){window.print();window.close();  },1000)</script>');
        },
        complete: function () {
            $(obj).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-file-pdf-o');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },

    });
}
