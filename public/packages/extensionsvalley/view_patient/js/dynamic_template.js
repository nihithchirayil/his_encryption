function loadDynamicTempateHistory(){
    var template_id = $('#select_dynamic_template').val();
    var url = base_url + "/form_template/loadDynamicTempateHistory";
}
var base_url = $("#base_url").val();

function loadDynamicTemplate(){
    var template_id = $('#select_dynamic_template').val();
    var url = base_url + "/form_template/loadDynamicTemplate";
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var encounter_id = $('#encounter_id').val();
    var doctor_id = $('#doctor_id').val();
    var visit_type = $('#visit_type').val();

    if(patient_id == ''){
        toastr.warning("Please select patient!");
        return false;
    }

    var filter_params = {
        patient_id:patient_id,
        visit_id:visit_id,
        encounter_id:encounter_id,
        doctor_id:doctor_id,
        visit_type:visit_type,
    };

    var params = {template_id:template_id,filter_params:filter_params};
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            tinymce.remove(".tiny_class");
            $('#dynamic_template_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#dynamic_template_data').html('');
        },
        success: function (data) {
            if(data !=0){
                var obj = JSON.parse(data);
                setTimeout(function(){
                    renderDataSet(obj.template_controls,obj.data_set_result);
                    fetchPatientHeader();
                },500);
                $('#dynamic_template_data').html(obj.view);
                $('.template-name').parent().removeAttr('style');
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                if ($('#dynamic_template_data #image_canvas_container').length > 0) {
                    loadImageEditorComponent();
                }
                renderStaticComponentData(obj.static_components);
            }else{
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
            $('#dynamic_template_data').LoadingOverlay("hide");
            initialize_html_controls();
            fetchFavourites(template_id);
            fetchClinicalData();
        },
        error: function () {
        },
    });
}

function loadImageEditorComponent(){
    var params = '';
    var url = base_url + "/form_template/loadImageEditorComponent";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {

        },
        success: function (data) {
            if(data !=0){
                $('#image_canvas_container').html(data);
            }else{
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
        },
        error: function () {
        },
    });

}

function renderStaticComponentData(static_components)
{
    if ($('#dynamic_template_data').find('#static_component_pain_assessment_scale').length) {
        $(document).on("click", 'img.pain_scale_smiley', function(e) {
            $(e.target).parents('.pain_scale_radio').find("input[type='radio']").click();
            $('.pain_scale_smiley').css('opacity','.5');
            $(e.target).css('opacity','1');
        });
    }

    if ($('#dynamic_template_data').find('.form_grid_component').length) {
        $(document).on("click", 'button.txt_box_collapsible', function() {
            if ($(this).find("i").hasClass('fa-arrow-down')) {
                $(this).find("i").removeClass('fa-arrow-down').addClass('fa-arrow-up');
            } else {
                $(this).find("i").removeClass('fa-arrow-up').addClass('fa-arrow-down');
            }
        });
    }

    if ($('#dynamic_template_data').find('#static_component_allergy_form').length) {

        $("#time_taken").datetimepicker({
            format: 'MMM-DD-YYYY hh:mm a'
        });
        $("#time_taken").val(moment().format('MMM-DD-YYYY hh:mm a'));

        var allergies = static_components.allergies;
        var other_allergies = static_components.other_allergies;
        $("#static_component_allergy_form #MedicineAllergyListData").empty()
        if (allergies.length > 0) {
            for (let i = 0; i < allergies.length; i++) {
                var countRow = i + 1;
                var allergy = allergies[i];
                var table = document.getElementById("allergymedicTable").getElementsByTagName('tbody')[0];
                var row = table.insertRow(table.rows.length);
                var cell0 = row.insertCell(0);
                var cell1 = row.insertCell(1);

                cell0.setAttribute("width", "90%;");
                cell0.innerHTML = "<input type='text' class='form-control bottom-border-text searchMedicine2 btn-without-border saveMedicationAllergyNewItem' placeholder='' value='"+ allergy.description +"' autocomplete='off' " +
                " onKeyup='searchMedicine2(this.id,event)' name='allergymedicine[]' id='allergymedicine-" + countRow + "'>" +
                "<div class='ajaxSearchBox' id='alergymedicineAjaxList-" + countRow + "' style='width:100%;max-height: 515px; display:none;'>" +
                "</div><input type='hidden' id='AllergymedCodeId-" + countRow + "' class='allergyNewItemHidden' name='alergylist_medicine_code_hidden[]' value='"+ allergy.item_code +"'>" +
                " <input type='hidden' name='alrg_type[]' id='alrg_type-" + countRow + "' value='"+ allergy.type +"'> ";
                cell1.innerHTML = "<i class='fa fa-times-circle' aria-hidden='true' style='/*color:#FFFF;*/font-size: 23px; margin: 7px;' onclick='deleteNews(this)' id='delete-" + countRow + "'></i>";
            }
        }
        addRow();
        if (other_allergies.length > 0) {
            other_allergy_arr = [];
            for (let i = 0; i < other_allergies.length; i++) {
                other_allergy_arr.push(other_allergies[i].allergy);
            }
            var allergy_txt = other_allergy_arr.join("\r\n");
            $("#static_component_allergy_form #otherAllergies").val(allergy_txt);
        }
    }

    if ($('#dynamic_template_data').find('#static_component_vital_form').length) {
        var vital_batch = static_components.vital_batch;
        var vitals = static_components.vitals;
        $("#static_component_vital_form #latest_vital_batch").val(vital_batch);

        if (vitals.length > 0) {
            jQuery.each(vitals, function(i, val) {
                if (val.vital_master_id == 2) {
                    if ($("#static_component_vital_form #weight").val() == 1) {
                        var weight_lbs = (val.vital_value * 2.205).toFixed(2);
                        $("#static_component_vital_form #weight_value").val(weight_lbs);
                    }
                    if ($("#static_component_vital_form #weight").val() == 2) {
                        $("#static_component_vital_form #weight_value").val(val.vital_value);
                    }
                }
                if (val.vital_master_id == 4) {
                    if ($("#static_component_vital_form #height").val() == 3) {
                        var height_inch = (val.vital_value / 30.48).toFixed(2);
                        $("#static_component_vital_form #height_value").val(height_inch);
                    }
                    if ($("#static_component_vital_form #height").val() == 4) {
                        $("#static_component_vital_form #height_value").val(val.vital_value);
                    }
                }

                if (val.vital_master_id == 9) {
                    $("#static_component_vital_form #temperature_value_f").val(val.vital_value);
                }
                if (val.vital_master_id == 10) {
                    $("#static_component_vital_form #temperature_value").val(val.vital_value);
                }
                if (val.vital_master_id == 5) {
                    $("#static_component_vital_form #bp_systolic").val(val.vital_value);
                }
                if (val.vital_master_id == 6) {
                    $("#static_component_vital_form #bp_diastolic").val(val.vital_value);
                }
                if (val.vital_master_id == 7) {
                    $("#static_component_vital_form #pulse").val(val.vital_value);
                }
                if (val.vital_master_id == 8) {
                    $("#static_component_vital_form #respiration").val(val.vital_value);
                }
                if (val.vital_master_id == 12) {
                    $("#static_component_vital_form #oxygen").val(val.vital_value);
                }
                if (val.vital_master_id == 13) {
                    $("#static_component_vital_form #head").val(val.vital_value);
                }
                if (val.vital_master_id == 14) {
                    $("#static_component_vital_form #bmi").val(val.vital_value);
                }
                if (val.vital_master_id == 20) {
                    $("#static_component_vital_form #waist").val(val.vital_value);
                }
                if (val.vital_master_id == 21) {
                    $("#static_component_vital_form #hip").val(val.vital_value);
                }
                if (val.vital_master_id == 22) {
                    $("#static_component_vital_form #blood_sug").val(val.vital_value);
                    $("#static_component_vital_form input[name='visibility_status'][value='1']").prop('checked', true);
                }
                if (val.vital_master_id == 23) {
                    $("#static_component_vital_form #blood_sug").val(val.vital_value);
                    $("#static_component_vital_form input[name='visibility_status'][value='2']").prop('checked', true);
                }
            });
            $("#static_component_vital_form #time_taken").val(vitals[0].time_taken);
        }
    }
}

function renderDataSet(template_controls,data_set_result){

    // console.log(template_controls);
    // console.log(data_set_result);

    for(var i=0;i<template_controls.length;i++){
        var result_value = '';
        var control_type = template_controls[i].control_type;
        var id = template_controls[i].id_name;
        var data_set_key = template_controls[i].data_set_key;

        var name = template_controls[i].control_name;
        result_value = data_set_result[data_set_key];

        var textcontrols = ['Text Box','Text Area','Header','Date Picker','Time Picker','Date Time Picker','Progressive Search','Hidden Filed']
        var checkboxcontrols = ['Checkbox'];
        var radiocontrols = ['Radio Button'];
        var selectboxcontrols = ['Select Box'];
        var multiselectboxcontrols = ['Select Box - Multi Select'];
        var TinyMceControls = ['TinyMce'];
        if(textcontrols.includes(control_type)){
            if (typeof result_value !== 'undefined') {
                $('#'+id).val(result_value);
            }
        }

        if(radiocontrols.includes(control_type)){
            if (typeof result_value !== 'undefined') {
                $("input[name="+name+"][value=" + result_value + "]").attr('checked', 'checked');
            }
        }
        if(checkboxcontrols.includes(control_type)){
            if (typeof result_value !== 'undefined') {
                $("input[name="+name+"][value=" + result_value + "]").attr('checked', 'checked');
            }
        }
        if(selectboxcontrols.includes(control_type)){
            if (typeof result_value !== 'undefined') {
                $('#'+id).val(result_value);
                $('#'+id).trigger('change');
            }
        }
        if(multiselectboxcontrols.includes(control_type)){
            if (typeof result_value !== 'undefined') {
                $('#'+id).val([result_value]);
                $('#'+id).trigger('change');
            }
        }

        if(TinyMceControls.includes(control_type)){
            if (typeof result_value !== 'undefined') {
                tinymce.get(id).setContent(result_value);
            }
        }

    }
}

function fetchFavourites(template_id){
    var url = base_url + "/form_template/fetchFavourites";
    var params = {template_id:template_id};
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {

        },
        success: function (data) {

        },
        complete: function () {
            $('[data-toggle="popover"]').popover({ html : true });
        },
        error: function () {

        },
    });
}

function initialize_html_controls() {
    tinymce.init({
        selector: '.tiny_class',
        branding: false,
        statusbar: false,
        height:250,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A',
    });
}

function addToDynamicBookmark(template_name){
    var template_id = $('#select_dynamic_template').val();
    var doctor_id =  $('#doctor_id').val();
    var content = $('#'+template_name).val();
        if(content !=''){
            content = btoa(content);
        }else{
            return false;
        }
    var url = base_url + "/form_template/addToDynamicBookmark";
    var dataParams = {
        template_id:template_id,
        doctor_id:doctor_id,
        content:content,
        template_name:template_name,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: dataParams,
        beforeSend: function () {

            $('#btn_past_history').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-star-o');
        },
        success: function (data) {
            if(data !=0){
                toastr.info("Added to favourites");
            }else{
                toastr.error("Error please check your internet connection");
            }
        },
        complete: function () {
            $('#btn_past_history').find('i').removeClass('fa fa-spinner fa-spin').addClass('fa fa-star-o');
        },
        error: function () {
        },
    });

}


function loadDynamicTempateHistory(){
    let patient_id = $('#patient_id').val();
    if(patient_id ==''){
        Command: toastr["warning"]("Select patient!");
        return false;
    }
    var url = $('#base_url').val() + "/emr/loadDynamicTempateHistory";
    if (patient_id) {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                patient_id: patient_id,
            },
            beforeSend:function(){
                $("#dynamic_template_history_section").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("#dynamic_template_history_section").LoadingOverlay("hide");
                $("#dynamic_template_history_section").html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            error: function () {
                $("#dynamic_template_history_section").LoadingOverlay("hide");
            }
        });
    }
}

function printPreviewDynamicTemplate(template_data_id,print_status){
    var url = $('#base_url').val() + "/emr/printPreviewDynamicTemplate";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            template_data_id: template_data_id,
        },
        beforeSend:function(){
            $('#dynamicTemplatePrintPreviewModal').modal('show');
            $('#dynamicTemplatePrintPreviewModalData').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#dynamicTemplatePrintPreviewModalData').LoadingOverlay("hide");
            if(data!=0){
                var obj = JSON.parse(data);
                var template_control_data = obj.template_control_data;
                var group_value = obj.group_value_data_array_container;
                var hospital_header = obj.hospital_header;

                var hospital_header_html = '<div class="hospital_header col-md-12 no-padding" style="display:none;margin-bottom:8px;">'+hospital_header+'</div>';
                $('.hospital_header').html(hospital_header);
                //console.log(hospital_header_html);
                $('#dynamicTemplatePrintPreviewModalData').html(hospital_header_html+obj.template_html);
                $('.contenteditable').hide();
                $('#dynamicTemplatePrintPreviewModalData label').css('color','darkgrey');
                if(print_status == 1){
                    $('#btn_printDynamicTemplate').show();
                }else{
                    $('#btn_printDynamicTemplate').hide();
                }
                var template_data = obj.template_data;
                template_data = JSON.parse(template_data);
                console.log(template_data);

                $('#show_print_id10').html(template_data.id10);
                $('#show_print_id11').html(template_data.id11);
                $('#show_print_id12').html(template_data.id12);
                $('#show_print_id13').html(template_data.id13);
                $('#show_print_id14').html(template_data.id14);
                $('#show_print_id15').html(template_data.id15);
                $('#show_print_id16').html(template_data.id16);
                $('#show_print_id17').html(template_data.id17);


                var keys = Object.keys(template_data);
                $('.hide_print').hide();

                $('.show_print').each(function(){
                    $(this).parent().css('margin-bottom','20px');
                });

                for(var i=0;i<keys.length;i++){

                    for(var j=0;j<template_control_data.length;j++){
                        if(template_control_data[j]['id_name'] == keys[i]){
                            let text_array = [1,2,14,8,9,10];
                            let checkbox_and_radio_array = [3,4];
                            let select_box_array = [5,6];

                            //----print textbox,text area, date time pickers data------------------------------------------
                            if(text_array.includes(template_control_data[j]['master_control_id'])){
                                if(template_data[keys[i]] !=''){
                                    $('#show_print_'+keys[i]).html(template_data[keys[i]]);
                                }else{
                                    $('#show_print_'+keys[i]).html('&nbsp;');
                                }

                            }

                            //----checkbox and radio button--------------------------------------------
                            if(checkbox_and_radio_array.includes(template_control_data[j]['master_control_id'])){
                                if(template_data[keys[i]] !=''){
                                    var checkArray = template_data[keys[i]];
                                    checkArray = checkArray.toString();
                                    checkArray = checkArray.replace(/,,+/g, ',');
                                    checkArray = checkArray.replace(/(^,|,$)/g, '');

                                    $('#show_print_'+keys[i]).html(checkArray);
                                }else{
                                    $('#show_print_'+keys[i]).html('&nbsp;');
                                }
                            }

                            //-----select box and multiple select box---------------------------------
                            if(select_box_array.includes(template_control_data[j]['master_control_id'])){
                                if(template_data[keys[i]] !=''){
                                    var select_box_data = template_data[keys[i]];
                                    var select_box_data_array = group_value[keys[i]];
                                    select_box_data_array = JSON.parse(select_box_data_array);
                                    if(select_box_data_array !=''){
                                        $('#show_print_'+keys[i]).html(select_box_data_array[template_data[keys[i]]]);
                                    }else{
                                        $('#show_print_'+keys[i]).html('&nbsp;');
                                    }
                                }
                            }

                        }
                    }

                }
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.show_print').css('background-color','#f6f6f6');
                $('.show_print').css('padding','5px');
                $('.show_print').css('display','flex');
                $('.show_print').css('min-height','24px !important');
                $('.show_print').css('width','100%');
                $('.show_print').css('padding-left','8px');
                $('.show_print').css('border-radius','3px');
                $('.show_print').css('font-size','13px');
                $('.show_print').parent().css('margin-bottom','10px !important;');
                $('.dynamic-template-name').css('text-align','center !important;');
                $('.dynamic-template-name').css('font-weight','700 !important;');
                $('.dynamic-template-name').css('text-decoration','none !important;');
                $('.dynamic-template-name').css('color','#777777 !important;');
                $('.dynamic-template-name').css('font-family','sans-serif !important;');

            }
        },
        error: function () {

        }
    });
}

$(document).on("click", "#chk_show_hospital_header", function(){
    if ($(this).is(":checked")) {
        $(".hospital_header").show();
    } else {
        $(".hospital_header").hide();
    }
})


function printDynamicTemplate(){
    var showw = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById('dynamicTemplatePrintPreviewModalData');
    showw = showw + msglist.innerHTML;
    // $('#dynamicTemplatePrintPreviewModal').modal('hide');
    var font_awesome_path = $('#font_awsome_css_path').val();
    var bootstrap_min_path = $('#bootstrap_min_path').val();
    var fontawsome_min_path = $('#fontawsome_min_path').val();
    var bootstrapmin_js_path = $('#bootstrapmin_js_path').val();



    var winCaPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');

    winCaPrint.document.write("<style>@page {size: A4; border:1px solid black;}</style>");
    winCaPrint.document.write('<style>.list-unstyled{list-style:none;}</style>');
    winCaPrint.document.write('<style>.pain_score_img li{width:25px;display:inline-block;}</style>');
    winCaPrint.document.write('<style>.pain_score_img li img{max-width:100%;}</style>');
    winCaPrint.document.write('<style>.box-body{border:1px solid #ddd !important;float:left;clear:right;width:99%;}</style>');
    winCaPrint.document.write('<style>.pagebreak{page-break-before: always;}</style>');
    winCaPrint.document.write('<style>.hospital_header{padding:0px !important;}</style>');

    winCaPrint.document.write(
        '<link href="' + font_awesome_path + '" rel="stylesheet">'
    );
    winCaPrint.document.write(
        '<link href="' + bootstrap_min_path + '" rel="stylesheet">'
    );
    winCaPrint.document.write(
        '<link href="' + fontawsome_min_path + '" rel="stylesheet">'
    );
    winCaPrint.document.write(
        '<link href="' + bootstrapmin_js_path + '" rel="stylesheet">'
    );

    winCaPrint.document.write('<link href="http://localhost/codfoxx/public/packages/extensionsvalley/dashboard/css/bootstrap.min.css" rel="stylesheet">');
    winCaPrint.document.write('<link href="http://localhost/codfoxx/public/packages/extensionsvalley/dashboard/css/font-awesome.min.css" rel="stylesheet">');

    winCaPrint.document.write('<script src="http://localhost/codfoxx/public/packages/extensionsvalley/dashboard/js/bootstrap.min.js"></script>');
    winCaPrint.document.write('<style>.pagebreak{text-align: center;font-weight:700;text-decoration:none;color:#777777;font-family:sans-serif;}</style>');
    winCaPrint.document.write('<style>.show_print{background:#f0f0f0 !important; padding: 5px 5px 5px 8px; display: flex; width: 100%; border-radius: 3px; font-size: 13px;} label{font-size:12px !important;color:#717070;}</style>');
    winCaPrint.document.write('<style>.hospital_header table{border-color:#f0e1e1 !important;}</style>');


    winCaPrint.document.write('<style>.col-md-1 {width:8%;  float:left;page-break-inside:auto;}.col-md-2 {width:16%; float:left;page-break-inside:auto;}.col-md-3 {width:25%; float:left;page-break-inside:auto;}.col-md-4 {width:33%; float:left;page-break-inside:auto;}.col-md-5 {width:42%; float:left;page-break-inside:auto;}.col-md-6 {width:50%; float:left;page-break-inside:auto;}.col-md-7 {width:58%; float:left;page-break-inside:auto;}.col-md-8 {width:66%; float:left;page-break-inside:auto;}.col-md-9 {width:75%; float:left;page-break-inside:auto;}.col-md-10{width:83%; float:left;page-break-inside:auto;}.col-md-11{width:92%; float:left;page-break-inside:auto;}.col-md-12{width:100%; float:left;page-break-inside:auto;}</style>');

    winCaPrint.document.write('<style> table{border-collapse: collapse !important;} table tr td{border-collapse: collapse !important;}</style>' + showw + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

}


function deleteDynamicTemplate(template_data_id){
    var url = $('#base_url').val() + "/emr/deleteDynamicTemplate";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            template_data_id: template_data_id,
        },
        beforeSend:function(){
            $("#dynamic_template_history_section").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("#dynamic_template_history_section").LoadingOverlay("hide");
            if(data ==1){
                Command: toastr["success"]("Deleted Successfully!");
                loadDynamicTempateHistory();
            }
        },
        error: function () {

        }
    });
}

function editDynamicTemplate(template_data_id){
    // var url = $('#base_url').val() + "/emr/editDynamicTemplate";
    // $.ajax({
    //     url: url,
    //     type: "POST",
    //     data: {
    //         template_data_id: template_data_id,
    //     },
    //     beforeSend:function(){
    //         $("#dynamic_template_history_section").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.6)", imageColor: '#337AB7' });
    //     },
    //     success: function (data) {
    //         $("#dynamic_template_history_section").LoadingOverlay("hide");
    //         if(data ==1){
    //             Command: toastr["success"]("Deleted Successfully!");
    //             loadDynamicTempateHistory();
    //         }
    //     },
    //     error: function () {

    //     }
    // });
}

// $(document).on('click', '.select_button li', function () {
//     $(this).toggleClass('active');
//     let sel_value = $(this).attr('data-value');
//     if (sel_value != "" && sel_value != undefined) {
//         if ($(this).hasClass('active')) {
//             $(this).find('input').val(sel_value);
//             $(this).find('input').prop("disabled", false);
//         } else {
//             $(this).find('input').val("");
//             $(this).find('input').prop("disabled", true);
//         }
//     }
//
//
//
//
//
//
//
//
//
//
// });

function closeDynamicTemplateModal(){
    $('#dynamicTemplatePrintPreviewModal').modal('hide');
    $('.hide_print').show();
    $('.show_print').each(function(){
        $(this).parent().css('margin-bottom','');
        $('.show_print').hide();
    });
}
function fetchPatientHeader(){
    url = base_url +"/emr/fetchPatientDetails";
    var dataparams = {
        'patient_id':$('#patient_id').val(),
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
        },
        success: function (data) {
           if(data!=0){
                var obj = JSON.parse(data);
                $('#id10').val(obj.uhid);
                $('#id11').val(obj.patient_name);
                $('#id12').val(obj.age+'/'+obj.gender);
                $('#id13').val(obj.phone);
                $('#id14').val(obj.address);
                $('#id15').val(obj.area);
                $('#id16').val(obj.visit_date);
                $('#id17').val(obj.doctor_name);
           }
        },
        complete: function () {
        }
    });
}

function fetchClinicalData(){
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    var base_url = $('#base_url').val();
    let _token = $('#c_token').val();
    var dataparams = {patient_id:patient_id,visit_id:visit_id};
    url = base_url +"/dynamic_template/fetchClinicalData";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
        },
        complete: function () {

        }
    });
}
