$(document).on("keyup", "#weight_value,#height_value", function () {
    let w_val = '';
    let w_type = '';
    let h_val = '';
    let h_type = '';

    w_val = $('#weight_value').val();
    h_val = $('#height_value').val();
    if (h_val == "") {
        h_val = $('#height_value').val();
    }
    w_type = $('#weight').val();
    h_type = $('#height').val();

    if (w_val != '' && h_val != '' && w_type != '') {
        calculateBmi(w_val, w_type, h_val, h_type);
    }
});

function calculateBmi(weight, wtype, height, htype) {

    let weight_type = "";
    let weight_kg = "";
    weight_type = wtype;
    if (weight_type == "1") {
        weight_kg = Math.round((parseFloat(weight) / 2.2046));
    } else {
        weight_kg = Math.round(parseFloat(weight));
    }

    let height_type = "";
    let height_cm = "";
    let height_m = "";
    height_type = htype;
    if (height_type == "3") {
        let vital_value_feet = height;

        height_m = parseFloat(vital_value_feet) / 3.2808;

    } else {
        height_cm = parseFloat(height);
        height_m = parseFloat(height_cm / 100);//convert to meter
    }

    if (weight_kg != '' && height_m != '') {
        let bmi_val = '';
        bmi_val = parseFloat(weight_kg / (height_m * height_m));
        $('input[name="bmi"]').val(bmi_val.toFixed(2));
    }

}

$(document).on('keyup', '#waist', function () {
    autoCalRatio();
});
$(document).on('keyup', '#hip', function () {
    autoCalRatio();
});

function autoCalRatio() {
    var waist = $('#waist').val().trim();
    var hip = $('#hip').val().trim();
    //console.log(waist,hip)
    if (waist != '' && hip != '') {
        var waist_hip_ratio = parseFloat(waist / hip).toFixed(2);
        $('#waist_hip_ratio').val(waist_hip_ratio);
    } else {
        $('#waist_hip_ratio').val('');
    }
}

function calculateTemperature(from_type) {
    if (parseInt(from_type) == 1) {
        vital_value_c = $("#temperature_value").val();
        if (vital_value_c != '') {
            vital_value_f = ((vital_value_c * (9 / 5)) + 32).toFixed(2);
            $('#temperature_value_f').val(vital_value_f);
        } else {
            $('#temperature_value_f').val('');
        }
    } else if (parseInt(from_type) == 2) {
        vital_value_f = $("#temperature_value_f").val();
        if (vital_value_f != '') {
            vital_value_c = ((vital_value_f - 32) * (5 / 9)).toFixed(2);
            $('#temperature_value').val(vital_value_c);
        } else {
            $('#temperature_value').val('');
        }
    }
}