
var emr_changes_array = $('#emr_changes').val() ? $('#emr_changes').val() : []; 
var emr_changes_array_parse = {};
if(emr_changes_array.length > 0){
    emr_changes_array_parse = JSON.parse(emr_changes_array);
}
console.log(emr_changes_array_parse);
var notes_template_type = emr_changes_array_parse.notes_template ? emr_changes_array_parse.notes_template : 0;   

$(document).on('click', '.editAssessmentBtn', function () {
    var formdata_head_id = $(this).parents('tr').attr('data-head-id');

    var url = $('#base_url').val() + "/emr_lite/fetchDoctorAssessmentById";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            formdata_head_id: formdata_head_id
        },
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            console.log(data);
            if (data.status == 1) {
                var dynamic_assessment = data.assessment_data[0].data_text;
                console.log(notes_template_type);
                dynamic_assessment = JSON.parse(dynamic_assessment);
                var flag = true;
                $.each(dynamic_assessment, function (key, val) {
                    if(parseInt(notes_template_type)==2){
                        $("#" + key).val(val);
                        if (flag == true && val != '') {
                            $("#" + key).focus();
                            $("#" + key).val(val);
    
                            flag = false;
                        }
                    }else{
                        $("#" + key).find('textarea').val(val);
                        $("#" + key).find('input').val(val);
                        if (flag == true && val != '') {
                            $("#" + key).find('textarea').focus();
                            $("#" + key).find('input').val(val);
    
                            flag = false;
                        }
                    }
                   
                });
            }
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
        }
    });

    // var form_data = $(".form_value_"+formdata_head_id).html();
    // $("#chief_com").val(form_data);
    $("#ca_head_id").val(formdata_head_id);

    $(".updateNotesDiv").show();
    $(".saveButtonDiv").hide();
});

$(document).on('click', '.copyAssessmentBtn', function () {
    var formdata_head_id = $(this).parents('tr').attr('data-head-id');
    var url = $('#base_url').val() + "/emr_lite/fetchDoctorAssessmentById";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            formdata_head_id: formdata_head_id
        },
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            // console.log(data);
            if (data.status == 1) {
                var dynamic_assessment = data.assessment_data[0].data_text;

                var emr_changes_array = $('#emr_changes').val() ? $('#emr_changes').val() : []; 
                var emr_changes_array_parse = {};
                if(emr_changes_array.length > 0){
                    emr_changes_array_parse = JSON.parse(emr_changes_array);
                }
                // var emr_changes_array = $('#emr_changes').val() ? $('#emr_changes').val() : [];
                // var emr_changes_array_parse = JSON.parse(emr_changes_array);
                // console.log(emr_changes_array_parse);
                var notes_template_type = emr_changes_array_parse.notes_template ? emr_changes_array_parse.notes_template : 0;
                console.log(notes_template_type);
                 notes_template_type=2
                dynamic_assessment = JSON.parse(dynamic_assessment);
                var flag = true;
                $.each(dynamic_assessment, function (key, val) {
                    if(parseInt(notes_template_type)==2){
                        $("#" + key).val(val);
                        if (flag == true && val != '') {
                            $("#" + key).focus();
                            $("#" + key).val(val);
    
                            flag = false;
                        }
                    }else{
                        $("#" + key).find('textarea').val(val);
                        $("#" + key).find('input').val(val);
                        if (flag == true && val != '') {
                            $("#" + key).find('textarea').focus();
                            $("#" + key).find('input').val(val);
    
                            flag = false;
                        }
                    }
                   
                });
            }
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
        }
    });

    $("#ca_head_id").val('');

    $(".updateNotesDiv").hide();
    if ($("#investigation_head_id").val() == "" && $("#prescription_head_id").val() == "") {
        $(".saveButtonDiv").show();
    }
});

$(document).on("click", '.updateNotesBtn', function () {
    var patient_clinical_data = {};
    let patient_id = $('#patient_id').val();
    // let visit_id = $('#visit_id').val();
    // let encounter_id = $('#encounter_id').val();
    let ca_head_id = $('#ca_head_id').val();
    // let form_id = 831;
    // let tableList = $('#table_names').val();
    // let gcheckboxNamesArr = $('#gcheckbox_names').val();
    // let ca_form = $('#ca-data-form').serializeArray();
    // let psearchSelectedArr = [];
    // let dataValueArr = [];

    // var tinyList = '';
    // if ($.trim($('#tinymce_names').val()) != "") {
    //     tinyList = JSON.parse($('#tinymce_names').val());
    // }

    // //table strucure check
    // //all table field name array
    // var tableNames = "";
    // if ($.trim(tableList) != "" && $.trim(tableList) != undefined) {
    //     tableNames = JSON.parse(tableList);
    // }
    // var tableFieldNames = [];
    // var arrTableRowColData = [];
    // if ((tableNames.length) > 0) {

    //     for (var j = 0; j < tableNames.length; j++) {
    //         colsCount = $('input[name="' + tableNames[j] + '_#colsCount"]').val();
    //         rowsCount = $('input[name="' + tableNames[j] + '_#rowsCount"]').val();
    //         var fieldName = tableNames[j];
    //         tableFieldNames.push(fieldName);
    //         arrTableRowColData.push({ 'cols': colsCount, 'rows': rowsCount });
    //     }

    // }

    // //group checkbox check
    // //all group checkbox field name array
    // var gcheckboxNames = gcheckboxNamesArr;

    // var psearchNames = '';
    // //all Progress Search field name array
    // if ($.trim($('#psearch_names').val()) != "") {
    //     psearchNames = JSON.parse($('#psearch_names').val());
    // }


    // var notes_data = {};
    // //loop through all fields and create json
    // $(ca_form).each(function (index, obj) {

    //     //tinymce data setup
    //     if ((tinyList.length) > 0) {
    //         //check its a tinymce field
    //         if (tinyList.indexOf(obj.name) >= 0) {
    //             let tinycontent = '';
    //             tinycontent = tinyMCE.get(obj.name).getContent();
    //             ca_form[index].value = tinycontent;
    //             notes_data[obj.name] = tinycontent;
    //             return;
    //         }
    //     }

    //     //progress search data setup [multiselect]
    //     if ((psearchNames.length) > 0) {
    //         //check its a progress search field
    //         if (psearchNames.indexOf(obj.name) >= 0) {
    //             notes_data[obj.name + '_psearch_' + obj.value] = obj.value;
    //             psearchSelectedArr.push(obj.name + '_psearch_' + obj.value);
    //         }
    //     }

    //     notes_data[obj.name] = obj.value;

    //     if (obj.value != '' && obj.value != undefined) {
    //         dataValueArr.push(obj.value);
    //     }

    // });

    // //remove progress old keys
    // if ((psearchNames.length) > 0) {
    //     $.each(psearchNames, function (index, psdata) {
    //         if (psdata != "" && psdata != undefined) {
    //             delete notes_data[psdata];
    //         }
    //     });
    // }

    // let validate = validateAssessment(dataValueArr);

    // // if (form_id == "") {
    // //     Command: toastr["warning"]("Error.! Select Form.");
    // //     return false;
    // // }

    // if(dataValueArr.length > 0){
    //     notes_data = JSON.stringify(notes_data);
    //     notes_data = encodeURIComponent(notes_data)
    // } else {
    //     notes_data = "";
    // }



    patient_clinical_data.patient_id = patient_id;
    // patient_clinical_data.visit_id = visit_id;
    // patient_clinical_data.encounter_id = encounter_id;
    patient_clinical_data.ca_head_id = ca_head_id;
    // patient_clinical_data.form_id = form_id;
    // patient_clinical_data.arrTableRowColData = JSON.stringify(arrTableRowColData);
    // patient_clinical_data.tableFieldNames = JSON.stringify(tableFieldNames);
    // patient_clinical_data.gcheckboxNames = gcheckboxNames;
    // patient_clinical_data.psearchNames = JSON.stringify(psearchSelectedArr);
    // patient_clinical_data.notes_data = JSON.stringify(notes_data);

    var dynamic_notes_data = {};
    if(parseInt(notes_template_type)==2){
        try {
            $('.notes_data_template').each(function (key, val) {
                var element = $(val).attr('id');
                var type = $(val).attr('data-element-type');
                console.log(element);
                console.log(type);
                        dynamic_notes_data[element] = $(val).val();
              
               
            });
            dynamic_notes_data = JSON.stringify(dynamic_notes_data);
            dynamic_notes_data = escape(dynamic_notes_data);
        } catch (e) {
            dynamic_notes_data = '';
        }
    }else{
        try {
            $('.tabcontent').each(function (key, val) {
                var element = $(val).attr('id');
                var type = $(val).attr('data-element-type');
                console.log(element);
                console.log(type);
    
                    if (type == 'dyn_textarea') {
                        dynamic_notes_data[element] = $(val).find('textarea').val();
                    } else if (type == 'dyn_textbox') {
                        dynamic_notes_data[element] = $(val).find('input').val();
                    }
              
               
            });
            dynamic_notes_data = JSON.stringify(dynamic_notes_data);
            dynamic_notes_data = escape(dynamic_notes_data);
        } catch (e) {
            dynamic_notes_data = '';
        }
    }
    
  

    patient_clinical_data.dynamic_notes_data = dynamic_notes_data;
    // console.log(patient_clinical_data);
    if (dynamic_notes_data != "") {
        var url = $('#base_url').val() + "/emr_lite/updateNotesData";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_clinical_data: patient_clinical_data
            },
            beforeSend: function () {
                $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if (data.status == 1) {
                    fetchPatientDetails();
                    $("#ca-data-form").find('textarea').val('');
                    $("#ca-data-form").find('input:text').val('');
                    $("#ca-data-form").find('input:radio').prop('checked', false);
                    $("#ca-data-form").find('input:checkbox').prop('checked', false);
                    $("#ca-data-form").find('li').removeClass('active');
                    $("#ca-data-form").find('textarea').val('');
                    $("#ca-data-form").find('input:text').val('');
                    $("#ca-data-form").find('input:radio').prop('checked', false);
                    $("#ca-data-form").find('input:checkbox').prop('checked', false);
                    $("#ca-data-form").find('li').removeClass('active');
                    $("#ca-data-form").find('input:hidden').val('');

                    $(".updateNotesDiv").hide();
                    $(".saveButtonDiv").show();

                    Command: toastr["success"]("Success");
                } else {
                    Command: toastr["error"]("Error please check your internet connection");
                }

            },
            complete: function () {
                $('body').LoadingOverlay("hide");
            }
        });

    } else {
        Command: toastr["error"]("Please enter any notes data.");
    }


})


$(document).on('click', '.deleteAssessmentBtn', function () {
    var formdata_head_id = $(this).parents('tr').attr('data-head-id');
    var that = this;
    var patient_id = $("#patient_id").val();

    bootbox.confirm({
        message: "Are you sure want to delete the assessment ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/emr_lite/deleteFormData";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        formdata_head_id: formdata_head_id,
                        patient_id: patient_id
                    },
                    beforeSend: function () {
                        $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Command: toastr["success"]("Success");
                            $($(that).parents('tr')[0]).hide();
                            $($(that).parents('tr')[0]).next().hide();
                            $($(that).parents('tr')[0]).next().next().hide();
                        } else {
                            Command: toastr["error"]("Error please check your internet connection");
                        }

                    },
                    complete: function () {
                        $('body').LoadingOverlay("hide");
                    }
                });
            }
        }
    });

});

$(document).on("keyup", '.dynamic_chief_complaints_textarea', function () {
    var text_val = $(this).val();
    $(".chief_complaint").html(text_val);
})

$(document).on("click", '.bookmarked_notes_text', function () {
    var text_val = $(this).attr('title').trim();
    var old_text = $(document.activeElement).val();
    // var tab_name = $(".tablinks.active").attr('data-head-name');
    // $("#" + tab_name).find('textarea').val($("#" + tab_name).find('textarea').val() + text_val);
    // if (tab_name == 'dynamic_chief_complaints') {
    //     $(".chief_complaint").html($(".dynamic_chief_complaints_textarea").val());
    // }
    // $(".dynamic_chief_complaints_textarea").val($(".dynamic_chief_complaints_textarea").val() + text_val);
    // $(document.activeElement).val($(".dynamic_chief_complaints_textarea").val() + text_val);

    $('div[data-element-type="dyn_textarea"][style*="display: block"] textarea').val($(".dynamic_chief_complaints_textarea").val() + text_val);

    $('.dynamic_chief_complaints_textarea').trigger('keyup');

})

$(document).on("click", '.deleteBookmarkedItemBtn', function () {

    var bookmarked_id = $(this).attr('data-bookmarked-id');
    var bookmarked_head_id = $(this).attr('data-bookmarked-head-id');
    var that = this;

    // bootbox.confirm({
    //     message: "Are you sure want to delete the bookmarked assessment ?",
    //     buttons: {
    //         confirm: {
    //             label: 'Yes',
    //             className: 'btn-success'
    //         },
    //         cancel: {
    //             label: 'No',
    //             className: 'btn-danger'
    //         }
    //     },
    //     callback: function (result) {
    //         if (result) {

                var url = $('#base_url').val() + "/emr_lite/deleteBookmarkedItem";
                var doctor_id = $('#doctor_id').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        bookmarked_id: bookmarked_id, doctor_id: doctor_id
                    },
                    beforeSend: function () {
                        $(that).removeClass('fa-times').addClass('fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Command: toastr["success"]("Success");
                            getDoctorBookmarkedNotes();

                        } else {
                            Command: toastr["error"]("Error please check your internet connection");
                        }

                    },
                    complete: function () {
                        $(that).removeClass('fa-spinner fa-spin').addClass('fa-times');
                    }
                });


    //         }
    //     }
    // });


    $(this).closest('tr').remove();
    fetchBookmarkedNotes(bookmarked_head_id);

})

$(document).on("click", '.printAssessmentBtn', function () {

    var formdata_head_id = $(this).parents('tr').attr('data-head-id');
    var form_id = $(this).parents('tr').attr('data-form-id');
    var that = this;

    printAssessment(formdata_head_id, form_id, that);
})


//print ca
function printAssessment(head_id, form_id, obj) {

    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        $.ajax({
            type: "POST",
            url: url + "/emr_lite/printAssessment",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
            },
            beforeSend: function () {
                $(obj).removeClass('fa-print printBtn').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                var response = '';
                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }
                var winCaPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winCaPrint.document.write(response + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

            },
            complete: function () {
                $(obj).removeClass('fa-spinner fa-spin').addClass('fa-print printBtn');
            }
        });
    }
}


$(document).on("click", '.canceAssessmentEditBtn', function () {
    $(".updateNotesDiv").hide();
    $(".prescription_list_table_body").empty();
    $("#chief_com").val('');
    $("#ca_head_id").val('');

    if ($("#investigation_head_id").val() == "" && $("#prescription_head_id").val() == "") {
        $(".saveButtonDiv").show();
    }
})

$(document).on("click", '.assessmentFavButton', function () {
    // var notes = $("#chief_com").val();
    var tab_name = $(".tablinks.active").attr('data-head-name');
    var head_id = $(".tablinks.active").attr('data-head-id');
    var notes ="";
    alert(head_id);
    notes = $("#"+tab_name).find('textarea').val();
    if (notes == '' || notes == undefined) {
        notes = $("#"+tab_name).find('input').val();
    }

    if (notes != '' && notes != undefined) {
        bootbox.prompt({
            title: "Enter name for bookmark.",
            centerVertical: true,
            callback: function (result) {
                if (result) {
                    let url = $('#base_url').val();
                    let patient_id = $("#patient_id").val();
                    var doctor_id = $('#doctor_id').val();
                    $.ajax({
                        type: "POST",
                        url: url + "/emr_lite/bookmarkAssessment",
                        data: {
                            notes: notes,
                            bookmark_name: result,
                            head_id: head_id,
                            doctor_id: doctor_id,
                        },
                        beforeSend: function () {
                            $(".assessmentFavButton").removeClass('fa-star').addClass('fa-spinner fa-spin');
                        },
                        success: function (data) {

                            if (data.status == 1) {
                                $(".notes_bookmarks_area").prepend('<div class="bookmarked_notes_item"><span class=" bookmarks-notes-text bookmarked_notes_text" title="' + notes + '"> ' + result + '</span> <i style="margin-left: 8px;" class="fa fa-times red deleteBookmarkedItemBtn" data-bookmarked-id="' + data.bookmarked_id + '"> </i></div>')
                            } else {
                                Command: toastr["error"]("Error please check your internet connection");
                            }

                        },
                        complete: function () {
                            $(".assessmentFavButton").removeClass('fa-spinner fa-spin').addClass('fa-star');
                        }
                    });
                }
            }
        });
    }
})

