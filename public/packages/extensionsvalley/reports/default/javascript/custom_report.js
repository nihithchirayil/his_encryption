$(document).ready(function () {
    setTimeout(function () {
        //$(body).addClass('sidebar-collapse');
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");
    }, 300);
    // $('tbody').sortable();
    $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });


});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.insurancereport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

// f

//---------generate csv------------------------------------------------------

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");
};

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}




function print_generate(printData) {
    //$('#print_config_modal').modal('hide');


     var showw = "";
     var printMode = $('input[name=printMode]:checked').val();
     var printhead = "";

     var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if($('#showTitle').is(":checked")){
        printhead  = $("#hospital_headder").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
mywindow.document.write('<head><style>');
mywindow.document.write('table {border-collapse:collapse;page-break-inside:auto;}');
mywindow.document.write('table td, table th{ border: 1px solid #000;padding:5px;}');
mywindow.document.write('tr{ page-break-inside:avoid; page-break-after:auto }');
mywindow.document.write('.td_common_numeric_rules {border-left: solid 1px #bbd2bd !important;text-align: right !important}');
mywindow.document.write('.td_common_numeric_rules { border-left: solid 1px #bbd2bd !important;text-align: right !important');
mywindow.document.write('body {margin-top: 2cm;}');
mywindow.document.write('</style></head>');
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin-top:2cm;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin-top:2cm;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
function getTransferData() {
    $('#ResultDataContainer').show();
    var url = route_json.report_data;

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        if (filters_id == 'bill_no_hidden') {
            filters_id = 'bill_no';
        }
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }
        if (filters_id == 'no_date') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_date';
            }else{
                filters_value = 0;
                filters_id = 'no_date';
            }
        }
        if (filters_id == 'no_opening') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_opening';
            }else{
                filters_value = 0;
                filters_id = 'no_opening';
            }
        }
        if (filters_id == 'show_naration') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'show_naration';
            }else{
                filters_value = 0;
                filters_id = 'show_naration';
            }
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}
function print_generate_without_boarder(printData) {
 //$('#print_config_modal').modal('hide');


     var showw = "";
     var printMode = $('input[name=printMode]:checked').val();
     var printhead = "";

     var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if($('#showTitle').is(":checked")){
        printhead  = $("#hospital_headder").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
mywindow.document.write('<head><style>');
mywindow.document.write('table {border-collapse:collapse;page-break-inside:auto;}');
mywindow.document.write('table td{padding:5px}');
mywindow.document.write('hr{margin-top: 0px;margin-bottom: 1px;border: 0;border-top: 1px solid black;}');
mywindow.document.write('tr{ page-break-inside:avoid; page-break-after:auto }');
mywindow.document.write('body {margin-top: 2cm;}');
mywindow.document.write('</style></head>');
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin-top:2cm;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin-top:2cm;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}
function print_generate_exclude_last_column(printData) {
    //$('#print_config_modal').modal('hide');


     var showw = "";
     var printMode = $('input[name=printMode]:checked').val();
     var printhead = "";

     var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if($('#showTitle').is(":checked")){
        printhead  = $("#hospital_headder").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
mywindow.document.write('<head><style>');
mywindow.document.write('table {border-collapse:collapse;page-break-inside:auto;}');
mywindow.document.write('table td, table th{ border: 1px solid #000;padding:5px;}');
mywindow.document.write('table td:last-child, table th:last-child{ display:none;}');
mywindow.document.write('tr{ page-break-inside:avoid; page-break-after:auto }');
mywindow.document.write('.td_common_numeric_rules {border-left: solid 1px #bbd2bd !important;text-align: right !important}');
mywindow.document.write('.td_common_numeric_rules { border-left: solid 1px #bbd2bd !important;text-align: right !important');
mywindow.document.write('body {margin-top: 2cm;}');
mywindow.document.write('</style></head>');
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin-top:2cm;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin-top:2cm;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}
