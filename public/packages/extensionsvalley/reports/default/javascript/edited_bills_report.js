$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});

function loadReportData() {
    var url = $('#base_url').val() + '/reports/editedBillsReportData';
    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var patient_id = $('#op_no_hidden').val();
    var bill_tag = $('#bill_number_hidden').val();
    var bill_number = $('#bill_number').val();

    var parm = { from_date: from_date, to_date: to_date, patient_id: patient_id, bill_tag: bill_tag, bill_number: bill_number };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            var enable_print = $('#enable_print').val();
            if (enable_print == '1') {
                $('#print_results').attr('disabled', false);
                $('#csv_results').attr('disabled', false);
            } else {
                $('#print_results').attr('disabled', true);
                $('#csv_results').attr('disabled', true);
            }

        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            setTimeout(function () {
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 300);
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });
}

function detailView(bill_no, bill_tag) {
    var url = $('#base_url').val() + '/reports/editedBillsDetailsReport';
    $('#edited_bill_detail_modal_header').html(bill_no);

    var parm = { bill_no: bill_no, bill_tag: bill_tag };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $("#edited_bill_detail_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#edited_bill_detail_modal_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#edited_bill_detail_modal_data').html(html);
        },
        complete: function () {
            $('#edited_bill_detail_modal_data').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }
    });
}

function search_clear() {
    var current_date = $('#current_date').val();
    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
    $('#op_no').val('');
    $('#bill_number').val('');
    $('#bill_number_hidden').val('');
    $('#op_no_hidden').val('');
}

//----Hidden Filed Search--------------------------------------------------------------------
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    input_id = $(this).attr('id');
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();
        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("").hide();
        } else {
            $.ajax({
                type: "GET",
                url: '',
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                        return "<span>No result found!!</span>";
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },
            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function exceller() {
    $(".theadfix_wrapper").floatThead('destroy');
    $('.hospital_header_excel').css('visibility', 'hidden');
    $('.hospital_header_excel').css('display', '');
    setTimeout(function () {
        var template_date = $('#exceller_data').val();
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = atob(template_date),
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            }
        var toExcel = document.getElementById("ResultDataContainer").innerHTML;
        var ctx = {
            worksheet: name || '',
            table: toExcel
        };
        var link = document.createElement("a");
        var menu_name = 'Bill Edit Report';
        link.download = menu_name + ".xls";
        link.href = uri + base64(format(template, ctx))
        link.click();

        $('.hospital_header_excel').css('display', 'none');
        $('.hospital_header_excel').css('visibility', '');
    }, 1000);

}