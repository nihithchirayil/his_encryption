$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    searchReturnList();
});

var base_url = $('#base_url').val();
var token = $('#c_token').val();

$('#return_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('return_no_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var return_no = $(this).val();
        if (!return_no) {
            $('#return_no_hidden').val('');
            $("#return_no_AjaxDiv").html("").hide();
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'stock_return_receive', search_key: return_no },
                beforeSend: function () {
                    $("#return_no_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#return_no_AjaxDiv").html(html).show();
                    $("#return_no_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('return_no_AjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillSearchDetials(return_id, return_no) {
    return_no = htmlDecode(return_no);
    $('#return_no').val(return_no);
    $('#return_no_hidden').val(return_id);
    $('#return_no_AjaxDiv').hide();
}


function searchReturnList() {
    var url = base_url + "/purchase/stockReturnsReceiveSearch";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var return_no = $('#return_no_hidden').val();
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var status = $('#status').val();
    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        return_no: return_no,
        from_location: from_location,
        to_location: to_location,
        status: status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#stock_return_receive_list").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#stock_return_receive_list_btn').attr('disabled', true);
            $('#stock_return_receive_list_spin').removeClass('fa fa-search');
            $('#stock_return_receive_list_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#stock_return_receive_list').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#stock_return_receive_list").LoadingOverlay("hide");
            $('#stock_return_receive_list_btn').attr('disabled', false);
            $('#stock_return_receive_list_spin').removeClass('fa fa-spinner fa-spin');
            $('#stock_return_receive_list_spin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function resetFilter() {
    $('#from_date').val('');
    $('#to_date').val('');
    $('#return_no').val('');
    $('#return_no_hidden').val('');
    $('#from_location').val('All').select2();
    $('#to_location').val('All').select2();
    $('#status').val('All').select2();
    searchReturnList();
}


function addWindowLoad(to_url, return_id) {
    var url = base_url + "/purchase/" + to_url + '/' + return_id;
    document.location.href = url;
}
