$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    $(".select2").select2({
        placeholder: "",
        maximumSelectionSize: 6
    });
    reqSearch();
});

var base_url = $('#base_url').val();
var token = $('#token').val();

function reqSearch() {
    var url = base_url + "/purchase/searchPurchaseReturn";
    var purchase_return_no = $('#purchase_return_no').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var return_store = $('#return_store').val();
    var vendor_id = $('#vendor').val();
    var status = $('#statusdata').val();
    var item_desc = $('#item_desc').val();
    var item_code = $('#item_desc_hidden').val();
    var param = {
        _token: token,
        purchase_return_no: purchase_return_no,
        from_date: from_date,
        to_date: to_date,
        vendor_id: vendor_id,
        return_store: return_store,
        from_type: 1,
        status: status,
        item_desc,
        item_code

    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#purchaseRetunDataList").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $('#reqSearchBtn').attr('disabled', true);
            $('#reqSearchSpin').removeClass('fa fa-search');
            $('#reqSearchSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#purchaseRetunDataList').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#purchaseRetunDataList").LoadingOverlay("hide");
            $('#reqSearchBtn').attr('disabled', false);
            $('#reqSearchSpin').removeClass('fa fa-spinner fa-spin');
            $('#reqSearchSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}



function DownloadPdfSendMail(mail_status) {
    var return_id = $('#return_id_printpreview').val();
    var url = base_url + "/purchase/getPurchaseReturnPreviewPdf";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            return_id: return_id,
            mail_status: mail_status
        },
        beforeSend: function () {
            $('#sendEmailBtn' + mail_status).attr('disabled', true);
            $('#sendEmailSpin' + mail_status).removeClass('fa fa-' + mail_status);
            $('#sendEmailSpin' + mail_status).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (mail_status == 'file') {
                var file_url = base_url + "/packages/uploads/Purchase/" + data;
                $("#downloadPoPFDlinka").attr("href", file_url);
            } else if (mail_status == 'envelope') {
                toastr.success('Mail Sent Successfully');
            }
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#sendEmailBtn' + mail_status).attr('disabled', false);
            $('#sendEmailSpin' + mail_status).removeClass('fa fa-spinner fa-spin');
            $('#sendEmailSpin' + mail_status).addClass('fa fa-' + mail_status);
            if (mail_status == 'file') {
                $("#downloadPoPFDlinkimg").trigger('click');
            }
        }
    });
}


function getPrintPreview(return_id) {
    var url = base_url + "/purchase/getPurchaseReturnPreview";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            return_id: return_id
        },
        beforeSend: function () {
            $('#print_po_modelListDiv').html('');
            $('#print_po_modelDiv').html('');
            $('#sendEmailBtn' + return_id).attr('disabled', true);
            $('#sendEmailSpin' + return_id).removeClass('fa fa-eye');
            $('#sendEmailSpin' + return_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#print_po_modelDiv').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $("#print_po_model").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#sendEmailBtn' + return_id).attr('disabled', false);
            $('#sendEmailSpin' + return_id).removeClass('fa fa-spinner fa-spin');
            $('#sendEmailSpin' + return_id).addClass('fa fa-eye');
        }
    });
}

function clear_search() {
    $('#purchase_return_no').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('#return_store').val('');
    $('#return_store').select2();
    $('#vendor').val('');
    $('#vendor').select2();
    $('#statusdata').val('');
    $('#statusdata').select2();
    $('#item_desc').val('');
    $('#item_desc_hidden').val('');
    reqSearch();
}

function printList() {
    var url = base_url + "/purchase/searchPurchaseReturn";
    var purchase_return_no = $('#purchase_return_no').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var return_store = $('#return_store').val();
    var vendor_id = $('#vendor').val();
    var status = $('#statusdata').val();
    var item_desc = $('#item_desc').val();
    var item_code = $('#item_desc_hidden').val();
    var param = {
        _token: token,
        purchase_return_no: purchase_return_no,
        from_date: from_date,
        to_date: to_date,
        vendor_id: vendor_id,
        return_store: return_store,
        from_type: 2,
        status: status,
        item_desc,
        item_code
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#print_po_modelListDiv').html('');
            $('#print_po_modelDiv').html('');
            $('#printlistBtn').attr('disabled', true);
            $('#printlistspin').removeClass('fa fa-print');
            $('#printlistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#print_po_modelListDiv').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $("#print_po_modelList").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#printlistBtn').attr('disabled', false);
            $('#printlistspin').removeClass('fa fa-spinner fa-spin');
            $('#printlistspin').addClass('fa fa-print');
        }
    });
}



function editReturnData(stock_id) {
    var main_uri = base_url + "/purchase/return_items_to_vendor_add";
    if (parseInt(stock_id) != 0) {
        main_uri = base_url + "/purchase/return_items_to_vendor_edit/" + stock_id;
    }
    document.location.href = main_uri;
}

/* product search for itemwise filter in report */
$('#item_desc').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('item_desc_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#item_desc_hidden').val() != "") {
            $('#item_desc_hidden').val('');
            $("#item_desc_AjaxDiv").hide();
        }
        var item_desc = $(this).val();
        if (item_desc == "") {
            $("#item_desc_AjaxDiv").html("").hide();
        } else {
            var url = base_url + "/grn/ProductNameSearch";

            $.ajax({
                type: "GET",
                url: url,
                data: "item_desc=" + item_desc,
                beforeSend: function () {
                    $("#item_desc_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#item_desc_AjaxDiv").html(html).show();
                    $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('item_desc_AjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillitem_desc(item_code, item_desc) {
    item_desc = htmlDecode(item_desc);
    $('#item_desc_hidden').val(item_code);
    $('#item_desc').val(item_desc);
    $('#item_desc_AjaxDiv').hide();
}