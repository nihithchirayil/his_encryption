var j = 1;
batch_array = {};
item_array = {};

$(document).ready(function () {
    getStockRecords();
});
function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("addNewItem");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
var base_url = $('#base_url').val();
var token = $('#c_token').val();

function getStockRecords() {
    var stock_id = $('#stock_id').val();
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'stockItemDetails=' + stock_id,
        beforeSend: function () {
            $("#fillrequestitemdetails").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            $("#fillrequestitemdetails").html(html);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#fillrequestitemdetails").LoadingOverlay("hide");
        },
    });
}

function getCheckedColumns() {
    request_array = new Array();
    $('#fillrequestitemdetails tr').each(function () {
        var item_id = $(this).find("input[name='save_update_id']").val();
        var item_code = $(this).find("input[name='item_code_hidden']").val();
        var reason_data = $(this).find("input[name='reason_data']").val();
        var item_expiry = $(this).find("input[name='item_batch_exp_hidden']").val();
        var item_batch = $(this).find("input[name='item_batch_hidden']").val();
        var status = $('#makePatiallyRequest' + item_id).is(":checked");
        if (status) {
            var request_qty = $(this).find("input[name='request_qty']").val();
            var reject_qty = $(this).find("input[name='reject_qty']").val();
            request_array.push({
                'item_id': item_id ? item_id : 0
                , 'item_code': item_code ? item_code : ''
                , 'item_expiry': item_expiry ? item_expiry : ''
                , 'reason_data': reason_data ? reason_data : ''
                , 'item_batch': item_batch ? item_batch : ''
                , 'request_qty': request_qty ? request_qty : 0
                , 'reject_qty': reject_qty ? reject_qty : 0
            });
        }
    });
    return request_array;
}


function saveItem() {
    var request_array = getCheckedColumns();
    if ((Object.entries(request_array).length) != 0) {
        bootbox.confirm({
            message: "Are you sure you want to Receive ?",
            buttons: {
                'confirm': {
                    label: "Receive",
                    className: 'btn-success',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var request_array_string = JSON.stringify(request_array);
                    var remarks = $('#requisition_comments').val();
                    var return_no = $('#return_no').val();
                    var from_location = $('#from_location').val();
                    var to_location = $('#to_location').val();
                    var reason_head = $('#reason_head').val();
                    var head_id = $('#request_stock_head_id').val();
                    var param = { head_id: head_id, return_no: return_no, from_location: from_location, to_location: to_location, reason_head: reason_head, remarks: remarks, request_array_string: request_array_string };
                    var url = base_url + '/purchase/receiveReturnStock';
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: param,
                        beforeSend: function () {
                            $('#saveitembtn').attr('disabled', true);
                            $('#saveitemspin').removeClass('fa fa-save');
                            $('#saveitemspin').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            confirmData(data);
                        },
                        complete: function () {
                            $('#saveitembtn').attr('disabled', false);
                            $('#saveitemspin').removeClass('fa fa-spinner fa-spin');
                            $('#saveitemspin').addClass('fa fa-save');
                        },
                        error: function () {
                            toastr.error("Error Please Check Your Internet Connection");
                        }
                    });

                }
            }
        });
    } else {
        toastr.warning("Please Select Items");
    }
}


function confirmData(return_no) {
    bootbox.alert({
        title: "Detalis Updated Successfully",
        message: "Return No : " + return_no,
        callback: function () {
            addWindowLoad('listStockReturnReceive');
        }
    });
}

function addWindowLoad(to_url) {
    var url = base_url + "/purchase/" + to_url;
    document.location.href = url;
}


function number_validation(e) {
    // var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
    //     val = e.value;
    // if (!valid) {
    //     e.value = val.substring(0, val.length - 1);
    // }

    var decimal_qty_ind_hidden = $(e).closest("tr").find("input[name='decimal_qty_ind_hidden[]']").val();
    if(decimal_qty_ind_hidden==1){
        var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
        val = e.value;
        if (!valid) {
            e.value = val.substring(0, val.length - 1);
            $(e).val('');
        }
    }else{
    var valid = /^\d{0,12}$/.test(e.value),
    val = e.value;
    if (!valid) {
    e.value = val.substring(0, val.length - 1);
    $(e).val('');
        }
    }
}

function checkStock(e) {
    var stock = $(e).parents("tr").find(".ret_req_qty").val();
    var ret_val = $(e).val();
    // if (/\D/.test(ret_val)) {
    //     $(e).val(0);
    // }
    if ((parseFloat(stock) < parseFloat(ret_val)) || stock == '') {
        toastr.error("Please check available stock");
        $(e).val(0);
    }

    var qty = parseFloat($(e).val());
    if (!qty) {
        qty = 0.0;
    }
    if ($(e).hasClass("ret_rec_qty")) {
        var reject_qty = parseFloat(stock) - qty;
        $(e).parents("tr").find(".ret_reject_qty").val(reject_qty);
    } else if ($(e).hasClass("ret_reject_qty")) {
        var reject_qty = parseFloat(stock) - qty;
        $(e).parents("tr").find(".ret_rec_qty").val(reject_qty);
    }
}


function makeFullyRequest() {
    var status = $('#select_all_records').is(":checked");
    if (status) {
        $('.makePatiallyRequest').prop('checked', true);
    } else {
        $('.makePatiallyRequest').prop('checked', false);
    }
}

function makeFullyRequestRejectItem() {
    var receive_reject = $('input[name="receive_reject"]:checked').val();
    $('#fillrequestitemdetails tr').each(function () {
        var item_id = $(this).find("input[name='save_update_id']").val();
        var return_qty = $('#return_qty' + item_id).val();
        if (receive_reject == '1') {
            $('#request_qty' + item_id).val(return_qty);
            $('#reject_qty' + item_id).val(0);
        } else if (receive_reject == '2') {
            $('#request_qty' + item_id).val(0);
            $('#reject_qty' + item_id).val(return_qty);
        }
    });
    $('#select_all_records').prop('checked', true);
    $('.makePatiallyRequest').prop('checked', true);
}
