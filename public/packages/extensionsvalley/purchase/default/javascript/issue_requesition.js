batch_array = {};
item_batch_array = {};
remarks_array = {};
var status = 0;
let url = $('#req_base_url').val();
to_location_id = $("#to_locationid").val();
from_location_id = $("#from_locationid").val();
stock_id = $("#issue_list_stock_id").val();
item_id = 1;
req_no = '';
location_type = $("#purchase_type").val();

$("#item_search_btn").click(function () {
    $("#issue_search_box").toggle();
    document.getElementById("issue_search_box").focus();
    $("#item_search_btn_text").toggle();
});
/**
 *
 * Java script search in the search item box
 */
function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("product_issue_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];

        label = td.getElementsByTagName("label");

        if (td) {
            if (td.textContent.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
function number_validation(e) {
    var decimal_qty_ind_hidden = $(e).closest("tr").find(".decimal_qty_ind").val();
    if(decimal_qty_ind_hidden==1){
        var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
        val = e.value;
        if (!valid) {
            e.value = val.substring(0, val.length - 1);
        }
    }else{
    var valid = /^\d{0,12}$/.test(e.value),
    val = e.value;
    if (!valid) {
    e.value = val.substring(0, val.length - 1);
        }
    }
}
function batch_qty_validation(obj, inc) {
    var num = obj.value;
    if (num) {
        var requsted_qty_get = 0;
        $(".btch_iss_qty_chng").each(function (key, val) {
            var item_value = val.value;
            requsted_qty_get += Number(item_value);
        });
        $('#batch_wise_issued_qty_total').html(requsted_qty_get);
        var itemstock = Number($('#batch_stock' + inc).html().trim());
        if (itemstock < num) {
            toastr.error('Less Quantity in Store');
            $("#add_batch_button").hide();
        } else {
            $("#add_batch_button").show();
        }

    } else {
        $('#batch_wise_issued_qty_total').html(0);
    }
}

function get_product_batch(item_id, qty, stock) {
    var item_code = $('#product_code_id' + item_id).val();
    var item_desc = $('#item_description_id' + item_id).html().trim();
    var item_qty = $('#final_issue_qty' + item_id).val();
    var item_batch_array = JSON.stringify(batch_array);
    if (item_qty == '0') {
        item_qty = qty;
    }
    var to_location = to_location_id;
    if (to_location != '') {
        var file_token = $('#hidden_filetoken').val();
        var param = {_token: file_token, get_product_batch_data: 1, batch_item_array: item_batch_array, batch_item_code: item_code, batch_to_location: to_location, batch_qty: item_qty, batch_stock: stock, batch_item_id: item_id,issue_id:stock_id};
        $.ajax({
            type: "POST",
            url: url + "/purchase/select_batch_view",
            data: param,
            beforeSend: function () {

                $('#final_issue_qty' + item_id).hide();//text box hiding
                $('#td_spin_' + item_id).show();//spinner showing
            },
            success: function (result) {
                $('#td_spin_' + item_id).hide();//hiding spinner
                $('#final_issue_qty' + item_id).show(); //textbox showing
                $(".issue_qty_pop").html('');
                $("#issue_qty_batch_pop_" + item_id).html(result); //appending data to the div
                $("#issue_qty_batch_pop_" + item_id).show(); //showing the div
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                    document.getElementById("batch_wise_issued_qty1").focus();
                }, 400);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });

            },
            complete: function () {
                $('#final_issue_qty' + item_id).removeClass('fa fa-spinner fa-spin');
                $('#modal_desc').html(item_desc);
            }
        });
    } else {
        toastr.error('To Location Is not Added');
    }
}

function create_batch_array(item_id) {
        var item_code = $('#item_code_for_batch').val();
        var total_issue_quantity_by_batch = 0;
        var issue_quantity_by_batch = 0;
        var disable_issue_greaterthan_approve_qty = $("#disable_issue_greaterthan_approve_qty").val();
        var ij = 1;
        batch_array[item_code] = {};
        $(".inc_class").each(function () {
            var batch_number = btoa($('#batch_no' + ij).val().trim());
            issue_quantity_by_batch = $('#batch_wise_issued_qty' + ij).val().trim();

            batch_array[item_code][batch_number] = issue_quantity_by_batch;
            total_issue_quantity_by_batch += parseInt(issue_quantity_by_batch);
            ij++;
        });

        var req_qty = $('#batch_wise_issued_qty_total').html();
        if (req_qty) {
            var apprvrd_wty_chk = $("#approved_item_qty"+item_id).html();
            if(parseFloat(apprvrd_wty_chk) < req_qty){
                toastr.error("You are Issuing more than approved quantity");
                if(disable_issue_greaterthan_approve_qty==1){
                    return false;
                }

            }
            $('#final_issue_qty' + item_id).val(req_qty);
        } else {
            $('#final_issue_qty' + item_id).val(0);
        }
        $("#issue_qty_batch_pop_" + item_id).hide();
        $(".issue_qty_pop").hide();

    console.log(batch_array);
}
function clear_batch_data(item_code)
{
    delete batch_array[item_code];
}
function hide_batch_div(item_id) {
    $("#issue_qty_batch_pop_" + item_id).hide();
    $(".issue_qty_pop").hide();
}

function save_product_list(save_type) {
    var item_batch_array = JSON.stringify(batch_array);
    var form_data = $('#save_product_list').serialize();
    var from_location = from_location_id;
    var to_location = to_location_id;
    var req_id = stock_id;
    var req_no = $('#rrequest_number').html().trim();
    var validate = validate_item_data();
    var i = 1;
    remarks_array = {};
    $(".stock_remark_clas").each(function () {
        item_code = $('#product_code_id' + i).val().trim();
        remarks_array[item_code] = {};
        remark_txt = $('#stock_remark_' + i).val().trim();
        remarks_array[item_code] = remark_txt;
        i++;
    });

    var remarks_array = JSON.stringify(remarks_array);
    if (validate) {

        $.ajax({
            type: "POST",
            url: url + "/purchase/save_issue_req",
            data: form_data + '&save_product_issue=' + 1 + '&req_id=' + req_id + '&req_no=' + req_no + '&save_type=' + save_type + '&item_batch_array=' + item_batch_array + '&from_location=' + from_location + '&to_location=' + to_location+ '&remarks_array=' + remarks_array,
            beforeSend: function () {
                $('#product_issue_table tr').removeClass();
                $('#product_issue_table tr').addClass('issue_saving');
                if (save_type == '1') {
                    $('#issue_product_spin').removeClass('fa fa-save');
                    $('#issue_product_spin').addClass('fa fa-spinner fa-spin');
                    $(".current_issue_items_request").prop('disabled', true);
                } else if (save_type == '2') {
                    $('#close_product_spin').removeClass('fa fa-remove');
                    $('#close_product_spin').addClass('fa fa-spinner fa-spin');
                }
            },
            success: function (result) {
                var json_data = JSON.parse(result);
                if (json_data.success == 1) {
                    toastr.success("Issued Successfully");
                    setTimeout(function () {
                        issue_page_refresh();
                    }, 500);
                }
                if (json_data.success == '5') {
                    if (json_data.success == '5')
                        toastr.success("Closed the Request Successfully");
                    else
                        toastr.success("Issued Successfully");
                    setTimeout(function () {
                        issue_page_refresh();
                    }, 200);
                } else if (json_data.product_stat != 1 && json_data.success == 0) {
                    var result =  json_data.item_code.join(',');
                    toastr.error("Quantity not available for the item codes:"+result);
                     $(json_data.item_code).each(function (key, val) {
                        $('#item_issue_row' + val).removeClass('issue_saving');
                        $('#item_issue_row' + val).addClass('bg-warning');
                    });

                    $(".current_issue_items_request").prop('disabled', false);

                }else if(json_data.success == '22'){
                    toastr.error("Already issued");
                    setTimeout(function () {
                        issue_page_refresh();
                    }, 200);
                }


            },
            complete: function () {
                if (save_type == '1') {
                    $('#issue_product_spin').removeClass('fa fa-spinner fa-spin');
                    $('#issue_product_spin').addClass('fa fa-save');
                } else if (save_type == '2') {
                    $('#close_product_spin').removeClass('fa fa-spinner fa-spin');
                    $('#close_product_spin').addClass('fa fa-remove');
                }


            }
        });
    }
}
function validate_item_data() {
    var flag = true;
    var stock_num = '';
    var issue = 0;
    var op = 1;
    var total_issue_validation = 0;
    var err_msg1 = 0;
    var err_msg2 = 0;
    var err_msg3 = 0;
    $('.vchk').each(function () {

        stock_num = $('#store_stock_id' + op).val();
        issue = $('#final_issue_qty' + op).val();
        approved_row_qty = $('#approved_item_qty' + op).text();

        item_code = $('#product_code_id' + op).val();
        total_issue_validation = parseFloat(total_issue_validation) + parseFloat(issue);
        if (parseFloat(stock_num) < parseFloat(issue)) {
            flag = false;
            $('#item_issue_row' + item_code).css('background-color', '#f1e0a0');
            err_msg1 = 1;
        }

        if (parseFloat(approved_row_qty) == 0) {
            err_msg2 = 2;
            $('#item_issue_row' + item_code).css('background-color', '#f1e0a0');
            flag = false;
        }
        if (parseFloat(total_issue_validation) == 0) {
            err_msg3 = 3;
            $('#item_issue_row' + item_code).css('background-color', '#f1e0a0');
            flag = false;
        }else{
            flag = true;
            err_msg3 = 0;
        }
        op++;
    });
    if (err_msg1 == 1) {
        toastr.error("No Sufficient Stock");
    }
    if (err_msg2 == 2) {
        toastr.error("Quantity is not approved");
    }
    if (err_msg3 == 3) {
        toastr.error("Please Enter the Issue Quantity");
    }
    return flag;
}
/**
 *
 * @param {type} item_code
 * Deleting Each item when click on the delete icon button in the table
 * its only active when not issue the request
 */
function delete_issue_items(item_code,item_id) {
    if (confirm("Are you sure you want to Delete Stock ?")) {


        var req_id = stock_id;
        var file_token = $('#hidden_filetoken').val();
        var param = {_token: file_token, delete_item_row: 1, item_code: item_code, req_id: req_id};

        $.ajax({
            type: "POST",
            url: '',
            data: param,
            beforeSend: function () {
                 $('#fa_delete_spin_' + item_id).removeClass('fa fa-times-circle');
                 $('#fa_delete_spin_' + item_id).addClass('fa fa-spinner fa-spin');
            },
            success: function (result) {
                if (result == 1) {
                    toastr.success('Item Deleted Succcessfuly !!');
                    $('#item_issue_row' + item_code).remove();
                }
            },
            complete: function () {
            }
        });
    }
}
/**
 *
 * Reload the current page after the issue the items
 */
function issue_page_refresh() {
    //window.location.reload();
    var urls = $('#req_base_url').val();
    var urls = urls + "/purchase/issue_requisition_list/1"
    window.location.href = urls;
}

function revert_check_box(e) {
    $('#product_issue_table tr').each(function () {
        if ($(this).find('input[name="revert_check_box[]"]:checked').length > 0) {
            $(this).find('input[name="revert_check_box[]"]').prop('checked', false);
        } else {
            $(this).find('input[name="revert_check_box[]"]').prop('checked', true);
        }
    });
}
/**
 *
 * Revert the stock
 */
function revert_stock_issue() {
    var item_code = [];
    var req_no = $('#rrequest_number').text().trim();
    var file_token = $('#hidden_filetoken').val();
    var i = 0;
    $(".revert_chk_box_class:checked").each(function () {
        item_code[i] = $(this).val();
        i++;
    });
    if(i == 0){
        toastr.error('No item selected for revert');
        return false;
    }
    if (confirm("If sure CLICK OK button")) {
        $.ajax({
            type: "POST",
            url: url + "/purchase/revert_issue_items",
            data: {_token: file_token, item_code: item_code, req_no: req_no},
            success: function (data) {
                if (data == 1 ) {
                    toastr.success('Succcessfuly revert the stock');
                    issue_page_refresh();
                } else {
                    toastr.error('Error in Reverting Stock');
                    issue_page_refresh();
                }
            }
        });
    }
}
/**
 * PRINT START HERE
 */
function print_product_list(status) {

    var file_token = $('#hidden_filetoken').val();
    var head_id = stock_id
    var is_rack_checked = $('#sort_all_print').is(':checked') ? 1 : 0;
    var param = {_token: file_token, head_id: head_id, status: status, is_rack_checked: is_rack_checked};
    $.ajax({
        type: "POST",
        url: url + "/purchase/print_issue_list",
        data: param,
        beforeSend: function () {

        },
        success: function (result) {
            if (result) {
                var popupWin = window.open('', 'my div', 'height=3508,width=2480');
                popupWin.document.write('<style>@page</style>');
                popupWin.document.write(result);
                popupWin.document.close();
                popupWin.focus();
                popupWin.print()
                popupWin.close();
                return true;
            }
        },
        complete: function () {
        }
    });
}


