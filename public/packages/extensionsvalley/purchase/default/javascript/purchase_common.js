$(document).ready(function () {
    $(document).on('keydown', function(event) {
       if (event.key == "Escape") {
           $(".ajaxSearchBox").hide();
       }
   });
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');

    //$('#menu_toggle').trigger('click');
    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });
    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('.date_time_picker').datetimepicker();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

     $(".selectdropdown").select2({
         placeholder: "",
         allowClear: true,
         width: '100%'
     });
     $(document).on('click', '.select_mat_type_input .mat_label', function(){
         $("select").select2("open");
     });

     $(document).on('click', '.mat_type_input, .mat_label', function(){
      $(this).addClass('active');
      $(this).find(".form-control").focus();
  });


     $(document).on('change', '.inputfile', function(e){
       var val = $(this).val();
       if (val==="") {
        $(this).next().removeClass('active');
    } else {
        $(this).next().addClass('active');
    }
});

     $('').trigger('select2:open');

     $(document).on('blur', '.mat_type_input input.form-control, .mat_type_input textarea.form-control', function(){
       if( !$(this).val() ) {
        $(this).parent().removeClass('active');
    }
});


     $(document).on('change', '.select_mat_type_input select', function(){

       $(this).parent().addClass('active');

       if ($(this).val() === "") {
        $(this).parent().addClass('remove_active');
    } else {
        $(this).parent().removeClass('remove_active');
    }

});


});
$(document).on('click', '.smooth_scroll', function (event) {
    event.preventDefault();
    $('.combined_theadscroll').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});
//$(".selectsearch").select2();

/**
 *
 * @param {type} ajax_div
 * @returns {undefined}
 * FOR KEY DOWN KY UP IN PROGRESSIVE  SEARCH
 */
function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}
function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var item_list = $("#" + ajax_div).find('li');
    var selected = item_list.filter('.liHover');
    if (event.keyCode === 13){
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    item_list.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current_item = item_list.eq(0);
        } else {
            current_item = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current_item = item_list.last();
        } else {
            current_item = selected.prev();
        }
    }
    current_item.addClass('liHover');
}
function clear_search() {
    var cur_url = window.location.href.split('?')[0];
    window.location.replace(cur_url);
}
$('body').click(function(){
    $('.ajaxSearchBox').hide();
});
function viewBillPrint(printData) {
    var detailArray1 = [];
    var amountArray1 = [];
    var invoiceArray1 = [];
    var invoiceDateArray1 = [];
    var party_name1 = '';
    var print_html_tbl = '';
    var ttl_selected_amount_label_span = $("#ttl_selected_amount_label_span").html();
    print_html_tbl += '<table style="border: solid 1px black" id="receipt_table1" style="margin: 0px 0px !important">\n\
                            <thead>\n\
                                <tr>\n\
                                <th style="text-align:center;width:10%;border-bottom: solid 1px black">Party</th>\n\
                                <th style="text-align:center;width:10%;border-bottom: solid 1px black">Invoice No</th>\n\
                                <th style="text-align:center;width:10%;border-bottom: solid 1px black">Invoice Date</th>\n\
                                <th style="text-align:center;width:8%;border-bottom: solid 1px black">Amount</th>\n\
                                </tr>\n\
                    </thead>\n\
                    <tbody>';
    $('input[name="detail_id[]"]:checked').each(function () {
        detailArray1.push(this.value);
        amountArray1.push($(this).attr('class'));
        invoiceArray1.push($(this).attr('data-invoice'));
        invoiceDateArray1.push($(this).attr('data-invoicedate'));
        party_name1 = $(this).parents("tr").find("input[name='party_name[]']").val();
    });
    if(invoiceArray1.length < 1){
         toastr.warning("Please select party");return false;
    }
    for(var i=0; i < invoiceArray1.length; i++){
        print_html_tbl += '<tr  style="border-bottom: 1px solid black;">\n\
                                <td class="common_td_rules" style="border-left: solid 1px #bbd2bd">'+ party_name1 +'</td>\n\
                                <td class="common_td_rules" style="border-left: solid 1px #bbd2bd">'+ invoiceArray1[i] +'</td>\n\
                                <td class="common_td_rules" style="border-left: solid 1px #bbd2bd">'+ invoiceDateArray1[i] +'</td>\n\
                                <td class="td_common_numeric_rules">'+ amountArray1[i] +'</td>\n\
                        </tr>';
    }
     print_html_tbl += '<tr><td  style="border-top: solid 1px black" colspan="4" class="td_common_numeric_rules"><b>'+ttl_selected_amount_label_span+'</b></td></tr>'; 
     print_html_tbl += '</tbody></table>'; 
    $("#prt_div").html(print_html_tbl);
    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }
    mywindow.document.write('<style>.table.no-border, .table.no-border td, .table.no-border th { border: 0; } .table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } .table_sm td span { line-height: 18px !important; } .table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } .table_head.tth { font-size: 12px !important; padding: 1px !important; } .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } .table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, .table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, .table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } .headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} th { background-color:#d9dfe0 !important;}.common_td_rules{ text-align: left !important; overflow: hidden !important;}.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; }</style>');
    mywindow.document.write(showw + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
    return true;

}