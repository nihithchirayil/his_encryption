$(document).ready(function () {
    token = $('#hidden_filetoken').val();
    decimalConfiguration = $('#decimal_configuration').val();
    base_url = $('#base_url').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $("#item_search_btn").click(function () {
        $("#issue_search_box").toggle();
        $("#item_search_btn_text").toggle();
    });
    $(".select2").select2();
    checkEditStatus();
    var rfq_id = $('#rfq_id').val();
    if (rfq_id != '0') {
        $('#location').attr("disabled", true);
    }
    $('.datepicker').blur();

    $('#quotation_number').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#quotation_no_hidden').val() != "") {
                $('#quotation_no_hidden').val('');
                $("#rfq_idAjaxDiv").html("").hide();
            }
            var quotation_no = $(this).val();
            if (quotation_no == "") {
                $("#rfq_idAjaxDiv").html("").hide();
            } else {
                var url = base_url + "/quotation/searchQuotationNo";
                var param = {
                    quotation_no: quotation_no
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#rfq_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#rfq_idAjaxDiv").html(html).show();
                        $("#rfq_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });
    setTimeout(function () {
        if ($('#po_id').val() == 0) {
            $('#addQuotationRowbtn').trigger('click');
        }
    }, 1100);


});


var base_url = $('#base_url').val();

$('#patient_uhid').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if ($('#patient_uhid_hidden').val() != "") {
            $('#patient_uhid_hidden').val('');
            $("#patient_uhidAjaxDiv").html("").hide();
        }
        var patient_name_search = $(this).val();
        if (patient_name_search == "") {
            $("#patient_uhidAjaxDiv").html("").hide();
        } else {
            var url = base_url + "/frame/patientSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: "patient_name_search=" + patient_name_search,
                beforeSend: function () {
                    $("#patient_uhidAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#patient_uhidAjaxDiv").html(html).show();
                    $("#patient_uhidAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }
    } else {
        ajax_list_key_down('patient_uhidAjaxDiv', event);
    }
});


function searchItemCode(id, event, row_id) { //alert(id);return;
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var ajax_div = $('#' + id).next().attr('id');
    var item_code = $('#' + id).val();
    var deprt_list = $('#deprt_list').val();
    var item_array_string = JSON.stringify(item_array);
    if (event.keyCode == 13) {
        ajaxlistenter(ajax_div);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (item_code) {
            if (value.match(keycheck)) {
                var location_defaultvalue = $('#location_defaultvalue').val();
                var url = base_url + "/frame/FrameItemSearch";
                var param = {
                    _token: token,
                    item_array_string: item_array_string,
                    item_code: item_code,
                    deprt_list: deprt_list,
                    location_defaultvalue: location_defaultvalue,
                    row_id: row_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#addQuotationRowbtn').attr('disabled', true);
                        $('#addQuotationRowSpin').removeClass('fa fa-plus');
                        $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                        $('#addQuotationRowbtn').attr('disabled', false);
                        $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                        $('#addQuotationRowSpin').addClass('fa fa-plus');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        } else {
            $("#" + ajax_div).html('').hide();
        }
    } else {
        ajax_list_key_down(ajax_div, event);
    }
}


function savePurchaseOrder(post_type) {
    var vendor_id = $('#vendoritem_id_hidden').val();
    var alltotals = getAllItemsTotals();
    var cancel_string = 'Cancel';
    var flag = validatePoData();
    var array_len = Object.keys(item_array).length;
    if (flag) {
        if (parseInt(array_len) != 0) {
            if (vendor_id != '0') {
                var post_string = '';
                if (post_type == '1') {
                    post_string = "Save";
                } else if (post_type == '2') {
                    post_string = "Close";
                } else if (post_type == '3') {
                    post_string = "Approve";
                } else if (post_type == '0') {
                    post_string = "Cancel";
                    cancel_string = "No";
                } else if (post_type == '4') {
                    post_string = "Amend";
                } else if (post_type == '5') {
                    post_string = "Verify";
                }
                bootbox.confirm({
                    message: "Do you want to " + post_string + " this Purchase Order?",
                    buttons: {
                        'confirm': {
                            label: post_string,
                            className: 'btn-success',
                            default: 'true'
                        },
                        'cancel': {
                            label: cancel_string,
                            className: 'btn-warning'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            var url = base_url + "/frame/savePOData";
                            var manufacturer_code = $('#manufacturer_list').val();
                            var vendor_code = $('#vendoritem_code_hidden').val();
                            var po_added = $('#po_added').val();
                            var po_date = $('#po_date').val();
                            var location = $('#deprt_list').val();
                            var payment_terms = '';
                            var payment_remarks = $('#payment_remarks').val();
                            var disc_mode = $('#disc_mode').val();
                            var bill_discount_type = $('#bill_discount_type').val();
                            var bill_discount_value = $('#bill_discount_value').val();
                            var bill_discount_amount = $('#bill_discount_amount').val();
                            var copy_status = $('#copy_status').val();
                            var round_off = $('#round_off').val();
                            var head_id = $('#po_id').val();
                            var total_cgst = 0.0;
                            var total_sgst = 0.0;
                            var total_igst = 0.0;
                            var total_cess = 0.0;
                            var total_discount = 0.0;
                            var net_total = 0.0;
                            var total_othercharges = 0.0;
                            var total_freightcharges = 0.0;
                            var total_amt = 0.0;
                            for (var key in item_array) {
                                var obj = item_array[key];
                                total_cgst += parseFloat(obj.cgst_amt);
                                total_sgst += parseFloat(obj.sgst_amt);
                                total_igst += parseFloat(obj.igst_amt);
                                total_discount += parseFloat(obj.discount_amt);
                                total_othercharges += parseFloat(obj.othercharge_amt);
                                total_freightcharges += parseFloat(obj.flightcharge_amt);
                                total_amt += parseFloat(obj.totalRate);
                                net_total += parseFloat(obj.netRate);
                            }
                            var net_amt = alltotals['total_net'];
                          var tot_tax = parseFloat(total_cgst) + parseFloat(total_sgst) + parseFloat(total_igst) + parseFloat(total_cess);
                            var oth_charges = parseFloat(total_othercharges) + parseFloat(total_freightcharges);

                            var item_string = JSON.stringify(item_array);
                            item_string = encodeURIComponent(item_string);

                            var po_terms = JSON.stringify(po_terms_conditions);
                            po_terms = encodeURIComponent(po_terms);
                            var cc_email_users_string = JSON.stringify(cc_email_users);

                            var param = {
                                _token: token,
                                manufacturer_code: manufacturer_code,
                                po_added: po_added,
                                po_date: po_date,
                                payment_terms: payment_terms,
                                payment_remarks: payment_remarks,
                                disc_mode: disc_mode,
                                bill_discount_type: bill_discount_type,
                                po_terms: po_terms,
                                vendor_id: vendor_id,
                                bill_discount_value: bill_discount_value,
                                total_amt: total_amt,
                                bill_discount_amount: bill_discount_amount,
                                total_discount: total_discount,
                                location: location,
                                vendor_code: vendor_code,
                                post_type: post_type,
                                copy_status: copy_status,
                                net_amt: net_amt,
                                round_off: round_off,
                                total_othercharges: total_othercharges,
                                total_freightcharges: total_freightcharges,
                                head_id: head_id,
                                net_total: net_total,
                                tot_tax: tot_tax,
                                oth_charges: oth_charges,
                                item_string: item_string,
                                cc_email_users_string: cc_email_users_string
                            };

                            $.ajax({
                                type: "POST",
                                url: url,
                                data: param,
                                beforeSend: function () {
                                    $("#mainDataDiv").LoadingOverlay("show", {
                                        background: "rgba(255, 255, 255, 0.7)",
                                        imageColor: '#337AB7'
                                    });
                                },
                                success: function (result) {
                                    confirmPONo(result, post_type);
                                },
                                error: function () {
                                    toastr.error('Please check your internet connection and try again');
                                },
                                complete: function () {
                                    $("#mainDataDiv").LoadingOverlay("hide");
                                }
                            });

                        }
                    }
                });
            } else {
                toastr.warning("Please Select a Vendor");
                $('#searchnewvendor').focus();
            }
        } else {
            toastr.warning("Please Select a Item");
        }
    } else {
        toastr.warning("Please validate items");
    }
}

$(document).keydown(function (event) {
    if (event.which == '113') {
        var item_code = $('#lastClickedItemCode').val();
        var row_id = $('#lastClickedRowID').val();
        if (item_code) {
            getIemsHistory(row_id, item_code);
        } else {
            toastr.warning("Please Select an Item !!!!");
        }
    }
});

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

var base_url = '';
var token = '';
var charge_item = {};
var item_array = {};
var request_array = new Array();
item_array_new = {};
item_charge_array = {};
purchase_requisition = {};
stock_details_array = [];
var po_terms_conditions = [];
var cc_email_users = new Array();
var previous_location = $('#deprt_list').val();

var product_each_charge = {};
product_each_charge['discount'] = {};
product_each_charge['tax'] = {};
product_each_charge['total'] = {};
product_each_charge['on_tax_amount'] = {};
product_each_charge['tax_free'] = {};
product_each_charge['othch'] = {};
product_each_charge['bill_discount'] = 0
var decimalConfiguration = '';



function tinyMceCreate(text_area, tinymce_height, read_only) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        imagetools_cors_hosts: ['picsum.photos'],
        browser_spellcheck: false,
        autosave_ask_before_unload: false,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: false,
        contextmenu: false,
        toolbar: false,
        menubar: false,
        importcss_append: false,
        height: 400,
        image_caption: false,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
        readonly: read_only,
    });
}

function searchProducts(input_box, table_id) {
    var input, filter, found, table, tr, td, i, j;
    input = document.getElementById(input_box);
    filter = input.value.toUpperCase();
    table = document.getElementById(table_id);
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                found = true;
            }
        }
        if (found) {
            tr[i].style.display = "";
            found = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}

function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    number = number.toFixed(parseInt(decimalConfiguration));
    return number;
}

function fillquotation_no(id, quotation_no) {
    $('#quotation_number').val(quotation_no);
    $('#quotation_no_hidden').val(id);
    $('#rfq_idAjaxDiv').hide();
}


function fillItemValues(list, item_code, item_desc, item_id, uom_name, uom_id, uom_value, row_id) {
    $('#item_desc_' + row_id).val(htmlDecode(item_desc));
    $('#item_code_hidden' + row_id).val(item_code);
    $('#item_id_hidden' + row_id).val(item_id);
    $('.ajaxSearchBox').hide();
    getNewRowInserted();
}

function changePurchaseLocation() {
    $('#po_post_btn').attr('disabled', false);
    $('#po_approve_btn').attr('disabled', false);
    var location = $('#deprt_list').val();
    var purchase_type = $('#purchase_type_hidden').val();
    var temp_loc = previous_location;
    var url = base_url + "/purchase/changeItemLocation";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            location: location,
            purchase_type: purchase_type
        },
        beforeSend: function () {
            $("#added_new_list_table_product").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var change_status = obj.change_status;
            var query_string = obj.query_string;
            var purchase_type_get = obj.purchase_type_get;
            var array_len = Object.keys(item_array).length;
            if (parseInt(change_status) == 2) {
                bootbox.alert('PO Transaction Sequence not generated for this particular location.');
                $('#po_post_btn').attr('disabled', true);
                $('#po_approve_btn').attr('disabled', true);

            } else if (parseInt(change_status) == 1 && parseInt(array_len) != 0) {
                bootbox.confirm({
                    message: "Items added to this location would be lost.Are you sure you want to continue ?",
                    buttons: {
                        'confirm': {
                            label: "Continue",
                            className: 'btn-warning',
                            default: 'true'
                        },
                        'cancel': {
                            label: 'Cancel',
                            className: 'btn-success'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $('#row_count_id').val(0);
                            $('#purchase_type_hidden').val(purchase_type_get);
                            $('#added_new_list_table_product').html('');
                            previous_location = $('#deprt_list').val();
                            $('#product_category_search_div').html(query_string);
                            $("#product_category_search").select2();
                            item_array = {};
                            getAllItemsTotals();
                        } else {
                            $("#deprt_list").val(temp_loc);
                            $("#deprt_list").select2();
                        }
                    }
                });
            } else if (parseInt(change_status) == 1) {
                $('#product_category_search_div').html(query_string);
                $("#product_category_search").select2();
            }
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $("#added_new_list_table_product").LoadingOverlay("hide");
        }
    });
}


function confirmPONo(rfq_no, post_type) {
    var post_string = '';
    if (post_type == '1') {
        post_string = "Saved";
    } else if (post_type == '2') {
        post_string = "Closed";
    } else if (post_type == '3') {
        post_string = "Approved";
    } else if (post_type == '0') {
        post_string = "Cancelled";
    } else if (post_type == '4') {
        post_string = "Amended";
    } else if (post_type == '5') {
        post_string = "Verified";
    }
    bootbox.alert({
        title: "PO Successfully " + post_string,
        message: "PO No. :" + rfq_no,
        callback: function () {
            url = base_url + "/frame/framePurchaseList";
            document.location.href = url;
        }
    });
}

function getLocationDefaultValue() {
    var location = $('#location').val();
    var url = base_url + "/quotation/getLocationDefaultValue";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            location: location
        },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#location_defaultvalue').val(data);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
        }
    });
}


function freeItem(row_id) {
    var is_free = $('#free_item_id').is(":checked");
    if (is_free) {
        $('#rate_per_uom').val(0);
        $('#charge_amount11').val(0);
        $('#charge_amount3').val(0);
        $('#tot_rate_cal').val(0);
        $('#charges_tot_pop').val(0);
        $('#rate_per_uom').prop('readonly', true);
        $('#request_rate' + row_id).prop('readonly', true);
    } else {
        $('#rate_per_uom').prop('readonly', false);
        $('#request_rate' + row_id).prop('readonly', false);
        $('#rate_per_uom').val(item_array[row_id].item_rate ? item_array[row_id].item_rate : 0);
        var item_id = item_array[row_id].item_id ? item_array[row_id].item_id : 0;
        calculateAmount(row_id, item_id, 100, 1);
    }
}


function getPrintPreview() {
    var vendor_id = $('#vendoritem_id_hidden').val();
    var po_number = $('#po_number_show').val();
    var deprt_list = $('#deprt_list').val();
    var po_date = $('#po_date').val();
    var po_status = $('#po_status_show').html();
    var manufacturer_code = $('#manufacturer_list').val();
    var remarks = tinymce.get('payment_terms').getContent();
    var item_string = JSON.stringify(item_array);
    var po_id = $('#po_id').val();
    var round_off = $('#round_off').val();
    var bill_discount = $('#bill_discount_amount').val();
    var po_terms_conditions_string = JSON.stringify(po_terms_conditions);
    item_string = encodeURIComponent(item_string);
    po_terms_conditions_string = encodeURIComponent(po_terms_conditions_string);
    if (vendor_id != '0') {
        var url = base_url + "/frame/printPO";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                po_status: po_status,
                round_off: round_off,
                item_string: item_string,
                bill_discount: bill_discount,
                po_id: po_id,
                po_terms_conditions_string: po_terms_conditions_string,
                vendor_id: vendor_id,
                po_number: po_number,
                deprt_list: deprt_list,
                po_date: po_date,
                remarks: remarks,
                manufacturer_code: manufacturer_code
            },
            beforeSend: function () {
                $('#print_preview_btn').attr('disabled', true);
                $('#print_preview_spin').removeClass('fa fa-eye');
                $('#print_preview_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                $('#print_po_modelDiv').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                $("#print_po_model").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#print_preview_btn').attr('disabled', false);
                $('#print_preview_spin').removeClass('fa fa-spinner fa-spin');
                $('#print_preview_spin').addClass('fa fa-eye');
            }
        });
    } else {
        toastr.warning("Please Select Any Supplier");
        $('#searchnewvendor').focus();
    }
}


function DownloadPdfSendMail(mail_status) {
    var vendor_id = $('#vendoritem_id_hidden').val();
    var po_number = $('#po_number_show').val();
    var deprt_list = $('#deprt_list').val();
    var po_date = $('#po_date').val();
    var po_status = $('#po_status_show').html();
    var manufacturer_code = $('#manufacturer_list').val();
    var remarks = tinymce.get('payment_terms').getContent();
    var item_string = JSON.stringify(item_array);
    var po_id = $('#po_id').val();
    var round_off = $('#round_off').val();
    var bill_discount = $('#bill_discount_amount').val();
    var po_terms_conditions_string = JSON.stringify(po_terms_conditions);
    item_string = encodeURIComponent(item_string);
    po_terms_conditions_string = encodeURIComponent(po_terms_conditions_string);
    var cc_email_users_string = JSON.stringify(cc_email_users);
    if (vendor_id != '0') {
        var url = base_url + "/purchase/DownloadPdfPO";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                po_status: po_status,
                mail_status: mail_status,
                round_off: round_off,
                item_string: item_string,
                bill_discount: bill_discount,
                po_id: po_id,
                po_terms_conditions_string: po_terms_conditions_string,
                vendor_id: vendor_id,
                po_number: po_number,
                deprt_list: deprt_list,
                po_date: po_date,
                remarks: remarks,
                manufacturer_code: manufacturer_code,
                cc_email_users_string: cc_email_users_string
            },
            beforeSend: function () {
                if (mail_status == '0') {
                    $('#file_pdf_btn').attr('disabled', true);
                    $('#file_pdf_spin').removeClass('fa fa-file');
                    $('#file_pdf_spin').addClass('fa fa-spinner fa-spin');
                } else if (mail_status == '1') {
                    $('#po_sendmailbtn').attr('disabled', true);
                    $('#po_sendmailbtn_spin').removeClass('fa fa-envelope');
                    $('#po_sendmailbtn_spin').addClass('fa fa-spinner fa-spin');
                }
            },
            success: function (data) {
                if (mail_status == '0') {
                    var file_url = base_url + "/packages/uploads/Purchase/" + data;
                    $("#downloadPoPFDlinka").attr("href", file_url);
                } else if (mail_status == '1') {
                    toastr.success('Mail Sent Successfully');
                }
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                if (mail_status == '0') {
                    $('#file_pdf_btn').attr('disabled', false);
                    $('#file_pdf_spin').removeClass('fa fa-spinner fa-spin');
                    $('#file_pdf_spin').addClass('fa fa-file');
                    $("#downloadPoPFDlinkimg").trigger('click');
                } else if (mail_status == '1') {
                    $('#po_sendmailbtn').attr('disabled', false);
                    $('#po_sendmailbtn_spin').removeClass('fa fa-spinner fa-spin');
                    $('#po_sendmailbtn_spin').addClass('fa fa-envelope');
                }
            }
        });
    } else {
        toastr.warning("Please Select Any Supplier");
        $('#searchnewvendor').focus();
    }
}


function search_product_category() {
    var category_id = $('#product_category_search').val();
    var url = base_url + "/purchase/searchProductCategory";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            category_id: category_id
        },
        beforeSend: function () {
            var subproduct_string = "<select class='form-control select2' id='product_subcategory_search'><option value=''>Product Sub Category</option></select>";
            $('#product_subcategory_div').html(subproduct_string);
            $('#product_subcategory_search').select2();
        },
        success: function (data) {
            $('#product_subcategory_div').html(data);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#product_subcategory_search').select2();
        }
    });
}

function updateEditArray(deatil_array, charge_array, po_head, po_terms, po_email_cc) {
    var row_id = 1;
    $.each(deatil_array, function (key, value) {
        var cgst_per = 0.0;
        var cgst_amt = 0.0;
        var sgst_per = 0.0;
        var sgst_amt = 0.0;
        var igst_per = 0.0;
        var igst_amt = 0.0;
        var discount_per = 0.0;
        var discount_amt = 0.0;
        var flightcharge_per = 0.0;
        var flightcharge_amt = 0.0;
        var othercharge_amt = 0.0;
        var othercharge_per = 0.0;

        if (typeof charge_array[value.detail_id] !== 'undefined') {
            cgst_per = charge_array[value.detail_id].cgst_per ? charge_array[value.detail_id].cgst_per : 0;
            cgst_amt = charge_array[value.detail_id].cgst_amt ? charge_array[value.detail_id].cgst_amt : 0;
            sgst_per = charge_array[value.detail_id].sgst_per ? charge_array[value.detail_id].sgst_per : 0;
            sgst_amt = charge_array[value.detail_id].sgst_amt ? charge_array[value.detail_id].sgst_amt : 0;
            igst_per = charge_array[value.detail_id].igst_per ? charge_array[value.detail_id].igst_per : 0;
            igst_amt = charge_array[value.detail_id].igst_amt ? charge_array[value.detail_id].igst_amt : 0;
            discount_per = charge_array[value.detail_id].discount_per ? charge_array[value.detail_id].discount_per : 0;
            discount_amt = charge_array[value.detail_id].discount_amt ? charge_array[value.detail_id].discount_amt : 0;
            flightcharge_per = charge_array[value.detail_id].flightcharge_per ? charge_array[value.detail_id].flightcharge_per : 0;
            flightcharge_amt = charge_array[value.detail_id].flightcharge_amt ? charge_array[value.detail_id].flightcharge_amt : 0;
            othercharge_per = charge_array[value.detail_id].othercharge_per ? charge_array[value.detail_id].othercharge_per : 0;
            othercharge_amt = charge_array[value.detail_id].othercharge_amt ? charge_array[value.detail_id].othercharge_amt : 0;
        }

        var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
        var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
        var total_othercharges = parseFloat(othercharge_amt) + parseFloat(flightcharge_amt);
        var item_totdiscount_amt = parseFloat(discount_amt);

        // $('#request_tax' + row_id).val(checkIsNaN(total_tax_amt));
        $('#request_discount' + row_id).val(checkIsNaN(item_totdiscount_amt));

        item_array[row_id] = {
            item_id: value.item_id,
            item_code: value.item_code,
            item_desc: value.item_description,
            pur_qty: value.quantity,
            item_unit: value.unit_rate,
            free_qty: value.free_quantity,
            is_free: value.is_free,
            mrp: value.mrp,
            previous_rate: value.previous_rate,
            item_rate: value.pur_rate,
            cgst_per: cgst_per,
            cgst_amt: cgst_amt,
            sgst_per: sgst_per,
            sgst_amt: sgst_amt,
            igst_per: igst_per,
            igst_amt: igst_amt,
            total_tax_perc: total_tax_perc,
            total_tax_amt: total_tax_amt,
            discount_per: discount_per,
            discount_amt: discount_amt,
            othercharge_per: othercharge_per,
            othercharge_amt: othercharge_amt,
            flightcharge_per: flightcharge_per,
            flightcharge_amt: flightcharge_amt,
            total_othercharges: total_othercharges,
            totalRate: value.total,
            netRate: value.ponet_amount,
            remarks: value.narration
        }
        row_id++;
    });

    if (typeof po_head[0] !== 'undefined') {
        var copy_status = $('#copy_status').val();
        if (parseInt(copy_status) != 1) {
            $('#po_date').val(po_head[0].po_date);
        }
        $('#searchnewvendor').val(po_head[0].vendor_name);
        $('#vendoritem_id_hidden').val(po_head[0].supplier_id);
        $('#vendoritem_code_hidden').val(po_head[0].vendor_code);
        $('#manufacturer_list').val(po_head[0].manufacture_code).select2();
        $('#deprt_list').val(po_head[0].location).select2();
        previous_location = po_head[0].location;
        $('#purchase_type_hidden').val(po_head[0].purchase_type);
        if (po_head[0].payment_terms) {
            tinymce.get('payment_terms').setContent(po_head[0].payment_terms);
        }
        $('#payment_remarks').val(po_head[0].remarks);
        $('#round_off').val(po_head[0].round_amount);
        $('#bill_discount_type').val(po_head[0].discount_cal_mode);
        $('#bill_discount_value').val(checkIsNaN(po_head[0].bill_discount_value));
        $('#bill_discount_amount').val(checkIsNaN(po_head[0].bill_discount_amount));
    }
    po_terms_conditions = [];
    $.each(po_terms, function (key, value) {
        terms_id = parseInt(value.terms_id);
        po_terms_conditions.push(terms_id);

    });

    $.each(po_email_cc, function (key, value) {
        manageEmailCCUsers(value.user_name, value.user_email);
        cc_email_users.push({
            'user_name': value.user_name,
            'user_email': value.user_email
        });
    });
    return row_id;
}

function setPoButtons(po_status, po_id) {}
// function setPoButtons(po_status, po_id) {
//     var copy_status = $('#copy_status').val();
//     alert(copy_status);
//     $('#po_approve_btn_id').hide();
//     $('#po_amend_btn_id').hide();
//     $('#po_verify_btn_id').hide();
//     $('#po_post_btn_id').hide();
//     $('#po_close_btn_id').hide();
//     $('#po_cancel_btn_id').hide();
//     $('#po_printpreview_btn_id').hide();
//     $('#po_sendmail_btn_id').hide();
//     $("#addTaxDetailsBtn").show();
//     if (parseInt(copy_status) == 1) {
//         var po_no = $('#pono_hidden').val();
//         $('#po_status_show').html('Copy PO');
//         $('#po_number_show').html("Old PO Number : " + po_no);
//         $('#po_post_btn_id').show();
//         $('#po_approve_btn_id').show();
//         $('#po_email_cc_btn_id').show();
//     } else if (parseInt(po_status) == 1) {
//         $('#po_status_show').html('PO Created');
//         $('#po_post_btn_id').show();
//         $('#po_verify_btn_id').show();
//         $('#po_cancel_btn_id').show();
//         $("#po_approve_btn_id").show();
//         $('#po_email_cc_btn_id').show();
//         $("#po_printpreview_btn_id").show();
//     } else if (parseInt(po_status) == 3) {
//         $('#po_status_show').html('PO Approved');
//         $('#po_close_btn_id').show();
//         $('#po_sendmail_btn_id').show();
//         $("#po_amend_btn_id").show();
//         $("#po_printpreview_btn_id").show();
//         $('#po_email_cc_btn_id').show();
//     } else if (parseInt(po_status) == 4) {
//         $('#po_status_show').html('PO Amend');
//         $('#po_close_btn_id').show();
//         $('#po_sendmail_btn_id').show();
//         $("#po_amend_btn_id").show();
//         $('#po_email_cc_btn_id').show();
//         $("#po_printpreview_btn_id").show();
//     } else if (parseInt(po_status) == 5) {
//         $('#po_status_show').html('PO Verified');
//         $('#po_email_cc_btn_id').show();
//         $('#po_cancel_btn_id').show();
//         $('#po_post_btn_id').show();
//         $('#po_approve_btn_id').show();
//         $("#po_printpreview_btn_id").show();
//     } else if (parseInt(po_status) == 0) {
//         $('#po_status_show').html('PO Cancelled');
//         $('#po_quotatinlist_btn_id').hide();
//         $('#po_pricecontract_list_btn_id').hide();
//         $('#po_reorderlist_btn_id').hide();
//         $('#po_post_btn_id').hide();
//         $('#po_productslist_btn_id').hide();
//         $("#po_printpreview_btn_id").show();
//         $("#addTaxDetailsBtn").hide();
//     } else if (parseInt(po_status) == 2) {
//         $('#po_status_show').html('PO Closed');
//         $('#po_quotatinlist_btn_id').hide();
//         $('#po_pricecontract_list_btn_id').hide();
//         $('#po_reorderlist_btn_id').hide();
//         $('#po_post_btn_id').hide();
//         $('#po_productslist_btn_id').hide();
//         $("#po_printpreview_btn_id").show();
//         $("#addTaxDetailsBtn").hide();
//     } else {
//         $('#po_status_show').html('New PO');
//         $('#po_email_cc_btn_id').show();
//         $('#po_approve_btn_id').show();
//         $('#po_post_btn_id').show();
//     }
// }

function checkEditStatus() {
    var po_id = $('#po_id').val();
    // var po_id  = 11;
    if (po_id != '0') {
        var url = base_url + "/frame/editPurchaseOrderRow";
        vendor_editstatus = 1;
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                po_id: po_id
            },
            beforeSend: function () {
                $('#addQuotationRowbtn').attr('disabled', true);
                $('#addQuotationRowSpin').removeClass('fa fa-plus');
                $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
                $('#row_count_id').val(1);
            },
            success: function (data) {
                var obj = JSON.parse(data);
                $("#added_new_list_table_product").html(obj.html);
                updateHeadDetails(obj.po_head);
                row_id = insertRowCalculation(obj.po_count, obj.is_igst);
                $('#row_count_id').val(row_id);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#addQuotationRowbtn').attr('disabled', false);
                $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addQuotationRowSpin').addClass('fa fa-plus');
            }
        });
    } else {
        setPoButtons(100, po_id);
    }
}

function insertRowCalculation(po_count) {
    var row_id = $('#row_count_id').val();
    for (var i = 1; i <= po_count; i++) {
        listDataCalculation(row_id);
        row_id++;
    }
    return row_id;
}



function updateHeadDetails(po_head) {
    $('#grnSavePosttype').val(po_head[0].status ? po_head[0].status : 0);
    $('#po_number_show').val(po_head[0].po_no ? po_head[0].po_no : '');
    $('#payment_remarks').val(po_head[0].remarks ? po_head[0].remarks : '');
    $('#searchnewvendor').val(po_head[0].vendor_name ? po_head[0].vendor_name : '');
    $('#vendoritem_code_hidden').val(po_head[0].vendor_code ? po_head[0].vendor_code : '');
    $('#vendoritem_id_hidden').val(po_head[0].vendor_id ? po_head[0].vendor_id : '');
    $('#manufacturer_list').val(po_head[0].manufacturer_code ? po_head[0].manufacturer_code : '').select2();
    $('#round_off').val(po_head[0].round_of_amount ? po_head[0].round_of_amount : 0);
    $('#po_date').val(po_head[0].po_date ? po_head[0].po_date : '');
    var is_igst = po_head[0].is_igst ? po_head[0].is_igst : 0;
    if (parseInt(is_igst) == 1) {
        $('#is_igstcheck').prop('checked', true);
    }
    $('#deprt_list').val(po_head[0].location ? po_head[0].location : '').select2();
}

function getNewRowInserted() {
    if ($(".row_class:last").find("input[name='item_desc[]']").val() != '') {
        var url = base_url + "/frame/addPurchaseOrderRow";
        var row_count = $('#row_count_id').val();
        $('#row_count_id').val(parseInt(row_count) + 1);
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                row_count: row_count
            },
            beforeSend: function () {
                $('#addQuotationRowbtn').attr('disabled', true);
                $('#addQuotationRowSpin').removeClass('fa fa-plus');
                $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                $('#added_new_list_table_product').append(html);
                row_ct = 1;
                $(".row_class").each(function (i) {
                    $(this).find('.row_count_class').text(row_ct); // row Count Re-arenge
                    row_ct++;
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#item_desc_' + row_count).focus();
                $('#addQuotationRowbtn').attr('disabled', false);
                $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addQuotationRowSpin').addClass('fa fa-plus');
                $(".theadfix_wrapper").floatThead('reflow');
                $('.form-control').css("border-color", "#EFEFEF");
            }
        });
    }
}

function addQuotationItems(from_type) {
    request_array = new Array();
    var flag = true;
    $('.form-control').css("border-color", "#EFEFEF");
    $('#added_new_list_table_product tr').each(function () {
        var item_id = $(this).find("input[name='item_id_hidden[]']").val();
        if (item_id != '') {
            var row_id = $(this).find("input[name='row_id_hidden[]']").val();
            var uom_val = $('#uom_select_id_' + row_id + ' option').data("uom_value");
            var request_qty = $(this).find("input[name='request_qty[]']").val();
            var comments = $(this).find("input[name='comments[]']").val();
            if (from_type == 1) {
                if (request_qty == 0 || !request_qty) {
                    flag = false;
                    $(this).find("input[name='request_qty[]']").css("border-color", "#ff0000");
                }
            }
            if (flag == true) {
                request_array.push({
                    'item_id': item_id,
                    'row_id': row_id,
                    'uom_val': uom_val,
                    'request_qty': request_qty,
                    'comments': comments
                });
            }
        }
    });
    return flag;
}

function getProductCharges(row_id, item_id) {
    var url = base_url + "/purchase/getPurchaseOrderPopup";
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_desc = $('#item_desc_' + row_id).val();
    var item_uom = $('#uom_select_id_' + row_id).val();
    var qty = $('#request_qty' + row_id).val();
    var rate = $('#request_rate' + row_id).val();
    var free_qty = $('#request_freeqty' + row_id).val();
    var mrp = $('#request_mrp' + row_id).val();
    var free_item_status = $('#free_vat_onselect' + row_id).val();
    var comments = $('#request_comments' + row_id).val();
    $('#calculationSelectRowID').val(row_id);
    var vendor_string = JSON.stringify(item_array);
    var param = {
        _token: token,
        row_id: row_id,
        vendor_string: vendor_string,
        free_qty: free_qty,
        mrp: mrp,
        free_item_status: free_item_status,
        comments: comments,
        item_code: item_code,
        item_desc: item_desc,
        item_uom: item_uom,
        qty: qty,
        rate: rate,
        item_id: item_id,
        detail_id: 0
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#chargesbtnbtn' + row_id).attr("disabled", true);
            $('#chargesbtnspin' + row_id).removeClass('fa fa-calculator');
            $('#chargesbtnspin' + row_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#purchase_charges_div').html(data);
            $("#purchase_charge_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#chargesbtnbtn' + row_id).attr("disabled", false);
            $('#chargesbtnspin' + row_id).removeClass('fa fa-spinner fa-spin');
            $('#chargesbtnspin' + row_id).addClass('fa fa-calculator');
            setTimeout(function () {
                calculateAmount(row_id, item_id, 100, 1);
                changeUOMVal(row_id, 1);
            }, 500);
        }
    });
}

function removeRow(row_id) {
    var item_desc = $('#item_desc_' + row_id).val();
    if (item_desc) {
        bootbox.confirm({
            message: "Are you sure you want to delete ?",
            buttons: {
                'confirm': {
                    label: "Delete",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    removeSingleRow(row_id);
                }
            }
        });
    } else {
        removeSingleRow(row_id);
    }

}

function removeSingleRow(row_id) {
    $("#row_data_" + row_id).remove();
    delete item_array[row_id]
    row_ct = 1;
    $(".row_class").each(function (i) {
        $(this).find('.row_count_class').text(row_ct);
        row_ct++;
    });
    getAllItemsTotals();
}

function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function getItemDescID(itemID) {
    var flag = 1;
    $(".itemdesc_id").each(function (i) {
        var itemID_get = $(this).val();
        if (parseInt(itemID) == parseInt(itemID_get)) {
            flag = 0;
        }
    });
    return flag;
}



function getItemDetailsData(item_code, row_id, item_id) {
    var url = base_url + "/purchase/selectPurchaseItem";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            is_igst: 0,
            item_code: item_code
        },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (result) {
            grn_dls = JSON.parse(result.grn_details);
            if (typeof (grn_dls[0]) !== "undefined")
                var rate = grn_dls[0].grn_rate;
            $('#request_rate' + row_id).val(checkIsNaN(rate));

            if (typeof (grn_dls[0]) !== "undefined")
                $('#request_mrp' + row_id).val(checkIsNaN(grn_dls[0].grn_mrp));

            if (typeof (grn_dls[0]) !== "undefined")
                var request_qty = grn_dls[0].request_qty
            $('#request_qty' + row_id).val(request_qty);

            if (typeof (grn_dls[0]) !== "undefined")
                $('#request_freeqty' + row_id).val(checkIsNaN(grn_dls[0].grn_free_qty));

            if (typeof (grn_dls[0]) !== "undefined") {
                tax_exclusive = grn_dls[0].tax_exclusive;
                if (tax_exclusive == '2')
                    is_exclusive = '1';
            }
        },
        complete: function () {
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
            setTimeout(function () {
                $('#chargesbtnbtn' + row_id).attr('onClick', 'getProductCharges(' + row_id + ',' + item_id + ')');
                $('#chargesbtnbtn' + row_id).attr("disabled", false);
                $('#getGlobalItemsbtn' + row_id).attr("disabled", false);
                getProductCharges(row_id, item_id);
            }, 1000);

        },
    })
}


function rawurlencode(str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
    replace(/\)/g, '%29').replace(/\*/g, '%2A');
}

function rawurldecode(str) {
    return decodeURIComponent((str + '')
        .replace(/%(?![\da-f]{2})/gi, function () {
            return '%25';
        }));
}

document.onkeydown = function (evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc");
    } else {
        isEscape = (evt.keyCode === 27);
    }
    if (isEscape) {
        $('#purchase_charge_modal').modal('hide');
    }
};


function getCalculations(row_id) {
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_id = $('#itemid_hidden' + row_id).val();
    item_codecharge(item_code, 0, item_id);
}

function fetchUOMData(item_id, row_id, uom_id, from_type) {
    var url = base_url + "/quotation/getItemUom";
    var item_code = $('#item_code_hidden' + row_id).val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            item_id: item_id,
            uom_id: uom_id,
            item_code: item_code
        },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            response = JSON.parse(response);
            $('#uom_select_id_' + row_id).html(response.option);
            $('#request_qty' + row_id).focus();
        },
        complete: function () {
            if (from_type == '1') {
                if ($(".row_class:last").find("input[name='item_code_hidden']").val() != '') {
                    getNewRowInserted();
                }
            }
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
            $('#item_desc_' + row_id).prop("readonly", true);
            $('#chargesbtnbtn' + row_id).attr('onClick', 'getProductCharges(' + row_id + ',' + item_id + ')');
            $('#chargesbtnbtn' + row_id).attr("disabled", false);
            $('#getGlobalItemsbtn' + row_id).attr("disabled", false);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

var vendor_type = '';

function searchNewVendor(id, event) {
    vendor_type = 'main';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxVendorSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $('#searchnewvendor').val();
        if (vendor_name == "") {
            $('#ajaxVendorSearchBox').hide();
            $("#vendoritem_id_hidden").val(0);
            $("#vendoritem_code_hidden").val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    vendor_name: vendor_name,
                    row_id: 1
                },
                beforeSend: function () {
                    $("#ajaxVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxVendorSearchBox").html(html).show();
                    $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxVendorSearchBox', event);
    }
}

function searchNewVendor(id, event) {
    vendor_type = 'main';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxVendorSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $('#searchnewvendor').val();
        if (vendor_name == "") {
            $('#ajaxVendorSearchBox').hide();
            $("#vendoritem_id_hidden").val(0);
            $("#vendoritem_code_hidden").val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    vendor_name: vendor_name,
                    row_id: 1
                },
                beforeSend: function () {
                    $("#ajaxVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxVendorSearchBox").html(html).show();
                    $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxVendorSearchBox', event);
    }
}


function searchSubVendor(id, event) {
    vendor_type = 'sub';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSubVendorSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $('#searchSubVendor').val();
        if (vendor_name == "") {
            $('#ajaxSubVendorSearchBox').hide();
            $("#vendorSubitem_id_hidden").val(0);
            $("#vendorSubitem_code_hidden").val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    vendor_name: vendor_name,
                    row_id: 1
                },
                beforeSend: function () {
                    $("#ajaxSubVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxSubVendorSearchBox").html(html).show();
                    $("#ajaxSubVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxSubVendorSearchBox', event);
    }
}



function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    if (vendor_type == 'sub') {
        vendor_name = htmlDecode(vendor_name);
        $('#searchSubVendor').val(vendor_name);
        $('#vendorSubitem_id_hidden').val(id);
        $('#vendorSubitem_code_hidden').val(vendor_code);
        $('#ajaxSubVendorSearchBox').hide();
    } else {
        vendor_name = htmlDecode(vendor_name);
        $('#searchnewvendor').val(vendor_name);
        $('#vendoritem_id_hidden').val(id);
        $('#vendoritem_code_hidden').val(vendor_code);
        $('#ajaxVendorSearchBox').hide();
    }
}

function discount_calculation(type, obj, item_gross, item_code, charge_id, vat_on, item_gross_mrp, gross_free) {
    if (type == 'percentage') {
        perc_val = parseFloat(obj.value);
        amount = item_gross * (perc_val / 100);
        amount = checkIsNaN(amount);
        $('#charge_amount' + charge_id).val(checkIsNaN(amount));
    } else if (type == 'amount') {
        amount = parseFloat(obj.value);
        perc_val = (amount * 100) / item_gross;
        perc_val = checkIsNaN(perc_val);
        $('#charge_percentage' + charge_id).val(perc_val);
    }
    if (!product_each_charge['discount'].hasOwnProperty(item_code)) {
        product_each_charge['discount'][item_code] = {};
        product_each_charge['discount'][item_code]['percentage'] = {};
    }
    product_each_charge['discount'][item_code]['percentage'][charge_id] = perc_val;
    re_calculation(item_code, item_gross, vat_on, item_gross_mrp, gross_free);
}

function validate_number(obj, post_type, vali_data) {
    var num = 0.0;
    if (post_type == '1') {
        num = obj.value;
    } else if (post_type == '2') {
        num = obj.val();
    }
    if (num) {
        if (isNaN(num)) {
            $('#' + obj.id).addClass('text_boxalert');
            return false;
        } else if (num.indexOf(' ') >= 0) {
            $('#' + obj.id).addClass('text_boxalert');
            $('#' + obj[0].id).addClass('text_boxalert');
            return false;
        } else {
            $('#' + obj.id).removeClass('text_boxalert');
            return true;
        }
    } else {
        if (vali_data == '1') {
            $('#' + obj.id).removeClass('text_boxalert');
            return true;
        } else {
            $('#' + obj.id).addClass('text_boxalert');
            return false;
        }
    }
}


function validateText() {
    $(".td_common_numeric_rules").attr('disabled', false);
    $(".td_common_numeric_rules").each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        } else if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        } else if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        } else if (!val) {
            $(this).val(0);
            flag = 1;
        }
        if ($(this).hasClass("percentage_class")) {
            if (parseFloat(val) > 100) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            }
        }
        if (flag == 0) {
            // val = parseFloat(val).toFixed(2);
            $(this).val(checkIsNaN(val));
        }
    });

    $(".number_class").blur(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        } else if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        } else if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        } else if (!val) {
            $(this).val(0);
            flag = 1;
        }
    });
}


function taxCalculation() {
    var req_qty = $('#purchase_qty').val();
    var item_price = $('#rate_per_uom').val();
    var tot_rate = req_qty * item_price;
    var tot_amt = 0.0;
    $('.amount_class').each(function () {
        gst_amt = $(this).val();
        item_type = $(this).attr("data-code");
        if (item_type == 'GA') {
            tot_amt -= parseFloat(gst_amt);
        } else {
            tot_amt += parseFloat(gst_amt);
        }
    });
    var net_rate = tot_rate + tot_amt;
    $('#tot_rate_cal').val(checkIsNaN(tot_rate));
    $('#charges_tot_pop').val(checkIsNaN(net_rate));
}



$(document).on('blur', '#bill_discount_value', function () {
    var disc_value = $('#bill_discount_value').val();
    if (disc_value == "") {
        $('#bill_discount_value').val(parseFloat(0.00));
    }
});

function BillDiscountcalculate() {
    var disc_type = $('#bill_discount_type').val();
    var disc_value = 0.0;
    var round_off = $('#round_off').val();
    if (isNaN(disc_value)) {
        disc_value = 0;
    }
    if (disc_type) {
        if (!isNaN(disc_value)) {
            var net_total = 0.0;
            var total_discount = 0.0;
            for (var key in item_array) {
                var obj = item_array[key];
                net_total += parseFloat(obj.netRate);
                net_discount = parseFloat(obj.discount_amt);
                total_discount += net_discount;
            }

            if (disc_type == '1') {
                per_amt = parseFloat($('#bill_discount_value').val());
                amount = (per_amt / 100) * net_total;
                disc_value = amount;
            } else if (disc_type == '2') {
                disc_value = parseFloat($('#bill_discount_value').val());
            }
            if (parseFloat(net_total) > parseFloat(disc_value)) {
                var disount_tot = (parseFloat(net_total) + parseFloat(round_off) - parseFloat(disc_value));
                $('#bill_discount_amount').val(checkIsNaN(disc_value));
                $('#po_amount').val(checkIsNaN(disount_tot));
                $('#payment_other').val(checkIsNaN(parseFloat(disc_value) + parseFloat(total_discount)));
                $('.savepobtn').attr('disabled', false);
                $('#bill_discount_generate').html("Apply Discount <i class='fa fa-dollar'></i>");
            } else {
                toastr.warning("Net Amount Should Be Greater Than Discount Amount");
            }
        } else {
            toastr.warning("Please Enter Valid Amount");
        }


    } else {
        toastr.warning("Choose a discount type");
    }
}


function getRoundOffAmt() {

    var re = /^-?\d*\.?\d{0,6}$/;
    var round_off = $('#round_off').val();
    var isValid = (round_off.match(re) !== null);
    var bill_discount_amount = parseFloat($('#bill_discount_amount').val());
    if (isValid) {
        var net_total = 0.0;
        for (var key in item_array) {
            var obj = item_array[key];
            net_total += parseFloat(obj.netRate);
        }
        if (parseFloat(net_total) > parseFloat(round_off)) {
            var round_tot = (parseFloat(net_total) + parseFloat(round_off) - parseFloat(bill_discount_amount));
            $('#po_amount').val(checkIsNaN(round_tot));
        }
    } else {
        $('#round_off').val(0.00);
    }
}


// function updateRoundAmt(check_id, row_id) {
//     var number = $('#' + check_id + row_id).val();
//     if (!number) {
//         $('#' + check_id).val(0.00);
//     } else if (isNaN(number)) {
//         $('#' + check_id).val(0.00);
//     }
// }
function updateRoundAmt() {
    var round_off_amt = $('#round_off').val();
    var gross_amt = $('#item_value').val();
    var payment_othercharges = $('#payment_othercharges').val();
    var po_amount = parseFloat(round_off_amt) + parseFloat(gross_amt) + parseFloat(payment_othercharges);
    $('#po_amount').val(parseFloat(po_amount));
}



function validatePoData() {
    // console.log(item_array);
    var flag = true;
    $('.form-control').removeClass('validate_data');
    $.each(item_array, function (index, value) {
        item_desc = value.item_desc;
        item_code = value.item_code;
        pur_unit = value.pur_unit;
        if (!item_desc) {
            $('#item_desc_' + index).addClass('validate_data');
            flag = false;
        }
        if (!item_code) {
            $('#item_desc_' + index).addClass('validate_data');
            flag = false;
        }
        if (parseInt(pur_unit) == 0) {
            $('#uom_select_id_' + index).addClass('validate_data');
            flag = false;
        }
    });
    return flag;
}


// function savePurchaseOrder(post_type) {
//     var vendor_id = $('#vendoritem_id_hidden').val();
//     var net_amt = $('#po_amount').val();
//     var cancel_string = 'Cancel';
//     var flag = validatePoData();
//     var array_len = Object.keys(item_array).length;
//     if (flag) {
//         if (parseInt(array_len) != 0) {
//             if (vendor_id != '0') {
//                 var post_string = '';
//                 if (post_type == '1') {
//                     post_string = "Save";
//                 } else if (post_type == '2') {
//                     post_string = "Close";
//                 } else if (post_type == '3') {
//                     post_string = "Approve";
//                 } else if (post_type == '0') {
//                     post_string = "Cancel";
//                     cancel_string = "No";
//                 } else if (post_type == '4') {
//                     post_string = "Amend";
//                 } else if (post_type == '5') {
//                     post_string = "Verify";
//                 }
//                 bootbox.confirm({
//                     message: "Do you want to " + post_string + " this Purchase Order?",
//                     buttons: {
//                         'confirm': {
//                             label: post_string,
//                             className: 'btn-success',
//                             default: 'true'
//                         },
//                         'cancel': {
//                             label: cancel_string,
//                             className: 'btn-warning'
//                         }
//                     },
//                     callback: function (result) {
//                         if (result) {
//                             var url = base_url + "/purchase/savePOData";
//                             var manufacturer_code = $('#manufacturer_list').val();
//                             var vendor_code = $('#vendoritem_code_hidden').val();
//                             var po_added = $('#po_added').val();
//                             var po_date = $('#po_date').val();
//                             var location = $('#deprt_list').val();
//                             var payment_terms = tinymce.get('payment_terms').getContent();
//                             var payment_remarks = $('#payment_remarks').val();
//                             var disc_mode = $('#disc_mode').val();
//                             var bill_discount_type = $('#bill_discount_type').val();
//                             var bill_discount_value = $('#bill_discount_value').val();
//                             var bill_discount_amount = $('#bill_discount_amount').val();
//                             var copy_status = $('#copy_status').val();
//                             var round_off = $('#round_off').val();

//                             var head_id = $('#po_id').val();

//                             var total_cgst = 0.0;
//                             var total_sgst = 0.0;
//                             var total_igst = 0.0;
//                             var total_cess = 0.0;
//                             var total_discount = 0.0;
//                             var net_total = 0.0;
//                             var total_othercharges = 0.0;
//                             var total_freightcharges = 0.0;
//                             var total_amt = 0.0;
//                             for (var key in item_array) {
//                                 var obj = item_array[key];
//                                 total_cgst += parseFloat(obj.cgst_amt);
//                                 total_sgst += parseFloat(obj.sgst_amt);
//                                 total_igst += parseFloat(obj.igst_amt);
//                                 total_discount += parseFloat(obj.discount_amt);
//                                 total_discount += parseFloat(obj.special_discount_amt);
//                                 total_othercharges += parseFloat(obj.othercharge_amt);
//                                 total_freightcharges += parseFloat(obj.flightcharge_amt);
//                                 total_amt += parseFloat(obj.totalRate);
//                                 net_total += parseFloat(obj.netRate);
//                             }

//                             var tot_tax = parseFloat(total_cgst) + parseFloat(total_sgst) + parseFloat(total_igst) + parseFloat(total_cess);
//                             var oth_charges = parseFloat(total_othercharges) + parseFloat(total_freightcharges);

//                             var item_string = JSON.stringify(item_array);
//                             item_string = encodeURIComponent(item_string);

//                             var po_terms = JSON.stringify(po_terms_conditions);
//                             po_terms = encodeURIComponent(po_terms);
//                             var cc_email_users_string = JSON.stringify(cc_email_users);

//                             var param = {
//                                 _token: token,
//                                 manufacturer_code: manufacturer_code,
//                                 po_added: po_added,
//                                 po_date: po_date,
//                                 payment_terms: payment_terms,
//                                 payment_remarks: payment_remarks,
//                                 disc_mode: disc_mode,
//                                 bill_discount_type: bill_discount_type,
//                                 po_terms: po_terms,
//                                 vendor_id: vendor_id,
//                                 bill_discount_value: bill_discount_value,
//                                 total_amt: total_amt,
//                                 bill_discount_amount: bill_discount_amount,
//                                 total_discount: total_discount,
//                                 location: location,
//                                 vendor_code: vendor_code,
//                                 post_type: post_type,
//                                 copy_status: copy_status,
//                                 net_amt: net_amt,
//                                 round_off: round_off,
//                                 total_othercharges: total_othercharges,
//                                 total_freightcharges: total_freightcharges,
//                                 head_id: head_id,
//                                 net_total: net_total,
//                                 tot_tax: tot_tax,
//                                 oth_charges: oth_charges,
//                                 item_string: item_string,
//                                 cc_email_users_string: cc_email_users_string
//                             };

//                             $.ajax({
//                                 type: "POST",
//                                 url: url,
//                                 data: param,
//                                 beforeSend: function () {
//                                     if (post_type == '0') {
//                                         $('#po_cancel_btn').attr('disabled', true);
//                                         $('#po_cancel_spin').removeClass('fa fa-fast-backward');
//                                         $('#po_cancel_spin').addClass('fa fa-spinner fa-spin');
//                                     } else if (post_type == '1') {
//                                         $('#po_post_btn').attr('disabled', true);
//                                         $('#po_post_spin').removeClass('fa fa-paper-plane-o');
//                                         $('#po_post_spin').addClass('fa fa-spinner fa-spin');
//                                     } else if (post_type == '2') {
//                                         $('#po_close_btn').attr('disabled', true);
//                                         $('#po_close_spin').removeClass('fa fa-times-circle-o');
//                                         $('#po_close_spin').addClass('fa fa-spinner fa-spin');
//                                     } else if (post_type == '3') {
//                                         $('#po_approve_btn').attr('disabled', true);
//                                         $('#po_approve_spin').removeClass('fa fa-buysellads');
//                                         $('#po_approve_spin').addClass('fa fa-spinner fa-spin');
//                                     } else if (post_type == '4') {
//                                         $('#po_amend_btn').attr('disabled', true);
//                                         $('#po_amend_spin').removeClass('fa fa-edit');
//                                         $('#po_amend_spin').addClass('fa fa-spinner fa-spin');
//                                     } else if (post_type == '5') {
//                                         $('#po_verify_btn').attr('disabled', true);
//                                         $('#po_verify_spin').removeClass('fa fa-user-secret');
//                                         $('#po_verify_spin').addClass('fa fa-spinner fa-spin');
//                                     }
//                                 },

//                                 success: function (result) {
//                                     confirmPONo(result, post_type);
//                                 },
//                                 error: function () {
//                                     toastr.error('Please check your internet connection and try again');
//                                 },
//                                 complete: function () {
//                                     if (post_type == '0') {
//                                         $('#po_cancel_btn').attr('disabled', false);
//                                         $('#po_cancel_spin').removeClass('fa fa-spinner fa-spin');
//                                         $('#po_cancel_spin').addClass('fa fa-fast-backward');
//                                     } else if (post_type == '1') {
//                                         $('#po_post_btn').attr('disabled', false);
//                                         $('#po_post_spin').removeClass('fa fa-spinner fa-spin');
//                                         $('#po_post_spin').addClass('fa fa-paper-plane-o');
//                                     } else if (post_type == '2') {
//                                         $('#po_close_btn').attr('disabled', false);
//                                         $('#po_close_spin').removeClass('fa fa-spinner fa-spin');
//                                         $('#po_close_spin').addClass('fa fa-times-circle-o');
//                                     } else if (post_type == '3') {
//                                         $('#po_approve_btn').attr('disabled', false);
//                                         $('#po_approve_spin').removeClass('fa fa-spinner fa-spin');
//                                         $('#po_approve_spin').addClass('fa fa-buysellads');
//                                     } else if (post_type == '4') {
//                                         $('#po_amend_btn').attr('disabled', false);
//                                         $('#po_amend_spin').removeClass('fa fa-spinner fa-spin');
//                                         $('#po_amend_spin').addClass('fa fa-edit');
//                                     } else if (post_type == '5') {
//                                         $('#po_verify_btn').attr('disabled', false);
//                                         $('#po_verify_spin').removeClass('fa fa-spinner fa-spin');
//                                         $('#po_verify_spin').addClass('fa fa-user-secret');
//                                     }
//                                 }
//                             });


//                         }
//                     }
//                 });
//             } else {
//                 toastr.warning("Please Select a Vendor");
//                 $('#vendoritem_id_hidden').focus();
//             }
//         } else {
//             toastr.warning("Please Select a Item");
//         }
//     } else {
//         toastr.warning("Please validate items");
//     }
// }

function changeUOMVal(row_id, from_type) {
    if (from_type == 1) {
        uom_val = $("#uom_select_pop_id option:selected").attr('data-uom_value');
        $('#uom_value_pop_id').val(uom_val);
    } else if (from_type == 2) {
        uom_val = $("#uom_select_id_" + row_id).val();
        if (item_array[row_id].item_unit) {
            item_array[row_id].item_unit = uom_val;
        }
    }
}


function getVendorSavedData(from_type) {
    var defaultCategory = $('#defaultCategory').val();
    if (defaultCategory) {
        $('#product_category_search').val(defaultCategory);
        $('#product_category_search').select2();
        search_product_category();
    }
    $('#searchpolistitem').val('');
    $('#poListfromtype').val(from_type);
    var vendor_name = $('#searchnewvendor').val();
    var vendor_id = $('#vendoritem_id_hidden').val();
    var vendor_code = $('#vendoritem_code_hidden').val();
    var SubVendor = $('#searchSubVendor').val();
    if (from_type == '1') {
        $('.rfqnumber_sequencediv').show();
        $('.vendorcontract_sequencediv').hide();
        $('#searchDataDiv').html('');
        $('#getVendorQuotationModelHeader').html('RFQ List');
        searchQuotation();
        $("#getVendorQuotationModel").modal({
            backdrop: 'static',
            keyboard: false
        });

    } else if (from_type == '2') {
        $('.rfqnumber_sequencediv').hide();
        $('.vendorcontract_sequencediv').show();
        $('#getVendorQuotationModelHeader').html('Price Contract');
        searchVendorContract();
        $("#getVendorQuotationModel").modal({
            backdrop: 'static',
            keyboard: false
        });
    } else if (from_type == '3') {
        if (vendor_id && !SubVendor) {
            $('#searchSubVendor').val(vendor_name)
            $('#vendorSubitem_id_hidden').val(vendor_id)
            $('#vendorSubitem_code_hidden').val(vendor_code)
        }
        $('#getAllProductModelHeader').html('ReOrder List');
        $('#searchListDataDiv').html('');
        $("#getAllProductModel").modal({
            backdrop: 'static',
            keyboard: false
        });
        searchListItems();

    } else if (from_type == '4') {
        if (vendor_id && !SubVendor) {
            $('#searchSubVendor').val(vendor_name)
            $('#vendorSubitem_id_hidden').val(vendor_id)
            $('#vendorSubitem_code_hidden').val(vendor_code)
        }
        $('#getAllProductModelHeader').html('All Products');
        $('#searchListDataDiv').html('');
        $('#product_subcategory_search').val('').select2();
        $("#getAllProductModel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
}

function getPoTermsConditions() {
    var po_terms = JSON.stringify(po_terms_conditions);
    po_terms = encodeURIComponent(po_terms);
    var url = base_url + "/purchase/getPoTermsConditions";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_terms: po_terms
        },
        beforeSend: function () {
            $('#po_terms_condtion_btn').attr('disabled', true);
            $('#po_terms_condtion_spin').removeClass('fa fa-tumblr');
            $('#po_terms_condtion_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#getPoTermsConditionsDiv").html(data);
            $("#getPoTermsConditionsModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            $('#po_terms_condtion_btn').attr('disabled', false);
            $('#po_terms_condtion_spin').removeClass('fa fa-spinner fa-spin');
            $('#po_terms_condtion_spin').addClass('fa fa-tumblr');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function addPotermsConditions(terms_id) {
    terms_id = parseInt(terms_id);
    var status = $('#po_terms_conditions' + terms_id).is(":checked");
    if (status) {
        po_terms_conditions.push(terms_id);
    } else {
        var index = po_terms_conditions.indexOf(terms_id);
        if (index !== -1) {
            po_terms_conditions.splice(index, 1);
        }
    }
}


function searchQuotation() {
    var quotation_id = $('#quotation_no_hidden').val();
    var to_date = $('#to_date').val();
    var from_date = $('#from_date').val();
    var url = base_url + "/quotation/searchVendorQuotation";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            quotation_id: quotation_id,
            to_date: to_date,
            from_date: from_date
        },
        beforeSend: function () {
            $('#searchQuotationBtn').attr('disabled', true);
            $('#searchQuotationSpin').removeClass('fa fa-search');
            $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchQuotationBtn').attr('disabled', false);
            $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchQuotationSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function searchVendorContract() {
    var contract_id = $('#contract_id_hidden').val();
    var to_date = $('#to_date').val();
    var from_date = $('#from_date').val();
    var url = base_url + "/quotation/searchVendorContractList";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            contract_id: contract_id,
            status: 1,
            from_po: 1,
            to_date: to_date,
            from_date: from_date
        },
        beforeSend: function () {
            $('#searchContractBtn').attr('disabled', true);
            $('#searchContractSpin').removeClass('fa fa-search');
            $('#searchContractSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchContractBtn').attr('disabled', false);
            $('#searchContractSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchContractSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function searchContractNo() {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var contractno = $('#searchContract').val();
        if (contractno == "") {
            $('#ajaxContractSearchBox').hide();
            $("#contract_id_hidden").val(0);
        } else {
            var url = base_url + "/quotation/contractno_search";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    contractno: contractno
                },
                beforeSend: function () {
                    $("#ajaxContractSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxContractSearchBox").html(html).show();
                    $("#ajaxContractSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }

    }
}

function getQuotationData(list_id, from_type, list_no) {
    var url = base_url + "/purchase/getQuotationData";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            list_id: list_id,
            from_type: from_type
        },
        beforeSend: function () {
            $('#getQuotationItemListModelHeader').html(list_no);
            $("#searchDataDiv").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            $("#getVendorQuotationModel").modal('toggle');
            $("#getQuotationItemListModelDiv").html(html);
            $("#getQuotationItemListModel").modal({
                backdrop: 'static',
                keyboard: false
            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#searchDataDiv").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function searchListItems() {
    var from_type = $('#poListfromtype').val();
    var product_category = $('#product_category_search').val();
    var product_subcategory = $('#product_subcategory_search').val();
    var url = base_url + "/purchase/searchListItems";
    var location = $('#deprt_list').val();
    var searchDesc = $('#searchpolistitem').val();
    var vendor_code = $('#vendorSubitem_code_hidden').val();
    var item_array_string = JSON.stringify(item_array);
    if (product_category || from_type == '3') {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                item_array_string: item_array_string,
                vendor_code: vendor_code,
                location: location,
                searchDesc: searchDesc,
                product_category: product_category,
                product_subcategory: product_subcategory,
                from_type: from_type
            },
            beforeSend: function () {
                $('#searchListItemBtn').attr('disabled', true);
                $('#searchListItemSpin').removeClass('fa fa-search');
                $('#searchListItemSpin').addClass('fa fa-spinner fa-spin');
                $('#selectAllItemBtn').attr('disabled', true);
            },
            success: function (data) {
                $('#searchListDataDiv').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function () {
                $('#searchListItemBtn').attr('disabled', false);
                $('#searchListItemSpin').removeClass('fa fa-spinner fa-spin');
                $('#searchListItemSpin').addClass('fa fa-search');
                var item_cnt = $('#fulllist_itemcount').val();
                if (item_cnt) {
                    $('#selectAllItemBtn').attr('disabled', false);
                }
                unselectAllListItems(1);
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning("Please Select Product Category");
        $('#product_category_search').select2('open');
    }
}


function selectAllListItem() {
    var status = $('#checkAllListItems').is(":checked");
    if (status) {
        bootbox.confirm({
            message: "Are you sure, you want to select all item ?",
            buttons: {
                'confirm': {
                    label: "Select All",
                    className: 'btn-warning',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if (result) {
                    var item_cnt = $('#fulllist_itemcount').val();
                    var list_type = $('#poListfromtype').val();
                    var item_array_string = JSON.stringify(item_array);
                    var deprt_list = $('#deprt_list').val();
                    if (item_cnt) {
                        var url = base_url + "/purchase/selectAllListedData";
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                _token: token,
                                item_array_string: item_array_string,
                                item_cnt: item_cnt,
                                deprt_list: deprt_list,
                                from_type: 1
                            },
                            beforeSend: function () {
                                $("#searchListDataDiv").LoadingOverlay("show", {
                                    background: "rgba(255, 255, 255, 0.7)",
                                    imageColor: '#337AB7'
                                });
                                $('#checkAllListItems').attr('disabled', true);
                            },
                            success: function (result) {
                                var obj = JSON.parse(result);
                                insertFullListItems(obj.result_grn, obj.tax_array, obj.item_uom, list_type);
                            },
                            complete: function () {
                                $("#searchListDataDiv").LoadingOverlay("hide");
                                $('#checkAllListItems').attr('disabled', false);
                                $('.checkListItems').prop('checked', true);
                            },
                            error: function () {
                                toastr.error('Please check your internet connection and try again');
                            }
                        });

                    }
                } else {
                    $('#checkAllListItems').prop('checked', false);
                }
            }
        });
    } else {
        bootbox.confirm({
            message: "Are you sure, you want to unelect all item ?",
            buttons: {
                'confirm': {
                    label: "Unselect All",
                    className: 'btn-warning',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if (result) {
                    unselectAllListItems(2);
                } else {
                    $('#checkAllListItems').prop('checked', true);
                }
            }
        });
    }
}

function unselectAllListItems(from_type) {
    var item_cnt = $('#fulllist_itemcount').val();
    var item_cnt_array = JSON.parse(item_cnt);
    var item_id_array = [];
    var flag = false;

    for (var key in item_array) {
        var obj = item_array[key];
        item_id = parseFloat(obj.item_id);
        flag = inArray(item_id, item_cnt_array);
        if (parseInt(from_type) == 2 && flag) {
            removeSingleRow(key);
            $('.checkListItems').prop('checked', false);
        }
        if (parseInt(from_type) == 1) {
            item_id_array.push(obj.item_id);
        }
    }
    if (parseInt(from_type) == 1) {
        $.each(item_cnt_array, function (index, value) {
            flag = inArray(value, item_id_array);
        });
        if (flag) {
            $('#checkAllListItems').prop('checked', true);
        } else {
            $('#checkAllListItems').prop('checked', false);
        }
    }

}

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) {
            return true;
        }
    }
    return false;
}


function validateNumber(obj) {
    obj.value = obj.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
}

function listDataCalculation(row_id) {
    var item_code = $('#item_code_hidden' + row_id).val();
    if (item_code) {
        validateText(row_id);
        var qty = $('#request_qty' + row_id).val();
        var item_price = $('#request_rate' + row_id).val();
        var discount_amt = 0.0;
        var discount_per = 0.0;
        var without_discount = 0.0;
        if (!item_price) {
            item_price = 0;
            $('#unit_rate' + row_id).val(0);
        }
        var discountAmountType = $('#discount_type' + row_id).val();
        var gst_amt = 0.0;
        var amount = 0.0;
        var tot_rate = qty * item_price;
        var discount_amt = $('#tot_dis_amt' + row_id).val();
        if (!discount_amt || (parseFloat(discount_amt) > parseFloat(tot_rate))) {
            discount_amt = 0;
        }
        gst_amt = $('#tot_dis_amt' + row_id).val();
        if (!gst_amt) {
            gst_amt = 0;
        }
        if (parseInt(discountAmountType) == 1) {
            if (parseFloat(gst_amt) > 100) {
                gst_amt = 0;
                discount_per = 0.0;
                $('#tot_dis_amt' + row_id).val(checkIsNaN(0));
            }
            amount = (gst_amt * tot_rate) / 100;
            if (amount === Infinity) {
                amount = 0;
            }
            discount_amt = amount;
            discount_per = gst_amt;
        } else if (parseInt(discountAmountType) == 2) {
            if (parseFloat(gst_amt) >= parseFloat(tot_rate)) {
                gst_amt = 0;
                $('#tot_dis_amt' + row_id).val(checkIsNaN(0));
            }
            discount_amt = gst_amt;
            if (tot_rate == '' || parseFloat(tot_rate) == 0) {
                amount = 0.0;
            } else {
                amount = (100 * gst_amt) / tot_rate;
                if (amount === Infinity) {
                    amount = 0;
                }
            }
            discount_per = amount;
        }
        var without_discount = parseFloat(tot_rate) - parseFloat(discount_amt);

        gst_amt = $('#request_tax' + row_id).val();
        if (parseFloat(gst_amt) > 100) {
            gst_amt = 0;
            $('#request_tax' + row_id).val('');
        }
        if (!gst_amt) {
            gst_amt = 0;
        }
        amount = (gst_amt * without_discount) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        $('#tot_tax_amt' + row_id).val(checkIsNaN(amount));
        var net_rate = parseFloat(without_discount) + parseFloat(amount);
        $('#request_netamt' + row_id).val(checkIsNaN(net_rate));
        updateItemArray(row_id, net_rate, amount, discount_amt, discount_per);
    } else {
        toastr.warning("Please Seclect any Item");
    }
}


function updateItemArray(row_id, net_rate, total_tax, discount_amt, discount_per) {
    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
    var item_id = $('#item_id_hidden' + row_id).val();
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_desc = $('#item_desc_' + row_id).val();
    var grn_qty = $('#request_qty' + row_id).val();
    var item_rate = $('#request_rate' + row_id).val();
    var discount_type = $('#discount_type' + row_id).val();
    var tot_dis_amt = $('#tot_dis_amt' + row_id).val();
    var tax_type = $('#request_tax' + row_id).val();
    var unit_mrp = $('#request_mrp' + row_id).val();
    var tot_tax_amt = $('#tot_tax_amt' + row_id).val();
    var net_rate = $('#request_netamt' + row_id).val();
    var is_igst = $('#is_igstcheck').is(":checked");
    var tot_rate = grn_qty * item_rate;
    var is_free = 0;

    var sgst_amt = 0;
    var cgst_amt = 0;
    var sgst_per = 0;
    var cgst_per = 0;
    var igst_per = 0;
    var igst_amt = 0;
    if (!is_igst) {
        if (tax_type) {
            var tax_division = $('#request_tax' + row_id + ' option:selected').attr('attt-id');
            sgst_per = checkIsNaN(tax_division);
            cgst_per = checkIsNaN(tax_division);
            sgst_amt = checkIsNaN(total_tax) / 2;
            cgst_amt = checkIsNaN(total_tax) / 2;
        }
    } else {
        if (tax_type) {
            igst_per = tax_type;
            igst_amt = $('#tot_tax_amt' + row_id).val();
        }
    }
    var free_qty = 0;
    var uom_value = 1;

    var item_full_rates = getItemPrice(item_rate, grn_qty, free_qty, uom_value, net_rate, total_tax);
    var unit_rate = item_full_rates.unit_rate;
    var unit_cost = item_full_rates.unit_cost;
    var total_qty = item_full_rates.total_qty;
    var tot_free_qty = item_full_rates.tot_free_qty;
    var all_total_qty = item_full_rates.all_total_qty;
    var unit_cost_with_out_tax = item_full_rates.unit_cost_with_out_tax;
    var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
    var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
    var total_othercharges = 0;
    var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(discount_amt);

    item_array[row_id] = {
        bill_detail_id: bill_detail_id,
        item_id: item_id,
        item_code: item_code,
        item_desc: item_desc,
        grn_qty: grn_qty,
        item_unit: 1,
        unit_cost: unit_cost,
        uom_val: uom_value,
        free_qty: free_qty,
        is_free: is_free ? 1 : 0,
        total_qty: total_qty,
        tot_free_qty: checkIsNaN(tot_free_qty),
        all_total_qty: all_total_qty,
        discount_type: discount_type,
        grn_mrp: 0,
        unit_mrp: unit_mrp,
        item_rate: checkIsNaN(item_rate),
        unit_rate: checkIsNaN(unit_rate),
        cgst_per: checkIsNaN(cgst_per),
        cgst_amt: checkIsNaN(cgst_amt),
        sgst_per: checkIsNaN(sgst_per),
        sgst_amt: checkIsNaN(sgst_amt),
        igst_per: checkIsNaN(igst_per),
        igst_amt: checkIsNaN(igst_amt),
        tot_dis_amt: tot_dis_amt,
        tot_tax_amt: tot_tax_amt,
        tax_type: tax_type,
        total_tax_perc: checkIsNaN(total_tax_perc),
        total_tax_amt: checkIsNaN(total_tax_amt),
        discount_per: checkIsNaN(discount_per),
        discount_amt: checkIsNaN(discount_amt),
        total_othercharges: checkIsNaN(total_othercharges),
        totalRate: checkIsNaN(tot_rate),
        netRate: checkIsNaN(net_amt)
    }
    console.log(item_array);
    getAllItemsTotals();
}



function getItemPrice(rate, grn_qty, free_qty, uom_unit, net_amt, total_tax_amt) {
    var unit_cost = 0;
    var unit_cost_with_out_tax = 0;
    var return_data = {}
    if (!uom_unit) {
        uom_unit = 0;
    }


    var tot_unit_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * uom_unit;
    var unit_rate = 0;
    if (parseInt(uom_unit) != 0) {
        unit_rate = (rate / uom_unit);
    }


    if (!tot_unit_qty || parseFloat(tot_unit_qty) == 0) {
        tot_unit_qty = 0;
    } else {
        unit_cost = (net_amt) / tot_unit_qty;
    }

    if (parseFloat((parseFloat(grn_qty) * parseFloat(uom_unit))) != 0) {
        unit_cost_with_out_tax = (parseFloat(unit_cost)) - ((parseFloat(total_tax_amt) / ((parseFloat(grn_qty)) * parseFloat(uom_unit))));
    }


    var total_qty = parseFloat(grn_qty) * parseFloat(uom_unit);
    var tot_free_qty = parseFloat(free_qty) * parseFloat(uom_unit);
    var all_total_qty = parseFloat(tot_unit_qty);

    return_data = {
        unit_rate: unit_rate,
        unit_cost: unit_cost,
        total_qty: total_qty,
        tot_free_qty: tot_free_qty,
        all_total_qty: all_total_qty,
        unit_cost_with_out_tax: unit_cost_with_out_tax
    }
    return return_data;
}



function getAllItemsTotals() {
    $sgst_amt_total = 0.0;
    $igst_amt_total = 0.0;
    discount_amt_total = 0.0;
    var total_array = [];

    var bill_discount = 0.0;
    var total_gross = 0.0;
    var total_othercharges = 0.0;
    var total_tax = 0.0;

    $.each(item_array, function (index, value) {
        total_tax += parseFloat(value.tot_tax_amt);
        discount_amt_total = parseFloat(discount_amt_total) + parseFloat(value.discount_amt);
        total_gross = parseFloat(total_gross) + parseFloat(value.totalRate);
    });

    var round_off = $('#round_off').val();
    if (!round_off) {
        $('#round_off').val(0);
        round_off = 0.0;
    }

    var total_discount = parseFloat(discount_amt_total);
    var net_amount = (parseFloat(total_gross) + parseFloat(total_tax) + parseFloat(total_othercharges)) - parseFloat(total_discount);

    if ($('#gross_amount_hd').val()) {
        $('#gross_amount_hd').val(checkIsNaN(total_gross));
    }
    if ($('#discount_hd').val()) {
        $('#discount_hd').val(checkIsNaN(total_discount));
    }
    if ($('#tot_tax_amt_hd').val()) {
        $('#tot_tax_amt_hd').val(checkIsNaN(total_tax));
    }
    if ($('#net_amount_hd').val()) {
        $('#net_amount_hd').val(checkIsNaN(net_amount));
    }

    net_amount = (parseFloat(net_amount) + parseFloat(round_off));
    total_array['total_gross'] = checkIsNaN(total_gross);
    total_array['total_dis'] = checkIsNaN(total_discount);
    total_array['total_tax'] = checkIsNaN(total_tax);
    total_array['total_net'] = checkIsNaN(net_amount);
    total_array['bill_discount'] = checkIsNaN(bill_discount);
    return total_array;
}



function calculateListNetTotal(tot_rate, tax_array) {

    var tot_amt = 0.0;
    detailtax_array = [];
    detailtax_array['cgst_per'] = 0.0;
    detailtax_array['cgst_amt'] = 0.00;
    detailtax_array['sgst_per'] = 0.0;
    detailtax_array['sgst_amt'] = 0.00;
    detailtax_array['igst_per'] = 0.0;
    detailtax_array['igst_amt'] = 0.00;
    detailtax_array['discount_per'] = 0.00;
    detailtax_array['discount_amt'] = 0.00;
    detailtax_array['Freight_Charges_per'] = 0.0;
    detailtax_array['flightcharge_amt'] = 0.00;
    detailtax_array['other_charges_per'] = 0.0;
    detailtax_array['othercharge_amt'] = 0.00;

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        if (parseInt(index) == 1) {
            detailtax_array['discount_per'] = value;
            detailtax_array['discount_amt'] = amount;
            tot_amt -= parseFloat(amount);
            tot_rate = parseFloat(tot_rate) - parseFloat(amount);
        }
        if (parseInt(index) == 32) {
            tot_amt -= parseFloat(amount);
            tot_rate = parseFloat(tot_rate) - parseFloat(amount);
        }

        if (parseInt(index) == 10) {
            detailtax_array['cgst_per'] = value;
            detailtax_array['cgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 9) {
            detailtax_array['sgst_per'] = value;
            detailtax_array['sgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 12) {
            detailtax_array['igst_per'] = value;
            detailtax_array['igst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }

        if (parseInt(index) == 11) {
            detailtax_array['Freight_Charges_per'] = value;
            detailtax_array['flightcharge_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 3) {
            detailtax_array['other_charges_per'] = value;
            detailtax_array['othercharge_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
    });
    return detailtax_array;
}


function getGlobalItems(row_id, from_date = 1) {
    if (!row_id) {
        row_id = $('#without_batch_no').val();
    } else {
        $('#without_batch_no').val(row_id);
    }

    var url = base_url + "/purchase/getGlobalItems";
    var batch_status = $('#without_batch_no').is(":checked");
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_desc = $('#item_desc_' + row_id).val();
    var param = {
        _token: token,
        item_code: item_code,
        batch_status: batch_status
    };
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            $("#getQuotationItemListModelDiv").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $('#getQuotationItemListModelHeader').html(item_desc);
            $('#getGlobalItemsbtn' + row_id).attr('disabled', true);
            $('#getGlobalItemsspin' + row_id).removeClass('fa fa-globe');
            $('#getGlobalItemsspin' + row_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (res) {
            $("#getQuotationItemListModelDiv").html(res);
            if (from_date == 1) {
                $("#getQuotationItemListModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#getGlobalItemsbtn' + row_id).attr('disabled', false);
            $('#getGlobalItemsspin' + row_id).removeClass('fa fa-spinner fa-spin');
            $('#getGlobalItemsspin' + row_id).addClass('fa fa-globe');
            $("#getQuotationItemListModelDiv").LoadingOverlay("hide");
        }
    });
}

function getIemsHistory(row_id, item_code = '') {
    var url = base_url + "/purchase/getLastAddedPODetalis";
    if (!item_code) {
        item_code = $('#item_code_hidden' + row_id).val();
    }
    var item_desc = $('#item_desc_' + row_id).val();
    var param = {
        _token: token,
        item_code: item_code
    };
    $.ajax({
        url: url,
        type: "POST",
        data: param,
        beforeSend: function () {
            $("#poplast_item_box_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
            $('#poItemHistoryModelHeader').html(item_desc);
            $('#ItemHistoryBtn' + row_id).attr('disabled', true);
            $('#ItemHistoryBtnspin' + row_id).removeClass('fa fa-history');
            $('#ItemHistoryBtnspin' + row_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if ($("#po_requisitionli").hasClass("active"))
                $('#poplast_item_box_div_req').html(data).show();
            else
                $('#poplast_item_box_div').html(data).show();

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#ItemHistoryBtn' + row_id).attr('disabled', false);
            $('#ItemHistoryBtnspin' + row_id).removeClass('fa fa-spinner fa-spin');
            $('#ItemHistoryBtnspin' + row_id).addClass('fa fa-history');
            $("#poplast_item_box_div").LoadingOverlay("hide");
        }
    });
}

function addProductListItems(item_id, list_type, row_id = 0) {
    var checkListItems = $('#checkListItems' + item_id).is(":checked");
    if (checkListItems) {
        $('#checkListItems' + item_id).prop('checked', false);
    } else {
        $('#checkListItems' + item_id).prop('checked', true);
    }
    addCategoryListItem(item_id, list_type, row_id, 1);
}


function addCategoryListItem(item_id, list_type, row_id = 0, from_type = 1) {
    var checkListItems = $('#checkListItems' + item_id).is(":checked");
    if (checkListItems || list_type == 'new') {
        var single_item_array = [];
        single_item_array.push(item_id);
        var url = base_url + "/purchase/selectAllListedData";
        var item_cnt = JSON.stringify(single_item_array);
        var item_array_string = JSON.stringify(item_array);
        var deprt_list = $('#deprt_list').val();
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                list_type: list_type,
                deprt_list: deprt_list,
                item_array_string: item_array_string,
                item_cnt: item_cnt,
                from_type: from_type
            },
            beforeSend: function () {
                if (list_type != 'new') {
                    $("#searchListDataDiv").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                } else {
                    $("#added_new_list_table_product").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                }
            },
            success: function (result) {
                var obj = JSON.parse(result);
                insertFullListItems(obj.result_grn, obj.tax_array, obj.item_uom, list_type, row_id);
            },
            complete: function () {
                if (list_type != 'new') {
                    $("#searchListDataDiv").LoadingOverlay("hide");
                    unselectAllListItems(1);
                } else {
                    $("#added_new_list_table_product").LoadingOverlay("hide");
                }
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        removeSelectedListItems(item_id);
    }
}


function removeSelectedListItems(item_id) {
    var row_id = '';
    for (var key in item_array) {
        var obj = item_array[key];
        item_id_get = obj.item_id;
        if (item_id_get == item_id) {
            row_id = key;
        }
    }
    if (row_id) {
        removeSingleRow(row_id);
    }
}


function addListItem(head_id, item_id, item_code, list_type) {
    var url = base_url + "/purchase/getPoItemsFromList";
    var rowdata_id = $('#list_added_check' + list_type + item_id).val();
    if (parseInt(rowdata_id) != 0) {
        delete item_array[row_id];
        $('#addNewRow' + list_type + item_id).removeClass('info');
        $('#list_added_check' + list_type + item_id).val(0);
        $("#row_data_" + row_id).remove();
    } else {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                list_type: list_type,
                head_id: head_id,
                item_code: item_code,
                item_id: item_id
            },
            beforeSend: function () {
                $('#addNewRow' + list_type + item_id).addClass('info');
                $("#getQuotationItemListModelDiv").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (result) {
                var obj = JSON.parse(result);
                insertListItems(item_id, item_code, obj.result_grn, obj.tax_array, list_type, rowdata_id);
            },
            complete: function () {
                $("#getQuotationItemListModelDiv").LoadingOverlay("hide");
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    }
    row_ct = 1;
    $(".row_class").each(function (i) {
        $(this).find('.row_count_class').text(row_ct);
        row_ct++;
    });
    getAllItemsTotals();
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
}

function fillcontract_no(id, contract_no) {
    vendor_name = htmlDecode(contract_no);
    $('#searchContract').val(contract_no);
    $('#contract_id_hidden').val(id);
    $('#ajaxContractSearchBox').hide();
}

function resetheadScrollData() {
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30

    });
    setTimeout(function () {
        $('.theadfix_wrapper').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    }, 1000);
}

function lastClicKItemRow(row_id) {
    var item_code = item_array[row_id] ? item_array[row_id].item_code : '';
    $('#lastClickedItemCode').val(item_code);
    $('#lastClickedRowID').val(row_id);
}

function close_last_itemlist() {
    $('#poplast_item_box_div').hide();
    $('#poplast_item_box_div_req').hide();
}



function createPoSendEmailUsers() {
    $("#sendEmailCCModel").modal({
        backdrop: 'static',
        keyboard: false
    });
    manageEmailCCUsers('', '');
}

function searchEmailUsers(event, email_count) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchEmailUsers' + email_count);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var email_users = $('#cc_user_name' + email_count).val();
        console.log(email_users);
        if (email_users == "") {
            $('#ajaxSearchEmailUsers' + email_count).hide();
        } else {
            var url = base_url + "/purchase/search_emailusers";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    email_users: email_users,
                    email_count: email_count
                },
                beforeSend: function () {
                    $('#ajaxSearchEmailUsers' + email_count).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (parseInt(html) == 0) {
                        $('#ajaxSearchEmailUsers' + email_count).hide();
                    } else {
                        $('#ajaxSearchEmailUsers' + email_count).html(html).show();
                        $('#ajaxSearchEmailUsers' + email_count).find('li').first().addClass('liHover');
                    }

                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxSearchEmailUsers' + email_count, event);
    }
}

function fillEmailUsers(user_name, email, email_count) {
    $('#cc_user_name' + email_count).val(user_name);
    $('#cc_user_email' + email_count).val(email);
    $('#ajaxSearchEmailUsers' + email_count).hide();
    manageEmailCCUsers('', '');
}

var email_cc_users = 1;

function manageEmailCCUsers(user_name, email) {
    if ($(".row_class_email:last").find("input[name='cc_user_name']").val() != '') {
        email_body = "<tr class='row_class_email' style='background: #FFF;' id='email_cc_rows" + email_cc_users + "'>";
        email_body += "<td><input autocomplete='off' value='" + user_name + "' onkeyup='searchEmailUsers(event," + email_cc_users + ")'  class='form-control' name='cc_user_name' id='cc_user_name" + email_cc_users + "' type='text'>";
        email_body += "<div id='ajaxSearchEmailUsers" + email_cc_users + "' class='ajaxSearchBox' style='margin-top: -30px; width: 100%; z-index: 9999;'></div></td>";
        email_body += "<td> <input autocomplete='off' class='form-control' value='" + email + "' name='cc_user_email' id='cc_user_email" + email_cc_users + "' type='text'></td>";
        email_body += "<td style='text-align: center'> <i style='padding: 5px 8px; font-size: 15px;' onclick='removeUserEmailRow(" + email_cc_users + ")' class='fa fa-trash text-red'></i></td>";
        email_body += "</tr>";
        $('#addUsersToEmail').append(email_body);
        $('#cc_user_name' + email_cc_users).focus();
        email_cc_users++;
    }
}

function removeUserEmailRow(email_count) {
    $('#email_cc_rows' + email_count).remove();
}

function validateEmail(value) {
    var input = document.createElement('input');

    input.type = 'email';
    input.required = true;
    input.value = value;

    return typeof input.checkValidity === 'function' ? input.checkValidity() : /\S+@\S+\.\S+/.test(value);
}

function addUserEmailCC() {
    cc_email_users = new Array();
    var email_validate = true;
    $('.form-control').removeClass('bg-red');
    $('#addUsersToEmail tr').each(function () {
        var user_name = $(this).find("input[name='cc_user_name']").val();
        var user_email = $(this).find("input[name='cc_user_email']").val();
        if (user_name && !user_email) {
            $(this).find("input[name='cc_user_name']").addClass('bg-red');
            email_validate = false;
            return false;
        } else if (user_email) {
            email_validate = validateEmail(user_email);
            if (email_validate) {
                cc_email_users.push({
                    'user_name': user_name,
                    'user_email': user_email
                });
            } else {
                $(this).find("input[name='cc_user_email']").addClass('bg-red');
                email_validate = false;
                return false;
            }
        }
    });
    if (email_validate) {
        $("#sendEmailCCModel").modal('toggle');
    }
}

function getPrintPreviewNew() {
    var vendor_id = $('#vendoritem_id_hidden').val();
    var po_number = $('#po_number_show').val();
    var deprt_list = $('#deprt_list').val();
    var po_date = $('#po_date').val();
    var manufacturer_code = $('#manufacturer_list').val();
    var remarks = $('#payment_remarks').val();
    var po_id = $('#po_id').val();
    var round_off = $('#round_off').val();
    if (vendor_id != '0') {
        var url = base_url + "/frame/printPO";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                round_off: round_off,
                po_id: po_id,
                vendor_id: vendor_id,
                po_number: po_number,
                deprt_list: deprt_list,
                po_date: po_date,
                remarks: remarks,
                manufacturer_code: manufacturer_code
            },
            beforeSend: function () {
                $('#print_preview_btn').attr('disabled', true);
                $('#print_preview_spin').removeClass('fa fa-eye');
                $('#print_preview_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                $('#print_po_modelDiv').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                $("#print_po_model").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#print_preview_btn').attr('disabled', false);
                $('#print_preview_spin').removeClass('fa fa-spinner fa-spin');
                $('#print_preview_spin').addClass('fa fa-eye');
            }
        });
    } else {
        toastr.warning("Please Select Any Supplier");
        $('#searchnewvendor').focus();
    }
}
