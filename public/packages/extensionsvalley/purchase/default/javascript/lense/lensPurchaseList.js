$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    setTimeout(function () {

        searchLensePo();
        $('.ajaxSearchBox').hide();

    }, 800);
    $(".select2").select2();
});
var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();


$('#po_number').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('po_idAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var po_no = $(this).val();
        if (!po_no) {
            $("#po_idAjaxDiv").html("").hide();
            $('#po_id_hidden').val('');
        } else {
            var url = base_url + "/lense/po_number_search";
            var param = {
                _token: token,
                po_no: po_no
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#po_idAjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#po_idAjaxDiv").html(html).show();
                    $("#po_idAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('po_idAjaxDiv', event);
    }
});


$('#supplier_number').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('supplier_idAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $(this).val();
        if (!vendor_name) {
            $("#supplier_idAjaxDiv").html("").hide();
            $('#supplier_id_hidden').val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            var param = {
                vendor_name: vendor_name
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#supplier_idAjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#supplier_idAjaxDiv").html(html).show();
                    $("#supplier_idAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('supplier_idAjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function removeAllWhiteSpace(s) {
    s = s.replace(/(\d)[\s.]+(?=\d)/g, '$1');
    s = s.replace(/(?<=\d)[\s.]+(?=\d)/g, '');
    return s;
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#supplier_number').val(vendor_name);
    $('#supplier_id_hidden').val(vendor_code);
    $('#supplier_idAjaxDiv').hide();
    searchLensePo();
}

function fillPoNumber(id, po_number) {
    po_number = htmlDecode(po_number);
    $('#po_number').val(po_number);
    $('#po_id_hidden').val(id);
    $('#po_idAjaxDiv').hide();
    searchLensePo();
}

function searchLensePo() {
    var url = base_url + "/lense/searchLenseList";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            vendor: vendor,
            location: location,
            po_dated: po_dated,
            from_date: from_date,
            to_date: to_date,
            po_status: po_status
        },
        beforeSend: function () {
            $('#searchQuotationBtn').attr('disabled', true);
            $('#searchQuotationSpin').removeClass('fa fa-search');
            $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchQuotationBtn').attr('disabled', false);
            $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchQuotationSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function addWindowLoad(to_url, po_id, copy) {
    var url = base_url + "/lense/" + to_url + '/' + po_id + '/' + copy;
    document.location.href = url;
}

function addWindowLoadNew(to_url) {
    var url = base_url + "/lense/" + to_url ;
    document.location.href = url;
}


function downloadPoListExcel() {
    var url = base_url + "/purchase/downloadPoListExcel";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            vendor: vendor,
            location: location,
            po_dated: po_dated,
            from_date: from_date,
            to_date: to_date,
            po_status: po_status
        },
        beforeSend: function () {
            $('#downloadPoListExcelBtn').attr('disabled', true);
            $('#downloadPoListExcelSpin').removeClass('fa fa-file-excel-o');
            $('#downloadPoListExcelSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            if (html.trim() != 'NODATA') {
                html = removeAllWhiteSpace(html)
                var download_file = base_url + '/packages/uploads/Purchase/' + html;
                $('#downloadfile').attr('href', download_file);
                $('#downloadfile')[0].click();
            } else {
                toastr.warning("No records found");
            }
        },
        complete: function () {
            $('#downloadPoListExcelBtn').attr('disabled', false);
            $('#downloadPoListExcelSpin').removeClass('fa fa-spinner fa-spin');
            $('#downloadPoListExcelSpin').addClass('fa fa-file-excel-o');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}
