var url = $("#ret_base_url").val();
var approve_status_check = $('#approve_status_check').val();
var decimalConfiguration = $('#decimal_configuration').val();
var is_edit = 0;
$('#department').on('select2:select', function (evt) {
    if ($('select').val() == '') {
        add_purchase_return_row(1);
    }
});

$(document).ready(function () {
    if (parseInt(approve_status_check) == 1 || parseInt(approve_status_check) == 2) {
        getListItemsCount('purchase_return_tbl_body_edit');
    }
    updateRoundOff();
})
$('#to_expiry_date').datetimepicker({
    format: "DD-MMM-YYYY",
});
$('body').on('click', '.return_items_pages a', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var page = GetUrlParameter(url, 'page');
    vendor_return_items(page);
});
if (window.location.href.indexOf("return_items_to_vendor_edit") > -1) {
    add_purchase_return_row_edit();
}
return_item_array = {};

function reqSearch() {
    var makeSearchUrl = '';
    $('#form').submit();
}
$(document).on('click', '.smooth_scroll', function (event) {
    event.preventDefault();
    $('.combined_theadscroll').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});
$(".selectsearch").select2();

function clear_search() {
    var cur_url = window.location.href.split('?')[0];
    window.location.replace(cur_url);
}

function add_return_to_vendor() {
    is_edit = 0;
    window.location = url + "/purchase/return_items_to_vendor_add";
}

function add_purchase_return_row(in_id = 0) {
    if (in_id == 0) {
        var k = 0;
        if ($("#department").val() == '') {
            toastr.warning("Please Select Return Store");
            return;
        }
        if ($("#vendor").val() == '') {
            toastr.warning("Please Select Vendor");
            return;
        }
    }

    response = '<tr><td ><input type="text" value="" name="item_desc[]" autofocus autocomplete="off" class="form-control return_item_desc" onkeyup="select_item_desc(this.id,event)">\n\
                        <div class="ajaxSearchBox search_return_item_box" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; \n\
                        margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;\n\
                        border: 1px solid rgba(0, 0, 0, 0.3);"> </div></td>\n\
                                            <td><input type="text" value="" name="item_code[]" class="form-control"> </td>\n\
                                            <td ><i class="fa fa-info batch_wise_info_class" style="cursor:pointer" onclick="select_batch_wise_item(this)"></i></td>\n\
                                            <td ><input type="text" value="" name="quantity[]" autocomplete="off" readonly="" class="form-control"></td>\n\
                                            <td ><input type="text" value="" name="remarks[]" class="form-control"> </td>\n\
                                            <td ><input type="text" value="" name="net_tax[]" readonly=""  class="form-control item_code_net_tax"> </td>\n\
                                            <td ><input type="text" value="" name="net_amount[]" readonly=""  class="form-control item_code_net_amount"> </td>\n\
                                            <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';
    $('#purchase_return_tbl_body').append(response);
    var divElements = document.querySelectorAll('.search_return_item_box');
    var descElements = document.querySelectorAll('.return_item_desc');
    var batch_class = document.querySelectorAll('.batch_wise_info_class');
    for (var i = 0; i < divElements.length; i++) {
        divElements[i].id = 'search_return_item_box-' + i;
        descElements[i].id = 'return_item_desc_box-' + i;
        batch_class[i].id = 'batch_wise_info_btn-' + i;
    }
    setTimeout(function () {
        var fcs_id = i - 1;
        document.getElementById("return_item_desc_box-" + fcs_id).focus();
    }, 2000);
    getListItemsCount('purchase_return_tbl_body');
}

$("#purchase_return_tbl_body").on('click', '.delete_return_entry_added', function () {
    if (parseInt(approve_status_check) != 1 && parseInt(approve_status_check) != 2) {
        $(this).closest('tr').remove();
        var tr_id = $(this).closest('tr').attr('item-code');
        delete return_item_array[tr_id];
    }
    getListItemsCount('purchase_return_tbl_body');
});



$("#purchase_return_tbl_body_edit").on('click', '.delete_return_entry_added', function () {
    if (parseInt(approve_status_check) != 1 && parseInt(approve_status_check) != 2) {
        $(this).closest('tr').remove();
        var tr_id = $(this).closest('tr').attr('item-code');
        delete return_item_array[tr_id];
        getListItemsCount('purchase_return_tbl_body_edit');
    }
});


function select_item_desc(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
    var ajax_div = 'search_return_item_box-' + id;
    var vendor = $("#vendor").val();

    if (value.match(keycheck)) {
        var item_desc = $('#return_item_desc_box-' + id).val();
        if (item_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: url + "/purchase/return_item_search",
                data: 'item_desc=' + item_desc + '&search_item_desc=1&vendor=' + vendor,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function fill_desc_details(e, desc, item_code) {
    if ($('#tr_' + item_code).length) {
        toastr.warning("Item Alredy Selected");
        $(e).closest("div").hide();
        return;
    }
    $(e).closest("tr").find("input[name='item_code[]']").val(item_code);
    $(e).closest("tr").attr("item-code", item_code);
    $(e).closest("tr").find("input[name='item_desc[]']").val(desc);
    $(e).closest("tr").find("input[name='quantity[]']").attr("id", 'item_code_qty_' + item_code);
    $(e).closest("tr").find("input[name='net_tax[]']").attr("id", 'item_code_net_tax_' + item_code);
    $(e).closest("tr").find("input[name='net_amount[]']").attr("id", 'item_code_net_amount_' + item_code);
    $(e).closest("tr").find('.batch_wise_info_class').trigger('click');
    $(e).closest("tr").attr("id", "tr_" + item_code);
    $(e).closest("div").hide();

}

function select_batch_wise_item(e) {
    var item_code = $(e).closest("tr").find("input[name='item_code[]']").val();
    var item_name = $(e).closest("tr").find("input[name='item_desc[]']").val();
    var vendor = $("#vendor").val();
    var department = $("#department").val();
    $.ajax({
        type: "GET",
        url: "",
        data: 'item_code=' + item_code + '&select_vendor= ' + vendor + '&select_batch=1&department=' + department + '&item_return_array=' + JSON.stringify(return_item_array),
        beforeSend: function () {
            $(e).closest('i').removeClass('fa fa-info');
            $(e).closest('i').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            $("#itemts_batchwise_div").html(html);
            $("#item_return_batch_model").modal('show');
            $("#modal_item_code").html(item_name);
            setTimeout(function () {
                $("#batch_bill_pop_0").focus();
            }, 300);
        },
        complete: function () {
            $(e).closest('i').removeClass('fa fa-spinner fa-spin');
            $(e).closest('i').addClass('fa fa-info');
            $("#item_code_hidden").val(item_code);
            $('.return_invoice_date').datetimepicker({
                format: "DD-MMM-YYYY",
            });
            $('.return_expiry_date').datetimepicker({
                format: "DD-MMM-YYYY",
            });

        }
    });
}

function changeReturnQty(row_id, item_code) {
    var rowId = row_id;
    var itemCode = item_code;
    var ret_qty = parseFloat($('#ret_item_qty' + rowId).val());
    var rej_qty = parseFloat($('#rejected_qty' + rowId).val());
    if (isNaN(rej_qty)) {
        rej_qty = 0;
    }
    if (isNaN(ret_qty)) {
        ret_qty = 0;
    }
    var ret_qty = ret_qty - rej_qty;
    if (isNaN(ret_qty)) {
        ret_qty = 0;
    }
    $('#ret_item_qty' + rowId).val(ret_qty);
}

function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    return number.toFixed(parseInt(decimalConfiguration));
}

function return_qty_calculate(row_id, item_code) {
    var rowId = row_id;
    var itemCode = item_code;
    var unit = $('#item_unit_batch' + rowId).val();
    var ret_qty = parseFloat($('#ret_item_qty' + rowId).val());
    var rej_qty = parseFloat($('#rejected_qty' + rowId).val());
    if (isNaN(rej_qty)) {
        rej_qty = 0;
    }
    if (isNaN(ret_qty)) {
        ret_qty = 0;
    }
    var discount_perc = parseFloat($('#discount_perc' + rowId).val());
    if (isNaN(discount_perc)) {
        discount_perc = 0;
    }
    var cur_stock = parseFloat($('#return_stock' + rowId).text());
    if (isNaN(cur_stock)) {
        cur_stock = 0;
    }

    var convertion_data = $('#item_unit_hidden').val();
    var convertion_data_new = JSON.parse(convertion_data);
    var conversion_detail = convertion_data_new[itemCode];
    var convertion_factor = 1; //parseFloat(conversion_detail[unit]);
    if (isNaN(convertion_factor)) {
        convertion_factor = 0;
    }
    var conv_value = convertion_factor * ret_qty;
    var act_conv_value = convertion_factor * ret_qty;
    var purchase_rate = parseFloat($('#actual_purchase_rate' + rowId).val());
    var purchase_cost = parseFloat($('#purchase_cost' + rowId).val());

    if (isNaN(purchase_rate)) {
        purchase_rate = 0;
    }
    var tax_rate = parseFloat($('#return_tax_perce' + rowId).val());
    if (isNaN(tax_rate)) {
        tax_rate = 0;
    }
    if (ret_qty == 0) {
        toastr.error('Please enter return quantity!');
        $('#ret_item_qty' + rowId).val(0);
    }

    if (ret_qty < 0) {
        toastr.error('Return quantity is less than zero!');
        $('#ret_item_qty' + rowId).val(0);
    }
    if ((conv_value > act_conv_value)) {
        toastr.error('return qty greater than actual return quantity!');
        $('#ret_item_qty' + rowId).val(ret_qty);
    } else {
        var net_amount = purchase_cost * conv_value;
        var net_amount_without_discount = (net_amount / (100 + discount_perc)) * 100;
        var discount_amount = net_amount - net_amount_without_discount;
        if (isNaN(discount_amount)) {
            discount_amount = 0;
        }
        var net_amount_without_tax = (net_amount / (100 + tax_rate)) * 100;
        var tax_amount = net_amount - net_amount_without_tax;
        var total_discount_curr = 0;
        if (100 - discount_perc == 0) {
            total_discount_curr = net_amount;
        } else {
            var total_item_rate_curr = (net_amount * (100 * 100)) / ((100 - discount_perc) * (100 + tax_rate));
            total_discount_curr = total_item_rate_curr * (discount_perc / 100);
        }
        $('#discount_amt' + rowId).val(checkIsNaN(total_discount_curr));
        $('#return_net_amount' + rowId).val(checkIsNaN(net_amount));
        $('#return_net_tax' + rowId).val(checkIsNaN(tax_amount));
    }
}


function save_return_list(arry_cnt) {
    if (arry_cnt != '0') {
        var ret_item_qty = '';
        var return_batch = '';
        var return_expirydate = '';
        var return_bill_no = '';
        var return_invoice_date = '';
        var return_stock = '';
        var print_batch = '';
        var rejected_qty = '';
        var return_item_unit_batch = '';
        var return_purchase_cost = '';
        var return_discount_perc = '';
        var return_discount_amt = '';
        var return_tax_perc = '';
        var return_mrp = '';
        var return_net_amount = '';
        var return_net_tax = '';
        var actual_purchase_rate = '';
        var total_net_amount = 0.00;
        var total_net_tax = 0.00;
        var item_code = $('#item_code_hidden').val();
        var total_qty = 0;
        var return_purchase_rate = '';
        var edit_values = '';
        var round_off = $('#round_off').val();
        if (!round_off) {
            round_off = 0;
        } else {
            round_off = checkIsNaN(round_off);
        }
        $('#round_off').val(round_off);

        return_item_array[item_code] = {};
        return_selected_item_list = new Array();
        if (is_edit == 1) {
            var tr_id = "item_return_tbody_edit";
        } else {
            var tr_id = "item_return_tbody";
        }
        $('#' + tr_id + ' tr').each(function () {
            if ($.type($(this).find('input[name="ret_item_qty[]"]').val().trim()) != "undefined") {
                ret_item_qty = $(this).find('input[name="ret_item_qty[]"]').val().trim();
            }


            if ($.type($(this).find('td[class="return_batch"]').html().trim()) != "undefined") {
                return_batch = $(this).find('td[class="return_batch"]').html().trim();
            }
            if ($.type($(this).find('input[name="edit_value[]"]').val()) != "undefined") {
                edit_values = $(this).find('input[name="edit_value[]"]').val().trim();
            }

            if ($.type($(this).find('input[name="return_expiry_date[]"]').val().trim()) != "undefined") {
                return_expirydate = $(this).find('input[name="return_expiry_date[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="return_bill_no[]"]').val().trim()) != "undefined") {
                return_bill_no = $(this).find('input[name="return_bill_no[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="return_invoice_date[]"]').val().trim()) != "undefined") {
                return_invoice_date = $(this).find('input[name="return_invoice_date[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="print_batch[]"]').val().trim()) != "undefined") {
                print_batch = $(this).find('input[name="print_batch[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="rejected_qty[]"]').val()) != "undefined") {
                rejected_qty = $(this).find('input[name="rejected_qty[]"]').val().trim();
            }
            if ($.type($(this).find('td[class="return_stock"]').html().trim()) != "undefined") {
                return_stock = $(this).find('td[class="return_stock"]').html().trim();
            }
            if ($.type($(this).find('select[name="item_unit_batch[]"]').val().trim()) != "undefined") {
                return_item_unit_batch = $(this).find('select[name="item_unit_batch[]"]').val().trim();
            }

            if ($.type($(this).find('input[name="purchase_cost[]"]').val().trim()) != "undefined") {
                return_purchase_cost = $(this).find('input[name="purchase_cost[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="discount_perc[]"]').val()) != "undefined") {
                return_discount_perc = $(this).find('input[name="discount_perc[]"]').val();
            }
            if ($.type($(this).find('input[name="discount_amt[]"]').val()) != "undefined") {
                return_discount_amt = $(this).find('input[name="discount_amt[]"]').val();
            }

            if ($.type($(this).find('input[name="return_tax_perce[]"]').val()) != "undefined") {
                return_tax_perc = $(this).find('input[name="return_tax_perce[]"]').val().trim();
            }

            if ($.type($(this).find('td[class="return_mrp"]').html().trim()) != "undefined") {
                return_mrp = $(this).find('td[class="return_mrp"]').html().trim();
            }
            if ($.type($(this).find('input[name="return_net_amount[]"]').val().trim()) != "undefined") {
                return_net_amount = $(this).find('input[name="return_net_amount[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="return_net_tax[]"]').val().trim()) != "undefined") {
                return_net_tax = $(this).find('input[name="return_net_tax[]"]').val().trim();
            }

            if ($.type($(this).find('input[name="actual_purchase_rate[]"]').val().trim()) != "undefined") {
                actual_purchase_rate = $(this).find('input[name="actual_purchase_rate[]"]').val().trim();
            }
            if ($.type($(this).find('input[name="return_purchase_rate_data[]"]').val()) != "undefined") {
                return_purchase_rate = $(this).find('input[name="return_purchase_rate_data[]"]').val();
            }


            if (isNaN(return_net_amount) || return_net_amount == '') {
                return_net_amount = 0;
            }
            if (isNaN(return_net_tax) || return_net_tax == '') {
                return_net_tax = 0;
            }
            if (isNaN(ret_item_qty) || ret_item_qty == '') {
                ret_item_qty = 0;
            }
            if (isNaN(rejected_qty) || rejected_qty == '') {
                rejected_qty = 0;
            }
            total_net_amount = parseFloat(total_net_amount) + parseFloat(return_net_amount);
            total_net_tax = parseFloat(total_net_tax) + parseFloat(return_net_tax);
            total_qty = parseFloat(total_qty) + parseFloat(ret_item_qty);
            return_selected_item_list.push({
                'return_qty': ret_item_qty,
                'return_batch': return_batch,
                'return_expirydate': return_expirydate,
                'return_bill_no': return_bill_no,
                'return_invoice_date': return_invoice_date,
                'print_batch': print_batch,
                'rejected_qty': rejected_qty,
                'return_stock': return_stock,
                'return_item_unit_batch': return_item_unit_batch,
                'return_purchase_cost': return_purchase_cost,
                'return_discount_perc': return_discount_perc,
                'return_discount_amt': return_discount_amt,
                'return_tax_perc': return_tax_perc,
                'return_mrp': return_mrp,
                'return_net_amount': return_net_amount,
                'return_net_tax': return_net_tax,
                'actual_purchase_rate': actual_purchase_rate,
                'return_purchase_rate': return_purchase_rate,
                'edit_values': edit_values
            });
        });

        if (total_qty > 0) {
            var all_total_net_tax = 0;
            var all_total_net_amount = 0;
            $('#item_code_net_amount_' + item_code).val(checkIsNaN(total_net_amount));
            $('#item_code_net_tax_' + item_code).val(checkIsNaN(total_net_tax));
            $('#item_code_qty_' + item_code).val(total_qty);
            return_item_array[item_code] = return_selected_item_list;
            var array_length = Object.keys(return_item_array).length;
            $(".item_code_net_tax").each(function () {
                if (isNaN(this.value) || this.value == '') {
                    this.value = 0;
                }
                all_total_net_tax = parseFloat(all_total_net_tax) + parseFloat(this.value);
            });
            $(".item_code_net_amount").each(function () {
                if (isNaN(this.value) || this.value == '') {
                    this.value = 0;
                }
                all_total_net_amount = parseFloat(all_total_net_amount) + parseFloat(this.value);
            });
            $("#net_total_tax").val(checkIsNaN(all_total_net_tax));
            all_total_net_amount = parseFloat(all_total_net_amount) + parseFloat(round_off);
            $("#net_amount").val(checkIsNaN(all_total_net_amount));

        } else {
            toastr.warning("Please Enter Quantity");
            return;
        }
    }
    if (is_edit == 1) {
        var chk_new_row_create_edit = 0;
        $('#purchase_return_tbl_body_edit tr').each(function () {
            if ($(this).find('input[name="item_desc[]"]').val() == '') {
                chk_new_row_create_edit = 1;
            }
        });
        if (chk_new_row_create_edit == 0) {
            add_purchase_return_row_edit();
        }
        $('#item_return_batch_model_edit').modal('hide');
        getListItemsCount('purchase_return_tbl_body_edit');
    } else {
        var chk_new_row_create = 0;
        $('#purchase_return_tbl_body tr').each(function () {
            if ($(this).find('input[name="item_desc[]"]').val() == '') {
                chk_new_row_create = 1;
            }
        });
        if (chk_new_row_create == 0) {
            add_purchase_return_row();
        }
        getListItemsCount('purchase_return_tbl_body');
    }
    $('#item_return_batch_model').modal('hide');

}

function updateRoundOff() {
    var round_off = $('#round_off').val();
    var net_amount = 0;
    $(".item_code_net_amount").each(function () {
        if (isNaN(this.value) || this.value == '') {
            this.value = 0;
        }
        net_amount = parseFloat(net_amount) + parseFloat(this.value);
    });

    if (!round_off) {
        round_off = 0;
    } else {
        round_off = checkIsNaN(round_off);
    }
    net_amount = checkIsNaN(net_amount);
    $('#round_off').val(checkIsNaN(round_off));
    re_net_amount = parseFloat(net_amount) + parseFloat(round_off);
    $('#net_amount').val(checkIsNaN(re_net_amount));
}

function save_all_data(post_type, from_edit = 0, is_print = 0) {
    var list_cnt = [];
    if (parseInt(from_edit) == 0) {
        list_cnt = getListItemsCount('purchase_return_tbl_body');
    } else {
        list_cnt = getListItemsCount('purchase_return_tbl_body_edit');
    }
    var vendor_id = $('#vendor').val();
    var from_depart = $('#department').val();
    if (vendor_id) {
        if (from_depart) {
            if (parseInt(list_cnt) != 0) {
                var save_string = "Save";
                if ((parseInt(post_type) == 1) && parseInt(is_print) == 0) {
                    save_string = "Approve";
                } else if ((parseInt(post_type) == 1) && parseInt(is_print) == 1) {
                    save_string = "Approve and Print";
                } else if ((parseInt(post_type) == 0) && parseInt(is_print) == 0) {
                    save_string = "Save";
                } else if ((parseInt(post_type) == 0) && parseInt(is_print) == 1) {
                    save_string = "Save and Print";
                }

                bootbox.confirm({
                    message: "Are you sure you want to " + save_string + " ?",
                    buttons: {
                        'confirm': {
                            label: save_string,
                            className: 'btn-success',
                            default: 'true'
                        },
                        'cancel': {
                            label: 'Cancel',
                            className: 'btn-warning'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            var urls = "";
                            var ret_quantity_head = 0;
                            if (is_edit == 1 || from_edit == 1) {
                                urls = url + "/purchase/edit_purchase_return_save";
                                $('#purchase_return_tbl_body_edit tr').each(function () {
                                    if ($(this).find('input[name="quantity[]"]').val() == '') {
                                        ret_quantity_head += ret_quantity_head;
                                    }
                                });
                            } else {
                                urls = url + "/purchase/add_purchase_return";
                                $('#purchase_return_tbl_body tr').each(function () {
                                    if ($(this).find('input[name="quantity[]"]').val() == '') {
                                        ret_quantity_head += ret_quantity_head;
                                    }
                                });
                            }

                            var purchase_category = $('#purchase_category').val();
                            var gate_pass_no = $('#gate_pass_no').val();
                            var account_date = $('#accounting_date').val();
                            var round_off = $('#round_off').val();
                            var net_amount = $('#net_amount').val();
                            var net_total_tax = $('#net_total_tax').val();
                            var net_total_items = $('#net_total_items').val();
                            var is_igst_enable = $('input[name="tax_category"]:checked').val();
                            var purchase_return_no = $('#pur_return_no').val();
                            var save_type = post_type;
                            var purchase_return_head_id = $("#purchase_return_head_id").val();
                            var credit_bill_date = $("#credit_bill_date").val();
                            var credit_bill_no = $("#credit_bill_no").val();
                            if (save_type == 2) {
                                if (credit_bill_date == "" || credit_bill_no == "") {
                                    toastr.error('Please Enter Credit Bill Date and Credit Bill No');
                                    return false;
                                }
                            }
                            var param = {
                                'purchase_category': purchase_category,
                                'vendor_id': vendor_id,
                                'gate_pass_no': gate_pass_no,
                                'from_depart': from_depart,
                                'account_date': account_date,
                                'net_total_items': net_total_items,
                                'net_total_tax': net_total_tax,
                                'net_amount': net_amount,
                                'tax_rate': is_igst_enable,
                                'credit_bill_date': credit_bill_date,
                                'credit_bill_no': credit_bill_no,
                                'ret_quantity_head': ret_quantity_head,
                            };
                            var item_code_temp;
                            var item_remark;
                            item_remarks_array = [];
                            $('#purchase_return_tbl_body tr').each(function () {
                                if ($.type($(this).attr('item-code')) != "undefined") {
                                    item_code_temp = $(this).attr('item-code');
                                    if ($.type($(this).find('input[name="remarks[]"]').val()) != "undefined") {
                                        item_remark = $(this).find('input[name="remarks[]"]').val();
                                        item_remarks_array.push({
                                            'item_remark': item_remark,
                                            'item_code': item_code_temp
                                        });
                                    }
                                }
                            });

                            var return_data = JSON.stringify(return_item_array);
                            var item_remarks_array = JSON.stringify(item_remarks_array);
                            var file_token = $('#hidden_filetoken').val();
                            var net_amount = $('#net_amount').val();
                            $.ajax({
                                type: "POST",
                                url: urls,
                                data: {
                                    _token: file_token,
                                    item_remark: item_remarks_array,
                                    return_data: return_data,
                                    param: param,
                                    return_to_vendor_save: 1,
                                    purchase_return_no: purchase_return_no,
                                    save_type: save_type,
                                    round_off: round_off,
                                    purchase_return_head_id: purchase_return_head_id
                                },
                                beforeSend: function () {
                                    if (save_type == 0) {
                                        if (is_print == 1) {
                                            $('#save_all_btn_savespin_p').removeClass('fa fa-save');
                                            $('#save_all_btn_savespin_p').addClass('fa fa-spinner fa-spin');
                                        } else {
                                            $('#save_all_btn_savespin').removeClass('fa fa-save');
                                            $('#save_all_btn_savespin').addClass('fa fa-spinner fa-spin');
                                        }
                                    } else if (save_type == 1) {
                                        if (is_print == 1) {
                                            $('#approve_all_btn_approvespin_p').removeClass('a fa-thumbs-up');
                                            $('#approve_all_btn_approvespin_p').addClass('fa fa-spinner fa-spin');
                                        } else {
                                            $('#approve_all_btn_approvespin').removeClass('a fa-thumbs-up');
                                            $('#approve_all_btn_approvespin').addClass('fa fa-spinner fa-spin');
                                        }
                                    } else if (save_type == 2) {
                                        if (is_print == 1) {
                                            $('#credit_approve_all_btn_approvespin_p').removeClass('a fa-thumbs-up');
                                            $('#credit_approve_all_btn_approvespin_p').addClass('fa fa-spinner fa-spin');
                                        } else {
                                            $('#credit_approve_all_btn_approvespin').removeClass('a fa-thumbs-up');
                                            $('#credit_approve_all_btn_approvespin').addClass('fa fa-spinner fa-spin');
                                        }
                                    }
                                },
                                success: function (rslt) {
                                    var result = JSON.parse(rslt);
                                    if (result.type == 1) {
                                        toastr.success("Purchse Return Approved successfully");

                                        if (is_print == 1) {
                                            $('#approve_all_btn_approvespin_p').removeClass('fa fa-spinner fa-spin');
                                            $('#approve_all_btn_approvespin_p').addClass('a fa-thumbs-up');
                                        } else {
                                            $('#approve_all_btn_approvespin').removeClass('fa fa-spinner fa-spin');
                                            $('#approve_all_btn_approvespin').addClass('a fa-thumbs-up');
                                        }
                                        if (result.pu_head_id != '' && $.type(result.pu_head_id) != 'undefined') {
                                            purchase_return_head_id = result.pu_head_id;
                                        }


                                    }
                                    if (result.type == 2) {
                                        toastr.success("Purchse Return Credit Approved successfully");

                                        if (is_print == 1) {
                                            $('#credit_approve_all_btn_approvespin_p').removeClass('fa fa-spinner fa-spin');
                                            $('#credit_approve_all_btn_approvespin_p').addClass('a fa-thumbs-up');
                                        } else {
                                            $('#credit_approve_all_btn_approvespin').removeClass('fa fa-spinner fa-spin');
                                            $('#credit_approve_all_btn_approvespin').addClass('a fa-thumbs-up');
                                        }
                                        if (result.pu_head_id != '' && $.type(result.pu_head_id) != 'undefined') {
                                            purchase_return_head_id = result.pu_head_id;
                                        }


                                    }
                                    if (result.type == 0) {
                                        toastr.success("Purchse Return Saved successfully");
                                        $(".save_all_btn_cls").prop('disabled', true);
                                        $(".save_all_btn_cls").prop('disabled', true);
                                        if (is_print == 1) {
                                            $('#save_all_btn_savespin_p').removeClass('fa fa-spinner fa-spin');
                                            $('#save_all_btn_savespin_p').addClass('fa fa-save');
                                        } else {
                                            $('#save_all_btn_savespin').removeClass('fa fa-spinner fa-spin');
                                            $('#save_all_btn_savespin').addClass('fa fa-save');
                                        }
                                        if (result.pu_head_id != '' && $.type(result.pu_head_id) != 'undefined') {
                                            purchase_return_head_id = result.pu_head_id;
                                        }

                                    }
                                    if (result.status == 10) {
                                        toastr.error("Please select one Item");
                                        setTimeout(function () {
                                            returnToList();
                                        }, 300);
                                    }
                                    $("#pur_return_no").val(result.request_no);
                                    if (is_print == 1) {
                                        print_out(purchase_return_head_id);
                                    }
                                },
                                complete: function () {
                                    setTimeout(function () {
                                        returnToList();
                                    }, 300);
                                }
                            });
                        }
                    }
                });
            } else {
                toastr.warning("Please select any item");
            }
        } else {
            toastr.warning("Please select Return Store");
        }
    } else {
        toastr.warning("Please select supplier");
    }
}


function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
        val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}

function return_items_to_vendor_edit(id) {
    window.location = url + "/purchase/return_items_to_vendor_edit/" + id;
}

function select_batch_wise_item_edit(e, type = 0) {
    is_edit = 1;
    var item_code = $(e).closest("tr").find("input[name='item_code[]']").val();
    var item_name = $(e).closest("tr").find("input[name='item_desc[]']").val();
    var department = $("#department").val();
    var purchase_return_head_id = $("#purchase_return_head_id").val();
    var base_url = $("#ret_base_url").val();
    var url = base_url + "/purchase/getBatchDetails";
    var json_string = JSON.stringify(return_item_array);
    var param = {
        item_code: item_code,
        select_batch_edit: 1,
        department: department,
        purchase_return_head_id: purchase_return_head_id,
        item_return_array: json_string,
        type: type
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $(e).closest('i').removeClass('fa fa-info');
            $(e).closest('i').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            $("#itemts_batchwise_div_edit").html(html);
            $("#item_return_batch_model_edit").modal('show');
            $("#modal_item_code").html(item_name);
            setTimeout(function () {
                $("#batch_bill_pop_0").focus();
            }, 300);
        },
        complete: function () {
            $(e).closest('i').removeClass('fa fa-spinner fa-spin');
            $(e).closest('i').addClass('fa fa-info');
            $("#item_code_hidden").val(item_code);
            $('.return_invoice_date').datetimepicker({
                format: "DD-MMM-YYYY",
            });
            $('.return_expiry_date').datetimepicker({
                format: "DD-MMM-YYYY",
            });

        }
    });
}

function add_purchase_return_row_edit() {
    if ($("#department").val() == '') {
        toastr.warning("Please Select Return Store");
        return;
    }
    if ($("#vendor").val() == '') {
        toastr.warning("Please Select Vendor");
        return;
    }
    if ($("#approve_status_check").val() == '1' || $("#approve_status_check").val() == '2') {
        return;
    }

    response = '<tr><td ><input type="text" value="" name="item_desc[]"  autocomplete="off" class="form-control return_item_desc" onkeyup="select_item_desc(this.id,event)">\n\
                        <div class="ajaxSearchBox search_return_item_box" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; \n\
                        margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;\n\
                        border: 1px solid rgba(0, 0, 0, 0.3);"> </div></td>\n\
                                            <td><input type="text" value="" name="item_code[]" class="form-control"> </td>\n\
                                            <td ><i class="fa fa-info batch_wise_info_class" style="cursor:pointer" onclick="select_batch_wise_item_edit(this,1)"></i></td>\n\
                                            <td ><input type="text" value="" name="quantity[]" autocomplete="off" readonly="" class="form-control" style="text-align: right !important" ></td>\n\
                                            <td ><input type="text" value="" name="remarks[]" class="form-control"> </td>\n\
                                            <td ><input type="text" value="" name="net_tax[]" readonly=""  class="form-control item_code_net_tax" style="text-align: right !important" > </td>\n\
                                            <td ><input type="text" value="" name="net_amount[]" readonly=""  class="form-control item_code_net_amount" style="text-align: right !important" > </td>\n\
                                            <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';
    $("#purchase_return_tbl_body_edit").append(response);

    var divElements = document.querySelectorAll('.search_return_item_box');
    var descElements = document.querySelectorAll('.return_item_desc');
    var batch_class = document.querySelectorAll('.batch_wise_info_class');
    for (var i = 0; i < divElements.length; i++) {
        divElements[i].id = 'search_return_item_box-' + i;
        descElements[i].id = 'return_item_desc_box-' + i;
        batch_class[i].id = 'batch_wise_info_btn-' + i;

    }
    var k = $('#p_return_edit_tbl tr').length;
    $('#p_return_edit_tbl tr').each(function () {
        if (k > 12) {
            var row = $('#p_return_edit_tbl').find('tr').eq(k - 1);
            $(row).addClass('myClass');
        }
        k++;
    });
    setTimeout(function () {
        var fcs_id = i - 1;
        document.getElementById("return_item_desc_box-" + fcs_id).focus();
    }, 2000);
    getListItemsCount('purchase_return_tbl_body_edit');
}

function delete_return_entry(purchase_return_head_id, item_code, tax_amount, net_amount) {
    if (parseInt(approve_status_check) != 1 && parseInt(approve_status_check) != 2) {
        bootbox.confirm({
            message: "Are you sure you want to delete ?",
            buttons: {
                'confirm': {
                    label: 'Delete',
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var param = {
                        purchase_return_head_id: purchase_return_head_id,
                        item_code: item_code,
                        tax_amount: tax_amount,
                        net_amount: net_amount
                    };
                    $.ajax({
                        type: "POST",
                        url: url + "/purchase/delete_purchase_return_dtls",
                        data: param,
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Deleted Successfully...');
                        },
                        complete: function () {
                            window.location.reload();

                        }
                    });
                }
            }
        });


    } else {
        toastr.warning("Item cannot delete");
    }
}

function print_out(head_id) {
    url = "";
    url = $("#ret_base_url").val();
    urls = url + "/purchase/purchase_return_print_out?purchase_return_id=" + head_id;
    window.open(urls, 'div', 'height=3508,width=2480');
}

function returnToList() {
    window.location = url + "/purchase/purchase_return_list";
}

function GetUrlParameter(sPageURL, sParam) {
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function add_vendor_return_items() {
    var vendor = $('#vendor').val();

    if ($("#department").val() == '') {
        toastr.warning("Please Select Return Store");
        return;
    }
    $("#vendor_popup").val(vendor);
    $('#vendor_popup').select2().trigger('change');
    $("#vendor_return_items_modal").modal('show');
    vendor_return_items(0);

}

function clear_vendor_returnSearch() {
    $('#vendor_item_search').val("");
    $('#to_expiry_date').val("");
    $('#chk_exp_items').attr("checked", false);
    $("#vendor_popup").val('');
    $('#vendor_popup').select2().trigger('change');
    vendor_return_items(0);
}

function changeVendorPopup(obj) {
    var vendor = $(obj).val();
    $("#vendor").val(vendor);
    $('#vendor').select2().trigger('change');
}

function vendor_return_items(page) {
    var vendor_item_search = $('#vendor_item_search').val();
    var vendor_popup = $('#vendor_popup').val();
    var to_expiry_date = $('#to_expiry_date').val();
    var chk_exp_items = 0;
    var department = $("#department").val();
    if ($('#chk_exp_items').prop("checked") == true) {
        chk_exp_items = 1;
    } else {
        chk_exp_items = 0;
    }
    $.ajax({
        type: "GET",
        url: url + "/purchase/vendor_return_items_search",
        data: 'vendor_item_search=' + vendor_item_search + '&department=' + department + '&vendor_popup=' + vendor_popup + '&chk_exp_items=' + chk_exp_items + '&to_expiry_date=' + to_expiry_date + '&page_value=' + page,
        beforeSend: function () {
            $("#fill_vendor_return_items_list").html('');
            $.LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            if ($('#vendor_return_items_modal').hasClass('show')) {
                // console.log($('#vendor_return_items_modal').hasClass('show'));
            } else {
                $("#vendor_return_items_modal").modal('show');
            }
            if (html) {
                $("#fill_vendor_return_items_list").html(html);
            } else {
                $("#fill_vendor_return_items_list").append("<tr><td colspan='8'>No  Items Found!</td></tr>");
            }
        },
        complete: function () {
            $.LoadingOverlay("hide");
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead('reflow');
            }, 2000);
        }
    });
}

function list_vendor_return_items(obj, item_code, item_desc) {
    var vendor_popup = $('#vendor_popup').val();
    var item_code_exts = 0;
    if (vendor_popup == "") {
        toastr.warning("Please Select Vendor");
        $(obj).attr("checked", false);
        return;
    }
    $('#purchase_return_tbl_body tr').each(function () {
        item_code_temp = $(this).attr('item-code');
        if (item_code == item_code_temp) {
            item_code_exts = 1;
        }
    });
    $('#purchase_return_tbl_body_edit tr').each(function () {
        item_code_temp = $(this).attr('item-code');
        if (item_code == item_code_temp) {
            item_code_exts = 1;
        }
    });
    if (item_code_exts == 1) {
        toastr.error("Selected Item Is Already Existing!");
        return false;
    }
    if ($(obj).prop("checked") == true) {
        if ($("#purchase_return_tbl_body_edit").children().length != 0) {
            response = '<tr item-code="' + item_code + '" id="tr_' + item_code + '"><td ><input type="text" value="' + item_desc + '" name="item_desc[]"  autocomplete="off" class="form-control return_item_desc" onkeyup="select_item_desc(this.id,event)">\n\
                        <div class="ajaxSearchBox search_return_item_box" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; \n\
                        margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;\n\
                        border: 1px solid rgba(0, 0, 0, 0.3);"> </div></td>\n\
                        <td><input type="text" value="' + item_code + '" name="item_code[]" class="form-control"> </td>\n\
                        <td ><i class="fa fa-info batch_wise_info_class" style="cursor:pointer" onclick="select_batch_wise_item_edit(this,1)"></i></td>\n\
                        <td ><input type="text" value="" name="quantity[]" autocomplete="off" id="item_code_qty_' + item_code + '" readonly="" class="form-control" style="text-align: right !important" ></td>\n\
                        <td ><input type="text" value="" name="remarks[]" class="form-control"> </td>\n\
                        <td ><input type="text" value="" name="net_tax[]" readonly="" id="item_code_net_tax_' + item_code + '" class="form-control item_code_net_tax" style="text-align: right !important" > </td>\n\
                        <td ><input type="text" value="" name="net_amount[]" readonly="" id="item_code_net_amount_' + item_code + '"  class="form-control item_code_net_amount" style="text-align: right !important" > </td>\n\
                        <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';
            $("#purchase_return_tbl_body_edit").append(response);

            var divElements = document.querySelectorAll('.search_return_item_box');
            var descElements = document.querySelectorAll('.return_item_desc');
            var batch_class = document.querySelectorAll('.batch_wise_info_class');
            for (var i = 0; i < divElements.length; i++) {
                divElements[i].id = 'search_return_item_box-' + i;
                descElements[i].id = 'return_item_desc_box-' + i;
                batch_class[i].id = 'batch_wise_info_btn-' + i;

            }
            var k = $('#p_return_edit_tbl tr').length;
            $('#p_return_edit_tbl tr').each(function () {
                if ($(this).find(".return_item_desc").val() == "") {
                    $(this).remove();
                }
                if (k > 12) {
                    var row = $('#p_return_edit_tbl').find('tr').eq(k - 1);
                    $(row).addClass('myClass');
                }
                k++;
            });
            getListItemsCount('purchase_return_tbl_body_edit');
        } else {
            response = '<tr item-code="' + item_code + '" id="tr_' + item_code + '"><td ><input type="text" value="' + item_desc + '" name="item_desc[]" autofocus autocomplete="off" class="form-control return_item_desc" onkeyup="select_item_desc(this.id,event)">\n\
                            <div class="ajaxSearchBox search_return_item_box" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; \n\
                            margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;\n\
                            border: 1px solid rgba(0, 0, 0, 0.3);"> </div></td>\n\
                        <td><input type="text" value="' + item_code + '" name="item_code[]" class="form-control"> </td>\n\
                        <td ><i class="fa fa-info batch_wise_info_class" style="cursor:pointer" onclick="select_batch_wise_item(this)"></i></td>\n\
                        <td ><input type="text" value="" name="quantity[]" id="item_code_qty_' + item_code + '" autocomplete="off" readonly="" class="form-control"></td>\n\
                        <td ><input type="text" value="" name="remarks[]" class="form-control"> </td>\n\
                        <td ><input type="text" value="" name="net_tax[]" id="item_code_net_tax_' + item_code + '" readonly=""  class="form-control item_code_net_tax"> </td>\n\
                        <td ><input type="text" value="" name="net_amount[]" id="item_code_net_amount_' + item_code + '" readonly=""  class="form-control item_code_net_amount"> </td>\n\
                        <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';
            $('#purchase_return_tbl_body').append(response);
            var divElements = document.querySelectorAll('.search_return_item_box');
            var descElements = document.querySelectorAll('.return_item_desc');
            var batch_class = document.querySelectorAll('.batch_wise_info_class');
            for (var i = 0; i < divElements.length; i++) {
                divElements[i].id = 'search_return_item_box-' + i;
                descElements[i].id = 'return_item_desc_box-' + i;
                batch_class[i].id = 'batch_wise_info_btn-' + i;
            }
            $('#purchase_return_tbl_body tr').each(function () {
                if ($(this).find(".return_item_desc").val() == "") {
                    $(this).remove();
                }
            });
            getListItemsCount('purchase_return_tbl_body');
        }
    }
}

function getListItemsCount(table_name) {
    var list_cnt = 0;
    $('#' + table_name + ' tr').each(function () {
        if ($(this).find("input[name='item_desc[]']").val() != "") {
            list_cnt = list_cnt + 1;
        }
    });
    $('#net_total_items').val(list_cnt);
    return list_cnt;
}

$(document).on('click', '#remove_all_tax', function (e) {
    if ($(this).is(":checked") == true) {
        $('.return_tax_perce_all').each(function () {
            $(this).val('0');
            $(this).blur();
        });
    } else {
        $('.return_tax_perce_all').each(function () {
            var prev_tax = $(this).closest("td").find("input[name='return_tax_perce_hid[]']").val();
            $(this).val(prev_tax);
            $(this).blur();
        });
    }
})

function print_purchase_return() {
    let url = $('#ret_base_url').val();
    var status = $("#status option:selected").val();
    var vendor = $("#vendor option:selected").val();
    var to_date = $("#to_date").val();
    var from_date = $("#from_date").val();
    var purchase_return_no = $("#purchase_return_no").val();
    var return_store = $("#return_store option:selected").val();
    var file_token = $('#hidden_filetoken').val();
    $.ajax({
        type: "POST",
        url: url + "/purchase/print_purchase_return",
        data: {

            _token: file_token,
            vendor: vendor,
            status: status,
            return_store: return_store,
            purchase_return_no: purchase_return_no,
            from_date: from_date,
            to_date: to_date
        },
        beforeSend: function () {},
        success: function (result) {
            let res = JSON.parse(result);
            var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write(res.html);
            winPrint.document.close();
            winPrint.focus();
            winPrint.print();
        },
        complete: function () {

        }
    });
}
