selected_item_array = {};
item_id = 1;
to_location_id = $("#from_locationid").val();
from_location_id = $("#to_locationid").val();
request_no = '';
stock_id = $("#stock_id_for_receive_list").val();
var status = 0;
$("#item_search_btn").click(function () {
    $("#receive_search_box").toggle();
    document.getElementById("receive_search_box").focus();
    $("#item_search_btn_text").toggle();
});

function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("receive_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("receive_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];

        label = td.getElementsByTagName("label");

        if (td) {
            if (td.textContent.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}



function save_all() {

    /**
     *
     * creating array
     */
    var error_flg = 0;

    $('#save_recieve_items_btn_spin').removeClass('fa fa-save');
    $('#save_recieve_items_btn_spin').addClass('fa fa-spinner fa-spin');
    $(".save_recieve_items").prop('disabled', true);

    var total_rejected_quantity = 0;
    $(".issue_qty_val").each(function (key, val) {
        var req_qty_id = val.id;
        var res = req_qty_id.split("item_issue_qty_id_");
        var item_head_id = res[1].trim();
        var item_val = val.value;
        selected_item_array[item_head_id] = {};
        selected_item_array[item_head_id].recevied_qty = item_val;

    });
    console.log(selected_item_array);
    $(".rejected_qty_val").each(function (key, val) {
        var rejected_qty_id = val.id;
        var res = rejected_qty_id.split("product_reje_qty_id_");
        var item_head_id = res[1].trim();
        var rejected_counts = $('#' + rejected_qty_id).val();
        var item_val = val.value;

        var receved_counts = parseFloat($('#' + rejected_qty_id).closest("tr").find("input[name='item_receive_qty[]']").val());
        var issued_counts = parseFloat($('#' + rejected_qty_id).closest("tr").find("input[name='hidden_item_quanty[]']").val());
        total_rejected_quantity = parseFloat(total_rejected_quantity) + parseInt(rejected_counts);
        if ((issued_counts - (receved_counts + rejected_counts) > 0) && issued_counts < receved_counts && receved_counts != 0 && rejected_counts != 0) {
            error_flg = 1;
        } else if (receved_counts == 0 && rejected_counts == 0) {
            delete selected_item_array[item_head_id];
            return;
        } else {
            selected_item_array[item_head_id].rejected_qty = item_val;
        }
    });
    $(".reject_remark").each(function (key, val) {
        var rejected_qty_id = val.id;
        var res = rejected_qty_id.split("reject_remark_item_id_");
        var item_head_id = res[1].trim();
        var item_val = val.value;
        if (selected_item_array[item_head_id]) {
            selected_item_array[item_head_id].rejected_qty_remark = item_val;
        }

    });

    if (error_flg == 1) {

        $('#save_recieve_items_btn_spin').removeClass('fa fa-spinner fa-spin');
        $('#save_recieve_items_btn_spin').addClass('fa fa-save');
        $(".save_recieve_items").prop('disabled', false);

    }


    /**
     *
     * Array creation end
     */

    var form_data = $('#receive_product_list').serialize();
    var item_array = JSON.stringify(selected_item_array);
    var from_location = from_location_id;
    var to_location = to_location_id;
    var req_id = stock_id;
    var request_no = $('#rrequest_number').html().trim();
    var er_flag = 1;
    if (item_array != '{}') {
        er_flag = 1;
    } else {
        er_flag = 0;
        $('#save_recieve_items_btn_spin').removeClass('fa fa-spinner fa-spin');
        $('#save_recieve_items_btn_spin').addClass('fa fa-save');
        $(".save_recieve_items").prop('disabled', false);
    }


    if (er_flag) {
        $.ajax({
            type: "POST",
            url: '',
            data: form_data + '&receive_item=' + 1 + '&item_collection=' + item_array + '&from_location=' + from_location + '&to_location=' + to_location + '&request_id=' + req_id + '&request_no=' + request_no,
            beforeSend: function () {

            },
            success: function (result) {
                var json_data = JSON.parse(result);
                if (json_data.success == '1') {
                    toastr.success("Items received successfully");

                    setTimeout(function () {
                        cancel_receive();
                    }, 400);
                } else if (json_data.success == '5') {
                    toastr.error("Receive quantity greater than issue qty");
                } else {
                    if (json_data.error_message != "") {
                        toastr.error(result.error_message);
                    } else {
                        toastr.error("Please check Received qty");
                    }

                }
            },
            complete: function () {

                // $('#save_recieve_items_btn_spin').removeClass('fa fa-spinner fa-spin');
                // $('#save_recieve_items_btn_spin').addClass('fa fa-save');

            }
        });
    } else {
        toastr.error('Error in Quantity');
    }


}

$('input.fully_receive_reject').on('change', function () {
    $('input.fully_receive_reject').not(this).prop('checked', false);
    if (this.id == 'fully_receive_chk') {
        $(".item_full_rej_rec").each(function (key, val) {
            if (!$(this).closest('tr').find('.issue_qty_val').is('[readonly]')) {
                var req_qty_id = val.value;
                if (req_qty_id == '' || req_qty_id == null) {
                    req_qty_id = 0;
                }
                if ($(this).closest('tr').find('.rejected_qty_val').val() != '') {
                    var reject_val = $(this).closest('tr').find('.rejected_qty_val').val();
                } else {
                    var reject_val = 0;
                }
                var balace_qnty = $(this).closest('tr').find('.issue_status_indicator').val();

                var item_val = parseFloat(balace_qnty) + parseFloat(reject_val)
                if (parseFloat(item_val) > req_qty_id) {
                    toastr.warning('You are receiving more than the issued Quantity !!!');
                    balace_qnty = '0';
                }
                if (balace_qnty < 0) {
                    balace_qnty = '0';
                }

                if (balace_qnty > 0) {
                    if ($("#fully_receive_chk").prop('checked') == true) {
                        $(this).closest('td').next('td').find('input').val(balace_qnty);
                        $(this).closest('td').next().next('td').find('input').val('0.00');
                    } else {
                        $(this).closest('td').next('td').find('input').val('0.00');
                    }
                }
            }

        });
    } else {
        $(".item_full_rej_rec").each(function (key, val) {
            if (!$(this).closest('tr').find('.issue_qty_val').is('[readonly]')) {
                var req_qty_id = val.value;
                if (req_qty_id == '' || req_qty_id == null) {
                    req_qty_id = 0.00;
                }
                if ($("#fully_reject_chk").prop('checked') == true) {
                    $(this).closest('td').next('td').find('input').val('0.00');
                    $(this).closest('td').next().next('td').find('input').val(req_qty_id);
                } else {
                    $(this).closest('td').next().next('td').find('input').val('0.00');
                }
            }
        });
    }
});

function load_page() {
    window.location.reload();
}

function cancel_receive() {
    var url = $("#req_base_url").val();
    window.location = url + "/purchase/issue_requisition_list/2";
}
$(document).ready(function () {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
});

function checkReceivedQuantity(e, item_id) {
    if ($("#item_issue_qty_id_" + item_id).val() != '') {
        var receive_val = $("#item_issue_qty_id_" + item_id).val();
    } else {
        var receive_val = 0;
    }
    if ($("#item_balance_qnt_" + item_id).val() != '') {
        var balance_id = $("#item_balance_qnt_" + item_id).val();
    } else {
        var balance_id = 0;
    }
    if ($("#product_reje_qty_id_" + item_id).val() != '') {
        var reject_val = $("#product_reje_qty_id_" + item_id).val();
    } else {
        var reject_val = 0;
    }

    var item_val = parseFloat(receive_val) + parseFloat(reject_val)
    if (parseFloat(item_val) > balance_id) {
        toastr.warning('You are receiving more than the issued Quantity !!!');
        e.value = '0';
    }

    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
        val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }

}
