$(document).ready(function () {
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $('.datepicker').datetimepicker({
        minDate:moment().format('MMM-DD-YYYY'),
        format: 'MMM-DD-YYYY',
    });
    // $( "#datepicker" ).datepicker({ minDate: 0});
    $('.uhid').hide();
    $('.otp').hide();
    $('.name').hide();

    $(window).scrollTop(0);
});


$('input[type=radio][name=patient_type]').on('change', function(){
    var patient_type = $('input[type=radio][name=patient_type]:checked').val();
    $('#phone').val('');
    $('#verify_otp').val('');
    $('#name').val('');
    $('#uhid').val('');
    $('#patient_id').val('');
    if(patient_type == 1){
        $('.uhid').hide();
        $('.otp').hide();
        $('.name').hide();
    }else{
        $('.uhid').show();
        $('.otp').hide();
        $('.name').hide();
        $('#uhid').attr("readonly", false);
    }
});




var base_url = $("#base_url").val();
function selectStatusDoctorsList() {
    var url = base_url + "/appointment_api/select_doctors_using_speciality";
    var speciality = $('#status_department').val();
    var param = {speciality: speciality};
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#status_doctor_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#status_doctor_id').LoadingOverlay("hide");
            $('#status_doctor_id').empty().trigger("change");
            var doctors_list = obj.response;
            doctors_list = $.parseJSON(doctors_list);
            if(doctors_list) {
                $.each(doctors_list, function (key, value) {
                    $("#status_doctor_id").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
                $('#status_doctor_id').empty().trigger("change");
            }
        },
        complete: function () {
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
function selectDoctorsList() {
    var url = base_url + "/appointment_api/select_doctors_using_speciality";
    var speciality = $('#department').val();
    var param = {speciality: speciality};
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#doctor_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#doctor_id').LoadingOverlay("hide");
            $('#doctor_id').empty().trigger("change");
            var doctors_list = obj.response;
            doctors_list = $.parseJSON(doctors_list);
            if(doctors_list) {
                $.each(doctors_list, function (key, value) {
                    $("#doctor_id").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
                $('#doctor_id').empty().trigger("change");
            }
        },
        complete: function () {
            getDoctorSlots();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

$('#doctor_id').on('change', function () {
    if($('#doctor_id').val() != null){
        setTimeout(function () {
            getDoctorSlots();
        },1000);
    }
});

function getDoctorSlots(){
    setTimeout(function(){
        var url = base_url + "/appointment_api/getDoctorSlots";
        var appointment_date = $('#appointment_date').val();
        var doctor_id = $('#doctor_id').val();
        var block_future_booking_status = $('#block_future_booking_status').val();
        if(doctor_id == '' || appointment_date == ''){
            toastr.warning("Warning Please select doctor and date");
            return false;
        }
        console.log(appointment_date);
        var param = {appointment_date: appointment_date,doctor_id:doctor_id,block_future_booking_status:block_future_booking_status};
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#slots_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#slots_area').LoadingOverlay("hide");
                $('#slots_area').html(data);
            },
            complete: function () {

                $('input[type=radio][name=slot_div]').click(function() {
                    $('.slot_container').removeClass('bg-info');
                    $(this).parent().addClass('bg-info');
                });

            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    },500);

}

function verifyPhone(){
    var phone_no = $('#phone').val();
    // var patient_type = $('input[type=radio][name=patient_type]:checked').val();
    if(phone_no ==''){
        toastr.warning("Enter phone no");
        return false;
    }

    var filter = /^[0-9-+]+$/;
    if (filter.test(phone_no)) {
        verifyOtp(phone_no);
    }
    else{
        return false;
    }

}
var random_no = 0;
// function verifyOtp(){
//     random_no = Math.floor(100000 + Math.random() * 900000);
//     bootbox.alert("SMS gateway delays may occur due to network traffic.<br> use <b style='color:red;'>"+random_no+"</b> as temporary OTP!");
//     $('.otp').show();
//     $('#verify_otp').focus();
// }

function verifyOtp(phone_no){
    random_no = Math.floor(100000 + Math.random() * 900000);
    var url = base_url + "/appointment_api/sentOTPtoMobile";
    var param = {
        phone_no: phone_no,
        random_no: random_no,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {

        },
        success: function (data) {
            if(data != 0){
                bootbox.alert("otp for online booking for ROHINI has been sent to your mobile number please check it out!");
                $('.otp').show();
                $('#verify_otp').focus();

            }else{
                toastr.error("Error Please Check Your Internet Connection");
            }
        },
        complete: function () {
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function verifyContinue(){
    var patient_type = $('input[type=radio][name=patient_type]:checked').val();
    var phone_no = $('#phone').val();
    var verify_otp = $('#verify_otp').val();
    if(verify_otp != random_no){
        toastr.warning("OTP verification failed!");
        $('#verify_otp').focus();
        return false;
    }else{
        toastr.info("Verified!");
    }
    if(patient_type == 2){
        checkMultiplePatientStatus(phone_no);
    }else{
        // $('.uhid').show();
        $('.name').show();
    }
}

function checkMultiplePatientStatus(){
    var url = base_url + "/appointment_api/checkMultiplePatientStatus";
    var phone_no = $('#phone').val();
    var param = {phone_no: phone_no};
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {

        },
        success: function (data) {
            if(data != 0){
                $('#patient_select_modal').modal('show');
                $('#patient_select_modal_content').html(data);
            }else{
                toastr.info('No patients are associated with this number!');
                // $('.uhid').show();
                $('.name').show();
            }
        },
        complete: function () {
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function setPatient(id,patient_name,uhid){
    $('#patient_id').val(id);
    $('#name').val(patient_name);
    $('#uhid').val(uhid);
    $('#patient_select_modal').modal('hide');
    $('.uhid').show();
    $('.name').show();
    $('#uhid').attr("readonly", "readonly");
}

function saveBooking(){
    var url = base_url + "/appointment_api/saveBooking";
    var patient_name = $('#name').val();
    var uhid = $('#uhid').val();
    var patient_id = $('#patient_id').val();
    var verify_otp = $('#verify_otp').val();
    var phone = $('#phone').val();
    var doctor_id = $('#doctor_id').val();
    var appointment_date = $('#appointment_date').val();
    var slot = $('input[type=radio][name=slot_div]:checked').val();
    var session_id = $('input[type=radio][name=slot_div]:checked').attr("data-attr-session_id");
    var token_no = $('input[type=radio][name=slot_div]:checked').attr("data-attr-token_no");
    var consulting_time_from = $('input[type=radio][name=slot_div]:checked').attr("data-attr-consulting_time_from");
    var consulting_time_to = $('input[type=radio][name=slot_div]:checked').attr("data-attr-consulting_time_to");
    var patient_type = $('input[type=radio][name=patient_type]:checked').val();
    var block_future_booking_status = $('#block_future_booking_status').val();
    var current_date = $('#current_date').val();
    var current_time = $('#current_time').val();

    var time1 = new Date(current_date + ' ' + current_time);
    var time2 = new Date(appointment_date + ' ' + consulting_time_to);
    // if(strtotime(current_time) > strtotime(consulting_time_to)){
    if (time1.getTime() > time2.getTime()) {
        toastr.warning('Doctors\' consultation time has ended. Appointments are not available at the moment!');
        return false;
    }

    if(block_future_booking_status == 1 && current_date != appointment_date)
    {
        toastr.warning('Future booking is disabled for selected doctor!');
        return false;
    }

    if(doctor_id == ''){
        toastr.warning("Select doctor!");
        return false;
    }
    if(appointment_date == ''){
        toastr.warning("Select appointment date!");
        return false;
    }
    if(patient_name == ''){
        toastr.warning("Enter patient name!");
        $('#name').focus();
        return false;
    }

    if(verify_otp == ''){
        toastr.warning("Enter OTP!");
        $('#verify_otp').focus();
        return false;
    }

    if(verify_otp != random_no){
        toastr.warning("OTP verification failed!");
        $('#verify_otp').focus();
        return false;
    }
    if(phone == ''){
        toastr.warning("Enter phone no!");
        $('#phone').focus();
        return false;
    }

    if(slot == undefined){
        toastr.warning("Select slot!");
        return false;
    }

    var param = {
        patient_name: patient_name,
        uhid:uhid,
        patient_id:patient_id,
        phone:phone,
        doctor_id:doctor_id,
        appointment_date:appointment_date,
        slot:slot,
        session_id:session_id,
        token_no:token_no,

    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('body').LoadingOverlay("hide");
            if(data == 1){

                bootbox.alert("Patient booking completed successfully!.<br> Token No:<b style='color:red;'>"+token_no+"</b><br>Booked slot time:<b style='color:red;'>"+slot+"</b><br><hr><br><i> Doctors current session starts at: <b style='color:red;'>"+consulting_time_from+"</b> and ends at: <b style='color:red;'>"+consulting_time_to+"</b></i><br>Please report at front office atleast 15 minutes before!");
                getDoctorSlots();
                $("#new_patient").prop("checked", true);

                // if(patient_type == 1){
                    $('#phone').val('');
                    $('#verify_otp').val('');
                    $('#name').val('');
                    $('#uhid').val('');
                    $('#patient_id').val('');
                // }
                $('.otp').hide();
                $('.name').hide();
                $('.uhid').hide();

            } else if (data == 2) {
                toastr.error("Already has an appointment with this doctor!");
            }else{
                toastr.error("Error!");
            }
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function CheckAppointment(){
    $('#modal_check_status').modal('show');
    $("#status_department").val($('#department').val()).trigger("change");
    setTimeout(function () {
        $("#status_doctor_id").val($('#doctor_id').val()).trigger("change");
    },500);
    $('#status_appointment_date').val($('#appointment_date').val());
    $('#status_phone').val($('#phone').val());
}
function CheckAppointmentStatus(){
    var phone = $('#status_phone').val();
    var name = $('#status_name').val();
    var patient_id = $('#patient_id').val();
    var doctor_id = $('#status_doctor_id').val();
    var appointment_date = $('#status_appointment_date').val();
    var url = base_url + "/appointment_api/CheckAppointmentStatus";
    var current_date = $('#current_date').val();
    if(current_date != appointment_date){
        toastr.warning('Appointment date is not current date!');
        return false;
    }

    if(phone ==''){
        toastr.warning("Enter phone number for status check!");
        return false;
    }
    if(doctor_id ==''){
        toastr.warning("Select doctor for status check!");
        return false;
    }

    var param = {
        phone:phone,
        name:name,
        patient_id:patient_id,
        doctor_id:doctor_id,
        appointment_date:appointment_date,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('body').LoadingOverlay("hide");
            bootbox.alert(data);
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });


}

$('.slot_container').hover(function(){
    $(this).find('input[type=radio]').css('background-color', '#d9edf7 !important');
});
