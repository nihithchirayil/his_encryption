$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function chartOfAccountsData() {

    var url = route_json.chartOfAccountsReportData;

    //-------filters---------------------------------------------

    var ledger_id = $('#ledger_id_hidden').val();
    var summary = 0;
    if ($("#summary").is(':checked') == true) {
        $("#summary_hid").val("1");
        summary = 1;
    } else {
        summary = 0;
        $("#summary_hid").val("0");
    }
    var exclude_doctor = $('#exclude_doctor').is(":checked");

    var parm = { ledger_id:ledger_id };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}




function datarst() {

    var current_date = $('#current_date').val();

    $("#depatment_list").select2('val', '');
    $('#date_from').val(current_date);

}
function editAccount(id){
    var url = $("#base_url").val();
    var urls = url + "/master/list_ledger_master?hidden_ledger_name_id="+id+"&ledger_name="+account_name;
    window.open(urls, '_blank');
}
function deleteAccount(id){
    var parm = { delete_id:id };
$.ajax({
        type: "GET",
        url: "",
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            //$('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
           $("#"+id).remove();
           Command: toastr["Success"]("Successfully Deleeted!");
        },

        complete: function () {

        },

    });
}
function excelleExp(type, fn, dl) {
     let tbl1 = document.getElementById("hospital_headers");
   let tbl2 = document.getElementById("result_data_table");

   let worksheet_tmp1 = XLSX.utils.table_to_sheet(tbl1);
   let worksheet_tmp2 = XLSX.utils.table_to_sheet(tbl2);
      
   let a = XLSX.utils.sheet_to_json(worksheet_tmp1, { header: 1 });
   let b = XLSX.utils.sheet_to_json(worksheet_tmp2, { header: 1 });
      
   a = a.concat(['']).concat(b)
     
   let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true })
   
   const new_workbook = XLSX.utils.book_new();
   XLSX.utils.book_append_sheet(new_workbook, worksheet, "worksheet");
   XLSX.writeFile(new_workbook, 'chart_of_accounts.xls');
//    var type = 'xlsx';
//    var elt = document.getElementById('result_data_table');
//    let tbl1 = document.getElementById("hospital_headers");
//    elt.concat(['']).concat(tbl1)
//    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
//    return dl ?
//        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
//        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}