var arr = new Array();
var arr1 = new Array();
$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $(".excludeItemButton").hide();
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}
function fillSearchGroupDetials(id, name, serach_key_id,balance_sheet_ident) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
    $("balance_sheet_ident").val(balance_sheet_ident);
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getgeneralReportData() {

    var url = route_json.allAccountResults;

    //-------filters---------------------------------------------

    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var ledger_id = $('#ledger_id_hidden').val();
    var ledger_group_id = $('#ledger_group_id_hidden').val();
    var from_type =$('#balance_sheet_ident').val();
    var parm = { from_date: from_date,to_date:to_date,ledger_id:ledger_id,ledger_group_id:ledger_group_id,from_type:from_type };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
           // $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}




function datarst() {

    var current_date = $('#current_date').val();

    $("#depatment_list").select2('val', '');
    $('#date_from').val(current_date);

}
function getSingleAccountDetails(e,ledger_id){
        var added=false;
        $.map(arr, function(elementOfArray, indexInArray) { 
                if (elementOfArray.id == ledger_id) {
                added = true;
                $("."+ledger_id).remove();
                arr = arr.filter(function( obj ) {
                return obj.id !== ledger_id;
                });
            }
        });
if (!added) {
arr.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = 'Trasaction Data';
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name}
    
     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataDetail",
                data: param,
                beforeSend: function () {
                //$.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $(e).closest('tr').after(data);
                   // $.LoadingOverlay("hide");
                },
                complete: function () {
                }
            });
}
}
function editLedgerHeder(booking_id,v_type_id){
    var url = $("#base_url").val();
    var urls = url + "/accounts/editLedgerBooking/"+booking_id+"/"+v_type_id;
    window.open(urls, '_blank');
}
function getSingleAccountGroupDetails(e,ledger_id){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        var ledger_name = 'trail_balance';
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,first_td_text:first_td_text}

        $.ajax({
            type: "POST",
            url: url + "/accounts/getSingleTransactionReportDataDetailByGroupTrail",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                       // $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
            }
        });
    }
}
function getSingleLedgerDetailsTrail(e,ledger_id,report_type){
    var added1 = false;
    $.map(arr1, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added1 = true;
            $("." + ledger_id).remove();
            arr1 = arr1.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added1) {
        arr1.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:report_type}
    
     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataTrail",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                        $("#getLeaderDetailModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#getLeaderDetailData').html(data);
                        $.LoadingOverlay("hide");
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                            });
                                        setTimeout(function(){
            var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                        scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                        }

                        });
                        $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                                minScrollbarLength: 30
                        });
                        $('.fixed_header').floatThead({
                        position: 'absolute',
                                scrollContainer: true
                        });
                    },300);
                },
                complete: function () {
                }
            });
        }
}
function color_row(e){
    if($(e).prop('checked') == true){
    $(e).closest('tr').css('background-color', '#f7cdcd');
    }else{
        $(e).closest('tr').css('background-color', '');
    }
}
function narationRow(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').hide();
             $(e).closest('tr').attr('data-narations',2);
             return false;
         }
    }else{
        $(e).closest('tr').attr('data-narations',1);
        $(e).closest('tr').next('tr').show();
    }
}