$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
var base_url = $('#base_url').val();
var token = $('#token').val();

function processInsuranceData() {
    var servieDate = $("#servieDate").val();
        if (servieDate) {
            var url = base_url + "/accounts/processInsuranceData";
            var param = { _token: token,servieDate: servieDate };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#postdatadiv').html('');
                    $('#processdatabtn').attr('disabled', true);
                    $('#processdataspin').removeClass('fa fa-list');
                    $('#processdataspin').addClass('fa fa-spinner fa-spin');
                    $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                },
                success: function (data) {
                    if (data != 3) {
                        $('#postdatadiv').html(data);
                        setTimeout(function () {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);

                        $('.theadscroll').perfectScrollbar({
                            minScrollbarLength: 30
                        });
                    } else {
                        toastr.warning('Already Posted');
                    }
                },
                complete: function () {
                    $('#processdataspin').removeClass('fa fa-spinner fa-spin');
                    $('#processdataspin').addClass('fa fa-list');
                    $('#processdatabtn').attr('disabled', false);
                    $('#postdatadiv').LoadingOverlay("hide");
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
}

function insertInsuranceData() {
    var servieDate = $("#servieDate").val();
        if (servieDate) {
            var url = base_url + "/accounts/postInsAdvData";
            var param = { _token: token, servieDate: servieDate };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#nextsequencedatabtn').attr('disabled', true);
                    $('#nextsequencedataspin').removeClass('fa fa-forward');
                    $('#nextsequencedataspin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (data == 1) {
                        toastr.success("Successfully Posted");
                        setTimeout(function () {
                            window.location.reload();
                        }, 300);
                    } else {
                        toastr.error("Error Occured");
                    }
                },
                complete: function () {

                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
   
}
function revertData() {
    if (confirm("Are you sure you want to Delete ?")) {
    var servieDate = $("#servieDate").val();
        if (servieDate) {
            var url = base_url + "/accounts/revertInsAdvData";
            var param = { _token: token, servieDate: servieDate };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#revertbtn').attr('disabled', true);
                    $('#revertbtn').removeClass('fa fa-reply');
                    $('#revertbtn').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (data == 1) {
                        toastr.success("Successfully Reverted");
                        setTimeout(function () {
                            window.location.reload();
                        }, 300);
                    }if (data == 2){
                        toastr.error("Data exist.!");
                    $('#revertbtn').attr('disabled', false);
                    $('#revertbtn').removeClass('fa fa-spinner fa-spin');
                    $('#revertbtn').addClass('fa fa-reply');
                    } else {
                        toastr.error("Error Occured");
                    }
                },
                complete: function () {

                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
   
}
}
function reloadData(){
    window.location.reload();
}