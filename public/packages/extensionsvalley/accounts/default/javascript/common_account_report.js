var arr = new Array();
var arr1 = new Array();
var exp_arr = new Array();
var inc = 1;
$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    var table_company_code = $("#table_company_code").val();
    if(table_company_code == 'DAYAH'){
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
         minDate : '04-01-2022',
         maxDate : 'now'
    });
}else{
       $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
}
    $(".select2").select2({placeholder: "", maximumSelectionSize: 6});
});
//SEARCH DATA
function ResultData() {
        if($("#expand_all").prop('checked')==true){
            $("#expand_all").prop('checked',false);
        }
        if($("#expand_all_trail").prop('checked')==true){
            $("#expand_all_trail").prop('checked',false);
        }
    var url = route_json.reportResults;
    //-------filters---------------------------------------------

    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    if($('#opening_balance_nil').prop('checked') == true){
       var opening_balance_nil = 1;
    }else{
        var opening_balance_nil = 0;
    }
    if($('#opening_only_trail').prop('checked') == true){
        var opening_only_trail = 1;
     }else{
         var opening_only_trail = 0;
     }

    var parm = {bill_date_from: from_date, bill_date_to: to_date,opening_balance_nil:opening_balance_nil,opening_only_trail:opening_only_trail};
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            if($("#exclude_option").prop("checked")){
                $(".excludeItemButton").show();
                $(".excludeItemButton").each(function(key, val){
                    //$(val).closest('td').prev().attr('colspan', 6)
                });
            }
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}
//GET SINGLE AMOUNT EXAMPLE P&L TYPE FORMAT
function showSingleAmountDetail(e, ledger_id,report_type) {

    $(e).removeClass('expand_all');
    if($("#expand_all").prop("checked")){
        showSingleAmountDetailExpand(e, ledger_id,report_type);
    }else{
        showSingleAmountDetailNormal(e, ledger_id,report_type);
    }

}
function showSingleAmountDetailExpand(e, ledger_id,report_type){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        if($('#opening_balance_nil').prop('checked') == true){
            var opening_balance_nil = 1;
        }else{
            var opening_balance_nil = 0;
        }
        $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        var ledger_name = report_type;
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,opening_balance_nil:opening_balance_nil,first_td_text:first_td_text}
        setTimeout(function(){
        $.ajax({
            type: "POST",
            url: url + "/accounts/getSingleTransactionReportDataDetailByGroup",
            data: param,
            beforeSend: function () {

            },
            success: function (data) {
                $(e).closest('tr').after(data);
                setTimeout(function(){
                        $.LoadingOverlay("hide");
                },3000);

            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                       // $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
                if($("#expand_all").prop("checked")){
                            expand_all(inc);
            }
        }
        });
        inc++;
    },3000);

}
}
function showSingleAmountDetailNormal(e, ledger_id,report_type){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        if($('#opening_balance_nil').prop('checked') == true){
            var opening_balance_nil = 1;
        }else{
            var opening_balance_nil = 0;
        }
        var ledger_name = report_type;
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,opening_balance_nil:opening_balance_nil,first_td_text:first_td_text}
        $.ajax({
            type: "POST",
            url: url + "/accounts/getSingleTransactionReportDataDetailByGroup",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                       // $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
                if($("#expand_all").prop("checked")){
                            expand_all(inc);
            }
        }
        });
        inc++;


}
}

function getSingleLedgerDetails(e,ledger_id,report_type){
    var added1 = false;
    // $.map(arr1, function (elementOfArray, indexInArray) {
    //     if (elementOfArray.id == ledger_id) {
    //         added1 = true;
    //         $("." + ledger_id).remove();
    //         arr1 = arr1.filter(function (obj) {
    //             return obj.id !== ledger_id;
    //         });
    //     }
    // });
    if (!added1) {
        arr1.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:'report_type'}

     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportData",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                 },
                success: function (data) {
                if(report_type=='trail_balance'){
                    $(e).closest('tr').after(data);
                    $.LoadingOverlay("hide");
                    }else{
                        $("#getLeaderDetailModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#getLeaderDetailData').html(data);
                        $.LoadingOverlay("hide");
                        setTimeout(function(){

                            $('.theadscroll').perfectScrollbar({
                                minScrollbarLength: 30
                            });

                            var $table = $('table.theadfix_wrapper');
                            $table.floatThead({
                                scrollContainer: function($table){
                                    return $table.closest('.theadscroll');
                                }
                            });

                    },3000);
                }
                },
                complete: function () {

                }
            });
        }
}
function getAdvanceFromPatients(page = 0){
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={bill_date_from:bill_date_from,bill_date_to:bill_date_to,page_value:page};

     $.ajax({
                type: "POST",
                url: url + "/accounts/getAdvanceFromPatientData",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                 },
                success: function (data) {

                        $("#getAdvDetailModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#getAdvData').html(data);
                        $.LoadingOverlay("hide");

                },
                complete: function () {
                    var ttl_adv_pat_amnt = $("#ttl_adv_pat_amnt").val();
                    $("#adv_name_header").html("Advance from patients: "+ttl_adv_pat_amnt);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
                }
            });

}
function editLedgerHeder(booking_id,v_type){
    if(v_type=='Journal' || v_type == 3 ){
        var v_no = 3;
    }else{
        var v_no = 0;
    }
    var url = $("#base_url").val();
    var urls = url + "/accounts/editLedgerBooking/"+booking_id+"/"+v_no
    window.open(urls, '_blank');
}
function excelleExpGrp(type, fn, dl) {
   window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#result_container_div').html()));
   e.preventDefault();
}
function excelleExpGrp(excel_name, colspan=5) {
     var style = "<style>.headerclass { color: black !important; }</style>";
    var  template_date = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' + style + '</head><body><table>{table}</table></body></html>';
    var printhead = ''
    var excel_data = atob(printhead);
    excel_data = excel_data.replace("colspan_no", colspan);
    var uri = 'data:application/vnd.ms  -excel;base64,',
        template = template_date,
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    var toExcel = document.getElementById("result_container_div").innerHTML;
    var excel_data = excel_data + toExcel;

    var ctx = {
        worksheet: name || '',
        table: excel_data
    };
    var link = document.createElement("a");
    link.download = excel_name + ".xls";
    link.href = uri + base64(format(template, ctx))
    link.click();
}
function excelleExp(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}
function datarst() {
    window.location.reload();
}

function excludeItem(item){
    $(item).parents('tr').hide();
    $('.'+$(item).parents('tr').attr('id')).remove();
    var current_debit_amount = parseFloat($(item).parents('tr').find('.debit_amount').html().replaceAll(",", "")).toFixed(2);
    var current_credit_amount = parseFloat($(item).parents('tr').find('.credit_amount').html().replaceAll(",", "")).toFixed(2);
    $(item).parents('tr')[0].classList.forEach(function(val){
        if($('#'+val).length > 0){
            var debit_amount = parseFloat($('#'+val).find('.debit_amount').html().replaceAll(",", "")).toFixed(2);
            var credit_amount = parseFloat($('#'+val).find('.credit_amount').html().replaceAll(",", "")).toFixed(2);
            var new_debit_amount = debit_amount - current_debit_amount;
            var new_credit_amount = credit_amount - current_credit_amount;

            $('#'+val).find('.debit_amount').html(number_format(new_debit_amount, 2));
            $('#'+val).find('.credit_amount').html(number_format(new_credit_amount, 2));
        }

    });

    var total_debit_amount = parseFloat($(".total_debit_amount").html().replaceAll(",", "")).toFixed(2);
    var total_credit_amount = parseFloat($(".total_credit_amount").html().replaceAll(",", "")).toFixed(2);
    var new_total_debit_amount = total_debit_amount - current_debit_amount;
    var new_total_credit_amount = total_credit_amount - current_credit_amount;
    $(".total_debit_amount").html(number_format(new_total_debit_amount, 2))
    $(".total_credit_amount").html(number_format(new_total_credit_amount, 2))
}

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

$(document).on("click", "#exclude_option", function(){
    if($(this).prop("checked")){
        $(".excludeItemButton").show();
        $(".excludeItemButton").each(function(key, val){
           // $(val).closest('td').prev().attr('colspan', 6)
        });
    } else {
        $(".excludeItemButton").hide();
        $(".excludeItemButton").each(function(key, val){
            //$(val).closest('td').prev().attr('colspan', 7)
        });
    }

});
function print_generate_accounts(printData) {
$(".excludeItemButton").hide();
    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }
    mywindow.document.write('<style>.table.no-border, .table.no-border td, .table.no-border th { border: 0; } \n\
.table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } \n\
.table_sm td span { line-height: 18px !important; } \n\
.table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } \n\
.table_head.tth { font-size: 12px !important; padding: 1px !important; } \n\
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, \n\
.table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } \n\
.table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, \n\
.table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, \n\
.table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } \n\
.headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} \n\
th { background-color:#d9dfe0 !important;}.common_td_rules{ text-align: left !important; overflow: hidden !important;}\n\
.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; }\n\
.excludeItemButton{ display: none !important; }</style>');
    mywindow.document.write(showw + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
    return true;

}
function narationRow_old(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').remove();
             return false;
         }
    }
     $.ajax({
                type: "GET",
                url: url + "/accounts/show_naration_row/",
                data: {head_id:head_id},
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                 },
                success: function (data) {
                    $(e).closest('tr').after('');
                    $(e).closest('tr').attr('data-narations',1);
                  $(e).closest('tr').after(data);
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $.LoadingOverlay("hide");
                }
            });
    }
    function color_row(e){
    if($(e).prop('checked') == true){
    $(e).closest('tr').css('background-color', '#f7cdcd');
    }else{
        $(e).closest('tr').css('background-color', '');
    }
}
$(document).on("click", "#show_all_narration", function(){
    if($(this).prop("checked")){
        $('.checkAllNarration').attr('data-narations',1);
        $(".append_narration").css('display','');
    } else {
        $('.checkAllNarration').attr('data-narations',2);
        $(".append_narration").css('display','none');
    }

});
function narationRow(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').hide();
             $(e).closest('tr').attr('data-narations',2);
             return false;
         }
    }else{
        $(e).closest('tr').attr('data-narations',1);
        $(e).closest('tr').next('tr').show();
    }
}
$('body').on('click', '.advance_from_pages a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var page = GetUrlParameter(url,'page');
            // console.log("   ---url---  "+page); return false;
            getAdvanceFromPatients(page);
});
function GetUrlParameter(sPageURL, sParam){
    // var sPageURL = window.location.search.substring(1);
     console.log(sPageURL);
    var sURLVariables = sPageURL.split('?');

    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] == sParam)

        {
            return sParameterName[1];
        }
    }
}
$("#expand_all").on('click',function(){
    if($(this).prop('checked')==false){
        window.location.reload();
    }else{
    $('.expand_all').each(function(){
        $(".expand_all").trigger('click');
    });
    }
});
function expand_all(inc){
    $('.expand_all').each(function(){
        setTimeout(function(){
        $(".expand_all").trigger('click');
        },2000);
    });

}
function print_generate_bs_pl(printData) {

    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }
    mywindow.document.write('<style>#result_data_table{display:none} .table.no-border, .table.no-border td, .table.no-border th { border: 0; } .table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } .table_sm td span { line-height: 18px !important; } .table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } .table_head.tth { font-size: 12px !important; padding: 1px !important; } .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } .table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, .table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, .table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } .headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} th { background-color:#d9dfe0 !important;}.common_td_rules{ text-align: left !important; overflow: hidden !important;}.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; }</style>');
    mywindow.document.write(showw + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
    return true;

}
function getSingleTransactionReportDataDetailBySpeciality(e, ledger_id,report_type,ledger_code){
    var added = false;
    $(e).removeClass('expand_all');
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        if($('#opening_balance_nil').prop('checked') == true){
            var opening_balance_nil = 1;
        }else{
            var opening_balance_nil = 0;
        }
        var ledger_name = report_type;
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,opening_balance_nil:opening_balance_nil,first_td_text:first_td_text,ledger_code:ledger_code}
        $.ajax({
            type: "POST",
            url: url + "/accounts/getSingleTransactionReportDataDetailBySpeciality",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                       // $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
                if($("#expand_all").prop("checked")){
                            expand_all(inc);
            }
        }
        });
        inc++;


}
}
function showSpecialityAmount(e, ledger_id,report_type,ledger_code){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $(e).removeClass('expand_all');
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id && elementOfArray.ledger_code == ledger_code) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id,ledger_code: ledger_code});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        if($('#opening_balance_nil').prop('checked') == true){
            var opening_balance_nil = 1;
        }else{
            var opening_balance_nil = 0;
        }
        show_row_group = $(e).attr('data-parent-ids');
        var ledger_name = report_type;
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,opening_balance_nil:opening_balance_nil,first_td_text:first_td_text,show_row_group:show_row_group,ledger_code:ledger_code}
        $.ajax({
            type: "POST",
            url: url + "/accounts/showSpecialityAmount",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                       // $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
                if($("#expand_all").prop("checked")){
                            expand_all(inc);
            }
        }
        });
        inc++;


}
}
