function showCostMedPLDetail(){
//    var sale_from_date = $("#bill_date_from").val();
//    var sale_to_date = $("#bill_date_to").val();
//    var url = $("#base_url").val();
//    var urls = url + "/accounts/showCostMedPLDetail/"+sale_from_date+"/"+sale_to_date
//    window.open(urls, '_blank');
$.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
$(".cost_of_med").toggle();
$.LoadingOverlay("hide");
}
function showOpeningDetail(){
    var sale_from_date = $("#bill_date_from").val();
    var sale_to_date = $("#bill_date_to").val();
    var url = $("#base_url").val();
    var urls = url + "/accounts/getItemStockTaxwiseOpening?from_date="+sale_from_date;
    //window.open(urls, '_blank');
}
function getPlOpeningBalance(e,ttl_amnt){
    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var url = $("#base_url").val();
    var opb_filter =  $(e).closest('tr').attr('data-opb_filter');
    var param = {bill_date_from: from_date, bill_date_to: to_date,ttl_amnt:ttl_amnt};
    if(opb_filter == 1){
             $('.opening_balance_filter').remove();
             $(e).closest('tr').attr('data-opb_filter',2);
             return false;
    }else{
        $.ajax({
            type: "POST",
            url: url + "/accounts/getPlOpeningBalance",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $(e).closest('tr').attr('data-opb_filter',1);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                    
            }
        });
        }
}
function expand_all_row(){
   // $('.click_all_tr').trigger('click');
}