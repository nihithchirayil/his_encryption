var url = $("#ins_base_url").val();
function viewAuditLog(head_id,table_name,is_detail){
    param = {head_id:head_id,table_name:table_name,is_detail:is_detail};
    $.ajax({
        type: "POST",
        url: url + "/accounts/getAuditLogDetail",
        data: param,
        beforeSend: function () {
            $('#historyspin-'+head_id).removeClass('fa fa-history');
            $('#historyspin-'+head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
        $("#audit_log_body").html(data);
        $("#getAuditLog").modal('show');
        },
        complete: function () {
            $('#historyspin-'+head_id).removeClass('fa fa-spinner fa-spin');
            $('#historyspin-'+head_id).addClass('fa fa-history');
    }
    });
}
function viewAuditLogMaster(head_id){
    param = {head_id:head_id};
    $.ajax({
        type: "POST",
        url: url + "/accounts/getAuditLogLedgerMaterDetail",
        data: param,
        beforeSend: function () {
            $('#history_spin_'+head_id).removeClass('fa fa-history');
            $('#history_spin_'+head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
        $("#audit_log_body").html(data);
        $("#getAuditLog").modal('show');
        },
        complete: function () {
            $('#history_spin_'+head_id).removeClass('fa fa-spinner fa-spin');
            $('#history_spin_'+head_id).addClass('fa fa-history');
    }
    });
}
function glMappingAuditLog(head_id){
    urls = $("#base_url").val();
    param = {head_id:head_id};
    $.ajax({
        type: "POST",
        url: urls + "/accounts/getAuditLogLedgerMaping",
        data: param,
        beforeSend: function () {
            $('#history_gl_mapping_'+head_id).removeClass('fa fa-history');
            $('#history_gl_mapping_'+head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
        $("#audit_log_body").html(data);
        $("#getAuditLog").modal('show');
        },
        complete: function () {
            $('#history_gl_mapping_'+head_id).removeClass('fa fa-spinner fa-spin');
            $('#history_gl_mapping_'+head_id).addClass('fa fa-history');
    }
    });
}
function closingStockAuditLog(head_id){
    urls = $("#base_url").val();
    param = {head_id:head_id};
    $.ajax({
        type: "POST",
        url: urls + "/accounts/getAuditLogLedgerClosing",
        data: param,
        beforeSend: function () {
            $('#history_closing_'+head_id).removeClass('fa fa-history');
            $('#history_closing_'+head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
        $("#audit_log_body").html(data);
        $("#getAuditLog").modal('show');
        },
        complete: function () {
            $('#history_closing_'+head_id).removeClass('fa fa-spinner fa-spin');
            $('#history_closing_'+head_id).addClass('fa fa-history');
    }
    });
}
