var arr = new Array();
var arr1 = new Array();
$(function() {
  Highcharts.setOptions({
    global: {
      useUTC: false,
     timezoneOffset: (-1)*(-180) //UTC-5:00 time zone

    }
  });
});
$(document).ready(function () {
            setTimeout(function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            }, 300);
           var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            LoadAllData('fy');
  });
  function getCustomDates(date_filt){
      $("#bill_date_from").val('');
      $("#bill_date_to").val('');
      $("#select_month").val('');
      $('input[name="daterange"]').val('custom');
      $("#current_year").val(date_filt);
     LoadAllData(date_filt);
  }
  function select_month(e){
     var mnth = e;
     var fiscyear = getCurrentFinancialYear();
     var fisc_arr = fiscyear.split('-');
     var year_type = $("#current_year").val();
     if(mnth==0){
         LoadAllData(year_type);
         return false;
     }
     if(year_type == 'fy'){
        if(mnth <=3 ){
            var last_day = new Date(fisc_arr[1], mnth, 0).getDate();
            $("#bill_date_from").val(fisc_arr[1]+'-'+mnth+'-01');
            $("#bill_date_to").val(fisc_arr[1]+'-'+mnth+'-'+last_day);
        }else{
            var last_day = new Date(fisc_arr[0], mnth, 0).getDate();
            $("#bill_date_from").val(fisc_arr[0]+'-'+mnth+'-01');
            $("#bill_date_to").val(fisc_arr[0]+'-'+mnth+'-'+last_day);
        }
     }else{
         if(mnth >=4 ){
            var last_day = new Date(fisc_arr[0]-1, mnth, 0).getDate();
            $("#bill_date_from").val(fisc_arr[0]-1+'-'+mnth+'-01');
            $("#bill_date_to").val(fisc_arr[0]-1+'-'+mnth+'-'+last_day);
        }else{
            var last_day = new Date(fisc_arr[0], mnth, 0).getDate();
            $("#bill_date_from").val(fisc_arr[0]+'-'+mnth+'-01');
            $("#bill_date_to").val(fisc_arr[0]+'-'+mnth+'-'+last_day);
        }
     }

     LoadAllData(year_type);
  }
function LoadAllData(date_filter){
     setTimeout(function () {
       loadPlData(date_filter);
   },300);
   setTimeout(function () {
       loadCurrentMonthProfit('current_month',date_filter);
   },300);
    setTimeout(function () {
       loadCurrentMonthProfit('last_month',date_filter);
   },300); 
    setTimeout(function () {
       loadCurrentMonthProfit('fy',date_filter);
   },300); 
    setTimeout(function () {
       loadPatientData("op_data",date_filter);
   },300);
       setTimeout(function () {
       loadPatientData("pharmacy_purchase",date_filter);
   },300);
       setTimeout(function () {
       loadPatientData("ip_data",date_filter);
   },300);
       setTimeout(function () {
       loadPatientData("company_credit",date_filter);
   },300);
       setTimeout(function () {
       loadPatientData("total_discount",date_filter);
   },300);
          setTimeout(function () {
       loadPatientData("unpaind_amnt",date_filter);
   },300);
        setTimeout(function () {
       loadExpIncChart(date_filter);
   },300);
    setTimeout(function () {
       loadBankData(date_filter);
   },300);
    setTimeout(function () {
       loadClosingData(date_filter);
   },300);
   setTimeout(function () {
       loadCollectionData(date_filter);
   },300);
}
//=============================EXPENSE INCOME BLOCK STARTS==================
function loadPlData(date_filter){
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
     var param = {month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to,opening_balance_nil:''};
            $.ajax({
                type: "GET",
                url: url + "/accounts/accountsDashboardData",
                data: param,
                beforeSend: function () {
                  $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (data) {
                    var data_array = JSON.parse(data);
                    $("#direct_income_span").html(data_array.direct_incomes);
                    $("#indirect_income_span").html(data_array.indirect_incomes);
                    $("#pharmacy_sale_span").html(data_array.sales_accounts);
                    $("#other_income_span").html(data_array.other_income);
                    $("#direct_exp_span").html('0');
                    $("#indirect_exp_span").html(data_array.indirect_expenses);
                    $("#fixed_asset_span").html(data_array.fixed_asset);
                    $("#purchase_span").html(data_array.purchase_accounts);
                    
                    $(".direct_income_block").attr('id',data_array.direct_incomes_id);
                    $(".indirect_income_block").attr('id',data_array.indirect_incomes_id);
                    $(".pharmacy_block").attr('id',data_array.sales_accounts_id);
                    $(".other_income_block").attr('id',data_array.other_income_id);
                    $(".direct_expense_block").attr('id',0);
                    $(".indirect_expense_block").attr('id',data_array.indirect_expenses_id);
                    $(".fixed_asset_block").attr('id',data_array.fixed_asset_id);
                    $(".purchase_accounts_block").attr('id',data_array.purchase_accounts_id);
                    $("#bill_date_from").val(data_array.from_date);
                    $("#bill_date_to").val(data_array.to_date);
                },
                complete: function () {
                }
            });
}
//============================================= EXPENSE INCOME BLOCK ENDS ===============================================================

function loadCurrentMonthProfit(filter_type,date_filter){
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param = {filter_type:filter_type,month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to,opening_balance_nil:''};
    $.ajax({
            type: "POST",
            url: url + "/accounts/loadNetProfit",
            data: param,
            beforeSend: function () {
               $("#current_month_net_i").addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                 var data_net_array = JSON.parse(data);
                 if(filter_type=='current_month'){
                $("#current_month_net_i").removeClass('fa fa-spinner fa-spin');
                $("#current_month_net").html(data_net_array.net_profit);
                $("#this_month_label").html(data_net_array.curr_month);
                 }
                 if(filter_type=='last_month'){
                $("#last_month_net_i").removeClass('fa fa-spinner fa-spin');
                $("#last_month_net").html(data_net_array.net_profit);
                $("#prev_month_label").html(data_net_array.prev_month);
                 }
                 if(filter_type=='fy'){
                $("#fy_month_net").html(data_net_array.net_profit);
                $("#fy_month_net_i").removeClass('fa fa-spinner fa-spin');
                $("#fy_label").html('FY '+data_net_array.fy_year);
                 }
                 
            },
            complete: function () {
                
                }
        });
}
function loadPatientData(types,date_filter){
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
     var param = {month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to,type:types};
     var url = $("#base_url").val();
    $.ajax({
            type: "POST",
            url: url + "/accounts/loadPatientData",
            data: param,
            beforeSend: function () {
                
                    $("#total_op_data_i").addClass('fa fa-spinner fa-spin');
                    $("#pharmacy_purchase_data_i").addClass('fa fa-spinner fa-spin');
                    $("#ip_data_i").addClass('fa fa-spinner fa-spin');
                    $("#company_credit_data_i").addClass('fa fa-spinner fa-spin');
                    
                    
            },
            success: function (data) {
                
            if(types == 'op_data'){
                var data_op_array = JSON.parse(data);
                $("#total_op_data_i").removeClass('fa fa-spinner fa-spin');
                $("#total_op_data").html(data_op_array.ttl_res);
                $("#total_renew_data").html(data_op_array.renewalrenewal_res);
                $("#total_follow_data").html(data_op_array.follow_up_res);
                $("#total_new_data").html(data_op_array.reg_res);
                
            }else if(types == 'pharmacy_purchase'){
                
            var data_phamacy_purchase_array = JSON.parse(data);
                $("#pharmacy_purchase_data_i").removeClass('fa fa-spinner fa-spin');
                
                $("#pharmacy_purchase_data").html(data_phamacy_purchase_array.pharmacy_res);
                $("#pharmacy_sales_data").html(data_phamacy_purchase_array.pharmacy_sales);
                var sp = parseFloat(data_phamacy_purchase_array.pharmacy_sales);
                var cp = parseFloat(data_phamacy_purchase_array.pharmacy_res);
                //var profit = ((sp-cp)/cp)*100;
                var profit_per = parseFloat(data_phamacy_purchase_array.profit_margin, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
                $("#pharmacy_margin_data").html(profit_per);
                
            }else if(types == 'ip_data'){
                
                var data_ip_array = JSON.parse(data);
                $("#ip_data_i").removeClass('fa fa-spinner fa-spin');
                $("#ip_data").html(data_ip_array.ip_count);
                $("#los_data").html(data_ip_array.los);
                $("#ip_income_data").html(data_ip_array.ip_collection);
                
            }else if(types == 'company_credit'){
                var company_credit_array = JSON.parse(data);
                $("#company_credit_data_i").removeClass('fa fa-spinner fa-spin');
                $("#company_credit_data").html(company_credit_array.company_credit);
                $("#company_credit_outstanding").html(company_credit_array.company_credit_outstanding);
            }else if(types == 'total_discount'){
                var total_discount_array = JSON.parse(data);
                $("#total_discount_data_i").removeClass('fa fa-spinner fa-spin');
                $("#total_discount_data").html(total_discount_array.total_discount);
                $("#share_discount_data").html(total_discount_array.share_holder_discount);
                $("#other_discount_data").html(total_discount_array.other_discount);
            }else if(types == 'unpaind_amnt'){
                var total_unpaid_array = JSON.parse(data);
                $("#unpaid_data").html(total_unpaid_array.total_unpaid_amnt);
                $("#unpaid_ot_data").html(total_unpaid_array.ot_unpaid_amnt);
            }
            },
            complete: function () {
                
                }
        });
}
function loadCollectionData(types,date_filter){
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
     var param = {month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to,type:types};
     var url = $("#base_url").val();
    $.ajax({
            type: "POST",
            url: url + "/accounts/loadCollectionData",
            data: param,
            beforeSend: function () {
                    
            },
            success: function (data) {
                var data_col_array = JSON.parse(data);
                $("#lab_span").html(data_col_array.lab);
                $("#ph_collection_span").html(data_col_array.pharmacy);
                $("#rad_span").html(data_col_array.radiology);
                $("#icu_span").html(data_col_array.icu);
                $("#ot_span").html(data_col_array.ot_sql);
                $("#other_coll_span").html(data_col_array.other);
                
           
            },
            complete: function () {
                
                }
        });
}
function loadExpIncChart(date_filter){
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param = {month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to};
    var url = $("#base_url").val();
    var exp_arr = new Array();
    var income_arr = new Array();
    var income_arr_table = new Array();
    var exp_arr_table = new Array();
    var table_bdy = '';
    $.ajax({
            type: "POST",
            url: url + "/accounts/loadExpIncChart",
            data: param,
            beforeSend: function () {
            },
            success: function (data) {
               var parse_chart_array = JSON.parse(data);
               exp_arr = parse_chart_array['expense'];
               income_arr = parse_chart_array['income'];
               income_arr_table = parse_chart_array['income_table'];
               exp_arr_table = parse_chart_array['expense_table'];
               var start_time = parse_chart_array['start_time'];
               if(exp_arr_table.length > income_arr_table.length){
                   table_bdy += '<tr><td></td>';
               for(var e=0; e<= exp_arr_table.length;e++){
               for (var key in exp_arr_table[e]) {
                table_bdy += '<td>'+key+'</td>';
                    }
                }
               }else{
                   table_bdy += '<tr><td></td>';
               for(var e=0; e<= income_arr_table.length;e++){
               for (var key in income_arr_table[e]) {
                table_bdy += '<td>'+key+'</td>';
                    }
                }
               }
               
               table_bdy += '</tr>';
               table_bdy += '<tr><td>Income</td>';
               var incc=0;
               for(var inc=0;inc <= income_arr_table.length;inc++){
               for (var key1 in income_arr_table[inc]) {
                   for (var key3 in exp_arr_table[incc]) {
                       if(key1==key3){
                table_bdy += '<td  style="color:green;">'+(income_arr_table[inc][key1]).toFixed(2)+'</td>';
                       }else{
                           incc++;
                table_bdy += '<td  style="color:green;">0</td>';
                table_bdy += '<td  style="color:green;">'+(income_arr_table[inc][key1]).toFixed(2)+'</td>';
                       }
                       incc++;
                        }
                    }
                }
               table_bdy += '</tr>';
                table_bdy += '<tr><td>Expense</td>';
               for(var exp=0;exp <= exp_arr_table.length;exp++){
               for (var key2 in exp_arr_table[exp]) {
                table_bdy += '<td  style="color:orange;">'+(exp_arr_table[exp][key2]).toFixed(2)+'</td>';
                       }
                        }
               table_bdy += '</tr>';
               $("#exp_inc_tbody").html(table_bdy);
               Highcharts.chart('container', {
    chart:{
        backgroundColor:'aliceblue',
    },

    title: {
        text: ''
    },
     credits: {
        enabled: false
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Expense Income'
        }
    },

    xAxis: {
        type: 'datetime',
        tickInterval: 1000 * 3600 * 24 *30 // 1 month
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            //pointStart: Date.UTC(2010, 0, 1)
        }
    },

    series: [{
        name: 'Income',
        data: income_arr,
        color: 'green',
    }, {
        name: 'Expense',
        data: exp_arr,
        color: 'orange',
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
 $.LoadingOverlay("hide");
            },
            complete: function () {
                }
        });
}
function loadClosingData(date_filter){
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param = {month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to,opening_balance_nil:''};
    $.ajax({
            type: "POST",
            url: url + "/accounts/loadClosingData",
            data: param,
            beforeSend: function () {
            },
            success: function (data) {
                 var data_closing_array = JSON.parse(data);
                 $("#closing_med_data").html(data_closing_array.closing_medicine);
                 $("#closing_xray_data").html(data_closing_array.stock_of_xray);
                 $("#lab_clos_data").html(data_closing_array.lab_res);
            },
            complete: function () {
                
                }
        });
}
function showDetailData(e,label,span_id){
    var ledger_id = $(e).attr('id');
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ttl_span_amount = $("#"+span_id).html();
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:'balance_amnt',opening_balance_nil:'1'}
    
     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataDetailByGroupDashboard",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                        $("#getLeaderDetailModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#getLeaderDetailData').html(data);
                        $("#leder_name_header").html(label);
                        $("#leder_amount_header").html(ttl_span_amount);
                        $.LoadingOverlay("hide");
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                            });
                },
                complete: function () {
                }
            });
        
}
function showSingleAmountDetail(e, ledger_id,report_type) {
    var added = false;
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        if($('#opening_balance_nil').prop('checked') == true){
            var opening_balance_nil = 1; 
        }else{
            var opening_balance_nil = 1; 
        }
        var ledger_name = report_type;
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,opening_balance_nil:opening_balance_nil}

        $.ajax({
            type: "POST",
            url: url + "/accounts/getSingleTransactionReportDataDetailByGroupDashboard",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                        $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
            }
        });
    }
    }
    function loadBankData(date_filter){
    var url = $("#base_url").val();
    var tbd = '';
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
     var param = {month_type:date_filter,bill_date_from:bill_date_from,bill_date_to:bill_date_to,opening_balance_nil:''};
            $.ajax({
                type: "GET",
                url: url + "/accounts/loadBankData",
                data: param,
                beforeSend: function () {
                },
                success: function (data) {
                    var data_bank_array = JSON.parse(data);
                    for(var k=0;k<data_bank_array.length;k++){
                        tbd += '<tr>\n\
                                <td style="text-align:left">'+data_bank_array[k]['ledger_name']+'</td>\n\
                                <td style="text-align:right">'+parseFloat(data_bank_array[k]['opening']).toFixed(2)+'</td>\n\
                                <td style="text-align:rightt">'+parseFloat(data_bank_array[k]['cr']).toFixed(2)+'</td>\n\
                                <td style="text-align:right">'+parseFloat(data_bank_array[k]['dr']).toFixed(2)+'</td>\n\
                                <td style="text-align:right">'+((parseFloat(data_bank_array[k]['opening'])+parseFloat(data_bank_array[k]['cr']))-parseFloat(data_bank_array[k]['dr'])).toFixed(2)+'</td>\n\
                                </tr>';
                    }
                    $("#bank_tbdy").html(tbd);
                },
                complete: function () {
                }
            });
}
function getSingleLedgerDetailsDashBoard(e,ledger_id,report_type){
    var added1 = false;
    $.map(arr1, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added1 = true;
            $("." + ledger_id).remove();
            arr1 = arr1.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added1) {
        arr1.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={ledger_id:ledger_id,opening_balance_nil:'1',bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:report_type}
    
     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataDashBoard",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                if(report_type=='trail_balance'){
                    $(e).closest('tr').after(data);
                    $.LoadingOverlay("hide");
                    }else{
                        $("#getLeaderDetailModeltr").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#getLeaderDetailDatatr').html(data);
                        $.LoadingOverlay("hide");
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                            });
                }
                },
                complete: function () {
                }
            });
        }
}
function narationRow_old(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').remove();
             return false;
         }
    }
     $.ajax({
                type: "GET",
                url: url + "/accounts/show_naration_row/",
                data: {head_id:head_id},
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $(e).closest('tr').after('');
                    $(e).closest('tr').attr('data-narations',1);
                  $(e).closest('tr').after(data);
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $.LoadingOverlay("hide");
                }
            });
    }
  function getCurrentFinancialYear() {
  var fiscalyear = "";
  var today = new Date();
  if ((today.getMonth() + 1) <= 3) {
    fiscalyear = (today.getFullYear() - 1) + "-" + today.getFullYear()
  } else {
    fiscalyear = today.getFullYear() + "-" + (today.getFullYear() + 1)
  }
  return fiscalyear
}
$(document).on("click", "#show_all_narration", function(){
    if($(this).prop("checked")){
        $('.checkAllNarration').attr('data-narations',1);
        $(".append_narration").css('display','');
    } else {
        $('.checkAllNarration').attr('data-narations',2);
        $(".append_narration").css('display','none');
    }
    
});
function narationRow(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').hide();
             $(e).closest('tr').attr('data-narations',2);
             return false;
         }
    }else{
        $(e).closest('tr').attr('data-narations',1);
        $(e).closest('tr').next('tr').show();
    }
}