var excludeArr = new Array();
var tr_inc = 1;
function getSingleAccountDetails(e,ledger_id){
        var added=false;
        $.map(arr, function(elementOfArray, indexInArray) {
                if (elementOfArray.id == ledger_id) {
                added = true;
                $("."+ledger_id).remove();
                arr = arr.filter(function( obj ) {
                return obj.id !== ledger_id;
                });
            }
        });
if (!added) {
arr.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = 'trail_balance';
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name}

     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataDetail",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                 },
                success: function (data) {
                    $(e).closest('tr').after(data);
                    $.LoadingOverlay("hide");
                },
                complete: function () {
                }
            });
}
}
function showSingleAmountDetailTrail(e, ledger_id,report_type) {

    $(e).removeClass('expand_all');
    if($("#expand_all_trail").prop("checked")){
    showSingleAmountDetailTrailExpand(e, ledger_id,report_type);
    }else{
    showSingleAmountDetailTrailNormal(e, ledger_id,report_type);
    }
}
function showSingleAmountDetailTrailExpand(e, ledger_id,report_type){
    var added = false;
    $(e).closest('tr').removeClass('expand_all_trail');
    if($("#exclude_option_trail").prop("checked")){
    excludeArr1 = JSON.stringify(excludeArr);
} else {
    excludeArr=[];
    excludeArr1= JSON.stringify(excludeArr);
}

var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
$.map(arr, function (elementOfArray, indexInArray) {
    if (elementOfArray.id == ledger_id) {
        added = true;
        $("." + ledger_id).remove();
        arr = arr.filter(function (obj) {
            return obj.id !== ledger_id;
        });
    }
});
if (!added) {
    arr.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = report_type;

    if($('#opening_only_trail').prop('checked') == true){
        var opening_only_trail = 1;
     }else{
         var opening_only_trail = 0;
     }
    var param = {opening_only_trail:opening_only_trail,ledger_id: ledger_id,excludeArr: excludeArr1,bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,first_td_text:first_td_text}
    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
    if(tr_inc < 90){
    setTimeout(function(){
    $.ajax({
        type: "POST",
        url: url + "/accounts/getSingleTransactionReportDataDetailByGroupTrail",
        data: param,
        beforeSend: function () {

        },
        success: function (data) {
            $(e).closest('tr').after(data);
            setTimeout(function(){
                $.LoadingOverlay("hide");
        },6000);
        },
        complete: function () {
            if($("#exclude_option_trail").prop("checked")){
                $(".excludeItemButton").show();
                $(".excludeItemButton").each(function(key, val){
                   // $(val).closest('td').prev().attr('colspan', 6)
                });
            }
            if($("#expand_all_trail").prop("checked")){``
            expand_all_trail(inc);
        }
        }
    });
},3000);
tr_inc++;
}
}
}
function showSingleAmountDetailTrailNormal(e, ledger_id,report_type){
    var added = false;
    $(e).closest('tr').removeClass('expand_all_trail');
    if($("#exclude_option_trail").prop("checked")){
    excludeArr1 = JSON.stringify(excludeArr);
} else {
    excludeArr=[];
    excludeArr1= JSON.stringify(excludeArr);
}

var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
$.map(arr, function (elementOfArray, indexInArray) {
    if (elementOfArray.id == ledger_id) {
        added = true;
        $("." + ledger_id).remove();
        arr = arr.filter(function (obj) {
            return obj.id !== ledger_id;
        });
    }
});
if (!added) {
    arr.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = report_type;
    if($('#opening_only_trail').prop('checked') == true){
        var opening_only_trail = 1;
     }else{
         var opening_only_trail = 0;
     }
    var param = {opening_only_trail:opening_only_trail,ledger_id: ledger_id,excludeArr: excludeArr1,bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,first_td_text:first_td_text}

    $.ajax({
        type: "POST",
        url: url + "/accounts/getSingleTransactionReportDataDetailByGroupTrail",
        data: param,
        beforeSend: function () {
           $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (data) {
            $(e).closest('tr').after(data);
            $.LoadingOverlay("hide");
        },
        complete: function () {
            if($("#exclude_option_trail").prop("checked")){
                $(".excludeItemButton").show();
                $(".excludeItemButton").each(function(key, val){
                   // $(val).closest('td').prev().attr('colspan', 6)
                });
            }
            if($("#expand_all_trail").prop("checked")){``
            expand_all_trail(inc);
        }
        }
    });
}
}
function getSingleLedgerDetailsTrail(e,ledger_id,report_type){
    var added1 = false;
    $.map(arr1, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added1 = true;
            $("." + ledger_id).remove();
            arr1 = arr1.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added1) {
        arr1.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:report_type}

     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataTrail",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                 },
                success: function (data) {
                        $("#getLeaderDetailModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#getLeaderDetailData').html(data);
                        $.LoadingOverlay("hide");
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                            });
                            setTimeout(function(){
            var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                        scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                        }

                        });
                        $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                                minScrollbarLength: 30
                        });
                        $('.fixed_header').floatThead({
                        position: 'absolute',
                                scrollContainer: true
                        });
                    },300);
                },
                complete: function () {
                }
            });
        }
}
function color_row(e){
    if($(e).prop('checked') == true){
    $(e).closest('tr').css('background-color', '#f7cdcd');
    }else{
        $(e).closest('tr').css('background-color', '');
    }
}

function excludeItemTrail(item,parentid){
    excludeArr.push(parentid);
    $(item).parents('tr').hide();
    $('.'+$(item).parents('tr').attr('id')).remove();
    var current_debit_amount = parseFloat($(item).parents('tr').find('.debit_amount').html().replaceAll(",", "")).toFixed(2);
    var current_credit_amount = parseFloat($(item).parents('tr').find('.credit_amount').html().replaceAll(",", "")).toFixed(2);
    $(item).parents('tr')[0].classList.forEach(function(val){
        if($('#'+val).length > 0){
            var debit_amount = parseFloat($('#'+val).find('.debit_amount').html().replaceAll(",", "")).toFixed(2);
            var credit_amount = parseFloat($('#'+val).find('.credit_amount').html().replaceAll(",", "")).toFixed(2);
            var new_debit_amount = debit_amount - current_debit_amount;
            var new_credit_amount = credit_amount - current_credit_amount;

            $('#'+val).find('.debit_amount').html(number_format(new_debit_amount, 2));
            $('#'+val).find('.credit_amount').html(number_format(new_credit_amount, 2));
        }

    });

    var total_debit_amount = parseFloat($(".total_debit_amount").html().replaceAll(",", "")).toFixed(2);
    var total_credit_amount = parseFloat($(".total_credit_amount").html().replaceAll(",", "")).toFixed(2);
    var new_total_debit_amount = total_debit_amount - current_debit_amount;
    var new_total_credit_amount = total_credit_amount - current_credit_amount;
    $(".total_debit_amount").html(number_format(new_total_debit_amount, 2))
    $(".total_credit_amount").html(number_format(new_total_credit_amount, 2))
}
$(document).on("click", "#exclude_option_trail", function(){
    if($(this).prop("checked")){
        $(".excludeItemButton").show();
        $(".excludeItemButton").each(function(key, val){
        });
    } else {
        $(".excludeItemButton").hide();
        $("#search_results").trigger('click');
    }

});
$("#expand_all_trail").on('click',function(){
    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
    if($(this).prop('checked')==false){
        window.location.reload();
    }else{
    $('.expand_all_trail').each(function(){
        $('.expand_all_trail').closest('tr').children('td:first').trigger('click');
    });
    }
    setTimeout(function(){
        $.LoadingOverlay("hide");
        },3000);
});
function expand_all_trail(inc){
    $('.expand_all_trail').each(function(){
        setTimeout(function(){
            $('.expand_all_trail').closest('tr').children('td:first').trigger('click');
        },2000);
    });
    $.LoadingOverlay("hide");
}
function getSingleTransactionReportDataDetailTrailBySpeciality(e, ledger_id,report_type,ledger_code){
    var added = false;
    $(e).closest('tr').removeClass('expand_all_trail');
    if($("#exclude_option_trail").prop("checked")){
    excludeArr1 = JSON.stringify(excludeArr);
} else {
    excludeArr=[];
    excludeArr1= JSON.stringify(excludeArr);
}
var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
$.map(arr, function (elementOfArray, indexInArray) {
    if (elementOfArray.id == ledger_id) {
        added = true;
        $("." + ledger_id).remove();
        arr = arr.filter(function (obj) {
            return obj.id !== ledger_id;
        });
    }
});
if (!added) {
    arr.push({id: ledger_id});
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = report_type;
    if($('#opening_only_trail').prop('checked') == true){
        var opening_only_trail = 1;
     }else{
         var opening_only_trail = 0;
     }
    var param = {opening_only_trail:opening_only_trail,ledger_id: ledger_id,excludeArr: excludeArr1,bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,first_td_text:first_td_text,ledger_code:ledger_code}

    $.ajax({
        type: "POST",
        url: url + "/accounts/getSingleTransactionReportDataDetailTrailBySpeciality",
        data: param,
        beforeSend: function () {
           $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (data) {
            $(e).closest('tr').after(data);
            $.LoadingOverlay("hide");
        },
        complete: function () {
            if($("#exclude_option_trail").prop("checked")){
                $(".excludeItemButton").show();
                $(".excludeItemButton").each(function(key, val){
                   // $(val).closest('td').prev().attr('colspan', 6)
                });
            }
            if($("#expand_all_trail").prop("checked")){``
            expand_all_trail(inc);
        }
        }
    });
}
}
function showSpecialityTrailAmount(e, ledger_id,report_type,ledger_code){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $(e).closest('tr').removeClass('expand_all_trail');
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id && elementOfArray.ledger_code == ledger_code) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id,ledger_code: ledger_code});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();
        if($('#opening_balance_nil').prop('checked') == true){
            var opening_balance_nil = 1;
        }else{
            var opening_balance_nil = 0;
        }
        show_row_group = $(e).closest('tr').attr('data-parent-ids');
        var ledger_name = report_type;
        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to, from_type: ledger_name,opening_balance_nil:opening_balance_nil,first_td_text:first_td_text,show_row_group:show_row_group,ledger_code:ledger_code}
        $.ajax({
            type: "POST",
            url: url + "/accounts/showSpecialityTrailAmount",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                       // $(val).closest('td').prev().attr('colspan', 6)
                    });
                }
                if($("#expand_all").prop("checked")){
                            expand_all(inc);
            }
        }
        });
        inc++;


}
}
