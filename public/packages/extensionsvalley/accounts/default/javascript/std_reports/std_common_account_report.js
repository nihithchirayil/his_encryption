var arr = new Array();
var arr1 = new Array();
var route_value = $('#route_value').val();
var base_url = $("#base_url").val();
var send_url = base_url + '/' + route_value;
$(document).ready(function () {

    var table_company_code = $("#table_company_code").val();
    if (table_company_code == 'DAYAH') {
        $('.datepicker').datetimepicker({
            format: 'MMM-DD-YYYY',
            minDate: '04-01-2022',
        });
    } else {
        $('.datepicker').datetimepicker({
            format: 'MMM-DD-YYYY',
        });
    }
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    $('.datepicker_month').datetimepicker({
        format: 'MMM-YYYY',
    });
});
//SEARCH DATA
function ResultData() {
    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var consolidate_report = $('#consolidate_report').val();
    if(consolidate_report != undefined){
        if($('#consolidate_report').is(':checked')){
            consolidate_report = 1;
        }else{
            consolidate_report =0;
        }
    }
    var parm = {bill_date_from: from_date, bill_date_to: to_date,consolidate_report:consolidate_report};
    $.ajax({
        type: "POST",
        url: send_url,
        data: parm,
        beforeSend: function () {
            $('#print_results').addClass('disabled');
            $('#csv_results').addClass('disabled');
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});

        },
        success: function (html) {
            if(html == 2){
                Command: toastr["error"]("No data to show!");
                $('#ResultDataContainer').css('display', 'hide');
            }else{
            $('#ResultsViewArea').html(html);
            //$('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            setTimeout(function(){

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });

                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.theadscroll');
                    }
                });

        },3000);
        }
        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}


function excelleExpCustom(custom_name) {
    var element = document.createElement('a');
            element.setAttribute('href', 'data:application/vnd.ms-excel,' + encodeURIComponent($('#ResultsViewArea').html()));
            element.setAttribute('download', custom_name);
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
   element.preventDefault();
}
function excelleExpGrp(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}
function datarst() {
    window.location.reload();
}
function exceller_template_styled_std(excel_name, colspan) {
    var style = "<style>.headerclass { color: black !important; } .sticky-col-cust{ background-color: white !important} .first-col{ background-color: white !important}</style>";
   var  template_date = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' + style + '</head><body><table>{table}</table></body></html>';
   var printhead = $("#hospital_address").val();
   var excel_data = atob(printhead);
   excel_data = excel_data.replace("colspan_no", colspan);
   var uri = 'data:application/vnd.ms  -excel;base64,',
       template = template_date,
       base64 = function (s) {
           return window.btoa(unescape(encodeURIComponent(s)))
       },
       format = function (s, c) {
           return s.replace(/{(\w+)}/g, function (m, p) {
               return c[p];
           })
       }
   var toExcel = document.getElementById("ResultsViewArea").innerHTML;
   var excel_data = excel_data + toExcel;

   var ctx = {
       worksheet: name || '',
       table: excel_data
   };
   var link = document.createElement("a");
   link.download = excel_name + ".xls";
   link.href = uri + base64(format(template, ctx))
   link.click();
}
function editLedgerHeder(booking_id){
    var url = $("#base_url").val();
    var urls = url + "/accounts/editLedgerBooking/"+booking_id+"/0";
    window.open(urls, '_blank');
}
