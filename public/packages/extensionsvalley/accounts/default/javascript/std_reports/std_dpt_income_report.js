function getDptSub(e, ledger_id, report_type) {
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var bill_date_to = $("#bill_date_to").val();

        var param = {ledger_id: ledger_id, bill_date_to: bill_date_to, first_td_text: first_td_text, report_type: report_type}

        $.ajax({
            type: "POST",
            url: base_url + "/accounts/getStdDptReportByGroup",
            data: param,
            beforeSend: function () {
                $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                if(data != '2'){
                $(e).closest('tr').after(data);
                }
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }

                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
                $('#ResultsViewArea').LoadingOverlay("hide");
            },
            complete: function () {

            }
        });
    }
}