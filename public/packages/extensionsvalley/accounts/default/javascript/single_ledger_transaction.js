var arr = new Array();
var voucher_no_arr = [];
$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    var table_company_code = $("#table_company_code").val();
    if(table_company_code == 'DAYAH'){
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate : '04-01-2022',
    });
}else{
       $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
}
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + encodeURIComponent(search_key) + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getgeneralReportData() {

    var url = route_json.getSingleledgertransactionreport;

    //-------filters---------------------------------------------

    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var ledger_id = $('#ledger_id_hidden').val();
    var dr_cr = $('#dr_cr').val();
    $("#exclude_option").prop('checked',false);
    $("#show_all_entry").prop('checked',false);
    voucher_no_arr.splice(0);

        if($('#opening_balance_nil').prop('checked') == true){
       var opening_balance_nil = 1;
    }else{
        var opening_balance_nil = 0;
    }
    if(ledger_id == 0){
       Command: toastr["warning"]("Plese select the account!");
       return false;
    }
    var parm = { bill_date_from: from_date,bill_date_to:to_date,ledger_id:ledger_id,dr_cr:dr_cr,opening_balance_nil:opening_balance_nil };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
           $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');
            if($("#show_all_narration").prop("checked")){
                $('.checkAllNarration').attr('data-narations',1);
                $(".append_narration").css('display','');
            } else {
                $('.checkAllNarration').attr('data-narations',2);
                $(".append_narration").css('display','none');
            }


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}




function datarst() {

    var current_date = $('#current_date').val();

    $("#depatment_list").select2('val', '');
    $('#date_from').val(current_date);

}
function expandRow(e,id){
    var added=false;
$.map(arr, function(elementOfArray, indexInArray) {
if (elementOfArray.id == id) {
added = true;
$("."+id).remove();
arr = arr.filter(function( obj ) {
return obj.id !== id;
});
//arr.pop({id: id});
}
});
if (!added) {
arr.push({id: id});
var filters_list = new Object();
var filters_value = '';
var filters_id = '';

$('.filters').each(function () {
 filters_id = this.id;
 if (filters_id == 'bill_no_hidden') {
     filters_id = 'bill_no';
 }
 filters_value = $('#' + filters_id).val();
 filters_id = filters_id.replace('_hidden', '');

 if (filters_id == 'bill_date_from') {
     filters_id = 'from_date';
 }
 if (filters_id == 'bill_date_to') {
     filters_id = 'to_date';
 }
 if (filters_id == 'no_date') {
     if($('#' + filters_id).prop("checked") == true){
         filters_value = 1;
         filters_id = 'no_date';
     }else{
         filters_value = 0;
         filters_id = 'no_date';
     }
 }
 if (filters_id == 'no_opening') {
     if($('#' + filters_id).prop("checked") == true){
         filters_value = 1;
         filters_id = 'no_opening';
     }else{
         filters_value = 0;
         filters_id = 'no_opening';
     }
 }
 if (filters_id == 'show_naration') {
     if($('#' + filters_id).prop("checked") == true){
         filters_value = 1;
         filters_id = 'show_naration';
     }else{
         filters_value = 0;
         filters_id = 'show_naration';
     }
 }

 if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
     filters_list[filters_id] = filters_value;
 }
});
if (id != '0') {
         filters_value_det = id;
         filters_id_det = 'group_id';
     }
filters_list[filters_id_det] = filters_value_det;
filters_value_inc = 'include_cr_dr';
filters_id_inc = 'include_cr_dr';
filters_list[filters_id_inc] = filters_value_inc;
$("#remove_group_id_"+id).show();
$("#expand_group_id_"+id).hide();

var url = $("#base_url").val();
$.ajax({
         type: "GET",
         url: url + "/reports/common_row_expand",
         data: filters_list,
         beforeSend: function () {
         $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
          },
         success: function (data) {
           $(e).closest('tr').after(data);
         $.LoadingOverlay("hide");
         $(".theadfix_wrapper").floatThead('reflow');
         },
         complete: function () {
         }
     });
}
}
function getSingleLeaderDetails(e,ledger_id){

    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = 'Trasaction Data';
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name}

     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportData",
                data: param,
                beforeSend: function () {
                    $('#leder_name_header').html(ledger_name);
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $("#getLeaderDetailModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#getLeaderDetailData').html(data);
                    $.LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                    });
                },
                complete: function () {
                }
            });
}
function editLedgerHeder(booking_id,v_type){
    if(v_type=='Journal' || v_type == 3){
        var v_no = 3;
    }else{
        var v_no = 0;
    }
    var url = $("#base_url").val();
    var urls = url + "/accounts/editLedgerBooking/"+booking_id+"/"+v_no
    window.open(urls, '_blank');
}
function narationRow_old(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').remove();
             return false;
         }
    }
     $.ajax({
                type: "GET",
                url: url + "/accounts/show_naration_row/",
                data: {head_id:head_id},
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $(e).closest('tr').after('');
                    $(e).closest('tr').attr('data-narations',1);
                  $(e).closest('tr').after(data);
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $.LoadingOverlay("hide");
                }
            });
    }
    $(document).on("click", "#exclude_option", function(){
    if($(this).prop("checked")){
        $(".excludeItemButton").show();
    } else {
        $(".excludeItemButton").hide();
    }

});
function excludeItem(item,voucher_no){
    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = 'Trasaction Data';
    var ledger_id = $("#ledger_id_hidden").val();
    var dr_cr = $('#dr_cr').val();
    voucher_no_arr.push(voucher_no);
            if($('#opening_balance_nil').prop('checked') == true){
       var opening_balance_nil = 1;
    }else{
        var opening_balance_nil = 0;
    }
    var param={ledger_id:ledger_id,dr_cr:dr_cr,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name,voucher_no:voucher_no_arr,opening_balance_nil:opening_balance_nil}

     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportDataHide",
                data: param,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                $('#ResultsViewArea').html(data);
                    $.LoadingOverlay("hide");
                },
                complete: function () {
                $(item).parents('tr').hide();
                $('.'+$(item).parents('tr').attr('id')).remove();
                }
            });
}
function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
function color_row(e){
    if($(e).prop('checked') == true){
    $(e).closest('tr').css('background-color', '#f7cdcd');
    }else{
        $(e).closest('tr').css('background-color', '');
    }
}
$(document).on("click", "#show_all_narration", function(){
    if($(this).prop("checked")){
        $('.checkAllNarration').attr('data-narations',1);
        $(".append_narration").css('display','');
    } else {
        $('.checkAllNarration').attr('data-narations',2);
        $(".append_narration").css('display','none');
    }

});
$(document).on("click", "#show_all_entry", function(){
    if($(this).prop("checked")){
        AllEntryGeneralReportData();
    } else {
        getgeneralReportData();
    }
});
function narationRow(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').hide();
             $(e).closest('tr').attr('data-narations',2);
             return false;
         }
    }else{
        $(e).closest('tr').attr('data-narations',1);
        $(e).closest('tr').next('tr').show();
    }
}
function excelleExp(type, fn, dl) {
//     let tbl1 = document.getElementById("hospital_headers");
//   let tbl2 = document.getElementById("result_data_table");
//
//   let worksheet_tmp1 = XLSX.utils.table_to_sheet(tbl1);
//   let worksheet_tmp2 = XLSX.utils.table_to_sheet(tbl2);
//
//   let a = XLSX.utils.sheet_to_json(worksheet_tmp1, { header: 1 });
//   let b = XLSX.utils.sheet_to_json(worksheet_tmp2, { header: 1 });
//
//   a = a.concat(['']).concat(b)
//
//   let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true })
//
//   const new_workbook = XLSX.utils.book_new();
//   XLSX.utils.book_append_sheet(new_workbook, worksheet, "worksheet");
//   XLSX.writeFile(new_workbook, 'genereal_ledger.xls');
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table');

    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('genereal_ledger.' + (type || type)));
}
function exceller_template_ledger_report(excel_name, colspan) {
    var style = "<style>.headerclass { color: black !important; } .clr_class { color: black !important; } .hid_cls { display:none !important;} </style>";
    var  template_date = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' + style + '</head><body><table>{table}</table></body></html>';
    var printhead = $("#hospital_address").val();
    var excel_data = atob(printhead);
    excel_data = excel_data.replace("colspan_no", colspan);
    var uri = 'data:application/vnd.ms  -excel;base64,',
        template = template_date,
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
        var exTable = $('#print_data').clone();
            exTable.find('.hid_cls').remove();
    $("#exp_hide_div").html(exTable);
    var toExcel = document.getElementById("exp_hide_div").innerHTML;
    var excel_data = excel_data + toExcel;

    var ctx = {
        worksheet: name || '',
        table: excel_data
    };
    var link = document.createElement("a");
    link.download = excel_name + ".xls";
    link.href = uri + base64(format(template, ctx))
    link.click();
}
function print_generate_ledger(printData) {

    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }
    mywindow.document.write('<style>.table.no-border, .table.no-border td, .table.no-border th { border: 0; } .table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } .table_sm td span { line-height: 18px !important; } .table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } .table_head.tth { font-size: 12px !important; padding: 1px !important; } .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } .table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, .table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, .table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } .headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} th { background-color:#d9dfe0 !important;}.common_td_rules{ text-align: left !important; overflow: hidden !important;}.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; } .hid_cls{display:none}</style>');
    mywindow.document.write(showw + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
    return true;

}

function AllEntryGeneralReportData() {

    var url = $("#base_url").val();

    //-------filters---------------------------------------------

    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var ledger_id = $('#ledger_id_hidden').val();
    var dr_cr = $('#dr_cr').val();
    $("#exclude_option").prop('checked',false);
    voucher_no_arr.splice(0);

    if($('#opening_balance_nil').prop('checked') == true){
       var opening_balance_nil = 1;
    }else{
        var opening_balance_nil = 0;
    }
    if($('#show_all_entry').prop('checked') == true){
        var show_all_entry = 1;
     }else{
         var show_all_entry = 0;
     }
    if(ledger_id == 0){
       Command: toastr["warning"]("Plese select the account!");
       return false;
    }
    var parm = { bill_date_from: from_date,bill_date_to:to_date,ledger_id:ledger_id,dr_cr:dr_cr,opening_balance_nil:opening_balance_nil,show_all_entry:show_all_entry };
    $.ajax({
        type: "GET",
        url: url + "/accounts/showAllEntry",
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
           $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');
            if($("#show_all_narration").prop("checked")){
                $('.checkAllNarration').attr('data-narations',1);
                $(".append_narration").css('display','');
            } else {
                $('.checkAllNarration').attr('data-narations',2);
                $(".append_narration").css('display','none');
            }


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}

