batch_array = {};
delete_array = [];
is_deleted_row = 0;
$(document).ready(function () {
    var voucher_type_id_fcs = $("#voucher_type_id").val();
    if(voucher_type_id_fcs == 10){
        $("#purchase_invoice_no").focus();
    }else{
        $("#ledger_item_desc-0").focus();
    }
     $(document).on('keydown', function(event) {
       if (event.key == "Escape") {
           $(".ajaxSearchBox").hide();
       }
   });
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');

    //$('#menu_toggle').trigger('click');
    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });
    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    var table_company_code = $("#table_company_code").val();
    if(table_company_code == 'DAYAH'){
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate : '04-01-2022',
    });
}else{
       $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
}
    $('.date_time_picker').datetimepicker();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

     $(".selectdropdown").select2({
         placeholder: "",
         allowClear: true,
         width: '100%'
     });
     $(document).on('click', '.select_mat_type_input .mat_label', function(){
         $("select").select2("open");
     });

     $(document).on('click', '.mat_type_input, .mat_label', function(){
      $(this).addClass('active');
      $(this).find(".form-control").focus();
  });


     $(document).on('change', '.inputfile', function(e){
       var val = $(this).val();
       if (val==="") {
        $(this).next().removeClass('active');
    } else {
        $(this).next().addClass('active');
    }
});

     $('').trigger('select2:open');

     $(document).on('blur', '.mat_type_input input.form-control, .mat_type_input textarea.form-control', function(){
       if( !$(this).val() ) {
        $(this).parent().removeClass('active');
    }
});


     $(document).on('change', '.select_mat_type_input select', function(){

       $(this).parent().addClass('active');

       if ($(this).val() === "") {
        $(this).parent().addClass('remove_active');
    } else {
        $(this).parent().removeClass('remove_active');
    }

});
});

function select_item_desc(id, event,type,is_header=0) {
    var amount_header = $("#amount_header").val();
    var party_header = $("#ledger_id_head-0").val();
    var header_cr = $("#header_cr").val();
    if(id != "ledger_item_desc-0"){
    if(amount_header == '' || party_header == '' || header_cr == '-1'){
        toastr.warning("Please fill Header section");
        $("#"+id).val('');
        return false;
        }
    }
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
        var ajax_div = 'search_ledger_item_box-' + id;
        var ledger_desc = $('#ledger_item_desc-' + id).val();


        if (event.keyCode == 13) {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            return false;
            }
        else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
             var url = $("#ins_base_url").val();
            $.ajax({
                type: "GET",
                url: url + "/accounts/searchLedgerMaster",
                data: 'ledger_desc=' + encodeURIComponent(ledger_desc) + '&type='+type+'&search_ledger_desc=1&is_header='+is_header,
                beforeSend: function () {
                    $("#" + ajax_div).css('top', '');
                    if(is_header == 1){
                        $("#" + ajax_div).css('margin-left', '-5px');
                    }else{
                        $("#" + ajax_div).css('margin-left', '370px');
                    }
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();

                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();


                    if(parseFloat($("#" + ajax_div+" li").size()) > 15 && id != '0'){
                    $("#" + ajax_div).css('top', '-12vh');
                    $("#" + ajax_div).css('position', 'absolute');
                    }else{
                         $("#" + ajax_div).css('top', '');
                    }

                    $("#" + ajax_div).css('z-index', '1004');
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}


function fillLedgerValues(e, ledger_name, id,ledger_group_id,ledger_code,current_balance,allow_multiple_entry) {

    var cls = "'amount'";
    var cur_dif = $("#ttl_diff").val();
    var voucher_type_id = $("#voucher_type_id").val();
    var selected_voucher_type = '1';
    var enable_multiple_naration = $("#enable_multiple_naration").val();
    console.log(enable_multiple_naration);
    if(voucher_type_id != '' || voucher_type_id !== undefined){
        if(voucher_type_id == '9'){
           var selected_voucher_type =  '9';
        }
    }
if(allow_multiple_entry != '1'){
    var id_exts = 0;
    var ledger_id_head_0 = $("#ledger_id_head-0").val();
    if(ledger_id_head_0 == id){
        id_exts = 1;
    }
   $('.accounts_tbody tr').each(function () {
            id_temp = $(this).find("input[name='ledger_id[]']").val();
           if(id == id_temp){
                id_exts = 1;
           }
   });
   if(id_exts == 1){
       toastr.error("Selected Ledger Is Already Existing!");
       return false;
   }
}
    $(e).closest("tr").find("input[name='ledger[]']").val(ledger_name.replace(/&amp;/g, '&'));
    $(e).closest("tr").find("input[name='ledger_id[]']").val(id);
    if(parseFloat(current_balance) < 0){
    $(e).closest("tr").find("input[name='acc_no[]']").val((current_balance*-1)+' .Dr');
    }else{
        if(current_balance==''){
            current_balance = 0;
        }
    $(e).closest("tr").find("input[name='acc_no[]']").val(current_balance+' .Cr');
    }
    $(e).closest("tr").find("input[name='group_id[]']").val(ledger_group_id);
    if(cur_dif > 0){
    $(e).parents("tr").find("input[name='amnt[]']").val(cur_dif);
    }
    $(e).parents("tr").find("input[name='amnt[]']").focus();
    $(e).closest("div").hide();
    var divElements = document.querySelectorAll('.search_ledger_item_box');
    var row_id = divElements.length;
    if(is_deleted_row == 1){
        row_id = 501;
    }
    var row_id = parseFloat(row_id) + parseFloat(divElements.length);;
    var dr_cr_value = $("#detail_cr_1").val();
    if(dr_cr_value == undefined){
        header_cr = $("#header_cr").val();
        if(header_cr == 'cr'){
            dr_cr_value = 'dr';
        }else{
            dr_cr_value = 'cr';
        }
    }
    response = '<tr class="'+dr_cr_value+'">\n\
                    <td>\n\
                        <input type="text" value="" name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,'+selected_voucher_type+')" id="ledger_item_desc-' + row_id + '">\n\
                        <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-' + row_id + '">\n\
                        <input type="hidden" value="" name="group_id[]" autocomplete="off"  readonly=""  class="form-control" id="group_' + row_id + '">\n\
                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-' + row_id + '" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;\n\
                        margin: -2px 0px 0px 0px;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"> \n\
                        </div>\n\
                    </td>\n\
                    <td><input type="text" readonly="" value="" name="acc_no[]" style="border:none">\n\
                       </td>\n\
                    <td>\n\
                        <input type="text" value="" name="amnt[]" class="form-control amount" onkeyup="calculate_table_total(' + cls + ');number_validation(this)" onblur="calculate_table_total(' + cls + ');" id="amount_' + row_id + '"> \n\
                    </td>\n\
                    <td>\n\
                        <select name="detail_cr[]" class="form-control detail_cr" id="detail_cr_' + row_id + '" onchange="current_dr_cr(this);calculate_table_total(' + cls + ');">\n\
                                <option value="dr">Debit</option>\n\
                                <option value="cr">Credit</option>\n\
                    </select>\n\
                    </td>';
                    if(enable_multiple_naration==1){
                        response +=     '<td>\n\
                        <input type="text" value="" name="multiple_naration[]" class="form-control" > \n\
                    </td>';
                    }

                    response +=  '<td>\n\
                        <i class="fa fa-trash-o delete_font_aws delete_return_entry_added" style="cursor:pointer"></i>\n\
                    </td>\n\
            </tr>';
    var row_ck = 0;
        $('#payment_receipt_tbody tr').each(function () {
        if ($(this).find('input[name="ledger[]"]').val() ==''){
            row_ck = 1;
        }
    });
    if(row_ck == 0){
    $('#payment_receipt_tbody').append(response);
    if(voucher_type_id==18 || voucher_type_id==30){
        $(e).closest('tr').addClass(dr_cr_value);
    }

    $(".theadfix_wrapper").floatThead('reflow');
    calculate_table_total('amount');
    }
    $("#detail_cr_"+row_id).val(dr_cr_value);
    $('.ajaxSearchBox').hide();
    is_deleted_row = 0;
}
function fillParty(e,ledger_name,id,ledger_group_id,ledger_code,current_balance){
    var id_exts = 0;
   $('.accounts_tbody tr').each(function () {
            id_temp = $(this).find("input[name='ledger_id[]']").val();
           if(id == id_temp){
                id_exts = 1;
           }
   });
   if(id_exts == 1){
       toastr.error("Selected Ledger Is Already Existing!");
       return false;
   }
    $("#ledger_item_desc-0").val(ledger_name);
    $("#ledger_id_head-0").val(id);
    $("#ledger_group_head-0").val(ledger_group_id);
    if(parseFloat(current_balance) < 0){
     $("#ledger_code").html((current_balance*-1)+' .Dr');
    }else{
        if(current_balance == ''){
            current_balance = 0;
        }
    $("#ledger_code").html(current_balance+' .Cr');
    }
    $("#amount_header").focus();
    $('.ajaxSearchBox').hide();
}
function fillJournalValues(e, ledger_name, id,ledger_group_id,ledger_code) {

    var cls = "'amount'";
    var journal = "'journal'";
    var journal_all = "'All'";
    $(e).closest("tr").find("input[name='ledger[]']").val(ledger_name.replace(/&amp;/g, '&'));
    $(e).closest("tr").find("input[name='ledger_id[]']").val(id);
    $(e).closest("tr").find("input[name='acc_no[]']").val(ledger_code);
    $(e).closest("tr").find("input[name='group_id[]']").val(ledger_group_id);

    $(e).closest("div").hide();
    var divElements = document.querySelectorAll('.search_ledger_item_box');
    var row_id = divElements.length;
    var row_id = parseFloat(row_id) + 1;

    response = '<tr>\n\
                        <td> \n\
                        <input type="text" value="" name="description[]" class="form-control" ></td>\n\
                        <td>\n\
                        <input type="text" value="" name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,'+journal+','+journal_all+')" id="ledger_item_desc-' + row_id + '">\n\
                        <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-' + row_id + '">\n\
                        <input type="hidden" value="" name="group_id[]" autocomplete="off"  readonly=""  class="form-control" id="group_' + row_id + '">\n\
                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-' + row_id + '" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;\n\
                        margin: -2px 0px 0px 0px;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"> \n\
                        </div>\n\
                    </td>\n\
                    <td><input type="text" readonly="" value="" name="acc_no[]" style="border:none"></td>\n\
                    <td>\n\
                        <input type="text" value="" name="dr_amnt[]" class="form-control amount dra_amnt" onkeyup="calculate_dr_total(this);" onblur="calculate_dr_total(this);" id=dr_"amount_' + row_id + '"> \n\
                    </td>\n\
                    <td>\n\
                        <input type="text" value="" name="cr_amnt[]" class="form-control amount cra_amnt" onkeyup="calculate_cr_total(this);" onblur="calculate_cr_total(this);" id=cr_"amount_' + row_id + '"> \n\
                    </td>\n\
            </tr>';
    var row_ck = 0;
        $('#payment_receipt_tbody tr').each(function () {
        if ($(this).find('input[name="ledger[]"]').val() ==''){
            row_ck = 1;
        }
    });
    if(row_ck == 0){
    $('#payment_receipt_tbody').append(response);
    $(".theadfix_wrapper").floatThead('reflow');
    calculate_table_total('amount');
    }
}
function select_invoice(id, event) {

    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        var ajax_div = 'search_invoice_box-0';
        var ledger_desc = $('#purchase_invoice_no').val();


    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
             var url = $("#ins_base_url").val();
            $.ajax({
                type: "GET",
                url: url + "/accounts/searchInvoice",
                data: 'invoice=' + ledger_desc + '&search_invoice_desc=1',
                beforeSend: function () {
                    $("#" + ajax_div).css('top', '');
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();

                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillInvoice(e,invoice_no,ledger_name,ledger_id,group_id,amount){
    $("#ledger_item_desc-0").val(ledger_name);
    $("#ledger_id_head-0").val(ledger_id);
    $("#ledger_group_head-0").val(group_id);
    $("#purchase_invoice_no").val(invoice_no);
    $("#amount_header").val(amount);
    $("#amount_header").focus();
}
function amount_fetch(e){
    var amount  = parseFloat($(e).val());
    if($("#amount_1").val() == 0 || $("#amount_1").val() == ''){
   // $("#amount_1").val(amount.toFixed(2));
    }

}
function calculate_table_total(cls) {
    var ttl_amnt_cr =0; var ttl_amnt_dr =0;var amnt_cr = 0; var amnt_dr = 0;var k = 1;
    $("." + cls).each(function () {
        var tr_row = ($(this). closest('tr').attr('class'));
        if(tr_row == 'cr'){
            var amnt_cr = $(this).closest('tr').find('.amount').val(); console.log(amnt_cr);
            if(amnt_cr != '' && amnt_cr != undefined){
            ttl_amnt_cr = parseFloat(ttl_amnt_cr) + parseFloat(amnt_cr);
            }
        }else{
            var amnt_dr = $(this).closest('tr').find('.amount').val();//$("#amount_"+k).val(); console.log(amnt_dr);
            if (amnt_dr != '' && amnt_dr != undefined) {
            ttl_amnt_dr = parseFloat(ttl_amnt_dr) + parseFloat(amnt_dr);
        }

    }
    k++;
    });
    var header_cr = $("#header_cr").val();
    if(header_cr == 'cr'){
        ttl_amnt_cr = parseFloat(ttl_amnt_cr) + parseFloat($("#amount_header").val());
        $("#total_amount_bottom").val(ttl_amnt_dr.toFixed(2));
    }else{
        ttl_amnt_dr = parseFloat(ttl_amnt_dr) + parseFloat($("#amount_header").val());
         $("#total_amount_bottom").val(ttl_amnt_cr.toFixed(2));
    }
    if ($("#total_amount_bottom").val() != '' && $("#amount_header").val() != '') {
        var cur_difference = parseFloat(ttl_amnt_cr) - parseFloat(ttl_amnt_dr).toFixed(2);
        if($("#ledger_item_desc-1").val() != ''){
        $("#ttl_diff").val(cur_difference.toFixed(2));
        $("#ttl_dif_label").html('');
        $("#ttl_dif_label").html(parseFloat(cur_difference, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
    }
    }

}
function calculate_cr_total(e) {
    var ttl_amntq = 0;
    $('.cra_amnt').each(function () {
        var amnt = $(this).val();
        if (amnt != '') {
            ttl_amntq = parseFloat(ttl_amntq) + parseFloat(amnt);
        }
    });
    $("#total_cr_amount_bottom").val(ttl_amntq.toFixed(2));
    if ($("#total_cr_amount_bottom").val() != '' && $("#total_dr_amount_bottom").val() != '') {
        var cur_difference = parseFloat($("#total_cr_amount_bottom").val()).toFixed(2) - parseFloat($("#total_dr_amount_bottom").val()).toFixed(2);
        $("#ttl_dr_cr_dif").val(cur_difference.toFixed(2));
        $("#ttl_dr_cr_dif_label").html('');

        $("#ttl_dr_cr_dif_label").html(parseFloat(cur_difference, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
    }

}
function calculate_dr_total(e) {
    var ttl_amnt = 0;
    $('.dra_amnt').each(function () {
        var amnt = $(this).val();
        if (amnt != '') {
            ttl_amnt = parseFloat(ttl_amnt) + parseFloat(amnt);
        }

    });
    $("#total_dr_amount_bottom").val(ttl_amnt.toFixed(2));
    if ($("#total_cr_amount_bottom").val() != '' && $("#total_dr_amount_bottom").val() != '') {
        var cur_difference = parseFloat($("#total_cr_amount_bottom").val()).toFixed(2) - parseFloat($("#total_dr_amount_bottom").val()).toFixed(2);
        $("#ttl_dr_cr_dif").val(cur_difference.toFixed(2));
        $("#ttl_dr_cr_dif_label").html('');

        $("#ttl_dr_cr_dif_label").html(parseFloat(cur_difference, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
    }

}

$("#payment_receipt_tbody").on('click', '.delete_return_entry_added', function () {
    $(this).closest('tr').remove();
    calculate_table_total("amount");
    is_deleted_row = 1;
});
function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function deleteBookingEntry(id,e) {
    if (confirm("Are you sure you want to Delete ?")) {
        delete_array.push(id);
        $(e).closest('tr').remove();
        console.log(delete_array);
//        var params = {delete_ledger_list: 1, detail_id: id};
//         var url = $("#ins_base_url").val();
//        $.ajax({
//            type: "GET",
//                url: url + "/accounts/deleteBookingEntry",
//            data: params,
//            beforeSend: function () {
//                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
//            },
//            success: function (data) {
//                if (data == 1) {
//                    calculate_table_total("amount");
//                    $.LoadingOverlay("hide");
//                    $('#edit_ledger_booking_' + id).remove();
//                    toastr.success('Deleted Succcessfuly !!');
//                }
//            },
//            complete: function () {
//
//            }
//        });
calculate_table_total('amount');
    }
}

function saveForm(save_type) {

    var db_total = $("#amount_header").val();
    var cd_total = $("#total_amount_bottom").val();
    var ttl_diff = parseFloat(db_total) - parseFloat(cd_total);
    var header_ledger_id = $("#ledger_id_head-0").val();
    var header_group_id = $("#ledger_group_head-0").val();
    var ledger_name = $("#ledger_item_desc-0").val();
    if($("#purchase_invoice_date").val() == ''){
        toastr.error('Please select date...');
        return false;
    }
    //$("#ttl_diff").val(ttl_diff.toFixed(2));
    var header_cr = $("#header_cr").val();
    if(header_cr < 0){
        bootbox.alert("Plese select Debit or Credit");
        return false;
    }
//    if (parseFloat(cd_total) != parseFloat(db_total)) {
//        bootbox.alert("Debit and Credit totals are not equal");
//        return false;
//    }
    if($("#ttl_diff").val() != 0){
        bootbox.alert("Debit and Credit totals are not equal");
        return false;
    }
   var filled_item_desc_count = $('input[name="ledger[]"]').filter((i, el) => el.value.trim() != '').length;
   var filled_item_desc_id_count = $('input[name="ledger_id[]"]').filter((i, el) => el.value.trim() != '').length;
   var filled_amount_count = $('input[name="amnt[]"]').filter((i, el) => el.value.trim() != '').length;
   if(parseFloat(filled_amount_count) != parseFloat(filled_item_desc_count)){
       bootbox.alert("Plese fill ledger details for each amount entry");
       return false;
   }
   if(parseFloat(filled_item_desc_id_count) != parseFloat(filled_item_desc_count)){
       bootbox.alert("Account should selected from the list!!!");
       return false;
   }
   if(parseFloat(filled_amount_count) == 0 && parseFloat(filled_item_desc_count)==0 ){
       bootbox.alert("No data selected for update");return false;
   }

    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/accounts/saveBookingEntry",
        data: $("#form").serialize(),
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            if(save_type==1){
            $('#add_share_holder_spin').removeClass('fa fa-save');
            $('#add_share_holder_spin').addClass('fa fa-spinner fa-spin');
        }else{
            $('#add_share_holder_spin_t').removeClass('fa fa-thumbs-up');
            $('#add_share_holder_spin_t').addClass('fa fa-spinner fa-spin');
        }
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            if (data == 1) {
                toastr.success('Successfully Added...');
            }else if(data == 3){
                toastr.warning('Same invoice exist...');
            }else {
                toastr.error('Error Occured Please try again later...');
            }
            if(save_type==1){
            setTimeout(function () {
                backToList();
            },300);
            }else{
            setTimeout(function () {
                $('#add_share_holder_spin_t').removeClass('fa fa-spinner fa-spin');
                $('#add_share_holder_spin_t').addClass('fa fa-thumbs-up');
                pageRefresh(header_ledger_id,header_group_id,ledger_name);
            },300);
            }
        },
        complete: function () {
            $('#add_share_holder_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin').addClass('fa fa-save');

        }
    });
}
/**
 *
 * FOR UPDATE LEDGER BOOKING
 */
function saveJournalForm(save_type) {
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/accounts/saveJournalBookingEntry",
        data: $("#form").serialize(),
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            if(save_type==1){
            $('#add_share_holder_spin').removeClass('fa fa-save');
            $('#add_share_holder_spin').addClass('fa fa-spinner fa-spin');
        }else{
            $('#add_share_holder_spin_t').removeClass('fa fa-thumbs-up');
            $('#add_share_holder_spin_t').addClass('fa fa-spinner fa-spin');
        }
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            if (data == 1) {
                toastr.success('Successfully Added...');
            } else {
                toastr.error('Error Occured Please try again later...');
            }
            $('#add_share_holder_spin_t').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin_t').addClass('fa fa-thumbs-up');
            $('#payment_receipt_tbody tr').each(function () {
            if ($(this).find('input[name="ledger[]"]').val() !=''){
                    $(this).find('input[name="ledger[]"]').prop('readonly', true);
                    $(this).find('input[name="description[]"]').prop('readonly', true);
                    $(this).find('input[name="dr_amnt[]"]').prop('readonly', true);
                    $(this).find('input[name="cr_amnt[]"]').prop('readonly', true);
                    $(this).find('input[name="ledger[]"]').attr('name', 'posted_ledger');
                    $(this).find('input[name="description[]"]').attr('name', 'posted_description');
                    $(this).find('input[name="dr_amnt[]"]').attr('name', 'posted_dr_amnt');
                    $(this).find('input[name="cr_amnt[]"]').attr('name', 'posted_cr_amnt');
                    $(this).find('input[name="ledger_id[]"]').attr('name', 'posted_ledger_id');
                    $(this).find('input[name="group_id[]"]').attr('name', 'posted_group_id');
                    $("#ttl_dr_cr_dif_label").val(0);
                    $("#total_cr_amount_bottom").val(0);
                    $("#total_dr_amount_bottom").val(0);
                    $("#ttl_dr_cr_dif").val(0);
                }
            });
            if(save_type==1){
                backToList();
            }else{
                window.location.reload();
            }
        },
        complete: function () {
            $('#add_share_holder_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin').addClass('fa fa-save');

        }
    });
}
function updateForm(save_type) {
   var db_total = $("#amount_header").val();
    var cd_total = $("#total_amount_bottom").val();
    var ttl_diff = parseFloat(db_total) - parseFloat(cd_total);
    //$("#ttl_diff").val(ttl_diff);

//    if (parseFloat(cd_total) != parseFloat(db_total)) {
//        bootbox.alert("Debit and Credit totals are not equal");
//        return false;
//    }
    if($("#ttl_diff").val() != 0){
        bootbox.alert("Debit and Credit totals are not equal");
        return false;
    }
    var filled_item_desc_count = $('input[name="ledger[]"]').filter((i, el) => el.value.trim() != '').length;
    var filled_amount_count = $('input[name="amnt[]"]').filter((i, el) => el.value.trim() != '').length;
    var filled_item_desc_id_count = $('input[name="ledger_id[]"]').filter((i, el) => el.value.trim() != '').length;
   if(parseFloat(filled_amount_count) != parseFloat(filled_item_desc_count)){
       bootbox.alert("Plese fill ledger details for each amount entry");
       return false;
   }
   if(parseFloat(filled_amount_count) == 0 && parseFloat(filled_item_desc_count)==0 ){
       bootbox.alert("No data selected for update");
       return false;
   }
      if(parseFloat(filled_item_desc_id_count) != parseFloat(filled_item_desc_count)){
       bootbox.alert("Account should selected from the list!!!");
       return false;
   }

    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/accounts/updateBookingEntry",
        data: $("#form").serialize() + '&delete_array=' + JSON.stringify(delete_array),
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            $('#add_share_holder_spin').removeClass('fa fa-save');
            $('#add_share_holder_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            if (data == 1) {
                toastr.success('Successfully Updated...');
            } else {
                toastr.error('Error Occured Please try again later...');
            }
                backToList();

        },
        complete: function () {
            $('#add_share_holder_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin').addClass('fa fa-save');

        }
    });
}
function updateJournalForm(save_type) {


    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/accounts/updateJournalBookingEntry",
        data: $("#form").serialize(),
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            if(save_type==1){
            $('#add_share_holder_spin').removeClass('fa fa-save');
            $('#add_share_holder_spin').addClass('fa fa-spinner fa-spin');
        }else{
            $('#add_share_holder_spin_t').removeClass('fa fa-thumbs-up');
            $('#add_share_holder_spin_t').addClass('fa fa-spinner fa-spin');
        }
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            if (data == 1) {
                toastr.success('Successfully Added...');
            } else {
                toastr.error('Error Occured Please try again later...');
            }
            $('#add_share_holder_spin_t').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin_t').addClass('fa fa-thumbs-up');
            $('#payment_receipt_tbody tr').each(function () {
            if ($(this).find('input[name="ledger[]"]').val() !=''){
                    $(this).find('input[name="ledger[]"]').prop('readonly', true);
                    $(this).find('input[name="description[]"]').prop('readonly', true);
                    $(this).find('input[name="dr_amnt[]"]').prop('readonly', true);
                    $(this).find('input[name="cr_amnt[]"]').prop('readonly', true);
                    $(this).find('input[name="ledger[]"]').attr('name', 'posted_ledger');
                    $(this).find('input[name="description[]"]').attr('name', 'posted_description');
                    $(this).find('input[name="dr_amnt[]"]').attr('name', 'posted_dr_amnt');
                    $(this).find('input[name="cr_amnt[]"]').attr('name', 'posted_cr_amnt');
                    $(this).find('input[name="ledger_id[]"]').attr('name', 'posted_ledger_id');
                    $(this).find('input[name="group_id[]"]').attr('name', 'posted_group_id');
                    $("#ttl_dr_cr_dif_label").val(0);
                    $("#total_cr_amount_bottom").val(0);
                    $("#total_dr_amount_bottom").val(0);
                    $("#ttl_dr_cr_dif").val(0);
                }
            });
            if(save_type==1){
                backToList();
            }
        },
        complete: function () {
            $('#add_share_holder_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin').addClass('fa fa-save');

        }
    });
}
function backToList() {
    var url = $("#ins_base_url").val();
    var voucher_type_list = $("#voucher_type_id").val();
    window.location = url + "/accounts/backToList/"+voucher_type_list;
}
        /**
         *
         * @param {type} e
         * @returns {undefined}
         * SHOWS MODAL BOX FOR NEW LEDGER ADD
         */
function saveNewLedger(e){
    var led_id = $(e).parents("tr").find('.ledger_item_desc').attr('id');
    var led_name = $(e).parents("tr").find('.ledger_item_desc').val();
    var row_ids = (led_id).split('-');
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "GET",
        url: url + "/master/list_ledger_master",
        data: {data:1},
        beforeSend: function () {
             $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            $('#add_new_ledger_spin').removeClass('fa fa-plus');
            $('#add_new_ledger_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                var html_content = $(data).find('#ledger_master_form_whole');
                $("#append_ledger_master").html(html_content);
                $("#ledger_name_update").val(led_name);
                $("#led_cancel_btn").hide();
                $("#from_booking_entry").val('1');
                $("#row_ids").val(led_id);
                 $('#ledger_master_form_whole').removeClass('col-md-4 padding_sm');
                $("#search_ref_modal").modal('show');
                $.LoadingOverlay("hide");
            }
        },
        complete: function () {
            $('#add_new_ledger_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_new_ledger_spin').addClass('fa fa-plus');
            $(".ajaxSearchBox").hide();
        }
    });
}
   function select_group_asset(id, event) {
    var url = $("#ins_base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        var ajax_div = 'group_asset_name_box-1';
        var ledger_desc = $('#group_asset_name').val();

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: url + "/master/list_asset_group",
                data: 'search_group_asset=' + ledger_desc,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillAssetGroupValues(e, ledger_name, id,typ,is_asset) {
      event.preventDefault();
      $("#group_asset_name").val(ledger_name.replace(/&amp;/g, '&'));
      $("#group_asset_id").val(id);
      $("#is_asset_etry").val(is_asset);
      if(is_asset == 0){
    $(".credit_div").show();
 }else{
        $(".credit_div").hide();
        $("#credit_days").val('');
        $("#credit_amount").val('');
        $("input[type=radio][name=pl_type]").prop('checked', false);
    }
}
function showCreditData(e){
    if($(e).val() > 0){
        $(".credit_div").show();
    }else{
        $(".credit_div").hide();
        $("#credit_days").val('');
        $("#credit_amount").val('');
        $("input[type=radio][name=pl_type]").prop('checked', false);
    }
}
function saveLedgerMaster(){
   var list_url = $("#ins_base_url").val();

   var url = $('#ledger_master_form').attr('action');
   var led_id = $("#row_ids").val();
   var row_ids = (led_id).split('-');
   if($("#ledger_name_update").val()==''){
       alert("Please enter Ledger name");
       return false;
   }
   if($("#group_asset_id").val()=='' || $("#group_asset_name").val()==''){
       alert("Please select group");
       return false;
   }
  $.ajax({
        type: "GET",
        url: url ,
        data: $("#ledger_master_form").serialize(),
        beforeSend: function () {
             if($("#from_booking_entry").val() == 1){
                 $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
             }
            $('#add_new_ledger_spin').removeClass('fa fa-save');
            $('#add_new_ledger_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var json_data = JSON.parse(data);
            if (json_data.success == 2) {
                 toastr.error('Default Payment Bank already selected !...');
            }
             if (json_data.success == 3) {
                 toastr.error('Default Receive Bank already selected !...');
            }
            if (json_data.success == 4) {
                 toastr.error('Ledger name exist !...');
            }
            if (json_data.success == 0) {
                 toastr.error('Error occured !...');
            }
            if (json_data.success == 1) {
                toastr.success(json_data.message);
                if($("#from_booking_entry").val() == 1){
                    var row_idss = row_ids[1];
                    var up_below_part = row_ids[0]
                    if(up_below_part == 'ledger_item_desc_cd'){
                    $("#ledger_item_desc_cd-"+row_idss).val(json_data.ledger_name);
                    $("#ledger_item_desc_hidden_cd-"+row_idss).val(json_data.ledger_id);
                    $("#group_id_cd_"+row_idss).val(json_data.group_id);
                    fill_newrow();

                    }else{
                    $("#ledger_item_desc-"+row_idss).val(json_data.ledger_name);
                    $("#ledger_item_desc_hidden-"+row_idss).val(json_data.ledger_id);
                    $("#group_id_"+row_idss).val(json_data.group_id);
                     fillLedgerValues('',json_data.ledger_name,json_data.ledger_id,json_data.group_id);
                }
                    $.LoadingOverlay("hide");
                    $("#search_ref_modal").modal('hide');
                }else{
                window.location = list_url + "/master/list_ledger_master";
            }
            }
        },
        complete: function () {
            $('#add_new_ledger_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_new_ledger_spin').addClass('fa fa-save');
        }
    });
}

function pageRefresh(header_ledger_id,header_group_id,ledger_name){
var enable_save_continue = $("#enable_save_continue").val();
if(enable_save_continue==1){
    var bank_voucher_generated = $("#bank_voucher_generated").val();
    var voucher_type_id = $("#voucher_type_id").val();
    if(bank_voucher_generated==1){
        generateBankVoucherForRepeat(header_ledger_id);
        $("#ledger_id_head-0").val(header_ledger_id);
        $("#ledger_group_head-0").val(header_group_id);
        $("#ledger_item_desc-0").val(ledger_name);
    }else{
        generateVoucherForRepeat(voucher_type_id);
    }
    }else{
        window.location.reload();
    }

}
function widhAdjust(v){
    if(v==1){
         $("#th_ref").css("width","11%");
         $("#th_nar").css("width","37%");
         $(".reference_no").css("height", "26px");
    }else{
    $("#th_nar").css("width","11%");
    $("#th_ref").css("width","37%");
    }
    console.log(v);
    $(".theadfix_wrapper").floatThead('reflow');
}
function previousLedgerHistory(id,cr_dr_val){
     var url = $("#ins_base_url").val();
        $.ajax({
                type: "GET",
                url: url + "/master/select_previous_booking",
                data: 'ledger_id=' + id+'&cr_dr='+cr_dr_val,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                 },
                success: function (data) {
                var json_data = JSON.parse(data);
                $.LoadingOverlay("hide");
                if(parseFloat(json_data.cnt) > 0){
                    for(var i=0; i < 1;i++){
                        $('#payment_receipt_tbody_two tr').each(function () {
                            if ($(this).find('input[name="ledger_cd[]"]').val() ==''){
                                var ledger_name_id =($(this).find('input[name="ledger_cd[]"]').attr("id"));
                                var ledger_id =($(this).find('input[name="ledger_id_cd[]"]').attr("id"));
                                var ledger_group_id =($(this).find('input[name="group_id_cd[]"]').attr("id"));
                                $("#"+ledger_name_id).val(json_data.dtls[i].ledger_name);
                                $("#"+ledger_id).val(json_data.dtls[i].ledger_id);
                                $("#"+ledger_group_id).val(json_data.dtls[i].group_id);
                            }
                        });
                        fill_newrow();
                    }
                }
                },
                complete: function () {
                }
            });
}
function drCrChange(e){
    $('#pay_rec_id tr').removeClass();
    if(e=='dr'){
        $(".detail_cr").val('cr');
        $('#pay_rec_id tr').addClass('cr');
    }else{
        $(".detail_cr").val('dr');
        $('#pay_rec_id tr').addClass('dr');
    }
    calculate_table_total('amount');
}
function current_dr_cr(e){
    $(e).closest('tr').removeClass();
    $(e).closest('tr').addClass($(e).val());
    calculate_table_total('amount');
}
$(document).on('click', '.smooth_scroll', function (event) {
    event.preventDefault();
    $('.combined_theadscroll').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});
function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}
function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var item_list = $("#" + ajax_div).find('li');
    var selected = item_list.filter('.liHover');
    if (event.keyCode === 13){
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    item_list.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current_item = item_list.eq(0);
        } else {
            current_item = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current_item = item_list.last();
        } else {
            current_item = selected.prev();
        }
    }
    current_item.addClass('liHover');
}
function clear_search() {
    var cur_url = window.location.href.split('?')[0];
    window.location.replace(cur_url);
}
//$('body').click(function(){
//    $('.ajaxSearchBox').hide();
//});

function refreshAllData(){
    window.location.reload();
}
function printAllData(){


    var db_total = $("#amount_header").val();
    var cd_total = $("#total_amount_bottom").val();
    var ttl_diff = parseFloat(db_total) - parseFloat(cd_total);
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/accounts/printBookingEntry",
        data: $("#form").serialize(),
        beforeSend: function () {
        },
        success: function (data) {
                var popupWin = window.open('', 'my div', 'height=3508,width=2480');
                popupWin.document.write('<style>@page</style>');
                popupWin.document.write(data);
                popupWin.document.close();
                popupWin.focus();
                popupWin.print()
                popupWin.close();
                return true;
        },
        complete: function () {


        }
    });
}
function appendRawNew(){
    var cls = "'amount'";
    var enable_multiple_naration = $("#enable_multiple_naration").val();
    var voucher_type_id = $("#voucher_type_id").val();
    var selected_voucher_type = '1';
    if(voucher_type_id != '' || voucher_type_id !== undefined){
        if(voucher_type_id == '9'){
           var selected_voucher_type =  '9';
        }
    }
    var divElements = document.querySelectorAll('.search_ledger_item_box');
    var row_id = divElements.length;
    if(is_deleted_row == 1){
        row_id = 501;
    }
    var row_id = parseFloat(row_id) + parseFloat(divElements.length);
    var dr_cr_value = $("#detail_cr_1").val();
    if(dr_cr_value == undefined){
        header_cr = $("#header_cr").val();
        if(header_cr == 'cr'){
            dr_cr_value = 'dr';
        }else{
            dr_cr_value = 'cr';
        }
    }
    response = '<tr class="'+dr_cr_value+'">\n\
                    <td>\n\
                        <input type="text" value="" name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,'+selected_voucher_type+')" id="ledger_item_desc-' + row_id + '">\n\
                        <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-' + row_id + '">\n\
                        <input type="hidden" value="" name="group_id[]" autocomplete="off"  readonly=""  class="form-control" id="group_' + row_id + '">\n\
                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-' + row_id + '" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;\n\
                        margin: -2px 0px 0px 0px;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"> \n\
                        </div>\n\
                    </td>\n\
                    <td><input type="text" readonly="" value="" name="acc_no[]" style="border:none">\n\
                       </td>\n\
                    <td>\n\
                        <input type="text" value="" name="amnt[]" class="form-control amount" onkeyup="calculate_table_total(' + cls + ');number_validation(this)" onblur="calculate_table_total(' + cls + ');" id="amount_' + row_id + '"> \n\
                    </td>\n\
                    <td>\n\
                        <select name="detail_cr[]" class="form-control detail_cr" id="detail_cr_' + row_id + '" onchange="current_dr_cr(this);calculate_table_total(' + cls + ');">\n\
                                <option value="dr">Debit</option>\n\
                                <option value="cr">Credit</option>\n\
                    </select>\n\
                    </td>';
                    if(enable_multiple_naration==1){
                        response +=     '<td>\n\
                        <input type="text" value="" name="multiple_naration[]" class="form-control" > \n\
                    </td>';
                    }
                    response += '<td>\n\
                        <i class="fa fa-trash-o delete_font_aws delete_return_entry_added" style="cursor:pointer"></i>\n\
                    </td>\n\
            </tr>';
    var row_ck = 0;
        $('#payment_receipt_tbody tr').each(function () {
        if ($(this).find('input[name="ledger[]"]').val() ==''){
            row_ck = 1;
        }
    });
    if(row_ck == 0){
    $('#payment_receipt_tbody').append(response);
    $(".theadfix_wrapper").floatThead('reflow');
    calculate_table_total('amount');
    }
    $("#detail_cr_"+row_id).val(dr_cr_value);
    $('.ajaxSearchBox').hide();
    is_deleted_row = 0;
}
$("#purchase_invoice_date").on("blur", function() {
    var add_entry_value = $("#add_entry_value").val();
    var block_thirty_days = $("#block_thirty_days").val();
    if(add_entry_value==1){
    var newValue = $(this).val();
    var parsedDate = new Date(newValue);
    var today = new Date();
    var thirtyDaysAgo = new Date();
        thirtyDaysAgo.setDate(today.getDate() - 30);
        if (parsedDate < thirtyDaysAgo && block_thirty_days ==1) {
            var voucher_type = $('#voucher_type_id').val();
            var vaoucher_date = $("#purchase_invoice_date").val()
        var params = {voucher_type : voucher_type,vaoucher_date:vaoucher_date};
        var url = $("#ins_base_url").val();

        $.ajax({
            type: "POST",
            url: url + "/accounts/chkEntryBeforeThrity",
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                if(data == 1){
                    $(".saving_div_class").hide();
                    $(".send_for_approvel").hide();
                    $(".send_for_approvel_requested").show();
                }else if(data!=2){
                    $(".saving_div_class").hide();
                    $(".send_for_approvel_requested").hide();
                    $(".send_for_approvel").show();
                }else{
                    $("#add_entry_approved").val(1);
                    $(".saving_div_class").show();
                    $(".send_for_approvel").hide();
                    $(".send_for_approvel_requested").hide();
                }

            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });



        } else {
            $(".saving_div_class").show();
            $(".send_for_approvel").hide();
            $(".send_for_approvel_requested").hide();
            console.log("The selected date is not more than 30 days before today.");
        }
    }
});
function sendApproveForAdd(){
    $('#remarks').val('');
    $("#getrequstapproveModel").modal({
                backdrop: 'static',
                keyboard: false
            });
}
function saverequesttoedit(){
    var voucher_type = $('#voucher_type_id').val();
    var remarks = $('#remarks').val();
    var vaoucher_date = $("#purchase_invoice_date").val()
    var voucher_no = $("#generated_vouchr_text").val()
    var params = {voucher_type : voucher_type,remarks : remarks,vaoucher_date:vaoucher_date,voucher_no : voucher_no};
         var url = $("#ins_base_url").val();
         if($("#remarks").val() == ''){
            Command: toastr["warning"]("Please enter Remarks");
            return;
        }
        $.ajax({
            type: "POST",
            url: url + "/accounts/requesttoedit",
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                toastr.success('Requested Succcessfuly !!');
                $(".send_for_approvel").hide();
                $(".send_for_approvel_requested").show();
                searchList();
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
}
function select_item_desc_voucher(id, event,type,is_header=0) {
    var amount_header = $("#amount_header").val();
    var party_header = $("#ledger_id_head-0").val();
    var header_cr = $("#header_cr").val();
    if(id != "ledger_item_desc-0"){
    if(amount_header == '' || party_header == '' || header_cr == '-1'){
        toastr.warning("Please fill Header section");
        $("#"+id).val('');
        return false;
        }
    }
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
        var ajax_div = 'search_ledger_item_box-' + id;
        var ledger_desc = $('#ledger_item_desc-' + id).val();

        if (event.keyCode == 13) {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            return false;
            }
            else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
             var url = $("#ins_base_url").val();
            $.ajax({
                type: "GET",
                url: url + "/accounts/searchLedgerMasterForVoucher",
                data: 'ledger_desc=' + encodeURIComponent(ledger_desc) + '&type='+type+'&search_ledger_desc=1&is_header='+is_header,
                beforeSend: function () {
                    $("#" + ajax_div).css('top', '');
                    if(is_header == 1){
                        $("#" + ajax_div).css('margin-left', '-5px');
                    }else{
                        $("#" + ajax_div).css('margin-left', '370px');
                    }
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();

                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();


                    if(parseFloat($("#" + ajax_div+" li").size()) > 15 && id != '0'){
                    $("#" + ajax_div).css('top', '-12vh');
                    $("#" + ajax_div).css('position', 'absolute');
                    }else{
                         $("#" + ajax_div).css('top', '');
                    }

                    $("#" + ajax_div).css('z-index', '1004');
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillBankForVoucher(e,ledger_name,id,ledger_group_id,ledger_code,current_balance){
    var id_exts = 0;
   $('.accounts_tbody tr').each(function () {
            id_temp = $(this).find("input[name='ledger_id[]']").val();
           if(id == id_temp){
                id_exts = 1;
           }
   });
   if(id_exts == 1){
       toastr.error("Selected Ledger Is Already Existing!");
       return false;
   }
    $("#ledger_item_desc-0").val(ledger_name);
    $("#ledger_id_head-0").val(id);
    $("#ledger_group_head-0").val(ledger_group_id);
    if(parseFloat(current_balance) < 0){
     $("#ledger_code").html((current_balance*-1)+' .Dr');
    }else{
        if(current_balance == ''){
            current_balance = 0;
        }
    $("#ledger_code").html(current_balance+' .Cr');
    }
    $("#amount_header").focus();
    $('.ajaxSearchBox').hide();
    $("bank_voucher_generated").val('1');
    generateBankVoucher(ledger_code);
}
function generateBankVoucher(ledger_code){
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "GET",
        url: url + "/accounts/generateBankVoucher",
        data: 'ledger_code=' + encodeURIComponent(ledger_code) ,
        beforeSend: function () {

        },
        success: function (html) {
            $("#generated_vouchr_label").html('<b>'+html+'</b>');
            $("#generated_vouchr_text").val(html);
        },
        complete: function () {

        }
    });
}
    function generateBankVoucherForRepeat(ledger_id){
        var url = $("#ins_base_url").val();
        $.ajax({
            type: "GET",
            url: url + "/accounts/generateBankVoucher",
            data: 'ledger_id=' + encodeURIComponent(ledger_id) ,
            beforeSend: function () {
                $('.delete_return_entry_added').trigger('click');
                appendRawNew();
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
            },
            success: function (html) {
                $("#generated_vouchr_label").html('<b>'+html+'</b>');
                $("#generated_vouchr_text").val(html);
                $.LoadingOverlay("hide");
            },
            complete: function () {

            }
        });
}
function generateVoucherForRepeat(voucher_type_id){
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "GET",
        url: url + "/accounts/generateVoucherNoRepeat",
        data: 'voucher_type_id=' + voucher_type_id ,
        beforeSend: function () {
            $('.delete_return_entry_added').trigger('click');
            appendRawNew();
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (html) {
            $("#generated_vouchr_label").html('<b>'+html+'</b>');
            $("#generated_vouchr_text").val(html);
            $.LoadingOverlay("hide");


        },
        complete: function () {

        }
    });
}
