var arr = new Array();
$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getgeneralReportData() {

    var url = route_json.generalLedgerResults;

    //-------filters---------------------------------------------

    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var ledger_id = $('#ledger_id_hidden').val();

    var parm = { from_date: from_date,to_date:to_date,ledger_id:ledger_id };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
           // $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}




function datarst() {

    var current_date = $('#current_date').val();

    $("#depatment_list").select2('val', '');
    $('#date_from').val(current_date);

}
function expandRow(e,id){
    var added=false;
$.map(arr, function(elementOfArray, indexInArray) {
if (elementOfArray.id == id) {
added = true;
$("."+id).remove();
arr = arr.filter(function( obj ) {
return obj.id !== id;
});
//arr.pop({id: id});
}
});
if (!added) {
arr.push({id: id});
var filters_list = new Object();
var filters_value = '';
var filters_id = '';

$('.filters').each(function () {
 filters_id = this.id;
 if (filters_id == 'bill_no_hidden') {
     filters_id = 'bill_no';
 }
 filters_value = $('#' + filters_id).val();
 filters_id = filters_id.replace('_hidden', '');

 if (filters_id == 'bill_date_from') {
     filters_id = 'from_date';
 }
 if (filters_id == 'bill_date_to') {
     filters_id = 'to_date';
 }
 if (filters_id == 'no_date') {
     if($('#' + filters_id).prop("checked") == true){
         filters_value = 1;
         filters_id = 'no_date';
     }else{
         filters_value = 0;
         filters_id = 'no_date';
     }
 }
 if (filters_id == 'no_opening') {
     if($('#' + filters_id).prop("checked") == true){
         filters_value = 1;
         filters_id = 'no_opening';
     }else{
         filters_value = 0;
         filters_id = 'no_opening';
     }
 }
 if (filters_id == 'show_naration') {
     if($('#' + filters_id).prop("checked") == true){
         filters_value = 1;
         filters_id = 'show_naration';
     }else{
         filters_value = 0;
         filters_id = 'show_naration';
     }
 }

 if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
     filters_list[filters_id] = filters_value;
 }
});
if (id != '0') {
         filters_value_det = id;
         filters_id_det = 'group_id';
     }
filters_list[filters_id_det] = filters_value_det;
filters_value_inc = 'include_cr_dr';
filters_id_inc = 'include_cr_dr';
filters_list[filters_id_inc] = filters_value_inc;
$("#remove_group_id_"+id).show();
$("#expand_group_id_"+id).hide();

var url = $("#base_url").val();
$.ajax({
         type: "GET",
         url: url + "/reports/common_row_expand",
         data: filters_list,
         beforeSend: function () {
         $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
          },
         success: function (data) {
           $(e).closest('tr').after(data);
         $.LoadingOverlay("hide");
         $(".theadfix_wrapper").floatThead('reflow');
         },
         complete: function () {
         }
     });
}
}
function getSingleLeaderDetails(e,ledger_id){

    var url = $("#base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var ledger_name = 'Trasaction Data';
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name}
    
     $.ajax({
                type: "POST",
                url: url + "/accounts/getSingleTransactionReportData",
                data: param,
                beforeSend: function () {
                    $('#leder_name_header').html(ledger_name);
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $("#getLeaderDetailModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#getLeaderDetailData').html(data);
                    $.LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                    });
                },
                complete: function () {
                }
            });
}
function editLedgerHeder(booking_id,v_type){
    if(v_type=='Journal' || v_type == 3){
        var v_no = 3;
    }else{
        var v_no = 0;
    }
    var url = $("#base_url").val();
    var urls = url + "/accounts/editLedgerBooking/"+booking_id+"/"+v_no
    window.open(urls, '_blank');
}