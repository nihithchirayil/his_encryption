batch_array = {};
$(document).ready(function () {
});


function backToList() {
    var url = $("#ins_base_url").val();
    localStorage.removeItem("type_val");
    localStorage.removeItem("exp_pay_type");
    localStorage.removeItem("bill_entry_date");
    window.location = url + "/master/ledger_booking_entry_list";
}
        /**
         *
         * @param {type} e
         * @returns {undefined}
         * SHOWS MODAL BOX FOR NEW LEDGER ADD
         */
function saveNewLedger(e){
    var led_id = $(e).parents("tr").find('.ledger_item_desc').attr('id');
    var led_name = $(e).parents("tr").find('.ledger_item_desc').val();
    var row_ids = (led_id).split('-');
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "GET",
        url: url + "/master/list_ledger_master",
        data: {data:1},
        beforeSend: function () {
             $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            $('#add_new_ledger_spin').removeClass('fa fa-plus');
            $('#add_new_ledger_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                var html_content = $(data).find('#ledger_master_form_whole');
                $("#append_ledger_master").html(html_content);
                $("#ledger_name_update").val(led_name);
                $("#led_cancel_btn").hide();
                $("#from_booking_entry").val('1');
                $("#row_ids").val(led_id);
                 $('#ledger_master_form_whole').removeClass('col-md-4 padding_sm');
                $("#search_ref_modal").modal('show');
                $.LoadingOverlay("hide");
            }
        },
        complete: function () {
            $('#add_new_ledger_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_new_ledger_spin').addClass('fa fa-plus');
            $(".ajaxSearchBox").hide();
        }
    });
}
   function select_group_asset(id, event) {
    var url = $("#ins_base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        var ajax_div = 'group_asset_name_box-1';
        var ledger_desc = $('#group_asset_name').val();

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: url + "/master/list_asset_group",
                data: 'search_group_asset=' + ledger_desc,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillAssetGroupValues(e, ledger_name, id,typ,is_asset) {
      event.preventDefault();
      $("#group_asset_name").val(ledger_name.replace(/&amp;/g, '&'));
      $("#group_asset_id").val(id);
      $("#is_asset_etry").val(is_asset);
      if(is_asset == 0){
    $(".credit_div").show();
 }else{
        $(".credit_div").hide();
        $("#credit_days").val('');
        $("#credit_amount").val('');
        $("input[type=radio][name=pl_type]").prop('checked', false);
    }
}
function showCreditData(e){
    if($(e).val() > 0){
        $(".credit_div").show();
    }else{
        $(".credit_div").hide();
        $("#credit_days").val('');
        $("#credit_amount").val('');
        $("input[type=radio][name=pl_type]").prop('checked', false);
    }
}
function saveLedgerMaster(){
   var list_url = $("#ins_base_url").val();

   var url = $('#ledger_master_form').attr('action');
   var led_id = $("#row_ids").val();
   var row_ids = (led_id).split('-');
   if($("#ledger_name_update").val()==''){
       alert("Please enter Ledger name");
       return false;
   }
   if($("#group_asset_id").val()=='' || $("#group_asset_name").val()==''){
       alert("Please select group");
       return false;
   }
  $.ajax({
        type: "GET",
        url: url ,
        data: $("#ledger_master_form").serialize(),
        beforeSend: function () {
            $("#ledger_master_form_whole").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            $("#saveLedgerMasterBtn").attr('disabled',true);
            $('#add_new_ledger_spin').removeClass('fa fa-save');
            $('#add_new_ledger_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var json_data = JSON.parse(data);
            if (json_data.success == 2) {
                 toastr.error('Default Payment Bank already selected !...');
            }
             if (json_data.success == 3) {
                 toastr.error('Default Receive Bank already selected !...');
            }
            if (json_data.success == 4) {
                 toastr.error('Ledger name exist !...');
            }
            if (json_data.success == 0) {
                 toastr.error('Error occured !...');
            }
            if (json_data.success == 1) {
                toastr.success(json_data.message);
                if($("#from_booking_entry").val() == 1){
                    var row_idss = row_ids[1];
                    var up_below_part = row_ids[0]
                    if(up_below_part == 'ledger_item_desc_cd'){
                    $("#ledger_item_desc_cd-"+row_idss).val(json_data.ledger_name);
                    $("#ledger_item_desc_hidden_cd-"+row_idss).val(json_data.ledger_id);
                    $("#group_id_cd_"+row_idss).val(json_data.group_id);
                    fill_newrow();

                    }else{
                    $("#ledger_item_desc-"+row_idss).val(json_data.ledger_name);
                    $("#ledger_item_desc_hidden-"+row_idss).val(json_data.ledger_id);
                    $("#group_id_"+row_idss).val(json_data.group_id);
                     fillLedgerValues('',json_data.ledger_name,json_data.ledger_id,json_data.group_id);
                }
                    $.LoadingOverlay("hide");
                    $("#search_ref_modal").modal('hide');
                }else{
                window.location = list_url + "/master/list_ledger_master";
            }
            }
        },
        complete: function () {
            $('#add_new_ledger_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_new_ledger_spin').addClass('fa fa-save');
            $("#saveLedgerMasterBtn").attr('disabled',false);
            $("#ledger_master_form_whole").LoadingOverlay("hide");
        }
    });
}
function selectExpenseIncome(val){
    var url = $("#ins_base_url").val();
    var generated_vouchr_text = $("#generated_vouchr_text").val();
    $.ajax({
                type: "GET",
                url: url + "/master/select_expense_income",
                data: 'search_expense_income=' + val+'&generated_vouchr_text='+generated_vouchr_text,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    var json_data1 = JSON.parse(data);
                    $.LoadingOverlay("hide");
                    changeBookingType(json_data1.v_type);
                    $("#type").val(json_data1.v_type);
                    if(json_data1.v_no != 0){
                    $("#generated_vouchr_label").html('<b>'+json_data1.v_no+'</b>');
                     $("#generated_vouchr_text").val(json_data1.v_no);
                 }
                 setTimeout(function(){
                    $("#ledger_item_desc-1").focus();
                },300);
                },
                complete: function () {
                }
            });
}
function pageRefresh(){
window.location.reload();
}
function widhAdjust(v){
    if(v==1){
         $("#th_ref").css("width","11%");
         $("#th_nar").css("width","37%");
         $(".reference_no").css("height", "26px");
    }else{
    $("#th_nar").css("width","11%");
    $("#th_ref").css("width","37%");
    }
    console.log(v);
    $(".theadfix_wrapper").floatThead('reflow');
}
function previousLedgerHistory(id,cr_dr_val){
     var url = $("#ins_base_url").val();
        $.ajax({
                type: "GET",
                url: url + "/master/select_previous_booking",
                data: 'ledger_id=' + id+'&cr_dr='+cr_dr_val,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                var json_data = JSON.parse(data);
                $.LoadingOverlay("hide");
                if(parseFloat(json_data.cnt) > 0){
                    for(var i=0; i < 1;i++){
                        $('#payment_receipt_tbody_two tr').each(function () {
                            if ($(this).find('input[name="ledger_cd[]"]').val() ==''){
                                var ledger_name_id =($(this).find('input[name="ledger_cd[]"]').attr("id"));
                                var ledger_id =($(this).find('input[name="ledger_id_cd[]"]').attr("id"));
                                var ledger_group_id =($(this).find('input[name="group_id_cd[]"]').attr("id"));
                                $("#"+ledger_name_id).val(json_data.dtls[i].ledger_name);
                                $("#"+ledger_id).val(json_data.dtls[i].ledger_id);
                                $("#"+ledger_group_id).val(json_data.dtls[i].group_id);
                            }
                        });
                        fill_newrow();
                    }
                }
                },
                complete: function () {
                }
            });
}
function AdvanceEditSearch(){
    var url = $("#ins_base_url").val();
    var ledger_name = $("#ledger_item_desc-11").val();
    if(ledger_name !=''){
    var ledger_id = $("#ledger_item_desc_hidden-11").val();
    }else{
        var ledger_id = '';
    }
    var amount = $("#amount_adv_edit").val();
    var narration = $("#narration_adv_edit").val();
    var voucher_no = $("#voucherno_adv_edit").val();
    var from_adv_date = $("#from_entry_date_adv").val();
    var to_adv_date = $("#to_entry_date_adv").val();
    var v_type_adv = $("#v_type_adv").val();
    var ref_adv_edit = $("#ref_adv_edit").val();
    var params = {ledger_id: ledger_id, amount: amount,narration:narration,voucher_no:voucher_no,from_adv_date:from_adv_date,to_adv_date:to_adv_date,
    v_type_adv:v_type_adv,ref_adv_edit:ref_adv_edit};
        $.ajax({
                type: "GET",
                url: url + "/master/advanced_ledger_edit_list",
                data: params,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                 $("#edit_apend_modal").html(data);
                $("#adv_search_modal").modal('show');
                $.LoadingOverlay("hide");
                $(".theadfix_wrapper").floatThead('reflow');
                },
                complete: function () {
                }
            });

}
 function select_item_desc_adv(id, event, is_bank) {
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
    if (is_bank == 1) {
    var ajax_div = 'search_ledger_item_box-' + id;
    var ledger_desc = $('#ledger_item_desc-' + id).val();
    } else {
    var ajax_div = 'search_ledger_item_box_cd-' + id;
    var ledger_desc = $('#ledger_item_desc_cd-' + id).val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


    if (ledger_desc == "") {
    $("#" + ajax_div).html("");
    } else {
    var url = $("#ins_base_url").val();
    $.ajax({
    type: "GET",
                url: url + "/master/searchLedger",
            data: 'ledger_desc=' + ledger_desc + '&search_ledger_desc=1&is_adv=' + 1,
            beforeSend: function () {
            $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#" + ajax_div).html(html).show();
            $("#" + ajax_div).find('li').first().addClass('liHover');
            },
            complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            }
    });
    }

    } else {
    ajaxProgressiveKeyUpDown(ajax_div, event);
    }
    }
    function fillLedgerValues_adv(e, ledger_name, id, ledger_group_id) {
    $("#ledger_item_desc-11").val(ledger_name.replace(/&amp;/g, '&'));
    $("#ledger_item_desc_hidden-11").val(id);
    }
   function refreshAllData(){
    localStorage.removeItem("type_val");
    localStorage.removeItem("exp_pay_type");
    localStorage.removeItem("bill_entry_date");
    window.location.reload();
    }
    function searchLedgerMaster() { 
        var base_url = $("#ins_base_url").val();
        var token = $('#token_hiddendata').val();
        var url = base_url + "/master/searchLedgerMaster";
        var group_name = $('#group_name').val();
        var hidden_group_names_id = $('#hidden_group_names_id').val();
        var ledger_name = $('#ledger_name').val();
        var hidden_ledger_name_id = $('#hidden_ledger_name_id').val();
        if( $('#is_bank_search').is(":checked") ){
            var is_bank_search = 1;
        }else{
            var is_bank_search = 0
        }
        var param = {
            _token: token,
            ledger_name: ledger_name,
            hidden_ledger_name_id: hidden_ledger_name_id,
            status: status,
            group_name:group_name,
            hidden_group_names_id:hidden_group_names_id,
            is_bank_search:is_bank_search
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function() {
                $("#searchLedgerMasterDataDiv").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
                $('#searchMasterBtn').attr('disabled', true);
                $('#searchMasterBtnSpin').removeClass('fa fa-search');
                $('#searchMasterBtnSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function(data) {
                $('#searchLedgerMasterDataDiv').html(data.html);
                // $('#total_cntspan').html(data.total_records);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
    
                });
                setTimeout(function() {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function() {
                $("#searchLedgerMasterDataDiv").LoadingOverlay("hide");
                $('#searchMasterBtn').attr('disabled', false);
                $('#searchMasterBtnSpin').removeClass('fa fa-spinner fa-spin');
                $('#searchMasterBtnSpin').addClass('fa fa-search');
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }
