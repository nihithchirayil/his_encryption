var arr = new Array();
var arr1 = new Array();
var exp_arr = new Array();
var inc = 1;
var voucher_no_arr = [];
$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    var table_company_code = $("#table_company_code").val();
    if(table_company_code == 'DAYAH'){
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate : '04-01-2022',
    });
}else{
       $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
}
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();




        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.multiGroupReport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + encodeURIComponent(search_key) + '&search_key_id=' + input_id,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
function getgeneralReportData() {

    var url = route_json.reportResults;

    //-------filters---------------------------------------------

    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var ledger_id = $('#ledger_id_hidden').val();

    if(ledger_id == 0){
       Command: toastr["warning"]("Plese select the account!");
       return false;
    }
    var parm = { bill_date_from: from_date,bill_date_to:to_date,ledger_id:ledger_id };
    $.ajax({
        type: "POST",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
           $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');
//                        var $table = $('table.theadfix_wrapper');
//                        $table.floatThead({
//                        scrollContainer: function ($table) {
//                        return $table.closest('.theadscroll');
//                        }
//
//                        });
//                        $('.theadscroll').perfectScrollbar({
//                        wheelPropagation: true,
//                                minScrollbarLength: 30
//                        });
//                        $('.fixed_header').floatThead({
//                        position: 'absolute',
//                                scrollContainer: true
//                        });


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}
function showGroupAmountDetailMulti(e, ledger_id){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();

        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to,first_td_text:first_td_text}
        $.ajax({
            type: "POST",
            url: url + "/accounts/expandGroupAmountDetailMultiByGroup",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {
                if($("#exclude_option").prop("checked")){
                    $(".excludeItemButton").show();
                    $(".excludeItemButton").each(function(key, val){
                    });
                }
                if($("#expand_all").prop("checked")){``
                            expand_all(inc);
            }
        }
        });
        inc++;


}
}
function showLedgerAmountDetailMulti(e,ledger_id){
    var added = false;
    var first_td_text = $(e).closest('tr').find("td:eq(0)").text();
    $.map(arr, function (elementOfArray, indexInArray) {
        if (elementOfArray.id == ledger_id) {
            added = true;
            $("." + ledger_id).remove();
            arr = arr.filter(function (obj) {
                return obj.id !== ledger_id;
            });
        }
    });
    if (!added) {
        arr.push({id: ledger_id});
        var url = $("#base_url").val();
        var bill_date_from = $("#bill_date_from").val();
        var bill_date_to = $("#bill_date_to").val();

        var param = {ledger_id: ledger_id, bill_date_from: bill_date_from, bill_date_to: bill_date_to,first_td_text:first_td_text}
        $.ajax({
            type: "POST",
            url: url + "/accounts/showLedgerAmountDetailMulti",
            data: param,
            beforeSend: function () {
               $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
            },
            complete: function () {

        }
        });
        inc++;


}
}
