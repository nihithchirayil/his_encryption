var empArr = new Array();
var unCheckedArr = new Array();
$(document).ready(function () {
    $(document).on('click', '.grn_drop_btn', function (e) {
        e.stopPropagation();
        $(".grn_btn_dropdown").hide();
        $(this).next().slideDown('');
    });
    $(document).on('click', '.btn_group_box', function (e) {
        e.stopPropagation();
    });

    $(document).on('click', function () {
        $(".grn_btn_dropdown").hide();
    });

    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });

    $(document).on('click', '.notes_sec_list ul li', function () {
        var disset = $(this).attr("id");
        $('.notes_sec_list ul li').removeClass("active");
        $(this).addClass("active");
        $(this).closest('.notes_box').find(".note_content").css("display", "none");
        $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
    });

    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });

    var $table = $('table.theadfix_wrapper');
    setTimeout(function () {
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    }, 300);
    $(document).on('click', '.grn_drop_btn', function (e) {
        e.stopPropagation();
        $(".grn_btn_dropdown").hide();
        $(this).next().slideDown('');
    });
    $(document).on('click', '.btn_group_box', function (e) {
        e.stopPropagation();
    });


    $(document).on('click', function () {
        $(".grn_btn_dropdown").hide();
    });

    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });

    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    $('.date_time_picker').datetimepicker();


    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

    $('#existing_patient_Modal').on('shown.bs.modal', function (e) {
        var $table = $('table.theadfix_wrapper');
        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });
    });
    searchGrnList();
});

var base_url = $('#base_url').val();
var token = $('#token').val();
  $('#grn_receicved_checkbox').on('change',function(){
    searchGrnList();
  })

function searchGrnList() {
    var url = base_url + "/accounts/searchAccountsGrnList";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var grn_id = $('#grn_id_hidden').val();
    var bill_no = $('#bill_no_search').val();
    var vendor_id = $('#vendoritem_id_hidden').val();
    var item_code = $('#item_desc_hidden').val();
    var location = $('#to_location').val();
    var status = $('#status').val();
    var receive_status = $('#grn_receicved_checkbox').is(':checked') ? 1 : 0;
    var param = {
        _token: token,
        grn_id: grn_id,
        from_date: from_date,
        to_date: to_date,
        vendor_id: vendor_id,
        item_code: item_code,
        bill_no: bill_no,
        location: location,
        status: status,
        receive_status:receive_status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchGrnListData").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#searchgrnlistBtn').attr('disabled', true);
            $('#searchgrnlistSpin').removeClass('fa fa-search');
            $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchGrnListData').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#searchGrnListData").LoadingOverlay("hide");
            $('#searchgrnlistBtn').attr('disabled', false);
            $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchgrnlistSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getGrnListPreview() {
    var url = base_url + "/accounts/getAccountsGrnFullList";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var grn_id = $('#grn_id_hidden').val();
    var bill_no = $('#bill_no_search').val();
    var vendor_id = $('#vendoritem_id_hidden').val();
    var item_code = $('#item_desc_hidden').val();
    var location = $('#to_location').val();
    var status = $('#status').val();
    var param = {
        _token: token,
        grn_id: grn_id,
        from_date: from_date,
        to_date: to_date,
        vendor_id: vendor_id,
        item_code: item_code,
        bill_no: bill_no,
        location: location,
        status: status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#grnlistpreviewBtn').attr('disabled', true);
            $('#grnlistpreviewSpin').removeClass('fa fa-eye');
            $('#grnlistpreviewSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#grnlist_div").html(data)
            $("#grnlist_model").modal({
                backdrop: 'static',
                keyboard: false
            });

        },
        complete: function () {
            $('#grnlistpreviewBtn').attr('disabled', false);
            $('#grnlistpreviewSpin').removeClass('fa fa-spinner fa-spin');
            $('#grnlistpreviewSpin').addClass('fa fa-eye');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function grn_listview(from_type, grn_id) {
    var url = base_url + "/purchase/listGrnItems";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var bill_no = $('#bill_no').val();
    var vendor_code = '';
    var flag = true;
    if (from_type == '2') {
        vendor_code = $('#search_vendor_code_hidden').val();
        if (!vendor_code) {
            flag = false;
        }
    }
    if (flag) {
        var param = {
            _token: token,
            grn_id: grn_id,
            vendor_code: vendor_code,
            from_date: from_date,
            to_date: to_date,
            from_type: from_type,
            bill_no: bill_no
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                if (from_type == '2') {
                    $('#Vendordatabtn').attr('disabled', true);
                    $('#Vendordataspin').removeClass('fa fa-industry');
                    $('#Vendordataspin').addClass('fa fa-spinner fa-spin');
                } else if (from_type == '1') {
                    $('#grn_listviewbtn' + grn_id).attr('disabled', true);
                    $('#grn_listviewspin' + grn_id).removeClass('fa fa-info');
                    $('#grn_listviewspin' + grn_id).addClass('fa fa-spinner fa-spin');
                }
            },
            success: function (data) {
                $("#grnlist_div").html(data)
                $("#grnlist_model").modal({
                    backdrop: 'static',
                    keyboard: false
                });

            },
            complete: function () {
                if (from_type == '1') {
                    $('#grn_listviewbtn' + grn_id).attr('disabled', false);
                    $('#grn_listviewspin' + grn_id).removeClass('fa fa-spinner fa-spin');
                    $('#grn_listviewspin' + grn_id).addClass('fa fa-info');
                } else if (from_type == '2') {
                    $('#Vendordatabtn').attr('disabled', false);
                    $('#Vendordataspin').removeClass('fa fa-spinner fa-spin');
                    $('#Vendordataspin').addClass('fa fa-industry');
                }
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select a Vendor");
    }
}



//----------- Item Detail search------------
$('#item_desc').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('item_desc_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#item_desc_hidden').val() != "") {
            $('#item_desc_hidden').val('');
            $("#item_desc_AjaxDiv").hide();
        }
        var item_desc = $(this).val();
        if (item_desc == "") {
            $("#item_desc_AjaxDiv").html("").hide();
        } else {
            var url = base_url + "/reports/ItemStockTaxWise";

            $.ajax({
                type: "GET",
                url: url,
                data: "item_desc=" + item_desc,
                beforeSend: function () {
                    $("#item_desc_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#item_desc_AjaxDiv").html(html).show();
                    $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('item_desc_AjaxDiv', event);
    }
});


$('#vendor_name_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxVendorSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $('#vendor_name_search').val();
        if (vendor_name == "") {
            $('#ajaxVendorSearchBox').hide();
            $("#vendoritem_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, vendor_name: vendor_name, row_id: 1 },
                beforeSend: function () {
                    $("#ajaxVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxVendorSearchBox").html(html).show();
                    $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxVendorSearchBox', event);
    }
});


$('#grn_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxGrnNoSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var grn_number = $('#grn_no_search').val();
        if (grn_number == "") {
            $('#ajaxGrnNoSearchBox').hide();
            $("#grn_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'grn_number', search_key: grn_number },
                beforeSend: function () {
                    $("#ajaxGrnNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxGrnNoSearchBox").html(html).show();
                    $("#ajaxGrnNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxGrnNoSearchBox', event);
    }
});


$('#bill_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxBillNoSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#bill_no_search').val();
        if (bill_no == "") {
            $('#ajaxBillNoSearchBox').hide();
            $("#bill_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'invoice_number', search_key: bill_no },
                beforeSend: function () {
                    $("#ajaxBillNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxBillNoSearchBox").html(html).show();
                    $("#ajaxBillNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxBillNoSearchBox', event);
    }
});


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillitem_desc(item_code, item_desc) {
    item_desc = htmlDecode(item_desc);
    $('#item_desc_hidden').val(item_code);
    $('#item_desc').val(item_desc);
    $('#item_desc_AjaxDiv').hide();
}

function fillSearchDetials(grn_id, grn_number) {
    grn_number = htmlDecode(grn_number);
    $('#grn_no_search').val(grn_number);
    $('#grn_id_hidden').val(grn_id);
    $('#ajaxGrnNoSearchBox').hide();
}


function fillInvoice_no(grn_id, bill_no) {
    bill_no = htmlDecode(bill_no);
    $('#bill_no_search').val(bill_no);
    $('#bill_id_hidden').val(grn_id);
    $('#ajaxBillNoSearchBox').hide();
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#vendor_name_search').val(vendor_name);
    $('#vendoritem_id_hidden').val(id);
    $('#ajaxVendorSearchBox').hide();
}
$(document).on("click", ".grn_receicved", function(){
    var bil_id =$(this).val();
    if($(this).prop("checked")){
        empArr.push({
            'bil_id' : bil_id,
        });
        } else {
            empArr = $.grep(empArr, function(e){
                return e.bil_id != bil_id;
           });
    }
});
$(document).on("click", ".grn_receicved", function(){
    var bil_id =$(this).val();
    if(!$(this).prop("checked")){
        unCheckedArr.push({
            'bil_id' : bil_id,
        });
        } else {
            unCheckedArr = $.grep( unCheckedArr, function(e){
                return e.bil_id != bil_id;
           });
    }
});
function updateAccountsGrn(){
    var url = base_url + "/accounts/updateAccountsGrn";
    if($('#grn_receicved_checkbox').is(':checked')){
        type=0;
        rev_Arr=unCheckedArr;
    }else{
        type=1;
        rev_Arr=empArr;
    }
    if(rev_Arr.length ==0){
       toastr.warning('Nothing to save.');
       return;
    }
    var param = {
        _token: token,
        accData: rev_Arr,
        type: type,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchGrnListData").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#updateAccountsGrn').attr('disabled', true);
            $('#updateAccountsGrn_spin').removeClass('fa fa-save');
            $('#updateAccountsGrn_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
           if(parseInt(data)==1){
            toastr.success('Success');
            searchGrnList();
           }else{
            toastr.warning('Opps,Something went wrong.');
           }
        },
        complete: function () {
            $("#searchGrnListData").LoadingOverlay("hide");
            $('#updateAccountsGrn').attr('disabled', false);
            $('#updateAccountsGrn_spin').removeClass('fa fa-spinner fa-spin');
            $('#updateAccountsGrn_spin').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
