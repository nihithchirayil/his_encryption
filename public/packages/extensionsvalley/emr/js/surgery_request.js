function NewSurgeryRequest(){
    var patient_id = $('#patient_id').val();
    var surgery_date = $('#surgery_date').val();
    var anaesthesia_id = $('#anesthesia').val();
    var surgeon = $('#surgeon').val();
    var anaesthetist = $('#anesthetist').val();
    var diaganosis = $('#diagnosis').val();
    var surgery_id = $('#surgery_id').val();
    var primary_surgery_id = $('#primary_surgery_id').val();
    var base_url = $("#base_url").val();
    var token = $('#c_token').val();
    var surgery_time = $('#surgery_time').val();
    var url = base_url + "/surgery_request/addSurgeryRequest";
    var instrument_id = '';
    var instrument_id_array = [];
    if(primary_surgery_id == ''){
        toastr.warning('Please select a surgery');
        $('#primary_surgery_id').focus();
        return false;
    }

    $('.instruments').each(function () {
         instrument_id = $(this).val();
        if(instrument_id != ''){
            instrument_id_array.push(instrument_id);
        }
    });
    var dataparams ={
        _token:token,
        patient_id:patient_id,
        surgery_date:surgery_date,
        anaesthesia_id:anaesthesia_id,
        surgeon:surgeon,
        anaesthetist:anaesthetist,
        diaganosis:diaganosis,
        instrument_id_array:instrument_id_array,
        primary_surgery_id:primary_surgery_id,
        surgery_id:surgery_id,
        surgery_time:surgery_time,
    };
    $.ajax({
        url:url,
        type:"POST",
        data:dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            if(data == 1){
                toastr.success("Saved Successfully!!!");
                $('#instrument_list').html('');
                $('#reset_surgery_request').trigger('click');
                $('.select2').select2();
                $('#surgery_request_modal').modal('hide');
            }else{
                toastr.error("Error Please Check Your Internet Connection");
            }
        },
    });

}



function surgery_request(){
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var base_url = $("#base_url").val();
    var url = base_url + "/surgery_request/surgeryRequest";
    $.ajax({
        url:url,
        type:"GET",
        data:'',
        beforeSend: function () {
            $('#surgery_request_modal').modal('show');
            $('#surgery_request_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#surgery_request_modal_body').html(data);
        },
        complete: function () {
            $('#surgery_request_modal_body').LoadingOverlay("hide");
            $('.select2').select2().on("change", function (e) {
                select_instruments($(this).val(),$(this).attr('id'));
            });

            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });


        },
    });
}

// function listSurgeryRequest(patient_id){
//     var base_url = $("#base_url").val();
//     var url = base_url + "/surgery_request/listSurgeryRequest";
//     $.ajax({
//         url:url,
//         type:"GET",
//         data:{patient_id:patient_id},
//         beforeSend: function () {
//             $('#surgery_request_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
//         },
//         success: function (data) {
//             $('#surgery_request_list').html(data);
//         },
//         complete: function () {
//             $('#surgery_request_list').LoadingOverlay("hide");
//         },
//     });
// }

function select_instruments(value,id){
    var base_url = $("#base_url").val();
    var value1 = '';
    var value2 = '';
    var url = base_url + "/surgery_request/select_instruments";
    if(id == 'primary_surgery_id'){
        value1 = value.split(",");
    }

    if(id =='surgery_id'){
        value2 = $('#primary_surgery_id').val();
        value2 = value2.split(",");
        value = value+','+value2;
    }
    value = value.split(",");

    $.ajax({
        url:url,
        type:"GET",
        data:{value:value},
        beforeSend: function () {
            $('#instrument_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if(data !=0){
                $('#instrument_list').html(data);
            }else{
                $('#instrument_list').html('');
            }
        },
        complete: function () {
            $('#instrument_list').LoadingOverlay("hide");
        },
    });

}

$(document).on("click", "#item_search_btn", function (e) {
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
});

function searchbyName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("result_data_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

