$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
    });
    $('.block_future_date').datetimepicker({
        format: 'DD-MM-YYYY',
        maxDate: new Date()
    });

    $('.savegrnDiv').hide();
    $(".select2").select2();
    document.onkeyup = KeyCheck;
    checkGrnEditStatus();
    getGrnButtons();

    $('#payment_mode_detail_block').hide();
    if ($('#payment_mode').val() != '' && ($('#payment_mode').val() != 'cash' && $('#payment_mode').val() != 'credit')) {
        $('#payment_mode_detail_block').show();
    } else {
        $('#payment_mode_detail_block').hide();
    }

});

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

$("#item_search_btn").click(function () {
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
    $('#issue_search_box').focus();
});


function getGrnButtons() {
    var post_type = $('#approve_status').val();
    var partially_receive_status = $('#partially_receive_status').val();
    if (parseInt(partially_receive_status) == 1) {
        $('#savegrnDiv6').show();
        $('#savegrnDiv1').show();
        $('#savegrnDiv2').show();
        $('#savegrnDiv3').show();
        $('#savegrnDiv4').show();
        $('#grn_status_head').text('GRN Partially Approved');
    } else if (parseInt(post_type) == 0) {
        $('#savegrnDiv1').show();
        $('#savegrnDiv2').show();
        $('#savegrnDiv3').show();
        $('#savegrnDiv4').show();
        $('#grn_status_head').text('New GRN');
    } else if (parseInt(post_type) == 1) {
        $('#savegrnDiv5').show();
        $('#savegrnDiv1').show();
        $('#savegrnDiv2').show();
        $('#savegrnDiv3').show();
        $('#savegrnDiv4').show();
        $('#grn_status_head').text('GRN Created');
    } else if (parseInt(post_type) == 2) {
        $('#savegrnDiv6').show();
        $('#savegrnDiv1').show();
        $('#savegrnDiv2').show();
        $('#savegrnDiv3').show();
        $('#savegrnDiv4').show();
        $('#grn_status_head').text('GRN Approved');
    } else if (parseInt(post_type) == 4) {
        $('#grn_status_head').text('GRN Closed');
    } else if (parseInt(post_type) == 3) {
        $('#grn_status_head').text('GRN Rejected');
    }
}

var base_url = $('#base_url').val();
var grn_id = $('#grnBillId').val();
var token = $('#hidden_filetoken').val();
var decimalConfiguration = $('#decimal_configuration').val();
var item_array = {};

function checkGrnEditStatus() {
    if (parseInt(grn_id) != 0) {
        var url = base_url + '/grn/getGeneralGrnItemDetalis';
        var tot_discount_amount = 0;
        var param = {
            bill_id: grn_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#row_body_data").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
                $('#is_igstcheck').prop('checked', false);
            },
            success: function (data) {
                var obj = JSON.parse(data);
                tot_discount_amount = obj.po_head[0].bill_discount_value ? obj.po_head[0].bill_discount_value : 0.0;
                $("#row_body_data").html(obj.html);
                $('#grnSavePosttype').val(obj.po_head[0].approve_status ? obj.po_head[0].approve_status : 0);
                $('#bill_discount_type').val(obj.po_head[0].bill_discount_type ? obj.po_head[0].bill_discount_type : 1);
                $('#bill_discount_value').val(checkIsNaN(tot_discount_amount));
                $('#tot_bill_discount_amt').html(obj.po_head[0].bill_discount_amount ? obj.po_head[0].bill_discount_amount : 0.0);
                var bill_duplication = obj.po_head[0].bill_duplication ? obj.po_head[0].bill_duplication : 0;
                if (parseFloat(bill_duplication) == 1) {
                    $('#bill_duplication_check').prop('checked', true);
                }
                $('#grn_no').html(obj.po_head[0].grn_no ? obj.po_head[0].grn_no : '');
                $('#gatepass').val(obj.po_head[0].gatepass ? obj.po_head[0].gatepass : '');
                $('#remark_txt').val(obj.po_head[0].remark ? obj.po_head[0].remark : '');
                $('#po_no').val(obj.po_head[0].po_no ? obj.po_head[0].po_no : '');
                $('#po_date').val(obj.po_head[0].po_date ? obj.po_head[0].po_date : '');
                $('#po_id').val(obj.po_head[0].po_head_id ? obj.po_head[0].po_head_id : 0);
                $('#item_supplier').val(obj.po_head[0].vendor_name ? obj.po_head[0].vendor_name : '');
                $('#vender_code').val(obj.po_head[0].vendor_code ? obj.po_head[0].vendor_code : '');
                $('#vender_id').val(obj.po_head[0].vendor_id ? obj.po_head[0].vendor_id : '');
                $('#bill_no').val(obj.po_head[0].bill_no ? obj.po_head[0].bill_no : '');
                $('#round_off').val(obj.po_head[0].round_amount ? obj.po_head[0].round_amount : 0);
                $('#bill_date').val(obj.po_head[0].bill_date ? obj.po_head[0].bill_date : '');
                var is_igst = obj.po_head[0].is_igst ? obj.po_head[0].is_igst : 0;
                if (parseInt(is_igst) == 1) {
                    $('#is_igstcheck').prop('checked', true);
                }
                $('#purchaseCategoryID').val(obj.po_head[0].purchase_category ? obj.po_head[0].purchase_category : 0);
                $('#location').val(obj.po_head[0].location_code ? obj.po_head[0].location_code : '');
                $('#location').select2();
                var row_id = insertGeneralGrnCalculation(obj.item_c_array, obj.po_charges_dts, obj.charge_type, 1, 2);
                var location = obj.po_head[0].location_code ? obj.po_head[0].location_code : '';
                $('#row_count_id').val(row_id);
                getPurchaseCategory(location);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function () {
                $("#row_body_data").LoadingOverlay("hide");
                $('#bill_discount_value').val(checkIsNaN(tot_discount_amount));
                checkIgstStatus();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        var location = $('#location').val();
        $('#item_supplier').focus();
        getPurchaseCategory(location);
        getNewRowInserted();
        checkIgstStatus();
    }

}

function getSelectedRowFocus() {
    if ($(".row_class:last").find("input[name='item_desc[]']").val() == '') {
        setTimeout(function () {
            $(".row_class:last").find("input[name='item_desc[]']").focus();
        }, 1000);
    }
}

function checkAllPo() {
    var status = $('#po_select_all').is(":checked");
    if (status) {
        $('.po_dl_id').prop('checked', true);
    } else {
        $('.po_dl_id').prop('checked', false);
    }
}

var po_deatilID = [];

function getPoDetailId() {
    po_deatilID = [];
    $('.po_dl_id').each(function (index) {
        var status = $(this).is(":checked");
        if (status) {
            po_deatilID.push(parseInt($(this).val()));
        }
    });
    return po_deatilID;
}

function ViewPo(po_id) {
    var poDeatilString = JSON.stringify(po_deatilID);
    var po_id_get = $('#po_id').val();
    if (parseInt(po_id_get) == 0 || (parseInt(po_id_get) == po_id)) {
        var url = base_url + '/grn/viewPoList';

        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                poDeatilString: poDeatilString,
                po_id: po_id
            },
            beforeSend: function () {
                var poNumber = $('#poNumber' + po_id).html();
                $('#po_select_all').prop('checked', false);
                $('#po_id_viewd').val(po_id);
                $('#po_list_detail_header').html(poNumber);
                $('#po_list_btn' + po_id).attr('disabled', true);
                $('#po_list_spin' + po_id).removeClass('fa fa-list');
                $('#po_list_spin' + po_id).addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                $('#row_po_dl_list').html(html);
                $('#po_list_detail').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            },
            complete: function () {
                $('#po_list_btn' + po_id).attr('disabled', false);
                $('#po_list_spin' + po_id).removeClass('fa fa-spinner fa-spin');
                $('#po_list_spin' + po_id).addClass('fa fa-list');
                setTimeout(function () {
                    $(".theadfix_wrapper").floatThead('reflow');
                }, 500);
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        var po_no = $('#po_no').val();
        toastr.warning("Another po is already added with PO No." + po_no);
    }
}


function AddPo(po_id) {
    var array_len = Object.keys(item_array).length;
    if (parseInt(array_len) != 0) {
        bootbox.confirm({
            message: "Added items would be lost,Are you sure you want tot continue ?",
            buttons: {
                'confirm': {
                    label: "Continue",
                    className: 'btn-primary',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    item_array = {};
                    addFullPO(po_id);
                    $('#row_count_id').val(0);
                    $("#row_body_data").html('');
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                }
            }
        });
    } else {
        item_array = {};
        addFullPO(po_id);
        $('#row_count_id').val(0);
        $("#row_body_data").html('');
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30

        });
        setTimeout(function () {
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        }, 400);
    }
}



function addFullPO(po_id) {
    var url = base_url + "/grn/generalGrnWithPo";
    var row_id = 0;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            row_id: row_id
        },
        beforeSend: function () {
            $('#po_add_btn' + po_id).attr('disabled', true);
            $('#po_add_spin' + po_id).removeClass('fa fa-plus');
            $('#po_add_spin' + po_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (typeof obj.po_head[0] !== 'undefined' && obj.po_head.length > 0) {
                $("#row_body_data").html(obj.html);
                $('#purchaseCategoryID').val(0);
                $('#po_no').val(obj.po_head[0].po_no ? obj.po_head[0].po_no : '');
                $('#po_date').val(obj.po_head[0].po_date ? obj.po_head[0].po_date : '');
                $('#location').val(obj.po_head[0].location ? obj.po_head[0].location : '');
                $('#bill_discount_type').val(obj.po_head[0].bill_discount_type ? obj.po_head[0].bill_discount_type : 1);
                $('#bill_discount_value').val(obj.po_head[0].bill_discount_value ? obj.po_head[0].bill_discount_value : 0.0);
                $('#tot_bill_discount_amt').html(obj.po_head[0].bill_discount_amount ? obj.po_head[0].bill_discount_amount : 0.0);
                $('#location').select2();
                var location = obj.po_head[0].location ? obj.po_head[0].location : '';
                getPurchaseCategory(location);
                $('#item_supplier').val(obj.po_head[0].vendor_name ? obj.po_head[0].vendor_name : '');
                $('#vender_code').val(obj.po_head[0].vendor_code ? obj.po_head[0].vendor_code : '');
                $('#vender_id').val(obj.po_head[0].vendor_id ? obj.po_head[0].vendor_id : '');
                var row_id = insertGeneralGrnCalculation(obj.item_c_array, obj.po_charges_dts, obj.charge_type, 1, 1);
                $('#row_count_id').val(row_id);
                $('#po_list_model').modal('toggle');
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            } else {
                toastr.warning("No records found");
            }

        },
        complete: function () {
            $('#po_add_btn' + po_id).attr('disabled', false);
            $('#po_add_spin' + po_id).removeClass('fa fa-spinner fa-spin');
            $('#po_add_spin' + po_id).addClass('fa fa-plus');
            $('#po_id').val(po_id);
            serialNoCalculation('row_class', 'row_count_class');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function addPolisttoGRN() {
    var poDeatils = getPoDetailId();
    if (poDeatils.length != 0) {
        var url = base_url + "/grn/generalGrnWithPo";

        var poDeatilString = JSON.stringify(poDeatils);
        var po_id = $('#po_id_viewd').val();
        $('#po_id').val(po_id);
        var row_id = $('#row_count_id').val();
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                poDeatilString: poDeatilString,
                po_id: po_id,
                row_id: row_id
            },
            beforeSend: function () {
                if ($(".row_class:last").find("input[name='item_code_hidden']").val() == '') {
                    $(".row_class:last").remove();
                }
                $('#addPolisttoGRNBtn').attr('disabled', true);
                $('#addPolisttoGRNSpin').removeClass('fa fa-plus');
                $('#addPolisttoGRNSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (typeof obj.po_head[0] !== 'undefined' && obj.po_head.length > 0) {
                    $("#row_body_data").append(obj.html);
                    $('#purchaseCategoryID').val(0);
                    $('#po_no').val(obj.po_head[0].po_no ? obj.po_head[0].po_no : '');
                    $('#po_date').val(obj.po_head[0].po_date ? obj.po_head[0].po_date : '');
                    $('#location').val(obj.po_head[0].location ? obj.po_head[0].location : '');
                    var location = obj.po_head[0].location ? obj.po_head[0].location : '';
                    $('#location').select2();
                    getPurchaseCategory(location);
                    $('#item_supplier').val(obj.po_head[0].vendor_name ? obj.po_head[0].vendor_name : '');
                    $('#vender_code').val(obj.po_head[0].vendor_code ? obj.po_head[0].vendor_code : '');
                    $('#vender_id').val(obj.po_head[0].vendor_id ? obj.po_head[0].vendor_id : '');
                    row_id = parseInt(row_id) + 1;
                    row_id = insertGeneralGrnCalculation(obj.item_c_array, obj.po_charges_dts, obj.charge_type, row_id, 1);
                    $('#row_count_id').val(row_id);
                    $('#po_list_detail').modal('toggle');
                } else {
                    toastr.warning("No records found");
                }
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function () {
                $('#addPolisttoGRNBtn').attr('disabled', false);
                $('#addPolisttoGRNSpin').removeClass('fa fa-spinner fa-spin');
                $('#addPolisttoGRNSpin').addClass('fa fa-plus');
                serialNoCalculation('row_class', 'row_count_class');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning('Please select any item');
    }

}

$('#poCheckBox').click(function () {
    if ($("#poCheckBox").is(":checked") == true) {
        $('#po_list_model').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#po_list_model').modal('show');
    } else {
        var url = base_url + "/grn/addEditGeneralStoreGrn/" + grn_id;
        window.history.pushState('', '', url);
        location.reload();
    }
});

function KeyCheck(e) {
    var KeyID = (window.event) ? event.keyCode : e.keyCode;
    if (KeyID == 113) {
        purchase_history();
    }
    if (KeyID == 27) {
        $('#item_charges_model').modal('hide');
    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function closePoPopup() {
    $('#poCheckBox').prop('checked', false); // Unchecks it
    $('#po_list_model').modal('toggle');
}

function serach_po() {
    var url = base_url + "/purchase/Show_po";

    var po_department = $('#po_department').val();
    var po_date_from = $('#po_created_date_from').val();
    var po_date_to = $('#po_created_date_to').val();
    var po_id = $('#po_number_search_hidden').val();
    var vendor_code = $('#po_vendor').val();
    var po_no = $('#po_number_search').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_department: po_department,
            po_date_from: po_date_from,
            po_date_to: po_date_to,
            vendor_code: vendor_code,
            po_id: po_id,
            po_no: po_no
        },
        beforeSend: function () {
            $('#searchpobtn').attr('disabled', true);
            $('#posearchdataspin').removeClass('fa fa-serach');
            $('#posearchdataspin').addClass('fa fa-spinner fa-spin');
            $("#row_po_list").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            $('#po_list_model').modal('show');
            $('#row_po_list').html(html);
            $(".theadfix_wrapper").floatThead('reflow');
        },
        complete: function () {
            $('#searchpobtn').attr('disabled', false);
            $('#posearchdataspin').removeClass('fa fa-spinner fa-spin');
            $('#posearchdataspin').addClass('fa fa-serach');
            $("#row_po_list").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getNewRowInserted() {
    if ($(".row_class:last").find("input[name='item_code_hidden']").val() != '') {
        var url = base_url + '/grn/generalGrnAddNewRow';
        var row_count = $('#row_count_id').val();
        $('#row_count_id').val(parseInt(row_count) + 1);
        $.ajax({
            type: "POST",
            url: url,
            data: {
                row_count: row_count,
                add_row: 1,
                _token: token
            },
            beforeSend: function () {
                $('#addNewGrnRow').attr('disabled', true);
                $('#addNewGrnRowSpin').removeClass('fa fa-plus');
                $('#addNewGrnRowSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                $('#row_body_data').append(html);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function () {
                $('#addNewGrnRow').attr('disabled', false);
                $('#addNewGrnRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addNewGrnRowSpin').addClass('fa fa-plus');
                serialNoCalculation('row_class', 'row_count_class');
                $('.expiry_date').datetimepicker({
                    format: 'DD-MM-YYYY',
                });
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }
}

function searchItemCode(id, event, row_id) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = $('#' + id).next().attr('id');
    var item_code = htmlDecode($('#' + id).val());
    var search_type = $('input[name="search_type_item"]:checked').val() ? $(
        'input[name="search_type_item"]:checked').val() : 'item_desc';
    if (event.keyCode == 13) {
        ajaxlistenter(ajax_div);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (item_code) {
            var list_status = 0;
            if ($("#getallgrnitems")) {
                list_status = $("#getallgrnitems").is(":checked");
            }
            var location = $('#location').val();
            $('#grn_listrow_id').val(row_id);
            var url = base_url + "/grn/productSearch";
            var param = {
                item_code: item_code,
                location: location,
                search_type: search_type,
                list_status: list_status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                    if (search_type && search_type == 'barcode') {
                        if ($("#" + ajax_div).find('li').length == 1) {
                            $("#" + ajax_div).find('li').first().click();
                        }
                    }
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            $("#" + ajax_div).html('').hide();
        }
    } else {
        ajax_list_key_down(ajax_div, event);
    }
}

function fillItemValues(list, item_code, item_desc, item_id, uom_name, uom_id, uom_value) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    var row_id = $('#grn_listrow_id').val();
    $('#item_code_hidden' + row_id).val(item_code);
    $('#item_id_hidden' + row_id).val(item_id);
    $('#' + itemCodeTextId).val(htmlDecode(item_desc));
    $('#' + itemCodeTextId).attr('disabled', true);
    $('#' + itemCodeListDivId).hide();
    fetchUOMData(item_id, row_id, uom_id);
    item_history(row_id);
}

function changeItemAmountType(chargeId, row_id) {
    var amountType = $('#popdiscount_type' + chargeId + row_id).val();
    $('#charge_percentage' + chargeId + row_id).attr('readonly', false);
    $('#charge_amount' + chargeId + row_id).attr('readonly', false);
    if (parseInt(amountType) == 1) {
        $('#charge_amount' + chargeId + row_id).attr('readonly', true);
    } else if (parseInt(amountType) == 2) {
        $('#charge_percentage' + chargeId + row_id).attr('readonly', true);
    }
}


function fetchUOMData(item_id, row_id, uom_id) {
    var url = base_url + "/grn/searchItemUOM";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            item_id: item_id,
            uom_id: uom_id,
        },
        beforeSend: function () {
            $('#addNewGrnRow').attr('disabled', true);
            $('#addNewGrnRowSpin').removeClass('fa fa-plus');
            $('#addNewGrnRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            var obj = JSON.parse(response);
            var array_len = Object.keys(obj.item_arr).length;
            if (array_len != 0) {
                row_id_get = insertGeneralGrnCalculation(obj.item_arr, obj.charges_dts, obj.charge_type, row_id, 1);
                $('#row_count_id').val(row_id_get);
            }
            $('#uom_select_id_' + row_id).html(obj.option);
            getUomConvensionFactor(row_id);
        },
        complete: function () {
            $('#addNewGrnRow').attr('disabled', false);
            $('#addNewGrnRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addNewGrnRowSpin').addClass('fa fa-plus');
            if ($(".row_class:last").find("input[name='item_code_hidden[]']").val() != '') {
                getNewRowInserted();
            }

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getUomConvensionFactor(row_id) {
    $('#uom_vlaue' + row_id).val($('#uom_select_id_' + row_id + ' :selected').attr('data-uom_value'));
    calculateListAmount(row_id, 1);
}

function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}

function item_isfree(row_id) {
    var is_free = $('#is_freecheck' + row_id).is(":checked");
    if (is_free) {
        $('#grn_qty' + row_id).attr('readonly', true);
        $('#unit_rate' + row_id).attr('readonly', true);
        $('#tax_type' + row_id).attr('readonly', true);
        $('#tot_dic_amt_' + row_id).attr('readonly', true);
        $('#grn_qty' + row_id).val(0);
        $('#unit_rate' + row_id).val(0);
        $('#tax_type' + row_id).val('');
        $('#tot_dic_amt_' + row_id).val(0);
    } else {
        $('#grn_qty' + row_id).attr('readonly', false);
        $('#unit_rate' + row_id).attr('readonly', false);
        $('#tax_type' + row_id).attr('readonly', false);
        $('#tot_dic_amt_' + row_id).attr('readonly', false);
    }
    calculateListAmount(row_id, 1);
}


function setChargesDetails(row_id) {
    var gst_val = $('#charge_percentage9' + row_id).val();
    var igst_val = $('#charge_percentage12' + row_id).val();
    gst_val = parseFloat(gst_val) * 2;
    if (parseInt(gst_val) > 0) {
        $('#gst_tax_type' + row_id).val(parseInt(gst_val));
    }
    if (parseInt(igst_val) > 0) {
        $('#igst_tax_type' + row_id).val(parseInt(igst_val));
    }
}


function blockSpecialChar(e) {
    var k = e.keyCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));

}

function item_history(row_id) {
    $('#hdn_item_history').val(row_id);
    $('.row_class').css('background', '#FFF');
    $('#row_data_' + row_id).css('background-color', '#40f54066');
}

function changeChargeType(from_type, row_id) {
    if (parseInt(from_type) == 1) {
        var gst_tax_type = $('#gst_tax_type' + row_id).val();
        if (gst_tax_type) {
            $('#igst_tax_type' + row_id).val('');
            $('#charge_percentage12' + row_id).val(checkIsNaN(0));
            var tax_division = $('#gst_tax_type' + row_id + ' option:selected').attr('attt-id');
            $('#charge_percentage9' + row_id).val(checkIsNaN(tax_division));
            $('#charge_percentage10' + row_id).val(checkIsNaN(tax_division));
        } else {
            $('#charge_percentage9' + row_id).val(0);
            $('#charge_percentage10' + row_id).val(0);
        }
        $('#charge_percentage10' + row_id).trigger('onchange');
    } else if (parseInt(from_type) == 2) {
        var gst_tax_type = $('#igst_tax_type' + row_id).val();
        if (gst_tax_type) {
            $('#gst_tax_type' + row_id).val('');
            $('#charge_percentage9' + row_id).val(checkIsNaN(0));
            $('#charge_percentage10' + row_id).val(checkIsNaN(0));
            $('#charge_percentage12' + row_id).val(checkIsNaN(gst_tax_type));
            $('#charge_percentage12' + row_id).attr("readonly", true);
        } else {
            $('#charge_percentage12' + row_id).val(0);
        }
        $('#charge_percentage12' + row_id).trigger('onchange');
    }
}

//----------- Saarch Supplier------------
$('#item_supplier').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox_supplier');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var item_supplier = $('#item_supplier').val();
        if (!item_supplier) {
            $("#vender_id").val('');
            $("#vender_code").val('');
            $("#ajaxSearchBox_supplier").hide();
        } else {
            var url = base_url + "/grn/vendorSearch";
            var param = {
                item_supplier: item_supplier
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxSearchBox_supplier").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxSearchBox_supplier").html(html).show();
                    $("#ajaxSearchBox_supplier").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxSearchBox_supplier', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name) {
    vendor_name = htmlDecode(vendor_name);
    $("#item_supplier").val(vendor_name);
    $("#vender_id").val(id);
    $("#vender_code").val(vendor_code);
    $("#ajaxSearchBox_supplier").hide();
    $('#bill_no').focus();
}

function getPurchaseCategory(location = '') {
    if (!location || location == undefined) {
        location = $('#location').val();
    }
    var purchaseCategoryID = $('#purchaseCategoryID').val();
    var url = base_url + "/grn/getPurchaseCategory";
    var param = {
        location: location,
        purchaseCategoryID: purchaseCategoryID
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#pur_category').html("<option value='All'>Select</option>");
            $("#row_body_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $('#pur_category').html(data);
            $('#pur_category').select2();
        },
        complete: function () {
            $("#row_body_data").LoadingOverlay("hide");
            checkLocationStatus(location);
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}



function checkLocationStatus(location) {
    var url = base_url + "/grn/checkLocationStatus";
    var param = {
        location: location,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('.disable_on_save').attr('disabled', false);
            $("#row_body_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            if (!data.grn_sequence_name) {
                $('.disable_on_save').attr('disabled', true);
                toastr.warning("Please map sequence for this location in manage location screen");
            }
        },
        complete: function () {
            $("#row_body_data").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


var delete_item = [];

function removeRow(row_id) {
    var item_code = $('#item_code_hidden' + row_id).val();
    if (item_code) {
        var item_desc = $('#item_desc_' + row_id).val();
        bootbox.confirm({
            message: "Are you sure, you want delete " + item_desc + " ?",
            buttons: {
                'confirm': {
                    label: "Delete",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var grn_status = $('#grnSavePosttype').val();
                    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
                    if (bill_detail_id && bill_detail_id != 'undefined') {
                        delete_item.push(bill_detail_id);
                    }
                    if (bill_detail_id && bill_detail_id != 'undefined' && parseInt(grn_id) != 0 && (parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) {
                        var tot_qty = $('#tot_qty_' + row_id).val();
                        var url = base_url + "/purchase/checkItemProductStock";

                        var location_code = $('#location').val();
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                _token: token,
                                location_code: location_code,
                                bill_detail_id: bill_detail_id,
                                item_code: item_code,
                            },
                            beforeSend: function () {
                                $("#row_body_data").LoadingOverlay("show", {
                                    background: "rgba(89, 89, 89, 0.6)",
                                    imageColor: '#337AB7'
                                });
                            },
                            success: function (response) {
                                $('#grn_delete_check').val(JSON.stringify(response));
                            },
                            complete: function () {
                                $("#row_body_data").LoadingOverlay("hide");
                                checkStockProductQty(tot_qty, row_id, 2);
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });
                    } else {
                        if (item_array[row_id]) {
                            var item_id = item_array[row_id].item_id ? item_array[row_id].item_id : 0;
                            const index = po_deatilID.indexOf(parseInt(item_id));
                            if (index > -1) {
                                po_deatilID.splice(index, 1);
                            }
                            delete item_array[row_id];
                        }
                        $('#row_data_' + row_id).remove();
                        serialNoCalculation('row_class', 'row_count_class');
                        getAllItemsTotals();
                    }

                }
            }
        });
    } else {
        $('#row_data_' + row_id).remove();
    }
}


function checkBtachExpiry(row_id) {
    var grn_status = $('#grnSavePosttype').val();
    var grn_qty = $('#grn_qty' + row_id).val();
    var free_qty = $('#free_qty' + row_id).val();
    var uom_val = $('#uom_val_model' + row_id).val();
    var batch = $('#batch_name_model' + row_id).val();
    $('#batchname' + row_id).html(batch);
    if (parseInt(grn_id) != 0 && (parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) {
        var tot_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * parseFloat(uom_val);
        checkStockProductQty(tot_qty, row_id, 3);
    }
}

function checkIgstStatus() {
    var is_igst = $('#is_igstcheck').is(":checked");
    if (is_igst) {
        $('.sgst_tax').val(checkIsNaN(0));
        $('.cgst_tax').val(checkIsNaN(0));
        $('#IGSTstatusrow').show();
    } else {
        $.each(item_array, function (index, value) {
            $('#sgst_tax' + parseInt(index)).val(checkIsNaN(value.sgst_amt ? parseFloat(value.sgst_amt) : 0));
            $('#cgst_tax' + parseInt(index)).val(checkIsNaN(value.cgst_amt ? parseFloat(value.cgst_amt) : 0));
        });
        $('#IGSTstatusrow').hide();
    }
    getAllItemsTotals();
}


function checkStockProductQty(tot_qty, row_id, from_fn) {
    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
    if (bill_detail_id && bill_detail_id != 'undefined') {
        var obj = '';
        if (parseInt(from_fn) == 2) {
            obj = $('#grn_delete_check').val();
        } else {
            obj = $('#stock_data').val();
        }

        var data = JSON.parse(obj);
        var transfered_qty = parseInt(data.all_total_quantity) - parseInt(data.item_stock);
        if (parseInt(transfered_qty) < 0) {
            transfered_qty = parseInt(data.item_stock) - parseInt(data.all_total_quantity);
        }
        if (parseInt(from_fn) == 1) {
            if (parseFloat(data.all_total_quantity) <= parseFloat(data.item_stock)) {
                calculatePopupCharges(2);
            } else if (parseFloat(tot_qty) < parseFloat(transfered_qty)) {
                $('#grn_qty').val(data.grn_qty ? data.grn_qty : 0);
                $('#free_qty').val(data.grn_free_qty ? data.grn_free_qty : 0);
                toastr.warning("Grn cannot be edited.Stock already transfered");
            } else {
                calculatePopupCharges(2);
            }
        } else if (parseInt(from_fn) == 2) {
            if (parseFloat(data.all_total_quantity) == parseFloat(data.item_stock)) {
                if (item_array[row_id]) {
                    var item_id = item_array[row_id].item_id ? item_array[row_id].item_id : 0;
                    const index = po_deatilID.indexOf(parseInt(item_id));
                    if (index > -1) {
                        po_deatilID.splice(index, 1);
                    }
                    delete item_array[row_id];
                }
                $('#row_data_' + row_id).remove();
                serialNoCalculation('row_class', 'row_count_class');
                getAllItemsTotals();
            } else if (parseFloat(tot_qty) > parseFloat(data.item_stock)) {
                toastr.warning("Grn cannot be edited.Stock already transfered");
            } else if (parseFloat(data.all_total_quantity) > parseFloat(tot_qty)) {
                toastr.warning("Grn cannot be edited.Stock already transfered");
            } else {
                if (item_array[row_id]) {
                    var item_id = item_array[row_id].item_id ? item_array[row_id].item_id : 0;
                    const index = po_deatilID.indexOf(parseInt(item_id));
                    if (index > -1) {
                        po_deatilID.splice(index, 1);
                    }
                    delete item_array[row_id];
                }
                $('#row_data_' + row_id).remove();
                serialNoCalculation('row_class', 'row_count_class');
                getAllItemsTotals();
            }
        } else if (parseInt(from_fn) == 3) {
            if (parseFloat(data.all_total_quantity) != parseFloat(data.item_stock)) {
                $('#batch_name_model').val(data.batch);
                $('#expiry_date_model').val(data.expiry);
            }
        }
    } else {
        if (parseInt(from_fn) == 1) {
            calculatePopupCharges(2);
        } else if (parseInt(from_fn) == 2) {
            if (item_array[row_id]) {
                var item_id = item_array[row_id].item_id ? item_array[row_id].item_id : 0;
                const index = po_deatilID.indexOf(parseInt(item_id));
                if (index > -1) {
                    po_deatilID.splice(index, 1);
                }
                delete item_array[row_id];
            }
            $('#row_data_' + row_id).remove();
            serialNoCalculation('row_class', 'row_count_class');
            getAllItemsTotals();
        }
    }
}

function validateGRNData() {
    $('.form-control').removeClass('validate_data');
    var flag = true;
    var message = '';
    var return_array = {};
    var validate_item_batch = [];
    $.each(item_array, function (index, value) {

        if (parseFloat(value.netRate) == 0 && parseInt(value.is_free) != 1 && value.item_code) {
            $('#net_rate' + index).addClass('validate_data');
            flag = false;
            message = "Net amount canot be zero";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }
        var itemcode_batch = value.item_code + value.batch_no;
        if (!validate_item_batch.includes(itemcode_batch)) {
            validate_item_batch.push(itemcode_batch);
        } else {
            $('#batch_row' + index).addClass('validate_data');
            flag = false;
            message = "Duplicate Item Found";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }


    });
    return_array = {
        status: flag,
        message: message
    }
    return return_array;
}


function grnSaveData(postType, printVal, fromType) {
    var url = base_url + "/grn/saveGenaralGrnData";
    var item_array_string = JSON.stringify(item_array);
    var item_string = encodeURIComponent(item_array_string);
    var deleted_item = JSON.stringify(delete_item);
    var vender_id = $('#vender_id').val();
    var is_igst = $('#is_igstcheck').is(":checked");
    var vender_code = $("#vender_code").val();
    var location = $('#location').val();
    var bill_no = $('#bill_no').val();
    var bill_date = $('#bill_date').val();
    var pur_category = $('#pur_category').val();
    var bill_duplication_check = $('#bill_duplication_check').is(":checked");
    var array_len = Object.keys(item_array).length;
    var validate_grn = validateGRNData();
    var payment_mode = $('#payment_mode').val();
    if (payment_mode == 'cash' || payment_mode == 'credit') {
        $('#bank').val('');
        $('#card_no').val('');
        $('#payment_phone_no').val('');
        $('#exp_month').val('');
        $('#exp_year').val('');
    }
    var bank = $('#bank').val();
    var card_no = $('#card_no').val();
    var payment_phone_no = $('#payment_phone_no').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();
    var payment_validataion_status = validatePaymentModes();
    if (payment_validataion_status == false) {
        return false;
    }
    if (validate_grn.status) {
        if (location) {
            if (vender_id) {
                if (bill_no) {
                    if (bill_date) {
                        if (pur_category) {
                            if (parseInt(array_len) != 0) {
                                var string_data = "";
                                if (parseInt(fromType) == 1) {
                                    string_data = "Save"
                                } else if (parseInt(fromType) == 2) {
                                    string_data = "Save &amp; Print"
                                } else if (parseInt(fromType) == 3) {
                                    string_data = "Approve"
                                } else if (parseInt(fromType) == 4) {
                                    string_data = "Approve &amp; Print"
                                } else if (parseInt(fromType) == 5) {
                                    string_data = "Cancel"
                                } else if (parseInt(fromType) == 6) {
                                    string_data = "Close Bill"
                                }
                                bootbox.confirm({
                                    message: "Are you sure, you want " + string_data + " ?",
                                    buttons: {
                                        'confirm': {
                                            label: 'Yes',
                                            className: 'btn-danger',
                                            default: 'true'
                                        },
                                        'cancel': {
                                            label: 'No',
                                            className: 'btn-warning'
                                        }
                                    },
                                    callback: function (result) {
                                        if (result) {
                                            var grn_no = $('#grn_no').html();
                                            var status = 0;
                                            var po_date = $('#po_date').val();
                                            var po_no = $('#po_no').val();
                                            var po_id = $('#po_id').val();
                                            var gatepass = $('#gatepass').val();
                                            var remark_txt = $('#remark_txt').val();
                                            var round_off = $('#round_off').val();
                                            var bill_discount_type = $("#bill_discount_type").val();
                                            var bill_discount_value = $("#bill_discount_value").val();
                                            var getTotals = getAllItemsTotals();
                                            var total_gross = getTotals['total_gross'];
                                            var total_dis = getTotals['total_dis'];
                                            var total_tax = getTotals['total_tax'];
                                            var total_net = getTotals['total_net'];
                                            var bill_discount_amount = getTotals['bill_discount'];
                                            $.ajax({
                                                type: "POST",
                                                url: url,
                                                data: {
                                                    _token: token,
                                                    grn_id: grn_id,
                                                    po_id: po_id,
                                                    po_date: po_date,
                                                    grn_no: grn_no,
                                                    po_no: po_no,
                                                    postType: postType,
                                                    is_igst: is_igst,
                                                    item_string: item_string,
                                                    total_gross: total_gross,
                                                    total_dis: total_dis,
                                                    total_tax: total_tax,
                                                    round_off: round_off,
                                                    total_net: total_net,
                                                    deleted_item: deleted_item,
                                                    vender_id: vender_id,
                                                    vender_code: vender_code,
                                                    bill_discount_type: bill_discount_type,
                                                    bill_discount_value: bill_discount_value,
                                                    bill_discount_amount: bill_discount_amount,
                                                    bill_no: bill_no,
                                                    bill_date: bill_date,
                                                    bill_duplication_check: bill_duplication_check,
                                                    pur_category: pur_category,
                                                    gatepass: gatepass,
                                                    remark_txt: remark_txt,
                                                    array_len: array_len,
                                                    location: location,
                                                    payment_mode: payment_mode,
                                                    bank: bank,
                                                    card_no: card_no,
                                                    payment_phone_no: payment_phone_no,
                                                    exp_month: exp_month,
                                                    exp_year: exp_year,

                                                },
                                                beforeSend: function () {
                                                    $('.disable_on_save').prop('disabled', true);
                                                    $('#savegrnSpin' + fromType).removeClass('fa fa-save');
                                                    $('#savegrnSpin' + fromType).addClass('fa fa-spinner fa-spin');
                                                },
                                                success: function (data) {
                                                    status = data.status;
                                                    if (parseInt(data.status) == 1) {
                                                        toastr.success(data.message);
                                                        $('#grn_no').html(data.grn_no);
                                                        grn_id = data.grn_head_id;
                                                        $('#grnSavePosttype').val(postType);
                                                    } else {
                                                        toastr.warning(data.message);
                                                    }
                                                },

                                                complete: function () {
                                                    $('.disable_on_save').prop('disabled', false);
                                                    $('#savegrnSpin' + fromType).removeClass('fa fa-spinner fa-spin');
                                                    $('#savegrnSpin' + fromType).addClass('fa fa-save');
                                                    if (parseInt(status) == 1) {
                                                        if (parseInt(printVal) == 0) {
                                                            showBillNo();
                                                        } else {
                                                            getPrintPreview(fromType, postType, grn_no);
                                                        }
                                                    } else if (parseInt(status) == 4) {
                                                        $('#bill_no').focus();
                                                    }
                                                },
                                                error: function () {
                                                    toastr.error("Error Please Check Your Internet Connection");
                                                }
                                            });
                                        }
                                    }
                                });

                            } else {
                                toastr.warning("Please Select any Item !!");
                            }

                        } else {
                            toastr.warning("Please select any Purchase Category");
                            $('#pur_category').focus();
                        }
                    } else {
                        toastr.warning("Please enter Bill Date");
                        $('#bill_date').focus();
                    }
                } else {
                    toastr.warning("Please enter Bill Number");
                    $('#bill_no').focus();
                }
            } else {
                toastr.warning("Please select any Supplier");
                $('#item_supplier').focus();
            }
        } else {
            toastr.warning("Please select any Supply Location");
            $('#location').focus();
        }
    } else {
        if (validate_grn.message) {
            toastr.warning(validate_grn.message);
        }
    }
}


function DownloadPdfSendMail(mail_status) {
    var url = base_url + "/grn/downloadGRNPdf";
    var grn_no = $('#grn_no').html();
    var postType = $('#grnSavePosttype').val();
    var array_len = Object.keys(item_array).length;
    var item_array_string = JSON.stringify(item_array);
    var item_string = encodeURIComponent(item_array_string);
    var vender_id = $('#vender_id').val();
    var bill_no = $('#bill_no').val();
    var bill_date = $('#bill_date').val();
    var pur_category = $('#pur_category').val();
    var postStringData = postString(postType);
    if (vender_id) {
        if (bill_no) {
            if (bill_date) {
                if (pur_category) {
                    if (parseInt(array_len) != 0) {
                        var po_date = $('#po_date').val();
                        var po_no = $('#po_no').val();
                        var po_id = $('#po_id').val();
                        var gatepass = $('#gatepass').val();
                        var remark_txt = $('#remark_txt').val();
                        var location = $('#location').val();
                        var round_off = $('#round_off').val();
                        var vender_code = $("#vender_code").val();
                        var bill_discount_type = $("#bill_discount_type").val();
                        var bill_discount_value = $("#bill_discount_value").val();
                        var getTotals = getAllItemsTotals();
                        var total_gross = getTotals['total_gross'];
                        var total_dis = getTotals['total_dis'];
                        var total_tax = getTotals['total_tax'];
                        var total_net = getTotals['total_net'];
                        var bill_discount_amount = getTotals['bill_discount'];

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                _token: token,
                                grn_no: grn_no,
                                grn_id: grn_id,
                                postStringData: postStringData,
                                po_id: po_id,
                                po_date: po_date,
                                po_no: po_no,
                                item_string: item_string,
                                total_gross: total_gross,
                                total_dis: total_dis,
                                total_tax: total_tax,
                                round_off: round_off,
                                total_net: total_net,
                                vender_id: vender_id,
                                vender_code: vender_code,
                                bill_discount_type: bill_discount_type,
                                bill_discount_value: bill_discount_value,
                                bill_discount_amount: bill_discount_amount,
                                bill_no: bill_no,
                                bill_date: bill_date,
                                pur_category: pur_category,
                                gatepass: gatepass,
                                remark_txt: remark_txt,
                                array_len: array_len,
                                location: location,
                                mail_status: mail_status
                            },
                            beforeSend: function () {
                                $('#file_pdf_btn' + mail_status).prop('disabled', true);
                                $('#file_pdf_spin' + mail_status).removeClass('fa fa-file');
                                $('#file_pdf_spin' + mail_status).addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                if (parseInt(mail_status) == 0) {
                                    var file_url = base_url + "/packages/uploads/Purchase/" + data;
                                    $("#downloadPoPFDlinka").attr("href", file_url);
                                } else if (parseInt(mail_status) == 1) {
                                    toastr.success('Mail Sent Successfully');
                                }
                            },
                            complete: function () {
                                $('#file_pdf_btn' + mail_status).prop('disabled', false);
                                $('#file_pdf_spin' + mail_status).removeClass('fa fa-spinner fa-spin');
                                $('#file_pdf_spin' + mail_status).addClass('fa fa-file');
                                if (parseInt(mail_status) == 0) {
                                    $("#downloadPoPFDlinkimg").trigger('click');
                                }

                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });

                    } else {
                        toastr.warning("Please Select any Item !!");
                    }

                } else {
                    toastr.warning("Please select any Purchase Category");
                    $('#pur_category').focus();
                }
            } else {
                toastr.warning("Please enter bill date");
                $('#bill_date').focus();
            }
        } else {
            toastr.warning("Please enter bill no");
            $('#bill_no').focus();
        }
    } else {
        toastr.warning("Please select any supplier");
        $('#vender_id').focus();
    }
}


function purchase_history() {
    var row_id = $('#hdn_item_history').val();
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_desc = $('#item_desc_' + row_id).val();
    if (item_code) {
        var url = base_url + "/grn/lastGRNHistory";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                item_code: item_code,
            },
            beforeSend: function () {
                $('#purchase_historymodel_header').html(item_desc);
                $("#row_body_data").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (data != 1) {
                    setTimeout(function () {
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 500);
                    $('#history_dl_list').html(data);
                    $("#purchase_historymodel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            },
            complete: function () {
                $("#row_body_data").LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });

    } else {
        toastr.warning("Please Select A Item !!");
    }

}


function getPrintPreview(fromType) {
    var grn_no = $('#grn_no').html();
    var postType = $('#grnSavePosttype').val();
    var url = base_url + "/grn/printGeneralGrnData";
    var array_len = Object.keys(item_array).length;
    var item_array_string = JSON.stringify(item_array);
    var item_string = encodeURIComponent(item_array_string);
    var vender_id = $('#vender_id').val();
    var bill_no = $('#bill_no').val();
    var bill_date = $('#bill_date').val();
    var pur_category = $('#pur_category').val();
    var postStringData = postString(postType);
    var is_igst = $('#is_igstcheck').is(":checked");
    if (vender_id) {
        if (bill_no) {
            if (bill_date) {
                if (pur_category) {
                    if (parseInt(array_len) != 0) {
                        var po_date = $('#po_date').val();
                        var po_no = $('#po_no').val();
                        var po_id = $('#po_id').val();
                        var gatepass = $('#gatepass').val();
                        var remark_txt = $('#remark_txt').val();
                        var location = $('#location').val();
                        var round_off = $('#round_off').val();
                        var vender_code = $("#vender_code").val();
                        var bill_discount_type = $("#bill_discount_type").val();
                        var bill_discount_value = $("#bill_discount_value").val();
                        var getTotals = getAllItemsTotals();
                        var total_gross = getTotals['total_gross'];
                        var total_dis = getTotals['total_dis'];
                        var total_tax = getTotals['total_tax'];
                        var total_net = getTotals['total_net'];
                        var bill_discount_amount = getTotals['bill_discount'];

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                _token: token,
                                grn_no: grn_no,
                                grn_id: grn_id,
                                postStringData: postStringData,
                                po_id: po_id,
                                po_date: po_date,
                                po_no: po_no,
                                is_igst: is_igst,
                                item_string: item_string,
                                total_gross: total_gross,
                                total_dis: total_dis,
                                total_tax: total_tax,
                                round_off: round_off,
                                total_net: total_net,
                                vender_id: vender_id,
                                vender_code: vender_code,
                                bill_discount_type: bill_discount_type,
                                bill_discount_value: bill_discount_value,
                                bill_discount_amount: bill_discount_amount,
                                bill_no: bill_no,
                                bill_date: bill_date,
                                pur_category: pur_category,
                                gatepass: gatepass,
                                remark_txt: remark_txt,
                                array_len: array_len,
                                location: location
                            },
                            beforeSend: function () {
                                $('#savegrnBtn' + fromType).prop('disabled', true);
                                $('#savegrnSpin' + fromType).removeClass('fa fa-save');
                                $('#savegrnSpin' + fromType).addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                $('#print_po_modelDiv').html(data);
                                $('.theadscroll').perfectScrollbar({
                                    wheelPropagation: true,
                                    minScrollbarLength: 30

                                });
                                setTimeout(function () {
                                    $('.theadfix_wrapper').floatThead({
                                        position: 'absolute',
                                        scrollContainer: true
                                    });
                                }, 400);
                                $("#print_po_model").modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            },
                            complete: function () {
                                $('#savegrnBtn' + fromType).prop('disabled', false);
                                $('#savegrnSpin' + fromType).removeClass('fa fa-spinner fa-spin');
                                $('#savegrnSpin' + fromType).addClass('fa fa-save');
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });

                    } else {
                        toastr.warning("Please Select any Item !!");
                    }

                } else {
                    toastr.warning("Please select any Purchase Category");
                    $('#pur_category').focus();
                }
            } else {
                toastr.warning("Please enter bill date");
                $('#bill_date').focus();
            }
        } else {
            toastr.warning("Please enter bill no");
            $('#bill_no').focus();
        }
    } else {
        toastr.warning("Please select any supplier");
        $('#vender_id').focus();
    }
}


function postString(post_type) {
    var title = '';
    if (post_type == 1)
        title = "GRN Saved Successfuly";
    if (post_type == 2)
        title = "GRN Approved Successfuly";
    if (post_type == 3)
        title = "GRN Rejected Successfuly";
    if (post_type == 4)
        title = "GRN Closed Successfuly";
    return title;
}

function showBillNo() {
    var grn_no = $('#grn_no').html();
    var post_type = $('#grnSavePosttype').val();
    var url = base_url + "/grn/generalGrnList";
    var title = postString(post_type);
    if (!grn_no)
        grn_no = $("#grn_no").html();
    bootbox.alert({
        title: title,
        message: "GRN No. :" + grn_no,
        callback: function () {
            window.location.href = url;
        }
    });
}

function reloadToList() {
    var url = base_url + "/grn/generalGrnList/";
    window.location.href = url;
}


function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("main_row_tbl");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

//-----payment mode changes-------------------------------------

$(document).on('change', '#payment_mode', function () {
    var payment_mode = $(this).val();
    if (payment_mode != '' && (payment_mode != 'cash' && payment_mode != 'credit')) {
        $('#payment_mode_detail_block').show();
    } else {
        $('#payment_mode_detail_block').hide();
    }
});


function checkContentLength(id, length) {
    var value = $('#' + id).val();
    if (value.length > length) {
        value = value.substring(0, length);
        $('#' + id).val(value);
    }
    if (id == 'exp_month') {
        if (value > 12 || value < 1) {
            $('#' + id).val('');
        }
    }
    if (id == 'exp_year') {
        if (value > 2099) {
            $('#' + id).val('');
        }
    }
}


function validatePaymentModes() {
    // return true;
    var payment_mode = $('#payment_mode').val();
    var bank = $('#bank').val();
    var card_no = $('#card_no').val();
    var payment_phone_no = $('#payment_phone_no').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();

    if (payment_mode == '') {
        toastr.warning("Please select payment mode!");
        return false;
    } else if (payment_mode == 'cash' || payment_mode == 'credit') {
        return true;
    } else if (payment_mode == 'cheque') {
        if (bank == '') {
            toastr.warning("Please select bank!");
            return false;
        }
        if (card_no == '') {
            toastr.warning("Please enter card no!");
            return false;
        }

        if (payment_phone_no == '') {
            toastr.warning("Please enter phone no!");
            return false;
        }
    } else if (payment_mode == 'creditcard' || payment_mode == 'debitcard') {
        if (bank == '') {
            toastr.warning("Please select bank!");
            return false;
        }
        if (card_no == '') {
            toastr.warning("Please enter card no!");
            return false;
        }
        if (payment_phone_no == '') {
            toastr.warning("Please enter phone no!");
            return false;
        }
        if (exp_month == '') {
            toastr.warning("Please enter expiry month!");
            return false;
        }
        if (exp_year == '') {
            toastr.warning("Please enter expiry year!");
            return false;
        }
    } else if (payment_mode == 'demanddraft' || payment_mode == 'upi') {
        if (payment_phone_no == '') {
            toastr.warning("Please enter phone no!");
            return false;
        }
    }

}

//-----payment mode changes ends-------------------------------
