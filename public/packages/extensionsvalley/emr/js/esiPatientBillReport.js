$(document).ready(function () {

    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $(".select2").select2();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (event.keyCode == 13) {
            ajaxlistenter('patient_idAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if ($('#patient_id_hidden').val() != "") {
                $('#patient_id_hidden').val('');
            }
            var patient_name = $(this).val();
            patient_name = patient_name.trim();
            if (patient_name == "") {
                $("#patient_idAjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "patient_name_search=" + patient_name,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        //  alert(html); return;
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        } else {
            ajax_list_key_down('patient_idAjaxDiv', event);
        }
    });

    //-----------Bill No search------------
    $('#bill_no').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (event.keyCode == 13) {
            ajaxlistenter('bill_no_AjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if ($('#bill_no_hidden').val() != "") {
                $('#bill_no_hidden').val('');
            }
            var bill_no = $(this).val();
            if (bill_no == "") {
                $("#bill_no_AjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "bill_no=" + bill_no,
                    beforeSend: function () {
                        $("#bill_no_AjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        // alert(html); return;
                        $("#bill_no_AjaxDiv").html(html).show();
                        $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        } else {
            ajax_list_key_down('bill_no_AjaxDiv', event);
        }
    });
});


function fillPatientValues(uhid, patient_name, from_type = 0) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
}

/* For BillNo Search, filling values */
function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
    $('#bill_no').val(bill_no);
    $('#patient_uhid_hidden').val(uhid);
    $('#bill_id_hidden').val(bill_id);
    $('#bill_no_AjaxDiv').hide();
    if (from_type != 1) {
        fillPatientValues(uhid, patient_name, 1);
    }
}

function getReportData() {
    var url = route_json.getesipatientbillreport;
    $('.ajaxSearchBox').hide();
    var patient_name = $('#patient_name').val();
    var bill_tag = $('#bill_tag').val();
    var payment_type = $('#payment_type').val();
    var ip_number_hidden = $('#ip_number_hidden').val();
    var cash_bills = $('#cash_bills').is(":checked");
    if (patient_name || ip_number_hidden) {
        var bill_from = $('#bill_fromdate').val();
        var bill_to = $('#bill_todate').val();
        var bill_id = $('#bill_id_hidden').val();
        var uhid = $('#patient_uhid_hidden').val();
        var visit_status = $('#visit_status').val();
        var param = { cash_bills: cash_bills, bill_tag: bill_tag, payment_type: payment_type, bill_from: bill_from, bill_to: bill_to, bill_id: bill_id, uhid: uhid, visit_status: visit_status,ip_number_hidden:ip_number_hidden };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#search_results').attr('disabled', true);
                $('#serachResultSpin').removeClass('fa fa-search');
                $('#serachResultSpin').addClass('fa fa-spinner');
                $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (html) {
                $('#ResultsViewArea').html(html);
                $('#print_results').removeClass('disabled');
                $('#csv_results').removeClass('disabled');
            },
            complete: function () {
                $('#search_results').attr('disabled', false);
                $('#serachResultSpin').removeClass('fa fa-spinner');
                $('#serachResultSpin').addClass('fa fa-search');
                $('#ResultsViewArea').LoadingOverlay("hide");
                $('#ResultDataContainer').css('display', 'block');
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    } else {
        toastr.warning("Please Select Any Patient");
    }

}

function search_clear() {
    var current_date = $('#current_date').val();
    $('#bill_fromdate').val(current_date);
    $('#bill_todate').val(current_date);
    $('#bill_no_hidden').val('');
    $('#bill_id_hidden').val('');
    $('#bill_no').val('');
    $('#patient_name').val('');
    $('#patient_id_hidden').val('');
    $('#patient_uhid_hidden').val('');
    $('#visit_status').val('').select2();
    $('#bill_tag').val('').select2();
    $('#payment_type').val('').select2();
    $('#cash_bills').prop('checked', false);
    $('#ResultsViewArea').html('');
    $('#ip_number').val('');
    $('#ip_number_hidden').val('');
    // getReportData();

}
$('#ip_number').keyup(function (event) {
    console.log('sdsdsd');
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (event.keyCode == 13) {
        ajaxlistenter('ip_numberAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#ip_number_hidden').val() != "") {
            $('#ip_number_hidden').val('');
        }
        var ip_no = $(this).val();
        ip_no = ip_no.trim();
        if (ip_no == "") {
            $("#ip_numberAjaxDiv").html("");
        } else {
            var url = '';
            $.ajax({
                type: "GET",
                url: url,
                data: "ip_number_search=" + ip_no,
                beforeSend: function () {
                    $("#ip_numberAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    //  alert(html); return;
                    $("#ip_numberAjaxDiv").html(html).show();
                    $("#ip_numberAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ip_numberAjaxDiv', event);
    }
});
function fillLabPatientDetails(ip_no,uhid,patient_name,phone) {
    $('#ip_number_hidden').val(uhid);
    $('#ip_number').val(ip_no);
    $('#patient_name').val(patient_name);
    $('#ip_numberAjaxDiv').hide();
}