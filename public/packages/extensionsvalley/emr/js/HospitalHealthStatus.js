$(document).ready(function () {
    getCustomDates(1);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});

var base_url = $('#base_url').val();
var token = $('#c_token').val();

function getCustomDates(from_type) {
    var date_data = $('#date_datahidden').val();
    var obj = JSON.parse(date_data);
    $('#daterange_hidden').val(from_type);
    if (from_type == '1') {
        $('#from_date').val(obj.current_date);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.current_date);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Today');
        getAllGraphs();
    } else if (from_type == '2') {
        $('#from_date').val(obj.yeasterday);
        $('#to_date').val(obj.yeasterday);
        $('#from_datadis').html(obj.yeasterday);
        $('#to_datadis').html(obj.yeasterday);
        $('#range_typedata').html('Yesterday');
        getAllGraphs();
    } else if (from_type == '3') {
        $('#from_date').val(obj.week_last_sunday);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.week_last_sunday);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Week Till Date');
        getAllGraphs();
    } else if (from_type == '4') {
        $('#from_date').val(obj.lastweek_from);
        $('#to_date').val(obj.lastweek_to);
        $('#from_datadis').html(obj.lastweek_from);
        $('#to_datadis').html(obj.lastweek_to);
        $('#range_typedata').html('Last Week');
        getAllGraphs();
    } else if (from_type == '5') {
        $('#from_date').val(obj.firstday_thismonth);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.firstday_thismonth);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Month Till Date');
        getAllGraphs();
    } else if (from_type == '6') {
        $('#from_date').val(obj.firstday_lastmonth);
        $('#to_date').val(obj.lastday_lastmonth);
        $('#from_datadis').html(obj.firstday_lastmonth);
        $('#to_datadis').html(obj.lastday_lastmonth);
        $('#range_typedata').html('Last Month');
        getAllGraphs();
    } else if (from_type == '7') {
        $("#customdatapopmodel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

}

function getCustomDateRange() {
    var from_date = $('#from_date').val();
    var todate = $('#to_date').val();
    $('#range_typedata').html('Custom Date Range Selection');
    $('#from_datadis').html(from_date);
    $('#to_datadis').html(todate);
    getAllGraphs();
    $("#customdatapopmodel").modal('toggle');
}

function getAllGraphs() {
    setTimeout(function () {
        getHealthDashbord(1);
    }, 500);
    setTimeout(function () {
        getHealthDashbord(2);
    }, 900);
    setTimeout(function () {
        getHealthDashbord(3);
    }, 1200);
    setTimeout(function () {
        getPatientBarCharts(4);
    }, 1500);
    setTimeout(function () {
        getHealthDashbord(5);
    }, 1800);
    setTimeout(function () {
        getPatientBarCharts(7);
    }, 2100);

    setTimeout(function () {
        getHealthDashbord(8);
    }, 2400);
    setTimeout(function () {
        getHealthDashbord(9);
    }, 2700);
    setTimeout(function () {
        getHealthDashbord(10);
    }, 3000);

    setTimeout(function () {
        getHealthDashbord(11);
    }, 3300);
    setTimeout(function () {
        getHealthDashbord(13);
    }, 3600);
}


function getHealthDashbord(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getHealthDashbord";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#list_data' + from_type).LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#7add30' });
            $('#list_data' + from_type).html("Please Wait!!!");
        },
        success: function (data) {
            $('#list_data' + from_type).html(data);
        },
        complete: function () {
            $('#list_data' + from_type).LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getPatientBarCharts(from_type) {
    var title_string = '';
    var subtitle_string = '';
    if (from_type == '4') {
        title_string = 'Average Patient Waiting Time';
        subtitle_string = 'Patient Data';
    } else if (from_type == '7') {
        title_string = 'Discharge';
        subtitle_string = 'Discharge Data';
    }
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getHealthGraphs";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#list_data' + from_type).LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#7add30' });
            $('#list_data' + from_type).html("Please Wait!!!");
        },
        success: function (data) {
            if (data != '0') {
                var obj = JSON.parse(data);
                Highcharts.chart('list_data' + from_type, {
                    chart: {
                        type: 'column',
                        backgroundColor: '#FFFFFF',
                        color: '#000000',
                    },

                    title: {
                        text: title_string,
                        color: '#FFF',
                        style: {
                            color: '#000000',
                            fontWeight: 'bold'
                        }
                    },
                    accessibility: {
                        announceNewData: {
                            enabled: true
                        }
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y:.1f} Mins'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} Mins</b> of total<br/>'
                    },

                    series: [
                        {
                            name: subtitle_string,
                            colorByPoint: true,
                            data: obj.data_send,
                        }
                    ],
                });
            } else {
                toastr.warning('No Result Found');
            }
        },
        complete: function () {
            $('#list_data' + from_type).LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}
