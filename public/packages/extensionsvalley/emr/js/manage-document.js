function manageDocs() {
    $('#manageDocs').modal('show');
    loadUploadImages()

}

var k = 0;

function add_new_row_files() {
    let docType = "";
    let text = "";
    let value = "";
    $('select[name="document_type_hidden"] option').each(function () {
        value = $(this).val();
        text = $(this).text();
        docType += '<option value="' + value + '">' + text + '</option>';
    });
    var documentTypeSelect = '<select class="form-control" required="required" name="document_type[]">\n\
    ' + docType + '</select>';
    var title = '<input type="text" name="title[]" class="form-control" id="description" placeholder="Title">';
    var description = '<input type="text" name="description[]" class="form-control" id="description" placeholder="Description">';
    $("#roww").append("<div  class='remove_" + k + " col-sm-12 no-padding'>\n\
    <div class='col-md-3 padding_sm'>\n\
    <input class='inputfile inputfile-1 hidden' id='file_upload" + k + "' data-multiple-caption='{count} files selected'  data-show-upload='false' name='file_upload[]' type='file' >\n\
    <label class='no-margin btn-block' for='file_upload" + k + "'><i class='ion ion-ios-folder' style='font-size: 18px;'></i> <span>Choose a file ...</span></label>\n\
    </div>\n\
    <div class='col-md-2 padding_sm'>\n\
    " + documentTypeSelect + "\
    </div>\n\
    <div class='col-md-3 padding_sm'>\n\
    " + description + "\
    </div>\n\
    <div class='col-md-2 padding_sm'>\n\
    <button class='btn btn-danger btn-block' onclick='remove_upload_file(" + k + ");' type='button' ><i class='fa fa-trash-o'></i>" + " Clear" + "</button>\n\
    </div>\n\
    </div>\n\
    ")
    k++;
}

function remove_upload_file(k) {
    if (k == 'default') {
        let options = "";
        let text = "";
        let value = "";
        $('select[name="document_type_hidden"] option').each(function () {
            value = $(this).val();
            text = $(this).text();
            options += '<option value="' + value + '">' + text + '</option>';
        });
        $(".remove_00").replaceWith('<div class="remove_00 col-sm-12 no-padding"><div class="col-md-3 padding_sm"><input type="file" name="file_upload[]" id="f_upload" class="inputfile inputfile-1 hidden" data-show-upload="false" data-multiple-caption="{count} files selected"><label class="no-margin btn-block" for="f_upload"><i class="ion ion-ios-folder" style="font-size: 18px;"></i> <span id="uploadspan_documentsdocumenttab">Choose a file ...</span></label><span style="color: #d14;"></span></div><div class="col-md-2 padding_sm"><select name="document_type[]" required="required" class="form-control">' + options + '</select></div><div class="col-md-2 padding_sm"><input type="text" placeholder="Title" id="description" class="form-control" name="title[]"></div><div class="col-md-3 padding_sm"><input type="text" placeholder="Description" id="description" class="form-control" name="description[]"></div><div class="col-md-2 padding_xs"><input type="hidden" name="patient_id" id="patient_id" value="{{$pid}}"><input type="hidden" name="encounter_id" id="encounter_id" value="{{$encounter_id}}"><!--  <button class="btn btn-success btn-block" type="submit" id="doc_file_upload"><i class="fa fa-upload"></i> Upload</button> --></div><div class="col-md-2 padding_sm"><button class="btn btn-danger btn-block" onclick="remove_upload_file(\'' + k + '\');" type=button"><i class="fa fa-trash-o"></i> Remove</button></div></div>');
    } else {
        $('.remove_' + k).remove();
    }
}

function clear_upload_file() {
    $(".document_type_upload").val('');
    $(".description_upload").val('');
    $('#manageDocsForm')[0].reset();
    // $('#uploadspan_documentsdocumenttab').html('Choose a file ...');
    $('.uploadedImg').hide();
}

function clear_upload_file_tk() {
    $(".description_upload_tk").val('');
    $('#manageDocsForm_tk')[0].reset();
    // $('#uploadspan_documentsdocumenttab').html('Choose a file ...');
    $('.uploadedImg_tk').hide();
}

/*upload image*/
$("#manageDocsForm").on('submit', (function (value) {
    value.preventDefault();
    var upload_cnt = 0;
    var type_cnt = 0;
    $("#images_save input[type=file]").each(function () {
        if ($(this).get(0).files.length == 0) {
            type_cnt++;
        }
    });
    $("select[name='document_type[]']").each(function () {
        if ($(this).val() != '') {
            upload_cnt++;
        }
    });
    if (type_cnt == 0) {
        $("#doc_file_upload").attr("disabled", "disabled");
        var url = $('#base_url').val() + "/emr/document-upload";
        var param = new FormData(this);
        var visit_id = $("#visit_id").val();
        var patient_id = $("#patient_id").val();
        var encounter_id = $("#encounter_id").val();
        var document_save_type = $('#document_save_type').val();
        var document_upload_path = $('#document_upload_path').val();
        var file_name = $('#f_upload')[0].files[0]['name'];
        var file_type = $('#f_upload')[0].files[0]['type'];
        var description = $('#description').val();
        var document_type = $('.document_type_upload').val();

        if (document_save_type == 1) {
            // url = 'http://localhost/documentsUpload.php';
            url = document_upload_path;
        }
        param.append('visit_id', visit_id);
        param.append('patient_id', patient_id);
        param.append('encounter_id', encounter_id);
        param.append('functionName', 'documentUpload');
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            data: param,
            contentType: false,
            cache: false,
            processData: false,
            functionName: 'documentUpload',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'x-requested-with': 'XMLHttpRequest'
            },
            crossDomain: true,
            beforeSend: function () {},
            success: function (data) {
                $("#doc_file_upload").attr("enabled", "enabled");
                if (data == 1) {
                    Command: toastr["success"]('Saved Successfully');
                    if (document_save_type == 1) {
                        updateFileUploadInDB(patient_id, visit_id, file_name, file_type, description, document_type);
                    }
                    $('#manageDocsForm')[0].reset();
                    $('#roww').html('');
                    loadUploadImages();
                    $('.uploadedImg').hide();

                }
                else {
                    Command: toastr["error"]('File upload failed.');
                }
            },
            error: function () {
                $("#doc_file_upload").attr("enabled", "enabled");
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $("#doc_file_upload").removeAttr("disabled");
            }
        });
    } else {
        Command: toastr["warning"]('Please Attach a File');
    }
}));

function updateFileUploadInDB(patient_id, visit_id, file_name, file_type, description, document_type) {
    var url = $('#base_url').val() + "/emr/updateFileUploadInDB";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id,
            visit_id: visit_id,
            file_name: file_name,
            file_type: file_type,
            description: description,
            document_type: document_type,
        },
        beforeSend: function () {

        },
        success: function (data) {

        },
        complete: function () {

        }
    });
}

//load history
function loadUploadImages(page) {
    var url = $('#base_url').val() + "/emr/load-document-list";
    var patient_id = $('#patient_id').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            document_list: 1,
            page: page,
            patient_id: patient_id,
            _token: _token,
        },
        beforeSend: function () {
            $('#document-data-list').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#document-data-list').html(data);
            }
        },
        complete: function () {
            $('#document-data-list').LoadingOverlay("hide");
        }
    });
}

//documen pagination
$(document).on('click', '.document_pagination .pagination li a', function (event) {
    event.preventDefault();
    $('li').removeClass('active');
    $(this).parent('li').addClass('active');
    var page = $(this).attr('href').split('page=')[1];
    loadUploadImages(page);
});

function docImgDelete(docID) {
    if (docID == 0) {
        Command: toastr["error"]("You don't have access to remove the document!");
        return false;

    }
    bootbox.confirm({
        title: 'Delete Document',
        size: 'small',
        message: 'Are you sure, you want to delete the document?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                style: 'margin-left: 5px;'
            },
            'confirm': {
                label: 'Ok',
            }
        },

        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/emr/delete-document";
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    data: 'doc_id=' + docID,
                    success: function (data) {
                        if (parseInt(data) == 1) {
                            Command: toastr["success"]('Document Deleted');
                            loadUploadImages();
                        }
                        else {
                            Command: toastr["error"]('Error.!');
                        }
                    },
                    error: function () {
                        Command: toastr["warning"]('Error.!');
                    }
                });
            }
        }
    });
}

function docImgPreview(docID, storage_type = 'server', mime_type = '') {
    if (docID == 0 || docID == undefined) {
        Command: toastr["error"]("Error.!");
        return false;
    }
    var url = $('#base_url').val() + "/emr/preview-document";
    var id = docID;
    $.ajax({
        type: "GET",
        url: url,
        data: "id=" + id + '&storage_type=' + storage_type,
        beforeSend: function () {
            $('#view_document_modal').modal('show');
            $('#document_data').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            $('#document_data').html('');
            if (mime_type == "image/png" ||
                mime_type == "image/jpeg" ||
                mime_type == "image/pdf" ||
                mime_type == "image/doc") {
                $('#document_data').html('<iframe src="" width="100%" height="100%" id="myframe"></iframe>');
            }
        },
        success: function (response) {
            if (mime_type == "image/png" ||
                mime_type == "image/jpeg" ||
                mime_type == "image/pdf" ||
                mime_type == "image/doc") {
                $('#document_data').html('<div class="" style="width: 50%;height: 450px; margin-right:auto; margin-left:auto;text-align: center;margin-top: 30px;"><img src="' + response + '" class="ezoom" style="max-width: 100%;height: auto"></img><div>');
            } else {
                $('#document_data').html('<iframe src="" width="100%" height="100%" id="myframe"></iframe>');
                $('#myframe').attr("src", response);
            }
        },
        complete: function () {
            $('#document_data').LoadingOverlay("hide");
        }
    });
}

$(document).on("change", '.document_upload', function (event) {
    var filename = event.currentTarget.files[0].name;
    // $('#uploadspan_documentsdocumenttab').html(filename);
    $('.uploadedImg').attr('href', URL.createObjectURL(event.currentTarget.files[0]));
    $('.uploadedImg').html(filename);
    $('.uploadedImg').show();
});
$(document).on("change", '.document_upload_tk', function (event) {
    var filename = event.currentTarget.files[0].name;
    // $('#uploadspan_documentsdocumenttab').html(filename);
    $('.uploadedImg_tk').attr('href', URL.createObjectURL(event.currentTarget.files[0]));
    $('.uploadedImg_tk').html(filename);
    $('.uploadedImg_tk').show();
});
// function loadRadioUploadImages(page) {
//     var url = $('#base_url').val() + "/emr/loadRadioDocumentList";
//     var patient_id = $('#patient_id').val();
//     let _token = $('#c_token').val();
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: {
//             document_list: 1,
//             page: page,
//             patient_id: patient_id,
//             _token: _token,
//         },
//         beforeSend: function () {
//             $('#document-radio-data-list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
//         },
//         success: function (data) {
//             if (data != '' && data != undefined && data != 0) {
//                 $('#document-radio-data-list').html(data);
//             }
//         },
//         complete: function () {
//             $('#document-radio-data-list').LoadingOverlay("hide");
//         }
//     });
// }
$(document).on('submit', '#manageDocsForm_tk', (function (value) {
    value.preventDefault();
    var upload_cnt = 0;
    var type_cnt = 0;
    $("#images_save input[type=file]").each(function () {
        if ($(this).get(0).files.length == 0) {
            type_cnt++;
        }
    });
    $("select[name='document_type[]']").each(function () {
        if ($(this).val() != '') {
            upload_cnt++;
        }
    });
    if (type_cnt == 0) {
        $("#doc_file_upload_tk").attr("disabled", "disabled");
        var url = $('#base_url').val() + "/emr/documentRadioUpload";
        var param = new FormData(this);
        var visit_id = $("#visit_id").val();
        var patient_id = $("#patient_id").val();
        var encounter_id = $("#encounter_id").val();
        param.append('visit_id', visit_id);
        param.append('patient_id', patient_id);
        param.append('encounter_id', encounter_id);
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            data: param,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {},
            success: function (data) {
                $("#doc_file_upload_tk").attr("enabled", "enabled");
                if (data.status == 1) {
                    Command: toastr["success"]('Saved Successfully');
                    $('#manageDocsForm_tk')[0].reset();
                    $('#manageRadioDocs').modal('hide');
                    $('#roww').html('');
                    fetchRadiologyUploadsData();
                    clear_upload_file_tk();
                    // $('#uploadspan_documentsdocumenttab').html('Choose a file ...');
                    $('.uploadedImg_tk').hide();

                }
                else {
                    Command: toastr["error"]('File upload failed.');
                }
            },
            error: function () {
                $("#doc_file_upload_tk").attr("enabled", "enabled");
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $("#doc_file_upload_tk").removeAttr("disabled");
            }
        });
    } else {
        Command: toastr["warning"]('Please Attach a File');
    }
}));

function docRadioImgDelete(docID) {
    if (docID == 0) {
        Command: toastr["error"]("You don't have access to remove the document!");
        return false;

    }
    bootbox.confirm({
        title: 'Delete Document',
        size: 'small',
        message: 'Are you sure, you want to delete the document?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                style: 'margin-left: 5px;'
            },
            'confirm': {
                label: 'Ok',
            }
        },

        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/emr/delete-document";
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    data: 'doc_id=' + docID,
                    success: function (data) {
                        if (parseInt(data) == 1) {
                            Command: toastr["success"]('Document Deleted');
                            fetchRadiologyUploadsData()
                        }
                        else {
                            Command: toastr["error"]('Error.!');
                        }
                    },
                    error: function () {
                        Command: toastr["warning"]('Error.!');
                    }
                });
            }
        }
    });
}

$(document).on("click", '#document_data', function () {
    ezoom.onInit(($('#document_data img')), {
        hideControlBtn: false,
        onClose: function (result) {
            console.log(result);
        },
        onRotate: function (result) {
            console.log(result);
        },
    });
});
