$(document).ready(function () {


    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    var doc_id = $('#doc_id').val();
    if (doc_id != 0) {
        var name = $('#doc_name').val();
        $('#doctor').val(name);
        $('#doctor_hidden').val(doc_id);

    }
    setTimeout(getAppointmentList(), 1500);


});
$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
//----Hidden Filed Search--------------------------------------------------------------------
var token = $("#c_token").val();
var base_url = $("#base_url").val();
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = base_url + "/master/AjaxSearch";
            var bill_no = $('#bill_no').val();
            $.ajax({
                type: "GET",
                url: url,
                data: 'bill_no=' + bill_no + '&search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(htmlDecode(name));
    $('#' + serach_key_id).attr('title', (name));
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
function getAppointmentList() {
    var created = $('#created').is(":checked");
    var appointment = $('#appointment').is(":checked");
    var booked = $('#booked').is(":checked");
    var cancelled_bill = $('#cancelled_bill').is(":checked");
    var checked_in = $('#checked_in').is(":checked");
    var visited = $('#visited').is(":checked");
    var paid_app = $('#paid_app').is(":checked");
    var unpaid_app = $('#unpaid_app').is(":checked");
    var app_no = $('#app_no').val();
    var doctor_hidden = $('#doctor_hidden').val();
    var speciality = $('#speciality').val();
    var uhid = $('#uhid').val();
    var patient_hidden = $('#patient_hidden').val();
    var mobile = $('#mobile').val();
    var token = $('#token').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var params = {
        created: created,
        appointment: appointment,
        booked: booked,
        cancelled_bill: cancelled_bill,
        checked_in: checked_in,
        visited: visited,
        paid_app: paid_app,
        unpaid_app: unpaid_app,
        app_no: app_no,
        speciality: speciality,
        doctor_hidden: doctor_hidden,
        uhid: uhid,
        patient_hidden: patient_hidden,
        mobile: mobile,
        token: token,
        from_date: from_date,
        to_date: to_date,

    };
    var url = base_url + "/master/getAppointmentList";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $('#appointmentlist').html(' ');
            $('#appointmentlist').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#btn_save').attr('disabled', true);
            $("#btn_spin").removeClass("fa fa-search");
            $("#btn_spin").addClass("fa fa-spinner fa-spin");

        },
        success: function (data) {
            if (data) {
                $('#appointmentlist').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }

        },
        complete: function () {
            $('#btn_save').attr('disabled', false);
            $('#appointmentlist').LoadingOverlay("hide");
            $("#btn_spin").removeClass("fa fa-spinner fa-spin");
            $("#btn_spin").addClass("fa fa-search");

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}
function resetapp() {
    $('.reset').val('');
    $('.checkit').prop('checked', false);
    $('#appointment').prop('checked', true);
    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    getAppointmentList();
}
$('.checkit').click(function () {
    var name = $(this).attr('name');

    if ($(this).hasClass('checked')) {
        $(this).prop('checked', false);
        $(this).removeClass('checked');
        getAppointmentList();
    } else {
        $('input[name="' + name + '"]').removeClass('checked');
        $(this).addClass('checked');
        getAppointmentList();
    }
});

function displayCancelStatus(head_id) {
    $('#cancelAppSetup').modal('show');
    $('#head_id_hidden').val(head_id);
    var url = base_url + "/master/displayCancel";
    $.ajax({
        type: "POST",
        url: url,
        data: { head_id: head_id },
        beforeSend: function () {
            $('#apppointmentSetupBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#appointment_no').val('');
            $('#token_no_c').val(' ');
            $('#cancel_patient').val(' ');
            $('#booking_date').val(' ');
            $('#time_slotdesc').val(' ');
            $('#specialityname').val(' ');
            $('#doctor_name').val(' ');
            $('#createduser').val(' ');
            $('#Cancel_reason').val(' ');

        },
        success: function (data) {
            if (data) {
                $('#appointment_no').val(data[0].appointment_no);
                $('#token_no_c').val(data[0].token_no);
                $('#cancel_patient').val(data[0].patient_name);
                $('#booking_date').val(data[0].booking_date);
                $('#time_slotdesc').val(data[0].time_slotdesc);
                $('#specialityname').val(data[0].specialityname);
                $('#doctor_name').val(data[0].doctor_name);
                $('#createduser').val(data[0].createduser);
            }

        },
        complete: function () {
            $('#apppointmentSetupBody').LoadingOverlay("hide")

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function reprintRegistrationBill(head_id, visit_id) {
    var url = base_url + "/master/reprintRegistrationBill";
    $.ajax({
        type: "POST",
        url: url,
        data: { visit_id: visit_id },
        beforeSend: function () {
            $('#popup' + head_id).hide();
            $('#apppointmentSetupBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if (data) {
                toastr.success("Print successfully queued");
            } else {
                toastr.warning("Bill Not Found");
            }
        },
        complete: function () {
            $('#apppointmentSetupBody').LoadingOverlay("hide")

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function cancelAppointment() {
    var Cancel_reason = $('#Cancel_reason').val();
    var cancel_status = Cancel_reason.replace(/ /g, '');
    if (cancel_status) {
        var head_id = $('#head_id_hidden').val();
        if (head_id != 0) {
            var url = base_url + "/master/cancelappointment";
            $.ajax({
                type: "POST",
                url: url,
                data: { head_id: head_id, Cancel_reason: Cancel_reason },
                beforeSend: function () {
                    $('#Cancel').attr('disabled', true);
                    $('#Cancel_spin').removeClass('fa fa-trash');
                    $('#Cancel_spin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (data) {
                        toastr.success('Appointment cancelled successfully.');
                        $('#popup' + head_id).remove();
                        $('#cancelAppSetup').modal('hide');
                        $('.cancel_bg' + head_id).removeClass('booked-bg');
                        $('.cancel_bg' + head_id).addClass('cancelled-bg');
                        $('#popup' + head_id).hide();

                    }
                },
                complete: function () {
                    $('#Cancel').attr('disabled', false);
                    $('#Cancel_spin').removeClass('fa fa-spinner fa-spin');
                    $('#Cancel_spin').addClass('fa fa-trash');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });
        }
    } else {
        toastr.warning("Please enter cancel reason");
        $('#Cancel_reason').focus();
    }
}

function listAppoinmentDetailView(appointment_id, visit_id) {
    var url = base_url + "/master/listAppoinmentDetailView";
    $.ajax({
        type: "POST",
        url: url,
        data: { appointment_id: appointment_id, visit_id: visit_id },
        beforeSend: function () {
            $('#popup' + appointment_id).hide();
            $('#appointmentlist').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $("#patientFullViewModelDiv").html(data);
            $("#patientFullViewModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#appointmentlist').LoadingOverlay("hide")
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function displayDetails(id,bill_head_id=0) {
    var head_id = $('#head_id_hidden').val();
    if (head_id != id) {
        $('#popup' + head_id).hide();
    }
    $('#head_id_hidden').val(id);
    $('#bill_head_hidden').val(bill_head_id);
    if ($('#popup' + id).is(":visible")) {

        $('#popup' + id).hide();
    } else if ($('#popup' + id).is(":hidden")) {
        $('#popup' + id).show();
    }
    $('#ppclose' + id).click(function () {
        $('#popup' + id).hide();
    });

}
$(document).on('click', '.print_serv_bill', function (event) {
    $('#serv_print_config_modal').modal('toggle');
});

function PrintBill() {
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var printBillId = $('#bill_head_hidden').val();
    var bill_tag = 'CONS';
    var package = $('#package_id_hidden').val() ? $('#package_id_hidden').val() : 0;
    var is_duplicate = $("#is_duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var include_hospital_header = $("#showTitle").prop('checked') ? 1 : 0;
    var param = { is_duplicate: is_duplicate, include_hospital_header: include_hospital_header, package: package, printBillId: printBillId, location_id: location_id, location_name: location_name, bill_tag: bill_tag };
    var base_url = $('#base_url').val();
    if (!printBillId && !bill_tag) {
        toastr.warning('Please select bill....');
        return;
    }
    var url = base_url + "/service_bill/print_bill_detail";
    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function () {
            $('#popup' + printBillId).hide();
            $('#print_list_btn').attr('disabled', true);
            $('#print_list_spin').removeClass('fa fa-print');
            $('#print_list_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write('<style>@page{size:A5 landscape;} .patient_head{ font-size:10px !important; } </style>' + response + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

        },
        complete: function () {
            $('#print_status').val('');
            $('#print_bill_id').val(' ');
            $('#search_bill_tag').val(' ').select2();
            $('#package_id').val(' ');
            $('#print_list_btn').attr('disabled', false);
            $('#print_list_spin').removeClass('fa fa-spinner fa-spin');
            $('#print_list_spin').addClass('fa fa-print');
            $('#serv_print_config_modal').modal('toggle');
        },

    });
}

function OpPrescriptionPrint(appointment_id,visit_id){
    var url = base_url + "/master/OpPrescriptionPrint";
    $.ajax({
        type: "POST",
        url: url,
        data: {visit_id: visit_id },
        beforeSend: function () {
            $('#popup' + appointment_id).hide();
            $('#appointmentlist').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (response) {
            console.log(response);
            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;}</style>' + response + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

        },
        complete: function () {
            $('#appointmentlist').LoadingOverlay("hide")
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

