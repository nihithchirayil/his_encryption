$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    default_nursing_station = $("#default_nursing_station").val();
    //$(body).addClass('sidebar-collapse');
    selectDashBoardData(default_nursing_station);

    loadRoomsChart(default_nursing_station);
    loadRoomsTypes(default_nursing_station);
    setTimeout(function () {
        resetBootstrapTable();
    },2000);
    $(document).on("click", function(event){
        var $trigger = $(".ajaxSearchBox");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $(".ajaxSearchBox").hide();
        }            
    });
});

//-------loading all nursing stations list---------------------------------------------

var default_station_id = $('#default_nursing_station').val();
$(document).on('click', '#default_nursing_station', function (event) {
    var url = $('#base_url').val() + "/nursing/selectNursingStation";
    $.ajax({
        type: "GET",
        url: url,
        data: 'default_station_id=' + default_station_id,
        beforeSend: function () {
            $('#NursingStationPopup').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            // console.log(data);
            $('#NursingStationPopup').html("<b>" + data + "</b>");
        },
        complete: function () {
            $('#NursingStationPopup').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        }
    });

});



//--------changing default nursing station of the user---------------------------------

$(document).on('click', '.ns_station_list', function (event) {
    var station_id = $(this).attr("data-station_id");
    var user_id = $('#user_id').val();
    var station_name = $(this).html();
    var url = $('#base_url').val() + "/nursing/updateDefaultNursingStation";
    $.ajax({
        type: "GET",
        url: url,
        data: 'default_station_id=' + station_id + '&user_id=' + user_id,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                $('#default_nursing_station').val(station_id);
                $('#default_nursing_station').html(station_name);
            }
        },
        complete: function () {
            selectDashBoardData(station_id);
            loadRoomsChart(station_id);
            loadRoomsTypes(station_id);
        }
    });
});


//-------select nursing dashboard informations-----------------------------------
function selectDashBoardData(station_id) {
    var url = $('#base_url').val() + "/nursing/selectDashBoardData";
    $.ajax({
        type: "GET",
        url: url,
        data: 'station_id=' + station_id,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            obj = JSON.parse(data);
            $("#admitted_patients_list_data").empty();
            $("#admitted_patients_list_data").append(obj.admitted_patients_data);
            $("#rooms_list_data").html(obj.rooms_data);
            $("#room_request_count").html(obj.room_request_count);
            apply_admit_patients_filter_conditions();
            $('#ip_name_check').click();
            $("#admitted_patients_data tbody tr.vacent").hide();

        },
        complete: function () {
            $('body').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}

function gotoNsPatientDashboard(patient_id) {
    window.location.href = $('#base_url').val() + "/nursing/nurse_patient-view/" + patient_id;

    // var url = $('#base_url').val()+"/nursing/checkPatientIPVisitExists";
    // $.ajax({
    //     type: "GET",
    //     url: url,
    //     data: "patient_id=" + patient_id,
    //     beforeSend: function () {
    //         $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
    //     },
    //     success: function (html) {
    //         //$('body').LoadingOverlay("hide");
    //         if(html == 1){
    //             window.location.href = $('#base_url').val() + "/nursing/nurse_patient-view/" + patient_id;
    //         }else{
    //             Command: toastr["warning"]("Patient doesn't have admission!");

    //         }
    //     },
    //     complete: function () {
    //     },
    // });
}

function patient_occupi(bed_id,visit_id) {
    var url = $('#base_url').val() + "/nursing/patientOccupi";
    $.ajax({
        type: "GET",
        url: url,
        data: 'bed_id=' + bed_id +'&visit_id=' + visit_id,
        beforeSend: function () {
            $('#admitted_patients_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Patient is admitted!");
                $station_id = $("#default_nursing_station").val();
                selectDashBoardData($station_id);
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {
            $('#admitted_patients_list_data').LoadingOverlay("hide");
        }
    });
}

function showBedStatusModal(){
    $('#modal_bed_status').modal('toggle');
}

$('#global_search_flag').click(function () {
    if($('#global_search_flag').is(':checked')) {
        $('#table_search').hide();
        $('#global_search').show();
    }else{
        $('#table_search').show();
        $('#global_search').hide();
    }
});

$('#global_search_flag_op_search').click(function () {
    if ($('#global_search_flag_op_search').is(':checked')) {
        $('.patient_local_search').addClass('hidden');
        $('.patient_global_search').removeClass('hidden');
    } else {
        $('.patient_local_search').removeClass('hidden');
        $('.patient_global_search').addClass('hidden');
        $('#patient_name_hidden').val('');
        $('#patient_name').val('');
        $('#patient_nameAjaxDiv').hide();
        search_patient_list();
    }
});

function apply_admit_patients_filter_conditions(){
    var all_count = $("#admitted_patients_data tbody tr.admitpt").length;
    var admitted_but_not_occupied_count = $("#admitted_patients_data tbody tr.2").length;
    var checked_in_count = $("#admitted_patients_data tbody tr.3").length;
    var transfer_requested_count = $("#admitted_patients_data tbody tr.4").length;
    var discharge_intimated_count = $("#admitted_patients_data tbody tr.5").length;
    var sent_for_billing_count = $("#admitted_patients_data tbody tr.6").length;
    var bill_generated_count = $("#admitted_patients_data tbody tr.7").length;

    $('#all_count').html(all_count);
    $('#admitted_but_not_occupied_count').html(admitted_but_not_occupied_count);
    $('#checked_in_count').html(checked_in_count);
    $('#transfer_requested_count').html(transfer_requested_count);
    $('#discharge_intimated_count').html(discharge_intimated_count);
    $('#sent_for_billing_count').html(sent_for_billing_count);
    $('#bill_generated_count').html(bill_generated_count);
}

function applyFilter (select_value){
    $("#admitted_patients_data tbody tr").hide();
    $("#admitted_patients_data tbody tr."+select_value).show();
    if(select_value == 0){
        $("#admitted_patients_data tbody tr").show();
    }
}

function sortTable(tableId,n,typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.getElementsByTagName("TR");
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 1; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        /*check if the two rows should switch place,
        based on the direction, asc or desc:*/
          if(typeid == 1){
             if (dir == "asc") {
                if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
              } else if (dir == "desc") {
                if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
             }
          }else{
              if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
              } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
             }
          }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        //Each time a switch is done, increase this count by 1:
        switchcount ++;
      } else {
        /*If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again.*/
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }

function search_patient(search_key) {
    var keytype = $('input[name="search_type"]:checked').val();
    search_key = search_key.toUpperCase();
    $('.patient_det_widget').css('display', 'none');
    if(keytype == 1){
        $(".patient_det_widget[data-patientname *='"+search_key+"']").css('display','block');
        }else if(keytype == 2){
        $(".patient_det_widget[data-uhid *='"+search_key+"']").css('display','block');
    }else{
        $(".patient_det_widget[data-phone *='"+search_key+"']").css('display','block');
    }
    if(search_key ==''){
        $('.patient_det_widget').css('display', 'block');
    }
}

var public_sort_var = 2;
function myFunction(search_type,InputId,tableId){
 var searchType = $('input[name='+search_type+']:checked').val();
 var serch_method_conf = "anywhere";
// alert(searchType);return;
  var input, filter, table, tr, td, i;
  input = document.getElementById(InputId);
  filter = input.value.toUpperCase();
  table = document.getElementById(tableId);
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[searchType];
    if (td) {
        if(serch_method_conf == "anywhere"){
            if (td.innerHTML.toUpperCase().match(filter)) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
        }else{
            if (td.innerHTML.toUpperCase().startsWith(filter)) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
        }
    }
    $('.theadscroll').perfectScrollbar("update");
  }

    if(searchType == 5){
      sortTable('myTable',5,public_sort_var);
        if(public_sort_var == 1){
          public_sort_var = 2;
        }else{
          public_sort_var = 1;
        }
    }


}
// function patient_general_search(txt){
//     var last_search_key = '';
//     var request_flag = '';
//     if(txt.length >= 2 && $.trim(txt) != ''){

//         if(last_search_key == txt || request_flag == 1 ){
//           return false;
//         }
//         last_search_key = txt;
//         var url = $('#base_url').val()+"/nursing/nursingPatientSearch";
//         $.ajax({
//                 type: "GET",
//                 url: url,
//                 data: 'txt='+txt,
//                 beforeSend: function () {
//                   $(".input_rel .input_spinner").removeClass('hide');
//                   request_flag = 1
//                 },
//                 success: function (data) {
//                   $("#completepatientbox").show();
//                   $("#completepatientbox").html(data);
//                   $(".input_rel .input_spinner").addClass('hide');
//                   request_flag = 0;
//                 }
//               });
//         }
// }
$(document).on('keyup','.hidden_search',function(event) {
    
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    console.log(input_id);
    //alert(input_id);return;
    var current;
    if ((value.match(keycheck) || event.keyCode == '8') && (event.keyCode != '40' && event.keyCode != '38' && event
    .keyCode != '13')) {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val()+"/nursing/ajax_patient_search";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajaxProgressiveKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name,serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
    $("#" + ajax_div).hide();
}

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}
//--------------patient name search -------------------------

$('#op_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#op_no_hidden').val() != "") {
            $('#op_no_hidden').val('');
        }
        var op_no = $(this).val();
        op_no = op_no.replace("/[^\w\s-_\.]/gi");
        op_no = op_no.trim();
        if (op_no == "") {
            $("#OpAjaxDiv").html("");
        } else {
            var url = $('#base_url').val()+"/nursing/nursingPatientSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: "op_no=" + op_no,
                beforeSend: function () {

                    $("#OpAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    console.log(html);
                    $("#OpAjaxDiv").html(html).show();
                    $("#OpAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                    //  $('#loading_image').hide();
                }
            });
        }

    } else {
        ajax_list_key_down('OpAjaxDiv', event);
    }
});

/* setting for enter key press in ajaxDiv listing */
$("#op_no").on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('OpAjaxDiv');
        return false;
    }
});

function ajaxlistenter(ajax_div){
    $("#"+ajax_div).find('li.liHover').trigger('click');
}

//--------------patient name search ends--------------------------

function loadRoomsChart(default_nursing_station){
    var url = $('#base_url').val()+"/nursing/selectBedCount";
    $.ajax({
        type: "GET",
        url: url,
        data: "default_nursing_station=" + default_nursing_station,
        beforeSend: function () {
            $('#roomChart').remove();
            $('#roomChart_graph-container').append('<canvas id="roomChart" width="230" height="230" style="margin: 10px 10px 10px 0"></canvas>');

        },
        success: function (html) {
            var obj = JSON.parse(html);
            $('#room_status_legend_container').html( obj.roomTypeLegends);

            if(html != 0){

                var options = {
                    plugins: {
                        legend: {
                            labels: {
                                fontcolor: '#00FF00'
                            },
                            position: 'bottom',
                            display: false,

                        }
                    },
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Rooms Status',
                        fontSize: 15,
                        fontStyle: 'bold'
                    },
                    responsive: true,

                    onClick: function(evt,val) {

                        var activePoints = myChart.getElementsAtEvent(evt);
                        if (activePoints[0]) {
                            var chartData = activePoints[0]['_chart'].config.data;
                            var idx = activePoints[0]['_index'];

                            var label = chartData.labels[idx];
                            var value = chartData.datasets[0].data[idx];

                            filterAsRoomChart(label,default_nursing_station);


                        }

                    }
                };

                var myChart = new Chart(document.getElementById("roomChart"), {
                    type: 'pie',
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    data: {

                          // labels : [],
                          labels: obj.label_array,

                        datasets: [{
                            data : obj.count_array,
                            backgroundColor : obj.color_array,
                        }]
                    },
                    options: options
                });

            }

        },
        complete: function () {

        }
    });
}

function loadRoomsTypes(default_nursing_station){
    var url = $('#base_url').val()+"/nursing/loadRoomsTypes";
    $.ajax({
        type: "GET",
        url: url,
        data: "default_nursing_station=" + default_nursing_station,
        beforeSend: function () {

            $('#roomTypeChart').remove();
            $('#roomTypeChart_graph-container').append('<canvas id="roomTypeChart" width="230" height="230" style="margin: 10px 10px 10px 0"></canvas>');
        },

        success: function (html) {
            console.log(html);
            var obj = JSON.parse(html);
            if(html != 0){

                var options = {
                    plugins: {
                        legend: {
                            display: true,
                            labels: {
                                color: 'rgb(255, 99, 132)'
                            },
                            position: 'bottom',
                        }
                    },
                    title: {
                        display: true,
                        text: 'Room List',
                        fontSize: 15,
                        fontStyle: 'bold'
                    },

                    barValueSpacing: 20,
                    scales: {
                      yAxes: [{
                        ticks: {
                          min: 0,
                        }
                      }]
                    },

                    responsive: true,
                    barValueSpacing: 20,
                      scales: {
                        yAxes: [{
                          ticks: {
                            min: 0,
                          }
                        }]
                      },

                    onClick: function(evt,val) {

                        var activePoints = roomTypeChart.getElementsAtEvent(evt);
                        if (activePoints[0]) {
                            var chartData = activePoints[0]['_chart'].config.data;
                            var idx = activePoints[0]['_index'];

                            var label = chartData.labels[idx];
                            var value = chartData.datasets[0].data[idx];
                            filterAsRoomTypes(label,default_nursing_station);

                        }

                    },


                };

                var data = {
                    labels: obj.room_types,
                    datasets: [
                        {
                            label: "Total",
                            backgroundColor: "#8F82E9",
                            data: obj.totalRooms,
                        },

                        {
                            label: "Occupied",
                            backgroundColor: "#c76000",
                            data: obj.occupied,
                        },

                    ]
                };

                var roomTypeChart = new Chart(document.getElementById("roomTypeChart"), {
                    type: 'bar',
                    data: data,
                    options: options,
                  });


            }
        },
        complete: function () {

        },
    });
}

function filterAsRoomTypes(str,nursing_station){
    str = str.split(' -')[0];

    $("#admitted_patients_data tbody tr").hide();
    $("#admitted_patients_data tbody tr."+str).show();

}

function filterAsRoomChart(room_status,nursing_station){
    room_status = room_status.split('(')[0];
    room_status = room_status.toLowerCase();
    showBedsUsingRoomsFilter(room_status,nursing_station);
    $("#admitted_patients_data tbody tr").hide();
    $("#admitted_patients_data tbody tr."+room_status).show();
}

function listroomsInCurrentStatus(room_status){
    room_status = room_status.toLowerCase();
    alert(room_status);
}

$(".poplink").popover({
    html: true,
    placement: "right",
    trigger: "hover",

    content: function () {
        return $(".pop-content").html();
    }
});


function showBedsUsingRoomsFilter(str,nursing_station){

    //alert(str);return;
    var url = $('#base_url').val()+"/nursing/showBedsUsingRoomStatusFilter";
    $('#popRoomType').html('');
    $.ajax({
        type: "GET",
        url: url,
        data: "nursing_station=" + nursing_station + '&room_status=' + str,
        beforeSend: function () {
            $('#popRoomchart').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            //console.log(html);
            $('#popRoomchart').html(html);
            $(this).popover('show');

        },
        complete: function () {
            $('#popRoomchart').LoadingOverlay("hide");
        },
    });
}

function markOcucpiAlert(){
    Command: toastr["info"]("Please admit patient first!");
}

function openRoomRequestList(){
    var nursing_station = $('#default_nursing_station').val();
    var url = $('#base_url').val()+"/nursing/openRoomRequestList";
    $.ajax({
        type: "GET",
        url: url,
        data: "nursing_station=" + nursing_station,
        beforeSend: function () {
            $('#room_transfer_request_list_modal').modal('show');
            $('#room_transfer_request_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#room_transfer_request_data').LoadingOverlay("hide");
            $('#room_transfer_request_data').html(html);
        },
        complete: function () {
        },
    });
}

function op_patient_list(){
    $("#op_patient_list").modal({
        backdrop: 'static',
        keyboard: false
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    search_patient_list()
}

function search_patient_list(page = 1){
    var data = {};
    var url = $('#base_url').val()+"/nursing/op_patient_list";
    data.page = page;
    data.from_date = $('#from_date').val();
    data.to_date = $('#to_date').val();
    data.patient_name = $('#patient_name').val();
    data.patient_id = $('#patient_name_hidden').val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $('#btn_search_op_patient_list').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-search');
        },
        success: function (html) {
            $('#op_patient_list_data').LoadingOverlay("hide");
            $('#op_patient_list_data').html(html); 
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });          
        },
        complete: function () {
            $('#btn_search_op_patient_list').find('i').addClass('fa-search').removeClass('fa-spinner fa-spin');
            setTimeout(function() {
                $(".theadscroll").perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $("table.theadfix_wrapper");
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest(".theadscroll");
                    }
                });
                $(".theadscroll").perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead("reflow");
            }, 1000);
        },
    });
}

$('#op_patient_list').on('hidden.bs.modal', function() { 
    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    $('#patient_name').val('');
    $('#patient_name_hidden').val('');
    $('#op_patient_search_txt').val('');
    $('#global_search_flag_op_search').prop('checked', false);
    $('.patient_local_search').removeClass('hidden');
    $('.patient_global_search').addClass('hidden');
});

$(document).on('click', '.page-link', function (e) {
    e.preventDefault();
    if ($(e.target).parent().hasClass('active') == false) {
        search_patient_list($(this).attr('href').split('page=')[1]);
    }
});
function selectRoomRequestBed(id,patient_id,visit_id,nursing_station,retain_status){
    var url = $('#base_url').val()+"/nursing/selectRoomRequestBed";
    var nursing_station = $('#default_nursing_station').val();
    $.ajax({
        type: "GET",
        url: url,
        data: "request_id=" + id
              +"&nursing_station="+nursing_station
              +"&patient_id="+patient_id
              +"&visit_id="+visit_id
              +"&retain_status="+retain_status,
        beforeSend: function () {
            $('#room_transfer_request_choose_bed_modal').modal('show');
            $('#room_transfer_request_choose_bed').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#room_transfer_request_choose_bed').LoadingOverlay("hide");
            $('#room_transfer_request_choose_bed').html(html);
            $('#room_request_request_id').val(id);
            $('#room_request_visit_id').val(visit_id);
            $('#room_request_patient_id').val(patient_id);
            $('#room_request_retain_status').val(retain_status);
        },
        complete: function () {
        },
    });
}

function admitRoomRequest(){
    var url = $('#base_url').val()+"/nursing/admitRoomRequest";
    var request_id = $('#room_request_request_id').val();
    var visit_id = $('#room_request_visit_id').val();
    var patient_id = $('#room_request_patient_id').val();
    var room_type = $('#room_request_room_type').val();
    var retain_status = $('#room_request_retain_status').val();
    var nursing_station = $('#default_nursing_station').val();
    var bed_id = $('input[name="room_transfer_bed"]:checked').val();
    $.ajax({
        type: "GET",
        url: url,
        data: "request_id=" + request_id
              +"&patient_id="+patient_id
              +"&visit_id="+visit_id
              +"&room_type="+room_type
              +"&nursing_station="+nursing_station
              +"&bed_id="+bed_id
              +"&retain_status="+retain_status,
        beforeSend: function () {
            $('#room_transfer_request_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#room_transfer_request_data').LoadingOverlay("hide");
            var obj = JSON.parse(html);
            if(obj.status == 1){
                Command: toastr["success"](obj.message);
                $('#room_transfer_request_choose_bed_modal').modal('hide');
                //openRoomRequestList();
                location.reload();
            }else{
                Command: toastr["error"](obj.message);
            }

        },
        complete: function () {
        },
    });
}

$('#show_vacent_beds').on('click', function(){
    if(this.checked == true){
        $("#admitted_patients_data tbody tr.vacent").show();
    }else{
        $("#admitted_patients_data tbody tr.vacent").hide();
    }
});
