$(document).ready(function() {
    $(".select2").select2();
    makeTableDraggable();
    listRadiologyScheduleOnDateChange();
    $(".datepicker").datetimepicker({
        format: "MMM-DD-YYYY"
    });
    var $table = $("table.theadfix_wrapper");
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest(".theadscroll");
        }
    });
    $(".theadscroll").perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $(".fixed_header").floatThead({
        position: "absolute",
        scrollContainer: true
    });
});
var base_url = $("#base_url").val();
var token = $("#c_token").val();

function listRadiologyScheduleOnDateChange() {
    var schedule_date = $("#schedule_date").val();
    var url = base_url + "/radiology_schedule/listRadiologySchedule";
    $.ajax({
        url: url,
        type: "GET",
        data: { schedule_date: schedule_date },
        beforeSend: function() {
            $("#time_allocation_container").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(data) {
            $("#time_allocation_container").html(data);
            $("#time_allocation_container").LoadingOverlay("hide");
            makeTableDraggable();

        },
        complete: function() {
            $("#time_allocation_container").LoadingOverlay("hide");
            setTimeout(function() {
                $('[data-toggle="tooltip"]').tooltip();

                var schedules = $("#schedule_id_hidden_val").val();
                if(schedules != ''){
                schedules = schedules.split(",");
                $.each(schedules, function(key, val) {
                    $("." + val).each(function(key1, val1) {
                        if (key1 == 0) {
                            $(val1).attr("rowspan", $("." + val).length);
                        } else {
                            $(val1).remove();
                        }
                    });
                });
            }
                var $table = $("table.theadfix_wrapper");
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest(".theadscroll");
                    }
                });
                $(".theadscroll").perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $(".fixed_header").floatThead({
                    position: "absolute",
                    scrollContainer: true
                });
            }, 200);

        }
    });
}
function makeTableDraggable() {
    var isMouseDown = false;
    $("#shedule_time_table td")
        .mousedown(function() {
            isMouseDown = true;
            if ($(this).hasClass("time_list")) {
                return false;
            }

            $(this).toggleClass("highlighted");
            return false; // prevent text selection
        })
        .mouseover(function() {
            if (isMouseDown) {
                if ($(this).hasClass("time_list")) {
                    return false;
                }

                $(this).toggleClass("highlighted");
            }
        });

    $(document).mouseup(function() {
        isMouseDown = false;
    });
}
function saveRadiologyschedule() {
    var ot_id = $(".highlighted").attr("data-ot");
    var start_time = "";
    var stop_time = "";
    var i = 0;

    var service_id = $("#service_id").val();
    var radiologist = $("#radiologist").val();
    var schedule_date = $("#schedule_date").val();
    var patient_name = $("#patient_name").val();
    if (patient_name == "") {
        toastr.warning("Please select patient");
        return false;
    }
    if (schedule_date == "") {
        toastr.warning("Please select date");
        return false;
    }
    if (radiologist == "-1") {
        toastr.warning("Please select radiologist");
        return false;
    }
    if (service_id == "-1") {
        toastr.warning("Please select Procedure");
        return false;
    }
    var patient_name_hidden = $("#patient_name_hidden").val();
    var j = 0;
    var current_column = ot_id;
    var is_multiple_ot_selected = 0;
    $(".highlighted").each(function() {
        if (j == 0) {
            current_column = $(this).attr("data-ot");
        }
        if ($(this).attr("data-ot") != current_column) {
            is_multiple_ot_selected = 1;
        }
        j++;
    });

    if (is_multiple_ot_selected == 1) {
        toastr.warning("Aviod multiple selection!");
        return false;
    }

    $(".highlighted").each(function() {
        if (i == 0) {
            start_time = $(this).attr("data-attr-time");
        }
        stop_time = $(this).attr("data-attr-time");
        i++;
    });
    if (start_time == "" || stop_time == "") {
        toastr.warning("Please schedule time and theater !");
        return false;
    }

    start_time = start_time.split("-")[0];
    stop_time = stop_time.split("-")[1];
    var url = base_url + "/radiology_schedule/saveRadiologySchedule";
    var dataparams = {
        service_id: service_id,
        radiologist: radiologist,
        schedule_date: schedule_date,
        start_time: start_time,
        stop_time: stop_time,
        machine_id: ot_id,
        patient_name_hidden: patient_name_hidden
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function() {
            $("#time_allocation_container").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(data) {
            if (data == 1) {
                toastr.success("Success fully Created ");
                listRadiologyScheduleOnDateChange();
            }else{
                toastr.error("Error Occured..!");
            }
        },
        complete: function() {
            $("#time_allocation_container").LoadingOverlay("hide");
        }
    });
}
function deleteRadiologySchedule(shedule_id) {
    var url = base_url + "/radiology_schedule/deleteRadiologySchedule";

    bootbox.confirm({
        message: "Are you sure ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success",
                default: "true"
            },
            cancel: {
                label: "No",
                className: "btn-warning"
            }
        },
        callback: function(result) {
            if (result) {

                var dataparams = { shedule_id: shedule_id };
                $.ajax({
                    url: url,
                    type: "POST",
                    data: dataparams,
                    beforeSend: function() {
                        $("#time_allocation_container").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#009869"
                        });
                    },
                    success: function(data) {
                        $("#time_allocation_container").LoadingOverlay("hide");
                        if (data == 1) {
                            toastr.success("Success fully deleted ");
                            listRadiologyScheduleOnDateChange();
                        }else{
                            toastr.error("Error Occured..!");
                        }
                    }
                });
            }
        }
    });
}
$("#patient_name").keyup(function(event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#AjaxDiv").html("");
            $("#patient_name_hidden").val("");
        } else {
            try {
                var send_url = base_url + "/modality/modalityList";
                var param = {
                    search_string: op_no_search,
                    op_no_search_prog: 1,ident:'patient_name'
                };
                $.ajax({
                    type: "GET",
                    url: send_url,
                    data: param,
                    beforeSend: function() {
                        $("#AjaxDiv")
                            .html(
                                '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            )
                            .show();
                    },
                    success: function(html) {
                        $("#AjaxDiv")
                            .html(html)
                            .show();
                        $("#AjaxDiv")
                            .find("li")
                            .first()
                            .addClass("liHover");
                    },
                    complete: function() {}
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown("AjaxDiv", event);
    }
});
$("#patient_name").on("keydown", function(event) {
    if (event.keyCode === 13) {
        ajaxlistenter("AjaxDiv");
        return false;
    }
});
function fillSearchDetials(id, uhid, patient_name, input_id) {
    $("#patient_name_hidden").val(id);
    $("#patient_name").val(patient_name);
    $("#AjaxDiv").hide();
}
