$(document).ready(function () {
    $(document).ready(function () {
        $('#patient_name').keyup(function (event) {
            var keycheck = /[a-zA-Z0-9 ]/;
            var value = event.key;
            var current;
            if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                if ($('#patient_id_hidden').val() != "") {
                    $('#patient_id_hidden').val('');
                }
                var patient_name = $(this).val();
                patient_name = patient_name.trim();
                if (patient_name == "") {
                    $("#patient_idAjaxDiv").html("");
                } else {
                    var url = '';
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: "patient_name_search=" + patient_name,
                        beforeSend: function () {
                            $("#patient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                        },
                        success: function (html) {
                            //  alert(html); return;
                            $("#patient_idAjaxDiv").html(html).show();
                            $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                        }
                    });
                }

            }
        });
        $('.datepicker').datetimepicker({
            format: 'MMM-DD-YYYY',
        });
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });
    });

});


function fillPatientValues(uhid, patient_name, company_id) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name_div').html('Patient Name :' + patient_name);
    $('#patient_uhid_div').html('UHID :' + uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
    $('#companyid').val(company_id);
    $('#companyid').select2();
    getCompanyType();
    searchBill();
}

function checkallvisit() {
    var status = $('#checkallvisit').is(":checked");
    if (status) {
        $('.visit_class').prop('checked', true);
    } else {
        $('.visit_class').prop('checked', false);
    }
}

function searchBill() {
    var base_url = $('#base_url').val();
    var patient_name = $('#patient_name').val();
    var bill_no = $('#bill_number').val();
    var all_bills = $('#all_bills').is(":checked");
    var uhid = '';
    if (patient_name || bill_no) {
        uhid = $('#patient_uhid_hidden').val();
        var url = base_url + "/purchase/getPatientPaymode";
        var param = { uhid: uhid, bill_no: bill_no, all_bills: all_bills };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled', true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
            },
            success: function (msg) {
                $('#pending_bil_list').html(msg);
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                $('#companydetailsdiv').show();
            }, error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient Or Bill");
    }

}


function changepaymentType() {
    var companyid = $('#companyid').val();
    var base_url = $('#base_url').val();
    var companypricing = $('#companypricing').val();
    var companytype = $('#companytype').val();
    var visit_array = [];
    $('.visit_class').each(function (i, obj) {
        var status = $(this).is(":checked");
        if (status) {
            visit_array.push($(this).val());
        }
    });
    if (companyid && companytype && companypricing) {
        if (length.visit_array != 0) {
            var message_show = 'Are you sure you want to Change This Payment';
            bootbox.confirm({
                message: message_show,
                buttons: {
                    'confirm': {
                        label: 'Okay',
                        className: 'btn-primary',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-warning'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var visit_arraystring = JSON.stringify(visit_array);
                        var url = base_url + "/purchase/changepaymentMode";
                        var param = { visit_arraystring: visit_arraystring, companypricing: companypricing, companyid: companyid, companytype: companytype };
                        $.ajax({
                            type: "GET",
                            async: true,
                            url: url,
                            data: param,
                            cache: false,
                            beforeSend: function () {
                                $('#paymentTypeBtn').attr('disabled', true);
                                $('#paymentTypeSpin').addClass('fa fa-save');
                                $('#paymentTypeSpin').addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                if (parseInt(data) == 1) {
                                    toastr.success("Successfully Updated");
                                    searchBill();
                                } else {
                                    toastr.error("Please check your internet connection and try again");
                                }
                            },
                            complete: function () {
                                $('#paymentTypeBtn').attr('disabled', false);
                                $('#paymentTypeSpin').removeClass('fa fa-spinner fa-spin');
                                $('#paymentTypeSpin').addClass('fa fa-save');
                            }
                        });

                    }
                }
            });
        } else {
            toastr.warning('Please select any Bill');
        }
    } else {
        toastr.warning('Please select any Company Pricing and Type ');
    }
}

function getCompanyType() {
    var base_url = $('#base_url').val();
    var company_type = $('#companyid').val();
    if (company_type) {
        var url = base_url + "/purchase/getCompanyType";
        $.ajax({
            type: "GET",
            url: url,
            data: "&company_type=" + company_type,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled', true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
            },
            success: function (data) {
                $('#getcompanytypediv').html(data);
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
            }, error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });
    }
}
