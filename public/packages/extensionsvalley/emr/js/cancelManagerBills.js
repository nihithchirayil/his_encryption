$(document).ready(function () {

    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //POST the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('patient_idAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var value = event.key;
            var patient_name = $(this).val();
            patient_name = patient_name.trim();
            if (patient_name == "") {
                $("#patient_idAjaxDiv").html("").hide();
                $('#patient_id_hidden').val('');
            } else {
                var url = '';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: "patient_name_search=" + patient_name,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        //  alert(html); return;
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("patient_idAjaxDiv", event);
        }
    });

    //-----------Bill No search------------
    $('#bill_no').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //POST the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('bill_no_AjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var value = event.key;
            var cancelled_bills = $('#cancelled_bills_only').is(":checked");
            var bill_no = $(this).val();
            if (!bill_no) {
                $("#bill_no_AjaxDiv").html("").hide();
                $('#bill_no_hidden').val('');
            } else {
                var url = '';
                var param = {
                    bill_no: bill_no,
                    cancelled_bills: cancelled_bills
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#bill_no_AjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        $("#bill_no_AjaxDiv").html(html).show();
                        $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("bill_no_AjaxDiv", event);
        }
    });

});

/* For Patient Search, filling values */
function fillPatientValues(uhid, patient_name, from_type = 0) {
    var iscancel_reqVisible = $('#cancel_bill_req_modal').is(':visible');
    if (iscancel_reqVisible) {
        $('#req_patient_uhid_hidden').val(uhid);
        $('#req_patient_name').val(patient_name);
        $('#req_patient_name_AjaxDiv').hide(); 
        searchCancelReqBill();
    } else {
        $('#patient_uhid_hidden').val(uhid);
        $('#patient_name').val(patient_name);
        $('#patient_idAjaxDiv').hide();
        searchBill();
    }  
}

/* For BillNo Search, filling values */
function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
    var iscancel_reqVisible = $('#cancel_bill_req_modal').is(':visible');
    if (iscancel_reqVisible) {
        $('#req_bill_no').val(bill_no);
        $('#req_patient_uhid_hidden').val(uhid);
        $('#req_bill_id_hidden').val(bill_id);
        $('#req_bill_no_AjaxDiv').hide();
        if (from_type != 1) {
            fillPatientValues(uhid, patient_name, 1);
        }
    } else {
        $('#bill_no').val(bill_no);
        $('#patient_uhid_hidden').val(uhid);
        $('#bill_id_hidden').val(bill_id);
        $('#bill_no_AjaxDiv').hide();
        if (from_type != 1) {
            fillPatientValues(uhid, patient_name, 1);
            searchBill();
        }
    }
}

/*For Fetch Bill Details */
function fetchBill(id, bill_tag) {
    var url = '';
    var iscancel_reqVisible = $('#cancel_bill_req_modal').is(':visible');
    var scree_type = 0;
    if (iscancel_reqVisible) {
        scree_type = 1;
    }
    var param = { fetch: 1, bill_id: id, bill_tag: bill_tag, scree_type: scree_type };
    $.ajax({
        type: "POST",
        data: param,
        url: url,
        beforeSend: function () {
            if (iscancel_reqVisible) {
                $('#fetchBillReqBtn' + id).attr('disabled', true);
                $('#fetchBillReqSpin' + id).removeClass('fa fa-eye');
                $('#fetchBillReqSpin' + id).addClass('fa fa-spinner fa-spin');
            } else {
                $("#cancelBillDetails").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                $('#fetchBillBtn' + id).attr('disabled', true);
                $('#fetchBillSpin' + id).removeClass('fa fa-eye');
                $('#fetchBillSpin' + id).addClass('fa fa-spinner fa-spin');
            }
        },
        success: function (msg) { //alert(msg); return;
            if (iscancel_reqVisible) { 
                $('#cancel_bill_view_data').html(msg);
                $("#cancel_bill_view_modal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                $('#cancelBillDetails').html(msg);
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            
        },
        complete: function () {
            if (iscancel_reqVisible) {
                $('#fetchBillReqBtn' + id).attr('disabled', false);
                $('#fetchBillReqSpin' + id).removeClass('fa fa-spinner fa-spin');
                $('#fetchBillReqSpin' + id).addClass('fa fa-eye');
            } else {
                $("#cancelBillDetails").LoadingOverlay("hide");
                $('#fetchBillBtn' + id).attr('disabled', false);
                $('#fetchBillSpin' + id).removeClass('fa fa-spinner fa-spin');
                $('#fetchBillSpin' + id).addClass('fa fa-eye');
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            }
        },

        error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

function cancel_approve_revertbill(bill_id, from_type, list_type, bill_type) {
    var print_string = " Cancel of this bill ?";
    var sucess_string = "Bill Cancelled Successfully";
    if (from_type == '2') {
        print_string = "  Revert of this bill ?";
        sucess_string = "Bill Reveted Successfully";
    }
    var message_show = 'Are you sure you want to' + print_string;

    var iscancel_reqVisible = $('#cancel_bill_req_modal').is(':visible');
    if (iscancel_reqVisible) {
        var CancelCreditBills = $("#ReqCancelCreditBills").is(":checked");
    } else {
        var CancelCreditBills = $("#CancelCreditBills").is(":checked");
    }
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Yes',
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'No',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var param = {
                    bill_id: bill_id,
                    from_type: from_type,
                    bill_type: bill_type,
                    CancelCreditBills: CancelCreditBills
                };
                var base_url = $('#base_url').val();
                var url = base_url + "/purchase/approveCancelBill";
                $.ajax({
                    type: "POST",
                    async: true,
                    url: url,
                    data: param,
                    cache: false,
                    beforeSend: function () {
                        if (iscancel_reqVisible) { 
                            if (list_type == '1') {
                                $('#req_approve_reject').attr('disabled', true);
                                $('#req_approve_reject_spin').removeClass('fa fa-save');
                                $('#req_approve_reject_spin').addClass('fa fa-spinner fa-spin');
                            } else if (list_type == '2') {
                                $('#approve_Reqreject_btn' + bill_id).attr('disabled', true);
                                $('#approve_Reqreject_spin' + bill_id).removeClass('fa fa-save');
                                $('#approve_Reqreject_spin' + bill_id).addClass('fa fa-spinner fa-spin');
                            }
                        } else {
                            if (list_type == '1') {
                                $('#approve_reject_btn').attr('disabled', true);
                                $('#approve_reject_spin').removeClass('fa fa-save');
                                $('#approve_reject_spin').addClass('fa fa-spinner fa-spin');
                            } else if (list_type == '2') {
                                $('#approve_reject_btn' + bill_id).attr('disabled', true);
                                $('#approve_reject_spin' + bill_id).removeClass('fa fa-save');
                                $('#approve_reject_spin' + bill_id).addClass('fa fa-spinner fa-spin');
                            }
                        }
                    },
                    success: function (data) {
                        if (data == 1) {
                            toastr.success(sucess_string);
                            if (iscancel_reqVisible) { 
                                var page = $('#table_req_cancel_table').attr('table_page_count');
                                searchCancelReqBill(page);
                            } else {
                                searchBill();
                            }
                        } else {
                            toastr.error("Error Please Check Your Internet Connection");
                        }

                    },
                    complete: function () {
                        if (iscancel_reqVisible) { 
                            if (list_type == '1') {
                                $('#req_approve_reject').attr('disabled', false);
                                $('#req_approve_reject_spin').removeClass('fa fa-spinner fa-spin');
                                $('#req_approve_reject_spin').addClass('fa fa-save');
                                $("#cancel_bill_view_modal").modal('hide');
                            } else if (list_type == '1') {
                                $('#approve_reject_btn' + bill_id).attr('disabled', false);
                                $('#approve_reject_spin' + bill_id).removeClass('fa fa-spinner fa-spin');
                                $('#approve_reject_spin' + bill_id).addClass('fa fa-save');
                            }
                        } else {
                            if (list_type == '1') {
                                $('#approve_reject_btn').attr('disabled', false);
                                $('#approve_reject_spin').removeClass('fa fa-spinner fa-spin');
                                $('#approve_reject_spin').addClass('fa fa-save');
                            } else if (list_type == '1') {
                                $('#approve_reject_btn' + bill_id).attr('disabled', false);
                                $('#approve_reject_spin' + bill_id).removeClass('fa fa-spinner fa-spin');
                                $('#approve_reject_spin' + bill_id).addClass('fa fa-save');
                            }
                        }
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }
        }
    });
}

function searchBill() {
    var patient_name = $('#patient_name').val();
    var uhid = $('#patient_uhid_hidden').val();
    var bill_id = $('#bill_id_hidden').val();
    var cancelled_bills = $('#cancelled_bills_only').is(":checked");
    var bill_no = $('#bill_no').val();
    var param = {
        search: 1,
        patient_name: patient_name,
        bill_id: bill_id,
        patient_uhid_hidden: uhid,
        bill_no: bill_no,
        cancelled_bills: cancelled_bills
    };
    if (patient_name || bill_no) {
        $.ajax({
            type: "POST",
            url: '',
            data: param,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled', true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner fa-spin');
            },
            success: function (msg) {
                $('#cancelPatientDetails').html(msg);
                $('#cancelBillDetails').html('');
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
                $('#searchdataspin').addClass('fa fa-search');
            },
            error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });

    } else {
        toastr.warning("Please enter Patient Name or Bill No.!");
    }
}

// New Search Cancel ReqBill
$(document).on('click', '.page-link', function (e) {
    e.preventDefault();
    if ($(e.target).parent().hasClass('active') == false) {
        searchCancelReqBill($(this).attr('href').split('page=')[1]);
       
    }
});

function searchCancelReqBill(page =1) {
    var iscancel_reqVisible = $('#cancel_bill_req_modal').is(':visible');
    if (!iscancel_reqVisible) {
        $('#req_patient_name_hidden').val('');
        $('#req_patient_uhid_hidden').val('');
        $('#req_patient_name').val('');
        $('#req_bill_no').val('');
        $('#req_bill_no_hidden').val('');
        $('#req_bill_id_hidden').val('');
    }
    var patient_name = $('#req_patient_name').val();
    var uhid = $('#req_patient_uhid_hidden').val();
    var bill_id = $('#req_bill_id_hidden').val();
    var bill_no = $('#req_bill_no').val();    
    var cancelled_bills = $('#req_cancelled_bills_only').is(":checked");    
    var param = {
        search: 1,
        patient_name: patient_name,
        bill_id: bill_id,
        patient_uhid_hidden: uhid,
        bill_no: bill_no,
        cancelled_bills: cancelled_bills,
        page: page       
    };
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/cancelRequestedBills";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {           
            $('#search_cancelReqbillbtn').attr('disabled', true);
            $('#search_cancelReqbillbtn').find('i').removeClass('fa fa-search');
            $('#search_cancelReqbillbtn').find('i').addClass('fa fa-spinner fa-spin');
            if (!iscancel_reqVisible) {
                $('#search_reqcancelbtn').attr('disabled', true);
                $('#search_reqcancelbtn').find('i').removeClass('fa fa-list');
                $('#search_reqcancelbtn').find('i').addClass('fa fa-spinner fa-spin');               
                $('.ajaxSearchBox').hide();
            }
        },
        success: function (data) {
            $('#cancel_bill_req_modal_data').html(data);
            $("#cancel_bill_req_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });         
        },
        complete: function () {
            $('#search_cancelReqbillbtn').attr('disabled', false);
            $('#search_cancelReqbillbtn').find('i').removeClass('fa fa-spinner fa-spin');
            $('#search_cancelReqbillbtn').find('i').addClass('fa fa-search');
            if (!iscancel_reqVisible) {
                $('#search_reqcancelbtn').attr('disabled', false);
                $('#search_reqcancelbtn').find('i').removeClass('fa fa-spinner fa-spin');
                $('#search_reqcancelbtn').find('i').addClass('fa fa-list');
            }
            $('#table_req_cancel_table').attr('table_page_count', page);
        },
        error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

function resetReqcancell() {
    $('#req_patient_name_hidden').val('');
    $('#req_patient_uhid_hidden').val('');
    $('#req_patient_name').val('');
    $('#req_bill_no').val('');
    $('#req_bill_no_hidden').val('');
    $('#req_bill_id_hidden').val('');
    $('.ajaxSearchBox').hide();
    searchCancelReqBill();
}

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    if (event.keyCode == 13) {
        ajaxlistenter(input_id + '_AjaxDiv');      
    return false;    
    }  else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();
        if (search_key == "") {
            $('#' + input_id + '_AjaxDiv').html("").hide();
            $('#' + input_id + '_hidden').val('');
            if (input_id == 'req_patient_name') {
                $('#req_patient_uhid_hidden').val('');
            } else {
                $('#req_bill_id_hidden').val('');
            }
        } else {
            var cancelled_bills = $('#req_cancelled_bills_only').is(":checked");
            if (input_id == 'req_patient_name') {
                var param = {
                    patient_name_search: search_key                    
                };
            } else {
                var param = {
                    bill_no: search_key,
                    cancelled_bills: cancelled_bills
                };
            }     
                
            var url = '';
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#' + input_id + '_AjaxDiv').html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    //  alert(html); return;
                    $('#' + input_id + '_AjaxDiv').html(html).show();
                    $('#' + input_id + '_AjaxDiv').find('li').first().addClass('liHover');
                },
                error: function () {
                    toastr.error("Please Check Internet Connection");
                }
            });
        }

    } else {
        ajax_list_key_down(input_id + '_AjaxDiv', event);
    }   
});
