function downloadExcelData() {
    var base_url = $('#base_url').val();
    var url = base_url + '/flexy_report/ServiceBillDetailDataExcel';
    var form_data = $('#servicebilldetail_form').serialize();
    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;

        filters_value = $('#' + filters_id).val();
        if (Array.isArray(filters_value)) {

            var filter_aarry = jQuery.grep(filters_value, function (n, i) {
                return (n !== "" && n != null);
            });


            if (filter_aarry.length == 0) {
                filters_value = '';
            }

            if (filter_aarry.find(function (element) {
                return element == 'All';
            })) {
                filters_value = '';
            }
        }
        filters_id = filters_id.replace('_hidden', '');

        if (filters_value && filters_value !== "" && filters_value !== "All" && filters_value !== [] && filters_value !== null && filters_value != 'All') {
            filters_list[filters_id] = filters_value;
        }
    });
    var visit_type = $("#visit_type").val();
    filters_list['visit_type'] = visit_type;
    $.ajax({
        url: url,
        type: 'POST',
        data: filters_list,
        beforeSend: function () {
            $('#csvResultsBtn').attr('disabled', true);
            $('#csvResultsSpin').removeClass('fa fa-file-excel-o');
            $('#csvResultsSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                toastr.success("Successfully queued in generation");
            }
        },
        complete: function () {
            $('#csvResultsBtn').attr('disabled', false);
            $('#csvResultsSpin').removeClass('fa fa-spinner fa-spin');
            $('#csvResultsSpin').addClass('fa fa-file-excel-o');
        }
    });
}

function generatedExcelData(report_name, from_type) {
    var base_url = $('#base_url').val();
    var url = base_url + '/flexy_report/generatedReportExcelData';
    var param = {
        report_name: report_name
    }
    $.ajax({
        url: url,
        type: 'POST',
        data: param,
        beforeSend: function () {
            $('#generatedExcelDataModelDiv').html("Please Wait!!!!!!!!!");
            $('#generatedExcelDataBtn').attr('disabled', true);
            $('#generatedExcelDataSpin').removeClass('fa fa-list');
            $('#generatedExcelDataSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#generatedExcelDataModelDiv').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            if (parseInt(from_type) == 1) {
                $("#generatedExcelDataModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        },
        complete: function () {
            $('#generatedExcelDataBtn').attr('disabled', false);
            $('#generatedExcelDataSpin').removeClass('fa fa-spinner fa-spin');
            $('#generatedExcelDataSpin').addClass('fa fa-list');
        }
    });
}