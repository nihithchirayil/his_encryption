$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    //-----------Bill No search------------
    $('#item_desc').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#item_desc_hidden').val() != "") {
                $('#item_desc_hidden').val('');
                $("#item_desc_AjaxDiv").hide();
            }
            var item_desc = $(this).val();
            if (item_desc == "") {
                $("#item_desc_AjaxDiv").html("").hide();
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "item_desc=" + item_desc,
                    beforeSend: function () {
                        $("#item_desc_AjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#item_desc_AjaxDiv").html(html).show();
                        $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });
    $('#transaction_no').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#transaction_no_hidden').val() != "") {
                $('#transaction_no_hidden').val('');
                $("#transaction_no_AjaxDiv").hide();
            }
            var transaction_no = $(this).val();
            if (transaction_no == "") {
                $("#transaction_no_AjaxDiv").html("").hide();
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "transaction_no=" + transaction_no,
                    beforeSend: function () {
                        $("#transaction_no_AjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        // alert(html); return;
                        $("#transaction_no_AjaxDiv").html(html).show();
                        $("#transaction_no_AjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });
    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#patient_id_hidden').val() != "") {
                $('#patient_id_hidden').val('');
                $('#patient_uhid_hidden').val('');
                $("#patient_idAjaxDiv").html("").hide();
            }
            var patient_name_search = $(this).val();
            if (patient_name_search == "") {
                $("#patient_idAjaxDiv").html("").hide();
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "patient_name_search=" + patient_name_search,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        // alert(html); return;
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

function fillitem_desc(item_code, item_desc) {
    $('#item_desc_hidden').val(item_code);
    $('#item_desc').val(item_desc);
    $('#item_desc_AjaxDiv').hide();
}
function fillPatientValues(uhid, patient_name) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
}

/* For BillNo Search, filling values */
function filltransction_no(id, transaction_no) {
    $('#transaction_no_hidden').val(transaction_no);
    $('#transaction_no').val(transaction_no);
    $('#transaction_no_AjaxDiv').hide();
}

function getReportData() {
    var base_url = $('#base_url').val();
    var url = base_url + "/reports/getStockLedgerReport";
    var from_date = $("#fromdate").val();
    var to_date = $("#todate").val();
    var item_desc = $('#item_desc_hidden').val();
    var patient_uhid = $('#patient_uhid_hidden').val();
    var tranaction_id=$('#transaction_no_hidden').val();
    var location = $('#location').val();
    if (location) {
        var param = { from_date: from_date, to_date: to_date, item_desc: item_desc, patient_uhid: patient_uhid,tranaction_id:tranaction_id, location: location };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#search_results').attr('disabled', true);
                $('#serachResultSpin').removeClass('fa fa-search');
                $('#serachResultSpin').addClass('fa fa-spinner');
                $('#ResultDataContainer').css('display', 'block');
                $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (html) {
                $('#ResultsViewArea').html(html);
                $('#print_results').removeClass('disabled');
                $('#csv_results').removeClass('disabled');
            },
            complete: function () {
                $('#search_results').attr('disabled', false);
                $('#serachResultSpin').removeClass('fa fa-spinner');
                $('#serachResultSpin').addClass('fa fa-search');
                $('#ResultsViewArea').LoadingOverlay("hide");

            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }
        });
    }else{
        toastr.warning("Please Select Any Location");
    }
}

function search_clear() {

    var current_date = $('#current_date').val();

    $('#fromdate').val(current_date);
    $('#todate').val(current_date);
    $('#item_desc_hidden').val('');
    $('#item_desc').val('');
    $('#item_desc_AjaxDiv').hide();
    $('#patient_uhid_hidden').val('');
    $('#patient_name').val('');
    $('#patient_idAjaxDiv').hide();
    $('#transaction_no_hidden').val('');
    $('#transaction_no').val('');
    $('#transaction_no_AjaxDiv').hide();
    $('#location').val('');
    $('#location').select2();
}
