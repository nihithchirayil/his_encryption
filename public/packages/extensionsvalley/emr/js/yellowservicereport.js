$(document).ready(function() {
    setTimeout(function() {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.admithistoryreport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
//----------------------------------------------relative select box----------------------------------------------------------
$("#patient_nameAjaxDiv").on('click', 'li', function(){
    var ward = $('#patient_name_hidden').val();


    if (ward == '') {
      var ward = 'a';

    }


 if(ward) {


     var url = route_json.ajaxipvisits;

        $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    data: {'ward':ward},
                    beforeSend: function () {
                   // $('#ip_visits').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    $('#s2id_ip_visits').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                    $( "#ip_visits" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {
                       // alert(html);



                                if(html){

                                       $("#ip_visits").empty();

                                        $("#ip_visits").html('<option value="">Select visits</option>');
                                        $.each(html,function(key,value){
                                            ($("#ip_visits").append('<option value="'+value.id+'">From:'+value.admission_date+'    To:'+value.discharge_date+'</option>'));
                                        });

                                }else{
                                        $("#ip_visits").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["warning"]("Please  select a proper item !");
                        $( "#ip_visits" ).prop( "disabled", false );



                    },
                    complete: function () {
                       // $('#ip_visits').LoadingOverlay("hide");
                       $('#warning1').remove();
                       $( "#ip_visits" ).prop( "disabled", false );

                    }

                });

        }else {
                $('#patient_name').focus();
                $("#ip_visits").empty();
            }

    });
//---------------------------------------------------------------end--------------------------------------------------------------------------------------------------------------------


function getReportData() {
    var url = route_json.yellowservicereportresult;
    // var visit_id= $('#ip_visits').val();
    // alert(visit_id);
    // return

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}
function occupancy() {
    var url = route_json.currentoccupanciesreport;

    //-------filters---------------------------------------------

    var filters_list = new Object();

    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}

function search_clear(){


    $("#ip_visits").select2('val', '');
    $('#patient_name').val('');
    $('#patient_name_hidden').val('');


}
