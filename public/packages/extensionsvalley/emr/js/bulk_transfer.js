    var change_lo_location='';
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });

  
    function getFilteredItems(url){
         //alert('sads');
        // return;
        
        var data = {};
        console.log(data);
        data.from_location = $("#from_location").val();
     //   $("#from_location").select2();
        data.to_location = $("#to_location").val();
        data.item = $("#item").val();
      //  $("#to_location").select2();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                change_lo_location='';
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($("#from_location").val().trim() == ''){
            Command: toastr["error"]("Please enter From Location");
            return;
        }
        if($("#to_location").val().trim() == ''){
            Command: toastr["error"]("Please enter To Location");
            return;
        }
        data.bulk_id = $(".reference_id").val();
        data.from_location = $("#from_location").val();
       // alert(data.from_location );
        data.to_location = $("#to_location").val();
        data.item = $("#item").val();
       
       
        // if (data.status == true){
        //     $("#status_add").prop("checked"==1);
        // } else{
        //     $("#status_add").prop("checked"==0);
        // }
     
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".transfer").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.transfer').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Transfer Already Exit!");
                    $(".transfer").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.transfer').attr('disabled', false);
    
                    }else{
                    $(".transfer").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.transfer').attr('disabled', false);
                    Command: toastr["success"]("success");
                $("#from_location").val("").select2();
                $("#to_location").val("").select2();
                $("#item").val("").select2();
                getFilteredItems(window.search_url);
               // window.location.reload();
            }
        },
        complete: function () {

        }
    });
}
   
    function editItem(element){
        // $("#button").click(function(){
        //     if( status ==1){
        //     $(".status_add").prop("checked", true);
        //     }else{
        //         $(".status_add").prop("checked", false);

        //     }
        // });
      
        let bulk_id = $(element).parents('tr').attr('data-id');
        let from_location = $(element).parents('tr').attr('data-vul-name');
        let to_location = $(element).parents('tr').attr('data-bulk-name');
        let item = $(element).parents('tr').attr('data-item');
       
       
        $(".reference_id").val(bulk_id);
        $("#item").val(item);
        $("#item").select2().trigger('change');
        $("#from_location").val(from_location);
        $("#from_location").select2().trigger('change');
        $("#to_location").val(to_location);
        change_lo_location = to_location;
        $("#to_location").select2().trigger('change');
        $(".transfer").html('<i class="fa fa-save"></i> Transfer');

    }
    

    // function resetForm(){
    //     $(".reference_id").val('');
    //     $(".vulnerable_add").val('');
    //     $(".code_add").val('');
    //     $('input:checkbox').removeAttr('checked');
    //     $("#add").html('Add Vulnerable');
    //     $(".age_from_add").val('');
    //     $(".age_to_add").val('');
    //     $(".gender_add").val('');
    //     $(".saveButton").html('<i class="fa fa-save"></i> Save');
    // }

    function changeLocationCheck(e){
        var selected_loc = $(e).val();
        var base_url = $("#base_url").val();
        var url = base_url +"/purchase/getToLocation";
        var loc_searchstring = '';
        $.ajax({
                    type: "POST",
                    url: url,
                    data: 'selected_loc='+selected_loc+'&all_location_excl_tax=1',
                    beforeSend: function () {
                        $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                    },
                    success: function (data) {
                        var datas = JSON.parse(data);
                        loc_searchstring += "<option value=''>Select Location</option>";
                        $.each(datas, function (key, val) {
                            if( val.location_code == change_lo_location){
                                var sel = 'selected';
                            }else{
                                var sel = '';
                            }
                               
                                
                                loc_searchstring += "<option " + sel + " value='" + val.location_code + "'>" + val.location_name + "</option>";
                            });
                    },
                    complete: function (){
                        $.LoadingOverlay("hide");
                        var html_data = '<label for="">To Location</label><div class="clearfix"></div>'+
                        '<select class=""form-control to_location select2" id="to_location" name="to_location">' + loc_searchstring + '</select>';
                        $("#append_to_loc").html(html_data);
                        // $("#to_location").val(to_location);
                        $("#to_location").select2().trigger('change');
                    }
        
                });
            }