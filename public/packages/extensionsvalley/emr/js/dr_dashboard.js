//const { upperCase } = require("lodash");

$(document).ready(function () {
    window.current_occupancy = true;
    $(document).on('click', '.op_btn', function (event) {
        $('.op_slide_box').addClass('open')
    })

    $(document).on('click', '.slide_close_btn', function (event) {
        $('.op_slide_box').removeClass('open')
    })

    $(document).on('click', '.ip_btn', function (event) {
        $('.ip_slide_box').addClass('open')
    })

    $(document).on('click', '.slide_close_btn', function (event) {
        $('.ip_slide_box').removeClass('open')
    })

    $(document).on('click', '.smooth_scroll', function (event) {
        event.preventDefault()

        $('.theadscroll').animate(
            {
                scrollTop: $($.attr(this, 'href')).offset().top,
            },
            500,
        )
    })

    $('.modal').on('shown.bs.modal', function (e) {
        $('.theadfix_wrapper').floatThead('reflow')
    })

    $('.selectsearch').select2()

    $(document).on('click', '.combined_view_date_list ul li a', function () {
        $('.combined_view_date_list ul li a').removeClass('active')
        $(this).addClass('active')
    })

    $(document).on('click', '.combined_view_date_list ul li', function () {
        var disset = $(this).attr('id')
        $(this)
            .closest('.combined_view_wrapper')
            .find('tr#' + disset)
            .addClass('active')
    })

    $(document).on('click', '.filter_expand', function () {
        $(this).closest('#filter_area').toggleClass('col-md-8 col-md-12')
        $('.collapse_content').toggleClass('hidden')
    })
    $(document).on('click', '.expand_preview_btn', function () {
        $(this).addClass('collapse_preview_btn')
        $(this).removeClass('expand_preview_btn')
        $(this).toggleClass('fa-expand fa-minus')
        $('.preview_left_box').toggleClass('col-md-12 col-md-6')
        $('.preview_container_wrapper').addClass('expand')
        $('.preview_right_box').removeClass('hidden')

        setTimeout(function () {
            $('.theadfix_wrapper').floatThead('reflow')
        }, 400)
    })

    $(document).on('click', '.collapse_preview_btn', function () {
        $(this).removeClass('collapse_preview_btn')
        $(this).addClass('expand_preview_btn')
        $(this).toggleClass('fa-minus fa-expand')
        $('.preview_left_box').toggleClass('col-md-6 col-md-12')
        $('.preview_container_wrapper').removeClass('expand')
        $('.preview_right_box').addClass('hidden')

        setTimeout(function () {
            $('.theadfix_wrapper').floatThead('reflow')
        }, 400)
    })

    $(document).on('click', '.expand_prescription_btn', function () {
        $(this).addClass('collapse_prescription_btn')
        $(this).removeClass('expand_prescription_btn')
        $(this).toggleClass('fa-expand fa-minus')
        $('.prescription_wrapper').addClass('expand')

        setTimeout(function () {
            $('.theadfix_wrapper').floatThead('reflow')
        }, 400)
    })

    $(document).on('click', '.collapse_prescription_btn', function () {
        $(this).addClass('expand_prescription_btn')
        $(this).removeClass('collapse_prescription_btn')
        $(this).toggleClass('fa-minus fa-expand')
        $('.prescription_wrapper').removeClass('expand')

        setTimeout(function () {
            $('.theadfix_wrapper').floatThead('reflow')
        }, 400)
    })

    setTimeout(function () {
        // var not_seen_count = $('.totalNotSeenCount').val();
        // var seen_count = $('.totalSeenCount').val();
        // var total_count = not_seen_count+ seen_count;

        var normal_patients_count = $('.normalPatientsCount').val();
        var icu_patients_count = $('.icuPatientsCount').val();
        // // var total_ip_count = parseInt(normal_patients_count)+ parseInt(icu_patients_count);

        // $('.not_seen_count').html(not_seen_count);
        // // $('.seen_count').html(seen_count);
        // $('.total_booked_patients').html(total_count);

        $('.icu_patients_count').html(icu_patients_count);
        $('.normal_patients_count').html(normal_patients_count);
        // // $('#ip_patients_count').html(total_ip_count);

    }, 600)




    $(document).on('click', '.expand_rightbox_btn', function () {
        $(this).addClass('collapse_rightbox_btn')
        $(this).removeClass('expand_rightbox_btn')
        $(this).toggleClass('fa-expand fa-minus')
        $('.right_wrapper_box').addClass('expand')

        setTimeout(function () {
            $('.theadfix_wrapper').floatThead('reflow')
        }, 400)
    })

    $(document).on('click', '.collapse_rightbox_btn', function () {
        $(this).addClass('expand_rightbox_btn')
        $(this).removeClass('collapse_rightbox_btn')
        $(this).toggleClass('fa-minus fa-expand')
        $('.right_wrapper_box').removeClass('expand')

        setTimeout(function () {
            $('.theadfix_wrapper').floatThead('reflow')
        }, 400)
    })

    $('.select_button li').click(function () {
        $(this).toggleClass('active')
    })

    $(document).on('click', '.notes_sec_list ul li', function () {
        var disset = $(this).attr('id')
        $('.notes_sec_list ul li').removeClass('active')
        $(this).addClass('active')
        $(this).closest('.notes_box').find('.note_content').css('display', 'none')
        $(this)
            .closest('.notes_box')
            .find('div#' + disset)
            .css('display', 'block')
    })

    $('.month_picker').datetimepicker({
        format: 'MM',
    })
    $('.year_picker').datetimepicker({
        format: 'YYYY',
    })

    var $table = $('table.theadfix_wrapper')

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll')
        },
    })

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    })
    //$('.date_time_picker').datetimepicker()

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30,
    })

    //       $('.fixed_header').floatThead({
    //     position: 'absolute',
    //     scrollContainer: true
    // });

    if($("#group_doctor").length > 0){
        var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
        if(!isNaN(selected_dr_id) && selected_dr_id > 0){
            $("#group_doctor").val(selected_dr_id).trigger('change');
        } else {
            show_op_patients_list();
        }
    } else {
        show_op_patients_list();
    }

    getDoctorShiftDetails();

})

function getHealthPackagePatientList(){
    var file_token = $('#hidden_filetoken').val();
    var search_date = $("#op_patient_search_date_hp").val();
    var doctor_id = $("#group_doctor_hp").val();
    var param = { _token: file_token, search_date: search_date, doctor_id: doctor_id };
    var url = $('#base_url').val() + "/emr/getHealthPackagePatientList";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            //console.log(data);
            $(".health_package_list_table_container").html(data);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('.theadfix_wrapper').floatThead('reflow');
            $("body").LoadingOverlay("hide");
        }
    });
}

function search_patient(search_key) {
    var keytype = $('input[name="search_type"]:checked').val();
    search_key = search_key.toUpperCase();
    $('.patient_det_widget').css('display', 'none');
    if (keytype == 1) {
        $(".patient_det_widget[data-patientname^='" + search_key + "']").css('display', 'block');
    } else if (keytype == 2) {
        $(".patient_det_widget[data-uhid^='" + search_key + "']").css('display', 'block');
    } else {
        $(".patient_det_widget[data-phone^='" + search_key + "']").css('display', 'block');
    }
    if (search_key == '') {
        $('.patient_det_widget').css('display', 'block');
    }
}


function search_patient_hp() {
    var search_key = $('#patient_search_txt_hp').val();
    var keytype = $('input[name="search_type_hp"]:checked').val();
    search_key = search_key.toUpperCase();
    $('.patient_det_widget_hp').hide();
    if (keytype == 1) {
        $(".patient_det_widget_hp[data-patientname^='" + search_key + "']").show();
    } else if (keytype == 2) {
        $(".patient_det_widget_hp[data-uhid^='" + search_key + "']").show();
    } else {
        $(".patient_det_widget_hp[data-phone^='" + search_key + "']").show();
    }
    if (search_key == '') {
        $('.patient_det_widget_hp').show();
    }
}

function search_ip_patient(search_key) {
    var keytype = $('input[name="search_type"]:checked').val();
    search_key = search_key.toUpperCase();
    $('.ip_patient_det_widget').css('display', 'none');
    if (keytype == 1) {
        $(".ip_patient_det_widget[data-patientname^='" + search_key + "']").css('display', 'block');
    } else if (keytype == 2) {
        $(".ip_patient_det_widget[data-uhid^='" + search_key + "']").css('display', 'block');
    } else {
        $(".ip_patient_det_widget[data-phone^='" + search_key + "']").css('display', 'block');
    }
    if (search_key == '') {
        $('.ip_patient_det_widget').css('display', 'block');
    }
}

function goToSelectedPatient(patient_id) {
    // var enable_emr_lite_version = $("#enable_emr_lite_version").val();
    // if(parseInt(enable_emr_lite_version) > 0){
    //     window.location.href = $('#base_url').val() + "/emr/view-patient/" + patient_id;
    // } else{
        window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
    //}
}

function goToGynecologyPatient(patient_id) {
    window.location.href = $('#base_url').val() + "/gynecology/Gynecology/" + patient_id;
}

function goToInfertilityPatient(patient_id) {
    window.location.href = $('#base_url').val() + "/infertility/Infertility/" + patient_id;
}
function goToSelectedPatientCasuality(patient_id) {
    window.location.href = $('#base_url').val() + "/casuality/casuality/" + patient_id;
}

function show_op_patients_list() {
    var doctor_id = $('#doctor_id').val();
    var selected_doctor_id = $("#group_doctor").val() ? $("#group_doctor").val() : '';
    var op_patient_search_date = $('#op_patient_search_date').val();
    var my_own_visit = $(".my_own_visit").prop('checked') ? 1 : 0;
    var url = $('#base_url').val() + "/emr/show-op-patient-list-dashboard";
    var sort_by = $("#sort_by").val();;
    $.ajax({
        type: "GET",
        url: url,
        data: 'doctor_id=' + doctor_id + '&op_patient_search_date=' + op_patient_search_date + '&my_own_visit=' + my_own_visit + '&selected_doctor_id=' + selected_doctor_id + '&sort_by=' + sort_by,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            //console.log(data);
            $("#patient_search_txt").val('');
            if (data.status == 0) {
                Command: toastr["warning"]("No patients found!");
                $('#show_op_patient_list_html').html('');
            } else {
                $('#show_op_patient_list_html').html(data.html);
                $('#ip_patients_count').html(data.ip_patients_count);
                $('#discharge_intimation_count').html(data.discharge_intimation_count);
                $('#total_booked_patients').html(data.total_booked_patients);
                $('#patient_statusspan').html(data.patient_status);
                $('.seen_count').html(data.seen_patients);

                if($('.check_in_box').is(':checked')){
                    $(".call_token_btn").show();
                } else {
                    $(".call_token_btn").hide();
                }

                checkEmergencyStatus();


            }
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('.theadfix_wrapper').floatThead('reflow');
            $("body").LoadingOverlay("hide");
            var not_seen_count = $('.totalNotSeenCount').val();
            var seen_count = $('.totalSeenCount').val();
            var total_count = not_seen_count + seen_count;
            $('.not_seen_count').html(not_seen_count);
            $('.seen_count').html(seen_count);
            $('.total_booked_patients').html(total_count);
        }
    });
}

function checkEmergencyStatus() {
    var doctor_emergency_status = localStorage.getItem('doctor_emergency_status') ? localStorage.getItem('doctor_emergency_status') : 0;
    if (doctor_emergency_status && doctor_emergency_status == "TRUE") {
        $(".call_token_btn").attr("disabled", true);
    } else {
        $(".call_token_btn").attr("disabled", false);
    }
}

function apply_filter(classname) {
    $('.patient_det_widget').css('display', 'none');
    $("." + classname).css('display', 'block');
    if (classname == 'all') {
        $('.patient_det_widget').css('display', 'block');
    }
}

var search_timeout = null;
function patient_general_search(txt) {
    var last_search_key = '';
    var request_flag = '';
    if (txt.length >= 2 && $.trim(txt) != '') {

        clearTimeout(search_timeout);
        search_timeout = setTimeout(function () {

            if (last_search_key == txt || request_flag == 1) {
                return false;
            }
            last_search_key = txt;
            var url = $('#base_url').val() + "/emr/generalPatientSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'txt=' + txt,
                beforeSend: function () {
                    $("#completepatientbox").empty();
                    $("#completepatientbox").show();
                    $(".input_rel .input_spinner").removeClass('hide');
                    $("#completepatientbox").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    request_flag = 1
                },
                success: function (data) {

                    $("#completepatientbox").LoadingOverlay("hide");
                    $("#completepatientbox").html(data);
                    $(".input_rel .input_spinner").addClass('hide');
                    request_flag = 0;
                }
            });
        });
    } else if (txt.length === 0) {
        $("#completepatientbox").hide();
        $("#completepatientbox").LoadingOverlay("hide");
    }
}

function myCalendar() {
    $('#calendarModal').modal('toggle');
    var doctor_id = $('#calendar_doctor_name_hidden').val();
    if (doctor_id != '') {
        //alert('ddd');
        myCalendarData();
    }
}

$('.showDischargeSummary').click(function () {
    window.patient_for_discharge_summary = 0;
    showDischargeSummary();
});
function myCalendarData() {
    var doctor_id = $('#calendar_doctor_name_hidden').val();
    var calendar_search_date = $('#calendar_search_date').val();
    var url = $('#base_url').val() + "/emr/getMyCalendarData";
    $.ajax({
        type: "GET",
        url: url,
        data: 'doctor_id=' + doctor_id + '&calendar_search_date=' + calendar_search_date,
        beforeSend: function () {
            $("#calendarModal").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            //console.log(data);
            $("#calendarModal").LoadingOverlay("hide");
            if (data != 0) {
                $("#calendarData").html(data);
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function (data) {
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            })
        }
    });
}

function create_appointment(slot_time) {
    $('#create_appointment_modal').modal('toggle');
    $('#slot_time').val(slot_time);
    myCalendarData();
}

/*Patient Name Search*/
$('#patient_list_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var search_filter = $(this).val();
        search_filter = search_filter.trim();
        if (search_filter == "") {
            $("#patient_searchCodeAjaxDiv").html("");
            $("#patient_search_hidden").val("");
        } else {
            try {
                var file_token = $('#hidden_filetoken').val();
                var param = { _token: file_token, search_filter: search_filter, patient_name_op: 1 };
                $.ajax({
                    type: "POST",
                    url: $('#base_url').val() + "/emr/getMyCalendarData",
                    data: param,
                    beforeSend: function () {
                        $("#patient_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        // console.log(html);
                        $("#patient_searchCodeAjaxDiv").html(html).show();
                        $("#patient_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        //  $('#loading_image').hide();
                    }
                });
            }
            catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('patient_searchCodeAjaxDiv', event);
    }
});
$('#patient_list_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('patient_searchCodeAjaxDiv');
        return false;
    }
});
function fillOpDetials(list, op_id, id, patient_name, gender, mobile_no, address, age) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#patient_search_hidden').val(op_id);
    $('#patient_address').val(address);
    $('#patient_phone').val(mobile_no);
    $('#patient_gender').val(gender);
    $('#patient_age').val(age);
    $('#' + itemCodeListDivId).hide();
    $('#patient_list_search').val(patient_name + '[' + op_id + ']');

    $('#patient_address').attr('disabled', 'disabled');
    $('#patient_phone').attr('disabled', 'disabled');
    $('#patient_gender').attr('disabled', 'disabled');
    $('#patient_age').attr('disabled', 'disabled');
}

function setAsNewPatient() {
    $('#patient_searchCodeAjaxDiv').hide();
    $('#patient_address').removeAttr('disabled');
    $('#patient_phone').removeAttr('disabled');
    $('#patient_gender').removeAttr('disabled');
    $('#patient_age').removeAttr('disabled');
}

function create_new_appointment() {
    var patient_name = $('#patient_list_search').val();
    var patient_id = $('#patient_search_hidden').val();
    var age = $('#patient_age').val();
    var gender = $('#patient_gender').val();
    var phone = $('#patient_phone').val();
    var slot_time = $('#slot_time').val();
    var address = $('#patient_address').val();
    var doctor_id = $('#calendar_doctor_name_hidden').val();
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/emr/create_new_appointment",
        data: 'patient_name=' + patient_name + '&patient_id=' + patient_id + '&age=' + age + '&gender=' + gender + '&phone=' + phone + '&slot_time=' + slot_time + '&address=' + address + '&doctor_id=' + doctor_id,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            myCalendarData();
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('#create_appointment_modal').modal('hide');
        }
    });

}


//----doctor name search--------------------------------------------------------------------
$('#calendar_doctor_name').keyup(function (event) {

    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#calendar_doctor_name_hidden').val() != "") {
            $('#calendar_doctor_name_hidden').val('');
        }
        var doctor_name = $(this).val();
        doctor_name = doctor_name.trim();
        if (doctor_name == "") {
            $("#calendar_doctorAjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + "/emr/getMyCalendarData";
            $.ajax({
                type: "GET",
                url: url,
                data: "calendar_doctor_name=" + doctor_name,
                beforeSend: function () {
                    $("#calendar_doctorAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#calendar_doctorAjaxDiv").html(html).show();
                    $("#calendar_doctorAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                }
            });
        }

    } else {
        ajax_list_key_down('calendar_doctorAjaxDiv', event);
    }
});
function fillDoctorDetials(id, doctor_name) {
    $('#calendar_doctor_name').val(doctor_name);
    $('#calendar_doctor_name_hidden').val(id);
    $('#calendar_doctorAjaxDiv').hide();
    myCalendarData();
}

/* setting for enter key press in ajaxDiv listing */
$("#doctor_name_hidden").on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('calendar_doctorAjaxDiv');
        return false;
    }
});


function saveMedicationAllergy() {

    var values_allergy = $("input[name='alergylist_medicine_code_hidden[]']")
        .map(function () {
            var tt = $(this).val();
            if (tt != '') {
                return $(this).val();
            }
        }).get();
    var allergy_ct = values_allergy.length;
    if (allergy_ct > 0) {

        var url = $('#base_url').val() + "/emr/saveallergyprescription";
        var patientId = $('#patient_id').val();

        var token = $('#formallergyprescription').find('input[name="_token"]').val();
        var dataparams = $('#formallergyprescription').serialize();
        // alert(dataparams);return;
        $.ajax({
            url: url,
            data: '_token=' + token + '&MedicineData=' + dataparams + '&patientId=' + patientId,
            async: true,
            type: 'post',
            //dataType: "json",
            success: function (html) {
                if (html == 1) {
                    Command: toastr["success"]("Saved Successfully");
                    // loadPrescription ();
                    fetch_all_allergy(patientId);
                    fetchpatientMedicineAllerg();

                }
                else {
                    Command: toastr["error"]("Something went wrong");
                }
            }
        });
    } else {
        Command: toastr["error"]("Fill Medicine For Allergies ");

    }
}

function filter_nursing_station() {
    if (window.current_occupancy) {
        get_current_occupancy();
    } else {
        get_ip_list_by_daterange();
    }
}

function get_current_occupancy() {
    window.current_occupancy = true;
    var doctor_id = $('#doctor_id').val();
    var nursing_location = $('#nursing_station_select').val();
    var url = $('#base_url').val() + "/emr/getCurrentOcuupancy";
    $.ajax({
        url: url,
        data: 'doctor_id=' + doctor_id + "&nursing_location=" + nursing_location,
        async: true,
        type: 'get',
        success: function (html) {
            if (html != 0) {
                $('#ip_patients_list_data').html(html);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            } else {
                Command: toastr["error"]("Something went wrong");
            }

            var normal_patients_count = $('.normalPatientsCount').val();
            var icu_patients_count = $('.icuPatientsCount').val();
            var total_ip_count = parseInt(normal_patients_count) + parseInt(icu_patients_count);

            $('.icu_patients_count').html(icu_patients_count);
            $('.normal_patients_count').html(normal_patients_count);
            $('#ip_patients_count').html(total_ip_count);

        }
    });
}

function get_ip_list_by_daterange() {
    window.current_occupancy = false;
    var doctor_id = $('#doctor_id').val();
    var from_date = $('#ip_patient_search_from_date').val();
    var to_date = $('#ip_patient_search_to_date').val();
    var nursing_location = $('#nursing_station_select').val();
    if(from_date != to_date){
        if(moment(moment(to_date).format('YYYY-MM-DD')).isAfter(moment(from_date).format('YYYY-MM-DD'))==false){
            Command: toastr["error"]("From Date Should be smaller than To Date");
            return;
        }
    }
    var url = $('#base_url').val() + "/emr/getIpListByDateRange";
    $.ajax({
        url: url,
        data: 'doctor_id=' + doctor_id + "&from_date=" + from_date + "&to_date=" + to_date + "&nursing_location=" + nursing_location,
        async: true,
        type: 'get',
        success: function (html) {
            if (html != 0) {
                $('#ip_patients_list_data').html(html);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }
            else {
                Command: toastr["error"]("Something went wrong");
            }
        }
    });
}

function showDischargeSummary(page = 1) {
    var doctor_id = $('#doctor_id').val();
    var url = $('#base_url').val() + "/emr/showDischargeSummaryList";
    $.ajax({
        url: url,
        type: "GET",
        data: "doctor_id=" + doctor_id + "&patient_id=" + window.patient_for_discharge_summary + "&page=" + page,
        beforeSend: function () {
            $('#summary_list_modal').modal('show');
            $('#summary_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_list_data').LoadingOverlay("hide");
            $('#summary_list_data').html(data);
        },
        complete: function () {

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
}


$('#summary_list_data').on('click', '.pagination a', function (e) {
    e.preventDefault();
    showDischargeSummary($(this).attr('href').split('page=')[1]);
});

function showSummary(summary_id) {
    var url = $('#base_url').val() + "/emr/showSummary";

    $.ajax({
        url: url,
        type: "GET",
        data: "summary_id=" + summary_id,
        beforeSend: function () {
            $('#summary_list_modal').modal('hide');
            $('#summary_view_modal').modal('show');
            $('#summary_view_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            $('#summary_view_data').html('<div class="" style="overflow:scroll; height:400px;">' + data.summary + '</div>');

            if (data.final_status == 0) {
                $('#summary_view_data').append('<button style="margin-top: 20px;" title="Mark as Finalized" onclick="finalizeDischargeSummary(' + summary_id + ');" class="btn btn-success"><i class="fa fa-check"></i>Finalize Summary</button><button style="margin-top: 20px;" title="Edit Summary" onclick="editDischargeSummary(' + data.visit_id + ');" class="btn btn-success"><i class="fa fa-edit"></i> Edit Summary</button>');
            }
        },
        complete: function () {

        }
    });
}

function finalizeDischargeSummary(summary_id) {
    var url = $('#base_url').val() + "/emr/finalizeDischargeSummary";
    $.ajax({
        url: url,
        type: "GET",
        data: "summary_id=" + summary_id,
        beforeSend: function () {
            $('#summary_view_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            if ($('#summary_view_modal').hasClass('in')) {
                $('#summary_view_modal').modal('hide');
            } else {
                showDischargeSummary();
            }
            if (data.status == 1) {
                toastr.success("Discharge Summary Finalized..!");
            }

        },
        complete: function () {

        }
    });
}


//--------------patient name search -------------------------

$(document).on('keyup', '.global_op_no', function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        var op_no = $(this).val();
        op_no = op_no.replace("/[^\w\s-_\.]/gi");
        op_no = op_no.trim();
        if (op_no == "") {
            $("#global_search_dis").find("#OpAjaxDiv_dis").html("");
        } else {
            var url = $('#base_url').val() + "/emr/globalPatientSearchForDischargeSummary";
            $.ajax({
                type: "GET",
                url: url,
                data: "op_no=" + op_no,
                beforeSend: function () {

                    $("#global_search_dis").find("#OpAjaxDiv_dis").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    console.log(html);
                    $("#global_search_dis").find("#OpAjaxDiv_dis").html(html).show();
                    // $("#global_search_dis").find("#OpAjaxDiv_dis").find('li').first().addClass('liHover');
                },
                complete: function () {
                    //  $('#loading_image').hide();
                }
            });
        }

    } else {
        ajax_list_key_down('OpAjaxDiv_dis', event);
    }
});

/* setting for enter key press in ajaxDiv listing */
$(document).on('keydown', ".global_op_no", function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('OpAjaxDiv_dis');
        return false;
    }
});

function ajaxlistenter(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}

//--------------patient name search ends--------------------------

function selectPatientForDischargeSummaryList(patient_id) {
    window.patient_for_discharge_summary = patient_id;
    showDischargeSummary();

}

$(document).on("click", '.resetGlobalPatientSearch', function (e) {
    window.patient_for_discharge_summary = 0;
    showDischargeSummary();
});


function editDischargeSummary(visit_id) {
    window.location = $('#base_url').val() + '/summary/dischargesummary/' + visit_id;
}

//function mark_patient_discharge(visit_id){

$(".mark_discharge").click(function (event) {

    event.stopPropagation();
    var visit_id = this.id;
    visit_id = visit_id.replace('mark_discharge_', "");
    var url = $('#base_url').val() + "/emr/markPatientDischarge";
    bootbox.confirm({
        message: "Are you sure you want to Mark discharge this patient?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "visit_id=" + visit_id,
                    beforeSend: function () {

                    },
                    success: function (html) {
                        if (html == 1) {
                            $('#mark_discharge_' + visit_id).remove();
                        }
                    },
                    complete: function () {

                    }
                });
            }
        }
    });

});






function getAppointments() {
    var doctor_id = $('#doctor_id').val();

    var url = $('#base_url').val() + "/emr/appointment-details";
    $.ajax({
        url: url,
        type: "GET",
        data: "doctor_id=" + doctor_id,
        beforeSend: function () {
            $('#showAppointmentsbtn').attr('disabled', true);
            $('#appointments_modal').modal('show');
            $('#appointments_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (res) {
            $('#appointments_list_data').LoadingOverlay("hide");
            $('#appointments_list_data').html(res);
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            })
            var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
            if(!isNaN(selected_dr_id) && selected_dr_id > 0){
                $("#group_doctor_app_list").val(selected_dr_id).trigger('change');
            } else {
                show_appoint_list();
            }

        },
        complete: function () {
            $('#showAppointmentsbtn').attr('disabled', false);
        }
    });
}


function show_appoint_list() {
    var doctor_id = $('#group_doctor_app_list').val() ? $('#group_doctor_app_list').val() : $('#doctor_id').val();
    var pat_search_date = $('#pat_search_date').val();
    var url = $('#base_url').val() + "/emr/fetch-appointment-details";
    $.ajax({
        url: url,
        type: "GET",
        data: "doctor_id=" + doctor_id + "&pat_search_date=" + pat_search_date,
        beforeSend: function () {
            $('.appointment_list_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (res) {
            $('.appointment_list_container').LoadingOverlay("hide");
            $('.appointment_list_container').html(res);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            var bill_count=$('#booking_listdatacount').val();
            $('#booking_listcount').html('Count :'+bill_count);
        }
    });
}


$('.showLabResults').click(function () {
    var doctor_id = $('#doctor_id').val();

    var url = $('#base_url').val() + "/emr/lab-result-details";
    $.ajax({
        url: url,
        type: "GET",
        data: "doctor_id=" + doctor_id,
        beforeSend: function () {
            $('#lab_result_modal').modal('show');
            $('#lab_result_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (res) {
            $('#lab_result_data').LoadingOverlay("hide");
            $('#lab_result_data').html(res);
        },
        complete: function () {

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
});


function show_lab_result_list() {
    var doctor_id = $('#doctor_id').val();
    var pat_search_date = $('#pat_search_date').val();
    var url = $('#base_url').val() + "/emr/lab-result-details";
    $.ajax({
        url: url,
        type: "GET",
        data: "doctor_id=" + doctor_id + "&pat_search_date=" + pat_search_date,
        beforeSend: function () {
            $('#lab_result_modal').modal('show');
            $('#lab_result_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (res) {
            $('#lab_result_data').LoadingOverlay("hide");
            $('#lab_result_data').html(res);
        },
        complete: function () {
            $('input[name="pat_search_date"]').val(pat_search_date);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
}


function callTokenByBookingId(booking_id){
    var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
      if(booking_id) {

        var shift_id = $('.selectShift').val();

          var url = $('#base_doumenturl').val() + "/emr/callTokenByBookingId";
          $.ajax({
              url: url,
              type: "POST",
              data: {
                  current_booking_id: booking_id,
                  selected_dr_id:selected_dr_id,
                  shift_id:shift_id
              },
              beforeSend:function (){
                    $(".current_token").html("<i class='fa fa-spinner fa-spin'></i>");
              },
              success: function(data) {
                  if(data){
                      if(data.status == 1){
                          localStorage.setItem('current_booking_id', data.booking_id);
                          $(".current_token").html(data.token);
                          $(".current_token").addClass("blink_me");
                          setTimeout(() => {
                            $(".current_token").removeClass("blink_me");
                          }, (2000));
                      } else {
                          $(".current_token").html(0);
                          toastr.success("No appointments found.");
                      }
                  }
              },
              error: function(){
                  toastr.success("Call Failed");
              }
          });
      }
}

function getDoctorShiftDetails(){
    var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
    if(!selected_dr_id) {
        selected_dr_id = $('#doctor_id').val();
    }
        var url = $('#base_url').val() + "/emr/getDoctorShiftDetails";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                doctor_id:selected_dr_id
            },
            beforeSend:function (){

            },
            success: function(data) {
                if(data){
                    $(".selectShift").empty();
                    if(data.status == 1){
                        $.each(data.shift_data, function(key, value) {
                            var option = "<option value='"+value.id+"'> "+value.shift+" </option>";
                            $(".selectShift").append(option);
                        });

                        var shift_id = localStorage.getItem('shift_id') ? parseInt(localStorage.getItem('shift_id')) : 0;
                        if(shift_id > 0){
                            $(".selectShift").val(shift_id);
                        }
                    }
                }
            },
            error: function(){

            }
        });
}
function getRadiologyPatientList(){
    var file_token = $('#hidden_filetoken').val();
    var search_date = $("#op_patient_search_date_radio").val();
    var param = { _token: file_token, search_date: search_date };
    var url = $('#base_url').val() + "/emr/getRadiologyPatientList";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if(data==0){
                Command: toastr["warning"]("No patients found!");
                 $(".radiology_patient_list_table_container").html('');
            }else{
            $(".radiology_patient_list_table_container").html(data);
        }
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('.theadfix_wrapper').floatThead('reflow');
            $("body").LoadingOverlay("hide");
        }
    });
}
function search_patient_radio() {
    var search_key = $('#patient_search_txt_radio').val();
    var keytype = $('input[name="search_type_radio"]:checked').val();
    search_key = search_key.toUpperCase();
    $('.patient_det_widget_radio').hide();
    if (keytype == 1) {
        $(".patient_det_widget_radio[data-patientname^='" + search_key + "']").show();
    } else if (keytype == 2) {
        $(".patient_det_widget_radio[data-uhid^='" + search_key + "']").show();
    } else {
        $(".patient_det_widget_radio[data-phone^='" + search_key + "']").show();
    }
    if (search_key == '') {
        $('.patient_det_widget_radio').show();
    }
}
