$(document).ready(function() {
    $(".select2").select2();
    getAllPackages();
});

var editable_array = [];
function getAllPackages() {
    var token = $("#c_token").val();
    var url = $("#base_url").val() + "/emr/getAllPackages";
    var param = {
        _token: token
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchDataDiv_body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(html) {
            $("#searchDataDiv").html(html);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function() {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function() {
            $("#searchDataDiv_body").LoadingOverlay("hide");
        }
    });
}

function searchDivUser() {
    $("#doc_active").prop("checked", false);
    $("#doc_all").prop("checked", true);
    input = document.getElementById("Search_usr");
    filter = input.value.toUpperCase();
    var length = document.getElementsByClassName("search_user").length;

    for (i = 0; i < length; i++) {
        if (
            document
                .getElementsByClassName("search_user")
                [i].innerHTML.toUpperCase()
                .indexOf(filter) > -1
        ) {
            document.getElementsByClassName("search_user")[i].style.display =
                "block";
        } else {
            document.getElementsByClassName("search_user")[i].style.display =
                "none";
        }
    }
}

function resetSettingDiv() {
    $("#selected_package_name").val("");
    $("#selected_service_item").val("");
    $("#ttl_package_amount").val("");
    $("#selected_package_cood").val("");
    $("#add_service_to_list").html("");
    $(".addSerRow").val("");
    $("#hidden_id").val("");
}

function pacDetailsEdit(pac_id) {
    var token = $("#c_token").val();
    var url = $("#base_url").val() + "/emr/pacDetailsEdit";
    var param = {
        _token: token,
        pac_id: pac_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#usrDetailsEdit" + pac_id).attr("disabled", true);
            $("#usrDetailsSpin" + pac_id).removeClass("fa fa-edit");
            $("#usrDetailsSpin" + pac_id).addClass("fa fa-spinner fa-spin");
            $('#add_service_to_list').html('');
            $("#selected_service_item").val('');
            $("#selected_package_cood").val('');
        },
        success: function(html) {
            $(".search_user").removeClass("tr_active");
            $("#hidden_id").val(pac_id);
            $("#doctorTr" + pac_id).addClass("tr_active");

            obj = JSON.parse(html);
            if (obj.details.length > 0) {
                var package_name = obj.details[0].package_name;
                var coordinator = obj.details[0].coordinator;
                $("#selected_service_item").val(package_name);
                $("#selected_package_cood").val(coordinator);
                if (obj.details.length > 0) {
                    obj.details.forEach(function(res) {
                        var service_desc = res.service_desc;
                        var service_id = res.service_id;
                        var service_code = res.service_code;
                        var service_package_price = res.package_amount;
                        var service_amount = res.price;
                        var active = res.status;
                        var discount = res.discount_value;
                        var dept_name = res.dept_name;
                        var department_id = res.department_id;
                        var pack_qty = res.quantity;
                        var add_tr =
                            "<tr class='pack_" +
                            service_code +
                            "'><td class='common_td_rules service_desc'>" +
                            service_desc +
                            "</td><td style='display:none;' class='active_ser'>" +
                            active +
                            "</td><td style='display:none;' class='pack_qty'>" +
                            pack_qty +
                            "</td><td style='display:none;' class='service_id'>" +
                            service_id +
                            "</td><td style='display:none;'class='service_code'>" +
                            service_code +
                            "</td><td></td><td class='td_common_numeric_rules sumOfPackageAmount'>" +
                            service_package_price +
                            "</td><td  class='td_common_numeric_rules service_amount'>" +
                            service_amount +
                            "</td><td class='td_common_numeric_rules discount'>" +
                            discount +
                            "</td><td class='common_td_rules dept_name'>" +
                            dept_name +
                            "</td><td style='display:none;' class='department_id'>" +
                            department_id +
                            "</td><td onclick='' class='deleteThisRow'><i class='fa fa-trash'></i></td>";
                        $("#add_service_to_list").prepend(add_tr);
                        sumOfPackageAmount();
                        $(".theadscroll").perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                        setTimeout(function() {
                            $(".theadfix_wrapper").floatThead({
                                position: "absolute",
                                scrollContainer: true
                            });
                        }, 400);
                        setTimeout(function() {
                            $(".theadscroll").perfectScrollbar("update");
                            $(".theadfix_wrapper").floatThead("reflow");
                        }, 250);
                    });
                }
            }
          
                
        },
        complete: function() {
            $("#usrDetailsEdit" + pac_id).attr("disabled", false);
            $("#usrDetailsSpin" + pac_id).removeClass("fa fa-spinner fa-spin");
            $("#usrDetailsSpin" + pac_id).addClass("fa fa-edit");
        }
    });
}

function pacDetailsDelete(pac_id) {
    bootbox.confirm({
        message: "Are you sure, you want to Delete ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success",
                default: "true"
            },
            cancel: {
                label: "No",
                className: "btn-warning"
            }
        },
        callback: function(result) {
            if (result) {
                var token = $("#c_token").val();
                var url = $("#base_url").val() + "/emr/pacDetailsDelete";
                var param = {
                    _token: token,
                    pac_id: pac_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#usrDetailsDelete" + pac_id).attr("disabled", true);
                        $("#usrDetailsDelSpin" + pac_id).removeClass(
                            "fa fa-trash"
                        );
                        $("#usrDetailsDelSpin" + pac_id).addClass(
                            "fa fa-spinner fa-spin"
                        );
                    },
                    success: function(html) {
                        if (html) {
                            toastr.success("Deleted.");
                            resetSettingDiv();
                            getAllPackages();
                        }
                    },
                    complete: function() {
                        $("#usrDetailsDelete" + pac_id).attr("disabled", false);
                        $("#usrDetailsDelSpin" + pac_id).removeClass(
                            "fa fa-spinner fa-spin"
                        );
                        $("#usrDetailsDelSpin" + pac_id).addClass(
                            "fa fa-trash"
                        );
                    }
                });
            }
        }
    });
}

$("#service_item").keyup(function(event) {
    var token = $("#c_token").val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter("service_itemAjaxDiv");
        return false;
    } else if (
        value.match(keycheck) ||
        event.keyCode == 8 ||
        event.keyCode == 46
    ) {
        var search_key = $("#service_item").val();
        if (search_key == "") {
            $("#service_itemAjaxDiv").hide();
            $("#service_item_hidden").val("");
        } else {
            var url = $("#base_url").val() + "/emr/ajaxSearch";
            var param = {
                _token: token,
                search_key: search_key,
                search_key_id: "service_item"
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#service_itemAjaxDiv")
                        .html(
                            '<li style="width:100%;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        )
                        .show();
                },
                success: function(html) {
                    $("#service_itemAjaxDiv")
                        .html(html)
                        .show();
                    $("#service_itemAjaxDiv")
                        .find("li")
                        .first()
                        .addClass("liHover");
                },
                complete: function() {},
                error: function() {
                    toastr.error(
                        "Please check your internet connection and try again"
                    );
                }
            });
        }
    } else {
        ajax_list_key_down("service_itemAjaxDiv", event);
    }
});
function fillSearchDetials(
    id,
    name,
    serach_key_id,
    service_code,
    department_id,
    dept_name,
    price
) {
    $("#" + serach_key_id + "_hidden").val(id);
    $("#service_code_hidden").val(service_code);
    $("#service_department_id_hidden").val(department_id);
    $("#service_dept_name_hidden").val(dept_name);
    $("#" + serach_key_id).val(htmlDecode(name));
    $("#" + serach_key_id).attr("title", htmlDecode(name));
    $(".ajaxSearchBox").hide();
    $("#service_price").val(parseFloat(price));
    $("#service_quantity").val(1);
    $("#service_amount").val(parseFloat(price));
    $("#service_package_price").val(parseFloat(price));
}
function htmlDecode(input) {
    var e = document.createElement("textarea");
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
$(document).on("keyup", "#service_quantity", function() {
    var qty = $("#service_quantity")
        .val()
        .trim();
    var price = $("#service_price")
        .val()
        .trim();
    if (qty != "" && qty != 0) {
        $("#service_amount").val(parseFloat(qty * price));
        $("#service_package_price").val(parseFloat(qty * price));
    } else {
        $("#service_amount").val(parseFloat(0.0));
        $("#service_package_price").val(parseFloat(0.0));
        $("#service_quantity").focus();

        toastr.warning("Please select quantity.");
    }
});
$(".addServiceToPackage").click(function() {
    $("#service_itemAjaxDiv").hide();
    var pack_qty = $("#service_quantity")
        .val()
        .trim();
    var service_code = $("#service_code_hidden")
        .val()
        .trim();
    if ($("#add_service_to_list tr").hasClass("pack_" + service_code)) {
        toastr.warning("Service already exist.");
    } else {
        var discount = 0.0;
        if (pack_qty != "" && pack_qty != 0) {
            var service_desc = $("#service_item")
                .val()
                .trim();
            var service_id = $("#service_item_hidden")
                .val()
                .trim();
            var service_code = $("#service_code_hidden")
                .val()
                .trim();
            var department_id = $("#service_department_id_hidden")
                .val()
                .trim();
            var dept_name = $("#service_dept_name_hidden")
                .val()
                .trim();
            var price = $("#service_price")
                .val()
                .trim();
            var active = $("#active_service").is(":checked") ? 1 : 0;
            var service_package_price = $("#service_package_price")
                .val()
                .trim();
            var service_amount = $("#service_amount")
                .val()
                .trim();
            if (
                parseFloat(service_package_price) < parseFloat(service_amount)
            ) {
                // discount=parseFloat(Math.round(((service_amount-service_package_price)/service_amount)*100));
                discount =
                    parseFloat(service_amount) -
                    parseFloat(service_package_price);
            }
            var add_tr =
                "<tr class='pack_" +
                service_code +
                "'><td class='common_td_rules service_desc'>" +
                service_desc +
                "</td><td style='display:none;' class='active_ser'>" +
                active +
                "</td><td style='display:none;' class='pack_qty'>" +
                pack_qty +
                "</td><td style='display:none;' class='service_id'>" +
                service_id +
                "</td><td style='display:none;'class='service_code'>" +
                service_code +
                "</td><td></td><td class='td_common_numeric_rules sumOfPackageAmount'>" +
                service_package_price +
                "</td><td  class='td_common_numeric_rules service_amount'>" +
                service_amount +
                "</td><td class='td_common_numeric_rules discount'>" +
                discount +
                "</td><td class='common_td_rules dept_name'>" +
                dept_name +
                "</td><td style='display:none;' class='department_id'>" +
                department_id +
                "</td><td onclick='' class='deleteThisRow'><i class='fa fa-trash'></i></td>";
            $("#add_service_to_list").prepend(add_tr);
            $(".addSerRow").val("");
            sumOfPackageAmount();
            var $table = $("table.theadfix_wrapper");
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest(".theadscroll");
                }
            });
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            $(".fixed_header").floatThead({
                position: "absolute",
                scrollContainer: true
            });
            setTimeout(function() {
                $(".theadscroll").perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead("reflow");
            }, 250);
        } else {
            toastr.warning("Please select quantity.");
        }
    }
});
$(document).on("click", ".deleteThisRow", function() {
    $(this)
        .parent("tr")
        .remove();
    sumOfPackageAmount();
});
function sumOfPackageAmount() {
    var total = 0;
    $(".sumOfPackageAmount").each(function() {
        total =
            total +
            parseFloat(
                $(this)
                    .html()
                    .trim()
            );
    });
    $("#ttl_package_amount").val(total);
}

$("#saveUserMappedDetails").click(function() {
    var details_array = [];
    var grand_amount = 0;
    var discount_value = 0;
    var package_name = $("#selected_service_item")
        .val()
        .trim();
    var hidden_id = $("#hidden_id").val();
    var selected_package_cood = $("#selected_package_cood").val();
    $(".service_amount").each(function() {
        grand_amount =
            grand_amount +
            parseFloat(
                $(this)
                    .html()
                    .trim()
            );
    });
    $(".discount").each(function() {
        return (discount_value =
            discount_value +
            parseFloat(
                $(this)
                    .html()
                    .trim()
            ));
    });
    var package_amt = $("#ttl_package_amount")
        .val()
        .trim();
    if (package_name) {
        $("#add_service_to_list tr").each(function() {
            details_array.push({
                pack_qty: $(this)
                    .find(".pack_qty")
                    .html()
                    .trim(),
                service_id: $(this)
                    .find(".service_id")
                    .html()
                    .trim(),
                status: $(this)
                    .find(".active_ser")
                    .html()
                    .trim(),
                price: $(this)
                    .find(".service_amount")
                    .html()
                    .trim(),
                package_amount: $(this)
                    .find(".sumOfPackageAmount")
                    .html()
                    .trim(),
                discount_value: $(this)
                    .find(".discount")
                    .html()
                    .trim(),
                speciality_id: 0
            });
        });

        if (details_array.length != 0) {
            var token = $("#c_token").val();
            var url = $("#base_url").val() + "/emr/saveMappedDetails";
            var param = {
                _token: token,
                hidden_id: hidden_id,
                selected_package_cood: selected_package_cood,
                package_name: package_name,
                details_array: details_array,
                grand_amount: grand_amount,
                package_amt: package_amt,
                discount_value: discount_value
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#saveUserMappedDetails").attr("disabled", true);
                    $("#saveUserMappedDetailsSpin").removeClass("fa fa-save");
                    $("#saveUserMappedDetailsSpin").addClass(
                        "fa fa-spinner fa-spin"
                    );
                },
                success: function(html) {
                    if (parseInt(html.status) == 1) {
                        toastr.success("Details added successfully.");
                        $(".search_user").removeClass("tr_active");
                        getAllPackages();
                        resetSettingDiv();
                    } else {
                        toastr.warning("Sorry,something went wrong.");
                    }
                },
                complete: function() {
                    $("#saveUserMappedDetails").attr("disabled", false);
                    $("#saveUserMappedDetailsSpin").removeClass(
                        "fa fa-spinner fa-spin"
                    );
                    $("#saveUserMappedDetailsSpin").addClass("fa fa-save");
                }
            });
        } else {
            toastr.warning("Nothing to save.");
        }
    } else {
        toastr.warning("Nothing to save.");
    }
});
function saveDoctorMappedDetails() {
    var assessment_data = [];
    $(".save_assessment_check").each(function() {
        if ($(this).is(":checked")) {
            assessment_data.push($(this).val());
        }
    });
    var save_doc = $("#save_doc").val();
    var string_assessment_data = JSON.stringify(assessment_data);
    if (save_doc) {
        if (
            assessment_data.sort().toString() !=
            editable_array.sort().toString()
        ) {
            if (string_assessment_data.length != 0) {
                var token = $("#c_token").val();
                var url = $("#base_url").val() + "/emr/saveDoctorMappedDetails";
                var param = {
                    _token: token,
                    save_doc: save_doc,
                    string_assessment_data: string_assessment_data
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#saveUserMappedDetails").attr("disabled", true);
                        $("#saveUserMappedDetailsSpin").removeClass(
                            "fa fa-save"
                        );
                        $("#saveUserMappedDetailsSpin").addClass(
                            "fa fa-spinner fa-spin"
                        );
                    },
                    success: function(html) {
                        if (parseInt(html.status) == 1) {
                            toastr.success("Details added successfully.");
                            $(".search_user").removeClass("tr_active");
                            resetSettingDiv();
                        } else {
                            toastr.warning("Sorry,something went wrong.");
                        }
                    },
                    complete: function() {
                        $("#saveUserMappedDetails").attr("disabled", false);
                        $("#saveUserMappedDetailsSpin").removeClass(
                            "fa fa-spinner fa-spin"
                        );
                        $("#saveUserMappedDetailsSpin").addClass("fa fa-save");
                    }
                });
            } else {
                toastr.warning("Nothing to save.");
            }
        } else {
            toastr.warning("No change found.");
        }
    } else {
        toastr.warning("Please select a doctor.");
    }
}
