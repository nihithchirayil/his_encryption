
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_radiology_service_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});
$("#department_add").on('change', function() {
    var department = $('#department_add').val();
    if (department == '') {
      var department = 'a';

    }

 if(department) {
    var base_url=$('#base_url').val();
    var url = base_url + "/master/search_radiology_service";
   // var url= route_json.save_sub_department;

        $.ajax({
                    type: "GET",
                    url: url,
                    data: 'department=' + department,
                    beforeSend: function () {
                        $('#s2id_subdepartment_add').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#subdepartment_add" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#subdepartment_add").empty();

                                        $("#subdepartment_add").html('<option value="">Select Sub Department</option>');
                                        $.each(html,function(key,value){
                                          $("#subdepartment_add").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#subdepartment_add").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                     // alert('Please check your internet connection and try again');
                     $( "#subdepartment_add" ).prop( "disabled", false );

                    },
                    complete: function () {
                        $('#warning1').remove();
                        $( "#subdepartment_add" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $("#department_add").focus();
                $("#subdepartment_add").empty();
            }

    });
    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
        data.service_name = $("#service_name").val();
        data.room_type = $("#room_type").val();
      
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }
   
    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".service_name_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Service Name");
            return;
        }
        if($(".service_code_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Service Code");
            return;
        }
    
        
        data.service_id = $(".reference_id").val();
        data.service_name = $(".service_name_add").val();
        data.service_code = $(".service_code_add").val();
        data.department = $("#department_add").val();
        data.subdepartment = $("#subdepartment_add").val();
        data.is_billable = $(".is_billable").val();
        data.is_refundable = $(".is_refundable").val();
        data.is_discountable = $(".is_discountable").val();
        data.is_price_editable = $(".is_price_editable").val();
        data.applicable_gender = $(".applicable_gender_add").val();
        data.status = $(".status_add").val();
        data.effective_date = $(".effective_date").val();
        data.ipprice = $(".ipprice").val();
        data.opprice = $(".opprice").val();       
        data.status_name = $(".status_name").val();
       
       
        if($(".status_add").prop('checked') == true){
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".status_name").prop('checked') == true){
            data.status_name =1;
        }else{
            data.status_name =0;
        }
      
      
        if($(".is_billable").prop('checked') == true){
            data.is_billable ='t';
        }else{
            data.is_billable ='f';
        }
        if($(".is_refundable").prop('checked') == true){
            data.is_refundable ='t';
        }else{
            data.is_refundable ='f';
        }
        if($(".is_discountable").prop('checked') == true){
            data.is_discountable ='t';
        }else{
            data.is_discountable ='f';
        }
        if($(".is_price_editable").prop('checked') == true){
            data.is_price_editable ='t';
        }else{
            data.is_price_editable ='f';
        }
       
       
       
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);
            
            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Service Code Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                  
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
    
    function editItem(element){
        // $('#restrict_duration').click( function() {
            
        //    }); 
        let service_id = $(element).parents('tr').attr('data-id');
        let service_name = $(element).parents('tr').attr('data-service_desc');
        let service_code = $(element).parents('tr').attr('data-service_code');
        let department = $(element).parents('tr').attr('data-department_id');
        let subdepartment = $(element).parents('tr').attr('data-subdepartment_id');
        let is_billable = $(element).parents('tr').attr('data-billable');
        let is_refundable = $(element).parents('tr').attr('data-refundable');
        let is_discountable = $(element).parents('tr').attr('data-discountable');
        let is_price_editable = $(element).parents('tr').attr('data-price_editable');
        let applicable_gender = $(element).parents('tr').attr('data-gender');
        let status = $(element).parents('tr').attr('data-status');
        let opprice = $(element).parents('tr').attr('data-opprice');
        let ipprice = $(element).parents('tr').attr('data-ipprice');      
        let effective_date = $(element).parents('tr').attr('data-effective_date');
        let status_name = $(element).parents('tr').attr('data-status_service_charge');
       
        $(".reference_id").val(service_id);
        $(".service_name_add").val(service_name);
        $(".service_code_add").val(service_code);
        $("#department_add").val(department);
        $("#department_add").select2().trigger('change'); 
        setTimeout(relativeSubdepartment(subdepartment),1000);
      
        $(".applicable_gender_add").val(applicable_gender);
        $(".opprice").val(opprice);
        $(".ipprice").val(ipprice);
        $("#effective_date").val(moment(effective_date).format('YYYY-MM-DD'));
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
        $("#add").html('Edit Radiology Service');
    
      
        if(status_name==1){
            $(".status_name").prop("checked", true);

        } else {
             $(".status_name").prop("checked", false);

        }
        if(status==1){
            $(".status_add").prop("checked", true);

        } else {
             $(".status_add").prop("checked", false);

        }
        if(is_billable==1){
            $(".is_billable").prop("checked", true);

         } else {
             $(".is_billable").prop("checked", false);
         
         }
        
         if(is_refundable==1){
            $(".is_refundable").prop("checked", true);

        } else {
             $(".is_refundable").prop("checked", false);

        }
        if(is_discountable==1){
            $(".is_discountable").prop("checked", true);

         } else {
             $(".is_discountable").prop("checked", false);
         
         }
         if(is_price_editable==1){
             $(".is_price_editable").prop("checked", true);
         
         } else {
            $(".is_price_editable").prop("checked", false);
         
         }
         setTimeout(getopipprice(service_id),3000);

       
        }
    function resetForm(){
        $(".reference_id").val('');
        $(".service_name_add").val('');
        $(".service_code_add").val('');
        $("#department_add").val('').trigger('change');
        $("#subdepartment_add").val('').trigger('change');
        $('input:checkbox').removeAttr('checked');
        $(".applicable_gender_add").val('');
        $(".opprice").val('');
        $(".ipprice").val('');     
        $(".effective_date").val('');
        $("#add").html('Add Radiology Service');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

    function getopipprice(service_id){
        var url=$('#base_url').val();
        $(".reference_id").val(service_id);
        var param = {service_id:service_id};
            
         $.ajax({
            type: "GET",
            url: url + "/master/getopipprice",
            data: param,
            beforeSend: function () {
              
            
            },
            success: function (data) {  
               
                $('.opprice').val(data.oppricing);
                $('.ipprice').val(data.ippricing);
            },
            complete: function () {

            }
        });
    }
$('#department').on('change',function(){
 relativeSubdepartment();
})
function relativeSubdepartment(id=0){
    var department = $('#department_add').val();
    if (department == '') {
      var department = 'a';

    }

 if(department) {
    var base_url=$('#base_url').val();
    var url = base_url + "/master/search_radiology_service";
   // var url= route_json.save_sub_department;

        $.ajax({
                    type: "GET",
                    url: url,
                    data: 'department=' + department,
                    beforeSend: function () {
                        $('#s2id_subdepartment_add').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#subdepartment_add" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#subdepartment_add").empty();

                                        $("#subdepartment_add").html('<option value="">Select Sub Department</option>');
                                        $.each(html,function(key,value){
                                          $("#subdepartment_add").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#subdepartment_add").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                     // alert('Please check your internet connection and try again');
                     $( "#subdepartment_add" ).prop( "disabled", false );

                    },
                    complete: function () {
                        if(id!=0){
                            $("#subdepartment_add").val(id).select2();
                        }
                        $('#warning1').remove();
                        $( "#subdepartment_add" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $("#department_add").focus();
                $("#subdepartment_add").empty();
            }

    
}
