$(document).ready(function (){
    setTimeout(function () {
        var fav_form_id = $('#fav_form_id').val();
        var form_id = $('#form_id').val();
        if(form_id =='' || form_id == 0){
            if(fav_form_id !=''){
               // loadform(fav_form_id,0);
            }
        }
    },3000);

});


$('select[name="forms"]').select2({
    minimumInputLength: 2,
    ajax: {
        url: $('#base_url').val() + '/clinical_notes/searchFormsAppVersion',
        dataType: 'json',
        data: function (params) {
            var query = {
                term: params.term,
            }
            return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

$(window).load(function () {
    fetchClinicalAssessmentForms();
    setTimeout(function () {
        $('#Timetaken').datetimepicker({
            format: 'MMM-DD-YYYY hh:mm A'
        });

        $("#Timetaken").attr("autocomplete", "off");
        let form_id = $('#form_id').val();
        let form_list_id = $('#form_list_id').val();
        if (form_list_id != 0) {
            assessmentEditMode(form_id, form_list_id, 'edit');
        }

    }, 500);

});

function fetchClinicalAssessmentForms() {
    let _token = $('#c_token').val();
    let user_id = $('#user_id').val();
    $.ajax({
        type: "POST",
        async: false,
        url: $('#base_url').val() + '/clinical_notes/fetchClinicalAssessmentFormsAppVersion',
        data: {
            _token: _token,
            user_id: user_id,
        },
        beforeSend: function () {

        },
        success: function (data) {
            // console.log(data);
            $("#forms").empty();
            $("#forms").append("<option value=''>Select Template</option>");
            let favourited_form_id = $('#favourited_form_id').val();
            let selected = "";
            $.each(data.forms, function (key, val) {

                if (val != null && val != undefined) {

                    if (parseInt(val.id) == parseInt(favourited_form_id)) {
                        selected = "selected='selected'";
                    } else {
                        selected = "";
                    }

                    $("#forms").append("<option " + selected + " value='" + val.id + "'> " + val.name + "</option>");
                }


            });
            $("#forms").select2();


            if (window.latest_form_id) {
                $('#ca_head_id').val(window.latest_form_head_id);
                $('#ca_edit_status').val(1);
                window.clinical_form_id = window.latest_form_id;
                loadForm(window.latest_form_id, window.latest_form_head_id);
                // $("#forms").val(window.latest_form_id).trigger('change');


            } else if ($("#fav_form_id").val()) {
                $("#forms").val($("#fav_form_id").val()).trigger('change');
                $('.fav-button-forms').addClass('bg-orange');
            }


        },
        complete: function () {

        }
    });
}

$('select[name="forms"]').on('change', function (event) {
    event.preventDefault();
    $('body').LoadingOverlay("show", { background: "rgba(0, 0, 0, 0.7)", imageColor: '#009869' });
    let form_id = $(this).val();
    setTimeout(function () {
        selectTemplateForm(form_id);
    }, 500);

});

function selectTemplateForm(form_id) {
    window.clinical_form_id = form_id;
    $('#ca_head_id').val('');
    $('#ca_edit_status').val(0);
    if (form_id != '' && form_id != undefined) {
        loadForm(form_id, 0);
       // $('#form-fields-container').LoadingOverlay("hide");
    } else {
        $(".clinical_data_content").show();
        $('body').LoadingOverlay("hide");

    }
}

function loadForm(form_id, head_id = 0) {
    let patient_id = $('#patient_id').val();
    let user_id = $('#user_id').val();
    let doctor_id = $('#doctor_id').val();
    if (form_id != '' && form_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            async: false,
            url: url + "/clinical_notes/loadFormFieldsAppVersion",
            url: url + "/dr_casuality/load-new-template-modal",
            data: {
                form_id: form_id,
                head_id: head_id,
                user_id: user_id,
                patient_id: patient_id,
                doctor_id: doctor_id,
                _token: _token
            },
            beforeSend: function () {

            },
            success: function (data) {
                var obj = JSON.parse(data);
                $('#template-content-load-div').css('padding', '20px');
                $('#template-content-load-div').html(obj.response);
                $(".clinical_data_content").show();
                $(".investigation_data_content").show();
                $(".prescription_data_content").show();
                $(".body_spinner").hide();
                $('body').LoadingOverlay("hide");
                $('#forms').select2({ dropdownAutoWidth: true });
                $('#template_save_container').css('display', 'block');
                $('.fav-list-table').parent().css('display', 'none');

            },
            complete: function () {
                var include_patient_header = $('#include_patient_header').val();
                if (head_id == 0 || head_id == "") {
                    //tinymce
                    //initTinymce(include_patient_header);
                    initTinymce(0);
                    //progressive search
                    initProgressSearch();

                    initLibraries();
                } else {

                    if ($.trim($('#tinymce_names').val()) == "") {
                        var tinyArr = '';
                    } else {
                        var tinyArr = JSON.parse($('#tinymce_names').val());
                    }
                    //destroy
                    if (tinyArr != '' && tinyArr != undefined) {
                        tinymce.remove("textarea.texteditor");
                        // $.each(tinyArr,function(ind,el){
                        // var editorCache = tinymce.get(el); //get from cache
                        // console.log(editorCache);
                        // if(editorCache != '' || editorCache != null|| editorCache != undefined){
                        // tinymce.execCommand('mceRemoveControl', true, el);
                        // }
                        // });
                    }

                    //tinymce
                    setTimeout(function () { initTinymce() }, 3000);

                    //progressive search
                    initProgressSearch();

                    initLibraries();

                }

                setTimeout(function () {
                    $('#Timetaken').datetimepicker({
                        format: 'MMM-DD-YYYY hh:mm A'
                    });
                    $("#Timetaken").attr("autocomplete", "off");
                }, 500);


            }
        });
    }
}

function initLibraries() {
    //datepicker
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('.theadscroll').perfectScrollbar({
        // suppressScrollX: true,
        minScrollbarLength: 30
    });

}

function initTinymce(include_patient_header) {

    tinymce.init({
        selector: 'textarea.texteditor',
        autoresize_min_height: '90',
        mobile: {
            menubar: false
        },

        plugins: ' paste importcss  autosave save directionality code visualblocks visualchars advlist lists    help emoticons autoresize',
        imagetools_cors_hosts: ['picsum.photos'],
        toolbar: 'undo redo | bold italic underline | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify |  numlist bullist |',
        browser_spellcheck: true,
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        contextmenu: false,
        skin: "oxide-dark",
        content_css: "dark",

        importcss_append: true,

        height: 800,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',

        branding: false,
        statusbar: false,
        forced_root_block: '',
        setup: function (editor) {


        },


    });


    // if (include_patient_header == 1) {
    //     setTimeout(function () { includePatientHeaderTemplate(); }, 3000);
    // }

}

function includePatientHeaderTemplate() {
    var visit_id = $('#visit_id').val();
    let url = $('#base_url').val();
    $.ajax({
        type: "GET",
        async: false,
        url: url + "/emr/load-patient-header",
        data: {
            visit_id: visit_id,
        },
        beforeSend: function () {

        },
        success: function (data) {
            tinymce.activeEditor.setContent(data);
        },
        complete: function () {

        },
    });
}

function initProgressSearch() {
    $(".progress-search-multiple").select2({
        placeholder: 'Search...',
        width: '100%',
        allowClear: false,
        ajax: {
            url: $('#base_url').val() + '/emr/form-progress-search',
            dataType: 'json',
            data: function (params) {
                var query = {
                    term: params.term,
                    table: $(this).attr('data-table'),
                    value: $(this).attr('data-value'),
                    text: $(this).attr('data-text'),
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}

var ca_data_changed = 0;
var ca_tab_changed = 0;
//trigger when click li
$(document).on('click', '#form-content-fields-section li', function (e) {
    ca_tab_changed = 1;
    if (ca_tab_changed == 1 && ca_data_changed == 1) {
        //alert('save');
        ca_data_changed = 0;
        ca_tab_changed = 0;
    }
});

//trigger when data changed
$(document).on('click', '#form-content-data-section :input', function (e) {
    ca_data_changed = 1;
});

//preview button
function loadPreviewTemplate() {
    saveClinicalTemplate(1);
}

var save_ca_starts = 0;
//save clinical assessment
function saveClinicalTemplate(save_status, dont_show_toastr = false) {
    let _token = $('#c_token').val();

    if (save_ca_starts == 1) {
        return false;
    }



    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let ca_head_id = $('#ca_head_id').val();
    let forms = window.clinical_form_id;
    //  let forms = $('#forms').val();
    //alert(forms);return;
    let tableList = $('#table_names').val();
    let gcheckboxNamesArr = $('#gcheckbox_names').val();
    let psearchNamesArr = $('#psearch_names').val();
    let admitting_doctor_id = $('#admitting_doctor_id').val();
    let ca_form = $('#ca-data-form').serializeArray();
    let user_id = $('#user_id').val();
    let psearchSelectedArr = [];
    //to check there is no data entered
    let dataValueArr = [];
    var edit_status = $('#ca_edit_status').val();

    //tinymce datas list
    if ($.trim($('#tinymce_names').val()) == "") {
        var tinyList = '';
    } else {
        var tinyList = JSON.parse($('#tinymce_names').val());
    }

    //table strucure check
    //all table field name array
    var tableNames = "";
    if ($.trim(tableList) != "" && $.trim(tableList) != undefined) {
        tableNames = JSON.parse(tableList);
    }
    var tableFieldNames = [];
    var arrTableRowColData = [];
    if ((tableNames.length) > 0) {

        for (var j = 0; j < tableNames.length; j++) {
            var colsFieldName = '';
            var tableWiseCols = '';
            colsCount = $('input[name="' + tableNames[j] + '_#colsCount"]').val();
            rowsCount = $('input[name="' + tableNames[j] + '_#rowsCount"]').val();
            var fieldName = tableNames[j];
            tableFieldNames.push(fieldName);
            arrTableRowColData.push({ 'cols': colsCount, 'rows': rowsCount });
        }

    }

    //group checkbox check
    //all group checkbox field name array
    var gcheckboxNames = gcheckboxNamesArr;

    //all Progress Search field name array
    if ($.trim($('#psearch_names').val()) == "") {
        var psearchNames = '';
    } else {
        var psearchNames = JSON.parse($('#psearch_names').val());
    }


    var datas = {};
    //loop through all fields and create json
    $(ca_form).each(function (index, obj) {

        //tinymce data setup
        if ((tinyList.length) > 0) {
            //check its a tinymce field
            if (tinyList.indexOf(obj.name) >= 0) {
                let tinycontent = '';
                tinycontent = tinyMCE.get(obj.name).getContent();
                ca_form[index].value = tinycontent;
                datas[obj.name] = tinycontent;
                return;
            }
        }

        //progress search data setup [multiselect]
        if ((psearchNames.length) > 0) {
            //check its a progress search field
            if (psearchNames.indexOf(obj.name) >= 0) {
                datas[obj.name + '_psearch_' + obj.value] = obj.value;
                psearchSelectedArr.push(obj.name + '_psearch_' + obj.value);
            }
        }

        datas[obj.name] = obj.value;

        if (obj.value != '' && obj.value != undefined) {
            dataValueArr.push(obj.value);
        }

    });

    //remove progress old keys
    if ((psearchNames.length) > 0) {
        $.each(psearchNames, function (index, psdata) {
            if (psdata != "" && psdata != undefined) {
                delete datas[psdata];
            }
        });
    }

    let validate = validateAssessment(dataValueArr);

    if (forms == "") {
        Command: toastr["warning"]("Error.! Select Form.");
        //alert('Error.! Select Form.');
        return false;
    }

    datas = JSON.stringify(datas);
    datas = encodeURIComponent(datas)

    if (validate) {
        var url = $('#base_url').val() + "/clinical_notes/saveclinicalAssessmentAppVersion";
        save_ca_starts = 1;
        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&ca_head_id=' + ca_head_id + '&form_id=' + forms + '&arrTableRowColData=' + JSON.stringify(arrTableRowColData) + '&tableFieldNames=' + JSON.stringify(tableFieldNames) + '&gcheckboxNames=' + gcheckboxNames + '&psearchNames=' + JSON.stringify(psearchSelectedArr) + '&datas=' + datas + '&edit_status=' + edit_status + '&user_id=' + user_id,
            beforeSend: function () {
                $('body').LoadingOverlay("show", { background: "rgba(0, 0, 0, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('body').LoadingOverlay("hide");
                // console.log(data);return;
                if (data.status == 1) {
                    window.atleastOneSavedRecords = true;
                    if (dont_show_toastr != true) {
                        Command: toastr["info"]("Save.");
                        //alert('save');
                    }

                    if (window.save_encounter_status && window.save_encounter_status == "TRUE") {
                        window.save_encounter_status = "FALSE";
                        var investigation_head_id_c = $("#investigation_head_id").val();
                        var url = $('#base_url').val() + "/emr/save-doctors-encounter-datas";
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&investigation_head_id=' + investigation_head_id_c,
                            beforeSend: function () {

                            },
                            success: function () {
                                $('.draft_mode_indication_ca').empty()
                            },
                            complete: function () {

                            }
                        });

                    } else {
                        $('.draft_mode_indication_ca').html('<span class="bg-orange draft-badge"> Draft Mode <i class="fa fa-exclamation-circle" title="Clinical Notes saved as draft" aria-hidden="true"></i></span>');
                    }

                    assessmentEditMode(data.head_id, data.form_id, 'edit');
                    //if ($("#print_clinical_notes").is(':checked')) {
                    //    printFormView(data.head_id,data.form_id);
                    // }
                    //loadassessmentPreview(data.head_id,data.form_id);

                } else {
                    Command: toastr["warning"]("Error.");
                    //alert('Error');
                }
                save_ca_starts = 0;
                $('#assesment_draft_mode').val(0);

                if (save_status == 2) {
                    printFormView(data.head_id, data.form_id);
                    $('.draft_mode_indication_ca').html('');
                }



            },
            complete: function () {
                //location.reload();
                // medicine_listing_table_changed = 0;
                $('.save_btn_bg').prop({ disable: 'false' });

                if (save_status == 2) {


                    var nursing_station = $("#nursing_station").val();
                    if (nursing_station && nursing_station != "") {
                        $('#ca_edit_status').val(0);

                        // ---------clear all entered data for nursing module---------------
                        $('#section1').find('textarea').val('');
                        $('#section1').find('input:text').val('');
                        $('#section1').find('input:radio').prop('checked', false);
                        $('#section1').find('input:checkbox').prop('checked', false);
                        $('#section1').find('li').removeClass('active');
                        $('#tab1').find('textarea').val('');
                        $('#tab1').find('input:text').val('');
                        $('#tab1').find('input:radio').prop('checked', false);
                        $('#tab1').find('input:checkbox').prop('checked', false);
                        $('#tab1').find('li').removeClass('active');
                        $(".select_button").find('input:hidden').val('')

                    }

                }



                ca_data_changed = 0;
                ca_tab_changed = 0;

                initTinymce();

                try {
                    updateSeenstatus(visit_id);
                } catch (e) {
                    // console.log(e)
                }
                $('#assesment_draft_mode').val(0);
                // window.onbeforeunload = null;

            }
        });

    } else {
        // medicine_listing_table_changed = 0;
        $('.save_btn_bg').prop({ disable: 'false' });
    }

}
function validateAssessment(dataValueArr) {
    if (Array.isArray(dataValueArr) && dataValueArr.length) {
        return true;
    }
    return false;
}
function assessmentEditMode(form_id, head_id, mode = 'edit') {
    if (head_id != '' && form_id != '') {
        $mode = 'edit';
        //alert(form_id + '##@@@@' + head_id + '##' + mode);
        //console.log(form_id + '##' + head_id);
        if (mode == 'edit') {
            //  alert(form_id + '##@@@@' + head_id + '##' + mode);
            $("#ca_head_id").val(head_id);
            $('#ca_edit_status').val(1);
        } else {
            $("#ca_head_id").val(0);
        }
        window.clinical_form_id = form_id;
        loadForm(head_id, form_id);
    }
}
function assessmentCopyMode(form_id, head_id, mode = 'copy') {
    if (head_id != '' && form_id != '' && mode != '') {
        if (mode == 'copy') {
            $("#ca_head_id").val(head_id);
        } else {
            $("#ca_head_id").val(0);
        }
        $('#ca_edit_status').val(0);
        window.clinical_form_id = form_id;
        loadForm(form_id, head_id);
        $("#ca_head_id").val(0);
    }
}
function loadassessmentPreview(head_id, form_id) {
    let _token = $('#c_token').val();
    let patient_id = $("#patient_id").val();
    if (head_id > 0) {
        var url = $('#base_url').val() + "/emr/load-preview";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&head_id=' + head_id + '&form_id=' + form_id + '&patient_id=' + patient_id,
            beforeSend: function () {

            },
            success: function (data) {
                if (data.status = 1) {
                    $(".preview_div").html(data.html);
                    $(".expand_preview_btn").trigger('click');
                }
            },
            complete: function () {

            }
        });
    }
}
//load ca view
function loadFormViewPopover(head_id, form_id) {

    if (head_id != '' && head_id != undefined) {

        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        let _token = $('#c_token').val();
        let popover = '1';
        $.ajax({
            type: "POST",
            url: url + "/emr/load-preview",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
                _token: _token,
                popover: popover
            },
            beforeSend: function () {
                // $('.pop-content-history').html('<div class="row"><div colspan="5" class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div></div>');
            },
            success: function (data) {
                $('.pop-content-history').html('');
                let response = '';

                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }

                $('.pop-content-history').html(response);



            },
            complete: function () {

            }
        });
    }

}

function loadFormView(head_id, form_id) {
    if (head_id != '' && head_id != undefined) {
        $('#form_det_list_modal').modal('show');
        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/load-preview",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $('#form_det_list_modal_content').html('<div class="row"><div colspan="5" class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div></div>');
            },
            success: function (data) {
                let response = '';

                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }

                $('#form_det_list_modal_content').html(response);
            },
            complete: function () {

            }
        });
    }
}


$(document).on('click', '.select_button li', function () {
    $(this).toggleClass('active');
    let sel_value = $(this).attr('data-value');
    if (sel_value != "" && sel_value != undefined) {
        if ($(this).hasClass('active')) {
            $(this).find('input').val(sel_value);
            $(this).find('input').prop("disabled", false);
        } else {
            $(this).find('input').val("");
            $(this).find('input').prop("disabled", true);
        }
    }
});

$(document).on('click', '.field-label-tab', function () {
    //Command: toastr["info"]("label changed.");
    alert("label changed");
});

function saveFavTemplate() {
    let selected_form = $('#forms').val();
    //alert(selected_form);
    if (selected_form != '' && selected_form != undefined) {
        let _token = $('#c_token').val();
        let user_id = $('#user_id').val();
        let doctor_id = $('#doctor_id').val();
        var url = $('#base_url').val() + "/clinical_notes/saveFormFavoriteAppVersion";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                selected_form: selected_form,
                _token: _token,
                user_id: user_id,
                doctor_id: doctor_id

            },
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == '1') {
                    Command: toastr["info"]('Bookmarked Successfully');
                } else {
                    Command: toastr["warning"]('Error..!');
                }
            },
            complete: function () {

            }
        });
    }
}

$(document).on("click", ".fav-list-table thead th", function () {
    //th element
    toogleFavoriteList(this);
});

function toogleFavoriteList(obj) {
    /*active class*/
    $(".fav-list-table").closest('.floating-table-list').not($(obj).closest('.floating-table-list')).removeClass('active-favorite');
    $(obj).closest('.floating-table-list').addClass('active-favorite');
    /*active class*/
    /*tbody hide and show*/
    $(".fav-list-table tbody").not($(obj).closest('table').find('tbody')).hide();
    /*tbody hide and show*/
    /* icon toogle */
    $(".fav-list-table thead th:first-child i").not($(obj).find('i')).removeClass('fa fa-caret-down').addClass('fa fa-caret-left');
    if ($(obj).find('i').hasClass('fa-caret-left')) {
        $(obj).find('i').removeClass('fa fa-caret-left').addClass('fa fa-caret-down');
    } else {
        $(obj).find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-left');
    }
    /* icon toogle */
    $(obj).closest('table').find('tbody').slideToggle();
}

$(document).on('focus', '.custom-text-box,.custom-text-area', function () {
    let name = $(this).prop('name');
    //if favorite enable
   //alert($(this));
    //if (name != '' && name != undefined && $('#fav-' + name).length > 0) {
        favoriteViewEnable($(this), name);
    //}
});

function favoriteViewEnable(element, name) {
  //  if (name != '' && name != undefined && $(element).closest('.row').find('#fav-' + name).length > 0) {
        let obj1 = 'fav-' + name;
        $('.fav-list-table').parent().css('display', 'none');
       // alert(obj1);
        $('#' + obj1).parent().css('display', 'block');
        $('#' + obj1).css('display', 'block');
  //  }
}

function favoriteViewShrinkAll() {
    $('.fav-list-table tbody:visible').prev('thead').find('th').trigger('click');
}

function addToFavFormItem(obj, form_item_id, from = 'field') {
    let fav_text = '';

    //in case add favorite from right side bootbox modal then take  favorite text from there else field
    var favSelectObj = $(obj).parent().find(':input');
    fav_text = $(favSelectObj).val();

    var form_id = $('#fav_form_id').val();
    if (form_item_id == "" || form_item_id == undefined) {
        Command: toastr["warning"]("Could Not Get Current Item Details..!");
        //alert("Could Not Get Current Item Details..!")
        return false;
    }

    if (fav_text == "" || fav_text == undefined) {
        Command: toastr["warning"]("No Data Found..!");
        //alert('No Data Found..!');
        $(favSelectObj).focus();
        return false;
    }

    let _token = $('#c_token').val();
    let user_id = $('#user_id').val();
    let doctor_id = $('#doctor_id').val();
    var url = $('#base_url').val() + "/dr_casuality/saveFormFieldFavoriteAppVersion";
    //alert(form_id);
    if (form_id != '' && form_id != undefined) {
        //addToFavoriteTable = 2 in case textarea or text box
        var urlData = "addToFavoriteTable=" + 2 + '&fav_form_id=' + form_id + '&fav_form_item_id=' + form_item_id + '&fav_text=' + fav_text + '&_token=' + _token + '&user_id=' + user_id + '&doctor_id=' + doctor_id;
        $.ajax({
            type: "POST",
            url: url,
            data: urlData,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == 1) {
                    Command: toastr["info"]("Saved Successfully..!");
                    //alert("Saved Successfully..!");
                    var name = "";
                    var type_name = "";


                    name = $(favSelectObj).attr('name');
                    if ($(favSelectObj).is('textarea')) {
                        type_name = "TEXT AREA";
                    } else {
                        type_name = "TEXT";
                    }

                    var id = data.id;
                    var tableDiv = $('#template-content-load-div').find('#fav-' + name);

                    var trlen = $(tableDiv).find('table tbody tr').length;

                    if ($(tableDiv).find('table').hasClass('hidden')) {
                        $(tableDiv).find('table').removeClass('hidden');
                    }


                    //in case all rows deleted manually then
                    if (trlen == 0) {
                        $(tableDiv).find('table tbody').append('<tr style=""><td style="height:30px;cursor: pointer;" onclick="addToSelected(\'' + name + '\',\'' + btoa(fav_text) + '\',\'' + type_name + '\')" >' + fav_text + '</td><td width="5%"><a style="float:right;color:white;" class="btn bg-red" name="remove" onclick="removeFavorite(\'' + id + '\',\'' + btoa(fav_text) + '\',this)" title="remove From List"><i class="fa fa-trash-o"></i></a></td></tr>');
                    } else {
                        //already data exist check
                        var dataexist = false;

                        $(tableDiv).find('table tbody tr td:first-child').each(function (index, el) {
                            var cur_el_text = $(el).text();
                            if (cur_el_text == fav_text) {
                                dataexist = true;
                            }
                        });

                        if (dataexist == true) {
                            return false;
                        }

                        $(tableDiv).find('table tbody tr:last').after('<tr style=""><td style="height:30px;cursor: pointer;" onclick="addToSelected(\'' + name + '\',\'' + btoa(fav_text) + '\',\'' + type_name + '\')" >' + fav_text + '</td><td width="5%"><a style="float:right;color:white;" class="btn bg-red" name="remove" onclick="removeFavorite(\'' + id + '\',\'' + btoa(fav_text) + '\',this)" title="remove From List"><i class="fa fa-trash-o"></i></a></td></tr>');
                    }

                    $(tableDiv).find('table thead tr:first').removeClass('table_header_bg');
                    $(tableDiv).find('table thead tr:first').css('background-color', '#3498db');
                    $(tableDiv).find('table thead tr:first').css('color', 'white');
                    $(tableDiv).find('table thead tr:first').css('border', 'none');

                    let table_tr_elm = $(tableDiv).find('table tbody tr:first').text();
                    if (table_tr_elm != undefined) {
                        if (table_tr_elm.trim() == "No Data Found ..!") {
                            $(tableDiv).find('table tbody tr:first').remove();
                        }
                    }
                }
            },
            complete: function () {
                $('.theadfix_wrapper').floatThead("reflow");
                $('.theadscroll').perfectScrollbar('update');
            }
        });
    } else {
        Command: toastr["warning"]("Could Not Get Form Details..!");
        //alert("Could Not Get Form Details..!");
        return false;
    }

}

function removeFavorite(id, cur_fav_text, obj) {
    cur_fav_text = atob(cur_fav_text);
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/clinical_notes/removeFormFieldFavoriteAppVersion";

    if (id != '') {
        $.ajax({
            type: "POST",
            url: url,
            data: "removeFavorite=" + id + "&_token=" + _token,
            beforeSend: function () {
            },
            success: function (data) {
                if (data == 1) {
                    $(obj).closest('tr').remove();
                    Command: toastr["info"]("Removed Successfully..!");
                    //alert('Removed Successfully..!');
                }
            },
            complete: function () {
            }
        });
    } else {
        Command: toastr["warning"]("Could Not Get Current Item Details..!");
        // alert("Could Not Get Current Item Details..!");
    }
}

function oldDataPreFetch(field_name, form_id, patient_id) {
    if (field_name != '' && form_id != '' && patient_id != '') {
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/emr/load-old-data-form-field";
        $.ajax({
            type: "POST",
            url: url,
            data: "select_old_prefetchdata=" + 1 + "&patient_id=" + patient_id + "&field_name=" + field_name + "&form_id=" + form_id + "&_token=" + _token,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.result != '' && data.type != '') {
                    if (data.type == 'Table') {
                        let resultData = data.result;
                        let j = 0;
                        for (let elem in resultData) {
                            let numRows = resultData[elem].length;
                            for (i = 0; i < numRows; i++) {
                                let dataId = field_name + '_' + j + '_' + i;
                                $('#' + dataId).val(resultData[elem][i]);
                            }
                            j++;
                        }
                    } else if (data.type == 'Grouped CheckBox' || data.type == 'Radio Button' || data.type == 'Check Box') {
                        if (data.result != undefined && data.result != "") {
                            $('input[name="' + field_name + '"][value="' + data.result + '"]').prev().trigger('click');
                        }
                    } else if (data.type == 'Select Box') {
                        if (data.result != undefined && data.result != "") {
                            $('select[name="' + field_name + '"]').val(data.result);
                        }
                    } else {
                        if (data.result != undefined && data.result != "") {
                            $('#' + field_name).val(data.result);
                        }
                    }
                }
            },
            complete: function () {

            }
        });
    }
}

function addToSelected(element_id, cur_fav_text, input_type) {
    cur_fav_text = atob(cur_fav_text);
    if (cur_fav_text != '') {
        //in case text box or textarea
        if (input_type != "" && input_type == "TEXT AREA") {
            var value = $('textarea[name="' + element_id + '"]').val();
            if (value == '') {
                $('textarea[name="' + element_id + '"]').val(cur_fav_text);
            } else {
                $('textarea[name="' + element_id + '"]').val(value + ' , ' + cur_fav_text);
            }
        } else if (input_type != "" && input_type == "TEXT") {
            var value = $('input[name="' + element_id + '"]').val();
            if (value == '') {
                $('input[name="' + element_id + '"]').val(cur_fav_text);
            } else {
                $('input[name="' + element_id + '"]').val(value + ' , ' + cur_fav_text);
            }

        }
    } else {
        Command: toastr["warning"]("Could Not Get Current Item Details..!");
        //alert("Could Not Get Current Item Details..!");
    }
}
function assessmentDeleteMode(obj, id) {
    if (id != "" && id > 0) {
        var result = confirm("Want to delete?");
        if (result) {

            let _token = $('#c_token').val();
            var url = $('#base_url').val() + "/emr/remove-patient-form-data";
            $.ajax({
                type: "POST",
                url: url,
                data: "removeFormData=" + id + "&_token=" + _token,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status == 1) {
                        $(obj).closest('tr').remove();
                        Command: toastr["info"]("Removed Successfully..!");
                        //alert('Removed Successfully..!');
                    }
                },
                complete: function () {
                }
            });
        }
    }
}
function removeFormFromFavorites(form_fav_id) {
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/removeFormFromFavorites";
    var that = this;
    $.ajax({
        type: "POST",
        url: url,
        data: "_token=" + _token + "&form_fav_id=" + form_fav_id,
        beforeSend: function () {
            $(that).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).find('i').addClass('fa-trash').removeClass('fa-spinner').removeClass('fa-spin');
            Command: toastr["info"]("Removed Successfully..!");
            //alert('Removed Successfully..!');
            fetchFavoriteTemplates();
        },
        complete: function () {
        }
    });
}


function showForm(form_id, formdata_id) {
    if (formdata_id) {
        loadForm(form_id, formdata_id);
    } else {
        $("#forms").val(window.latest_form_id).trigger('change');
    }

    window.clinical_form_id = form_id;


    $("#form_fav_list_modal").modal('hide');
}

//load history
function fetchAssessmentHistory(page) {
    var url = $('#base_url').val() + "/emr/fetchAssessmentHistory";
    var patient_id = $('#patient_id').val();
    let _token = $('#c_token').val();
    var show_doctor_notes = 0;
    var show_nursing_notes = 0;
    if ($('#chk_show_doctor_notes').prop("checked") == true) {
        show_doctor_notes = 1;
    }
    if ($('#chk_show_nursing_notes').prop("checked") == true) {
        show_nursing_notes = 1;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: {
            clinical_history: 1,
            page: page,
            patient_id: patient_id,
            show_doctor_notes: show_doctor_notes,
            show_nursing_notes: show_nursing_notes,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#ca-history-data-list').html(data);
            }

            $(".pop-history-btn").popover({
                html: true,
                placement: "right",
                trigger: "hover",

                content: function () {
                    return $(".pop-content-history").html();
                }
            });

            $(".pop-history-btn").mouseenter(function () {
                $(".pos_stat_hover ").css('position', 'static');
            });


            $(".pop-history-btn").mouseleave(function () {
                $(".pos_stat_hover ").css('position', 'relative');
            });

        },
        complete: function () {
        }
    });
}



$('#ca-history-data-list').on('click', '.pagination a', function (e) {
    fetchAssessmentHistory($(this).attr('href').split('page=')[1]);
    e.preventDefault();
});


function viewNursesNotes() {
    var visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/emr/viewNursesNotes";
    $.ajax({
        type: "GET",
        url: url,
        data: "&visit_id=" + visit_id,
        beforeSend: function () {
            $('#nursing_notes_modal').modal('show');

            $('#nursing_notes_modal_data').LoadingOverlay("show", { background: "rgba(0, 0, 0, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#nursing_notes_modal_data').html(data);
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            $('#nursing_notes_modal_data').LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead("reflow");
            $('.theadscroll').perfectScrollbar('update');
        }
    });
}


function favoriteForm(formdata_id, form_id) {
    console.log(formdata_id);
    console.log(form_id);
    var that = this;

    var url = $('#base_url').val() + "/emr/bookmarkAssessment";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            formdata_id: formdata_id,
            form_id: form_id,
            _token: _token
        },
        beforeSend: function () {
            $(that).removeClass('fa-star').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-star');
        },
        complete: function () {
            Command: toastr["info"]("Bookmarked..!");
            //alert("Bookmarked..!");
        }
    });


}
