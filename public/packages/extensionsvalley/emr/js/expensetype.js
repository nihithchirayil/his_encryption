$(document).ready(function() {
    searchType();
    console.log('hiiii');
 });
 
 function searchType() {
     var expense_type = $("#expense_type").val().trim();
     var base_url = $("#base_url").val();
     var token = $("#token_hiddendata").val();
     var param = { _token: token, expense_type: expense_type };
 
     var url = base_url + "/master/searchExpenseType";
 
  
     $.ajax({
         type: "POST",
         url: url,
         data: param,
         beforeSend: function() {
            $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
             $("#searchBtn").attr("disabled", true);
             $("#searchSpin").removeClass("fa fa-search");
             $("#searchSpin").addClass("fa fa-spinner fa-spin");
         },
         success: function(data) {
 
             $("#searchDataDiv").html(data);
 
         },
         complete: function() {
             $("#searchBtn").attr("disabled", false);
             $("#searchSpin").removeClass("fa fa-spinner fa-spin");
             $("#searchSpin").addClass("fa fa-search");
             $("#searchDataDiv").LoadingOverlay("hide");

         },
         error: function() {
             toastr.error("Error Please Check Your Internet Connection");
         },
     });
  
 }
 
 function formReset() {
    $('.reset').val('');
    $('#status').val(1);
    $('#hid_id').val('');
     $("#savebtnBtn").html('<i id="savebtnSpin" class="fa fa-save"></i> Save');
 
 }
 
 function saveType() {
     var base_url = $("#base_url").val();
     var token = $("#token_hiddendata").val();
     var type_name = $("#type_name").val();
     var status = $("#status").val();
     var hid_id = $("#hid_id").val();
     if(type_name){
     var url = base_url + "/master/saveType";
     var params = {
         _token: token,
          hid_id: hid_id,
          type_name: type_name,
          status: status
     };
 
    
     $.ajax({
         type: "POST",
         url: url,
         data: params,
         beforeSend: function() {
             $("#savebtnBtn").attr("disabled", true);
             $("#savebtnSpin").removeClass("fa fa-save");
             $("#savebtnSpin").addClass("fa fa-spinner fa-spin");
         },
         success: function(data) {
              console.log(data);
             if (data.status==1) {
                 toastr.success("Success.");
                 formReset();
                 searchType();
 
             }else if(data.status==2){
                 toastr.warning("Existing type name.");
                 $('#type_name').val('');
                 $('#type_name').focus();
             }
         },
         complete: function() {
             $("#savebtnBtn").attr("disabled", false);
             $("#savebtnSpin").removeClass("fa fa-spinner fa-spin");
             $("#savebtnSpin").addClass("fa fa-save");
 
 
         },
         warning: function() {
             toastr.error("Error Please Check Your connection ");
         },
     });
 }else{
     toastr.warning('Please add type name.');
 }
 }
 
 
 
 
 function editItem(id,name,status) {
    
    $('#type_name').val(name);
    $('#status').val(status);
    $('#hid_id').val(id);
     $("#savebtnBtn").html('<i id="savebtnSpin" class="fa fa-save"></i> Update');
 
 }
 
 function delete_menu(id) {
     var base_url = $("#base_url").val();
     var token = $("#token_hiddendata").val();
     bootbox.confirm({
        message: "Are You Sure You want Delete ?",
        buttons: {
            confirm: {
                label: "Delete",
                className: "btn-danger",
                
            },
            cancel: {
                label: "Cancel",
                className: "btn-warning",
                default: "true",
            },
        },
        callback: function (result) {
            if (result) {
    
                let url = base_url + "/master/deleteType";
                let data = { _token: token, id: id };
        
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                        $("#deleteButton_" + id).attr("disabled", true);
                        $("#deleteButton_" + id).removeClass("fa fa-trash");
                        $("#deleteButton_" + id).addClass("fa fa-spinner fa-spin");
        
        
                    },
                    success: function(data) {
                        $("#trlist_" + id).remove();
                        toastr.success('Deleted.');
                        searchType();
                    },
                    complete: function() {
        
                        $("#deleteButton_" + id).attr("disabled", false);
                        $("#deleteButton_" + id).removeClass("fa fa-spinner fa-spin");
                        $("#deleteButton_" + id).addClass("fa fa-trash");
        
                    },
        
                });
            
            }
        },
    });
 
 }