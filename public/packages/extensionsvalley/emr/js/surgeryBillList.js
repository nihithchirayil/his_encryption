$(document).ready(function() {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    searchSurgeryBill();

    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
});

var base_url = $("#base_url").val();
var token = $("#c_token").val();

$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();




        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $('#base_url').val()

            var url = base_url + "/surgery/surgery_AjaxSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();

                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        // ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();


}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
});

function searchSurgeryBill() {
    var bill_head_id = $('#surgery_bill_no_hidden').val();
    var fromDate = $('#from_date').val();
    var toDate = $('#to_date').val();
    var url = base_url + "/surgery/surgeryBillListing"
    var param = {
        bill_head_id: bill_head_id,
        fromDate: fromDate,
        toDate: toDate
    }
    console.log(param);


    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $('#searchSurgeryBillListDiv').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            $('#searchSurgeryBillBtn').attr('disabled', true);
            $('#searchSurgeryMasterSpin').removeClass('fa fa-search');
            $('#searchSurgeryMasterSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function(html) {
            $('#searchSurgeryBillListDiv').html(html);


        },
        complete: function() {

            $('#searchSurgeryBillListDiv').LoadingOverlay("hide");
            $('#searchSurgeryBillBtn').attr('disabled', false);
            $('#searchSurgeryMasterSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchSurgeryMasterSpin').addClass('fa fa-search');
        },
        error: function() {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function addNewSurgeryBIll() {

    var url = base_url + '/surgerybilling/BillCreation';
    document.location.href = url;
}



function cancelSurgeryBIll(id) {

    bootbox.confirm({
        message: "<lable>Cancle Reason:</lable> <input id='cancel_reason'class='form-control'/>",
        buttons: {
            confirm: {
                label: "Cancel",
                className: "btn-warning",
                default: "true",
            },
            cancel: {
                label: "Revert",
                className: "btn-danger",
            },
        },
        callback: function(result) {
            if (result) {
                var url = base_url + "/surgery/surgeryBillListing"
                var param = {
                    bill_had_id: id,

                }
                console.log(param);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#searchSurgeryBillListDiv').LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#009869'
                        });
                        $('#searchSurgeryBillBtn').attr('disabled', true);
                        $('#searchSurgeryMasterSpin').removeClass('fa fa-search');
                        $('#searchSurgeryMasterSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        toastr.success('Cancelled Success Fully')


                    },
                    complete: function() {

                        $('#searchSurgeryBillListDiv').LoadingOverlay("hide");
                        $('#searchSurgeryBillBtn').attr('disabled', false);
                        $('#searchSurgeryMasterSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchSurgeryMasterSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error('Please check your internet connection and try again');
                    }
                });

            }
        },
    });



}

function resetSurgeryList() {

    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    $('#surgery_bill_no_hidden').val('');
    $('#surgery_bill_no').val('');


}