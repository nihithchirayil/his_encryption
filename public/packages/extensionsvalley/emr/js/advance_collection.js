$(document).ready(function () {
    setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
document.getElementById("date_from").blur();
document.getElementById("date_to").blur();

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getCollectionReportData() {

    var url = route_json.AdvanceCollectionData;

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'date_from') {
            filters_id = 'date_from';
        }
        if (filters_id == 'date_to') {
            filters_id = 'date_to';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }



    });

    if ($('input[name="appy_date"]:checked').length > 0) {

        var all_visit = 1;


    } else {
        var all_visit = 0;

    }
    var remaining_bal =0;
    if($("#remaining_bal").prop('checked') == true){
        remaining_bal  = 1;
    }else{
        remaining_bal = 0;

    }    
    filters_list['checkbox'] = all_visit;
    filters_list['remaining_bal'] = remaining_bal;


    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });
}

function datefilterstatus(obj) {
    if (obj.value == '1' || obj.value == '3') {
        $('#datefilterdataddiv').show();
    } else {
        $('#datefilterdataddiv').hide();
    }
}

function hideandShowDateFilters() {

    if ($('input[name="appy_date"]:checked').length > 0) {
        // $("#date_filter_div").load("#date_filter_div");

        var x = document.getElementById("checkbox");
        if (x.style.display === "none") {
            x.style.display = "block";
        }
        $('.filter_label2').show();
        $('#date_from').show();
        $('#date_to').show();

    } else {

        $('.filter_label2').hide();
        $('#date_from').hide();
        $('#date_to').hide();

        var months = ["Jan", "Feb", "March", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dece"];
        var today = new Date();
        var MM = months[today.getMonth()];
        var dd = String(today.getDate()).padStart(2, '0');
        var YYYY = today.getFullYear();
        today = MM + '-' + dd + '-' + YYYY;
        $("#date_from").val(today);
        $("#date_to").val(today);
    }

}

function datarest() {
    $('.filter_label2').hide();
    var current_date = $('#current_date').val();
    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
    $('#patient_hidden').val('');
    $('#patient').val('');
    $('#patient_type').select2('val', '');
    getCollectionReportData();

}

function advpopup(id) {
    $("#datapopup").modal('show');
    var url = route_json.popupdata;

    var id = id;
    var fromdate = document.getElementById("date_from").value;
    var todate = document.getElementById("date_to").value;
    $.ajax({
        type: "GET",
        url: url,
        data: { id1: id, fromdate: fromdate, todate: todate },
        beforeSend: function (html) {
            $('#popup').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#popup').html(html);
        },
        complete: function () {
            $('#popup').LoadingOverlay("hide");
            //     // $('#popup2').css('display', 'block');


        },

    });
}

function advance_generate_csv() {

    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");

    // $("#result_data_table").on('submit', function(){

    //     $("#csv_results").attr("disabled",true);
    // });
};
