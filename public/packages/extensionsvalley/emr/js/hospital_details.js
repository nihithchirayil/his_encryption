
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
   
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_hospital_details_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
      
        data.name = $("#name").val();
        data.code = $("#code").val();
       
              $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }
    function add() {
        $("#datapopup").modal('show');
        $(".saveButton").css("display", "block");
        $(".clearFormButton").css("display", "block");
        $("#img1").css("display","none");
        $("#img2").css("display","none");
        $("#img3").css("display","none");
        $('#hospital_logo1').hide();
        $('#nabh_logo1').hide();
        $('#report_header_img1').hide();
        $('#id').show();
        $('#id1').show();
        $('#id2').show();
        $("#name_add").prop('disabled',false);
        $("#hospital_header").prop('disabled',false);
        $("#code_add").prop('disabled',false);
        $("#state_add").prop('disabled',false);
        $("#state_add").select2().trigger('change'); 
        $("#city_add").prop('disabled',false);
        $("#address_add").prop('disabled',false);
        $("#pincode_add").prop('disabled',false);
        $("#default_renewal_validity_add").prop('disabled',false);
        $("#default_free_visits_add").prop('disabled',false);
        $("#default_free_visits_after_discharge_add").prop('disabled',false);
        $("#phone_add").prop('disabled',false);
        $("#booking_no_add").prop('disabled',false);
        $("#gst_no_add").prop('disabled',false);
        $("#tin_no_add").prop('disabled',false);
        $("#pan_no_add").prop('disabled',false);
        $("#drug_licence_no_add").prop('disabled',false);
        $("#vice_president_add").prop('disabled',false);
        $("#manage_director_add").prop('disabled',false);
        $("#country_add").prop('disabled',false);
        $(".status_add").prop('disabled',false);
        $('.titleName').html('Add New Hospital');
    }
    function view(element) {
        $("#datapopup").modal('show');
       // $(".saveButton").remove();
        $(".saveButton").css("display", "none");
        $(".clearFormButton").css("display", "none");
      //  $(".clearFormButton").remove();
      $("#img1").css("display","block");
      $("#img2").css("display","block");
      $("#img3").css("display","block");
      $('#hospital_logo1').show();
      $('#nabh_logo1').show();
      $('#report_header_img1').show();
        $('.titleName').html(' Hospital Details');

        // $('.min_sellable_div').hide();
     //   $('#datapopup')[0].reset();
        let company_id = $(element).parents('tr').attr('data-id');
        let name = $(element).parents('tr').attr('data-name');
        let hospital_header = $(element).parents('tr').attr('data-hospital_header');
        let code = $(element).parents('tr').attr('data-code');
        let city = $(element).parents('tr').attr('data-city');
        let address = $(element).parents('tr').attr('data-address');
        let pincode = $(element).parents('tr').attr('data-pincode');
        let default_renewal_validity = $(element).parents('tr').attr('data-default_renewal_validity');
        let default_free_visits = $(element).parents('tr').attr('data-default_free_visits');
        let default_free_visits_after_discharge = $(element).parents('tr').attr('data-default_free_visits_after_discharge');
        let phone = $(element).parents('tr').attr('data-phone');
        let booking_no = $(element).parents('tr').attr('data-booking_no');
        let status = $(element).parents('tr').attr('data-status');
        let gst_no = $(element).parents('tr').attr('data-gst_no');
        let tin_no = $(element).parents('tr').attr('data-tin_no');
        let pan_no = $(element).parents('tr').attr('data-pan_no');
        let drug_licence_no = $(element).parents('tr').attr('data-drug_licence_no');
        let vice_president = $(element).parents('tr').attr('data-vice_president');
        let state = $(element).parents('tr').attr('data-state');
        let manage_director = $(element).parents('tr').attr('data-manage_director');
        let country = $(element).parents('tr').attr('data-country');
// alert(effective_date);
        if(status==1){
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", true);

            } else{
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", false);
                   }
                  
      
        $(".reference_id").val(company_id);
        $("#name_add").val(name);
        $("#hospital_header").val(hospital_header);
        $("#code_add").val(code);
        $("#state_add").val(state);
        $("#state_add").select2().trigger('change'); 
        $("#city_add").val(city);
        $("#address_add").val(address);
        $("#pincode_add").val(pincode);
        $("#default_renewal_validity_add").val(default_renewal_validity);
        $("#default_free_visits_add").val(default_free_visits);
        $("#default_free_visits_after_discharge_add").val(default_free_visits_after_discharge);
        $("#phone_add").val(phone);
        $("#booking_no_add").val(booking_no);
        $("#gst_no_add").val(gst_no);
        $("#tin_no_add").val(tin_no);
        $("#pan_no_add").val(pan_no);
        $("#drug_licence_no_add").val(drug_licence_no);
        $("#vice_president_add").val(vice_president);
        $("#manage_director_add").val(manage_director);
        $("#country_add").val(country);
        $(".status_add").val(status);
        var url=$('#base_url').val();
   
        var param = {company_id:company_id};

         $.ajax({
            type: "GET",
            dataType: 'json',
            url: url + "/master/image_details",
            data:param,
        //    cache : false,
        //    processData: false,
            beforeSend: function () {
                //$(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
               // console.log(data);

            },
            success: function (data) {  
               // $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                console.log(data[0].hl);
                console.log(data[0].nl);
                console.log(data[0].rh);
                $('img').error( function() {
                    $("img").attr('alt','Image Not Found');
                    });
                $("#hospital_logo1").attr('src', 'data:image/jpg;base64, '+data[0].hl) .width(200).height(150);
                $("#nabh_logo1").attr('src', 'data:image/jpg;base64, '+data[0].nl) .width(200).height(150);
                $("#report_header_img1").attr('src', 'data:image/jpg;base64, '+data[0].rh) .width(200).height(150);
                $('#hospital_logo1').show();
                $('#nabh_logo1').show();
                $('#report_header_img1').show();
                $('#id').hide();
                $('#id1').hide();
                $('#id2').hide();
                $("#name_add").prop('disabled',true);
                $("#hospital_header").prop('disabled',true);
                $("#code_add").prop('disabled',true);
                $("#state_add").prop('disabled',true);
                $("#state_add").select2().trigger('change'); 
                $("#city_add").prop('disabled',true);
                $("#address_add").prop('disabled',true);
                $("#pincode_add").prop('disabled',true);
                $("#default_renewal_validity_add").prop('disabled',true);
                $("#default_free_visits_add").prop('disabled',true);
                $("#default_free_visits_after_discharge_add").prop('disabled',true);
                $("#phone_add").prop('disabled',true);
                $("#booking_no_add").prop('disabled',true);
                $("#gst_no_add").prop('disabled',true);
                $("#tin_no_add").prop('disabled',true);
                $("#pan_no_add").prop('disabled',true);
                $("#drug_licence_no_add").prop('disabled',true);
                $("#vice_president_add").prop('disabled',true);
                $("#manage_director_add").prop('disabled',true);
                $("#country_add").prop('disabled',true);
                $(".status_add").prop('disabled',true);
                // Command: toastr["success"]("success");
                // getFilteredItems(window.search_url);
                // resetForm();
            },
            complete: function () {

            }
        });
    }
    function saveForm(){
        // $("#datapopup").modal('show');
       // Creating object of FormData class
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($("#name_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Hospital Name");
            return;
        }
        // if($("#location_code_add").val().trim() == ''){
        //     Command: toastr["error"]("Please enter Code");
        //     return;
        // }
        // if($("#location_type_add").val().trim() == ''){
        //     Command: toastr["error"]("Please enter  Location Type");
        //     return;
        // }
        var company_id = $(".reference_id").val();
        var hospital_logo = $('#hospital_logo')[0].files;
        var nabh_logo = $('#nabh_logo')[0].files;
        var report_header_img = $('#report_header_img')[0].files;
        var hospital_header   =  $("#hospital_header").val();
        var name   =  $("#name_add").val();
         console.log(company_id );
         var code   =  $("#code_add").val();
         var state   =  $("#state_add").val();
         var city   =  $("#city_add").val();
         var address   =  $("#address_add").val();
         var pincode   =  $("#pincode_add").val();
         var default_renewal_validity   =  $("#default_renewal_validity_add").val();
         var default_free_visits   =  $("#default_free_visits_add").val();
         var default_free_visits_after_discharge   =  $("#default_free_visits_after_discharge_add").val();
         var phone   =  $("#phone_add").val();
         var booking_no   =  $("#booking_no_add").val();
         var gst_no   =  $("#gst_no_add").val();
         var tin_no   =  $("#tin_no_add").val();
         var pan_no   =  $("#pan_no_add").val();
         var drug_licence_no   =  $("#drug_licence_no_add").val();
         var vice_president   =  $("#vice_president_add").val();
         var manage_director   =  $("#manage_director_add").val();
         var country   =  $("#country_add").val();
        //  var status        =  $(".status_add").val();
        var form_data = new FormData();
        form_data.append("company_id", company_id)
        form_data.append("hospital_logo", hospital_logo[0])
        form_data.append("nabh_logo", nabh_logo[0])
        form_data.append("report_header_img", report_header_img[0])
        form_data. append("hospital_header", hospital_header)                                   
        form_data. append("name", name)                                   
        form_data.append("code", code)
        form_data. append("state", state)
        form_data.append("city", city)
        form_data.append("address", address)
        form_data.append("pincode", pincode)
        form_data.append("default_renewal_validity", default_renewal_validity)
        form_data.append("default_free_visits", default_free_visits)
        form_data.append("default_free_visits_after_discharge", default_free_visits_after_discharge)
        form_data.append("phone", phone)
        form_data.append("booking_no", booking_no)
        form_data.append("gst_no", gst_no)
        form_data.append("tin_no", tin_no)
        form_data.append("pan_no", pan_no)
        form_data. append("drug_licence_no", drug_licence_no) 
        form_data.append("vice_president", vice_president)
        form_data. append("manage_director", manage_director) 
        form_data.append("country", country)
// alert(form_data.allow_pay_from_cash);
        var status=1;
        if($(".status_add").prop('checked') == true){
            // alert("chcked");
             status =1;
        }else{
             status =0;
        }
        form_data. append("status", status)

        $.ajax({
            type: "POST",
            url: url,
            // dataType: 'json',
            cache: false,
            data: form_data, // Setting the data attribute of ajax with file_data
            contentType: false,
            processData: false,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"](" Code Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }
                  
                   else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    $("#datapopup").modal('hide');
                   
                  //  getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
    function previewFile(input, id) {
        $("#img1").css("display","block");
        $('#hospital_logo1').show();
        id = id || '#hospital_logo1';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
     
            reader.onload = function (e) {
                $(id)
                        .attr('src', e.target.result)
                        .width(200)
                        .height(150);
            };
     
            reader.readAsDataURL(input.files[0]);
        }
     }
     function previewFile1(input, id) {
        $("#img2").css("display","block");
        $('#nabh_logo1').show();
        id = id || '#nabh_logo1';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
     
            reader.onload = function (e) {
                $(id)
                        .attr('src', e.target.result)
                        .width(200)
                        .height(150);
            };
     
            reader.readAsDataURL(input.files[0]);
        }
     }
     function previewFile2(input, id) {
        $("#img3").css("display","block");
        $('#report_header_img1').show();
        id = id || '#report_header_img1';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
     
            reader.onload = function (e) {
                $(id)
                        .attr('src', e.target.result)
                        .width(200)
                        .height(150);
            };
     
            reader.readAsDataURL(input.files[0]);
        }
     }
 
    function editItem(element){
      
        $("#datapopup").modal('show');
        $(".saveButton").css("display", "block");
        $(".clearFormButton").css("display", "block");
        $("#img1").css("display","block");
        $("#img2").css("display","block");
        $("#img3").css("display","block");
        $('#hospital_logo1').show();
        $('#nabh_logo1').show();
        $('#report_header_img1').show();
        let company_id = $(element).parents('tr').attr('data-id');
        let name = $(element).parents('tr').attr('data-name');
        let hospital_header = $(element).parents('tr').attr('data-hospital_header');
        let code = $(element).parents('tr').attr('data-code');
        let city = $(element).parents('tr').attr('data-city');
        let address = $(element).parents('tr').attr('data-address');
        let pincode = $(element).parents('tr').attr('data-pincode');
        let default_renewal_validity = $(element).parents('tr').attr('data-default_renewal_validity');
        let default_free_visits = $(element).parents('tr').attr('data-default_free_visits');
        let default_free_visits_after_discharge = $(element).parents('tr').attr('data-default_free_visits_after_discharge');
        let phone = $(element).parents('tr').attr('data-phone');
        let booking_no = $(element).parents('tr').attr('data-booking_no');
        let status = $(element).parents('tr').attr('data-status');
        let gst_no = $(element).parents('tr').attr('data-gst_no');
        let tin_no = $(element).parents('tr').attr('data-tin_no');
        let pan_no = $(element).parents('tr').attr('data-pan_no');
        let drug_licence_no = $(element).parents('tr').attr('data-drug_licence_no');
        let vice_president = $(element).parents('tr').attr('data-vice_president');
        let state = $(element).parents('tr').attr('data-state');
        let manage_director = $(element).parents('tr').attr('data-manage_director');
        let country = $(element).parents('tr').attr('data-country');
// alert(effective_date);
        if(status==1){
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", true);

            } else{
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", false);
                   }
                  
                   var data = {};
        $(".reference_id").val(company_id);
       console.log(company_id);
       $("#hospital_header").val(hospital_header);
        $("#name_add").val(name);
        $("#code_add").val(code);
        $("#state_add").val(state);
        $("#state_add").select2().trigger('change'); 
        $("#city_add").val(city);
        $("#address_add").val(address);
        $("#pincode_add").val(pincode);
        $("#default_renewal_validity_add").val(default_renewal_validity);
        $("#default_free_visits_add").val(default_free_visits);
        $("#default_free_visits_after_discharge_add").val(default_free_visits_after_discharge);
        $("#phone_add").val(phone);
        $("#booking_no_add").val(booking_no);
        $("#gst_no_add").val(gst_no);
        $("#tin_no_add").val(tin_no);
        $("#pan_no_add").val(pan_no);
        $("#drug_licence_no_add").val(drug_licence_no);
        $("#vice_president_add").val(vice_president);
        $("#manage_director_add").val(manage_director);
        $("#country_add").val(country);
        $(".status_add").val(status);
      
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $(".titleName").html('Edit Hospital Details');
      //  var url = route_json.image_details;
      var url=$('#base_url').val();
   
        var param = {company_id:company_id};

         $.ajax({
            type: "GET",
            dataType: 'json',
            url: url + "/master/image_details",
            data:param,
        //    cache : false,
        //    processData: false,
            beforeSend: function () {
                //$(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
               // console.log(data);

            },
            success: function (data) {  
               // $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                console.log(data[0].hl);
                console.log(data[0].nl);
                console.log(data[0].rh);
                $('img').error( function() {
                    $("img").attr('alt','Image Not Found');
                    });
          
                $("#hospital_logo1").attr('src', 'data:image/jpeg;base64, '+data[0].hl) .width(200).height(150);
                $("#nabh_logo1").attr('src', 'data:image/jpeg;base64, '+data[0].nl) .width(200).height(150);
                $("#report_header_img1").attr('src', 'data:image/jpeg;base64, '+data[0].rh) .width(200).height(150);
                $('#hospital_logo1').show();
                $('#nabh_logo1').show();
                $('#report_header_img1').show();
                $('#id').show();
                $('#id1').show();
                $('#id2').show();
                $("#name_add").prop('disabled',false);
                $("#hospital_header").prop('disabled',false);
                $("#code_add").prop('disabled',false);
                $("#state_add").prop('disabled',false);
                $("#state_add").select2().trigger('change'); 
                $("#city_add").prop('disabled',false);
                $("#address_add").prop('disabled',false);
                $("#pincode_add").prop('disabled',false);
                $("#default_renewal_validity_add").prop('disabled',false);
                $("#default_free_visits_add").prop('disabled',false);
                $("#default_free_visits_after_discharge_add").prop('disabled',false);
                $("#phone_add").prop('disabled',false);
                $("#booking_no_add").prop('disabled',false);
                $("#gst_no_add").prop('disabled',false);
                $("#tin_no_add").prop('disabled',false);
                $("#pan_no_add").prop('disabled',false);
                $("#drug_licence_no_add").prop('disabled',false);
                $("#vice_president_add").prop('disabled',false);
                $("#manage_director_add").prop('disabled',false);
                $("#country_add").prop('disabled',false);
                $(".status_add").prop('disabled',false);
                // Command: toastr["success"]("success");
                // getFilteredItems(window.search_url);
                // resetForm();
            },
            complete: function () {

            }
        });

    }
   
  
    function resetForm(){
        $(".reference_id").val('');
        $("#hospital_logo").val('');
        $("#nabh_logo").val('');
        $("#report_header_img").val('');
        $("#img1").css("display","none");
        $("#img2").css("display","none");
        $("#img3").css("display","none");
        $('#hospital_logo1').hide();
        $('#nabh_logo1').hide();
        $('#report_header_img1').hide();
        $("#state_add").val('').trigger('change');
        $("#name_add").val('');
        $("#hospital_header").val('');
        $("#code_add").val('');
        $("#city_add").val('');
        $("#address_add").val('');
        $("#pincode_add").val('');
        $("#default_renewal_validity_add").val('');
        $("#default_free_visits_add").val('');
        $("#default_free_visits_after_discharge_add").val('');
        $("#phone_add").val('');
        $("#booking_no_add").val('');
        $("#gst_no_add").val('');
        $("#tin_no_add").val('');
        $("#pan_no_add").val('');
        $("#drug_licence_no_add").val('');
        $("#vice_president_add").val('');
        $("#manage_director_add").val('');
        $("#country_add").val('');
        $('#status_add').removeAttr('checked');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }
    function reset1(){
        $(".reference_id").val('');
        $("#hospital_logo1").attr("src","");
        $("#nabh_logo1").attr("src","");
        $("#report_header_img1").attr("src","");
        $("#state_add").val('').trigger('change');
        $("#hospital_logo").val('');
        $("#nabh_logo").val('');
        $("#report_header_img").val('');
        $("#name_add").val('');
        $("#hospital_header").val('');
        $("#code_add").val('');
        $("#city_add").val('');
        $("#address_add").val('');
        $("#pincode_add").val('');
        $("#default_renewal_validity_add").val('');
        $("#default_free_visits_add").val('');
        $("#default_free_visits_after_discharge_add").val('');
        $("#phone_add").val('');
        $("#booking_no_add").val('');
        $("#gst_no_add").val('');
        $("#tin_no_add").val('');
        $("#pan_no_add").val('');
        $("#drug_licence_no_add").val('');
        $("#vice_president_add").val('');
        $("#manage_director_add").val('');
        $("#country_add").val('');
        $('#status_add').removeAttr('checked');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }
