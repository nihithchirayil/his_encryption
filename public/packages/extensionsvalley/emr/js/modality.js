var limit = cur_limit = 20;
var offset = 0;
var total_rec = 0;
var url = $('#base_url').val();
var token = $('#c_token').val();
$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    setTimeout(function () {
        searchList(15, 0);
    }, 300);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});

function searchList(limit = 15, offset = 0) {
    var send_url = url + "/modality/modalityList";
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var patient_name = $("#patient_name").val();
    var patient_name_hidden = $("#patient_name_hidden").val();
    var service_name = $("#service_desc").val();
    var service_id = $("#service_desc_hidden").val();
    var modality_name = $("#modality_desc").val();
    var modality_code = $("#modality_desc_hidden").val();
    var status_filter = $("#status_filter").val();
    var params = {_token: token, from_date: from_date, to_date: to_date, patient_name: patient_name,
        patient_name_hidden: patient_name_hidden, service_name: service_name, service_id: service_id,status_filter:status_filter,
        search_data: 1,modality_name:modality_name,modality_code:modality_code, limit: limit, offset: offset};
    $.ajax({
        type: "POST",
        url: send_url,
        data: params,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner');
            $('#modality_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (msg) {
            var obj = JSON.parse(msg);
            msg = obj.viewData;
            total_rec = obj.total_rec;
            if (offset == 0) {
                $('#modality_list_div').html(msg);
            } else {
                $('#receipt_table1').append(msg);
            }
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner');
            $('#searchdataspin').addClass('fa fa-search');
            $('#modality_list_div').LoadingOverlay("hide");

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}

function updatePacsStatus(e, row_id, status_update, current_status,is_popup,is_revert) {
    if(is_popup == '3'){
        ImageModal(e,row_id, status_update, is_revert, cls_name);
        return;
    }
    if(is_popup == '4'){
        ReportModal(e,row_id, status_update, is_revert, cls_name);
        return;
    }
    var send_url = url + "/modality/updateModalityStatus";
    var params = {_token: token, row_id: row_id, status_update: status_update, is_revert: is_revert};
    var cls_name = ($('#pc_master_' + status_update + '' + row_id).attr('class'));
    if (is_revert == 1) {
        if(status_update != (current_status)){
            toastr.error('Plese revert in order wise');return false;
        }
    }
    if (is_revert == 0) {
        if(status_update != (parseInt(current_status) + 1)){
            toastr.error('Plese update in order wise');return false;
        }
    }
    if (is_popup == '1') {
        checkListModal(row_id, status_update, is_revert, cls_name);
        return;
    }
    if(is_popup == '2'){
        TimeModal(row_id, status_update, is_revert, cls_name);
        return;
    }

    cnfirm = true;
    if (is_revert == 1) {
        var cnfirm = confirm("Are you sure you want to revert ?");
    }
    if (cnfirm) {
        $.ajax({
            type: "POST",
            url: send_url,
            data: params,
            beforeSend: function () {
                $('.btn_disable').attr('disabled', true);
                $('#pc_master_' + status_update + '' + row_id).removeClass(cls_name);
                $('#pc_master_' + status_update + '' + row_id).addClass('fa fa-spinner fa-pulse');

            },
            success: function (msg) {
                var obj = JSON.parse(msg);
                var msg = obj.msg;
                var status = obj.status;
                if (status == 1) {
                    toastr.success(msg);
                } else {
                    toastr.error(msg);
                }
            },
            complete: function () {
                $('.btn_disable').attr('disabled', false);
                $('#pc_master_' + status_update + '' + row_id).removeClass('fa fa-spinner fa-pulse');
                $('#pc_master_' + status_update + '' + row_id).addClass(cls_name);
                setTimeout(function () {
                    searchList(cur_limit, 0);
                }, 500);

                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#modality_list_div').LoadingOverlay("hide");


                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');


            }, error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });
    }
}
function checkListModal(row_id, status_update, is_revert, cls_name) {
    $.ajax({
        type: "POST",
        url: url + "/modality/pacsCheckList",
        data: {_token: token, row_id: row_id, status_update: status_update},
        beforeSend: function () {
            $('.btn_disable').attr('disabled', true);
            $('#pc_master_' + status_update + '' + row_id).removeClass(cls_name);
            $('#pc_master_' + status_update + '' + row_id).addClass('fa fa-spinner fa-pulse');
        },
        success: function (data) {
            if (data) {
                $("#append_check_list").html(data);
                $("#check_list_modal").modal('show');
            }
        },
        complete: function () {
            $('.btn_disable').attr('disabled', false);
            $('#pc_master_' + status_update + '' + row_id).removeClass('fa fa-spinner fa-pulse');
            $('#pc_master_' + status_update + '' + row_id).addClass(cls_name);
            setTimeout(function () {
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            }, 300);
        }
    });
}
function saveCheckList(row_id, status_update, is_revert) {
    var chklist = []; var nachklist = [];
    $(".chklist").each(function () {
        if ($(this).prop('checked')) {
            chklist.push($(this).val());
        }
    });
    $(".nachklist").each(function () {
        if ($(this).prop('checked')) {
            nachklist.push($(this).val());
        }
    });
    var chekbox_length = $('input[name="check_list[]"]').length;
    if($(nachklist).not(chklist).length === 0 && $(chklist).not(nachklist).length === 0 && is_revert == 0){
       alert("Please check one entry for one row");
        return false;
    }
    if(chekbox_length != (parseInt(nachklist.length)+parseInt(chklist.length)) && is_revert == 0){
         alert("Please check one entry for one row");
        return false;
    }
    $.ajax({
        type: "POST",
        url: url + "/modality/saveCheckList",
        data: {_token: token, row_id: row_id, status_update: status_update, chklist: chklist, na_chklist: nachklist, is_revert: is_revert},
        beforeSend: function () {
            $('.chkbtncls').attr('disabled', true);
            if (is_revert == 1) {
                $('#revertcheckspin').removeClass('fa fa-save');
                $('#revertcheckspin').addClass('fa fa-spinner fa-pulse');
            } else {
                $('#savecheckdataspin').removeClass('fa fa-save');
                $('#savecheckdataspin').addClass('fa fa-spinner fa-pulse');
            }
        },
        success: function (msg) {
            if (msg) {
                var obj = JSON.parse(msg);
                var msg = obj.msg;
                var status = obj.status;
                if (status == 1) {
                    toastr.success(msg);
                    $("#check_list_modal").modal('hide');
                    setTimeout(function () {
                        searchList(cur_limit, 0);
                    }, 500);
                } else {
                    toastr.error(msg);
                }
            }
        },
        complete: function () {
            $('.chkbtncls').attr('disabled', false);
            if (is_revert == 1) {
                $('#revertcheckspin').removeClass('fa fa-spinner fa-pulse');
                $('#revertcheckspin').addClass('fa fa-save');
            } else {
                $('#savecheckdataspin').removeClass('fa fa-spinner fa-pulse');
                $('#savecheckdataspin').addClass('fa fa-save');
            }

        }
    });
}
function progressiveSearchFn(patient) {

        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var op_no_search = $('#' + patient).val();
            op_search = op_no_search.trim();
            if (op_search == "") {
                $("#" + patient + "AjaxDiv").html("");
                $("#" + patient + "_hidden").val("");
            } else {
                try {
                    var send_url = url + "/modality/modalityList";
                    var param = {search_string: op_no_search, op_no_search_prog: 1, ident: patient};
                    $.ajax({
                        type: "GET",
                        url: send_url,
                        data: param,
                        beforeSend: function () {
                            $("#" + patient + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                        },
                        success: function (html) {
                            $("#" + patient + "AjaxDiv").html(html).show();
                            $("#" + patient + "AjaxDiv").find('li').first().addClass('liHover');
                        },
                        complete: function () {
                        }
                    });
                } catch (err) {
                    document.getElementById("demo").innerHTML = err.message;
                }
            }
        } else {
            ajaxProgressiveKeyUpDown(patient + "AjaxDiv", event);
        }
}
function fillSearchDetials(id, uhid, patient_name, input_id) {
    $('#' + input_id + '_hidden').val(id);
    $('#'+input_id+'_uhid_hidden').val(uhid);
    $('#' + input_id).val(patient_name);
    $("#" + input_id + "AjaxDiv").hide();
}
function fillServiceDescDetails(service_desc, id, input_id) {
    $('#' + input_id + '_hidden').val(id);
    $('#' + input_id).val(service_desc);
    $('#' + input_id + 'AjaxDiv').hide();
}
function fillDoctorDetails(doctor_name,id,input_id){
    $('#' + input_id + '_hidden').val(id);
    $('#' + input_id).val(doctor_name);
    $('#' + input_id + 'AjaxDiv').hide();
}
function fillModalityDetails(name,code,input_id){
    $('#' + input_id + '_hidden').val(code);
    $('#' + input_id).val(name);
    $('#' + input_id + 'AjaxDiv').hide();
}
function addNewPatient() {
    try {
        var send_url = url + "/modality/searchPatientList";
        var param = {search_patient: 1};
        $.ajax({
            type: "GET",
            url: send_url,
            data: param,
            beforeSend: function () {
                $("#add_new_patient").prop('disabled', true);
                $("#add_new_patient_spin").removeClass('fa fa-plus');
                $("#add_new_patient_spin").addClass('fa fa-spinner fa-pulse');
            },
            success: function (data) {
                if (data) {
                    $("#pat_name_header").html("Add new patient");
                    $("#append_pat_modal").html(data);
                    $("#dialog_modal").css('width','80%');
                    $("#modalbody").css('height','300px');
                    $("#add_new_pat_modal").modal('show');
                }
                $("#add_new_patient_spin").removeClass('fa fa-spinner fa-pulse');
                $("#add_new_patient_spin").addClass('fa fa-plus');
                $("#add_new_patient").prop('disabled', false);
            },
            complete: function () {
            }
        });
    } catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }
}
function saveNewPatient(){
     var patient_id = $('#new_patient_name_hidden').val();
     var serviec_id = $('#new_service_desc_hidden').val();
     var uhid =  $('#new_patient_name_uhid_hidden').val();
     var patient_name = $('#new_patient_name_hidden').val();
     var serviec_name = $('#new_service_desc_hidden').val();
     if(patient_name == '' || patient_id == '' || uhid == ''){
          toastr.error('Patient name is required');
          return false;
     }
     if(serviec_name == '' || serviec_id == ''){
          toastr.error('Service name is required');
          return false;
     }

     var param = {patient_id:patient_id,serviec_id:serviec_id,uhid:uhid};
      try {
        var send_url = url + "/modality/saveNewPatient";
        $.ajax({
            type: "GET",
            url: send_url,
            data: param,
            beforeSend: function () {
                $("#savenewpatientbtn").prop('disabled', true);
                $("#savenewpatientbtnspin").removeClass('fa fa-plus');
                $("#savenewpatientbtnspin").addClass('fa fa-spinner fa-pulse');
            },
            success: function (data) {
                if (data == '1') {
                    toastr.success('Successfully added');
                    $("#add_new_pat_modal").modal('hide');
                }
                $("#savenewpatientbtn").prop('disabled', false);
                $("#savenewpatientbtnspin").removeClass('fa fa-spinner fa-pulse');
                $("#savenewpatientbtnspin").addClass('fa fa-plus');
            },
            complete: function () {
                searchList(15,0);
            }
        });
    } catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }
}
function ImageModal(e,row_id, status_update, is_revert, cls_name){
    let pacs_viewer_prefix = $('#pacs_viewer_prefix').val();
    var accession_no = $(e).attr('data-accession-no');
    if (pacs_viewer_prefix && accession_no) {
        $("#pacs_viewer_iframe").attr('src', pacs_viewer_prefix+accession_no);
    }
    $("#radiology_pacs_viewer_iframe_modal").modal('show');
}
function ReportModal(e,row_id, status_update, is_revert, cls_name){
    let pacs_report_prefix = $('#pacs_report_prefix').val();
    var accession_no = $(e).attr('data-accession-no');
    window.open(pacs_report_prefix+accession_no, '_blank');
}
function showPacsStatus(e,row_id){
     var param = {row_id:row_id};
      try {
        var send_url = url + "/modality/showStaus";
        $.ajax({
            type: "GET",
            url: send_url,
            data: param,
            beforeSend: function () {
                $(".btn_disable").prop('disabled', true);
                $("#statusspin_"+row_id).removeClass('fa fa-info');
                $("#statusspin_"+row_id).addClass('fa fa-spinner fa-pulse');
            },
            success: function (data) {
                $("#pat_name_header").html("Status list");
                $("#append_pat_modal").html(data);
                $("#dialog_modal").css('width','40%');
                $("#modalbody").css('height','500px');
                $("#add_new_pat_modal").modal('show');
                $(".btn_disable").prop('disabled', false);
                $("#statusspin_"+row_id).removeClass('fa fa-spinner fa-pulse');
                $("#statusspin_"+row_id).addClass('fa fa-info');
            },
            complete: function () {
            }
        });
    } catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }
}
function TimeModal(row_id, status_update, is_revert, cls_name) {
    var send_url = url + "/modality/timeListModal";
    $.ajax({
        type: "POST",
        url: send_url,
        data: {_token: token, row_id: row_id, status_update: status_update},
        beforeSend: function () {
            $('.btn_disable').attr('disabled', true);
            $('#pc_master_' + status_update + '' + row_id).removeClass(cls_name);
            $('#pc_master_' + status_update + '' + row_id).addClass('fa fa-spinner fa-pulse');
        },
        success: function (data) {
            if (data) {
                $("#append_time_list").html(data);
                $("#time_list_modal").modal('show');
            }
        },
        complete: function () {
            $('.btn_disable').attr('disabled', false);
            $('#pc_master_' + status_update + '' + row_id).removeClass('fa fa-spinner fa-pulse');
            $('#pc_master_' + status_update + '' + row_id).addClass(cls_name);

        }
    });
}
function saveDateTime(row_id,status_update,is_revert){
    var updated_time = $("#date_time").val();
    var dt = moment(updated_time, ["h:mm:ss A"]).format("HH:mm:ss");
    var param = {row_id:row_id,status_update:status_update,updated_time:dt,save_new_time:1,is_revert:is_revert};
      try {
        var send_url = url + "/modality/timeListModal";
        $.ajax({
            type: "GET",
            url: send_url,
            data: param,
            beforeSend: function () {
                $(".time_btn").prop('disabled', true);
            },
            success: function (msg) {
                var obj = JSON.parse(msg);
                var msg = obj.msg;
                var status = obj.status;
                if (status == '1') {
                    toastr.success(msg);
                    $("#time_list_modal").modal('hide');
                }
                $(".time_btn").prop('disabled', false);
            },
            complete: function () {
                searchList(15,0);
            }
        });
    } catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }
}
function showPacsHistory(e,row_id){
     var param = {row_id:row_id};
      try {
        var send_url = url + "/modality/showHistory";
        $.ajax({
            type: "GET",
            url: send_url,
            data: param,
            beforeSend: function () {
                $(".btn_disable").prop('disabled', true);
                $("#historyspin_"+row_id).removeClass('fa fa-history');
                $("#historyspin_"+row_id).addClass('fa fa-spinner fa-pulse');
            },
            success: function (data) {
                $("#pat_name_header").html("History");
                $("#append_pat_modal").html(data);
                $("#dialog_modal").css('width','60%');
                $("#modalbody").css('height','500px');
                $("#add_new_pat_modal").modal('show');
                $(".btn_disable").prop('disabled', false);
                $("#historyspin_"+row_id).removeClass('fa fa-spinner fa-pulse');
                $("#historyspin_"+row_id).addClass('fa fa-history');
                setTimeout(function () {
                $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                },300);
            },
            complete: function () {
            }
        });
    } catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }

}
function RevertAll(e,id){
    var send_url = url + "/modality/updateModalityStatus";
    var params = {_token: token, row_ids: id, revert_all: 1};
    cnfirm = true;
    var cnfirm = confirm("Are you sure you want to revert ?");
    if (cnfirm) {
        $.ajax({
            type: "POST",
            url: send_url,
            data: params,
            beforeSend: function () {
                $('.btn_disable').attr('disabled', true);
                $('#revertspin_'+id).removeClass('fa fa-trash-o');
                $('#revertspin_'+id).addClass('fa fa-spinner fa-pulse');

            },
            success: function (msg) {
                var obj = JSON.parse(msg);
                var msg = obj.msg;
                var status = obj.status;
                if (status == 1) {
                    toastr.success(msg);
                } else {
                    toastr.error(msg);
                }
            },
            complete: function () {
                $('.btn_disable').attr('disabled', false);
                $('#revertspin_'+id).removeClass('fa fa-spinner fa-pulse');
                $('#revertspin_'+id).addClass('fa fa-trash-o');
                setTimeout(function () {
                    searchList(cur_limit, 0);
                }, 500);

                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#modality_list_div').LoadingOverlay("hide");


                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');


            }, error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });
    }
}
function getStatusFilter(status){
    $("#status_filter").val(status);
    $("#status_filter").select2().trigger('change');
    $("#searchdatabtn").trigger('click');
}
function serviceBill(e,id){
    var send_url = url + "/service/serviceBilling/0/"+id;
    window.open(send_url, '_blank');
}

// function fillServiceDescDetails(service_desc, id) {
//     $('#service_desc_hidden').val(id);
//     $('#service_desc').val(service_desc);
//     $('#ServiceAjaxDiv').hide();
// }
