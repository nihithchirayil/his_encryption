$(document).ready(function () {
    base_url = $('#base_url').val();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30

    });
    setTimeout(function () {
        $('.theadfix_wrapper').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
    }, 400);
    firstBtnClick();
    setInterval(blink_text, 700);
    tinyMceCreate('vendor_notes', 300, 0);
});

base_url = '';
vendor_array = {};
remarks_data = {};

function firstBtnClick() {
    updateEditDetalis();
    setTimeout(function () {
        if ($('.additemPurchaseDetails').attr('id')) {
            var btn = $('.additemPurchaseDetails').attr('id');
            $('#' + btn).trigger('click');
        }
    }, 1500);
}

function blink_text() {
    $('.closing_date').fadeOut(300);
    $('.closing_date').fadeIn(300);
}

function tinyMceCreate(text_area, tinymce_height, read_only) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        imagetools_cors_hosts: ['picsum.photos'],
        browser_spellcheck: false,
        autosave_ask_before_unload: false,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: false,
        contextmenu: false,
        toolbar: false,
        menubar: false,
        importcss_append: false,
        height: 400,
        image_caption: false,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
        readonly: read_only,
        init_instance_callback: function (editor) {
            editor.on('blur', function (e) {
                getitemRemarks();
            });
        }
    });
}

function validateText() {
    var row_id = $('#rowid_hidden').val();
    var rate = $('#itemsellingprice' + row_id).val();
    if (rate == 0) {
        $(".td_common_numeric_rules").attr('disabled', true);
        $('.selling_price').attr('disabled', false);
    } else {
        $(".td_common_numeric_rules").attr('disabled', false);
        $(".td_common_numeric_rules").each(function (index) {
            var flag = 0;
            var val = $(this).val();
            if (isNaN(val)) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            } else if (val == 'Infinity') {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            } else if (!val) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            }
            if ($(this).hasClass("percentage_class")) {
                if (parseFloat(val) > 100) {
                    $(this).val(0);
                    $(this).blur();
                    flag = 1;
                }
            }
            if (flag == 0) {
                val = parseFloat(val).toFixed(2);
                $(this).val(val);
            }
        });
    }
}

function setunitValues(row_id, item_id) {
    tinymce.get('vendor_notes').setContent('');
    if (typeof vendor_array[row_id] != "undefined") {
        if (vendor_array[row_id]['unit_cost']) {
            $('#itemUnitCost').val(vendor_array[row_id]['unit_cost']);
        }
        if (vendor_array[row_id]['totalRate']) {
            $('#itemTotalRate').val(vendor_array[row_id]['totalRate']);
        }
        if (vendor_array[row_id]['netRate']) {
            $('#itemNetRate').val(vendor_array[row_id]['netRate']);
        }
    }
    if (typeof remarks_data[item_id] != "undefined") {
        var remarks = decodeURIComponent(remarks_data[item_id]);
        if (remarks) {
            remarks = JSON.parse(remarks);
            tinymce.get('vendor_notes').setContent(remarks);
        }
    }
}

function getitemRemarks() {
    var item_id = $('#itemid_hidden').val();
    var tinymce_notes = tinymce.get('vendor_notes').getContent();
    var remarks = JSON.stringify(tinymce_notes);
    remarks = encodeURIComponent(remarks);
    remarks_data[item_id] = remarks;
}

function updateEditDetalis() {
    vendor_array = {};
    var vendor_details = $('#vendor_status_hidden').val();
    var obj = JSON.parse(atob(vendor_details));
    if (obj.length != 0) {
        var head_array = obj['head'];
        $('#totalbill_discountAmount').val(Math.round(head_array.total_bill_discount * 100) / 100);
        var amount = (100 * parseFloat(head_array.total_bill_discount)) / parseFloat(head_array.net_total_bill);
        $('#totalbill_discountPercentage').val(Math.round(amount * 100) / 100);
        $('#head_id_hidden').val(head_array.head_id);
        $('#quotation_vendor_title').html('Quotation No. : ' + head_array.quotation_no);

        var detail_array = obj['detail'];
        var charge_array = obj['charge'];
        row_id = 1;
        $.each(detail_array, function (key, value) {
            var cess_per = 0.0;
            var cess_value = 0.0;
            if (charge_array[key]['CESS']) {
                cess_per = charge_array[key]['CESS']['perc'];
                cess_value = charge_array[key]['CESS']['value'];
            }
            var cgst_per = 0.0;
            var cgst_value = 0.0;
            if (charge_array[key]['CGST']) {
                cgst_per = charge_array[key]['CGST']['perc'];
                cgst_value = charge_array[key]['CGST']['value'];
            }
            var sgst_per = 0.0;
            var sgst_value = 0.0;
            if (charge_array[key]['SGST']) {
                sgst_per = charge_array[key]['SGST']['perc'];
                sgst_value = charge_array[key]['SGST']['value'];
            }
            var othchg_per = 0.0;
            var othchg_value = 0.0;
            if (charge_array[key]['OTHCHG']) {
                othchg_per = charge_array[key]['OTHCHG']['perc'];
                othchg_value = charge_array[key]['OTHCHG']['value'];
            }
            var frchg_per = 0.0;
            var frchg_value = 0.0;
            if (charge_array[key]['FRCHG']) {
                frchg_per = charge_array[key]['FRCHG']['perc'];
                frchg_value = charge_array[key]['FRCHG']['value'];
            }
            var igst_per = 0.0;
            var igst_value = 0.0;
            if (charge_array[key]['IGST']) {
                igst_per = charge_array[key]['IGST']['perc'];
                igst_value = charge_array[key]['IGST']['value'];
            }
            var itemdisc_per = 0.0;
            var itemdisc_value = 0.0;
            if (charge_array[key]['ITEMDISC']) {
                itemdisc_per = charge_array[key]['ITEMDISC']['perc'];
                itemdisc_value = charge_array[key]['ITEMDISC']['value'];
            }

            var tot_tax = parseFloat(cgst_value) + parseFloat(sgst_value) + parseFloat(igst_value) + parseFloat(cess_value);
            $('#item_tax_amt' + row_id).val(tot_tax);
            $('#item_dicount_amt' + row_id).val(itemdisc_value);

            var remarks = JSON.stringify(value.item_remarks);
            remarks = encodeURIComponent(remarks);

            vendor_array[row_id] = {
                detail_id: parseInt(key), item_id: value.item_id,
                free_item: value.free_qty, mrp: value.item_mrp, item_rate: value.item_rate,
                unit_cost: value.unit_cost, totalRate: value.item_total_rate, netRate: value.item_net_rate,
                remarks: remarks, cess_per: cess_per, cess_amt: cess_value, cgst_per: cgst_per,
                cgst_amt: cgst_value, sgst_per: sgst_per, sgst_amt: sgst_value, igst_per: igst_per, igst_amt: igst_value,
                discount_per: itemdisc_per, discount_amt: itemdisc_value, othercharge_per: othchg_per, othercharge_amt: othchg_value,
                flightcharge_per: frchg_per, flightcharge_amt: frchg_value
            }
            remarks_data[value.item_id] = remarks;
            row_id++;
        });
    }
    validateText();
}


function addItemDetails(row_id, item_id, detail_id) {
    var vendor_string = JSON.stringify(vendor_array);
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/addQuotationitems";
    var item_desc = $('#item_desc_' + row_id).val();
    var uom = $('#uom_select_id_' + row_id).val();
    var qty = $('#request_qty' + row_id).val();
    setunitValues(row_id, item_id);
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, vendor_string: vendor_string, row_id: row_id, item_id: item_id, uom: uom, qty: qty, detail_id: detail_id },
        beforeSend: function () {
            $('#item_descheader').html(item_desc);
            $("#model_remarks_div").hide();
            $("#model_charges_div").html('');
            if ($('#editItemdataBtn')) {
                $('#editItemdataBtn').attr('disabled', true);
            }
            $("#model_charges_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("#model_charges_div").html(data);
            $('.select2').select2();
        },
        complete: function () {
            $("#model_charges_div").LoadingOverlay("hide");
            $('.addItemData').removeClass('selected_rowclass');
            $('.item_selection_' + row_id).addClass('selected_rowclass');
            if ($('#editItemdataBtn')) {
                $('#editItemdataBtn').attr('disabled', false);
            }
            $("#model_remarks_div").show();
            taxCalculation();
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function taxCalculation() {
    var row_id = $('#rowid_hidden').val();
    var itemid = $('#itemid_hidden').val();
    var detail_id = $('#detail_id_hidden').val();
    var req_qty = $('#item_quantity' + row_id).val();
    var free_qty = $('#free_item' + row_id).val();
    var qty = parseInt(req_qty) + parseInt(free_qty);
    var item_price = $('#itemsellingprice' + row_id).val();
    var tot_rate = qty * item_price;
    var tot_amt = 0.0;
    $('#itemTotalRate').val();
    $('.nummeric_class').each(function () {
        gst_amt = $(this).val();
        item_type = $(this).attr("data-code");
        if (item_type == 'GA') {
            tot_amt -= parseFloat(gst_amt);
        } else {
            tot_amt += parseFloat(gst_amt);
        }
    });
    var tot_rate = req_qty * item_price;
    var net_rate = tot_rate + tot_amt;
    $('#itemTotalRate').val(tot_rate);
    $('#itemNetRate').val(Math.round(net_rate * 100) / 100);
    var unit_cost = parseFloat(net_rate) / qty;
    $('#itemUnitCost').val(Math.round(unit_cost * 100) / 100);
    addItemVendorDetails(row_id, itemid, detail_id);
    validateText();
}

function calculateAmount(charge_id, from_type) {
    var gst_amt = 0.0;
    var amount = 0.0;
    var row_id = $('#rowid_hidden').val();
    var item_type = $('#charge_percentage' + charge_id).attr("data-code");
    var qty = $('#item_quantity' + row_id).val();
    var item_price = $('#itemsellingprice' + row_id).val();
    var tot_rate = qty * item_price;

    gst_amt = $('#charge_percentage1').val();
    amount = (gst_amt * tot_rate) / 100;
    if (amount === Infinity) {
        amount = 0;
    }
    $('#charge_amount1').val(amount);
    gst_amt = $('#charge_amount1').val();
    amount = (100 * gst_amt) / tot_rate;
    if (amount === Infinity) {
        amount = 0;
    }
    if (item_type == 'OTHCH') {
        $('#charge_percentage1').val(Math.round(amount * 100) / 100);
    } else {
        $('#charge_percentage1').val(amount);
    }

    discount_amt = $('#charge_amount1').val();
    without_discount = parseFloat(tot_rate) - parseFloat(discount_amt);

    gst_amt = $('#charge_percentage9').val();
    amount = (gst_amt * without_discount) / 100;
    if (amount === Infinity) {
        amount = 0;
    }
    $('#charge_amount9').val(amount);

    gst_amt = $('#charge_percentage10').val();
    amount = (gst_amt * without_discount) / 100;
    if (amount === Infinity) {
        amount = 0;
    }
    $('#charge_amount10').val(amount);

    gst_amt = $('#charge_percentage12').val();
    amount = (gst_amt * without_discount) / 100;
    if (amount === Infinity) {
        amount = 0;
    }
    $('#charge_amount12').val(amount);

    gst_amt = $('#charge_percentage13').val();
    amount = (gst_amt * without_discount) / 100;
    if (amount === Infinity) {
        amount = 0;
    }
    $('#charge_amount13').val(amount);

    taxCalculation();
}


function getTotalAmounts() {
    var total_cgst = 0.0;
    var total_sgst = 0.0;
    var total_igst = 0.0;
    var total_cess = 0.0;
    var total_discount = 0.0;
    var total_freightcharges = 0.0;
    var total_othercharges = 0.0;
    var total_amt = 0.0;
    var net_total = 0.0;

    for (var key in vendor_array) {
        var obj = vendor_array[key];
        total_cgst += parseFloat(obj.cgst_amt);
        total_sgst += parseFloat(obj.sgst_amt);
        total_igst += parseFloat(obj.igst_amt);
        total_cess += parseFloat(obj.cess_amt);
        total_discount += parseFloat(obj.discount_amt);
        total_othercharges += parseFloat(obj.othercharge_amt);
        total_freightcharges += parseFloat(obj.flightcharge_amt);
        total_amt += parseFloat(obj.totalRate);
    }
    var tot_tax = parseFloat(total_cgst) + parseFloat(total_sgst) + parseFloat(total_igst) + parseFloat(total_cess);
    var oth_charges = parseFloat(total_othercharges) + parseFloat(total_freightcharges);
    net_total = (parseFloat(total_amt) + parseFloat(tot_tax) + parseFloat(oth_charges)) - (parseFloat(total_discount));
    $('#SGSTtotal_amt').html(total_sgst);
    $('#CGSTtotal_amt').html(total_cgst);
    $('#IGSTtotal_amt').html(total_igst);
    $('#CESStotal_amt').html(total_cess);
    $('#ITEMDISCtotal_amt').html(total_discount);
    $('#FRCHGtotal_amt').html(total_freightcharges);
    $('#OTHCHGtotal_amt').html(total_othercharges);

    $('#gross_amount_hd').val(total_amt);
    $('#discount_hd').val(total_discount);
    $('#tot_tax_amt_hd').val(tot_tax);
    $('#tot_other_charges_hd').val(oth_charges);
    $('#net_amount_hd').val(net_total);
    $('#bill_nettotal_hidden').val(net_total);
}

function addTotalDiscount(obj, from_type) {
    validateText();
    var amt = obj.value;
    var amount = 0.0;
    var tot_rate = $('#bill_nettotal_hidden').val();
    var net_rate = $('#net_amount_hd').val();
    if (from_type == '1') {
        amount = (amt * tot_rate) / 100;
        $('#totalbill_discountAmount').val(Math.round(amount * 100) / 100);
        $('#bill_total_discount').val(Math.round(amount * 100) / 100);
        net_rate = parseFloat(tot_rate) - parseFloat(amount);
    } else if (from_type == '2') {
        amount = (100 * amt) / tot_rate;
        $('#totalbill_discountPercentage').val(Math.round(amount * 100) / 100);
        $('#bill_total_discount').val(Math.round(amt * 100) / 100);
        net_rate = parseFloat(tot_rate) - parseFloat(amt);
    }
    $('#net_amount_hd').val(Math.round(net_rate * 100) / 100);
}

function addItemVendorDetails(row_id, item_id, detail_id) {
    var free_item = $('#free_item' + row_id).val();
    var qty = $('#item_quantity' + row_id).val();
    var tot_item = parseInt(free_item) + parseInt(qty);
    var item_rate = $('#itemsellingprice' + row_id).val();
    var unit_cost = $('#itemUnitCost').val();
    var totalRate = $('#itemTotalRate').val();
    var netRate = $('#itemNetRate').val();

    $('#request_qty' + row_id).val(tot_item);
    $('#item_rate' + row_id).val(item_rate);
    $('#item_unit_cost' + row_id).val(unit_cost);

    var mrp = $('#itemmrp_peruom' + row_id).val();

    var cgst_per = 0.0;
    var cgst_amt = 0.0;
    var sgst_per = 0.0;
    var sgst_amt = 0.0;
    var igst_per = 0.0;
    var igst_amt = 0.0;
    var cess_per = 0.0;
    var cess_amt = 0.0;
    var discount_per = 0.0;
    var discount_amt = 0.0;
    var othercharge_per = 0.0;
    var othercharge_amt = 0.0;
    var flightcharge_per = 0.0;
    var flightcharge_amt = 0.0;

    $('.tax_class').each(function () {
        var item_type = $(this).attr("data-id");
        if (item_type == 'CGST') {
            if ($(this).prop("name") == "taxpercentage") {
                cgst_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                cgst_amt = $(this).val();
            }
        }
        if (item_type == 'SGST') {
            if ($(this).prop("name") == "taxpercentage") {
                sgst_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                sgst_amt = $(this).val();
            }
        }

        if (item_type == 'IGST') {
            if ($(this).prop("name") == "taxpercentage") {
                igst_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                igst_amt = $(this).val();
            }
        }

        if (item_type == 'CESS') {
            if ($(this).prop("name") == "taxpercentage") {
                cess_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                cess_amt = $(this).val();
            }
        }

        if (item_type == 'ITEMDISC') {
            if ($(this).prop("name") == "taxpercentage") {
                discount_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                discount_amt = $(this).val();
            }
        }
        if (item_type == 'OTHCHG') {
            if ($(this).prop("name") == "taxpercentage") {
                othercharge_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                othercharge_amt = $(this).val();
            }
        }
        if (item_type == 'FRCHG') {
            if ($(this).prop("name") == "taxpercentage") {
                flightcharge_per = $(this).val();
            } else if ($(this).prop("name") == "taxamount") {
                flightcharge_amt = $(this).val();
            }
        }
    });

    var tot_tax = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt) + parseFloat(cess_amt);
    $('#item_tax_amt' + row_id).val(tot_tax);
    $('#item_dicount_amt' + row_id).val(discount_amt);
    $('#item_net_amt' + row_id).val(netRate);

    vendor_array[row_id] = {
        detail_id: detail_id,
        item_id: item_id,
        free_item: free_item,
        mrp: mrp,
        item_rate: item_rate,
        cgst_per: cgst_per,
        cgst_amt: cgst_amt,
        sgst_per: sgst_per,
        sgst_amt: sgst_amt,
        igst_per: igst_per,
        igst_amt: igst_amt,
        cess_per: cess_per,
        cess_amt: cess_amt,
        discount_per: discount_per,
        discount_amt: discount_amt,
        othercharge_per: othercharge_per,
        othercharge_amt: othercharge_amt,
        flightcharge_per: flightcharge_per,
        flightcharge_amt: flightcharge_amt,
        unit_cost: unit_cost,
        totalRate: totalRate,
        netRate: netRate,
    }
    getTotalAmounts();
}

function reloadSubmit(vendor_id) {
    bootbox.confirm({
        message: "Are you sure you want to reload ?",
        buttons: {
            'confirm': {
                label: "Reload",
                className: 'btn-warning',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-success'
            }
        },
        callback: function (result) {
            if (result) {
                url = base_url + "/quotation/submitVendorQuotation/" + vendor_id;
                document.location.href = url;
            }
        }
    });
}

function saveVendorQuotation(rfq_id, vendor_id, vendor_encode, rfqvendor_id) {
    bootbox.confirm({
        message: "Are you sure you want to Save ?",
        buttons: {
            'confirm': {
                label: "Save",
                className: 'btn-success',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var token = $('#token_hiddendata').val();
                var url = base_url + "/quotation/saveVendorQuotation";
                var total_amt = $('#gross_amount_hd').val();
                if (total_amt != 0) {
                    var vendor_string = JSON.stringify(vendor_array);
                    vendor_string = encodeURIComponent(vendor_string);
                    var remarks_string = JSON.stringify(remarks_data);
                    remarks_string = encodeURIComponent(remarks_string);
                    var discount_amt = $('#discount_hd').val();
                    var tax_amt = $('#tot_tax_amt_hd').val();
                    var othercharge_amt = $('#tot_other_charges_hd').val();
                    var bill_discount = $('#bill_total_discount').val();
                    var bill_nettotal = $('#bill_nettotal_hidden').val();
                    var remarks = $('#fullbill_remarks').val();
                    var head_id = $('#head_id_hidden').val();

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            _token: token, vendor_string: vendor_string, remarks_string: remarks_string, rfq_id: rfq_id, vendor_id: vendor_id,
                            total_amt: total_amt, discount_amt: discount_amt, tax_amt: tax_amt, head_id: head_id,
                            othercharge_amt: othercharge_amt, bill_discount: bill_discount, rfqvendor_id: rfqvendor_id,
                            bill_nettotal: bill_nettotal, remarks: btoa(remarks)
                        },
                        beforeSend: function () {
                            $('#saveVendorQuotationbtn').attr('disabled', true);
                            $('#saveVendorQuotationSpin').removeClass('fa fa-save');
                            $('#saveVendorQuotationSpin').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            if (data) {
                                toastr.success("Successfully Updated");
                                url = base_url + "/quotation/submitVendorQuotation/" + vendor_encode;
                                document.location.href = url;
                            }
                        },
                        complete: function () {
                            $('#saveVendorQuotationbtn').attr('disabled', false);
                            $('#saveVendorQuotationSpin').removeClass('fa fa-spinner fa-spin');
                            $('#saveVendorQuotationSpin').addClass('fa fa-save');
                        },
                        error: function () {
                            toastr.error('Please check your internet connection and try again');
                        }
                    });
                } else {
                    toastr.warning("Please Add any items");
                }
            }
        }
    });
}
