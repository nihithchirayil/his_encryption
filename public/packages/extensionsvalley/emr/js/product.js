$(document).ready(function() {
    setTimeout(function() {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#admission_date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#admission_date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
         if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

 if(category) {


        $.ajax({
                    type: "GET",
                    url: '',
                    data: 'category=' + category,
                    beforeSend: function () {

                        $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#sub_category" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#sub_category").empty();

                                        $("#sub_category").html('<option value="">Select Sub catogery</option>');
                                        $.each(html,function(key,value){
                                          $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#sub_category").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                     // alert('Please check your internet connection and try again');
                     $( "#sub_category" ).prop( "disabled", false );

                    },
                    complete: function () {
                        $('#warning1').remove();
                        $( "#sub_category" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $("#category").focus();
                $("#sub_category").empty();
            }

    });
function getProductData() {
    var url = route_json.ProductDetailsData;

    //-------filters---------------------------------------------

   var product_name=$('#product_name_hidden').val();
   var product_code=$('#product_code').val();
   var generic_name=$('#generic_name').val();
   var category=$('#category').val();
   var sub_category=$('#sub_category').val();
   var item_type=$('#item_type').val();
   var hsn_code=$('#hsn_code').val();
   var status=$('#status').val();

   var parm={product_name:product_name,product_code:product_code,generic_name:generic_name,category:category,
    sub_category:sub_category,
    item_type:item_type,hsn_code:hsn_code,status:status};


    if( $('select[name=status] option:selected').val() == 'Active' ) {
       var status='active';
    }else if( $('select[name=status] option:selected').val() == 'Inactive' ) {
       var status='inactive';
    }
    parm['status']=status;
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
         // alert(html);

        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            var is_print =$("#is_print").val();

            if(is_print==0 || is_print==undefined){
                   $("#print_results").addClass("disabled");
               }
           var is_excel =$("#is_excel").val();

           if(is_excel==0 || is_excel==undefined){
                   $("#csv_results").addClass("disabled");
                }
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}
function datarst3() {


    $('.ajaxSearchBox').hide();
    $(".datapicker").select2('val', '');
    $('#product_name_hidden').val('');
    $('#product_name').val('');
}

//---------generate csv------------------------------------------------------

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");
};

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}

function print_generate(printData) {
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    if($('#showTitle').is(":checked")){
        printhead =$("#hospital_header").val();
        showw=showw+atob(printhead);
    }
    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:0;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
