
function SaveStockDetails() {
    let _token = $('#c_token').val();
    var dataParam = $('#stock_data').serialize();
    var url = $('#base_url').val() + "/stockclosingbalance/save-stock-balance";

    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: dataParam + '&_token=' + _token,
        beforeSend: function() {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {

            if (data.status == 1) {
                Command: toastr["success"]("Saved Successfully.");
                 window.location.href = $('#base_url').val() + "/stockclosingbalance/stock-closing-balance";
            }
            else {
                Command: toastr["error"]("Please check internet connection!");
            }
           
        },
        complete: function() {
           $('body').LoadingOverlay("hide");
        }
    });

}

