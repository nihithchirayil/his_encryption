$(document).ready(function() {
    $('.datepicker').datetimepicker({
        format: 'MMMM-DD-YYYY'
    });
});


function saveAdmissionRequest(){

        var encounter_id = $('#encounter_id').val();
        var visit_id = $('#visit_id').val();
        var patient_id = $('#patient_id').val();
        var priority = $('#priority').val();
        var referring_doctor = $('#referring_doctor').val();
        var investigation_head_id = $('#investigation_head_id').val();
        if(priority == ""){
            Command: toastr["warning"]('Error.! Select Priority');
            return false;
        }
        // if(referring_doctor == ""){
        //     Command: toastr["warning"]('Error.! Select Refferring Doctor');
        //     return false;
        // }
        var dataparams = $('#formAdmission').serialize();

        if (patient_id != "") {
            $('#admit_btn').prop('disabled', true);
            let _token = $('#c_token').val();
            var url = $('#base_url').val() + "/emr/admission-request";
            $.ajax({
                type: "POST",
                async: false,
                url: url,
                data: 'req_type=save&encounter_id=' + encounter_id + '&visit_id=' + visit_id + '&patient_id=' + patient_id + '&priority=' + priority +'&investigation_head_id='+ investigation_head_id + '&dataparams=' + dataparams + "&_token=" + _token,
                beforeSend: function () {
                    $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
                },
                success: function (responsedata) {
                    if (responsedata.success == true) {
                        Command: toastr["success"](responsedata.message);
                        $('#admitModal :input').val('');
                        $('#admitModal').modal('hide');
                        window.history.back();
                    } else {
                        Command: toastr["warning"](responsedata.message);
                        return;
                    }

                },
                error: function () {
                    Command: toastr["warning"]('Error.!');
                },
                complete: function () {
                    $('#admit_btn').prop('disabled', false);
                    $("body").LoadingOverlay("hide");

                }
            });

        } else {
            Command: toastr["warning"]('Please fill all fields.');
        }


}
