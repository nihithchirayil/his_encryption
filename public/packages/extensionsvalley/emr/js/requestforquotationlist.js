$(document).ready(function () {
    base_url = $('#base_url').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    setTimeout(function () {
        $(".select2").select2();
        searchQuotation();
    }, 800);

    $('#rfq_number').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var base_url = $('#base_url').val();
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#rfq_id_hidden').val() != "") {
                $('#rfq_id_hidden').val('');
                $("#rfq_idAjaxDiv").html("").hide();
            }
            var rfq_no = $(this).val();
            if (rfq_no == "") {
                $("#rfq_idAjaxDiv").html("").hide();
            } else {
                var url = base_url + "/reports/searchRFQNumber";
                var param = { rfq_no: rfq_no };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#rfq_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        // alert(html); return;
                        $("#rfq_idAjaxDiv").html(html).show();
                        $("#rfq_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });

});
base_url = '';

function searchNewVendor() {
    var token = $('#token_hiddendata').val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var vendor_name = $('#vendor_name').val();
        if (vendor_name == "") {
            $("#ajaxVendorSearchBox").html("");
        } else {
            var url = base_url + "/quotation/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, vendor_name: vendor_name, row_id: 1 },
                beforeSend: function () {
                    $("#ajaxVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxVendorSearchBox").html(html).show();
                    $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }

    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#vendor_name').val(vendor_name);
    $('#vendor_id').val(id);
    $('#ajaxVendorSearchBox').hide();
}

function searchQuotation() {
    var rfq_id = $('#rfq_id_hidden').val();
    var to_date = $('#to_date').val();
    var from_date = $('#from_date').val();
    var to_location = $('#to_location').val();
    var status = $('#status').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/searchQuotation";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, rfq_id: rfq_id, to_location: to_location, to_date: to_date, from_date: from_date, status: status },
        beforeSend: function () {
            $('#searchQuotationBtn').attr('disabled', true);
            $('#searchQuotationSpin').removeClass('fa fa-search');
            $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchQuotationBtn').attr('disabled', false);
            $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchQuotationSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function fillrfq_no(id, rfq_number) {
    $('#rfq_number').val(rfq_number);
    $('#rfq_id_hidden').val(id);
    $('#rfq_idAjaxDiv').hide();
}

function listQuotationItems(head_id) {
    var quotation_no = $('#quotation_no_data' + head_id).html();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/listQuotationItems";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, head_id: head_id },
        beforeSend: function () {
            $('#quotationListHeader').html(quotation_no);
            $('#listQuotationItemsBtn' + head_id).attr('disabled', true);
            $('#listQuotationItemsSpin' + head_id).removeClass('fa fa-list');
            $('#listQuotationItemsSpin' + head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#quotationListDiv").html(data);
            $("#quotationListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#listQuotationItemsBtn' + head_id).attr('disabled', false);
            $('#listQuotationItemsSpin' + head_id).removeClass('fa fa-spinner fa-spin');
            $('#listQuotationItemsSpin' + head_id).addClass('fa fa-list');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function addWindowLoad(to_url, rfq_id, from_string) {
    var url = base_url + "/quotation/" + to_url + '/' + rfq_id + '/' + from_string;
    document.location.href = url;
}
