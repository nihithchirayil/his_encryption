var token = $("#c_token").val();
var base_url = $("#base_url").val();

$(window).load(function() {
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    if (location_id != '') {
        $("#billing_location").val(location_id);
    }

    if (parseInt($("#bill_id").val()) > 0) {
        fetchServiceBillDetails($("#bill_id").val());
    }

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });

    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm:ss A'
    });

    $(".from_date_invest").val($("#current_date").val());
    $(".to_date_invest").val($("#current_date").val());

    $(".select2").select2();
    addNewRow();
    $("#op_no").focus();

    var get_uhid = $("#get_uhid").val();
    if(get_uhid){
        getPatientDetailsByUHID(get_uhid);
    }
});



$(document).on('input', '.number_class', function() {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    let new_val = cur_obj_value.replace(/[^\d.]+/g, "").replace(/(\..*)\./g, '$1');
    $(cur_obj).val(new_val);
});


$(document).on("keyup", ".discount_value", function(){
    if($("#discount_type").val() == 1 && ($(".discount_value").val() > 100 || $(".discount_value").val() == 0)){
        $(".discount_value").val('');
    } else if($("#discount_type").val() == 2 ){
        var net_amount = $(".net_amount").attr('data-net-amount');
        var discount_value = $(".discount_value").val();

        if(net_amount && discount_value){
            net_amount = parseInt(net_amount);
            discount_value = parseInt(discount_value);
            if( (discount_value > net_amount) || discount_value == 0 ){
                $(".discount_value").val('');
            }
        } else {
            $(".discount_value").val('');
        }
    }
    calculateTotalDiscount();
    $(".discount_value").focus();
})

$(document).on("change", "#discount_type", function(){
    if($("#discount_type").val() == 1 && ($(".discount_value").val() > 100 || $(".discount_value").val() == 0)){
        $(".discount_value").val('');
    } else if($("#discount_type").val() == 2 ){
        var net_amount = $(".net_amount").attr('data-net-amount');
        var discount_value = $(".discount_value").val();
        if(net_amount && discount_value){
            net_amount = parseInt(net_amount);
            discount_value = parseInt(discount_value);
            if( (discount_value > net_amount) || discount_value == 0 ){
                $(".discount_value").val('');
            } else {
                $(".discount_value").val('');
            }
        }
    }
    calculateTotalDiscount();
    $(".discount_value").focus();
})


$(document).on("change", "#bill_tag", function(){
    if($("#op_no_hidden").val()){
        $(".service_billing_table_body").empty();
        addNewRow();
    }
    if($(this).val() != $("#package_bill_tag_id").val()){
        $("#select_package_btn").attr('disabled', true);
    } else {
        if($("#package_bill_tag_id").val() != "" && $(this).val() != ""){
            $("#select_package_btn").attr('disabled', false);
            $("#select_package_btn").trigger('click');
        }

    }
})

$(document).on("click", "#select_package_btn", function(){
    if($("#op_no_hidden").val()){
        fetchPackageList();
    } else {
        Command: toastr["error"]("Please select patient");
    }
})

$(document).on("click", "#select_investigation_btn", function(){
    showIndentScreen();
})

$(document).on('click', '.advanceSearchBtn', function(event) {
    advancePatientSearch(1);
});

function selectedPatientFromAdvanceSearch(patient_details){
    var uhid = patient_details.uhid;
    getPatientDetailsByUHID(uhid);
}

function getPatientDetailsByUHID(uhid){
    var url = $('#base_url').val() + "/service/getPatientDetailsByUHID";
    var discharge_bill_head_id = $("#discharge_bill_head_id").val() ? $("#discharge_bill_head_id").val(): 0;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            uhid: uhid,
            discharge_bill_head_id:discharge_bill_head_id
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if ($('#select_investigation_modal').hasClass('in')) {
                $("#patient_uhid").val(data.patient_details.uhid);
                $("#patient_uhid_hidden").val(data.patient_details.patient_id);
                $("#patient_uhidAjaxDiv").hide();
                $("#patient_name_invest").val(data.patient_details.patient_name);
                fetchIndentList();
            } else {
                $("#op_noAjaxDiv").hide();
                $("#patient_id").val(data.patient_details.patient_id);
                $("#op_no").val(data.patient_details.uhid);
                $("#op_no_hidden").val(data.patient_details.patient_id);
                $("#patient_name_bill").html(data.patient_details.patient_name);
                $("#ip_number").html(data.patient_details.admission_no);
                if(data.patient_details.doctor_id != ""){
                    $("#consulting_doctor").val(parseInt(data.patient_details.doctor_id)).trigger('change');
                } else {
                    $("#consulting_doctor").val(data.patient_details.doctor_id).trigger('change');
                }
                $("#current_room").html(data.patient_details.room_name);
                $("#company").val(data.patient_details.company_id);
                $("#pricing").val(data.patient_details.pricing_id);
                $(".patient_advance").val(parseFloat(data.patient_details.patient_advance).toFixed(2));
                $("#visit_id").val(data.patient_details.visit_id);
                $("#room_type").val(data.patient_details.room_type_id);
                $("#payment_type").empty();

                $.each(data.payment_types, function(key, value){
                    $("#payment_type").append('<option value="' + key + '">' + value + '</option>');
                });
                if(data.default_payment_type != ''){
                    $('#payment_type option[value="opcredit"]').attr("selected",true);
                }


                fetchPatientPendingBills(false);

                if($("#bill_tag").val()){
                    $(".service_billing_table_body").empty();
                    addNewRow();

                    if($("#bill_tag").val() != $("#package_bill_tag_id").val()){
                        $("#select_package_btn").attr('disabled', true);
                    } else {
                        if($("#package_bill_tag_id").val() != "" && $('#bill_tag').val() != ""){
                            $("#select_package_btn").attr('disabled', false);
                            $("#select_package_btn").trigger('click');
                        }
                    }

                }
            }
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}



$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: "",
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});


$(document).on("click", "#out_side_patient", function(e) {
    if ($("#out_side_patient").prop("checked")) {
        $("#op_no").prop('disabled', true);
        $(".advanceSearchBtn").prop('disabled', true);
        showOutsidePatientRegForm();
    } else {
        $("#op_no").prop('disabled', false);
        $(".advanceSearchBtn").prop('disabled', false);
    }
});


$(document).on('click', '#out_side_doctor', function(event) {
    if ($("#out_side_doctor").prop('checked')) {
        $("#outside_doctor_name").removeAttr('disabled');
        $("#outside_doctor_name").removeAttr('readonly');
        $("#consulting_doctor").attr('disabled', true);
        $("#consulting_doctor").attr('readonly', true);
        $("#outside_doctor_name").focus();
    } else {
        $("#outside_doctor_name").attr('disabled', true);
        $("#outside_doctor_name").attr('readonly', true);
        $("#consulting_doctor").removeAttr('disabled');
        $("#consulting_doctor").removeAttr('readonly');
    }
});


function outsidePatientRegistrationSuccess(patient_details) {
    getPatientDetailsByUHID(patient_details.uhid);
    $("#op_no").prop('disabled', true);

    if (patient_details.external_doctor_check == 1) {
        $("#out_side_doctor").prop("checked", true);
        $("#outside_doctor_name").val(patient_details.doctor_name);
        $("#consulting_doctor").attr('disabled', true);
    } else {
        $("#out_side_doctor").prop("checked", false);
        $("#outside_doctor_name").val('');
        $("#consulting_doctor").val(patient_details.doctor).trigger('change');
        $("#consulting_doctor").attr('disabled', false);
    }
    $("#out_side_patient").attr('disabled', true);
}

function closedOutsidePatientRegistrationModal(){
    $("#out_side_patient").prop("checked", false);
    $("#op_no").prop('disabled', false);
    $(".advanceSearchBtn").prop('disabled', false);
}


$(document).on('click', '.addNewRowBtn', function(event) {
    addNewRow();

});


function addNewRow() {
    var row_length = 0;
    $('.item_description').each(function(key, value) {
        if (!$(value).val()){
            row_length = row_length + 1;
            $(value).focus();
        }

    });
    if (row_length > 0){
        return;
    }



    var row_html = '<tr style="background: #FFF;" class="row_class"><td class="serial_no">1</td><td><input class="form-control item_description" type="text" value=""></td><td><input class="form-control item_price number_class" data-actual-price="" type="text" value="" readonly disabled></td><td><input class="form-control quantity number_class" type="text" value=""></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="0"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
    $(".service_billing_table_body").append(row_html);
    resetSerialNumbers();

    $(".service_billing_table_body").each(function(key, val){
        if(!$(val).hasClass('row_class_removed')){
            $(val).focus();
        }
    })

}


function deleteRow(button) {
    var service_length = 0;
    $('.item_description').each(function(key, value) {
        if ($(value).val())
            service_length = service_length + 1;
    });

    if ($("#bill_id").val() != "0") {
        $(button).parents('tr.row_class').removeClass('row_class').addClass('row_class_removed').hide();
    } else {
        $(button).parents('tr.row_class').remove();
    }

    calculateTotalAmount();
    resetSerialNumbers();
}


function resetSerialNumbers() {
    $('tr.row_class').find(".serial_no").each(function(key, val) {
        $(this).html(key + 1);
    })
}





var timeout = null;
var last_search_string = '';
$(document).on('keyup', '.item_description', function(event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        return false;
    }
    if(!$("#op_no_hidden").val()){
        Command: toastr["error"]("Please select patient");
        $(this).val('');
        return false;
    }
    if(!$("#bill_tag").val()){
        Command: toastr["error"]("Please select bill tag");
        $(this).val('');
        return false;
    }
    if($("#bill_tag").val() != $("#package_bill_tag_id").val()){
        /* Act on the event */
        var obj = $(this);
        var search_string = $(this).val().trim();
        searchServiceItem(search_string, obj);
    }


});

var searchServiceItemAjax = '';
function searchServiceItem(search_string, obj) {
    var patient_id = $("#op_no_hidden").val();
    var pricing_id = $("#pricing").val();
    var bill_tag = $("#bill_tag").val();
    var visit_id = $("#visit_id").val();
    var room_type = $("#room_type").val();

    let tr = $(obj).closest('tr');
    window.tr = tr;
    let row_id = $(obj).closest('tr').html();

    if (search_string == "") {
        $(tr).find('.quantity').val('');
        $(tr).find('.total_amount').val(0);
        $('.service-list-div-row-listing').hide();
    }

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        var service_list = $('.service-list-div-row-listing');
        $('.list-service-search-data-row-listing').empty();
        $(service_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/service/searchServiceItem";
            searchServiceItemAjax = $.ajax({
                type: "POST",
                url: url,
                data: {
                    patient_id: patient_id,
                    pricing_id: pricing_id,
                    search_string: search_string,
                    bill_tag: bill_tag,
                    visit_id: visit_id,
                    room_type: room_type
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                    $('.item_description').blur();
                },
                success: function(data) {

                    if(data.status == 1){

                        let response = data.service_list;
                        let res_data = "";

                        if (response.length > 0) {
                            for (var i = 0; i < response.length; i++) {

                                var service_desc = response[i].service_desc;
                                var service_code = response[i].service_code;
                                var dept_name = response[i].dept_name;
                                var sub_dept_name = response[i].sub_dept_name;
                                var calculatedprice = response[i].calculatedprice;
                                var actualvalue = response[i].actualvalue;
                                var is_discountable = response[i].is_discountable;
                                var is_price_editable = response[i].is_price_editable;

                                res_data += '<tr data-actual-selling-price="' + actualvalue + '" data-service-code="' + service_code + '" data-service-desc="' + service_desc + '" data-calculated-price="' + calculatedprice + '" data-is-discountable="'+is_discountable+'" data-is-price-editable="'+is_price_editable+'"><td>' + service_desc + '</td><td>' + dept_name + '</td><td>' + sub_dept_name + '</td><td>' + calculatedprice + '</td></tr>';
                            }
                        } else {
                            res_data = '<tr class="text-center"><td colspan="4">No Data Found..!</td></tr>';
                        }

                        $(".list-service-search-data-row-listing").html(res_data);
                    } else {
                        $(service_list).hide();
                    }
                    last_search_string = search_string;

                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                    $('.theadfix_wrapper').floatThead("reflow");
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }
            });
        }, 1000)

    }
}



$(document).on('click', '.service-list-div-row-listing > .close_btn_service_search', function(event) {
    event.preventDefault();
    $(".service-list-div-row-listing").hide();
});


$(document).on('keydown', function(event) {
    if ($('.service-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-service-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                if ($(".list-service-search-data-row-listing").find('tr.active_item').length == 0 || $(".list-service-search-data-row-listing").find('tr.active_item').is(':first-child')) {
                    $(".list-service-search-data-row-listing").find('tr').removeClass('active_item');
                    $(".list-service-search-data-row-listing").find('tr').last().addClass('active_item');
                } else {
                    $(".list-service-search-data-row-listing").find('tr.active_item').prev().addClass('active_item');
                    $(".list-service-search-data-row-listing").find('tr.active_item').last().removeClass('active_item');
                }
            } else if (event.which == 40) { // down
                if ($(".list-service-search-data-row-listing").find('tr.active_item').length == 0 || $(".list-service-search-data-row-listing").find('tr.active_item').is(':last-child')) {
                    $(".list-service-search-data-row-listing").find('tr').removeClass('active_item');
                    $(".list-service-search-data-row-listing").find('tr').first().addClass('active_item');
                } else {
                    $(".list-service-search-data-row-listing").find('tr.active_item').next().addClass('active_item');
                    $(".list-service-search-data-row-listing").find('tr.active_item').first().removeClass('active_item');
                }
            } else if (event.which == 13) {
                $(".list-service-search-data-row-listing").find('tr.active_item').trigger('click');
            } else if (event.which == 8) {
                $('.service-list-div-row-listing').hide();
                $(window.tr).find('.item_description').focus();
            }
        }
    } else if($("#substitute_item_descAjaxDiv").css('display') == 'block'){
        if (event.which == 13) {
            $("#substitute_item_descAjaxDiv").find('li.liHover').trigger('click');

        }
    } else if($('#op_noAjaxDiv').css('display') == 'block'){
        if (event.which == 13) {
            $("#op_noAjaxDiv").find('li.liHover').trigger('click');
        }
    }
});


$(document).on('click', '.list-service-search-data-row-listing > tr', function(event) {
    event.preventDefault();
    var service_desc = $(event.currentTarget).attr('data-service-desc');
    var calculated_price = $(event.currentTarget).attr('data-calculated-price');
    var actual_selling_price = $(event.currentTarget).attr('data-actual-selling-price');
    var service_code = $(event.currentTarget).attr('data-service-code');
    var is_discountable = $(event.currentTarget).attr('data-is-discountable');
    var is_price_editable = $(event.currentTarget).attr('data-is-price-editable');

    if ($(".item_description[data-service-code='" + service_code + "']").length > 0) {
        $('.service-list-div-row-listing').hide()
        bootbox.alert('Item already exists.');
        $(".item_description[data-service-code='" + service_code + "']").parents('tr').addClass('alerts-border');

        setTimeout(function() {
            $(".item_description[data-service-code='" + service_code + "']").parents('tr').removeClass('alerts-border');
            var end = $($(".item_description[data-service-code='" + service_code + "']")[0]).val().length;
            $(".item_description[data-service-code='" + service_code + "']")[0].setSelectionRange(end, end);
            $($(".item_description[data-service-code='" + service_code + "']")[0]).focus();
            $(window).scrollTop($(".item_description[data-service-code='" + service_code + "']").position().top);
        }, 3000)

        return;
    }

    if(is_price_editable == "true"){
        $(window.tr).find('.item_price').removeAttr('disabled');
        $(window.tr).find('.item_price').removeAttr('readonly');
    } else {
        $(window.tr).find('.item_price').attr('disabled', true);
        $(window.tr).find('.item_price').attr('readonly', true);
    }

    $(window.tr).find('.item_price').val(parseInt(calculated_price));
    $(window.tr).find('.item_price').attr('data-actual-price', parseInt(calculated_price));
    $(window.tr).find('.total_amount').val(calculated_price);
    $(window.tr).find('.total_amount').attr('data-is-discountable', is_discountable);
    $(window.tr).find('.item_description').val(service_desc);
    $(window.tr).find('.item_description').attr('data-service-code', service_code);
    $(window.tr).find('.quantity').focus().val(1);
    delete window.tr;
    $(".service-list-div-row-listing").hide();
    addNewRow();
    calculateTotalAmount();
});


$(document).on('keyup', '.quantity', function() {

    calculateTotalAmount();
    var qty = $(this).val();
    if (qty == 0) {
        $(this).val('');
    }

});

$(document).on('keyup', '.item_price', function() {
    calculateTotalAmount();
});


function resetServiceBill(){
    window.location.reload();
}


function saveServiceBill(){
    var patient_id = $("#patient_id").val();
    if(!patient_id){
        Command: toastr["error"]("Please select patient");
        return false;
    }
    var billing_location = $("#billing_location").val();
    if(!billing_location){
        Command: toastr["error"]("Please select billing location");
        return false;
    }
    var consulting_doctor = $("#consulting_doctor").val();
    if(!consulting_doctor){
        Command: toastr["error"]("Please select doctor");
        return false;
    }
    var payment_type = $("#payment_type").val();
    if(!payment_type){
        Command: toastr["error"]("Please select payment type");
        return false;
    }
    var discount_type = $("#discount_type").val();
    if(discount_type){
        var discount = $(".discount_value").val();
        if(!discount){
            Command: toastr["error"]("Please enter the discount");
            return false;
        }
    }
    var out_side_doctor = $("#out_side_doctor").prop('checked');
    if (!out_side_doctor) {
        if ($("#doctor").val() == "") {
            Command: toastr['error']('Please select doctor');
            return false;
        }
    } else {
        if ($("#outside_doctor_name").val() == "") {
            Command: toastr['error']('Please enter doctor name');
            return false;
        }
    }
    $('.row_class').each(function() {
        var quantity = $(this).find('.quantity').val();
        var item_description = $(this).find('.item_description').val();
        if (!quantity && item_description) {
            Command: toastr['error']('Please enter the quantity');
            $(this).find('.quantity').focus();
            return false;
        }
    });

    var message_show = '<p>Are you sure you want to save this bill. </p> <p> Net amount : ' + $(".net_amount").val() + '</p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Save',
                className: 'btn-success',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function(result) {
            if (result) {
                var data = {};
                data.bill_id = $("#bill_id").val();
                data.patient_id = $("#op_no_hidden").val();
                data.uhid = $("#op_no_hidden").val();
                data.payment_type = $("#payment_type").val();
                data.billing_location = $("#billing_location").val();
                data.bill_tag = $("#bill_tag").val();

                data.out_side_doctor = $("#out_side_doctor").prop('checked');
                if (data.out_side_doctor) {
                    data.doctor_name = $("#outside_doctor_name").val();
                    data.doctor_id = $("#external_doctor_id").val();
                } else {
                    data.doctor_id = $("#consulting_doctor").val();
                    data.doctor_name = $("#consulting_doctor option:selected").text();
                }
                data.discount_type = $("#discount_type").val();
                data.discount = $(".discount_value").val();
                data.discount_narration = $("#discount_narration").val();
                data.total_discount = $(".total_discount").val();
                data.company = $("#company").val();
                data.pricing = $("#pricing").val();
                data.patient_advance = $(".patient_advance").val();
                data.visit_id = $("#visit_id").val();

                data.service_items = [];
                if($("#bill_tag").val() == $("#package_bill_tag_id").val()){
                    data.package_id = $(".service_billing_table_body").find("tr.row_class:first").attr('data-package-id');
                } else {
                    data.package_id = 0;
                }
                $(".service_billing_table_body").find('tr').each(function(key, val) {
                    var service_code = $(val).find('.item_description').attr('data-service-code');
                    if(service_code){
                        var item_detail = {};
                        if (data.bill_id) {
                            if ($(val).attr('data-detail-id') && ($(val).find('.item_description').attr('data-service-code') != $(val).find('.item_description').attr('data-old-service-code'))) {
                                item_detail.detail_status = 2;
                                item_detail.detail_id = $(val).attr('data-detail-id');
                            } else if ($(val).attr('data-detail-id') && ($(val).find('.quantity').attr('data-old-quantity') != $(val).find('.quantity').val())) {
                                item_detail.detail_status = 2;
                                item_detail.detail_id = $(val).attr('data-detail-id');
                            } else if (!$(val).attr('data-detail-id')) {
                                item_detail.detail_status = 1;
                                item_detail.detail_id = $(val).attr('data-detail-id');
                            } else if ($(val).attr('data-detail-id')) {
                                item_detail.detail_status = 2;
                                item_detail.detail_id = $(val).attr('data-detail-id');
                            }

                            if ($(val).hasClass('row_class_removed')) {
                                item_detail.detail_status = 3;
                            }
                        }
                        item_detail.sl_no = key+1;

                        item_detail.intend_id = $(val).attr('data-indent-id') ? $(val).attr('data-indent-id') : 0;
                        item_detail.service_code = service_code;
                        item_detail.service_desc = $(val).find('.item_description').val();
                        item_detail.quantity = $(val).find('.quantity').val();
                        item_detail.item_price = $(val).find('.item_price').val();
                        item_detail.actual_selling_price = $(val).find('.item_price').attr('data-actual-price') ? parseInt($(val).find('.item_price').attr('data-actual-price')) : $(val).find('.item_price').val();
                        item_detail.total_amount = $(val).find('.total_amount').val();
                        item_detail.is_discountable = $(val).find('.total_amount').attr('data-is-discountable');
                        data.service_items.push(item_detail);
                    }

                });



                data.net_amount = $(".net_amount").val();
                data.bill_amount = $(".bill_amount").val();
                data.patient_payable = $(".patient_payable").val();
                data.payor_payable = $(".payor_payable").val();

                var url = $('#base_url').val() + "/service/saveServiceBill";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        service_bill_data: data,
                    },
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                    },
                    success: function(response) {
                        if(response.status == 1){
                            Command: toastr["success"]("Bill successfully saved.");
                            $(".save_message_bill_no").html(response.bill_no);
                            var bill_head_id = response.bill_head_id;
                            var bill_entry_status = response.bill_entry_status;;
                            $("#bill_id").val(bill_head_id);
                            $("#bill_no").val(response.bill_no);
                            if($("#bill_tag option:selected").text()=='PACKAGE BILL'){

                                $('#detail_summary_print').show();
                            }else{
                                $('#detail_summary_print').hide();

                            }
                            if(parseInt($(".patient_payable").val()) > 0){
                                var bill_ids = [];
                                bill_ids.push(bill_head_id);
                                if($("#get_uhid").val()){
                                    window.close();
                                }
                                if(bill_entry_status == "1"){
                                    if(data.payment_type == 'cash/Card' || payment_type == 'opcredit'){
                                        loadCashCollection(bill_ids);
                                    } else {

                                        $("#print_config_modal").modal('show');
                                    }

                                } else if(bill_entry_status == "2"){
                                    if(window.paid_status == 0 && (data.payment_type == 'cash/Card' || payment_type == 'opcredit')){
                                        loadCashCollection(bill_ids);
                                    } else {
                                        $("#print_config_modal").modal('show');
                                    }
                                }

                            } else {
                                $("#print_config_modal").modal('show');
                            }
                        } else {
                            Command: toastr["error"]("Faild to save service bill");
                        }

                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
        }
    });
}

function calculateTotalAmount(){


    $(".service_billing_table_body").find('tr.row_class').each(function(key, value) {
        var item = $(value).find('.item_description').val();
        var quantity = $(value).find('.quantity').val();
        var price = $(value).find('.item_price').val();
        var item_total = 0.00;

        if (quantity && price && item) {
            quantity = parseInt(quantity);
            price = parseFloat(price);
            item_total = parseFloat(item_total) + parseFloat(price * quantity);
            $(value).find('.total_amount').val(parseFloat(item_total).toFixed(2));
        } else {
            $(value).find('.total_amount').val(0.00);
        }
    });


    var bill_amount = 0;
    var net_amount = 0;
    $(".service_billing_table_body").find("tr.row_class").each(function(key, val){
        bill_amount = parseFloat(bill_amount) + parseFloat($(val).find('.total_amount').val())
    });

    var discount_type = $("#discount_type").val();
    var discount = $(".discount_value").val();
    var payment_type = $('#payment_type').val();
    if(discount_type && discount){
        if(discount > 0){
            if(discount_type == 1){
                net_amount = bill_amount - ((bill_amount*discount)/100);
            } else {
                net_amount = bill_amount - discount;
            }
        } else {
            net_amount = bill_amount;
        }

        if (payment_type == 'cash/Card' || payment_type == 'ipcredit' || payment_type == 'opcredit') {
            $('.patient_payable').val(parseFloat(net_amount).toFixed(2));
            $('.payor_payable').val(parseFloat(0).toFixed(2));
        } else if (payment_type == "credit" || payment_type == "insurance") {
            $('.patient_payable').val(parseFloat(0).toFixed(2));
            $('.payor_payable').val(parseFloat(net_amount).toFixed(2));
        }
        $('.net_amount').val(parseFloat(net_amount).toFixed(2));
        $(".net_amount").attr('data-net-amount', net_amount);
    } else {
        net_amount = bill_amount;
        if (payment_type == 'cash/Card' || payment_type == 'ipcredit' || payment_type == 'opcredit') {
            $('.patient_payable').val(parseFloat(net_amount).toFixed(2));
            $('.payor_payable').val(parseFloat(0).toFixed(2));
        } else if (payment_type == "credit" || payment_type == "insurance") {
            $('.patient_payable').val(parseFloat(0).toFixed(2));
            $('.payor_payable').val(parseFloat(net_amount).toFixed(2));
        }
        $('.net_amount').val(parseFloat(net_amount).toFixed(2));
        $(".net_amount").attr('data-net-amount', net_amount);
    }

    var patient_advance = $(".patient_advance").val();
    if(parseInt(patient_advance) > 0){
        var balance_in_advance = parseFloat(patient_advance) - parseFloat(net_amount);
        if(parseInt(balance_in_advance) < 0){
            $(".balance_in_advance").val(0.00);
        } else {
            $(".balance_in_advance").val(parseFloat(balance_in_advance).toFixed(2));
        }
    } else {
        $(".balance_in_advance").val(0.00);
    }

    var total_discount = parseFloat(bill_amount) - parseFloat(net_amount);
    $(".total_discount").val(parseFloat(total_discount).toFixed(2));


    $(".bill_amount").val(parseFloat(bill_amount).toFixed(2));

}


function calculateTotalDiscount(){
    var bill_amount = $(".bill_amount").val();
    var net_amount = 0;

    var discount_type = $("#discount_type").val();
    var discount = $(".discount_value").val();
    var payment_type = $('#payment_type').val();
    if(discount_type && discount){
        if(discount > 0){
            if(discount_type == 1){
                net_amount = bill_amount - ((bill_amount*discount)/100);
            } else {
                net_amount = bill_amount - discount;
            }
        } else {
            net_amount = bill_amount;
        }

        if (payment_type == 'cash/Card' || payment_type == 'ipcredit' || payment_type == 'opcredit') {
            $('.patient_payable').val(parseFloat(net_amount).toFixed(2));
            $('.net_amount').val(parseFloat(net_amount).toFixed(2));
            $('.payor_payable').val(parseFloat(0).toFixed(2));
        } else if (payment_type == "credit" || payment_type == "insurance") {
            $('.patient_payable').val(parseFloat(0).toFixed(2));
            $('.payor_payable').val(parseFloat(net_amount).toFixed(2));
            $('.net_amount').val(parseFloat(net_amount).toFixed(2));
        }
    } else {
        net_amount = bill_amount;
        if (payment_type == 'cash/Card' || payment_type == 'ipcredit' || payment_type == 'opcredit') {
            $('.patient_payable').val(parseFloat(net_amount).toFixed(2));
            $('.net_amount').val(parseFloat(net_amount).toFixed(2));
            $('.payor_payable').val(parseFloat(0).toFixed(2));
        } else if (payment_type == "credit" || payment_type == "insurance") {
            $('.patient_payable').val(parseFloat(0).toFixed(2));
            $('.payor_payable').val(parseFloat(net_amount).toFixed(2));
            $('.net_amount').val(parseFloat(net_amount).toFixed(2));
        }
    }

    var total_discount = parseFloat(bill_amount) - parseFloat(net_amount);
    $(".total_discount").val(parseFloat(total_discount).toFixed(2));
}

function fetchServiceBillDetails(bill_id){
    var url = $('#base_url').val() + "/service/fetchServiceBillDetails";
    var discharge_bill_head_id = $("#discharge_bill_head_id").val() ? $("#discharge_bill_head_id").val(): 0;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
            discharge_bill_head_id:discharge_bill_head_id
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if(data.status == 1){
                $("#payment_type").empty();

                $.each(data.payment_types, function(key, value){
                    $("#payment_type").append('<option value="' + key + '">' + value + '</option>');
                });

                if(data.default_payment_type != ''){
                    $('#payment_type option[value="opcredit"]').attr("selected",true);
                }

                $(".service_bill_no").html(" - "+data.service_bill_details[0].bill_no);
                $(".save_message_bill_no").html(data.service_bill_details[0].bill_no);
                $("#bill_no").val(data.service_bill_details[0].bill_no);
                $("#op_no").attr("disabled", true);
                $(".advanceSearchBtn").attr("disabled", true);
                $("#out_side_patient").attr("disabled", true);
                $("#select_investigation_btn").attr("disabled", true);
                $("#bill_tag").attr("disabled", true);
                if(data.service_bill_details[0].discount_type == 0){
                    $("#discount_type").val("");
                } else {
                    $("#discount_type").val(data.service_bill_details[0].discount_type)
                }
                if(parseInt(data.service_bill_details[0].discount)>0){
                    $(".discount_value").val(data.service_bill_details[0].discount)
                } else {
                    $(".discount_value").val('');
                }

                window.paid_status = data.service_bill_details[0].paid_status;
                if(data.service_bill_details[0].paid_status==1){
                      $('.saveServiceBillBtn').hide();
                }else{
                    $('.saveServiceBillBtn').show();
                }

                $("#discount_narration").val(data.service_bill_details[0].discount_reason)
                $(".total_discount").val(data.service_bill_details[0].discount_amount)
                $(".bill_amount").val(data.service_bill_details[0].bill_amount)
                $(".net_amount").val(data.service_bill_details[0].net_amount)
                $(".patient_payable").val(data.service_bill_details[0].patient_to_pay)
                $(".payor_payable").val(data.service_bill_details[0].company_to_pay)
                $(".patient_advance").val(data.service_bill_details[0].patient_advance)
                $("#bill_tag").val(data.service_bill_details[0].bill_tag)
                $("#patient_name_bill").html(data.service_bill_details[0].patient_name)
                $("#ip_number").html(data.service_bill_details[0].admission_no)
                $("#current_room").html(data.service_bill_details[0].room_name)
                $("#billing_location").val(data.service_bill_details[0].location)
                if(data.service_bill_details[0].is_ext_doctor == 1){
                    $("#out_side_doctor").prop('checked', true);
                    $("#outside_doctor_name").val(data.service_bill_details[0].consulting_doctor_name);
                } else {
                    $("#consulting_doctor").val(data.service_bill_details[0].consulting_doctor_id).trigger('change');
                }
                if(data.service_bill_details[0].is_outside_patient == 1){
                    $("#out_side_patient").prop('checked', true);
                }
                $("#company").val(data.service_bill_details[0].company_id)
                $("#pricing").val(data.service_bill_details[0].pricing_id)
                $("#payment_type").val(data.service_bill_details[0].payment_type)
                $("#op_no").val(data.service_bill_details[0].uhid)
                $("#op_no_hidden").val(data.service_bill_details[0].patient_id)
                $("#patient_id").val(data.service_bill_details[0].patient_id)
                $("#room_type").val(data.service_bill_details[0].room_type_id)
                $("#visit_id").val(data.service_bill_details[0].visit_id)

                $(".service_billing_table_body").empty();
                $.each(data.service_bill_details, function(key, val){
                    var row_html = '<tr data-indent-id="' + val.intend_id + '" data-detail-id="' + val.bill_detail_id + '" style="background: #FFF;" class="row_class"><td class="serial_no">'+(key+1)+'</td><td><input class="form-control item_description" type="text" value="'+val.service_desc+'" data-service-code="'+val.service_code+'" data-old-service-code="' + val.service_code + '"></td><td><input class="form-control item_price number_class" data-actual-price="'+parseInt(val.actual_selling_price)+'"  type="text" value="'+val.selling_price_wo_tax+'"></td><td><input class="form-control quantity number_class" type="text" value="'+parseInt(val.quantity)+'" data-old-quantity="' + parseInt(val.quantity) + '"></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="'+val.net_amount+'" data-is-discountable="'+val.is_discountable.toString()+'"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
                    $(".service_billing_table_body").append(row_html);
                })
                resetSerialNumbers();

                if(data.service_bill_details[0].package_id){
                    $(".selected_package_head_name").html(data.service_bill_details[0].package_name);
                    $(".selected_package_head_div").show();
                    $(".service_billing_table_body").find('.item_description, .quantity').attr('disabled', true);
                    $(".service_billing_table_body").find('.item_description, .quantity').attr('readonly', true);
                    $(".service_billing_table_body").find('.deleteRowBtn').hide();
                    $(".addNewRowBtn").hide();
                    $("#select_investigation_btn").attr('disabled', true);
                }

                calculateTotalAmount();
                if(!data.service_bill_details[0].package_id && data.service_bill_details[0].paid_status == 0 && data.service_bill_details[0].cancelled_status != 3){
                    addNewRow();
                }

                var enable_edit_paid_bills = $('#enable_edit_paid_bills').val();
                if((data.service_bill_details[0].paid_status == 1 && enable_edit_paid_bills==0)  || data.service_bill_details[0].cancelled_status == 3){
                    $(".saveServiceBillBtn").remove();
                    $(".addNewRowBtn").remove();
                    $(".deleteRowBtn").remove();
                    $("#consulting_doctor").attr('disabled', true);
                    $("#payment_type").attr('disabled', true);
                    $(".item_description").attr('disabled', true);
                    $(".quantity").attr('disabled', true);
                    $("#out_side_doctor").attr('disabled', true);
                    $("#discount_type").attr('disabled', true);
                    $(".discount_value").attr('disabled', true);
                    $("#discount_narration").attr('disabled', true);
                } else {
                    checkItemPriceEditable();
                }

                fetchPatientPendingBills(false);


            }
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}


function paymentCompleted() {
    $("#print_config_modal").modal('show');

}

function printBillDetails(bill_id){
    if ( !bill_id || bill_id == 0) {
        bill_id = $("#bill_id").val();
    }
    var include_hospital_header = $("#include_hospital_header").prop('checked') ? 1 : 0;
    var detail_print = $("#detail_print").prop('checked') ? 1 : 0;
    var summary_print = $("#summary_print").prop('checked') ? 1 : 0;
    var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var printMode = $('input[name=printMode]:checked').val();
    var url = $('#base_url').val() + "/service/printBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
            location_code: location_code,
            include_hospital_header: include_hospital_header,
            printMode: printMode,
            detail_print:detail_print,summary_print:summary_print
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            // print bill in A4 setup
            // if (printMode == 1) {
            //     var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
            //     winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data + '<script>setTimeout(function(){window.print(); window.close();  },1000)</script>');
            // } else {
            //     var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            //     winPrint.document.write('<style>@page{size:A4 landscape;} .patient_head{ font-size:10px !important; } </style>' + data + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
            // }
            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            if (printMode == 1) {
                winPrint.document.write('<style>@page{size:A5 portrait;} .patient_head{ font-size:10px !important; } </style>' + data + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
            } else {
            winPrint.document.write('<style>@page{size:A5 landscape;} .patient_head{ font-size:10px !important; } </style>' + data + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
            }
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}


$('#print_config_modal').on('hidden.bs.modal', function() {
    var bill_id = $('#bill_id').val();
    var base_url = $('#base_url').val();
    if (!bill_id) {
        window.location.reload();
    } else {
        var url = base_url + '/service/serviceBilling';
        window.location.href = url;
    }
});


$('#modalCashRecive').on('hidden.bs.modal', function() {
    $("#print_config_modal").modal('show');
});

function fetchPackageList(){
    var url = $('#base_url').val() + "/service/fetchPackageList";
    $.ajax({
        type: "POST",
        url: url,
        data: {
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $(".packageListDiv").html(data);
            $("#package_list_modal").modal('show');
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}


function fetchPackageDetails(package_id){
    var url = $('#base_url').val() + "/service/fetchPackageDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            package_id:package_id
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $(".packageDetailsDiv").html(data);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}

$(document).on("click", ".select_package", function(){
    var package_id = $(this).attr("data-package-id");
    fetchPackageDetails(package_id);
})

$(document).on("keyup", ".searchPakacgeListItem", function(){
    $(".search_package_head_class").hide();
    var filter = $(this).val().toUpperCase();

    if (filter) {
        $(".search_package_head_class:contains(" + filter + ")").show();
    } else {
        $(".search_package_head_class").show();
    }

    var number = 0;
    $(".package_head_table_body").find('.serial_no').each(function(key, val){
        if($(this).css("display") != "none"){
            number = number +1;
            $(this).html(number);
        }
    });
})

$(document).on("keyup", ".searchPakacgeDetailItem", function(){
    $(".search_package_detail_class").hide();
    var filter = $(this).val().toUpperCase();

    if (filter) {
        $(".search_package_detail_class:contains(" + filter + ")").show();
    } else {
        $(".search_package_detail_class").show();
    }

    var number = 0;
    $(".package_detail_table_body").find('.serial_no').each(function(key, val){
        if($(this).css("display") != "none"){
            number = number +1;
            $(this).html(number);
        }
    });
})

$(document).on("click", ".apply_package_btn", function(){
    var package_id = $('input[name="select_package"]:checked').attr('data-package-id');
    var package_name = $('input[name="select_package"]:checked').attr('data-package-name');
    var package_total_amount = $('input[name="select_package"]:checked').attr('data-package-amount');

    $(".selected_package_head_name").html(package_name);
    $(".selected_package_head_div").show();
    $(".service_billing_table_body").empty();
    $(".search_package_detail_class").each(function(key, val) {
        var service_code = $(val).attr('data-service-code');
        var service_desc = $(val).attr('data-service-desc');
        var package_amount = $(val).attr('data-package-amount');

        var row_html = '<tr data-package-id="'+package_id+'" data-indent-id="0" data-detail-id="0" style="background: #FFF;" class="row_class"><td class="serial_no">1</td><td><input class="form-control item_description" type="text" value="'+service_desc+'" data-service-code="'+service_code+'" data-old-service-code="" readonly disabled></td><td><input class="form-control item_price number_class" data-actual-price="'+parseInt(package_amount)+'"  type="text" value="'+package_amount+'" readonly disabled></td><td><input class="form-control quantity number_class" type="text" value="1" data-old-quantity="" readonly disabled></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="'+package_amount+'" data-is-discountable="false"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
        $(".service_billing_table_body").append(row_html);

    });

    $(".service_billing_table_body").find('.deleteRowBtn').hide();
    $(".addNewRowBtn").hide();
    $("#select_investigation_btn").attr('disabled', true);

    calculateTotalAmount();
    resetSerialNumbers();
    $("#package_list_modal").modal('hide');

})



function showIndentScreen() {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
    $('#select_investigation_detail_data').empty();
    $("#select_investigation_modal").modal('show');
    fetchIndentList();
}


function fetchIndentList(){
    var patient_id = $("#patient_uhid_hidden").val();
    var doctor_id = $("#doctor_invest").val();
    var nursing_location = $("#nursing_location").val();
    var pharmacy_location = $("#pharmacy_location").val();
    var visit_type_invest = $("#visit_type_invest").val();
    var status_invest = $("#status_invest").val();
    var investigation_type_text = $("#investigation_type_text").val();
    var from_date_invest = $(".from_date_invest").val();
    var to_date_invest = $(".to_date_invest").val();
    var search_by_date = $("#search_by_date").prop("checked") ? "1" : "2";
    var pending_or_partial = $("#pending_or_partial").prop("checked") ? "1" : "2";
    var url = $('#base_url').val() + "/service/fetchIndentList";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id,
            doctor_id: doctor_id,
            nursing_location: nursing_location,
            pharmacy_location: pharmacy_location,
            visit_type_invest: visit_type_invest,
            status_invest: status_invest,
            investigation_type_text:investigation_type_text,
            from_date_invest: from_date_invest,
            to_date_invest: to_date_invest,
            search_by_date: search_by_date,
            pending_or_partial: pending_or_partial,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            window.selected_patient_id_for_convertions = 0;
            $(".select_investigation_head_data").html(data.select_investigation_head_data);
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}


function fetchInvestigationDetail(head_id, investigation_type) {
    var url = $('#base_url').val() + "/service/fetchInvestigationDetail";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            head_id: head_id,
            investigation_type:investigation_type
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            });
        },
        success: function(data) {
            $('#select_investigation_detail_data').html(data);
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadfix_detail').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
    });

}


function printPatientInvestigation(id) {
    if (id) {
        let url = $('#base_url').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/print_investigation_history",
            data: {
                investigation_head_id: id,
            },
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close();  },1000)</script>');
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }
}


window.selected_patient_id_for_convertions = 0;
window.selected_doctor_id_for_convertions = 0;
window.selected_investigation_type_for_convertions = '';
$(document).on("click", ".invest_select_check", function(e) {
    var patient_id = $(e.currentTarget).parents('tr').attr('data-patient-id');
    var doctor_id = $(e.currentTarget).parents('tr').attr('data-doctor-id');
    var investigation_type = $(e.currentTarget).parents('tr').attr('data-investigation-type');

    if (window.selected_patient_id_for_convertions != 0) {
        if (patient_id != window.selected_patient_id_for_convertions) {
            Command: toastr["error"]("Please select prescription of same patient to convert.");
            $(e.currentTarget).prop('checked', false);
            return;
        }
    }

    if (window.selected_doctor_id_for_convertions != 0) {
        if (doctor_id != window.selected_doctor_id_for_convertions) {
            Command: toastr["error"]("Please select prescription of same doctor to convert.");
            $(e.currentTarget).prop('checked', false);
            return;
        }
    }
    if (window.selected_investigation_type_for_convertions != '') {
        if (investigation_type != window.selected_investigation_type_for_convertions) {
            Command: toastr["error"]("Please select prescription of same investigation type to convert.");
            $(e.currentTarget).prop('checked', false);
            return;
        }
    }

    window.selected_patient_id_for_convertions = parseInt(patient_id);
    window.selected_doctor_id_for_convertions = parseInt(doctor_id);
    window.selected_investigation_type_for_convertions = investigation_type;

    if ($(".invest_select_check:checked").length == 0) {
        window.selected_patient_id_for_convertions = 0;
    }

});

function convertToBill(){
    var investigations_to_convert = [];
    var patient_id = '';
    var doctor_id = '';
    var uhid = '';
    $(".invest_select_check").each(function(key, val) {
        if ($(val).prop("checked")) {
            var investigation_id = $(val).parents('tr').attr('data-detail-id');
            if(investigation_id != ""){
                investigation_id = investigation_id.split(',');
                $.each(investigation_id, function(k, value){
                    investigations_to_convert.push(value);
                });
            }

            patient_id = $(val).parents('tr').attr('data-patient-id');
            doctor_id = $(val).parents('tr').attr('data-doctor-id');
            uhid = $(val).parents('tr').attr('data-patient-uhid');
        }
    });

    if (investigations_to_convert.length == 0) {
        Command: toastr['error']('Please select prescriptions to convert.');
        return;
    }

    if (patient_id == "") {
        Command: toastr['error']('Please select prescriptions to convert.');
        return;
    }

    var url = $('#base_url').val() + "/service/getInvestigationListToConvert";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            investigation_list: investigations_to_convert,
            patient_id: patient_id,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            window.selected_patient_id_for_convertions = 0;
            window.selected_doctor_id_for_convertions = 0;
            window.selected_investigation_type_for_convertions = '';
            $("#select_investigation_modal").modal('hide');
            var patient_details = data.patient_details;

            if(data.bill_tag){
                $("#bill_tag").val(data.bill_tag);
            }
            $("#op_noAjaxDiv").hide();
            $("#patient_id").val(patient_details.patient_id);
            $("#op_no").val(patient_details.uhid);
            $("#op_no_hidden").val(patient_details.patient_id);
            $("#patient_name_bill").html(patient_details.patient_name);
            $("#ip_number").html(patient_details.admission_no);
            $("#consulting_doctor").val(parseInt(doctor_id)).trigger('change');
            $("#current_room").html(patient_details.room_name);
            $("#company").val(patient_details.company_id);
            $("#pricing").val(patient_details.pricing_id);
            $(".patient_advance").val(parseFloat(patient_details.patient_advance).toFixed(2));
            $("#visit_id").val(patient_details.visit_id);
            $("#room_type").val(patient_details.room_type_id);
            $("#payment_type").empty();

            $("#op_no").attr('disabled', true);
            $("#bill_tag").attr('disabled', true);
            $("#out_side_patient").attr('disabled', true);
            $("#out_side_doctor").attr('disabled', true);

            $.each(data.payment_types, function(key, value){
                $("#payment_type").append('<option value="' + key + '">' + value + '</option>');
            });

            fetchPatientPendingBills(false);

            $(".service_billing_table_body").empty();
            $.each(data.service_bill_details, function(key, val){
                var row_html = '<tr data-indent-id="' + val.indent_id + '" data-detail-id="" style="background: #FFF;" class="row_class"><td class="serial_no">'+(key+1)+'</td><td><input class="form-control item_description" type="text" value="'+val.service_desc+'" data-service-code="'+val.service_code+'" data-old-service-code="" readonly disabled></td><td><input class="form-control item_price number_class" data-actual-price="'+parseInt(val.calculatedprice)+'"  type="text" value="'+val.calculatedprice+'"></td><td><input class="form-control quantity number_class" type="text" value="'+parseInt(val.quantity)+'" data-old-quantity=""></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="'+val.item_total+'" data-is-discountable="'+val.is_discountable.toString()+'"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="display:none; border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
                $(".service_billing_table_body").append(row_html);
            })

            checkItemPriceEditable();

            calculateTotalAmount();
            resetSerialNumbers();
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        }
    });
}


$(document).on("click", "#pending_or_partial", function(e) {
    if ($(e.currentTarget).prop('checked')) {
        $("#status_invest").attr('disabled', true);
        $("#status_invest").attr('readonly', true);
    } else {
        $("#status_invest").removeAttr('disabled');
        $("#status_invest").removeAttr('readonly');
    }
});


$(document).on("click", "#service_bill_list_btn", function(e) {
    var base_url = $('#base_url').val();
    var url = base_url + "/billlist/billlist/1";
    document.location.href = url;
});


function replaceItems(e){
    $("#substitute_item_desc").val('');
    $("#substitute_item_desc").removeAttr('data-service-code');
    $("#substitute_item_desc").removeAttr('data-investigation-type');
    $("#substitute_item_modal").modal('show');
    var item_desc = $(e).parents('tr').attr('data-item_name');
    var item_id = $(e).parents('tr').attr('data-id');
    var item_head_id = $(e).parents('tr').attr('data-head-id');
    window.replace_tr = $(e).parents('tr');

    $("#sub_item_desc").val(item_desc);
    $("#sub_item_desc").attr('data-item-id', item_id);
    $("#sub_item_desc").attr('data-item-head-id', item_head_id);
}


$('#substitute_item_desc').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: "",
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function selectServiceItemForReplace(service_code, service_desc, investigation_type){
    $("#substitute_item_desc").val(service_desc);
    $("#substitute_item_desc").attr('data-service-code', service_code);
    $("#substitute_item_desc").attr('data-investigation-type', investigation_type);
    $("#substitute_item_descAjaxDiv").hide();
}


$(document).on("click", ".substitute_item_btn", function(e) {
    var substitute_item_desc = $("#substitute_item_desc").val();
    var substitute_service_code = $("#substitute_item_desc").attr('data-service-code');
    var item_id = $("#sub_item_desc").attr('data-item-id');
    var investigation_type = $("#substitute_item_desc").attr('data-investigation-type');

    if(substitute_item_desc && substitute_service_code && item_id){
        let url = $('#base_url').val();
        $.ajax({
            type: "POST",
            url: url + "/service/substituteServiceItem",
            data: {
                item_id: item_id,
                substitute_service_code:substitute_service_code,
                investigation_type:investigation_type
            },
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(data) {
                if(data.status == 1){
                    $("#substitute_item_modal").modal('hide');
                    var service_head_id = $(window.replace_tr).attr('data-head_id');
                    var service_type = $(window.replace_tr).attr('data-investigation-type');
                    var outside_investigation_count = $('tr[data-invest-id="'+service_head_id+'"]').attr('data-outside-investigation-count');
                    $('tr[data-invest-id="'+service_head_id+'"][data-investigation-type="'+service_type+'"]').find('.fetchInvestigationDetailBtn').trigger('click');
                    if(parseInt(outside_investigation_count) > 0){
                        fetchIndentList();
                    }
                }
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr["error"]("Please select service item for replacement");
    }
});


$(document).on("click", ".page-link", function (e) {
    e.preventDefault();
    var url = $(this).attr("href");
    var patient_id = $("#patient_uhid_hidden").val();
    var doctor_id = $("#doctor_invest").val();
    var nursing_location = $("#nursing_location").val();
    var pharmacy_location = $("#pharmacy_location").val();
    var visit_type_invest = $("#visit_type_invest").val();
    var status_invest = $("#status_invest").val();
    var investigation_type_text = $("#investigation_type_text").val();
    var from_date_invest = $(".from_date_invest").val();
    var to_date_invest = $(".to_date_invest").val();
    var search_by_date = $("#search_by_date").prop("checked") ? "1" : "2";
    var pending_or_partial = $("#pending_or_partial").prop("checked") ? "1" : "2";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id,
            doctor_id: doctor_id,
            nursing_location: nursing_location,
            pharmacy_location: pharmacy_location,
            visit_type_invest: visit_type_invest,
            status_invest: status_invest,
            investigation_type_text:investigation_type_text,
            from_date_invest: from_date_invest,
            to_date_invest: to_date_invest,
            search_by_date: search_by_date,
            pending_or_partial: pending_or_partial,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            window.selected_patient_id_for_convertions = 0;
            $(".select_investigation_head_data").html(data.select_investigation_head_data);
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
    return false;

});

function checkItemPriceEditable(){
    $(".item_price").each(function(key, val){
        if($(val).attr('data-is-price-editable') == 'true'){
            $(val).removeAttr('disabled');
            $(val).removeAttr('readonly');
        }
    })
}
