//document ready function
$(document).ready(function() {
    $('.select2').select2();
    // loadUserGroups(0);
});

$("input[name='inv_user_type']").on('change',function(){
    if ($(this).is(':checked') && $(this).val() == 1) {
        $('#nursing_station_block').css('display','none');
        $('#doctor_selection_block').css('display','block');
        $('#doctor_id').val('').change();
        $('#nursing_station').val('').change();
        $('#group_list_data').html('');

    }else{
        $('#nursing_station_block').css('display','block');
        $('#doctor_selection_block').css('display','none');
        $('#doctor_id').val('').change();
        $('#nursing_station').val('').change();
        $('#group_list_data').html('');
    }
});

$(document).on('keyup', 'input[name="search_inv_item"]', function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var search_type = $("input[name='i_search_type']:checked").val();

    if (value.match(keycheck) || event.keyCode == '8' || event.keyCode == '96') {
    var search_inv_item = $(this).val();

    if (search_inv_item == "") {
        $("#invAjaxDiv").html("");
    }else{
        var url = '';
        $.ajax({
        type: "GET",
            url: $('#base_url').val() + "/favourite_investigation/favouriteInvItemSearch",
            data: "search_key_string=" + search_inv_item +'&search_type='+search_type,
            beforeSend: function () {

            $("#invAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
                $("#invAjaxDiv").html(html).show();
                $("#invAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {

            }
        });
    }

 }else {
        ajax_list_key_down('invAjaxDiv',event);
    }
});

/* setting for enter key press in ajaxDiv listing */
$("#search_inv_item").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter('invAjaxDiv');
        return false;
    }
});


//close medicine search
$(document).on('click', '.investigation-list-div > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".investigation-list-div").hide();
});


// function SaveGroup() {

//     let group_name = $('#groupname').val();
//     var grouplist = [];


//     if ( group_name != '') {
//         var url = $('#base_url').val() + "/favourite_investigation/save-new-group";
//         $('.inv-grp-name').each(function(){
//             grouplist.push($(this).html());
//         });

//         let isInArray = grouplist.includes(group_name);
//         if(isInArray ==false){

//             $.ajax({
//                 type: "GET",

//                 url: url,
//                 data: 'group_name=' +group_name,
//                 beforeSend: function () {

//                 },
//                 success: function (data) {

//                 },
//                 complete: function () {
//                     $("#group_add_modal").modal('hide');
//                 }
//             });

//         }else{
//             toastr.warning("Group name already exists!");
//         }

//     }else{
//         toastr.warning("Type Group Name");
//     }
// }

function loadUserGroups(){
    var user_type = $("input[name='inv_user_type']:checked").val();
    var doctor_id = $("#doctor_id").val();
    var nursing_station = $('#nursing_station').val();
    var url = $('#base_url').val() + "/favourite_investigation/load-user-groups";
    var dataparams = {
        user_type:user_type,
        doctor_id:doctor_id,
        nursing_station:nursing_station,
    };
    $.ajax({
        type: "GET",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#group_list").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            $('#inv_list_data').html('');
        },
        success: function (data) {
            $("#group_list_data").html(data);
        },
        complete: function () {
            resetBootstrapTable();
            $("#group_list").LoadingOverlay("hide");
        },
    });
}

$('#doctor_id').on('change', function(){
    if($('#doctor_id').val() !=''){
        loadUserGroups();
    }
});
$('#nursing_station').on('change', function(){
    if($('#nursing_station').val() !=''){
        loadUserGroups();
    }
});


function fetchFavoriteGroupItems(group_id) {
    var group_id_array = [];
    $("input[name='fav_group[]'").prop('checked',false);
    $('#fav_group_'+group_id).prop('checked','checked');
    $("input[name='fav_group[]']:checked").each(function () {
        if ($(this).is(':checked')) {
            group_id_array.push($(this).val());
        }
    });

    window.favoriteGroupId = group_id;
    let url = $('#base_url').val();
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/favourite_investigation/fetchFavoriteGroupItems",
        data: {
            group_id: group_id_array,
        },
        beforeSend: function () {
            $('#inv_list').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (data) {
            $('#inv_list_data').html(data);
        },
        complete: function () {
            $('#inv_list').LoadingOverlay("hide");
        }
    });

}

$('#groupname').on('keyup', function(event){
    if(event.keyCode == 13){
        saveGroup();
    }
});
function saveGroup(){
    var value = $('#groupname').val();
    var user_type = $("input[name='inv_user_type']:checked").val();
    var doctor_id = $("#doctor_id").val();
    var nursing_station = $('#nursing_station').val();

    if(user_type == 1 && doctor_id ==''){
        toastr.warning("Select Doctor!");
        return false;
    }
    if(user_type == 2 && nursing_station ==''){
        toastr.warning("Select Nursing Station!");
        return false;
    }

    var dataparms = {
        group_name:value,
        user_type:user_type,
        doctor_id:doctor_id,
        nursing_station:nursing_station,
    };


    if (value != '' && user_type != ''){
        var url = $('#base_url').val() + "/favourite_investigation/save-new-group";
        $.ajax({
            type: "GET",
            url: url,
            data: dataparms,
            beforeSend: function () {
                $("#group_list").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (data) {
                if(data == 1){
                    toastr.success("Group saved successfully!");
                    loadUserGroups();
                    $('#groupname').val('');
                }else{
                    toastr.error("Something went wrong!");
                }
            },
            complete: function () {
                $("#group_list").LoadingOverlay("hide");
            }
        });
    }else{
        toastr.warning("Enter group name!");
    }
}

function deleteFavoriteGroup(id){
    var url = $('#base_url').val() + "/favourite_investigation/delete-favorite-group";
    $.ajax({
        type: "GET",
        url: url,
        data: 'id=' +id,
        beforeSend: function () {
            $("#group_list").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (data) {
            if(data == 1){
                toastr.success("Group deleted successfully!");
                loadUserGroups();
            }else{
                toastr.error("Something went wrong!");
            }
        },
        complete: function () {
            $("#group_list").LoadingOverlay("hide");
        }
    });
}

function SaveFavItem(service_code,dept_name){
    var user_id = $("#doctor_id").val();
    var user_type = $("input[name='inv_user_type']:checked").val();
    var group_id_array = [];
    var group_id =$("input[name='fav_group[]']:checked").val();
    $("input[name='fav_group[]']:checked").each(function () {
        if ($(this).is(':checked')) {
            group_id_array.push($(this).val());
        }
    });


    if (group_id_array.length > 0) {
        var url = $('#base_url').val() + "/favourite_investigation/save-favorite-item";

        $.ajax({
            type: "GET",
            url: url,
            data: 'service_code=' +service_code+'&user_type='+user_type+'&dept_name='+dept_name+'&user_id='+user_id+'&group_id_array='+group_id_array,
            beforeSend: function () {
                $('#inv_list').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (data) {
                if(data == 1){
                    toastr.success("Item saved successfully!");
                    fetchFavoriteGroupItems(group_id);
                }else{
                    toastr.error("Something went wrong!");
                }
            },
            complete: function () {
                $('#inv_list').LoadingOverlay("hide");
                $('.ajaxSearchBox').val('');
                $('.ajaxSearchBox').hide();
            },
        });

    }else{
        toastr.warning("Select atleast one group!");
        return false;
    }
}

function deleteFavItem(id){
    var url = $('#base_url').val() + "/favourite_investigation/delete-favorite-items";
    $.ajax({
        type: "GET",
        url: url,
        data: 'id=' +id,
        beforeSend: function () {

        },
        success: function (data) {
            if(data == 1){
                toastr.success("Item deleted successfully!");
                fetchFavoriteGroupItems(window.favoriteGroupId);
            }else{
                toastr.error("Something went wrong!");
            }
        },
        complete: function () {
        }
    });
}



