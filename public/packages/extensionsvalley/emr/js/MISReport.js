$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#patient_id_hidden').val() != "") {
                $('#patient_id_hidden').val('');
            }
            var patient_name = $(this).val();
            patient_name = patient_name.trim();
            if (patient_name == "") {
                $("#patient_idAjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "patient_name_search=" + patient_name,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });
});

var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();

function fillPatientValues(uhid, patient_name) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
}

function getInsurance_type() {
    var patient_type = $('#patient_type').val();
    $('#esi_type_id').hide();
    $('#echs_type_id').hide();
    $('#insurance_type_id').hide();
    if (patient_type == 'esi') {
        $('#esi_type_id').show();
    } else if (patient_type == 'echs') {
        $('#echs_type_id').show();
    } else if (patient_type == '3') {
        $('#insurance_type_id').show();
    }

}

function getReportData() {
    var url = base_url + "/reports/getMISReport";
    var bill_from = $('#bill_fromdate').val();
    var bill_to = $('#bill_todate').val();
    var uhid = $('#patient_uhid_hidden').val();
    var visit_status = $('#visit_status').val();
    var insurance_type = $('#insurance_type').val();
    var echs_type = $('#echs_type').val();
    var esi_type = $('#esi_type').val();
    var patient_type = $('#patient_type').val();
    var service_type = $('#service_type').val();
    var param = { _token: token, bill_from: bill_from, echs_type: echs_type, esi_type: esi_type, patient_type: patient_type, bill_to: bill_to, insurance_type: insurance_type, service_type: service_type, uhid: uhid, visit_status: visit_status };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#search_results').attr('disabled', true);
            $('#ResultDataContainer').css('display', 'block');
            $('#serachResultSpin').removeClass('fa fa-search');
            $('#serachResultSpin').addClass('fa fa-spinner');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#search_results').attr('disabled', false);
            $('#serachResultSpin').removeClass('fa fa-spinner');
            $('#serachResultSpin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }
    });

}
function search_clear() {
    var current_date = $('#current_date').val();
    $('#bill_fromdate').val(current_date);
    $('#bill_todate').val(current_date);
    $('#patient_name').val('');
    $('#patient_id_hidden').val('');
    $('#patient_uhid_hidden').val('');
    $('#insurance_type').val('').select2();
    $('#esi_type').val('').select2();
    $('#esi_type').val('').select2();
    $('#patient_type').val('ALL').select2();
    $('#service_type').val('').select2();
    $('#visit_status').val('ALL').select2();
    $('#esi_type_id').hide();
    $('#echs_type_id').hide();
    $('#insurance_type_id').hide();
    getReportData();
}
