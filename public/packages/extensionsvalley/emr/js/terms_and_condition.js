
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });

    });

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
        data.terms_and_condition = $(".terms_and_condition").val();
        data.status = $(".status").val();

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".terms_and_condition_add").val().trim() == ''){
            Command: toastr["error"]("Please enter terms");
            return;
        }
        if($(".status_add").val().trim() == ''){
            Command: toastr["error"]("Please select status");
            return;
        }

        data.terms_and_condition_id = $(".reference_id").val();
        data.terms_and_condition = $(".terms_and_condition_add").val();
        data.status = $(".status_add").val();
        data.status_id = $(".reference_id").val();
       


        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {  
                $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                Command: toastr["success"]("success");
                getFilteredItems(window.search_url);
                resetForm();
            },
            complete: function () {

            }
        });
    }

    function editItem(element){
        let terms_and_condition_id = $(element).parents('tr').attr('data-id');
        let terms_and_condition_name = $(element).parents('tr').attr('data-tac-name');
        let branch_id = $(element).parents('tr').attr('data-branch-id');
        let status = $(element).parents('tr').attr('data-status');

        $(".reference_id").val(terms_and_condition_id);
        $(".terms_and_condition_add").val(terms_and_condition_name);
        $(".status_add").val(status);
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
        
    }

    function deleteItem(element){

        let confirm = window.confirm('Are you sure want to delete ?');
        if(confirm){
            let data = {};
            let url = $(".table_body_contents").attr('data-delete-url');
            let terms_and_condition_id = $(element).parents('tr').attr('data-id');
            data.terms_and_condition_id = terms_and_condition_id;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function () {
                    $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                },
                success: function (data) {  
                    $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                },
                complete: function () { 

                }
            });
        }
        
    }

    function resetForm(){
        $(".reference_id").val('');
        $(".terms_and_condition_add").val('');
        $(".status_add").val('');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

