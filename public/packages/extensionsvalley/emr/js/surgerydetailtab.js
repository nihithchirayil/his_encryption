$(document).ready(function () {
    $(function () {
        $("#progressbar li:first-child a").tab("show");
    });
    loadSurgeryMaster();
    $("#progressbar li").click(function (e) {
        $(this).find('a').tab('show');
    });
});


var base_url = $('#base_url').val();
var token = $('#c_token').val();
var instrument_list = [];
var instrument_row_id = 1;
var medicine_list = [];
var medicine_row_id = 1;
var consumables_list = [];
var consumables_row_id = 1;
var equipment_list = [];
var equipment_row_id = 1;

function loadSurgeryMaster() {
    var surgery_id = $('#surgery_id').val();
    var surgery_full_deatils = $('#surgery_full_deatils').val();
    if (surgery_id != 0) {
        surgery_array = JSON.parse(atob(surgery_full_deatils));
        console.log(surgery_array);
        var surgery_name = surgery_array.surgery_master[0].name;
        var service_code = surgery_array.surgery_master[0].service_code;
        var detail_amount = surgery_array.surgery_master[0].approximate_amount;
        $('#service_item').val(surgery_name);
        $('#service_item_hidden').val(service_code);
        $('#detail_amount').val(detail_amount);

        $(surgery_array.surgery_instruments).each(function (index, value) {
            addInstrument(value.service_desc, number_format(value.amount, 2, '.', ''), value.service_code);
        });
        $(surgery_array.surgery_equipment).each(function (index, value) {
            addequipment(value.service_desc, number_format(value.amount, 2, '.', ''), value.service_code);
        });

        $(surgery_array.surgery_medicine).each(function (index, value) {
            addMedicineListData(value.item_code, value.item_desc, number_format(value.amount, 2, '.', ''), value.generic_name, value.quantity);
        });

        $(surgery_array.surgery_consumables).each(function (index, value) {
            addConsumablesListData(value.item_code, value.item_desc, number_format(value.amount, 2, '.', ''), value.quantity);
        });
    }
}

function getPreviousTab() {
    $(".nav-tabs > .active").prev("li").find("a").tab("show");
}


$('#service_item').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('service_itemAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#service_item').val();
        if (search_key == "") {
            $('#service_itemAjaxDiv').hide();
            $("#service_item_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var surgery_id = $('#surgery_id').val();
            var param = {
                _token: token,
                surgery_id: surgery_id,
                search_key: search_key,
                search_key_id: 'service_item'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#service_itemAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#service_itemAjaxDiv").html(html).show();
                    $("#service_itemAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('service_itemAjaxDiv', event);
    }
});


$('#medicine_serach').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('medicine_serachAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#medicine_serach').val();
        if (search_key == "") {
            $('#medicine_serachAjaxDiv').hide();
            $("#medicine_serach_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(medicine_list);
            var medicine_search = $('input[name="medicine_name_search"]:checked').val();
            var param = {
                _token: token,
                medicine_search: medicine_search,
                list_array: listArrayString,
                search_key: search_key,
                search_key_id: 'medicine_serach'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#medicine_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#medicine_serachAjaxDiv").html(html).show();
                    $("#medicine_serachAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('medicine_serachAjaxDiv', event);
    }
});



$('#consumables_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('consumables_serachAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#consumables_search').val();
        if (search_key == "") {
            $('#consumables_serachAjaxDiv').hide();
            $("#consumables_search_hidden`").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(consumables_list);
            var medicine_search = $('input[name="medicine_name_search"]:checked').val();
            var param = {
                _token: token,
                medicine_search: medicine_search,
                list_array: listArrayString,
                search_key: search_key,
                search_key_id: 'consumables_search'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#consumables_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#consumables_serachAjaxDiv").html(html).show();
                    $("#consumables_serachAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('consumables_serachAjaxDiv', event);
    }
});


$('#equipment_list').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('equipment_listAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#equipment_list').val();
        if (search_key == "") {
            $('#equipment_listAjaxDiv').hide();
            $("#equipment_list_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(equipment_list);
            var param = {
                _token: token,
                list_array: listArrayString,
                search_key: search_key,
                search_key_id: 'equipment_list'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#equipment_listAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#equipment_listAjaxDiv").html(html).show();
                    $("#equipment_listAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('equipment_listAjaxDiv', event);
    }
});


$('#instrument_list').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('instrument_listAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#instrument_list').val();
        if (search_key == "") {
            $('#instrument_listAjaxDiv').hide();
            $("#instrument_list_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(instrument_list);
            var param = {
                _token: token,
                list_array: listArrayString,
                search_key: search_key,
                search_key_id: 'instrument_list'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#instrument_listAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#instrument_listAjaxDiv").html(html).show();
                    $("#instrument_listAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('instrument_listAjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function updateListData(id, name, serach_key_id, price, master_id) {
    if (serach_key_id == 'service_item') {
        if (master_id) {
            bootbox.confirm({
                message: "Surgery master " + titleCase(name) + " already created. <br> Do you want to edit ?",
                buttons: {
                    'confirm': {
                        label: "Edit",
                        className: 'btn-primary',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-warning'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var url = base_url + "/surgery/getSurgeryMaster/" + master_id;
                        document.location.href = url;
                    } else {
                        $('#' + serach_key_id + '_hidden').val('');
                        $('#' + serach_key_id).val(htmlDecode(''));
                        $('#' + serach_key_id).attr('title', htmlDecode(''));
                        $('#detail_amount').val('');
                    }
                }
            });
        } else {
            $('#detail_amount').val(number_format(price, 2, '.', ''));
        }
    } else if (serach_key_id == 'instrument_list') {
        $('#' + serach_key_id).val('');
        addInstrument(name, price, id);
    } else if (serach_key_id == 'equipment_list') {
        $('#' + serach_key_id).val('');
        addequipment(name, price, id);
    }
}

function fillSearchDetials(id, name, serach_key_id, price, master_id = 0) {
    console.log(serach_key_id);
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(htmlDecode(name));
    $('#' + serach_key_id + '_surgery_name').val(htmlDecode(name));
    $('#' + serach_key_id).attr('title', htmlDecode(name));
    $('.ajaxSearchBox').hide();
    updateListData(id, name, serach_key_id, price, master_id);
}

function fillMedicineData(code, desc, serach_key_id, stock, price, generic_name) {
    $('#' + serach_key_id + '_hidden').val(code);
    $('#' + serach_key_id).val('');
    $('.ajaxSearchBox').hide();
    if (serach_key_id == 'medicine_serach') {
        addMedicineListData(code, desc, price, generic_name, 1);
    } else if (serach_key_id == 'consumables_search') {
        addConsumablesListData(code, desc, price, 1);
    }
}

function validateNumber(obj) {
    obj.value = obj.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
}

function getTotalInstrumentPrice(from_type) {
    var total = 0.0;
    var price = 0.0;
    if ('.' + from_type + '_price') {
        $('.' + from_type + '_price').each(function (index) {
            price = 0.0;
            if ($(this).val()) {
                price = $(this).val();
            }
            total += parseFloat(price);
        });
        $('#' + from_type + '_charge').html(number_format(total, 2, '.', ''));
    }
    if ('.' + from_type + '_class') {
        row_ct = 1;
        $('.' + from_type + '_class').each(function (i) {
            $(this).find('.' + from_type + '_count_class').text(row_ct);
            row_ct++;
        });
    }
}


function addInstrument(name, price, code) {
    $('#instrumentlist').append('<tr class="Instrument_class" id="Instrument_row_id' + instrument_row_id + '"><td class="common_td_rules Instrument_count_class"></td><td class="common_td_rules">' + name + '</td><td class="td_common_numeric_rules"><input class="form-control Instrument_price" oninput="validateNumber(this)" value="' + price + '" autocomplete="off" onkeyup="getTotalInstrumentPrice(\'Instrument\')" type="text" id="Instrument_price' + instrument_row_id + '" name="Instrument_price"> <input class="form-control" type="hidden" value="' + code + '" name="Instrument_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="Instrument_price"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + instrument_row_id + ', \'' + code + '\', \'Instrument\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    instrument_row_id++;
    instrument_list.push(code);
    getTotalInstrumentPrice('Instrument');
}

function addequipment(name, price, code) {
    $('#equipmentlist').append('<tr class="equipment_class" id="equipment_row_id' + equipment_row_id + '"><td class="common_td_rules equipment_count_class"></td><td class="common_td_rules">' + name + '</td><td class="td_common_numeric_rules"><input class="form-control equipment_price" oninput="validateNumber(this)" value="' + price + '" autocomplete="off" onkeyup="getTotalInstrumentPrice(\'equipment\')" type="text" id="equipment_price' + equipment_row_id + '" name="equipment_price"> <input class="form-control" type="hidden" value="' + code + '" name="equipment_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="equipment_price"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + equipment_row_id + ', \'' + code + '\', \'equipment\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    equipment_row_id++;
    equipment_list.push(code);
    getTotalInstrumentPrice('equipment');
}

function addMedicineListData(code, desc, price, generic_name, qty) {
    $('#medicinelist').append('<tr class="Medicine_class" id="Medicine_row_id' + medicine_row_id + '"><td class="common_td_rules Medicine_count_class"><td class="common_td_rules">' + desc + '</td><td class="common_td_rules">' + generic_name + '</td><td class="common_td_rules"><input class="form-control" type="hidden" value="' + code + '" name="medicine_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="medicine_price"><input class="form-control" oninput="validateNumber(this)" value="' + qty + '" autocomplete="off" onkeyup="getListTotalQty(' + medicine_row_id + ',\'Medicine\',' + price + ')" type="text" id="Medicine_qty' + medicine_row_id + '" name="medicine_qty"></td> <td class="td_common_numeric_rules"> ' + price + ' </td><td id="Medicine_price_total' + medicine_row_id + '" class="Medicine_price td_common_numeric_rules"> ' + price + ' </td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + medicine_row_id + ', \'' + code + '\', \'Medicine\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    getListTotalQty(medicine_row_id, 'Medicine', price);
    medicine_row_id++;
    medicine_list.push(code);
    calculateTableDataPrice('Medicine');
}

function addConsumablesListData(code, desc, price, qty) {
    $('#consumableslist').append('<tr class="Consumables_class" id="Consumables_row_id' + consumables_row_id + '"><td class="common_td_rules Consumables_count_class"><td class="common_td_rules">' + desc + '</td><td class="common_td_rules"><input class="form-control" type="hidden" value="' + code + '" name="consumables_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="consumables_price"><input class="form-control" oninput="validateNumber(this)" value="' + qty + '" autocomplete="off" onkeyup="getListTotalQty(' + consumables_row_id + ',\'Consumables\',' + price + ')" type="text" id="Consumables_qty' + consumables_row_id + '" name="Consumables_qty"></td><td class="td_common_numeric_rules"> ' + price + ' </td><td id="Consumables_price_total' + consumables_row_id + '" class="Consumables_price td_common_numeric_rules"> ' + price + ' </td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + consumables_row_id + ', \'' + code + '\', \'Consumables\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    getListTotalQty(consumables_row_id, 'Consumables', price);
    consumables_row_id++;
    consumables_list.push(code);
    calculateTableDataPrice('Consumables');
}

function getListTotalQty(list_id, from_type, price) {
    var qty = $('#' + from_type + '_qty' + list_id).val();
    if (!qty) {
        qty = 0;
    }
    var total = parseFloat(price * qty);
    $('#' + from_type + '_price_total' + list_id).html(number_format(total, 2, '.', ''));
    calculateTableDataPrice(from_type);
}


function calculateTableDataPrice(from_type) {
    var total = 0.0;
    var price = 0.0;
    if ('.' + from_type + '_price') {
        $('.' + from_type + '_price').each(function (index) {
            price = 0.0;
            if ($(this).html()) {
                price = $(this).html();
            }
            total += parseFloat(price);
        });
        $('#' + from_type + '_charge').html(number_format(total, 2, '.', ''));
    }
    if ('.' + from_type + '_class') {
        row_ct = 1;
        $('.' + from_type + '_class').each(function (i) {
            $(this).find('.' + from_type + '_count_class').text(row_ct);
            row_ct++;
        });
    }
}


function removeTableData(list_id, code, from_type) {
    bootbox.confirm({
        message: "Are your sure to want remove this " + from_type + " ?",
        buttons: {
            'confirm': {
                label: "Remove",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $('#' + from_type + '_row_id' + list_id).remove();
                if (from_type == 'Instrument') {
                    var index = instrument_list.indexOf(code);
                    if (index > -1) {
                        instrument_list.splice(index, 1);
                    }
                } else if (from_type == 'Medicine') {
                    var index = medicine_list.indexOf(code);
                    if (index > -1) {
                        medicine_list.splice(index, 1);
                    }
                } else if (from_type == 'Consumables') {
                    var index = consumables_list.indexOf(code);
                    if (index > -1) {
                        consumables_list.splice(index, 1);
                    }
                }
                calculateTableDataPrice(from_type);
            }
        }
    });
}

function searchbyName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("result_data_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function getDepartmentlist() {
    var url = base_url + "/surgery/getDepartmentList";
    var surgery_id = $('#surgery_id').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            surgery_id: surgery_id
        },
        beforeSend: function () {
            $('#department_list').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#department_list').html(html);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

        },
        complete: function () {
            $('#department_list').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}


function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

$(document).on("click", "#item_search_btn", function (e) {
    $('#issue_search_box').focus();
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
});


function getLevelSetting() {
    var surgery_levels = new Array();
    $('#surgery_levels_table tr').each(function () {
        var detail_amount = $(this).find("input[name='detail_amount']").val();
        var level_id = $(this).find("input[name='surgery_levels']").val();
        var edit_surgery_price = $('#edit_surgery_price' + level_id).is(":checked");
        surgery_levels.push({
            'detail_amount': detail_amount,
            'level_id': level_id,
            'edit_surgery_price': edit_surgery_price,
        });
    });
    return surgery_levels;
}


function saveSurgeryDeatils() {
    var service_item = $('#service_item').val();
    var surgery_name = $('#service_item_surgery_name').val();
    var service_code = $('#service_item_hidden').val();
    var surgery_id = $('#surgery_id').val();
    var detail_amount = $('#detail_amount').val();
    var surgery_levels_string = JSON.stringify(getLevelSetting());

    if (service_code) {
        if (detail_amount) {
            var param = {
                _token: token,
                surgery_id: surgery_id,
                service_item: service_item,
                service_code: service_code,
                surgery_name: surgery_name,
                detail_amount: detail_amount,
                surgery_levels_string: surgery_levels_string
            };
            var url = base_url + "/surgery/saveSurgeryDepartment";
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#departmentBtn').attr('disabled', true);
                    $('#departmentSpin').removeClass('fa fa-save');
                    $('#departmentSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status == 'success') {
                        toastr.success(obj.message);
                        $('#surgery_id').val(obj.surgery_id);
                        $(".nav-tabs > .active").next("li").find("a").tab("show");
                    } else {
                        toastr.error(obj.message);
                    }

                },
                complete: function () {
                    $('#departmentBtn').attr('disabled', false);
                    $('#departmentSpin').removeClass('fa fa-spinner fa-spin');
                    $('#departmentSpin').addClass('fa fa-save');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });

        } else {
            toastr.warning('Please select approximate amount');
            $('#detail_amount').focus();
        }
    } else {
        toastr.warning('Please select surgery name');
        $('#service_item').focus();
    }
}


function saveSurgeryInstrument(from_type) {
    var surgeryListDataArray = new Array();
    $('#instrumentlist tr').each(function () {
        var serive_code = $(this).find("input[name='Instrument_serive_code']").val();
        var price = $(this).find("input[name='Instrument_price']").val();
        surgeryListDataArray.push({
            'serive_code': serive_code,
            'price': price,
        });
    });
    var surgeryListDataString = JSON.stringify(surgeryListDataArray);
    var surgery_id = $('#surgery_id').val();
    if (surgery_id != 0) {
        var param = {
            _token: token,
            from_type: from_type,
            surgery_id: surgery_id,
            surgeryListDataString: surgeryListDataString
        };
        var url = base_url + "/surgery/saveInstrumentCharges";
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveSurgeryInstrumentBtn').attr('disabled', true);
                $('#saveSurgeryInstrumentSpin').removeClass('fa fa-save');
                $('#saveSurgeryInstrumentSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == 'success') {
                    toastr.success(obj.message);
                    $(".nav-tabs > .active").next("li").find("a").tab("show");
                } else {
                    toastr.error(obj.message);
                }
            },
            complete: function () {
                $('#saveSurgeryInstrumentBtn').attr('disabled', false);
                $('#saveSurgeryInstrumentSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveSurgeryInstrumentSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning('Please save surgery details');
    }
}

function saveSurgeryEquipment(from_type) {
    var surgeryListDataArray = new Array();
    $('#equipmentlist tr').each(function () {
        var serive_code = $(this).find("input[name='equipment_serive_code']").val();
        var price = $(this).find("input[name='equipment_price']").val();
        surgeryListDataArray.push({
            'serive_code': serive_code,
            'price': price,
        });
    });
    var surgeryListDataString = JSON.stringify(surgeryListDataArray);
    var surgery_id = $('#surgery_id').val();
    if (surgery_id != 0) {
        var param = {
            _token: token,
            from_type: from_type,
            surgery_id: surgery_id,
            surgeryListDataString: surgeryListDataString
        };
        var url = base_url + "/surgery/saveInstrumentCharges";
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveSurgeryequipmentBtn').attr('disabled', true);
                $('#saveSurgeryequipmentSpin').removeClass('fa fa-save');
                $('#saveSurgeryequipmentSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == 'success') {
                    toastr.success(obj.message);
                    $(".nav-tabs > .active").next("li").find("a").tab("show");
                } else {
                    toastr.error(obj.message);
                }
            },
            complete: function () {
                $('#saveSurgeryequipmentBtn').attr('disabled', false);
                $('#saveSurgeryequipmentSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveSurgeryequipmentSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning('Please save surgery details');
    }
}


function saveSurgeryMedicine(from_type) {
    var surgeryListDataArray = new Array();
    $('#medicinelist tr').each(function () {
        var medicine_qty = $(this).find("input[name='medicine_qty']").val();
        var serive_code = $(this).find("input[name='medicine_serive_code']").val();
        var medicine_price = $(this).find("input[name='medicine_price']").val();
        surgeryListDataArray.push({
            'medicine_qty': medicine_qty,
            'serive_code': serive_code,
            'medicine_price': medicine_price,
        });
    });
    var surgeryListDataString = JSON.stringify(surgeryListDataArray);
    var surgery_id = $('#surgery_id').val();
    if (surgery_id != 0) {
        var param = {
            _token: token,
            from_type: from_type,
            surgery_id: surgery_id,
            surgeryListDataString: surgeryListDataString
        };
        var url = base_url + "/surgery/saveInstrumentCharges";
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveSurgeryMedicineBtn').attr('disabled', true);
                $('#saveSurgeryMedicineSpin').removeClass('fa fa-save');
                $('#saveSurgeryMedicineSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == 'success') {
                    toastr.success(obj.message);
                    $(".nav-tabs > .active").next("li").find("a").tab("show");
                } else {
                    toastr.error(obj.message);
                }
            },
            complete: function () {
                $('#saveSurgeryMedicineBtn').attr('disabled', false);
                $('#saveSurgeryMedicineSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveSurgeryMedicineSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning('Please save surgery details');
    }
}


function saveSurgeryConsumables(from_type) {
    var surgeryListDataArray = new Array();
    $('#consumableslist tr').each(function () {
        var consumables_qty = $(this).find("input[name='Consumables_qty']").val();
        var serive_code = $(this).find("input[name='consumables_serive_code']").val();
        var consumable_price = $(this).find("input[name='consumables_price']").val();
        surgeryListDataArray.push({
            'consumables_qty': consumables_qty,
            'serive_code': serive_code,
            'consumable_price': consumable_price,
        });
    });
    var surgeryListDataString = JSON.stringify(surgeryListDataArray);
    var surgery_id = $('#surgery_id').val();
    if (surgery_id != 0) {
        var param = {
            _token: token,
            from_type: from_type,
            surgery_id: surgery_id,
            surgeryListDataString: surgeryListDataString
        };
        var url = base_url + "/surgery/saveInstrumentCharges";
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveSurgeryConsumablesBtn').attr('disabled', true);
                $('#saveSurgeryConsumablesSpin').removeClass('fa fa-save');
                $('#saveSurgeryConsumablesSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == 'success') {
                    toastr.success(obj.message);
                    $(".nav-tabs > .active").next("li").find("a").tab("show");
                } else {
                    toastr.error(obj.message);
                }
            },
            complete: function () {
                $('#saveSurgeryConsumablesBtn').attr('disabled', false);
                $('#saveSurgeryConsumablesSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveSurgeryConsumablesSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning('Please save surgery details');
    }
}


function saveCompleteSurgery() {
    var service_item = $('#service_item').val();
    var service_code = $('#service_item_hidden').val();
    var surgery_id = $('#surgery_id').val();
    var detail_amount = $('#detail_amount').val();
    var surgeryFullArray = {}
    var surgerySubArray = new Array();
    if (service_code) {
        if (detail_amount) {
            if (surgery_id != 0) {
                $('#equipmentlist tr').each(function () {
                    var serive_code = $(this).find("input[name='equipment_serive_code']").val();
                    var price = $(this).find("input[name='equipment_price']").val();
                    surgerySubArray.push({
                        'serive_code': serive_code,
                        'price': price,
                    });
                });
                surgeryFullArray['Equipment'] = surgerySubArray;
                surgerySubArray = new Array();

                $('#medicinelist tr').each(function () {
                    var medicine_qty = $(this).find("input[name='medicine_qty']").val();
                    var serive_code = $(this).find("input[name='medicine_serive_code']").val();
                    var medicine_price = $(this).find("input[name='medicine_price']").val();
                    surgerySubArray.push({
                        'medicine_qty': medicine_qty,
                        'serive_code': serive_code,
                        'medicine_price': medicine_price,
                    });
                });

                surgeryFullArray['Medicine'] = surgerySubArray;
                surgerySubArray = new Array();
                $('#consumableslist tr').each(function () {
                    var consumables_qty = $(this).find("input[name='Consumables_qty']").val();
                    var serive_code = $(this).find("input[name='consumables_serive_code']").val();
                    var consumable_price = $(this).find("input[name='consumables_price']").val();
                    surgerySubArray.push({
                        'consumables_qty': consumables_qty,
                        'serive_code': serive_code,
                        'consumable_price': consumable_price,
                    });
                });
                surgeryFullArray['Consumables'] = surgerySubArray;
                surgerySubArray = new Array();

                $('#new_surgergy_division_per tr').each(function () {
                    var surgergy_percentage = $(this).find("input[name='new_surgergy_percentage']").val();
                    var division_id = $(this).find("input[name='new_surgergy_percentage_id']").val();
                    surgerySubArray.push({
                        'surgergy_percentage': surgergy_percentage,
                        'division_id': division_id,
                    });
                });

                $('#sub_sequence_surgergy_division_per tr').each(function () {
                    var surgergy_percentage = $(this).find("input[name='sub_sequence_surgery_percentage']").val();
                    var division_id = $(this).find("input[name='sub_sequence_surgery_id']").val();
                    surgerySubArray.push({
                        'surgergy_percentage': surgergy_percentage,
                        'division_id': division_id,
                    });
                });
                surgeryFullArray['Division'] = surgerySubArray;
                surgerySubArray = new Array();
                var surgeryFullstring = JSON.stringify(surgeryFullArray);
                var param = {
                    _token: token,
                    surgery_id: surgery_id,
                    surgeryFullstring: surgeryFullstring,
                    service_item: service_item,
                    service_code: service_code,
                    detail_amount: detail_amount
                };
                var url = base_url + "/surgery/saveCompleteSurgery";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#saveCompleteSurgeryBtn').attr('disabled', true);
                        $('#saveCompleteSurgerySpin').removeClass('fa fa-save');
                        $('#saveCompleteSurgerySpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 'success') {
                            toastr.success(obj.message);
                            confirmSurgeryMaster();
                        } else {
                            toastr.error(obj.message);
                        }
                    },
                    complete: function () {
                        $('#saveCompleteSurgeryBtn').attr('disabled', false);
                        $('#saveCompleteSurgerySpin').removeClass('fa fa-spinner fa-spin');
                        $('#saveCompleteSurgerySpin').addClass('fa fa-save');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            } else {
                toastr.warning('Please save surgery details');
            }

        } else {
            toastr.warning('Please select approximate amount');
            $('#detail_amount').focus();
        }
    } else {
        toastr.warning('Please select surgery name');
        $('#service_item').focus();
    }
}

function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}


function confirmSurgeryMaster() {
    var surgery_name = $('#service_item').val();
    var detail_amount = $('#detail_amount').val();
    bootbox.alert({
        title: "Surgery " + titleCase(surgery_name) + " Successfully Created",
        message: "Surgery Approximate Amount: " + detail_amount,
        callback: function () {
            url = base_url + "/surgery/SurgeryMasterList";
            document.location.href = url;
        }
    });
}
