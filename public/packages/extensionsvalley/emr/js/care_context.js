$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});
$(document).on("click", ".document_data_save", function () {
    var filename = $(this).attr('filename');
    var patient_id = $(this).attr('patient_id');
    window.location.href = $('#base_url').val() + "/report/downloadPdfData/"+ patient_id + '/' + filename;
});

function getReportData(){
    var base_url = $('#base_url').val();
    var url = base_url + "/report/careContextLinkingReportData";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var dataparams = {
        from_date:from_date,
        to_date:to_date,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}
