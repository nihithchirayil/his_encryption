$(document).ready(function () {
    $('.date-picker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    $('.select2').select2();
});


function getPharmacyBillCountData()
{
    var url = route_json.bill_count_report_data;   
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();  
    
    var parm = { from_date: from_date, to_date: to_date};
    $.ajax({
        type: "POST",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').html('');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
          
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");   
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
           
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });
}

function resetFilter() {
    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    getPharmacyBillCountData();
}

