$(document).ready(function (){
    initTinymce();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });
    expandTinymce();
    //$('.trimister').toggle();

});

$('.select_button li').click(function () {
    $(this).toggleClass('active')
})

$("#poplink").popover({
    html: true,
    placement: "right",
    trigger: "hover",

    content: function () {
        return $(".pop-content").html();
    }
});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
    panel.style.display = "none";
    } else {
    panel.style.display = "block";
    }
});
}


$(window).load(function(){
    checkVisitExist();
    if($("#medicine-listing-table").find('tr').length == 0){
        addNewMedicine();
    }
    // $(document).on('keypress', '.gen_exam_height, .gen_exam_weight, .gen_exam_pulse, .gen_exam_bmi, .gen_exam_respiration, .gen_exam_temperature', function (e) {
    //     var keyCode = e.keyCode || e.which;

    //     //Regex for Valid Characters i.e. Numbers.
    //     var regex = /^[0-9]+$/;

    //     //Validate TextBox value against the Regex.
    //     var isValid = regex.test(String.fromCharCode(keyCode));
    //     if (!isValid) {
    //         e.preventDefault();
    //     }

    //     return true;
    // });
})



function initTinymce(){

    tinymce.init({
        selector: 'textarea.texteditor11',
        max_height: 90,
        autoresize_min_height: '90',
        themes: "modern",
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
        imagetools_cors_hosts: ['picsum.photos'],
        // menubar: 'file edit view insert format tools table help',
        menubar: false,
        statusbar: false,
        toolbar: false,
        // toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        // toolbar_sticky: true,
        browser_spellcheck: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,

        importcss_append: true,

        height: 600,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',

        branding: false,
        statusbar: false,
        forced_root_block: '',
        setup: function(editor) {

            editor.ui.registry.addButton('Ucase', {
                text: 'A^',
                onAction: function() {
                    TextToUpperCase();
                },
            });
            editor.ui.registry.addButton('Lcase', {
                text: 'a^',
                onAction: function() {
                    TextToLowerCase();
                },
            });
            editor.ui.registry.addButton('Icase', {
                text: 'I^',
                onAction: function() {
                    TextToInterCase();
                },
            });
            editor.ui.registry.addButton('Ccase', {
                text: 'C^',
                onAction: function() {
                    FirstLetterToInterCase();
                },
            });

        },


    });
}

function expandTinymce(){
    $('.accordion').trigger('click');
    $('.tiny_block_toggle').trigger('click');
}

function blockSelect(block){
    if(block =='1'){
        $('#female_block').css('display','none');
        $('#male_block').css('display','block');
    }else{
        $('#female_block').css('display','block');
        $('#male_block').css('display','none');
    }
}

/**
 * OP NUMBER SEARCH STARTS
 */
 $('#op_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv").html("");
            $("#op_no_search_hidden").val("");
            $('.husband_header').html('Husband / Male : ');
        } else {
            try {
                var url = '';
                var param = { op_no_search: op_no_search, op_no_search_prog: 1, search_type:1 };
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#op_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#op_no_searchCodeAjaxDiv").html(html).show();
                        $("#op_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('op_no_searchCodeAjaxDiv', event);
    }
});
$('#op_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_id(list, patient_id, uhid, patient_name, age, blood_group, last_visit_datetime, visit_id, encounter_id, search_type) {
    var itemCodeListDivId = $(list).parent().attr('id');
    if(visit_id > 0){

        if(search_type == 1){
            $('#op_no_search_hidden').val(patient_id);
            $('#op_no_search').val(uhid);
            $('#husband_patient_id').val(patient_id);
            $('#husband_visit_id').val(visit_id);
            $('#husband_encounter_id').val(encounter_id);
            $('#husband_blood_group').val(blood_group);
            $('#husband_age').val(age);
            $('#husband_name').val(patient_name);
            $('#husband_last_visit_date').val(moment(last_visit_datetime).format('MMM-DD-YYYY'));
            $('.husband_header').html('Husband / Male : ' + patient_name.toUpperCase()+ '( '+uhid+' )');
        } else if(search_type == 2){
            $('#op_no_search_hidden_wife').val(patient_id);
            $('#wife_uhid').val(uhid);
            $('#wife_patient_id').val(patient_id);
            $('#wife_visit_id').val(visit_id);
            $('#wife_encounter_id').val(encounter_id);
            $('#wife_blood_group').val(blood_group);
            $('#wife_age').val(age);
            $('#wife_name').val(patient_name);
            $('#wife_last_visit_date').val(moment(last_visit_datetime).format('MMM-DD-YYYY'));
            $('.wife_header').html('Husband / Male : ' + patient_name.toUpperCase()+ '( '+uhid+' )');
        }
        
        $('#' + itemCodeListDivId).hide();

        fetchInfertilityDetails();
    } else {
        $('#' + itemCodeListDivId).hide();
        if(search_type == 1){
            bootbox.alert('This Husband/Male has no visit for today. Please contact front office.');
        } else if(search_type == 1){
            bootbox.alert('This Wife/Female has no visit for today. Please contact front office.');
        }
    }
    
}

/**
 * OP NUMBER SEARCH STARTS
 */
 $('.wife_uhid').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv_wife").html("");
            $("#op_no_search_hidden_wife").val("");
            $('.wife_header').html('Wife / Female : ');
        } else {
            try {
                var url = '';
                var param = { op_no_search: op_no_search, op_no_search_prog: 1, search_type:2 };
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#op_no_searchCodeAjaxDiv_wife").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#op_no_searchCodeAjaxDiv_wife").html(html).show();
                        $("#op_no_searchCodeAjaxDiv_wife").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('op_no_searchCodeAjaxDiv_wife', event);
    }
});
$('.wife_uhid').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv_wife');
        return false;
    }
});

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}
function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}

/**
 *
 * OP NUMBER SEARCH ENDS
 */

function getFormattedJSON(){
    var infertility_json = {};
    var wife_json = {};
    var husband_json = {};
    wife_json.infertility = $('input[name="infertility_female"]:checked').val();
    wife_json.ob_gyn_history = {};
    wife_json.ob_gyn_history.menarche = $('.menarche').val();
    wife_json.ob_gyn_history.staying_together = $('.staying_together').val();
    wife_json.ob_gyn_history.married_on = $('.married_on').val();
    wife_json.ob_gyn_history.details = [];

    $(".ob_gyn_history_multi_combo").find('li.active').each(function(key, val){
        wife_json.ob_gyn_history.details.push($(val).attr('data-value'));
    });

    wife_json.menstural_cycle = {};
    wife_json.menstural_cycle.lmp = $('.lmp_date').val();
    wife_json.menstural_cycle.pmc = $('.pmc_date').val();
    wife_json.menstural_cycle.details = [];

    $(".menstural_cycle_multi_combo").find('li.active').each(function(key, val){
        wife_json.menstural_cycle.details.push($(val).attr('data-value'));
    });

    wife_json.hormonal_factor = {};
    wife_json.hormonal_factor.weight = $('input[name="female_hormonal_weight"]:checked').val();
    wife_json.hormonal_factor.mastalgin = $('input[name="female_hormonal_mastalgin"]:checked').val();
    wife_json.hormonal_factor.hirtusism = $('input[name="female_hormonal_hirtusism"]:checked').val();
    wife_json.hormonal_factor.galactorrhoea = $('input[name="female_hormonal_galactorrhoea"]:checked').val();
    wife_json.hormonal_factor.acne = $('input[name="female_hormonal_acne"]:checked').val();
    
    wife_json.pelvic_factor = {};
    wife_json.pelvic_factor.female_pelvic_pain = $('input[name="female_pelvic_pain"]:checked').val();
    wife_json.pelvic_factor.female_pelvic_infection = $('input[name="female_pelvic_infection"]:checked').val();
    wife_json.pelvic_factor.female_pelvic_surgery = $('input[name="female_pelvic_surgery"]:checked').val();
    wife_json.pelvic_factor.female_pelvic_dyspareunia = $('input[name="female_pelvic_dyspareunia"]:checked').val();
    wife_json.pelvic_factor.female_pelvic_dysmenorria = $('input[name="female_pelvic_dysmenorria"]:checked').val();
    
    wife_json.cervical_factor = {};
    wife_json.cervical_factor.female_cervical_surgery = $('input[name="female_cervical_surgery"]:checked').val();
    wife_json.cervical_factor.female_cervical_injury = $('input[name="female_cervical_injury"]:checked').val();
    wife_json.cervical_factor.female_cervical_infection = $('input[name="female_cervical_infection"]:checked').val();
    wife_json.cervical_factor.details = $('.female_cervical_details').val();

    wife_json.medical_history = $('#medical_history_ifr')[0].contentDocument.body.innerHTML;
    wife_json.surgical_history = $('#surgical_history_ifr')[0].contentDocument.body.innerHTML;
    wife_json.medications = $('#medications_ifr')[0].contentDocument.body.innerHTML;

    wife_json.previous_infertility_investigation = {};
    wife_json.previous_infertility_investigation.hgs = {};
    wife_json.previous_infertility_investigation.hgs.description = $(".female_previous_infertility_hgs_detail").val();
    wife_json.previous_infertility_investigation.hgs.date = $(".female_previous_infertility_hgs_date").val();
    wife_json.previous_infertility_investigation.hgs.place = $(".female_previous_infertility_hgs_place").val();
    wife_json.previous_infertility_investigation.uterus = $('input[name="female_previous_infertility_uterus"]:checked').val();
    wife_json.previous_infertility_investigation.tubes = $('input[name="female_previous_infertility_tubes"]:checked').val();

    wife_json.diagnosis_laparoscopy = {};
    wife_json.diagnosis_laparoscopy.date = $(".female_diagnosis_laparoscopy_date").val();
    wife_json.diagnosis_laparoscopy.ut = $(".female_diagnosis_laparoscopy_ut").val();
    wife_json.diagnosis_laparoscopy.tube = $('input[name="female_diagnosis_laparoscopy_ovaries"]:checked').val();
    wife_json.diagnosis_laparoscopy.ovaries = $('input[name="female_diagnosis_laparoscopy_tube"]:checked').val();
    wife_json.diagnosis_laparoscopy.pelvis = $(".female_diagnosis_laparoscopy_pelvis").val();
    wife_json.diagnosis_laparoscopy.others = $(".female_diagnosis_laparoscopy_others").val();
    
    wife_json.hysteroscopy = {};
    wife_json.hysteroscopy.date = $(".female_hysteroscopy_date").val();
    wife_json.hysteroscopy.cavity = $('input[name="female_hysteroscopy_cavity"]:checked').val();
    wife_json.hysteroscopy.place = $(".female_hysteroscopy_place").val();
    wife_json.hysteroscopy.ostia = $('input[name="female_hysteroscopy_ostia"]:checked').val();
    
    
    wife_json.previous_induction = {};
    wife_json.previous_induction.no_of_cycles = $(".female_previous_induction_no_of_cycles").val();
    wife_json.previous_induction.year = $(".female_previous_induction_year").val();
    wife_json.previous_induction.response = $(".female_previous_induction_response").val();
    wife_json.previous_induction.letrozole = $(".female_previous_induction_letrozole").val();
    wife_json.previous_induction.hmg_fsh = $(".female_previous_induction_hmg_fsh").val();
    wife_json.previous_induction.clomphene = $(".female_previous_induction_clomphene").val();
    
    wife_json.previous_ivi = {};
    wife_json.previous_ivi.year = $(".female_previous_ivi_year").val();
    wife_json.previous_ivi.place = $(".female_previous_ivi_place").val();
    wife_json.previous_ivi.no_of_cycles = $(".female_previous_ivi_no_of_cycles").val();
    wife_json.previous_ivi.response = $(".female_previous_ivi_response").val();
    
    wife_json.general_examination = {};
    wife_json.general_examination.height = $(".female_general_examination_height").val();
    wife_json.general_examination.weight = $(".female_general_examination_weight").val();
    wife_json.general_examination.bmi = $(".female_general_examination_bmi").val();
    wife_json.general_examination.bp = $(".female_general_examination_bp").val();
    wife_json.general_examination.cvs = $(".female_general_examination_cvs").val();
    wife_json.general_examination.rs = $(".female_general_examination_rs").val();
    wife_json.general_examination.abdomen = $(".female_general_examination_abdomen").val();
    wife_json.general_examination.acne = $("input[name='female_general_examination_acne']:checked").val();
    wife_json.general_examination.striae = $("input[name='female_general_examination_striae']:checked").val();
    wife_json.general_examination.thyroid = $("input[name='female_general_examination_thyroid']:checked").val();
    wife_json.general_examination.galactorrhoea = $("input[name='female_general_examination_galactorrhoea']:checked").val();
    wife_json.general_examination.hirtusism = $("input[name='female_general_examination_hirtusism']:checked").val();
    
    wife_json.pelvic_examination = {};
    wife_json.pelvic_examination.introitus = $("input[name='female_general_examination_introitus']:checked").val();
    wife_json.pelvic_examination.cx = $("input[name='female_general_examination_cx']:checked").val();
    wife_json.pelvic_examination.uterus_size = $("input[name='female_general_examination_uterus_size']:checked").val();
    wife_json.pelvic_examination.rv_av = $(".female_general_examination_rv_av").val();
    wife_json.pelvic_examination.mobility = $("input[name='female_general_examination_mobility']:checked").val();
    wife_json.pelvic_examination.adnexa = $("input[name='female_general_examination_adnexa']:checked").val();
    infertility_json.wife_json = wife_json;

    husband_json.infertility = $('input[name="infertility_male"]:checked').val();
    husband_json.medical_and_surgical_history = {};
    husband_json.medical_and_surgical_history.history_data = [];
    husband_json.medical_and_surgical_history.possible_testicular_damage = [];
    husband_json.medical_and_surgical_history.surgery = [];
    
    $(".medical_and_surgical_history_multi_combo").find('li.active').each(function(key, val){
        husband_json.medical_and_surgical_history.history_data.push($(val).attr('data-value'));
    });
    
    $(".medical_and_surgical_history_possible_testicular_damage_multi_combo").find('li.active').each(function(key, val){
        husband_json.medical_and_surgical_history.possible_testicular_damage.push($(val).attr('data-value'));
    });
    
    $(".medical_and_surgical_history_surgery_multi_combo").find('li.active').each(function(key, val){
        husband_json.medical_and_surgical_history.surgery.push($(val).attr('data-value'));
    });

    husband_json.medical_and_surgical_history.fever_in_past = $('input[name="medical_and_surgical_history_fever_in_past"]:checked').val();
    husband_json.medical_and_surgical_history.urinary_infection = $('input[name="medical_and_surgical_history_urinary_infection"]:checked').val();
    husband_json.medical_and_surgical_history.std = $('input[name="medical_and_surgical_history_std"]:checked').val();
    husband_json.medical_and_surgical_history.epididymitis = $('input[name="medical_and_surgical_history_epididymitis"]:checked').val();
    husband_json.medical_and_surgical_history.testicular_maldescent = $('input[name="medical_and_surgical_history_testicular_maldescent"]:checked').val();
    husband_json.medical_and_surgical_history.varicocele_treatment = $('input[name="medical_and_surgical_history_varicocele_treatment"]:checked').val();
    husband_json.medical_and_surgical_history.details = $('.medical_and_surgical_history_details').val();
    
    husband_json.habits_and_environmental_factors = {};
    husband_json.habits_and_environmental_factors.smoking = $('input[name="habits_and_environmental_factors_smoking"]:checked').val();
    husband_json.habits_and_environmental_factors.alcohol = $('input[name="habits_and_environmental_factors_alcohol"]:checked').val();
    husband_json.habits_and_environmental_factors.environmental_and_occupation_factor = $('input[name="habits_and_environmental_factors_environmental_and_occupation_factor"]:checked').val();
    husband_json.habits_and_environmental_factors.drug_abuse = $('input[name="habits_and_environmental_factors_drug_abuse"]:checked').val();
    husband_json.habits_and_environmental_factors.details = $('.habits_and_environmental_factors_details').val();
    
    husband_json.sexaul_and_ejaculatory_function = {};
    husband_json.sexaul_and_ejaculatory_function.avg_frequency = $('input[name="sexaul_and_ejaculatory_function_avg_frequency"]:checked').val();
    husband_json.sexaul_and_ejaculatory_function.erection = $('input[name="sexaul_and_ejaculatory_function_erection"]:checked').val();
    husband_json.sexaul_and_ejaculatory_function.ejaculation = $('input[name="sexaul_and_ejaculatory_function_ejaculation"]:checked').val();
    
    husband_json.general_and_physical_examination = {};
    husband_json.general_and_physical_examination.height = $('.general_and_physical_examination_height').val();
    husband_json.general_and_physical_examination.weight = $('.general_and_physical_examination_weight').val();
    husband_json.general_and_physical_examination.general_and_physical_exam = $('input[name="general_and_physical_examination_general_and_physical_exam"]:checked').val();
    husband_json.general_and_physical_examination.siens_of_virilization = $('input[name="general_and_physical_examination_siens_of_virilization"]:checked').val();
    husband_json.general_and_physical_examination.gynecomastia = $('input[name="general_and_physical_examination_gynecomastia"]:checked').val();
    
    husband_json.uro_general_examination = {};
    husband_json.uro_general_examination.penis = $('input[name="uro_general_examination_penis"]:checked').val();
    husband_json.uro_general_examination.tests = $('input[name="uro_general_examination_tests"]:checked').val();
    husband_json.uro_general_examination.site = $('input[name="uro_general_examination_site"]:checked').val();
    husband_json.uro_general_examination.volume = $('input[name="uro_general_examination_volume"]:checked').val();
    husband_json.uro_general_examination.volume_text = $('.uro_general_examination_volume_text').val();
    husband_json.uro_general_examination.epididymitis = $('input[name="uro_general_examination_epididymitis"]:checked').val();
    husband_json.uro_general_examination.varicocele = $('input[name="uro_general_examination_varicocele"]:checked').val();
    husband_json.uro_general_examination.injuinal_exam = $('input[name="uro_general_examination_injuinal_exam"]:checked').val();
    husband_json.uro_general_examination.details = $('.uro_general_examination_details').val();
    husband_json.uro_general_examination.scrotal_swelling = $('input[name="uro_general_examination_scrotal_swelling"]:checked').val();
    husband_json.uro_general_examination.injuinal_exam = $('input[name="uro_general_examination_injuinal_exam"]:checked').val();
    husband_json.uro_general_examination.rectual_exam = $('input[name="uro_general_examination_rectual_exam"]:checked').val();
    
    husband_json.semen_analysis = {};
    husband_json.semen_analysis.ejectable_volume = {};
    husband_json.semen_analysis.ejectable_volume.nsml = $('.ejectable_volume_nsml').val();
    husband_json.semen_analysis.ejectable_volume.dis = $('.ejectable_volume_dis').val();
    husband_json.semen_analysis.ejectable_volume.normal = $('.ejectable_volume_normal').val();

    husband_json.semen_analysis.ph = {};
    husband_json.semen_analysis.ph.nsml = $('.ph_nsml').val();
    husband_json.semen_analysis.ph.dis = $('.ph_dis').val();
    husband_json.semen_analysis.ph.normal = $('.ph_normal').val();

    husband_json.semen_analysis.sperm_consentration = {};
    husband_json.semen_analysis.sperm_consentration.nsml = $('.sperm_consentration_nsml').val();
    husband_json.semen_analysis.sperm_consentration.dis = $('.sperm_consentration_dis').val();
    husband_json.semen_analysis.sperm_consentration.normal = $('.sperm_consentration_normal').val();

    husband_json.semen_analysis.total_sperm_no = {};
    husband_json.semen_analysis.total_sperm_no.nsml = $('.total_sperm_no_nsml').val();
    husband_json.semen_analysis.total_sperm_no.dis = $('.total_sperm_no_dis').val();
    husband_json.semen_analysis.total_sperm_no.normal = $('.total_sperm_no_normal').val();

    husband_json.semen_analysis.percent_motility = {};
    husband_json.semen_analysis.percent_motility.nsml = $('.percent_motility_nsml').val();
    husband_json.semen_analysis.percent_motility.dis = $('.percent_motility_dis').val();
    husband_json.semen_analysis.percent_motility.normal = $('.percent_motility_normal').val();

    husband_json.semen_analysis.forward_progression = {};
    husband_json.semen_analysis.forward_progression.nsml = $('.forward_progression_nsml').val();
    husband_json.semen_analysis.forward_progression.dis = $('.forward_progression_dis').val();
    husband_json.semen_analysis.forward_progression.normal = $('.forward_progression_normal').val();

    husband_json.semen_analysis.morphology = {};
    husband_json.semen_analysis.morphology.nsml = $('.morphology_nsml').val();
    husband_json.semen_analysis.morphology.dis = $('.morphology_dis').val();
    husband_json.semen_analysis.morphology.normal = $('.morphology_normal').val();

    husband_json.semen_analysis.sperm_aglutination = {};
    husband_json.semen_analysis.sperm_aglutination.nsml = $('.sperm_aglutination_nsml').val();
    husband_json.semen_analysis.sperm_aglutination.dis = $('.sperm_aglutination_dis').val();
    husband_json.semen_analysis.sperm_aglutination.normal = $('.sperm_aglutination_normal').val();

    husband_json.semen_analysis.viscosity = {};
    husband_json.semen_analysis.viscosity.nsml = $('.viscosity_nsml').val();
    husband_json.semen_analysis.viscosity.dis = $('.viscosity_dis').val();
    husband_json.semen_analysis.viscosity.normal = $('.viscosity_normal').val();


    infertility_json.husband_json = husband_json;

    infertility_json.family_history = {};
    infertility_json.family_history.ht = {};
    infertility_json.family_history.ht.male = $("input[name='family_history_ht_male']:checked").val();
    infertility_json.family_history.ht.female = $("input[name='family_history_ht_female']:checked").val();
    infertility_json.family_history.diabetes = {};
    infertility_json.family_history.diabetes.male = $("input[name='family_history_diabetes_male']:checked").val();
    infertility_json.family_history.diabetes.female = $("input[name='family_history_diabetes_female']:checked").val();
    infertility_json.family_history.thyroid = {};
    infertility_json.family_history.thyroid.male = $("input[name='family_history_thyroid_male']:checked").val();
    infertility_json.family_history.thyroid.female = $("input[name='family_history_thyroid_female']:checked").val();
    infertility_json.family_history.kochs = {};
    infertility_json.family_history.kochs.male = $("input[name='family_history_kochs_male']:checked").val();
    infertility_json.family_history.kochs.female = $("input[name='family_history_kochs_female']:checked").val();
    infertility_json.family_history.infertility  = {};
    infertility_json.family_history.infertility.male  = $("input[name='family_history_infertility_male']:checked").val();
    infertility_json.family_history.infertility.female  = $("input[name='family_history_infertility_female']:checked").val();
    infertility_json.family_history.anomalies  = {};
    infertility_json.family_history.anomalies.male  = $("input[name='family_history_anomalies_male']:checked").val();
    infertility_json.family_history.anomalies.female  = $("input[name='family_history_anomalies_female']:checked").val();


    infertility_json.male_investigations = [];
    infertility_json.female_investigations = [];

    $('.male_investigations').find(".form-check-input").each(function(key, val){
        if($(val).is(":checked")){
            var temp_inv = {};
            temp_inv.service_code = $(val).val();
            infertility_json.male_investigations.push(temp_inv);
        }
    });
    
    $('.female_investigations').find(".form-check-input").each(function(key, val){
        if($(val).is(":checked")){
            var temp_inv = {};
            temp_inv.service_code = $(val).val();
            infertility_json.female_investigations.push(temp_inv);
        }
    });
    
    // infertility_json = '{"wife_json":{"infertility":"primary","ob_gyn_history":{"menarche":"5","staying_together":"5","married_on":"Jan-01-2019","details":["ND"]},"menstural_cycle":{"details":["Regular"]},"hormonal_factor":{"weight":"gain","mastalgin":"no","hirtusism":"no","galactorrhoea":"no","acne":"yes"},"pelvic_factor":{"female_pelvic_pain":"no","female_pelvic_infection":"no","female_pelvic_surgery":"no","female_pelvic_dyspareunia":"yes","female_pelvic_dysmenorria":"mild"},"cervical_factor":{"female_cervical_surgery":"no","female_cervical_injury":"no","female_cervical_infection":"no","details":"Surgery,Injury,Infections"},"medical_history":"Surgery,Injury,Infections","surgical_history":"Surgery,Injury,Infections","medications":"dolo","previous_infertility_investigation":{"hgs":{"description":"PREVIOUS INFERTILITY INVESTIGATIONS AND THERAPY","date":"Feb-01-2022","place":"pelvis"},"uterus":"normal","tubes":"lt_patent"},"diagnosis_laparoscopy":{"date":"Mar-01-2022","ut":"UT","tube":"rt","ovaries":"lt","pelvis":"Pelvis","others":""},"hysteroscopy":{"date":"Feb-01-2022","cavity":"normal","place":"cavity"},"previous_induction":{"date":"4","year":"4","response":"response","letrozole":"letrozole","hmg_fsh":"HMG","clomphene":"clomphene"},"previous_ivi":{"year":"4","place":"IVI","no_of_cycles":"4","response":"PREVIOUS"},"general_examination":{"height":"152","weight":"65","bmi":"24","bp":"120/110","cvs":"CVS","rs":"RS","abdomen":"abdomen","acne":"yes","striae":"no","thyroid":"normal","galactorrhoea":"present","hirtusism":"moderate"},"pelvic_examination":{"introitus":"normal","cx":"normal","uterus_size":"normal","rv_av":"R/V","mobility":"yes","adnexa":"normal"}},"husband_json":{"infertility":"primary","medical_and_surgical_history":{"history_data":["","",""],"possible_testicular_damage":["",""],"surgery":["",""],"fever_in_past":"yes","urinary_infection":"no","std":"no","epididymitis":"no","testicular_maldescent":"yes","varicocele_treatment":"yes"},"habits_and_environmental_factors":{"smoking":"yes","alcohol":"no","environmental_and_occupation_factor":"heat","drug_abuse":"yes","details":"ASDASD"},"sexaul_and_ejaculatory_function":{"avg_frequency":"normal","erection":"inadiquate","ejaculation":"normal"},"general_and_physical_examination":{"height":"22","weight":"44","general_and_physical_exam":"normal","siens_of_virilization":"normal","gynecomastia":"tannerstage"},"uro_general_examination":{"penis":"normal","tests":"palpable","site":"abnormal","volume":"rside","volume_text":"","epididymitis":"thickened","varicocele":"grede","injuinal_exam":"normal","details":"ASDASD","scrotal_swelling":"none","rectual_exam":"prostatetender"},"semen_analysis":{"ejectable_volume":{"nsml":"A","dis":"ASD","normal":"ASD"},"ph":{"nsml":"SDFN","dis":"SDKJFNJ","normal":"NSDF"},"sperm_consentration":{"nsml":"KFJNGKG","dis":"DJFNGJ","normal":"SJNF"},"total_sperm_no":{"nsml":"SDDF","dis":"SNF","normal":"SLDNF"},"percent_motility":{"nsml":"SLDNF","dis":"SDNF","normal":"SKDMF"},"forward_progression":{"nsml":"SLDFN","dis":"SDKLFM","normal":"SDNNF"},"morphology":{"nsml":"SDFFN","dis":"SDFK","normal":"LSKDMF"},"sperm_aglutination":{"nsml":"SLDFN","dis":"LSNDF","normal":"NDS"},"viscosity":{"nsml":"LSKNDF","dis":"FSM","normal":"SLDMF"}}}}';

    return infertility_json;
}


function saveInfertilityDetails(){
    var json_data = getFormattedJSON();
    var wife_patient_id = $("#wife_patient_id").val();
    var husband_patient_id = $("#husband_patient_id").val();
    var wife_visit_id = $("#wife_visit_id").val();
    var husband_visit_id = $("#husband_visit_id").val();
    var doctor_id = $("#doctor_id").val();
    var wife_encounter_id = $("#wife_encounter_id").val();
    var husband_encounter_id = $("#husband_encounter_id").val();
    var wife_uhid = $(".wife_uhid").val();
    var husband_uhid = $("#op_no_search").val();

    if(!wife_uhid){
        bootbox.alert('Please select Wife/Female.');
        return;
    }
    if(!husband_uhid){
        bootbox.alert('Please select Husband/Male');
        return;
    }

    json_data.wife_patient_id = wife_patient_id;
    json_data.husband_patient_id = husband_patient_id;
    json_data.wife_visit_id = wife_visit_id;
    json_data.husband_visit_id = husband_visit_id;
    json_data.husband_encounter_id = husband_encounter_id;
    json_data.wife_encounter_id = wife_encounter_id;
    json_data.doctor_id = doctor_id;
    json_data.wife_uhid = wife_uhid;
    json_data.husband_uhid = husband_uhid;

    var json_data = JSON.stringify(json_data);
    json_data = escape(json_data);
    
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/infertility/saveInfertilityDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: "json_data="+json_data+"&_token="+_token,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"](data.message);
                window.location.reload();
            } else {
                Command: toastr["error"](data.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}

function fetchInfertilityDetails(){
    var wife_patient_id = $("#wife_patient_id").val();
    var husband_patient_id = $("#husband_patient_id").val();
    var wife_visit_id = $("#wife_visit_id").val();
    var husband_visit_id = $("#husband_visit_id").val();
    var doctor_id = $("#doctor_id").val();
    let _token = $('#c_token').val();
    var request_data = {};
    request_data.wife_patient_id = wife_patient_id;
    request_data.husband_patient_id = husband_patient_id;
    request_data.wife_visit_id = wife_visit_id;
    request_data.husband_visit_id = husband_visit_id;
    request_data.doctor_id = doctor_id;
    request_data._token = _token;
    
    var url = $('#base_url').val() + "/infertility/fetchInfertilityDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: request_data,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data.status == 1) {
                var wife_json = JSON.parse(data.data.wife_json);
                if(wife_json.infertility)
                    $('#female_block').find('input[value='+wife_json.infertility+']').click();
                $('#female_block').find('.menarche').val(wife_json.ob_gyn_history.menarche);
                $('#female_block').find('.staying_together').val(wife_json.ob_gyn_history.staying_together);
                $('#female_block').find('.married_on').val(wife_json.ob_gyn_history.married_on);

                $('#female_block').find(".ob_gyn_history_multi_combo").find('li').each(function(key, val){
                    if($.inArray($(val).attr('data-value'), wife_json.ob_gyn_history.details) != -1 )
                        $(val).click();
                });

                $('#female_block').find('.lmp_date').val(wife_json.menstural_cycle.lmp);
                $('#female_block').find('.pmc_date').val(wife_json.menstural_cycle.pmc);
            
                $('#female_block').find(".menstural_cycle_multi_combo").find('li').each(function(key, val){
                    if($.inArray($(val).attr('data-value'), wife_json.menstural_cycle.details) != -1 )
                        $(val).click();
                });

                if(wife_json.hormonal_factor.weight)
                    $('.female_hormonal_weight').find('input[value="'+wife_json.hormonal_factor.weight+'"]').click();
                if(wife_json.hormonal_factor.mastalgin)
                    $('.female_hormonal_mastalgin').find('input[value="'+wife_json.hormonal_factor.mastalgin+'"]').click();
                if(wife_json.hormonal_factor.hirtusism)
                    $('.female_hormonal_hirtusism').find('input[value="'+wife_json.hormonal_factor.hirtusism+'"]').click();
                if(wife_json.hormonal_factor.galactorrhoea)
                    $('.female_hormonal_galactorrhoea').find('input[value="'+wife_json.hormonal_factor.galactorrhoea+'"]').click();
                if(wife_json.hormonal_factor.acne)
                    $('.female_hormonal_acne').find('input[value="'+wife_json.hormonal_factor.acne+'"]').click();
                
                if(wife_json.pelvic_factor.female_pelvic_pain)
                    $('.female_pelvic_pain').find('input[value="'+wife_json.pelvic_factor.female_pelvic_pain+'"]').click();
                if(wife_json.pelvic_factor.female_pelvic_infection)
                    $('.female_pelvic_infection').find('input[value="'+wife_json.pelvic_factor.female_pelvic_infection+'"]').click();
                if(wife_json.pelvic_factor.female_pelvic_surgery)
                    $('.female_pelvic_surgery').find('input[value="'+wife_json.pelvic_factor.female_pelvic_surgery+'"]').click();
                if(wife_json.pelvic_factor.female_pelvic_dyspareunia)
                    $('.female_pelvic_dyspareunia').find('input[value="'+wife_json.pelvic_factor.female_pelvic_dyspareunia+'"]').click();
                if(wife_json.pelvic_factor.female_pelvic_dysmenorria)
                    $('.female_pelvic_dysmenorria').find('input[value="'+wife_json.pelvic_factor.female_pelvic_dysmenorria+'"]').click();
               
                if(wife_json.cervical_factor.female_cervical_surgery)
                    $('.female_cervical_surgery').find('input[value="'+wife_json.cervical_factor.female_cervical_surgery+'"]').click();
                if(wife_json.cervical_factor.female_cervical_injury)
                    $('.female_cervical_injury').find('input[value="'+wife_json.cervical_factor.female_cervical_injury+'"]').click();
                if(wife_json.cervical_factor.female_cervical_infection)
                    $('.female_cervical_infection').find('input[value="'+wife_json.cervical_factor.female_cervical_infection+'"]').click();

                $('.female_cervical_details').val(wife_json.cervical_factor.details);
                
                $('#medical_history_ifr')[0].contentDocument.body.innerHTML = wife_json.medical_history;
                $('#surgical_history_ifr')[0].contentDocument.body.innerHTML = wife_json.surgical_history;
                $('#medications_ifr')[0].contentDocument.body.innerHTML = wife_json.medications;

                $(".female_previous_infertility_hgs_detail").val(wife_json.previous_infertility_investigation.hgs.description);
                $(".female_previous_infertility_hgs_date").val(wife_json.previous_infertility_investigation.hgs.date);
                $(".female_previous_infertility_hgs_place").val(wife_json.previous_infertility_investigation.hgs.place);
                if(wife_json.previous_infertility_investigation.uterus)
                    $('.female_previous_infertility_uterus').find('input[value="'+wife_json.previous_infertility_investigation.uterus+'"]').click();
                if(wife_json.previous_infertility_investigation.tubes)
                    $('.female_previous_infertility_tubes').find('input[value="'+wife_json.previous_infertility_investigation.tubes+'"]').click();
                
                if(wife_json.diagnosis_laparoscopy.ovaries)
                    $('.female_diagnosis_laparoscopy_ovaries').find('input[value="'+wife_json.diagnosis_laparoscopy.ovaries+'"]').click();
                if(wife_json.diagnosis_laparoscopy.tube)
                    $('.female_diagnosis_laparoscopy_tube').find('input[value="'+wife_json.diagnosis_laparoscopy.tube+'"]').click();
                $(".female_diagnosis_laparoscopy_date").val(wife_json.diagnosis_laparoscopy.date);
                $(".female_diagnosis_laparoscopy_ut").val(wife_json.diagnosis_laparoscopy.ut);
                $(".female_diagnosis_laparoscopy_pelvis").val(wife_json.diagnosis_laparoscopy.pelvis);
                $(".female_diagnosis_laparoscopy_others").val(wife_json.diagnosis_laparoscopy.others);
                
                $(".female_hysteroscopy_date").val(wife_json.hysteroscopy.date);
                $(".female_hysteroscopy_place").val(wife_json.hysteroscopy.place);
                if(wife_json.hysteroscopy.cavity)
                    $('.female_hysteroscopy_cavity').find('input[value="'+wife_json.hysteroscopy.cavity+'"]').click();
                if(wife_json.hysteroscopy.ostia)
                    $('.female_hysteroscopy_ostia').find('input[value="'+wife_json.hysteroscopy.ostia+'"]').click();
                
                
                $(".female_previous_induction_no_of_cycles").val(wife_json.previous_induction.no_of_cycles);
                $(".female_previous_induction_year").val(wife_json.previous_induction.year);
                $(".female_previous_induction_response").val(wife_json.previous_induction.response);
                $(".female_previous_induction_letrozole").val(wife_json.previous_induction.letrozole);
                $(".female_previous_induction_hmg_fsh").val(wife_json.previous_induction.hmg_fsh);
                $(".female_previous_induction_clomphene").val(wife_json.previous_induction.clomphene);

                $(".female_previous_ivi_year").val(wife_json.previous_ivi.year);
                $(".female_previous_ivi_place").val(wife_json.previous_ivi.place);
                $(".female_previous_ivi_no_of_cycles").val(wife_json.previous_ivi.no_of_cycles);
                $(".female_previous_ivi_response").val(wife_json.previous_ivi.response);


                $(".female_general_examination_height").val(wife_json.general_examination.height);
                $(".female_general_examination_weight").val(wife_json.general_examination.weight);
                $(".female_general_examination_bmi").val(wife_json.general_examination.bmi);
                $(".female_general_examination_bp").val(wife_json.general_examination.bp);
                $(".female_general_examination_cvs").val(wife_json.general_examination.cvs);
                $(".female_general_examination_rs").val(wife_json.general_examination.rs);
                $(".female_general_examination_abdomen").val(wife_json.general_examination.abdomen);
                
                if(wife_json.general_examination.acne)
                    $(".female_general_examination_acne").find('input[value="'+wife_json.general_examination.acne+'"]').click();
                if(wife_json.general_examination.striae)
                    $(".female_general_examination_striae").find('input[value="'+wife_json.general_examination.striae+'"]').click();
                if(wife_json.general_examination.thyroid)
                    $(".female_general_examination_thyroid").find('input[value="'+wife_json.general_examination.thyroid+'"]').click();
                if(wife_json.general_examination.galactorrhoea)
                    $(".female_general_examination_galactorrhoea").find('input[value="'+wife_json.general_examination.galactorrhoea+'"]').click();
                if(wife_json.general_examination.hirtusism)
                    $(".female_general_examination_hirtusism").find('input[value="'+wife_json.general_examination.hirtusism+'"]').click();
                
                $(".female_general_examination_rv_av").val(wife_json.pelvic_examination.rv_av);
                if(wife_json.pelvic_examination.introitus)
                    $(".female_general_examination_introitus").find('input[value="'+wife_json.pelvic_examination.introitus+'"]').click();
                if(wife_json.pelvic_examination.cx)
                    $(".female_general_examination_cx").find('input[value="'+wife_json.pelvic_examination.cx+'"]').click();
                if(wife_json.pelvic_examination.uterus_size)
                    $(".female_general_examination_uterus_size").find('input[value="'+wife_json.pelvic_examination.uterus_size+'"]').click();
                if(wife_json.pelvic_examination.mobility)
                    $(".female_general_examination_mobility").find('input[value="'+wife_json.pelvic_examination.mobility+'"]').click();
                if(wife_json.pelvic_examination.adnexa)
                    $(".female_general_examination_adnexa").find('input[value="'+wife_json.pelvic_examination.adnexa+'"]').click();
                

                var husband_json = JSON.parse(data.data.husband_json);
                if(husband_json.infertility)
                    $('#male_block').find('input[value='+husband_json.infertility+']').click();

                if(husband_json.medical_and_surgical_history.fever_in_past)
                    $('.medical_and_surgical_history_fever_in_past').find('input[value="'+husband_json.medical_and_surgical_history.fever_in_past+'"]').click();
                if(husband_json.medical_and_surgical_history.urinary_infection)
                    $('.medical_and_surgical_history_urinary_infection').find('input[value="'+husband_json.medical_and_surgical_history.urinary_infection+'"]').click();
                if(husband_json.medical_and_surgical_history.std)
                    $('.medical_and_surgical_history_std').find('input[value="'+husband_json.medical_and_surgical_history.std+'"]').click();
                if(husband_json.medical_and_surgical_history.epididymitis)
                    $('.medical_and_surgical_history_epididymitis').find('input[value="'+husband_json.medical_and_surgical_history.epididymitis+'"]').click();
                if(husband_json.medical_and_surgical_history.testicular_maldescent)
                    $('.medical_and_surgical_history_testicular_maldescent').find('input[value="'+husband_json.medical_and_surgical_history.testicular_maldescent+'"]').click();
                if(husband_json.medical_and_surgical_history.varicocele_treatment)
                    $('.medical_and_surgical_history_varicocele_treatment').find('input[value="'+husband_json.medical_and_surgical_history.varicocele_treatment+'"]').click();
                $('.medical_and_surgical_history_details').val(husband_json.medical_and_surgical_history.details);

                $('#male_block').find(".ob_gyn_history_multi_combo").find('li').each(function(key, val){
                    if($.inArray($(val).attr('data-value'), wife_json.ob_gyn_history.details) != -1 )
                        $(val).click();
                });

                $(".medical_and_surgical_history_multi_combo").find('li').each(function(key, val){
                    if($.inArray($(val).attr('data-value'), husband_json.medical_and_surgical_history.history_data) != -1 )
                        $(val).click();
                });
                
                $(".medical_and_surgical_history_possible_testicular_damage_multi_combo").find('li').each(function(key, val){
                    if($.inArray($(val).attr('data-value'), husband_json.medical_and_surgical_history.possible_testicular_damage) != -1 )
                        $(val).click();
                });
                
                $(".medical_and_surgical_history_surgery_multi_combo").find('li').each(function(key, val){
                    if($.inArray($(val).attr('data-value'), husband_json.medical_and_surgical_history.surgery) != -1 )
                        $(val).click();
                });

                if(husband_json.habits_and_environmental_factors.smoking)
                    $('.habits_and_environmental_factors_smoking').find('input[value="'+husband_json.habits_and_environmental_factors.smoking+'"]').click();
                if(husband_json.habits_and_environmental_factors.alcohol)
                    $('.habits_and_environmental_factors_alcohol').find('input[value="'+husband_json.habits_and_environmental_factors.alcohol+'"]').click();
                if(husband_json.habits_and_environmental_factors.environmental_and_occupation_factor)
                    $('.habits_and_environmental_factors_environmental_and_occupation_factor').find('input[value="'+husband_json.habits_and_environmental_factors.environmental_and_occupation_factor+'"]').click();
                if(husband_json.habits_and_environmental_factors.drug_abuse)
                    $('.habits_and_environmental_factors_drug_abuse').find('input[value="'+husband_json.habits_and_environmental_factors.drug_abuse+'"]').click();
                $('.habits_and_environmental_factors_details').val(husband_json.habits_and_environmental_factors.details);
                
                if(husband_json.sexaul_and_ejaculatory_function.avg_frequency)
                    $('.sexaul_and_ejaculatory_function_avg_frequency').find('input[value="'+husband_json.sexaul_and_ejaculatory_function.avg_frequency+'"]').click();
                if(husband_json.sexaul_and_ejaculatory_function.erection)
                    $('.sexaul_and_ejaculatory_function_erection').find('input[value="'+husband_json.sexaul_and_ejaculatory_function.erection+'"]').click();
                if(husband_json.sexaul_and_ejaculatory_function.ejaculation)
                    $('.sexaul_and_ejaculatory_function_ejaculation').find('input[value="'+husband_json.sexaul_and_ejaculatory_function.ejaculation+'"]').click();
                
                if(husband_json.general_and_physical_examination.general_and_physical_exam)
                    $('.general_and_physical_examination_general_and_physical_exam').find('input[value="'+husband_json.general_and_physical_examination.general_and_physical_exam+'"]').click();
                if(husband_json.general_and_physical_examination.siens_of_virilization)
                    $('.general_and_physical_examination_siens_of_virilization').find('input[value="'+husband_json.general_and_physical_examination.siens_of_virilization+'"]').click();
                if(husband_json.general_and_physical_examination.gynecomastia)
                    $('.general_and_physical_examination_gynecomastia').find('input[value="'+husband_json.general_and_physical_examination.gynecomastia+'"]').click();
                $('.general_and_physical_examination_height').val(husband_json.general_and_physical_examination.height);
                $('.general_and_physical_examination_weight').val(husband_json.general_and_physical_examination.weight);
                
                if(husband_json.uro_general_examination.penis)
                    $('.uro_general_examination_penis').find('input[value="'+husband_json.uro_general_examination.penis+'"]').click();
                if(husband_json.uro_general_examination.tests)
                    $('.uro_general_examination_tests').find('input[value="'+husband_json.uro_general_examination.tests+'"]').click();
                if(husband_json.uro_general_examination.site)
                    $('.uro_general_examination_site').find('input[value="'+husband_json.uro_general_examination.site+'"]').click();
                if(husband_json.uro_general_examination.volume)
                    $('.uro_general_examination_volume').find('input[value="'+husband_json.uro_general_examination.volume+'"]').click();
                if(husband_json.uro_general_examination.epididymitis)
                    $('.uro_general_examination_epididymitis').find('input[value="'+husband_json.uro_general_examination.epididymitis+'"]').click();
                if(husband_json.uro_general_examination.scrotal_swelling)
                    $('.uro_general_examination_scrotal_swelling').find('input[value="'+husband_json.uro_general_examination.scrotal_swelling+'"]').click();
                if(husband_json.uro_general_examination.varicocele)
                    $('.uro_general_examination_varicocele').find('input[value="'+husband_json.uro_general_examination.varicocele+'"]').click();
                if(husband_json.uro_general_examination.injuinal_exam)
                    $('.uro_general_examination_injuinal_exam').find('input[value="'+husband_json.uro_general_examination.injuinal_exam+'"]').click();
                if(husband_json.uro_general_examination.rectual_exam)
                    $('.uro_general_examination_rectual_exam').find('input[value="'+husband_json.uro_general_examination.rectual_exam+'"]').click();
                $('.uro_general_examination_details').val(husband_json.uro_general_examination.details);
                $('.uro_general_examination_volume_text').val(husband_json.uro_general_examination.volume_text);

                 $('.ejectable_volume_nsml').val(husband_json.semen_analysis.ejectable_volume.nsml);
                 $('.ejectable_volume_dis').val(husband_json.semen_analysis.ejectable_volume.dis);
                 $('.ejectable_volume_normal').val(husband_json.semen_analysis.ejectable_volume.normal);
                 $('.ph_nsml').val(husband_json.semen_analysis.ph.nsml);
                 $('.ph_dis').val(husband_json.semen_analysis.ph.dis);
                 $('.ph_normal').val(husband_json.semen_analysis.ph.normal);
                 $('.sperm_consentration_nsml').val(husband_json.semen_analysis.sperm_consentration.nsml);
                 $('.sperm_consentration_dis').val(husband_json.semen_analysis.sperm_consentration.dis);
                 $('.sperm_consentration_normal').val(husband_json.semen_analysis.sperm_consentration.normal);
                 $('.total_sperm_no_nsml').val(husband_json.semen_analysis.total_sperm_no.nsml);
                 $('.total_sperm_no_dis').val(husband_json.semen_analysis.total_sperm_no.dis);
                 $('.total_sperm_no_normal').val(husband_json.semen_analysis.total_sperm_no.normal);
                 $('.percent_motility_nsml').val(husband_json.semen_analysis.percent_motility.nsml);
                 $('.percent_motility_dis').val(husband_json.semen_analysis.percent_motility.dis);
                 $('.percent_motility_normal').val(husband_json.semen_analysis.percent_motility.normal);
                 $('.forward_progression_nsml').val(husband_json.semen_analysis.forward_progression.nsml);
                 $('.forward_progression_dis').val(husband_json.semen_analysis.forward_progression.dis);
                 $('.forward_progression_normal').val(husband_json.semen_analysis.forward_progression.normal);
                 $('.morphology_nsml').val(husband_json.semen_analysis.morphology.nsml);
                 $('.morphology_dis').val(husband_json.semen_analysis.morphology.dis);
                 $('.morphology_normal').val(husband_json.semen_analysis.morphology.normal);
                 $('.sperm_aglutination_nsml').val(husband_json.semen_analysis.sperm_aglutination.nsml);
                 $('.sperm_aglutination_dis').val(husband_json.semen_analysis.sperm_aglutination.dis);
                 $('.sperm_aglutination_normal').val(husband_json.semen_analysis.sperm_aglutination.normal);
                 $('.viscosity_nsml').val(husband_json.semen_analysis.viscosity.nsml);
                 $('.viscosity_dis').val(husband_json.semen_analysis.viscosity.dis);
                 $('.viscosity_normal').val(husband_json.semen_analysis.viscosity.normal);

                if(data.data.family_history){
                    var family_history = JSON.parse(data.data.family_history);
                    if(family_history.ht.male)
                        $(".family_history_ht_male").find('input[value="'+family_history.ht.male+'"]').click();
                    if(family_history.ht.female)
                        $(".family_history_ht_female").find('input[value="'+family_history.ht.female+'"]').click();
                    if(family_history.diabetes.male)
                        $(".family_history_diabetes_male").find('input[value="'+family_history.diabetes.male+'"]').click();
                    if(family_history.diabetes.female)
                        $(".family_history_diabetes_female").find('input[value="'+family_history.diabetes.female+'"]').click();
                    if(family_history.thyroid.male)
                        $(".family_history_thyroid_male").find('input[value="'+family_history.thyroid.male+'"]').click();
                    if(family_history.thyroid.female)
                        $(".family_history_thyroid_female").find('input[value="'+family_history.thyroid.female+'"]').click();
                    if(family_history.kochs.male)
                        $(".family_history_kochs_male").find('input[value="'+family_history.kochs.male+'"]').click();
                    if(family_history.kochs.female)
                        $(".family_history_kochs_female").find('input[value="'+family_history.kochs.female+'"]').click();
                    if(family_history.infertility.male)
                        $(".family_history_infertility_male").find('input[value="'+family_history.infertility.male+'"]').click();
                    if(family_history.infertility.female)
                        $(".family_history_infertility_female").find('input[value="'+family_history.infertility.female+'"]').click();
                    if(family_history.anomalies.male)
                        $(".family_history_anomalies_male").find('input[value="'+family_history.anomalies.male+'"]').click();
                    if(family_history.anomalies.female)
                        $(".family_history_anomalies_female").find('input[value="'+family_history.anomalies.female+'"]').click();
                }

                
            } else {
                $("#male_block").find('input[type="text"]').val('');
                $("#male_block").find(".select_button").find('li.active').removeClass('active')
                $("#male_block").find('.btn.bg-radio_grp.active').removeClass('active');
                $("#female_block").find('input[type="text"]').val('');
                $("#female_block").find(".select_button").find('li.active').removeClass('active')
                $("#female_block").find('.btn.bg-radio_grp.active').removeClass('active');
                $(".family_history_container").find('.btn.bg-radio_grp.active').removeClass('active');
                $('#medical_history_ifr')[0].contentDocument.body.innerHTML = '';
                $('#surgical_history_ifr')[0].contentDocument.body.innerHTML = '';
                $('#medications_ifr')[0].contentDocument.body.innerHTML = '';
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}


function fetchInvestigationHistoryMale (){
    if($('.investigationHistoryContainerMale').find('.popover').css('display') == 'block'){
        $('.investigationHistoryContainerMale').find('.popover').hide();
        $('.investigationHistoryContainerMale').find('.popover-content').empty();
    } else {
        let _token = $('#c_token').val();
        let husband_patient_id = $('#husband_patient_id').val();
        let husband_visit_id = $('#husband_visit_id').val();
        var url = $('#base_url').val() + "/infertility/fetchInvestigationHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "type=1&visit_id="+husband_visit_id+"&patient_id="+husband_patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".investigationHistoryContainerMale").find('.fetchInvestigationIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {
                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                        // $('.investigationHistoryContainerMale').find('.popover').hide();
                        // $('.investigationHistoryContainerMale').find('.popover-content').empty();
                    } else {
                        $('.investigationHistoryContainerMale').find('.popover-content').html(data.history);
                        $('.investigationHistoryContainerMale').find('.popover').show();
                        
                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }
                    
                } 
            },
            complete: function () {
                $(".investigationHistoryContainerMale").find('.fetchInvestigationIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-list');
            }
        });
    }
}


function fetchInvestigationHistoryFemale (){
    if($('.investigationHistoryContainerFemale').find('.popover').css('display') == 'block'){
        $('.investigationHistoryContainerFemale').find('.popover').hide();
        $('.investigationHistoryContainerFemale').find('.popover-content').empty();
    } else {
        let _token = $('#c_token').val();
        let wife_patient_id = $('#wife_patient_id').val();
        let wife_visit_id = $('#wife_visit_id').val();
        var url = $('#base_url').val() + "/infertility/fetchInvestigationHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "type=2&visit_id="+wife_visit_id+"&patient_id="+wife_patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".investigationHistoryContainerFemale").find('.fetchInvestigationIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {
                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                        // $('.investigationHistoryContainerFemale').find('.popover').hide();
                        // $('.investigationHistoryContainerFemale').find('.popover-content').empty();
                    } else {
                        $('.investigationHistoryContainerFemale').find('.popover-content').html(data.history);
                        $('.investigationHistoryContainerFemale').find('.popover').show();
                        
                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }
                    
                } 
            },
            complete: function () {
                $(".investigationHistoryContainerFemale").find('.fetchInvestigationIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-list');
            }
        });
    }
}

function selectPrescriptionType(){
    if($(".prescription_type").parent().hasClass('off')){
        $('#patient_id').val($("#husband_patient_id").val());
        $('#visit_id').val($("#husband_visit_id").val());
        $('#encounter_id').val($("#husband_encounter_id").val());
    } else {
        $('#patient_id').val($("#wife_patient_id").val());
        $('#visit_id').val($("#wife_visit_id").val());
        $('#encounter_id').val($("#wife_encounter_id").val());
    }
}


function clickToTop(){
    window.scrollTo(0,0);
}

function checkVisitExist(){
    var wife_visit_id = $('#wife_visit_id').val();
    var husband_visit_id = $('#husband_visit_id').val();

    if(parseInt(wife_visit_id) == 0 && $(".wife_uhid").val() != ''){
        bootbox.alert('The Wife/Female has no visit for today. Please contact front office.');
        $(".saveInfertilityDiv").hide();
        $(".wife_block_head").addClass('blink_me');
        $(".wife_block_head").removeClass('text-blue').addClass('text-red');
        return false;
    }

    if(parseInt(husband_visit_id) == 0 && $("#op_no_search").val() != ''){
        bootbox.alert('This Husband/Male has no visit for today. Please contact front office.');
        $(".saveInfertilityDiv").hide();
        $(".husband_block_head").addClass('blink_me');
        $(".husband_block_head").removeClass('text-blue').addClass('text-red');

        return false;
    }

    fetchInfertilityDetails();

    return true;
}


$(document).on("click", '.deleteInvestigationButton', function(){
    var that = this;
    var investigation_detail_id = $(this).attr('data-investigation-detail-id');
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/infertility/deleteInvestigation";
    $.ajax({
        type: "POST",
        url: url,
        async:true,
        data: "investigation_detail_id="+investigation_detail_id+"&_token="+_token,
        beforeSend: function () {
            $(that).find('.deleteInvestigationIcon').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"]("Success");
                if(parseInt($(that).attr("data-type")) == 1)
                    fetchInvestigationHistoryMale();
                else if(parseInt($(that).attr("data-type")) == 2)
                    fetchInvestigationHistoryFemale();
            } 
        },
        complete: function () {
            $(that).find('.deleteInvestigationIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
        }
    });
})