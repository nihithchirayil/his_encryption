$(document).ready(function(){
 $('#search_inves_type').hide();
    $('#inves_list').hide();
    $('#ListInvesFavorites').sortable({
        delay: 150,
        stop: function(event, ui) {

        rearrange_service_order();
        }
    });
});
function rearrange_service_order(){
    var array_grp  = [];
    var user_name = $("input[name=search_user_id_hidden]").val();
    var group_id = $('#ListInvesFavorites tr').attr('data-group-id');
    var type = $('#m_s_type_lab').val();
    $("#ListInvesFavorites tr td").each(function(){
        var service_code = $(this).find("input[name='fav_service_code[]']").val();
        var service_name = $(this).find("input[name='fav_service_name[]']").val();
        if(service_code != undefined && service_name != undefined){
            array_grp.push({service_code:service_code,service_name:service_name})
        }
    })
    console.log(array_grp)

    var url = $('#base_url').val() + "/investigation/update_group_service";
        $.ajax({
            url:url,
            type:'post',
            data:{position:array_grp,group_id:group_id,type:type,user_name:user_name},
            success:function(data){
                toastr.success('Your Change Successfully Saved.');
            }
        })
    console.log(array_grp);
}
//investigation item search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_inv_item"]', function (event) {

    let groups = $("input[name=get_grp_id]").val();
    if(groups == 0){
         Command: toastr["warning"]("Select Group");
    }else{
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
   // var patient_id = $('#patient_id').val();
    var search_type = $("input[name='i_search_type']:checked").val();
    var inv_list = $('.investigation-list-div');

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(inv_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/investigation/favourite-inv-item-search";

            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: search_string,
                    search_type: search_type
                },
                beforeSend: function () {
                    $('#InvestigationTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    //alert(data);
                    let response = data;
                    let res_data = "";


                    var search_list = $('#ListInvestigationSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let service_desc = response[i].service_desc;
                            let service_code = response[i].service_code;
                            let dept_name = response[i].dept_name;

                            res_data += '<tr><td>' + service_desc + '</td><td>' + service_code + '</td><input type="hidden" name="list_service_name_hid[]" id="list_service_name_hid-' + i + '" value="' + service_desc + '"><input type="hidden" name="list_service_code_hid[]" id="list_service_code_hid-' + i + '" value="' + service_code + '"><input type="hidden" name="list_search_type_hid[]" id="list_search_type_hid-' + i + '" value="' + dept_name + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".inv_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.inv_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}

});



//when select medicine
$(document).on('dblclick', '#ListInvestigationSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let service_name = $(tr).find('input[name="list_service_name_hid[]"]').val();
    let service_code = $(tr).find('input[name="list_service_code_hid[]"]').val();
    let search_type = $(tr).find('input[name="list_search_type_hid[]"]').val();


    if (service_name != '' && service_code != '') {
        $('input[name="search_inv_item"]').val(service_name);
        $('input[name="search_service_code_hidden"]').val(service_code);
        $('input[name="search_type_hidden"]').val(search_type);

        $(".investigation-list-div").hide();

    }

});




//add medicine
function addNewService() {
    let status = validateRow();
    if (status == false) {
        console.log("Error");
        return false;
    }

    let mode = 'add';

    getNewRowInserted(mode);

    $('input[name="search_inv_item"]').focus();
    $("#td_1").hide();
}

//add row
function getNewRowInserted(mode = 'add', data = {}) {

    let code = "";
    if (mode == 'add') {
        code = $('input[name="search_service_code_hidden"]').val();
    }else{
        if (Object.keys(data).length > 0) {
            code = data.service_code;
        }
    }


    var table = document.getElementById("FavTableList");
    let row_count = table.rows.length;
    let row_id = row_count;
    if (parseInt(row_count) > 0) {
        let last_id = $('#FavTableList tr:last').attr('row-id');
        if (last_id != undefined) {
            row_id = parseInt(last_id) + 1;
        }
    }
    var grp_id = $('.group_id').val();
    // var attr_grp_id = $('ListInvesFavorites tr').attr('data-group-id','11');
    var row = table.insertRow(row_count);

    row.setAttribute("row-id", row_id, 0);
    row.setAttribute("data-group-id", grp_id);

    var cell1 = row.insertCell(0);
    cell1.width = '50%';
  //  var cell2 = row.insertCell(1);
   // cell2.width = '30%';
    var cell2 = row.insertCell(1);
    cell2.classList.add("text-center");
    cell2.width = '5%';

    let service_code = "";
    let service_name = "";
    let search_type = "";
    let id = "";

    if (mode == 'add') {

        service_code = $('input[name="search_service_code_hidden"]').val();
        service_name = $('input[name="search_inv_item"]').val();
        search_type = $("input[name='i_search_type']:checked").val();

    } else {
        //load bsed on prescription_id
        if (Object.keys(data).length > 0) {
            service_code = data.service_code;
            service_name = data.service_name;
            search_type = data.search_type;
            id = data.id;

            if(mode == 'copy'){
                id = "";
            }

        }
    }

    let service_name_popup_search = '<div class="investigation-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_med_search">X</a><div class="inv_theadscroll" style="position: relative;"><table id="InvestigationTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped inv_theadfix_wrapper"><thead><tr class="light_purple_bg"><th>Medicine</th><th>Generic Name</th></tr></thead><tbody id="ListInvestigationSearchDataRowListing-' + row_id + '" class="list-investigation-search-data-row-listing" ></tbody></table></div></div>';

    cell1.innerHTML = "<span class='service_name'>" + service_name + "</span> <input type='hidden' name='fav_service_code[]' id='selected_service_code-" + row_id + "' value='" + service_code + "'><input type='hidden' class='form-control' name='fav_service_name[]' id='selected_service_name-" + row_id + "' value='" + service_name + "'><input type='hidden' name='selected_search_type[]' id='selected_search_type-" + row_id + "' value='" + search_type + "'><input type='hidden' name='selected_edit_id[]' id='selected_edit_id-" + row_id + "' value=''>" + service_name_popup_search;
   // cell2.innerHTML = "<span class='search_type'>" + search_type + "</span> <input type='hidden' class='form-control' name='' id='" + row_id + "' value=''>";
    // cell2.innerHTML = "<i class='fa fa-trash delete-investigation-row'></i> &nbsp<i class='fa fa-edit edit-investigation-row'></i>";
 cell2.innerHTML = "<button class='btn btn-success edit-inves-row' title='Edit' type='button'><i  class='fa fa-edit'></i></button> &nbsp;<button class='btn  btn-danger del-inves-row' title='Delete' type='button'><i  class='fa fa-trash-o'></i></button>";
    if (mode == 'add') {
        clearmedfields();
    }
    saveInvestigationFavourite();
}

//clear fields
function clearmedfields() {
   // $('input[name="search_service_code_hidden"]').val('');
    $('input[name="search_inv_item"]').val('');
}

//validate top row
function validateRow() {
    let err = 0;

    let service_name = $('input[name="search_inv_item"]').val();

    let service_code = $('input[name="search_service_code_hidden"]').val();
    if (service_name == '' || service_name == undefined) {
        Command: toastr["error"]("Enter Item.");
        err = 1;
    }

    //same item already exists
    let alredy_exist = $('input[name="fav_service_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == service_code && service_code != "") {
            return service_code;
        }
    });

    if (alredy_exist.length > 0) {
        Command: toastr["error"]("Item already exist.");
        err = 1;
    }

     //same item already exists
    let alredy_exist1 = $('input[name="fav_service_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == service_code && service_code != "") {
            return service_code;
        }
    });

    if (alredy_exist1.length > 0) {
        Command: toastr["error"]("Item already exist.");
        err = 1;
    }

    if (err == 1) {
        return false;
    } else {
        return true;
    }
}





var save_starts = 0;
//save Investigation
function saveDoctorInvestigationFav() {

    let _token = $('#c_token').val();

    let groups = $("input[name=get_grp_id]").val();

     let user_id = $("input[name=search_user_id_hidden]").val();

      let search_service_code_hidden = $("input[name=search_service_code_hidden]").val();

       let search_type_hidden = $("input[name=search_type_hidden]").val();

  //  let investigation_form = $('#investigation-data-form').serialize();
     var edit_id = $("input[name='edit_id']").val();
    var i_search_type = $("input[name='i_search_type']:checked").val();
//alert(investigation_form);
    if (edit_id != '' && edit_id != 0) {
            deleteInvestigationRow(edit_id);
             $('#FavTableList tr#'+edit_id+'').remove();
        }

        var url = $('#base_url').val() + "/investigation/save-investigation-favourite";
        save_starts = 1;
        $('.save_btn_bg').prop({ disable: 'true' });
        $.ajax({
            type: "POST",
            async: false,
            url: url,
           data: {
                   groups: groups,
                    search_service_code_hidden: search_service_code_hidden,
                     search_type_hidden: search_type_hidden,
                    user_id: user_id,
                    _token: _token
                },
            beforeSend: function () {
                 $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
            },
            success: function (data) {
                if (data.status == 1) {
                    Command: toastr["success"]("Saved.");
                   $('input[name="favitemid[]"]').val(data.id);
                    $('input[name="selected_edit_id[]"]').val(data.id);
                }

            },
            complete: function () {

                $('.save_btn_bg').prop({ disable: 'false' });
                 $("body").LoadingOverlay("hide");
            }
        });


}


$(document).on('click', '.del-inves-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="selected_edit_id[]"]').val();
       // alert(edit_id);
        if (edit_id != '' && edit_id != 0) {
            deleteRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});

//delete single row from db
function deleteRowFromDb(id) {
    var url = $('#base_url').val() + "/investigation/investigation-delete-item";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}



function saveInvestigationFavourite() {
    let _token = $('#c_token').val();
    let search_inv_item = $("input[name=search_inv_item]").val();
    if(search_inv_item != ''){
        addNewService();
    }

    //save as drafts when not saved
    //prescription
    if ($('#FavTableList tbody > tr').find('input[name="fav_service_code[]"]').length == 0) {
        //no medicine in the list
    } else {
        saveDoctorInvestigationFav();
    }

}





$(document).on('click', '.delete-investigation-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="favitemid[]"]').val();
        if (edit_id != '' && edit_id != 0) {
            deleteInvestigationRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});

//delete single row from db
function deleteInvestigationRowFromDb(id) {
    var url = $('#base_url').val() + "/investigation/investigation-delete-item";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}


function deleteInvestigationRow(id) {
    var url = $('#base_url').val() + "/investigation/investigation-delete-item";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
               // Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}


$(document).on('click', '.edit-investigation-row', function () {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="favitemid[]"]').val();
        let fav_service_name =  $(tr).find('input[name="fav_service_name[]"]').val();
        let fav_service_code =  $(tr).find('input[name="fav_service_code[]"]').val();
        let fav_type          =  $(tr).find('input[name="fav_type[]"]').val();

        if (fav_service_code != '' && edit_id != '') {
           //  deleteInvestigationRow(edit_id);
            $('input[name="search_inv_item"]').val(fav_service_name);
            $('input[name="edit_id"]').val(edit_id);
            $('input[name="search_service_code_hidden"]').val(fav_service_code);
            $('input[name="search_type_hidden"]').val(fav_type);

        }
});

$(document).on('click', '.edit-inves-row', function () {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="selected_edit_id[]"]').val();
      //  alert(edit_id);
        let fav_service_name =  $(tr).find('input[name="fav_service_name[]"]').val();
        let fav_service_code =  $(tr).find('input[name="fav_service_code[]"]').val();
        let fav_type          =  $(tr).find('input[name="selected_search_type[]"]').val();

        if (fav_service_code != '' && edit_id != '') {
           //  deleteInvestigationRow(edit_id);
            $('input[name="search_inv_item"]').val(fav_service_name);
            $('input[name="edit_id"]').val(edit_id);
            $('input[name="search_service_code_hidden"]').val(fav_service_code);
            $('input[name="search_type_hidden"]').val(fav_type);

        }
});


//close medicine search
$(document).on('click', '.investigation-list-div > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".investigation-list-div").hide();
});


//user search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_user"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var user_list = $('.user-list-div');

     $(".favorite-inves-list-popup").hide();
    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(user_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/frequency/user-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: search_string,

                },
                beforeSend: function () {
                    $('#UserTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    //alert(data);
                    let response = data;
                    let res_data = "";


                    var search_list = $('#ListUserSearchData');

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let user_name = response[i].user_name;
                             let user_id = response[i].user_id;

                            res_data += '<tr><td>' + user_name + '</td><input type="hidden" name="list_user_name_hid[]" id="list_user_name_hid-' + i + '" value="' + user_name + '"><input type="hidden" name="list_user_id_hid[]" id="list_user_id_hid-' + i + '" value="' + user_id + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".user_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.user_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});


$(document).on('dblclick', '#ListUserSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */
     $(".favorite-inves-list-popup").show();
    let _token = $('#c_token').val();
    let tr = $(this);
    let name = $(tr).find('input[name="list_user_name_hid[]"]').val();
    let user_id = $(tr).find('input[name="list_user_id_hid[]"]').val();
    $("#ListInvesFavorites").html('');
    if (name != '' && user_id != '') {
        $('input[name="search_user"]').val(name);
        $('input[name="search_user_id_hidden"]').val(user_id);

          var url = $('#base_url').val()+"/investigation/investigation-favorite-groups";
            $.ajax({
              type: "POST",
              url: url,
              async: false,
             data: "load_groups=" + 1+"&_token="+_token+'&user_id='+user_id,
              beforeSend: function () {
                $('#fav_groups').html('<div class="spinner_hide col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></div>');
              },
                success: function (data) { 
                        if(data.status == 1){
                            if ($.isArray(data.data_val)) {
                                var dataArray = data.data_val;
                            } else {
                                var dataArray = (typeof data.data_val === 'string') ? JSON.parse(data.data_val) : $.makeArray(data.data_val);
                            }
                            var i=0;
                            // var dataArray = JSON.parse(dataArray);

                            console.log(dataArray);

                            dataArray.forEach(function(val, key) {
                                var newRow = $('<tr>', {
                                  id: val.id,
                                  'data-display-order': val.display_order,
                                  'data-name': val.group_name,
                                }).addClass('fav_grp_clss');
                              
                                var cell = $('<td>', {
                                  id: val.id,
                                  style: 'height:30px;cursor: pointer;'
                                }).click(function() {
                                  list_itemsByGroup(val.id);
                                }).text(val.group_name);
                              
                                var hiddenInput = $('<input>', {
                                  type: 'hidden',
                                  name: 'del_grp_id[]',
                                  value: val.id
                                });
                              
                                var deleteButton = $('<button>', {
                                  class: 'btn btn-danger delete-group-row',
                                  type: 'button',
                                  style: 'float: right;'
                                }).click(function() {
                                  // Handle delete group action here
                                }).html('<i class="fa fa-trash-o"></i>');
                              
                                cell.append(hiddenInput, deleteButton);
                                newRow.append(cell);
                                $('#fav_groups').append(newRow);
                              });
                        }
                         $('#search_inves_type').show();
                        $('#inves_list').show();
                        $('.spinner_hide').hide();
                        $('#ListInvesFavorites').sortable({
                            delay: 150,
                            stop: function(event, ui) {
                    
                            rearrange_service_order();
                            }
                        });
                        $('#fav_groups tbody').sortable({
                            delay: 150,
                            stop: function(event, ui) {

                            rearrange_order();
                            }
                        });
                      },
                      complete: function () {
                        let group_length = $("#fav_groups td").length;
console.log(group_length);
                        if(group_length > 0){
                      let groupid = $('#fav_groups td:first').attr('id');
                       if(groupid == undefined){
                          grp_id = 0;
                      }else{
                          grp_id = groupid;
                      }
                      list_itemsByGroup(groupid);
                        }

                      }
            });

        $(".user-list-div").hide();

    }

});

function rearrange_order(){
    var array_grp  = [];
    var user_name = $("input[name=search_user_id_hidden]").val();
    $(".fav_grp_clss").each(function(){
        var grp_id = $(this).attr('id');
        var grp_name = $(this).attr('data-name');
        array_grp.push({id:grp_id,group_name:grp_name})
    })
    var url = $('#base_url').val() + "/investigation/update_group";
        $.ajax({
            url:url,
            type:'post',
            data:{position:array_grp,user_name : user_name},
            success:function(data){
                toastr.success('Your Change Successfully Saved.');
            }
        })
    console.log(array_grp);
}
// list items by groups
function list_itemsByGroup(id){
     clearmedfields();

      var user_id = $("input[name=search_user_id_hidden]").val();
       $('#fav_groups tr#'+id+'').addClass('highlight').siblings().removeClass('highlight');
       $('input[name="get_grp_id"]').val(id);

       $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
         let _token = $('#c_token').val();
         var url = $('#base_url').val()+"/investigation/investigation-favorite-list";
          $.ajax({
              type: "POST",
              url: url,
              data: 'load_group_items=1' + '&group_id=' +id+'&_token='+_token+'&user_id='+user_id,
              success: function (response) {
                if(response.status == 1){
                   $("#ListInvesFavorites").html(response.html);
                }
                //double_fav_click_prevent = 0;
                $("body").LoadingOverlay("hide");
                 $('input[name="search_inv_item"]').focus();
              },
              complete: function (e) {
                $("body").LoadingOverlay("hide");
              }
        });

    }

// user search box closing
$(document).on('click', '.close_btn_user_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".user-list-div").hide();
});


function addGroup() {

  $('input[name="groupname"]').val('');
 $("#group_add_modal").modal('show');
}


function SaveGroup() {

   var user_name = $("input[name=search_user_id_hidden]").val();
    let group_name = $('#groupname').val();
     let _token = $('#c_token').val();

 if ( group_name != '') {
      var url = $('#base_url').val() + "/investigation/save-new-group";
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: 'group_name=' +group_name+'&_token='+_token+'&user_name='+user_name,
            beforeSend: function () {

            },
            success: function (data) {
              
                if (data) {
                    console.log(data.display_order1);
                    Command: toastr["success"]("Saved.");
                     $('#fav_groups #hiderow').hide();
                     $("#fav_groups").append('<tr id='+data.id+' data-name='+ group_name+' data-display-order='+data.display_order1+' class="fav_grp_clss"><td id='+data.id+' style="height:30px;cursor: pointer;" onclick="list_itemsByGroup('+data.id+')">'+group_name+'<input type="hidden" class="" name="del_grp_id[]" value='+data.id+'><button style="float: right" class="btn  btn-danger delete-group-row" type="button"><i class="fa fa-trash-o"></i></button></td></tr>');
                }
                $('#ListInvesFavorites').sortable({
                    delay: 150,
                    stop: function(event, ui) {
            
                    rearrange_service_order();
                    }
                }); 
                $('#fav_groups tbody').sortable({
                    delay: 150,
                    stop: function(event, ui) {

                    rearrange_order();
                    }
                });
            },
            complete: function () {
                 $("#group_add_modal").modal('hide');
            }
        });
         } else {
        toastr.warning("Type Group Name");
    }

}
$('.fav_grp_clss').click(function() {
    // Get the attribute values
  
});

function copyInvData() {

    if($('#search_user').val() == ''){
      toastr.warning("Select User");
   }
   else{
    $("#user_list_modal").modal('show');

}
}


 $('#SaveUser').click(function() {

    var user_form = $('#user_name').val();
        //alert(selected);

     let _token = $('#c_token').val();

     let user_id = $("input[name=search_user_id_hidden]").val();

      var url = $('#base_url').val() + "/investigation/save-new-user";
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: {
                user_form: user_form,
                 user_id: user_id,
                _token: _token
            },
            beforeSend: function () {

            },
            success: function (data) {
                if (data) {
                    Command: toastr["success"]("Saved.");

                }
            },
            complete: function () {
                 $("#user_list_modal").modal('hide');
            }
        });


});


$(document).on('click', '.delete-group-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let del_grp_id = $(tr).find('input[name="del_grp_id[]"]').val();

      //  $('table#FavTableList tr.'+del_grp_id+'').remove();
        if (del_grp_id != '' && del_grp_id != 0) {
            deleteGroupRowFromDb(del_grp_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});

//delete single row from db
function deleteGroupRowFromDb(id) {
    var url = $('#base_url').val() + "/investigation/group-delete-item";
    let _token = $('#c_token').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {

            if (data != '' && data != undefined && data != 0) {
                 $('table#FavTableList tr.'+id+'').remove();
                  $("#td_1").show();
                 $("input[name=get_grp_id]").val("");
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}


