$(document).ready(function () {
    setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 10 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + "/report/advanceCollectionReport";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getcashReportData() {
    var url = route_json.cashcollectionreportresult;
    var filters_list = new Object();
    var chk = document.getElementById("detail"); //to check the  checkbox
    if (chk.checked) {
        var check_box = 1;
    } else {
        var check_box = 0;
    }
    filters_list['detail'] = check_box; //pushing the value into the list;
    var chk = document.getElementById("excludeot_bill"); //to check the  checkbox
    if (chk.checked) {
        var check_box = 1;
    } else {
        var check_box = 0;
    }
    filters_list['excludeot_bill'] = check_box; //pushing the value into the list;
    var filters_value = '';
    var filters_id = '';
    var chk = document.getElementById("exclude_dialysis_trust"); //to check the  checkbox
    if (chk.checked) {
        var check_box = 1;
    } else {
        var check_box = 0;
    }
    filters_list['exclude_dialysis_trust'] = check_box; //pushing the value into the list;
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}

function reset_filter() {
    var current_date = $('#current_date').val();
    $("#user").val([]).change();
    $('#cash_collected_date_from').val(current_date);
    $('#cash_collected_date_to').val(current_date);
    $('#op_no_hidden').val('');
    $('#op_no').val('');
    $('input:checkbox').removeAttr('checked');
    getcashReportData();
}

function showAdvDetails(e,bill_type,type,usr_id){
    $('.adv_refund_color').css('background','none');
    if(bill_type=='ADVANCE' || bill_type=='ADVANCE ADJUST' ){
       
        if($('#adv_'+type+'_'+usr_id).is(':visible')){
            $('#adv_'+type+'_'+usr_id).fadeOut();
            $(e).css('background','none');

        }else{
            $('.adv_refund').hide();
            $(e).css('background','darkseagreen');
            $('#adv_'+type+'_'+usr_id).fadeIn()
        }
    }
}
