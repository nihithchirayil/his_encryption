Highcharts.chart('container', {
    chart:{
        backgroundColor:'aliceblue',
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },

    xAxis: {
        type: 'datetime',
        tickInterval: 1000 * 3600 * 24 *30 // 1 month
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: Date.UTC(2014,0,1)
        }
    },

    series: [{
        name: 'Profit',
        data: [{x: Date.UTC(2014,0,1), y: 43934},
            {x: Date.UTC(2014,1,1), y: 52503},
            {x: Date.UTC(2014,2,1), y: 57177},
            {x: Date.UTC(2014,3,1), y: 69658},
            {x: Date.UTC(2014,4,1), y: 97031},
            {x: Date.UTC(2014,5,1), y: 119931},
            {x: Date.UTC(2014,6,1), y: 137133},
            {x: Date.UTC(2014,7,1), y: 154175}],
        color: 'green',
    }, {
        name: 'Loss',
        data: [{x: Date.UTC(2014,0,1), y: 24916},
            {x: Date.UTC(2014,1,1), y: 24064},
            {x: Date.UTC(2014,2,1), y: 29742},
            {x: Date.UTC(2014,3,1), y: 29851},
            {x: Date.UTC(2014,4,1), y: 32490},
            {x: Date.UTC(2014,5,1), y: 30282},
            {x: Date.UTC(2014,6,1), y: 38121},
            {x: Date.UTC(2014,7,1), y: 40434}],
        color: 'orange',
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});