$(document).ready(function () {
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    getNursingWardBed();
});


function getNursingWardBed() {
    var location_id = $("#location_id").val();
    var bed_idarray = getCheckedColumns('bedstatus_check');
    var bed_string = JSON.stringify(bed_idarray);
    var url = $('#base_url').val() + "/changebedstatus/get-bed-list";
    var param = { location_id: location_id, bed_string: bed_string };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#bedlistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#bedlistdiv').html(data);
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
        },
        complete: function () {

            $('#bedlistdiv').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function checkallBed() {
    var checkallBed = $('#checkall_bed').is(":checked");
    if (checkallBed) {
        $('.columnname_check').prop('checked', true);
    } else {
        $('.columnname_check').prop('checked', false);
    }
}
function getCheckedColumns(class_name) {
    column_array = [];
    $('.' + class_name).each(function (index) {
        var status = $(this).is(":checked");
        if (status) {
            column_array.push($(this).val());
        }
    });
    return column_array;
}

function changebedStatus(from_type) {
    var bed_idarray = getCheckedColumns('columnname_check');
    var token = $('#token_hiddendata').val();
    var bedstatus = '';
    if (bed_idarray.length != 0) {
        if (from_type != '0') {
            var bed_idarray = JSON.stringify(bed_idarray);
            var print_string = " ";
            var sucess_string = "";
            var label_text = '';
            if (from_type == '1') {
                print_string = " Change Bed to House Keeping ?";
                sucess_string = "Bed Successfully Changed To House Keeping";
                label_text = 'House Keeping';
            } else if (from_type == '2') {
                print_string = " Change Bed to Ready ?";
                sucess_string = "Bed Successfully Changed To Ready";
                label_text = 'Ready';
            } else if (from_type == '3') {

                bedstatus = $('input[name="bedstatuschange"]:checked').val();
                if (bedstatus) {
                    if (bedstatus == '1') {
                        print_string = " Ready";
                        sucess_string = "Bed Successfully Changed To Ready";
                        label_text = ' Ready';
                    } else if (bedstatus == '6') {
                        print_string = " House Keeping";
                        sucess_string = "Bed Successfully Changed To House Keeping";
                        label_text = ' House Keeping';
                    } else if (bedstatus == '7') {
                        print_string = " Awaiting Checkout";
                        sucess_string = "Bed Successfully Changed To Awaiting Checkout";
                        label_text = ' Awaiting Checkout';
                    }
                } else {
                    toastr.warning("Please Select Bed Type");
                    return false;
                }
            }
            var message_show = 'Are You Sure You Want To' + print_string;
            var url = $('#base_url').val() + "/changebedstatus/ChangeBedStatusNurseHouseKeeping";
            bootbox.confirm({
                message: message_show,
                buttons: {
                    'confirm': {
                        label: label_text,
                        className: 'btn-primary',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'No',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var param = {
                            _token: token,
                            bed_idarray: bed_idarray,
                            from_type: from_type,
                            bedstatus: bedstatus
                        };
                        $.ajax({
                            type: "POST",
                            async: true,
                            url: url,
                            data: param,
                            cache: false,
                            beforeSend: function () {
                                $('.checkbox').attr('disabled', true);
                                $('#bedlistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                            },
                            success: function (data) {
                                if (data == 1) {
                                    toastr.success(sucess_string);
                                    getNursingWardBed();
                                } else {
                                    toastr.error("Error Please Check Your Internet Connection");
                                }

                            },
                            complete: function () {
                                $('.checkbox').attr('disabled', false);
                                $('#bedlistdiv').LoadingOverlay("hide");
                            }
                        });
                    }
                }
            });
        } else {
            toastr.warning("Permission Denied");
        }
    } else {
        toastr.warning("Please Select Any Bed");
    }
}









