
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
   
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_bill_detail_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
      
        data.bill_no = $("#bill_no").val();
        data.bill_tag = $("#bill_tag").val();
     
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function updateForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};
      
        data.bill_head_id = $(".reference_id").val();
        data.admitting_dr = $("#adm_doctor_id").val();
        data.consulting_dr = $("#cons_doctor_id").val();
      console.log(data.consulting_dr);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
               
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    $("#datapopup").modal('hide');
                    getFilteredItems(window.search_url);
                    resetForm();
                    
            },
            complete: function () {

            }
        });
    }
    function view(element) {
        $("#table").show();
        $("#datapopup").modal('show');
        $(".saveButton").css("display", "none");
        $(".clearFormButton").css("display", "none");
        let bill_head_id = $(element).parents('tr').attr('data-id');
        $(".reference_id").val(bill_head_id);
        $(".titleName").html('Bill Details');
        var url=$('#base_url').val();
   
        var param = {bill_head_id:bill_head_id};

         $.ajax({
            type: "POST",
            url: url + "/master/view_bill_detail",
            data:param,
      
            beforeSend: function () {
                $('#datapopup').modal('show');
                $('#popup').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

            },
            success: function (html) {  
                $('#datapopup').modal('show');
                $("#popup").LoadingOverlay("hide");
                $('#popup').html(html);
                let bill_head_id = $(element).parents('tr').attr('data-id');
                let bill_no = $(element).parents('tr').attr('data-bill_no');
                let bill_tag = $(element).parents('tr').attr('data-bill_tag');
                let admitting_dr = $(element).parents('tr').attr('data-admitting_doctor_name');
                let consulting_dr = $(element).parents('tr').attr('data-consulting_doctor_name');
                let net_amount = $(element).parents('tr').attr('data-net_amount');
                let bill_amount = $(element).parents('tr').attr('data-bill_amount');
                let location = $(element).parents('tr').attr('data-location');
                let paid_status = $(element).parents('tr').attr('data-paid_status');
        
                $(".reference_id").val(bill_head_id);
                $("#bill_no_add").val(bill_no);
                $("#bill_no_add").prop("readonly", true);
                $("#bill_tag_add").val(bill_tag);
                $("#bill_tag_add").prop("readonly", true);
                $("#adm_doctor_id").val(admitting_dr);
                $("#adm_doctor_id").select2().trigger('change'); 
                $("#adm_doctor_id").prop( "disabled", true );           
                $("#cons_doctor_id").val(consulting_dr);
                $("#cons_doctor_id").select2().trigger('change'); 
                $("#cons_doctor_id").prop( "disabled", true );  
                $("#net_amount").val(net_amount);
                $("#net_amount").prop("readonly", true);
                $(".bill_amount").val(bill_amount);
                $("#bill_amount").prop("readonly", true);
                $(".location").val(location);
                $("#location").prop("readonly", true);
                $(".status").val(paid_status);
                $("#status").attr('readonly','readonly');            },
            complete: function () {

            }
        });
    }

    function editItem(element){
        $("#datapopup").modal('show');
       $("#result_table").hide();
        $(".saveButton").css("display", "block");
        $(".clearFormButton").css("display", "block");
        let bill_head_id = $(element).parents('tr').attr('data-id');
        let bill_no = $(element).parents('tr').attr('data-bill_no');
        let bill_tag = $(element).parents('tr').attr('data-bill_tag');
        let admitting_dr = $(element).parents('tr').attr('data-admitting_doctor_name');
        let consulting_dr = $(element).parents('tr').attr('data-consulting_doctor_name');
        let net_amount = $(element).parents('tr').attr('data-net_amount');
        let bill_amount = $(element).parents('tr').attr('data-bill_amount');
        let location = $(element).parents('tr').attr('data-location');
        let paid_status = $(element).parents('tr').attr('data-paid_status');

        $(".reference_id").val(bill_head_id);
        $("#bill_no_add").val(bill_no);
        $("#bill_no_add").prop("readonly", true);
        $("#bill_tag_add").val(bill_tag);
        $("#bill_tag_add").prop("readonly", true);
        $("#adm_doctor_id").val(admitting_dr);
        $("#adm_doctor_id").select2().trigger('change'); 
        $("#adm_doctor_id").prop( "disabled", false );  
        $("#cons_doctor_id").val(consulting_dr);
        $("#cons_doctor_id").select2().trigger('change'); 
        $("#cons_doctor_id").prop( "disabled", false );  
        $("#net_amount").val(net_amount);
        $("#net_amount").prop("readonly", true);
        $(".bill_amount").val(bill_amount);
        $("#bill_amount").prop("readonly", true);
        $(".location").val(location);
        $("#location").prop("readonly", true);
        $(".status").val(paid_status);
        $("#status").attr('readonly','readonly');
        $(".titleName").html('Edit Bill Details');

       

    }

    function reset1(){
        $("#popup").LoadingOverlay("hide");
        $("#result_table").hide();
        $(".reference_id").val('');
        $("#adm_doctor_id").val('').trigger('change');
        $("#cons_doctor_id").val('').trigger('change');
        $("#bill_no_add").val('');
        $("#bill_tag_add").val('');
        $("#net_amount").val('');
        $(".bill_amount").val('');
        $(".location").val('');
        $(".status").val('');
    }
    function resetForm(){
        $(".reference_id").val('');
        $("#result_table").hide();
        $("#adm_doctor_id").val('').trigger('change');
        $("#cons_doctor_id").val('').trigger('change');
        $("#bill_no_add").val('');
        $("#bill_tag_add").val('');
        $("#net_amount").val('');
        $(".bill_amount").val('');
        $(".location").val('');
        $(".status").val('');
      
    }

