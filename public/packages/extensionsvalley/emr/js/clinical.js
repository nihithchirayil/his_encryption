
// $('select[name="forms"]').select2({
//   minimumInputLength: 2,
//   ajax: {
//         url: $('#base_url').val()+'/emr/form-search',
//         dataType: 'json',
//         data: function (params) {
//               var query = {
//                 term: params.term,
//               }
//               return query;
//         },
//         processResults: function (data) {
//             return {
//                 results: data
//             };
//         },
//         cache: true
//   }
// });

$(window).load(function () {
    fetchClinicalAssessmentForms();
    setTimeout(function () {
        $('#Timetaken').datetimepicker({
            format: 'MMM-DD-YYYY hh:mm A'
        });
        $("#Timetaken").attr("autocomplete", "off");
    }, 500);

    $(document).on("click", ".select_button li", function(){
        $(this).toggleClass('active');
    });

});

// $(document).click('.select_button li', function () {
$(document).on("click", ".select_button li", function(){
    $(this).toggleClass('active');
    let sel_value = $(this).attr('data-value');
    if (sel_value != "" && sel_value != undefined) {
        if ($(this).hasClass('active')) {
            $(this).find('input').val(sel_value);
            $(this).find('input').prop("disabled", false);
        } else {
            $(this).find('input').val("");
            $(this).find('input').prop("disabled", true);
        }
    }
});
function fetchClinicalAssessmentForms() {
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        // async: false,
        url: $('#base_url').val() + '/emr/fetchClinicalAssessmentForms',
        data: {
            _token: _token
        },
        beforeSend: function () {

        },
        success: function (data) {
            // console.log(data);
            $("#forms").empty();
            $("#forms").append("<option value=''>Select Template</option>");
            let favourited_form_id = $('#favourited_form_id').val();
            let selected = "";
            $.each(data.forms, function (key, val) {

                if (val != null && val != undefined) {

                    if (parseInt(val.id) == parseInt(favourited_form_id)) {
                        selected = "selected='selected'";
                    } else {
                        selected = "";
                    }

                    $("#forms").append("<option " + selected + " value='" + val.id + "'> " + val.name + "</option>");
                }


            });
            $("#forms").select2();


            if (window.latest_form_id) {
                $('#ca_head_id').val(window.latest_form_head_id);
                $('#ca_edit_status').val(1);
                window.clinical_form_id = window.latest_form_id;
                loadForm(window.latest_form_id, window.latest_form_head_id);
                // $("#forms").val(window.latest_form_id).trigger('change');


            } else if ($("#fav_form_id").val()) {
                $("#forms").val($("#fav_form_id").val()).trigger('change');
                $('.fav-button-forms').addClass('bg-orange');
            }


        },
        complete: function () {

        }
    });

    document.querySelectorAll('.select_button li').forEach(function (li) {
        li.addEventListener('click', function () {
        this.classList.toggle('active');
        });
    });
}

$('select[name="forms"]').on('change', function (event) {
    event.preventDefault();
    /* Act on the event */
    let form_id = $(this).val();
    window.clinical_form_id = form_id;
    // let head_id = $('#ca_head_id').val();
    $('#ca_head_id').val('');
    $('#ca_edit_status').val(0);
    if (form_id != '' && form_id != undefined) {
        setTimeout(function () {
            loadForm(form_id, 0);
        },2000);

    } else {
        $(".clinical_data_content").show();
        $(".investigation_data_content").show();
        $(".prescription_data_content").show();
        $(".body_spinner").hide();
    }

    document.querySelectorAll('.select_button li').forEach(function (li) {
        li.addEventListener('click', function () {
        this.classList.toggle('active');
        });
    });
});

function loadForm(form_id, head_id = 0) {
    let patient_id = $('#patient_id').val();
    if (form_id != '' && form_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            // async: false,
            url: url + "/emr/load-form-fields",
            data: {
                form_id: form_id,
                head_id: head_id,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $('#template-content-load-div').html('<div class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                // console.log(obj);

                $('#template-content-load-div').html(obj.response);
                $(".clinical_data_content").show();
                $(".investigation_data_content").show();
                $(".prescription_data_content").show();
                $(".body_spinner").hide();
                $('#addendum').val(obj.adddendum_data);
                // enableGroupCheck();
                var include_patient_header = $('#include_patient_header').val();
                initTinymce(include_patient_header);

                setTimeout(function () {
                    if(tinymce.activeEditor){
                        if (obj.is_finalized == null || obj.is_finalized != 1) {
                            tinymce.activeEditor.mode.set("design");
                        } else {
                            tinymce.activeEditor.mode.set("readonly");
                        }
                    }
                },5000);

            },
            complete: function () {
                var include_patient_header = $('#include_patient_header').val();
                if (head_id == 0 || head_id == "") {
                    //tinymce
                //    initTinymce(include_patient_header);
                    //progressive search
                    initProgressSearch();

                    initLibraries();
                    checkIsFinalized(head_id);
                } else {

                    // if ($.trim($('#tinymce_names').val()) == "") {
                    //     var tinyArr = '';
                    // } else {
                    //     var tinyArr = JSON.parse($('#tinymce_names').val());
                    // }
                    //destroy
                    // if (tinyArr != '' && tinyArr != undefined) {
                    //     tinymce.remove("textarea.texteditor");
                        // $.each(tinyArr,function(ind,el){
                        // var editorCache = tinymce.get(el); //get from cache
                        // console.log(editorCache);
                        // if(editorCache != '' || editorCache != null|| editorCache != undefined){
                        // tinymce.execCommand('mceRemoveControl', true, el);
                        // }
                        // });
                    // }

                    //tinymce
                    // setTimeout(function () { initTinymce() }, 3000);

                    //progressive search
                    initProgressSearch();

                    initLibraries();

                }

                setTimeout(function () {
                    $('#Timetaken').datetimepicker({
                        format: 'MMM-DD-YYYY hh:mm A'
                    });
                    $("#Timetaken").attr("autocomplete", "off");
                    // enableGroupCheck();
                }, 500);

            }
        });
        // enableGroupCheck();
    }

    document.querySelectorAll('.select_button li').forEach(function (li) {
        li.addEventListener('click', function () {
          this.classList.toggle('active');
        });
    });
}

(function () {
    $(document).on("click", ".select_button li", function(){
        $(this).toggleClass('active');
    });
})();


function checkIsFinalized(head_id){
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        // async: false,
        url: url + "/emr/check-is-finalized",
        data: {
            head_id: head_id,
            _token: _token
        },
        beforeSend: function () {

        },
        success: function (data) {
            if(tinymce.activeEditor){
                if(data == 1){
                    tinymce.activeEditor.mode.set("readonly");
                }else{
                    // tinymce.activeEditor.getBody().setAttribute('contenteditable', true);
                    tinymce.activeEditor.mode.set("design");
                }
            }
        },
        complete: function () {

        }
    });
}

function initLibraries() {
    //datepicker
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('.theadscroll').perfectScrollbar({
        // suppressScrollX: true,
        minScrollbarLength: 30
    });

}

// var specialChars = [
//     { text: 'abdul azeez', value: 'Abdul azeez' },
//     { text: 'fazil', value: 'Fazil Aboobakar'},
//     { text: 'Iqbal', value: 'Iqbal'},
// ];



function initTinymce(include_patient_header) {

    tinymce.init({
        selector: 'textarea.texteditor',
        max_height: 450,
        autoresize_min_height: '90',

        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help bookmarks',
        toolbar: 'undo redo | bold italic underline strikethrough | customInsertButton | fontselect fontsizeselect bookmarks | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat formatselect | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        browser_spellcheck: true,
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,


        nonbreaking_force_tab: true,
        importcss_append: true,

        height: 600,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable addtodictionary',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',

        branding: false,
        statusbar: false,
        forced_root_block: '',
        setup: function (editor) {

            var onAction = function (autocompleteApi, rng, value) {
                editor.selection.setRng(rng);
                editor.insertContent(value);
                autocompleteApi.hide();
            };

            var getMatchedChars = function (pattern) {

                var specialChars = $('#usg_fav_content').val()
                specialChars = specialChars.substring(0,specialChars.length-1);
                specialChars = atob(specialChars);
                specialChars = JSON.parse(specialChars);
                return specialChars.filter(function (char) {
                  return char.text.indexOf(pattern) !== -1;
                });
            };

            editor.ui.registry.addAutocompleter('specialchars', {
                ch: ':',
                minChars: 1,
                columns: 'auto',
                onAction: onAction,
                fetch: function (pattern) {
                  return new tinymce.util.Promise(function (resolve) {
                    var results = getMatchedChars(pattern).map(function (char) {
                      return {
                        type: 'autocompleteitem',
                        value: char.value,
                        text: char.text,
                        icon: char.value
                      }
                    });
                    resolve(results);
                  });
                }
            });


            editor.ui.registry.addButton('bookmarks', {
                text: 'Bookmarks',
                onAction: function () {
                    loadAssesmentBookmarks();
                }
            });

            editor.ui.registry.addButton('addtodictionary', {
                tooltip: 'Add to dictionary',
                icon: 'fa fa-bookmark',
                onAction: function () {
                    addToTinyDictionary();
                },
            });


            editor.ui.registry.addButton('Ucase', {
                text: 'A^',
                onAction: function () {
                    TextToUpperCase();
                },
            });

            editor.ui.registry.addButton('Lcase', {
                text: 'a^',
                onAction: function () {
                    TextToLowerCase();
                },
            });
            editor.ui.registry.addButton('Icase', {
                text: 'I^',
                onAction: function () {
                    TextToInterCase();
                },
            });
            editor.ui.registry.addButton('Ccase', {
                text: 'C^',
                onAction: function () {
                    FirstLetterToInterCase();
                },
            });

            editor.on('keydown', function (event) {
                console.log(event.keyCode);
                //--17- ctrl
                if(event.keyCode == 17){
                    loadTemplateShortCodes();
                }
              });


            // eventTarget.addEventListener("keyup", (event) => {
            //     if (event.isComposing || event.keyCode === 229) {
            //       return;
            //     }
            //     alert('dddd');
            //     // do something
            // });


            // editor.on('change', function(e) {
            //     this.props.updateSectionContents(event.target.getContent());

            // });

        },


    });


    // if (include_patient_header == 1) {
    //     setTimeout(function () { includePatientHeaderTemplate(); }, 3000);
    // }

}

function addToTinyDictionary(){
    alert('ddd');
}

function loadTemplateShortCodes(){
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "GET",
        // async: false,
        url: url + "/emr/load-template-short-codes",
        data: {
            _token: _token
        },
        beforeSend: function () {
            $('#short_code_modal').modal('show');
        },
        success: function (data) {
            $('#short_code_modal_data').html(data);
        },
        complete: function () {

        }
    });
}

function fetchUsgFavToEditor(content){
    tinymce.activeEditor.execCommand('mceInsertContent', false, content);
    $('#short_code_modal').modal('hide');
}

function loadAssesmentBookmarks(){
    var url = $('#base_url').val() + "/ultrasound/favouriteTemplateList";
    var data = {
        showtemplate:'showtemplate'
    };
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        beforeSend: function() {
            $('#favourite_template_list_modal').modal('show');
            $("#favourite_template_list_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function(data) {
            $("#favourite_template_list_data").html(data);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });

            var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.theadscroll');
                    }
            });
        },
        complete: function() {
            $("#favourite_template_list_data").LoadingOverlay("hide", true);
        }

    });
}

function usg_delete_favoutite(id){
    var url = $('#domain_url').val() + "/ultrasound/deleteFavouriteTemplate";
    var data = {
        id: id
    };
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        beforeSend: function() {
            $("#favourite_template_list_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function(data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                Command: toastr["success"](obj.message);
                $('#template_row_' + id).remove();
            }
            else {
                Command: toastr["error"](obj.message);
            }
        },
        complete: function() {
            $("#favourite_template_list_data").LoadingOverlay("hide", true);
        }

    });
}

function usg_add_to_template(id){
    var url = $('#base_url').val() + "/ultrasound/addToTemplate";
    var data = {
        id: id
    };
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        beforeSend: function() {
            $("#favourite_template_list_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function(data) {
            $("#favourite_template_list_data").LoadingOverlay("hide", true);
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                Command: toastr["success"]('Favourite Content Added!');
                $('#favourite_template_list_modal').modal('hide');
                tinymce.activeEditor.execCommand('mceInsertContent', false, obj.content);


            }
            else {
                Command: toastr["error"](obj.message);
            }
        },

    });
}

function usg_edit_favoutite(id){
    // $('#fav_template_edit_modal').modal('show');
    var url = $('#base_url').val() + "/ultrasound/editFavouriteTemplate";
    var data = {
        id: id
    };
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        beforeSend: function() {
            $('#fav_template_edit_modal').modal('show');
            $("#fav_edit_modal_content").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function(data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                $('#fav_template_id').val(obj.template_data[0].id);
                $('#favourite_name_edit').val(obj.template_data[0].name);
                $('#favourite_template_content').val(obj.template_data[0].content);

            }
            else {
                Command: toastr["error"](obj.message);
            }
        },
        complete: function(data){
            $("#fav_edit_modal_content").LoadingOverlay("hide", true);
        },

    });
}

function usg_save_fav_template_edit(){
    var url = $('#base_url').val() + "/ultrasound/saveFavouriteTemplateEdit";
    var favourite_name = $('#favourite_name_edit').val();

    var template_content = $('#favourite_template_content').val();
    var id = $('#fav_template_id').val();
    var token = $('#token').val();
    if (favourite_name == '') {
        Command: toastr["warning"]('Please enter favourite name');
        return false;
    } else{
        var data = {
            favourite_name: favourite_name,
            template_content: template_content,
            id: id,
            _token: token
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function() {
                $("#fav_edit_modal_content").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function(data) {
                var obj = JSON.parse(data);
                if (obj.status == 1) {
                    Command: toastr["success"]('Favourite Content Updated!');
                    $('#fav_template_edit_modal').modal('hide');
                    favourite_template();

                }
                else {
                    Command: toastr["error"](obj.message);
                }
            },
            complete: function(data){
                $("#fav_edit_modal_content").LoadingOverlay("hide", true);
            },

        });
    }
}


// function loadAssesmentBookmarks(){
//     var url = $('#base_url').val();
//     var _token = $('#c_token').val();
//     $.ajax({
//         type: "POST",
//         url: url + "/emr/load-assessment-bookmarks",
//         data: {
//             _token: _token
//         },
//         beforeSend: function () {

//         },
//         success: function (data) {
//             $('#bookmarksModal').modal('show');
//             $('#bookmarksModalBody').html(data);
//         },
//         complete: function () {

//         }
//     });
// }

function includePatientHeaderTemplate() {
    var visit_id = $('#visit_id').val();
    let url = $('#base_url').val();
    $.ajax({
        type: "GET",
        // async: false,
        url: url + "/emr/load-patient-header",
        data: {
            visit_id: visit_id,
        },
        beforeSend: function () {

        },
        success: function (data) {
            tinymce.activeEditor.setContent(data);
        },
        complete: function () {

        },
    });
}

function initProgressSearch() {
    $(".progress-search-multiple").select2({
        placeholder: 'Search...',
        allowClear: false,
        ajax: {
            url: $('#base_url').val() + '/emr/form-progress-search',
            dataType: 'json',
            data: function (params) {
                var query = {
                    term: params.term,
                    table: $(this).attr('data-table'),
                    value: $(this).attr('data-value'),
                    text: $(this).attr('data-text'),
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}

var ca_data_changed = 0;
var ca_tab_changed = 0;
//trigger when click li
$(document).on('click', '#form-content-fields-section li', function (e) {
    ca_tab_changed = 1;
    if (ca_tab_changed == 1 && ca_data_changed == 1) {
        //alert('save');
        ca_data_changed = 0;
        ca_tab_changed = 0;
    }
});

//trigger when data changed
$(document).on('click', '#form-content-data-section :input', function (e) {
    ca_data_changed = 1;
});

//preview button
function loadPreviewTemplate() {
    saveClinicalTemplate(1);
}

var save_ca_starts = 0;
//save clinical assessment
function saveClinicalTemplate(save_status, dont_show_toastr = false) {
    let _token = $('#c_token').val();

    if (save_ca_starts == 1) {
        return false;
    }



    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let ca_head_id = $('#ca_head_id').val();
    let forms = window.clinical_form_id;
    //  let forms = $('#forms').val();
    //alert(forms);return;
    let tableList = $('#table_names').val();
    let gcheckboxNamesArr = $('#gcheckbox_names').val();
    let psearchNamesArr = $('#psearch_names').val();
    let admitting_doctor_id = $('#admitting_doctor_id').val();
    let doctor_id = $('#doctor_id').val();
    let ca_form = $('#ca-data-form').serializeArray();
    let psearchSelectedArr = [];
    //to check there is no data entered
    let dataValueArr = [];
    var edit_status = $('#ca_edit_status').val();
    var addendum = $('#addendum').val();

    //tinymce datas list
    if ($.trim($('#tinymce_names').val()) == "") {
        var tinyList = '';
    } else {
        var tinyList = JSON.parse($('#tinymce_names').val());
    }

    //table strucure check
    //all table field name array
    var tableNames = "";
    if ($.trim(tableList) != "" && $.trim(tableList) != undefined) {
        tableNames = JSON.parse(tableList);
    }
    var tableFieldNames = [];
    var arrTableRowColData = [];
    if ((tableNames.length) > 0) {

        for (var j = 0; j < tableNames.length; j++) {
            var colsFieldName = '';
            var tableWiseCols = '';
            colsCount = $('input[name="' + tableNames[j] + '_#colsCount"]').val();
            rowsCount = $('input[name="' + tableNames[j] + '_#rowsCount"]').val();
            var fieldName = tableNames[j];
            tableFieldNames.push(fieldName);
            arrTableRowColData.push({ 'cols': colsCount, 'rows': rowsCount });
        }

    }

    //group checkbox check
    //all group checkbox field name array
    var gcheckboxNames = gcheckboxNamesArr;

    //all Progress Search field name array
    if ($.trim($('#psearch_names').val()) == "") {
        var psearchNames = '';
    } else {
        var psearchNames = JSON.parse($('#psearch_names').val());
    }


    var datas = {};
    //loop through all fields and create json
    $(ca_form).each(function (index, obj) {

        //tinymce data setup
        if ((tinyList.length) > 0) {
            //check its a tinymce field
            if (tinyList.indexOf(obj.name) >= 0) {
                let tinycontent = '';
                tinycontent = tinyMCE.get(obj.name).getContent();
                ca_form[index].value = tinycontent;
                datas[obj.name] = tinycontent;
                return;
            }
        }

        //progress search data setup [multiselect]
        if ((psearchNames.length) > 0) {
            //check its a progress search field
            if (psearchNames.indexOf(obj.name) >= 0) {
                datas[obj.name + '_psearch_' + obj.value] = obj.value;
                psearchSelectedArr.push(obj.name + '_psearch_' + obj.value);
            }
        }

        datas[obj.name] = obj.value;

        if (obj.value != '' && obj.value != undefined) {
            dataValueArr.push(obj.value);
        }

    });

    //remove progress old keys
    if ((psearchNames.length) > 0) {
        $.each(psearchNames, function (index, psdata) {
            if (psdata != "" && psdata != undefined) {
                delete datas[psdata];
            }
        });
    }

    let validate = validateAssessment(dataValueArr);

    if (forms == "") {
        Command: toastr["warning"]("Error.! Select Form.");
        return false;
    }

    datas = JSON.stringify(datas);
    datas = encodeURIComponent(datas)

    if (validate) {
        var url = $('#base_url').val() + "/emr/save-ca";
        save_ca_starts = 1;
        $.ajax({
            type: "POST",
            url: url,
            // async: false,
            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&ca_head_id=' + ca_head_id + '&form_id=' + forms + '&arrTableRowColData=' + JSON.stringify(arrTableRowColData) + '&tableFieldNames=' + JSON.stringify(tableFieldNames) + '&gcheckboxNames=' + gcheckboxNames + '&psearchNames=' + JSON.stringify(psearchSelectedArr) + '&datas=' + datas + '&edit_status=' + edit_status + '&addendum=' + addendum + '&doctor_id=' + doctor_id,
            beforeSend: function () {
                $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('body').LoadingOverlay("hide");
                // console.log(data);return;
                if (data.status == 1) {
                    window.atleastOneSavedRecords = true;
                    if (dont_show_toastr != true) {
                        Command: toastr["success"]("Save.");
                    }

                    if (window.save_encounter_status && window.save_encounter_status == "TRUE") {
                        window.save_encounter_status = "FALSE";
                        var investigation_head_id_c = $("#investigation_head_id").val();
                        var url = $('#base_url').val() + "/emr/save-doctors-encounter-datas";
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&investigation_head_id=' + investigation_head_id_c,
                            beforeSend: function () {

                            },
                            success: function () {
                                $('.draft_mode_indication_ca').empty()
                            },
                            complete: function () {

                            }
                        });

                    } else {
                        $('.draft_mode_indication_ca').html('<span class="bg-orange draft-badge"> Draft Mode <i class="fa fa-exclamation-circle" title="Clinical Notes saved as draft" aria-hidden="true"></i></span>');
                    }

                   assessmentEditMode(data.form_id, data.head_id, 'edit');
                    //if ($("#print_clinical_notes").is(':checked')) {
                    //    printFormView(data.head_id,data.form_id);
                    // }
                    //loadassessmentPreview(data.head_id,data.form_id);

                } else {
                    Command: toastr["warning"]("Error.");
                }
                save_ca_starts = 0;
                $('#assesment_draft_mode').val(0);

                if (save_status == 2) {
                    printFormView(data.head_id, data.form_id);
                    $('.draft_mode_indication_ca').html('');
                }



            },
            complete: function () {
                //location.reload();
                // medicine_listing_table_changed = 0;
                $('.save_btn_bg').prop({ disable: 'false' });

                if (save_status == 2) {


                    var nursing_station = $("#nursing_station").val();
                    if(nursing_station && nursing_station != ""){
                        $('#ca_edit_status').val(0);

                        // ---------clear all entered data for nursing module---------------
                        $('#section1').find('textarea').val('');
                        $('#section1').find('input:text').val('');
                        $('#section1').find('input:radio').prop('checked', false);
                        $('#section1').find('input:checkbox').prop('checked', false);
                        $('#section1').find('li').removeClass('active');
                        $('#tab1').find('textarea').val('');
                        $('#tab1').find('input:text').val('');
                        $('#tab1').find('input:radio').prop('checked', false);
                        $('#tab1').find('input:checkbox').prop('checked', false);
                        $('#tab1').find('li').removeClass('active');
                        $(".select_button").find('input:hidden').val('')

                    }

                }



                ca_data_changed = 0;
                ca_tab_changed = 0;
                tinymce.remove();
                initTinymce();

                try {
                    updateSeenstatus(visit_id);
                } catch (e) {
                    // console.log(e)
                }
                $('#assesment_draft_mode').val(0);
                // window.onbeforeunload = null;

            }
        });

    } else {
        // medicine_listing_table_changed = 0;
        $('.save_btn_bg').prop({ disable: 'false' });
    }

}
function validateAssessment(dataValueArr) {
    if (Array.isArray(dataValueArr) && dataValueArr.length) {
        return true;
    }
    return false;
}
function assessmentEditMode(form_id, head_id, mode = 'edit') {
    if (head_id != '' && form_id != '' && mode != '') {
        if (mode == 'edit') {
            $("#ca_head_id").val(head_id);
            $('#ca_edit_status').val(1);
        } else {
            $("#ca_head_id").val(0);
        }
        window.clinical_form_id = form_id;
      loadForm(form_id, head_id);
    }
}
function assessmentCopyMode(form_id, head_id, mode = 'copy') {
    if (head_id != '' && form_id != '' && mode != '') {
        if (mode == 'copy') {
            $("#ca_head_id").val(head_id);
        } else {
            $("#ca_head_id").val(0);
        }
        $('#ca_edit_status').val(0);
        window.clinical_form_id = form_id;
        loadForm(form_id, head_id);
        $("#ca_head_id").val(0);
    }
}
function loadassessmentPreview(head_id, form_id) {
    let _token = $('#c_token').val();
    let patient_id = $("#patient_id").val();
    if (head_id > 0) {
        var url = $('#base_url').val() + "/emr/load-preview";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&head_id=' + head_id + '&form_id=' + form_id + '&patient_id=' + patient_id,
            beforeSend: function () {

            },
            success: function (data) {
                if (data.status = 1) {
                    $(".preview_div").html(data.html);
                    $(".expand_preview_btn").trigger('click');
                }
            },
            complete: function () {

            }
        });
    }
}
//load ca view
function loadFormViewPopover(head_id, form_id) {

    if (head_id != '' && head_id != undefined) {

        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        let _token = $('#c_token').val();
        let popover = '1';
        $.ajax({
            type: "POST",
            url: url + "/emr/load-preview",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
                _token: _token,
                popover: popover
            },
            beforeSend: function () {
                // $('.pop-content-history').html('<div class="row"><div colspan="5" class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div></div>');
            },
            success: function (data) {
                $('.pop-content-history').html('');
                let response = '';

                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }

                $('.pop-content-history').html(response);



            },
            complete: function () {

            }
        });
    }

}

function loadFormView(head_id, form_id) {
    if (head_id != '' && head_id != undefined) {
        $('#form_det_list_modal').modal('show');
        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/load-preview",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $('#form_det_list_modal_content').html('<div class="row"><div colspan="5" class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div></div>');
            },
            success: function (data) {
                let response = '';

                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }

                $('#form_det_list_modal_content').html(response);
            },
            complete: function () {

            }
        });
    }
}

//print ca
function printFormView(head_id, form_id) {

    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let patient_id = $("#patient_id").val();
        let _token = $('#c_token').val();
        var font_awesome_path = $('#font_awsome_css_path').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/load-preview",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $('#printFormViewbtn' + head_id).attr('disabled', true);
                $('#printFormViewspin' + head_id).removeClass('fa fa-print');
                $('#printFormViewspin' + head_id).addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                var response = '';
                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }
                //response = response + '<style>.table{border-collapse: collapse !important;} .table tr td{border-collapse: collapse !important;}</style>';
                var winCaPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');

                winCaPrint.document.write("<style>@page {size: A4; border:1px solid black;}</style>");
                winCaPrint.document.write('<style>.list-unstyled{list-style:none;}</style>');
                winCaPrint.document.write('<style>.pain_score_img li{width:25px;display:inline-block;}</style>');
                winCaPrint.document.write('<style>.pain_score_img li img{max-width:100%;}</style>');
                winCaPrint.document.write('<style>.box-body{border:1px solid #ddd !important;float:left;clear:right;width:99%;}</style>');
                winCaPrint.document.write('<style>.pagebreak{page-break-before: always;}</style>');


                winCaPrint.document.write('<style>.col-md-1 {width:8%;  float:left;page-break-inside:auto;}.col-md-2 {width:16%; float:left;page-break-inside:auto;}.col-md-3 {width:25%; float:left;page-break-inside:auto;}.col-md-4 {width:33%; float:left;page-break-inside:auto;}.col-md-5 {width:42%; float:left;page-break-inside:auto;}.col-md-6 {width:50%; float:left;page-break-inside:auto;}.col-md-7 {width:58%; float:left;page-break-inside:auto;}.col-md-8 {width:66%; float:left;page-break-inside:auto;}.col-md-9 {width:75%; float:left;page-break-inside:auto;}.col-md-10{width:83%; float:left;page-break-inside:auto;}.col-md-11{width:92%; float:left;page-break-inside:auto;}.col-md-12{width:100%; float:left;page-break-inside:auto;}</style>');


                winCaPrint.document.write(
                    '<link href="' + font_awesome_path + '" rel="stylesheet">'
                );


                winCaPrint.document.write('<style> table{border-collapse: collapse !important;} table tr td{border-collapse: collapse !important;}</style>' + response + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
                //table{border-collapse: collapse !important;}
                // winCaPrint.document.close();
                // winCaPrint.focus();
                // winCaPrint.print();
//
            },
            complete: function () {
                $('#printFormViewbtn' + head_id).attr('disabled', false);
                $('#printFormViewspin' + head_id).removeClass('fa fa-spinner fa-spin');
                $('#printFormViewspin' + head_id).addClass('fa fa-print');
            }
        });
    }
}

$(document).on('click', '.field-label-tab', function () {
    //Command: toastr["info"]("label changed.");
});

function saveFavTemplate() {
    let selected_form = $('#forms').val();
    if (selected_form != '' && selected_form != undefined) {
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/emr/save-formfavorite";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                selected_form: selected_form,
                _token: _token
            },
            beforeSend: function () {
            },
            success: function (data) {
                //console.log(data);return;
                if (data.status != '' && data.status == 1) {
                    Command: toastr["success"]('Bookmarked Successfully');
                } else {
                    Command: toastr["warning"]('Error..!');
                }
            },
            complete: function () {

            }
        });
    }
}

$(document).on("click", ".fav-list-table thead th", function () {
    //th element
    toogleFavoriteList(this);
});

function toogleFavoriteList(obj) {
    /*active class*/
    $(".fav-list-table").closest('.floating-table-list').not($(obj).closest('.floating-table-list')).removeClass('active-favorite');
    $(obj).closest('.floating-table-list').addClass('active-favorite');
    /*active class*/
    /*tbody hide and show*/
    $(".fav-list-table tbody").not($(obj).closest('table').find('tbody')).hide();
    /*tbody hide and show*/
    /* icon toogle */
    $(".fav-list-table thead th:first-child i").not($(obj).find('i')).removeClass('fa fa-caret-down').addClass('fa fa-caret-left');
    if ($(obj).find('i').hasClass('fa-caret-left')) {
        $(obj).find('i').removeClass('fa fa-caret-left').addClass('fa fa-caret-down');
    } else {
        $(obj).find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-left');
    }
    /* icon toogle */
    $(obj).closest('table').find('tbody').slideToggle();
}

$(document).on('focus', '.custom-text-box,.custom-text-area', function () {
    let name = $(this).prop('name');
    //if favorite enable
    if (name != '' && name != undefined && $('#fav-' + name).length > 0) {
        favoriteViewEnable($(this), name);
    }
});

function favoriteViewEnable(element, name) {
    //$('#fav-'+name+' > table th:first-child').trigger('click');
    if (name != '' && name != undefined && $(element).closest('.row').find('#fav-' + name).length > 0) {
        /*active class*/
        let obj = $(element).closest('.row').find('#fav-' + name + ' table thead th:first-child');
        $(".fav-list-table").closest('.floating-table-list').not($(obj).closest('.floating-table-list')).removeClass('active-favorite');
        $(obj).closest('.floating-table-list').addClass('active-favorite');
        /*active class*/
        /*tbody hide and show*/
        $(".fav-list-table tbody").not($(obj).closest('table').find('tbody')).hide();
        /*tbody hide and show*/
        /* icon toogle */
        $(".fav-list-table thead th:first-child i").not($(obj).find('i')).removeClass('fa fa-caret-down').addClass('fa fa-caret-left');
        $(obj).find('i').removeClass('fa fa-caret-left').addClass('fa fa-caret-down');
        /* icon toogle */
        $(obj).closest('table').find('tbody').show();
    }
}

function favoriteViewShrinkAll() {
    $('.fav-list-table tbody:visible').prev('thead').find('th').trigger('click');
}

function addToFavFormItem(obj, form_item_id, from = 'field') {
    let fav_text = '';

    //in case add favorite from right side bootbox modal then take  favorite text from there else field
    var favSelectObj = $(obj).parent().find(':input');
    fav_text = $(favSelectObj).val();

    var form_id = $('#forms').val();
    if (form_item_id == "" || form_item_id == undefined) {
        Command: toastr["warning"]("Could Not Get Current Item Details..!");
        return false;
    }

    if (fav_text == "" || fav_text == undefined) {
        Command: toastr["warning"]("No Data Found..!");
        $(favSelectObj).focus();
        return false;
    }

    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/save-form-fieldfavorite";

    if (form_id != '' && form_id != undefined) {
        //addToFavoriteTable = 2 in case textarea or text box
        var urlData = "addToFavoriteTable=" + 2 + '&fav_form_id=' + form_id + '&fav_form_item_id=' + form_item_id + '&fav_text=' + fav_text + '&_token=' + _token;
        $.ajax({
            type: "POST",
            url: url,
            data: urlData,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == 1) {
                    Command: toastr["success"]("Saved Successfully..!");
                    var name = "";
                    var type_name = "";


                    name = $(favSelectObj).attr('name');
                    if ($(favSelectObj).is('textarea')) {
                        type_name = "TEXT AREA";
                    } else {
                        type_name = "TEXT";
                    }

                    var id = data.id;
                    var tableDiv = $('#template-content-load-div').find('#fav-' + name);

                    var trlen = $(tableDiv).find('table tbody tr').length;

                    if ($(tableDiv).find('table').hasClass('hidden')) {
                        $(tableDiv).find('table').removeClass('hidden');
                    }


                    //in case all rows deleted manually then
                    if (trlen == 0) {
                        $(tableDiv).find('table tbody').append('<tr><td style="height:30px;cursor: pointer;" onclick="addToSelected(\'' + name + '\',\'' + btoa(fav_text) + '\',\'' + type_name + '\')" >' + fav_text + '</td><td width="5%"><a style="float: right;" class="color-red" name="remove" onclick="removeFavorite(\'' + id + '\',\'' + btoa(fav_text) + '\',this)" title="remove From List"><i class="fa fa-trash-o"></i></a></td></tr>');
                    } else {
                        //already data exist check
                        var dataexist = false;

                        $(tableDiv).find('table tbody tr td:first-child').each(function (index, el) {
                            var cur_el_text = $(el).text();
                            if (cur_el_text == fav_text) {
                                dataexist = true;
                            }
                        });

                        if (dataexist == true) {
                            return false;
                        }

                        $(tableDiv).find('table tbody tr:last').after('<tr><td style="height:30px;cursor: pointer;" onclick="addToSelected(\'' + name + '\',\'' + btoa(fav_text) + '\',\'' + type_name + '\')" >' + fav_text + '</td><td width="5%"><a style="float: right;" class="color-red" name="remove" onclick="removeFavorite(\'' + id + '\',\'' + btoa(fav_text) + '\',this)" title="remove From List"><i class="fa fa-trash-o"></i></a></td></tr>');
                    }


                    let table_tr_elm = $(tableDiv).find('table tbody tr:first').text();
                    if (table_tr_elm != undefined) {
                        if (table_tr_elm.trim() == "No Data Found ..!") {
                            $(tableDiv).find('table tbody tr:first').remove();
                        }
                    }
                }
            },
            complete: function () {
                $('.theadfix_wrapper').floatThead("reflow");
                $('.theadscroll').perfectScrollbar('update');
            }
        });
    } else {
        Command: toastr["warning"]("Could Not Get Form Details..!");
        return false;
    }

}

function removeFavorite(id, cur_fav_text, obj) {
    cur_fav_text = atob(cur_fav_text);
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/remove-form-fieldfavorite";

    if (id != '') {
        $.ajax({
            type: "POST",
            url: url,
            data: "removeFavorite=" + id + "&_token=" + _token,
            beforeSend: function () {
            },
            success: function (data) {
                if (data == 1) {
                    $(obj).closest('tr').remove();
                    Command: toastr["success"]("Removed Successfully..!");
                }
            },
            complete: function () {
            }
        });
    } else {
        Command: toastr["warning"]("Could Not Get Current Item Details..!");
    }
}

function oldDataPreFetch(field_name, form_id, patient_id) {
    if (field_name != '' && form_id != '' && patient_id != '') {
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/emr/load-old-data-form-field";
        $.ajax({
            type: "POST",
            url: url,
            data: "select_old_prefetchdata=" + 1 + "&patient_id=" + patient_id + "&field_name=" + field_name + "&form_id=" + form_id + "&_token=" + _token,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.result != '' && data.type != '') {
                    if (data.type == 'Table') {
                        let resultData = data.result;
                        let j = 0;
                        for (let elem in resultData) {
                            let numRows = resultData[elem].length;
                            for (i = 0; i < numRows; i++) {
                                let dataId = field_name + '_' + j + '_' + i;
                                $('#' + dataId).val(resultData[elem][i]);
                            }
                            j++;
                        }
                    } else if (data.type == 'Grouped CheckBox' || data.type == 'Radio Button' || data.type == 'Check Box') {
                        if (data.result != undefined && data.result != "") {
                            $('input[name="' + field_name + '"][value="' + data.result + '"]').prev().trigger('click');
                        }
                    } else if (data.type == 'Select Box') {
                        if (data.result != undefined && data.result != "") {
                            $('select[name="' + field_name + '"]').val(data.result);
                        }
                    } else {
                        if (data.result != undefined && data.result != "") {
                            $('#' + field_name).val(data.result);
                        }
                    }
                }
            },
            complete: function () {

            }
        });
    }
}

function addToSelected(element_id, cur_fav_text, input_type) {
    cur_fav_text = atob(cur_fav_text);
    if (cur_fav_text != '') {
        //in case text box or textarea
        if (input_type != "" && input_type == "TEXT AREA") {
            var value = $('textarea[name="' + element_id + '"]').val();
            if (value == '') {
                $('textarea[name="' + element_id + '"]').val(cur_fav_text);
            } else {
                $('textarea[name="' + element_id + '"]').val(value + ' , ' + cur_fav_text);
            }
        } else if (input_type != "" && input_type == "TEXT") {
            var value = $('input[name="' + element_id + '"]').val();
            if (value == '') {
                $('input[name="' + element_id + '"]').val(cur_fav_text);
            } else {
                $('input[name="' + element_id + '"]').val(value + ' , ' + cur_fav_text);
            }

        }
    } else {
        Command: toastr["warning"]("Could Not Get Current Item Details..!");
    }
}
function assessmentDeleteMode(obj, id) {
    if (id != "" && id > 0) {
        var result = confirm("Want to delete?");
        if (result) {

            let _token = $('#c_token').val();
            var url = $('#base_url').val() + "/emr/remove-patient-form-data";
            $.ajax({
                type: "POST",
                url: url,
                data: "removeFormData=" + id + "&_token=" + _token,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status == 1) {
                        $(obj).closest('tr').remove();
                        Command: toastr["success"]("Removed Successfully..!");
                    }
                },
                complete: function () {
                }
            });
        }
    }
}

function fetchFavoriteTemplates() {
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/fetchFavoriteTemplates";
    $.ajax({
        type: "POST",
        url: url,
        data: "_token=" + _token,
        beforeSend: function () {
            $(".fetchFavoriteTemplatesBtn").find('i').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            console.log(data);
            $(".fetchFavoriteTemplatesBtn").find('i').addClass('fa-list').removeClass('fa-spinner').removeClass('fa-spin');
            $(".form_fav_list_body").empty();
            if (data.form_fav_list.length > 0) {
                $.each(data.form_fav_list, function (key, val) {
                    var i = key + 1;
                    var actions = "<button type='button' class='btn btn-sm btn-primary' onclick='loadFormView(" + val.formdata_id + "," + val.form_id + ")' ><i class='fa fa-eye'></i> View </button><button type='button' class='btn btn-sm btn-primary' onclick='showForm(" + val.form_id + ", " + val.formdata_id + ");' ><i class='fa fa-copy'></i> Copy </button><button type='button' class='btn btn-sm btn-danger deleteFormFavoriteBtn' onclick='removeFormFromFavorites(" + val.id + ");' ><i class='fa fa-trash'></i> Delete</button>";
                    $(".form_fav_list_body").append("<tr><td>" + i + "</td><td>" + val.name + "</td><td>" + moment(val.created_at).format('DD-MMM-YYYY HH:mm:ss') + "</td><td>" + actions + "</td></tr>");
                });
            } else {
                $(".form_fav_list_body").append("<tr style='text-align:center;'><td colspan='4'>No Bookmarks Found..!</td></tr>");
            }
            $("#form_fav_list_modal").modal('show');
        },
        complete: function () {
        }
    });
}


function removeFormFromFavorites(form_fav_id) {
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/removeFormFromFavorites";
    var that = this;
    $.ajax({
        type: "POST",
        url: url,
        data: "_token=" + _token + "&form_fav_id=" + form_fav_id,
        beforeSend: function () {
            $(that).find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).find('i').addClass('fa-trash').removeClass('fa-spinner').removeClass('fa-spin');
            Command: toastr["success"]("Removed Successfully..!");
            fetchFavoriteTemplates();
        },
        complete: function () {
        }
    });
}


function showForm(form_id, formdata_id) {
    if (formdata_id) {
        loadForm(form_id, formdata_id);
    } else {
        $("#forms").val(window.latest_form_id).trigger('change');
    }

    window.clinical_form_id = form_id;


    $("#form_fav_list_modal").modal('hide');
}

//load history
function fetchAssessmentHistory(page) {
    var url = $('#base_url').val() + "/emr/fetchAssessmentHistory";
    var patient_id = $('#patient_id').val();
    let _token = $('#c_token').val();
    var show_doctor_notes = 0;
    var show_nursing_notes = 0;
    if ($('#chk_show_doctor_notes').prop("checked") == true) {
        show_doctor_notes = 1;
    }
    if ($('#chk_show_nursing_notes').prop("checked") == true) {
        show_nursing_notes = 1;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: {
            clinical_history: 1,
            page: page,
            patient_id: patient_id,
            show_doctor_notes: show_doctor_notes,
            show_nursing_notes: show_nursing_notes,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#ca-history-data-list').html(data);
            }

            $(".pop-history-btn").popover({
                html: true,
                placement: "right",
                trigger: "hover",

                content: function () {
                    return $(".pop-content-history").html();
                }
            });

            $(".pop-history-btn").mouseenter(function () {
                $(".pos_stat_hover ").css('position', 'static');
            });


            $(".pop-history-btn").mouseleave(function () {
                $(".pos_stat_hover ").css('position', 'relative');
            });

        },
        complete: function () {
        }
    });
}



$('#ca-history-data-list').on('click', '.pagination a', function (e) {
    fetchAssessmentHistory($(this).attr('href').split('page=')[1]);
    e.preventDefault();
});


function viewNursesNotes() {
    var visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/emr/viewNursesNotes";
    $.ajax({
        type: "GET",
        url: url,
        data: "&visit_id=" + visit_id,
        beforeSend: function () {
            $('#nursing_notes_modal').modal('show');

            $('#nursing_notes_modal_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#nursing_notes_modal_data').html(data);
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            $('#nursing_notes_modal_data').LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead("reflow");
            $('.theadscroll').perfectScrollbar('update');
        }
    });
}


function favoriteForm(formdata_id, form_id) {
    console.log(formdata_id);
    console.log(form_id);
    var that = this;

    var url = $('#base_url').val() + "/emr/bookmarkAssessment";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            formdata_id: formdata_id,
            form_id: form_id,
            _token: _token
        },
        beforeSend: function () {
            $(that).removeClass('fa-star').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            $(that).removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-star');
        },
        complete: function () {
            Command: toastr["success"]("Bookmarked..!");
        }
    });


}
function assessmentEditModeNew(form_id, head_id, mode = 'edit',timediff) {
    if (head_id != '' && form_id != '' && mode != '') {
        if (mode == 'edit') {
            $("#ca_head_id").val(head_id);
            $('#ca_edit_status').val(1);
        } else {
            $("#ca_head_id").val(0);
        }
        window.clinical_form_id = form_id;
        if(timediff > $('#notes_edit_max_time').val()){
           $('#modalLoginForm').modal('show');
           $('#modalform_id_hidden').val(form_id);
           $('#modalhead_id_hidden').val(head_id);
        }else{
            loadForm(form_id, head_id);
        }




    }
}

function confirmLoginEmrUser(e){
    var url = $('#base_url').val() + "/emr/confirmLoginEmrUser";
    let _token = $('#c_token').val();
    var password=$('#emr_con_password').val().trim();
    if(password){
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    password: password,
                    _token: _token
                },
                beforeSend: function () {
                    $(e).attr('disabled',true);
                    $(e).find('i').removeClass('fa-check').addClass('fa-spinner').addClass('fa-spin');
                },
                success: function (data) {
                    if(data.status==200){
                        form_id=$('#modalform_id_hidden').val();
                        head_id=$('#modalhead_id_hidden').val();
                        loadForm(form_id, head_id);
                        $('#modalLoginForm').modal('hide');

                    }else if(data.status==101){
                        toastr.warning('Credential not match.');
                    }else{
                         toastr.warning('Sorry,something went wrong.');
                    }
                },
                complete: function () {
                    $(e).attr('disabled',false);
                    $(e).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-check');


                }
            });
        }else{
            toastr.warning('Please enter password.');
        }


}
