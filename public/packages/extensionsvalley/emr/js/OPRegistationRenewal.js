$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

var base_url = $("#base_url").val();
var token = $("#c_token").val();

function getReportData() {
    var url = base_url + "/master/OPRegistrationRenewalDetailData";
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var doctor_list = $("#doctor_list").val();
    var param = { from_date: from_date, to_date, to_date, doctor_list: doctor_list }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });
}

function search_clear() {

    var current_date = $('#current_date').val();
    //$(".datapicker").select2('val', '');
    // $(".datapicker").trigger("change");
    $(".datapicker").val([]).change();

    $('#admission_date_from').val(current_date);
    $('#admission_date_to').val(current_date);
    $('#op_no_hidden').val('');
    $('#op_no').val('');
    $('input:checkbox').removeAttr('checked');
}
