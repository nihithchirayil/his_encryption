$(document).ready(function () {
    setTimeout(function () {
        //$(body).addClass('sidebar-collapse');
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");
    }, 300);
    // $('tbody').sortable();
    $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);


});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.labResultReport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();

    if(id!=null){
        fetchCurrentVisitDate(id);
    }

}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getLabReportData() {


    //-------filters---------------------------------------------

    var filters_list = new Object();

    var filters_value = '';
    var filters_id = '';
    var host_name = window.location.origin;
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
        $('#is_finalize').is(":checked") ? filters_list['is_finalize'] = 1 : filters_list['is_finalize'] = 0;

        filters_list['host_name'] = host_name;

 var url = $('#base_url').val() + "/report/labResultReportResults";
    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
             });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}
function occupancy() {
    var url = route_json.currentoccupanciesreport;

    //-------filters---------------------------------------------

    var filters_list = new Object();

    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}







//---------generate csv------------------------------------------------------

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");
};

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}




function print_generate(printData) {

    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');
    $('#total_row').css('display', '');
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;



    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }

    mywindow.document.write(
        '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
    );
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
    );
    mywindow.document.write(
        '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
    );
    mywindow.document.write(
        '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
    );
    mywindow.document.write(
        '<style>.pdf_row{display:none;}</style>'
    );

    mywindow.document.write(showw);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function() {
        mywindow.print();
        mywindow.close();
        InitPrint();
        InitTheadScroll();
    }, 1000);


    return true;
}

function InitPrint(){
    $('#resultDataTable tbody tr').hide();
    $('.pdf_row').show();
    $('#print_config_modal').modal('hide');

    bootbox.alert({
        message: "The PDF results are not included in this print. You may take them separately!",
        className: 'rubberBand animated'
    });

    $('.bg-primary').parent().prepend( "<i class='fa fa-circle text-danger Blink'></i>&nbsp;" );
}

function InitTheadScroll() {

    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
}

function search_clear(){
    $('#ResultsViewArea').html('');
}

function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

function fetchCurrentVisitDate(patient_id){
    var url = route_json.fetchLastVisitDate;
    $.ajax({
        type: "GET",
        url: url,
        data: {patient_id: patient_id},
        beforeSend: function () {

        },
        success: function (html) {
            $('#bill_date_from').val(html);
        },
        complete: function () {

        },
        error: function () {

        }

    });
}

function getLabResultRtf(lab_results_id) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    $.ajax({
        type: "GET",
        url: url,
        data: "lab_results_id=" + lab_results_id,
        beforeSend: function () {
            $('#lab_results_rtf_modal').modal('show');
        },
        success: function (data) {
            // console.log(data);
            $('#lab_results_rtf_data').html(data);
        },
        complete: function () {
        }
    });
}

$('#is_pdf').on('click', function(){
    if($(this).is(':checked')){
        $('#resultDataTable tbody tr').hide();
        $('.pdf_row').show();
    }else{
        $('#resultDataTable tbody tr').show();
    }
});


function exceller() {
    $(".theadfix_wrapper").floatThead('destroy');
    setTimeout(function() {
        var template_date = $('#exceller_data').val();
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = atob(template_date),
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            }
        var toExcel = document.getElementById("excelContainer").innerHTML;
        var ctx = {
            worksheet: name || '',
            table: toExcel
        };
        var link = document.createElement("a");
        link.download = "export.xls";
        link.href = uri + base64(format(template, ctx))
        link.click();

        InitTheadScroll();

    }, 1000);

}
