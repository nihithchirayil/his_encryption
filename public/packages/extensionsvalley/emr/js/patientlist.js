$(document).ready(function() {


});
let myInterval;
function loadpatient(){
    var i = 0;
function change() {
  var doc = document.getElementById("change_color");
  var color = ["white","antiquewhite"];
  doc.style.color = color[i];
  i = (i + 1) % color.length;
}
myInterval=setInterval(change, 800);
}

function searchPatientList(){
    var base_url=$('#base_url').val();
    var url = base_url + "/patient/setPatientListing";
    $.ajax({
        type: "POST",
        url: url,
        data: { },
        beforeSend: function() {
            $('#patient_list_container').html('<i class="fa fa-users" id="change_color" style="width: 58%;font-size: 365px;height: 52%;color: whitesmoke;"></i>');
             loadpatient();

        },
        success: function(data) {
            if (data) {
            $('#patient_list_container').html(data);
            }

        },
        complete: function() {
           $('#change_color').hide();
           clearInterval(myInterval);
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}
function dispalyPatientProfile(){
    $('#patient_profile').modal('show');
}