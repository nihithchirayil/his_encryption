$(document).ready(function () {
    search();
});
$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
//----Hidden Filed Search--------------------------------------------------------------------

$(".hidden_search").keyup(function (event) {
    var input_id = "";
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr("id");
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == "8") {
        if ($("#" + input_id + "_hidden").val() != "") {
            $("#" + input_id + "_hidden").val("");
        }
        var search_key = $(this).val();
        search_key = search_key.replace("/[^ws-_.]/gi", "");
        search_key = search_key.trim();
        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $("#base_url").val();
            var url =base_url+"/item/ajaxSearch_uom";
            $.ajax({
                type: "POST",
                url: url,
                data: "search_key=" + search_key + "&search_key_id=" + input_id,
                beforeSend: function () {
                    $("#" + input_id + "AjaxDiv")
                        .html(
                            '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        )
                        .show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv")
                            .html("No results found!")
                            .show();
                        $("#" + input_id + "AjaxDiv")
                            .find("li")
                            .first()
                            .addClass("liHover");
                    } else {
                        $("#" + input_id + "AjaxDiv")
                            .html(html)
                            .show();
                        $("#" + input_id + "AjaxDiv")
                            .find("li")
                            .first()
                            .addClass("liHover");
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },
            });
        }
    } else {
        ajaxListKeyUpDown(input_id + "AjaxDiv", event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
function search(){
    var uom_id=$('#uom_hidden').val();
    var base_url=$('#base_url').val();
    var token=$('#token_hidde').val();
    var url=base_url+"/item/uomlist";
    $.ajax({
        type:"POST",
        url:url,
        data:{token: token,uom_id:uom_id},
        beforeSend: function () {

            $("#searchBtn").attr("disabled", true);
            $("#searchSpin").removeClass("fa fa-search");
            $("#searchSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function (data) {
            $("#searchDataDiv").html(data);
            $(".page-item").attr("disabled", false);
            $(".page-item.active").attr("disabled", true);
        },
        complete: function () {
            $("#searchBtn").attr("disabled", false);
            $("#searchSpin").removeClass("fa fa-spinner fa-spin");
            $("#searchSpin").addClass("fa fa-search");
        },
        

    });
    
   
}
function saveuom(){
    var uom=$('#uom_save').val();
    var status=$('#status').val();
    var edit=$('#edit').val();
    var base_url = $("#base_url").val();
    var token = $("#token_hidden").val();
    var url=base_url+"/item/saveuom";
    if(!uom){
      toastr.warning('Please enter Uom Name.');
      return;
    }
    $.ajax({
        type: "POST",
        url: url,
        data: {token: token,uom: uom,status: status,edit:edit},
        beforeSend: function () {
            $("#savebtnBtn").attr("disabled", true);
            $("#savebtnSpin").removeClass("fa fa-save");
            $("#savebtnSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function (data) {
            if (data.status==1) {
                toastr.warning(""+data.message+"");
                formReset();
                search();
                
            }else{
                toastr.success(""+data.message+"");
                formReset();
                search();
            }
        },
        complete: function () {
            $("#savebtnBtn").attr("disabled", false);
            $("#savebtnSpin").removeClass("fa fa-spinner fa-spin");
            $("#savebtnSpin").addClass("fa fa-save");
           
        },
        warning: function () {
            toastr.error("Error Please Check Your connection ");
        },
    });


}
function editItem(id,uom_name,status){
    $('#edit').val(id);
    $('#uom_save').val(uom_name);
    $('#status').val(status);


}
function formReset(){
    $('#uom_save').val('');
    $('#status').val(1);
    $('#edit').val('');

}