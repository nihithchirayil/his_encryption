$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2();
    checkEditStatus();
});

var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();
var po_terms_conditions = [];
tinyMceCreate('payment_terms', 500, 0);

request_array = new Array();

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () { focusedElement.select(); }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

function tinyMceCreate(text_area, tinymce_height, read_only) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        imagetools_cors_hosts: ['picsum.photos'],
        browser_spellcheck: false,
        autosave_ask_before_unload: false,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: false,
        contextmenu: false,
        toolbar: false,
        menubar: false,
        importcss_append: false,
        height: 400,
        image_caption: false,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
        readonly: read_only,
    });
}

function setContractButtons(contract_status) {
    $('.savegrnDiv').hide();
    if (parseInt(contract_status) == 1) {
        $('#contract_status').val('Contract Created');
        $('#savegrnDiv4').show();
        $('#savegrnDiv3').show();
        $('#savegrnDiv2').show();
        $("#savegrnDiv1").show();
        $("#savegrnDiv5").show();
    } else if (parseInt(contract_status) == 2) {
        $('#contract_status').val('Contract Approved');
        $('#savegrnDiv4').show();
        $('#savegrnDiv3').show();
        $("#savegrnDiv5").show();
        $("#sendMailDiv").show();
    } else if (parseInt(contract_status) == 3) {
        $('#contract_status').val('Contract Cancelled');
    } else {
        $('#contract_status').val('New Contract');
        $('#savegrnDiv4').show();
        $('#savegrnDiv3').show();
        $('#savegrnDiv2').show();
        $("#savegrnDiv1").show();
    }
}


function checkEditStatus() {
    var contract_id = $('#contract_id').val();
    if (parseInt(contract_id) != 0) {
        var url = base_url + "/quotation/getVendorContractItems";
        vendor_editstatus = 1;
        $.ajax({
            type: "POST",
            url: url,
            data: { _token: token, contract_id: contract_id, from_type: 1 },
            beforeSend: function () {
                $('#addQuotationRowbtn').attr('disabled', true);
                $('#addQuotationRowSpin').removeClass('fa fa-plus');
                $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
                po_terms_conditions = [];
            },
            success: function (data) {
                $('#row_body_data').html(data.html);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                var contract_status = data.contract_head[0].approved_status ? data.contract_head[0].approved_status : 0;
                $('#contract_number').val(data.contract_head[0].contract_no ? data.contract_head[0].contract_no : '-');
                $('#contract_date').val(data.contract_head[0].contract_date ? data.contract_head[0].contract_date : '-');
                $('#closing_date').val(data.contract_head[0].closing_date ? data.contract_head[0].closing_date : '-');
                $('#contract_remarks').val(data.contract_head[0].vendor_remarks ? data.contract_head[0].vendor_remarks : '-');
                $('#item_supplier').val(data.contract_head[0].vendor_name ? data.contract_head[0].vendor_name : '-');
                $('#vender_id').val(data.contract_head[0].vendor_id ? data.contract_head[0].vendor_id : 0);
                $('#vender_code').val(data.contract_head[0].vendor_code ? data.contract_head[0].vendor_code : '');
                if (data.contract_head[0].contract_remarks) {
                    tinymce.get('payment_terms').setContent(data.contract_head[0].contract_remarks);
                }
                $.each(data.contract_terms, function (key, value) {
                    terms_id = parseInt(value.terms_id);
                    po_terms_conditions.push(terms_id);

                });
                setContractButtons(contract_status);
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#addQuotationRowbtn').attr('disabled', false);
                $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addQuotationRowSpin').addClass('fa fa-plus');
                var row_count = $('#editdatalistmaxrow').val();
                $('#row_count_id').val(parseInt(row_count));
            }
        });
    } else {
        setTimeout(function () {
            if ($(".row_class:last").find("input[name='item_desc[]']").val() != '') {
                getNewRowInserted();
            }
        }, 500);
        $('#contract_number').val('-');
        setContractButtons(0)
    }
}


function downloadVendorExcelContract() {
    addQuotationItems();
    var list_items_string = JSON.stringify(request_array);
    var url = base_url + "/quotation/downloadVendorExcelContract";
    var flag = 0;
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, list_items_string: list_items_string },
        beforeSend: function () {
            $('#downloadDocumentBtn').attr('disabled', true);
            $('#downloadDocumentSpin').removeClass('fa fa-download');
            $('#downloadDocumentSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 'NODATA') {
                var file_url = base_url + "/packages/uploads/Purchase/downloads/" + data;
                $("#downloadPoPFDlinka").attr("href", file_url);
                flag = 1;
            } else {
                toastr.warning("No Result Found");
            }
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#downloadDocumentBtn').attr('disabled', false);
            $('#downloadDocumentSpin').removeClass('fa fa-spinner fa-spin');
            $('#downloadDocumentSpin').addClass('fa fa-download');
            if (flag == 1) {
                $('#downloadPoPFDlinka')[0].click();
            }
        }
    });
}

function validateDoc(input_id) {
    var fileInput = document.getElementById(input_id);
    if (fileInput) {
        var filePath = fileInput.value;
        if (filePath) {
            var allowedExtensions = /(\.(xlsx|xls))$/i;
            var file_size = fileInput.size;
            if (!allowedExtensions.exec(filePath) || file_size > 5150) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}


$("#uploadVendorContractForm").on("submit", function (e) {
    e.preventDefault();
    var image_vai = validateDoc("vendor_document");
    if (image_vai) {
        if ($('#vendor_document').val()) {
            var url = base_url + "/quotation/uploadVendorContract";
            var formdata = new FormData(this);
            $.ajax({
                url: url,
                type: "POST",
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#uploadVendorDocumentBtn").attr("disabled", true);
                    $("#uploadVendorDocumentSpin").removeClass("fa fa-upload");
                    $("#uploadVendorDocumentSpin").addClass("fa fa-spinner fa-spin");
                },
                success: function (data) {
                    if (parseInt(data.status) == 1) {
                        $('#row_body_data').html(data.html);
                        document.getElementById('uploadVendorContractForm').reset();
                        $('#row_count_id').val(parseInt(data.item_count) + 1);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function () {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }
                },
                complete: function () {
                    $("#uploadVendorDocumentBtn").attr("disabled", false);
                    $("#uploadVendorDocumentSpin").removeClass("fa fa-spinner fa-spin");
                    $("#uploadVendorDocumentSpin").addClass("fa fa-upload");
                },
                error: function () {
                    toastr.error(
                        "Error Please Check Your Internet Connection and Try Again"
                    );
                },
            });


        } else {
            toastr.warning("<strong>File criteria.</strong><br>Extensions: .xlsx <br>Size : Maximum 5 MB");
        }

    } else {
        toastr.warning("<strong>File criteria.</strong><br>Extensions: .xlsx <br>Size : Maximum 5 MB");
    }


});


function getNewRowInserted() {
    if ($(".row_class:last").find("input[name='item_desc[]']").val() != '') {
        var url = base_url + "/quotation/addVendorPricingNewRow";
        var row_count = $('#row_count_id').val();
        $('#row_count_id').val(parseInt(row_count) + 1);
        $.ajax({
            type: "POST",
            url: url,
            data: { _token: token, row_count: row_count },
            beforeSend: function () {
                $('#addQuotationRowbtn').attr('disabled', true);
                $('#addQuotationRowSpin').removeClass('fa fa-plus');
                $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                $('#row_body_data').append(html);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                serialNoCalculation('row_class', 'row_count_class');

            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#addQuotationRowbtn').attr('disabled', false);
                $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addQuotationRowSpin').addClass('fa fa-plus');
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                $('.form-control').css("border-color", "#fff");
            }
        });
    }
}

function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}


function addQuotationItems() {
    request_array = new Array();
    var flag = true;
    $('.form-control').css("border-color", "#fff");
    $('#row_body_data tr').each(function () {
        var item_id = $(this).find("input[name='item_id_hidden[]']").val();
        if (item_id != '') {
            var row_id = $(this).find("input[name='row_id_hidden[]']").val();
            var uom_id = $('#uom_select_id_' + row_id).val();
            var uom_val = $('#uom_select_id_' + row_id + ' option').data("uom_value");
            var uom_text = $('#uom_select_id_' + row_id + ' option:selected').text();
            var is_free = $(this).find("input[name='is_free']:checked").val();
            var item_desc = $(this).find("input[name='item_desc[]']").val();
            var request_qty = $(this).find("input[name='request_qty[]']").val();
            var unit_rate = $(this).find("input[name='request_unitrate[]']").val();
            var free_qty = $(this).find("input[name='request_freeqty[]']").val();
            var comments = $(this).find("input[name='comments[]']").val();
            if (request_qty == 0 || !request_qty) {
                flag = false;
                $(this).find("input[name='request_qty[]']").css("border-color", "#ff0000");
            }
            if (flag == true) {
                request_array.push({
                    'item_id': item_id
                    , 'row_id': row_id
                    , 'item_desc': item_desc
                    , 'uom_id': uom_id
                    , 'uom_text': uom_text
                    , 'uom_val': uom_val
                    , 'request_qty': request_qty
                    , 'is_free': is_free
                    , 'unit_rate': unit_rate
                    , 'free_qty': free_qty
                    , 'comments': comments
                });
            }
        }
    });
    return flag;
}

function addPotermsConditions(terms_id) {
    terms_id = parseInt(terms_id);
    var status = $('#po_terms_conditions' + terms_id).is(":checked");
    if (status) {
        po_terms_conditions.push(terms_id);
    } else {
        var index = po_terms_conditions.indexOf(terms_id);
        if (index !== -1) {
            po_terms_conditions.splice(index, 1);
        }
    }
}

function getPoTermsConditions() {
    var po_terms = JSON.stringify(po_terms_conditions);
    po_terms = encodeURIComponent(po_terms);
    var url = base_url + "/purchase/getPoTermsConditions";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, po_terms: po_terms },
        beforeSend: function () {
            $('#vendor_contractterms_btn').attr('disabled', true);
            $('#vendor_contractterms_spin').removeClass('fa fa-gavel');
            $('#vendor_contractterms_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#getPoTermsConditionsDiv").html(data);
            $("#getPoTermsConditionsModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            $('#vendor_contractterms_btn').attr('disabled', false);
            $('#vendor_contractterms_spin').removeClass('fa fa-spinner fa-spin');
            $('#vendor_contractterms_spin').addClass('fa fa-gavel');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function searchItemCode(id, event, row_id) { //alert(id);return;
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = String.fromCharCode(event.keyCode); //get the charcode and convert to char
    var ajax_div = $('#' + id).next().attr('id');

    if (value.match(keycheck)) {
        var item_code = $('#' + id).val();
        if (item_code == '') {
            $("#" + ajax_div).html(html).hide();
        } else {
            var location_defaultvalue = $('#location_defaultvalue').val();
            var url = base_url + "/quotation/QuotationItemSearch";
            var param = { _token: token, item_code: item_code, location_defaultvalue: location_defaultvalue, row_id: row_id };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#addQuotationRowbtn').attr('disabled', true);
                    $('#addQuotationRowSpin').removeClass('fa fa-plus');
                    $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $('#addQuotationRowbtn').attr('disabled', false);
                    $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                    $('#addQuotationRowSpin').addClass('fa fa-plus');
                }, error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}


$('#item_supplier').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = String.fromCharCode(event.keyCode); //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox_supplier');
        return false;
    } else if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var item_supplier = $('#item_supplier').val();
        if (!item_supplier) {
            $("#vender_id").val('');
            $("#vender_code").val('');
            $("#ajaxSearchBox_supplier").hide();
        } else {
            var url = base_url + "/quotation/vendorNameSearch";
            var param = { item_supplier: item_supplier };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxSearchBox_supplier").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxSearchBox_supplier").html(html).show();
                    $("#ajaxSearchBox_supplier").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxSearchBox_supplier', event);
    }
});


function removeRow(row_id) {
    bootbox.confirm({
        message: "Are You Sure You want to delete ?",
        buttons: {
            'confirm': {
                label: "Delete",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $("#row_data_" + row_id).remove();
                row_ct = 1;
                $(".row_class").each(function (i) {
                    $(this).find('.row_count_class').text(row_ct);
                    row_ct++;
                });
            }
        }
    });
}


function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name) {
    vendor_name = htmlDecode(vendor_name);
    $("#item_supplier").val(vendor_name);
    $("#vender_id").val(id);
    $("#vender_code").val(vendor_code);
    $("#ajaxSearchBox_supplier").hide();
}



function getItemDescID(itemID) {
    var flag = 1;
    $(".itemdesc_id").each(function (i) {
        var itemID_get = $(this).val();
        if (parseInt(itemID) == parseInt(itemID_get)) {
            flag = 0;
        }
    });
    return flag;
}

function fillItemValues(list, item_code, item_desc, item_id, uom_name, uom_id, uom_value, row_id) {

    var duplicate_check = getItemDescID(item_id);
    if (duplicate_check == 1) {
        var itemCodeListDivId = $(list).parent().attr('id');
        var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
        $('#' + itemCodeTextId).val(item_desc);
        $('#' + itemCodeListDivId).hide();
        $('#' + itemCodeTextId).closest("tr").find("input[name='item_id_hidden[]']").val(item_id);
        fetchUOMData(item_id, row_id, uom_id);
        $('#itemdesc_idhidden' + row_id).val(item_id);
    } else {
        bootbox.confirm({
            message: "This item is already added.<br>Are you sure you want to add ?",
            buttons: {
                'confirm': {
                    label: "Add",
                    className: 'btn-warning',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var itemCodeListDivId = $(list).parent().attr('id');
                    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
                    $('#' + itemCodeTextId).val(item_desc);
                    $('#' + itemCodeListDivId).hide();
                    $('#' + itemCodeTextId).closest("tr").find("input[name='item_id_hidden[]']").val(item_id);
                    fetchUOMData(item_id, row_id, uom_id);
                    $('#itemdesc_idhidden' + row_id).val(item_id);
                }
            }
        });
    }
    setTimeout(function () {
        if ($(".row_class:last").find("input[name='item_desc[]']").val() != '') {
            getNewRowInserted();
        }
    }, 500);

}

function fetchUOMData(item_id, row_id, uom_id) {
    var url = base_url + "/quotation/getItemUom";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, item_id: item_id, uom_id: uom_id },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            response = JSON.parse(response);
            $('#uom_select_id_' + row_id).html(response.option);
            $('#request_qty' + row_id).focus();
        },
        complete: function () {
            setTimeout(function () {
                if ($(".row_class:last").find("input[name='item_desc[]']").val() != '') {
                    getNewRowInserted();
                }
            }, 500);
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
            $('#item_desc_' + row_id).prop("readonly", true);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function saveVendorPriceContract(postType, printVal, fromType) {
    var check_tems = addQuotationItems();
    if (check_tems) {
        var contract_id = $('#contract_id').val();
        var contract_no = $('#contract_number').val();
        var vendor_id = $("#vender_id").val();
        var po_terms_conditions_string = JSON.stringify(po_terms_conditions);
        if (vendor_id != 0) {
            if (request_array.length != 0) {
                var string_data = "";
                if (parseInt(fromType) == 1) {
                    string_data = "Save"
                } else if (parseInt(fromType) == 2) {
                    string_data = "Save &amp; Print"
                } else if (parseInt(fromType) == 3) {
                    string_data = "Approve"
                } else if (parseInt(fromType) == 4) {
                    string_data = "Approve &amp; Print"
                } else if (parseInt(fromType) == 5) {
                    string_data = "Cancel"
                } else if (parseInt(fromType) == 6) {
                    string_data = "Close Bill"
                }
                bootbox.confirm({
                    message: "Are you sure, you want " + string_data + " ?",
                    buttons: {
                        'confirm': {
                            label: 'Yes',
                            className: 'btn-success',
                            default: 'true'
                        },
                        'cancel': {
                            label: 'No',
                            className: 'btn-warning'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            var fetchitem = JSON.stringify(request_array);
                            fetchitem = encodeURIComponent(fetchitem);
                            var url = base_url + "/quotation/saveVendorPricing";
                            var closing_date = $('#closing_date').val();
                            var contract_date = $('#contract_date').val();
                            var contract_remarks = $('#contract_remarks').val();
                            var payment_terms = tinymce.get('payment_terms').getContent();
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: { _token: token, postType: postType, closing_date: closing_date, contract_date: contract_date, contract_id: contract_id, contract_no: contract_no, vendor_id: vendor_id, fetchitem: fetchitem, contract_remarks: contract_remarks, po_terms_conditions_string: po_terms_conditions_string, payment_terms: payment_terms },
                                beforeSend: function () {
                                    $('#savegrnBtn' + fromType).prop('disabled', true);
                                    $('#savegrnSpin' + fromType).removeClass('fa fa-save');
                                    $('#savegrnSpin' + fromType).addClass('fa fa-spinner fa-spin');
                                },
                                success: function (result) {
                                    confirmPONo(result.contract_no, postType, printVal);

                                },
                                complete: function () {
                                    $('#savegrnBtn' + fromType).prop('disabled', false);
                                    $('#savegrnSpin' + fromType).removeClass('fa fa-spinner fa-spin');
                                    $('#savegrnSpin' + fromType).addClass('fa fa-save');
                                },
                                error: function () {
                                    toastr.error('Please check your internet connection and try again');
                                }
                            });
                        }
                    }
                });



            } else {
                toastr.warning("Please Enter Any Item");
            }
        } else {
            toastr.warning("Please Enter Any Vendor");
        }
    }
}


function confirmPONo(contract_no, post_type, printVal) {
    var post_string = '';
    if (post_type == '1') {
        post_string = "Contract Created";
    } else if (post_type == '2') {
        post_string = "Contract Approved";
    } else if (post_type == '3') {
        post_string = "Contract Cancelled";
    }
    $('#contract_status').val(post_string);
    $('#contract_number').val(contract_no);
    bootbox.alert({
        title: post_string + " Successfully ",
        message: "Contract No. :" + contract_no,
        callback: function () {
            if (parseInt(printVal) == 1) {
                getPrintPreview(2);
            } else {
                url = base_url + "/quotation/vendorContractList";
                document.location.href = url;
            }

        }
    });
}


function getPrintPreview(from_type) {
    var check_tems = addQuotationItems();
    if (check_tems) {
        var contract_id = $('#contract_id').val();
        var contract_no = $('#contract_number').val();
        var vendor_id = $("#vender_id").val();
        var po_terms_conditions_string = JSON.stringify(po_terms_conditions);
        if (parseInt(from_type) == 1) {
            $('#loadPrintData').val(0);
        } else {
            $('#loadPrintData').val(1);
        }
        if (vendor_id != 0) {
            if (request_array.length != 0) {
                var fetchitem = JSON.stringify(request_array);
                fetchitem = encodeURIComponent(fetchitem);
                var url = base_url + "/quotation/printVendorContractPreview";
                var closing_date = $('#closing_date').val();
                var contract_date = $('#contract_date').val();
                var contract_remarks = $('#contract_remarks').val();
                var contract_status = $('#contract_status').val();
                var payment_terms = tinymce.get('payment_terms').getContent();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, closing_date: closing_date, contract_date: contract_date, contract_id: contract_id, contract_no: contract_no, vendor_id: vendor_id, fetchitem: fetchitem, contract_remarks: contract_remarks, po_terms_conditions_string: po_terms_conditions_string, payment_terms: payment_terms, contract_status: contract_status },
                    beforeSend: function () {
                        $('#contractPreviewBtn').prop('disabled', true);
                        $('#contractPreviewSpin').removeClass('fa fa-eye');
                        $('#contractPreviewSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        $('#contract_print_model_div').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function () {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                        $("#contract_print_model").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    },
                    complete: function () {
                        $('#contractPreviewBtn').prop('disabled', false);
                        $('#contractPreviewSpin').removeClass('fa fa-spinner fa-spin');
                        $('#contractPreviewSpin').addClass('fa fa-eye');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });

            } else {
                toastr.warning("Please Enter Any Item");
            }
        } else {
            toastr.warning("Please Enter Any Vendor");
        }
    }
}

function DownloadVendorContract(from_type) {
    var check_tems = addQuotationItems();
    if (check_tems) {
        var contract_id = $('#contract_id').val();
        var contract_no = $('#contract_number').val();
        var vendor_id = $("#vender_id").val();
        var po_terms_conditions_string = JSON.stringify(po_terms_conditions);
        if (vendor_id != 0) {
            if (request_array.length != 0) {
                var fetchitem = JSON.stringify(request_array);
                fetchitem = encodeURIComponent(fetchitem);
                var url = base_url + "/quotation/downloadVendorContract";
                var closing_date = $('#closing_date').val();
                var contract_date = $('#contract_date').val();
                var contract_remarks = $('#contract_remarks').val();
                var payment_terms = tinymce.get('payment_terms').getContent();
                var contract_status = $('#contract_status').val();
                var flag = 0;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, closing_date: closing_date, contract_date: contract_date, contract_id: contract_id, contract_no: contract_no, vendor_id: vendor_id, fetchitem: fetchitem, contract_remarks: contract_remarks, po_terms_conditions_string: po_terms_conditions_string, payment_terms: payment_terms, contract_status: contract_status, from_type: from_type },
                    beforeSend: function () {
                        if (parseInt(from_type) == 1) {
                            $('#DownloadVendorContractBtn').prop('disabled', true);
                            $('#DownloadVendorContractSpin').removeClass('fa fa-file');
                            $('#DownloadVendorContractSpin').addClass('fa fa-spinner fa-spin');
                        } else if (parseInt(from_type) == 2) {
                            $('#contractSendMailBtn').prop('disabled', true);
                            $('#contractSendMailSpin').removeClass('fa fa-envelope');
                            $('#contractSendMailSpin').addClass('fa fa-spinner fa-spin');
                        }
                    },
                    success: function (data) {
                        if (parseInt(from_type) == 1) {
                            var file_url = base_url + "/packages/uploads/Purchase/" + data;
                            $("#downloadPoPFDlinka").attr("href", file_url);
                            flag = 1;
                        } else if (parseInt(from_type) == 2) {
                            toastr.success('Mail Sent Successfully');
                        }
                    },
                    complete: function () {
                        if (parseInt(from_type) == 1) {
                            $('#DownloadVendorContractBtn').prop('disabled', false);
                            $('#DownloadVendorContractSpin').removeClass('fa fa-spinner fa-spin');
                            $('#DownloadVendorContractSpin').addClass('fa fa-file');
                            if (flag == 1) {
                                $('#downloadPoPFDlinka')[0].click();
                            }
                        } else if (parseInt(from_type) == 2) {
                            $('#contractSendMailBtn').attr('disabled', false);
                            $('#contractSendMailSpin').removeClass('fa fa-spinner fa-spin');
                            $('#contractSendMailSpin').addClass('fa fa-envelope');
                        }
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });

            } else {
                toastr.warning("Please Enter Any Item");
            }
        } else {
            toastr.warning("Please Enter Any Vendor");
        }
    }
}

function reloadToList() {
    var loadPrintData = $('#loadPrintData').val();
    if (parseInt(loadPrintData) == 1) {
        url = base_url + "/quotation/vendorContractList";
        document.location.href = url;
    }
}
