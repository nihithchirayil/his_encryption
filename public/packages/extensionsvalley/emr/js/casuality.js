$(document).ready(function (){
    initTinymce();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });
    expandTinymce();
    $('.timepicker').timepicker({
        template: false,
        showInputs: false,
        minuteStep: 5
    });

    addNewMedicine();
    $('#save_and_print_prescription1').css('visibility', 'hidden');


});

$(window).load(function ()
{
    setTimeout(function () {
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });
    },5000);

});

$(window).load(function(){
    // $('.hide_class').hide();
    // $('#save_and_print_prescription1').hide();
    // $('#save_and_print_prescription2').hide();
    // $('#doctor_prescription').css('visibility', 'hidden');
    // $('.card_title').hide();
});

$("td").click(function () {
    $(this).find('input:radio').attr('checked', true);
});

$('.select_button li').click(function () {
    $(this).toggleClass('active')
})

$("#poplink").popover({
    html: true,
    placement: "right",
    trigger: "hover",

    content: function () {
        return $(".pop-content").html();
    }
});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
    panel.style.display = "none";
    } else {
    panel.style.display = "block";
    }
});
}


function initTinymce(){

    tinymce.init({
        selector: 'textarea.texteditor11',
        height: '290',
        autoresize_min_height: '90',
        themes: "modern",
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
        imagetools_cors_hosts: ['picsum.photos'],
        // menubar: 'file edit view insert format tools table help',
        menubar: false,
        statusbar: false,
        toolbar: false,
        // toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        // toolbar_sticky: true,
        browser_spellcheck: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,

        importcss_append: true,

        height: 600,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',

        branding: false,
        statusbar: false,
        forced_root_block: '',
        setup: function(editor) {

            editor.ui.registry.addButton('Ucase', {
                text: 'A^',
                onAction: function() {
                    TextToUpperCase();
                },
            });
            editor.ui.registry.addButton('Lcase', {
                text: 'a^',
                onAction: function() {
                    TextToLowerCase();
                },
            });
            editor.ui.registry.addButton('Icase', {
                text: 'I^',
                onAction: function() {
                    TextToInterCase();
                },
            });
            editor.ui.registry.addButton('Ccase', {
                text: 'C^',
                onAction: function() {
                    FirstLetterToInterCase();
                },
            });

        },


    });
}

function expandTinymce(){
    $('.accordion').trigger('click');
    $('.tiny_block_toggle').trigger('click');
}

function blockSelect(block){
    if(block =='1'){
        $('#female_block').css('display','none');
        $('#male_block').css('display','block');
    }else{
        $('#female_block').css('display','block');
        $('#male_block').css('display','none');
    }
}

function updateSeenstatus(visit_id) {
    var url = $('#base_url').val() + "/emr/updateSeen";
    var visit_id = $(visit_id).val();
    $.ajax({
        url: url,
        type: "GET",
        data: "visit_id=" + visit_id,
        beforeSend: function () {

        },
        success: function (data) {
            //alert(data);return;
        },
        complete: function () {

        }
    });
}

function SaveCasuality(save_type){
    var url = $('#base_url').val() + "/casuality/save-casuality";
    var token = $('#c_token').val();
    var visit_id = $('#visit_id').val();
    var patient_id = $('#patient_id').val();
    var encounter_id = $('#encounter_id').val();
    var present_complaints = $('#present_complaints').val();
    var doctor_id = $('#doctor_id').val();
    var nursing_notes_date = $('#nursing_notes_date').val();
    var nursing_notes = $('#nursing_notes').val();
    var coma_scale_date = $('#coma_scale_date').val();
    var coma_scale_time1 = $('#coma_scale_time1').val();
    var coma_scale_time2 = $('#coma_scale_time2').val();
    var coma_scale_time3 = $('#coma_scale_time3').val();
    var eyes_open1 = $("input[name='eyes_open1']:checked").val();
    var eyes_open2 = $("input[name='eyes_open2']:checked").val();
    var eyes_open3 = $("input[name='eyes_open3']:checked").val();
    var best_motor_response1 = $("input[name='best_motor_response1']:checked").val();
    var best_motor_response2 = $("input[name='best_motor_response2']:checked").val();
    var best_motor_response3 = $("input[name='best_motor_response3']:checked").val();
    var best_verbel_response1 = $("input[name='best_verbel_response1']:checked").val();
    var best_verbel_response2 = $("input[name='best_verbel_response2']:checked").val();
    var best_verbel_response3 = $("input[name='best_verbel_response3']:checked").val();
    var pupils_right_size_reaction11 = $('#pupils_right_size_reaction11').val();
    var pupils_right_size_reaction12 = $('#pupils_right_size_reaction12').val();
    var pupils_right_size_reaction13 = $('#pupils_right_size_reaction13').val();
    var pupils_right_size_reaction21 = $('#pupils_right_size_reaction21').val();
    var pupils_right_size_reaction22 = $('#pupils_right_size_reaction22').val();
    var pupils_right_size_reaction23 = $('#pupils_right_size_reaction23').val();
    var pupils_left_size_reaction11 = $('#pupils_left_size_reaction11').val();
    var pupils_left_size_reaction12 = $('#pupils_left_size_reaction12').val();
    var pupils_left_size_reaction13 = $('#pupils_left_size_reaction13').val();
    var pupils_left_size_reaction21 = $('#pupils_left_size_reaction21').val();
    var pupils_left_size_reaction22 = $('#pupils_left_size_reaction22').val();
    var pupils_left_size_reaction23 = $('#pupils_left_size_reaction23').val();

    eyes_open1 = eyes_open1 == undefined ? '' : eyes_open1;
    eyes_open2 = eyes_open2 == undefined ? '' : eyes_open2;
    eyes_open3 = eyes_open3 == undefined ? '' : eyes_open3;

    best_motor_response1 = best_motor_response1 == undefined ? 0 : best_motor_response1;
    best_motor_response2 = best_motor_response2 == undefined ? 0 : best_motor_response2;
    best_motor_response3 = best_motor_response3 == undefined ? 0 : best_motor_response3;

    best_verbel_response1 = best_verbel_response1 == undefined ? 0 : best_verbel_response1;
    best_verbel_response2 = best_verbel_response2 == undefined ? 0 : best_verbel_response2;
    best_verbel_response3 = best_verbel_response3 == undefined ? 0 : best_verbel_response3;

    if(patient_id == '' || patient_id == undefined){
        toastr.warning("Patient data not found!");
        return false;
    }
    if(visit_id == '' || visit_id == undefined){
        toastr.warning("Patient data not found!");
        return false;
    }
    if(encounter_id == '' || encounter_id == undefined){
        toastr.warning("Patient data not found!");
        return false;
    }

    let medicine_name = $('#medicine-listing-table').find('input[name="selected_item_name[]"]').val();
    if (medicine_name != '' && medicine_name != undefined) {
        saveDoctorPrescriptions(1);
    }

    var params = {
        _token:token,
        type:save_type,
        visit_id:visit_id,
        patient_id:patient_id,
        encounter_id:encounter_id,
        doctor_id:doctor_id,
        present_complaints:present_complaints,
        nursing_notes_date:nursing_notes_date,
        nursing_notes:nursing_notes,
        coma_scale_date:coma_scale_date,
        coma_scale_time1:coma_scale_time1,
        coma_scale_time2:coma_scale_time2,
        coma_scale_time3:coma_scale_time3,
        eyes_open1:eyes_open1,
        eyes_open2:eyes_open2,
        eyes_open3:eyes_open3,
        best_motor_response1:best_motor_response1,
        best_motor_response2:best_motor_response2,
        best_motor_response3:best_motor_response3,
        best_verbel_response1:best_verbel_response1,
        best_verbel_response2:best_verbel_response2,
        best_verbel_response3:best_verbel_response3,
        pupils_right_size_reaction11:pupils_right_size_reaction11,
        pupils_right_size_reaction12:pupils_right_size_reaction12,
        pupils_right_size_reaction13:pupils_right_size_reaction13,
        pupils_right_size_reaction21:pupils_right_size_reaction21,
        pupils_right_size_reaction22:pupils_right_size_reaction22,
        pupils_right_size_reaction23:pupils_right_size_reaction23,
        pupils_left_size_reaction11:pupils_left_size_reaction11,
        pupils_left_size_reaction12:pupils_left_size_reaction12,
        pupils_left_size_reaction13:pupils_left_size_reaction13,
        pupils_left_size_reaction21:pupils_left_size_reaction21,
        pupils_left_size_reaction22:pupils_left_size_reaction22,
        pupils_left_size_reaction23:pupils_left_size_reaction23,
    };


    $.ajax({
        url:url,
        type:"POST",
        data:params,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {



            if(data == 1){
                toastr.success("Saved Successfully!!!");
            }else if(data == 0){
                toastr.error("Error Please Check Your Internet Connection");
            }else{
                setTimeout(function () {
                    print_data(data);
                },1000);
            }
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
        }
    });

}

function print_data(htmlData){
    //event.stopPropagation();

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    mywindow.document.write('<style>@page{size:portrait;margin:30;margin-left:35px;text-align:center;}@media print {p{margin-top:20px; color:black; }}</style>');

    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:14px !important} .titleclass{text-align:center;padding-top:5px;} .padding-top{padding-top:10px !important;}</style>');
    mywindow.document.write(htmlData);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


/*********** COMBINED VIEW *******************/
function combinedView() {
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_type = $('input[name="visittype"]:checked').val();
    let sort = $('input[name="sortresult"]:checked').val();

    if (patient_id != '') {
        $('#combined_view_modal').modal('show');
        var url = $('#base_url').val() + "/emr/load-combined-view";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_type=' + visit_type + '&sort=' + sort,
            beforeSend: function () {
                $('#combined-view-loader').removeClass('hide');
            },
            success: function (data) {
                if (data.status = 1) {
                    $('.combined_view_wrapper').html(data.html);

                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                } else {
                    Command: toastr["error"]("Error.");
                }
            },
            complete: function () {
                $('#combined-view-loader').addClass('hide');
                $('#tblQPVE').css('display','none');
                $('#print_visit').css('display','none');
                $('h3').css('font-size','14px');
            }
        });
    }
}

$(document).on('click', '.combined_view_date_list ul li a', function () {
    var disset = $(this).attr("id");
    $('.combined_view_date_list ul li a').removeClass("active");
    $('.table_header_bg_grey').removeClass("tr_highlight");
    $(this).addClass("active");
    $(this).closest('.combined_view_wrapper').find("tr." + disset).addClass('tr_highlight');
});

$('input[name="visittype"],input[name="sortresult"]').on('change', function () {
    combinedView();
});


$(document).on('click', '.bg-orange', function (event) {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
});

function specialNotes() {
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    //  let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/special-notes";
    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id + '&visit_id=' + visit_id,
        beforeSend: function () {
            // $(".btn_quest").prop('disabled', true);
        },
        success: function (res) {
            if (res == 0) {
                alert('Please check your internet connection and try again');
            } else {
                $('#special_notes_result').html(res);
            }

        },
        error: function () {
            alert('Please check your internet connection and try again');
        },
        complete: function () {
            //  $(".btn_quest").prop('disabled', false);
        }
    });
}

function referDoctor() {
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/doctor-referral";
    $.ajax({
        url: url,
        async: false,
        type: "POST",
        data: "req_type=show&patient_id=" + patient_id + "&visit_id=" + visit_id + "&_token=" + _token,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            $('#referDoctor .modal-body').html(response.html);
        },
        complete: function () {
            $('#referDoctor').modal('show');
            $("body").LoadingOverlay("hide");
        }
    });
}
function saveReferDoctor() {

    if ($('#doctor_ref_loaded').length > 0) {

        if (patient_id != "" && visit_id != "") {
            $('#refer_save_btn').prop('disabled', true);

            let patient_id = $('#patient_id').val();
            let visit_id = $('#visit_id').val();
            let encounter_id = $('#encounter_id').val();
            let _token = $('#c_token').val();
            let data_params = $('#referDoctorForm').serialize();
            let payment_status = $("input[name='payment_status']:checked").val();
            let reference_type = $('#reference_type').val();

            data_params += '&patient_id=' + patient_id;
            data_params += '&visit_id=' + visit_id;
            data_params += '&encounter_id=' + encounter_id;
            data_params += '&payment_status=' + payment_status;
            data_params += '&reference_type=' + reference_type;
            data_params += '&req_type=' + 'save';
            data_params += '&_token=' + _token;


            var url = $('#base_url').val() + "/emr/doctor-referral";
            $.ajax({
                url: url,
                async: false,
                type: "POST",
                data: data_params,
                beforeSend: function () {
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                },
                success: function (data) {
                    if (parseInt(data) > 0) {
                        //when data set to 1 saved successfully
                        //when data set to 2 saved partialy some already reffered [unit wise refferal]
                        //when data set to 3 already reffered [single doctor refferal]

                        if (parseInt(data) == 1) {
                            Command: toastr["success"]('Doctor Referred Successfully');
                        } else if (parseInt(data) == 2) {
                            Command: toastr["success"]('Some Doctors Already Referred');
                        } else if (parseInt(data) == 3) {
                            Command: toastr["warning"]('Already Referred');
                        }

                        $('#ref_speciality').val('');
                        $('#reference_type').val('');
                        $('#doctor_ref').val('');
                        $('textarea[name="refer_notes"]').val('');
                    } else {
                        Command: toastr["error"]('Insertion Failed.');
                    }
                },
                error: function () {
                    Command: toastr["warning"]('Error.!');
                },
                complete: function () {
                    $('#refer_save_btn').prop('disabled', false);
                    $("body").LoadingOverlay("hide");
                }
            });

        } else {
            Command: toastr["warning"]('Please fill all fields.');
        }

    }

}


function LabResultsTrend() {

    var patient_id = $('#patient_id').val();
    var encounter_id = $('#encounter_id').val();
    if (patient_id != '') {
        var url = $('#base_url').val() + "/nursing/labResultTrends";
        $.ajax({
            url: url,
            type: "GET",
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id,
            beforeSend: function () {

            },
            success: function (data) {
                $('#lab_restuls_data').html(data);
            },
            complete: function () {
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            }
        });
    }
}

function getLabSubtestResult(id) {
    var url = $('#base_url').val() + "/nursing/getLabSubtestResult";
    $.ajax({
        url: url,
        type: "GET",
        data: "result_id=" + id,
        beforeSend: function () {

        },
        success: function (data) {
            $('#modal_lab_sub_results').modal('toggle');
            $('#lab_sub_restuls_data').html(data);
        },
        complete: function () {

        }
    });
}

function getCurrentVisitDetails(){
    var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
    if(!selected_dr_id) {
        selected_dr_id = $('#admitting_doctor_id').val();
    }

    var visit_id = $('#visit_id').val();
    var patient_id = $('#patient_id').val();
    var url = $('#base_url').val() + "/emr/getCurrentVisitDetails";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            doctor_id:selected_dr_id,
            visit_id:visit_id,
            patient_id:patient_id,
        },
        beforeSend:function (){

        },
        success: function(data) {
            if(data){
                if(parseInt(data.lab_status) > 0 ){
                    $(".labResultBtn").addClass('blink-btn');
                }
                if(parseInt(data.discharge_summary_status) > 0 ){
                    $(".showDischargeSummary").addClass('blink-btn');
                }
                if(parseInt(data.private_note_status) > 0 ){
                    $("#btn_private_notes").addClass('blink-btn');
                }
            }
        },
        error: function(){

        }
    });

}

$(document).on("click", '#btn_private_notes', function () {
    $(".private_notes_modal").modal('show');
})

function showDischargeSummary() {

    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/emr/showDischargeSummaryList";
    $.ajax({
        url: url,
        type: "GET",
        data: "patient_id=" + patient_id + "&visit_id=" + visit_id,
        beforeSend: function () {
            $('#summary_list_modal').modal('show');
            $('#summary_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_list_data').LoadingOverlay("hide");
            $('#summary_list_data').html(data);
            $(".global_search_dis_container").hide();
        },
        complete: function () {

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
}


$('#next_review_date').datetimepicker({
    format: 'MMM-DD-YYYY',
    minDate: new Date(),
}).on('dp.change', function (e) {
    takeNextAppointment();
});


function takeNextAppointment() {
    var next_review_date = $('#next_review_date').val();
    var url = $('#base_url').val() + "/emr/selectDrAppointments";
    var visit_id = $("#current_visit_id").val();

    if (next_review_date) {
        $.ajax({
            type: "GET",
            url: url,
            data: 'visit_id=' + visit_id + '&next_review_date=' + next_review_date,
            beforeSend: function () {
                $('#appointment_modal').modal('show');
                $('#next_appointment_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#next_appointment_list_data').html(data);
                $('#next_appointment_list_data').LoadingOverlay("hide");
            },
            complete: function () {
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });


            }
        });
    }
}

function editDischargeSummary(visit_id) {
    window.location = $("#base_url").val() + '/summary/dischargesummary/' + visit_id;
}
