$(document).ready(function () {
    $('.date-picker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    $('.select2').select2();
});


function getBillRevenewData()
{
    var url = route_json.revenew_report_data;
    var payment_date =0;
    var bill_date =0;
    var not_paid =0;
    var bill_cancel_status =0;
    if ($('#payment_date').prop('checked') == true) {
        payment_date = 1;
    }
    if ($('#bill_date').prop('checked') == true) {
        bill_date = 1;
    } 
    if ($('#not_paid').prop('checked') == true) {
        not_paid = 1;
    }
    if ($('#bill_cancel_status').prop('checked') == true) {
        bill_cancel_status = 1;
    }
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var from_date_paid = $('#from_date_paid').val();
    var to_date_paid = $('#to_date_paid').val();
    var bill_tag = $('#bill_tag').val();    
    var company = $('#company').val();    
    var pricing_scheme = $('#pricing_scheme').val();    
    
    var parm = { from_date: from_date, to_date: to_date, from_date_paid: from_date_paid, to_date_paid: to_date_paid, bill_tag: bill_tag, company: company, pricing_scheme: pricing_scheme, payment_date: payment_date, bill_date: bill_date, not_paid: not_paid, bill_cancel_status: bill_cancel_status};
    $.ajax({
        type: "POST",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
          
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");   
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
           
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });
}

function resetFilter() {
    var current_date = $('#current_date').val();
    $('#revenew_from').val(current_date);
    $('#revenew_to').val(current_date);
    $('#visit_type').val('');
    getrevenewData();
}

