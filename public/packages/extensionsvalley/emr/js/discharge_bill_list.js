$(document).ready(function() {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $('#bill_no').focus();
    var bill_type = $('#bill_type').val();

    // if (bill_type == 'PH') {
    //     getDischargeBillList();
    // }
    getDischargeBillList();

});
$(document).on("click", function(event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
$(document).on('click', '.advanceSearchBtn', function(event) {
    advancePatientSearch(1);
});
//----Hidden Filed Search--------------------------------------------------------------------
var token = $("#c_token").val();
var base_url = $("#base_url").val();
$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("").hide();
            $('#' + input_id + '_hidden').val('');
        } else {
            var url = base_url + "/discharge/setAjaxSearch";
            var bill_no = $('#bill_no').val();
            var bill_type = $('#bill_type').val();
            var param = { bill_no: bill_no, search_key: search_key, search_key_id: input_id, bill_type: bill_type };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getDischargeBillList() {
    var paid_bill = $('#paid_bill').is(":checked");
    var Unpaid_bill = $('#Unpaid_bill').is(":checked");
    var cancelled_bill = $('#cancelled_bill').is(":checked");
    var draft_bills = $('#draft_bills').is(":checked");
    var weekly_bill = $('#weekly_bill').is(":checked");
    var credit_discharge = $('#credit_discharge').is(":checked");
    var discharge_bill = $('#discharge_bill').is(":checked");
    var exclude_cancelled_bills = $('#exclude_cancelled_bills').is(":checked");
    var death_patient = $('#death_patient').is(":checked");
    var cash_card = $('#cash_card').is(":checked");
    var credit = $('#credit').is(":checked");
    var insurance = $('#insurance').is(":checked");
    var company_credit = $('#company_credit').is(":checked");


    var bill_no = $('#bill_no').val();
    var payment_type = $('#payment_type').val();
    var prepared_by = $('#prepared_by').val();
    var patient = $('#patient_name_hidden').val();
    var uhid = $('#patient_uhid').val();
    var ip_no = $('#ip_no').val();
    var admitting_dr = $('#admitting_dr').val();
    var consulting_dr = $('#consulting_dr').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    // console.log(bill_type);
    var params = {
        cash_card: cash_card,
        credit: credit,
        insurance: insurance,
        company_credit: company_credit,
        death_patient: death_patient,
        exclude_cancelled_bills: exclude_cancelled_bills,
        consulting_dr: consulting_dr,
        admitting_dr: admitting_dr,
        draft_bills: draft_bills,
        weekly_bill: weekly_bill,
        credit_discharge: credit_discharge,
        cancelled_bill: cancelled_bill,
        Unpaid_bill: Unpaid_bill,
        paid_bill: paid_bill,
        discharge_bill: discharge_bill,
        bill_no: bill_no,
        payment_type: payment_type,
        prepared_by: prepared_by,
        patient: patient,
        uhid: uhid,
        ip_no: ip_no,
        from_date: from_date,
        to_date: to_date,
    };
    var url = base_url + "/discharge/getDischargeBillList";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function() {
            $('#Discharge_bill_data').html(' ');
            $('#Discharge_bill_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#DischargebilllistBtn').attr('disabled', true);
            $("#Dischargebilllistspin").removeClass("fa fa-search");
            $("#Dischargebilllistspin").addClass("fa fa-spinner fa-spin");

        },
        success: function(data) {
            if (data) {
                $('#Discharge_bill_data').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }

        },
        complete: function() {
            $('#DischargebilllistBtn').attr('disabled', false);
            $('#Discharge_bill_data').LoadingOverlay("hide");
            $("#Dischargebilllistspin").removeClass("fa fa-spinner fa-spin");
            $("#Dischargebilllistspin").addClass("fa fa-search");

        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}
$('#bill_noAjaxDiv').on('click', 'li', function() {
    var bill_type = $('#bill_type').val();
    if (bill_type == 'PH') {
        getDetailsFromBillNum();
    }
});

function getDetailsFromBillNum() {
    var url = base_url + "/discharge/getDetailsFromBillNum";
    var bill_no = $('#bill_no').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { bill_no: bill_no },
        beforeSend: function() {
            $('#filter_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function(data) {
            if (data) {
                console.log(data);
                $('#payment_type').val(data[0].payment_type).select2();
                $('#prepared_by').val(data[0].created_by).select2();
                $('#location').val(data[0].location).select2();
                $('#company').val(data[0].company_id).select2();
                $('#patient').val(data[0].patient_name);
                $('#patient_hidden').val(data[0].patient_id);
                $('#uhid').val(data[0].uhid);
                $('#ip_no').val(data[0].ip_no);
                $('#visit_type').val(data[0].visit_status);
            }

        },
        complete: function() {
            $('#filter_area').LoadingOverlay("hide")

        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });


}

function reset() {
    $('.reset').val('');
    $('#prepared_by').val('').select2();
    $('#admitting_dr').val('').select2();
    $('#consulting_dr').val('').select2();
    $('.checkit').prop('checked', false);
    $('#death_patient').prop('checked', false);
    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    getDischargeBillList();
}

function displayBillDetails(id, bill_tag, package_id) {
    var head_id = $('#head_id_hidden').val();
    if (head_id != id) {
        $('#popup' + head_id).hide();
    }
    $('#head_id_hidden').val(id);
    $('#bill_tag_hidden').val(bill_tag);
    $('#package_id_hidden').val(package_id);
    if ($('#popup' + id).is(":visible")) {

        $('#popup' + id).hide();
    } else if ($('#popup' + id).is(":hidden")) {
        $('#popup' + id).show();
    }

    $('#ppclose' + id).click(function() {
        $('#popup' + id).hide();
    });


}

function displayCancelBillStatus(head_id) {
    $('#cancelBillSetup').modal('show');
    $('#head_id_hidden').val(head_id);
    var url = base_url + "/discharge/displayCancelBillStatus";
    $.ajax({
        type: "POST",
        url: url,
        data: { head_id: head_id },
        beforeSend: function() {
            $('#cancelBillSetupBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#cancel_bill').val(' ');
            $('#cancel_patient').val(' ');
            $('#cancel_paytype').val(' ');
            $('#cancel_by').val(' ');
            $('#Cancel_reason').val(' ');

        },
        success: function(data) {
            // console.log(data);
            if (data) {
                $('#cancel_bill').val(data[0].bill_no);
                $('#cancel_patient').val(data[0].patient_name);
                $('#cancel_paytype').val(data[0].payment_type);
                var user_name = $('#user_name').val();
                $('#cancel_by').val(user_name);

            }

        },
        complete: function() {
            $('#cancelBillSetupBody').LoadingOverlay("hide")

        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function cancelBill() {
    var Cancel_reason = $('#Cancel_reason').val();
    var head_id = $('#head_id_hidden').val();
    if (head_id != 0) {
        var url = base_url + "/discharge/cancelBill";
        $.ajax({
            type: "POST",
            url: url,
            data: { head_id: head_id, Cancel_reason: Cancel_reason },
            beforeSend: function() {
                $('#Cancel').attr('disabled', true);
                $('#Cancel_spin').removeClass('fa fa-trash');
                $('#Cancel_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function(data) {
                if (data) {
                    toastr.success('Bill cancelled successfully.');
                    $('#cancel' + head_id).hide();
                    $('#cancelBillSetup').modal('hide');
                    $('#paid_bg' + head_id).removeClass();
                    $('#paid_bg' + head_id).addClass('cancelled-bg');
                    $('#popup' + head_id).hide();

                }


            },
            complete: function() {
                $('#Cancel').attr('disabled', false);
                $('#Cancel_spin').removeClass('fa fa-spinner fa-spin');
                $('#Cancel_spin').addClass('fa fa-trash');
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            },
        });
    }


}
$('.checkit').click(function() {
    var name = $(this).attr('name');

    if ($(this).hasClass('checked')) {
        $(this).prop('checked', false);
        $(this).removeClass('checked');
        getDischargeBillList();
    } else {
        $('input[name="' + name + '"]').removeClass('checked');
        $(this).addClass('checked');
        getDischargeBillList();
    }
});

function printData() {
    $('#print_config_modal').modal('toggle');
}

function printBillDetails(bill_id = 0) {

    if (bill_id == 0) {
        bill_id = $("#head_id_hidden").val();
    }
    var include_hospital_header = $("#showTitle").prop('checked') ? 1 : 0;
    var is_duplicate = $("#duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var printMode = $('input[name=printMode]:checked').val();

    var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var url = $('#base_url').val() + "/Discharge/printBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
            location_code: location_code,
            include_hospital_header: include_hospital_header,
            is_duplicate: is_duplicate,
            printMode: printMode
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if (printMode == 1) {
                var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data.print_data + '<script>setTimeout(function(){window.print();window.close()},1000)</script>');
            } else {
                var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write('<style>@page{size:A4 landscape;} .patient_head{ font-size:10px !important; } </style>' + data.print_data + '<script>setTimeout(function(){window.print();window.close()},1000)</script>');
            }
            // window.location.reload();
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}

$(document).on('click', '#print_serv_bill', function(event) {
    $('#serv_print_config_modal').modal('toggle');
});

function PrintBill() {

    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var printBillId = $('#head_id_hidden').val();
    var bill_tag = $('#bill_tag_hidden').val();
    var package = $('#package_id_hidden').val() ? $('#package_id_hidden').val() : 0;
    var is_duplicate = $("#is_duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var include_hospital_header = $("#titleShow").prop('checked') ? 1 : 0;
    var param = { is_duplicate: is_duplicate, include_hospital_header: include_hospital_header, package: package, printBillId: printBillId, location_id: location_id, location_name: location_name, bill_tag: bill_tag };
    var base_url = $('#base_url').val();
    if (!printBillId && !bill_tag) {
        toastr.warning('Please select bill....');
        return;
    }
    var url = base_url + "/service_bill/print_bill_detail";
    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function() {

            $("#searchlist_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function(response) {
            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write('<style>@page{size:A5 landscape;} .patient_head{ font-size:10px !important; } </style>' + response + '<script>setTimeout(function(){window.print();},1000)</script>');

        },
        complete: function() {
            $('#print_status').val('');
            $('#print_bill_id').val(' ');
            $('#search_bill_tag').val(' ').select2();
            $('#package_id').val(' ');
            $("#searchlist_div").LoadingOverlay("hide");
        },

    });



}

function cashReceive(bill_id){
    bill_id_array = [];
    bill_id_array.push(bill_id);
    loadCashCollection(bill_id_array,1);

}

function checkSameUhid(uhid, id) {
    $(".check_bill").each(function() {
        if ($(this).is(":checked")) {
            var checked_uhid = $(this).attr('data-uhid');
            if (uhid != checked_uhid) {
                $('.checked_bill' + id).prop('checked', false);
                toastr.warning('Please select same bill of patient')
                return;

            }
        }
    });
}
$('#payment_type').change(function() {
    var type = $("#payment_type").val()
    if (type == 'cash/Card') {
        var set_class = 'round_blue';
    } else if (type == 'ipcredit') {
        var set_class = 'lime';
    } else if (type == 'insurance') {
        var set_class = 'pink';
    } else if (type == 'credit') {
        var set_class = 'round_yellow';
    }
    $('#add_class').removeClass()
    $('#add_class').addClass(set_class);
})

function paymentCompleted() {

};
