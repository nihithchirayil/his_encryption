$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        if ($(e.target).parent().hasClass('active') == false) {
            searchBill($(this).attr('href').split('page=')[1]);           
        }
    });    
    searchBill();

    $('.hidden_search').keyup(function (event) {
        var input_id = '';
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        input_id = $(this).attr('id');
        var url = '';
        if (value.match(keycheck) || event.keyCode == '8' || event.keyCode == 46) {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            if (search_key == "") {
                $('#' + input_id + "AjaxDiv").html("");
                $('.ajaxSearchBox').hide();
            } else {
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                    beforeSend: function () {
                        $('#' + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 0) {
                            $('#' + input_id + "AjaxDiv").html("No results found!").show();
                            $('#' + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                            return "<span>No result found!!</span>";
                        } else {
                            $('#' + input_id + "AjaxDiv").html(html).show();
                            $('#' + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                        }
                    },
                    complete: function () {

                    },
                    error: function () {
                        Command: toastr["error"]("Network Error!");
                        return;
                    },
                });
            }
        } else {
            ajaxProgressiveKeyUpDown(input_id + "AjaxDiv", event);
        }
    });
});

function fillSearchDetials(id, name, serach_key_id) {

    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

function checkallvisit() {
    var status = $('#checkallvisit').is(":checked");
    if (status) {
        $('.visit_class').prop('checked', true);
    } else {
        $('.visit_class').prop('checked', false);
    }
}

function searchBill(page =1) {
    var base_url = $('#base_url').val();
    var patient_id = $('#patient_name_hidden').val();
    var bill_id = $('#bill_number_hidden').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();

    var url = base_url + "/purchase/getPatientPaymentMode";
    var param = { patient_id: patient_id, bill_id: bill_id, from_date: from_date, to_date: to_date,page :page };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (msg) {
            $('#patient_paidBillList').html(msg);
        },
        complete: function () {
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
            $('#searchdataspin').addClass('fa fa-search');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('#companydetailsdiv').show();
            $('#result_data_table').attr('table_page_count', page);
        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
function getBillDetails(obj, bill_id, bill_tag) {
    var base_url = $('#base_url').val();
    var param = { bill_id: bill_id, bill_tag: bill_tag };
    var url = base_url + "/purchase/getPatientBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $(obj).attr('disabled', true);
            $(obj).find('i').removeClass('fa fa-search');
            $(obj).find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#modal_cashReciveEditBody").html(data);
            $("#modal_cashReciveEdit").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $(obj).attr('disabled', false);
            $(obj).find('i').removeClass('fa fa-spinner fa-spin');
            $(obj).find('i').addClass('fa fa-search');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

var i = 0;
var collected_amount = 0;
var amount_total = 0;
var balance = 0;

function addNewPaymentMode() {

    var payment_mode = $('#payment_mode').val();
    var payment_mode_name = $('#payment_mode option:selected').text();
    var machine_bank = $('#machine_bank').val();
    var machine_bank_name = $('#machine_bank option:selected').text();
    var bank = $('#bank').val();
    var bank_name = $('#bank option:selected').text();
    var amount = $('#amount').val();
    var card_no = $('#card_no').val();
    var phone_no = $('#phone_no').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();
    var amount_to_be_paid = parseFloat($('#amount_to_be_paid').val());
    var html = '';
    var html = '';
    var append_flag = 0;
    var current_balance_amount = $('#balance_amount').val();
    if (current_balance_amount == '') {
        current_balance_amount = 0;
    }
    current_balance_amount = parseFloat(current_balance_amount);

    if (amount == 0 || amount == '') {
        return false;
    } else {
        amount = parseFloat(amount);
    }
    if (current_balance_amount == 0) {
        current_balance_amount = amount_to_be_paid;
    }

    if (parseFloat(amount) > parseFloat(current_balance_amount)) {
        Command: toastr["warning"]('Amount cannot be greater than current balance amount');
        return false;
    }

    if (payment_mode == '') {
        Command: toastr["warning"]('Please select payment mode!');
        return false;
    }

    var PaymentModeMandatory = checkPaymentModeMandatory(payment_mode);
    if (PaymentModeMandatory == false) {
        return false;
    }

    html += '<tr class="payment_mode_row" id="payment_mode_row_' + i + '">';
    html += '<td class="payment_mode">' + payment_mode_name + '</td>';
    html += '<td class="amount">' + amount + '</td>';
    html += '<td class="machine_bank_name">' + machine_bank_name + '</td>';
    html += '<td class="bank_name">' + bank_name + '</td>';
    html += '<td class="card_no">' + card_no + '</td>';
    html += '<td class="exp_date">' + exp_year + '/' + exp_month + '</td>';
    html += '<td class="phone_no">' + phone_no + '</td>';
    html += '<td><button type="button" onclick="deleteCurrentRow(this);" class="btn btn-danger deleteCurrentRow"><i class="fa fa-trash"></i></button></td>';
    html += '</tr>';

    if ($('#payment_mode_table tr').length > 1) {
        $("#payment_mode_table tbody tr").each(function () {
            var mode = $(this).find('td.payment_mode');
            if (mode.html() == payment_mode_name) {
                append_flag = 1;
                var current_amount = $(this).find('td.amount').html();
                var new_amount = parseFloat(current_amount) + parseFloat(amount);
                $(this).find('td.amount').html(new_amount);
            }
        });

        if (append_flag == 0) {
            $('#payment_mode_table tbody').append(html);
        }
    } else {
        $('#payment_mode_table tbody').append(html);
        append_flag = 1;
    }

    collected_amount_old = parseFloat($('#collected_amount').val());
    collected_amount = collected_amount_old + amount;
    $('#collected_amount').val(collected_amount);
    if ($('#patient_payback').val() == 0) {
        $('#collected_amount_hidden').val(collected_amount);
    }

    balance_amount = amount_to_be_paid - amount;
    amount_total = amount_total + amount;
    balance = current_balance_amount - amount;
    checkBalance();

}

function changePaymentMode(mode) {
    $('#payment_mode').val(mode);
    $('#machine_bank').val('');
    $('#bank').val('');
}

function deleteCurrentRow(obj) {
    $(obj).closest('tr').remove();
    var current_amount = $(obj).closest('tr').find('td.amount').html();
    current_amount = parseFloat(current_amount);
    var amount = $('#amount').val();
    amount = parseFloat(amount);
    $('#amount').val(parseFloat(amount) + parseFloat(current_amount));

    var collected_amount = $('#collected_amount').val();
    collected_amount = parseFloat(collected_amount);
    if(collected_amount!=0){
        $('#collected_amount').val(parseFloat(collected_amount) - parseFloat(current_amount));
        $('#collected_amount_hidden').val(parseFloat(collected_amount) - parseFloat(current_amount));
    }

    var balance_amount = $('#balance_amount').val();
    balance_amount = parseFloat($('#amount_to_pay').val())- parseFloat($('#collected_amount').val());
    $('#balance_amount').val(balance_amount);

    amount_total = amount_total - current_amount;
    balance = balance_amount;

    var current_payment_mode = $(obj).closest('tr').find('td.payment_mode').html();
    if (current_payment_mode == 'ADVANCE') {
        var advance_balance = $('#advance_balance').val();
        $('#advance_balance').val(parseFloat(advance_balance) + current_amount);
    }
    checkBalance();
}

function checkBalance() {
    var collected_amount_it = $('#collected_amount_hidden').val().trim();
    var amount_to_pay = $('#amount_to_pay').val();
    if (parseFloat(collected_amount_it || collected_amount_it == 0)) {
        var org_collected_amount = $('#collected_amount').val();
        var balance = parseFloat(amount_to_pay - collected_amount_it);
        var org_balance = parseFloat(amount_to_pay - org_collected_amount);
        if (Math.sign(balance) === -1) {
            if ((Math.sign(org_balance) != -1)) {
                $('#amount').val(org_balance);
                $('#balance_amount').val(org_balance);
            } else {
                $('#amount').val(0);
                $('#balance_amount').val(0);
            }
            var payback = Math.abs(balance);
            $('#patient_payback').val(payback);
        } else {
            $('#amount').val(balance);
            $('#balance_amount').val(balance);
            $('#patient_payback').val(0);
        }
    } else {
        $('#balance_amount').val(amount_to_pay);
        $('#amount').val(amount_to_pay);
        $('#patient_payback').val(0);
        $('.deleteCurrentRow').click();
    }

}

function checkPaymentModeMandatory(payment_mode) {
    var payment_mode_detail = $('#payment_mode_detail').val();
    var card_no = $('#card_no').val();
    var bank = $('#bank').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();

    payment_mode_detail = atob(payment_mode_detail);
    payment_mode_detail = JSON.parse(payment_mode_detail);
    if (payment_mode_detail[payment_mode].is_cardno_mandatory != false) {
        if (card_no == '') {
            Command: toastr["warning"]('Please enter card number!');
            return false;
        }
    }
    if (payment_mode_detail[payment_mode].is_bank_mandatory != false) {
        if (bank == '') {
            Command: toastr["warning"]('Please select bank!');
            return false;
        }
    }
    if (payment_mode_detail[payment_mode].is_expirydate_mandatory != false) {
        if (exp_month == '' || exp_year == '') {
            Command: toastr["warning"]('Please select expiry date!');
            return false;
        }
    }

}
$(document).on('keyup', '.collection_amt', function () {
    checkBalance();
})

function save_bill_payment() {
    addNewPaymentMode();
    var payment_mode_array = [];
    var amount_array = [];
    var machine_bank_array = [];
    var bank_array = [];
    var card_no_array = [];
    var exp_date_array = [];
    var phone_no_array = [];
    var i = 0;
    var token = $('#c_token').val();
    var is_discharge = $('#is_discharge').val();

    $("#payment_mode_table tbody tr").each(function () {
        var payment_mode = $(this).find('td.payment_mode').html();
        var amount       = $(this).find('td.amount').html();
        var machine_bank = $(this).find('td.machine_bank_name').html();
        var bank         = $(this).find('td.bank_name').html();
        var card_no      = $(this).find('td.card_no').html();
        var exp_date     = $(this).find('td.exp_date').html();
        var phone_no     = $(this).find('td.phone_no').html();

        payment_mode_array[i] = payment_mode != '' ? payment_mode : '';
        amount_array[i]       = amount != '' ? amount : 0;
        machine_bank_array[i] = machine_bank != '' ? machine_bank : '';
        bank_array[i]     = bank != '' ? bank : '';
        card_no_array[i]  = card_no != '' ? card_no : '';
        exp_date_array[i] = exp_date != '' ? exp_date : '';
        phone_no_array[i] = phone_no != '' ? phone_no : '';
        i++;
    });

    var collected_amount = parseFloat($('#collected_amount').val());
    var amount_to_pay = parseFloat($('#amount_to_pay').val());

    if (collected_amount > amount_to_pay) {
        Command: toastr["warning"]('Collected amount is greater than amount to be paid!');
        return false;
    }
    else if (collected_amount < amount_to_pay) {
        Command: toastr["warning"]('Collected amount is less than amount to be paid!');
        return false;
    }
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';

    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/saveCashReceiveEdit";
    var bill_amount = $('#total_bill_amount').val();
    var bill_head_id = $('#bill_head_id').val(); 
    var net_amount = $('#total_net_amount').val();
    var uhid = $('#uhid').val();
    var bill_no = $('#bill_no').val();
    var bill_tag = $('#bill_type').val();
    var dataparams = {
        '_token': token,
        'payment_mode_array': payment_mode_array,
        'amount_array': amount_array,
        'machine_bank_array': machine_bank_array,
        'bank_array': bank_array,
        'card_no_array': card_no_array,
        'exp_date_array': exp_date_array,
        'phone_no_array': phone_no_array,
        'received_amount': $('#received_amount').val(),
        'return_amount': $('#return_amount').val(),
        'collected_amount': $('#collected_amount').val(),
        'balance_amount': $('#balance_amount').val(),
        'amount_total': $('#amount_total').val(),
        'balance': $('#balance').val(),
        'bill_head_id': bill_head_id,
        'bill_amount': bill_amount,
        'net_amount': net_amount,
        'already_paid_amount': $('#already_paid_amount').val(),
        'amount_to_be_paid': $('#amount_to_be_paid').val(),
        'advance_balance': $('#advance_balance').val(),
        'location_id': location_id,
        'uhid': uhid,
        'is_discharge': is_discharge,
        'bill_tag':bill_tag,
        'bill_no':bill_no,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#modal_cashReciveEditBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        }, 
        success: function (html) {
            if (html != 0) {
                Command: toastr["success"]('Bill Received Successfully!');
                $('#modal_cashReciveEdit').modal('hide');
                var page = $('#result_data_table').attr('table_page_count');
                searchBill(page); 
            }
        },
        complete: function () {
            $('#modal_cashReciveEditBody').LoadingOverlay("hide");            
        },
    });

}

function checkContentLength(id, length) {
    var value = $('#' + id).val();
    if (value.length > length) {
        value = value.substring(0, length);
        $('#' + id).val(value);
    }
    if (id == 'exp_month') {
        if (value > 12 || value < 1) {
            $('#' + id).val('');
        }
    }
    if (id == 'exp_year') {
        if (value > 2099) {
            $('#' + id).val('');
        }
    }
}