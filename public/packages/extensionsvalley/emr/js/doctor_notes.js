var doctor_baseurl = $('#base_url').val();

$(document).ready(function () {
    tinyMceCreate('patientDoctorNotes');
});


function tinyMceCreate(text_area) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: 500,
        imagetools_cors_hosts: ['picsum.photos'],
        browser_spellcheck: false,
        autosave_ask_before_unload: false,
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: false,
        contextmenu: false,
        menubar: false,
        importcss_append: false,
        height: 500,
        image_caption: false,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
    });
}


function getDoctorNotes(patient_id, doctor_id, from_type = 0) {
    var url = doctor_baseurl + "/doctor_notes/getDoctorNotes";
    var visit_id = $("#visit_id").val();
    var param = {
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            // if (parseInt(from_type) == 1) {
                $('.getDoctorNotesModelBtn').find('i').addClass('fa-spinner fa-spin').removeClass('fa-sticky-note-o');

            // } else {
            //     $('#get_ip_op_proceduresbtn' + patient_id).attr('disabled', true);
            //     $('#get_ip_op_proceduresbtn' + patient_id).removeClass('fa fa-th-list');
            //     $('#get_ip_op_proceduresbtn' + patient_id).addClass('fa fa-spinner fa-spin');
            // }

        },
        success: function (data) {
            $("#doctornotesmodel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#doctornotesmodelHeader').html(data.headerhtml);
            $('#doctornotesmodelDiv').html(data.nursing_data);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
                var admission_date = $('#iopopprocedure_admission_date').val();
                var discharge_date = $('#iopopprocedure_discharge_date').val();
                $('#ipopservice_date').datetimepicker({
                    format: 'MMM-DD-YYYY',
                    minDate: admission_date,
                    maxDate: discharge_date
                });
                tinymce.activeEditor.focus();
            }, 1000);
        },
        complete: function () {
            // if (parseInt(from_type) == 1) {
                $('.getDoctorNotesModelBtn').find('i').addClass('fa-sticky-note-o').removeClass('fa-spinner fa-spin'); 

            // } else {
            //     $('#get_ip_op_proceduresbtn' + patient_id).attr('disabled', false);
            //     $('#get_ip_op_proceduresbtn' + patient_id).removeClass('fa fa-spinner fa-spin');
            //     $('#get_ip_op_proceduresbtn' + patient_id).addClass('fa fa-th-list');
            // }
            $('#doctornotes_scroll').animate({
                scrollTop: $(document).height()
            }, "slow");
            resetDoctorNotes();
        }
    });
}

function resetDoctorNotes() {
    tinymce.get('patientDoctorNotes').setContent('');
    $('#doctornotes_id').val(0);
}

function editDoctorNotes(notes_id) {
    var url = doctor_baseurl + "/doctor_notes/editDoctorNotes";
    $('#doctornotes_id').val(notes_id);
    var param = {
        notes_id: notes_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#editDoctorNotesBtn' + notes_id).attr('disabled', true);
            $('#editDoctorNotesSpin' + notes_id).removeClass('fa fa-edit');
            $('#editDoctorNotesSpin' + notes_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            tinymce.get('patientDoctorNotes').setContent(data);
        },
        complete: function () {
            $('#editDoctorNotesBtn' + notes_id).attr('disabled', false);
            $('#editDoctorNotesSpin' + notes_id).removeClass('fa fa-spinner fa-spin');
            $('#editDoctorNotesSpin' + notes_id).addClass('fa fa-edit');
        }
    });
}

function saveIpOpDoctorNotes() {
    var url = doctor_baseurl + "/doctor_notes/saveIpOpDoctorNotes";
    var patientid = $('#nursingnotes_patientid').val();
    var doctorid = $('#nursingnotes_doctorid').val()
    var visitid = $('#nursingnotes_visitid').val();
    var notes_id = $('#doctornotes_id').val();
    var doctor_notes = tinymce.get('patientDoctorNotes').getContent();
    doctor_notes_string = JSON.stringify(doctor_notes);
    var param = {
        patientid: patientid,
        doctorid: doctorid,
        visitid: visitid,
        notes_id: notes_id,
        doctor_notes_string: doctor_notes_string
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#saveIpOpDoctorNotesBtn').attr('disabled', true);
            $('#saveIpOpDoctorNotesSpin').removeClass('fa fa-save');
            $('#saveIpOpDoctorNotesSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data.status = 1) {
                toastr.success(data.message);
                resetDoctorNotes();
                $('#doctornotesmodelDiv').html(data.doctor_data);
                $(".theadscroll").perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30,
                });


            } else {
                toastr.warning(data.message);
            }
        },
        complete: function () {
            $('#saveIpOpDoctorNotesBtn').attr('disabled', false);
            $('#saveIpOpDoctorNotesSpin').removeClass('fa fa-spinner fa-spin');
            $('#saveIpOpDoctorNotesSpin').addClass('fa fa-save');
            $('#doctornotes_scroll').animate({
                scrollTop: $(document).height()
            }, "slow");
        }
    });
}
function deleteDoctorNotes(notes_id) { 
    var url = doctor_baseurl + "/doctor_notes/deleteDoctorNotes";
    var visitid = $('#nursingnotes_visitid').val();
    var param = {
        notes_id: notes_id,
        visitid: visitid,
    }; 
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#deletDoctorNotes' + notes_id).attr('disabled', true);
            $('#deleteDoctorNotesSpin' + notes_id).removeClass('fa fa-trash');
            $('#deleteDoctorNotesSpin' + notes_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data.status == 1) {
                $('.doctor_noteDiv' + notes_id).remove();
                $('#doctornotesmodelDiv').html(data.doctor_data);

            }
        },
        complete: function () {
            resetDoctorNotes();
            $('#deleteDoctorNotes' + notes_id).attr('disabled', false);
            $('#deleteDoctorNotesSpin' + notes_id).removeClass('fa fa-spinner fa-spin');
            $('#deleteDoctorNotesSpin' + notes_id).addClass('fa fa-trash');
        }
    });
}