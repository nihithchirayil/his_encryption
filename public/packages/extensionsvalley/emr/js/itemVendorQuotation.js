$(document).ready(function () {
    base_url = $('#base_url').val();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30

    });
    setTimeout(function () {
        $('.theadfix_wrapper').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
    }, 400);
    $(".select2").select2();
    firstBtnClick();
    tinyMceCreate('buyer_notes', 380, 0);
    tinyMceCreate('orthervendor_remarks', 400, 0);
});

base_url = '';
vendor_array = {};

function firstBtnClick() {
    vendor_array = {};
    var unit_price = 0;
    $(".item_unitprice_amount").each(function (index) {
        unit_price = this.value;
        if (unit_price != '0') {
            return false;
        }
    });
    if (unit_price == 0) {
        $('.hideitem_details').hide();
        $('.theadscroll').perfectScrollbar("update");
    }
    // item_charges
    var encoded_quoted_data = $('#quoted_head_details').val();
    var quoted_data = atob(encoded_quoted_data);
    quotes = JSON.parse(quoted_data);
    if (quotes.length != 0) {
        vendor_array = quotes;
    }

    setTimeout(function () {
        if ($('.row_class').attr('id')) {
            var btn = $('.row_class').attr('id');
            $('#' + btn).trigger('click');
        }
        $('.loadbtn').attr('disabled', false);
    }, 1500);

}

function tinyMceCreate(text_area, tinymce_height, read_only) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        imagetools_cors_hosts: ['picsum.photos'],
        browser_spellcheck: false,
        autosave_ask_before_unload: false,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: false,
        contextmenu: false,
        toolbar: false,
        menubar: false,
        importcss_append: false,
        height: 400,
        image_caption: false,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
        readonly: read_only,
    });
}


function getItemsMappedVendors(row_id, item_id, detail_id) {
    var item_desc = $('#item_desc_' + detail_id).val();
    var rfq_id = $('#rfq_id').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getItemsMappedVendors";

    var param = { _token: token, item_desc: btoa(item_desc), rfq_id: rfq_id, item_id: item_id, detail_id: detail_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#model_data_detail_div').hide();
            $('#model_charges_div').html('Please Wait!!!');
            $("#model_charges_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#model_data_detail_div').hide();
            $('#model_charges_div').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#model_charges_div").LoadingOverlay("hide");
            $('.addItemData').removeClass('selected_rowclass');
            $('.item_selection_' + row_id).addClass('selected_rowclass');
            if (vendor_array[detail_id]) {
                var from_type = vendor_array[detail_id]['from_type'];
                if (from_type == '1')
                    $('.setLowestVendor' + detail_id).prop('checked', true);
            }
        }, error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function compare_vendors() {
    comparevendor_array = {};
    var i = 0;
    $('.compare_vendor').each(function (index) {
        var status = $(this).is(":checked");
        if (status) {
            comparevendor_array[parseInt($(this).val())] = parseInt($(this).attr('data-detail-id'));
            i++;
        }
    });
    if (i == 0) {
        toastr.warning("Please Select Any Vendor");
    } else if (i >= 5) {
        toastr.warning("You can only 4 Vendors maximum");
    } else {
        var detail_id = $('#selected_detail_id').val();
        var item_desc = $('#item_desc_' + detail_id).val();
        var compare_data_string = JSON.stringify(comparevendor_array);
        var rfq_id = $('#rfq_id').val();
        var token = $('#token_hiddendata').val();
        var url = base_url + "/quotation/compareVendors";

        var param = { _token: token, detail_id: detail_id, compare_data_string: compare_data_string, rfq_id: rfq_id };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#compareVendorQuotationHeader').html('Compare Vendors (' + item_desc + ')');
                $('#compareVendorBtn').attr('disabled', true);
                $('#compareVendorSpin').removeClass('fa fa-anchor');
                $('#compareVendorSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                $('#compareVendorQuotationDiv').html(data);
                $("#compareVendorQuotationModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
            },
            complete: function () {
                $('#compareVendorBtn').attr('disabled', false);
                $('#compareVendorSpin').removeClass('fa fa-spinner fa-spin');
                $('#compareVendorSpin').addClass('fa fa-anchor');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    }
}

function vendorDataPush(quotation_detailid, detail_id, vendor_id, from_type, status) {
    var remarks = '';
    var item_id = $('#selected_item').val();
    var quotation_headid = $('#selected_vendor_head_id' + quotation_detailid).val();
    var qty = $('#request_qty' + detail_id).val();
    var vendor_price = $('#selectvendor_price' + quotation_detailid).html();
    vendor_price = parseFloat(vendor_price).toFixed(2);
    if (from_type == '2') {
        remarks = $('#orthervendor_remarks').val();
    }
    if (status) {
        vendor_array[detail_id] = {
            quotation_headid: quotation_headid,
            quotation_detailid: quotation_detailid,
            item_id: item_id,
            vendor_id: vendor_id,
            vendor_price: parseFloat(vendor_price),
            from_type: from_type,
            qty: parseFloat(qty),
            remarks: btoa(remarks)
        }
    } else {
        vendor_array[detail_id] = {};
    }
}


function getVendorQuotedDetalis(list_id, detail_id, vendor_id) {
    var vendor_name = $('#selectvendor_name' + vendor_id).html();
    var rfq_id = $('#rfq_id').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getVendorQuotedDetalis";

    var param = { _token: token, rfq_id: rfq_id, list_id: list_id, detail_id: detail_id, vendor_id: vendor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getVendorQuotedDetalisBtn' + list_id).attr('disabled', true);
            $('#getVendorQuotedDetalisSpin' + list_id).removeClass('fa fa-th-list');
            $('#getVendorQuotedDetalisSpin' + list_id).addClass('fa fa-spinner fa-spin');
            $('#model_charges_detail_header').html(vendor_name.trim());
            $('#model_data_detail_div').show();
            $("#model_charges_detail_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#model_charges_detail_div').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            $('#getVendorQuotedDetalisBtn' + list_id).attr('disabled', false);
            $("#model_charges_detail_div").LoadingOverlay("hide");
            $('.vendorItemData').removeClass('selected_rowclass');
            $('.vendor_selection_' + vendor_id).addClass('selected_rowclass');
            $('#getVendorQuotedDetalisSpin' + list_id).removeClass('fa fa-spinner fa-spin');
            $('#getVendorQuotedDetalisSpin' + list_id).addClass('fa fa-th-list');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function loadBuyerAllNotes(vendor_id) {
    var rfq_id = $('#rfq_id').val();
    var token = $('#token_hiddendata').val();
    var detail_id = $('#selected_detail_id').val();
    var url = base_url + "/quotation/getBuyerQuotedNotes";

    var param = { _token: token, rfq_id: rfq_id, detail_id: detail_id, vendor_id: vendor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
        },
        success: function (data) {
            $("#getBuyerQuotedNotesDiv").html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            getUserNotes(vendor_id);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function clearBuyerNewNotes(from_type) {
    $('#buyer_negotiate_price').val(0);
    tinymce.get('buyer_notes').setContent('');
    if (from_type == '1') {
        $('#notesdata_itemname').html('');
        $('#notesdata_vendornotes').html('');
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30

        });
    }
}

function getVendorItemQuotedNotes(list_id) {
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getVendorItemQuotedNotes";

    var param = { _token: token, list_id: list_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#notesdata_itemname').html(obj.item_desc);
            $('#notesdata_vendornotes').html(obj.item_remarks);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function FinalizeVendorItem() {
    var negotiate_price = $('#finalize_negotiate_price').val();
    if (parseFloat(negotiate_price) > 0) {
        bootbox.confirm({
            message: "Are you sure you want to make this item finalize to this vendor?",
            buttons: {
                'confirm': {
                    label: "Finalize",
                    className: 'btn-success',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var vendor_id = $('#selected_vendor_id').val();
                    var rfq_id = $('#rfq_id').val();
                    var detail_id = $('#selected_detail_id').val();
                    var item_id = $('#selected_item').val();
                    var buyernotesID = $('#buyernotesID').val();
                    var token = $('#token_hiddendata').val();
                    var url = base_url + "/quotation/addeditBuyerNotes";
                    var tinymce_notes = tinymce.get('orthervendor_remarks').getContent();
                    var buyer_data = JSON.stringify(tinymce_notes);
                    buyer_data = encodeURIComponent(buyer_data);
                    var param = { _token: token, buyer_data: buyer_data, item_id: item_id, from_type: 1, vendor_id: vendor_id, rfq_id: rfq_id, detail_id: detail_id, negotiate_price: negotiate_price, buyernotesID: buyernotesID };
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: param,
                        beforeSend: function () {
                            $('#FinalizeNotesBtn').attr('disabled', true);
                            $('#FinalizeNotesSpin').removeClass('fa fa-flag-checkered');
                            $('#FinalizeNotesSpin').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            if (data) {
                                $('#buyernotesID').val(data);
                                toastr.success("Successfully Updated");
                                $("#vendorQuotationmodel").modal('toggle');
                            }
                        },
                        complete: function () {
                            $('#FinalizeNotesBtn').attr('disabled', false);
                            $('#FinalizeNotesSpin').removeClass('fa fa-spinner fa-spin');
                            $('#FinalizeNotesSpin').addClass('fa fa-flag-checkered');
                            $('#vendordata_finalizebtn' + vendor_id).removeClass();
                            $('#vendordata_finalizebtn' + vendor_id).addClass('btn btn-success btn-block');
                            var qty = $('#request_qty' + detail_id).val();
                            var vendor_name = $('#selectvendor_name' + vendor_id).html();
                            $('#vendor_name' + detail_id).val(vendor_name.trim());

                            var net_amt = parseFloat(negotiate_price) * parseFloat(qty);
                            $('#item_unitprice_amount' + detail_id).val(negotiate_price);
                            $('#item_net_amount' + detail_id).val(Math.round(net_amt * 100) / 100);
                            $('#vendor_idhidden' + detail_id).val(vendor_id);
                        },
                        error: function () {
                            toastr.error('Please check your internet connection and try again');
                        }
                    });
                }
            }
        });
    } else {
        toastr.warning("Negotiate Price Cannot Be Zero");
        $('#finalize_negotiate_price').focus();
    }

}

function getUserNotes(vendor_id) {
    var rfq_id = $('#rfq_id').val();
    var item_id = $('#selected_item').val();
    var detail_id = $('#selected_detail_id').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getUserNotes";

    var param = { _token: token, item_id: item_id, rfq_id: rfq_id, detail_id: detail_id, vendor_id: vendor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var note_id = obj.note_id;
            var finalize_negotiation = obj.finalize_negotiation;
            var finialize_status = obj.finialize_status;
            if (note_id != 0) {
                editBuyerNotes(vendor_id, note_id, 2);
            } else {
                $('#buyernotesID').val(0);
                $('#buyer_negotiate_price').val(0);
                tinymce.get('buyer_notes').setContent('');
                $('#buyer_notes_label').html('Add Buyer Notes');
            }
            if (finialize_status == '1') {
                $('#finalizebtndiv').show();
            } else {
                $('#finalizebtndiv').hide();
            }

            if (finalize_negotiation == '0') {
                $('#notes_save_btn').show();
            } else {
                $('#notes_save_btn').hide();
            }
        },
        complete: function () {
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function getVendorQuotedNotes(list_id, vendor_id, detail_id) {
    var vendor_name = $('#selectvendor_name' + vendor_id).html();
    var rfq_id = $('#rfq_id').val();
    var item_id = $('#selected_item').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getBuyerQuotedNotes";
    var param = { _token: token, item_id: item_id, list_id: list_id, rfq_id: rfq_id, vendor_id: vendor_id, detail_id: detail_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            clearBuyerNewNotes(1);
            $('#getVendorNotesBtn' + list_id).attr('disabled', true);
            $('#getVendorNotesSpin' + list_id).removeClass('fa fa-sticky-note-o');
            $('#getVendorNotesSpin' + list_id).addClass('fa fa-spinner fa-spin');
            $('#VendorQuotedDetalisHeader').html(vendor_name.trim());
        },
        success: function (data) {
            $("#getBuyerQuotedNotesDiv").html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            $("#VendorQuotedDetalisModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#getVendorNotesBtn' + list_id).attr('disabled', false);
            $('#getVendorNotesSpin' + list_id).removeClass('fa fa-spinner fa-spin');
            $('#getVendorNotesSpin' + list_id).addClass('fa fa-sticky-note-o');
            $('#addbuyerNotesBtn').attr('onClick', 'addeditBuyerNotes(' + vendor_id + ',0' + ')');
            $('#finalizebuyerNotesBtn').attr('onClick', 'addeditBuyerNotes(' + vendor_id + ',1' + ')');
            getVendorItemQuotedNotes(list_id);
            getUserNotes(vendor_id);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}



function editBuyerNotes(vendor_id, note_id, from_type) {
    var token = $('#token_hiddendata').val();
    var vendor_name = $('#selectvendor_name' + vendor_id).html().trim();
    var rfq_id = $('#rfq_id').val();
    var detail_id = $('#selected_detail_id').val();
    var item_id = $('#selected_item').val();
    var url = base_url + "/quotation/editBuyerNotes";
    $('#buyer_notes_label').html('Edit Buyer Notes');
    $('#selected_vendor_id').val(vendor_id);
    var param = { _token: token, item_id: item_id, note_id: note_id, vendor_id: vendor_id, rfq_id: rfq_id, detail_id: detail_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            if (from_type == '1') {
                $('#vendorQuotationmodelHeader').html(vendor_name);
                $('#vendordata_finalizebtn' + vendor_id).attr('disabled', true);
                $('#vendordata_finalizespin' + vendor_id).removeClass('fa fa-flag-checkered');
                $('#vendordata_finalizespin' + vendor_id).addClass('fa fa-spinner fa-spin');
            } else if (from_type == '2') {
                $('#editBuyerNotesBtn' + note_id).attr('disabled', true);
                $('#editBuyerNotesSpin' + note_id).removeClass('fa fa-edit');
                $('#editBuyerNotesSpin' + note_id).addClass('fa fa-spinner fa-spin');
            }
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#buyernotesID').val(obj.notes_id);
            if (from_type == '1') {
                tinymce.get('orthervendor_remarks').setContent(obj.buyer_notes);
                var finalize_status = obj.finalize_status;
                if (finalize_status != 0) {
                    $('#FinalizeNotesBtn').hide();
                } else {
                    $('#FinalizeNotesBtn').show();
                }
                $('#finalize_negotiate_price').val(obj.negotiate_price);
                $("#vendorQuotationmodel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else if (from_type == '2') {
                tinymce.get('buyer_notes').setContent(obj.buyer_notes);
                $('#buyer_negotiate_price').val(obj.negotiate_price);
            }
        },
        complete: function () {
            if (from_type == '1') {
                $('#vendordata_finalizebtn' + vendor_id).attr('disabled', false);
                $('#vendordata_finalizespin' + vendor_id).removeClass('fa fa-spinner fa-spin');
                $('#vendordata_finalizespin' + vendor_id).addClass('fa fa-flag-checkered');
            } else if (from_type == '2') {
                $('#editBuyerNotesBtn' + note_id).attr('disabled', false);
                $('#editBuyerNotesSpin' + note_id).removeClass('fa fa-spinner fa-spin');
                $('#editBuyerNotesSpin' + note_id).addClass('fa fa-edit');
            }
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function addeditBuyerNotes(vendor_id, from_type) {
    var negotiate_price = $('#buyer_negotiate_price').val();
    if (from_type == 0 || negotiate_price != 0) {
        var rfq_id = $('#rfq_id').val();
        var detail_id = $('#selected_detail_id').val();
        var buyernotesID = $('#buyernotesID').val();
        var token = $('#token_hiddendata').val();
        var url = base_url + "/quotation/addeditBuyerNotes";
        var tinymce_notes = tinymce.get('buyer_notes').getContent();
        var buyer_data = JSON.stringify(tinymce_notes);
        buyer_data = encodeURIComponent(buyer_data);
        var param = { _token: token, buyer_data: buyer_data, from_type: from_type, rfq_id: rfq_id, detail_id: detail_id, negotiate_price: negotiate_price, buyernotesID: buyernotesID, vendor_id: vendor_id };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#addbuyerNotesBtn').attr('disabled', true);
                $('#addbuyerNotesSpin').removeClass('fa fa-plus');
                $('#addbuyerNotesSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (data) {
                    toastr.success("Successfully Added");
                    loadBuyerAllNotes(vendor_id);
                }
            },
            complete: function () {
                $('#addbuyerNotesBtn').attr('disabled', false);
                $('#addbuyerNotesSpin').removeClass('fa fa-spinner fa-spin');
                $('#addbuyerNotesSpin').addClass('fa fa-plus');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning("Please Enter Negotiate Price");
        $('#buyer_negotiate_price').focus();
    }
}


function updateNegotiatePrice(row_id, detail_id) {
    var negotiate_price = $('#negotiate_price' + row_id).val();
    var net_amt = 0.0;
    var qty = $('#request_qty' + detail_id).val();
    if (!negotiate_price || parseFloat(negotiate_price) == 0) {
        $('#negotiate_price' + row_id).val(0);
        negotiate_price = 0;
        var item_price = vendor_array[detail_id]['vendor_price'];
        net_amt = parseFloat(qty) * parseFloat(item_price);
    } else {
        net_amt = parseFloat(qty) * parseFloat(negotiate_price);
    }

    $('#item_net_amount' + detail_id).val(net_amt);
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/updateNegotiatePrice";
    var param = { _token: token, negotiate_price: negotiate_price, detail_id: detail_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#submit_itemvendorbtn').attr('disabled', true);
            $('#submit_itemvendorspin').removeClass('fa fa-th-list');
            $('#submit_itemvendorspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
        },
        complete: function () {
            $('#submit_itemvendorbtn').attr('disabled', false);
            $('#submit_itemvendorspin').removeClass('fa fa-spinner fa-spin');
            $('#submit_itemvendorspin').addClass('fa fa-th-list');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function finialzeQuotation(rfq_id) {
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getQuotationPoList";
    var param = { _token: token, rfq_id: rfq_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#finialzeQuotationBtn').attr('disabled', true);
            $('#finialzeQuotationSpin').removeClass('fa fa-flag-checkered');
            $('#finialzeQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#submitVendorPoModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#submitVendorPoDiv').html(data);
        },
        complete: function () {
            $('#finialzeQuotationBtn').attr('disabled', false);
            $('#finialzeQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#finialzeQuotationSpin').addClass('fa fa-flag-checkered');
            setTimeout(function () {
                if ($('.VendorRowDatatr').attr('id')) {
                    var btn = $('.VendorRowDatatr').attr('id');
                    $('#' + btn).trigger('click');
                }
            }, 1500);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function getQuotationGeneratePurchaseOrder() {
    var rfq_id = $('#rfq_id').val();
    var vendor_id = $('#vendorIDPolist').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getQuotationGeneratePurchaseOrder";
    var vendor_name = $('#VendorRowVndorName' + vendor_id).html();
    var param = { _token: token, rfq_id: rfq_id, vendor_id: vendor_id };
    bootbox.confirm({
        message: "Are you sure you want to generate PO for vendor" + vendor_name.trim() + " ?",
        buttons: {
            'confirm': {
                label: "Generate",
                className: 'btn-success',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#quotationGeneratePurchaseOrderBtn').attr('disabled', true);
                        $('#quotationGeneratePurchaseOrderSpin').removeClass('fa fa-list');
                        $('#quotationGeneratePurchaseOrderSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data) {
                            var po_id = data + '-0';
                            addWindowLoad('poReq', po_id, 2);
                        }
                    },
                    complete: function () {
                        $('#quotationGeneratePurchaseOrderBtn').attr('disabled', false);
                        $('#quotationGeneratePurchaseOrderSpin').removeClass('fa fa-spinner fa-spin');
                        $('#quotationGeneratePurchaseOrderSpin').addClass('fa fa-list');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        }
    });

}


function deleteBuyerNotes(buyerID, vendor_id) {
    bootbox.confirm({
        message: "Are you sure you want to delete this note ?",
        buttons: {
            'confirm': {
                label: "Delete",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var token = $('#token_hiddendata').val();
                var url = base_url + "/quotation/deleteBuyerNotes";
                var param = { _token: token, buyerID: buyerID };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#deleteBuyerNotesBtn' + buyerID).attr('disabled', true);
                        $('#deleteBuyerNotesSpin' + buyerID).removeClass('fa fa-trash');
                        $('#deleteBuyerNotesSpin' + buyerID).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data) {
                            $('#buyernotes_list' + buyerID).remove();
                            toastr.success('Successfully Deleted');
                            getUserNotes(vendor_id);
                        }
                    },
                    complete: function () {
                        $('#deleteBuyerNotesBtn' + buyerID).attr('disabled', false);
                        $('#deleteBuyerNotesSpin' + buyerID).removeClass('fa fa-spinner fa-spin');
                        $('#deleteBuyerNotesSpin' + buyerID).addClass('fa fa-trash');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        }
    });
}


function closeQuotation(rfq_id) {
    bootbox.confirm({
        message: "Are you sure you want to close this RFQ ?",
        buttons: {
            'confirm': {
                label: "Close",
                className: 'btn-warning',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var token = $('#token_hiddendata').val();
                var url = base_url + "/quotation/closeRfq";
                var param = { _token: token, rfq_id: rfq_id };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#closeQuotationBtn').attr('disabled', true);
                        $('#closeQuotationSpin').removeClass('fa fa-check');
                        $('#closeQuotationSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data) {
                            $('#rfq_closedatadiv').hide();
                            $('.closing_date').html('RFQ Closed');
                        }
                    },
                    complete: function () {
                        $('#closeQuotationBtn').attr('disabled', false);
                        $('#closeQuotationSpin').removeClass('fa fa-spinner fa-spin');
                        $('#closeQuotationSpin').addClass('fa fa-check');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        }
    });
}


function getQuotationPoVendorItems(vendor_id) {
    $('#vendorIDPolist').val(vendor_id);
    var rfq_id = $('#rfq_id').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getQuotationPoVendorItems";
    var vendor_name = $('#VendorRowVndorName' + vendor_id).html();
    var param = { _token: token, rfq_id: rfq_id, vendor_id: vendor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#poselected_itemvendornameheader').html(vendor_name.trim());
            $("#vendorQuotationItemsDiv").html("Please Wait!!!");
            $("#vendorQuotationItemsDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("#vendorQuotationItemsDiv").html(data);
        },
        complete: function () {
            setTimeout(function () {
                var po_insert_status = $('#po_check_status_hidden').val();
                if (po_insert_status == '0') {
                    $('#quotationGeneratePurchaseOrderBtn').attr('disabled', false);
                } else {
                    $('#quotationGeneratePurchaseOrderBtn').attr('disabled', true);
                }
            }, 1000);
            $("#vendorQuotationItemsDiv").LoadingOverlay("hide");
            $('.VendorRowDatatr').removeClass('bg-green');
            $('#VendorRowDatatr' + vendor_id).addClass('bg-green');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function addWindowLoad(to_url, rfq_id, from_type) {
    if (from_type == '1') {
        bootbox.confirm({
            message: "Are you sure you want to reload ?",
            buttons: {
                'confirm': {
                    label: "Reload",
                    className: 'btn-warning',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if (result) {
                    var url = base_url + "/quotation/" + to_url + '/' + rfq_id;
                    document.location.href = url;
                }
            }
        });
    } else {
        setTimeout(function () {
            var url = base_url + "/quotation/" + to_url + '/' + rfq_id;
            document.location.href = url;
        }, 1000);

    }
}

