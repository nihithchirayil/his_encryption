
/*********** COMBINED VIEW *******************/
function combinedView() {
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_type = $('input[name="visittype"]:checked').val();
    let sort = $('input[name="sortresult"]:checked').val();

    if (patient_id != '') {
        $('#combined_view_modal').modal('show');
        var url = $('#base_url').val() + "/emr/load-combined-view";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_type=' + visit_type + '&sort=' + sort,
            beforeSend: function () {
                $('#combined-view-loader').removeClass('hide');
            },
            success: function (data) {
                if (data.status = 1) {
                    $('.combined_view_wrapper').html(data.html);

                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                } else {
                    Command: toastr["error"]("Error.");
                }
            },
            complete: function () {
                $('#combined-view-loader').addClass('hide');
                $('h3').css('font-size','14px');
            }
        });
    }
}


function specialNotes() {
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    //  let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/special-notes";
    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id + '&visit_id=' + visit_id,
        beforeSend: function () {
            // $(".btn_quest").prop('disabled', true);
        },
        success: function (res) {
            if (res == 0) {
                alert('Please check your internet connection and try again');
            } else {
                $('#special_notes_result').html(res);
            }

        },
        error: function () {
            alert('Please check your internet connection and try again');
        },
        complete: function () {
            //  $(".btn_quest").prop('disabled', false);
        }
    });
}

$('input[name="visittype"],input[name="sortresult"]').on('change', function () {
    combinedView();
});


$(document).on('click', '.bg-orange', function (event) {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
});

$(function () {
    $('a[href^="#"]').on('click', function (e) {


        if (this.hash !== '') {
            e.preventDefault();

            var hash = this.hash;
            //var nav = $( '.navbar' ).outerHeight();

            $('html, body').animate({
                scrollTop: $(hash).offset().top - 200
            }, 200);
        }
    });
});

//  $(document).on('click', '.smooth_scroll_class', function (event) {

//     $('.emr_sub').addClass('top_margin');

// });

$(window).scroll(function () {
    if ($(window).scrollTop() > 100) {
        $(".position_fixed_header").css('margin-top', '-5px');
        $(".position_monitor_header").css('margin-top', '-43px');
    }
    else {
        $(".position_fixed_header").css('margin-top', '34px');
        $(".position_monitor_header").css('margin-top', '-5px');

    }
});




// $(document).on('click', '.smooth_scroll', function (event) {
//     event.preventDefault();

//     $('.combined_theadscroll').animate({
//         scrollTop: $($.attr(this, 'href')).offset().top
//     }, 500);
// });

//$(document).on('click', '.smooth_scroll_class', function (event) {
//event.preventDefault();

// $('html,body').animate({
//     scrollTop: $($.attr(this, 'href')).offset().top
// }, 500);
//});

// var toScroll = 0;
//     $(document).on('click', '.smooth_scroll', function (e) {
//         e.preventDefault();
//         var divWindow = $('.combined_theadscroll').offset().top;
//         var clickedLink = $(e.target.hash).offset().top;
//         toScroll += clickedLink - divWindow;
//         $('.combined_theadscroll').animate({ scrollTop: toScroll }, 500);

//     });

// document.querySelector('.combined_theadscroll').scrollIntoView({
//   behavior: 'smooth'
// });

$(document).on('click', '.combined_view_date_list ul li a', function () {
    var disset = $(this).attr("id");
    $('.combined_view_date_list ul li a').removeClass("active");
    $('.table_header_bg_grey').removeClass("tr_highlight");
    $(this).addClass("active");
    $(this).closest('.combined_view_wrapper').find("tr." + disset).addClass('tr_highlight');
});

/*********** COMBINED VIEW *******************/
//++++++++Printing Visits++++++++++++++//

function btnPrintVisit() {
    $('#printingVisits').modal('show');
    var patient_id = $('#patient_id').val();
    var visit_type = $('input[name="visittype"]:checked').val();
    var sort = $('input[name="sortresult"]:checked').val();
    var url = $('#base_url').val() + "/emr/fetch_visit_data";
    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id + '&visit_type=' + visit_type + '&sort=' + sort,
        beforeSend: function () {
            $('#print_visit-view-loader').removeClass('hide');
        },
        success: function (data) {
            if (data.status = 1) {
                $('#fetch_visit_data_div_id').html(data.html);

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            } else {
                Command: toastr["error"]("Error.");
            }
        },
        error: function () {
            alert('Please check your internet connection and try again');
        },
        complete: function () {
            $('#print_visit-view-loader').addClass('hide');
        }
    });
}
function printDrVisits() {
    //print visit view
    var patient_id = $('#patient_id').val();
    if (patient_id != '' && patient_id != undefined) {
        var url = $('#base_url').val();
        var _token = $('#c_token').val();
        let visit_type = $('input[name="visittype"]:checked').val();
        let sort = $('input[name="sortresult"]:checked').val();
        var data_params = $('#printVisitForm').serialize();

        data_params += '&patient_id=' + patient_id;
        data_params += '&visit_type=' + visit_type;
        data_params += '&sort=' + sort;
        data_params += '&_token=' + _token;

        $.ajax({
            type: "POST",
            async: false,
            url: url + "/emr/print-doctorvisits-details",
            data: data_params,
            beforeSend: function () {
            },
            success: function (data) {
                var res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html);
                winPrint.document.close();
                winPrint.focus();
                winPrint.print();
            },
            complete: function () {

            }
        });
    }

}
$(document).on('click', '.all_visit_dates', function () {
    var chkd = $(this).is(":checked");
    if (chkd == true) {
        $(this).val('1');
        $(".visit_dates").prop('checked', true);
        $(".visit_chk_li").removeClass('hide');
        $(".visit_dates_chk").prop('checked', true);
    } else {
        $(this).val('0');
        $(".visit_dates").prop('checked', false);
        $(".visit_chk_li").addClass('hide');
        $(".visit_dates_chk").prop('checked', false);
    }
});
function visit_chk_status(obj) {
    var chk_value = $(obj).is(":checked");
    if (chk_value == true) {
        $(obj).closest("li").find(".visit_chk_li").removeClass('hide');
        $(obj).closest("li").find(".visit_dates_chk").prop('checked', true);
    } else {
        $(obj).closest("li").find(".visit_chk_li").addClass('hide');
        $(obj).closest("li").find(".visit_dates_chk").prop('checked', false);
    }
}
//++++++++Printing Visits++++++++++++++//
$('.modal').on('shown.bs.modal', function (e) {
    $(".theadfix_wrapper").floatThead('reflow');
});

$(".selectsearch").select2();

function saveDoctorEncounterDatas() {
    // window.atleastOneSavedRecords = false;
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let investigation_head_id = $('#investigation_head_id').val();

    // let search_medicine = $("input[name=search_medicine]").val();

    // if (search_medicine != '') {
    //     addNewMedicine();
    // }


    disableSaveBtn();

    if (encounter_id == "" || visit_id == "") {
        Command: toastr["error"]("Error.! Visit And Encounter Not Found.");
        return false;
    }

    //save as drafts when not saved
    //prescription
    if ($('#medicine-listing-table tbody > tr').find('input[name="selected_item_name[]"]:not([value=""])').length > 0) {
        var doc_presc_save = saveDoctorPrescriptions(1);
        if (doc_presc_save == "FALSE") {
            return;
        }
    }

    //iv prescription
    if ($('#consumable-medicine-listing-table tbody > tr').find('input[name="consumable_selected_item_code[]"]').length == 0) {
        //no medicine in the list
    } else {
        saveDoctorConsumablePrescriptions();
    }
    //assessment
    saveClinicalTemplate(1);


    // updating dr seen status
    updateSeenstatus(visit_id);


    //convert drafts
    if (patient_id != '') {
        var url = $('#base_url').val() + "/emr/save-doctors-encounter-datas";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&investigation_head_id=' + investigation_head_id,
            beforeSend: function () {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                if (data.status = 1) {
                    //chewindow.atleastOneSavedRecordsck if prescription print is enabled
                    let prescription_head_id = $('#prescription_head_id').val();

                    // if(prescription_head_id != '' && prescription_head_id != undefined && parseInt(prescription_head_id) > 0 && $('#print_prescription:checked').length > 0){
                    //     printPescriptionView(prescription_head_id);
                    // }

                    finishConsultation();

                    if (window.atleastOneSavedRecords == true) {
                        Command: toastr["success"]("Saved Successfully.");
                        window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
                        window.atleastOneSavedRecords = false;
                    }


                } else if (data.status = 2) {

                } else {
                    Command: toastr["error"]("Error.");
                }
            },
            complete: function () {
                loadInvestigationHistory();
                enableSaveBtn();
                $("body").LoadingOverlay("hide");
            }
        });
    }

}

function disableSaveBtn() {
    $('#save_btn_emr').prop('disabled', true);
    $('#save_complete_btn_emr').prop('disabled', true);
}

function enableSaveBtn() {
    $('#save_btn_emr').prop('disabled', false);
    $('#save_complete_btn_emr').prop('disabled', false);
}


function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}


var public_sort_var = 2;
function myFunction(search_type, InputId, tableId) {
    var searchType = $('input[name=' + search_type + ']:checked').val();
    var serch_method_conf = "anywhere";
    var serch_mode = $("input[name='serch_by_type']:checked").val();

    var input, filter, table, tr, td, i;
    input = document.getElementById(InputId);
    filter = input.value.toUpperCase();
    table = document.getElementById(tableId);
    tr = table.getElementsByTagName("tr");
    if (serch_mode == 1) {
        serch_method_conf = "anywhere";
    } else {
        serch_method_conf = "";
    }
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[searchType];

        if (td) {
            if (serch_method_conf == "anywhere") {
                if (td.innerHTML.toUpperCase().match(filter)) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } else {
                if (td.innerHTML.toUpperCase().startsWith(filter)) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
        $('.theadscroll').perfectScrollbar("update");
    }

    if (searchType == 5) {
        sortTable('myTable', 5, public_sort_var);
        if (public_sort_var == 1) {
            public_sort_var = 2;
        } else {
            public_sort_var = 1;
        }
    }

}

function applyFilter(classname) {
    if (classname == 'all') {
        $("#myTable tbody tr").show();
    } else {
        $("#myTable tbody tr").hide();
        $("#myTable tbody tr." + classname).show();
    }
}

$("#poplink").popover({
    html: true,
    placement: "right",
    trigger: "click",
    title: function () {
        return $(".pop-title").html();
    },
    content: function () {
        return $(".pop-content").html();
    }
});


function goToSelectedPatient(patient_id) {
    window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
}

$(document).on('DOMMouseScroll mousewheel', '.Scrollable', function (ev) {
    var $this = $(this),
        scrollTop = this.scrollTop,
        scrollHeight = this.scrollHeight,
        height = $this.innerHeight(),
        delta = (ev.type == 'DOMMouseScroll' ?
            ev.originalEvent.detail * -40 :
            ev.originalEvent.wheelDelta),
        up = delta > 0;

    var prevent = function () {
        ev.stopPropagation();
        ev.preventDefault();
        ev.returnValue = false;
        return false;
    }

    if (!up && -delta > scrollHeight - height - scrollTop) {
        // Scrolling down, but this will take us past the bottom.
        $this.scrollTop(scrollHeight);
        return prevent();
    } else if (up && delta > scrollTop) {
        // Scrolling up, but this will take us past the top.
        $this.scrollTop(0);
        return prevent();
    }
});

function fetch_all_presc_footer_fields(encounter_id) {
    if (encounter_id > 0) {
        var url = $('#base_url').val() + "/emr/fetch-all-presc-footer-fields";
        $.ajax({
            url: url,
            type: "GET",
            data: "encounter_id=" + encounter_id + "&prescription_footer_fields=1",
            beforeSend: function () {

            },
            success: function (response) {
                let data = response.data;
                let special_notes = data.special_notes;
                let diagnosis = data.diagnosis;
                let reason_for_visit = data.reason_for_visit;
                let next_review_date = data.next_review_date;

                if (next_review_date) {
                    $('#next_review_date').val(moment(next_review_date).format('MMM-DD-YYYY'));
                } else {
                    $('#next_review_date').val('');
                }
                $('#pres_notes').val(special_notes);
                $('#pres_history').val(diagnosis);
                $('#visit_reason').val(reason_for_visit);

            },
            complete: function () {

            }
        });
    }
}

// $(document).on('click', '#admission_request', function () {
//     let patient_id = $('#patient_id').val();
//     let visit_id = $('#visit_id').val();
//     let _token = $('#c_token').val();
//     var url = $('#base_url').val() + "/emr/admission-request";
//     $.ajax({
//         url: url,
//         async: false,
//         type: "POST",
//         data: "req_type=show&patient_id=" + patient_id + "&visit_id=" + visit_id + "&_token=" + _token,
//         beforeSend: function () {
//             $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
//         },
//         success: function (response) {
//             $('#admitModal .modal-body').html(response.html);
//         },
//         complete: function () {
//             $('#admitModal').modal('show');
//             $("body").LoadingOverlay("hide");
//         }
//     });
// });


function admitHere() {
    if ($('#adm_req_loaded').length > 0) {
        var encounter_id = $('#encounter_id').val();
        var visit_id = $('#visit_id').val();
        var patient_id = $('#patient_id').val();
        var priority = $('#priority').val();
        if (priority == "") {
            Command: toastr["warning"]('Error.! Select Priority');
            return false;
        }
        var dataparams = $('#formAdmission').serialize();

        if (patient_id != "") {
            $('#admit_btn').prop('disabled', true);
            let _token = $('#c_token').val();
            var url = $('#base_url').val() + "/emr/admission-request";
            $.ajax({
                type: "POST",
                async: false,
                url: url,
                data: 'req_type=save&encounter_id=' + encounter_id + '&visit_id=' + visit_id + '&patient_id=' + patient_id + '&priority=' + priority + '&dataparams=' + dataparams + "&_token=" + _token,
                beforeSend: function () {
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                },
                success: function (responsedata) {
                    if (responsedata.success == true) {
                        Command: toastr["success"](responsedata.message);
                        $('#admitModal :input').val('');
                        $('#admitModal').modal('hide');
                    } else {
                        Command: toastr["warning"](responsedata.message);
                    }

                },
                error: function () {
                    Command: toastr["warning"]('Error.!');
                },
                complete: function () {
                    $('#admit_btn').prop('disabled', false);
                    $("body").LoadingOverlay("hide");
                }
            });

        } else {
            Command: toastr["warning"]('Please fill all fields.');
        }

    }
}
function referDoctor() {
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/emr/doctor-referral";
    $.ajax({
        url: url,
        async: false,
        type: "POST",
        data: "req_type=show&patient_id=" + patient_id + "&visit_id=" + visit_id + "&_token=" + _token,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            $('#referDoctor .modal-body').html(response.html);
        },
        complete: function () {
            $('#referDoctor').modal('show');
            $("body").LoadingOverlay("hide");
        }
    });
}
function saveReferDoctor() {

    if ($('#doctor_ref_loaded').length > 0) {

        if (patient_id != "" && visit_id != "") {
            $('#refer_save_btn').prop('disabled', true);

            let patient_id = $('#patient_id').val();
            let visit_id = $('#visit_id').val();
            let encounter_id = $('#encounter_id').val();
            let _token = $('#c_token').val();
            let data_params = $('#referDoctorForm').serialize();
            let payment_status = $("input[name='payment_status']:checked").val();
            let reference_type = $('#reference_type').val();

            data_params += '&patient_id=' + patient_id;
            data_params += '&visit_id=' + visit_id;
            data_params += '&encounter_id=' + encounter_id;
            data_params += '&payment_status=' + payment_status;
            data_params += '&reference_type=' + reference_type;
            data_params += '&req_type=' + 'save';
            data_params += '&_token=' + _token;


            var url = $('#base_url').val() + "/emr/doctor-referral";
            $.ajax({
                url: url,
                async: false,
                type: "POST",
                data: data_params,
                beforeSend: function () {
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                },
                success: function (data) {
                    if (parseInt(data) > 0) {
                        //when data set to 1 saved successfully
                        //when data set to 2 saved partialy some already reffered [unit wise refferal]
                        //when data set to 3 already reffered [single doctor refferal]

                        if (parseInt(data) == 1) {
                            Command: toastr["success"]('Doctor Referred Successfully');
                        } else if (parseInt(data) == 2) {
                            Command: toastr["success"]('Some Doctors Already Referred');
                        } else if (parseInt(data) == 3) {
                            Command: toastr["warning"]('Already Referred');
                        }

                        $('#ref_speciality').val('');
                        $('#reference_type').val('');
                        $('#doctor_ref').val('');
                        $('textarea[name="refer_notes"]').val('');
                    } else {
                        Command: toastr["error"]('Insertion Failed.');
                    }
                },
                error: function () {
                    Command: toastr["warning"]('Error.!');
                },
                complete: function () {
                    $('#refer_save_btn').prop('disabled', false);
                    $("body").LoadingOverlay("hide");
                }
            });

        } else {
            Command: toastr["warning"]('Please fill all fields.');
        }

    }

}


function updateSeenstatus(visit_id) {
    var url = $('#base_url').val() + "/emr/updateSeen";
    $.ajax({
        url: url,
        type: "GET",
        data: "visit_id=" + visit_id,
        beforeSend: function () {

        },
        success: function (data) {
            //alert(data);return;
        },
        complete: function () {

        }
    });
}


function showDischargeSummary() {

    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/emr/showDischargeSummaryList";
    $.ajax({
        url: url,
        type: "GET",
        data: "patient_id=" + patient_id + "&visit_id=" + visit_id,
        beforeSend: function () {
            $('#summary_list_modal').modal('show');
            $('#summary_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_list_data').LoadingOverlay("hide");
            $('#summary_list_data').html(data);
            $(".global_search_dis_container").hide();
        },
        complete: function () {

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
};




function showSummary(summary_id) {
    var url = $('#base_url').val() + "/emr/showSummary";

    $.ajax({
        url: url,
        type: "GET",
        data: "summary_id=" + summary_id,
        beforeSend: function () {
            $('#summary_list_modal').modal('hide');
            $('#summary_view_modal').modal('show');
            $('#summary_view_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            $('#summary_view_data').html('<div class="" style="height:515px;overflow-y:scroll">' + data.summary + '</div>');

            if (data.final_status == 0) {
                $('#summary_view_data').append('<button style="margin-top: 20px;" title="Mark as Finalized" onclick="finalizeDischargeSummary(' + summary_id + ');" class="btn btn-success"><i class="fa fa-check"></i>Finalize Summary</button><button style="margin-top: 20px;" title="Edit Summary" onclick="editDischargeSummary(' + data.visit_id + ');" class="btn btn-success"><i class="fa fa-edit"></i> Edit Summary</button>');
            }
        },
        complete: function () {

        }
    });
}

function finalizeDischargeSummary(summary_id) {
    var url = $('#base_url').val() + "/emr/finalizeDischargeSummary";
    $.ajax({
        url: url,
        type: "GET",
        data: "summary_id=" + summary_id,
        beforeSend: function () {
            $('#summary_view_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            if ($('#summary_view_modal').hasClass('in')) {
                $('#summary_view_modal').modal('hide');
            } else {
                showDischargeSummary();
            }
            if (data.status == 1) {
                toastr.success("Discharge Summary Finalized..!");
            }

        },
        complete: function () {

        }
    });
}



function editDischargeSummary(visit_id) {
    window.location = $("#base_url").val() + '/summary/dischargesummary/' + visit_id;
}

function create_new_discharge_summary() {
    var visit_id = $('#visit_id').val();
    window.location = $('#base_url').val() + '/summary/dischargesummary/' + visit_id;
}


$('#next_review_date').datetimepicker({
    format: 'MMM-DD-YYYY',
    minDate: new Date(),
}).on('dp.change', function (e) {
    takeNextAppointment();
});


function takeNextAppointment() {
    var next_review_date = $('#next_review_date').val();
    var url = $('#base_url').val() + "/emr/selectDrAppointments";
    var visit_id = $("#current_visit_id").val();

    if (next_review_date) {
        $.ajax({
            type: "GET",
            url: url,
            data: 'visit_id=' + visit_id + '&next_review_date=' + next_review_date,
            beforeSend: function () {
                $('#appointment_modal').modal('show');
                $('#next_appointment_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#next_appointment_list_data').html(data);
                $('#next_appointment_list_data').LoadingOverlay("hide");
            },
            complete: function () {
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });


            }
        });
    }


}

function LabResultsTrend() {

    var patient_id = $('#patient_id').val();
    var encounter_id = $('#encounter_id').val();
    if (patient_id != '') {
        var url = $('#base_url').val() + "/nursing/labResultTrends";
        $.ajax({
            url: url,
            type: "GET",
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id,
            beforeSend: function () {

            },
            success: function (data) {
                $('#lab_restuls_data').html(data);
            },
            complete: function () {
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });

                $('.select2').select2();
            }
        });
    }
}

function getLabSubtestResult(id) {
    var url = $('#base_url').val() + "/nursing/getLabSubtestResult";
    $.ajax({
        url: url,
        type: "GET",
        data: "result_id=" + id,
        beforeSend: function () {

        },
        success: function (data) {
            $('#modal_lab_sub_results').modal('toggle');
            $('#lab_sub_restuls_data').html(data);
        },
        complete: function () {

        }
    });
}



$(document).on('click', '#admission_request', function () {
    let patient_id = $('#patient_id').val();
    //  patient_id = btoa(patient_id);
    let visit_id = $('#visit_id').val();
    // visit_id = btoa(visit_id);
    var url = $('#base_url').val() + "/emr/admitPatientRequestNew";
    window.location.href = $('#base_url').val() + "/emr/admitPatientRequestNew/" + patient_id + '/' + visit_id;


});

$(document).on('click', '.combined_view_date_list ul li', function () {
    setTimeout(function () {
        $("#combined_view_modal").scrollTop(0);
    }, 50)

});


function getIPPatientList(from_type) {
    var from_date = $('#iplist_admitted_fromdate').val();
    var to_date = $('#iplist_admitted_todate').val();
    if ((from_type == '1' && from_date && to_date) || from_type == '0') {

        var base_url = $('#base_url').val();
        var patient_id = $('#patient_id').val();
        var url = base_url + "/emr/getIpPatientList";

        var param = { patient_id: patient_id, from_type: from_type, from_date: from_date, to_date: to_date };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#patient_iplist_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $(".ip_slide_box").addClass('open');
                $('#patient_iplist_data').html(data);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });
            },
            complete: function () {
                $('#patient_iplist_data').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select From date To date");
    }


}

function iplistSorting(table_id, search_field) {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(search_field);
    filter = input.value.toUpperCase();
    table = document.getElementById(table_id);
    var radio_val = $("input[name='ip_patientlist_radio']:checked").val();
    var serch_mode = $("input[name='ip_serch_by_type']:checked").val();
    tr = table.getElementsByTagName("tr");

    if (serch_mode == 1) {
        serch_method_conf = "anywhere";
    } else {
        serch_method_conf = "";
    }

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[radio_val];
        if (td) {
            if (serch_method_conf == "anywhere") {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().match(filter)) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } else {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().startsWith(filter)) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

}

function getIPPatient(patient_id) {
    window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
    window.atleastOneSavedRecords = false;
}



//-----------promt to save assement data remainder-----------------------------

$('#assesment_draft_mode').val(0);
$(document).on('change', "#section1 input[type=text],#section1 input[type=checkbox],#section1 input[type=radio],#section1 input[type=date],#section1 textarea", function (event) {
    $('#assesment_draft_mode').val(1);
    // window.onbeforeunload = function(event){
    //     event.preventDefault();
    //     return true;
    // }
});

$(document).on('click', ".user-profile,.btn_right_side_slide,.side-menu>li>a", function (event) {
    var active_visit_status = $('#active_visit_status').val();
    if (active_visit_status == 1) {
        checkAssesmentSaveValidation();
    }
});

function checkAssesmentSaveValidation() {
    //alert('ddd');
    var assesment_draft_mode = $('#assesment_draft_mode').val();
    if (assesment_draft_mode == 1) {


        cuteAlert({
            type: "question",
            title: "Assesment Changes Save Reminder",
            message: "Your Assesment changes not saved would you like to save?",
            confirmText: "Save",
            cancelText: "Discard"
        }).then((e) => {
            if (e == "confirm") {
                window.save_encounter_status = "TRUE";
                saveClinicalTemplate(1);
                $('#assesment_draft_mode').val(0);
                // window.onbeforeunload = null;

            } else {
                $('#assesment_draft_mode').val(0);
                // window.onbeforeunload = null;

            }
        })

    }
}


//-----------promt to save investigation data remainder-----------------------------

$('#investigation_draft_mode').val(0);
$(document).on('change', "#section3 input[type=text],#section3 input[type=checkbox],#section3 input[type=radio],#section3 input[type=date],#section3 textarea", function () {

    $('#investigation_draft_mode').val(1);
    // window.onbeforeunload = function(event){
    //     event.preventDefault();
    //     return true;
    // }


});

$(document).on('click', ".user-profile,.btn_right_side_slide,.side-menu>li>a", function (event) {
    if (active_visit_status == 1) {
        checkInvestigationSaveValidation();
    }

});

function checkInvestigationSaveValidation() {
    var investigation_draft_mode = $('#investigation_draft_mode').val();
    if (investigation_draft_mode == 1) {

        cuteAlert({
            type: "question",
            title: "Investigation Save Reminder",
            message: "Your Investigation changes not saved would you like to save?",
            confirmText: "Save",
            cancelText: "Discard"
        }).then((e) => {
            if (e == "confirm") {
                saveInvestigation();
                $('#investigation_draft_mode').val(0);
                // window.onbeforeunload = null;

            } else {
                $('#investigation_draft_mode').val(0);
                // window.onbeforeunload = null;

            }
        })

    }

}

//-----------promt to save prescription data remainder-----------------------------

$('#prescription_changes_draft_mode').val(0);
$(document).on('change', "#drug input[type=text],#section2 input[type=checkbox],#section2 input[type=radio],#section2 input[type=date],#section2 textarea", function () {
    $('#prescription_changes_draft_mode').val(1);
    //  window.onbeforeunload = function(event){
    //     event.preventDefault();
    //     return true;
    // }

});

$(document).on('click', ".user-profile,.btn_right_side_slide,.side-menu>li>a", function (event) {
    if (active_visit_status == 1) {
        checkPrescriptionSaveValidation();
    }
});

function checkPrescriptionSaveValidation() {
    var prescription_changes_draft_mode = $('#prescription_changes_draft_mode').val();
    if (prescription_changes_draft_mode == 1) {


        cuteAlert({
            type: "question",
            title: "Prescription Save Reminder",
            message: "Your Prescription changes not saved would you like to save?",
            confirmText: "Save",
            cancelText: "Discard"
        }).then((e) => {
            if (e == "confirm") {
                window.save_encounter_status = "TRUE";
                saveDoctorPrescriptions(1);
                $('#prescription_changes_draft_mode').val(0);
                //    window.onbeforeunload = null;

            } else {
                $('#prescription_changes_draft_mode').val(0);
                //    window.onbeforeunload = null;

            }
        })

    }
}


function saveInvestigation() {
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let investigation_head_id = $('#investigation_head_id').val();
    var location_id = 0;

    if (encounter_id == "" || visit_id == "") {
        Command: toastr["error"]("Error.! Visit And Encounter Not Found.");
        return false;
    }

    if (patient_id != '') {
        var url = $('#base_url').val() + "/emr/updateInvestigationStatus";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&investigation_head_id=' + investigation_head_id + '&location_id=' + location_id + '&visit_id=' + visit_id,
            beforeSend: function () {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("body").LoadingOverlay("hide");
                if (data == 1) {
                    Command: toastr["success"]("Saved Successfully.");
                    // if ($("#print_investigation").is(':checked')) {
                    //      printInvestigationHistoryList(investigation_head_id);
                    // }
                }
                else {
                    Command: toastr["error"]("Error.");
                }

                $('#investigation_draft_mode').val(0);
            },
            complete: function () {
                // location.reload();
                $('#investigation_draft_mode').val(0);
                // window.onbeforeunload = null;
            }
        });
    }
}

$(document).on('click', 'body', function (event) {
    let dont_show_toastr = true;
    if ($(event.target).parents('#section1').length == 0 && $('#assesment_draft_mode').val() == 1) {
        if (active_visit_status == 1) {
            saveClinicalTemplate(1, dont_show_toastr);
        }
    }
})


$(window).load(function () {
    // var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;

    startConsultation();

    $(document).on("click", '#btn_private_notes', function () {
        $(".private_notes_modal").modal('show');
    })






})

function startConsultation() {
    var booking_id = localStorage.getItem('current_booking_id') ? parseInt(localStorage.getItem('current_booking_id')) : 0;
    let patient_id = $('#patient_id').val();
    if (isNaN(booking_id)) {
        booking_id = 0;
    }
    if (booking_id > 0) {
        var url = $('#base_doumenturl').val() + "/emr/startConsultation";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                current_booking_id: booking_id,
                patient_id: patient_id
            },
            success: function (data) {
                console.log('consultation started..!')
            },
            error: function () {
                console.log('failed to start consultation..!')
            }
        });
    }
}


function finishConsultation() {
    var booking_id = localStorage.getItem('current_booking_id') ? parseInt(localStorage.getItem('current_booking_id')) : 0;
    let patient_id = $('#patient_id').val();
    if (isNaN(booking_id)) {
        booking_id = 0;
    }
    if (booking_id > 0) {
        var url = $('#base_doumenturl').val() + "/emr/finishConsultation";
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            data: {
                current_booking_id: booking_id,
                patient_id: patient_id
            },
            success: function (data) {
                console.log('consultation finished..!')
            },
            error: function () {
                console.log('failed to finish consultation..!')
            }
        });
    }
}
