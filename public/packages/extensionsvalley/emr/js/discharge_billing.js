var token = $("#c_token").val();
var base_url = $("#base_url").val();

$(document).on("click", "#new_service_bill_btn", function (){
    var patient_id = $("#patient_id").val();
    var url = '';
    if(patient_id){
        url = base_url + "/service/serviceBilling/0/"+patient_id;
    } else {
        url = base_url + "/service/serviceBilling";
    }
    
    var win = window.open(url, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
        win.onbeforeunload = function(){ getPendingBillsIntoDischargeBill(); }
    } else {
        //Browser has blocked it
        Command: toastr["error"]("Please allow popups for this website");
    }

})



$(window).load(function() {
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    if (location_id != '') {
        $("#billing_location").val(location_id);
    }

    if (parseInt($("#bill_id").val()) > 0) {
        fetchDischargeBillDetails($("#bill_id").val());
    }

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm:ss A'
    });

    $(".select2").select2();
    $("#op_no").focus();

});

$(document).on('click', '.advanceSearchBtn', function(event) {
    $("#searchpatient_type").empty();
    $("#searchpatient_type").append('<option value="IP">IP</option>');
    $("#searchpatient_type").val('IP').trigger('change');
    advancePatientSearch(2);
    $("#getPatientListModel").modal({
        backdrop: 'static',
        keyboard: false
    });
});



function getPendingBillsIntoDischargeBill(){
    var url = $('#base_url').val() + "/discharge/getPendingBillsIntoDischargeBill";
    var patient_id = $("#patient_id").val();
    var bill_id = $("#bill_id").val();
    bill_id = parseInt(bill_id);
    if(patient_id){
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                bill_id: bill_id,
            },
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(html) {
                $(".discharge_billing_table_body").empty();
                $(".discharge_billing_department_table_body").empty();
                $(".discharge_billing_table_body").html(html.result);
                $(".discharge_billing_department_table_body").html(html.result_department);

                calculateTotalAmount();
    
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }
    
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }
    
}


$(document).on('input', '.number_class', function() {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    let new_val = cur_obj_value.replace(/[^\d.]+/g, "").replace(/(\..*)\./g, '$1');
    $(cur_obj).val(new_val);
});

function selectedPatientFromAdvanceSearch(patient_details){
    var uhid = patient_details.uhid;
    getIPPatientDetailsByUHID(uhid);
}

function getIPPatientDetailsByUHID(uhid){
    var url = $('#base_url').val() + "/discharge/getIPPatientDetailsByUHID";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            uhid: uhid
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            console.log(data)
            $("#op_noAjaxDiv").hide();
            $("#patient_id").val(data.patient_details.patient_id);
            $("#op_no").val(data.patient_details.uhid);
            $("#op_no_hidden").val(data.patient_details.patient_id);
            $("#patient_name_dis").html(data.patient_details.patient_name);
            $("#ip_number").html(data.patient_details.admission_no);
            $("#consulting_doctor").val(data.patient_details.consulting_doctor);
            $("#current_room").html(data.patient_details.room_name);
            $("#company").val(data.patient_details.company_id);
            $("#pricing").val(data.patient_details.pricing_id);
            $("#admission_date").html(data.patient_details.admission_date);
            $(".patient_advance").val(data.patient_details.patient_advance);
            $("#payment_type").empty();

            $.each(data.payment_types, function(key, value){
                $("#payment_type").append('<option value="' + key + '">' + value + '</option>');
            });
            
            getPendingBillsIntoDischargeBill();
            fetchPatientPendingBills(false);
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}



$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: "",
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});


$(document).on("click", '.department_check', function(){
    $(".bills_row").hide();
    $(".department_check").each(function(){
        if($(this).prop("checked")){
            var department_id = $(this).val();
            $(".bill_row_"+department_id).show();
        }
    });
    resetSerialNumbers();
})

function resetDischargeBill(){
    getPendingBillsIntoDischargeBill();
}


$(document).on("click", '.include_exclude_btn', function(){
    getBillsForIncludeExclude();
    
})


function getBillsForIncludeExclude(){
    var url = $('#base_url').val() + "/discharge/getBillsForIncludeExclude";
    var patient_id = $("#patient_id").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var bill_no_search = $("#bill_no_search").val();
    var bill_tag_search = $("#bill_tag_search").val();
    if(patient_id){
        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                bill_date_from:bill_date_from,
                bill_date_to:bill_date_to,
                bill_no_search:bill_no_search,
                bill_tag_search:bill_tag_search
            },
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(data) {
                $(".include_exclude_bills_table_body").empty();
                $(".include_exclude_bills_table_body").html(data.result);
                $("#include_exclude_bill_modal").modal('show');
    
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }
    
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr["error"]("Please select patient.");
    }
}

function includeExcludeBill(bill_head_id, include_exclude_status){
    var url = $('#base_url').val() + "/discharge/includeExcludeBill";
    if(bill_head_id){
        $.ajax({
            type: "POST",
            url: url,
            data: {
                bill_head_id: bill_head_id,
                include_exclude_status:include_exclude_status
            },
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(data) {
                if(data.status == 1){
                    Command: toastr["success"]("Succefully saved");
                    getBillsForIncludeExclude();
                } else {
                    Command: toastr["error"]("Faild to include or exclude bill");
                }
                
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }
}

function resetIncludeExcludeSearchForm(){
    $("#bill_date_from").val("");
    $("#bill_date_to").val("");
    $("#bill_no_search").val("");
    $("#bill_tag_search").val("").trigger('change');
}

$(document).on("click", "#death_patient", function(){
    if($(this).prop("checked")){
        $("#death_time").attr('disabled', false);
        $("#death_time").attr('readonly', false);
    } else {
        $("#death_time").attr('disabled', true);
        $("#death_time").attr('readonly', true);
        $("#death_time").focus();
    }
})


function resetSerialNumbers() {
    var number = 0;
    $('tr.bills_row').find(".serial_no").each(function(key, val) {
        if($(this).parents('tr').css("display") != 'none'){
            number = number + 1;
            $(this).html(number);
        }
        
    })
}

function saveDischargeBill(discharge_status){
    
    var patient_id = $("#patient_id").val();
    if(!patient_id){
        Command: toastr["error"]("Please select patient");
        return false;
    }
    var billing_location = $("#billing_location").val();
    if(!billing_location){
        Command: toastr["error"]("Please select billing location");
        return false;
    }
    var consulting_doctor = $("#consulting_doctor").val();
    if(!consulting_doctor){
        Command: toastr["error"]("Please select doctor");
        return false;
    }
    var payment_type = $("#payment_type").val();
    if(!payment_type){
        Command: toastr["error"]("Please select payment type");
        return false;
    }
    var discount_type = $("#discount_type").val();
    if(discount_type){
        var discount = $(".discount_value").val();
        if(!discount){
            Command: toastr["error"]("Please enter the discount");
            return false;
        }
    }
    var death_patient = $("#death_patient").prop('checked');
    if(death_patient){
        var death_time = $("#death_time").val();
        if(!death_time){
            Command: toastr["error"]("Please enter the death date and time.");
            return false;
        }
    }
    var message_show = '<p>Are you sure you want to save this bill. </p> <p> Net amount : ' + $(".net_amount").val() + '</p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Save',
                className: 'btn-success',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function(result) {
            if (result) {
                var data = {};
                data.bill_id = $("#bill_id").val();
                data.patient_id = $("#op_no_hidden").val();
                data.uhid = $("#op_no_hidden").val();
                data.payment_type = $("#payment_type").val();
                data.billing_location = $("#billing_location").val();
                data.doctor_id = $("#consulting_doctor").val();
                data.doctor_name = $("#consulting_doctor option:selected").text();
                data.death_patient = $("#death_patient").prop('checked');
                data.death_time = $("#death_time").val();
                data.discount_type = $("#discount_type").val();
                data.discount = $(".discount_value").val();
                data.discount_narration = $("#discount_narration").val();
                data.company = $("#company").val();
                data.pricing = $("#pricing").val();
                data.discharge_reason = $(".discharge_reason").val();
                data.patient_advance = $(".patient_advance").val();

                data.discharge_items = [];
                $(".discharge_billing_table_body").find('tr').each(function(key, val) {
                    var bill_head_id= $(val).attr('data-bill-head-id');
                    var credit_bill_detail_id= $(val).attr('data-credit-bill-detail-id');
                    var bill_detail = {};
                    bill_detail.bill_head_id = bill_head_id;
                    bill_detail.credit_bill_detail_id = credit_bill_detail_id;
                    data.discharge_items.push(bill_detail);
                });

                data.net_amount = $(".net_amount").val();
                data.bill_amount = $(".bill_amount").val();
                data.patient_payable = $(".patient_payable").val();
                data.payor_payable = $(".payor_payable").val();

                var url = $('#base_url').val() + "/discharge/saveDischargeBill";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        discharge_bill_data: data,
                        discharge_status:discharge_status
                    },
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                    },
                    success: function(data) {
                        if(data.status == 1){
                            Command: toastr["success"]("Discharge bill successfully saved.");
                            if(discharge_status == 1){
                                var discharge_bill_head_id = data.discharge_bill_head_id;
                                var bill_ids = [];
                                bill_ids.push(discharge_bill_head_id);
                                loadCashCollection(bill_ids, 1);
                            } else {
                                $(".save_message_bill_no").html(data.bill_no);
                                $("#print_config_modal").modal('show');
                            }
                        } else {
                            Command: toastr["error"]("Faild to save discharge bill");
                        }
                        
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
        }
    });
}

function calculateTotalAmount(){
    var total_company_payable_amount = 0;
    var total_patient_payable_amount = 0;
    var bill_amount = 0;
    var net_amount = 0;
    var payment_type = $("#payment_type").val();
    $(".company_payable_amount").each(function(key, val){
        total_company_payable_amount = parseFloat(total_company_payable_amount) + parseFloat($(val).html())
    })
    $(".patient_payable_amount").each(function(key, val){
        total_patient_payable_amount = parseFloat(total_patient_payable_amount) + parseFloat($(val).html())
    })
    $(".net_amount_discharge").each(function(key, val){
        bill_amount = parseFloat(bill_amount) + parseFloat($(val).html())
    })

    
    var discount_type = $("#discount_type").val();
    var discount = $(".discount_value").val();
    if(discount_type && discount){
        if(discount > 0){
            if(discount_type == 1){
                net_amount = bill_amount - ((bill_amount*discount)/100);
                if(payment_type == 'ipcredit' || payment_type == 'cash/Card'){
                    total_patient_payable_amount = total_patient_payable_amount - ((bill_amount*discount)/100);
                } else if(payment_type == 'insurance' || payment_type == 'credit'){
                    total_company_payable_amount = total_company_payable_amount - ((bill_amount*discount)/100);
                }
                
            } else {
                net_amount = bill_amount - discount;
                if(payment_type == 'ipcredit' || payment_type == 'cash/Card'){
                    total_patient_payable_amount = total_patient_payable_amount - discount;
                } else if(payment_type == 'insurance' || payment_type == 'credit'){
                    total_company_payable_amount = total_company_payable_amount - discount;
                }
            }
        }
        $(".net_amount").val(parseFloat(net_amount).toFixed(2));
        $(".net_amount").attr('data-net-amount', parseFloat(net_amount).toFixed(2));
    } else {
        $(".net_amount").val(parseFloat(bill_amount).toFixed(2));
        $(".net_amount").attr('data-net-amount', parseFloat(bill_amount).toFixed(2));
    }

    $(".payor_payable").val(parseFloat(total_company_payable_amount).toFixed(2));
    $(".patient_payable").val(parseFloat(total_patient_payable_amount).toFixed(2));
    $(".bill_amount").val(parseFloat(bill_amount).toFixed(2));

}

function calculateTotalDiscount(){
    var bill_amount = $(".bill_amount").val();
    var discount_type = $("#discount_type").val();
    var discount = $(".discount_value").val();
    var total_patient_payable_amount = 0;
    var total_company_payable_amount = 0;
    var net_amount = 0;

    if(discount_type && discount){
        if(discount > 0){
            if(discount_type == 1){
                net_amount = bill_amount - ((bill_amount*discount)/100);
                if(payment_type == 'ipcredit' || payment_type == 'cash/Card'){
                    total_patient_payable_amount = total_patient_payable_amount - ((bill_amount*discount)/100);
                } else if(payment_type == 'insurance' || payment_type == 'credit'){
                    total_company_payable_amount = total_company_payable_amount - ((bill_amount*discount)/100);
                }
                
            } else {
                net_amount = bill_amount - discount;
                if(payment_type == 'ipcredit' || payment_type == 'cash/Card'){
                    total_patient_payable_amount = total_patient_payable_amount - discount;
                } else if(payment_type == 'insurance' || payment_type == 'credit'){
                    total_company_payable_amount = total_company_payable_amount - discount;
                }
            }
        }
        $(".net_amount").val(parseFloat(net_amount).toFixed(2));
    } else {
        $(".net_amount").val(parseFloat(bill_amount).toFixed(2));
    }

    $(".payor_payable").val(parseFloat(total_company_payable_amount).toFixed(2));
    $(".patient_payable").val(parseFloat(total_patient_payable_amount).toFixed(2));
}

function fetchDischargeBillDetails(bill_id){
    var url = $('#base_url').val() + "/discharge/fetchDischargeBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            console.log(data.discharge_bill_details);
           
            if(data.status == 1){
                if(data.discharge_bill_details.discharge_status == 1){
                    bootbox.alert('Discharge bill already generated.', function(){ 
                        var base_url = $('#base_url').val();
                        var url = base_url + '/discharge/dischargeBilling';
                        window.location.href = url;
                    });
                } else {
                    getIPPatientDetailsByUHID(data.discharge_bill_details.uhid);
                    $(".discharge_bill_no").html(" - "+data.discharge_bill_details.bill_no);
                    $(".save_message_bill_no").html(data.discharge_bill_details.bill_no);
                    $("#bill_no").val(data.discharge_bill_details.bill_no);
                    $("#op_no").attr("disabled", true);
                    $(".advanceSearchBtn").attr("disabled", true);
                    $("#discount_type").val(data.discharge_bill_details.discount_type)
                    $(".discount_value").val(data.discharge_bill_details.discount_value)
                    $("#discount_narration").val(data.discharge_bill_details.disc_narration)
                    $(".discharge_reason").val(data.discharge_bill_details.discharge_reason);

                    if(data.discharge_bill_details.death_datetime){
                        $("#death_patient").prop("checked", true);
                        $("#death_time").val(moment(data.discharge_bill_details.death_datetime).format('MMM-DD-YYYY hh:mm:ss A'));
                        $("#death_time").attr('disabled', false);
                        $("#death_time").attr('readonly', false);
                    }
                }
            }
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}


function paymentCompleted() {
    $("#print_config_modal").modal('show');

}

function printBillDetails(bill_id){
    if ( !bill_id || bill_id == 0) {
        bill_id = $("#bill_id").val();
    }
    var include_hospital_header = $("#include_hospital_header").prop('checked') ? 1 : 0;
    var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var printMode = $('input[name=printMode]:checked').val();
    var url = $('#base_url').val() + "/discharge/printDischargeBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
            location_code: location_code,
            include_hospital_header: include_hospital_header,
            printMode: printMode
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if(data.status == 1){
                if (printMode == 1) {
                    var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data.print_data + '<script>setTimeout(function(){window.print(); window.close();  },1000)</script>');
                } else {
                    var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 landscape;} .patient_head{ font-size:10px !important; } </style>' + data.print_data + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
                }
            } else {
                toastr.success('Bill print success.');
            }
            
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}

$(document).on("keyup", ".discount_value", function(){
    if($("#discount_type").val() == 1 && ($(".discount_value").val() > 100 || $(".discount_value").val() == 0)){
        $(".discount_value").val('');
    } else if($("#discount_type").val() == 2 ){
        var net_amount = $(".net_amount").attr('data-net-amount');
        var discount_value = $(".discount_value").val();
        
        if(net_amount && discount_value){
            net_amount = parseInt(net_amount);
            discount_value = parseInt(discount_value);
            if( (discount_value > net_amount) || discount_value == 0 ){
                $(".discount_value").val('');
            }
        }
    }
    calculateTotalDiscount();
    $(".discount_value").focus();
})

$(document).on("change", "#discount_type", function(){
    if($("#discount_type").val() == 1 && ($(".discount_value").val() > 100 || $(".discount_value").val() == 0)){
        $(".discount_value").val('');
    } else if($("#discount_type").val() == 2 ){
        var net_amount = $(".net_amount").attr('data-net-amount');
        var discount_value = $(".discount_value").val();
        if(net_amount && discount_value){
            net_amount = parseInt(net_amount);
            discount_value = parseInt(discount_value);
            if( (discount_value > net_amount) || discount_value == 0 ){
                $(".discount_value").val('');
            }
        }
    }
    calculateTotalDiscount();
    $(".discount_value").focus();
})

$('#print_config_modal').on('hidden.bs.modal', function() {
    var bill_id = $('#bill_id').val();
    var base_url = $('#base_url').val();
    if (!bill_id) {
        window.location.reload();
    } else {
        var url = base_url + '/discharge/dischargeBilling';
        window.location.href = url;
    }


});


$(document).on('keydown', function(event) {
    if($('#op_noAjaxDiv').css('display') == 'block'){
        if (event.which == 13) {
            $("#op_noAjaxDiv").find('li.liHover').trigger('click');
        } 
    }
});

$(document).on('click','.include_exclude_modal_close_btn',function(){
   $('#include_exclude_bill_modal').modal('hide');
   getPendingBillsIntoDischargeBill()
});

