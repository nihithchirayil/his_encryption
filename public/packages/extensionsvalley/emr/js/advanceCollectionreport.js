$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + "/report/advanceCollectionReport";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}



function getReportData() {
    var url = $('#base_url').val() + "/report/getAdvanceCollectionReport";

    //-------filters---------------------------------------------
    var from_date = $('#discharge_date_from').val();
    var to_date = $('#discharge_date_to').val();
    var adv_as_on = $('#adv_as_on').val()
    var uhid = $('#op_no_hidden').val();;
    var patient_type = $('#patient_type').val();
    var company_ids = $('#company_select').val();
    var param = { from_date: from_date, to_date: to_date,adv_as_on: adv_as_on, uhid: uhid, patient_type: patient_type,company_ids:company_ids };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#search_results').attr('disabled', true);
            $('#searchresultspin').removeClass('fa fa-search');
            $('#searchresultspin').addClass('fa fa-spinner');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#search_results').attr('disabled', false);
            $('#searchresultspin').removeClass('fa fa-spinner');
            $('#searchresultspin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}

function search_clear() {

    var current_date = $('#current_date').val();
    $(".datapicker").val('');
    $('#discharge_date_from').val(current_date);
    $('#discharge_date_to').val(current_date);
    $('#op_no').val('');
    $('#op_no_hidden').val('');
    $('#patient_type').val('ALL');
    $('#patient_type').select2();
    getReportData();
}
