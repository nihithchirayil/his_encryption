$(document).ready(function() {
    setTimeout(function() {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");

    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#admission_date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#admission_date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});
document.getElementById("admission_date_from").blur();
document.getElementById("admission_date_to").blur();
$('#category').on('change',function(){
    $('#item_name').val('');
    $('#item_name_hidden').val('');
    $('#item_name').focus();
});
$('#sub_category').on('change',function(){
    $('#item_name').val('');
    $('#item_name_hidden').val('');
    $('#item_name').focus();
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {


             var ward1 = $('#category').val();
             var ward2 = $('#sub_category').val();
             var ward3   = $('#item_name_hidden').val();
            var url = route_json.outletwisestocklistingreport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring +'&ward1=' + ward1 +'&ward2=' + ward2 +'&ward3='+ward3,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
function header()
{
    var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
                });
}

$("#choose_quantity").on('keyup', function() {
    $('#error').css('display','none');
$('#choose_quantity').css('background','none');
});
function getReportData() {
    if( !$('#choose_quantity').val() ) {
       $('#error').css('display','block');
       $('#choose_quantity').css('background','rgb(255 226 226 / 50%)');

       return;
     }
    if( !$('#transaction_type').val() ) {
        document.getElementById("transaction_type").selectedIndex = "1";
     }
    var url = route_json.stockmovementreportresult;

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var chk1 = document.getElementById("slow"); //to check the  checkbox
    var chk2 = document.getElementById("fast"); //to check the  checkbox
    var chk3 = document.getElementById("non_moving"); //to check the  checkbox

    if (chk1.checked){
        var check_box1 = 1;
    }
    if (chk2.checked){
        var check_box2 = 2;
    }
    if (chk3.checked){
        var check_box3 = 3;
    }
    filters_list['slow'] = check_box1; //pushing the value into the list;
    filters_list['fast'] = check_box2; //pushing the value into the list;
    filters_list['non_moving'] = check_box3; //pushing the value into the list;


    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}





//-------------------------------------------------RELATIVE SELECT BOX FOR SUB CATEGORY----------------------------------------------------------------------------------------
$("#category").on('change', function() {
    var ward = $('#category').val();
    var load = $('#warning').val();
   //alert(ward);

   if (ward == '') {
      var ward = 'a';

    }


 if(ward) {

         var url= route_json.ajaxsubcategorylist;

        $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    data: {'ward':ward},
                    beforeSend: function () {
                    document.getElementById("item_name"). disabled = true;
                    document.getElementById("sub_category"). disabled = true;
                    if(load==undefined){
                   // $('#s2id_sub_category').append('<p id="warning" style=color:red;padding-left:2px;position:relative>please wait......</p>');
                   // $('#item_name').after('<p id="warning1" style=color:red;padding-left:2px;position:relative>please wait......</p>');
                    }

                    },
                    success: function (html) {


                                if(html){

                                       $("#sub_category").empty();

                                        $("#sub_category").html('<option value="">   </option>');
                                        $.each(html,function(key,value){
                                            ($("#sub_category").append('<option value="'+value.id+'">'+value.subcategory_name+'</option>'));
                                        });
                                        $('#sub_category').select2();

                                }else{
                                        $("#sub_category").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["warning"]("Please  select a proper item !");


                    },
                    complete: function () {
                        $('#sub_category').LoadingOverlay("hide");
                        $('#warning').remove();
                        $('#warning1').remove();
                        document.getElementById("item_name"). disabled = false;
                        document.getElementById("sub_category"). disabled = false;
                    }

                });

        }else {
                $('#category').focus();
                $("#sub_category").empty();
            }

    });





function occupancy() {
    var url = route_json.currentoccupanciesreport;

    //-------filters---------------------------------------------

    var filters_list = new Object();

    var filters_value = '';
    var filters_id = '';
     $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}



$(document).ready(function(){
    $('.check').click(function() {
        $('.check').not(this).prop('checked', false);
    });
});
function search_clear(){
    $("#location").select2('val', '');
    $("#category").select2('val', '');
    $("#sub_category").select2('val', '');
    $("#area").select2('val', '');
    var current_date = $('#current_date').val();
     $('#item_type').select2('val', '');
    $('#transaction_type').select2('val', '');
    $('#sort_by').select2('val', '');
    $('#sort_by_desending').select2('val', '');
    $('#admission_date_from').val(current_date);
    $('#admission_date_to').val(current_date);
    $('#item_name_hidden').val('');
    $('#doctor_name_hidden').val('');
    $('#item_name').val('');
    $('#doctor_name').val('');
    $('#non_moving').val('');
    $('#fast').val('');
    $('#slow').val('');
    $('#select2').val('');
}
