$(document).ready(function () {
    setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});

$('#location').on('change', function () {
    removeItemSearch()
});
$('#item_type').on('change', function () {
    removeItemSearch()
});
$('#category').on('change', function () {
    removeItemSearch()
});
$('#sub_category').on('change', function () {

});

function removeItemSearch() {
    $('#product_name').focus();
    $('#product_name').val('');
    $('.ajaxSearchBox').hide();
    $('#product_name_hidden').val('');
}

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        var item_type = $('#item_type').val();
        var category = $('#category').val();
        var sub_category = $('#sub_category').val();
        var location = $('#location').val();
        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.Collection;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring + '&item_type=' + item_type + '&location=' + location + '&category=' + category + '&sub_category=' + sub_category,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
//------------------------------------------------------batch wise listing-------------------------------------------------------------------------------

function getCollectionReportDatabatch(item_code, item_desc) {
    $('.ps-scrollbar-x').remove();
    if (!$('#location').val()) {
        toastr.warning("Please select any Location !!");
        return;
    }
    $('#popup').html('');
    $("#datapopup").modal('show');

    var location_code = $('#location').val();
    var item_code = item_code;
    var item_desc = item_desc;
    $('#popup2').text(item_desc);
    batchwise(location_code, item_code, item_desc);


}
function batchwise(location_code, item_code) {

    var url = route_json.popupdata;

    var filters_list = { location_code: location_code, item_code: item_code };
    $.ajax({
        type: "POST",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#popup').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#popup').html(html);
        },
        complete: function () {
            $('#popup').LoadingOverlay("hide");
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $("#datapopup").modal('hide');
            return;

        }
    });
}


function getCollectionReportData() {
    if (!$('#location').val()) {
        toastr.warning("Please select any Location !!");
        return;
    }

    var url = route_json.CollectionData;

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'date_from') {
            filters_id = 'date_from';
        }
        if (filters_id == 'date_to') {
            filters_id = 'date_to';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }



    });
    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });
}

function datefilterstatus(obj) {
    if (obj.value == '1') {
        $('#datefilterdataddiv').show();
    } else {
        $('#datefilterdataddiv').hide();
    }
}

function hideandShowDateFilters() {

    if ($('input[name="appy_date"]:checked').length > 0) {
        // $("#date_filter_div").load("#date_filter_div");

        var x = document.getElementById("checkbox");
        if (x.style.display === "none") {
            x.style.display = "block";
        }
        $('.filter_label2').show();
        $('#date_from').show();
        $('#date_to').show();

    } else {

        $('.filter_label2').hide();
        $('#date_from').hide();
        $('#date_to').hide();

        var months = ["Jan", "Feb", "March", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dece"];
        var today = new Date();
        var MM = months[today.getMonth()];
        var dd = String(today.getDate()).padStart(2, '0');
        var YYYY = today.getFullYear();
        today = MM + '-' + dd + '-' + YYYY;
        $("#date_from").val(today);
        $("#date_to").val(today);
    }

}

//-------------------------------------------------RELATIVE SELECT BOX FOR SUB CATEGORY----------------------------------------------------------------------------------------
$("#category").on('change', function () {
    var ward = $('#category').val();
    var load = $('#warning').val();

    if (ward == '') {
        var ward = 'a';

    }


    if (ward) {

        var url = route_json.ajaxsubcategorylist;

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: { 'ward': ward },
            beforeSend: function () {
                document.getElementById("sub_category").disabled = true;
                if (load == undefined) {
                    $('#s2id_sub_category').append('<p id="warning" style=color:red;padding-left:2px;position:relative>please wait......</p>');
                }

            },
            success: function (html) {
                console.log(html);

                if (html) {

                    $("#sub_category").empty();

                    $("#sub_category").html('<option value="">Select Subcategory </option>');
                    $.each(html, function (key, value) {
                        ($("#sub_category").append('<option value="' + value.id + '">' + value.subcategory_name + '</option>'));
                    });
                    $('#sub_category').select2();

                } else {
                    $("#sub_category").empty();
                }

            },

            error: function () {
                Command: toastr["warning"]("Please  select a proper item !");


            },
            complete: function () {
                $('#sub_category').LoadingOverlay("hide");
                $('#warning').remove();
                document.getElementById("sub_category").disabled = false;
            }

        });

    } else {
        $('#category').focus();
        $("#sub_category").empty();
    }

});
//---------generate csv------------------------------------------------------

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function advance_generate_csv() {

    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");

    // $("#result_data_table").on('submit', function(){

    //     $("#csv_results").attr("disabled",true);
    // });
};

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------




function printAdvanceReportData() {
    $('#print_config_modal').modal('toggle');
    // $("#print_results").removeClass("disable");
}


function print_generate(printData) {
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:0;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
