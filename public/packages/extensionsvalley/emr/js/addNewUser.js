$(document).ready(function () {
    base_url = $('#base_url').val();
    token = $('#token_hiddendata').val();
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    resetUserForm(1);
});

var base_url = '';
var token = '';

function showImage() {
    $('#imageViewSign').empty();
    user_id = $('#user_id').val();
    img_url = $("#sign_img_"+user_id).val();
    $('#imageModal').modal('show');
    $('#imageViewSign').append($("<img id='signIm'>").attr({ src: img_url }));
    var myImage = document.getElementById("signIm");
    myImage.setAttribute("style", "width: 100%; height: 100%;");
}

function userEditLoadData(user_name, id, name, email_id, status, groups, is_cashier, is_nurse, store, location,
    nurse_station,default_landing_page,default_procedure_type,signature_name,is_casuality_user) {
    $('.userdataform').removeClass('bg-green');
    $('#user_id').val(id);
    $('#group_id').val(groups);
    $('#name').val(name);
    $('#user_name').val(user_name);
    $('#email').val(email_id);
    $('#status').val(status).select2();
    $('#default_station_id').val(nurse_station).select2();
    $('#default_landing_page').val(default_landing_page).select2();
    $('#default_store').val(store).select2();
    $('#default_location').val(location).select2();
    default_procedure_type = (default_procedure_type != '0' && default_procedure_type != '') ? default_procedure_type : '';
    $('#default_procedure_type').val(default_procedure_type).select2();
    $('#popupSign').addClass('hidden');
    if (signature_name) {
        $('#popupSign').removeClass('hidden');
    }
    if (is_cashier == 1) {
        $("#is_cashier").prop("checked", true);
    } else {
        $("#is_cashier").prop("checked", false);
    }
    if (is_nurse == 1) {
        $("#is_nurse").prop("checked", true);
    } else {
        $("#is_nurse").prop("checked", false);
    }
    if (is_casuality_user == 1) {
        $("#is_casuality_user").prop("checked", true);
    } else {
        $("#is_casuality_user").prop("checked", false);
    }
    $('#userHeaderString').html('Edit User');
    $('#userformrow' + id).addClass('bg-green');
    $('#password').val('');
    $('#password_confirmation').val('');
}

function resetUserForm(from_type) {
    var user_id = $('#user_id').val();
    $('#user_id').val(0);
    $('#group_id').val(0);
    $('#name').val('');
    $('#user_name').val('');
    $('#email').val('');
    $('#password').val('');
    $('#password_confirmation').val('');
    $('#status').val(1).select2();
    $('#default_station_id').val('').select2();
    $('#default_store').val('').select2();
    $('#default_location').val('').select2();
    $('#default_landing_page').val('').select2();
    $('#default_procedure_type').val('').select2();
    $("#is_cashier").prop("checked", false);
    $("#is_casuality_user").prop("checked", false);
    $("#is_nurse").prop("checked", false);
    $('#uploadSignature').val('');
    $('#userHeaderString').html('Add New User');
    if (user_id == '0' || from_type == '1') {
        searchUser();
    } else {
        // $('#userformrow' + user_id).trigger('click');
    }
}

function resetFilter() {
    $('#search_username').val('');
    $('#search_status').val('').select2();
    searchUser();
}

function searchUser() {
    var url = base_url + "/admin/searchUsers";
    var user_name = $('#search_username').val();
    var status = $('#search_status').val();
    var param = { _token: token, user_name: user_name, status: status };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchUserBtn').attr('disabled', true);
            $('#searchUserSpin').removeClass('fa fa-search');
            $('#searchUserSpin').addClass('fa fa-spinner');
            $('#sarchUsersDataDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $('#sarchUsersDataDiv').html(msg);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#searchUserBtn').attr('disabled', false);
            $('#searchUserSpin').removeClass('fa fa-spinner');
            $('#searchUserSpin').addClass('fa fa-search');
            $('#sarchUsersDataDiv').LoadingOverlay("hide");
        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function passwordConfirm() {
    var password = $('#password').val();
    var password_confirmation = $('#password_confirmation').val();
    if (password != password_confirmation) {
        toastr.warning("Confirm Password Do Not Match");
        $('#password_confirmation').val('');
    }
}

function checkEmailUserName(from_type) {
    var flag = 1;
    var check_string = '';
    if (from_type == '1') {
        check_string = $('#email').val();
        var email_status = isEmail(check_string);
        if (!email_status) {
            $('#email').val('');
            flag = 0;
            toastr.warning("Please Enter a Valid Email");
            return;
        }
    } else if (from_type == '2') {
        check_string = $('#user_name').val();
    }
    var user_id = $('#user_id').val();
    var url = base_url + "/admin/checkEmailUserName";
    if (check_string && flag == 1) {
        var param = { _token: token, user_id: user_id, from_type: from_type, check_string: check_string };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveUserFormBtn').attr('disabled', true);
                $('#saveUserFormSpin').removeClass('fa fa-save');
                $('#saveUserFormSpin').addClass('fa fa-spinner');
            },
            success: function (data) {
                if (parseInt(data) != 0) {
                    if (parseInt(from_type) == 1) {
                        toastr.warning("Email Already Used");
                        $('#email').val('');
                    } else if (parseInt(from_type) == 2) {
                        $('#user_name').val('');
                        toastr.warning("User Name Already Used");
                    }
                }
            },
            complete: function () {
                $('#saveUserFormBtn').attr('disabled', false);
                $('#saveUserFormSpin').removeClass('fa fa-spinner');
                $('#saveUserFormSpin').addClass('fa fa-save');
            }, error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });
    }
}

function validateImage(input) {
    var file = input.files[0];
    var imageType = /image\/png/;

    if (!file.type.match(imageType)) {
        toastr.error("Please select a valid PNG image.");
        input.value = "";
        return;
    }
}
function saveUserForm() {
    var user_id = $('#user_id').val();
    var group = $('#group_id').val();
    var name = $('#name').val();
    var email = $('#email').val();
    var user_name = $('#user_name').val();
    var password = $('#password').val();
    var password_confirmation = $('#password_confirmation').val();
    var is_cashier = $('#is_cashier').is(":checked");
    var is_casuality_user = $('#is_casuality_user').is(":checked");
    var is_nurse = $('#is_nurse').is(":checked");
    var default_location = $('#default_location').val();
    var default_station_id = $('#default_station_id').val();
    var default_store = $('#default_store').val();
    var status = $('#status').val();
    var default_landing_page = $('#default_landing_page').val();
    var default_procedure_type = $('#default_procedure_type').val();
    var fileInput = document.getElementById("uploadSignature").files[0];
    var formData = new FormData();
    formData.append('file', fileInput);
    formData.append('user_id', user_id);
    formData.append('group', group);
    formData.append('name', name);
    formData.append('email', email);
    formData.append('user_name', user_name);
    formData.append('password', password);
    formData.append('password_confirmation', password_confirmation);
    formData.append('is_cashier', is_cashier);
    formData.append('is_casuality_user', is_casuality_user);
    formData.append('is_nurse', is_nurse);
    formData.append('default_location', default_location);
    formData.append('default_station_id', default_station_id);
    formData.append('default_store', default_store);
    formData.append('status', status);
    formData.append('default_landing_page', default_landing_page);
    formData.append('default_procedure_type', default_procedure_type);
    if (status) {
        if (name) {
            if (email) {
                if (user_name) {
                    if (password || user_id != '0') {
                        if (password_confirmation || user_id != '0') {
                            var url = base_url + "/admin/saveUserData";
                            var param = {
                                _token: token, user_id: user_id, name: name, email: email, user_name: user_name, password: password,
                                is_cashier: is_cashier, is_nurse: is_nurse, default_location: default_location, group: group,
                                default_station_id: default_station_id, default_store: default_store, status: status,default_landing_page:default_landing_page,
                                default_procedure_type:default_procedure_type
                                ,is_casuality_user: is_casuality_user
                            };
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: formData,
                                processData: false,
                                contentType: false,
                                beforeSend: function () {
                                    $('#saveUserFormBtn').attr('disabled', true);
                                    $('#saveUserFormSpin').removeClass('fa fa-save');
                                    $('#saveUserFormSpin').addClass('fa fa-spinner');
                                },
                                success: function (data) {
                                    if (data) {
                                        resetUserForm(1);
                                        toastr.success("User Data Successfully Updated");
                                    } else {
                                        toastr.error("Please Check Internet Connection");
                                    }
                                    var url = base_url + "/admin/listUsers";
                                    window.location.href = url;
                                },
                                complete: function () {
                                    $('#saveUserFormBtn').attr('disabled', false);
                                    $('#saveUserFormSpin').removeClass('fa fa-spinner');
                                    $('#saveUserFormSpin').addClass('fa fa-save');
                                }, error: function () {
                                    toastr.error("Please Check Internet Connection");
                                }
                            });
                        } else {
                            toastr.warning("Please Enter Confirmation Password");
                            $('#password_confirmation').focus();
                        }
                    } else {
                        toastr.warning("Please Enter Password");
                        $('#password').focus();
                    }
                } else {
                    toastr.warning("Please Enter User Name");
                    $('#user_name').focus();
                }
            } else {
                toastr.warning("Please Enter Email");
                $('#email').focus();
            }
        } else {
            toastr.warning("Please Enter Name");
            $('#name').focus();
        }
    } else {
        toastr.warning("Please Select Status");
    }
}
