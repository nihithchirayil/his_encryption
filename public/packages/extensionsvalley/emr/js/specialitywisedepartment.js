$(document).ready(function() {
    setTimeout(function() {
        //$(body).addClass('sidebar-collapse');
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");
    }, 300);
    // $('tbody').sortable();
   // $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
   $('#discharge_date_from').datetimepicker({
    format: 'MMM-DD-YYYY',
});
$('#discharge_date_to').datetimepicker({
    format: 'MMM-DD-YYYY',
});

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

});

document.getElementById("discharge_date_from").blur();
document.getElementById("discharge_date_to").blur();



function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});



function getReportData() {
    var url = $('#base_url').val() + "/report/getSpecialitywiseDepartmentReport";

    //-------filters---------------------------------------------
    var from_date = $('#discharge_date_from').val();
    var to_date = $('#discharge_date_to').val();
    var filter_type = $('#filter_type').val();
    var doctor_id = $('#doctor_id').val();
    if(filter_type == 0){
        Command: toastr["warning"]("Select filter type!");
        return;
    }
    var param = { from_date: from_date, to_date: to_date, filter_type: filter_type,doctor_id:doctor_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function() {
            $('#ResultDataContainer').css('display', 'block');
            $('#search_results').attr('disabled', true);
            $('#searchresultspin').removeClass('fa fa-search');
            $('#searchresultspin').addClass('fa fa-spinner');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function() {
            $('#search_results').attr('disabled', false);
            $('#searchresultspin').removeClass('fa fa-spinner');
            $('#searchresultspin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}

function search_clear() {
    var current_date = $('#current_date').val();
    $(".datapicker").val('');
    $('#discharge_date_from').val(current_date);
    $('#discharge_date_to').val(current_date);
}
