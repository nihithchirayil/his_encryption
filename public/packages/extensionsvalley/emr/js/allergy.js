function fetch_all_allergy(patientId) {
    if (patientId > 0) {
        var url = $('#base_url').val() + "/emr/get-all-allergy";
        $.ajax({
            url: url,
            type: "GET",
            data: "patient_id=" + patientId + "&allergy_value=allergy",
            beforeSend: function () {
                $('#allegy_details').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
            },
            success: function (data) {
                $("#allegy_details").html(data);
            },
            complete: function () {
                var $table = $('table.theadfix_wrapper');
             $table.floatThead({
                  scrollContainer: function($table) {
                         return $table.closest('.theadscroll');
                  }
             });

            }
        });
    }
}

function allergy_check(item_code) {

    var selectedItems = [];
    $('input[name="selected_item_code[]"]').map(function (ind, el) {
        if ($(el).val() != '') {
            selectedItems.push($(el).val());
        }
    });

    let patId = $('#patient_id').val();

    var url = $('#base_url').val() + "/emr/allergy-check";

    $.ajax({
        type: "GET",
        url: url,
        async: false,
        data: 'patId=' + patId + '&Itemcode=' + item_code + '&selcodes=' + JSON.stringify(selectedItems),
        beforeSend: function () {

        },
        success: function (data) {
            let response = JSON.parse(data);
            if (response.status == 0) {
                //allergy true
                // bootbox.alert({
                //   message: response.message,
                //   className: 'rubberBand animated',
                //   buttons: {
                //       ok: {
                //           label: 'Close',
                //           className: 'btn-primary'
                //       }
                //   }
                // });
                Command: toastr["warning"](response.message);
            } else {
                if (response.is_interaction == 1) {
                    // bootbox.alert({
                    //   message: response.message,
                    //   className: 'rubberBand animated',
                    //   buttons: {
                    //       ok: {
                    //           label: 'Close',
                    //           className: 'btn-primary'
                    //       }
                    //   }
                    // });
                    Command: toastr["warning"](response.message);
                }
            }

        },
        complete: function () {

        }
    });
}
function fetchOtherAllergy(){
    var patient_id=$('#patient_id').val();
    var url = $('#base_url').val() + "/emr/getPatientAllergyData";
    if(patient_id>0) {
        $.ajax({
            url: url,
            type: "GET",
            data: 'patient_id='+patient_id+'&get_patient_allergy_data=1',
            beforeSend: function () {
            },
            success: function (html) {
                $("#allergyHistoryEditModalContent").html(html);
                $("#loader").html('');
                $('#allergy_info_popup_loaded').val(1);
            },
            error: function () {
            },
            complete: function () {
            }
        });
    } else {
        $('#allergyHistoryEditModal').modal();
    }
}


function fetchpatientMedicineAllerg(){
    var patient_id = $('#patient_id').val();
    var url = $('#base_url').val() + "/emr/fetchAllergyPrescription";
    //alert(url);
    $.ajax({
        url: url,
        type: "GET",
        data: 'patient_id='+patient_id,
        beforeSend: function () {
        },
        success: function (html) {
           // console.log(html);
            if(html != ''){
                $('#MedicineAllergyListData').html(html);
                setTimeout(function(){
                    if($('#MedicineAllergyListData').find('input[type="text"]:not([readonly])').length > 0 ){
                        $('#MedicineAllergyListData').find('input[type="text"]:not([readonly])').first().focus();
                    } else {
                        $('.addNewRowItem').click();
                    }
                }, 500);

            }
        },
        error: function () {
        },
        complete: function () {
        }
    });
}

function fetchPatientOtherAllergies(){
    var patient_id = $('#patient_id').val();
    var url = $('#base_url').val() + "/emr/fetchPatientOtherAllergies";
    $.ajax({
        url: url,
        type: "GET",
        data: 'patient_id='+patient_id,
        beforeSend: function () {
        },
        success: function (resp) {
            $('.otherAllergies').val(resp.otherAllergies);
        },
        error: function () {
        },
        complete: function () {
        }
    });
}


function fetchPatientAllergies(currentObj){
    $(currentObj).find('i').removeClass('fa-plus').addClass('fa-spinner').addClass('fa-spin');
    fetchpatientMedicineAllerg();
    fetchPatientOtherAllergies();
    $(currentObj).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-plus');
    $("#modal_allergy").modal('show');
}


function searchAllergyType(id,event,i) {

    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#'+id).next().attr('id');
    var ajaxHiddenId = $('#'+id).next().next().attr('id');
    var data = $('#'+id).val();
    if(data == ""){
        $('#'+ajaxHiddenId).val('');
    }
    if (value.match(keycheck)) {

        var allergytype_code = $('#'+id).val();
        allergytype_code = allergytype_code.trim();
    //alert(ajax_div); return;
    if (allergytype_code == "") {
        $("#"+ajax_div).html("");
        $("#"+ajaxHiddenId).val("");
    } else {
        var url = $('#base_url').val() + "/emr/getallergytype";
        $.ajax({
            type: "GET",
            url: url,
            data: 'search_key_alergy_type=' +allergytype_code,
            beforeSend: function () {

            },
            success: function (html) {
                //console.log(html);

                $("#"+ajax_div).html(html).show();
                $("#"+ajax_div).find('li').first().addClass('liHover');
                var data = id.split('-');
                $("#allergy-"+data[1]).val('');
                $("#medCodeId-"+data[1]).val('');
            },
            complete: function () {
            }
        });
    }

}else {
    ajax_list_key_down(ajax_div,event);
}

/* setting for enter key press in ajaxDiv listing */
$(this).on('keydown',function(event){
    if(event.keyCode === 13){
        ajaxlistenter(ajax_div);
        return false;
    }
});
}

function fillallegeryTypeValues(list,code,alergyType) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(alergyType);
    $('#'+itemCodeListDivId).hide();
    $('#'+itemCodeTextId).closest("tr").find("input[name='allergytype_code_hidden[]']").val(code);
}

function fillallegeryValues(list,item_code,item_desc) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(item_desc);
    $('#'+itemCodeListDivId).hide();
    $('#'+itemCodeTextId).closest("tr").find("input[name='allergy_code_hidden[]']").val(item_code);
}


function AddNewAllergy(obj){

    var IdMedi = $(obj).closest('tr').find('input[name="allergy[]"]').attr('id');
    var CodeMedi = $(obj).closest('tr').find('input[name="allergy_code_hidden[]"]').attr('id');
    var CodeAllergyType = $(obj).closest('tr').find('input[name="allergytype_code_hidden[]"]').attr('id');
    var typeVal = $('#'+CodeAllergyType).val();
    if(typeVal == "" || typeVal == undefined){
        Command: toastr["warning"]("Please Select Allergy Type.!");
        return false;
    }
    var ValMed = $('#'+IdMedi).val();
    if(ValMed != "" || ValMed != undefined){
        $('#addAllergy').find('#allergy').val(ValMed);
        $('#addAllergy').find('#src_name').val(CodeMedi);

    }
    vals = $(obj).parent().prev("input[name='allergy[]']").val();
    //alert(vals);
    if(vals != ""){
      SaveNewAllergy(vals,obj,typeVal);
    }
}

function AddNewAllergyType(obj){

    var IdAlerType = $(obj).closest('tr').find('input[name="allergytype[]"]').attr('id');
    var CodeAlerType = $(obj).closest('tr').find('input[name="allergytype_code_hidden[]"]').attr('id');
    var ValAlerType = $('#'+IdAlerType).val();
    if(ValAlerType != "" || ValAlerType != undefined){
        $('#addAllergyType').find('#allergytype').val(ValAlerType);
        $('#addAllergyType').find('#src_name').val(CodeAlerType);

    }
    vals = $(obj).parent().prev("input[name='allergytype[]']").val();
    if(vals != ""){
      SaveNewAllergyType(vals,obj);
    }
}

function SaveNewAllergy(vals,obj,type){
    var url = $('#base_url').val() + "/nursing/allergyAdd";
    $.ajax({
        type: "GET",
        url: url,
        data: 'allergy_name=' + vals+'&allergy_type_id=' + type,
     success: function (html) {
       if(html.status > 0){
        $('.ajaxSearchBox').hide();
       Command: toastr["success"](html.msg);
       $(obj).parent().next("input[name='allergy_code_hidden[]']").val(html.id)
     }


    }
    });
}


function SaveNewAllergyType(vals,obj){
    var url = $('#base_url').val() + "/nursing/allergyTypeAdd";
    $.ajax({
        type: "GET",
        url: url,
        data: 'allergy_type_name=' + vals,
     success: function (html) {
       if(html.status > 0){
        $('.ajaxSearchBox').hide();
       Command: toastr["success"](html.msg);
       $(obj).parent().next("input[name='allergytype_code_hidden[]']").val(html.id)
     }


    }
    });
}


function searchAllergy(id,event,i) {

    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#'+id).next().attr('id');
    var ajaxHiddenId = $('#'+id).next().next().attr('id');
    var data = $('#'+id).val();
    if(data == ""){
        $('#'+ajaxHiddenId).val('');
    }
    if (value.match(keycheck)) {

        var allergy_code = $('#'+id).val();
        allergy_code = allergy_code.replace('/[^\w\s-_\.]/gi', '');
        allergy_code = allergy_code.trim();
        var allergyTypeCodeId = '';
        allergyTypeCodeId = $('#allergyTypeCodeId-'+i).val();
        if(allergyTypeCodeId == undefined || allergyTypeCodeId == ""){
          allergyTypeCodeId = $('#allergyTypeCodeId-e'+i).val();
        }
    if (allergy_code == "") {
        $("#"+ajax_div).html("");
        $("#"+ajaxHiddenId).val("");
    } else {
        if(allergyTypeCodeId != "" && allergyTypeCodeId != undefined){
         var url = $('#base_url').val() + "/nursing/getallergy";
         $.ajax({
            type: "GET",
            url: url,
            data: 'search_key=' +allergy_code+'&allergyTypeId=' +allergyTypeCodeId,
            beforeSend: function () {
                //$("#inbox_area").html("");
                 $('#loading_image').show();
               // $("#"+ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
                //alert(html);
                //return;
                $("#"+ajax_div).html(html).show();
                $("#"+ajax_div).find('li').first().addClass('liHover');
            },
            complete: function () {
                  $('#loading_image').hide();
            }
        });
      }else{
        Command: toastr["warning"]("Please Select Allergy Type.!");
      }
    }

}else {
    ajax_list_key_down(ajax_div,event);
}

    /* setting for enter key press in ajaxDiv listing */
    $(this).on('keydown',function(event){
        if(event.keyCode === 13){
            ajaxlistenter(ajax_div);
            return false;
        }
    });
}


$(document).on('click', "#patient_allergy_add_edit_save", function() {
    // console.log('patient_history_add_edit_save');
     var input_fields = $('#allergyHistoryEditModalContent').find('input, textarea, checkbox').serialize();
    // alert(input_fields);return;
     var patient_id = $('#patient_id').val();
     var encounter_id=$('#encounter_id').val();
     var saveData = false;
      $('input[name="allergytype[]"]').each(function(index, el) {
         cur_allergy_type = $(this).val();
         cur_allergy = $(this).closest('tr').find('input[name="allergy[]"]').val();
         if(cur_allergy_type != '' && cur_allergy != ''){
            saveData = true;
         }
      });
     if(patient_id>0) {
        $.ajax({
           url: '',
           data: 'patient_id='+patient_id+'&encounter_id='+encounter_id+'&update_patient_allergy=1&'+input_fields,
        //data: {patient_id: patient_id, 'update_patient_history':1, input_fields: input_fields},
        type: "GET",
        cache: true,
        //async: false,
        beforeSend: function() {
            //$('#patient_allergy_add_edit_save_spin').removeClass('fa-save').addClass('fa-spinner fa-spin');
        },
        success: function (html) {
        //$('#allergyHistoryEditModal').modal ('toggle');
        //bootbox.alert("Patient Allergy Updated Successfully.");
            if(saveData == true){
                Command: toastr["success"]('Patient Allergy Updated Successfully.');
            }
            loadAllergyInfo();
            fetch_all_allergy(patient_id);
        },
        error: function (e) {

        },
        complete: function (e) {
        //$('#patient_allergy_add_edit_save_spin').addClass('fa-save').removeClass('fa-spinner fa-spin');
        }
        });
   }

});

function saveMedicationAllergy(){

    $(".saveMedicationAllergy").attr('disabled', true);

    var values_allergy = $("input[name='alergylist_medicine_code_hidden[]']")
          .map(function(){
            var tt=$(this).val();
        if(tt!='')
        {
        return $(this).val();
        }
        }).get();
        var allergy_ct = values_allergy.length;


    if(allergy_ct>0 || ($.trim($(".otherAllergies").val()) != '')){

        var url = $('#base_url').val()+"/emr/saveallergyprescription";
        var patientId = $('#patient_id').val();

        var token = $('#formallergyprescription').find('input[name="_token"]').val();
        var dataparams = $('#formallergyprescription').serialize();
        var otherAllergies = $(".otherAllergies").val();
        otherAllergies = btoa(otherAllergies);
    // alert(dataparams);return;
        $.ajax({
            url: url,
            data: '_token='+ token + '&MedicineData='+ dataparams + '&patientId=' +patientId + '&otherAllergies=' + otherAllergies,
            async: true,
            type: 'post',
            //dataType: "json",
            beforeSend: function (){

            },
            success: function (html) {

                if(html==1){
                    Command: toastr["success"]("Saved Successfully");
                    // loadPrescription ();
                    fetch_all_allergy(patientId);
                    fetchpatientMedicineAllerg();
                    $('#modal_allergy').modal('hide');

                } else{
                    Command: toastr["error"]("Something went wrong");
                }

                $(".saveMedicationAllergy").attr('disabled', false);
            },
            error: function(){
                $(".saveMedicationAllergy").attr('disabled', false);
            }

        });
    }else{
        Command: toastr["error"]("Fill Medicine For Allergies ");
        $(".saveMedicationAllergy").attr('disabled', false);
    }
}
