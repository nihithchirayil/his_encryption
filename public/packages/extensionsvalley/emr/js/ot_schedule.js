$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({
        placeholder: "",
        maximumSelectionSize: 6
    });

    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });

    searchOTSchedule();
});

var base_url = $("#base_url").val();
var token = $("#c_token").val();

function searchOTSchedule() {
    var url = base_url + "/ot_schedule/searchOTSchedule";
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var surgeon_name = $("#surgeon_name").val();
    var surgery_name = $("#surgery_name").val();

    $.ajax({
        url: url,
        type: "POST",
        data: {
            _token: token,
            from_date: from_date,
            to_date: to_date,
            surgeon_name: surgeon_name,
            surgery_name: surgery_name
        },
        beforeSend: function () {
            $('#searchOtSchedule').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            $('#searchOTScheduleBtn').attr('disabled', true);
            $('#searchOTScheduleSpin').removeClass('fa fa-search');
            $('#searchOTScheduleSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchOtSchedule').html(data);
        },
        complete: function () {
            $('#searchOtSchedule').LoadingOverlay("hide");
            $('#searchOTScheduleBtn').attr('disabled', false);
            $('#searchOTScheduleSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchOTScheduleSpin').addClass('fa fa-search');

            $('[data-toggle="popover"]').popover({
                html: true,
                animation: false,
                placement: 'auto right',

                // trigger:'focus'
            });

            $("[data-toggle='popover']").on('shown.bs.popover', function(){
                $('.timepicker').datetimepicker({
                    format: 'hh:mm A'
                });
            });

        },
    });

    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
}

function getScheduleDataOnDateChange() {
    var request_id = $('#request_id').val();
    var surgery_id = $('#surgery_id').val();
    var surgery_date = $('#surgery_date').val();
    var url = base_url + "/ot_schedule/scheduleSurgery";
    var dataparams = {
        id: request_id,
        surgery_id: surgery_id,
        surgery_date: surgery_date,
    };
    $.ajax({
        url: url,
        type: "GET",
        data: dataparams,
        beforeSend: function () {
            // $('#time_allocation_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },

        success: function (data) {
            $('#surgery_schedule_modal').modal('show');
            $('#surgery_schedule_modal_body').html(data);
            $('.select2').select2();
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });
            makeTableDraggable();


            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();

                var schedules = $("#schedule_id_hidden_val").val();
                schedules = schedules.split(",");
                $.each(schedules, function (key, val) {
                    console.log(val)
                    $('.' + val).each(function (key1, val1) {
                        console.log(val1)
                        if (key1 == 0) {
                            $(val1).attr('rowspan', $('.' + val).length);
                        } else {
                            $(val1).remove();
                        }
                    })
                });
            }, 200);
            resetTable();
        },
        complete: function () {
            //   $('#time_allocation_container').LoadingOverlay("hide");
        }

    });
}

function schedule_surgery(head_id, surgery_date = null, surgery_id = 0) {
    var url = base_url + "/ot_schedule/scheduleSurgery";
    $.ajax({
        url: url,
        type: "GET",
        data: {
            id: head_id,
            surgery_date: surgery_date
        },
        beforeSend: function () {
            $('#surgery_schedule_modal_body').html('');
            $('#schedule_surgerybtn' + head_id + surgery_id).attr('disabled', true);
            $('#schedule_surgeryspin' + head_id + surgery_id).removeClass('fa fa-clock-o');
            $('#schedule_surgeryspin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');
            $('#time_allocation_container').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#surgery_schedule_modal').modal('show');
            $('#surgery_schedule_modal_body').html(data);
            $('.select2').select2();
            $('#surgeon').select2();
            $('#anesthetist').select2();
            $('#anesthesia').select2();

            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });
            makeTableDraggable();
            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();
                var schedules = $("#schedule_id_hidden_val").val();
                if (schedules) {
                    schedules = schedules.split(",");
                    $.each(schedules, function (key, val) {
                        if (val && val != 'undefined') {
                            $('.' + val).each(function (key1, val1) {
                                console.log(val1)
                                if (key1 == 0) {
                                    $(val1).attr('rowspan', $('.' + val).length);
                                } else {
                                    $(val1).remove();
                                }
                            })
                        }
                    });
                }
            }, 200);

            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');

            $('#time_allocation_container').LoadingOverlay("hide");
            //getScheduleDataOnDateChange();

        },
        complete: function () {
            $('#schedule_surgerybtn' + head_id + surgery_id).attr('disabled', false);
            $('#schedule_surgeryspin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#schedule_surgeryspin' + head_id + surgery_id).addClass('fa fa-clock-o');
        },
    });
}

$('#schedule_surgery_date').on('change', function () {
    listOtScheduleOnDateChange();
});

function listOtScheduleOnDateChange() {
    var surgery_date = $('#schedule_surgery_date').val();
    var url = base_url + "/ot_schedule/listOtScheduleOnDateChange";
    $.ajax({
        url: url,
        type: "GET",
        data: {
            surgery_date: surgery_date
        },
        beforeSend: function () {
            $('#time_allocation_container').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#time_allocation_container').html(data);
            $('#time_allocation_container').LoadingOverlay("hide");
            makeTableDraggable();
        },
        complete: function () {
            $('#time_allocation_container').LoadingOverlay("hide");
            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();

                var schedules = $("#schedule_id_hidden_val").val();
                schedules = schedules.split(",");
                $.each(schedules, function (key, val) {
                    console.log(val);
                    if (val && val != 'undefined') {
                        $('.' + val).each(function (key1, val1) {
                            console.log(val1)
                            if (key1 == 0) {
                                $(val1).attr('rowspan', $('.' + val).length);
                            } else {
                                $(val1).remove();
                            }
                        });
                    }
                });

            }, 200);

            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');


        },
    });
}

function makeTableDraggable() {
    var isMouseDown = false;
    $("#shedule_time_table td")
        .mousedown(function () {
            if ($(this).hasClass("highlighted")) {
                $('.highlighted').removeClass('highlighted');
            }
            isMouseDown = true;
            if ($(this).hasClass("time_list")) {
                return false;
            }
            $(this).toggleClass("highlighted");
            return false;
        })
        .mouseover(function () {
            if (isMouseDown) {
                if ($(this).hasClass("time_list")) {
                    return false;
                }
                $(this).toggleClass("highlighted");
            }
        });

    $(document)
        .mouseup(function () {
            isMouseDown = false;
        });
}



function SaveOTschedule(approval_status) {
    var ot_id = $('.highlighted').attr('data-ot');
    var start_time = '';
    var stop_time = '';
    var i = 0;
    var request_id = $('#request_id').val();
    var nursing_station = $('#schedule_nursing_station').val();
    var surgery_id = $('#schedule_surgery_id').val();
    var anesthesia_id = $('#schedule_anesthesia').val();
    var surgeon_id = $('#schedule_surgeon').val();
    var anesthetist_id = $('#schedule_anesthetist').val();
    var diagnosis_id = $('#schedule_diagnosis').val();
    var surgery_date = $('#schedule_surgery_date').val();
    var j = 0;
    var current_column = ot_id;
    var is_multiple_ot_selected = 0;
    $('.highlighted').each(function () {
        if (j == 0) {
            current_column = $(this).attr('data-ot');
        }
        if ($(this).attr('data-ot') != current_column) {
            is_multiple_ot_selected = 1;
        }
        j++;
    });

    if (is_multiple_ot_selected == 1) {
        toastr.warning("Aviod multiple OT selection!");
        return false;
    }

    $('.highlighted').each(function () {
        if (i == 0) {
            start_time = $(this).attr('data-attr-time');
        }
        stop_time = $(this).attr('data-attr-time');
        i++;
    });
    if (start_time == '' || stop_time == '') {
        toastr.warning("Please schedule time and theater !");
        return false;
    }
    start_time = start_time.split('-')[0];
    stop_time = stop_time.split('-')[1];
    var url = base_url + "/ot_schedule/saveOTschedule";
    var dataparams = {
        ot_id: ot_id,
        start_time: start_time,
        nursing_station: nursing_station,
        stop_time: stop_time,
        request_id: request_id,
        surgery_date: surgery_date,
        surgery_id: surgery_id,
        anesthesia_id: anesthesia_id,
        surgeon_id: surgeon_id,
        anesthetist_id: anesthetist_id,
        diagnosis_id: diagnosis_id,
        approval_status: approval_status,
    };
    $.ajax({
        url: url,
        type: "GET",
        data: dataparams,
        beforeSend: function () {
            $('#time_allocation_container').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data != 0) {
                var obj = JSON.parse(data);
                var i = 0;
                $('.highlighted').addClass('scheduled');
                var shedule_length = $('.scheduled').length;
                $('.scheduled').each(function () {
                    if (i == 0) {

                        $(this).html(obj.html);
                        $(this).attr('data-toggle', 'tooltip');
                        $(this).attr('data-placement', 'right');
                        $(this).attr('data-html', 'true');
                        $(this).attr('data-original-title', obj.tooltip);

                        $(this).attr('rowspan', shedule_length);
                        $(this).removeClass('scheduled');
                        $(this).addClass('scheduled_fixed');
                    }
                    i++
                });
                $('.scheduled').remove();
                resetTable();
                $('[data-toggle="tooltip"]').tooltip();
                searchOTSchedule();
            } else {
                toastr.error("Something went wrong..!");
            }

        },
        complete: function () {
            $('#time_allocation_container').LoadingOverlay("hide");
            // $('#surgery_schedule_modal').modal('hide');
        }
    });
}

function resetTable() {
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
}


function mergeCell() {
    var rowSpan = 1;
    $('#shedule_time_table tbody tr').each(function (e) {
        var oldTDLength = $(this).find('td').length;
        for (xTD = 0; xTD < oldTDLength; xTD++) {
            crrTD = $(this).find('td:eq(' + xTD + ')');
            crrTDNext = $(this).find('td:eq(' + xTD + ')').next();
            var colSpan = 1;

            while (((crrTD.hasClass('highlighted')) && (crrTD.hasClass('highlighted') == crrTDNext.hasClass('highlighted')))) {
                var tempCell = crrTDNext; ///store old cell in temp
                crrTDNext = crrTDNext.next(); ///get next
                tempCell.remove(); ///remove next highlited cell
                colSpan++;
                xTD++; //to skip merged cell
            }
            if (colSpan > 1) crrTD.attr('colSpan', colSpan);
        } //td loop
    }); //tr loop
}


$("#surgery_date").change(getScheduleDataOnDateChange());

function CreateNewOTSchedule() {
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var base_url = $("#base_url").val();
    var url = base_url + "/surgery_request/surgeryRequest";
    $.ajax({
        url: url,
        type: "GET",
        data: '',
        beforeSend: function () {
            $('#surgery_request_modal').modal('show');
            $('#surgery_request_modal_body').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#surgery_request_modal_body').html(data);
        },
        complete: function () {
            $('#surgery_request_modal_body').LoadingOverlay("hide");
            // $('.select2').select2().on("change", function (e) {
            //     select_instruments($(this).val(),$(this).attr('id'));
            // });

            $('#surgeon').select2();
            $('#anesthetist').select2();

            $('#primary_surgery_id').select2({
                width: '100%',
                placeholder: '',
                language: {
                    noResults: function () {
                        return '<button class="btn btn-primary btn-sm" id="add_new_surgery_btn" onclick="AddNewSurgeryMaster()">+Add</a>';
                    },
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
            });

            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });


        },
    });

}

function AddNewSurgeryMaster() {
    var new_surgery = $('.primary_surgery_search_container').find('.select2-search__field').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/surgery_request/AddNewSurgeryMaster";
    if (new_surgery == '') {
        return false;
    }
    $.ajax({
        url: url,
        type: "GET",
        data: {
            new_surgery: new_surgery
        },
        beforeSend: function () {
            $('#primary_surgery_id').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data != 0) {
                var newOption = new Option(new_surgery, data, true, true);
                $('#primary_surgery_id').append(newOption).trigger('change');
                $('.primary_surgery_search_container').find('.select2-search__field').val('');

            }
        },
        complete: function () {
            $('#primary_surgery_id').LoadingOverlay("hide");
        },
    });

}

function select_instruments(value, id) {
    var base_url = $("#base_url").val();
    var value1 = '';
    var value2 = '';
    var url = base_url + "/surgery_request/select_instruments";
    if (id == 'primary_surgery_id') {
        value1 = value.split(",");
    }

    if (id == 'surgery_id') {
        value2 = $('#primary_surgery_id').val();
        value2 = value2.split(",");
        value = value + ',' + value2;
    }
    value = value.split(",");

    $.ajax({
        url: url,
        type: "GET",
        data: {
            value: value
        },
        beforeSend: function () {
            $('#instrument_list').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data != 0) {
                $('#instrument_list').html(data);
            } else {
                $('#instrument_list').html('');
            }
        },
        complete: function () {
            $('#instrument_list').LoadingOverlay("hide");
        },
    });

}

$(document).on("click", "#item_search_btn", function (e) {
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
});

function searchbyName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("result_data_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


//-----------global patient search---------------------------------
$(document).on("keyup", "#patient_uhid", function (e) {
    // $('#patient_uhid').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if ($('#ot_patient_id_hidden').val() != "") {
            $('#ot_patient_id_hidden').val('');
            $("#patient_uhidAjaxDiv").html("").hide();
        }
        var patient_name_search = $(this).val();
        if (patient_name_search == "") {
            $("#patient_uhidAjaxDiv").html("").hide();
        } else {
            var base_url = $("#base_url").val();
            var url = base_url + "/nursing_new/patientNameSearchOt";
            // console.log(url);
            $.ajax({
                type: "GET",
                url: url,
                data: "patient_name_search_ot=" + patient_name_search,
                beforeSend: function () {
                    $("#patient_uhidAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#patient_uhidAjaxDiv").html(html).show();
                    $("#patient_uhidAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }
    } else {
        ajax_list_key_down('patient_uhidAjaxDiv', event);
    }
});

$(document).on("keydown", "#patient_uhid", function (e) {
    if (event.keyCode == 13) {
        ajaxlistenter('patient_uhidAjaxDiv');
        return false;
    }
});

function fillOtPatientValues(id, patient_name, current_visit_id) {
    $('#patient_uhid').val(patient_name);
    $('#ot_patient_id_hidden').val(id);
    $('#patient_current_visit_id_hidden').val(current_visit_id);
    $('#patient_uhidAjaxDiv').hide();
}



function NewSurgeryRequest() {
    var patient_id = $('#ot_patient_id_hidden').val();
    var surgery_date = $('#surgery_date').val();
    var anaesthesia_id = $('#anesthesia').val();
    var nursing_station = $('#nursing_station').val();
    var surgeon = $('#surgeon').val();
    var anaesthetist = $('#anesthetist').val();
    var diaganosis = $('#diagnosis').val();
    var primary_surgery_id = $('#primary_surgery_id').val();
    var base_url = $("#base_url").val();
    var token = $('#c_token').val();
    var surgery_time = $('#surgery_time').val();
    var url = base_url + "/surgery_request/addSurgeryRequest";
    var instrument_id = '';
    var instrument_id_array = [];

    if (patient_id == '') {
        toastr.warning('Please select patient!');
        $('#ot_patient_id_hidden').focus();
        return false;
    }

    if (primary_surgery_id == null) {
        toastr.warning('Please select a surgery');
        $('#primary_surgery_id').focus();
        return false;
    }

    $('.instruments').each(function () {
        instrument_id = $(this).val();
        if (instrument_id != '') {
            instrument_id_array.push(instrument_id);
        }
    });

    if ($('.primary_surgery_search_container').find('.select2-search__field').val() != '') {
        toastr.warning('Add surgery to master list!');
        return false;
    }

    var dataparams = {
        _token: token,
        patient_id: patient_id,
        surgery_date: surgery_date,
        anaesthesia_id: anaesthesia_id,
        surgeon: surgeon,
        anaesthetist: anaesthetist,
        diaganosis: diaganosis,
        instrument_id_array: instrument_id_array,
        primary_surgery_id: primary_surgery_id,
        surgery_time: surgery_time,
        nursing_station: nursing_station,
    };
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                toastr.success("Saved Successfully!!!");
                $('#instrument_list').html('');
                $('#reset_surgery_request').trigger('click');
                $('.select2').select2();
                $('#surgery_request_modal').modal('hide');
            } else {
                toastr.error("Error Please Check Your Internet Connection");
            }

            searchOTSchedule();
        },
    });

}

function deleteOtSchedule(shedule_id) {
    var url = base_url + "/ot_schedule/deleteOtSchedule";
    var dataparams = {
        shedule_id: shedule_id
    };
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                listOtScheduleOnDateChange();
            }
        },
    });
}

function deleteOtRequest(shedule_id) {
    var url = base_url + "/ot_schedule/deleteOtRequest";
    var dataparams = {
        shedule_id: shedule_id
    };
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                searchOTSchedule();
            }
        },
    });
}

function postponeOtRequest(request_id, surgery_date) {
    $('#postpone_modal').modal('show');
    $('#postpone_request_id').val(request_id);
    $('#postponed_at').val(surgery_date);
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
}

function ConfirmPostponeSurgery() {
    bootbox.confirm({
        message: 'Are you sure you want to pospone surgery',
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            savePostponeSurgery();
        }
    });
}

function savePostponeSurgery() {
    var postponed_at = $('#postponed_at').val();
    var postpone_request_id = $('#postpone_request_id').val();
    var postpone_remarks = $('#postpone_remarks').val();
    postpone_remarks = btoa(postpone_remarks);
    if (postpone_remarks == '') {
        toastr.warning("Please enter remarks");
        $('#postpone_remarks').focus();
        return false;
    }
    if (postponed_at == '') {
        toastr.warning("Please enter New schedule date");
        $('#postponed_at').focus();
        return false;
    }
    if (postpone_request_id == '') {
        toastr.warning("Select Surgery");
        return false;
    }

    var url = base_url + "/ot_schedule/savePostponeSurgery";
    var dataparams = {
        postponed_at: postponed_at,
        postpone_remarks: postpone_remarks,
        postpone_request_id: postpone_request_id
    };
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#postpone_modal_body').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#postpone_modal_body').LoadingOverlay("hide");
            if (data == 1) {
                $('#postpone_modal').modal('hide');
                $('#postpone_remarks').val('');
                $('#postpone_request_id').val('');
                $('#postponed_at').val('');
                searchOTSchedule();
            } else {
                toastr.error("Someting went wrong");
            }
        },
    });
}



function AnaesthetiaRecord(head_id, surgery_detail_id, tab_status) {
    var url = base_url + "/ot_checklists/AnaesthetiaRecord";
    $('#head_id').val(head_id);
    $('#surgery_detail_id_hidden').val(surgery_detail_id);
    $('#list_type').val(2);
    $.ajax({
        url: url,
        type: "POST",
        data: {
            head_id: head_id,
            surgery_detail_id: surgery_detail_id
        },
        beforeSend: function () {
            if (tab_status == 0) {
                $("#anesthetia_record_checklist_modal").modal('show');
            } else {
                $('#anaesthesia_checklist').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            }
        },
        success: function (data) {
            if (tab_status == 0) {
                $('#anesthetia_record_checklist').html(data);
            } else {
                $('#anaesthesia_checklist').LoadingOverlay("hide");
                $('#anaesthesia_checklist').html(data);
            }
            $('.modal-body').css('min-height', '520px');

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

            fetchArRecord(head_id);
        },
        complete: function () {

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        },
    });

}

function saveSurgeryTimeTrack(surgery_id){
    var start_time = $('#actual_start_time_'+surgery_id).val();
    var end_time = $('#actual_end_time_'+surgery_id).val();
    var dataparams = {
        surgery_id:surgery_id,
        start_time:start_time,
        end_time:end_time,
    };
    var url = base_url + "/ot_schedule/saveSurgeryTimeTrack";

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#surgey_track_table_'+surgery_id).LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#surgey_track_table_'+surgery_id).LoadingOverlay("hide");
            if (data == 1) {
                toastr.success("Saved Successfully!");
            }else{
                toastr.error("Failed to save!");
            }
        },
    });
}
