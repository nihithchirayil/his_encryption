$(document).ready(function () {

    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

});





function getReportData() {
    var url = route_json.setCounterWiseRevenueReport;
  
        var bill_from = $('#bill_fromdate').val();
        var bill_to = $('#bill_todate').val();
        var counter = $('#counter').val();
        var doctor = $('#doctor').val();
        var payment_mode = $('#payment_mode').val();
        var bill_tag = $('#bill_tag').val();
        var param = { counter: counter, bill_from: bill_from, bill_to: bill_to, payment_mode: payment_mode, doctor: doctor, bill_tag: bill_tag };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#search_results').attr('disabled', true);
                $('#ResultDataContainer').css('display', 'block');
                $('#serachResultSpin').removeClass('fa fa-search');
                $('#serachResultSpin').addClass('fa fa-spinner fa-spin');
                $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (html) {
                $('#ResultsViewArea').html(html);
                $('#print_results').removeClass('disabled');
                $('#csv_results').removeClass('disabled');
            },
            complete: function () {
                $('#search_results').attr('disabled', false);
                $('#serachResultSpin').removeClass('fa fa-spinner fa-spin');
                $('#serachResultSpin').addClass('fa fa-search');
                $('#ResultsViewArea').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
   

}


function getResultDetails(service_id, service_desc) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    var param = { lab_results_id: service_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $("#servicedatatestbtn" + service_id).attr('disabled', true);
            $("#servicedatatestspin" + service_id).removeClass('fa fa-list');
            $("#servicedatatestspin" + service_id).addClass('fa fa-spinner fa-spin');
            $("#service_dataheader").html(service_desc);
        },
        success: function (data) {
            $('#service_datadiv').html(data);
            $("#service_datamodel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $("#servicedatatestbtn" + service_id).attr('disabled', false);
            $("#servicedatatestspin" + service_id).removeClass('fa fa-spinner fa-spin');
            $("#servicedatatestspin" + service_id).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}




function search_clear() {

    var current_date = $('#current_date').val();

    $('#bill_fromdate').val(current_date);
    $('#bill_todate').val(current_date);
   $('#counter').val('').select2();
   $('#bill_tag').val('').select2();
   getReportData();

}
