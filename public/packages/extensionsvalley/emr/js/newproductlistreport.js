$(document).ready(function() {
    setTimeout(function() {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");

    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});
document.getElementById("date_from").blur();
document.getElementById("date_to").blur();



//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dischargesummaryreport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

 if(category) {


        $.ajax({
                    type: "GET",
                    url: '',
                    data: 'category=' + category,
                    beforeSend: function () {

                        $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#sub_category" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#sub_category").empty();

                                        $("#sub_category").html('<option value="">Select Sub catogery</option>');
                                        $.each(html,function(key,value){
                                          $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#sub_category").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                        $( "#sub_category" ).prop( "disabled", false ); //Disable
                    },
                    complete: function () {
                        $( "#sub_category" ).prop( "disabled", false ); //Disable
                        $('#warning1').remove();

                    }

                });

        }
    else {
                $('#category').focus();
                $("#sub_category").empty();
            }

    });

function getReportData() {
    var url = route_json.Newproductlistdata;
  
   var date_from=$('#date_from').val();
   var date_to=$('#date_to').val();
   var generic_name=$('#generic_name').val();
   var category=$('#category').val();
   var sub_category=$('#sub_category').val();
 

   var parm={date_from:date_from,date_to:date_to,generic_name:generic_name,category:category,
    sub_category:sub_category};

    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
         // alert(html);

        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            var is_print =$("#is_print").val();

            if(is_print==0 || is_print==undefined){
                   $("#print_results").addClass("disabled");
               }
           var is_excel =$("#is_excel").val();

           if(is_excel==0 || is_excel==undefined){
                   $("#csv_results").addClass("disabled");
                }
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}

function search_clear() {
 var current_date = $('#current_date').val();
    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
}
