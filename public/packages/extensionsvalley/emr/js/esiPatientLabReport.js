$(document).ready(function () {

    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#patient_id_hidden').val() != "") {
                $('#patient_id_hidden').val('');
            }
            var patient_name = $(this).val();
            patient_name = patient_name.trim();
            if (patient_name == "") {
                $("#patient_idAjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "patient_name_search=" + patient_name,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        //  alert(html); return;
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });
});

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function fillPatientValues(uhid, patient_name, from_type = 0) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
}

/* For BillNo Search, filling values */
function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
    $('#bill_no').val(bill_no);
    $('#patient_uhid_hidden').val(uhid);
    $('#bill_id_hidden').val(bill_id);
    $('#bill_no_AjaxDiv').hide();
    if (from_type != 1) {
        fillPatientValues(uhid, patient_name, 1);
    }
}

function getReportData() {
    var url = route_json.getesipatientbillreport;
    var patient_name = $('#patient_name').val();
    var cash_bills = $('#cash_bills').is(":checked")
    if (patient_name) {
        var bill_from = $('#bill_fromdate').val();
        var bill_to = $('#bill_todate').val();
        var bill_id = $('#bill_id').val();
        var uhid = $('#patient_uhid_hidden').val();
        var visit_status = $('#visit_status').val();
        var param = { cash_bills: cash_bills, bill_from: bill_from, bill_to: bill_to, bill_id: bill_id, uhid: uhid, visit_status: visit_status };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#search_results').attr('disabled', true);
                $('#ResultDataContainer').css('display', 'block');
                $('#serachResultSpin').removeClass('fa fa-search');
                $('#serachResultSpin').addClass('fa fa-spinner');
                $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (html) {
                $('#ResultsViewArea').html(html);
                $('#print_results').removeClass('disabled');
                $('#csv_results').removeClass('disabled');
            },
            complete: function () {
                $('#search_results').attr('disabled', false);
                $('#serachResultSpin').removeClass('fa fa-spinner');
                $('#serachResultSpin').addClass('fa fa-search');
                $('#ResultsViewArea').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    } else {
        toastr.warning("Please Select Any Patient");
    }

}


function getResultDetails(service_id, service_desc) {
    var url = $('#base_url').val() + "/nursing/getLabResultRtf";
    var param = { lab_results_id: service_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $("#servicedatatestbtn" + service_id).attr('disabled', true);
            $("#servicedatatestspin" + service_id).removeClass('fa fa-list');
            $("#servicedatatestspin" + service_id).addClass('fa fa-spinner fa-spin');
            $("#service_dataheader").html(service_desc);
        },
        success: function (data) {
            $('#service_datadiv').html(data);
            $("#service_datamodel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $("#servicedatatestbtn" + service_id).attr('disabled', false);
            $("#servicedatatestspin" + service_id).removeClass('fa fa-spinner fa-spin');
            $("#servicedatatestspin" + service_id).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");
};

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}


function print_generate(printData) {
    //$('#print_config_modal').modal('hide');


    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_headder").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:0;margin-bottom:30px;margin-top:30px;margin-left:30px;margin-right:30px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:0;margin-bottom:30px;margin-top:30px;margin-left:30px;margin-right:30px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.table.no-border, .table.no-border td, .table.no-border th { border: 0; } .table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } .table_sm td span { line-height: 18px !important; } .table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } .table_head.tth { font-size: 12px !important; padding: 1px !important; } .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } .table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, .table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, .table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } .headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} th { background-color:#d9dfe0 !important;}.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; }</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


function search_clear() {

    var current_date = $('#current_date').val();

    $('#bill_fromdate').val(current_date);
    $('#bill_todate').val(current_date);
    $('.hidden_search').val('');
    $('.hidden').val('');

}
