
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }

        });

        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }
        });

    });
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_service_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});
$('#restrict_duration').click( function() {
    if($(".restrict_duration").prop('checked') == true){
        $('#duration').prop("disabled", false).addClass('enabled');
        $('#h').prop("disabled", false).addClass('enabled');
        $('#hourly').prop("disabled", false);
        $('#d').prop("disabled", false).addClass('enabled');
        $('#daily').prop("disabled", false);
        $('#w').prop("disabled", false).addClass('enabled');
        $('#warning').prop("disabled", false);
        $('#b').prop("disabled", false).addClass('enabled');
        $('#block').prop("disabled", false);
        $('#count').prop("disabled", false).addClass('enabled');
        $('#restrict_duration_count').prop("disabled", false);
    }else{
        $('#duration').prop("disabled", true).addClass('disabled');
        $('#h').attr("disabled", true).addClass('disabled');
        $('#hourly').prop("disabled", true);
        $('#d').prop("disabled", true).addClass('disabled');
        $('#daily').prop("disabled", true);
        $('#w').prop("disabled", true).addClass('disabled');
        $('#warning').prop("disabled", true);
        $('#b').prop("disabled", true).addClass('disabled');
        $('#block').prop("disabled", true);
        $('#count').prop("disabled", true).addClass('disabled');
        $('#restrict_duration_count').prop("disabled", true);
    }
   });
    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
        data.service_name = $("#service_name").val();
        data.department_id = $('#search_department').val();
        data.sub_department_id = $('#search_sub_department').val();
        data.service_code = $('#service_code').val();

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {

            }
        });

    }
    $('#hourly').click(function() {
        $('#daily').not('#hourly').removeAttr('checked');
      });
       $('#daily').click(function() {
        $('#hourly').not('#daily').removeAttr('checked');
      });
      $('#warning').click(function() {
        $('#block').not('#warning').removeAttr('checked');
      });
       $('#block').click(function() {
        $('#warning').not('#block').removeAttr('checked');
      });
      $('#duration').prop("disabled", true).addClass('disabled');
      $('#h').prop("disabled", true).addClass('disabled');
      $('#hourly').prop("disabled", true);
      $('#d').prop("disabled", true).addClass('disabled');
      $('#daily').prop("disabled", true);
      $('#w').prop("disabled", true).addClass('disabled');
      $('#warning').prop("disabled", true);
      $('#b').prop("disabled", true).addClass('disabled');
      $('#block').prop("disabled", true);
      $('#count').prop("disabled", true).addClass('disabled');
      $('#restrict_duration_count').prop("disabled", true);
    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".service_name_add").val() == ''){
            Command: toastr["error"]("Please enter Service Name");
            return;
        }

        if($(".service_code_add").val() == ''){
            Command: toastr["error"]("Please enter Service Code");
            return;
        }

        if($("#department_add").val()== ''){
            Command: toastr["error"]("Please select Department");
            return;
        }
        if($("#sub_department_add").val() == ''){
            Command: toastr["error"]("Please select Sub Department");
            return;
        }
        $('#add_service_area').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});

        data.service_id = $(".reference_id").val();

        var checkExists = checkServiceExist($(".service_name_add").val(), $(".service_code_add").val());
        if(data.service_id == ''){
            if(checkExists == 0){
                Command: toastr["error"]('Service Code already exists');
                $('#add_service_area').LoadingOverlay("hide");
                return false;
            }else if(checkExists == 1){
                Command: toastr["error"]('Service Name already exists');
                $('#add_service_area').LoadingOverlay("hide");
                return false;
            }
        }

        data.service_name = $(".service_name_add").val();
        data.service_code = $(".service_code_add").val();
        data.department = $("#department_add").val();
        data.subdepartment = $("#subdepartment_add").val();
        data.is_billable = $(".is_billable").val();
        data.is_refundable = $(".is_refundable").val();
        data.is_discountable = $(".is_discountable").val();
        data.is_price_editable = $(".is_price_editable").val();
        data.is_hosp_service = $(".is_hosp_service").val();
        data.is_doc_required = $(".is_doc_required").val();
        data.is_nursing_service = $(".is_nursing_service").val();
        data.applicable_gender = $(".applicable_gender_add").val();
        data.restrict_duration = $(".restrict_duration").val();
        data.duration_mode= $('input[name="duration_mode"]:checked').val();
        data.duration_type= $('input[name="duration_type"]:checked').val();
        data.status = $(".status_add").val();
        data.restrict_duration_count = $(".restrict_duration_count").val();
        data.is_lab_item = $(".is_lab_item").val();
        data.is_procedure = $(".is_procedure").val();
        data.is_discharge_item = $(".is_discharge_item").val();
        data.esi_name = $(".esi_name_add").val();
        data.esi_code = $(".esi_code_add").val();
        data.nabh_price = $(".nabh_price_add").val();
        data.room_type_name = $("#room_type_name").val();
        data.effective_date = $(".effective_date").val();
        data.price = $(".price").val();
        data.emergency = $(".emergency").val();
        data.status_name = $(".status_name").val();
        data.service_charge_id_hidden = $("#service_charge_id_hidden").val();
        data.sac_code = $(".sac_code_add").val();


        if($(".status_add").prop('checked') == true){
            // alert("chcked");
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".status_name").prop('checked') == true){
            // alert("chcked");
            data.status_name =1;
        }else{
            data.status_name =0;
        }
        if($(".is_lab_item").prop('checked') == true){
            // alert("chcked");
            data.is_lab_item =1;
        }else{
            data.is_lab_item =0;
        }
        if($(".is_procedure").prop('checked') == true){
            // alert("chcked");
            data.is_procedure =1;
        }else{
            data.is_procedure =0;
        }
        if($(".is_discharge_item").prop('checked') == true){
            // alert("chcked");
            data.is_discharge_item =1;
        }else{
            data.is_discharge_item =0;
        }
        if($(".restrict_duration").prop('checked') == true){
            // alert("chcked");
            data.restrict_duration =1;

        }else{
            data.restrict_duration =0;
            $("#id").attr("disabled", false);

        }
        if($(".is_billable").prop('checked') == true){
            // alert("chcked");
            data.is_billable ='t';
        }else{
            data.is_billable ='f';
        }
        if($(".is_refundable").prop('checked') == true){
            // alert("chcked");
            data.is_refundable ='t';
        }else{
            data.is_refundable ='f';
        }
        if($(".is_discountable").prop('checked') == true){
            // alert("chcked");
            data.is_discountable ='t';
        }else{
            data.is_discountable ='f';
        }
        if($(".is_price_editable").prop('checked') == true){
            // alert("chcked");
            data.is_price_editable ='t';
        }else{
            data.is_price_editable ='f';
        }
        if($(".is_hosp_service").prop('checked') == true){
            // alert("chcked");
            data.is_hosp_service ='t';
        }else{
            data.is_hosp_service ='f';
        }
        if($(".is_doc_required").prop('checked') == true){
            // alert("chcked");
            data.is_doc_required ='t';
        }else{
            data.is_doc_required ='f';
        }
        if($(".is_nursing_service").prop('checked') == true){
            // alert("chcked");
            data.is_nursing_service ='t';
        }else{
            data.is_nursing_service ='f';
        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                // $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                // $('.saveButton').attr('disabled', true);

            },
            success: function (data) {
                if(data==1){
                    Command: toastr["error"]("Service Code Already Exit!");
                    // $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    // $('.saveButton').attr('disabled', false);

                    }else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    $('#duration').prop("disabled", true).addClass('disabled');
                    $('#h').prop("disabled", true).addClass('disabled');
                    $('#hourly').prop("disabled", true);
                    $('#d').prop("disabled", true).addClass('disabled');
                    $('#daily').prop("disabled", true);
                    $('#w').prop("disabled", true).addClass('disabled');
                    $('#warning').prop("disabled", true);
                    $('#b').prop("disabled", true).addClass('disabled');
                    $('#block').prop("disabled", true);
                    $('#count').prop("disabled", true).addClass('disabled');
                    $('#restrict_duration_count').prop("disabled", true);
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {
                $('#add_service_area').LoadingOverlay("hide");
            }
        });
    }

    function checkServiceExist(service_name,service_code){
        var base_url = $('#base_url').val();
        var token = $('#token').val();
        var url = base_url + "/master/checkServiceExist";
        service_name = btoa(service_name);
        var returndata = '';
        var dataparams = {
            'token': token,
            'service_desc': service_name,
            'service_code': service_code,
        };
        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: dataparams,
            beforeSend: function () {
            },
            success: function (data) {
                returndata = data;
            }
        });
        return returndata;
    }

    function editItem(element,service_charge_id){
        // $('#restrict_duration').click( function() {

        //    });
        let service_id = $(element).parents('tr').attr('data-id');
        let service_name = $(element).parents('tr').attr('data-service_desc');
        let service_code = $(element).parents('tr').attr('data-service_code');
        let department = $(element).parents('tr').attr('data-department_id');
        let subdepartment = $(element).parents('tr').attr('data-subdepartment_id');
        let is_billable = $(element).parents('tr').attr('data-billable');
        let is_refundable = $(element).parents('tr').attr('data-refundable');
        let is_discountable = $(element).parents('tr').attr('data-discountable');
        let is_price_editable = $(element).parents('tr').attr('data-price_editable');
        let is_hosp_service = $(element).parents('tr').attr('data-hosp_service');
        let is_doc_required = $(element).parents('tr').attr('data-doc_required');
        let is_nursing_service = $(element).parents('tr').attr('data-nursing_service');
        let applicable_gender = $(element).parents('tr').attr('data-gender');
        let restrict_duration = $(element).parents('tr').attr('data-duration');
        let duration_mode = $(element).parents('tr').attr('data-duration_mode');
        let duration_type = $(element).parents('tr').attr('data-duration_type');
        let status = $(element).parents('tr').attr('data-status');
        let restrict_duration_count = $(element).parents('tr').attr('data-restrict_duration_count');
        let is_lab_item = $(element).parents('tr').attr('data-lab_item');
        let is_procedure = $(element).parents('tr').attr('data-procedure');
        let is_discharge_item = $(element).parents('tr').attr('data-discharge_item');
        let esi_name = $(element).parents('tr').attr('data-esi_name');
        let esi_code = $(element).parents('tr').attr('data-esi_code');
        let nabh_price = $(element).parents('tr').attr('data-nabh_price');
        let price = $(element).parents('tr').attr('data-price');
        let emergency = $(element).parents('tr').attr('data-emergency_charges');
        let effective_date = $(element).parents('tr').attr('data-effective_date');
        let room_type_name = $(element).parents('tr').attr('data-roomtype');
        let status_name = $(element).parents('tr').attr('data-status_service_charge');
        let sac_code = $(element).parents('tr').attr('data-sac_code');
        resetSubDepartment();
        $(".reference_id").val(service_id);
        $(".service_name_add").val(service_name);
        $(".service_code_add").val(service_code);
        $("#department_add").val(department);
        $("#department_add").select2().trigger('change');
        $("#subdepartment_add").val(subdepartment);
        $("#subdepartment_add").select2().trigger('change');
        $(".applicable_gender_add").val(applicable_gender);
        $(".restrict_duration").val(restrict_duration);
        $(".restrict_duration_count").val(restrict_duration_count);
        $(".esi_name_add").val(esi_name);
        $(".esi_code_add").val(esi_code);
        $(".nabh_price_add").val(nabh_price);
        $(".price").val(price);
        $(".emergency").val(emergency);
        $("#effective_date").val( moment(effective_date).format('YYYY-MM-DD'));
        $("#room_type_name").val(room_type_name);
        $("#room_type_name").select2().trigger('change');
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
        $("#add").html('Edit Service');
        $('#service_charge_id_hidden').val(service_charge_id);
        $(".sac_code_add").val(sac_code);

        if(duration_mode==0){
             $(".duration_mode").prop("checked", false);
         }else if(duration_mode==1){
            $("#hourly").prop("checked", true);

        } else if(duration_mode==2){
           $("#daily").prop("checked", true);

        }
        if(duration_type==0){
         $(".duration_type").prop("checked", false);
        }else if(duration_type==1){
           $("#warning").prop("checked", true);

        } else if(duration_type==2){
          $("#block").prop("checked", true);

        }
        if(status_name==1){
            $(".status_name").prop("checked", true);

        } else {
             $(".status_name").prop("checked", false);

        }
        if(status==1){
            $(".status_add").prop("checked", true);

        } else {
             $(".status_add").prop("checked", false);

        }
        if(is_billable==1){
            $(".is_billable").prop("checked", true);

         } else {
             $(".is_billable").prop("checked", false);

         }
         if(is_nursing_service==1){
             $(".is_nursing_service").prop("checked", true);

         } else {
            $(".is_nursing_service").prop("checked", false);

         }
         if(is_refundable==1){
            $(".is_refundable").prop("checked", true);

        } else {
             $(".is_refundable").prop("checked", false);

        }
        if(is_discountable==1){
            $(".is_discountable").prop("checked", true);

            } else {
                $(".is_discountable").prop("checked", false);

            }
            if(is_price_editable==1){
                $(".is_price_editable").prop("checked", true);

            } else {
            $(".is_price_editable").prop("checked", false);

            }
            if(is_hosp_service==1){
            $(".is_hosp_service").prop("checked", true);

        } else {
           $(".is_hosp_service").prop("checked", false);

        }
        if(is_doc_required==1){
            $(".is_doc_required").prop("checked", true);

        } else {
           $(".is_doc_required").prop("checked", false);

        }
        if(restrict_duration==1){
            $(".restrict_duration").prop("checked", true);
            $('#duration').prop("disabled", false).addClass('enabled');
            $('#h').prop("disabled", false).addClass('enabled');
            $('#hourly').prop("disabled", false);
            $('#d').prop("disabled", false).addClass('enabled');
            $('#daily').prop("disabled", false);
            $('#w').prop("disabled", false).addClass('enabled');
            $('#warning').prop("disabled", false);
            $('#b').prop("disabled", false).addClass('enabled');
            $('#block').prop("disabled", false);
            $('#count').prop("disabled", false).addClass('enabled');
            $('#restrict_duration_count').prop("disabled", false);
        }else {
           $(".restrict_duration").prop("checked", false);
           $('#duration').prop("disabled", true).addClass('disabled');
           $('#h').prop("disabled", true).addClass('disabled');
           $('#hourly').prop("disabled", true);
           $('#d').prop("disabled", true).addClass('disabled');
           $('#daily').prop("disabled", true);
           $('#w').prop("disabled", true).addClass('disabled');
           $('#warning').prop("disabled", true);
           $('#b').prop("disabled", true).addClass('disabled');
           $('#block').prop("disabled", true);
           $('#count').prop("disabled", true).addClass('disabled');
           $('#restrict_duration_count').prop("disabled", true);
        }
        if(is_lab_item==1){
            $(".is_lab_item").prop("checked", true);

        } else {
            $(".is_lab_item").prop("checked", false);

        }
        if(is_procedure==1){
            $(".is_procedure").prop("checked", true);

        } else {
            $(".is_procedure").prop("checked", false);

        }
        if(is_discharge_item==1){
            $(".is_discharge_item").prop("checked", true);

        } else {
            $(".is_discharge_item").prop("checked", false);

        }

    }

    function duration(){
        if ($("#restrict_duration").is(":checked")) {
            $('#duration').prop("disabled", false).addClass('enabled');
            $('#h').prop("disabled", false).addClass('enabled');
            $('#hourly').prop("disabled", false);
            $('#d').prop("disabled", false).addClass('enabled');
            $('#daily').prop("disabled", false);
            $('#w').prop("disabled", false).addClass('enabled');
            $('#warning').prop("disabled", false);
            $('#b').prop("disabled", false).addClass('enabled');
            $('#block').prop("disabled", false);
            $('#count').prop("disabled", false).addClass('enabled');
            $('#restrict_duration_count').prop("disabled", false);
        }else{
            $('#duration').prop("disabled", true).removeClass('enabled');
            $('#h').prop("disabled", true).removeClass('enabled');
            $('#hourly').prop("disabled", true);
            $('#d').prop("disabled", true).removeClass('enabled');
            $('#daily').prop("disabled", true);
            $('#w').prop("disabled", true).removeClass('enabled');
            $('#warning').prop("disabled", true);
            $('#b').prop("disabled", true).removeClass('enabled');
            $('#block').prop("disabled", true);
            $('#count').prop("disabled", true).removeClass('enabled');
            $('#restrict_duration_count').prop("disabled", true);
        }
    }

    function resetForm(){
        $(".reference_id").val('');
        $(".service_name_add").val('');
        $(".service_code_add").val('');
        $("#department_add").val('').trigger('change');
        $("#subdepartment_add").val('').trigger('change');
        $('input:checkbox').removeAttr('checked');
        $(".applicable_gender_add").val('');
        $(".esi_name_add").val('');
        $(".esi_code_add").val('');
        $(".nabh_price_add").val('');
        $(".restrict_duration_count").val('');
        $('#duration').prop("disabled", true).removeClass('enabled');
        $('#h').prop("disabled", true).removeClass('enabled');
        $('#hourly').prop("disabled", true);
        $('#d').prop("disabled", true).removeClass('enabled');
        $('#daily').prop("disabled", true);
        $('#w').prop("disabled", true).removeClass('enabled');
        $('#warning').prop("disabled", true);
        $('#b').prop("disabled", true).removeClass('enabled');
        $('#block').prop("disabled", true);
        $('#count').prop("disabled", true).removeClass('enabled');
        $('#restrict_duration_count').prop("disabled", true);
        $(".price").val('0');
        $(".emergency").val('');
        $(".effective_date").val('');
        $("#room_type_name").val('').trigger('change');
        $("#add").html('Add Service');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
        $(".sac_code_add").val('');
        resetSubDepartment();
    }


    function changeSubDepartment(){
        var base_url = $('#base_url').val();
        var department_id = $("#department_add").val();
        var url = base_url+'/master/getSubDepartment';
        $.ajax({
            url: url,
            type: 'POST',
            data: {department_id: department_id},
            beforeSend: function () {
                $('#subdepartment_add').html('<option value="">Loading...</option>');
                $('#subdepartment_add').empty();
                $('#subdepartment_add').select2('data', null);
                $("#subdepartment_add").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function(data){
                $("#subdepartment_add").html(data);
                $("#subdepartment_add").LoadingOverlay("hide");
            }
        });
    }

    function resetSubDepartment(){
        var sub_dept_html = $('#sub_dept_html').val();
        sub_dept_html = atob(sub_dept_html);
        $('#subdepartment_add').empty();
        $('#subdepartment_add').select2('data', null);
        $("#subdepartment_add").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        $("#subdepartment_add").html(sub_dept_html);
        $("#subdepartment_add").LoadingOverlay("hide");
    }
