$(document).ready(function() {
    setTimeout(function() {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");

    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});
document.getElementById("date_from").blur();
document.getElementById("date_to").blur();
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
         if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
$("#category").on('change', function() {
    var category = $('#category').val();
    var load = $('#warning1').val();

    if (category == '') {
      var category = 'a';

    }

 if(category) {


        $.ajax({
                    type: "GET",
                    url: '',
                    data: 'category=' + category,
                    beforeSend: function () {

                        $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#sub_category" ).prop( "disabled", true ); //Disable
                     },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#sub_category").empty();

                                        $("#sub_category").html('<option value="">Select Sub catogery</option>');
                                        $.each(html,function(key,value){
                                          $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                                        });
                                        $('#sub_category').select2();

                                }else{
                                        $("#sub_category").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                        $( "#sub_category" ).prop( "disabled", false );
                    },
                    complete: function () {
                        $('#warning1').remove();
                        $( "#sub_category" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $('#category').focus();
                $("#sub_category").empty();
            }

    });
    $("#location").on('click', function() {
        $('#warning2').remove();
    });
function getExpiryListingData() {
    var url = route_json.ExpiryListingData;

    //-------filters---------------------------------------------
    var from_date=$('#date_from').val();
    var to_date=$('#date_to').val();
   var location=$('#location').val();
   var category=$('#category').val();
   var sub_category=$('#sub_category').val();
   var item_name=$('#item_name_hidden').val();
   var vendor=$('#vendor_hidden').val();
   var checkbox1=$('#show_narcotic_item').val();


   var parm={from_date:from_date,to_date:to_date,location:location,category:category,
    sub_category:sub_category,item_name:item_name,
    vendor:vendor,
    checkbox1:checkbox1};
    if($('input[name="checkbox1"]:checked').length > 0){

        var checkbox1 = 0;


    }else{
        var checkbox1 = 1;

    }
    parm['checkbox1'] = checkbox1;



    if (location==null || location=="")
    {

        $('#s2id_location').append('<p id="warning2" style=color:red;padding-left:2px;>Required field......</p>');
        return false;
    }
   else{

    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#location").css({

                "border":'',
      "background": '',


    });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
         // alert(html);

        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
            var is_print =$("#is_print").val();

            if(is_print==0 || is_print==undefined){
                   $("#print_results").addClass("disabled");
               }
           var is_excel =$("#is_excel").val();

           if(is_excel==0 || is_excel==undefined){
                   $("#csv_results").addClass("disabled");
                }
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
    return false;

}

}


//     $('#location').on('submit', function (e) {
//       e.preventDefault();

//       getStockListingData();


//     });
//   });

// function datarst2() {


//     $('.ajaxSearchBox').hide();
// }

//---------generate csv------------------------------------------------------

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");
    var total=document.getElementById("total_data").innerHTML;
    var total1=document.getElementById("heading").innerHTML;
    csv.push(total1);
    csv.push(total);


    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
        {
           var innertext= cols[j].innerText.replace(/,/g, ' ');
            row.push(innertext);
        }


        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead('destroy');
    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");
};

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}



function datarst1() {

    var current_date = $('#current_date').val();
    $(".datapicker").select2('val', '');
   // $(".datapicker").trigger("change");

    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
    $('#item_name_hidden').val('');
    $('#vendor_hidden').val('');
    $('#vendor').val('');
    $('#item_name').val('');
    $('input:checkbox').removeAttr('checked');



}
