
$(document).on('ready', function () {

    $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    $(".to_date").focus();
    $(".from_date").focus();
    $(".searchBills").focus().click();

    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        var patient_name = $(this).val();
        if (event.keyCode == 13) {
            ajaxlistenter('patient_idAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if (!patient_name) {
                $('#patient_id_hidden').val('');
                $('#patient_uhid_hidden').val('');
            }

            patient_name = patient_name.trim();
            if (patient_name == "") {
                $("#patient_idAjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "patient_name_search=" + patient_name,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        //  alert(html); return;
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("patient_idAjaxDiv", event);
        }
    });

    //-----------Bill No search------------
    $('#bill_no').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        var bill_no = $(this).val();
        if (event.keyCode == 13) {
            ajaxlistenter('bill_no_AjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if (bill_no) {
                $('#bill_no_hidden').val('');
                $(".bill_no_search").val();
            }

            if (bill_no == "") {
                $("#bill_no_AjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "bill_no=" + bill_no,
                    beforeSend: function () {
                        $("#bill_no_AjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        $("#bill_no_AjaxDiv").html(html).show();
                        $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("bill_no_AjaxDiv", event);
        }
    });

})


$(document).on('click', '.searchBills', function (event) {
    if (!$(".from_date").val()) {
        $(".from_date").val(moment().format('MMM-DD-YYYY'));
    }
    if (!$(".to_date").val()) {
        $(".to_date").val(moment().format('MMM-DD-YYYY'));
    }
    billSearch(1);
});

$(document).on('click', '.saveBillTagBtn', function (event) {
    var bill_tag_id = $("#change_otbilltag_modal").attr('data-bill-tag-id');
    var bill_head_id = $("#change_otbilltag_modal").attr('data-bill-head-id');
    var new_bill_tag_id = $("#new_bill_tag").val();
    if (parseInt(bill_tag_id) === parseInt(new_bill_tag_id)) {
        toastr.error("Please select different bill tag");
        return;
    }

    var data = {};
    data.bill_head_id = bill_head_id;
    data.new_bill_tag_id = new_bill_tag_id;
    var url = $("#base_url").val() + "/otbilling/updateOTBillTag";
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(".saveBillTagBtn i").removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (response) {
            $(".saveBillTagBtn i").removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
            $('body').LoadingOverlay("hide");
            toastr.success("success");
            $(".searchBills").click();
            if (response) {
                $("#change_otbilltag_modal").modal('hide');
                bootbox.alert('New Bill Number : ' + response.bill_no);
            }

        },
        complete: function () { }
    });


});


$(document).on('click', '.changeOTBillTagBtn', function () {
    var bill_head_id = $(this).parents('tr').attr('data-bill-head-id');
    var bill_tag_id = $(this).parents('tr').attr('data-bill-tag-id');
    var bill_no = $(this).parents('tr').find('.bill_no').html();
    var bill_tag = $(this).parents('tr').find('.bill_tag').html();
    var patient_name = $(this).parents('tr').find('.patient_name').html();
    var uhid = $(this).parents('tr').find('.uhid').html();
    var admitting_doctor = $(this).parents('tr').find('.admitting_doctor').html();
    var bill_amount = $(this).parents('tr').find('.bill_amount').html();
    var total_net_amount = $(this).parents('tr').find('.total_net_amount').html();
    var bill_date = $(this).parents('tr').find('.bill_date').html();
    var created_date = $(this).parents('tr').find('.created_date').html();

    $("#change_otbilltag_modal").find('.bill_no').html(bill_no);
    $("#change_otbilltag_modal").find('.bill_tag').html(bill_tag);
    $("#change_otbilltag_modal").find('.patient_name').html(patient_name);
    $("#change_otbilltag_modal").find('.uhid').html(uhid);
    $("#change_otbilltag_modal").find('.admitting_doctor').html(admitting_doctor);
    $("#change_otbilltag_modal").find('.bill_amount').html(bill_amount);
    $("#change_otbilltag_modal").find('.total_net_amount').html(total_net_amount);
    $("#change_otbilltag_modal").find('.bill_date').html(bill_date);
    $("#change_otbilltag_modal").find('.created_date').html(created_date);
    $("#change_otbilltag_modal").attr('data-bill-tag-id', bill_tag_id);
    $("#change_otbilltag_modal").attr('data-bill-head-id', bill_head_id);

    $("#new_bill_tag").val('');

    $("#change_otbilltag_modal").modal('show');

});


$('.searchBillContent').on('click', '.pagination a', function (e) {
    billSearch($(this).attr('href').split('page=')[1]);
    e.preventDefault();
});


function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
    $('#bill_no').val(bill_no);
    $('#patient_uhid_hidden').val(uhid);
    $('#bill_id_hidden').val(bill_id);
    $('#bill_no_AjaxDiv').hide();
    if (from_type != 1) {
        fillPatientValues(uhid, patient_name, 1);
    }
}

function fillPatientValues(uhid, patient_name, from_type = 0) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
    if (from_type != 1) {
        $('#bill_no').val('');
        $('#bill_id_hidden').val('');
    }
}


function billSearch(page_no) {

    var bill_no = $(".bill_no_search").val();
    var uhid = $('.patient_uhid_search').val();
    var from_date = $(".from_date").val();
    var to_date = $(".to_date").val();
    var bill_tag_search = $("#bill_tag_search").val();
    var url = "";
    $.ajax({
        type: "GET",
        url: url,
        data: "&search=" + 1 + "&bill_no=" + bill_no + "&uhid=" + uhid + "&from_date=" + from_date + "&to_date=" + to_date + "&bill_tag_search=" + bill_tag_search + "&page=" + page_no,
        beforeSend: function () {
            $(".searchBills i").removeClass('fa-search').addClass('fa-spinner').addClass('fa-spin');
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (response) {
            $(".searchBills i").removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-search');
            $('body').LoadingOverlay("hide");
            $('.searchBillContent').html(response);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () { }
    });
}

$(document).on('click', '.clearSearch', function (event) {
    $(".bill_no_search").val('');
    $(".patient_uhid_search").val('');
    $('#patient_name').val('');
    $(".from_date").val(moment().format('MMM-DD-YYYY'));
    $(".to_date").val(moment().format('MMM-DD-YYYY'));
    $("#bill_tag_search").val('');
});
