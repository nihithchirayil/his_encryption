$(document).ready(function(){
    $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    fetchScreeningListData();


  

    $(".global_patient_check").click(function(){
        if($(this).is(":checked")){
            $("#patient_general_search_txt").attr("readonly", false); 
            $("#group_doctor").attr("readonly", true); 
            $("#search_date").attr("readonly", true); 
            $("#patient_search").attr("readonly", true); 
            
        } else {
            $("#patient_general_search_txt").attr("readonly", true); 
            $("#group_doctor").attr("readonly", false); 
            $("#search_date").attr("readonly", false); 
            $("#patient_search").attr("readonly", false); 
            $("#patient_general_search_txt").val('');
            
        }
        fetchScreeningListData();
        
    });
});


$( "#temperature_value").keyup(function() {
    vital_value_c = $("#temperature_value").val();
    if(vital_value_c != ''){
        vital_value_f = ((vital_value_c * (9 / 5)) + 32).toFixed(2);
        $('#temperature_value_f').val(vital_value_f);
    }else{
        $('#temperature_value_f').val('');
    }
});


$( "#temperature_value_f").keyup(function() {
    vital_value_f = $("#temperature_value_f").val();
    if(vital_value_f != ''){
        vital_value_c = ((vital_value_f - 32) * (5 / 9)).toFixed(2);
        $('#temperature_value').val(vital_value_c);
    }else{
        $('#temperature_value').val('');
    }
});
function fetchScreeningListData() {
    var doctor_id = $('#group_doctor').val() ? $('#group_doctor').val(): 0;
    var search_date = $('#search_date').val();
    var global_search = $(".global_patient_check").is(":checked") ? 1 : 0;
    var patient_general_search_txt = $("#patient_general_search_txt").val();
    var data = {
        doctor_id:doctor_id,
        search_date:search_date,
        global_search:global_search,
        patient_general_search_txt: patient_general_search_txt
    }

    if(global_search == 1 && patient_general_search_txt == ""){
        return;
    }
    

    if(doctor_id && search_date){
        var url = $('#base_url').val() + "/emr/fetchScreeningListData";
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            beforeSend: function () {
                $('.screening_list_table_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (res) {
                $('.screening_list_table_container').LoadingOverlay("hide");
                $('.screening_list_table_container').html(res);
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            },
            complete: function () {
                
            }
        });
    }

    
}

function showpatientVital(patient_id, visit_id,encounter_id,batch_no){
    var data = {
        patient_id:patient_id,
        visit_id:visit_id,
        // remark_id:remark_id,
        encounter_id:encounter_id,
        batch_no:batch_no,
        // remarks:remarks
    };

    var url = $('#base_url').val() + "/emr/fetchPatientLatestVital";
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (res) {
            $('body').LoadingOverlay("hide");
            $("#time_taken").datetimepicker({ format: 'DD-MMM-YYYY',
            maxDate: new Date(),
        });
            // $("#remarks").val(remarks);
            if(res.batch_no){
                console.log(res.temp_f);
                $("#vital_batch").val(res.batch_no);
                $("#weight_value").val(res.weight_val);
                $("#height_value").val(res.height_val);
                $("#bp_systolic").val(res.bp_systolic);
                $("#bp_diastolic").val(res.bp_diastolic);
                $("#temperature_value").val(res.temp);
                $("#temperature_value_f").val(res.temp_f);
                $("#pulse").val(res.pulse);
                $("#respiration").val(res.respiration);
                $("#head").val(res.head_circ);
                $("#oxygen").val(res.oxygen);
                $("#bmi").val(res.bmi);
                $("#spirometry").val(res.spirometry);
                $("#time_taken").val(res.time_taken);
                $("#remarks").val(res.remark_id);
            } else {
                $("#vital_batch").val('');
                $("#weight_value").val('');
                $("#height_value").val('');
                $("#bp_systolic").val('');
                $("#bp_diastolic").val('');
                $("#temperature_value").val('');
                $("#temperature_value_f").val('');
                $("#pulse").val('');
                $("#respiration").val('');
                $("#head").val('');
                $("#oxygen").val('');
                $("#bmi").val('');
                $("#spirometry").val('');
                $("#remarks").val('');
                $("#time_taken").val(res.current_time_taken);
            }


            // if(res.temp){
            //     $( "#temperature_value").trigger('keyup');
            // }
            // if(res.temp_f){
            //     $( "#temperature_value_f").trigger('keyup');
            // }
            
            $("#editvitalModal").attr('patient_id', patient_id);
            $("#editvitalModal").attr('visit_id', visit_id);
            // $("#editvitalModal").attr('remark_id', remark_id);
            // $("#editvitalModal").attr('remarks',remarks);
            $("#editvitalModal").attr('encounter_id', '');

            $("#editvitalModal").modal('show');


        },
        complete: function () {
            
        }
    });

    
}


function saveVitals() {
    var vital_value = {};
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    let patient_id = $("#editvitalModal").attr('patient_id');
    let visit_id = $("#editvitalModal").attr('visit_id');
    let remark_id = $("#editvitalModal").attr('remark_id');
    // let remarks = $("#editvitalModal").attr('remarks');
    let encounter_id = $("#editvitalModal").attr('encounter_id');
    let time_taken = $('#time_taken').val();
    let remarks = $('#remarks').val();
    let vital_batch = $('#vital_batch').val();
 
    if ($("#weight").val() == 1) {
        if ($("#weight_value").val() != '') {
            vital_value["1"] = $("#weight_value").val();
            vital_value["2"] = (($("#weight_value").val()) / 2.205).toFixed(2);
        }

    }
    if ($("#weight").val() == 2) {
        if ($("#weight_value").val() != '') {
            vital_value["2"] = $("#weight_value").val();
            vital_value["1"] = (($("#weight_value").val()) * 2.205).toFixed(2);
        }

    }



    if ($("#temperature").val() == 9) {
        if ($("#temperature_value_f").val() != '') {
            vital_value["9"] = $("#temperature_value_f").val();
            vital_value["10"] = (($("#temperature_value_f").val() - 32) * (5 / 9)).toFixed(2);
            console.log(vital_value)
        }

    }
    if ($("#temperature").val() == 10) {
        if ($("#temperature_value").val() != '') {
            vital_value["10"] = $("#temperature_value").val();
            vital_value["9"] = (($("#temperature_value").val() * (9 / 5)) + 32).toFixed(2);
        }

    }


    if ($("#height").val() == 3) {
        if ($("#height_value").val() != '') {
            vital_value["3"] = $("#height_value").val();
            vital_value["4"] = (($("#height_value").val()) * (30.48)).toFixed(2);
        }

    }
    if ($("#height").val() == 4) {
        if ($("#height_value").val() != '') {
            vital_value["4"] = $("#height_value").val();
            vital_value["3"] = ((($("#height_value").val()) / 30.48).toFixed(2));
            ;
        }

    }
    if ($("#bp_systolic").val() != '') {
        vital_value["5"] = $("#bp_systolic").val();
    }
    if ($("#bp_diastolic").val() != '') {
        vital_value["6"] = $("#bp_diastolic").val();
    }
    if ($("#pulse").val() != '') {
        vital_value["7"] = $("#pulse").val();
    }
    if ($("#respiration").val() != '') {
        vital_value["8"] = $("#respiration").val();
    }
    if ($("#temperature_location").val() != '') {
        vital_value["11"] = $("#temperature_location").val();
    }

    if ($("#oxygen").val() != '') {
        vital_value["12"] = $("#oxygen").val();
    }
    if ($("#head").val() != '') {
        vital_value["13"] = $("#head").val();
    }
    if ($("#bmi").val() != '') {
        vital_value["14"] = $("#bmi").val();
    }
    if ($("#spirometry").val() != '') {
        vital_value["20"] = $("#spirometry").val();
    }

    var vitals_length = Object.keys(vital_value).length;
    if(vitals_length < 2){
        toastr.info('Enter patient vitals')
        return;
    }

    $.ajax({
        type: "POST",
        url: url + "/emr/add_vital",
        data: {
            vital_array: vital_value,
            patient_id: patient_id,
            visit_id: visit_id,
            remark_id: remark_id,
            encounter_id: encounter_id,
            time_taken: time_taken,
            remarks: remarks,
            vital_batch: vital_batch,
            _token: _token
        },
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            console.log(data);
            $('body').LoadingOverlay("hide");
            if(data){
                Command: toastr["success"]("Success");
            $("#editvitalModal").modal('hide');
            $('#vital_batch').val(data);
                fetchScreeningListData();
            }
        },
        complete: function () {
          //  $('#editvitalModal').modal('toggle');
        }
    });
}


$('#weight_value,#height_value,#height_value').on('blur',function(){
    let w_val = '';
    let w_type = '';
    let h_val = '';
    let h_type = '';

    w_val = $('#weight_value').val();
    h_val = $('#height_value').val();
    if(h_val == ""){
        h_val = $('#height_value').val();
    }
    w_type = $('#weight').val();
    h_type = $('#height').val();

    if(w_val != '' && h_val != '' && w_type != ''){
        calculateBmi(w_val,w_type,h_val,h_type);
    }
});

function calculateBmi(weight,wtype,height,htype){

    //convert weight to LBS to KG (weight in KG)
    let weight_type = "";
    let weight_kg = "";
    weight_type = wtype;
    //1 LBS 2 KG
    if(weight_type == "1"){
        weight_kg = Math.round((parseFloat(weight)/2.2046));
    }else{
        weight_kg = Math.round(parseFloat(weight));
    }

    //convert height to (F/I) to M
    //3 INCH 4 CM
    let height_type = "";
    let height_cm = "";
    let height_m = "";
    height_type = htype;
    if(height_type == "3"){
      let vital_value_feet = height;

       //inch to meter
       height_m = parseFloat(vital_value_feet)/3.2808;

    }else{
        height_cm = parseFloat(height);
        height_m = parseFloat(height_cm/100);//convert to meter
    }

    if(weight_kg != '' && height_m != ''){
        let bmi_val = '';
        bmi_val = parseFloat(weight_kg/(height_m*height_m));
        $('input[name="bmi"]').val(bmi_val.toFixed(2));
    }

}


function search_patient() {
    var keytype = $('input[name="search_type"]:checked').val();
    var search_key = $("#patient_search").val();
    search_key = search_key.toUpperCase();
    $('.patient_det_widget_screening').hide();
    if (keytype == 1) {
        $(".patient_det_widget_screening[data-patientname^='" + search_key + "']").show();
    } else if (keytype == 2) {
        $(".patient_det_widget_screening[data-uhid^='" + search_key + "']").show();
    } else {
        $(".patient_det_widget_screening[data-phone^='" + search_key + "']").show();
    }
    if (search_key == '') {
        $('.patient_det_widget_screening').show();
    }
}


var search_timeout = null;
function patient_general_search(txt) {
    var last_search_key = '';
    var request_flag = '';
    if (txt.length >= 2 && $.trim(txt) != '') {

        clearTimeout(search_timeout);
        search_timeout = setTimeout(function () {

            if (last_search_key == txt || request_flag == 1) {
                return false;
            }
            last_search_key = txt;
            var url = $('#base_url').val() + "/emr/generalPatientSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'txt=' + txt,
                beforeSend: function () {
                    $("#completepatientbox").empty();
                    $("#completepatientbox").show();
                    $(".input_rel .input_spinner").removeClass('hide');
                    $("#completepatientbox").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    request_flag = 1
                },
                success: function (data) {

                    $("#completepatientbox").LoadingOverlay("hide");
                    $("#completepatientbox").html(data);
                    $(".input_rel .input_spinner").addClass('hide');
                    request_flag = 0;
                }
            });
        });
    } else if (txt.length === 0) {
        $("#completepatientbox").hide();
        $("#completepatientbox").LoadingOverlay("hide");
    }
}


// $(document).on("click", ".searched_items a", function(){
//     console.log("asdasd")
//     var uhid = $(this).attr('data-uhid');
//     $("#patient_general_search_txt").val(uhid);
//     $("#completepatientbox").hide();
//     fetchScreeningListData();
// })

function goToSelectedPatient(patient_id, uhid){
    $("#patient_general_search_txt").val(uhid);
    $("#completepatientbox").hide();
    fetchScreeningListData();
}