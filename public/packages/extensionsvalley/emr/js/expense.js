$(document).ready(function() {
    searchExpense();
   $('.date_picker').datetimepicker({
    format: 'MMM-DD-YYYY',
    maxDate : 'now'
});
$(".select2").select2();

 });
 
 function searchExpense() {
     var expense_type = $("#expense_search").val();
     var expense_from = $("#expense_from").val();
     var expense_to = $("#expense_to").val();
     var base_url = $("#base_url").val();
     var bill_no = $("#sch_bill").val();
     var voucher_no = $("#sch_voucher").val();
     var token = $("#token_hiddendata").val();
     var param = { _token: token, expense_type: expense_type,expense_from:expense_from,expense_to:expense_to,bill_no:bill_no,voucher_no:voucher_no  };
 
     var url = base_url + "/master/searchExpense";
 
  
     $.ajax({
         type: "POST",
         url: url,
         data: param,
         beforeSend: function() {
            $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
             $("#searchBtn").attr("disabled", true);
             $("#searchSpin").removeClass("fa fa-search");
             $("#searchSpin").addClass("fa fa-spinner fa-spin");
         },
         success: function(data) {
 
             $("#searchDataDiv").html(data);
 
         },
         complete: function() {
             $("#searchBtn").attr("disabled", false);
             $("#searchSpin").removeClass("fa fa-spinner fa-spin");
             $("#searchSpin").addClass("fa fa-search");
             $("#searchDataDiv").LoadingOverlay("hide");

         },
         error: function() {
             toastr.error("Error Please Check Your Internet Connection");
         },
     });
  
 }
 
 function formReset() {
    $('.reset').val('');
    $('#expense_type').val('').select2();
    $('#bank_name').val('').select2();
    $('#hid_id').val('');
    $('#approve_this_claim').fadeOut();
    $('#reject_this_claim').fadeOut();
    $('#pay_this_claim').fadeOut();
     $("#savebtnBtn").html('<i id="savebtnSpin" class="fa fa-save"></i> Save');
    $('#payment_type').val('1').select2();
 }
 
 function saveExpense(from_type=0) {
     var base_url = $("#base_url").val();
     var token = $("#token_hiddendata").val();
     var expense_type = $("#expense_type").val();
     var expense_date = $("#expense_date").val();
     var narration = $("#narration").val();
     var amount = $("#amount").val();
     var hid_id = $("#hid_id").val();
     var bill_no = $("#bill_no").val();
     var check_no = $("#check_no").val();
     var bank_name = $("#bank_name").val();
     var payment_type = $("#payment_type").val();
     if(expense_type){
     if(expense_date){
            if(amount){
                if(bill_no){
                if (payment_type) {
                var params = { status:from_type, _token: token,check_no:check_no,bank_name:bank_name,narration:narration,expense_type: expense_type,expense_date:expense_date,amount:amount,hid_id:hid_id,bill_no:bill_no ,payment_type:payment_type};
                var url= base_url +'/master/saveExpense';

                $.ajax({
                    type: "POST",
                    url: url,
                    data: params,
                    beforeSend: function() {
                        $("#savebtnBtn"+from_type).attr("disabled", true);
                        if(from_type==1){
                            $("#savebtnSpin"+from_type).removeClass("fa fa-thumbs-up");
                        }else if(from_type==2){
                            $("#savebtnSpin"+from_type).removeClass("fa fa-thumbs-down");
                        }else if(from_type==3){
                            $("#savebtnSpin"+from_type).removeClass("fa fa-check");
                        }else{
                            $("#savebtnSpin"+from_type).removeClass("fa fa-save");
                        }
                        $("#savebtnSpin"+from_type).addClass("fa fa-spinner fa-spin");
                    },
                    success: function(data) {
                         console.log(data);
                        if (data.status==1) {
                            toastr.success("Success.");
                            formReset();
                            searchExpense();
                            if(data.voucher_no!='' && data.voucher_no !=0){
                                bootbox.alert("Voucher number is "+data.voucher_no);

                            }
                
                        }else if(data.status==2){
                            toastr.warning("Existing data.");
                           
                        }
                    },
                    complete: function() {
                        $("#savebtnBtn"+from_type).attr("disabled", false);
                        $("#savebtnSpin"+from_type).removeClass("fa fa-spinner fa-spin");
                        if(from_type==1){
                            $("#savebtnSpin"+from_type).addClass("fa fa-thumbs-up");
                        }else if(from_type==2){
                            $("#savebtnSpin"+from_type).addClass("fa fa-thumbs-down");
                        }else if(from_type==3){
                            $("#savebtnSpin"+from_type).addClass("fa fa-check");
                        }else{
                            $("#savebtnSpin"+from_type).addClass("fa fa-save");                        }
                
                
                    },
                    warning: function() {
                        toastr.error("Error Please Check Your connection ");
                    },
                });
            } else {
                toastr.warning('Please add payment type.');
            }
            }else{
                toastr.warning('Please add bill no.');
            }
                
            }else{
                toastr.warning('Please add amount.');
            }

       
    }else{
        toastr.warning('Please add expense date.');
    }
}else{
    toastr.warning('Please select expense type.');
}
           
   
 
 }
 
 
 
 
 function editItem(status,id,bank_id,check_no,voucher_no,bill_no,amount,expense_type_id,date,narration,previllage,collection_mode) {
    
    $('#expense_type').val(expense_type_id).select2();
    $('#bank_name').val(bank_id).select2();
    $('#check_no').val(check_no);
    $('#amount').val(amount);
    $('#expense_date').val(date);
    $('#voucher_no').val(voucher_no);
    $('#bill_no').val(bill_no);
    $('#hid_id').val(id);
    $('#narration').val(narration);
    $('#payment_type').val(collection_mode).select2();
    if(previllage==1){
      $('#reject_this_claim').show();
      $('#approve_this_claim').show();
      if(status==1){
        $('#pay_this_claim').show();  
      }
    }else{
        $('#reject_this_claim').hide();
        $('#approve_this_claim').hide();  
        $('#pay_this_claim').hide();
    }
     $("#savebtnBtn").html('<i id="savebtnSpin" class="fa fa-save"></i> Update');
 
 }
 
 function deleteExpense(id) {
     var base_url = $("#base_url").val();
     var token = $("#token_hiddendata").val();
     bootbox.confirm({
        message: "Are You Sure You want Delete ?",
        buttons: {
            confirm: {
                label: "Delete",
                className: "btn-danger",
                
            },
            cancel: {
                label: "Cancel",
                className: "btn-warning",
                default: "true",
            },
        },
        callback: function (result) {
            if (result) {
    
                let url = base_url + "/master/deleteExpense";
                let data = { _token: token, id: id };
        
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                        $("#deleteButton_" + id).attr("disabled", true);
                        $("#deleteButton_" + id).removeClass("fa fa-trash");
                        $("#deleteButton_" + id).addClass("fa fa-spinner fa-spin");
        
        
                    },
                    success: function(data) {
                        $("#trlist_" + id).remove();
                        toastr.success('Deleted.');
                        searchExpense();
                    },
                    complete: function() {
        
                        $("#deleteButton_" + id).attr("disabled", false);
                        $("#deleteButton_" + id).removeClass("fa fa-spinner fa-spin");
                        $("#deleteButton_" + id).addClass("fa fa-trash");
        
                    },
        
                });
            
            }
        },
    });
 
 }

 function printClaimDetails(id){
    var base_url = $("#base_url").val();
    let url = base_url + "/master/printClaimDetails";
    let data = { id: id };

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
            $("#printClaimDetail" + id).attr("disabled", true);
            $("#printClaimDetailsSpin" + id).removeClass("fa fa-print");
            $("#printClaimDetailsSpin" + id).addClass("fa fa-spinner fa-spin");


        },
        success: function(data) {
            $('#expenseDetails').modal('show');
            $('#expenseDetails_content').html(data);
        },
        complete: function() {

            $("#printClaimDetail" + id).attr("disabled", false);
            $("#printClaimDetailsSpin" + id).removeClass("fa fa-spinner fa-spin");
            $("#printClaimDetailsSpin" + id).addClass("fa fa-print");

        },

    });
 }

function changeData(element)
{
    var option_payment = $("#payment_type  option:selected").text();
    if (option_payment == 'Cheque') {
        $(".check_no_div").removeClass('hidden');
        $(".bank_name_div").removeClass('hidden');
    } else {
        $(".check_no_div").addClass('hidden');
        $(".bank_name_div").addClass('hidden');
    }
}