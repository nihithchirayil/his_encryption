$(document).ready(function () {
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});

function save_patient(type_id) {
    var title = $('#title').val();
    var patient_name = $('#patient_name').val();
    var gender = $('#gender').val();
    var age = $('#age').val();
    var dob = $('#dob').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var address = $('#address').val();
    var country = $('#country').val();
    var state = $('#state').val();
    var district = $('#district').val();
    var area = $('#area').val();
    var pincode = $('#pincode').val();
    var remarks = $('#remarks').val();
    var age_creteria = $("input[name='age_criteria']:checked").val();
    var default_practice_loc = $("#default_practice_loc").val();

    if (patient_name == '') {
        Command: toastr["warning"]("Please enter Name");
        $('#patient_name').focus();
        return false;
    }

    if (gender == '') {
        Command: toastr["warning"]("Please select gender");
        $('#gender').focus();
        return false;
    }
    if (phone == '') {
        Command: toastr["warning"]("Please enter phone number");
        $('#phone').focus();
        return false;
    }

    if (phone != '') {
        var phoneno_reg_exp = /^\d{10}$/;
        if (phone.match(phoneno_reg_exp)) {
            //  Command: toastr["warning"]("Please enter invalid phone number");
        } else {
            Command: toastr["warning"]("Please enter valid phone number");
            $('#phone').focus();
            return false;
        }
    }



    if (age == '' && dob == '') {
        Command: toastr["warning"]("Please select age or Date of birth");
        $('#dob').focus();
        return false;
    }
    if (age != '' && age_creteria == '') {
        Command: toastr["warning"]("Please select age");
        $('#age').focus();
        return false;
    }

    if (pincode == '') {
        Command: toastr["warning"]("Please enter pincode");
        $('#pincode').focus();
        return false;
    }

    /* if (company == '') {
         Command: toastr["warning"]("Please select company");
         $('#company').focus();
         return false;
     }

     if (pricing == '') {
         Command: toastr["warning"]("Please select pricing");
         $('#pricing').focus();
         return false;
     }
     */

    if (email != '') {
        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        if (regexEmail.test(email)) { } else {
            Command: toastr['error']('Email is not valid', 'Error');
            return false;
        }
    }

    if (phone != '') {
        var phoneNum = phone.replace(/[^\d]/g, '');
        if (phoneNum.length < 6 && phoneNum.length > 11) {
            Command: toastr['error']('Phone Number is not valid', 'Error');
            return false;
        }
        /* var egExp = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}/;
         if (egExp.test(phone)) { } else {;
             Command: toastr['error']('Phone Number is not valid', 'Error');
             return false;
         };*/
    }

    var url = $('#domain_url').val() + "/patient_register/savePatientRegister";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            title: title,
            patient_name: patient_name,
            gender: gender,
            age: age,
            dob: dob,
            email: email,
            phone: phone,
            address: address,
            country: country,
            state: state,
            district: district,
            area: area,
            pincode: pincode,
            remarks: remarks,
            type_id: type_id,
            default_practice_loc: default_practice_loc,
            age_creteria: age_creteria
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (result) {
            $("body").LoadingOverlay("hide", true);
            var response = JSON.parse(result);
            if (response.status == 'success') {
                Command: toastr["success"](response.message + ' With UHID : ' + response.uhno);
                window.location.href = $('#domain_url').val() + "/patient_register/patientList/";
            }
            else {
                Command: toastr["error"]("Something went wrong.");
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide", true);
        }
    });
}
function update_patient() {
    var title = $('#title').val();
    var patient_id = $('#patient_id').val();
    var patient_name = $('#patient_name').val();
    var gender = $('#gender').val();
    var age = $('#age').val();
    var dob = $('#dob').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var address = $('#address').val();
    var country = $('#country').val();
    var state = $('#state').val();
    var district = $('#district').val();
    var area = $('#area').val();
    var pincode = $('#pincode').val();
    var remarks = $('#remarks').val();
    var age_creteria = $("input[name='age_criteria']:checked").val();
    var default_practice_loc = $("#default_practice_loc").val();

    if (patient_name == '') {
        Command: toastr["warning"]("Please enter Name");
        $('#patient_name').focus();
        return false;
    }

    if (gender == '') {
        Command: toastr["warning"]("Please select gender");
        $('#gender').focus();
        return false;
    }

    if (age == '' && dob == '') {
        Command: toastr["warning"]("Please select age or Date of birth");
        $('#dob').focus();
        return false;
    }
    if (age != '' && age_creteria == '') {
        Command: toastr["warning"]("Please select age");
        $('#age').focus();
        return false;
    }

    if (phone == '') {
        Command: toastr["warning"]("Please enter phone no");
        $('#phone').focus();
        return false;
    }
    if (pincode == '') {
        Command: toastr["warning"]("Please enter pincode");
        $('#pincode').focus();
        return false;
    }

    if (email != '') {
        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        if (regexEmail.test(email)) { } else {
            Command: toastr['error']('Email is not valid', 'Error');
            return false;
        }
    }

    if (phone != '') {
        var phoneNum = phone.replace(/[^\d]/g, '');
        if (phoneNum.length < 6 && phoneNum.length > 11) {
            Command: toastr['error']('Phone Number is not valid', 'Error');
            return false;
        }
    }

    var url = $('#domain_url').val() + "/patient_register/updatePatientRegister";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            title: title,
            patient_name: patient_name,
            gender: gender,
            age: age,
            dob: dob,
            email: email,
            phone: phone,
            address: address,
            country: country,
            state: state,
            district: district,
            area: area,
            pincode: pincode,
            remarks: remarks,
            default_practice_loc: default_practice_loc,
            age_creteria: age_creteria,
            patient_id: patient_id
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (result) {
            $("body").LoadingOverlay("hide", true);
            var response = JSON.parse(result);
            if (response.status == 'success') {
                Command: toastr["success"](response.message);
            }
            else {
                Command: toastr["error"]("Something went wrong.");
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide", true);
        }
    });
}

$('.number_only').keypress(function (event) {

    if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
        event.preventDefault(); //stop character from entering input
    }

});


