$(document).ready(function () {
    setTimeout(function () {
        //$(body).addClass('sidebar-collapse');
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");
    }, 300);
    // $('tbody').sortable();
    $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);


});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.insurancereport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});



function getInsuranceDiscount(bill_no,bill_tag,admission_date,discharge_date,pat,uhid,company_name,profit,company_ids,settled_date){
    var from_dates = $("#bill_date_from").val();
    var to_dates =  $("#bill_date_to").val();
            $.ajax({
                type: "GET",
                url: " ",
                data: 'bill_no=' + bill_no + '&bill_tag=' + bill_tag + '&admission_date=' + admission_date + '&discharge_date=' + discharge_date +'&pat=' + pat +'&uhid=' + uhid +'&company_name=' + company_name +'&to_dates=' + to_dates +'&from_dates=' + from_dates +'&profit='+ profit +'&vewinsurancediscount=1&company_ids='+company_ids+'&settled_date='+settled_date,
                beforeSend: function () {
                $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                },
               success: function (html) {
                   $("#view_ins_discount_modal").modal('show');
                   $("#append_ins_discount_modal").html(html);
                   $('#ResultsViewArea').LoadingOverlay("hide");
                },
                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
}
function printInsuranceReportProcessData(){
    $('#print_config_modal_insurance').modal('show');
}
function print_generate_insuranceReportProcessData(printData) {
    //$('#print_config_modal').modal('hide');


     var showw = "";
     var printMode = 1;//$('input[name=printModeIns]:checked').val();
     var printhead = "";

     var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if($('#showTitle').is(":checked")){
        printhead  = $("#hospital_headder").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:0;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write('<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>');
    mywindow.document.write('<style>.common_td_rules{text-align: left !important;overflow: hidden !important;border-right: solid 1px #bbd2bd !important;border-left: solid 1px #bbd2bd !important;max-width: 100px;text-overflow: ellipsis;white-space: nowrap;}</style>');
    mywindow.document.write('<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>');
    mywindow.document.write('<style>.common_td_rules{text-align: left !important;border-right: solid 1px #bbd2bd !important;border-left: solid 1px #bbd2bd !important;}</style>');
    mywindow.document.write('<style>.generate_sum_btn{display:none !important}</style>');
    mywindow.document.write('<style>.print_labal_text{display:none !important}</style>');
    mywindow.document.write('<style>.hide_td{display:none !important}</style>');
    mywindow.document.write('<style>.print_labels{padding-right:70% !important}</style>');
        mywindow.document.write('<style>.print_label{padding-left:50% !important}</style>');
    mywindow.document.write('<style>.col-md-3{float: left;padding-bottom: 10px;padding-right: 10px;padding-left: 10px;width: 40.33333333%;}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}
function modal_ins_hide(){
    $('#print_config_modal_insurance').modal().hide();
}
function exceller_ins(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table_exc');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}
function generate_sum(){
    var generate_sum_val = 0;
    $("input[type=checkbox]:checked").each(function() {
    generate_sum_val += parseFloat($(this).val());
  });
   var id = $("#div_id").val();
   var next_id = parseInt(id)+1;
   if(next_id==4||next_id==8|| next_id==12){
        var appnd_html = "<div class='row'><div class='col-md-3' id='"+next_id+"' style='min-height: 125px;'></div>";
   }else if(next_id==3||next_id==6|| next_id==9){
       var appnd_html = "<div class='col-md-3' id='"+next_id+"' style='min-height: 125px;'></div></div>";
   }else{
       var appnd_html = "<div class='col-md-3' id='"+next_id+"' style='min-height: 125px;'></div>";
   }
   $("#append_generated_sum").append(appnd_html);
   $("input[type=checkbox]:checked").each(function() {
       $(".generate_sum").attr("checked",false);
   });
   $("#div_id").val(next_id);
}
function generateCheckBoxSum(e){
   
   var item_dec =  $(e).closest('tr').find("td:first").html();
   var cur_amnt =  parseFloat($(e).val());
   var doctor_id =  parseFloat($(e).attr('data-id'));
   var next_id = $("#div_id").val();
   var generate_sum_val = 0;
   $("#sm_"+next_id).remove();
    $("input[type=checkbox]:checked").each(function() {
    generate_sum_val += parseFloat($(this).val());
  });
      var appnd_html = "<label class='print_labels' style='width:60%' id='item_"+next_id+"'>"+item_dec+"</label>\n\
<label class='print_label'>"+cur_amnt.toFixed(2)+"</label><br>\n\
<label class='print_label' style='padding-left:61%;border-top:1px solid' id='sm_"+next_id+"'>"+generate_sum_val.toFixed(2)+"</label>\n\
<input type='hidden' name='ins_item_desc[]' value='"+item_dec+"'><input type='hidden' name='item_amnt[]' value='"+cur_amnt.toFixed(2)+"'><input type='hidden' name='item_doctor[]' value='"+doctor_id+"'>";
   $("#"+next_id).append(appnd_html);
}
function generate_ins_report(){

    var ins_process_patient_uhid = $("#ins_process_patient_uhid").val();
    var ins_process_patient_company = $("#ins_process_patient_company").val();
    var ins_process_patient_discharge_date = $("#ins_process_patient_discharge_date").val();
    var ins_process_patient_approve_date = $("#ins_process_patient_approve_date").val();
            $.ajax({
                type: "POST",
                url: " ",
                data: $("#process_ins_gen").serialize()+
                '&generate_report=1&ins_process_patient_uhid=' 
                + ins_process_patient_uhid+'&ins_process_patient_company=' 
                + ins_process_patient_company+'&ins_process_patient_discharge_date=' + ins_process_patient_discharge_date+'&ins_process_patient_approve_date='+ins_process_patient_approve_date,
                beforeSend: function () {
                $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                },
               success: function (data) {
                $('#ResultsViewArea').LoadingOverlay("hide");
                if(data==1){
                    Command: toastr["success"]("Report generated successfully");
                }
                },
                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
}