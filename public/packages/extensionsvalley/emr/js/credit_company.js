
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_credit_company_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
        data.company_name = $("#company_name").val();
        data.company_type = $(".company_type").val();
        data.status = $(".status").val();
       console.log(data.status);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".company_name_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Company Name");
            return;
        }
     
        data.company_id = $(".reference_id").val();
        data.company_code = $(".company_code_add").val();
        data.company_name = $(".company_name_add").val();
        data.company_type = $(".company_type_add").val();
        data.address = $(".address_add").val();
        data.tel_no = $(".tel_no_add").val();
        data.fax_no = $(".fax_no_add").val();
        data.email_addr = $(".email_add").val();
        data.is_esi = $(".esi_add").val();
        data.status = $(".status_add").val();

       
        if($(".status_add").prop('checked') == true){
            // alert("chcked");
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".esi_add").prop('checked') == true){
            // alert("chcked");
            data.is_esi ='t';
        }else{
            data.is_esi ='f';
        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Company Code Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }
                    else if(data==2){
                        Command: toastr["error"]("Company Name Already Exit!");
                        $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                        $('.saveButton').attr('disabled', false);
        
                        }else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
   
    function editItem(element){
      
        let company_id = $(element).parents('tr').attr('data-id');
        let company_name = $(element).parents('tr').attr('data-company_name');
        let company_type = $(element).parents('tr').attr('data-company');
        let company_code = $(element).parents('tr').attr('data-company_code');
        let status = $(element).parents('tr').attr('data-status');
        let address = $(element).parents('tr').attr('data-address');
        let tel_no = $(element).parents('tr').attr('data-tel_no');
        let is_esi = $(element).parents('tr').attr('data-esi');
        let fax_no = $(element).parents('tr').attr('data-fax_no');
        let email_addr = $(element).parents('tr').attr('data-email_addr');
       
        if(status==1){
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", true);

            } else{
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", false);
                   }
            if(is_esi==1){
             //alert("CheckBox checked.");
                        $(".esi_add").prop("checked", true);
                
            } else{
            //alert("CheckBox checked.");
                         $(".esi_add").prop("checked", false);
                   }

        $(".reference_id").val(company_id);
        $(".company_name_add").val(company_name);
        $(".company_type_add").val(company_type);
        $(".company_code_add").val(company_code);
        $(".status_add").val(status);
        $(".address_add").val(address);
        $(".tel_no_add").val(tel_no);
        $(".esi_add").val(is_esi);
        $(".fax_no_add").val(fax_no);
        $(".email_add").val(email_addr);
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $("#add").html('Edit Credit Company');

    }

  
    function resetForm(){
        $(".reference_id").val('');
        $(".company_name_add").val('');
        $(".company_type_add").val('');
        $(".company_code_add").val('');
        $(".address_add").val('');
        $("#add").html('Add Credit Company');
        $(".status_add").val('');
        $(".tel_no_add").val('');
        $('input:checkbox').removeAttr('checked');
        $(".fax_no_add").val('');
        $(".email_add").val('');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

