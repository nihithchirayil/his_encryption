$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2();
    searchDiscountRequestedBills();
});

var base_url = $('#base_url').val();
var token = $('#hidden_filetoken').val();
var decimalConfiguration = $('#decimal_configuration').val();


function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    return number.toFixed(parseInt(decimalConfiguration));
}

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});



//----------- search patient name search------------
$('#search_patient_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var patient_name = $('#search_patient_name').val();
    if (event.keyCode == 13) {
        ajaxlistenter('patient_name_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (!patient_name) {
            $('#patient_uhid').val('');
            $('#patient_id').val('');
            $("#patient_name_AjaxDiv").hide();
        } else {
            var url = base_url + "/discount/searchApproveDiscountPatientName";
            $.ajax({
                type: "POST",
                url: url,
                data: "patient_name=" + patient_name,
                beforeSend: function () {
                    $("#patient_name_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#patient_name_AjaxDiv").html(html).show();
                    $("#patient_name_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('patient_name_AjaxDiv', event);
    }
});



//----------- search bill no search------------
$('#search_bill_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var bill_no = $('#search_bill_no').val();
    if (event.keyCode == 13) {
        ajaxlistenter('bill_no_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (!bill_no) {
            $('#bill_no').val('');
            $("#bill_no_AjaxDiv").hide();
        } else {
            var url = base_url + "/discount/searchApproveDiscountBillNO";
            $.ajax({
                type: "POST",
                url: url,
                data: "bill_no=" + bill_no,
                beforeSend: function () {
                    $("#bill_no_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#bill_no_AjaxDiv").html(html).show();
                    $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('bill_no_AjaxDiv', event);
    }
});


function fillBillNo(patient_id, bill_id, bill_no, uhid, patient_name) {
    $('#bill_id').val(bill_id);
    $('#search_bill_no').val(bill_no);
    $('#search_patient_name').val(patient_name);
    $('#patient_uhid').val(uhid);
    $('#patient_id').val(patient_id);
    $('.ajaxSearchBox').hide();
}


function fillPatientValues(uhid, patient_name, patient_id) {
    $('#search_patient_name').val(patient_name);
    $('#patient_id').val(patient_id);
    $('#patient_uhid').val(uhid);
    $('.ajaxSearchBox').hide();
}


function resetSearchFilters() {
    var current_date = $('#current_date').val();
    $('#request_fromdate').val(current_date);
    $('#request_todate').val(current_date);
    $('#bill_id').val('');
    $('#search_bill_no').val('');
    $('#search_bill_tag').val('').select2();
    $('#search_status').val('').select2();
    $('#search_patient_name').val('');
    $('#patient_uhid').val('');
    $('#patient_id').val(0);
    $('.ajaxSearchBox').hide();
    searchDiscountRequestedBills();
}


function searchDiscountRequestedBills() {
    var url = base_url + "/discount/searchApproveDiscount";
    var from_date = $('#request_fromdate').val();
    var to_date = $('#request_todate').val();
    var bill_id = $('#bill_id').val();
    var bill_no = $('#search_bill_no').val();
    var bill_tag = $('#search_bill_tag').val();
    var status = $('#search_status').val();
    var patient_name = $('#search_patient_name').val();
    var uhid = $('#patient_uhid').val();
    var patient_id = $('#patient_id').val();
    var param = {
        patient_id: patient_id,
        bill_id: bill_id,
        bill_no: bill_no,
        bill_tag: bill_tag,
        patient_name: patient_name,
        uhid: uhid,
        from_date: from_date,
        to_date: to_date,
        status: status
    }
    resetApproveBill();
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchDiscountRequestedBillsBtn').attr('disabled', true);
            $('#searchDiscountRequestedBillsSpin').removeClass('fa fa-search');
            $('#searchDiscountRequestedBillsSpin').addClass('fa fa-spinner fa-spin');
            $("#approveDiscountData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $('#approveDiscountData').html(data);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);
        },
        complete: function () {
            $('#searchDiscountRequestedBillsBtn').attr('disabled', false);
            $('#searchDiscountRequestedBillsSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchDiscountRequestedBillsSpin').addClass('fa fa-search');
            $("#approveDiscountData").LoadingOverlay("hide");
        }
    });
}


function approveDiscountRequest(request_id, status, bill_type) {
    var url = base_url + "/discount/approveDiscountRequest";
    $('#request_id').val(request_id);
    var param = {
        request_id: request_id,
        status: status,
        bill_type: bill_type
    };

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#approveDiscountRequestBtn' + request_id).attr('disabled', true);
            $('#approveDiscountRequestSPin' + request_id).removeClass('fa fa-list');
            $('#approveDiscountRequestSPin' + request_id).addClass('fa fa-spinner fa-spin');
            $("#approveDiscountList").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            if (data != 0) {
                $('#approveDiscountList').html(data);
                $(".theadscroll").perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30,
                });
                setTimeout(function () {
                    $(".theadfix_wrapper").floatThead({
                        position: "absolute",
                        scrollContainer: true,
                    });
                }, 1000);

            } else {
                toastr.warning("Bill already paid");
            }

        },
        complete: function () {
            $('#approveDiscountRequestBtn' + request_id).attr('disabled', false);
            $('#approveDiscountRequestSPin' + request_id).removeClass('fa fa-spinner fa-spin');
            $('#approveDiscountRequestSPin' + request_id).addClass('fa fa-list');
            $("#approveDiscountList").LoadingOverlay("hide");
            calculateDiscountValue();
        }
    });
}

function calculateDiscountValue() {
    var discount_type = $('#approve_discount_type').val();
    var discount_value = $('#approve_discount_value').val();
    var net_amount = $('#total_netamount').val();
    if (!discount_value) {
        discount_value = 0;
        $('#approve_discount_value').val(0);
    }
    if (!net_amount) {
        net_amount = 0;
    }
    cal_amount = 0;
    if (parseInt(discount_type) == 1) {
        if (discount_value <= 100) {
            cal_amount = (parseFloat(discount_value) * parseFloat(net_amount)) / 100;
            $('#approve_calculated_bill_amt').val(checkIsNaN(cal_amount));
            cal_amount = parseFloat(net_amount) - parseFloat(cal_amount);
        } else {
            toastr.warning("Percentage cannot be greater than 100");
            $('#approve_discount_value').val(0);
            $('#approve_calculated_bill_amt').val(0);
        }

    } else if (parseInt(discount_type) == 2) {
        cal_amount = parseFloat(net_amount) - parseFloat(discount_value);
        $('#approve_calculated_bill_amt').val(checkIsNaN(discount_value));
    } else {
        $('#approve_discount_value').val(0);
        $('#approve_calculated_bill_amt').val(0);
        cal_amount = net_amount;
    }
    if (parseFloat(cal_amount) < 0) {
        cal_amount = 0;
        $('#approve_discount_value').val(0);
        $('#approve_calculated_bill_amt').val(0);
        toastr.warning("Net amount less than discount amount");
    }
    $('#approve_calculated_value').val(checkIsNaN(cal_amount));
}



function saveDiscountRequest(post_type) {
    var save_approve = "Approve";
    if (parseInt(post_type) == 3) {
        save_approve = "Reject";
    }
    var url = base_url + "/discount/saveDiscountRequest";
    var request_id = $('#request_id').val();
    var discount_type = $('#approve_discount_type').val();
    var discount_value = $('#approve_discount_value').val();
    var discount_amount = $('#approve_calculated_bill_amt').val();
    var bill_amount = $('#approve_calculated_value').val();
    var net_amount = $('#total_netamount').val();

    var param = {
        request_id: request_id,
        post_type: post_type,
        discount_type: discount_type,
        discount_value: discount_value,
        discount_amount: discount_amount,
        bill_amount: bill_amount,
        net_amount: net_amount
    };

    bootbox.confirm({
        message: "Are you sure,you want to " + save_approve,
        buttons: {
            'confirm': {
                label: "Yes",
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'No',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#saveDiscountRequestBtn' + post_type).attr('disabled', true);
                        $('#saveDiscountRequestSpin' + post_type).removeClass('fa fa-save');
                        $('#saveDiscountRequestSpin' + post_type).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            searchDiscountRequestedBills();
                            resetApproveBill();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $('#saveDiscountRequestBtn' + post_type).attr('disabled', false);
                        $('#saveDiscountRequestSpin' + post_type).removeClass('fa fa-spinner fa-spin');
                        $('#saveDiscountRequestSpin' + post_type).addClass('fa fa-save');
                    }
                });
            }
        }
    });
}


function resetApproveBill() {
    var html_string = "<div class='green' style='text-align: center;margin-top:35vh'> <h4> Please Select any Bill </h4> </div>";
    $('#approveDiscountList').html(html_string);
}
