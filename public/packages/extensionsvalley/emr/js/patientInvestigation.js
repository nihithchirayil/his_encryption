$(document).ready(function() {


    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    var today = $('#current_date').val();
    $('#from_date').val(today);
    $('#to_date').val(today);
    $('.bill_date').val(moment().format('DD-MMM-YYYY HH:mm:ss'));
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });


    patientInvestiGationList();
});



function patientInvestiGationList() {
    var base_url = $('#base_url').val()
    var url = base_url + "/service_bill/patientInvestigationList";

    var visit_type = $("input[name='visit_type']:checked").val();
    console.log(visit_type);

    var uhid = $('#conuhid').val();
    var patient_name = $('#intent_patient_name').val();
    var doctor_name = $('#condoctor_hidden').val();
    var investigation_type = $('#contype').val();
    var investigation_no = $('#investigation_no').val();

    var fromDate = $('#from_date').val();
    var ToDate = $('#to_date').val();
    var emergency = $('#emergency').is(':checked');
    var regular = $('#regular').is(':checked');
    var new_admission = $('#new_admission').is(':checked');
    var discharge = $('#discharge').is(':checked');
    var prt_pending = $('#prt_pending').is(':checked');
    var converted = $('#converted').is(':checked');
    var partially = $('#partially').is(':checked');
    var pending = $('#pending').is(':checked');

    var param = {
        doctor_name: doctor_name,
        investigation_no: investigation_no,
        visit_type: visit_type,
        pending: pending,
        partially: partially,
        converted: converted,
        prt_pending: prt_pending,
        discharge: discharge,
        new_admission: new_admission,
        regular: regular,
        emergency: emergency,
        fromDate: fromDate,
        ToDate: ToDate,
        uhid: uhid,
        patient_name: patient_name,
        investigation_type: investigation_type
    };

    console.log(patient_name);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {

            $("#patient_investigation_div").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            });
        },
        success: function(data) {
            console.log(data);
            $('#patient_investigation_div').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        complete: function() {
            $("#patient_investigation_div").LoadingOverlay("hide");

        },
    });


}

function getdetails(head_id, investigation_type) {

    var base_url = $('#base_url').val()
    var url = base_url + "/service_bill/investigationDetails";


    console.log(head_id);
    var param = { head_id: head_id, investigation_type:investigation_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#bill_details").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            });
        },
        success: function(data) {
            console.log(data);
            $('#bill_details').html(data);


        },
        complete: function() {
            $("#bill_details").LoadingOverlay("hide");
            $('.theadfix_detail').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
    });

}


function convert(patient, doctor, head_id, investigation, outsideInvestigation) {

    $('.convert:checked').each(function() {
        var id = $(this).attr('data-head_id');
        var investigation_type = $(this).attr('data-investigation_type');
        var patient_id = $(this).attr('data-patient-id');
        var doctor_id = $(this).attr('data-doctor-id');
        if (outsideInvestigation == 1) {
            toastr.warning('Unknown Item Please Replace');

            $('#conver_' + head_id).prop('checked', false);
            return;
        }

        if ((patient_id != patient) || (doctor_id != doctor) || (investigation_type != investigation)) {

            toastr.warning('Please Select Same Investigation');
            $('#conver_' + head_id).prop('checked', false);
            return;
        }

    });
}



function printInvestigationHistoryList(id) {
    if (id != '' && id != undefined) {
        let url = $('#base_url').val();

        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/print_investigation_history",
            data: {
                investigation_head_id: id,
                _token: _token
            },
            beforeSend: function() {

            },
            success: function(data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html);
                winPrint.document.close();
                winPrint.focus();
                winPrint.print();


            },
            complete: function() {
                $('#print_list').attr('disabled', false);

            }
        });
    }
}


function replaceItems(element) {


    $('#replace-lg').modal('show');

    var detail_id = $(element).parents('tr').attr('data-id');
    var head_id = $(element).parents('tr').attr('data-head_id');
    var item_name = $(element).parents('tr').attr('data-item_name');
    var investigation_type = $(element).parents('tr').attr('data-investigation-type');
    $('#replace_item').val(item_name);
    $('#replace_id').val(detail_id);

    $('#head_id_hidden').val(head_id);
    $('#investigation_type_hidden').val(investigation_type);


}


function replace() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/replaceItem";


    var ser_code = $('#replace_hidden').val();
    var detail_id = $('#replace_id').val();
    var id = $('#head_id_hidden').val();
    var investigation_type = $('#investigation_type_hidden').val();

    var param = { detail_id: detail_id, ser_code: ser_code };

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {

        },
        success: function(data) {
            if (data == 1) {
                toastr.success('Update Successfully.');
            }

        },
        complete: function() {

            $('#replace-lg').modal('hide');
            getdetails(id, investigation_type);
            $('#replace').val('');
            $('#replace').val('');
        },
    });

}

function cancel_item(id, head_id, type) {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/Cancelitem";
    param = { id: id }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {

        },
        success: function(data) {
            if (data == 1) {
                toastr.success('Investigation deleted.');
            }

        },
        complete: function() {

            getdetails(head_id, type);
        },
    });


}

function clearInvestigation() {

    $('#conuhid').val(' ');
    $('#conpatient_name').val(' ');
    $('#condoctor').val(' ');
    $('#contype').val(' ');
    $('#investigation_no').val(' ');
}

function replaceModalclose() {
    // $('#replace-lg').toggle('hide');
    $('#replace-lg').modal('hide');


}
$('#IP').click(function() {

    if ($('#IP').is(':checked')) {
        $('#OP').attr('checked', false);
        $('#select').attr('checked', false);
    }
})
$('#OP').click(function() {

    if ($('#OP').is(':checked')) {
        $('#IP').attr('checked', false);
        $('#select').attr('checked', false);
    }
})

$('#close_patient_inves').click(function() {
    $('#convert_head_id').val('');
})

function refreshForm() {
    var today = $('#current_date').val();
    $('#from_date').val(today);
    $('#to_date').val(today);
    $('#conuhid').val('');
    $('#conuhid_hidden').val('');
    $('#conpatient_name').val('');
    $('#conpatient_name_hidden').val('');
    $('#condoctor').val('');
    $('#condoctor_hidden').val('');
    $('#contype').val('');
    $('#contype_hidden').val('');
}