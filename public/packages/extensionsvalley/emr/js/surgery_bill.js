$(document).ready(function () {
    $(function () {
        $("#progressbar li:first-child a").tab("show");
    });
    $("#progressbar li").click(function (e) {
        $(this).find('a').tab('show');
    });

    $(".select2").select2();
    addNewAdditionalService();
});

$(document).on("change", ".surgery_name", function(){
    getSurgeryDetails();
});

$(document).on("keyup", ".division_input", function(){
    calculateTotalSurgeryAmount();
});

$(document).on("keyup", ".surgeon_charge", function(){
    $(".bill_division_details").empty();
    calculateTotalSurgeryAmount();
});


var base_url = $('#base_url').val()
var token = $('#c_token').val()
var instrument_list = [];
var instrument_row_id = 1;
var medicine_list = [];
var medicine_row_id = 1;
var consumables_list = [];
var consumables_row_id = 1;
var check_list_row_id = 1;
var equipment_list = [];
var equipment_row_id = 1;

function getSurgeryDetails(){
    var url = base_url+"/surgerybilling/getSurgeryDetails";
    var surgery_id = $(".surgery_name").val();

    $(".surgeon_charge").val('');
    $(".bill_division_details").empty();
    calculateTotalSurgeryAmount();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            surgery_id :surgery_id,
            _token : token
        },
        dataType: "json" ,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            console.log()
            $("body").LoadingOverlay("hide");
            if(response.status == 1){

                $('#instrumentlist').empty();
                $('#equipmentlist').empty();
                $.each(response.data.instrument_detail, function(key, val){
                    fillSearchDetials(val.service_code,val.service_desc,"instrument_list",val.amount);
                });

                $.each(response.data.equipments_detail, function(key, val){
                    fillSearchDetials(val.service_code,val.service_desc,"equipment_list",val.amount);
                });
                // $.each(response.data.medicine_detail, function(key, val){
                //     fillMedicineData(val.item_code,val.item_desc,"medicine_serach","0",val.amount,val.generic_name)
                // });
                // $.each(response.data.consumable_detail, function(key, val){
                //     fillMedicineData(val.item_code,val.item_desc,"consumables_search","0",val.amount)
                // });



            }
        },
        complete: function () { }
    });
}


function getBillDivisionDetails(){
    var url = base_url+"/surgerybilling/getBillDivisionDetails";
    var surgery_id = $(".surgery_name").val();
    var surgeon_charge = $(".surgeon_charge").val() ? $(".surgeon_charge").val() :0;
    var request_id = window.surgery_request_id ? window.surgery_request_id : 0;
    if(!surgery_id){
        toastr.error("Please select surgery.");
        return;
    }
    if(!surgeon_charge){
        toastr.error("Please enter surgeon charge.");
        return;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: {
            surgery_id :surgery_id,
            surgeon_charge :surgeon_charge,
            request_id :request_id,
            _token : token
        },
        dataType: "json" ,
        beforeSend: function () {
            $(".getSugeryDetailsBtn").find('i').removeClass('fa-cog').addClass('fa-spin').addClass('fa-spinner');
            $(".bill_division_details").empty();
            $(".bill_division_details").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            console.log(response);
            $(".getSugeryDetailsBtn").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-cog');
            $(".bill_division_details").LoadingOverlay("hide");
            if(response.status == 1){

                $.each(response.data.surgery_detail, function(key, val){
                    $(".bill_division_details").append('<div class="col-md-6 padding_sm"> <div class="mate-input-box"> <label for="">'+val.division+' ('+val.percentage+'%) </label> <div class="clearfix"></div> <input class="form-control bill_division division_input" value="'+val.amount+'" data-service-code="'+val.service_code+'" data-division-id="'+val.division_id+'" autocomplete="off" type="text"> </div></div>');
                });

                calculateTotalSurgeryAmount();
            }
        },
        complete: function () { }
    });
}


function addNewAdditionalService(){
    var length = 0;
    $("#additional_services_table_tbody").find('tr').each(function(key, val){
        if($(val).find('.service_description').val() == ""){
            length = 1;
        }
    });
    if(length == 0){
        $("#additional_services_table_tbody").append('<tr class="service_item_row"><td class="serial_no">1</td><td><input type="text" class="form-control service_description" value="" /><div class="services-row-listing" style="display: none; position:relative;"><a style="float: left;" class="close_btn_service">X</a><div style="position: relative;"><table class="table table-bordered no-margin table_sm table-striped"><thead><tr class="light_purple_bg" style="background: green !important; color: white !important;"><th>Service Item</th><th>Price</th></tr></thead><tbody class="services-row-listing-search-data" ></tbody></table></div></div></td><td><input type="text" class="form-control service_price division_input" value="" /></td><td><button onclick="removeAdditionalService(this);" type="type" class="btn btn-default" ><i class="fa fa-trash red"></i></button></td></tr>');
    }

    resetAdditionalServiceSerialNumber();
}

function resetAdditionalServiceSerialNumber(){
    var i = 1;
    $("#additional_services_table_tbody").find('.serial_no').each(function(key, val){
        $(val).html(i);
        i++;
    });
}

function removeAdditionalService(btn){
    var service = $(btn).parents('tr').find('.service_description').val();
    if(service == ""){
        $(btn).parents('tr').remove();
    } else {

        bootbox.confirm({
            message: 'Are you sure want to delete this additional service ?',
            buttons: {
                'confirm': {
                    label: 'Confirm',
                    className: 'btn-success',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result) {
                    $(btn).parents('tr').remove();
                }
            }
        });
    }
    calculateTotalSurgeryAmount();
}


//medicine search listing(edit row)
var timeout = null;
var last_search_string = '';
$(document).on('keyup', '.service_description', function (event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        return false;
    }
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val().trim();
    let tr = $(obj).closest('tr');

    if (search_string == "") {
        $(tr).find('.services-row-listing').hide();
        $(tr).find('.services-row-listing').find('.services-row-listing-search-data').empty();
    }

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        var med_list = $(tr).find('.services-row-listing');
        $(med_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/surgerybilling/searchAdditionalServiceItem";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    search_key_string: search_string,
                },
                beforeSend: function () {
                    $(tr).find('.services-row-listing').find('.services-row-listing-search-data').empty();
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                },
                success: function (response) {
                    $("body").LoadingOverlay("hide");
                    $.each(response.data, function(key, val){
                        $(tr).find('.services-row-listing').find('.services-row-listing-search-data').append('<tr class="row-'+val.service_code+'" onclick="selectAdditionalServiceItem(\''+val.service_desc+'\', \''+val.price+'\', \''+val.service_code+'\');"><td style="text-align:left;">'+val.service_desc+'</td><td style="text-align:left;">'+val.price+'</td></tr>');
                    });

                },
                complete: function () {

                }
            });
        }, 500)

    }

});

function selectAdditionalServiceItem(service_name, price, service_code){
    $(".services-row-listing").hide();
    $(".row-"+service_code).parents('tr.service_item_row').find('.service_description').val(service_name);
    $(".row-"+service_code).parents('tr.service_item_row').find('.service_description').attr('data-service-code', service_code);
    $(".row-"+service_code).parents('tr.service_item_row').find('.service_price').val(price);
    $(".row-"+service_code).parents('tr').find('.services-row-listing').find('.services-row-listing-search-data').empty();
    calculateTotalSurgeryAmount();
}


function calculateTotalSurgeryAmount(){
    var total = 0;
    $(".division_input").each(function(key, val){
        var amount = $(val).val() ? $(val).val() : 0;
        total = parseFloat(total) + parseFloat(amount);
    })

    $("#surgery_charge").html(parseFloat(total).toFixed(2));
}

function getPreviousTab() {
    $(".nav-tabs > .active").prev("li").find("a").tab("show");
}


$('#service_item').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('service_itemAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#service_item').val();
        if (search_key == "") {
            $('#service_itemAjaxDiv').hide();
            $("#service_item_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var param = { _token: token, search_key: search_key, search_key_id: 'service_item' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#service_itemAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#service_itemAjaxDiv").html(html).show();
                    $("#service_itemAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('service_itemAjaxDiv', event);
    }
});


$('#medicine_serach').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('medicine_serachAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#medicine_serach').val();
        if (search_key == "") {
            $('#medicine_serachAjaxDiv').hide();
            $("#medicine_serach_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(medicine_list);
            var medicine_search = $('input[name="medicine_name_search"]:checked').val();
            var param = { _token: token, medicine_search: medicine_search, list_array: listArrayString, search_key: search_key, search_key_id: 'medicine_serach' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#medicine_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#medicine_serachAjaxDiv").html(html).show();
                    $("#medicine_serachAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('medicine_serachAjaxDiv', event);
    }
});



$('#consumables_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('consumables_serachAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#consumables_search').val();
        if (search_key == "") {
            $('#consumables_serachAjaxDiv').hide();
            $("#consumables_search_hidden`").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(consumables_list);
            var medicine_search = $('input[name="medicine_name_search"]:checked').val();
            var param = { _token: token, medicine_search: medicine_search, list_array: listArrayString, search_key: search_key, search_key_id: 'consumables_search' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#consumables_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#consumables_serachAjaxDiv").html(html).show();
                    $("#consumables_serachAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('consumables_serachAjaxDiv', event);
    }
});



$('#instrument_list').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('instrument_listAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#instrument_list').val();
        if (search_key == "") {
            $('#instrument_listAjaxDiv').hide();
            $("#instrument_list_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(instrument_list);
            var param = { _token: token, list_array: listArrayString, search_key: search_key, search_key_id: 'instrument_list' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#instrument_listAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#instrument_listAjaxDiv").html(html).show();
                    $("#instrument_listAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('instrument_listAjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function updateListData(id, name, serach_key_id, price) {
    if (serach_key_id == 'instrument_list') {
        $('#' + serach_key_id).val('');
        addInstrument(name, price, id);
    } else if (serach_key_id == 'equipment_list') {
        $('#' + serach_key_id).val('');
        addequipment(name, price, id);
    }
}

function fillSearchDetials(id, name, serach_key_id, price) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(htmlDecode(name));
    $('#' + serach_key_id).attr('title', htmlDecode(name));
    $('.ajaxSearchBox').hide();
    updateListData(id, name, serach_key_id, price);
}

function fillMedicineData(code, desc, serach_key_id, stock, price, generic_name, quantity = 1) {
    $('#' + serach_key_id + '_hidden').val(code);
    $('#' + serach_key_id).val('');
    $('.ajaxSearchBox').hide();
    if (serach_key_id == 'medicine_serach') {
        addMedicineListData(code, desc, price, generic_name, quantity);
    } else if (serach_key_id == 'consumables_search') {
        addConsumablesListData(code, desc, price, quantity);
    }
}

function validateNumber(obj) {
    obj.value = obj.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
}

function addInstrument(name, price, code) {
    $('#instrumentlist').append('<tr class="Instrument_class" id="Instrument_row_id' + instrument_row_id + '"><td class="common_td_rules Instrument_count_class">'+instrument_row_id+'</td><td class="common_td_rules">' + name + '</td><td class="Instrument_price td_common_numeric_rules"> <input type="text" class="instrument_price_input form-control" value="' + price + '"> <input class="form-control" type="hidden" value="' + code + '" name="Instrument_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="Instrument_price"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + instrument_row_id + ', \'' + code + '\', \'Instrument\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    instrument_row_id++;
    instrument_list.push(code);
    calculateInstrumentTotal();
}

function calculateInstrumentTotal(){
    var total = 0;
    $('.instrument_price_input').each(function(key, value){
        total = total + ($(value).val() ? parseFloat($(value).val()) : 0);
    })

    $('#Instrument_charge').html(number_format(total, 2, '.', ''));
}

function addequipment(name, price, code) {
    $('#equipmentlist').append('<tr class="equipment_class" id="equipment_row_id' + equipment_row_id + '"><td class="common_td_rules equipment_count_class"></td><td class="common_td_rules">' + name + '</td><td class="td_common_numeric_rules"><input class="form-control equipment_price" oninput="validateNumber(this)" value="' + price + '" autocomplete="off" onkeyup="getTotalInstrumentPrice(\'equipment\')" type="text" id="equipment_price' + equipment_row_id + '" name="equipment_price"> <input class="form-control" type="hidden" value="' + code + '" name="equipment_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="equipment_price"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + equipment_row_id + ', \'' + code + '\', \'equipment\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    equipment_row_id++;
    equipment_list.push(code);
    getTotalInstrumentPrice('equipment');
}

function addMedicineListData(code, desc, price, generic_name, quantity) {
    $('#medicinelist').append('<tr class="Medicine_class" id="Medicine_row_id' + medicine_row_id + '"><td class="common_td_rules Medicine_count_class"><td class="common_td_rules">' + desc + '</td><td class="common_td_rules">' + generic_name + '</td><td class="common_td_rules"><input class="form-control" type="hidden" value="' + code + '" name="medicine_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="medicine_price"><input class="form-control" oninput="validateNumber(this)" value="'+quantity+'" autocomplete="off" onkeyup="getListTotalQty(' + medicine_row_id + ',\'Medicine\',' + price + ')" type="text" id="Medicine_qty' + medicine_row_id + '" name="medicine_qty"></td> <td class="td_common_numeric_rules"> ' + price + ' </td><td id="Medicine_price_total' + medicine_row_id + '" class="Medicine_price td_common_numeric_rules"> ' + price + ' </td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + medicine_row_id + ', \'' + code + '\', \'Medicine\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    medicine_row_id++;
    medicine_list.push(code);
    calculateTableDataPrice('Medicine');
}

function addConsumablesListData(code, desc, price, quantity) {
    $('#consumableslist').append('<tr class="Consumables_class" id="Consumables_row_id' + consumables_row_id + '"><td class="common_td_rules Consumables_count_class"><td class="common_td_rules">' + desc + '</td><td class="common_td_rules"><input class="form-control" type="hidden" value="' + code + '" name="consumables_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="consumables_price"><input class="form-control" oninput="validateNumber(this)" value="'+quantity+'" autocomplete="off" onkeyup="getListTotalQty(' + consumables_row_id + ',\'Consumables\',' + price + ')" type="text" id="Consumables_qty' + consumables_row_id + '" name="Consumables_qty"></td><td class="td_common_numeric_rules"> ' + price + ' </td><td id="Consumables_price_total' + consumables_row_id + '" class="Consumables_price td_common_numeric_rules"> ' + price + ' </td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + consumables_row_id + ', \'' + code + '\', \'Consumables\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    consumables_row_id++;
    consumables_list.push(code);
    calculateTableDataPrice('Consumables');
}

function getListTotalQty(list_id, from_type, price) {
    var qty = $('#' + from_type + '_qty' + list_id).val();
    var total = parseFloat(price * qty);
    $('#' + from_type + '_price_total' + list_id).html(number_format(total, 2, '.', ''));
    calculateTableDataPrice(from_type);
}


function calculateTableDataPrice(from_type) {
    var total = 0.0;
    if ('.' + from_type + '_price') {
        $('.' + from_type + '_price').each(function (index) {
            var price = $(this).html();
            total += parseFloat(price);
        });
        $('#' + from_type + '_charge').html(number_format(total, 2, '.', ''));
    }
    if ('.' + from_type + '_class') {
        row_ct = 1;
        $('.' + from_type + '_class').each(function (i) {
            $(this).find('.' + from_type + '_count_class').text(row_ct);
            row_ct++;
        });
    }
}


function getTotalInstrumentPrice(from_type) {
    var total = 0.0;
    var price = 0.0;
    if ('.' + from_type + '_price') {
        $('.' + from_type + '_price').each(function (index) {
            price = 0.0;
            if ($(this).val()) {
                price = $(this).val();
            }
            total += parseFloat(price);
        });
        $('#' + from_type + '_charge').html(number_format(total, 2, '.', ''));
    }
    if ('.' + from_type + '_class') {
        row_ct = 1;
        $('.' + from_type + '_class').each(function (i) {
            $(this).find('.' + from_type + '_count_class').text(row_ct);
            row_ct++;
        });
    }
}




function removeTableData(list_id, code, from_type) {
    flag = 1;
    if (from_type == 'Notes') {
        var notes = $('#bill_check_list_notes' + list_id).val();
        if (!notes) {
            flag = 0;
            $('#' + from_type + '_row_id' + list_id).remove();
            calculateTableDataPrice(from_type);
        }
    }
    if (flag == 1) {
        bootbox.confirm({
            message: "Are your sure to want remove this " + from_type + " ?",
            buttons: {
                'confirm': {
                    label: "Remove",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#' + from_type + '_row_id' + list_id).remove();
                    if (from_type == 'Instrument') {
                        var index = instrument_list.indexOf(code);
                        if (index > -1) {
                            instrument_list.splice(index, 1);
                        }
                    } else if (from_type == 'Medicine') {
                        var index = medicine_list.indexOf(code);
                        if (index > -1) {
                            medicine_list.splice(index, 1);
                        }
                    } else if (from_type == 'Consumables') {
                        var index = consumables_list.indexOf(code);
                        if (index > -1) {
                            consumables_list.splice(index, 1);
                        }
                    }
                    calculateTableDataPrice(from_type);
                }
            }
        });
    }
}

function searchbyName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("result_data_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function getDepartmentlist() {
    var url = base_url + "/surgery/getDepartmentList";
    $.ajax({
        type: "GET",
        url: url,
        data: {},
        beforeSend: function () {
            $('#department_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#department_list').html(html);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

        },
        complete: function () {
            $('#department_list').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}


function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

$(document).on("click", "#item_search_btn", function (e) {
    $('#issue_search_box').focus();
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
});


function gotoNextTab(){
    $(".nav-tabs > .active").next("li").find("a").tab("show");
}


function fetchSurgeryRequestDetails(request_id){
    var url = base_url+"/surgerybilling/fetchSurgeryRequestDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            request_id :request_id,
            _token : token
        },
        dataType: "json" ,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            $("body").LoadingOverlay("hide");
            if(response.status == 1){
                var patient_id = response.data.patient_id;
                var patient_name = response.data.patient_name;
                var uhid = response.data.uhid;
                var surgeon = response.data.surgeon;
                var anaesthetist = response.data.anaesthetist;
                $(".surgery_name").select2("destroy");
                $(".surgery_name").empty();

                $.each(response.data.surgery_list, function(key, val){
                    $(".surgery_name").append('<option value="'+val.id+'">'+val.name+'</option>');
                })
                $(".surgery_name").select2();
                getSurgeryDetails();

                $("#payment_type").select2("destroy");
                $("#payment_type").empty();

                $.each(response.data.payment_types, function(key, val){
                    $("#payment_type").append('<option value="'+val.code+'">'+val.name+'</option>');
                })
                $("#payment_type").select2();

                $("#op_no").val(uhid).attr('disabled', true);
                $("#patient_name").val(patient_name);
                $("#surgeon").val(surgeon).trigger('change').attr('disabled', true);
                $("#anaesthetist").val(anaesthetist).trigger('change').attr('disabled', true);

            }
        },
        complete: function () { }
    });
}

function convertToJSON(){
    var bill_details = {};
    var surgery_details ={};
    surgery_details.uhid = $("#op_no").val();
    surgery_details.payment_type = $("#payment_type").val();
    surgery_details.surgeon = $("#surgeon").val();
    surgery_details.anaesthetist = $("#anaesthetist").val();
    surgery_details.surgery_id = $(".surgery_name").val();
    surgery_details.surgeon_charge = $(".surgeon_charge").val();

    if(!surgery_details.uhid){
        toastr.error('Please select patient.');
        return false;
    }
    if(!surgery_details.payment_type){
        toastr.error('Please select payment type.');
        return false;
    }
    if(!surgery_details.surgeon){
        toastr.error('Please select surgeon.');
        return false;
    }
    if(!surgery_details.anaesthetist){
        toastr.error('Please select anaesthetist.');
        return false;
    }
    if(!surgery_details.surgery_id){
        toastr.error('Please select surgery.');
        return false;
    }
    if(!surgery_details.surgeon_charge){
        toastr.error('Please enter surgeon charge.');
        return false;
    }
    surgery_details.bill_division_details = [];
    surgery_details.additional_services = [];
    $(".bill_division").each(function(key, val){
        var division = {};
        division.service_code = $(val).attr('data-service-code');
        division.amount = $(val).val();
        division.division_id = $(val).attr('data-division-id');
        surgery_details.bill_division_details.push(division);
    });
    $("#additional_services_table_tbody").find('tr').each(function(key, val){
        if($(val).find('.service_description').attr('data-service-code')){
            var services = {};
            services.service_code = $(val).find('.service_description').attr('data-service-code');
            services.amount = $(val).find('.service_price').val() ? $(val).find('.service_price').val() : 0;
            surgery_details.additional_services.push(services);
        }
    })
    bill_details.surgery_details = surgery_details;

    if(surgery_details.bill_division_details.length == 0){
        toastr.error('Please fetch bill division details.');
        return false;
    }

    var instrument_details = [];

    $("#instrumentlist").find('tr').each(function(key, val){
        var instrument = {};
        instrument.service_code = $(val).find('input[name="Instrument_serive_code"]').val();
        instrument.amount = $(val).find('.instrument_price_input').val();
        instrument_details.push(instrument);
    })
    bill_details.instrument_details = instrument_details;

    var equipment_details = [];

    $("#equipmentlist").find('tr').each(function(key, val){
        var equipment = {};
        equipment.service_code = $(val).find('input[name="equipment_serive_code"]').val();
        equipment.amount = $(val).find('input[name="equipment_price"]').val();
        equipment_details.push(equipment);
    })
    bill_details.equipment_details = equipment_details;

    // var medicine_details = [];

    // $("#medicinelist").find('tr').each(function(key, val){
    //     var medicine = {};
    //     medicine.product_code = $(val).find('input[name="medicine_serive_code"]').val();
    //     medicine.quantity = $(val).find('input[name="medicine_qty"]').val();
    //     medicine_details.push(medicine);
    // })
    // bill_details.medicine_details = medicine_details;

    // var consumable_details = [];

    // $("#consumableslist").find('tr').each(function(key, val){
    //     var consumable = {};
    //     consumable.product_code = $(val).find('input[name="consumables_serive_code"]').val();
    //     consumable.quantity = $(val).find('input[name="Consumables_qty"]').val();
    //     consumable_details.push(consumable);
    // })
    // bill_details.consumable_details = consumable_details;

    return bill_details;

}

function saveSurgeryBill(){
    var request_json = convertToJSON();
    var request_id = window.surgery_request_id ? window.surgery_request_id : 0;
    console.log(request_json);

    if(request_json){
        var url = base_url+"/surgerybilling/saveSurgeryBill";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                request_json :request_json,
                request_id:request_id,
                _token : token
            },
            dataType: "json" ,
            beforeSend: function () {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (response) {
                $("body").LoadingOverlay("hide");
                if(response.code == 100){
                    toastr.success('Bill saved successfully.');
                    if(window.surgery_request_id){
                        window.location.href = base_url+ "/ot_schedule/OTSchedule";
                    } else {
                        window.location.href = base_url+ "/surgery/SurgeryBillList";
                    }
                }
            },
            complete: function () { }
        });
    }


}


$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: "",
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetialsSurgeryBill(id, uhid, name, gender, age, phone, ip_number, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(uhid);
    $("#" + serach_key_id + "AjaxDiv").hide();
    $("#patient_name").val(name);
    getPaymentTypes(id);
}


function getPaymentTypes(patient_id){
    var url = $('#base_url').val() + "/pharmacy/getPaymentTypes";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id:patient_id
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {

            $("#payment_type").select2("destroy");
            $("#payment_type").empty();

            $.each(data.payment_types, function(key, value){
                $("#payment_type").append('<option value="'+key+'">'+value+'</option>');
            })
            $("#payment_type").select2();
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}
