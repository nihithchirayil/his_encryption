$(document).on('click','#company_wise', function() {
    $("#company_wise").prop('checked',true);    
    $(".hide_gender_age").hide();
});
$(document).on('click','#patient_wise', function() {
    $("#patient_wise").prop('checked',true);    
    $(".hide_gender_age").show();
});
$(document).on('keyup', '#out_side_patient_age, #out_side_patient_phone', function() {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9]/g, ""));
});

function saveOutsidePatientReg() {
    var patient_name = $("#out_side_patient_name").val();
    if (!patient_name) {
        patient_name = "Out side patient"
       Command: toastr["error"]("Please enter patient name");
       return;
    }
    var gender = $("#out_side_patient_gender").val();
    var age = $("#out_side_patient_age").val();
    if($('#patient_wise').is(':checked')){
        if (!gender) {
            Command: toastr["error"]("Please select gender");
            return;
        }
        if (!age) {
            Command: toastr["error"]("Please enter age");
            return;
        }
    }
  
    var phone = $("#out_side_patient_phone").val();
//    if (!phone) {
//        Command: toastr["error"]("Please enter phone number");
//        return;
//    }
    var doctor = '';
    var doctor_name = '';
    var external_doctor_check = ($("#out_side_patient_external_doctor_check").prop("checked")) ? 1 : 2;
    if (external_doctor_check == 1) {
        doctor_name = $("#out_side_patient_external_doctor_name").val();
        // doctor_name = "Outside Doctor";
       if (!doctor_name) {
           Command: toastr["error"]("Please enter external doctor name");
           return;
       }
    } else {
        doctor = $("#out_side_patient_doctor").val();
        if (doctor == "") {
            Command: toastr["error"]("Please select doctor");
            return;
        }
    }

    var req_data = {};
    req_data.patient_name = patient_name;
    req_data.gender = gender;
    req_data.age = age;
    req_data.phone = phone;
    req_data.doctor = doctor;
    req_data.doctor_name = doctor_name;
    req_data.external_doctor_check = external_doctor_check;

    var url = $('#base_url').val() + "/pharmacy/registerOutsidePatient";
    $.ajax({
        type: "POST",
        url: url,
        data: req_data,
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            Command: toastr["success"]("Success.");
            req_data.patient_id = data.patient_id;
            req_data.uhid = data.uhid;
            hideOutsidePatientRegForm();
            outsidePatientRegistrationSuccess(req_data);
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });


}

$(document).on('click', "#out_side_patient_external_doctor_check", function() {
    if ($("#out_side_patient_external_doctor_check").prop("checked")) {
        $("#out_side_patient_doctor").attr('disabled', true);
        $("#out_side_patient_external_doctor_name").attr('disabled', false);
    } else {
        $("#out_side_patient_doctor").attr('disabled', false);
        $("#out_side_patient_external_doctor_name").attr('disabled', true);
    }
});


function clearOutsidePatientRegForm() {
    $("#out_side_patient_name").val('');
    $("#out_side_patient_external_doctor_check").prop('checked', true);
    $("#out_side_patient_doctor").attr('disabled', true);
    $("#out_side_patient_external_doctor_name").attr('disabled', false);
    $("#out_side_patient_external_doctor_name").val('');
    $("#out_side_patient_gender").val('')
    $("#out_side_patient_age").val('');
    $("#out_side_patient_phone").val('');
}

function showOutsidePatientRegForm() {
    clearOutsidePatientRegForm();
    $("#patient_wise").prop('checked',true);  
    $(".hide_gender_age").show();  
    $("#outSidePatientRegistrationModal").modal({ backdrop: 'static', keyboard: false });
    $("#out_side_patient_name").focus();


}

function hideOutsidePatientRegForm() {
    $("#outSidePatientRegistrationModal").modal('hide');
}

function closeOutsidePatientRegModal(){
    $("#outSidePatientRegistrationModal").modal('hide');
    closedOutsidePatientRegistrationModal();
}