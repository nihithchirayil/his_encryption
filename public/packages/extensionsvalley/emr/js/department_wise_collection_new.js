$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

document.getElementById("date_from").blur();


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getDepartmentWiseCollectionDataNew() {

    var base_url = $('#base_url').val();
    var token = $('#c_token').val();
    var url = base_url + '/report/departmentwiseCollectionData'

    //-------filters---------------------------------------------

    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var department_id = $('#department_id').val();
    var subdepartment_id = $('#subdepartment_id').val();



    var parm = { _token: token, from_date: from_date, to_date: to_date, department_id: department_id, subdepartment_id: subdepartment_id };


    $.ajax({
        type: "POST",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}


function getsubdepartments(){
    
    department_id=$('#department_id').val();
    var base_url = $('#base_url').val();
    var url = base_url + '/report/getSubdepartments'

    var parm = { department_id:department_id};

    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function() {
            $('#search_results').attr('disable',true);
            $('#search_results_spin').removeClass('fa fa-search');
            $('#search_results_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            $('#subdepatmentdiv').html(data)
        },

        complete: function() {
            $('#search_results').attr('disable',false);
            $('#search_results_spin').removeClass('fa fa-spinner fa-spin');
            $('#search_results_spin').addClass('fa fa-search');
            $('#subdepartment_id').select2();
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });
}

function resetFilter() {
    var current_date = $('#current_date').val();
    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
    getDepartmentWiseCollectionDataNew();
}
