
$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    $('#servicename_add').attr('disabled',false)
    $("#hospital_share").on('keyup',function(){
        var hosptal_share=$("#hospital_share").val();
        var vendor_val=100-hosptal_share;
        $("#vendor_share").val(vendor_val);
        if(hosptal_share>100){
            Command: toastr["warning"]("Percentage must equal 100%");
            $("#vendor_share").val('');

        }
    })
    $("#vendor_share").on('keyup',function(){
        var vendor_share=$("#vendor_share").val();
        var hospital_val=100-vendor_share;
        $("#hospital_share").val(hospital_val);
        if(vendor_share>100){
            Command: toastr["warning"]("Percentage must equal 100%");
            $("#hospital_share").val('');
        }
    })
  
   
    window.search_url = $(".table_body_contents").attr('data-search-url');
    getFilteredItems(window.search_url);

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        if($(e.target).parent().hasClass('active') == false){
            var url = $(this).attr("href");
            getFilteredItems(url);
        }
        
    });
    
    $(document).on('click', '.searchBtn', function (e) {
        getFilteredItems(window.search_url);
    });
    $(document).on("click", function(event){
        var $trigger = $(".ajaxSearchBox");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $(".ajaxSearchBox").hide();
        }            
    });

});
function isNumber(txt, evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46) {
      //Check if the text already contains the . character
      if (txt.value.indexOf('.') === -1) {
        return true;
      } else {
        return false;
      }
    } else {
      if (charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return false;
    }
    return true;
  }
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
var input_id = '';
var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
var value = event.key; //get the charcode and convert to char
input_id = $(this).attr('id');
//alert(input_id);return;
var current;
if (value.match(keycheck) || event.keyCode == '8') {
    if ($('#' + input_id + '_hidden').val() != "") {
        $('#' + input_id + '_hidden').val('');
    }
    var search_key = $(this).val();
    search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
    search_key = search_key.trim();

    var department_hidden = $('#department_hidden').val();
   
    var datastring = '';
    if (input_id == 'sub_department') {
        datastring = '&department_id=' + department_hidden;
    }
    if (input_id == 'scheme') {
        datastring = '&company_id=' + company_hidden;
    }


    if (search_key == "") {
        $("#" + input_id + "AjaxDiv").html("");
    } else {
        var base_url=$('#base_url').val();
        var url = base_url + "/master/ajax_lab_vendor_share_search";
        $.ajax({
            type: "POST",
            url: url,
            data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
            beforeSend: function () {

                $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
           success: function (html) {
                if (html == 0) {
                    $("#" + input_id + "AjaxDiv").html("No results found!").show();
                    $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                } else {
                    $("#" + input_id + "AjaxDiv").html(html).show();
                    $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                }
            },
            complete: function () {
                //  $('#loading_image').hide();
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    }
} else {
    ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
}
});

function fillSearchDetials(id, name, serach_key_id) {
$('#' + serach_key_id + '_hidden').val(id);
$('#' + serach_key_id).val(name);
$('#' + serach_key_id).attr('title', name);
$("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
var input_id = '';
input_id = $(this).attr('id');
if (event.keyCode === 13) {
    ajaxListKeyEnter(input_id + 'AjaxDiv');
    return false;
}
});
function getFilteredItems(url){
    var data = {};
    data.service_name = $("#service_name").val();
    data.service_id = $("#service_name_hidden").val();
        $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
             $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (data) {
            $(".table_body_contents").LoadingOverlay("hide");
            $(".table_body_contents").html(data.html);
            $(".page-item").attr("disabled", false);
            $(".page-item.active").attr("disabled", true);
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
        complete: function () {
            
        }
    });

}
function selectservice(){
    var price=$("#servicename_add option:selected").attr("price");
    $("#price").val(price);
}
function saveForm(){
    var hospital=$("#hospital_share").val();
    var vendor=$(".vendor_share").val();
    var persent=hospital+vendor;
   
    $('#servicename_add').attr('disabled',false)
    var url = $(".table_body_contents").attr('data-action-url');
    var data = {};

    if($("#servicename_add").val().trim() == ''){
        Command: toastr["error"]("Please enter Service Name");
        return;
    }
    if($("#hospital_share").val().trim() == ''){
        Command: toastr["error"]("Please enter Hospital Share");
        return;
    }
    if($("#vendor_share").val().trim() == ''){
        Command: toastr["error"]("Please enter Vendor Share");
        return;
    }
    data.lab_vendor_share_id = $(".reference_id").val();
    data.service_name = $("#servicename_add").val();
    data.hospital_share = $("#hospital_share").val();
    data.vendor_share = $(".vendor_share").val();
    data.status = $(".status_add").val();
   
    if($(".status_add").prop('checked') == true){
        // alert("chcked");
        data.status =1;
    }else{
        data.status =0;
    }
    // if(persent>100 || persent<100){
    //     Command: toastr["warning"]("Please enter valid percentage");
    // }
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            $('.saveButton').attr('disabled', true);

        },
        success: function (data) {  
            if(data==1){
                Command: toastr["error"]("Service Name Already Exist!");
                $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                $('.saveButton').attr('disabled', false);

                }else{
                $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                $('.saveButton').attr('disabled', false);
                Command: toastr["success"]("success");
                getFilteredItems(window.search_url);
                resetForm();
                }
        },
        complete: function () {

        }
    });
}

function editItem(element){
    $('#servicename_add').attr('disabled',true)
    let lab_vendor_share_id = $(element).parents('tr').attr('data-id');
    let service_name = $(element).parents('tr').attr('data-service_name')
    let hospital_share = $(element).parents('tr').attr('data-hospital_share')
    let vendor_share = $(element).parents('tr').attr('data-vendor_share')
    let status = $(element).parents('tr').attr('data-status');
    let price = $(element).parents('tr').attr('data-price');
    if(status==1){
             $(".status_add").prop("checked", true);

        } else{
             $(".status_add").prop("checked", false);
               }
              
    $(".reference_id").val(lab_vendor_share_id);
    $("#servicename_add").val(service_name).select2().trigger('change');
    $("#hospital_share").val(hospital_share);
    $("#vendor_share").val(vendor_share);
    $("#price").val(price);
  
    $(".saveButton").html('<i class="fa fa-save"></i> Update');
   
    $("#add").html('Edit Lab Vendor Share');

}
function resetForm1(){
    $("#service_name").val('');
    $("#vendor_share").val('');
    $("#hospital_share").val('');
    $('#status').val('');
    $(".table_body_contents").LoadingOverlay("hide");
    getFilteredItems(window.search_url);
}

function resetForm(){
    $(".reference_id").val('');
    $("#servicename_add").val('').trigger('change');
    $("#vendor_share").val('');
    $("#price").val('');
    $("#hospital_share").val('');
    $("#add").html('Add Lab Vendor Share');
    $('#status_add').removeAttr('checked');
    $(".saveButton").html('<i class="fa fa-save"></i> Save');
}

