$(document).ready(function () {
    getPatientList();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});

var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val()

function getPatientList() {
    var url = base_url + '/patient_document/searchPatientDocument';
    var from_date = $("#document_from_date").val();
    var to_date = $("#document_to_date").val();
    var document_type = $("#patient_document_type").val();
    var patient_id = $("#searchpatient_name_hidden").val();
    var param = {
        patient_id: patient_id,
        document_type: document_type,
        from_date: from_date,
        to_date: to_date
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getPatientListBtn").attr("disabled", true);
            $("#getPatientListSpin").removeClass("fa fa-search");
            $("#getPatientListSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function (res) {
            $("#listPatientDataDivs").html(res);
            $(".theadscroll").perfectScrollbar("update");
        },
        complete: function () {
            $("#getPatientListBtn").attr("disabled", false);
            $("#getPatientListSpin").removeClass("fa fa-spinner fa-spin");
            $("#getPatientListSpin").addClass("fa fa-search");
        }
    });
}


$(document).on("keyup", ".hidden_search", function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var input_id = $(this).attr("id");
    var ajax_div = input_id + "AjaxDiv";
    var search_key_id = "";
    if (input_id == "searchpatient_name") {
        search_key_id = "patient";
    }
    if (input_id == "docpatient_name") {
        search_key_id = "docpatient_name";
    }
    if (event.keyCode == 13) {
        ajaxlistenter(ajax_div);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $(this).val();
        search_key = search_key.replace("/[^ws-_.]/gi", "");
        search_key = search_key.trim();
        if (search_key) {
            var url = base_url + "/patient_document/ajaxpatient_document_list";
            var param = {
                search_key: search_key,
                search_key_id: search_key_id,
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            if ($("#" + input_id + "_hidden").val() != "") {
                $("#" + input_id + "_hidden").val("");
            }
        }
    } else {
        ajax_list_key_down(ajax_div, event);
    }
});

function resetDocPatientSearch() {
    $("#document_from_date").val("");
    $("#document_to_date").val("");
    $("#patient_document_type").val("");
    $("#searchpatient_name").val("");
    $("#searchpatient_name_hidden").val("");
    getPatientList();
}

function addDocPatient() {
    $("#getpatientdocument").modal({
        backdrop: "static",
        keyboard: false
    });
    $("#docpatient_name_hidden").val("");
    $("#docpatient_name").val("");
    $("#docpatient_name").prop("readonly", false);
    $("#hidden_patient_id").val(0);
    $("#getpatientdocumentData").html('');
    $('#getpatientdocumentHeader').html('Add Document');
    resetdocument();
}

function editPatientDocument(patient_id) {
    var url = base_url + "/patient_document/editPatientDocument";
    $('#hidden_patient_id').val(patient_id);
    var patient_name = $('#patientName' + patient_id).html();
    $('#docpatient_name').val(patient_name);
    $("#docpatient_name").prop("readonly", true);
    $('#docpatient_name_hidden').val(patient_id);
    var param = {
        patient_id: patient_id
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#editPatientDocumentBtn" + patient_id).attr("disabled", true);
            $("#editPatientDocumentSpin" + patient_id).removeClass("fa fa-edit");
            $("#editPatientDocumentSpin" + patient_id).addClass("fa fa-spinner fa-spin");
        },
        success: function (data) {
            $("#getpatientdocumentData").html(data);
            $("#getpatientdocumentHeader").html(patient_name);
            $("#getpatientdocument").modal({
                backdrop: "static",
                keyboard: false
            });
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);
        },
        complete: function () {
            $("#editPatientDocumentBtn" + patient_id).attr("disabled", false);
            $("#editPatientDocumentSpin" + patient_id).removeClass("fa fa-spinner fa-spin");
            $("#editPatientDocumentSpin" + patient_id).addClass("fa fa-edit");
            $('#patientlevelvisitdata').trigger('click');
            resetdocument();
        }
    });
}


function addPatientDocument(patient_id) {
    var url = base_url + "/patient_document/editPatientDocument";
    $('#hidden_patient_id').val(patient_id);
    var patient_name = $('#patientName' + patient_id).html();
    var param = {
        patient_id: patient_id
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getpatientdocumentData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#getpatientdocumentData").html(data);
            $("#getpatientdocumentHeader").html(patient_name);
            $("#getpatientdocument").modal({
                backdrop: "static",
                keyboard: false
            });
        },
        complete: function () {
            $("#getpatientdocumentData").LoadingOverlay("hide");
            $('#patientlevelvisitdata').trigger('click');
        }
    });
}

function deletedocument(e, id, visit_id) {
    let confirm = window.confirm("Are you sure want to delete ?");
    if (confirm) {
        var url = base_url + "/patient_document/deletedocument";
        var data = {};
        data.document_list_id = id;
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(e)
                    .find("i")
                    .removeClass("fa-trash")
                    .addClass("fa-spinner")
                    .addClass("fa-spin");
            },
            success: function (data) {
                $(e)
                    .find("i")
                    .removeClass("fa-spinner")
                    .removeClass("fa-spin")
                    .addClass("fa-trash");
                $('li[visit_id=' + visit_id + ']').trigger('click');
                Command: toastr["success"]("Successfully Deleted");
            },
            complete: function () {}
        });
    }
}

function patient_level(e, visit_id = 0) {
    var url = base_url + "/patient_document/patientAndVisit_level_document";
    var patient_id = $('#hidden_patient_id').val();
    var data = {
        visit_id: visit_id,
        patient_id: patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(".document_btn_cl").removeClass("patient_doc_cl");
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $("#add_patient_documentdiv").html(data);
            $(e).addClass("patient_doc_cl");
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}

function editdocument(e, id, document_type, file_absolute_path) {
    $("#hidden_document_id").val(id)
    $("#document_type").val(document_type);
    $('.uploadedfile').html(file_absolute_path);
}

function viewfile(e, id, mime_type = "", file_absolute_path, filename) {
    $('.viewfilename').html(file_absolute_path);
    if (id == 0 || id == undefined) {
        Command: toastr["error"]("Error.!");
        return false;
    }
    var url = base_url + "/patient_document/previewpatientDocument";
    var id = id;
    $.ajax({
        type: "GET",
        url: url,
        data: "id=" + id,
        beforeSend: function () {
            $("#viewdocumentfileBtn" + id).attr("disabled", true);
            $("#viewdocumentfileSpin" + id).removeClass("fa fa-eye");
            $("#viewdocumentfileSpin" + id).addClass("fa fa-spinner fa-spin");
            $("#document_data").html("");
            if (
                mime_type != "image/png" ||
                mime_type != "image/jpeg" ||
                mime_type != "image/pdf" ||
                mime_type != "image/doc"
            ) {
                $("#document_data").html(
                    '<iframe src="" width="100%" height="100%" id="myframe"></iframe>'
                );
            }
        },
        success: function (response) {
            if (response == 1) {
                Command: toastr["error"]("File Does not exist!");
                return;
            }
            $("#view_patient_document_modal").modal("show");
            if (
                mime_type == "image/png" ||
                mime_type == "image/jpeg" ||
                mime_type == "image/pdf" ||
                mime_type == "image/doc"
            ) {
                $("#document_data").html(
                    '<div class="" style="width: 50%;height: 450px; margin-right:auto; margin-left:auto;text-align: center;margin-top: 30px;"><img src="' +
                    response +
                    '" class="ezoom" style="max-width: 100%;height: auto"></img><div>'
                );
            } else {
                $("#myframe").attr("src", response);
            }
        },
        complete: function () {
            $("#viewdocumentfileBtn" + id).attr("disabled", false);
            $("#viewdocumentfileSpin" + id).removeClass("fa fa-spinner fa-spin");
            $("#viewdocumentfileSpin" + id).addClass("fa fa-eye");
        }
    });
}

function file_Validate(input_id) {
    var fileInput = document.getElementById(input_id);
    var filePath = fileInput.value;
    if (filePath) {
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
        var file_size = fileInput.size;
        if (!allowedExtensions.exec(filePath) || file_size > 5150) {
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}
// function saveDocument() {
$("#upload_file").on("submit", function (value) {
    value.preventDefault();
    var url = base_url + "/patient_document/savePatientDocument";
    var data = {};
    if ($("#document_type").val().trim() == '') {
        Command: toastr["error"]("Please select Document Type");
        return;
    }
    if ($("#file_upload").val().trim() == '') {
        Command: toastr["error"]("Please select File");
        return;
    }
    var param = new FormData(this);
    var patient_id = $("#docpatient_name_hidden").val();
    var document_id = $("#hidden_document_id").val();
    var visit_id = $(".patient_doc_cl").attr("visit_id");
    var document_type = $("#document_type").val();
    var file_upload = $("#file_upload").val();
    var patient_level = $(".patient_level").attr("patient_level");
    var visit_level = $(".visit_level").attr("visit_level");
    console.log(patient_level);
    console.log(visit_id);
    param.append("visit_id", visit_id);
    param.append("patient_id", patient_id);
    param.append("document_type", document_type);
    param.append("document_id", document_id);
    param.append("visit_level", visit_level);
    param.append("functionName", "documentUpload");
    var image_validate = file_Validate("file_upload");
    if (image_validate) {
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            data: param,
            contentType: false,
            cache: false,
            processData: false,
            functionName: "documentUpload",
            beforeSend: function () {
                $(".saveButton")
                    .find("i")
                    .removeClass("fa-save")
                    .addClass("fa-spinner")
                    .addClass("fa-spin");
                $(".saveButton").attr("disabled", true);
            },
            success: function (data) {
                if (data == 1) {
                    Command: toastr["error"](" Doctor Already Exit!");
                    $(".saveButton")
                    .find("i")
                    .removeClass("fa-spinner")
                    .removeClass("fa-spin")
                    .addClass("fa-save");
                    $(".saveButton").attr("disabled", false);
                }
                else {
                    $(".saveButton")
                        .find("i")
                        .removeClass("fa-spinner")
                        .removeClass("fa-spin")
                        .addClass("fa-save");
                    $(".saveButton").attr("disabled", false);
                    $('li[visit_id=' + visit_id + ']').trigger('click');
                    Command: toastr["success"]("Successfully Uploaded");
                }
            },
            complete: function () {
                resetdocument();
            }
        });
    } else {
        toastr.error("Please upload jpg,jpeg,png,pdf");
    }
    // }
});

function resetdocument() {
    $("#upload_file")[0].reset();
    $("#hidden_document_id").val('')
    $('.uploadedfile').html('');
    $("#document_type").val("").trigger("change");
}


function fillSearchDetials(id, name, serach_key_id) {
    var serach_key_type_id = "";
    if (serach_key_id == "uhid") {
        serach_key_type_id = "searchpatient_uhid";
    } else {
        serach_key_type_id = "searchpatient_name";
    }
    $("#" + serach_key_type_id + "_hidden").val(id);
    $("#" + serach_key_type_id).val(htmlDecode(name));
    $("#" + serach_key_type_id).attr("title", name);
    $("#" + serach_key_type_id + "AjaxDiv").hide();
}

function fillSearchpatientDetials(id, name, serach_key_id) {
    var serach_key_type_id = "";
    if (serach_key_id == "docpatient_name") {
        serach_key_type_id = "docpatient_name";
        $("#getpatientdocumentData").html('');
        addPatientDocument(id);
    }
    $("#" + serach_key_type_id + "_hidden").val(id);
    $("#" + serach_key_type_id).val(htmlDecode(name));
    $("#" + serach_key_type_id).attr("title", name);
    $("#" + serach_key_type_id + "AjaxDiv").hide();
}

function htmlDecode(input) {
    var e = document.createElement("textarea");
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on("keydown", function (event) {
    var input_id = "";
    input_id = $(this).attr("id");
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + "AjaxDiv");
        return false;
    }
});

$(document).mouseup(function (e) {
    var container = $(".ajaxSearchBox");
    // Check if the click event was triggered on the div or one of its children
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        // Close the div
        container.hide();
    }
});
