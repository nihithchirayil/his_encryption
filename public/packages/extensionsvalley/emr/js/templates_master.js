$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $(".hide_table_content").addClass("hidden");

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

    window.search_url = $(".table_body_contents").attr('data-search-url');
    getFilteredItems(window.search_url);

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        if ($(e.target).parent().hasClass('active') == false) {
            var url = $(this).attr("href");
            getFilteredItems(url);
        }

    });

    $(document).on('click', '.searchBtn', function (e) {
        getFilteredItems(window.search_url);
    });
    $(document).on("click", function (event) {
        var $trigger = $(".ajaxSearchBox");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".ajaxSearchBox").hide();
        }
    });

});

var base_url = $('#base_url').val();
var token = $('#token').val();

function getFilteredItems(url) {
    var data = {};
    data.template_control = $("#search_temp_ctrl").val();
    data.search_name = $('#search_ctrl_name').val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(".table_body_contents").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $(".table_body_contents").LoadingOverlay("hide");
            $(".table_body_contents").html(data.html);
            $(".page-item").attr("disabled", false);
            $(".page-item.active").attr("disabled", true);

            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
        complete: function () {

        }
    });

}

function saveForm() {
    var url = $(".table_body_contents").attr('data-action-url');
    var data = {};

    if ($("#template_control_add").val() == '') {
        Command: toastr["error"]("Please select template control");
        return;
    }

    $('#add_service_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
    data.template_id = $(".reference_id").val();
    data.label = $(".template_label_add").val();
    data.control_id = $("#template_control_add").val();
    data.class = $(".template_class_add").val();
    data.placeholder = $(".template_placeholder_add").val();
    data.default_value = $(".default_value_add").val();
    data.sql = $(".sql_add").val();
    data.group_value = $(".group_value_add").val();
    data.size = $(".size_add").val();
    data.container_min_height = $(".container_min_add").val();
    data.row_count = $(".row_count_add").val();
    data.colum_count = $(".colum_count_add").val();
    data.colum_names = $(".colum_names_add").val();
    data.table_header = $(".table_header_add").val();
    data.dataSource = $("#dataSource").val();
    data.master_table_name = $("#template_save_master option:selected").text();
    data.coloumn_table_name = $("#template_save_table_detail").val();
    data.template_default_data_set = $("#template_default_data_set").val();
    data.template_data_set = $("#template_data_set").val();
    data.datset_data_value = $("#datset_data_div").val();
    data.data_saving_primary_key = $(".template_data_saving_primary_key").val();
    data.compare_key_in_emr = $("#template_compare_key_in_emr").val();
    data.data_point = $("#data_point").val();


    // data.service_charge_id_hidden = $("#service_charge_id_hidden").val();

    if ($("#status_add").prop('checked') == true) {
        data.status = 1;
    } else {
        data.status = 0;
    }
    if ($("#is_favourite").prop('checked') == true) {
        data.is_favourite = 1;
    } else {
        data.is_favourite = 0;
    }
    if ($("#is_primary_component").prop('checked') == true) {
        data.is_primary_component = 1;
    } else {
        data.is_primary_component = 0;
    }
    if ($("#hide_in_template").prop('checked') == true) {
        data.hide_in_template = 1;
    } else {
        data.hide_in_template = 0;
    }
    if ($("#hide_in_print").prop('checked') == true) {
        data.hide_in_print = 1;
    } else {
        data.hide_in_print = 0;
    }
    if ($("#is_readonly").prop('checked') == true) {
        data.is_readonly = 1;
    } else {
        data.is_readonly = 0;
    }
    if ($("#is_static_component").prop('checked') == true) {
        data.is_static_component = 1;
    } else {
        data.is_static_component = 0;
    }
    if ($("#is_mandatory").prop('checked') == true) {
        data.is_mandatory = 1;
    } else {
        data.is_mandatory = 0;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
        },
        success: function (data) {
            if (data.status == 101) {
                Command: toastr["error"](data.info);
            } else {
                $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                $('.saveButton').attr('disabled', false);
                Command: toastr["success"](data.info);
                getFilteredItems(window.search_url);
                resetForm();
            }
        },
        complete: function () {
            $('#add_service_area').LoadingOverlay("hide");
        }
    });
}

function editItem(element) {
    let template_id = $(element).parents('tr').attr('data-id');
    let template_content = $(element).parents('tr').attr('data-content');
    template_content = JSON.parse(template_content);
    hideControlContents(template_content.control_id);

    $(".reference_id").val(template_id);
    $("#datset_data_edit_val").val(template_content.data_set_key);
    $("#coloumn_name_edit").val(template_content.data_saving_colum);
    $(".template_label_add").val(template_content.label);
    $("#template_control_add").val(template_content.control_id);
    $("#template_control_add").select2().trigger('change');
    $(".template_class_add").val(template_content.class);
    $(".template_placeholder_add").val(template_content.placeholder);
    $(".default_value_add").val(template_content.default_value);
    $(".sql_add").val(template_content.sql);
    $(".group_value_add").val(template_content.group_value);
    $(".size_add").val(template_content.size);
    $(".container_min_add").val(template_content.container_min_height);
    $(".row_count_add").val(template_content.row_count);
    $(".colum_count_add").val(template_content.colum_count);
    $(".colum_names_add").val(template_content.colum_names);
    $(".table_header_add").val(template_content.table_header);
    $("#dataSource").val(template_content.data_source).select2();
    $("#template_save_master").val(template_content.data_saving_table).select2();
    $("#template_default_data_set").val(template_content.default_data_set).select2();
    $("#dataSource").trigger('onchange');
    $("#template_save_master").trigger('onchange');
    $("#template_data_set").val(template_content.data_set_id).select2();
    $("#template_data_set").trigger('onchange');
    $(".template_data_saving_primary_key").val(template_content.data_saving_primary_key);
    $("#template_compare_key_in_emr").val(template_content.compare_key_in_emr).select2().trigger('change');
    $("#data_point").val(template_content.data_point).select2().trigger('change');

    $(".saveButton").html('<i class="fa fa-save"></i> Update');
    $("#add").html('Edit Template');
    // $('#service_charge_id_hidden').val(template_id);


    if (template_content.status == 1) {
        $(".status_add").prop("checked", true);
    } else {
        $(".status_add").prop("checked", false);
    }
    if (template_content.is_favourite == 1) {
        $(".is_favourite").prop("checked", true);
    } else {
        $(".is_favourite").prop("checked", false);
    }
    if (template_content.is_primary_component == 1) {
        $(".is_primary_component").prop("checked", true);
    } else {
        $(".is_primary_component").prop("checked", false);
    }
    if (template_content.hide_in_template == 1) {
        $(".hide_in_template").prop("checked", true);
    } else {
        $(".hide_in_template").prop("checked", false);
    }
    if (template_content.hide_in_print == 1) {
        $(".hide_in_print").prop("checked", true);
    } else {
        $(".hide_in_print").prop("checked", false);
    }
    if (template_content.is_readonly == 1) {
        $(".is_readonly").prop("checked", true);
    } else {
        $(".is_readonly").prop("checked", false);
    }
    if (template_content.is_static_component == 1) {
        $(".is_static_component").prop("checked", true);
    } else {
        $(".is_static_component").prop("checked", false);
    }
    if (template_content.is_mandatory == 1) {
        $(".is_mandatory").prop("checked", true);
    } else {
        $(".is_mandatory").prop("checked", false);
    }

}

function resetForm() {
    $(".reference_id").val('');
    $(".template_label_add").val('');
    $("#template_control_add").val('').trigger('change');
    $(".template_class_add").val('');
    $('input:checkbox').removeAttr('checked');
    $("#status_add").prop("checked", true);
    $(".template_placeholder_add").val('');
    $(".default_value_add").val('');
    $(".sql_add").val('');
    $(".group_value_add").val('');
    $(".size_add").val(0).trigger('change');
    $(".container_min_add").val('');
    $(".row_count_add").val('');
    $(".colum_count_add").val('');
    $(".colum_names_add").val('');
    $(".table_header_add").val('');
    $(".saveButton").html('<i class="fa fa-save"></i> Save');
    $("#add").html('Add Template');
    $(".hide_table_content").addClass("hidden");
    $(".template_save_master_detail_div").addClass("hidden");
    $(".placeholder_content").removeClass("hidden");

    $("#data_point").val('').select2();
    $("#dataSource").val('').select2();
    $("#template_save_master").val('').select2();
    $("#template_default_data_set").val('').select2();
    $("#template_data_set").val('').select2();
    $("#datset_data_div").val('').select2();
    $("#data_point").val('').select2();
}

function hideControlContents(temp_control = 0) {
    if (temp_control != 0) {

    } else {
        var temp_control = $("#template_control_add").val();
        temp_control = parseInt(temp_control);
    }
    var is_control = $('#template_control_add option:selected').attr('attr_id');

    if (parseInt(is_control) == 1) {
        $(".dataset_datadiv").removeClass("hidden");
    } else {
        $(".dataset_datadiv").addClass("hidden");
    }

    switch (temp_control) {
        case 1:
        case 2:
        case 11:
            $(".hide_table_content").addClass("hidden");
            $(".placeholder_content").removeClass("hidden");
            $(".class_content").removeClass("hidden");
            $(".default_content").removeClass("hidden");
            $(".label_content").removeClass("hidden");
            $(".sql_content").addClass("hidden");
            break;
        case 3:
        case 4:
        case 5:
        case 6:
            $(".hide_table_content").addClass("hidden");
            $(".placeholder_content").addClass("hidden");
            $(".class_content").addClass("hidden");
            $(".default_content").removeClass("hidden");
            $(".label_content").removeClass("hidden");
            $(".sql_content").addClass("hidden");
            break;
        case 7:
        case 8:
        case 9:
        case 10:
        case 12:
        case 13:
        case 15:
        case 16:
        case 17:
        case 18:
            $(".hide_table_content").addClass("hidden");
            $(".placeholder_content").addClass("hidden");
            $(".class_content").addClass("hidden");
            $(".default_content").addClass("hidden");
            $(".label_content").removeClass("hidden");
            $(".sql_content").addClass("hidden");
            break;
        case 14:
            $(".hide_table_content").removeClass("hidden");
            $(".placeholder_content").addClass("hidden");
            $(".class_content").addClass("hidden");
            $(".default_content").addClass("hidden");
            $(".label_content").addClass("hidden");
            $(".sql_content").addClass("hidden");
            break;
        default:
            $(".hide_table_content").addClass("hidden");
            $(".placeholder_content").removeClass("hidden");
            $(".class_content").removeClass("hidden");
            $(".default_content").removeClass("hidden");
            $(".label_content").removeClass("hidden");
            $(".sql_content").addClass("hidden");
    }
}



function hideDataSource() {
    var datset = $("#dataSource").val();
    datset = parseInt(datset);
    switch (datset) {
        case 1:
            $(".sql_content").addClass("hidden");
            $(".dataset_content").removeClass("hidden");
            $(".group_content").addClass("hidden");
            $(".data_point_content").addClass("hidden");
            break;
        case 2:
            $(".sql_content").removeClass("hidden");
            $(".dataset_content").addClass("hidden");
            $(".group_content").addClass("hidden");
            $(".data_point_content").addClass("hidden");

            break;
        case 3:
            $(".group_content").removeClass("hidden");
            $(".sql_content").addClass("hidden");
            $(".dataset_content").addClass("hidden");
            $(".data_point_content").addClass("hidden");
            break;
        case 4:
            $(".data_point_content").removeClass("hidden");
            $(".group_content").addClass("hidden");
            $(".sql_content").addClass("hidden");
            $(".dataset_content").addClass("hidden");
            break;
        default:


    }
}


function getDataSetContent(from_type) {
    var url = base_url + "/form_template/getDataSetContent";
    var dataset_id = '';
    if (parseInt(from_type) == 1) {
        dataset_id = $("#template_data_set").val();
    } else {
        dataset_id = $("#template_default_data_set").val();
    }
    var datset_data_edit = $('#datset_data_edit_val').val();
    var dataset_name = $("#template_data_set option:selected").text();
    $('#dataset_name').html(dataset_name);
    $.ajax({
        type: "POST",
        url: url,
        data: { dataset_id: dataset_id, datset_data_edit: datset_data_edit, from_type: from_type },
        beforeSend: function () {
            if (parseInt(from_type) == 1) {
                $(".template_datasetDetailDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            } else {
                $('.group_content').removeClass("hidden");
                $(".group_content").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            }

        },
        success: function (data) {
            if (parseInt(from_type) == 1) {
                $('#datset_data_div').html(data.option_string);
                $('#datset_data_div').select2();
            } else {
                $('.group_value_add').val(data.selection_data);
            }
        },
        complete: function () {
            if (parseInt(from_type) == 1) {
                $(".template_datasetDetailDiv").LoadingOverlay("hide");
            } else {
                $(".group_content").LoadingOverlay("hide");
            }
        }
    });
}




function getTemplateMasterData() {
    var url = base_url + "/form_template/getColumnNameByTableName";
    var table_name = $("#template_save_master option:selected").text();
    if (table_name) {
        $('#template_save_table_detail_header').html(table_name);
        $('.template_save_master_detail_div').removeClass("hidden");
        var coloumn_name_edit = $('#coloumn_name_edit').val();
        $.ajax({
            type: "POST",
            url: url,
            data: { coloumn_name_edit: coloumn_name_edit, table_name: table_name },
            beforeSend: function () {
                $(".template_save_master_detail_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $('#template_save_table_detail').html(data);
                $('#template_save_table_detail').select2();
            },
            complete: function () {
                $(".template_save_master_detail_div").LoadingOverlay("hide");
            }
        });
    } else {
        $('.template_save_master_detail_div').addClass("hidden");
    }
}
