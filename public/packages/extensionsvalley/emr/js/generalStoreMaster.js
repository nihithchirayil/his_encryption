$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
    });
    $(".select2").select2();
    searchGeneralStore();
});

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});

var base_url = $('#base_url').val();
var token = $('#hidden_filetoken').val();
var uom_array = {};

function searchGeneralStore() {
    var url = base_url + '/general_store/searchGeneralStore';
    var item_type = $('#search_item_type').val();
    var product_id = $('#item_id_hidden').val();
    var category_master = $('#search_category_master').val();
    var subcategory_master = $('#search_subcategory_master').val();
    var param = {
        item_type: item_type,
        product_id: product_id,
        category_master: category_master,
        subcategory_master: subcategory_master,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchGeneralStoreBtn').attr('disabled', true);
            $('#searchGeneralStoreSpin').removeClass('fa fa-search');
            $('#searchGeneralStoreSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchGeneralStoreData').html(data);
        },
        complete: function () {
            $('#searchGeneralStoreBtn').attr('disabled', false);
            $('#searchGeneralStoreSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchGeneralStoreSpin').addClass('fa fa-search');
            resetSave();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function editGeneralStore(product_id) {
    resetSave();
    var url = base_url + '/general_store/editGeneralStore';
    $('#general_item_id').val(product_id);
    var param = {
        product_id: product_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('.editGeneralStore').removeClass('bg-blue');
            $("#searchGeneralStoreData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var obj = JSON.parse(data.result);
            $('#item_type').val(obj[0].item_type_id ? obj[0].item_type_id : '').select2();
            $('#item_code').val(obj[0].item_code ? obj[0].item_code : '');
            $('#item_description').val(obj[0].item_desc ? obj[0].item_desc : '');
            $('#category_master').val(obj[0].category_id ? obj[0].category_id : '').select2();
            $('#editsubcategory_item_id').val(obj[0].subcategory_id ? obj[0].subcategory_id : '');

            var is_purchasable = obj[0].purchasable_ind ? obj[0].purchasable_ind : 0;
            var is_sellable = obj[0].sellable_ind ? obj[0].sellable_ind : 0;
            var is_active = obj[0].status ? obj[0].status : 0;
            var uom_id = obj[0].uom_id ? obj[0].uom_id : 0;
            $('#std_uom').val(uom_id).select2();
            $('#is_purchasable').prop('checked', false);
            $('#is_sellable').prop('checked', false);
            $('#is_active').prop('checked', false);

            if (parseInt(is_active) == 1) {
                $('#is_active').prop('checked', true);
            }
            if (parseInt(is_purchasable) == 1) {
                $('#is_purchasable').prop('checked', true);
            }
            if (parseInt(is_sellable) == 1) {
                $('#is_sellable').prop('checked', true);
            }

            var pur_uom = JSON.parse(data.pur_uom);
            $.each(pur_uom, function (index, value) {
                $('#pur_uom' + index).val(value.uom_id).select2();
                $('#pur_con_fact' + index).val(value.conv_factor);
                if (parseInt(value.default) == 1) {
                    $('#pur_default' + index).prop('checked', true);
                } else {
                    $('#pur_default' + index).prop('checked', false);
                }
                if (value.uom_id) {
                    $('#pur_default' + index).attr('disabled', false);
                } else {
                    $('#pur_default' + index).attr('disabled', true);
                }
            });
        },
        complete: function () {
            $('#editGeneralStore' + product_id).addClass('bg-blue');
            $("#searchGeneralStoreData").LoadingOverlay("hide");
            getSubCategory('category_master', 'subcategory_master', 1);
            $('#item_title').html('Edit General Store Item Master');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function resetfilter() {
    $('#search_item_type').val('').select2();
    $('#search_item_description').val('');
    $('#item_id_hidden').val('');
    $('#search_category_master').val('').select2();
    $('#search_subcategory_master').val("<option value=''>Select</option>").select2();
    searchGeneralStore();
}

function getSubCategory(category_master, subcategory_master, from_type) {
    var category_master = $('#' + category_master).val();
    var html_data = "";
    if (category_master) {
        var url = base_url + '/general_store/getSubCategory';
        var param = {
            category_master: category_master
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#" + subcategory_master).LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                $("#" + subcategory_master).html(data).select2();
            },
            complete: function () {
                $("#" + subcategory_master).LoadingOverlay("hide");
                if (parseInt(from_type) == 1) {
                    var sub_catid = $('#editsubcategory_item_id').val();
                    $('#subcategory_master').val(sub_catid ? sub_catid : '').select2();
                }
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        html_data = "<option value=''>Select</option>";
        $("#" + subcategory_master).html(html_data).select2();
    }

}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function searchItemDesc(event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var item_desc = htmlDecode($('#search_item_description').val());
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxSearchBox_description');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (item_desc) {
            var url = base_url + "/general_store/searchGeneralItemDesc";
            var param = {
                item_desc: item_desc,
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxSearchBox_description").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxSearchBox_description").html(html).show();
                    $("#ajaxSearchBox_description").find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            $("#ajaxSearchBox_description").html('').hide();
            $("#item_id_hidden").val('');
        }
    } else {
        ajax_list_key_down('ajaxSearchBox_description', event);
    }
}

function fillGeneralStoreItem(product_id, product_desc) {
    $("#item_id_hidden").val(product_id);
    product_desc = htmlDecode(product_desc);
    $('#search_item_description').val(product_desc);
    $("#ajaxSearchBox_description").html('').hide();
}

function resetSave() {
    $('#general_item_id').val(0);
    $('#item_code').val('');
    $('#item_type').val('').select2();
    $('.purchase_uom').val('').select2();
    $('#std_uom').val('').select2();
    $('#std_uom').prop('disabled', true);
    $('#item_description').val('');
    $('.pur_con_fact').val('');
    $('#category_master').val('').select2();
    $('#subcategory_master').val("<option value=''>Select</option>").select2();
    $('#is_purchasable').prop('checked', false);
    $('#is_sellable').prop('checked', false);
    $('#is_active').prop('checked', true);
    $('.pur_default_cls').prop('disabled', true);
    $('.pur_default_cls').prop('checked', false);
    $('#item_title').html('Add General Store Item Master');
    $('.editGeneralStore').removeClass('bg-blue');
}


function getPurchaseUom() {
    uom_array = {};
    var flag = true;
    $('.purchase_uom').each(function (index) {
        var pur_id = $(this).attr('data-id');
        var pur_default_radio = $("#pur_default" + pur_id).is(":checked");
        var pur_con_fact = $('#pur_con_fact' + pur_id).val();
        var purchase_uom = $(this).val();
        var pur_default = 0;

        if (pur_default_radio && !purchase_uom) {
            toastr.warning("Please Enter Purchase UOM");
            flag = false;
            return flag;
        } else if (pur_default_radio && !pur_con_fact) {
            toastr.warning("Please Enter Conv Factor");
            flag = false;
            return flag;
        } else if (purchase_uom && !pur_con_fact) {
            toastr.warning("Please Enter Conv Factor");
            flag = false;
            return flag;
        } else if (pur_default_radio) {
            pur_default = 1;
        }
        if (purchase_uom) {
            uom_array[pur_id] = {
                purchase_uom: purchase_uom,
                pur_con_fact: pur_con_fact ? pur_con_fact : 0,
                pur_default: pur_default,
            }
        }
    });
    return flag;
}


function setenabledefault(list_id) {
    var pur_uom = $('#pur_uom' + list_id).val();
    if (pur_uom) {
        $('#pur_default' + list_id).attr('disabled', false);
        $('#pur_default' + list_id).prop('checked', true);
    } else {
        $('#pur_default' + list_id).attr('disabled', true);
        $('#pur_default' + list_id).prop('checked', false);
    }
    var list_id_get = $('input[name="pur_default"]:checked').val();
    if (list_id_get) {
        setStdUom(list_id_get);
    } else {
        setStdUom(list_id);
    }
}

function setStdUom(list_id) {
    var pur_uom = $('#pur_uom' + list_id).val();
    $('#std_uom').val(pur_uom).select2();
}

function saveGeneralStore() {
    var general_item_id = $('#general_item_id').val();
    var item_type = $('#item_type').val();
    var item_description = $('#item_description').val();
    var category_master = $('#category_master').val();
    var subcategory_master = $('#subcategory_master').val();
    var std_uom = $('#std_uom').val();
    var is_purchasable = $("#is_purchasable").is(":checked");
    var is_sellable = $("#is_sellable").is(":checked");
    var is_active = $("#is_active").is(":checked");
    var uom_status = getPurchaseUom();
    if (item_type) {
        if (item_description) {
            if (category_master) {
                if (subcategory_master) {
                    if (std_uom) {
                        if (uom_status) {
                            var url = base_url + '/general_store/saveGeneralStore';
                            var param = {
                                general_item_id: general_item_id,
                                item_type: item_type,
                                item_description: item_description,
                                category_master: category_master,
                                subcategory_master: subcategory_master,
                                std_uom: std_uom,
                                uom_array_string: JSON.stringify(uom_array),
                                is_purchasable: is_purchasable,
                                is_sellable: is_sellable,
                                is_active: is_active
                            };
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: param,
                                beforeSend: function () {
                                    $('#saveGeneralStoreBtn').attr('disabled', true);
                                    $('#saveGeneralStoreSpin').removeClass('fa fa-save');
                                    $('#saveGeneralStoreSpin').addClass('fa fa-spinner fa-spin');
                                },
                                success: function (data) {
                                    if (parseInt(data.status) == 1) {
                                        toastr.success(data.message);
                                        resetSave();
                                    } else {
                                        toastr.warning(data.message);
                                    }
                                },
                                complete: function () {
                                    $('#saveGeneralStoreBtn').attr('disabled', false);
                                    $('#saveGeneralStoreSpin').removeClass('fa fa-spinner fa-spin');
                                    $('#saveGeneralStoreSpin').addClass('fa fa-save');
                                },
                                error: function () {
                                    toastr.error("Error Please Check Your Internet Connection");
                                }
                            });
                        }
                    } else {
                        toastr.warning("Please select any Standard UOM")
                    }
                } else {
                    toastr.warning("Please select any sub category")
                }
            } else {
                toastr.warning("Please select any category master")
            }
        } else {
            toastr.warning("Please enter item description")
        }
    } else {
        toastr.warning("Please select any item type")
    }
}
