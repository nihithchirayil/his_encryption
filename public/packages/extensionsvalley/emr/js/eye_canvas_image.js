var zoomLevel = 1;
var isDrawing = false;
var pencilPath = null;
var pathCommands = [];
var canvas = new fabric.Canvas('image_canvas');
$(document).ready(function () {
    getEyeFloderImages();
    $("#uploadEyeImageDocumentData").on("submit", function (e) {
        e.preventDefault();
        var image_vai = validateImageDoc("upload_eyemarkdocument");
        var param = new FormData(this);
        if (image_vai) {
            var url = base_url + "/eye_emr/uploadEyeImageDocument";
            $.ajax({
                url: url,
                type: "POST",
                data: param,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#saveEyeImageDataBtn").attr("disabled", true);
                    $("#saveEyeImageDataSpin").removeClass("fa fa-save");
                    $("#saveEyeImageDataSpin").addClass("fa fa-spinner fa-spin");
                },
                success: function (data) {
                    if (parseInt(data.status) == 1) {
                        toastr.success(data.message);
                        $("#uploadEyeImagesModel").modal('toggle');
                        document.getElementById("uploadEyeImageDocumentData").reset();
                        getEyeFloderImages();
                    } else {
                        toastr.warning(data.message);
                    }
                },
                complete: function () {
                    $("#saveEyeImageDataBtn").attr("disabled", false);
                    $("#saveEyeImageDataSpin").removeClass("fa fa-spinner fa-spin");
                    $("#saveEyeImageDataSpin").addClass("fa fa-save");
                },
                error: function () {
                    toastr.error(
                        "Error Please Check Your Internet Connection and Try Again"
                    );
                },
            });
        } else {
            bootbox.alert(
                "<strong>Please select any file with criteria.</strong><br>Extensions: jpg gif png <br>Size : Maximum 5 MB"
            );
        }
    });

    canvas.setDimensions({
        width: 800,
        height: 495
    });
    canvas.renderAll();

    $('#addTextBtn').on('click', function () {
        var text = new fabric.IText(' ', {
            left: 100,
            top: 100,
            fontFamily: 'Arial',
            fontSize: 20,
            fill: '#FFF',
            backgroundColor: '#808080', // Set the desired background color
            selectable: true,
            hasControls: false,
            hoverCursor: 'text'
        });

        var closeButton = document.createElement('div');
        closeButton.innerHTML = '&#10006;';
        closeButton.classList.add('close-button');

        var canvasContainer = document.getElementById('canvas-container');
        canvasContainer.appendChild(closeButton);

        closeButton.addEventListener('click', function () {
            canvas.remove(text);
            canvasContainer.removeChild(closeButton);
            canvas.renderAll();
        });

        text.on('selected', function () {
            closeButton.style.display = 'block';
        });

        text.on('deselected', function () {
            closeButton.style.display = 'none';
        });


        canvas.add(text);
        canvas.setActiveObject(text);
        text.enterEditing();
        text.selectAll();
        canvas.renderAll();
    });

    document.addEventListener('DOMContentLoaded', function () {
        var canvas = new fabric.Canvas('canvas');
        var imageInput = document.getElementById('imageInput');

        imageInput.addEventListener('change', function (e) {
            var file = e.target.files[0];
            var reader = new FileReader();

            reader.onload = function (event) {
                var imageUrl = event.target.result;

                fabric.Image.fromURL(imageUrl, function (img) {
                    canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                        scaleX: canvas.width / img.width,
                        scaleY: canvas.height / img.height
                    });
                });
            };

            reader.readAsDataURL(file);
        });
    });

    $('#zoomInBtn').on('click', function () {
        zoomLevel = parseFloat(zoomLevel) + 0.5;
        canvas.setZoom(zoomLevel);
    });
    // $('#zoomInBtn').on('click', function () {
    //     var zoomCenter = new fabric.Point(canvas.width / 2, canvas.height / 2);
    //     zoomCenter.subtract(canvas.viewportTransform[4], canvas.viewportTransform[5]);
    //     zoomCenter.divide(canvas.viewportTransform[0], canvas.viewportTransform[3]);
    //     var newScaleX = canvas.viewportTransform[0] * zoomLevel;
    //     var newScaleY = canvas.viewportTransform[3] * zoomLevel;
    //     var newTranslateX = canvas.width / 2 - (zoomCenter.x * newScaleX);
    //     var newTranslateY = canvas.height / 2 - (zoomCenter.y * newScaleY);
    //     canvas.viewportTransform[0] = newScaleX;
    //     canvas.viewportTransform[3] = newScaleY;
    //     canvas.viewportTransform[4] = newTranslateX;
    //     canvas.viewportTransform[5] = newTranslateY;
    //     canvas.requestRenderAll();
    // });
    $('#zoomOutBtn').on('click', function () {
        if (parseFloat(zoomLevel) > 1) {
            zoomLevel = parseFloat(zoomLevel) - 0.5;
        } else {
            zoomLevel = 1;
        }
        canvas.setZoom(zoomLevel);
    });


    $('#pencilBtn').on('click', function () {
        var button = document.getElementById('pencilBtn');
        button.addEventListener('click', getBoundingBox);
    });


});

function changeBackgroundImage(imageUrl) {
    fabric.Image.fromURL(imageUrl, function (img) {
        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
        img.left = (canvas.width - img.width) / 2;
        img.top = (canvas.height - img.height) / 2;
        canvas.renderAll();
    });
}



function getBoundingBox() {
    var path = new fabric.Path('M 10 10 L 100 100', {
        left: 10,
        top: 10,
        stroke: 'red',
        strokeWidth: 2,
    });
    canvas.add(path);
    var boundingRect = path.getBoundingRect();
    console.log('Left:', boundingRect.left);
    console.log('Top:', boundingRect.top);
    console.log('Width:', boundingRect.width);
    console.log('Height:', boundingRect.height);
    var closeButton = document.createElement('div');
    closeButton.innerHTML = '&#10006;';
    closeButton.classList.add('close-button');

    var canvasContainer = document.getElementById('canvas-container');
    canvasContainer.appendChild(closeButton);

    closeButton.addEventListener('click', function () {
        canvas.remove(path);
        canvasContainer.removeChild(closeButton);
        canvas.renderAll();
    });
}

function removePath() {
    canvas.remove(path);
}

function uploadImage() {
    $("#uploadEyeImagesModel").modal({
        backdrop: 'static',
        keyboard: false
    });
}


function inImageArray(needle) {
    var haystack = ['jpg', 'gif', 'png']
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}


function validateImageDoc(input_id) {
    var fileInput = document.getElementById(input_id);
    if (fileInput) {
        var filePath = fileInput.value;
        if (filePath) {
            var fileExt = filePath.split('.').pop();
            var file_size = fileInput.size;
            if (!inImageArray(fileExt) || file_size > 5150) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getImageUrl(obj) {
    var images = $(obj).attr('src');
    changeBackgroundImage(images);
}

function getEyeFloderImages() {
    var url = base_url + "/eye_emr/getEyeFloderImages";
    var html_string = '';
    $.ajax({
        type: "POST",
        url: url,
        beforeSend: function () {
            $("#uploadImageBtn").attr("disabled", true);
            $("#uploadImageSpin").removeClass("fa fa-cloud-upload");
            $("#uploadImageSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $(obj).each(function (index, element) {
                html_string += "<img style='cursor: pointer' onclick='getImageUrl(this)' class='canvasbackImage' src='" + element + "'>";
            });
        },
        complete: function () {
            $("#uploadImageBtn").attr("disabled", false);
            $("#uploadImageSpin").removeClass("fa fa-spinner fa-spin");
            $("#uploadImageSpin").addClass("fa fa-cloud-upload");
            $('#image-slider').html(html_string);
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function resetImageMarker() {
    canvas.clear();
}
