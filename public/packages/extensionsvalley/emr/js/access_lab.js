
$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

// $("#bill_no").blur(function(){
//     var name=$('#bill_no').val();
//     if(name.length == 0){
//         $('#bill_no').after('<div class="red">Required....</div>');
//     }
//     else {
//         $('#bill_no').next(".red").remove(); // *** this line have been added ***
//         return true;
//     }
// });

function getAccessLabData() {
    var url = route_json.AccessLabBillDetailData;

    //-------filters---------------------------------------------

    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var bill_no = $('#bill_no_hidden').val();
    var uhid = $('#uhid_hidden').val();
    var paid = $('#paid').val();
    var un_paid = $('#un_paid').val();
    var all = $('#all').val();


    var parm = {
        from_date: from_date, to_date: to_date, bill_no: bill_no, uhid: uhid,
        paid: paid, un_paid: un_paid,
        all: all
    };
    if ($('input[name="paid"]:checked').length > 0) {

        var paid = 1;


    } else {
        var paid = 0;

    }
    parm['paid'] = paid;

    if ($('input[name="un_paid"]:checked').length > 0) {

        var un_paid = 1;


    } else {
        var un_paid = 0;

    }
    parm['un_paid'] = un_paid;

    if ($('input[name="all"]:checked').length > 0) {

        var all = 1;


    } else {
        var all = 0;

    }
    parm['all'] = all;


    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');

        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");

            var is_print = $("#is_print").val();

            if (is_print == 0 || is_print == undefined) {
                $("#print_results").addClass("disabled");
            }
            var is_excel = $("#is_excel").val();

            if (is_excel == 0 || is_excel == undefined) {
                $("#csv_results").addClass("disabled");
            }
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
    return false;

}




function datarst1() {
    var current_date = $('#current_date').val();
    $(".datapicker").val([]).change();
    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
    $('#bill_no_hidden').val();
    $('#bill_no').val();
    $('#uhidAjaxDiv').val();
    $('#uhid_hidden').val();
    $('#uhid').val();
    $('#paid').val();
    $('#un_paid').val();
    $('#all').val();
    getAccessLabData();
}
