
$(document).ready(function() {
    var route_value = $('#route_value').val();
     var decode_route = atob(route_value);
     route_json = JSON.parse(decode_route);
      $('.datepicker').datetimepicker({
         format: 'MMM-DD-YYYY',
     });
     $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
 });


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
         if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
function getLedgerGroupData() {
    var url = route_json.LedgerGroupReportData;

    //-------filters---------------------------------------------

   var date_from=$('#date_from').val();
   var date_to=$('#date_to').val();
   var group=$('#group').val();
   var parm={date_from:date_from,date_to:date_to,group:group};
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');

        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");

            var is_print =$("#is_print").val();

            if(is_print==0 || is_print==undefined){
                   $("#print_results").addClass("disabled");
               }
           var is_excel =$("#is_excel").val();

           if(is_excel==0 || is_excel==undefined){
                   $("#csv_results").addClass("disabled");
                }
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}

$("#department").on('change', function() {
    var department = $('#department').val();
    if (department == '') {
      var department = 'a';

    }

 if(department) {


        $.ajax({
                    type: "GET",
                    url: '',
                    data: 'department=' + department,
                    beforeSend: function () {

                        $('#s2id_doctor').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#doctor" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#doctor").empty();

                                        $("#doctor").html('<option value="">Select doctor</option>');
                                        $.each(html,function(key,value){
                                          $("#doctor").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#doctor").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                        $( "#doctor" ).prop( "disabled", false );
                    },
                    complete: function () {
                        $('#warning1').remove();
                        $( "#doctor" ).prop( "disabled", false );

                    }

                });

        }
    else {
                $('#department').focus();
                $("#doctor").empty();
            }

    });

function datarst2() {


  
    $('#group').val();
    $('#group_hidden').val();




}
