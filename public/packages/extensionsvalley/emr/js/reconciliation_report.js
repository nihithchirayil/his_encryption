var token = $("#c_token").val();
var base_url = $("#base_url").val();

$(document).ready(function() {
    $(".datepicker").datetimepicker({ format: 'MMM-DD-YYYY' });
    var today = moment().format('MMM-DD-YYYY');
    $("#from_date").val(today);
    $("#to_date").val(today);

});


$(document).on("click", ".getReport", function(){
    fetchReconciliationReport();
})

$(document).on("click", ".printReport", function(){
    $('#print_config_modal').modal('toggle');
});


function print_generate(printData) {

    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');
    $('#total_row').css('display', '');
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;



    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }

    mywindow.document.write(
        '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
    );
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
    );
    mywindow.document.write(
        '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
    );
    mywindow.document.write(
        '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
    );

    mywindow.document.write(showw);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function() {
        mywindow.print();
        mywindow.close();
        InitTheadScroll();
        $('#total_row').css('display', 'none');
    }, 1000);


    return true;
}



function fetchReconciliationReport(){
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var ledger_bank_id = $("#ledger_bank_id").val();
    var url = base_url + "/accounts/fetchReconciliationReport";
    var param = { 
        _token: token,
        from_date: from_date,
        to_date: to_date,
        ledger_bank_id:ledger_bank_id,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('.getReport').attr('disabled', true);
            $(".getReport").find("i").removeClass('fa-search').addClass("fa-spinner fa-spin");
        },
        success: function (data) {
            if(data.status == 1){
                $("#reconciliationReportDataDiv").html(data.html);
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 15
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }

            
        },
        complete: function () {
            $('.getReport').attr('disabled', false);
            $(".getReport").find("i").removeClass('fa-spinner fa-spin').addClass("fa-search");
        },
    });
}