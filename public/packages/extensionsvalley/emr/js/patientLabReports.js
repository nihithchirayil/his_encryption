var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();
var interval = '';

function searchPatient() {
    var uhid = $('#searchpatient_uhid').val();
    var url = base_url + '/labreports/getPatientDetails';
    if (uhid) {
        $.ajax({
            type: "POST",
            url: url,
            data: { _token: token, uhid: uhid },
            beforeSend: function () {
                $("#search_patientBtn").attr('disabled', true);
                $("#search_patientSpin").removeClass('fa fa-search');
                $("#search_patientSpin").addClass('fa fa-spinner fa-spin');
                $('#patientDataDiv').html('');
                $('#patientOTPDiv').hide();
                $('#patientOTPEnterDiv').hide();
                $('#patientDataDiv').hide();
            },
            success: function (data) {
                var obj = JSON.parse(data);
                var patient_id = obj.patient_id ? obj.patient_id : '';
                var patient_name = obj.patient_name ? obj.patient_name : '';
                var phone = obj.patient_phone ? obj.patient_phone : '';
                fillLabPatientDetails(patient_id, patient_name, phone);
                if (!patient_id) {
                    toastr.warning('Patient not found');
                    $('#searchpatient_uhid').focus();
                }
            },
            complete: function () {
                $("#search_patientBtn").attr('disabled', false);
                $("#search_patientSpin").removeClass('fa fa-spinner fa-spin');
                $("#search_patientSpin").addClass('fa fa-search');

            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning('Please Enter UHID');
        $('#searchpatient_uhid').focus();
    }
}


function submitOTP() {
    var otp = $('#enterPatientOtp').val();
    var patient_id = $('#patient_id_hidden').val();
    var uhid = $('#searchpatient_uhid').val();
    var track_id = $('#smstrack_id').val();
    if (otp) {
        if (uhid) {
            if (track_id) {
                var url = base_url + '/labreports/submitPatientOTP';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, otp: otp, patient_id: patient_id, track_id: track_id },
                    beforeSend: function () {
                        $("#submitatientOtpBtn").attr('disabled', true);
                        $("#submitPatientOtpSpin").removeClass('fa fa-save');
                        $("#submitPatientOtpSpin").addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data == 'otp_incorrect') {
                            toastr.warning("Incorrect OTP");
                        } else {
                            $('#patientDataDiv').html(data).show();
                            $('#patientOTPDiv').hide();
                            $('#patientOTPEnterDiv').hide();
                            $('#patientOTPEnterTimeOutDiv').hide();
                            $("#sendPatientOtpBtnSpan").html('Send OTP');
                            $('#sendPatientOtpBtn').removeClass('btn-warning');
                            $('#sendPatientOtpBtn').addClass('btn-primary');
                            clearInterval(interval);
                        }
                    },
                    complete: function () {
                        $("#submitatientOtpBtn").attr('disabled', false);
                        $("#submitPatientOtpSpin").removeClass('fa fa-spinner fa-spin');
                        $("#submitPatientOtpSpin").addClass('fa fa-save');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });

            } else {
                toastr.warning("Please OTP is not send correctly Please resend OTP");
                $('#searchpatient_uhid').focus();
            }

        } else {
            toastr.warning('Please Enter UHID');
            $('#searchpatient_uhid').focus();
        }
    } else {
        toastr.warning("Please Enter OTP");
        $('#enterPatientOtp').focus();
    }
}

function sendOTP() {
    var patient_id = $('#patient_id_hidden').val();
    var uhid = $('#searchpatient_uhid').val();
    var patient_name = $('#patient_name_hidden').val();
    if (patient_id) {
        var url = base_url + '/labreports/sendPatientOTP';
        $.ajax({
            type: "POST",
            url: url,
            data: { _token: token, uhid: uhid, patient_name: patient_name, patient_id: patient_id },
            beforeSend: function () {
                clearInterval(interval);
                $("#sendPatientOtpBtn").attr('disabled', true);
                $("#sendPatientOtpSpin").removeClass('fa fa-comment');
                $("#sendPatientOtpSpin").addClass('fa fa-spinner fa-spin');
                $('#enterPatientOtp').val('');
                $('#patientOTPEnterDiv').hide();
                $('#patientOTPEnterTimeOutDiv').hide();
                $('#time').html('');
            },
            success: function (data) {
                $('#smstrack_id').val(data);
                $('#patientOTPEnterDiv').show();
                validateOtpTimmer(180);
            },
            complete: function () {
                $("#sendPatientOtpBtn").attr('disabled', false);
                $("#sendPatientOtpSpin").removeClass('fa fa-spinner fa-spin');
                $("#sendPatientOtpSpin").addClass('fa fa-comment');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning('Please select patient');
    }
}


function validateOtpTimmer(counter) {
    interval = setInterval(function () {
        counter--;
        if (counter <= 0) {
            clearInterval(interval);
            $('#patientOTPEnterDiv').hide();
            $('#patientOTPEnterTimeOutDiv').show();
            $('#sendPatientOtpBtn').removeClass('btn-primary');
            $('#sendPatientOtpBtn').addClass('btn-warning');
            $("#sendPatientOtpBtnSpan").html('Resend OTP');
            $("#smstrack_id").val('');
            $('#time').html('');
            return;
        } else {
            $('#time').text(counter);
        }
    }, 1000);
}


function fillLabPatientDetails(patient_id, patient_name, phone) {
    $('#patient_id_hidden').val(patient_id);
    $('#patient_name_hidden').val(patient_name);
    $('#patient_phone_hidden').html("OTP will be send to phone ending with last 4 digit " + phone);
    if (patient_id) {
        $('#patientOTPDiv').show();
    }
    $('#patientOTPEnterDiv').hide();
    $('#patientOTPEnterTimeOutDiv').hide();
    $("#sendPatientOtpBtnSpan").html('Send OTP');
    $('#sendPatientOtpBtn').removeClass('btn-warning');
    $('#sendPatientOtpBtn').addClass('btn-primary');
}
