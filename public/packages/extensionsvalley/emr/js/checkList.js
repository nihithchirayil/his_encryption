$(document).ready(function () {
    searchCheckList();
    $(".select2").select2();
});

function searchCheckList() {
    var check_list_name = $('#checkList_hidden').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/searchCheckList";
    var param = {
        check_list_name: check_list_name,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchCheckList").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })


        },
        success: function (data) {
            $("#searchCheckList").html(data);
        },
        complete: function () {

            $("#searchCheckList").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
    });
}

function saveCheckList() {
    var check_list_name = $('#name').val();
    if(!check_list_name){
        toastr.warning('Please enter check list name.')
    }
    var status = $('#status').val();
    var checkListId = $('#hidden_id').val();
    var modality=$('#search_modality').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveCheckList";
    var param = {
        check_list_name: check_list_name,
        status: status,
        modality:modality,
        checkListId: checkListId,
    }
    console.log(param);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchCheckList").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            console.log(data);
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.error(obj.message);
            }
        },
        complete: function () {
            $("#searchCheckList").LoadingOverlay("hide");
            searchCheckList();
            clearItem();
        },
    });
}
function editItem(id, name, status,modality) {
    $('#hidden_id').val(id);
    $('#name').val(name);
    $('#status').val(status);
    $('#search_modality').val(modality);
}
function deleteItem(id) {
    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deleteItem";
                var param = {
                    id: id
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#searchCheckList").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        }
                        else {
                            toastr.error(obj.message);
                        }
                    },
                    complete: function () {
                        $("#searchCheckList").LoadingOverlay("hide");
                        searchCheckList();
                    },
                });
            }
        }
    });
}

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();



        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $('#base_url').val();
            var url = base_url + "/master/ajaxSearchCheckList";
            console.log(base_url);
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();

                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        // ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
    // doctorSearchList();

}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
});

function clearItem() {
    $('#hidden_id').val('');
    $('#name').val('');
    $('#search_modality').val('');
}
