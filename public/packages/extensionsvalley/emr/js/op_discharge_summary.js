
$(document).ready(function () {

    $('#surgery_date').datetimepicker({
        format: "MMM-DD-YYYY",
    });

    $('#discharge_date').datetimepicker({
      format: "MMM-DD-YYYY",
    });

    $('#investigation_from').datetimepicker({
      format: "MMM-DD-YYYY",
    });
    $('#investigation_to').datetimepicker({
      format: "MMM-DD-YYYY",
    });

    $('.date_floatinput').datetimepicker({
        format: "MMM-DD-YYYY",
    });
    $('#admit_date').datetimepicker({
        format: "MMM-DD-YYYY hh:mm:A",
    });


    $('.time_floatinput').datetimepicker({
        format: "hh:mm:A",
    });


    var visit_id = $('#visit_id').val();
    if(visit_id != ''){
        selectPatientVisits(visit_id);
    }
});
function setPlainText() {
    var ed = tinyMCE.get('elm1');
    ed.pasteAsPlainText = true;
    if (tinymce.isOpera || /Firefox\/2/.test(navigator.userAgent)) {
        ed.onKeyDown.add(function (ed, e) {
            if (((tinymce.isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45))
                ed.pasteAsPlainText = true;
        });
    }else{
        ed.onPaste.addToTop(function (ed, e) {
            ed.pasteAsPlainText = true;
        });
    }
}

$('#op_no').keyup(function(event){
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;

    if (value.match(keycheck) || event.keyCode == '8' || event.keyCode == '96') {
    var op_no = $(this).val();

    if (op_no == "") {
        $("#op_noAjaxDiv").html("");
    } else {
        var url = '';
        $.ajax({
        type: "GET",
            url:"",
            data: "op_no=" + op_no,
            beforeSend: function () {

            $("#op_noAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
                $("#op_noAjaxDiv").html(html).show();
                $("#op_noAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {

            }
        });
    }

 }else {
        ajax_list_key_down('op_noAjaxDiv',event);
    }
});

$("#op_no").on('keydown',function(event){
    if(event.keyCode == 13){
        ajaxlistenter('op_noAjaxDiv');
        return false;
    }
});




function selectPatientValues(id){
    var url = "";
      $.ajax({
      type: "GET",
          url: url,
          data: 'patient_id_select=' + id,
          beforeSend: function () {
              $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
          },
          success: function (html) {
            $("#op_noAjaxDiv").html('').hide();
            $('#ModalSelectVisit').modal('toggle');
            $('#ModalSelectVisitData').html(html);
            $("body").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
          },
          complete: function () {

          }
      });
}

 function selectPatientVisits(visit_id){
    var url = $('#base_url').val() + "/Opsummary/selectPatientVisitContent";
    $.ajax({
    type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            reloadMedicationPage();
        },
        success: function (html) {
            if(html == 2){
                $("body").LoadingOverlay("hide");
                    Command: toastr["warning"]('Please add Consulting doctor in discharge summary doctors list before edit!');
            }else{
                if(tinymce.get('diagnosis')){
                    tinyMCE.get('diagnosis').setContent('');
                }
                if(tinymce.get('allergies')){
                    tinyMCE.get('allergies').setContent('');
                }
                if(tinymce.get('surgery')){
                    tinyMCE.get('surgery').setContent('');
                }
                if(tinymce.get('history_findings')){
                    tinyMCE.get('history_findings').setContent('');
                }
                if(tinymce.get('investigation')){
                    tinyMCE.get('investigation').setContent('');
                }
                if(tinymce.get('surgery_findings_treatments')){
                    tinyMCE.get('surgery_findings_treatments').setContent('');
                }
                if(tinymce.get('treatment_progress')){
                    tinyMCE.get('treatment_progress').setContent('');
                }
                if(tinymce.get('course_in_hospital')){
                    tinyMCE.get('course_in_hospital').setContent('');
                }
                if(tinymce.get('discussion')){
                    tinyMCE.get('discussion').setContent('');
                }
                if(tinymce.get('plan')){
                    tinyMCE.get('plan').setContent('');
                }
                if(tinymce.get('advice_on_discharge')){
                    tinyMCE.get('advice_on_discharge').setContent('');
                }
                if(tinymce.get('condition_of_patient')){
                    tinyMCE.get('condition_of_patient').setContent('');
                }
                // if(tinymce.get('time_of_discharge')){
                //     tinyMCE.get('time_of_discharge').setContent('');
                // }
                if(tinymce.get('presenting_symptoms')){
                    tinyMCE.get('presenting_symptoms').setContent('');
                }
                if(tinymce.get('physical_findings')){
                    tinyMCE.get('physical_findings').setContent('');
                }
                if(tinymce.get('clinical_impression')){
                    tinyMCE.get('clinical_impression').setContent('');
                }
                if(tinymce.get('treatment_and_course')){
                    tinyMCE.get('treatment_and_course').setContent('');
                }
                if(tinymce.get('final_diagnosis')){
                    tinyMCE.get('final_diagnosis').setContent('');
                }
                if(tinymce.get('condition_at_discharge')){
                    tinyMCE.get('condition_at_discharge').setContent('');
                }
                if(tinymce.get('recommendations')){
                    tinyMCE.get('recommendations').setContent('');
                }
                if(tinymce.get('follow_up_details')){
                    tinyMCE.get('follow_up_details').setContent('');
                }

                $("body").LoadingOverlay("hide");
                var obj = JSON.parse(html);
                console.log(obj);
                $('#uhid_label').html(obj.uhid);
                $('#op_no').val(obj.uhid);
                $('#finalized_by').val(obj.finalized_by);
                $('#patient_name_label').html(obj.patient_name);
                $('#age_gender').html(obj.age+'/'+obj.gender);
                $('#surgery_date').val(obj.surgery_date);
                $('#investigation_from').val(obj.admission_date);
                $('#patient_address').html(obj.address);
                $('#dov').html(obj.time_of_visit);
                $('#patient_id_hidden').val(obj.patient_id);
                $('#patient_id').val(obj.patient_id);
                $('#encounter_id').val(obj.encounter_id);
                $('#visit_id').val(obj.visit_id);
                $('#discharge_department').val(obj.treatment_doctor);
                $('#procedure_date').val(obj.procedure_date);
                $('#procedure_time').val(obj.procedure_time);
                $("#ModalPrescription").find('#doctor_prescription').val(obj.treatment_doctor).select2();
                $('#ModalSelectVisit').modal('hide');
                $('#op_noAjaxDiv').hide();
                $('#summary_patient_header_section').html(obj.summaryPatientDetails);
                $('#label_department_name').html(obj.department_name);
                $('#department_dr_head').html(obj.doctor_headder);
                $('#summary_content_section').html(obj.summaryPrintData);
                $('#summary_footer_section').html(obj.doctor_footer);

                if(obj.summary_content['diagnosis'] !='' && tinyMCE.get('diagnosis')){
                    tinyMCE.get('diagnosis').setContent(obj.summary_content['diagnosis']);
                }
                if(obj.summary_content['allergies'] !='' && tinyMCE.get('allergies')){
                    tinyMCE.get('allergies').setContent(obj.summary_content['allergies']);
                }
                if(obj.summary_content['history_findings'] !='' && tinyMCE.get('history_findings')){
                    tinyMCE.get('history_findings').setContent(obj.summary_content['history_findings']);
                }
                if(obj.summary_content['surgery'] !='' && tinyMCE.get('surgery')){
                    tinyMCE.get('surgery').setContent(obj.summary_content['surgery']);
                }
                if(obj.summary_content['investigation'] !='' && tinyMCE.get('investigation')){
                    tinyMCE.get('investigation').setContent(obj.summary_content['investigation']);
                }
                if(obj.summary_content['surgery_findings_treatments'] !='' && tinyMCE.get('surgery_findings_treatments')){
                    tinyMCE.get('surgery_findings_treatments').setContent(obj.summary_content['surgery_findings_treatments']);
                }
                if(obj.summary_content['advice_on_discharge'] !='' && tinyMCE.get('advice_on_discharge')){
                    tinyMCE.get('advice_on_discharge').setContent(obj.summary_content['advice_on_discharge']);
                }
                if(obj.summary_content['course_in_hospital'] !='' && tinyMCE.get('course_in_hospital')){
                    tinyMCE.get('course_in_hospital').setContent(obj.summary_content['course_in_hospital']);
                }
                if(obj.summary_content['treatment_progress'] !='' && tinyMCE.get('treatment_progress')){
                    tinyMCE.get('treatment_progress').setContent(obj.summary_content['treatment_progress']);
                }
                if(obj.summary_content['discussion'] !='' && tinyMCE.get('discussion')){
                    tinyMCE.get('discussion').setContent(obj.summary_content['discussion']);
                }
                if(obj.summary_content['plan'] !='' && tinyMCE.get('plan')){
                    tinyMCE.get('plan').setContent(obj.summary_content['plan']);
                }
                if(obj.summary_content['urgent_care'] !='' && tinyMCE.get('condition_of_patient')){
                    tinyMCE.get('condition_of_patient').setContent(obj.summary_content['urgent_care']);
                }
                if(obj.summary_content['presenting_symptoms'] !='' && tinyMCE.get('presenting_symptoms')){
                    tinyMCE.get('presenting_symptoms').setContent(obj.summary_content['presenting_symptoms']);
                }
                if(obj.summary_content['physical_findings'] !='' && tinyMCE.get('physical_findings')){
                    tinyMCE.get('physical_findings').setContent(obj.summary_content['physical_findings']);
                }
                if(obj.summary_content['clinical_impression'] !='' && tinyMCE.get('clinical_impression')){
                    tinyMCE.get('clinical_impression').setContent(obj.summary_content['clinical_impression']);
                }
                if(obj.summary_content['treatment_and_course'] !='' && tinyMCE.get('treatment_and_course')){
                    tinyMCE.get('treatment_and_course').setContent(obj.summary_content['treatment_and_course']);
                }
                if(obj.summary_content['final_diagnosis'] !='' && tinyMCE.get('final_diagnosis')){
                    tinyMCE.get('final_diagnosis').setContent(obj.summary_content['final_diagnosis']);
                }
                if(obj.summary_content['condition_at_discharge'] !='' && tinyMCE.get('condition_at_discharge')){
                    tinyMCE.get('condition_at_discharge').setContent(obj.summary_content['condition_at_discharge']);
                }
                if(obj.summary_content['recommendations'] !='' && tinyMCE.get('recommendations')){
                    tinyMCE.get('recommendations').setContent(obj.summary_content['recommendations']);
                }
                if(obj.summary_content['follow_up_details'] !='' && tinyMCE.get('follow_up_details')){
                    tinyMCE.get('follow_up_details').setContent(obj.summary_content['follow_up_details']);
                }


            }




        },
        complete: function () {
            $('.ajaxSearchBox').css("display", "none");
        }
    });
}


function summary_dates_change(admitdate){
    var procedure_date = $('#procedure_date').val();
    var procedure_time = $('#procedure_time').val();
    var url = $('#base_url').val()+"/Opsummary/summaryDatesChange";
    var visit_id = $('#visit_id').val();
    $.ajax({
        type:"GET",
        url:url,
        data: 'visit_id=' +visit_id +'&procedure_date='+ procedure_date +'&procedure_time='+ procedure_time,
        beforeSend: function () {

        },
        success: function (html) {

        },
        complete: function () {

        }
    });
}

function saveDischargeSummaryContent(ed_id){

    var summary_field_name = ed_id;
    var visit_id = $('#visit_id').val();
    if(visit_id == ''){
        Command: toastr["warning"]('Please Select Patient');
        return false;
    }

    if(summary_field_name == 'condition_of_patient'){
        summary_field_name = 'urgent_care';
    }

    var department_id = '';
        department_id = $('#discharge_department').val();
        if(department_id == null){
            Command: toastr["warning"]('Please Select Doctor-department');
            return false;
        }

    var dataContent = tinymce.get(ed_id).getContent();
        dataContent = encodeURIComponent(dataContent.replace('&','\&'));
        dataContent = dataContent.trim();
        // if(dataContent == ""){
        //     return false;
        // }

    var paient_id = $('#patient_id_hidden').val();
    var token = $('#frm-summary').find('input[name="_token"]').val();


    var url = $('#base_url').val()+"/Opsummary/dischargesummaryUpdate";
    $.ajax({
        type:"POST",
        url:url,
        data: '_token='+ token
                +'&visit_id='+ visit_id
                +'&paient_id='+ paient_id
                +'&department_id='+ department_id
                +'&summarydataContent=' + dataContent
                +'&dischargeSummaryUpdate=1'
                +'&summary_field_name='+summary_field_name,

        beforeSend: function () {

        },
        success: function (html) {
           if(html == 0){
            Command: toastr["error"]('Something went wrong!');
           }else{
               var obj = JSON.parse(html);
               Command: toastr["info"](obj.datatitle+' updated successfully!');
               //console.log(obj.datatitle);
               //console.log(dataContent);
               $('#'+obj.datatitle+'_data_content').html(dataContent);
           }
        },
        complete: function () {

        }
    });
}

function print_summary(){
        event.stopPropagation();
        var showw = "";

        var mywindow = window.open('', 'my div', 'height=3508,width=2480');

        var msglist = document.getElementById('summary_content');
        showw = showw + msglist.innerHTML;

        mywindow.document.write('<style>@page{size:portrait;margin:30;margin-left:35px;text-align:center;}@media print {p{margin-top:20px; color:black; }}</style>');

        mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:14px !important;}</style>');
        mywindow.document.write('<style>.summary_label{font-size:21px !important;}</style>');
        mywindow.document.write(showw);
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print()
        mywindow.close();
        return true;
}

function updatePreview(){
    setTimeout(function(){ update_preview_summary();}, 2000);
}

function update_preview_summary(){
    var visit_id = $('#visit_id').val();
    if(visit_id == ''){
        Command: toastr["warning"]('Select patient!');
        return false;
    }
    var url = $('#base_url').val()+"/Opsummary/update_preview_summary";
    $.ajax({
        type:"get",
        url:url,
        data: 'visit_id='+ visit_id,
        beforeSend: function () {
            $("#summary_content_section").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (html) {
            $("#summary_content_section").LoadingOverlay("hide");
            $('#summary_content_section').html(html);

            update_summary_final_data(visit_id);

        },
        complete: function () {

            var procedure_date = $('#procedure_date').val();
            var procedure_time = $('#procedure_time').val();

            $('#summary_discharge_date_time').html(procedure_date+' '+procedure_time);

        }
    });
}

function snipMe() { alert('d');
    page_count++;
    if (page_count > max_pages) {
      return;
    }
    var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    var children = $(this).children().toArray();
    var removed = [];
    while (long > 0 && children.length > 0) {
      var child = children.pop();
      $(child).detach();
      removed.unshift(child);
      long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    }
    if (removed.length > 0) {
      var a4 = $('<div class="summaryViewArea"></div>');
      a4.append(removed);
      $(this).after(a4);
      snipMe.call(a4[0]);
    }
  }

function update_summary_final_data(visit_id){
    var url = $('#base_url').val()+"/Opsummary/update-summary-final_data";
    var token =  $('#c_token').val();
    var summaryData = $('#summaryViewArea').html();
        summaryData = encodeURIComponent(summaryData.replace('&','\&'));
        summaryData = summaryData.trim();
    $.ajax({
        type:"POST",
        url:url,
        data: '_token='+ token +'&visit_id='+ visit_id +'&summaryData='+summaryData,
        beforeSend: function () {

        },
        success: function (html) {
            if(html == 0){
                Command: toastr["error"]('Summary final data content is not updating correctly!');
            }

        },
        complete: function () {

        }
    });
}

function show_medication(){
    $('#ModalPrescription').modal('toggle');
    $('#p_type_discharge').attr('checked','checked');
}


function reloadMedicationPage(){
    var url = $('#base_url').val()+"/Opsummary/showModalPrescriptionContent";
    $.ajax({
        type:"get",
        url:url,
        data: '',
        beforeSend: function () {

            $("#prescription_modal_body_content").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (html) {
            $('#prescription_modal_body_content').html(html);

        },
        complete: function () {
            $("#prescription_modal_body_content").LoadingOverlay("hide");

        }
    });
}


function save_medication(){


   saveDoctorPrescriptions(1);

   $('#ModalPrescription').modal('toggle');

}

function fetchSummaryPatientInvestigation(){
    var patient_id = $('#patient_id').val();
    var from_date = $('#investigation_from').val();
    var to_date = $('#investigation_to').val();
    if(patient_id == ''){
        Command: toastr["warning"]('Select patient!');
        return false;
    }
    var url = $('#base_url').val()+"/Opsummary/fetchSummaryPatientInvestigation";
    $.ajax({
        type:"get",
        url:url,
        data: 'patient_id='+ patient_id+'&from_date='+from_date+'&to_date='+to_date,
        beforeSend: function () {

        },
        success: function (html) {
            //console.log(html);
            if(html!=''){
                tinyMCE.activeEditor.execCommand('mceInsertRawHTML', false,html);
                $('#modalFetchLab').modal('toggle');
            }else{
                Command: toastr["warning"]('No results found!');
            }

        },
        complete: function () {

        }
    });
}

function fetchConsultation(){
    var visit_id = $('#visit_id').val();
    if(visit_id == ''){
        Command: toastr["warning"]('Select patient!');
        return false;
    }
    var url = $('#base_url').val()+"/Opsummary/fetchConsultation";
    $.ajax({
        type:"get",
        url:url,
        data: 'visit_id='+ visit_id,
        beforeSend: function () {

        },
        success: function (html) {
            //console.log(html);
            if(html!=''){
                tinyMCE.activeEditor.execCommand('mceInsertRawHTML', false,html);

            }else{
                Command: toastr["warning"]('No results found!');
            }

        },
        complete: function () {

        }
    });
}

function approve_summary(val){
    var visit_id = $('#visit_id').val();
    var finalized_by = $('#finalized_by').val();

    if(visit_id == ''){
        Command: toastr["warning"]('Select patient!');
        return false;
    }
    var url = $('#base_url').val()+"/Opsummary/finalizeOrDefinalizeSummary";
    $.ajax({
        type:"get",
        url:url,
        data: 'visit_id='+ visit_id+'&finalized_by='+finalized_by+'&approve_status='+val,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (html) {
             $("body").LoadingOverlay("hide");
            if(html==1){
                Command: toastr["success"]('Updated successfully!');

            }

        },
        complete: function () {

        }
    });
}

function loadFavorites(){
    var url = $('#base_url').val()+"/Opsummary/loadSummaryFavourites";
    $.ajax({
    type: "GET",
        url: url,
        data: 'load_favorite='+ 1 ,
        beforeSend: function () {
        },
        success: function (data) {

           $('#myModalFavoriteListData').html(data);
           $('#myModalFavoriteList').modal('toggle');

        },
        complete: function () {

        }
    });

}

function addToFavorites(){
    var url = $('#base_url').val()+"/Opsummary/addToFavorites";
    var visit_id = $('#visit_id').val();
    var paient_id = $('#patient_id_hidden').val();
     $.ajax({
     type: "GET",
         url: url,
         data:'add_favorite='+ 1 +'&paient_id='+ paient_id +"&visit_id=" + visit_id,
         beforeSend: function () {
         },
         success: function (data) {
           var response = JSON.parse(data);
             if(response.status == 1){
                 Command: toastr["success"](response.msg);
             }else{
                 Command: toastr["warning"](response.msg);
             }

         },
         complete: function () {

         }
     });
}

$("select[name='discharge_department']").change(function(){
    var dept_id = $(this).val();
    if(dept_id!=''){
        var url = $('#base_url').val()+"/Opsummary/changeSummaryDepartment";
        var visit_id = $('#visit_id').val();
          $.ajax({
            type: "GET",
            url:url,
            data: 'change_summary_dept='+dept_id+'&visit_id='+visit_id,
            beforeSend: function () {
            },
            success: function (data) {
                //console.log(data);return;
                if(data == 0){
                  Command: toastr["warning"]('Something went wrong');
                }else{
                   var data = JSON.parse(data);
                   $('#summary_discharged_doctor_name').html(data.doctor_name);
                   $('#label_department_name').html(data.dept_name);
                   $('#department_dr_head').html(data.doctor_headder);
                   $('#summary_footer_section').html(data.doctor_footer);
                }
            },
            complete: function () {
            }
          });
    }
});


function fillData(visit_id){
    if(checkFillablesSelected() <= 0){
        Command: toastr["warning"]('Check Fillables..!');
        return false;
    }
    var paient_id = $('#patient_id_hidden').val();
    if(visit_id == '' || visit_id == undefined){
         Command: toastr["warning"]('Error..!');
         return false;
    }
    if(paient_id == '' || paient_id == undefined){
         Command: toastr["warning"]('Select Patient..!');
         return false;
    }


    var ArrChecked = [];
    $('#fillablesTable tr input[type="checkbox"]:checked').each(function(index,element){
        let checkedItem = $(element).val();
        if(checkedItem != '' && checkedItem != undefined){
            ArrChecked.push(checkedItem);
        }
    });

   // var url = '';
   // var token = $('#discharge_summary_content').find('input[name="_token"]').val();
    $.ajax({
    type: "GET",
        url: $('#base_url').val()+"/Opsummary/selectedFromFavoutites",
        data: 'selected_from_favorite='+ 1+'&ArrChecked='+ArrChecked+'&visit_id='+visit_id+'&patient_id='+paient_id ,
        beforeSend: function () {
        },
        success: function (response) {
            if(response != ''){
                let result = JSON.parse(response);
                for(var datas in result){
                    if($('#'+datas+'_data').length > 0){
                        let responseHtml = '';
                        let Header = '';
                        switch(datas) {
                            case 'diagnosis':
                                Header = 'Diaganosis';
                                break;
                            case 'surgery':
                                Header = 'Surgery';
                                break;
                            case 'history_findings':
                                Header = 'History And Physical Findings';
                                break;
                            case 'investigation':
                                Header = 'Investigation';
                                break;
                            case 'surgery_findings_treatments':
                                Header = 'Surgery Findings And Treatments';
                                break;
                            case 'treatment_progress':
                                Header = 'Treatment And Progress';
                                break;
                            case 'course_in_hospital':
                                Header = 'Course in Hospital';
                                break;
                            case 'discussion':
                                Header = 'Discussion';
                                break;
                            case 'surgery':
                                Header = 'Surgery';
                                break;
                            case 'plan':
                                Header = 'Plan';
                                break;
                            case 'advice_on_discharge':
                                Header = 'Advice on Discharge';
                                break;
                            case 'urgent_care':
                                Header = 'Conditions Requiring Urgent Care';
                                break;
                            // case 'time_of_discharge':
                            //     Header = 'Time Of Discharge';
                            //     break;
                            case 'presenting_symptoms':
                                Header = 'Presenting Symptoms';
                                break;
                            case 'physical_findings':
                                Header = 'Physical Findings';
                                break;
                            case 'clinical_impression':
                                Header = 'Clinical Impression';
                                break;
                            case 'treatment_and_course':
                                Header = 'Treatment And Course';
                                break;
                            case 'final_diagnosis':
                                Header = 'Final Diagnosis';
                                break;
                            case 'condition_at_discharge':
                                Header = 'Condition at discharge';
                                break;
                            case 'recommendations':
                                Header = 'Recommendations';
                                break;
                            case 'follow_up_details':
                                Header = 'Follow Up Details';
                                break;
                            default:
                                Header = '';
                        }
                        responseHtml = '<p style="margin-bottom:3px"><u><b>'+Header+'</b></u></p><p>'+result[datas]+'</p>';
                        tinymce.get(datas).setContent(result[datas]);
                        saveDischargeSummaryContent(datas);

                        $('#'+datas+'_data').html(result[datas]);
                    }else{
                        let responseHtml = '';
                        let Header = '';
                        switch(datas) {
                            case 'diagnosis':
                                Header = 'Diagnosis';
                                break;
                            case 'surgery':
                                Header = 'Surgery';
                                break;
                            case 'history_findings':
                                Header = 'History And Physical Findings';
                                break;
                            case 'investigation':
                                Header = 'Investigation';
                                break;
                            case 'surgery_findings_treatments':
                                Header = 'Surgery Findings And Treatments';
                                break;
                            case 'treatment_progress':
                                Header = 'Treatment And Progress';
                                break;
                            case 'course_in_hospital':
                                Header = 'Course in Hospital';
                                break;
                            case 'discussion':
                                Header = 'Discussion';
                                break;
                            case 'surgery':
                                Header = 'Surgery';
                                break;
                            case 'plan':
                                Header = 'Plan';
                                break;
                            case 'advice_on_discharge':
                                Header = 'Advice on Discharge';
                                break;
                            case 'urgent_care':
                                Header = 'Conditions Requiring Urgent Care';
                                break;
                            // case 'time_of_discharge':
                            //     Header = 'Time Of Discharge';
                            //     break;
                            case 'presenting_symptoms':
                                Header = 'Presenting Symptoms';
                                break;
                            case 'physical_findings':
                                Header = 'Physical Findings';
                                break;
                            case 'clinical_impression':
                                Header = 'Clinical Impression';
                                break;
                            case 'treatment_and_course':
                                Header = 'Treatment And Course';
                                break;
                            case 'final_diagnosis':
                                Header = 'Final Diagnosis';
                                break;
                            case 'condition_at_discharge':
                                Header = 'Condition at discharge';
                                break;
                            case 'recommendations':
                                Header = 'Recommendations';
                                break;
                            case 'follow_up_details':
                                Header = 'Follow Up Details';
                                break;
                            default:
                                Header = '';
                        }
                        responseHtml = '<div id="'+datas+'_data" style="margin-top:5px;margin-bottom: 5px"><p style="margin-bottom:3px"><u><b>'+Header+'</b></u></p><p>'+result[datas]+'</p></div>';
                        $('#summaryContentArea').append(responseHtml);

                        tinymce.get(datas).setContent(result[datas]);
                        saveDischargeSummaryContent(datas);
                    }
                }
            }
        },
        complete: function () {
            $('#myModalFavoriteList').modal('toggle');
        }
    });

}

function checkFillablesSelected(){
   return  $('#fillablesTable tr').find('input[type="checkbox"]:checked').length;
}

function loadViewData(visit_id){
    $.ajax({
          type: "GET",
          url:  $('#base_url').val()+"/Opsummary/LoadFavouriteSummary",
          data: 'favourite_summary_visit_id='+visit_id,
          beforeSend: function() {
          },
          success: function (data){
              if(data!= '0'){
                 var result = "";
                     result = JSON.parse(data);
                 var mywindow = window.open('', 'my div', 'height=3508,width=2480');
                 var summary_border_inactive = 0;
                 var summary_page_margin_top = 20;
                 var summary_default_font = "Times New Roman";

                 mywindow.document.write("<style>@page {size: A4; margin-top:'"+summary_page_margin_top+"'px !important; page-break-inside:auto; width: 21cm;min-height: 29.7cm;}.headergroupbg{background-color: #d2e8fc; font-size: 14px !important; font-family: "+summary_default_font+" !important;} body{margin:15px;outline: solid 0px ; padding :10px 10px 10px 10px;margin-bottom:35px;} div, table, p{ margin: auto; page-break-inside :auto}#doctor_footer_part{margin-bottom:20px;}</style>");

                 mywindow.document.write(result);
                 mywindow.document.close(); // necessary for IE >= 10
                 mywindow.focus(); // necessary for IE >= 10

                 mywindow.print();
                 mywindow.close();
                 return true;

              }else{
                Command: toastr["warning"]('Something went wrong');
              }
          },
          complete: function(){
          }
    });
}

function fetch_death_summary_templates(){
    $('#diagnosis').click();
    $("#category_name_search").val('');
    show_summary_templates('death');
}

function show_summary_templates(category_type){

    var url = $('#base_url').val()+"/Opsummary/showSummaryTemplate";
    var template_name = $('#template_name_search').val();
    var category = $("#category_name_search").val();
    var default_category = (category_type =='death')?'death':'0';
    $('#discharge_summary_main_header').html('DEATH SUMMARY');
    var obj = '';
    $.ajax({
        url:url,
        type: "GET",
        data: "show_template_value="+1+'&template_name='+template_name+'&category='+category+'&default_category='+default_category,
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success:function(data){
            if(data.substring(0, 11) == 'template_id'){
              var template_id = (data.split('=')[1]);
              fetch_discharge_summary_template(template_id,'DeathSummary');
              $("body").LoadingOverlay("hide");
              //$('#modalTemplateMaster').modal('show');
            }else{
              $('#summary_template_data').html(data);
              $("body").LoadingOverlay("hide");
             // $('#modalTemplateMaster').modal('show');
            }
        },
        complete: function(){
        }
    });
}

function showSummaryModal(){
    $('#summaryHistoryModal').modal('toggle');
 }

 //============fetch summary history=========================================================
 function showDischargeSummaryHistory(){
    var fromDate = $("#history_date_from").val();
    var toDate = $("#history_date_to").val();
    var user_id = $("#user_name_hidden").val();
    var url = $('#base_url').val()+"/Opsummary/showDischargeSummaryHistory";
    $.ajax({
                type: "GET",
                url: url,
                data: 'from_date='+ fromDate + '&to_date='+ toDate +'&user_id=' + user_id,
                beforeSend: function () {
                 $('#summary_history_content').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
                },
                success: function (data) {
                    $("#summary_history_content").LoadingOverlay("hide");
                    $('#summary_history_content').html(data);
                    var $table = $('table.theadfix_wrapper');

                    $table.floatThead({
                        scrollContainer: function($table){
                            return $table.closest('.theadscroll');
                        }

                    })

                    $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                    });

                },
                complete: function () {
                }
            });
}
//============fetch summary history ends=========================================================

//==========summary print preview creation=======================================================
    function createSummaryPrintFile(summary_id){
        var url = $('#base_url').val()+"/Opsummary/createSummaryPrintPreview";
        $.ajax({
            url:url,
            type: "GET",
            data: "summary_id="+summary_id,
            beforeSend: function() {
            },
            success:function(data){
                obj = JSON.parse(data);
                    var showw = "";
                    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
                    var msglist = obj;
                        showw = showw + msglist;
                    var summary_border_inactive = 0;
                    var summary_page_margin_top = "10";
                    var summary_default_font = "robotoregular";
                        mywindow.document.write("<style>@page {size: A4; margin-top:'"+summary_page_margin_top+"'px !important; page-break-inside:auto; width: 21cm;min-height: 29.7cm;}.headergroupbg{background-color: #d2e8fc; font-size: 14px !important; font-family: "+summary_default_font+" !important;} body{margin: 7px;outline: solid 0px ; padding :10px 10px 10px 10px;margin-bottom:35px;} div, table, p{ margin: auto; page-break-inside :auto}#doctor_footer_part{margin-bottom:20px;}</style>");
                    mywindow.document.write(showw);
                    mywindow.document.close(); // necessary for IE >= 10
                    mywindow.focus(); // necessary for IE >= 10
                    setTimeout(function(){ mywindow.print();
                    mywindow.close();
                    return true;}, 1500);
            },
            complete: function(){

            }
        });
    }
//==========summary print preview creation ends==================================================

//===========discharge summary templates=========================================================
function show_discharge_summary_templates(category_type){
    var url = $('#base_url').val()+"/Opsummary/showDischargeSummaryTemplate";
    var template_name = $('#template_name_search').val();
    var category = $("#category_name_search").val();
    var default_category = (category_type =='death')?'death':'0';
    var obj = '';
    $.ajax({
        url:url,
        type: "GET",
        data: "show_template_value="+1+'&template_name='+template_name+'&category='+category+'&default_category='+default_category,
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success:function(data){
            if(data.substring(0, 11) == 'template_id'){
              var template_id = (data.split('=')[1]);
              fetch_discharge_summary_template(template_id,'DeathSummary');
              $("body").LoadingOverlay("hide");
              $('#modalTemplateMaster').modal('show');
            }else{
              $('#summary_template_data').html(data);
              $("body").LoadingOverlay("hide");
              $('#modalTemplateMaster').modal('show');
            }
        },
        complete: function(){
        }
    });
}
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();


 $(document).on('click', '.pagination li a',function(event){
     event.preventDefault();
    $('li').removeClass('active');
    $(this).parent('li').addClass('active');
    event.preventDefault();
    var myurl = $(this).attr('href');
   var page=$(this).attr('href').split('page=')[1];
   summary_template_pagination(page);
});

function summary_template_pagination(page){

    var url = $('#base_url').val()+"/Opsummary/showDischargeSummaryTemplate";
    $.ajax({
        url:url,
        type: "GET",
        data: "show_template_value="+1+'&summary_template_page='+page,
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success:function(data){
            $('#summary_template_data').html(data);
            $("body").LoadingOverlay("hide");
        },
        complete: function(){
        }
    });
}

function addTemplate(){
    $('#modalAddSummaryTemplate').modal('toggle');
}

$('#btn_save_template').on('click', function() {

       var template_name = $('#template_name_add').val();
       var category = $( "#category_name_add" ).val();
       var edit_status = $("#summary_template_edit_status").val();
       var template_id = $("#summary_template_add_id").val();
       var dataparams = $("#form_add_template").serialize();
       var dataContent = tinyMCE.get('template_conent_text').getContent();
       var visibility_status = $("input[name='visibility_status']:checked"). val();
       dataContent = encodeURIComponent(dataContent.replace('&','\&'));
       if(template_name == ''){
           Command: toastr["warning"]('Please enter template name');
       }else if(dataContent == ''){
           Command: toastr["warning"]('Please enter template content');
       }else{
         var url = $('#base_url').val()+"/Opsummary/saveDischargeSummaryTemplate";
         $.ajax({
               type: "POST",
               url: url,
               data: dataparams +'&template_name='+ template_name + '&category='+ category +'&dataContent='+ dataContent+'&visibility_status='+visibility_status +'&template_id='+template_id+'&edit_status='+edit_status,
               beforeSend: function () {
               $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
               $('#btn_save_template').addClass('disabled');
               $("#btn_save_template").removeClass("fa fa-save");
               $("#btn_save_template").addClass("fa fa-spinner fa-spin'");
              },
              success: function (data) {
               if(data != 0){
                   if(data==1){
                       Command: toastr["success"]('Saved successfully');
                   }
                  $("body").LoadingOverlay("hide");
                  $("#btn_save_template").removeClass("disabled");
                  $("#btn_save_template").removeClass("fa fa-spinner fa-spin");
                  $("#btn_save_template").addClass("fa fa-save");
                  $('#form_add_template').find("input[type=text], textarea").val("");
                  tinyMCE.activeEditor.setContent('');
                  $('#modalAddTemplate').modal('hide');
                  if(data==2){
                       Command: toastr["success"]('Templated updated successfully');
                       $('#modalTemplateMaster').modal('hide');
                       setTimeout(function(){ show_summary_templates(); }, 1000);
                   }
               }else{
                  Command: toastr["error"]('Something went wrong');
                  $("body").LoadingOverlay("hide");
                  $("#btn_save_template").removeClass("disabled");
                  $("#btn_save_template").removeClass("fa fa-spinner fa-spin");
                  $("#btn_save_template").addClass("fa fa-save");
               }
              },
        });
    }

});

function preview_summary_custom_template(id){
    var url = $('#base_url').val()+"/Opsummary/previewDischargeSummaryCustomTemplate";
    $.ajax({
        type:"GET",
        url: url,
        data:'template_id='+ id,
        beforeSend: function () {
           $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (data) {
            console.log(data);
         $("body").LoadingOverlay("hide");
           if(data != 0){
             $('#customTemplatePreviewContent').html('');
             $('#customTemplatePreviewContent').html(data);
             $('#customTemplatePreviewModel').modal('toggle');
           }else{
             Command: toastr["error"]('Something went wrong');
           }
        },
    });
}

function fetch_discharge_summary_template(id,category_name){

    var url = $('#base_url').val()+"/Opsummary/fetchDischargeSummaryTemplate";

        $.ajax({
             type: "get",
             url: url,
             data: 'template_id='+ id,
             beforeSend: function () {
             $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});

             },
            success: function (data) {
             $("body").LoadingOverlay("hide");
             if(data != 0){
               Command: toastr["info"]('Template fetched into the editor');
               obj = JSON.parse(data);
               $('#modalTemplateMaster').modal('hide');
               var parentEditor = parent.tinyMCE.activeEditor;
               parentEditor.execCommand('mceInsertRawHTML', false, obj);

               if(category_name == 'DeathSummary'){
                 $('#discharge_summary_main_header').html('DEATH SUMMARY');
                 $('#summary_discharged_doctor_name_label').html('');
                 $('#summary_discharged_doctor_name').html('');

                 $('#header_row_5').remove();
               }

             }else{
               Command: toastr["error"]('Something went wrong');
             }
            },
      });
}


function edit_discharge_summary_template(id){
    var url = $('#base_url').val()+"/Opsummary/editDischargeSummaryTemplate";
          $.ajax({
                type: "get",
                url: url,
                data: 'template_id='+ id,
                beforeSend: function () {
                $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});

               },
               success: function (data) {
                  // console.log(data);
                $("body").LoadingOverlay("hide");
                if(data != 0){
                  var obj = JSON.parse(data);
                   $('#modalAddTemplate').modal('toggle');
                   $('#summary_template_edit_status').val(obj.edit_status);
                   $('#summary_template_add_id').val(obj.id);
                   $('#template_name_add').val(obj.template_name);
                   $('#category_name_add').val(obj.category_id).attr("selected", "selected");
                   tinyMCE.get('template_conent_text').setContent(obj.template_content);
                   $("input[name=visibility_status][value=" + obj.visibility_status + "]").prop('checked', true);
                }else{
                  Command: toastr["error"]('Something went wrong');
                }
               },
         });
  }

   //-----------------delete discharge summary template--------------------------------------
   function delete_summary_template(id){
    var url = $('#base_url').val()+"/Opsummary/deleteSummaryTemplate";
      $.ajax({
           type: "get",
           url: url,
           data: 'template_id='+ id,
           beforeSend: function () {
           $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});

           },
          success: function (data) {
           $("body").LoadingOverlay("hide");
           if(data != 0){
             Command: toastr["error"]('Template deleted successfully');
             $('#modalTemplateMaster').modal('hide');
              setTimeout(function(){ show_summary_templates(); }, 1000);
           }else{
             Command: toastr["error"]('Something went wrong');
           }
          },
    });
   }

//===========discharge summary templates ends====================================================

function add_new_template_category(){
    $('#modalAddTemplateCategory').modal('toggle');
}

function save_new_template_category(){
    var category_name = $('#template_category_name_add').val();
    if(category_name == ''){
        Command: toastr["error"]('Please enter category name!');
    }else{
      var url = $('#base_url').val()+"/Opsummary/templateCategoryAdd";
       $.ajax({
             type: "get",
             url: url,
             data: 'category_name='+ category_name,
             beforeSend: function () {
             $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});

             },
            success: function (data) {
             $("body").LoadingOverlay("hide");
             if(data == 2){
               Command: toastr["warning"]('Category already exists!');
             }else if(data == 0){
               Command: toastr["error"]('Something went wrong!');
             }
             else{
               Command: toastr["success"]('category added successfully');
               obj = JSON.parse(data);
               $('#category_name_add').append($('<option/>', {
                    value: obj.id,
                    text : obj.name
                }));

               $('#modalAddTemplateCategory').modal('hide');
            }

            },
      });

    }
}

function email_summary(){
    var visit_id = $('#visit_id').val();
    if(visit_id == ''){
        Command: toastr["warning"]('Select patient!');
        return false;
    }else{
        $('#summaryEmailModal').modal('toggle');
    }
}

function send_summary_via_email(){

        var patient_email = $('#patient_email').val();
        var visit_id = $('#visit_id').val();
        if(patient_email !='' && visit_id != '' ){
          if (validateEmail(patient_email)) {
            var url = $('#base_url').val()+"/Opsummary/sendSummaryViaEmail";
            $.ajax({
            type: "GET",
                    url: url,
                    data: 'visit_id='+visit_id+'&patient_email='+patient_email,
                    beforeSend: function(){
                       $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
                    },
                    success: function(html){
                        $("body").LoadingOverlay("hide");
                        if(html ==1){
                            Command: toastr["success"]('Email sent successfully!');
                            $('#summaryEmailModal').modal('toggle');
                        }else{
                            Command: toastr["error"]('Something went wrong!');
                        }
                    },
                    complete: function(){
                    }
            });
          }else{
             Command: toastr["warning"]('Invalid email address');
          }

        }else{
          Command: toastr["warning"]('Plese enter patient email');
        }

}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$(document).on('dblclick', '#summary_content', function(){
    pasteHtmlAtCaret('<p style="page-break-after: always;">&nbsp;</p>');
})






function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("span");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}

function deleteFavouriteSummary(summary_id){
    var url = $('#base_url').val()+"/Opsummary/deleteFavouriteSummary";
    $.ajax({
    type: "GET",
            url: url,
            data: 'summary_id='+summary_id,
            beforeSend: function(){
               $("#myModalFavoriteListData").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function(html){

                $("#myModalFavoriteListData").LoadingOverlay("hide");
                if(html == 1){
                    $('#'+summary_id).remove();
                    Command: toastr["success"]('Favourite deleted!');
                }
            },
            complete: function(){
            }
    });
}

function summaryRefresh(){
    var visit_id = $('#visit_id').val();
    if(visit_id == ''){
        Command: toastr["warning"]('Please select patient!');
        return false;
    }
    var url = $('#base_url').val()+"/Opsummary/summaryRefresh";
    bootbox.confirm({
        message: "Are you sure you want to refresh the summary. Please note the saved content may be updated!",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true){

                $.ajax({
                    type: "GET",
                            url: url,
                            data: 'visit_id='+visit_id,
                            beforeSend: function(){
                                $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
                            },
                            success: function(html){

                                if(html != '0'){
                                    var obj = JSON.parse(html);
                                    if(obj.diagnosis != null && obj.diagnosis!=''){
                                        tinyMCE.get('diagnosis').setContent(obj.diagnosis);
                                    }
                                    if(obj.allergies != null && obj.allergies !=''){
                                        tinyMCE.get('allergies').setContent(obj.allergies);
                                    }
                                    if(obj.history_findings != null && obj.history_findings !=''){
                                        tinyMCE.get('history_findings').setContent(obj.history_findings);
                                    }
                                    if(obj.surgery != null && obj.surgery != ''){
                                        tinyMCE.get('surgery').setContent(obj.surgery);
                                    }
                                    if(obj.investigation != null && obj.investigation !=''){
                                        tinyMCE.get('investigation').setContent(obj.investigation);
                                    }
                                    if(obj.surgery_findings_treatments != null && obj.surgery_findings_treatments !=''){
                                        tinyMCE.get('surgery_findings_treatments').setContent(obj.surgery_findings_treatments);
                                    }
                                    if(obj.advice_on_discharge != null && obj.advice_on_discharge !=''){
                                        tinyMCE.get('advice_on_discharge').setContent(obj.advice_on_discharge);
                                    }
                                    if(obj.course_in_hospital != null && obj.course_in_hospital !=''){
                                        tinyMCE.get('course_in_hospital').setContent(obj.course_in_hospital);
                                    }
                                    if(obj.treatment_progress != null && obj.treatment_progress !=''){
                                        tinyMCE.get('treatment_progress').setContent(obj.treatment_progress);
                                    }
                                    if(obj.discussion != null && obj.discussion !=''){
                                        tinyMCE.get('discussion').setContent(obj.discussion);
                                    }
                                    if(obj.plan != null && obj.plan !=''){
                                        tinyMCE.get('plan').setContent(obj.plan);
                                    }
                                    if(obj.urgent_care != null && obj.urgent_care !=''){
                                        tinyMCE.get('condition_of_patient').setContent(obj.urgent_care);
                                    }
                                    // if(obj.time_of_discharge != null && obj.time_of_discharge !=''){
                                    //     tinyMCE.get('time_of_discharge').setContent(obj.time_of_discharge);
                                    // }
                                    if(obj.presenting_symptoms != null && obj.presenting_symptoms !=''){
                                        tinyMCE.get('presenting_symptoms').setContent(obj.presenting_symptoms);
                                    }
                                    if(obj.physical_findings != null && obj.physical_findings !=''){
                                        tinyMCE.get('physical_findings').setContent(obj.physical_findings);
                                    }
                                    if(obj.clinical_impression != null && obj.clinical_impression !=''){
                                        tinyMCE.get('clinical_impression').setContent(obj.clinical_impression);
                                    }
                                    if(obj.treatment_and_course != null && obj.treatment_and_course !=''){
                                        tinyMCE.get('treatment_and_course').setContent(obj.treatment_and_course);
                                    }
                                    if(obj.final_diagnosis != null && obj.final_diagnosis !=''){
                                        tinyMCE.get('final_diagnosis').setContent(obj.final_diagnosis);
                                    }
                                    if(obj.condition_at_discharge != null && obj.condition_at_discharge !=''){
                                        tinyMCE.get('condition_at_discharge').setContent(obj.condition_at_discharge);
                                    }
                                    if(obj.recommendations != null && obj.recommendations !=''){
                                        tinyMCE.get('recommendations').setContent(obj.recommendations);
                                    }
                                    if(obj.follow_up_details != null && obj.follow_up_details !=''){
                                        tinyMCE.get('follow_up_details').setContent(obj.follow_up_details);
                                    }

                                }
                                $("body").LoadingOverlay("hide");
                            },
                            complete: function(){
                            }
                    });

            }
        }
    });

}
