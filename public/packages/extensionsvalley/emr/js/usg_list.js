var search_timeout = null;
function usg_patient_search(txt) {
    var last_search_key = '';
    var request_flag = '';
    if (txt.length >= 2 && $.trim(txt) != '') {

        clearTimeout(search_timeout);
        search_timeout = setTimeout(function () {

            if (last_search_key == txt || request_flag == 1) {
                return false;
            }
            last_search_key = txt;
            var url = $('#base_url').val() + "/emr/generalPatientSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'txt=' + txt +'&usg_search=1',
                beforeSend: function () {
                    $("#usgpatientbox").empty();
                    $("#usgpatientbox").show();
                    $(".input_rel .input_spinner").removeClass('hide');
                    $("#usgpatientbox").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    request_flag = 1
                },
                success: function (data) {

                    $("#usgpatientbox").LoadingOverlay("hide");
                    $("#usgpatientbox").html(data);
                    $(".input_rel .input_spinner").addClass('hide');
                    request_flag = 0;
                }
            });
        });
    } else if (txt.length === 0) {
        $("#usgpatientbox").hide();
        $("#usgpatientbox").LoadingOverlay("hide");
    }
}

function fetchUsgPatient(id,patient_name,title){
    $('.ajaxSearchBox').hide();
    $('#usg_patient_search_hidden').val(id);
    $('#usg_patient_search_txt').val(title+' '+patient_name);
}

function usg_report_search(){
    var from_date = $('#usg_from_date').val();
    var to_date = $('#usg_to_date').val();
    var patient_id = $('#usg_patient_search_hidden').val();
    var url = $('#base_url').val() + "/ultrasound/usgReportSearch";
    $.ajax({
        type: "GET",
        url: url,
        data: 'from_date=' + from_date + '&to_date=' + to_date + '&patient_id=' + patient_id,
        beforeSend: function () {
            $("#usg_reports_list_container").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("#usg_reports_list_container").html(data);
        },
        complete: function () {
            $("#usg_reports_list_container").LoadingOverlay("hide");
            setTimeout(function () {
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            }, 1000);

        }
    });
}

function view_usg_report(head_id, form_id,patient_id) {
    if (head_id != '' && head_id != undefined) {

        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/emr/load-preview",
            data: {
                head_id: head_id,
                form_id: form_id,
                patient_id: patient_id,
                _token: _token
            },
            beforeSend: function () {
                $('#usg_template_viewer_modal').modal('show');
                $('#usg_result_view').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                let response = '';

                if (data.status = 1) {
                    response = data.html;
                } else {
                    response = '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                }

                $('#usg_result_view').html(response);
            },
            complete: function () {
                $('#usg_result_view').LoadingOverlay("hide");
            }
        });
    }
}

function finalize_usg_report(report_id) {
    if (report_id != '' && report_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/ultrasound/finalize-usg-report",
            data: {
                id: report_id,
                _token: _token
            },
            beforeSend: function () {
                $('#'+report_id+'_action').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 1) {
                    toastr.success(data.message);
                    usg_report_search();
                } else {
                    toastr.error(data.message);
                }
            },
            complete: function () {
                $('#'+report_id+'_action').LoadingOverlay("hide");
            }
        });
    }
}
function definalize_usg_report(report_id) {
    if (report_id != '' && report_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/ultrasound/definalize-usg-report",
            data: {
                id: report_id,
                _token: _token
            },
            beforeSend: function () {
                $('#'+report_id+'_action').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 1) {
                    toastr.success(data.message);
                    usg_report_search();
                } else {
                    toastr.error(data.message);
                }
            },
            complete: function () {
                $('#'+report_id+'_action').LoadingOverlay("hide");
            }
        });
    }
}
