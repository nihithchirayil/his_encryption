
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
   
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_room_type_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});
$('#hourly_rent').click(function() {
    $('#daily_rent').not('#hourly_rent').removeAttr('checked');
  }); 
   $('#daily_rent').click(function() {
    $('#hourly_rent').not('#daily_rent').removeAttr('checked');
  });
    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};

        data.room_type = $("#room_type").val();
        data.service_desc = $("#service_desc").val();
        data.hourly_rent = $(".hourly_rent").val();
        data.daily_rent = $(".daily_rent").val();
        data.icu = $(".icu").val();
        data.day_care_bed = $(".day_care_bed").val();
        data.baby_bed = $(".baby_bed").val();
        data.required_last_day_rent = $(".required_last_day_rent").val();
        console.log(data.service_desc);

        if ($("#hourly_rent").is(":checked")) {
            // alert("chcked");
            data.hourly_rent =1;
        }else{
            data.hourly_rent =0;
        }
        if($(".daily_rent").is(":checked")) {
            // alert("chcked");
            data.daily_rent =2;
        }else{
            data.daily_rent =0;
        }
        if($(".icu").prop('checked') == true){
            // alert("chcked");
            data.icu ='t';
        }else{
            data.icu ='f';
        }
        if($(".day_care_bed").prop('checked') == true){
            // alert("chcked");
            data.day_care_bed ='t';
        }else{
            data.day_care_bed ='f';
        }
        if($(".baby_bed").prop('checked') == true){
            // alert("chcked");
            data.baby_bed ='t';
        }else{
            data.baby_bed ='f';
        }
        if($(".required_last_day_rent").prop('checked') == true){
            // alert("chcked");
            data.required_last_day_rent ='t';
        }else{
            data.required_last_day_rent ='f';
        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }
    $('#hourly_rent_add').click(function() {
        $('#daily_rent_add').not('#hourly_rent_add').removeAttr('checked');
      }); 
       $('#daily_rent_add').click(function() {
        $('#hourly_rent_add').not('#daily_rent_add').removeAttr('checked');
      });
    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".room_type_name").val().trim() == ''){
            Command: toastr["error"]("Please enter Room Type Name");
            return;
        }
        if($("#service_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Service");
            return;
        }

        data.room_type_id = $(".reference_id").val();
        data.room_type_name = $("#room_type_name").val();
        data.service_desc = $("#service_add").val();
        data.icu = $(".icu_add").val();
        data.day_care_bed = $("#day_care_bed_add").val();
        data.baby_bed = $(".baby_bed_add").val();
        data.required_last_day_rent = $(".required_last_day_rent_add").val();
        data.order_level = $(".order_level").val();
        data.status = $(".status_add").val();
// alert(data.allow_pay_from_cash);
       
        if($(".status_add").prop('checked') == true){
            // alert("chcked");
            data.status =1;
        }else{
            data.status =0;
        }
       data.hourly_rent= $('input[name="hourly"]:checked').val();
      
        if($(".icu_add").prop('checked') == true){
            // alert("chcked");
            data.icu ='t';
        }else{
            data.icu ='f';
        }
        if($(".day_care_bed_add").prop('checked') == true){
            // alert("chcked");
            data.day_care_bed ='t';
        }else{
            data.day_care_bed ='f';
        }
        if($(".baby_bed_add").prop('checked') == true){
            // alert("chcked");
            data.baby_bed ='t';
        }else{
            data.baby_bed ='f';
        }
        if($(".required_last_day_rent_add").prop('checked') == true){
            // alert("chcked");
            data.required_last_day_rent ='t';
        }else{
            data.required_last_day_rent ='f';
        }
      console.log(data);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Room Type Name Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }
                   else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
   
    function editItem(element){

        let room_type_id = $(element).parents('tr').attr('data-id');
        let room_type_name = $(element).parents('tr').attr('data-room_type_name');
        let service = $(element).parents('tr').attr('data-service');
        let hourly_rent = $(element).parents('tr').attr('data-rent');
        let icu = $(element).parents('tr').attr('data-icu');
        let day_care_bed = $(element).parents('tr').attr('data-day_care_bed');
        let baby_bed = $(element).parents('tr').attr('data-baby_bed');
        let required_last_day_rent = $(element).parents('tr').attr('data-required_last_day_rent');
        let order_level = $(element).parents('tr').attr('data-order_level');
        let status = $(element).parents('tr').attr('data-status');
        if(status==1){
                        $(".status_add").prop("checked", true);

            } else{
                        $(".status_add").prop("checked", false);
                   }
        
            if(hourly_rent==0){
                        $(".rent_add").prop("checked", false);
                
            } else if(hourly_rent==1){
                         $("#hourly_rent_add").prop("checked", true);

            } else if(hourly_rent==2){
                $("#daily_rent_add").prop("checked", true);

            }
          
           if(icu==1){
                        $(".icu_add").prop("checked", true);
                
            } else{
                         $(".icu_add").prop("checked", false);
                   }
           if(day_care_bed==1){
                       $(".day_care_bed_add").prop("checked", true);
               
           } else{
                        $(".day_care_bed_add").prop("checked", false);
                  }
           if(baby_bed==1){
                        $(".baby_bed_add").prop("checked", true);
                
            } else{
                         $(".baby_bed_add").prop("checked", false);
                   }
           if(required_last_day_rent==1){
                        $(".required_last_day_rent_add").prop("checked", true);
                
            } else{
                         $(".required_last_day_rent_add").prop("checked", false);
                   }
         
        $(".reference_id").val(room_type_id);
        $("#room_type_name").val(room_type_name);
        $("#service_add").val(service);
        $("#service_add").select2().trigger('change');    
        $(".order_level").val(order_level);
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $("#add").html('Edit Room Type');

    }

  
    function resetForm(){
        $(".reference_id").val('');
        $(".room_type_name").val('');
        $(".select2").val('').trigger('change');
        $(".order_level").val('');
        $("#add").html('Add Room Type');
        $('#status_add').removeAttr('checked');
        $('#hourly_rent_add').removeAttr('checked');
        $('#daily_rent_add').removeAttr('checked');
        $('#icu_add').removeAttr('checked');
        $('#day_care_bed_add').removeAttr('checked');
        $('#baby_bed_add').removeAttr('checked');
        $('#required_last_day_rent_add').removeAttr('checked');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

