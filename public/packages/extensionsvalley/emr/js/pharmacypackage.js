$(document).ready(function() {
    packageHeadList();
});
$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();
        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
            $('#item_nameAjaxDiv').hide();
        } else {
            var base_url = $('#base_url').val()

            var url = base_url + "/master/ajaxsearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();

                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);


    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();

}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
});

function packageHeadList() {

    var base_url = $('#base_url').val();
    var url = base_url + "/master/pharmacyPackageList";
    $.ajax({
        type: "POST",
        url: url,
        beforeSend: function() {
            $("#package_head_list").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })


        },
        success: function(data) {
            // console.log(data);
            $("#package_head_list").html(data);

        },
        complete: function() {

            $("#package_head_list").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
    });
}

function packageDetailList(id) {
    var base_url = $('#base_url').val();
    $('#newPkgId').val(id);
    var url = base_url + "/master/pharmacyPackageDetails";
    var param = {
        id: id
    }
    var pkg_name = $('.pkg_head_' + id).html();
    $('#package_name').html(pkg_name);
    $('#packageName').val(pkg_name);
    $('#pkg_edit').addClass("fa fa-edit");



    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#package_detail").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })


        },
        success: function(data) {
            // console.log(data);
            $("#packageDetails").html(data);

        },
        complete: function() {

            $("#package_detail").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
    });


}
$("#pkgBtn").click(function() {
    $("#packageName").toggle();
    $("#pkghd").toggle();
    $('#newPkgId').val('');
    $('#package_name').html('');
    $('#packageName').val('');
    $('#packageDetails').empty();

    $('#pkg_edit').removeClass("fa fa-edit");
});

var wage = document.getElementById("packageName");
wage.addEventListener("keydown", function(event) {
    if (event.keyCode === 13) {
        //checks whether the pressed key is "Enter"
        addNewPackage(event);
    }
});

function addNewPackage(event) {
    if (event) {
        var base_url = $('#base_url').val();
        var url = base_url + "/master/addNewPackage";
        var package_name = event.target.value;
        var edit_head_id = $('#newPkgId').val();
        console.log(package_name);
        // $('#package_name').html(package_name);
        $("#packageName").toggle();
        $("#pkghd").toggle();
        param = {
            package_name: package_name,
            edit_head_id: edit_head_id,
        }

        if (package_name) {
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#package_head_list").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: "#337AB7",
                    })
                    clearDetails();


                },
                success: function(data) {
                    var obj = JSON.parse(data);
                    if (obj.status == 1) {
                        toastr.success(obj.message);
                        packageDetailList(obj.head_id);
                        $('#newPkgId').val(obj.head_id);

                    } else {
                        toastr.warning(obj.message);
                    }
                },
                complete: function() {
                    $("#package_head_list").LoadingOverlay("hide");
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                    packageHeadList();
                    $('#item_name').focus();
                    $('#package_name').html(package_name);


                },
            });

        }
    }
}

function addNewpackageItem() {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/savePackageItem";
    var phg_head_id = $('#newPkgId').val() > 0 ? $('#newPkgId').val() : 0;
    var item_name = $('#item_name').val();
    var Quantity = $('#Qty').val() > 0 ? $('#Qty').val() : 1;
    var amount = $('#amount').val() > 0 ? $('#amount').val() : 0;
    var item_id = $('#item_name_hidden').val();
    var param = {
        phg_head_id: phg_head_id,
        item_name: item_name,
        Quantity: Quantity,
        amount: amount,
        item_id: item_id,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $('#package_detail').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function(data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
                clearDetails();
                packageDetailList(phg_head_id);
            } else {
                toastr.error(obj.message);
            }
        },
        complete: function() {
            $("#package_detail").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            $('#item_name').focus();



        },
    });

}

function clearDetails() {
    $('#item_name_hidden').val('');
    $('#amount').val('');
    $('#Qty').val('');
    $('#item_name').val('');
}

function editPackage() {
    $("#packageName").toggle();
    $("#pkghd").toggle();
    var pkg_name = $('#package_name').html();
    $('#packageName').val(pkg_name);

}

function deletePackage() {
    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function(result) {
            if (result) {

                var base_url = $('#base_url').val();
                var url = base_url + "/master/deletePackage";
                var delete_id = [];
                $('.delet_pkg:checked').each(function() {
                    var del_id = $(this).attr('data-id');
                    delete_id.push(del_id);
                })
                param = {
                    delete_id: delete_id,
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#package_head_list").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })


                    },
                    success: function(data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        } else {
                            toastr.warning(obj.message);
                        }
                    },
                    complete: function() {
                        $("#package_head_list").LoadingOverlay("hide");
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                        packageHeadList();
                        $('#packageDetails').empty();
                        $('#pkg_edit').removeClass("fa fa-edit");
                        $('#package_name').html('');

                    },
                });
            }
        }
    });


}

function deleteItem(id) {

    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function(result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deletePackageItems";
                var head_id = $('#newPkgId').val();
                var param = {
                    head_id: head_id,
                    detail_id: id,
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#package_head_list").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })
                    },
                    success: function(data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        } else {
                            toastr.success(obj.message);
                        }
                    },
                    complete: function() {
                        $("#package_head_list").LoadingOverlay("hide");
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                        packageHeadList();
                        packageDetailList(head_id)
                    },
                });
            }
        }
    });


}