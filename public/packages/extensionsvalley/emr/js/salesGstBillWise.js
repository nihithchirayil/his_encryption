$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({
        placeholder: "",
        maximumSelectionSize: 6
    });
});

$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
document.getElementById("bill_date_from").blur();
document.getElementById("bill_date_to").blur();
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = $(this).attr('id');
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var search_key = $(this).val();

    var param = {
        bill_no: search_key,
    };

    if (input_id == 'patient_name') {
        param = {
            patient_name: search_key,
        };
    }

    if (event.keyCode == 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (search_key) {

            $.ajax({
                type: "GET",
                url: '',
                data: param,
                beforeSend: function () {
                    $("#" + input_id + 'AjaxDiv').html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + input_id + 'AjaxDiv').html(html).show();
                    $("#" + input_id + 'AjaxDiv').find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            $("#" + input_id + 'AjaxDiv').html('').hide();
            if (input_id == 'bill_no') {
                $('#bill_no_hidden').val('');
                $('#bill_type_hidden').val('');
            } else {
                $('#patient_nameuhid').val('');
            }
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillFullBillNo(bill_id, bill_no, bill_type) {
    $('#bill_no_hidden').val(bill_id);
    $('#bill_no').val(bill_no);
    $('#bill_type_hidden').val(bill_type);
    $("#bill_noAjaxDiv").hide();
}

function fillPatientValues(uhid, patient_name, patient_id) {
    $('#patient_nameuhid').val(uhid);
    $('#patient_name').val(patient_name);
    $("#patient_nameAjaxDiv").hide();
}


function getReportData() {
    var base_url = $('#base_url').val();
    var from_date = $('#bill_date_from').val();
    var to_date = $('#bill_date_to').val();
    var bill_no = $('#bill_no_hidden').val();
    var bill_type = $('#bill_type_hidden').val();
    var uhid = $('#patient_nameuhid').val();
    var patient_type = $('#patient_type').val();
    var paid_status = $('#paid_status').val();
    var param = {
        from_date: from_date,
        to_date: to_date,
        bill_no: bill_no,
        bill_type: bill_type,
        uhid: uhid,
        patient_type: patient_type,
        paid_status: paid_status
    }

    var url = base_url + "/reports/getSalesBillWiseGSTReportData";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });

}



function search_clear() {
    var current_date = $('#current_date').val();
    $(".datepicker").val(current_date);
    $('#bill_no').val('');
    $('#bill_no_hidden').val('');
    $('#bill_type_hidden').val('');
    $('#patient_name').val('');
    $('#patient_nameuhid').val('');
    $('#bill_type_hidden').val('');
    $('#patient_type').val('All').select2();
    $('#paid_status').val('All').select2();
    getReportData();
}
