$(document).ready(function () {
    $('#bill_no').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('bill_no_AjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var value = event.key;
            var cancelled_bills = $('#cancelled_bills_only').is(":checked");
            var bill_no = $(this).val();
            if (!bill_no) {
                $("#bill_no_AjaxDiv").html("");
                $('#bill_no_hidden').val('');
            } else {
                var url = '';
                var param = {
                    bill_no: bill_no,
                    cancelled_bills: cancelled_bills
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#bill_no_AjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        $("#bill_no_AjaxDiv").html(html).show();
                        $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("bill_no_AjaxDiv", event);
        }
    });
});

function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
    $('#bill_no').val(bill_no);
    $('#bill_id_hidden').val(bill_id);
    $('#bill_no_AjaxDiv').hide();
    searchBill();
}

$(document).keypress(function (event) {
    if (event.keyCode == 13) {
        searchBill();
    }
});

function ClearDiscount() {
    $('#bill_no').val('');
}


function searchBill() {
    var bill_no = $('#bill_no').val();
    var bill_id = $('#bill_id_hidden').val();
    var token = $('#token_hiddendata').val();
    var contain_search = $('#contain_search').is(":checked");
    if (bill_no) {
        var param = {
            _token: token,
            bill_no: bill_no,
            bill_id: bill_id,
            contain_search: contain_search
        };
        var url = $('#base_url').val() + "/discountbill/discountbilllist";
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#pending_bil_list").html('Please Wait!!!');
                $('#searchdatabtn').attr('disabled', true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner fa-spin');
            },
            success: function (msg) {
                $('#pending_bil_list').html(msg);
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
                $('#searchdataspin').addClass('fa fa-search');
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            error: function () {
                toastr.error("Please Check Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Bill Number");
    }
}


function showBillingDetail(bill_id, bill_type) {
    $('#bill_amount').val("");
    var url = $('#base_url').val();
    var bill_no = $('#bill_nodata' + bill_id).html();
    var token = $('#token_hiddendata').val();
    var param = { _token: token, bill_id: bill_id, bill_type: bill_type };
    $.ajax({
        type: "POST",
        url: url + "/discountbill/billingdetailview",
        data: param,
        beforeSend: function () {
            $('#billing_detail_head').html(bill_no);
            $('#showBillingDetailBtn' + bill_id).attr('disabled', true);
            $('#showBillingDetailSpin' + bill_id).removeClass('fa fa-eye');
            $('#showBillingDetailSpin' + bill_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#billing_detail_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#billing_detail_content').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#showBillingDetailBtn' + bill_id).attr('disabled', false);
            $('#showBillingDetailSpin' + bill_id).removeClass('fa fa-spinner fa-spin');
            $('#showBillingDetailSpin' + bill_id).addClass('fa fa-eye');
        },
        error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}


function SaveDiscount(id, bill_type) {
    bootbox.confirm({
        message: "Are you sure you want to give discount ?",
        buttons: {
            'confirm': {
                label: "Discount",
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/discountbill/SaveDiscount";
                var token = $('#token_hiddendata').val();
                var bill_head_id = $('#bill_head_id' + id).val();
                var discount_type = $('#discount_type_' + id).val();
                var discount_value = $('#discount_value_' + id).val();
                var discount_amount = $('#discount_amount_' + id).val();
                var bill_amount = $('#bill_amount_' + id).val();
                var net_amount = $('#net_amount_' + id).val();
                var param = { _token: token, bill_type: bill_type, net_amount: net_amount, discount_type: discount_type, discount_value: discount_value, id: id, bill_head_id: bill_head_id, discount_amount: discount_amount, bill_amount: bill_amount };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#SaveDiscountBtn' + id).attr('disabled', true);
                        $('#SaveDiscountSpin' + id).removeClass('fa fa-save');
                        $('#SaveDiscountSpin' + id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (parseInt(obj.status) == 1) {
                            toastr.success('Successfully updated');
                        } else if (parseInt(obj.status) == 3) {
                            bootbox.confirm({
                                message: "Do you want to refresh bill ?",
                                buttons: {
                                    'confirm': {
                                        label: "Yes",
                                        className: 'btn-primary',
                                        default: 'true'
                                    },
                                    'cancel': {
                                        label: 'No',
                                        className: 'btn-warning'
                                    }
                                },
                                callback: function (result) {
                                    if (result) {
                                        searchBill();
                                    }
                                }
                            });
                            toastr.warning(obj.message);
                        } else {
                            toastr.warning(obj.message);
                        }
                    },
                    complete: function () {
                        $('#SaveDiscountBtn' + id).attr('disabled', false);
                        $('#SaveDiscountSpin' + id).removeClass('fa fa-spinner fa-spin');
                        $('#SaveDiscountSpin' + id).addClass('fa fa-save');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }
        }
    });
}

function deleteDiscount(id, bill_type) {
    bootbox.confirm({
        message: "Are you sure you want to Delete Discount ?",
        buttons: {
            'confirm': {
                label: "Delete",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var url = $('#base_url').val() + "/discountbill/deleteDiscount";
                var token = $('#token_hiddendata').val();
                var bill_head_id = $('#bill_head_id' + id).val();
                var discount_type = $('#discount_type_' + id).val();
                var discount_value = $('#discount_value_' + id).val();
                var discount_amount = $('#discount_amount_' + id).val();
                var bill_amount = $('#bill_amount_' + id).val();
                var net_amount = $('#net_amount_' + id).val();
                var param = { _token: token, discount_type: discount_type, bill_type: bill_type, discount_value: discount_value, id: id, bill_head_id: bill_head_id, discount_amount: discount_amount, bill_amount: bill_amount, net_amount: net_amount };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#deleteDiscountBtn' + id).attr('disabled', true);
                        $('#deleteDiscountSpin' + id).removeClass('fa fa-trash');
                        $('#deleteDiscountSpin' + id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data == 1) {
                            toastr.success('Deleted Successfully');
                            searchBill();
                        }
                        else {
                            toastr.warning("Dicount Cannot Delete!<br>Please Recheck Bill Status.");
                        }
                    },
                    complete: function () {
                        $('#deleteDiscountBtn' + id).attr('disabled', false);
                        $('#deleteDiscountSpin' + id).removeClass('fa fa-spinner fa-spin');
                        $('#deleteDiscountSpin' + id).addClass('fa fa-trash');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }
        }
    });
}

function itemAmountCalculation(i) {
    var discount_type = ($('#discount_type_' + i).val() != '') ? $('#discount_type_' + i).val() : 0;
    var tot_amount = ($('#bill_amount_' + i).val() != '') ? $('#bill_amount_' + i).val() : 0;
    //var bill_amount = ($('#netamtroundhidden' + i).val() != '') ? $('#netamtroundhidden' + i).val() : 0;
    //alert($('#bill_amount_hidden' + i).val());
    var bill_amount = parseFloat(tot_amount);
    if (discount_type == 1) {
        bill_amount = ($('#bill_amount_hidden' + i).val() != '') ? $('#bill_amount_hidden' + i).val() : 0;
    }
    var bill_amount_new = parseFloat(bill_amount);
    var discount_value = ($('#discount_value_' + i).val() != '') ? $('#discount_value_' + i).val() : 0;
    if (discount_value == '' || parseFloat(discount_value) <= 0) {
        $('#discount_amount_' + i).val(parseFloat(tot_amount) - parseFloat(bill_amount));
        $('#net_amount_' + i).val(bill_amount);
    } else if (bill_amount_new != 0 && discount_type == 2) {
        if (parseFloat(discount_value) > bill_amount) {
            $('#discount_value_' + i).val(0);
            $('#discount_amount_' + i).val(parseFloat(tot_amount) - parseFloat(bill_amount));
            $('#net_amount_' + i).val(bill_amount);
            toastr.warning("Discount Cannot Be Greater Than Bill Amount");
        } else {
            $('#discount_amount_' + i).val(discount_value);
            $('#net_amount_' + i).val(parseFloat(bill_amount) - parseFloat(discount_value));
        }
    } else if (bill_amount_new != 0 && discount_type == 1) {
        if (parseFloat(discount_value) > 100) {
            $('#discount_value_' + i).val(100);
            discount_value = 100;
        }
        var percent = (discount_value / 100) * bill_amount;
        $('#discount_amount_' + i).val(percent.toFixed(2));

        $('#net_amount_' + i).val(parseFloat(tot_amount) - parseFloat(percent));
    }
}


function myFunction(i) {
    if ($('#discount_value_' + i).val())
        $('#discount_value_' + i).trigger('keyup');
    /*
    var discount_type = ($('#discount_type_' + i).val() != '') ? $('#discount_type_' + i).val() : 0;
    var discount_value = ($('#discount_value_' + i).val() != '') ? $('#discount_value_' + i).val() : 0;
    //var bill_amount = ($('#bill_amount_' + i).val() != '') ? $('#bill_amount_' + i).val() : 0;
    var bill_amount = parseFloat(tot_amount);
    var bill_amount_new = parseFloat(bill_amount);
    var net_amount = $('#netamtroundhidden' + i).val();
    if (discount_value != '' && parseFloat(discount_value) > 0) {
        if (bill_amount_new != 0 && discount_type == 2) {
            if (parseFloat(discount_value) > bill_amount) {
                $('#discount_value_' + i).val(0);
                $('#discount_amount_' + i).val(parseFloat(bill_amount) - parseFloat(net_amount));
                $('#net_amount_' + i).val(net_amount);
                toastr.warning("Discount Cannot Be Greater Than Bill Amount");
            } else {
                $('#discount_amount_' + i).val(discount_value);
                $('#net_amount_' + i).val(parseFloat(net_amount) - parseFloat(discount_value));
            }
        } else if (discount_type == 1) {
            if (parseFloat(discount_value) > 100) {
                $('#discount_value_' + i).val(100);
                itemAmountCalculation(i);
            } else {
                if (discount_value == 0) {
                    $('#net_amount_' + i).val(bill_amount);
                } else {

                    var percent = (discount_value / 100) * net_amount;
                    $('#discount_amount_' + i).val(percent);
                    //$('#net_amount_' + i).val(parseFloat(net_amount) - parseFloat(percent));
                    $('#net_amount_' + i).val(parseFloat(bill_amount) - parseFloat(percent));
                }
            }
        }
    } else {
        $('#discount_amount_' + i).val(parseFloat(bill_amount) - parseFloat(net_amount));
        $('#net_amount_' + i).val(net_amount);
    }
    */
}












