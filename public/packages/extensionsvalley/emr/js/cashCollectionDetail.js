$(document).ready(function() {
    setTimeout(function() {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#payment_date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#payment_date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 10 });
});
$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});
document.getElementById("payment_date_from").blur();
document.getElementById("payment_date_to").blur();


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});



function getReportData() {
    var url = route_json.cashierCollectionReporResults;

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function() {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function() {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function() {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
           
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}


function cashierCollectionDetailsReport() {
    var url = route_json.searchcashiercollectionreportdetail;

    //-------filters---------------------------------------------

    var filters_list = new Object();



    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function() {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
    var bill_date_sort =0;
    if($("#sort_by_billdate").prop('checked') == true){
        bill_date_sort  = 1;

    }else{
        bill_date_sort = 0;
    }
    filters_list['bill_date_sort'] = bill_date_sort;
    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function() {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function() {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.ps-scrollbar-x-rail').css('top','0px');
            window.scrollBy(0, 400);

        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}

function search_clear() {
    var current_date = $('#current_date').val();

    // $(".datapicker").select2('val', '');
    $('#payment_date_from').val(current_date);
    $('#payment_date_to').val(current_date);
    $('.hidden_search').val('');
    $('.hidden').val('');
    $('#op_no_hidden').val('');
    $('#bill_no_hidden').val('');
    $('input:checkbox').removeAttr('checked');
    cashierCollectionDetailsReport();
}
function destroyThread(){
    $('.theadscroll').perfectScrollbar('destroy');
    $('#input_table').css('width','100%');
    $('#summary_data').css('height','');
    $('#summary_data_retrun').css('height','');
    $('#mode_summary').css('height','');
    $('#mode_summary_return').css('height','');
    print_generate('result_container_div')
}

function closeModal(){
    //$('#input_table').css('width','130%');
    $('#summary_data').css('height','500px');
    $('#summary_data_retrun').css('height','500px');
    $('#mode_summary').css('height','500px');
    $('#mode_summary_return').css('height','500px');
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('#print_config_modal').modal('hide');

}
var scrolled = false;

$(window).scroll(function() {

    //if I scroll more than 1000px...
    if($(window).scrollTop() > 10 && scrolled == false){
       $('#filter_area').css('top','0px');
      scrolled = true;
    } else if($(window).scrollTop() == 0) {
      scrolled = false;
      $('#filter_area').css('top','');
    }
});