
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_lab_service_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});
$('#restrict_duration').click( function() {
    if($(".restrict_duration").prop('checked') == true){
        $('#duration').prop("disabled", false).addClass('enabled');
        $('#h').prop("disabled", false).addClass('enabled');
        $('#hourly').prop("disabled", false);
        $('#d').prop("disabled", false).addClass('enabled');
        $('#daily').prop("disabled", false);
        $('#w').prop("disabled", false).addClass('enabled');
        $('#warning').prop("disabled", false);
        $('#b').prop("disabled", false).addClass('enabled');
        $('#block').prop("disabled", false);
        $('#count').prop("disabled", false).addClass('enabled');
        $('#restrict_duration_count').prop("disabled", false);
    }else{
        $('#duration').prop("disabled", true).addClass('disabled');
        $('#h').attr("disabled", true).addClass('disabled');
        $('#hourly').prop("disabled", true);
        $('#d').prop("disabled", true).addClass('disabled');
        $('#daily').prop("disabled", true);
        $('#w').prop("disabled", true).addClass('disabled');
        $('#warning').prop("disabled", true);
        $('#b').prop("disabled", true).addClass('disabled');
        $('#block').prop("disabled", true);
        $('#count').prop("disabled", true).addClass('disabled');
        $('#restrict_duration_count').prop("disabled", true);
    }
   }); 
   $("#department_add").on('change', function() {
    var department = $('#department_add').val();
    if (department == '') {
      var department = 'a';

    }

 if(department) {
    var base_url=$('#base_url').val();
    var url = base_url + "/master/search_lab_service";
   // var url= route_json.save_sub_department;

        $.ajax({
                    type: "GET",
                    url: url,
                    data: 'department=' + department,
                    beforeSend: function () {
                        $('#s2id_subdepartment_add').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#subdepartment_add" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#subdepartment_add").empty();

                                        $("#subdepartment_add").html('<option value="">Select Sub Department</option>');
                                        $.each(html,function(key,value){
                                          $("#subdepartment_add").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#subdepartment_add").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                     // alert('Please check your internet connection and try again');
                     $( "#subdepartment_add" ).prop( "disabled", false );

                    },
                    complete: function () {
                        $('#warning1').remove();
                        $( "#subdepartment_add" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $("#department_add").focus();
                $("#subdepartment_add").empty();
            }

    });
    
    function getFilteredItems(url){
       
        var data = {};
        data.service_name = $("#service_name").val();
        data.room_type = $("#room_type").val();
      
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }
    $('#hourly').click(function() {
        $('#daily').not('#hourly').removeAttr('checked');
      }); 
       $('#daily').click(function() {
        $('#hourly').not('#daily').removeAttr('checked');
      });
      $('#warning').click(function() {
        $('#block').not('#warning').removeAttr('checked');
      }); 
       $('#block').click(function() {
        $('#warning').not('#block').removeAttr('checked');
      });
      $('#duration').prop("disabled", true).addClass('disabled');
      $('#h').prop("disabled", true).addClass('disabled');
      $('#hourly').prop("disabled", true);
      $('#d').prop("disabled", true).addClass('disabled');
      $('#daily').prop("disabled", true);
      $('#w').prop("disabled", true).addClass('disabled');
      $('#warning').prop("disabled", true);
      $('#b').prop("disabled", true).addClass('disabled');
      $('#block').prop("disabled", true);
      $('#count').prop("disabled", true).addClass('disabled');
      $('#restrict_duration_count').prop("disabled", true);
    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".service_name_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Service Name");
            return;
        }
        if($(".service_code_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Service Code");
            return;
        }
        if($("#report_type").val().trim() == ''){
            Command: toastr["error"]("Please enter Report Type");
            return;
        }
        if($("#samplename").val().trim() == ''){
            Command: toastr["error"]("Please enter Sample Name");
            return;
        }
        if($("#vacutainer").val().trim() == ''){
            Command: toastr["error"]("Please enter Vacutainer");
            return;
        }
        
        data.service_id = $(".reference_id").val();
        data.service_name = $(".service_name_add").val();
        data.service_code = $(".service_code_add").val();
        data.department = $("#department_add").val();
        data.subdepartment = $("#subdepartment_add").val();
        data.is_billable = $(".is_billable").val();
        data.is_refundable = $(".is_refundable").val();
        data.samplename = $("#samplename").val();
        data.is_price_editable = $(".is_price_editable").val();
        data.report_type = $("#report_type").val();
        data.vacutainer = $("#vacutainer").val();
        data.applicable_gender = $(".applicable_gender_add").val();
        data.restrict_duration = $(".restrict_duration").val();
        data.duration_mode= $('input[name="duration_mode"]:checked').val();
        data.duration_type= $('input[name="duration_type"]:checked').val();
        data.status = $(".status_add").val();
        data.restrict_duration_count = $(".restrict_duration_count").val();
        data.shortname = $(".shortname").val();
        data.printorder = $(".printorder").val();
        data.nabh_price= $(".nabh_price_add").val();
        data.ipprice = $(".ipprice").val();
        data.opprice = $(".opprice").val();
        data.effective_date = $(".effective_date").val();
        data.status_name = $(".status_name").val();
       
        if($(".status_add").prop('checked') == true){
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".status_name").prop('checked') == true){
            // alert("chcked");
            data.status_name =1;
        }else{
            data.status_name =0;
        }
       
        if($(".restrict_duration").prop('checked') == true){
            data.restrict_duration =1;

        }else{
            data.restrict_duration =0;
            $("#id").attr("disabled", false);

        }
        if($(".is_billable").prop('checked') == true){
            data.is_billable ='t';
        }else{
            data.is_billable ='f';
        }
        if($(".is_refundable").prop('checked') == true){
            data.is_refundable ='t';
        }else{
            data.is_refundable ='f';
        }
      
        if($(".is_price_editable").prop('checked') == true){
            data.is_price_editable ='t';
        }else{
            data.is_price_editable ='f';
        }
       
       
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);
            
            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Service Code Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    $('#duration').prop("disabled", true).addClass('disabled');
                    $('#h').prop("disabled", true).addClass('disabled');
                    $('#hourly').prop("disabled", true);
                    $('#d').prop("disabled", true).addClass('disabled');
                    $('#daily').prop("disabled", true);
                    $('#w').prop("disabled", true).addClass('disabled');
                    $('#warning').prop("disabled", true);
                    $('#b').prop("disabled", true).addClass('disabled');
                    $('#block').prop("disabled", true);
                    $('#count').prop("disabled", true).addClass('disabled');
                    $('#restrict_duration_count').prop("disabled", true);
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
    function editItem(element){
      
        let service_id = $(element).parents('tr').attr('data-id');
        let service_name = $(element).parents('tr').attr('data-service_desc');
        let service_code = $(element).parents('tr').attr('data-service_code');
        let department = $(element).parents('tr').attr('data-department_id');
        let subdepartment = $(element).parents('tr').attr('data-subdepartment_id');
        let is_billable = $(element).parents('tr').attr('data-billable');
        let is_refundable = $(element).parents('tr').attr('data-refundable');
        let is_price_editable = $(element).parents('tr').attr('data-price_editable');
        let applicable_gender = $(element).parents('tr').attr('data-gender');
        let restrict_duration = $(element).parents('tr').attr('data-duration');
        let duration_mode = $(element).parents('tr').attr('data-duration_mode');
        let duration_type = $(element).parents('tr').attr('data-duration_type');
        let status = $(element).parents('tr').attr('data-status');
        let restrict_duration_count = $(element).parents('tr').attr('data-restrict_duration_count');
        let effective_date = $(element).parents('tr').attr('data-effective_date');
        let nabh_price = $(element).parents('tr').attr('data-nabh_price');
        let samplename = $(element).parents('tr').attr('data-samplename');
        let report_type = $(element).parents('tr').attr('data-report_type');
        let vacutainer = $(element).parents('tr').attr('data-vacutainer');
        let shortname = $(element).parents('tr').attr('data-shortname');
        let printorder = $(element).parents('tr').attr('data-printorder');
        let status_name = $(element).parents('tr').attr('data-status_service_charge');
        let opprice = $(element).parents('tr').attr('data-opprice');
        let ipprice = $(element).parents('tr').attr('data-ipprice');

        $(".reference_id").val(service_id);
        $(".service_name_add").val(service_name);
        $(".service_code_add").val(service_code);
        $("#department_add").val(department);
        $("#department_add").select2().trigger('change'); 
        setTimeout(relativeSubdepartment(subdepartment),1000);
       
        $(".applicable_gender_add").val(applicable_gender);
        $(".opprice").val(opprice);
        $(".ipprice").val(ipprice);
        $(".restrict_duration").val(restrict_duration);
        $(".restrict_duration_count").val(restrict_duration_count);
        $("#report_type").val(report_type);
        $("#report_type").select2().trigger('change');
        $("#vacutainer").val(vacutainer);
        $("#vacutainer").select2().trigger('change');
        $(".nabh_price_add").val(nabh_price);
        $(".shortname").val(shortname);
        $(".printorder").val(printorder);
        $("#effective_date").val(moment(effective_date).format('YYYY-MM-DD'));
        $("#samplename").val(samplename);
        $("#samplename").select2().trigger('change'); 
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
        $("#add").html('Edit Lab Service');
    
        if(duration_mode==0){
             $(".duration_mode").prop("checked", false);
         }else if(duration_mode==1){
            $("#hourly").prop("checked", true);
        
        } else if(duration_mode==2){
           $("#daily").prop("checked", true);
         
        }
        if(duration_type==0){
         $(".duration_type").prop("checked", false);
        }else if(duration_type==1){
           $("#warning").prop("checked", true);
        
        } else if(duration_type==2){
          $("#block").prop("checked", true);
        
        }
        if(status_name==1){
            $(".status_name").prop("checked", true);

        } else {
             $(".status_name").prop("checked", false);

        }
        if(status==1){
            $(".status_add").prop("checked", true);

        } else {
             $(".status_add").prop("checked", false);

        }
        if(is_billable==1){
            $(".is_billable").prop("checked", true);

         } else {
             $(".is_billable").prop("checked", false);
         
         }
       
         if(is_refundable==1){
            $(".is_refundable").prop("checked", true);

        } else {
             $(".is_refundable").prop("checked", false);

        }
       
         if(is_price_editable==1){
             $(".is_price_editable").prop("checked", true);
         
         } else {
            $(".is_price_editable").prop("checked", false);
         
         }
        
        if(restrict_duration==1){
            $(".restrict_duration").prop("checked", true);
            $('#duration').prop("disabled", false).addClass('enabled');
            $('#h').prop("disabled", false).addClass('enabled');
            $('#hourly').prop("disabled", false);
            $('#d').prop("disabled", false).addClass('enabled');
            $('#daily').prop("disabled", false);
            $('#w').prop("disabled", false).addClass('enabled');
            $('#warning').prop("disabled", false);
            $('#b').prop("disabled", false).addClass('enabled');
            $('#block').prop("disabled", false);
            $('#count').prop("disabled", false).addClass('enabled');
            $('#restrict_duration_count').prop("disabled", false);
        } else {
           $(".restrict_duration").prop("checked", false);
           $('#duration').prop("disabled", true).addClass('disabled');
           $('#h').prop("disabled", true).addClass('disabled');
           $('#hourly').prop("disabled", true);
           $('#d').prop("disabled", true).addClass('disabled');
           $('#daily').prop("disabled", true);
           $('#w').prop("disabled", true).addClass('disabled');
           $('#warning').prop("disabled", true);
           $('#b').prop("disabled", true).addClass('disabled');
           $('#block').prop("disabled", true);
           $('#count').prop("disabled", true).addClass('disabled');
           $('#restrict_duration_count').prop("disabled", true);
        }
        setTimeout(getprices(service_id),3000);

    }
    function duration(){
        if ($("#restrict_duration").is(":checked")) {
            $('#duration').prop("disabled", false).addClass('enabled');
            $('#h').prop("disabled", false).addClass('enabled');
            $('#hourly').prop("disabled", false);
            $('#d').prop("disabled", false).addClass('enabled');
            $('#daily').prop("disabled", false);
            $('#w').prop("disabled", false).addClass('enabled');
            $('#warning').prop("disabled", false);
            $('#b').prop("disabled", false).addClass('enabled');
            $('#block').prop("disabled", false);
            $('#count').prop("disabled", false).addClass('enabled');
            $('#restrict_duration_count').prop("disabled", false);
    }else{
        $('#duration').prop("disabled", true).removeClass('enabled');
        $('#h').prop("disabled", true).removeClass('enabled');
        $('#hourly').prop("disabled", true);
        $('#d').prop("disabled", true).removeClass('enabled');
        $('#daily').prop("disabled", true);
        $('#w').prop("disabled", true).removeClass('enabled');
        $('#warning').prop("disabled", true);
        $('#b').prop("disabled", true).removeClass('enabled');
        $('#block').prop("disabled", true);
        $('#count').prop("disabled", true).removeClass('enabled');
        $('#restrict_duration_count').prop("disabled", true);
    }
}
    function resetForm(){
        $(".reference_id").val('');
        $(".service_name_add").val('');
        $(".service_code_add").val('');
        $("#department_add").val('').trigger('change');
        $("#subdepartment_add").val('').trigger('change');
        $("#samplename").val('').trigger('change');
        $("#report_type").val('').trigger('change');
        $("#vacutainer").val('').trigger('change');
        $('input:checkbox').removeAttr('checked');
        $(".applicable_gender_add").val('');
        $(".shortname").val('');
        $(".printorder").val('');
        $(".opprice").val('');
        $(".ipprice").val('');
        $(".restrict_duration_count").val('');
        $('#duration').prop("disabled", true).removeClass('enabled');
        $('#h').prop("disabled", true).removeClass('enabled');
        $('#hourly').prop("disabled", true);
        $('#d').prop("disabled", true).removeClass('enabled');
        $('#daily').prop("disabled", true);
        $('#w').prop("disabled", true).removeClass('enabled');
        $('#warning').prop("disabled", true);
        $('#b').prop("disabled", true).removeClass('enabled');
        $('#block').prop("disabled", true);
        $('#count').prop("disabled", true).removeClass('enabled');
        $('#restrict_duration_count').prop("disabled", true);
        $(".effective_date").val('');
       $(".nabh_price_add").val('');
        $("#add").html('Add Lab Service');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

    function getprices(service_id){
        var url=$('#base_url').val();
        $(".reference_id").val(service_id);
        var param = {service_id:service_id};
            
         $.ajax({
            type: "GET",
            url: url + "/master/getprice",
            data: param,
            beforeSend: function () {
              
            
            },
            success: function (data) {  
               
                $('.opprice').val(data.oppricing);
                $('.ipprice').val(data.ippricing);
            },
            complete: function () {

            }
        });
    }
$('#department').on('change',function(){
 relativeSubdepartment();
})
function relativeSubdepartment(id=0){
    var department = $('#department_add').val();
    if (department == '') {
      var department = 'a';

    }

 if(department) {
    var base_url=$('#base_url').val();
    var url = base_url + "/master/search_lab_service";
   // var url= route_json.save_sub_department;

        $.ajax({
                    type: "GET",
                    url: url,
                    data: 'department=' + department,
                    beforeSend: function () {
                        $('#s2id_subdepartment_add').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                        $( "#subdepartment_add" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                       $("#subdepartment_add").empty();

                                        $("#subdepartment_add").html('<option value="">Select Sub Department</option>');
                                        $.each(html,function(key,value){
                                          $("#subdepartment_add").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#subdepartment_add").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                     // alert('Please check your internet connection and try again');
                     $( "#subdepartment_add" ).prop( "disabled", false );

                    },
                    complete: function () {
                        if(id!=0){
                            $("#subdepartment_add").val(id).select2();
                        }
                        $('#warning1').remove();
                        $( "#subdepartment_add" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $("#department_add").focus();
                $("#subdepartment_add").empty();
            }

    
}
