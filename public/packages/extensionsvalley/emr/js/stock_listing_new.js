$(document).ready(function () {
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('#pur_cost_div').hide();

});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = $(this).attr('id');
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {
                    $("#" + input_id + "AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();

                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
$("#category").on('change', function () {
    var category = $('#category').val();
    if (category == '') { var category = 'a'; }
    if (category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#sub_category').html(
                    '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                ).show();
            },
            success: function (html) {
                if (html) {
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub-Catogery</option>');
                    $.each(html, function (key, value) {
                        $("#sub_category").append('<option value="' + key + '">' + value + '</option>');
                    });
                } else {
                    $("#sub_category").empty();
                }
            },
            error: function () {
                Command: toastr["error"]("Please check your internet connection and try again!");
            },
            complete: function () {
                $('#sub_category').LoadingOverlay("hide");
                $('#sub_category').select2();
            }
        });
    } else {
        $('#category').focus();
        $("#sub_category").empty();
    }
});



$(document).on('click', '.batch_wise', function (e) {
    if ($(".batch_wise").prop('checked') == true) {
        $('#pur_cost_div').show();
        $('#reorder_level_div').hide();
    } else {
        $('#pur_cost_div').hide();
        $('#reorder_level_div').show();
    }
});
$(document).on('click', '.stock_zero', function (e) {
    if ($(".stock_zero").prop('checked') == true) {
        $(".stk_grtr_zero").prop('checked', false);
    }
});
$(document).on('click', '.stk_grtr_zero', function (e) {
    if ($(".stk_grtr_zero").prop('checked') == true) {
        $(".stock_zero").prop('checked', false);
    }
});

function getStockListReportData() {
    var url = route_json.StockListingData;
    var location = $('#location').val();
    var rack = $('#rack').val();
    var category = $('#category').val();
    var sub_category = $('#sub_category').val();
    var item_name = $('#item_name_hidden').val();
    var item_type = $('#item_type').val();
    var doctor = $('#doctor_hidden').val();
    var manufaturer = $('#manufacturer_name_hidden').val();
    var vendor_id = $('#vendor_name_hidden').val();
    var sort_by = $('#sort_by').val();
    var sort_by_descending = $('#sort_by_descending').val();
    var ved = $('#ved').val();
    var narcotic_item = 0; var shedule_h1 = 0; var batch_wise = 0;
    var pur_cost = 0; var stock_zero = 0; var stk_grtr_zero = 0;
    var expired_stock = 0;
    var without_expiry = 0;
    var expiry_item = 0;
    var reorder_level = 0;

    if ($('#narcotic_item').is(":checked")) { narcotic_item = 1; }
    if ($('#shedule_h1').is(":checked")) { shedule_h1 = 1; }
    if ($('#batch_wise').is(":checked")) { batch_wise = 1; }
    if ($('#pur_cost').is(":checked")) { pur_cost = 1; }
    if ($('#stock_zero').is(":checked")) { stock_zero = 1; }
    if ($('#stk_grtr_zero').is(":checked")) { stk_grtr_zero = 1; }
    if ($('#expired_stock').is(":checked")) { expired_stock = 1; }
    if ($('#without_expiry').is(":checked")) { without_expiry = 1; }
    if ($('#expiry_item').is(":checked")) { expiry_item = 1; }
    if ($('#reorder_level').is(":checked")) { reorder_level = 1; }

    var parm = {
        location: location, rack: rack, category: category,
        sub_category: sub_category, item_name: item_name,
        item_type: item_type, doctor: doctor, manufaturer: manufaturer,
        sort_by: sort_by, sort_by_descending: sort_by_descending,
        ved: ved, vendor_id: vendor_id,
        narcotic_item: narcotic_item, shedule_h1: shedule_h1,
        batch_wise: batch_wise, pur_cost: pur_cost, stock_zero: stock_zero,
        stk_grtr_zero: stk_grtr_zero, expired_stock: expired_stock, without_expiry: without_expiry, expiry_item: expiry_item, reorder_level: reorder_level
    };
    if (location == null || location == "") {
        toastr.warning("Please Select Any Location");
    } else {

        $.ajax({
            type: "POST",
            url: url,
            data: parm,
            beforeSend: function () {
                $('#ResultDataContainer').css('display', 'block');
                $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                $(".multiselect").css({
                    "border": '',
                    "background": '',
                });
            },
            success: function (html) {
                $('#ResultsViewArea').html(html);
                $('#print_results').removeClass('disabled');
                $('#csv_results').removeClass('disabled');
            },
            complete: function () {
                $('#ResultsViewArea').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                $('#ResultDataContainer').css('display', 'hide');
                return;
            }
        });
        return false;
    }
}

function datarst2() {
    $('.ajaxSearchBox').hide();
    $('.select2').val('All').select2();
    $('#item_name').val('');
    $('#vendor_name').val('');
    $('#item_name_hidden').val('');
    $('#vendor_name_hidden').val('');
    $('.check ').prop('checked',false);
}

function GetUrlParameter(sPageURL, sParam) {
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
