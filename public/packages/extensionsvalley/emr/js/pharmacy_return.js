$(document).ready(function() {

    if ($('#bill_no_hidden').val() != 0) {
        var bill_no = $('#bill_no_hidden').val();
        $('#bill_no').val(bill_no)

    }
    var ph = $('#ph').val();
    if (ph == 0) {
        $('#bill_type_select').show();
        $('#bill_tag').val('PH');
    } else if (ph == 'PH') {
        $('#pharmacy_bill').prop('checked', true)
        getDetailsFromBillNum();


    } else if (ph != 'PH') {
        $('#service_bill').prop('checked', true)
        $('#select_All').hide();
        getDetailsFromBillNum();

    }
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2();
    $('#bill_no').focus();

    $('#pharmacy_bill').change(function() {
        if ($('#pharmacy_bill').is(':checked')) {
            $('#bill_tag').val('PH');
            $('#bill_tag').attr('disabled', true);
            $('#select_All').show();
            $('#bill_tag option[value="PH"]').prop('disabled', false);
            reset();


        }
    });
    $('#service_bill').change(function() {
        if ($('#service_bill').is(':checked')) {
            $('#bill_tag').attr('disabled', false);
            $('#bill_tag').val('');
            $('#select_All').hide();
            $('#bill_tag option[value="PH"]').prop('disabled', true);
            reset();


        }
    });

});
$(document).on("click", function(event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
$(document).on('click', '.advanceSearchBtn', function(event) {
    advancePatientSearch(1);
});
//----Hidden Filed Search--------------------------------------------------------------------
var token = $("#c_token").val();
var base_url = $("#base_url").val();
$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = base_url + "/billreturn/setAjaxSearch";
            if ($('#service_bill').is(':checked')) {
                type = 2;
            } else {
                type = 1;
            }
            var bill_no = $('#bill_no').val();
            $.ajax({
                type: "GET",
                url: url,
                data: 'type=' + type + 'bill_no=' + bill_no + '&search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
$('#bill_noAjaxDiv').on('click', 'li', function() {
    getDetailsFromBillNum();
});

function getDetailsFromBillNum(type = 0) {
    if (type == 0) {
        if ($('#pharmacy_bill').is(':checked')) {
            type = 1;
        } else if ($('#service_bill').is(':checked')) {
            type = 2;
        }
    }
    var url = base_url + "/billreturn/getDetailsFromBillNum";
    var bill_no = $('#bill_no').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { bill_no: bill_no, type: type },
        beforeSend: function() {
            $('.return').attr('disabled', true);
            $('#filter_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function(data) {
            if (data) {
                $('#payment_type').val(data[0].payment_type);
                $('#location').val(data[0].location);
                $('#patient_name').val(data[0].patient_name);
                $('#patient_hidden').val(data[0].patient_id);
                $('#patient_uhid').val(data[0].uhid);
                $('#bill_tag').val(data[0].bill_tg_id);
            }

        },
        complete: function() {
            $('#filter_area').LoadingOverlay("hide");
            getBillList();



        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });


}

function getBillList() {
    if ($('#pharmacy_bill').is(':checked')) {
        type = 1;
    } else if ($('#service_bill').is(':checked')) {
        type = 2;
    }
    var uhid = $('#patient_uhid').val().trim();
    var bill_tag = $('#bill_tag').val();
    var bill_no = $('#bill_no').val().trim();
    $(".ajaxSearchBox").hide()
    if (!uhid) {
        toastr.warning('Please select bill number or uhid.');
        return;
    }
    var patient_hidden = $('#patient_hidden').val();
    var payment_type = $('#payment_type').val();
    var location = $('#location').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var url = base_url + "/billreturn/getBillList";
    $.ajax({
        type: "POST",
        url: url,
        data: { type: type, bill_no: bill_no, uhid: uhid, bill_tag: bill_tag, patient_hidden, payment_type: payment_type, location: location, from_date: from_date, to_date: to_date },
        beforeSend: function() {
            $('#bill_history').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#search_bill').attr('disabled', true);
            $("#bill_spin").removeClass("fa fa-search");
            $("#bill_spin").addClass("fa fa-spinner fa-spin");
            $('#bill_history_data').html(' ');
            $('#bill_detail_data').html(' ');
            $('#bill_return_data  tbody').empty();


        },
        success: function(data) {
            if (data) {
                $('#bill_history_data').html(data);
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        complete: function() {
            if ($('#bill_no_hidden').val() != 0) {
                getAllBillDetails();
            } else if ($('#bill_tag').val() == 'PB') {
                getAllBillDetails();

            }
            $('#bill_history').LoadingOverlay("hide")
            $('#search_bill').attr('disabled', false);
            $("#bill_spin").removeClass("fa fa-spinner fa-spin");
            $("#bill_spin").addClass("fa fa-search");

        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });

}

function getBillDetails(bill_no) {
    if ($('#pharmacy_bill').is(':checked')) {
        type = 1;
    } else if ($('#service_bill').is(':checked')) {
        type = 2;
    }
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var bill_tag = $('#bill_tag').val();
    var uhid = $('#patient_uhid').val().trim();
    $('#search_based_on').val(bill_no);
    var url = base_url + "/billreturn/getBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: { bill_no: bill_no, type: type, from_date: from_date, to_date: to_date, uhid: uhid,bill_tag:bill_tag },
        beforeSend: function() {
            $('#bill_detail_data').html(' ');
            $('#bill_return_data  tbody').empty();

            resetReturnData();
            $('#bill_detail').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {
            if (data) {
                $('#bill_detail_data').html(data);
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });

        },
        complete: function() {
            $('#bill_detail').LoadingOverlay("hide")
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });

}

function getAllBillDetails() {
    if ($('#pharmacy_bill').is(':checked')) {
        type = 1;
    } else if ($('#service_bill').is(':checked')) {
        type = 2;
    }
    var url = base_url + "/billreturn/getBillDetails";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var uhid = $('#patient_uhid').val().trim();
    var bill_no = $('#bill_no').val();
    if (!uhid) {
        toater.warning('Please select Uhid.');
        return;
    }
    $('#search_based_on').val(uhid);
    $.ajax({
        type: "POST",
        url: url,
        data: { uhid: uhid, bill_no: bill_no, from_date: from_date, to_date: to_date, type: type },
        beforeSend: function() {
            $('#bill_detail_data').html(' ');
            $('#bill_return_data  tbody').empty();
            resetReturnData();
            $('#bill_detail').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {
            if (data) {
                $('#bill_detail_data').html(data);
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });

        },
        complete: function() {
            $('#bill_detail').LoadingOverlay("hide")
            if ($('#bill_tag').val() == 'PB') {
                $('#allreturn').trigger('click');
                $('#bill_return_data').css('pointer-events', 'none')
                $('#bill_detail').css('pointer-events', 'none');

            }
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });

}

function addToReturn(net_amt, tax_amount, selling_price_with_tax, tax_percent, other_tax_per, other_tax_amnt, bill_detail_id, billheadid, item_id, unit_tax_amount, exp_date, btch, bill_no, item_desc, actual_selling_price, qty, net_total, bill_discount_split, detail_qty) {

    resetReturnData();
    $('#net_amt').val(net_amt); //single service
    $('#bill_no').blur();
    $('#return_qty').focus();
    $('#billheadid').val(billheadid);
    $('#item_id_hidden').val(item_id);
    $('#unit_tax_amount').val(unit_tax_amount);
    $('#exp_date').val(exp_date);
    $('#bill_discount_split').val(bill_discount_split);
    $('#batch_no_hidden').val(btch);
    $('#bill_no_hidden').val(bill_no);
    $('#return_itm').val(item_desc);
    if ($('#bill_tag').val() == 'CONS') {
        $('#org_price').val(net_amt);
    } else {
        $('#org_price').val(actual_selling_price);
    }
    $('#net_amount').val(net_total);
    $('#selling_price_with_tax').val(selling_price_with_tax);
    $('#tax_percent').val(tax_percent);
    $('#other_tax_per').val(other_tax_per);
    $('#other_tax_amnt').val(other_tax_amnt);
    $('#bill_detail_id').val(bill_detail_id);
    $('#tax_amount').val(tax_amount);
    $('#detail_qty').val(detail_qty);
    var item = $('#item_id_hidden').val();
    var billheadid = $('#billheadid').val();
    var rem_qty = $('#rem_qty' + item + '_' + billheadid).html();
    $('#org_qty').val(qty);

}

function resetReturnData() {
    $('#net_amt').val(' ');
    $('#return_itm').val(' ');
    $('#tax_amount').val(' ');
    $('#org_price').val(' ');
    $('#org_qty').val(' ');
    $('#net_amount').val(' ');
    $('#return_qty').val(' ');
    $('#return_total').val(' ');
    $('#item_id_hidden').val(' ');
    $('#unit_tax_amount').val(' ');
    $('#exp_date').val(' ');
    $('#batch_no_hidden').val(' ');
    $('#bill_no_hidden').val(' ');
    $('#selling_price_with_tax').val(' ');
    $('#tax_percent').val(' ');
    $('#other_tax_per').val(' ');
    $('#other_tax_amnt').val(' ');
    $('#bill_detail_id').val(' ');
    $('#return_reason1').val('');
    $('#return_reason2').val(' ');
    $('#error_status').hide();
    $('#detail_qty').val('');
}

function addSingleTdData() {
    if ($('#org_qty').val() == 0) {
        toastr.warning('Nothing to Return.');
        return;
    }
    var return_qty = $('#return_qty').val().trim();
    if (return_qty == '' || return_qty == 0) {
        toastr.warning('Please fill return quantity.');
        return;
    }
    if ($('#pharmacy_bill').is(':checked')) {
        var exp_date = $('#exp_date').val();
        var btch = $('#batch_no_hidden').val();
    } else if ($('#service_bill').is(':checked')) {
        var exp_date = ' ';
        var btch = ' ';
    }
    var bill_no = $('#bill_no_hidden').val();
    var return_itm = $('#return_itm').val();
    var return_total = $('#return_total').val();
    var selling_price_with_tax = $('#selling_price_with_tax').val();
    var tax_percent = $('#tax_percent').val();
    var other_tax_per = $('#other_tax_per').val();
    var other_tax_amnt = $('#other_tax_amnt').val();
    var bill_detail_id = $('#bill_detail_id').val();
    var tax_amount = $('#tax_amount').val();
    var item = $('#item_id_hidden').val();
    var billheadid = $('#billheadid').val();
    var rem_qty = $('#rem_qty' + item + '_' + billheadid).html();
    var rem = parseFloat(rem_qty) - parseFloat(return_qty);
    $('#rem_qty' + item + '_' + billheadid).html(rem);
    var return_id = +item + '_' + billheadid;
    var tax = $('#unit_tax_amount').val();
    var detail_totaltax = parseFloat(tax * return_qty).toFixed(2);
    var org_qty = $('#org_qty').val();
    var return_qty = $('#return_qty').val();
    var selling_price_with_tax = $('#selling_price_with_tax').val();
    var bill_discount_split = $('#bill_discount_split').val();
    var split_amt_per_item=bill_discount_split/org_qty;
    var split_over_bill=parseFloat(split_amt_per_item * return_qty);
    if ($('*').hasClass("returnqty" + return_id + "") && $('*').hasClass("detail_amt" + return_id + "")) {
        var set_qty = $('.returnqty' + return_id + '').html();
        var set_amt = $('.detail_amt' + return_id + '').html();
        var split_bill = $('.bill_split' + return_id + '').html();
        console.log(split_over_bill);

        console.log(split_bill);

        var split = parseFloat(split_bill) + parseFloat(split_over_bill);
        console.log(split);

        var set_price = parseFloat(set_amt) / parseFloat(set_qty);
        var set_ttl_qty = parseFloat(set_qty) + parseFloat(return_qty);
        var set_ttl_tax = parseFloat(tax * set_ttl_qty).toFixed(2);
        var rtn_qty_amt = set_ttl_qty * set_price;
        $('.returnqty' + return_id + '').html(set_ttl_qty);
        $('.detail_amt' + return_id + '').html(rtn_qty_amt);
        $('.detail_totaltax' + return_id + '').html(set_ttl_tax);
        $('.bill_split' + return_id + '').html(split);

    } else {
        $('#bill_return_data').append('<tr style="cursor: pointer;background-color:aqua"><td class="bill_split_ds bill_split' +return_id+'"  style="display:none">'+split_over_bill+'</td><td  class="tax_amount" width="15%" style="display:none">' + tax_amount + '</td><td  class="selling_price_with_tax" width="15%" style="display:none">' + selling_price_with_tax + '</td><td  class="tax_percent" width="15%" style="display:none">' + tax_percent + '</td><td  class="other_tax_per" width="15%" style="display:none">' + other_tax_per + '</td><td  class="other_tax_amnt" width="15%" style="display:none">' + other_tax_amnt + '</td><td  class="bill_detail_id" width="15%" style="display:none">' + bill_detail_id + '</td><td class="detail_totaltax detail_totaltax' + return_id + '" width="15%" style="display:none">' + detail_totaltax + '</td><td  class="detail_itemid" width="15%" style="display:none";>' + item + '</td><td  class="detail_headid" width="15%" style="display:none";>' + billheadid + '</td><td  class="common_td_rules detail_bill" width="15%">' + bill_no + '</td><td width="15%" class="detail_item common_td_rules" >' + return_itm + '</td><td class="common_td_rules detail_exp" width="10%">' + exp_date + '</td><td width="15%"  class="common_td_rules detail_batch">' + btch + '</td><td width="10%"  class="common_td_rules detail_return returnqty' + return_id + '">' + return_qty + '</td><td width="10%" class="td_common_numeric_rules detail_amt detail_amt' + return_id + '">' + return_total + '</td><td width="5%" class="td_common_numeric_rules delete_row"  data-id="' + return_id + '" data-rtn_qty="' + return_qty + '" data-rtn_amt ="' + return_total + '"><i class="fa fa-trash reset_return"></i></td></tr>');
    }

    // var replace_amount = $('#replace_amt' + item + '_' + billheadid).html();

    // var item_total = parseFloat(replace_amount) - parseFloat(return_total);

    // var replace_amount = $('#replace_amt' + item + '_' + billheadid).html(item_total);
    resetReturnData();

}

function calculateTaxAmount() {
    var org_price = $('#org_price').val();
    var org_qty = $('#org_qty').val();
    var return_qty = $('#return_qty').val();
    var selling_price_with_tax = $('#selling_price_with_tax').val();
    var bill_discount_split = $('#bill_discount_split').val();
    var detail_qty = $('#detail_qty').val();
    var net_amt = $('#net_amt').val();

    if (return_qty) {
        if (parseFloat(return_qty) > parseFloat(org_qty)) {
            toastr.warning('Return quantity must be less than Remaining Quantity.');
            $('#return_qty').val(' ');
            $('#return_total').val(' ');
            return;
        }
        // var split_amt_per_item=bill_discount_split/org_qty;
        // var net_taxadded_Amt = parseFloat((selling_price_with_tax * return_qty) - (split_amt_per_item * return_qty)).toFixed(4);
        var item_total = parseFloat(net_amt - bill_discount_split);
        var single_item_amt = parseFloat(item_total / detail_qty);
        var net_taxadded_Amt = parseFloat(single_item_amt * return_qty).toFixed(4);
        if (net_taxadded_Amt != 'NaN') {
            $('#return_total').val(net_taxadded_Amt);

        }

    }

}
$('#return_qty').keypress(function(e) {
    if (e.which == 13) {
        addSingleTdData()
        return false;
    }
});

$(document).on('click', '.delete_row', function() {
    var reduce_id = $(this).attr('data-id');
    var reduce_qty = $('.returnqty' + reduce_id + '').html();

    var item_amount = $(this).attr('data-rtn_amt');
    var rtn_qty = $('#rem_qty' + reduce_id).html();

    var item_return_amt = $('#replace_amt' + reduce_id).html();
    var add_amt = parseFloat(item_amount) + parseFloat(item_return_amt);
    //$('#replace_amt' + reduce_id).html(add_amt);
    var net_qty = (parseFloat(reduce_qty) + parseFloat(rtn_qty));
    $('#rem_qty' + reduce_id).html(net_qty);
    $(this).closest("tr").remove();
    resetReturnData();
});
$("#bill_return_data").bind("DOMSubtreeModified", function() {
    calculateTotalReturnQty();
    if ($('#bill_return_data').find('tr').length > 0) {
        $('#save_return').attr('disabled', false)
        $('#print_return').attr('disabled', false)
    }


});

function calculateTotalReturnQty() {
    var totat_return = 0;
    var total_amt = 0;
    var total_tax = 0;

    $('.detail_return').each(function() {
        totat_return += parseFloat($(this).html());
    })
    $('.detail_amt').each(function() {
        total_amt += parseFloat($(this).html());
    })
    $('.detail_totaltax').each(function() {
        total_tax += parseFloat($(this).html());
    })
    $('#show_return_qty').val(parseFloat(totat_return).toFixed(2));
    $('#return_amt').val(parseFloat(total_amt).toFixed(2));
    $('#return_tax').val(parseFloat(total_tax).toFixed(2));
}

function wantTextArea() {
    if ($('#text_checkbox').is(':checked')) {
        $('#wanttext').show();
        $('#wantSelect').hide();
    } else {
        $('#wanttext').hide();
        $('#wantSelect').show();
    }
}


$(document).ready(function() {
    $("#issue_search_box1").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#detail_search tr").filter(function() {
            $(this).toggle($(this).text()
                .toLowerCase().indexOf(value) > -1)
        });
    });
});
$("#allreturn").click(function() {
    $('.reset_return').click();
    //$('.delete_row').click();
    var total_rem = 0;

    $('.list_rem_qty').each(function() {
        total_rem += parseFloat($(this).html());
    })
    if (total_rem == 0) {
        toastr.warning('Nothing to return.');
        return;
    }
    $("#bill_detail_container").find("tbody>tr.returnable_item").each(function() {
        var bill_no = $(this).find('td:eq(0)').text();
        var item_desc = $(this).find('td:eq(1)').text();
        var rem = $(this).find('td:eq(4)').text();
        $(this).find('td:eq(4)').text(0);
        var net_amt = $(this).find('td:eq(7)').text();
        if ($('#pharmacy_bill').is(':checked')) {
            var rtn_amext_date = $(this).find('td:eq(8)').text();
            var batc = $(this).find('td:eq(2)').text();
        } else if ($('#service_bill').is(':checked')) {
            var rtn_amext_date = ' ';
            var batc = ' ';
        }
        // $(this).find('td:eq(7)').text(0);

        var billheadid = $(this).find('td:eq(9)').text();
        var item_id = $(this).find('td:eq(10)').text();
        var unit_tax_amount = $(this).find('td:eq(11)').text();
        var selling_price_with_tax = $(this).find('td:eq(12)').text();
        var tax_percent = $(this).find('td:eq(13)').text();
        var other_tax_per = $(this).find('td:eq(14)').text();
        var other_tax_amnt = $(this).find('td:eq(15)').text();
        var bill_detail_id = $(this).find('td:eq(16)').text();
        var tax_amount = $(this).find('td:eq(17)').text();
        var bill_discount_split = $(this).find('td:eq(18)').text();
        var net_amount = $(this).find('td:eq(19)').text();
        var detail_qty = $(this).find('td:eq(20)').text();
        var total_tax = parseFloat(unit_tax_amount * rem).toFixed(2);
        // var rtn_amt = parseFloat((selling_price_with_tax * rem) - (bill_discount_split)).toFixed(2);
        var item_total = parseFloat(net_amount - bill_discount_split);
        var single_item_amt = parseFloat(item_total / detail_qty);
        var rtn_amt = parseFloat(single_item_amt * rem).toFixed(4);
        console.log(rem+','+selling_price_with_tax+','+rtn_amt);
        var return_id = +item_id + '_' + billheadid;
        if (rem > 0) {
            $("#bill_return_data").append("<tr style='cursor: pointer;background-color:aqua'><td class='bill_split_ds bill_split" +return_id+"'  style='display:none'>"+bill_discount_split+"</td><td  class='tax_amount' width='15%' style='display:none'>" + tax_amount + "</td><td  class='selling_price_with_tax' width='15%' style='display:none'>" + selling_price_with_tax + "</td><td  class='tax_percent' width='15%' style='display:none'>" + tax_percent + "</td><td  class='other_tax_per' width='15%' style='display:none'>" + other_tax_per + "</td><td  class='other_tax_amnt' width='15%' style='display:none'>" + other_tax_amnt + "</td><td  class='bill_detail_id' width='15%' style='display:none'>" + bill_detail_id + "</td><td  class='detail_totaltax' width='15%' style='display:none'>" + total_tax + "</td><td  class='detail_itemid' width='15%' style='display:none'>" + item_id + "</td><td  class='detail_headid' width='15%' style='display:none'>" + billheadid + "</td><td class='common_td_rules detail_bill' width='15%'>" + bill_no + "</td><td class='common_td_rules detail_item' width='20%'>" + item_desc + "</td><td class='common_td_rules detail_exp' width='10%' >" + rtn_amext_date + "</td><td class='common_td_rules detail_batch' width='10%'>" + batc + "</td><td class='common_td_rules detail_return returnqty" + return_id + "' width='10%' id='rem_qty" + return_id + "'>" + rem + "</td><td class='td_common_numeric_rules detail_amt detail_amt" + billheadid + "' width='10%'>" + rtn_amt + "</td><td class='td_common_numeric_rules delete_row' data-id=" + item_id + '_' + billheadid + " data-rtn_qty=" + rem + " data-rtn_amt=" + net_amt + " width='05%'><i class='fa fa-trash reset_return'></i></td></tr>")
        }


    });
    $('#bill_full_return >.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    var $table = $('#bill_full_return > table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
    resetReturnData()

});

function saveReturnDetails() {
    if ($('#pharmacy_bill').is(':checked')) {
        type = 1;
    } else if ($('#service_bill').is(':checked')) {
        type = 2;
    }
    var return_reason1 = $('#return_reason1').val();
    var return_reason2 = $('#return_reason2').val().trim();
    if (return_reason1 != '') {
        var return_reason = return_reason1;
    } else if (return_reason2 != '') {
        var return_reason = return_reason2;
    } else {
        toastr.warning('Please select or add reason.');
        return;
    }
    var return_qty = $('#show_return_qty').val();
    if (return_qty <= 0.00) {
        toastr.warning('Nothing to return.');
        return;
    }


    var detail_array = [];
    var head_array = [];
    var bills = [];
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var uhid = $('#head_uhid').val();
    var return_tax_amount = $('#return_tax').val();
    var return_amount = $('#return_amt').val();
    var return_type = $('#head_return_type').val();
    var visit_id = $('#head_visit_id').val();
    var counter_id = $('#head_counter_id').val();
    var payment_type = $('#head_payment_type').val();
    head_array.push({
        'return_tax_amount': return_tax_amount,
        'return_amount': return_amount,
        'return_type': return_type,
        'uhid': uhid,
        'visit_id': visit_id,
        'counter_id': counter_id,
        'payment_type': payment_type,
        'return_reason': return_reason,
        'location': location_id
    })
    $("#bill_return_data tr").each(function() {
        var bill_split_ds = $(this).find(".bill_split_ds").html();
        var tax_amount = $(this).find(".tax_amount").html();
        var selling_price_with_tax = $(this).find(".selling_price_with_tax").html();
        var tax_percent = $(this).find(".tax_percent").html();
        var other_tax_per = $(this).find(".other_tax_per").html();
        var other_tax_amnt = $(this).find(".other_tax_amnt").html();
        var bill_detail_id = $(this).find(".bill_detail_id").html();
        var item_id = $(this).find(".detail_itemid").html();
        var bill_head_id = $(this).find(".detail_headid").html();
        var bill_no = $(this).find(".detail_bill").html();
        var item_desc = $(this).find(".detail_item").html();
        var batch = $(this).find(".detail_batch").html();
        var expiry = $(this).find(".detail_exp").html();
        var return_qty = $(this).find(".detail_return").html();
        var return_amount = $(this).find(".detail_amt").html();
        detail_array.push({
            'item_id': item_id,
            'selling_price_with_tax': selling_price_with_tax,
            'tax_percent': tax_percent,
            'tax_amount': tax_amount,
            'other_tax_per': other_tax_per,
            'other_tax_amnt': other_tax_amnt,
            'bill_detail_id': bill_detail_id,
            'bill_head_id': bill_head_id,
            'bill_no': bill_no,
            'item_desc': item_desc,
            'batch': batch,
            'expiry': expiry,
            'return_qty': return_qty,
            'return_amount': return_amount,
            'bill_split_ds': bill_split_ds,
        });
    });
    $('#main_list_data tr').each(function() {
        var billheadid = $(this).find(".billheadid").html();
        var total_amt_rtn = 0;
        $('.detail_amt' + billheadid).each(function() {
            total_amt_rtn += parseFloat($(this).html());
        })
        if (total_amt_rtn != 0) {
            var bill_no = $(this).find(".bill_no").html();
            var bill_date = $(this).find(".bill_date").html();
            var net_amt = $(this).find(".net_amount").html();
            var bill_tag = $(this).find(".bill_tag").html();

            var balance_amt = parseFloat(net_amt) - parseFloat(total_amt_rtn);
            bills.push({
                'bill_no': bill_no,
                'uhid': uhid,
                'bill_date': bill_date,
                'net_amt': net_amt,
                'visit_id': visit_id,
                'bill_tag': bill_tag,
                'balance_amt': balance_amt

            });
        }


    })

    var url = base_url + "/billreturn/saveReturnDetails";

    $.ajax({
        type: "POST",
        url: url,
        data: { detail_array: detail_array, head_array: head_array, bills: bills, type: type },
        beforeSend: function() {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#save_return').attr('disabled', true);
            $("#spin_return").removeClass("fa fa-save");
            $("#spin_return").addClass("fa fa-spinner fa-spin");
            $('#pay_button').fadeIn();

        },
        success: function(data) {
            if (data) {
                if (data.status == 101) {
                    toastr.warning('' + data.message + '');
                    $('#error_status').show();
                    $('#error_status').html('' + data.message + '');
                } else if (data.status == 102) {
                    toastr.success('Bill returned successfully.');
                    $('#print_id').val(data.bill_return_id);
                    $('#print_no').val(data.bill_return_no);
                    payReturn();
                } else if (data.status == 103) {
                    toastr.error('Bill return failed.');

                }
            }

        },
        complete: function() {
            $('#return_reason1').val('');
            $('#return_reason2').val(' ');
            $('body').LoadingOverlay("hide");
            $('#save_return').attr('disabled', false);
            $("#spin_return").removeClass("fa fa-spinner fa-spin");
            $("#spin_return").addClass("fa fa-save");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });


}

function reset() {
    $('#return_itm').val(' ');
    $('#tax_amount').val(' ');
    $('#org_price').val(' ');
    $('#org_qty').val(' ');
    $('#net_amount').val(' ');
    $('#return_qty').val(' ');
    $('#return_total').val(' ');
    $('#item_id_hidden').val(' ');
    $('#unit_tax_amount').val(' ');
    $('#exp_date').val(' ');
    $('#batch_no_hidden').val(' ');
    $('#bill_no_hidden').val(' ');
    $('#selling_price_with_tax').val(' ');
    $('#tax_percent').val(' ');
    $('#other_tax_per').val(' ');
    $('#other_tax_amnt').val(' ');
    $('#bill_detail_id').val(' ');
    $('#patient_uhid').val(' ');
    $('#patient_uhid_hidden').val(' ');
    $('#bill_no').val(' ');
    $('#patient_name').val(' ');
    $('#payment_type').val('');
    $('#patient_name_hidden').val(' ');
    $('#location').val('');
    $('#from_date').val(' ');
    $('#to_date').val(' ');
    $('#bill_tag').val('');
    $('#bill_detail_data').html(' ');
    $('#bill_return_data  tbody').empty();
    $('#bill_history_data  tbody').empty();
    $('#patient_name').attr('disabled', false);
    $('#from_date').attr('disabled', false);
    $('#to_date').attr('disabled', false);
    $('#patient_uhid').attr('disabled', false);
    $('#bill_detail').css('pointer-events', '');
    $('#bill_return_data').css('pointer-events', '')



}

function printData() {
    $('#print_config_modal').modal('toggle');
}

function PrintBill() {
    var return_id = $('#print_id').val();
    var return_no = $('#print_no').val();
    if (return_id != 0 && return_no != 0) {
        var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
        var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
        var bill_tag = $('#bill_tag').val();
        var is_duplicate = $("#duplicate").prop('checked') ? '(DUPLICATE)' : '';
        var include_hospital_header = $("#showTitle").prop('checked') ? 1 : 0;
        var printMode = $('input[name=printMode]:checked').val();
        var param = { printMode: printMode, include_hospital_header: include_hospital_header, is_duplicate: is_duplicate, return_id: return_id, return_no: return_no, location_id: location_id, location_name: location_name, bill_tag: bill_tag };
        var base_url = $('#base_url').val();
        var url = base_url + "/billreturn/print_bill_detail";
        $.ajax({
            type: "post",
            url: url,
            data: param,
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(data) {
                if (printMode == 1) {
                    var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data + '<script>setTimeout(function(){window.print();window.close()},1000)</script>');
                } else {
                    var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 landscape;} .patient_head{ font-size:10px !important; } </style>' + data + '<script>setTimeout(function(){window.print();window.close()},1000)</script>');
                }
                // window.location.reload();
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            },

        });


    } else {
        toastr.error("Error Please Check Your Internet Connection");
    }


}

function payReturn() {
    var return_no = $('#print_id').val();
    loadCashReturn(return_no);
}

function cashReturnComplete() {
    if ($("#enable_dotmatrix_printout_clinic").val() == "1") {
        window.location.reload();
    } else {
        printData();
    }

}
