$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

});


function RestOfBillsDetailCollectionData() {

    var url = route_json.getrest_of_bills_jb;

    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var depatment_list = $('#department_id').val();
    var parm = { from_date: from_date, to_date: to_date, depatment_list: depatment_list };


    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#search_results').attr('disabled',true);
            $('#serachbtnspin').removeClass('fa fa-search');
            $('#serachbtnspin').addClass('fa fa-spinner fa-spin');
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },

        complete: function () {
            $('#search_results').attr('disabled',false);
            $('#serachbtnspin').removeClass('fa fa-spinner fa-spin');
            $('#serachbtnspin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}

function datarst() {

    var current_date = $('#current_date').val();
    $("#department_id").select2('val', '');
    $('#date_from').val(current_date);

}
