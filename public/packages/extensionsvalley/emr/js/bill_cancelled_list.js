$(document).ready(function () {
    $('.date-picker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    billcancelledData();
});
var base_url=$('#base_url').val();
$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
            $("#" + input_id + "AjaxDiv").hide();
        } else {
            var base_url = $('#base_url').val()

            var url = base_url + "/master/ajaxbillcancelsearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();

                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
        $('body').click(function() {
            $("#" + input_id + "AjaxDiv").hide();
        });
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetial(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();

}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
});
function billcancelledData()
{
    var url = base_url + "/master/bill_cancelled_request_listdetail";
    var data = {};
    data.bill_date_from = $('#bill_date_from').val();
    data.bill_date_to = $('#bill_date_to').val();
    data.bill_no = $('#bill_no').val();
    data.patient_name = $('#patient_name').val();
    data.bill_tag = $('#bill_tag').val();
    
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        beforeSend: function () {
            $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});

        },
        success: function (html) {
            $(".table_body_contents").LoadingOverlay("hide");
            $(".table_body_contents").html(html);
            $('#theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
          
        },
        complete: function () {
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }
    });
}
function cancel_approved(obj,id){
    var url = base_url + "/master/updatecancelledstatus";
    var data = {};
  
    data.bill_head_id = id;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(".approved").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            $('.approved').attr('disabled', true);

        },
        success: function (data) {  
           
                $(".approved").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                $('.approved').attr('disabled', false);
                Command: toastr["success"]("Cancel Approved");
                billcancelledData();
                resetFilter();
                
        },
        complete: function () {

        }
    });
}
function rejected(obj,id){
    var url = base_url + "/master/updatereject";
    var data = {};
    var txt = '';
    txt = 'Reason for rejection!';
    data.bill_head_id = id;
    bootbox.prompt({
        title: txt,
        inputType: 'text',
        callback: function (result) {
            if (result || result == '') {
                data.reject_reason = result;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    beforeSend: function () {
                        // $(".rejected").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                        // $('.rejected').attr('disabled', true);
            
                    },
                    success: function (data) {  
                       
                            // $(".rejected").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                            // $('.rejected').attr('disabled', false);
                            Command: toastr["success"]("Rejected");
                            billcancelledData();
                            resetFilter();
                            
                    },
                    complete: function () {
            
                    }
                });
            }
        }
    });
}
function resetFilter() {
    var current_date = $('#current_date').val();
    $('#bill_date_from').val(current_date);
    $('#bill_date_to').val(current_date);
    $('#bill_no').val('');
    $('#patient_name').val('');
    $('#bill_tag').val('');
    billcancelledData();
}

