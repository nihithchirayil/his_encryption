$(window).load(function(){
    $("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });
    $("input[data-attr='date']").trigger("focus");
    $(".getReport").trigger("focus");
    $(".report_date").html("Report Date : " + moment().format('DD-MMM-YYYY HH:mm:ss'));
})

$(document).ready(function(){
    



    $(document).on('click', '.getReport', function(event) { 
        getTheatreAndSurgeonReport();
    });
    $(document).on('click', '.printReport', function(event) { 
        printReport();
    });
    

    function getTheatreAndSurgeonReport(){
        var from_date = $('.from_date').val();
        var to_date = $('.to_date').val();
        var doctor = $('.doctor').val();

        if(moment(from_date).isAfter(to_date)){
            toastr.error("Please check the Date Range !!");
            return;
        }

        let _token = $('#c_token').val();
        // var url = "/otbilling/fetchTheatreAndSurgeonReport";
        var url = $("#base_url").val()+"/otbilling/fetchTheatreAndSurgeonReport";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                from_date :from_date,
                to_date :to_date,
                doctor :doctor,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".otbill_theatreandsurgeon_body").empty();
                $(".getReport").find('i').removeClass('fa-search').addClass('fa-spinner').addClass('fa-spin');
                $(".otbill_theatreandsurgeon_body").append("<tr><td colspan='10' style='text-align: center;'><i class='fa fa-spinner fa-spin' style='font-size: 20px;'></i></td></tr>")
            },
            success: function (response) {
                console.log(response);
                $(".getReport").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-search');
                
                
                if(response.code === 100){
                    $(".otbill_theatreandsurgeon_body").empty();

                    var amount_GRAND_total = OT_GRAND_TOTAL = ANS_HSP_GRAND_TOTAL = SUR_SH_GRAND_TOTAL = 0;
                    
                    $.each(response.data, function(key, val){
                        $(".otbill_theatreandsurgeon_body").append("<tr><td colspan='10' style='color:#36A693;'><b>"+key+"</b></td></tr>");
                        
                        var amount_total = OT_TOTAL = ANAST_TOTAL = ANS_HSP_TOTAL = GF_TOTAL = MED_PACK_TOTAL = Overtime_TOTAL = SUR_SH_TOTAL = 0;
                        $.each(val, function(key1, val1){
                            // total = parseFloat(total) + parseFloat(val1.total_amount);
                            var OT,ANAST,ANS_HSP,GF,MED_PACK,Overtime, SUR_SH  ='';
                            
                            OT = val1.OT ? val1.OT : 0.00;
                            ANS_HSP = val1.ANS_HSP ? val1.ANS_HSP: 0.00;
                            SUR_SH = val1.SUR_SH ? val1.SUR_SH: 0.00;
                            
                            OT_TOTAL = parseFloat(OT_TOTAL)+ parseFloat(OT);
                            ANS_HSP_TOTAL = parseFloat(ANS_HSP_TOTAL)+ parseFloat(ANS_HSP);
                            SUR_SH_TOTAL = parseFloat(SUR_SH_TOTAL)+ parseFloat(SUR_SH);
                            amount_total = parseFloat(amount_total)+ parseFloat(val1.total_amount);

                            $(".otbill_theatreandsurgeon_body").append("<tr><td colspan='10'><b>"+moment(val1.bill_datetime).format('DD-MMM-YYYY HH:mm:ss')+"</b></td></tr><tr><td>"+val1.bill_no+"</td><td>"+val1.uhid+"</td><td colspan='8' >"+val1.doctor+"</td></tr><tr><td colspan='2' >"+val1.patient+"</td><td>"+val1.total_amount+"</td><td>"+OT+"</td><td>"+SUR_SH+"</td><td>"+ANS_HSP+"</td></tr>");
                        });
                        $(".otbill_theatreandsurgeon_body").append("<tr><td colspan='2' style='color:#b71100;'><b>Sub Total:</b></td><td style='color:#b71100;'><b>"+amount_total.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+OT_TOTAL.toFixed(2)+"</b><td style='color:#b71100;'><b>"+SUR_SH_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+ANS_HSP_TOTAL.toFixed(2)+"</b></td></tr>");

                        OT_GRAND_TOTAL = parseFloat(OT_TOTAL)+ parseFloat(OT_GRAND_TOTAL);
                        ANS_HSP_GRAND_TOTAL = parseFloat(ANS_HSP_TOTAL)+ parseFloat(ANS_HSP_GRAND_TOTAL);
                        SUR_SH_GRAND_TOTAL = parseFloat(SUR_SH_TOTAL)+ parseFloat(SUR_SH_GRAND_TOTAL);
                        amount_GRAND_total = parseFloat(amount_total)+ parseFloat(amount_GRAND_total);

                    });

                    $(".otbill_theatreandsurgeon_body").append("<tr><td colspan='2' style='color:#c59017;'><b>Grand Total:</b></td><td style='color:#c59017;'><b>"+amount_GRAND_total.toFixed(2)+"</b></td><td style='color:#c59017;'><b>"+OT_GRAND_TOTAL.toFixed(2)+"</b></td><td style='color:#c59017;'><b>"+SUR_SH_GRAND_TOTAL.toFixed(2)+"</b></td><td style='color:#c59017;'><b>"+ANS_HSP_GRAND_TOTAL.toFixed(2)+"</b></td></tr>");
                    
                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { }
        });
    }

    function printReport(){
        var printMode = $('input[name=printMode]:checked').val();
        var showw = "";
        var mywindow = window.open('', 'my div', 'height=3508,width=2480');
        var msglist = document.getElementById('printData');
        showw = showw + msglist.innerHTML;
        
        if(printMode == 1){
            mywindow.document.write('<style>td,th{padding:3px;}tabele,td,th{border:1px solid #000;font-size:12px;}table{width:100%;border-collapse: collapse;}@media  print table,td,tr{page-break-inside:auto;width:100%;}</style>');            
        }else{
            mywindow.document.write('<style>td,th{padding:3px;}tabele,td,th{border:1px solid #000;font-size:12px;}table{width:100%;border-collapse: collapse;}@media  print table,td,tr{page-break-inside:auto;width:100%;}</style>');
        }
    
        mywindow.document.write(showw);
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
    
        mywindow.print()
        mywindow.close();
        return true;
    }
});