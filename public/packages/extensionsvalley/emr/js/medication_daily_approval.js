$(document).ready(function () {

    $(document).on('click', '#select_all', function (event) {
        if ($('#select_all').is(":checked")) {
            $(".select_button li").addClass('active');
            $(".select_button li").find('span').text('Selected');
        } else {
            $(".select_button li").removeClass('active');
            $(".select_button li").find('span').text('Select');
        }
    });

    $('.datepicker').datetimepicker({
        format: 'MMMM-DD-YYYY'
    });

});
//----UHID search--------------------------------------------------------------------
$('#op_no_approval').keyup(function (event) {
    //alert('dddd'); return;
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#op_no_approval_hidden').val() != "") {
            $('#op_no_approval_hidden').val('');
        }
        var op_no = $(this).val();
        op_no = op_no.replace("/[^\w\s-_\.]/gi");
        op_no = op_no.trim();
        if (op_no == "") {
            $("#OpAjaxDiv_approval").html("");
        } else {
            var url = '';
            $.ajax({
                type: "GET",
                url: url,
                data: "op_no_approval=" + op_no,
                beforeSend: function () {

                    $("#OpAjaxDiv_approval").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {

                    $("#OpAjaxDiv_approval").html(html).show();
                    $("#OpAjaxDiv_approval").find('li').first().addClass('liHover');
                },
                complete: function () {
                    //  $('#loading_image').hide();
                }
            });
        }

    } else {
        ajax_list_key_down('OpAjaxDiv_approval', event);
    }
});


function fillOpDetials_approve(patient_id, op_id, id, patient_name, current_visit_id) {
    $('#op_no_approval_hidden').val(patient_id);
    $('#op_no_approval').val(op_id);
    $('#label_patient_name_approval').html(patient_name);
    $('#current_visit_id_approval').val(current_visit_id);
    $('#patient_id_approval').val(id);
    $('#OpAjaxDiv_approval').hide();
}

/* setting for enter key press in ajaxDiv listing */
$("#op_no_approval").on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('OpAjaxDiv_approval');
        return false;
    }
});

//----------UHID search ends-----------------------------------------------------------


function getDailyApprovalReport() {
    var current_visit_id = $('#current_visit_id_approval').val();
    if (current_visit_id == '') {
        Command: toastr["warning"]("Please select patient!");
        $('#op_no').focus();
        return;
    }
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/getDailyApprovalReport",
        data: "current_visit_id=" + current_visit_id,
        beforeSend: function () {
            $('#medication_daily_approval_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#medication_daily_approval_data').LoadingOverlay("hide");
            if (html == 0) {
               // Command: toastr["warning"]("No approved medications found!");
            } else {
                $('#medication_daily_approval_data').html(html);
            }
        },
        complete: function () {
            $(".select_button li").click(function () {
                $(this).toggleClass('active');
                if ($(this).hasClass("active")) {
                    $(this).find('span').text('Selected');
                } else {
                    $(this).find('span').text('Select');
                }
            });
        }
    });
}

function approve_medications() {
    var approved_medications = [];
    var approved_frequency = [];
    var visit_id = $('#current_visit_id_approval').val();
    $('.select_button li').each(function () {
        if ($(this).hasClass("active")) {
            approved_medications.push($(this).find('input[type="hidden"]').val());
            approved_frequency.push($(this).find('input[type="hidden"]').attr("data-frequency"));
        }
    });

    if (approved_medications.length <= 0) {
        Command: toastr["warning"]("No medications Selected!");
        return 0;
    }
    var url = $('#base_url').val() + "/nursing/approveDailyMedications";
    $.ajax({
        type: "GET",
        url: url,
        data: 'approved_medications=' + approved_medications + '&approved_frequency=' + approved_frequency + '&visit_id=' + visit_id,
        beforeSend: function () {
            $('#approve_spin').removeClass('fa-check');
            $('#approve_spin').addClass('fa-spinner fa-spin');
        },
        success: function (html) {
            console.log(html);
            $('#approve_spin').removeClass('fa-spinner fa-spin');
            $('#approve_spin').addClass('fa-check');
            if (html == 1) {
                Command: toastr["success"]("Approved Successfully!");
            } else {
                Command: toastr["error"]("No Internet Connection!");
            }
        },
        complete: function () {

        }
    });

}

function stop_medications(type) {
    var stoped_medications = [];
    var visit_id = $('#current_visit_id_approval').val();
    $('.select_button li').each(function () {
        if ($(this).hasClass("active")) {
            stoped_medications.push($(this).find('input[type="hidden"]').val());
        }
    });

    if (stoped_medications.length <= 0) {
        Command: toastr["warning"]("No medications Selected!");
        return 0;
    }
    var url = $('#base_url').val() + "/nursing/approveDailyMedications";
    $.ajax({
        type: "GET",
        url: url,
        data: 'approved_medications=' + approved_medications + '&approved_frequency=' + approved_frequency + '&visit_id=' + visit_id,
        beforeSend: function () {
            $('#approve_spin').removeClass('fa-check');
            $('#approve_spin').addClass('fa-spinner fa-spin');
        },
        success: function (html) {
            console.log(html);
            $('#approve_spin').removeClass('fa-spinner fa-spin');
            $('#approve_spin').addClass('fa-check');
            if (html == 1) {
                Command: toastr["success"]("Approved Successfully!");
            } else {
                Command: toastr["error"]("No Internet Connection!");
            }
        },
        complete: function () {

        }
    });
}

function add_new_medications(){
    var url = $('#base_url').val() + "/nursing/newMedication";
    $.ajax({
        type: "GET",
        url: url,
        data: 'approved_medications=1',
        beforeSend: function () {
            $('#add_new_medications').removeClass('fa-plus');
            $('#add_new_medications').addClass('fa-spinner fa-spin');
        },
        success: function (html) {
            //console.log(html);
            $('#modal_dialogue').modal('toggle');
            $('#modal_title').html('Add new medication');
            $('#modal_data').html(html);

        },
        complete: function () {

        }
    });
}

//============= Add new prescription ===================================================
//medicine search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_medicine"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var patient_id = $('#patient_id_approval').val();
    var search_type = $("input[name='m_search_type']:checked").val();
    var med_list = $('.medicine-list-div');

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(med_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('#MedicationTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    console.log(data);
                    let response = data.medicine_list;
                    let res_data = "";


                    var search_list = $('#ListMedicineSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;

                            res_data += '<tr><td>' + item_desc + '</td><td>' + generic_name + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    if (response.length > 0) {
                        $('input[name="search_medicine"]').removeClass('outside-medicine');
                    } else {
                        $('input[name="search_medicine"]').addClass('outside-medicine');
                        $(med_list).hide();
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".presc_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});




//outside medicine colour code
$(document).on('blur', 'input[name="search_medicine"]', function (event) {
    let search_item_code_hidden = $('input[name="search_item_code_hidden"]').val();
    let search_medicine = $('input[name="search_medicine"]').val();
    if (search_medicine != "" && search_medicine != undefined && search_item_code_hidden == "") {
        $('input[name="search_medicine"]').addClass('outside-medicine');
    } else {
        $('input[name="search_medicine"]').removeClass('outside-medicine');
    }
});

//close medicine search
$(document).on('click', '.medicine-list-div > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".medicine-list-div").hide();
});

//when select medicine
$(document).on('dblclick', '#ListMedicineSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let name = $(tr).find('input[name="list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="list_med_code_hid[]"]').val();
    let price = $(tr).find('input[name="list_med_price_hid[]"]').val();

    if (name != '' && code != '') {
        $('input[name="search_medicine"]').val(name);
        $('input[name="search_item_code_hidden"]').val(code);
        $('input[name="search_item_name_hidden"]').val(name);
        $('input[name="search_item_price_hidden"]').val(price);

        $(".medicine-list-div").hide();
        $("input[name='search_medicine']").focus();
    }

});

//frequency search
var freq_timeout = null;
var freq_last_search_string = '';

// $('#search_frequency').on('keyup', function () {
    $(document).on('keyup', 'input[id="search_frequency"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_freq_string = $(this).val();
    var patient_id = $('#patient_id_approval').val();
    var freq_list = $('.frequency-list-div');
    if (search_freq_string == "" || search_freq_string.length < 2) {
        freq_last_search_string = '';
        return false;
    } else {
        $(freq_list).show();
        clearTimeout(freq_timeout);
        freq_timeout = setTimeout(function () {
            if (search_freq_string == freq_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/frequency-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_freq_string: search_freq_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('#FrequencyTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";


                    var freq_search_list = $('#ListFrequencySearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let frequency = response[i].frequency;
                            let frequency_value = response[i].frequency_value;

                            res_data += '<tr><td>' + frequency + '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' + i + '" value="' + frequency_value + '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' + i + '" value="' + frequency + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(freq_search_list).html(res_data);
                    freq_last_search_string = search_freq_string;
                    $(".freq_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.freq_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close frequency search
$(document).on('click', '.close_btn_freq_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".frequency-list-div").hide();
});

//when select frequency
$(document).on('dblclick', '#ListFrequencySearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
    let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();

    if (name != '' && value != '') {
        $('input[name="search_frequency"]').val(name);
        $('input[name="search_freq_value_hidden"]').val(value);
        $('input[name="search_freq_name_hidden"]').val(name);

        $(".frequency-list-div").hide();
        $('input[name="search_frequency"]').focus();
    }

});

function addNewMedicine() {
    let status = validateRow();
    if (status == false) {
        console.log("Error");
        return false;
    }

    let mode = 'add';

    getNewRowInserted(mode);

    $('input[name="search_medicine"]').focus();
}

//add row
function getNewRowInserted(mode = 'add', data = {}) {

    let code = "";
    if (mode == 'add') {
        code = $('input[name="search_item_code_hidden"]').val();
    }else{
        if (Object.keys(data).length > 0) {
            code = data.med_code;
        }
    }
    if(code != ""){
      allergy_check(code);
    }

    var table = document.getElementById("medicine-listing-table");
    let row_count = table.rows.length;
    let row_id = row_count;
    if (parseInt(row_count) > 0) {
        let last_id = $('#medicine-listing-table tr:last').attr('row-id');
        if (last_id != undefined) {
            row_id = parseInt(last_id) + 1;
        }
    }

    var row = table.insertRow(row_count);

    row.setAttribute("row-id", row_id, 0);

    var cell1 = row.insertCell(0);
    cell1.width = '30%';
    var cell2 = row.insertCell(1);
    cell2.width = '10%';
    var cell3 = row.insertCell(2);
    cell3.width = '10%';
    var cell4 = row.insertCell(3);
    cell4.width = '10%';
    var cell5 = row.insertCell(4);
    cell5.width = '7%';
    var cell6 = row.insertCell(5);
    cell6.width = '15%';
    var cell7 = row.insertCell(6);
    cell7.width = '15%';
    var cell8 = row.insertCell(7);
    cell8.classList.add("text-center");
    cell8.width = '3%';

    let med_code = "";
    let med_name = "";
    let duration = "";
    let dose = "";
    let quantity = "";
    let route = "";
    let freq_value = "";
    let freq_name = "";
    let remarks = "";
    let id = "";
    let price = 0;

    if (mode == 'add') {

        med_code = $('input[name="search_item_code_hidden"]').val();

        //in case of outside medicine
        if (med_code == "") {
            med_name = $('input[name="search_medicine"]').val();
            if (med_name != "") {
                cell1.classList.add("outside-medicine");
                $('input[name="search_medicine"]').removeClass('outside-medicine');
            } else {
                cell1.classList.remove("outside-medicine");
            }
        } else {
            med_name = $('input[name="search_item_name_hidden"]').val();
            cell1.classList.remove("outside-medicine");
        }

        dose = $('input[name="search_dose"]').val();
        duration = $('input[name="search_duration"]').val();
        quantity = $('input[name="search_quantity"]').val();

        freq_value = $('input[name="search_freq_value_hidden"]').val();
        freq_name = $('input[name="search_freq_name_hidden"]').val();

        route = $('input[name="search_route"]').val();
        remarks = $('input[name="search_instructions"]').val();

        price = $('input[name="search_item_price_hidden"]').val();

    } else {
        //load bsed on prescription_id
        if (Object.keys(data).length > 0) {
            med_code = data.med_code;
            med_name = data.med_name;
            out_medicine_name = data.out_medicine_name;
            duration = data.duration;
            quantity = data.quantity;
            dose = data.dose;
            price = data.price;
            freq_value = data.freq_value;
            freq_name = data.freq_name;
            route = data.route;
            remarks = data.remarks;
            id = data.id;
            head_id = data.head_id;

            //in case of outside medicine
            if (med_code == "") {
                if (out_medicine_name != "") {
                    med_name = out_medicine_name;
                    cell1.classList.add("outside-medicine");
                } else {
                    cell1.classList.remove("outside-medicine");
                }
            } else {
                cell1.classList.remove("outside-medicine");
            }

            if (head_id != '' && head_id != 0) {
                $('#prescription_head_id').val(head_id);
            }
        }
    }

    let medicine_popup_search = '<div class="medicine-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_med_search">X</a><div class=" presc_theadscroll" style="position: relative;"><table id="MedicationTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><thead><tr class="light_purple_bg"><th>Medicine</th><th>Generic Name</th></tr></thead><tbody id="ListMedicineSearchDataRowListing-' + row_id + '" class="list-medicine-search-data-row-listing" ></tbody></table></div></div>';

    let medicine_freq_popup_search = '<div class="frequency-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_freq_search">X</a><div class=" freq_theadscroll" style="position: relative;"><table id="FrequencyTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><tbody id="ListFrequencySearchDataRowListing-' + row_id + '" class="list-frequency-search-data-row-listing" ></tbody></table></div></div>';

    cell1.innerHTML = "<span class='med_name'>" + med_name + "</span> <input type='hidden' name='selected_item_code[]' id='selected_item_code-" + row_id + "' value='" + med_code + "'><input type='hidden' class='form-control' name='selected_item_name[]' id='selected_item_name-" + row_id + "' value='" + med_name + "'><input type='hidden' name='selected_edit_id[]' id='selected_edit_id-" + row_id + "' value='" + id + "'><input type='hidden' name='selected_item_price[]' id='selected_item_price-" + row_id + "' value='" + price + "'>" + medicine_popup_search;
    cell2.innerHTML = "<span class='dose'>" + dose + "</span> <input type='hidden' class='form-control' name='selected_item_dose[]' id='selected_item_dose-" + row_id + "' value='" + dose + "'>";
    cell3.innerHTML = "<span class='duration'>" + duration + "</span> <input type='hidden' class='form-control' name='selected_item_duration[]' id='selected_item_duration-" + row_id + "' value='" + duration + "'>";
    cell4.innerHTML = "<span class='frequency'>" + freq_name + "</span> <input type='hidden' class='form-control' name='selected_item_frequency[]' id='selected_item_frequency-" + row_id + "' value='" + freq_name + "'><input type='hidden' name='selected_frequency_value[]' id='selected_frequency_value-" + row_id + "' value='" + freq_value + "'>" + medicine_freq_popup_search;
    cell5.innerHTML = "<span class='quantity'>" + quantity + "</span> <input type='hidden' class='form-control' name='selected_item_quantity[]' id='selected_item_quantity-" + row_id + "' value='" + quantity + "'>";
    cell6.innerHTML = "<span class='route'>" + route + "</span> <input type='hidden' class='form-control' name='selected_item_route[]' id='selected_item_route-" + row_id + "' value='" + route + "'>";
    cell7.innerHTML = "<span class='remarks'>" + remarks + "</span> <input type='hidden' class='form-control' name='selected_item_remarks[]' id='selected_item_remarks-" + row_id + "' value='" + remarks + "'>";
    cell8.innerHTML = "<button class='btn_sm delete_row del-presc-list-row'><i class='fa fa-trash'></i></button>";

    if (mode == 'add') {
        clearmedfields();
    }
    calculateTotalMedicineAmount();
}

//clear fields
function clearmedfields() {
    $('input[name="search_item_code_hidden"]').val('');
    $('input[name="search_item_name_hidden"]').val('');
    $('input[name="search_medicine"]').val('');

    $('input[name="search_duration"]').val('');
    $('input[name="search_dose"]').val('');
    $('input[name="search_route"]').val('');
    $('input[name="search_quantity"]').val('');

    $('input[name="search_freq_value_hidden"]').val('');
    $('input[name="search_freq_name_hidden"]').val('');
    $('input[name="search_frequency"]').val('');

    $('input[name="search_instructions"]').val('');
}

//validate top row
function validateRow() {
    let err = 0;

    let itm_name = $('input[name="search_medicine"]').val();
    let itm_code = $('input[name="search_item_code_hidden"]').val();
    if (itm_name == '' || itm_name == undefined) {
        Command: toastr["error"]("Enter Medicine.");
        err = 1;
    }

    let itm_duration = $('input[name="search_duration"]').val();
    if (itm_duration == '' || itm_duration == undefined) {
        Command: toastr["error"]("Enter Duration.");
        err = 1;
    }

    let itm_quantity = $('input[name="search_quantity"]').val();
    if (itm_quantity == '' || itm_quantity == undefined) {
        Command: toastr["error"]("Enter Quantity.");
        err = 1;
    }

    let itm_freq_val = $('input[name="search_freq_value_hidden"]').val();
    let itm_freq_name = $('input[name="search_freq_name_hidden"]').val();
    if (itm_freq_val == '' || itm_freq_val == undefined || itm_freq_name == '' || itm_freq_name == undefined) {
        Command: toastr["error"]("Enter Frequency.");
        err = 1;
    }

    //same item already exists
    let alredy_exist = $('input[name="selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == itm_code && itm_code != "") {
            return itm_code;
        }
    });

    if (alredy_exist.length > 0) {
        Command: toastr["error"]("Medicine already exist.");
        err = 1;
    }

    if (err == 1) {
        return false;
    } else {
        return true;
    }
}


//check any changes occur in listing
var medicine_listing_table_changed = 0;
var medicine_saved_after_edit = 0;


var prescription_callback = function (mutationsList, observer) {
    for (var mutation of mutationsList) {
        if (mutation.type == 'childList') {
            // console.log('childList');
            medicine_listing_table_changed = 1;
        }
        else if (mutation.type == 'attributes') {
            // console.log('attributes');
            medicine_listing_table_changed = 1;
        }
    }
};

// // Create an observer instance linked to the callback function
// var prescriptionObserver = new MutationObserver(prescription_callback);

// // Select the node that will be observed for mutations
// var targetNode = document.getElementById('medicine-listing-table');

// // Options for the observer (which mutations to observe)
// var config = { attributes: true, childList: true, subtree: true };

// // Start observing the target node for configured mutations
// prescriptionObserver.observe(targetNode, config);


// //trigger when click outside
// window.addEventListener('click', function (e) {
//     if (document.getElementById('prescription_wrapper_card_body').contains(e.target)) {
//         // Clicked in prescription_wrapper
//     } else {
//         // Clicked outside the prescription_wrapper
//         if (medicine_listing_table_changed == 1 && medicine_saved_after_edit == 0) {
//             saveDoctorPrescriptions();
//         }
//     }
// });

//Route search
var route_timeout = null;
var route_last_search_string = '';
 $(document).on('keyup', 'input[name="search_route"]', function (event) {

    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_route_string = $(this).val();
    var patient_id = $('#patient_id_approval').val();
    var route_list = $('.route-list-div');

    if (search_route_string == "" || search_route_string.length < 2) {
        route_last_search_string = '';
        return false;
    } else {
        var route_list = $('.route-list-div');
        $(route_list).show();
        clearTimeout(route_timeout);
        route_timeout = setTimeout(function () {
            if (search_route_string == route_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/route-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_route_string: search_route_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $('#RouteTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var route_search_list = $('#ListRouteSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let route = response[i].route;

                            res_data += '<tr><td>' + route + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(route_search_list).html(res_data);
                    route_last_search_string = search_route_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close route search
$(document).on('click', '.close_btn_route_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".route-list-div").hide();
});


//when select route
$(document).on('dblclick', '#ListRouteSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let route = $(tr).text();

    if (route != '') {
        $('input[name="search_route"]').val(route);

        $(".route-list-div").hide();
    }

});

//calculate Final Total Amount;
function calculateTotalMedicineAmount() {
    var price = 0;
    var quantity = 0;
    var final_total = 0;
    $("input[name='selected_item_price[]']").each(function (input_index, input_val) {
        price = $('input[name="selected_item_price[]"]').eq(input_index).val();
        quantity = $('input[name="selected_item_quantity[]"]').eq(input_index).val();
        if (parseFloat(quantity) > 0) {
            final_total = parseFloat(final_total) + (parseFloat(price) * parseFloat(quantity));
        } else {
            final_total = parseFloat(final_total) + parseFloat(price);
        }
    });
    if (isNaN(final_total)) {
        final_total = 0;
    }
    $("#med_total_amnt").text(final_total.toFixed(2));
    $("#approx_amnt").val(final_total.toFixed(2));
}

var p_footer_fields_timeout = null;
function update_prescription_footer_fields(){
  let next_review_date = $('#next_review_date').val();
  let reason_for_visit = $('#visit_reason').val();
  let diagnosis = $('#pres_history').val();
  let special_notes = $('#pres_notes').val();
  let encounter_id = $('#encounter_id').val();
  if(encounter_id != "" && encounter_id > 0 && encounter_id != undefined){

    clearTimeout(p_footer_fields_timeout);
    p_footer_fields_timeout = setTimeout(function () {

      let url = $('#base_url').val();
      let _token = $('#c_token').val();
      $.ajax({
          type: "POST",
          url: url + "/emr/prescription-footer-fields-save",
          data: {
              encounter_id: encounter_id,
              next_review_date : next_review_date,
              reason_for_visit : reason_for_visit,
              diagnosis : diagnosis,
              special_notes : special_notes,
              _token: _token
          },
          beforeSend: function () {

          },
           success: function (data) {

          },
          complete: function () {

          }
      });

    }, 500);

  }else{
    Command: toastr["warning"]("Error.! Encounter Not Found.");
  }
}
function prescriptionDeleteMode(obj,id){
  if(id != "" && id > 0){
      var result = confirm("Want to delete?");
      if (result) {

        let _token = $('#c_token').val();
        var url = $('#base_url').val()+"/emr/remove-patient-prescription";
        $.ajax({
          type: "POST",
          url: url,
          data: "removePrescrption=" + id+"&_token="+_token,
          beforeSend: function () {
          },
          success: function (data) {
            if(data.status == 1){
              $(obj).closest('tr').remove();
               Command: toastr["success"]("Removed Successfully..!");
            }
          },
          complete: function () {
          }
        });
      }
  }
}

function allergy_check(item_code) {

    var selectedItems = [];
    $('input[name="selected_item_code[]"]').map(function (ind, el) {
        if ($(el).val() != '') {
            selectedItems.push($(el).val());
        }
    });

    let patId =  $('#patient_id_approval').val();

    var url = $('#base_url').val() + "/emr/allergy-check";

    $.ajax({
        type: "GET",
        url: url,
        async: false,
        data: 'patId=' + patId + '&Itemcode=' + item_code + '&selcodes=' + JSON.stringify(selectedItems),
        beforeSend: function () {

        },
        success: function (data) {
            let response = JSON.parse(data);
            if (response.status == 0) {
                //allergy true
                // bootbox.alert({
                //   message: response.message,
                //   className: 'rubberBand animated',
                //   buttons: {
                //       ok: {
                //           label: 'Close',
                //           className: 'btn-primary'
                //       }
                //   }
                // });
                Command: toastr["warning"](response.message);
            } else {
                if (response.is_interaction == 1) {
                    // bootbox.alert({
                    //   message: response.message,
                    //   className: 'rubberBand animated',
                    //   buttons: {
                    //       ok: {
                    //           label: 'Close',
                    //           className: 'btn-primary'
                    //       }
                    //   }
                    // });
                    Command: toastr["warning"](response.message);
                }
            }

        },
        complete: function () {

        }
    });
}

function deleteRowFromDb(id) {
    var url = $('#base_url').val() + "/emr/prescription-delete-item";
    var patient_id =  $('#patient_id_approval').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            patient_id: patient_id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}

function addTapperDose(item,item_code){


    var item = $('#search_item_name_hidden').val();
    var item_code = $('#search_item_code_hidden').val();

    if(item == '' || item_code == ''){
        Command: toastr["warning"]("Select Medicine First!");
    }else{
        $('#modal_tapper_dose').modal('toggle');
        $('#tapper_medicine_name').html(item);
        $('#tapper_medicine_code').val(item_code);
    }
}

function add_new_row_tapper_dose(){
    var table = document.getElementById("tapper_dose_table");
    let row_count = table.rows.length;
    let row_id = row_count;
    if (parseInt(row_count) > 0) {
        let last_id = $('#tapper_dose_table tr:last').attr('row-id');
        if (last_id != undefined) {
            row_id = parseInt(last_id) + 1;
        }
    }
    var row = table.insertRow(row_count);
    row.setAttribute("row-id", row_id, 0);

    var cell1 = row.insertCell(0);
    cell1.width = '30%';
    var cell2 = row.insertCell(1);
    cell2.width = '30%';
    var cell3 = row.insertCell(2);
    cell3.width = '30%';
    var cell4 = row.insertCell(3);
    cell4.width = '20%';

    cell1.innerHTML = "<input type='text'name='dosage[]' id='dosage_"+row_id+"' class='form-control'>";

    cell2.innerHTML = "<div class='col-md-12 padding_xs' style='text-align:center'><input type='text' id='start_at_"+row_id+"' name='start_at[]' class='form-control datepicker' data-attr='date' placeholder='Date'></div>";

    cell3.innerHTML = "<div class='col-md-12 padding_xs' style='text-align:center'><input type='text' id='stop_at_"+row_id+"' name='stop_at[]' class='form-control datepicker' data-attr='date' placeholder='Date'></div>";

    cell4.innerHTML = "<button type='button' class='btn btn-sm bg-red del-tapperdose-list-row'><i class='fa fa-trash'></i></button>";
}

$(document).on('click', '.del-tapperdose-list-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        $(tr).remove();

    }
});

var save_starts = 0;
//save prescription
function saveNewPrescriptions() {
    let validate = validatePrescription();
    let _token = $('#c_token').val();

    if (save_starts == 1) {
        return false;
    }

    let patient_id = $('#patient_id_approval').val();
    let visit_id = $('#current_visit_id_approval').val();
    let encounter_id = $('#encounter_id').val();
    let prescription_head_id = $('#prescription_head_id').val();
    let presc_form = $('#presc-data-form').serialize();
    var p_type = $("input[name='p_search_type']:checked").val();

    if (validate) {
        var url = $('#base_url').val() + "/emr/save-prescription";

        $('.save_btn_bg').prop({ disable: 'true' });
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: presc_form + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&prescription_head_id=' + prescription_head_id + '&intend_type=' + p_type + 'ns_prescription=1',
            beforeSend: function () {

            },
            success: function (data) {
                if (data.status == 1) {
                    Command: toastr["success"]("Saved Successfully.");
                    $('#modal_dialogue').modal('toggle');
                    $('#add_new_medications').addClass('fa-plus');
                    $('#add_new_medications').removeClass('fa-spinner fa-spin');
                }
            },
            complete: function () {

            }
        });

    } else {
        medicine_listing_table_changed = 0;
        $('.save_btn_bg').prop({ disable: 'false' });
    }
}
function validatePrescription() {
    //check rows exist
    if ($('#medicine-listing-table tbody > tr').find('input[name="selected_item_code[]"]').length == 0) {
        Command: toastr["error"]("Select Medicine.");
        return false;
    }

    return true;
}
//============= Add new prescription ends ==============================================
