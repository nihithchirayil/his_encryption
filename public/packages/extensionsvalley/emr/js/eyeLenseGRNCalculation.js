function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    return number.toFixed(parseInt(decimalConfiguration));
}

function validateText(row_id) {
    $(".active_class" + row_id).attr('disabled', false);
    $(".active_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        } else if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        } else if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        } else if (!val) {
            $(this).val(0);
            flag = 1;
        }
        if ($(this).hasClass("percentage_class" + row_id)) {
            if (parseFloat(val) > 100) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            }
        }
        if (flag == 0) {
            $(this).val(checkIsNaN(val));
        }
    });

    $(".number_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        }
        if (!val) {
            $(this).val(0);
            flag = 1;
        }
    });
}

function insertEyeLensGrnCalculation(po_count, is_igst) {
    var row_id = $('#row_count_id').val();
    for (var i = 1; i <= po_count; i++) {
        calculateListAmount(row_id, 1);
        row_id++;
    }
    return row_id;
}


function calculateListNetTotal(tot_rate, tax_array, charge_type) {
    var tot_amt = 0.0;
    detailtax_array = [];
    detailtax_array['cgst_per'] = 0.0;
    detailtax_array['cgst_amt'] = 0.00;
    detailtax_array['sgst_per'] = 0.0;
    detailtax_array['sgst_amt'] = 0.00;
    detailtax_array['igst_per'] = 0.0;
    detailtax_array['igst_amt'] = 0.00;
    detailtax_array['discount_per'] = 0.00;
    detailtax_array['discount_amt'] = 0.00;

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        charge_type_val = 1;
        if (charge_type[parseInt(index)]) {
            charge_type_val = charge_type[parseInt(index)];
        }
        if (parseInt(index) == 1) {
            detailtax_array['discount_per'] = value;
            detailtax_array['discount_amt'] = amount;
            tot_amt -= parseFloat(amount);
            tot_rate = parseFloat(tot_rate) - parseFloat(amount);
        }
    });

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        if (parseInt(index) == 11) {
            detailtax_array['Freight_Charges_per'] = value;
            detailtax_array['flightcharge_amt'] = amount;
            tot_amt += parseFloat(amount);
        }

        if (parseInt(index) == 10) {
            detailtax_array['cgst_per'] = value;
            detailtax_array['cgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 9) {
            detailtax_array['sgst_per'] = value;
            detailtax_array['sgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 12) {
            detailtax_array['igst_per'] = value;
            detailtax_array['igst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
    });
    return detailtax_array;
}


function calculateListAmount(row_id, from_type) {
    var item_code = $('#item_code_hidden' + row_id).val();
    if (item_code) {
        validateText(row_id);
        var qty = $('#grn_qty' + row_id).val();
        var uom_vlaue = $('#uom_vlaue' + row_id).val();
        var free_qty = $('#free_qty' + row_id).val();
        var item_price = $('#unit_rate' + row_id).val();
        var discount_amt = 0.0;
        var discount_per = 0.0;
        var without_discount = 0.0;
        var total_qty = parseFloat(uom_vlaue) * ((parseFloat(qty) + parseFloat(free_qty)));
        if (!item_price) {
            item_price = 0;
            $('#unit_rate' + row_id).val(0);
        }
        var discountAmountType = $('#discount_type' + row_id).val();
        $('#tot_qty_' + row_id).val(checkIsNaN(total_qty));

        var gst_amt = 0.0;
        var amount = 0.0;
        var tot_rate = qty * item_price;
        var discount_amt = $('#tot_dis_amt' + row_id).val();
        if (!discount_amt || (parseFloat(discount_amt) > parseFloat(tot_rate))) {
            discount_amt = 0;
        }
        gst_amt = $('#tot_dis_amt' + row_id).val();
        if (!gst_amt) {
            gst_amt = 0;
        }
        if (parseInt(discountAmountType) == 1) {
            if (parseFloat(gst_amt) > 100) {
                gst_amt = 0;
                discount_per = 0.0;
                $('#tot_dis_amt' + row_id).val(checkIsNaN(0));
            }
            amount = (gst_amt * tot_rate) / 100;
            if (amount === Infinity) {
                amount = 0;
            }
            discount_amt = amount;
            discount_per = gst_amt;
        } else if (parseInt(discountAmountType) == 2) {
            if (parseFloat(gst_amt) >= parseFloat(tot_rate)) {
                gst_amt = 0;
                $('#tot_dis_amt' + row_id).val(checkIsNaN(0));
            }
            discount_amt = gst_amt;
            if (tot_rate == '' || parseFloat(tot_rate) == 0) {
                amount = 0.0;
            } else {
                amount = (100 * gst_amt) / tot_rate;
                if (amount === Infinity) {
                    amount = 0;
                }
            }
            discount_per = amount;
        }
        var without_discount = parseFloat(tot_rate) - parseFloat(discount_amt);

        gst_amt = $('#tax_type' + row_id).val();
        if (parseFloat(gst_amt) > 100) {
            gst_amt = 0;
            $('#tax_type' + row_id).val('');
        }
        if (!gst_amt) {
            gst_amt = 0;
        }
        amount = (gst_amt * without_discount) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        $('#tot_tax_amt' + row_id).val(checkIsNaN(amount));
        var net_rate = parseFloat(without_discount) + parseFloat(amount);
        $('#net_rate' + row_id).val(checkIsNaN(net_rate));
        console.log(discount_amt);
        updateItemArray(row_id, net_rate, amount, discount_amt, discount_per);
    } else {
        toastr.warning("Please Seclect any Item");
    }
}


function updateItemArray(row_id, net_rate, total_tax, discount_amt, discount_per) {
    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
    var po_head_id = $('#po_head_id_hidden' + row_id).val();
    var po_detail_id = $('#po_detail_id_hidden' + row_id).val();
    var item_id = $('#item_id_hidden' + row_id).val();
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_desc = $('#item_desc' + row_id).val();
    var refe_no = $('#refe_no' + row_id).val();
    var resdv = $('#resdv' + row_id).val();
    var resnv = $('#resnv' + row_id).val();
    var recdv = $('#recdv' + row_id).val();
    var recnv = $('#recnv' + row_id).val();
    var reanv = $('#reanv' + row_id).val();
    var readv = $('#readv' + row_id).val();
    var lesdv = $('#lesdv' + row_id).val();
    var lesnv = $('#lesnv' + row_id).val();
    var lecdv = $('#lecdv' + row_id).val();
    var lecnv = $('#lecnv' + row_id).val();
    var leanv = $('#leanv' + row_id).val();
    var leadv = $('#leadv' + row_id).val();
    var patient_id = $('#patientid_hidden' + row_id).val();
    var grn_qty = $('#grn_qty' + row_id).val();
    var item_rate = $('#unit_rate' + row_id).val();
    var selling_rate = $('#selling_rate' + row_id).val();
    var discount_type = $('#discount_type' + row_id).val();
    var tot_dis_amt = $('#tot_dis_amt' + row_id).val();
    var tax_type = $('#tax_type' + row_id).val();
    var tot_tax_amt = $('#tot_tax_amt' + row_id).val();
    var net_rate = $('#net_rate' + row_id).val();
    var patientname = $('#patientname' + row_id).val();
    var glass_prescription_id = $('#glass_prescription_id' + row_id).val();
    var is_igst = $('#is_igstcheck').is(":checked");
    var tot_rate = grn_qty * item_rate;
    var is_free = 0;

    var sgst_amt = 0;
    var cgst_amt = 0;
    var sgst_per = 0;
    var cgst_per = 0;
    var igst_per = 0;
    var igst_amt = 0;
    if (!is_igst) {
        if (tax_type) {
            var tax_division = $('#tax_type' + row_id + ' option:selected').attr('attt-id');
            sgst_per = checkIsNaN(tax_division);
            cgst_per = checkIsNaN(tax_division);
            sgst_amt = checkIsNaN(total_tax) / 2;
            cgst_amt = checkIsNaN(total_tax) / 2;
        }
    } else {
        if (tax_type) {
            igst_per = tax_type;
            igst_amt = $('#tot_tax_amt' + row_id).val();
        }
    }
    var free_qty = 0;
    var uom_value = 1;

    var item_full_rates = getItemPrice(item_rate, grn_qty, free_qty, uom_value, net_rate, total_tax);

    var unit_rate = item_full_rates.unit_rate;
    var unit_cost = item_full_rates.unit_cost;
    var total_qty = item_full_rates.total_qty;
    var tot_free_qty = item_full_rates.tot_free_qty;
    var all_total_qty = item_full_rates.all_total_qty;
    var unit_cost_with_out_tax = item_full_rates.unit_cost_with_out_tax;
    var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
    var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
    var total_othercharges = 0;
    var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(discount_amt);

    item_array[row_id] = {
        bill_detail_id: bill_detail_id,
        patient_id: patient_id,
        patientname: patientname,
        po_head_id: po_head_id,
        po_detail_id: po_detail_id,
        item_id: item_id,
        item_type: 0,
        hsn_code: '',
        item_code: item_code,
        item_desc: item_desc,
        refe_no: refe_no,
        resdv: resdv,
        resnv: resnv,
        recdv: recdv,
        recnv: recnv,
        readv: readv,
        reanv: reanv,
        lesdv: lesdv,
        lesnv: lesnv,
        lecdv: lecdv,
        lecnv: lecnv,
        leanv: leanv,
        leadv: leadv,
        grn_qty: grn_qty,
        item_unit: 1,
        unit_cost: unit_cost,
        unit_cost_with_out_tax: unit_cost_with_out_tax,
        uom_val: uom_value,
        free_qty: free_qty,
        is_free: is_free ? 1 : 0,
        total_qty: total_qty,
        tot_free_qty: checkIsNaN(tot_free_qty),
        all_total_qty: all_total_qty,
        discount_type: discount_type,
        selling_rate: selling_rate,
        grn_mrp: 0,
        unit_mrp: 0,
        glass_prescription_id: glass_prescription_id,
        item_rate: checkIsNaN(item_rate),
        unit_rate: checkIsNaN(unit_rate),
        cgst_per: checkIsNaN(cgst_per),
        cgst_amt: checkIsNaN(cgst_amt),
        sgst_per: checkIsNaN(sgst_per),
        sgst_amt: checkIsNaN(sgst_amt),
        igst_per: checkIsNaN(igst_per),
        igst_amt: checkIsNaN(igst_amt),
        tot_dis_amt: tot_dis_amt,
        tot_tax_amt: tot_tax_amt,
        tax_type: tax_type,
        total_tax_perc: checkIsNaN(total_tax_perc),
        total_tax_amt: checkIsNaN(total_tax_amt),
        discount_per: checkIsNaN(discount_per),
        discount_amt: checkIsNaN(discount_amt),
        total_othercharges: checkIsNaN(total_othercharges),
        totalRate: checkIsNaN(tot_rate),
        netRate: checkIsNaN(net_amt)
    }
    console.log(item_array);
    getAllItemsTotals();
}


function getItemPrice(rate, grn_qty, free_qty, uom_unit, net_amt, total_tax_amt) {
    var unit_cost = 0;
    var unit_cost_with_out_tax = 0;
    var return_data = {}
    if (!uom_unit) {
        uom_unit = 0;
    }


    var tot_unit_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * uom_unit;
    var unit_rate = 0;
    if (parseInt(uom_unit) != 0) {
        unit_rate = (rate / uom_unit);
    }


    if (!tot_unit_qty || parseFloat(tot_unit_qty) == 0) {
        tot_unit_qty = 0;
    } else {
        unit_cost = (net_amt) / tot_unit_qty;
    }

    if (parseFloat((parseFloat(grn_qty) * parseFloat(uom_unit))) != 0) {
        unit_cost_with_out_tax = (parseFloat(unit_cost)) - ((parseFloat(total_tax_amt) / ((parseFloat(grn_qty)) * parseFloat(uom_unit))));
    }


    var total_qty = parseFloat(grn_qty) * parseFloat(uom_unit);
    var tot_free_qty = parseFloat(free_qty) * parseFloat(uom_unit);
    var all_total_qty = parseFloat(tot_unit_qty);

    return_data = {
        unit_rate: unit_rate,
        unit_cost: unit_cost,
        total_qty: total_qty,
        tot_free_qty: tot_free_qty,
        all_total_qty: all_total_qty,
        unit_cost_with_out_tax: unit_cost_with_out_tax
    }
    return return_data;
}



function getAllItemsTotals() {
    $sgst_amt_total = 0.0;
    $igst_amt_total = 0.0;
    discount_amt_total = 0.0;
    var total_array = [];

    var bill_discount = 0.0;
    var total_gross = 0.0;
    var total_othercharges = 0.0;
    var total_tax = 0.0;

    $.each(item_array, function (index, value) {
        total_tax += parseFloat(value.tot_tax_amt);
        discount_amt_total = parseFloat(discount_amt_total) + parseFloat(value.discount_amt);
        total_gross = parseFloat(total_gross) + parseFloat(value.totalRate);
    });

    var round_off = $('#round_off').val();
    if (!round_off) {
        $('#round_off').val(0);
        round_off = 0.0;
    }
    var freight_charges = $('#freight_charges').val();
    var other_charges = $('#other_charges').val();
    if (!freight_charges) {
        freight_charges = 0.0;
    }
    if (!other_charges) {
        other_charges = 0.0;
    }
    $('#freight_charges').val(checkIsNaN(freight_charges));
    $('#other_charges').val(checkIsNaN(other_charges));
    $('#tototherfreight_amt').html(checkIsNaN(parseFloat(checkIsNaN(freight_charges)) + parseFloat(checkIsNaN(other_charges))));

    var total_discount = parseFloat(discount_amt_total) + parseFloat(bill_discount);
    var net_amount = (parseFloat(total_gross) + parseFloat(freight_charges) + parseFloat(other_charges) + parseFloat(total_tax) + parseFloat(total_othercharges)) - parseFloat(total_discount);

    var bill_discount_type = $('#bill_discount_type').val();
    var bill_discount_value = $('#bill_discount_value').val();

    if (!bill_discount_value) {
        bill_discount_value = 0.0;
    }

    if (parseFloat(net_amount) > 0) {
        if (parseInt(bill_discount_type) == 1) {
            if (bill_discount_value > 100) {
                bill_discount_value = 0.0;
            }
            bill_discount = (net_amount * bill_discount_value) / 100;
        } else if (parseInt(bill_discount_type) == 2) {
            if (bill_discount_value > net_amount) {
                bill_discount_value = 0.0;
            }
            bill_discount = bill_discount_value;
        }
    } else {
        bill_discount_value = 0.0;
    }
    $('#bill_discount_value').val(checkIsNaN(bill_discount_value));
    $('#tot_bill_discount_amt').html(checkIsNaN(bill_discount));
    net_amount = (parseFloat(net_amount) + parseFloat(round_off)) - (parseFloat(bill_discount));

    total_array['total_gross'] = checkIsNaN(total_gross);
    total_array['total_dis'] = checkIsNaN(total_discount);
    total_array['total_tax'] = checkIsNaN(total_tax);
    total_array['total_net'] = checkIsNaN(net_amount);
    total_array['bill_discount'] = checkIsNaN(bill_discount);

    if ($('#ITEMDISC_amot')) {
        $('#ITEMDISC_amot').html(checkIsNaN(discount_amt_total));
    }
    if ($('#gross_amount_hd').val()) {
        $('#gross_amount_hd').val(checkIsNaN(total_gross));
    }
    if ($('#discount_hd').val()) {
        $('#discount_hd').val(checkIsNaN(total_discount));
    }
    if ($('#tot_tax_amt_hd').val()) {
        $('#tot_tax_amt_hd').val(checkIsNaN(total_tax));
    }
    if ($('#net_amount_hd').val()) {
        $('#net_amount_hd').val(checkIsNaN(net_amount));
    }

    if ($('#tot_gross_footer')) {
        $('#tot_gross_footer').html(checkIsNaN(total_gross));
    }
    if ($('#tot_tax_footer')) {
        $('#tot_tax_footer').html(checkIsNaN(total_tax));
    }
    if ($('#tot_dicount_footer')) {
        $('#tot_dicount_footer').html(checkIsNaN(total_discount));
    }
    if ($('#tot_net_amt_footer')) {
        $('#tot_net_amt_footer').html(checkIsNaN(net_amount));
    }
    return total_array;
}
