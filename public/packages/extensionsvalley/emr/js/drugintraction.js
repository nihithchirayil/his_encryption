$(document).ready(function() {
    searcIntraction();
});

function searcIntraction() {
    var gen_id = $("#generic_name_id").val();
    var base_url = $("#base_url").val();
    var token = $("#token_hiddendata").val();
    var param = { _token: token, gen_id: gen_id };

    var url = base_url + "/master/searcIntraction";

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchBtn").attr("disabled", true);
            $("#searchSpin").removeClass("fa fa-search");
            $("#searchSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {

            $("#searchDataDiv").html(data);

            // formReset();
        },
        complete: function() {
            $("#searchBtn").attr("disabled", false);
            $("#searchSpin").removeClass("fa fa-spinner fa-spin");
            $("#searchSpin").addClass("fa fa-search");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function formReset() {
    $("#edit_id").val("");
    $("#generic_name1").val("");
    $("#generic_name2").val("");
    $("#generic_name_id1").val("");
    $("#generic_name_id2").val("");
    $("#severity_id").val("");
    $("#action_recommender").val("");
    $("#intraction").val("");
    $('#box_head').html('Add Interaction')
    $("#savebtnBtn").html('<i class="fa fa-save"></i> Save');
}



function getSubMenu() {
    $('#sub_menu_id').css('display', 'block');
    var base_url = $("#base_url").val();
    var sub_id = $("#parent_id").val();
    var params = { sub_id: sub_id };
    var url = base_url + "/admin/submenuautofill";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function() {

            $("#sub_id").empty();

        },
        success: function(data) {
            if (data) {
                $("#sub_id").html('<option value="">Select Sub Menu</option>');
                $.each(data, function(key, value) {
                    ($("#sub_id").append('<option value="' + value.id + '">' + value.name + '</option>'));
                })

            }

        },


    });
}


function editItem(id,g1,gn1,g2,gn2,sev,ac,intr) {
       $("#savebtnBtn").html('<i class="fa fa-save"></i> Update');
       $('#box_head').html('Update Interaction');
       $('#edit_id').val(id);
       $('#generic_name_id1').val(g1);
       $('#generic_name1').val(gn1);
       $('#generic_name_id2').val(g2);
       $('#generic_name2').val(gn2);
       $('#severity_id').val(sev);
       $('#action_recommender').val(atob(ac));
       $('#intraction').val(atob(intr));

}

function deleteIntraction(id) {
    var base_url = $("#base_url").val();
    var token = $("#token_hiddendata").val();
   
bootbox.confirm({
    message: 'Are you sure,You want to delete this interaction ?',
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-danger',
            default: "true",
        },
        cancel: {
            label: "Cancel",
            className: "btn-success",
        },
    },
    callback: function (result) {
        if (result) {
            let url = base_url + "/master/delete";
            let data = { _token: token, id: id };
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function() {
                    $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                    $("#deleteButton_" + id).attr("disabled", true);
                    $("#deleteButton_" + id).removeClass("fa fa-trash");
                    $("#deleteButton_" + id).addClass("fa fa-spinner fa-spin");
            
            
                },
                success: function(data) {
                    $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                    toastr.success('Deleted.');
                    searcIntraction();
                },
                complete: function() {
            
                    $("#deleteButton_" + id).attr("disabled", false);
                    $("#deleteButton_" + id).removeClass("fa fa-spinner fa-spin");
                    $("#deleteButton_" + id).addClass("fa fa-trash");
            
                },
            
            });
        }
    }
});

}
function genericName1Search(obj, e, type = 0) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(e.keyCode);
    if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
        var product = $(obj).val();
        if($(obj).val().trim()==''){
            $('#generic_name1div').hide();
            return;
       }
        if (product == "") {
            $("#generic_name1div").html("");
        } else {
            var url = $('#base_url').val()+"/master/ajaxSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'generic_name1=' + product,
                beforeSend: function() {
                    $("#generic_name1div").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function(html) {
                    $("#generic_name1div").html(html).show();
                    $("#generic_name1div").find('li').first().addClass('liHover');
                },
                complete: function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown('generic_name1div', e);
    }
}

function genericName2Search(obj, e, type = 0) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(e.keyCode);
    if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
        var sd_product = $(obj).val();
        if($(obj).val().trim()==''){
             $('#generic_name2div').hide();
             return;
        }
        if (sd_product == "") {
            $("#generic_name2div").html("");
        } else {
            var url = $('#base_url').val()+"/master/ajaxSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'generic_name2=' + sd_product,
                beforeSend: function() {
                    $("#generic_name2div").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function(html) {
                    $("#generic_name2div").html(html).show();
                    $("#generic_name2div").find('li').first().addClass('liHover');
                },
                complete: function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown('generic_name2div', e);
    }
}

function fillProductStockDetails2(e, code, name, id) {
    if(id== $("#generic_name_id1").val()){
        toastr.warning('Generic Names cannot be the same.');
    }else{
        $('#generic_name2').val(name);
        $("#generic_name_id2").val(id);
        $(".ajaxSearchBox").hide();
    }
   

}
function fillProductStockDetails3(e, code, name, id) {
   
        $('#generic_name').val(name);
        $("#generic_name_id").val(id);
        $(".ajaxSearchBox").hide();
   

}
function genericNameSearch(obj, e, type = 0) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(e.keyCode);
    if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
        var sd_product = $(obj).val();
        if($(obj).val().trim()==''){
             $('#generic_namediv').hide();
             $('#generic_name_id').val('');
             return;
        }
        if (sd_product == "") {
            $("#generic_namediv").html("");
        } else {
            var url = $('#base_url').val()+"/master/ajaxSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'generic_name=' + sd_product,
                beforeSend: function() {
                    $("#generic_namediv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function(html) {
                    $("#generic_namediv").html(html).show();
                    $("#generic_namediv").find('li').first().addClass('liHover');
                },
                complete: function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown('generic_namediv', e);
    }
}

function fillProductStockDetails1(e, code, name, id) {
    if(id==$("#generic_name_id2").val()){
        toastr.warning('Generic Names cannot be the same.');
    }else{
        $('#generic_name1').val(name);
        $("#generic_name_id1").val(id);
        $(".ajaxSearchBox").hide();
    }
   
}

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var item_list = $("#" + ajax_div).find('li');
    var selected = item_list.filter('.liHover');
    if (event.keyCode === 13){
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    item_list.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current_item = item_list.eq(0);
        } else {
            current_item = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current_item = item_list.last();
        } else {
            current_item = selected.prev();
        }
    }
    current_item.addClass('liHover');
}
function saveIntractions() {
    var base_url = $("#base_url").val();
    var token = $("#token_hiddendata").val();
    var edit_id = $("#edit_id").val();
    var generic_name1=$('#generic_name_id1').val();
    var generic_name2=$('#generic_name_id2').val();
    if(generic_name1 && generic_name2){
        var severity=$('#severity_id').val();
       if(severity){
           var action_recommender=$('#action_recommender').val().trim();
           if(action_recommender){
           var interaction=$('#intraction').val().trim();
           if(interaction){
            var url = base_url + "/master/saveIntractions";
            var params = {
                _token: token,
                edit_id:edit_id,
                generic_name1:generic_name1,
                generic_name2:generic_name2,
                severity:severity,
                action_recommender:action_recommender,
                interaction:interaction

            };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function() {
                    $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    $("#savebtnBtn").attr("disabled", true);
                    $("#savebtnSpin").removeClass("fa fa-save");
                    $("#savebtnSpin").addClass("fa fa-spinner fa-spin");
                },
                success: function(data) {
                    if (data) {
                        if(data.status==1){
                            toastr.success("Saved.");
                            formReset();
                            searcIntraction();
                        }else if(data.status==2){
                            toastr.success("Updated.");
                            formReset();
                            searcIntraction();
                        }else if(data.status==3){
                            toastr.warning("Same interaction already exist.");
                        }else{
                            toastr.error("Some thing went wrong..");
                        }
                      
                       
        
                    }
                },
                complete: function() {
                    $("#savebtnBtn").attr("disabled", false);
                    $("#savebtnSpin").removeClass("fa fa-spinner fa-spin");
                    $("#savebtnSpin").addClass("fa fa-save");
                    $("#searchDataDiv").LoadingOverlay("hide");

        
                },
                warning: function() {
                    toastr.error("Error Please Check Your connection ");
                },
            });
           }else{
            toastr.warning('Interaction is required.');
           }
           }else{
            toastr.warning('Action Recommended is required.');
           }
       }else{
        toastr.warning('Please select a severity.');
       }
    }else{
        toastr.warning('Please add both generic names.');
    }

   
}