
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
   
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_bed_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
      
        var data = {};
        data.room_type = $("#room_type").val();
        data.bed_name = $("#bed_name").val();
        data.ward = $("#ward").val();
        data.nurse_loc = $("#nurse_loc").val();
        data.bed_status = $(".bed_status").val();
        data.status = $(".status").val();

        if ($("#status").is(":checked")) {
            data.status =1;
        }else{
            data.status =0;
        }
      
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".bed_name_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Bed Name");
            return;
        }
        if($("#nurse_loc_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Nursing Location");
            return;
        }
        if($("#room_type_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Room Type");
            return;
        }
        if($("#ward_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Ward");
            return;
        }
        if($("#bed_status_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Bed Status");
            return;
        }
        if($("#description").val().trim() == ''){
            Command: toastr["error"]("Please enter Description");
            return;
        }
        if($("#number_of_occupancy").val().trim() == ''){
            Command: toastr["error"]("Please enter No Of Occupancy");
            return;
        }
        if($("#floor").val().trim() == ''){
            Command: toastr["error"]("Please enter Floor");
            return;
        }
        data.bed_id = $(".reference_id").val();
        data.room_type = $("#room_type_add").val();
        data.bed_name = $("#bed_name_add").val();
        data.ward = $("#ward_add").val();
        data.nurse_loc = $("#nurse_loc_add").val();
        data.bed_status = $(".bed_status_add").val();
        data.description = $(".description").val();
        data.no_of_occupancy = $(".number_of_occupancy").val();
        data.is_multiple = $(".is_multiple").val();
        data.allow_retain = $(".allow_retain").val();
        data.dummy_bed = $(".dummy_bed").val();
        data.er_bed = $(".er_bed").val();
        data.consider_occupancy = $(".consider_occupancy").val();
        data.building_floor = $(".floor").val();
        data.status = $(".status_add").val();
       
        if($(".status_add").prop('checked') == true){
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".is_multiple").prop('checked') == true){
            data.is_multiple ='t';
        }else{
            data.is_multiple ='f';
        }
        if($(".allow_retain").prop('checked') == true){
            data.allow_retain =1;
        }else{
            data.allow_retain =0;
        }
        if($(".dummy_bed").prop('checked') == true){
            data.dummy_bed =1;
        }else{
            data.dummy_bed =0;
        }
        if($(".er_bed").prop('checked') == true){
            data.er_bed =1;
        }else{
            data.er_bed =0;
        }
        if($(".consider_occupancy").prop('checked') == true){
            data.consider_occupancy =1;
        }else{
            data.consider_occupancy =0;
        }
      console.log(data);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Bed Name Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }else if(data==2){
                        Command: toastr["error"]("Bed Status Cant Be Edited!");
                        $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                        $('.saveButton').attr('disabled', false);
                    }
                   else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
   
    function editItem(element){
        let bed_status = $(element).parents('tr').attr('data-bed_status');

        if(bed_status==2){
            Command: toastr["error"]("Bed Status is Reserved,It Cant Be Edited!");
        
        }else if(bed_status==3){
            Command: toastr["error"]("Bed Status is Occupied,It Cant Be Edited!");
        
        }else if(bed_status==4){
            Command: toastr["error"]("Bed Status is Vacant,It Cant Be Edited!");
        
        }else if(bed_status==5){
            Command: toastr["error"]("Bed Status is Blocked,It Cant Be Edited!");
        
        }else if(bed_status==7){
            Command: toastr["error"]("Bed Status is Awaiting Checkout,It Cant Be Edited!");
        
        }else{
        let bed_id = $(element).parents('tr').attr('data-id');
        let room_type_name = $(element).parents('tr').attr('data-room_type_name');
        let bed_name = $(element).parents('tr').attr('data-bed_name');
        let ward = $(element).parents('tr').attr('data-ward_name');
        let nurse_loc = $(element).parents('tr').attr('data-nurse_loc');
        let bed_status = $(element).parents('tr').attr('data-bed_status');
        let description = $(element).parents('tr').attr('data-description');
        let no_of_occupancy = $(element).parents('tr').attr('data-number_of_occupancy');
        let building_floor = $(element).parents('tr').attr('data-floor_name');
        let is_multiple = $(element).parents('tr').attr('data-multiple');
        let allow_retain = $(element).parents('tr').attr('data-retain');
        let dummy_bed = $(element).parents('tr').attr('data-dummy_bed');
        let er_bed = $(element).parents('tr').attr('data-er_bed');
        let consider_occupancy = $(element).parents('tr').attr('data-occupancy');
        let status = $(element).parents('tr').attr('data-status');
      
        if(status==1){
             $(".status_add").prop("checked", true);

        } else{
            $(".status_add").prop("checked", false);
              }
        if(is_multiple==1){
            $(".is_multiple").prop("checked", true);
             
        } else{
            $(".is_multiple").prop("checked", false);
              }
        if(allow_retain==1){
            $(".allow_retain").prop("checked", true);
        } else{
            $(".allow_retain").prop("checked", false);
             }
        if(dummy_bed==1){
            $(".dummy_bed").prop("checked", true);
            
        } else{
           $(".dummy_bed").prop("checked", false);
             } 
        if(er_bed==1){
           $(".er_bed").prop("checked", true);
           
        } else{
           $(".er_bed").prop("checked", false);
            } 
        if(consider_occupancy==1){
           $(".consider_occupancy").prop("checked", true);
            
        } else{
           $(".consider_occupancy").prop("checked", false);
             }                    
        $(".reference_id").val(bed_id);
        $("#room_type_add").val(room_type_name);
        $("#room_type_add").select2().trigger('change');   
        $("#bed_name_add").val(bed_name);
        $("#ward_add").val(ward);
        $("#ward_add").select2().trigger('change');  
        $("#nurse_loc_add").val(nurse_loc);
        $("#nurse_loc_add").select2().trigger('change'); 
        $(".bed_status_add").val(bed_status);
        $(".description").val(description);
        $(".number_of_occupancy").val(no_of_occupancy);
        $(".floor").val(building_floor);
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $("#add").html('Edit Bed');
      }

    }
    function deleteItem(element){

        let confirm = window.confirm('Are you sure want to delete ?');
        if(confirm){
            let data = {};
            let url = $(".table_body_contents").attr('data-delete-url');
            let bed_id = $(element).parents('tr').attr('data-id');
            let bed_status = $(element).parents('tr').attr('data-bed_status');

            data.bed_id = bed_id;
            data.bed_status = bed_status;
          if(data.bed_status==2){
              Command: toastr["error"]("Bed Status is Reserved,It Cant Be Deleted!");
          
          }else if(data.bed_status==3){
              Command: toastr["error"]("Bed Status is Occupied,It Cant Be Deleted!");
          
          }else if(data.bed_status==4){
              Command: toastr["error"]("Bed Status is Vacant,It Cant Be Deleted!");
          
          }else if(data.bed_status==5){
              Command: toastr["error"]("Bed Status is Blocked,It Cant Be Deleted!");
          
          }else if(data.bed_status==7){
              Command: toastr["error"]("Bed Status is Awaiting Checkout,It Cant Be Deleted!");
          
          }
          else{
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function () {
                    $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                },
                success: function (data) {  
                    if(data==1){
                        Command: toastr["error"]("Bed Status Cant Be Deleted!");
                        $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                        $('.deleteButton').attr('disabled', false);
        
                        }
                    $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                       
                },
                complete: function () { 

                }
            });
        }
        }
        
    }
  
    function resetForm(){
        $(".reference_id").val('');
        $(".bed_name_add").val('');
        $(".bed_status_add").val('');
        $("#room_type_add").val('').trigger('change');
        $("#ward_add").val('').trigger('change');
        $("#nurse_loc_add").val('').trigger('change');
        $(".floor").val('');
        $(".description").val('');
        $(".number_of_occupancy").val('');
        $("#add").html('Add Bed');
        $('#status_add').removeAttr('checked');
        $('#is_multiple').removeAttr('checked');
        $('#allow_retain').removeAttr('checked');
        $('#dummy_bed').removeAttr('checked');
        $('#er_bed').removeAttr('checked');
        $('#consider_occupancy').removeAttr('checked');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

