$(document).ready(function() {
    searchMenu();
    $(".select2").select2();
});

function searchMenu() {
    var menu = $("#search_menu").val();
    var base_url = $("#base_url").val();
    var parent_menu = $("#parent_menu").val();
    var token = $("#token_hiddendata").val();
    var param = { _token: token, menu: menu, parent_menu: parent_menu };

    var url = base_url + "/admin/searchmenu";

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchBtn").attr("disabled", true);
            $("#searchSpin").removeClass("fa fa-search");
            $("#searchSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {

            $("#searchDataDiv").html(data);

            // formReset();
        },
        complete: function() {
            $("#searchBtn").attr("disabled", false);
            $("#searchSpin").removeClass("fa fa-spinner fa-spin");
            $("#searchSpin").addClass("fa fa-search");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function formReset() {
    $("#search_menu").val("").select2();
    $("#parent_menu").val("");
    $("#menu_name").val("");
    $("#parent_id").val('').select2();
    $("#sub_id").val('').select2();
    $("#menu_url").val("");
    $("#route").val("");
    $("#order").val("");
    $("#level").val("");
    $("#status").val("");
    $("#icon").val("");
    $("#hidden_id").val("");

    $("#savebtnBtn").html('<i class="fa fa-save"></i> Save');
    $('#desktop_menu').prop('checked', false);

}

function savemenu() {
    var base_url = $("#base_url").val();
    var token = $("#token_hiddendata").val();
    var menu_name = $("#menu_name").val();
    var del_id = $("#hidden_id").val();

    var checkbox = $('#desktop_menu').is(":checked");

    if (checkbox == true) {

        var is_desktop_menu = 0;
    }
    if (checkbox == false) {
        var is_desktop_menu = 1;
    }

    console.log(del_id);

    if (menu_name == '') {
        toastr.warning("Please Enter Menu Name");
        return;
    }
    var parent_id = $("#parent_id").val();

    var sub_id = $("#sub_id").val();
    var menu_url = $("#menu_url").val();
    var route = $("#route").val();

    var order = $("#order").val();
    if (order == '') {
        toastr.warning("Please Enter order");
        return;
    }
    var level = $("#level").val();
    var status = $("#status").val();
    if (status == '') {
        toastr.warning("Please Enter Status");
        return;
    }
    var icon = $("#icon").val();



    var url = base_url + "/admin/savemenu";
    var params = {
        _token: token,
        is_desktop_menu: is_desktop_menu,
        menu_name: menu_name,
        parent_id: parent_id,
        menu_url: menu_url,
        sub_id: sub_id,
        route: route,
        order: order,
        level: level,
        status: status,
        icon: icon,
        del_id: del_id
    };

    console.log(params);
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function() {
            $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $("#savebtnBtn").attr("disabled", true);
            $("#savebtnSpin").removeClass("fa fa-save");
            $("#savebtnSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {
            if (data) {
                $("#searchDataDiv").LoadingOverlay("hide");
                toastr.success("Save Successfully");
                formReset();
                searchMenu();

            }
        },
        complete: function() {
            $("#savebtnBtn").attr("disabled", false);
            $("#savebtnSpin").removeClass("fa fa-spinner fa-spin");
            $("#savebtnSpin").addClass("fa fa-save");
            $('#desktop_menu').prop('checked', false);

        },
        warning: function() {
            toastr.error("Error Please Check Your connection ");
        },
    });
}

function getSubMenu() {
    $('#sub_menu_id').css('display', 'block');
    var base_url = $("#base_url").val();
    var sub_id = $("#parent_id").val();
    var params = { sub_id: sub_id };
    var url = base_url + "/admin/submenuautofill";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function() {

            $("#sub_id").empty();

        },
        success: function(data) {
            if (data) {
                $("#sub_id").html('<option value="">Select Sub Menu</option>');
                $.each(data, function(key, value) {
                    ($("#sub_id").append('<option value="' + value.id + '">' + value.name + '</option>'));
                })

            }

        },


    });
}


function editItem(id) {
    var base_url = $("#base_url").val();
    var token = $("#token_hiddendata").val();
    var url = base_url + "/admin/editMenu";
    var id = id;
    $('#hidden_id').val(id);
    var params = {
        _token: token,
        id: id,

    }


    $.ajax({
        type: "post",
        url: url,
        data: params,

        beforeSend: function() {
            $("#editButton_" + id).attr("disabled", true);
            $("#editButton_" + id).removeClass("fa fa-edit");
            $("#editButton_" + id).addClass("fa fa-spinner fa-spin");

        },

        success: function(data) {


            var obj = JSON.parse(data);

            $('#menu_name').val(obj[0].name);
            $('#parent_id').val(obj[0].parent_name).select2();
            $('#menu_url').val(obj[0].url);
            $('#route').val(obj[0].route);
            $('#icon').val(obj[0].menu_icon);
            $('#order').val(obj[0].order);
            $('#status').val(obj[0].status_id);
            var checkbox = obj[0].desktop_menu;

            if (checkbox == 0) {
                $('#desktop_menu').prop('checked', true);
            }

            if (checkbox == 1) {
                $('#desktop_menu').prop('checked', false);
            }


        },
        complete: function() {

            $("#editButton_" + id).attr("disabled", false);
            $("#editButton_" + id).removeClass("fa fa-spinner fa-spin");
            $("#editButton_" + id).addClass("fa fa-edit");


        }

    });

    $("#savebtnBtn").html('<i class="fa fa-save"></i> Update');

}

function delete_menu(id) {
    var base_url = $("#base_url").val();
    var token = $("#token_hiddendata").val();
    let confirm = window.confirm('Are you sure want to delete ?');
    if (confirm) {
        let url = base_url + "/admin/deleteMenu";
        let menu_id = id;
        let data = { _token: token, menu_id: menu_id };

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function() {
                $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                $("#deleteButton_" + id).attr("disabled", true);
                $("#deleteButton_" + id).removeClass("fa fa-trash");
                $("#deleteButton_" + id).addClass("fa fa-spinner fa-spin");


            },
            success: function(data) {
                $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                toastr.success('successfully Deleted');
                searchMenu();
            },
            complete: function() {

                $("#deleteButton_" + id).attr("disabled", false);
                $("#deleteButton_" + id).removeClass("fa fa-spinner fa-spin");
                $("#deleteButton_" + id).addClass("fa fa-trash");

            },

        });
    }

}