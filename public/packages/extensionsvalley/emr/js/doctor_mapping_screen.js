$(document).ready(function() {
    $(".select2").select2();
    getAllDoctors();
    getAllAssessments();
});

var editable_array = [];
function getAllDoctors() {
    var token = $("#c_token").val();
    var url = $("#base_url").val() + "/emr/getAllDoctors";
    var param = {
        _token: token
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchDataDiv_body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(html) {
            $("#searchDataDiv").html(html);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function() {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function() {
            $("#searchDataDiv_body").LoadingOverlay("hide");
        }
    });
}
function getAllAssessments(type=0) {
    var token = $("#c_token").val();
    var url = $("#base_url").val() + "/emr/getAllAssessments";
    var param = {
        _token: token
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            if(type==0){
                $("#assessment_list_body").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: "#009869"
                });
            }
           
        },
        success: function(html) {
            $("#assesment_list").html(html);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function() {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function() {
            if(type==0){
                $("#assessment_list_body").LoadingOverlay("hide");
            }
           
        }
    });
}

function searchDivAssessment2() {
    input = $('#Search_assessment2').val();
    if(input){
        filter = input.toUpperCase();
        var length = document.getElementsByClassName("assessment_search2").length;
    
        for (i = 0; i < length; i++) {
            if (
                document
                    .getElementsByClassName("assessment_search2")
                    [i].innerHTML.toUpperCase()
                    .indexOf(filter) > -1
            ) {
                document.getElementsByClassName("assessment_search2")[
                    i
                ].style.display = "block";
            } else {
                document.getElementsByClassName("assessment_search2")[
                    i
                ].style.display = "none";
            }
        }
    }
    
}
function searchDivAssessment() {
    input = document.getElementById("Search_assessment");
    filter = input.value.toUpperCase();
    var length = document.getElementsByClassName("assessment_search").length;

    for (i = 0; i < length; i++) {
        if (
            document
                .getElementsByClassName("assessment_search")
                [i].innerHTML.toUpperCase()
                .indexOf(filter) > -1
        ) {
            document.getElementsByClassName("assessment_search")[
                i
            ].style.display = "block";
        } else {
            document.getElementsByClassName("assessment_search")[
                i
            ].style.display = "none";
        }
    }
}
function searchDivUser() {
    $('#doc_active').prop('checked',false);
    $('#doc_all').prop('checked',true);
    input = document.getElementById("Search_usr");
    filter = input.value.toUpperCase();
    var length = document.getElementsByClassName("search_user").length;

    for (i = 0; i < length; i++) {
        if (
            document
                .getElementsByClassName("search_user")
                [i].innerHTML.toUpperCase()
                .indexOf(filter) > -1
        ) {
            document.getElementsByClassName("search_user")[i].style.display =
                "block";
        } else {
            document.getElementsByClassName("search_user")[i].style.display =
                "none";
        }
    }
}

function saveDoctorMappedDetails() {
    var assessment_data = [];
    $(".save_assessment_check").each(function() {
        if ($(this).is(":checked")) {
            assessment_data.push($(this).val());
        }
    });
    var save_doc = $("#save_doc").val();
    var string_assessment_data = JSON.stringify(assessment_data);
    if (save_doc) {
        if (assessment_data.sort().toString() != editable_array.sort().toString()) {
            if (string_assessment_data.length != 0) {
                var token = $("#c_token").val();
                var url = $("#base_url").val() + "/emr/saveDoctorMappedDetails";
                var param = {
                    _token: token,
                    save_doc: save_doc,
                    string_assessment_data: string_assessment_data
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#saveUserMappedDetails").attr("disabled", true);
                        $("#saveUserMappedDetailsSpin").removeClass(
                            "fa fa-save"
                        );
                        $("#saveUserMappedDetailsSpin").addClass(
                            "fa fa-spinner fa-spin"
                        );
                    },
                    success: function(html) {
                        if (parseInt(html.status) == 1) {
                            toastr.success("Details added successfully.");
                            $(".search_user").removeClass("tr_active");
                            resetSettingDiv();
                        } else {
                            toastr.warning("Sorry,something went wrong.");
                        }
                    },
                    complete: function() {
                        $("#saveUserMappedDetails").attr("disabled", false);
                        $("#saveUserMappedDetailsSpin").removeClass(
                            "fa fa-spinner fa-spin"
                        );
                        $("#saveUserMappedDetailsSpin").addClass("fa fa-save");
                    }
                });
            } else {
                toastr.warning("Nothing to save.");
            }
        } else {
            toastr.warning("No change found.");
        }
    } else {
      
        toastr.warning("Please select a doctor.");
    }
}

function resetSettingDiv() {
    $(".resetMe").prop("checked", false);
    $("#this_is_head").html('Not Selected');
    $("#save_doc").val('');
    $(".search_user").removeClass('tr_active');

}

function docDetailsEdit(doc_id) {
    $(".resetMe").prop("checked", false);
    var token = $("#c_token").val();
    var url = $("#base_url").val() + "/emr/docDetailsEdit";
    var param = {
        _token: token,
        doc_id: doc_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#usrDetailsEdit" + doc_id).attr("disabled", true);
            $("#usrDetailsSpin" + doc_id).removeClass("fa fa-edit");
            $("#usrDetailsSpin" + doc_id).addClass("fa fa-spinner fa-spin");
        },
        success: function(html) {
           
            $(".search_user").removeClass("tr_active");
            $("#save_doc").val(doc_id);
            $("#doctorTr" + doc_id).addClass("tr_active");

            obj = JSON.parse(html);
            console.log(obj);
            editable_array = obj.assessment_details;
            $("#this_is_head").html(obj.doc_name);
            if (obj.assessment_details) {
                obj.assessment_details.forEach(function(res) {
                    var tile = $("#assessment_set" + res).html();
                    var setable =
                        '<div class="col-md-12 padding_sm assessment_search" id="assessment_set' +
                        res +
                        '" style="border: 1px solid #a52a2a59;margin-bottom: 4px;padding: 5px !important;">' +
                        tile +
                        "</div>";
                    $("#assessment_set" + res).remove();
                    $("#assesment_list_init").prepend(setable);
                    $("#rec_checkbox" + res).prop("checked", true);
                });
            }
        },
        complete: function() {
            $("#usrDetailsEdit" + doc_id).attr("disabled", false);
            $("#usrDetailsSpin" + doc_id).removeClass("fa fa-spinner fa-spin");
            $("#usrDetailsSpin" + doc_id).addClass("fa fa-edit");
        }
    });
}

function docDetailsDelete(doc_id) {
    bootbox.confirm({
        message: "Are you sure, you want to Delete ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success",
                default: "true"
            },
            cancel: {
                label: "No",
                className: "btn-warning"
            }
        },
        callback: function(result) {
            if (result) {
                var token = $("#c_token").val();
                var url = $("#base_url").val() + "/emr/docDetailsDelete";
                var param = {
                    _token: token,
                    doc_id: doc_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#usrDetailsDelete" + doc_id).attr("disabled", true);
                        $("#usrDetailsDelSpin" + doc_id).removeClass(
                            "fa fa-trash"
                        );
                        $("#usrDetailsDelSpin" + doc_id).addClass(
                            "fa fa-spinner fa-spin"
                        );
                    },
                    success: function(html) {
                        if (html) {
                            toastr.success("Deleted.");
                            resetSettingDiv();
                            getAllDoctors();
                        }
                    },
                    complete: function() {
                        $("#usrDetailsDelete" + doc_id).attr("disabled", false);
                        $("#usrDetailsDelSpin" + doc_id).removeClass(
                            "fa fa-spinner fa-spin"
                        );
                        $("#usrDetailsDelSpin" + doc_id).addClass(
                            "fa fa-trash"
                        );
                    }
                });
            }
        }
    });
}

$(document).on("change", "#doc_active", function() {
    setDoctorList();
    resetSettingDiv();
    $('#save_doc').val('');
});
$(document).on("change", "#doc_all", function() {
    resetSettingDiv();
    $('#save_doc').val('');

    if($('#doc_all').is(':checked')){
        $('#doc_active').prop('checked',false);
        $(".isActive").show();
        $(".isInActive").show(); 
    }else{
        $('#doc_active').prop('checked',true);
        $(".isActive").show();
        $(".isInActive").hide();
    }
  
});
function setDoctorList() {
     $('#doc_all').prop('checked',false);
    if ($("#doc_active").is(":checked")) {
        $(".isActive").show();
        $(".isInActive").hide();
    } else {
        $(".isActive").hide();
        $(".isInActive").show();
    }
}
function showInputBox(){
    $('#add_new_assesment').hide();
    $('#add_new_assesment_input').show();
 }
 function hideInputBox(){
    $('#add_new_assesment').show();
    $('#add_new_assesment_input').hide();
 }

 function showInputModal(type=0){
    var token = $("#c_token").val();
    var url = $("#base_url").val() + "/emr/getAllAssessmentsToEdit";
    var param = {
        _token: token
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
           
               
                if(type==1){
                    $("#addAssessmentList").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: "#009869"
                    });
                }else{
                    $('#add_btn_spin').removeClass('fa fa-plus');
                    $('#add_btn_spin').addClass('fa fa-spin fa-spinner');
                }
               
            
          
        },
        success: function(html) {
            $("#addAssessmentList").html(html);
            if(type==0){
                $('#addAssessmentModal').modal('show');
            }
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function() {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function() {
            if(type==1){
                $("#addAssessmentList").LoadingOverlay("hide");
            }else{
                $('#add_btn_spin').removeClass('fa fa-spin fa-spinner'); 
                $('#add_btn_spin').addClass('fa fa-plus');
            }
               
            
           
         
        }
    });
    
 }

 function addNewAssessment(){
    if($("#Search_assessment2").val().trim() == ''){
        Command: toastr["error"]("Please Enter Assessment");
        return;
    }
    var token = $("#c_token").val();
    var Search_assessment = $("#Search_assessment2").val();
    var type = $("#type").val();
    var url = $("#base_url").val() + "/emr/addNewAssessment";
    var param = {
        _token: token,
        assessment_name:Search_assessment,type:type
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $('#spin_assessment_add').find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
           $('#spin_assessment_add').attr('disabled', true);
        },
        success: function(html) {
        if(html.status!=1 && html.status!=2 ){
            $("#spin_assessment_add").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
            $('#spin_assessment_add').attr('disabled', false);
            toastr.success('Success.');
            $("#addAssessmentList").html(html);

                showInputModal();
                 $('#Search_assessment2').val('');
                 searchDivAssessment2();
            
       
        }else if(html.status==2){
            toastr.warning('Existing Assessment.');
        }
        else{
            toastr.warning('Sorry,Something went wrong.');
        }
           
           
        },
        complete: function() {
         
        }
    }); 
 }
 function deleteAssessemnt(id){
    bootbox.confirm({
        message: "Are you sure you want to delete this assessment ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-warning",
                default: "true",
            },
            cancel: {
                label: "No",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                var token = $("#c_token").val();
                var url = $("#base_url").val() + "/emr/deleteAssessment";
                var param = {
                    _token: token,
                    assessment_id:id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                       $('#delete_this'+id).attr('disabled',true);
                       $('#delete_spin'+id).removeClass('fa fa-times');
                       $('#delete_spin'+id).addClass('fa fa-spin fa-spinner');
                    },
                    success: function(html) {
                    if(html==1){
                        toastr.success('Deleted.');
                      
                    $("#assessment_set_2"+id).remove();
                    $("#assessment_set"+id).remove();
                    }else{
                        toastr.warning('Sorry,Something went wrong.');
                    }
                       
                       
                    },
                    complete: function() {
                        $('#delete_this'+id).attr('disabled',false);
                        $('#delete_spin'+id).removeClass('fa fa-spin fa-spinner');
                        $('#delete_spin'+id).addClass('fa fa-times');
            
                     
                    }
                });   
            }
        },
    });
 }

