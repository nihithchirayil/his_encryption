$(document).ready(function(){
    $(".select2").select2();

    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });

});

// $(document).on("click", ".ans_chk", function(){
//     $(this).find('input[type="checkbox"]').trigger('click');
// });

//-----time auto increment-------------------------
$(document).on("blur", "#ar_time_1", function(){
    setTimeLine();
});

// function setTimeLine(){
//     var selected_time = $('#ar_time_1').val();
//     selected_time =moment(selected_time, 'h:mm A');
//     var formatted_time  = selected_time.format('HH:mm:ss');
//     var duration = $('#default_time_duration').val();
//     // formatted_time = moment(formatted_time,'HH:mm:ss');
//     var input_conunt = document.getElementsByClassName("ar_time");
//         input_conunt = input_conunt.length;
//     for (let i = 2; i <= input_conunt; i++) {
//         formatted_time = moment(formatted_time,'HH:mm:ss');
//         formatted_time = formatted_time.add(duration, 'minutes');
//         formatted_time = formatted_time.format('hh:mm A');
//         $('#ar_time_'+ i).val(formatted_time);
//     }
// }

function setTimeLine() {
    var selected_time = $('#ar_time_1').val();
    selected_time = moment(selected_time, 'h:mm A');
    var formatted_time = selected_time.format('HH:mm:ss');
    // var duration = $('#default_time_duration').val();
    var duration = 10;
    var input_count = document.getElementsByClassName("ar_time").length;

    for (let i = 2; i <= input_count; i++) {
        selected_time.add(duration, 'minutes');
        formatted_time = selected_time.format('hh:mm A');
        $('#ar_time_'+ i).val(formatted_time);
    }
}

function add_new_o2_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.o2_row').length+1;
    var html = "<tr class='o2_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'> 0<sub>2</sub> </label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='o2_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_o2_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.n2o_row').before(html);
    html = '';

}


function add_new_n2o_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.n2o_row').length+1;
    var html = "<tr class='n2o_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>N<sub>2</sub>o</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='n2o_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_n2o_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.air_row').before(html);
    html = '';

}


function add_new_air_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.air_row').length+1;
    var html = "<tr class='air_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>Air</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='air_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_air_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.induction_row').before(html);
    html = '';

}


function add_new_induction_label_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.induction_row').length+1;
    var html = "<tr class='induction_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>Induction</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='induction_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_induction_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.inhalation_row').before(html);
    html = '';

}

function add_new_inhalation_label_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.inhalation_row').length+1;
    var html = "<tr class='inhalation_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>Inhalation</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='inhalation_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_inhalation_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.relaxant_row').before(html);
    html = '';

}

function add_new_relaxant_label_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.relaxant_row').length+1;
    var html = "<tr class='relaxant_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>Relaxant</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='relaxant_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_relaxant_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.blood_ivf_row').before(html);
    html = '';

}

function add_new_blood_ivf_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.blood_ivf_row').length+1;
    var html = "<tr class='blood_ivf_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>Blood/IVF</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='blood_ivf_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_blood_ivf_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.ans_others_row').before(html);
    html = '';

}

function add_new_ans_others_row(){

    var tbody = document.getElementById("ans_record_table_tbody");
    var numRows = tbody.childElementCount;
    numRows = numRows+1;
    var colum_conunt = $('.time_value_col').length;
    var row_count = $('.ans_others_row').length+1;
    var html = "<tr class='ans_others_row'><td colspan='2' class='common_td_rules' style='min-width: 250px !important;'><label style='width:30%;float:left;clar:right;'>Others</label><input style='width:60%;float:left;clar:right;' type='text' class='form-control' name='ans_others_ratio[]'/><i style='width:10%;float:right;text-align:right;margin-top:5px;' class='fa fa-trash delete-row-btn'></i></td>";

    for(var i=1;i<=colum_conunt;i++){
        html+="<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_ans_others_ratio[]'/></td>";
    }
    html+='</tr>';
    $('.200_row').before(html);
    html = '';

}


//---delete current row--------------------------------
$(document).on("click", ".delete-row-btn", function(){
    $(this).closest('tr').remove();
});


$(document).on("click", ".chart_code_entry", function(){
    $('#chartCodeDataModal').modal('show');
    $(this).addClass('chart_code-active');
    var data_chart_code = $(this).attr('data-chart-code');
    var data_value_chart_code = $(this).attr('data-value-chart-code');
    var data_colum_count = $(this).attr('data-colum-count');
    $('#current_chart_colum_count').val(data_colum_count);
    if(data_value_chart_code !== ''){
        data_value_chart_code = JSON.parse(data_value_chart_code);
    }
    if(data_chart_code !== ''){
        data_chart_code = data_chart_code.split(",");
    }

    $.each(data_chart_code, function(index,value) {
        $("#chk_chart_codes_"+value).prop("checked", true);
        $('#val_chart_codes_'+value).val(data_value_chart_code[value]);
    });

});


function applyChartCodeModal() {
    var chart_value_array = {}; // Using an object to store chart values
    var current_chart_colum_count = $('#current_chart_colum_count').val();
    var chartCodesInColumn = {}; // To track chart codes in the column

    $('.chk_chart_codes').each(function() {
        if ($(this).is(':checked')) {
            var chk_value = $(this).val().trim();
            var text_value = $('#val_chart_codes_' + chk_value).val();

            if (text_value === '') {
                toastr.warning("Enter value!");
                return false;
            } else {
                chart_value_array[chk_value] = text_value;

                // Find all the <td> elements with the same column count
                var updatedColumns = {}; // To track which columns have been updated

                $('td[data-min-range][data-max-range][data-colum-count="' + current_chart_colum_count + '"]').each(function() {
                    var $this = $(this);
                    var minRange = parseInt($this.data('min-range'));
                    var maxRange = parseInt($this.data('max-range'));

                    if (minRange <= text_value && text_value <= maxRange) {
                        // Update the visual representation (html) here
                        var html = "<span title='" + text_value + "' class='badge chk_chart_codes_" + chk_value + "'>&nbsp;</span>";
                        $this.empty().append(html);

                        // Add or update 'data-value-chart-code' attribute for this column
                        var existingChartValues = $this.data('value-chart-code') || {};
                        existingChartValues[chk_value] = text_value;
                        $this.data('value-chart-code', existingChartValues);

                        // Track chart codes in the column
                        chartCodesInColumn[chk_value] = true;

                        // Mark this column as updated
                        updatedColumns[current_chart_colum_count] = true;
                    } else {
                        // Remove the chart code from other rows in the same column
                        if (chartCodesInColumn[chk_value]) {
                            var existingChartValues = $this.data('value-chart-code') || {};
                            delete existingChartValues[chk_value];
                            $this.data('value-chart-code', existingChartValues);
                            // Remove the corresponding HTML element
                            $this.find('.chk_chart_codes_' + chk_value).remove();
                        }
                    }
                });

                // Update the 'data-chart-code' attribute for all rows in the column
                if (updatedColumns[current_chart_colum_count]) {
                    $('td[data-min-range][data-max-range][data-colum-count="' + current_chart_colum_count + '"]').each(function() {
                        var $columnTd = $(this);
                        var existingChartValues = $columnTd.data('value-chart-code') || {};
                        var chartCodeList = Object.keys(existingChartValues).join(',');
                        $columnTd.attr('data-chart-code', chartCodeList);
                    });
                }
            }
        }
    });

    // Convert the chart_value_array to a JSON string
    var chart_value_array_json = JSON.stringify(chart_value_array);

    // Update 'data-value-chart-code' attribute for all relevant '.chart_code-entry' elements
    $('td[data-colum-count="' + current_chart_colum_count + '"]').not('.chart_code-active').attr('data-value-chart-code', function() {
        var currentTdChartValues = $(this).data('value-chart-code') || {};
        return JSON.stringify(currentTdChartValues);
    });

    $('.scrollable').css('overflow-y', 'scroll');
}











// function applyChartCodeModal(){
//     var chart_code_array = [];
//     var chart_value_array = {};
//     var html = '';
//     var chk_value = '';
//     var text_value = '';
//     $('.chk_chart_codes').each(function(){
//         if ($(this).is(':checked')) {
//             chk_value = $(this).val();
//             chk_value = chk_value.trim();
//             chart_code_array.push($(this).val());
//             text_value = $('#val_chart_codes_'+chk_value).val();
//             if(text_value == ''){
//                 toastr.warning("Enter value!");
//                 return false;
//             }
//             chart_value_array[chk_value]=text_value;
//             html+="<span title="+text_value+" class='badge chk_chart_codes_"+chk_value+"'>&nbsp;</span>";
//         }
//     });

//     chart_value_array = JSON.stringify(chart_value_array);
//     $('.chart_code-active').html(html);
//     $('.chart_code-active').attr('data-chart-code',chart_code_array);
//     $('.chart_code-active').attr('data-value-chart-code',chart_value_array);
//     $('.scrollable').css('overflow-y','scroll');
// }








// function applyChartCodeModal() {
//     var chart_code_data = [];
//     var current_chart_colum_count = $('#current_chart_colum_count').val();

//     $('.chk_chart_codes').each(function () {
//       if ($(this).is(':checked')) {
//         var chk_value = $(this).val().trim();
//         var text_value = parseInt($('#val_chart_codes_' + chk_value).val().trim());

//         if (isNaN(text_value) || text_value === '') {
//           toastr.warning('Enter a valid numeric value!');
//           return false;
//         } else {
//           chart_code_data.push({ code: chk_value, value: text_value });
//         }
//       }
//     });

//     chart_code_data.sort(function (a, b) {
//       return a.value - b.value;
//     });

//     $('td[data-colum-count="' + current_chart_colum_count + '"]').each(function () {
//       var min_range = parseInt($(this).data('min-range'));
//       var max_range = parseInt($(this).data('max-range'));
//       var data_value_chart_code = {};

//       for (var i = 0; i < chart_code_data.length; i++) {
//         var code = chart_code_data[i].code;
//         var value = chart_code_data[i].value;
//         if (value >= min_range && value <= max_range) {
//           data_value_chart_code[code] = value;
//         }
//       }

//       $(this).attr('data-value-chart-code', JSON.stringify(data_value_chart_code));
//       $(this).empty();

//       for (var key in data_value_chart_code) {
//         var value = data_value_chart_code[key];
//         var html = '<span title="' + value + '" class="badge chk_chart_codes_' + key + '">&nbsp;</span>';
//         $(this).append(html);
//       }
//     });

//     var chart_code_array = chart_code_data.map(function (entry) {
//       return entry.code;
//     });

//     $('.chart_code-active').attr('data-chart-code', chart_code_array.join(','));

//     $('.scrollable').css('overflow-y', 'scroll');
//   }


function closeChartCodeModal(){
    applyChartCodeModal();
    $('.chart_code_entry').removeClass('chart_code-active');
    $(".chk_chart_codes").prop("checked", false);
    $('#chartCodeDataModal').modal('hide');
    $('.chart_code_reading').val('');
    $('.scrollable').css('overflow-y','scroll');
}


function addNewTimeRow(){
    var total_cols = $('.ar_time').length;
    total_cols = total_cols+1;
    $('.time_row').append("<td class='common_td_rules time_value_col' style='text-align:center;min-width:67px !important;'><input type='text' id='ar_time_"+total_cols+"' name='ar_time[]' class='form-control timepicker ar_time' style='border:none;background-color:cornflowerblue;color:white;curser:pointer;'/></td>");

    $('.o2_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_o2_ratio[]"/></td>')
    });
    $('.n2o_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_n2o_ratio[]"/></td>')
    });
    $('.air_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_air_ratio[]"/></td>')
    });
    $('.induction_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_induction_ratio[]"/></td>')
    });
    $('.inhalation_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_inhalation_ratio[]"/></td>')
    });
    $('.relaxant_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_relaxant_ratio[]"/></td>')
    });
    $('.blood_ivf_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_blood_ivf_ratio[]"/></td>')
    });
    $('.ans_others_row').each(function(){
        $(this).append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_ans_others_ratio[]"/></td>')
    });

    $('.200_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.190_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.180_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.170_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.160_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.150_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.140_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.130_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.120_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.110_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.100_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.90_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.80_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.70_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.60_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.50_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.40_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.30_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.20_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');
    $('.10_row').append('<td class="chart_code_entry" data-value-chart-code ="" data-chart-code="" style="text-align:center;">');

    $('.spontaneous_row').append("<td style='text-align:center;' class='ans_chk'><input type='checkbox' name='chk_spontaneous[]'/></td>");

    $('.assisted_row').append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_assisted[]" /></td>');

    $('.controlled_row').append('<td style="text-align:center;" class="ans_chk"><input type="checkbox" name="chk_controlled[]" /></td>');

    $(".select2").select2();
    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    setTimeLine();
}

function saveAR(){

    var surgery_head_id = $('#ar_surgery_head_id').val();
    var ar_data_array = {};
    var time_array = [];

    var o2_ratio = [];
    var o2_ratio_count = 0;
    var o2_ratio_vals = [[]];

    var n2o_ratio = [];
    var n2o_ratio_count = 0;
    var n2o_ratio_vals = [[]];

    var air_ratio = [];
    var air_ratio_count = 0;
    var air_ratio_vals = [[]];

    var induction_ratio = [];
    var induction_ratio_count = 0;
    var induction_ratio_vals = [[]];

    var inhalation_ratio = [];
    var inhalation_ratio_count = 0;
    var inhalation_ratio_vals = [[]];

    var relaxant_ratio = [];
    var relaxant_ratio_count = 0;
    var relaxant_ratio_vals = [[]];

    var blood_ivf_ratio = [];
    var blood_ivf_ratio_count = 0;
    var blood_ivf_ratio_vals = [[]];

    var ans_others_ratio = [];
    var ans_others_ratio_count = 0;
    var ans_others_ratio_vals = [[]];


    $("input[name='ar_time[]']").each(function(){
        time_array.push($(this).val());
    });

    //----o2---------------------------------------------------
    $("input[name='o2_ratio[]']").each(function(){
        o2_ratio.push($(this).val());
    });

    $('.o2_row').each(function(){
        $(this).find("input[name='chk_o2_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                o2_ratio_vals[o2_ratio_count].push(1);
            }else{
                o2_ratio_vals[o2_ratio_count].push(0);
            }
        });
        o2_ratio_count++;
        o2_ratio_vals.push([]);
    });
    o2_ratio_vals.pop();

    //----n2o---------------------------------------------------
    $("input[name='n2o_ratio[]']").each(function(){
        n2o_ratio.push($(this).val());
    });

    $('.n2o_row').each(function(){
        $(this).find("input[name='chk_n2o_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                n2o_ratio_vals[n2o_ratio_count].push(1);
            }else{
                n2o_ratio_vals[n2o_ratio_count].push(0);
            }
        });
        n2o_ratio_count++;
        n2o_ratio_vals.push([]);
    });
    n2o_ratio_vals.pop();


    // //----air---------------------------------------------------
    $("input[name='air_ratio[]']").each(function(){
        air_ratio.push($(this).val());
    });

    $('.air_row').each(function(){
        $(this).find("input[name='chk_air_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                air_ratio_vals[air_ratio_count].push(1);
            }else{
                air_ratio_vals[air_ratio_count].push(0);
            }
        });
        air_ratio_count++;
        air_ratio_vals.push([]);
    });
    air_ratio_vals.pop();


    // //----induction---------------------------------------------------
    $("input[name='induction_ratio[]']").each(function(){
        induction_ratio.push($(this).val());
    });

    $('.induction_row').each(function(){
        $(this).find("input[name='chk_induction_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                induction_ratio_vals[induction_ratio_count].push(1);
            }else{
                induction_ratio_vals[induction_ratio_count].push(0);
            }
        });
        induction_ratio_count++;
        induction_ratio_vals.push([]);
    });
    induction_ratio_vals.pop();

    //---inhalation----------------------------------------------------------
    $("input[name='inhalation_ratio[]']").each(function(){
        inhalation_ratio.push($(this).val());
    });

    $('.inhalation_row').each(function(){
        $(this).find("input[name='chk_inhalation_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                inhalation_ratio_vals[inhalation_ratio_count].push(1);
            }else{
                inhalation_ratio_vals[inhalation_ratio_count].push(0);
            }
        });
        inhalation_ratio_count++;
        inhalation_ratio_vals.push([]);
    });
    inhalation_ratio_vals.pop();

    //---relaxant----------------------------------------------------------
    $("input[name='relaxant_ratio[]']").each(function(){
        relaxant_ratio.push($(this).val());
    });

    $('.relaxant_row').each(function(){
        $(this).find("input[name='chk_relaxant_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                relaxant_ratio_vals[relaxant_ratio_count].push(1);
            }else{
                relaxant_ratio_vals[relaxant_ratio_count].push(0);
            }
        });
        relaxant_ratio_count++;
        relaxant_ratio_vals.push([]);
    });
    relaxant_ratio_vals.pop();

    //---blood_ivf----------------------------------------------------------
    $("input[name='blood_ivf_ratio[]']").each(function(){
        blood_ivf_ratio.push($(this).val());
    });

    $('.blood_ivf_row').each(function(){
        $(this).find("input[name='chk_blood_ivf_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                blood_ivf_ratio_vals[blood_ivf_ratio_count].push(1);
            }else{
                blood_ivf_ratio_vals[blood_ivf_ratio_count].push(0);
            }
        });
        blood_ivf_ratio_count++;
        blood_ivf_ratio_vals.push([]);
    });
    blood_ivf_ratio_vals.pop();

    //---ans others----------------------------------------------------------
    $("input[name='ans_others_ratio[]']").each(function(){
        ans_others_ratio.push($(this).val());
    });

    $('.ans_others_row').each(function(){
        $(this).find("input[name='chk_ans_others_ratio[]']").each(function(){
            if ($(this).is(':checked')) {
                ans_others_ratio_vals[ans_others_ratio_count].push(1);
            }else{
                ans_others_ratio_vals[ans_others_ratio_count].push(0);
            }
        });
        ans_others_ratio_count++;
        ans_others_ratio_vals.push([]);
    });
    ans_others_ratio_vals.pop();

    //----chart code entry data-------------------------------------
    var chart_code_entry_data = {'200': [],
                                '190': [],
                                '180': [],
                                '170': [],
                                '160': [],
                                '150': [],
                                '140': [],
                                '130': [],
                                '120': [],
                                '110': [],
                                '100': [],
                                '90': [],
                                '80': [],
                                '70': [],
                                '60': [],
                                '50': [],
                                '40': [],
                                '30': [],
                                '20': [],
                                '10': [],
                                };
    $('.200_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['200'].push($(this).attr('data-value-chart-code'));
    });
    $('.190_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['190'].push($(this).attr('data-value-chart-code'));
    });
    $('.180_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['180'].push($(this).attr('data-value-chart-code'));
    });
    $('.170_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['170'].push($(this).attr('data-value-chart-code'));
    });
    $('.160_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['160'].push($(this).attr('data-value-chart-code'));
    });
    $('.150_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['150'].push($(this).attr('data-value-chart-code'));
    });
    $('.140_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['140'].push($(this).attr('data-value-chart-code'));
    });
    $('.130_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['130'].push($(this).attr('data-value-chart-code'));
    });
    $('.120_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['120'].push($(this).attr('data-value-chart-code'));
    });
    $('.110_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['110'].push($(this).attr('data-value-chart-code'));
    });
    $('.100_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['100'].push($(this).attr('data-value-chart-code'));
    });
    $('.90_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['90'].push($(this).attr('data-value-chart-code'));
    });
    $('.80_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['80'].push($(this).attr('data-value-chart-code'));
    });
    $('.70_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['70'].push($(this).attr('data-value-chart-code'));
    });
    $('.60_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['60'].push($(this).attr('data-value-chart-code'));
    });
    $('.50_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['50'].push($(this).attr('data-value-chart-code'));
    });
    $('.40_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['40'].push($(this).attr('data-value-chart-code'));
    });
    $('.30_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['30'].push($(this).attr('data-value-chart-code'));
    });
    $('.20_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['20'].push($(this).attr('data-value-chart-code'));
    });
    $('.10_row').find('.chart_code_entry').each(function(){
        chart_code_entry_data['10'].push($(this).attr('data-value-chart-code'));
    });

    //----chart code data-------------------------------------
    var chart_code_data = {'200': [],
                           '190': [],
                           '180': [],
                           '170': [],
                           '160': [],
                           '150': [],
                           '140': [],
                           '130': [],
                           '120': [],
                           '110': [],
                           '100': [],
                           '90': [],
                           '80': [],
                           '70': [],
                           '60': [],
                           '50': [],
                           '40': [],
                           '30': [],
                           '20': [],
                           '10': [],
                          };
    $('.200_row').find('.chart_code_entry').each(function(){
        chart_code_data['200'].push($(this).attr('data-chart-code'));
    });
    $('.190_row').find('.chart_code_entry').each(function(){
        chart_code_data['190'].push($(this).attr('data-chart-code'));
    });
    $('.180_row').find('.chart_code_entry').each(function(){
        chart_code_data['180'].push($(this).attr('data-chart-code'));
    });
    $('.170_row').find('.chart_code_entry').each(function(){
        chart_code_data['170'].push($(this).attr('data-chart-code'));
    });
    $('.160_row').find('.chart_code_entry').each(function(){
        chart_code_data['160'].push($(this).attr('data-chart-code'));
    });
    $('.150_row').find('.chart_code_entry').each(function(){
        chart_code_data['150'].push($(this).attr('data-chart-code'));
    });
    $('.140_row').find('.chart_code_entry').each(function(){
        chart_code_data['140'].push($(this).attr('data-chart-code'));
    });
    $('.130_row').find('.chart_code_entry').each(function(){
        chart_code_data['130'].push($(this).attr('data-chart-code'));
    });
    $('.120_row').find('.chart_code_entry').each(function(){
        chart_code_data['120'].push($(this).attr('data-chart-code'));
    });
    $('.110_row').find('.chart_code_entry').each(function(){
        chart_code_data['110'].push($(this).attr('data-chart-code'));
    });
    $('.100_row').find('.chart_code_entry').each(function(){
        chart_code_data['100'].push($(this).attr('data-chart-code'));
    });
    $('.90_row').find('.chart_code_entry').each(function(){
        chart_code_data['90'].push($(this).attr('data-chart-code'));
    });
    $('.80_row').find('.chart_code_entry').each(function(){
        chart_code_data['80'].push($(this).attr('data-chart-code'));
    });
    $('.70_row').find('.chart_code_entry').each(function(){
        chart_code_data['70'].push($(this).attr('data-chart-code'));
    });
    $('.60_row').find('.chart_code_entry').each(function(){
        chart_code_data['60'].push($(this).attr('data-chart-code'));
    });
    $('.50_row').find('.chart_code_entry').each(function(){
        chart_code_data['50'].push($(this).attr('data-chart-code'));
    });
    $('.40_row').find('.chart_code_entry').each(function(){
        chart_code_data['40'].push($(this).attr('data-chart-code'));
    });
    $('.30_row').find('.chart_code_entry').each(function(){
        chart_code_data['30'].push($(this).attr('data-chart-code'));
    });
    $('.20_row').find('.chart_code_entry').each(function(){
        chart_code_data['20'].push($(this).attr('data-chart-code'));
    });
    $('.10_row').find('.chart_code_entry').each(function(){
        chart_code_data['10'].push($(this).attr('data-chart-code'));
    });


    //---spontaneous_row-----------------------
    var spontaneous_row = [];
    $('.spontaneous_row').find("input[name='chk_spontaneous[]']").each(function(){
        if ($(this).is(':checked')) {
            spontaneous_row.push(1);
        }else{
            spontaneous_row.push(0);
        }
    });

    //---assisted_row-----------------------
    var assisted_row = [];
    $('.assisted_row').find("input[name='chk_assisted[]']").each(function(){
        if ($(this).is(':checked')) {
            assisted_row.push(1);
        }else{
            assisted_row.push(0);
        }
    });

    //---controlled_row-----------------------
    var controlled_row = [];
    $('.controlled_row').find("input[name='chk_controlled[]']").each(function(){
        if ($(this).is(':checked')) {
            controlled_row.push(1);
        }else{
            controlled_row.push(0);
        }
    });

    //----monitors---------------------------
    var monitor_data = [];
    // $('select[name="monitor"]').each(function(){
    //     monitor_data.push($(this).val());
    // });

    for(var i=1;i<=7;i++){
        if($('#monitor_'+i).is(':checked')){
            monitor_data.push($('#monitor_'+i).val());
        }
    }

    var monitor_description = $('#monitor_description').val();


    var too_depressed =  $('input[name="too_depressed"]:checked').val();
    var worn_off =  $('input[name="worn_off"]:checked').val();
    var pprehensive =  $('input[name="pprehensive"]:checked').val();
    var inadequate =  $('input[name="inadequate"]:checked').val();
    var excitement =  $('input[name="excitement"]:checked').val();
    var laryngospasm =  $('input[name="laryngospasm"]:checked').val();
    var vomiting =  $('input[name="vomiting"]:checked').val();
    var cyanosis =  $('input[name="cyanosis"]:checked').val();
    var maintenance_excitement =  $('input[name="maintenance_excitement"]:checked').val();
    var maintenance_laryngospasm =  $('input[name="maintenance_laryngospasm"]:checked').val();
    var maintenance_sweating =  $('input[name="maintenance_sweating"]:checked').val();
    var maintenance_mucous =  $('input[name="maintenance_mucous"]:checked').val();
    var mask = $('#mask').val();
    var endotrachael_size = $('#endotrachael_size').val();
    var endotrachael_type =  $('input[name="endotrachael_type"]:checked').val();
    var oral =  $('input[name="oral"]:checked').val();
    var tracheostomy =  $('input[name="tracheostomy"]:checked').val();
    var endobrononial =  $('input[name="endobrononial"]:checked').val();
    var cuff =  $('input[name="cuff"]:checked').val();
    var local_spray =  $('input[name="local_spray"]:checked').val();
    var pack =  $('input[name="pack"]:checked').val();
    var reflex =  $('input[name="reflex"]:checked').val();
    var retching =  $('input[name="retching"]:checked').val();
    var excitement =  $('input[name="excitement"]:checked').val();
    var recovery_vomiting =  $('input[name="recovery_vomiting"]:checked').val();
    var recovery_bp = $('#recovery_bp').val();
    var recovery_pulse =  $('#recovery_pulse').val();
    var recovery_spo2 =  $('#recovery_spo2').val();
    var recovery_etco2 =  $('#recovery_etco2').val();
    var estimated_blood_loss =  $('#estimated_blood_loss').val();
    var total_fluid_given_in_or =  $('#total_fluid_given_in_or').val();
    var recovery_blood =  $('#recovery_blood').val();
    var recovery_other =  $('#recovery_other').val();
    var ans_position =  $('#ans_position').val();
    var ans_start_time =  $('#ans_start_time').val();
    var ans_end_time =  $('#ans_end_time').val();
    var surgery_start_time =  $('#surgery_start_time').val();
    var surgery_end_time =  $('#surgery_end_time').val();

    var adequate_resp_efforts =  $('input[name="adequate_resp_efforts"]:checked').val();
    var eye_opening =  $('input[name="eye_opening"]:checked').val();
    var obeying_commands =  $('input[name="obeying_commands"]:checked').val();
    var sustained_head_lift =  $('input[name="sustained_head_lift"]:checked').val();
    var tongue_protrusion =  $('input[name="tongue_protrusion"]:checked').val();
    var airway_reflexes =  $('input[name="airway_reflexes"]:checked').val();
    var after_giving_thorough =  $('input[name="after_giving_thorough"]:checked').val();

    var post_extubation_pr = $('#post_extubation_pr').val();
    var post_extubation_rr = $('#post_extubation_rr').val();
    var post_extubation_spo2 = $('#post_extubation_spo2').val();
    var post_extubation_bp = $('#post_extubation_bp').val();

    var chk_time_of_completion = $('#chk_time_of_completion').val();
    var chk_time_of_extubation = $('#chk_time_of_extubation').val();

    var pre_op_diaganosis = $('#pre_op_diaganosis').val();
    var operation_done = $('#operation_done').val();
    var premedication = $('#premedication').val();
    var ar_scheduled_at = $('#ar_scheduled_at').val();
    var asa_grade = $('#asa_grade').val();
    var ans_technique = $('#ans_technique').val();



    ar_data_array.too_depressed = too_depressed;
    ar_data_array.worn_off = worn_off;
    ar_data_array.pprehensive = pprehensive;
    ar_data_array.inadequate = inadequate;
    ar_data_array.excitement = excitement;
    ar_data_array.laryngospasm = laryngospasm;
    ar_data_array.vomiting = vomiting;
    ar_data_array.cyanosis = cyanosis;
    ar_data_array.maintenance_excitement = maintenance_excitement;
    ar_data_array.maintenance_laryngospasm = maintenance_laryngospasm;
    ar_data_array.maintenance_sweating = maintenance_sweating;
    ar_data_array.maintenance_mucous = maintenance_mucous;
    ar_data_array.mask = mask;
    ar_data_array.endotrachael_size = endotrachael_size;
    ar_data_array.endotrachael_type = endotrachael_type;
    ar_data_array.oral = oral;
    ar_data_array.tracheostomy = tracheostomy;
    ar_data_array.endobrononial = endobrononial;
    ar_data_array.cuff = cuff;
    ar_data_array.local_spray = local_spray;
    ar_data_array.pack = pack;
    ar_data_array.reflex = reflex;
    ar_data_array.retching = retching;
    ar_data_array.excitement = excitement;
    ar_data_array.recovery_vomiting = recovery_vomiting;
    ar_data_array.recovery_bp = recovery_bp;
    ar_data_array.recovery_pulse = recovery_pulse;
    ar_data_array.recovery_spo2 = recovery_spo2;
    ar_data_array.recovery_etco2 = recovery_etco2;
    ar_data_array.estimated_blood_loss = estimated_blood_loss;
    ar_data_array.total_fluid_given_in_or = total_fluid_given_in_or;
    ar_data_array.recovery_blood = recovery_blood;
    ar_data_array.recovery_other = recovery_other;
    ar_data_array.ans_position = ans_position;
    ar_data_array.ans_start_time = ans_start_time;
    ar_data_array.ans_end_time = ans_end_time;
    ar_data_array.surgery_start_time = surgery_start_time;
    ar_data_array.surgery_end_time = surgery_end_time;

    ar_data_array.adequate_resp_efforts = adequate_resp_efforts;
    ar_data_array.eye_opening = eye_opening;
    ar_data_array.obeying_commands = obeying_commands;
    ar_data_array.sustained_head_lift = sustained_head_lift;
    ar_data_array.tongue_protrusion = tongue_protrusion;
    ar_data_array.airway_reflexes = airway_reflexes;
    ar_data_array.after_giving_thorough = after_giving_thorough;

    ar_data_array.post_extubation_pr = post_extubation_pr;
    ar_data_array.post_extubation_rr = post_extubation_rr;
    ar_data_array.post_extubation_spo2 = post_extubation_spo2;
    ar_data_array.post_extubation_bp = post_extubation_bp;
    ar_data_array.chk_time_of_completion = chk_time_of_completion;
    ar_data_array.chk_time_of_extubation = chk_time_of_extubation;

    ar_data_array.time_data = time_array;

    ar_data_array.o2_ratio = o2_ratio;
    ar_data_array.o2_ratio_vals = o2_ratio_vals;

    ar_data_array.n2o_ratio = n2o_ratio;
    ar_data_array.n2o_ratio_vals = n2o_ratio_vals;

    ar_data_array.air_ratio = air_ratio;
    ar_data_array.air_ratio_vals = air_ratio_vals;

    ar_data_array.induction_ratio = induction_ratio;
    ar_data_array.induction_ratio_vals = induction_ratio_vals;

    ar_data_array.inhalation_ratio = inhalation_ratio;
    ar_data_array.inhalation_ratio_vals = inhalation_ratio_vals;

    ar_data_array.relaxant_ratio = relaxant_ratio;
    ar_data_array.relaxant_ratio_vals = relaxant_ratio_vals;

    ar_data_array.blood_ivf_ratio = blood_ivf_ratio;
    ar_data_array.blood_ivf_ratio_vals = blood_ivf_ratio_vals;

    ar_data_array.ans_others_ratio = ans_others_ratio;
    ar_data_array.ans_others_ratio_vals = ans_others_ratio_vals;

    ar_data_array.chart_code_data = chart_code_data;
    ar_data_array.chart_code_entry_data = chart_code_entry_data;
    ar_data_array.spontaneous_row = spontaneous_row;
    ar_data_array.assisted_row = assisted_row;
    ar_data_array.controlled_row = controlled_row;
    ar_data_array.monitor_data = monitor_data;
    ar_data_array.monitor_description = monitor_description;

    ar_data_array.pre_op_diaganosis = pre_op_diaganosis;
    ar_data_array.operation_done = operation_done;
    ar_data_array.premedication = premedication;
    ar_data_array.ar_scheduled_at = ar_scheduled_at;
    ar_data_array.asa_grade = asa_grade;
    ar_data_array.ans_technique = ans_technique;

    ar_data_array = JSON.stringify(ar_data_array);

    var dataparams = {
        ar_data_array:ar_data_array,
        surgery_head_id:surgery_head_id,
    };

    var url = base_url + "/ot_checklists/saveArData";
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#anesthetia_record_checklist').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#anesthetia_record_checklist').LoadingOverlay("hide");
            if(data==1){
                toastr.success("Saved successfully");
            }else{
                toastr.error("Something went wrong..!");
            }
        },
        complete: function () {

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        },
    });

}

function fetchArRecord(head_id){
    var url = base_url + "/ot_checklists/fetchArRecord";
    var dataparams = {head_id:head_id};
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {

        },
        success: function (data) {
           if(data!=0){
            fetchArRecordData(data);
           }
        },
        complete: function () {

        },
    });
}

function fetchArRecordData(data){
    var obj = JSON.parse(data);
    var time_data = obj.time_data;

    var current_time_col_count = $('.ar_time').length;
    var saved_col_count = time_data.length;
    var diff = 0;
    if(saved_col_count>current_time_col_count){
        var diff = saved_col_count-current_time_col_count;
        for(var i=0;i<diff; i++){
            addNewTimeRow();
        }
    }

    var count = 0;
    $('input[name="ar_time[]"]').each(function(){
        $(this).val(time_data[count]);
        count++;
    });


    //----chart codes--------------------------------
    var chart_code_data = obj.chart_code_data;
    var chart_code_entry_data = obj.chart_code_entry_data;
    var chart_code_value = '';
    var chart_code_readings = [200,190,180,170,160,150,140,130,120,110,100,90,80,70,60,50,40,30,20,10];

    for(var j=0;j<chart_code_readings.length;j++){
        var count = 0;
        $('.'+chart_code_readings[j]+'_row').find('.chart_code_entry').each(function(){
            var html = '';
            chart_code_value = chart_code_data[chart_code_readings[j]];
            chart_code_param = chart_code_data[chart_code_readings[j]][count];

            chart_code_entry_value = chart_code_entry_data[chart_code_readings[j]];
            chart_code_entry_param = chart_code_entry_data[chart_code_readings[j]][count];
            if(chart_code_param != null){
                $(this).attr('data-chart-code',chart_code_param);
                $(this).attr('data-value-chart-code',chart_code_entry_param);

                // chart_code_entry_param1 = JSON.stringify(chart_code_entry_param);
                if(chart_code_entry_param !=''){
                    var obj1 =  JSON.parse(chart_code_entry_param);
                    console.log(typeof obj1);
                }


                chart_code_param_array = chart_code_param.split (",");

                for (var i = 0; i < chart_code_param_array.length; i++) {
                    if(chart_code_param_array[i] !=''){
                        var title_value = '';
                        if (typeof obj1 !== 'undefined') {
                            title_value = obj1[chart_code_param_array[i]];
                        }
                        html += "<span title='"+title_value+"' class='badge chk_chart_codes_" + chart_code_param_array[i] + "'>&nbsp;</span>";
                    }
                }
            }
            $(this).html(html);
            count++;
        });
    }


    //---ot ratio---------------------------
    o2_ratio = obj.o2_ratio;
    o2_ratio_length = o2_ratio.length;
    o2_ratio_current_length = $('.o2_row').length;
    var diff = 0;
    if(o2_ratio_length > o2_ratio_current_length){
        var diff = o2_ratio_length - o2_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_o2_row();
        }
    }
    var count = 0;
    $("input[name='o2_ratio[]']").each(function(){
        $(this).val(o2_ratio[count]);
        count++;
    });


    o2_ratio_vals = obj.o2_ratio_vals;
    var count = 0;
    $('.o2_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_o2_ratio[]']").each(function(){
            if(typeof o2_ratio_vals[count][increment] !== 'undefined' && o2_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---n2o ratio---------------------------
    n2o_ratio = obj.n2o_ratio;
    n2o_ratio_length = n2o_ratio.length;
    n2o_ratio_current_length = $('.n2o_row').length;
    var diff = 0;
    if(n2o_ratio_length > n2o_ratio_current_length){
        var diff = n2o_ratio_length - n2o_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_n2o_row();
        }
    }

    var count = 0;
    $("input[name='n2o_ratio[]']").each(function(){
        $(this).val(n2o_ratio[count]);
        count++;
    });

    n2o_ratio_vals = obj.n2o_ratio_vals;
    var count = 0;
    $('.n2o_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_n2o_ratio[]']").each(function(){
            if(typeof n2o_ratio_vals[count][increment] !== 'undefined' && n2o_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---air ratio---------------------------
    air_ratio = obj.air_ratio;
    air_ratio_length = air_ratio.length;
    air_ratio_current_length = $('.air_row').length;
    var diff = 0;
    if(air_ratio_length > air_ratio_current_length){
        var diff = air_ratio_length - air_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_air_row();
        }
    }

    var count = 0;
    $("input[name='air_ratio[]']").each(function(){
        $(this).val(air_ratio[count]);
        count++;
    });

    air_ratio_vals = obj.air_ratio_vals;
    var count = 0;
    $('.air_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_air_ratio[]']").each(function(){
            if(typeof air_ratio_vals[count][increment] !== 'undefined' && air_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---induction ratio---------------------------
    induction_ratio = obj.induction_ratio;
    induction_ratio_length = induction_ratio.length;
    induction_ratio_current_length = $('.induction_row').length;
    var diff = 0;
    if(induction_ratio_length > induction_ratio_current_length){
        var diff = induction_ratio_length - induction_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_induction_row();
        }
    }

    var count = 0;
    $("input[name='induction_ratio[]']").each(function(){
        $(this).val(induction_ratio[count]);
        count++;
    });

    induction_ratio_vals = obj.induction_ratio_vals;
    var count = 0;
    $('.induction_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_induction_ratio[]']").each(function(){
            if(typeof induction_ratio_vals[count][increment] !== 'undefined' && induction_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---inhalation ratio---------------------------
    inhalation_ratio = obj.inhalation_ratio;
    inhalation_ratio_length = inhalation_ratio.length;
    inhalation_ratio_current_length = $('.inhalation_row').length;
    var diff = 0;
    if(inhalation_ratio_length > inhalation_ratio_current_length){
        var diff = inhalation_ratio_length - inhalation_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_inhalation_row();
        }
    }

    var count = 0;
    $("input[name='inhalation_ratio[]']").each(function(){
        $(this).val(inhalation_ratio[count]);
        count++;
    });

    inhalation_ratio_vals = obj.inhalation_ratio_vals;
    var count = 0;
    $('.inhalation_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_inhalation_ratio[]']").each(function(){
            if(typeof inhalation_ratio_vals[count][increment] !== 'undefined' && inhalation_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---relaxant ratio---------------------------
    relaxant_ratio = obj.relaxant_ratio;
    relaxant_ratio_length = relaxant_ratio.length;
    relaxant_ratio_current_length = $('.relaxant_row').length;
    var diff = 0;
    if(relaxant_ratio_length > relaxant_ratio_current_length){
        var diff = relaxant_ratio_length - relaxant_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_relaxant_row();
        }
    }

    var count = 0;
    $("input[name='relaxant_ratio[]']").each(function(){
        $(this).val(relaxant_ratio[count]);
        count++;
    });

    relaxant_ratio_vals = obj.relaxant_ratio_vals;
    var count = 0;
    $('.relaxant_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_relaxant_ratio[]']").each(function(){
            if(typeof relaxant_ratio_vals[count][increment] !== 'undefined' && relaxant_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---blood_ivf ratio---------------------------
    blood_ivf_ratio = obj.blood_ivf_ratio;
    blood_ivf_ratio_length = blood_ivf_ratio.length;
    blood_ivf_ratio_current_length = $('.blood_ivf_row').length;
    var diff = 0;
    if(blood_ivf_ratio_length > blood_ivf_ratio_current_length){
        var diff = blood_ivf_ratio_length - blood_ivf_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_blood_ivf_row();
        }
    }

    var count = 0;
    $("input[name='blood_ivf_ratio[]']").each(function(){
        $(this).val(blood_ivf_ratio[count]);
        count++;
    });

    blood_ivf_ratio_vals = obj.blood_ivf_ratio_vals;
    var count = 0;
    $('.blood_ivf_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_blood_ivf_ratio[]']").each(function(){
            if(typeof blood_ivf_ratio_vals[count][increment] !== 'undefined' && blood_ivf_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---ans_others ratio---------------------------
    ans_others_ratio = obj.ans_others_ratio;
    ans_others_ratio_length = ans_others_ratio.length;
    ans_others_ratio_current_length = $('.ans_others_row').length;
    var diff = 0;
    if(ans_others_ratio_length > ans_others_ratio_current_length){
        var diff = ans_others_ratio_length - ans_others_ratio_current_length;
        for(var i=0;i<diff; i++){
            add_new_ans_others_row();
        }
    }

    var count = 0;
    $("input[name='ans_others_ratio[]']").each(function(){
        $(this).val(ans_others_ratio[count]);
        count++;
    });

    ans_others_ratio_vals = obj.ans_others_ratio_vals;
    var count = 0;
    $('.ans_others_row').each(function(){
        increment = 0;
        $(this).find("input[name='chk_ans_others_ratio[]']").each(function(){
            if(typeof ans_others_ratio_vals[count][increment] !== 'undefined' && ans_others_ratio_vals[count][increment] == 1){
                $(this).attr('checked', 'checked');
            }else{
                $(this).removeAttr('checked');
            }
            increment++;
        });
        count++;
    });


    //---monitor data-------------------
    $('#monitor_description').val(obj.monitor_description);
    var monitor_data = obj.monitor_data;

    for(var i=1;i<=7;i++){
        $('#monitor_'+i).prop('checked', false);
    }
    $.each(monitor_data, function(index, value) {
        $('#monitor_'+value).prop('checked', true);
    });


    var spontaneous_data = obj.spontaneous_row;
    var count= 0;
    $('input[name="chk_spontaneous[]"]').each(function(){
        if(typeof spontaneous_data[count] !== 'undefined' && spontaneous_data[count] == 1){
            $(this).attr('checked', 'checked');
        }else{
            $(this).removeAttr('checked');
        }
        count++;
    });

    var assisted_data = obj.assisted_row;
    var count= 0;
    $('input[name="chk_assisted[]"]').each(function(){
        if(typeof assisted_data[count] !== 'undefined' && assisted_data[count] == 1){
            $(this).attr('checked', 'checked');
        }else{
            $(this).removeAttr('checked');
        }
        count++;
    });

    var controlled_data = obj.controlled_row;
    var count= 0;
    $('input[name="chk_controlled[]"]').each(function(){
        if(typeof controlled_data[count] !== 'undefined' && controlled_data[count] == 1){
            $(this).attr('checked', 'checked');
        }else{
            $(this).removeAttr('checked');
        }
        count++;
    });

    var monitor_data = obj.monitor_data;
    var count= 0;
    $('input[name="chk_monitor[]"]').each(function(){
        if(typeof monitor_data[count] !== 'undefined' && monitor_data[count] == 1){
            $(this).attr('checked', 'checked');
        }else{
            $(this).removeAttr('checked');
        }
        count++;
    });



    if (typeof obj.too_depressed !== 'undefined') {
        $("input[name=too_depressed][value=" + obj.too_depressed + "]").attr('checked', 'checked');
    }
    if (typeof obj.worn_off !== 'undefined') {
        $("input[name=worn_off][value=" + obj.worn_off + "]").attr('checked', 'checked');
    }
    if (typeof obj.pprehensive !== 'undefined') {
        $("input[name=pprehensive][value=" + obj.pprehensive + "]").attr('checked', 'checked');
    }
    if (typeof obj.inadequate !== 'undefined') {
        $("input[name=inadequate][value=" + obj.inadequate + "]").attr('checked', 'checked');
    }
    if (typeof obj.excitement !== 'undefined') {
        $("input[name=excitement][value=" + obj.excitement + "]").attr('checked', 'checked');
    }
    if (typeof obj.laryngospasm !== 'undefined') {
        $("input[name=laryngospasm][value=" + obj.laryngospasm + "]").attr('checked', 'checked');
    }
    if (typeof obj.vomiting !== 'undefined') {
        $("input[name=vomiting][value=" + obj.vomiting + "]").attr('checked', 'checked');
    }
    if (typeof obj.cyanosis !== 'undefined') {
        $("input[name=cyanosis][value=" + obj.cyanosis + "]").attr('checked', 'checked');
    }
    if (typeof obj.maintenance_excitement !== 'undefined') {
        $("input[name=maintenance_excitement][value=" + obj.maintenance_excitement + "]").attr('checked', 'checked');
    }
    if (typeof obj.maintenance_laryngospasm !== 'undefined') {
        $("input[name=maintenance_laryngospasm][value=" + obj.maintenance_laryngospasm + "]").attr('checked', 'checked');
    }
    if (typeof obj.maintenance_sweating !== 'undefined') {
        $("input[name=maintenance_sweating][value=" + obj.maintenance_sweating + "]").attr('checked', 'checked');
    }
    if (typeof obj.maintenance_mucous !== 'undefined') {
        $("input[name=maintenance_mucous][value=" + obj.maintenance_mucous + "]").attr('checked', 'checked');
    }
    if (typeof obj.mask !== 'undefined') {
        $("#mask").val(obj.mask);
    }
    if (typeof obj.endotrachael_size !== 'undefined') {
        $("#endotrachael_size").val(obj.endotrachael_size);
    }
    if (typeof obj.endotrachael_size !== 'undefined') {
        $("#endotrachael_size").val(obj.endotrachael_size);
    }

    if (typeof obj.endotrachael_type !== 'undefined') {
        $("input[name=endotrachael_type][value=" + obj.endotrachael_type + "]").attr('checked', 'checked');
    }
    if (typeof obj.oral !== 'undefined') {
        $("input[name=oral][value=" + obj.oral + "]").attr('checked', 'checked');
    }
    if (typeof obj.tracheostomy !== 'undefined') {
        $("input[name=tracheostomy][value=" + obj.tracheostomy + "]").attr('checked', 'checked');
    }
    if (typeof obj.endobrononial !== 'undefined') {
        $("input[name=endobrononial][value=" + obj.endobrononial + "]").attr('checked', 'checked');
    }
    if (typeof obj.cuff !== 'undefined') {
        $("input[name=cuff][value=" + obj.cuff + "]").attr('checked', 'checked');
    }
    if (typeof obj.local_spray !== 'undefined') {
        $("input[name=local_spray][value=" + obj.local_spray + "]").attr('checked', 'checked');
    }
    if (typeof obj.pack !== 'undefined') {
        $("input[name=pack][value=" + obj.pack + "]").attr('checked', 'checked');
    }
    if (typeof obj.reflex !== 'undefined') {
        $("input[name=reflex][value=" + obj.reflex + "]").attr('checked', 'checked');
    }
    if (typeof obj.retching !== 'undefined') {
        $("input[name=retching][value=" + obj.retching + "]").attr('checked', 'checked');
    }
    if (typeof obj.excitement !== 'undefined') {
        $("input[name=excitement][value=" + obj.excitement + "]").attr('checked', 'checked');
    }
    if (typeof obj.recovery_vomiting !== 'undefined') {
        $("input[name=recovery_vomiting][value=" + obj.recovery_vomiting + "]").attr('checked', 'checked');
    }
    if (typeof obj.recovery_bp !== 'undefined') {
        $("#recovery_bp").val(obj.recovery_bp);
    }
    if (typeof obj.recovery_pulse !== 'undefined') {
        $("#recovery_pulse").val(obj.recovery_pulse);
    }
    if (typeof obj.recovery_spo2 !== 'undefined') {
        $("#recovery_spo2").val(obj.recovery_spo2);
    }
    if (typeof obj.recovery_etco2 !== 'undefined') {
        $("#recovery_etco2").val(obj.recovery_etco2);
    }
    if (typeof obj.estimated_blood_loss !== 'undefined') {
        $("#estimated_blood_loss").val(obj.estimated_blood_loss);
    }
    if (typeof obj.total_fluid_given_in_or !== 'undefined') {
        $("#total_fluid_given_in_or").val(obj.total_fluid_given_in_or);
    }
    if (typeof obj.recovery_blood !== 'undefined') {
        $("#recovery_blood").val(obj.recovery_blood);
    }
    if (typeof obj.recovery_other !== 'undefined') {
        $("#recovery_other").val(obj.recovery_other);
    }
    if (typeof obj.ans_position !== 'undefined') {
        $("#ans_position").val(obj.ans_position);
    }
    if (typeof obj.ans_start_time !== 'undefined') {
        $("#ans_start_time").val(obj.ans_start_time);
    }
    if (typeof obj.ans_end_time !== 'undefined') {
        $("#ans_end_time").val(obj.ans_end_time);
    }
    if (typeof obj.surgery_start_time !== 'undefined') {
        $("#surgery_start_time").val(obj.surgery_start_time);
    }
    if (typeof obj.surgery_end_time !== 'undefined') {
        $("#surgery_end_time").val(obj.surgery_end_time);
    }
    if (typeof obj.adequate_resp_efforts !== 'undefined') {
        $("input[name=adequate_resp_efforts][value=" + obj.adequate_resp_efforts + "]").attr('checked', 'checked');
    }
    if (typeof obj.eye_opening !== 'undefined') {
        $("input[name=eye_opening][value=" + obj.eye_opening + "]").attr('checked', 'checked');
    }
    if (typeof obj.obeying_commands !== 'undefined') {
        $("input[name=obeying_commands][value=" + obj.obeying_commands + "]").attr('checked', 'checked');
    }
    if (typeof obj.sustained_head_lift !== 'undefined') {
        $("input[name=sustained_head_lift][value=" + obj.sustained_head_lift + "]").attr('checked', 'checked');
    }
    if (typeof obj.tongue_protrusion !== 'undefined') {
        $("input[name=tongue_protrusion][value=" + obj.tongue_protrusion + "]").attr('checked', 'checked');
    }
    if (typeof obj.airway_reflexes !== 'undefined') {
        $("input[name=airway_reflexes][value=" + obj.airway_reflexes + "]").attr('checked', 'checked');
    }
    if (typeof obj.after_giving_thorough !== 'undefined') {
        $("input[name=after_giving_thorough][value=" + obj.after_giving_thorough + "]").attr('checked', 'checked');
    }
    if (typeof obj.post_extubation_pr !== 'undefined') {
        $("#post_extubation_pr").val(obj.post_extubation_pr);
    }
    if (typeof obj.post_extubation_rr !== 'undefined') {
        $("#post_extubation_rr").val(obj.post_extubation_rr);
    }
    if (typeof obj.post_extubation_spo2 !== 'undefined') {
        $("#post_extubation_spo2").val(obj.post_extubation_spo2);
    }
    if (typeof obj.post_extubation_bp !== 'undefined') {
        $("#post_extubation_bp").val(obj.post_extubation_bp);
    }
    if (typeof obj.chk_time_of_completion !== 'undefined') {
        $("#chk_time_of_completion").val(obj.chk_time_of_completion);
    }
    if (typeof obj.chk_time_of_extubation !== 'undefined') {
        $("#chk_time_of_extubation").val(obj.chk_time_of_extubation);
    }
    if (typeof obj.pre_op_diaganosis !== 'undefined') {
        $("#pre_op_diaganosis").val(obj.pre_op_diaganosis);
    }
    if (typeof obj.operation_done !== 'undefined') {
        $("#operation_done").val(obj.operation_done);
    }
    if (typeof obj.premedication !== 'undefined') {
        $("#premedication").val(obj.premedication);
    }
    if (typeof obj.ar_scheduled_at !== 'undefined') {
        $("#ar_scheduled_at").val(obj.ar_scheduled_at);
    }
    if (typeof obj.asa_grade !== 'undefined') {
        $("#asa_grade").val(obj.asa_grade);
    }
    if (typeof obj.ans_technique !== 'undefined') {
        $("#ans_technique").val(obj.ans_technique);
    }

}

