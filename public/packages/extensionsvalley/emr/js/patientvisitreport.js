
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key;
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: "",
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, uhid, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name.toUpperCase()+' ( '+uhid+' ) ');
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});


$(document).on('click', '.getReport', function(){
    var patient_id = $("#op_no_hidden").val();
    fetchPatientVisitReport(patient_id);
});


function fetchPatientVisitReport(patient_id){
    var url = $('#base_url').val() + "/report/fetchPatientVisitReport";
    let _token = $('#c_token').val();
    var request_data = {};
    request_data.patient_id = patient_id;
    request_data._token = _token;
    $.ajax({
        type: "POST",
        url: url,
        data: request_data,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {

            $("#printData").html(html);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

        },
        complete: function () {
            $('body').LoadingOverlay("hide");
            
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}


function printVisitReport(patient_id, visit_id){
    var url = $('#base_url').val() + "/report/printVisitReport";
    let _token = $('#c_token').val();
    var request_data = {};
    request_data.patient_id = patient_id;
    request_data.visit_id = visit_id;
    request_data._token = _token;
    $.ajax({
        type: "POST",
        url: url,
        data: request_data,
        beforeSend: function () {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {

            // console.log(html)
            // var mywindow = window.open("", "Print Visit Report", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=780,height=200,top="+(window.screen.height-400)+",left="+(window.screen.width-840));
            // mywindow.document.body.innerHTML = html;
            var mywindow = window.open('', 'Print Visit Report', 'height=3508,width=2480');
            mywindow.document.write(html+'<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

        },
        complete: function () {
            $('body').LoadingOverlay("hide");
            
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}