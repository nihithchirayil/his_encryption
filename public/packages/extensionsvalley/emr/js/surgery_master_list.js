
$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    searchSurgeryMaster();

});

var base_url = $("#base_url").val();
var token = $("#c_token").val();

function searchSurgeryMaster() {
    var url = base_url + "/surgery/searchSurgeryMaster";
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var surgery_master = $("#surgery_master").val();

    $.ajax({
        url: url,
        type: "POST",
        data: { _token: token, from_date: from_date, to_date: to_date, surgery_master: surgery_master },
        beforeSend: function () {
            $('#searchSurgeryMasterDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#searchSurgeryMasterBtn').attr('disabled', true);
            $('#searchSurgeryMasterSpin').removeClass('fa fa-search');
            $('#searchSurgeryMasterSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchSurgeryMasterDiv').html(data);
        },
        complete: function () {
            $('#searchSurgeryMasterDiv').LoadingOverlay("hide");
            $('#searchSurgeryMasterBtn').attr('disabled', false);
            $('#searchSurgeryMasterSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchSurgeryMasterSpin').addClass('fa fa-search');
        },
    });
}

function resetSugrgeryMaster() {
    $('#from_date').val('');
    $('#to_date').val('');
    $('#surgery_master').val('All');
    $('#surgery_master').select2();
    searchSurgeryMaster();
}

function addWindowLoad(to_url, head_id) {
    var url = base_url + "/surgery/" + to_url + '/' + head_id;
    document.location.href = url;
}

