
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_ot_bill_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
        data.surgery_charge = $("#surgery_charge").val();
        data.surgery = $(".surgery_type").val();
       
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".surgery_charge_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Surgery Charge");
            return;
        }
      

        data.ot_bill_id = $(".reference_id").val();
        data.surgery_charge = $(".surgery_charge_add").val();
        data.cssd_charge = $(".cssd_charge_add").val();
        data.asst_surgeon_charge = $(".asst_surgeon_charge_add").val();
        data.status = $(".status_add").val();
        data.local_anesthesia_charge = $(".local_anesthesia_charge_add").val();
        data.surgery_type = $(".surgery_type_add").val();
       
      
      
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Surgery Charge Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
   
    function editItem(element){
      
        let ot_bill_id = $(element).parents('tr').attr('data-id');
        let surgery_charge = $(element).parents('tr').attr('data-surgery_charge');
        let cssd_charge = $(element).parents('tr').attr('data-cssd_charge');
        let asst_surgeon_charge = $(element).parents('tr').attr('data-asst_surgeon_charge');
        let status = $(element).parents('tr').attr('data-status');
        let local_anesthesia_charge = $(element).parents('tr').attr('data-local_anesthesia_charge');
        let surgery_type = $(element).parents('tr').attr('data-surgery_type');
       
        $(".reference_id").val(ot_bill_id);
        $(".surgery_charge_add").val(surgery_charge);
        $(".cssd_charge_add").val(cssd_charge);
        $(".asst_surgeon_charge_add").val(asst_surgeon_charge);
        $(".status_add").val(status);
        $(".local_anesthesia_charge_add").val(local_anesthesia_charge);
        $(".surgery_type_add").val(surgery_type);
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $("#add").html('Edit Ot Bill Surgery Charge');

    }

    function deleteItem(element){

        let confirm = window.confirm('Are you sure want to delete ?');
        if(confirm){
            let data = {};
            let url = $(".table_body_contents").attr('data-delete-url');
            let ot_bill_id = $(element).parents('tr').attr('data-id');
            data.ot_bill_id = ot_bill_id;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function () {
                    $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                },
                success: function (data) {  
                    $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                },
                complete: function () { 

                }
            });
        }
        
    }

   
    function resetForm(){
        $(".reference_id").val('');
        $(".surgery_charge_add").val('');
        $(".cssd_charge_add").val('');
        $(".asst_surgeon_charge_add").val('');
        $("#add").html('Add Ot Bill Surgery Charge');
        $(".status_add").val('');
        $(".local_anesthesia_charge_add").val('');
        $(".surgery_type_add").val('');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

