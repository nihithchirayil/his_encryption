$(document).ready(function(){

    fetchOTBillConfiguration();
    

    $('#bill_tag').change(function(){
        fetchOTBillConfiguration();
    });
    
    $('.saveConfiguration').click(function(){
        saveOTBillConfiguration();
    });
    
    
    function fetchOTBillConfiguration(){
        var bill_tag = $('#bill_tag').val();
        let _token = $('#c_token').val();
        // var url = "/otbilling/fetchOTBillConfiguration";
        var url = $("#base_url").val()+"/otbilling/fetchOTBillConfiguration";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                bill_tag :bill_tag,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".bill_configuration_body").empty();
                $(".bill_configuration_body").append("<tr><td colspan='3' style='text-align: center;'><i class='fa fa-spinner fa-spin' style='font-size: 20px;'></i></td></tr>")
            },
            success: function (response) {
                console.log(response);
                
                if(response.code === 100){
                    $(".saveConfigurationDiv").show();
                    $(".bill_configuration_body").empty();
                    if(response.data.mode == 1){
                        $.each(response.data.configuration, function(key, val){
                            var html = "<tr class='component_row' data-config-mode='"+response.data.mode+"' data-config-id='0' data-component-id='"+val.component_id+"'><td>"+val.name+"</td><td><input type='number' step='0.001' max='100' min='0' autocomplete='off' value='0' class='form-control percentage' onkeyup='calculateTotalPercent();'  ></input></td><td><select class='form-control config_status'  style='padding:1.5px 4px;'><option value='1' selected='selected'>Active</option><option value='0'>Inactive</option></select></td></tr>";
                            $(".bill_configuration_body").append(html)
                        })
                    } else {
                        
                        $.each(response.data.configuration, function(key, val){
                            var select_options = '';
                            if(val.status == 1){
                                select_options = "<option value='1' selected='selected'>Active</option><option value='0'>Inactive</option>";
                            } else {
                                select_options = "<option value='1'>Active</option><option value='0' selected='selected'>Inactive</option>";
                            }
                            var html = "<tr class='component_row' data-config-mode='"+response.data.mode+"' data-config-id='"+val.configuration_id+"' data-component-id='"+val.component_id+"'><td>"+val.name+"</td><td><input type='number' step='0.001' max='100' min='0' autocomplete='off' value='"+val.percentage+"' class='form-control percentage' onkeyup='calculateTotalPercent();' ></input></td><td><select class='form-control config_status'  style='padding:1.5px 4px;'>"+select_options+"</select></td></tr>";
                            $(".bill_configuration_body").append(html)
                        })

                        calculateTotalPercent();
                    }
                    
                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { }
        });
    }
    
    function saveOTBillConfiguration(){
        var bill_tag = $('#bill_tag').val();
        if(!bill_tag){
            toastr.error("Please select Bill Tag..!!");
            return;
        }

        var tot_percent=0;
        $('.percentage').each( function(){
            tot_percent = parseFloat(tot_percent) + parseFloat($(this).val());
        })

        if(tot_percent > 100 || tot_percent < 100){
            toastr.error("Total percentage should be 100 !!");
            return;
        }

        var request_data = [];
        $('.component_row').each( function(){
            var temp = {};
            temp.bill_tag = bill_tag;
            temp.mode = $(this).attr("data-config-mode");
            temp.config_id = $(this).attr("data-config-id");
            temp.component_id = $(this).attr("data-component-id");
            temp.percentage = $(this).find(".percentage").val();
            temp.status = $(this).find(".config_status").val();
            request_data.push(temp);
        })


        let _token = $('#c_token').val();
        // var url = "/otbilling/saveOTBillConfiguration";
        var url = $("#base_url").val()+"/otbilling/saveOTBillConfiguration";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                request_data :request_data,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".saveConfiguration").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (response) {
                console.log(response);
                $(".saveConfiguration").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-save');
                
                if(response.code === 100){
                    toastr.success("Configuration Saved !!");
                    $('#bill_tag').val('').trigger('change');
                } else {
                    toastr.error("Something went wrong !!");
                }
            },
            complete: function () { }
        });
    }


    

});

/* calculate Total Percentage */
function calculateTotalPercent(){
    var tot_percent=0;
    $('.percentage').each( function(){
        tot_percent = parseFloat(tot_percent) + parseFloat($(this).val());
    })
    $('.total_percentage').html(tot_percent);
    if(tot_percent > 100 || tot_percent < 100){
        $('.total_percentage').css('color','red');
        $('.total_percentage_error').html('Total percentage should be 100 !!');
    }else{
        $('.total_percentage_error').html('');
        $('.total_percentage').css('color','black');
    }
}