$('document').ready(function () {
    $('#search_med_type').hide();
    $('#prescription_list').hide();
});

//medicine search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_medicine"]', function (event) {

    let groups = $("input[name=get_grp_id]").val();
    //alert(groups);
    if (groups == 0) {
        Command: toastr["warning"]("Select Group");
    } else {
        event.preventDefault();
        /* Act on the event */
        var obj = $(this);
        var search_string = $(this).val();
        // var patient_id = $('#patient_id').val();
        var search_type = $("input[name='m_search_type']:checked").val();
        var med_list = $('.medicine-list-div');


        if (search_string == "" || search_string.length < 3) {
            last_search_string = '';
            return false;
        } else {
            $(med_list).show();
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                if (search_string == last_search_string) {
                    return false;
                }
                var url = $('#base_url').val() + "/prescription/favourite-medicine-search";

                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        search_key_string: search_string,
                        search_type: search_type
                    },
                    beforeSend: function () {
                        $('#MedicationTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                    },
                    success: function (data) {
                        //alert(data);
                        let response = data;
                        let res_data = "";


                        var search_list = $('#ListMedicineSearchData');


                        if (response.length > 0) {
                            for (var i = 0; i < response.length; i++) {

                                let item_desc = response[i].item_desc;
                                let item_code = response[i].item_code;
                                let generic_name = response[i].generic_name;
                                let price = response[i].price;

                                res_data += '<tr><td>' + item_desc + '</td><td>' + generic_name + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"></tr>';
                            }
                        } else {
                            res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                        }

                        if (response.length > 0) {
                            $('input[name="search_medicine"]').removeClass('outside-medicine');
                        } else {
                            $('input[name="search_medicine"]').addClass('outside-medicine');
                            // $(med_list).hide();
                        }

                        $(search_list).html(res_data);
                        last_search_string = search_string;
                        $(".presc_theadscroll").animate({ scrollTop: 0 }, "slow");

                    },
                    complete: function () {
                        $('.presc_theadfix_wrapper').floatThead("reflow");
                    }
                });
            }, 500)

        }
    }

});

//outside medicine colour code
$(document).on('blur', 'input[name="search_medicine"]', function (event) {
    let search_item_code_hidden = $('input[name="search_item_code_hidden"]').val();
    let search_medicine = $('input[name="search_medicine"]').val();
    if (search_medicine != "" && search_medicine != undefined && search_item_code_hidden == "") {
        $('input[name="search_medicine"]').addClass('outside-medicine');
    } else {
        $('input[name="search_medicine"]').removeClass('outside-medicine');
    }
});

//close medicine search
$(document).on('click', '.medicine-list-div > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".medicine-list-div").hide();
});

//when select medicine
$(document).on('dblclick', '#ListMedicineSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let name = $(tr).find('input[name="list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="list_med_code_hid[]"]').val();
    let price = $(tr).find('input[name="list_med_price_hid[]"]').val();

    if (name != '' && code != '') {
        $('input[name="search_medicine"]').val(name);
        $('input[name="search_item_code_hidden"]').val(code);
        $('input[name="search_item_name_hidden"]').val(name);
        $('input[name="search_item_price_hidden"]').val(price);

        $(".medicine-list-div").hide();
        $("input[name='search_dose']").focus();
    }

});

//duration
$('#search_duration,#search_quantity').on('keyup', function () {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9.]/g, ""));
    if (cur_obj_value.length > 3) {
        $(cur_obj).val(Number(cur_obj_value.slice(0, 3)));
    }
});

$('#search_duration,#search_frequency').on('blur change', function () {
    let search_duration = $(this).closest('tr').find('input[name="search_duration"]').val();
    let search_freq_value_hidden = $(this).closest('tr').find('input[name="search_freq_value_hidden"]').val();
    if (search_duration != "" && search_duration != undefined && search_freq_value_hidden != "" && search_freq_value_hidden != undefined) {
        let qty = parseInt(search_duration) * parseInt(search_freq_value_hidden);
        $(this).closest('tr').find('input[name="search_quantity"]').val(qty);
    } else {
        $(this).closest('tr').find('input[name="search_quantity"]').val(1);
    }
});

$('input[name="selected_item_duration[]"],input[name="selected_item_frequency[]"]').on('blur change', function () {
    let search_duration = $(this).closest('tr').find('input[name="selected_item_duration[]"]').val();
    let search_freq_value_hidden = $(this).closest('tr').find('input[name="selected_frequency_value[]"]').val();
    if (search_duration != "" && search_duration != undefined && search_freq_value_hidden != "" && search_freq_value_hidden != undefined) {
        let qty = parseInt(search_duration) * parseInt(search_freq_value_hidden);
        $(this).closest('tr').find('input[name="selected_item_quantity[]"]').val(qty);
    } else {
        $(this).closest('tr').find('input[name="selected_item_quantity[]"]').val(1);
    }
});

//frequency search
var freq_timeout = null;
var freq_last_search_string = '';
$('#search_frequency').on('keyup', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchFrequency(obj);
});

$('#search_frequency').on('focus', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchFrequency(obj, 1);
});

function searchFrequency(obj, all = 0) {
    var search_freq_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var freq_list = $('.frequency-list-div');
    if ((search_freq_string == "" || search_freq_string.length < 2) && all == 0) {
        freq_last_search_string = '';
        return false;
    } else {
        $(freq_list).show();
        clearTimeout(freq_timeout);
        freq_timeout = setTimeout(function () {
            if (search_freq_string == freq_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/frequency-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_freq_string: search_freq_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#FrequencyTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";


                    var freq_search_list = $('#ListFrequencySearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let frequency = response[i].frequency;
                            let frequency_value = response[i].frequency_value;

                            res_data += '<tr><td>' + frequency + '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' + i + '" value="' + frequency_value + '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' + i + '" value="' + frequency + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(freq_search_list).html(res_data);
                    freq_last_search_string = search_freq_string;
                    $(".freq_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.freq_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}

//close frequency search
$(document).on('click', '.close_btn_freq_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".frequency-list-div").hide();
});

//when select frequency
$(document).on('dblclick', '#ListFrequencySearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
    let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();

    if (name != '' && value != '') {
        $('input[name="search_frequency"]').val(name);
        $('input[name="search_freq_value_hidden"]').val(value);
        $('input[name="search_freq_name_hidden"]').val(name);

        $(".frequency-list-div").hide();
        $('input[name="search_duration"]').focus();
    }

});

//add medicine
function addNewMedicine() {
    let status = validateRow();
    if (status == false) {
        console.log("Error");
        return false;
    }

    let mode = 'add';

    getNewRowInserted(mode);

    $('input[name="search_medicine"]').focus();
    $("#td_1").hide();
}

//add row
function getNewRowInserted(mode = 'add', data = {}) {

    let code = "";
    if (mode == 'add') {
        code = $('input[name="search_item_code_hidden"]').val();
    } else {
        if (Object.keys(data).length > 0) {
            code = data.med_code;
        }
    }


    var table = document.getElementById("FavTableList");
    let row_count = table.rows.length;
    let row_id = row_count;
    if (parseInt(row_count) > 0) {
        let last_id = $('#FavTableList tr:last').attr('row-id');
        if (last_id != undefined) {
            row_id = parseInt(last_id) + 1;
        }
    }

    var row = table.insertRow(row_count);

    row.setAttribute("row-id", row_id, 0);

    var cell1 = row.insertCell(0);
    cell1.width = '30%';
    var cell2 = row.insertCell(1);
    cell2.width = '10%';
    var cell3 = row.insertCell(2);
    cell3.width = '10%';
    var cell4 = row.insertCell(3);
    cell4.width = '10%';
    var cell5 = row.insertCell(4);
    cell5.width = '7%';
    var cell6 = row.insertCell(5);
    cell6.width = '15%';
    var cell7 = row.insertCell(6);
    cell7.width = '15%';
    var cell8 = row.insertCell(7);
    cell8.classList.add("text-center");
    cell8.width = '3%';

    let med_code = "";
    let med_name = "";
    let duration = "";
    let dose = "";
    let quantity = "";
    let route = "";
    let freq_value = "";
    let freq_name = "";
    let remarks = "";
    let id = "";
    let price = 0;

    if (mode == 'add') {

        med_code = $('input[name="search_item_code_hidden"]').val();

        //in case of outside medicine
        if (med_code == "") {
            med_name = $('input[name="search_medicine"]').val();
            if (med_name != "") {
                cell1.classList.add("outside-medicine");
                $('input[name="search_medicine"]').removeClass('outside-medicine');
            } else {
                cell1.classList.remove("outside-medicine");
            }
        } else {
            med_name = $('input[name="search_item_name_hidden"]').val();
            cell1.classList.remove("outside-medicine");
        }

        dose = $('input[name="search_dose"]').val();
        duration = $('input[name="search_duration"]').val();
        quantity = $('input[name="search_quantity"]').val();

        freq_value = $('input[name="search_freq_value_hidden"]').val();
        freq_name = $('input[name="search_freq_name_hidden"]').val();

        route = $('input[name="search_route"]').val();
        remarks = $('input[name="search_instructions"]').val();

        price = $('input[name="search_item_price_hidden"]').val();

    } else {
        //load bsed on prescription_id
        if (Object.keys(data).length > 0) {
            med_code = data.med_code;
            med_name = data.med_name;
            out_medicine_name = data.out_medicine_name;
            duration = data.duration;
            quantity = data.quantity;
            dose = data.dose;
            price = data.price;
            freq_value = data.freq_value;
            freq_name = data.freq_name;
            route = data.route;
            remarks = data.remarks;
            id = data.id;
            head_id = data.head_id;
            if (mode == 'copy') {
                id = "";
                head_id = "";
            }

            //in case of outside medicine
            if (med_code == "") {
                if (out_medicine_name != "") {
                    med_name = out_medicine_name;
                    cell1.classList.add("outside-medicine");
                } else {
                    cell1.classList.remove("outside-medicine");
                }
            } else {
                cell1.classList.remove("outside-medicine");
            }

            if (head_id != '' && head_id != 0) {
                $('#prescription_head_id').val(head_id);
            }
        }
    }

    let medicine_popup_search = '<div class="medicine-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_med_search">X</a><div class=" presc_theadscroll" style="position: relative;"><table id="MedicationTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><thead><tr class="light_purple_bg"><th>Medicine</th><th>Generic Name</th></tr></thead><tbody id="ListMedicineSearchDataRowListing-' + row_id + '" class="list-medicine-search-data-row-listing" ></tbody></table></div></div>';

    let medicine_freq_popup_search = '<div class="frequency-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_freq_search">X</a><div class=" freq_theadscroll" style="position: relative;"><table id="FrequencyTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><tbody id="ListFrequencySearchDataRowListing-' + row_id + '" class="list-frequency-search-data-row-listing" ></tbody></table></div></div>';

    let medicine_route_popup_search = '<div class="route-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_route_search">X</a><div class=" route_theadscroll" style="position: relative;"><table id="RouteTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><tbody id="ListRouteSearchDataRowListing-' + row_id + '" class="list-route-search-data-row-listing" ></tbody></table></div></div>';

    let medicine_instruction_popup_search = '<div class="instruction-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_instruction_search">X</a><div class=" instruction_theadscroll" style="position: relative;"><table id="InstructionTableRowListing-' + row_id + '"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><tbody id="ListInstructionSearchDataRowListing-' + row_id + '" class="list-instruction-search-data-row-listing" ></tbody></table></div></div>';

    cell1.innerHTML = "<span class='med_name'>" + med_name + "</span> <input type='hidden' name='selected_item_code[]' id='selected_item_code-" + row_id + "' value='" + med_code + "'><input type='hidden' name='favitemid[]'  value=''><input type='hidden' class='form-control' name='selected_item_name[]' id='selected_item_name-" + row_id + "' value='" + med_name + "'><input type='hidden' name='selected_edit_id[]' id='selected_edit_id-" + row_id + "' value=''><input type='hidden' name='selected_item_price[]' id='selected_item_price-" + row_id + "' value='" + price + "'>" + medicine_popup_search;
    cell2.innerHTML = "<span class='dose'>" + dose + "</span> <input type='hidden' class='form-control' name='selected_item_dose[]' id='selected_item_dose-" + row_id + "' value='" + dose + "'>";
    cell3.innerHTML = "<span class='frequency'>" + freq_name + "</span> <input type='hidden' class='form-control' name='selected_item_frequency[]' id='selected_item_frequency-" + row_id + "' value='" + freq_name + "'><input type='hidden' name='selected_frequency_value[]' id='selected_frequency_value-" + row_id + "' value='" + freq_value + "'>" + medicine_freq_popup_search;
    cell4.innerHTML = "<span class='duration'>" + duration + "</span> <input type='hidden' class='form-control' name='selected_item_duration[]' id='selected_item_duration-" + row_id + "' value='" + duration + "'>";
    cell5.innerHTML = "<span class='quantity'>" + quantity + "</span> <input type='hidden' class='form-control' name='selected_item_quantity[]' id='selected_item_quantity-" + row_id + "' value='" + quantity + "'>";
    cell6.innerHTML = "<span class='route'>" + route + "</span> <input type='hidden' class='form-control' name='selected_item_route[]' id='selected_item_route-" + row_id + "' value='" + route + "'>" + medicine_route_popup_search;
    cell7.innerHTML = "<span class='remarks' title='" + remarks + "'>" + remarks + "</span> <input type='hidden' class='form-control' name='selected_item_remarks[]' id='selected_item_remarks-" + row_id + "' value='" + remarks + "'>" + medicine_instruction_popup_search;
    cell8.innerHTML = "<button class='btn btn-success edit-med-row' title='Edit' type='button'><i  class='fa fa-edit'></i></button> &nbsp;<button class='btn  btn-danger del-med-row' title='Delete' type='button'><i  class='fa fa-trash-o'></i></button>";

    if (mode == 'add') {
        // clearmedfields();
    }
    savePrescriptionFavourite();
}

//clear fields
function clearmedfields() {
    $('input[name="search_item_code_hidden"]').val('');
    $('input[name="search_item_name_hidden"]').val('');
    $('input[name="search_medicine"]').val('');

    $('input[name="search_duration"]').val('');
    $('input[name="search_dose"]').val('');
    $('input[name="search_route"]').val('');
    $('input[name="search_quantity"]').val('');

    $('input[name="search_freq_value_hidden"]').val('');
    $('input[name="search_freq_name_hidden"]').val('');
    $('input[name="search_frequency"]').val('');

    $('input[name="search_instructions"]').val('');
}



//validate top row
function validateRow() {
    let err = 0;

    let itm_name = $('input[name="search_medicine"]').val();
    let itm_code = $('input[name="search_item_code_hidden"]').val();
    if (itm_name == '' || itm_name == undefined) {
        Command: toastr["error"]("Enter Medicine.");
        err = 1;
    }

    let itm_duration = $('input[name="search_duration"]').val();
    if (itm_duration == '' || itm_duration == undefined) {
        Command: toastr["error"]("Enter Days.");
        err = 1;
    }

    let itm_quantity = $('input[name="search_quantity"]').val();
    if (itm_quantity == '' || itm_quantity == undefined) {
        Command: toastr["error"]("Enter Quantity.");
        err = 1;
    }

    let itm_freq_val = $('input[name="search_freq_value_hidden"]').val();
    let itm_freq_name = $('input[name="search_freq_name_hidden"]').val();

    if (itm_freq_val == '' || itm_freq_val == undefined || itm_freq_name == '' || itm_freq_name == undefined) {
        Command: toastr["error"]("Enter Frequency.");
        err = 1;
    }

    //same item already exists
    let alredy_exist = $('input[name="selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == itm_code && itm_code != "") {
            return itm_code;
        }
    });

    if (alredy_exist.length > 0) {
        //   Command: toastr["error"]("Medicine already exist.");
        err = 1;
    }

    let alredy_exist1 = $('input[name="fav_medicine_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == itm_code && itm_code != "") {
            return itm_code;
        }
    });

    if (alredy_exist1.length > 0) {
        Command: toastr["error"]("Medicine already exist.");
        err = 1;
    }

    if (err == 1) {
        return false;
    } else {
        return true;
    }
}




var save_starts = 0;
//save prescription
function saveDoctorPrescriptionsFav() {
    //  let validate = validatePrescription();
    let _token = $('#c_token').val();

    let groups = $("input[name=get_grp_id]").val();

    let user_id = $("input[name=search_user_id_hidden]").val();

    let search_item_code_hidden = $("input[name=search_item_code_hidden]").val();
    let search_item_name_hidden = $("input[name=search_item_name_hidden]").val();
    let search_dose = $("input[name=search_dose]").val();
    let search_freq_value_hidden = $("input[name=search_freq_value_hidden]").val();
    let search_freq_name_hidden = $("input[name=search_freq_name_hidden]").val();
    let search_duration = $("input[name=search_duration]").val();
    let search_quantity = $("input[name=search_quantity]").val();
    let search_route = $("input[name=search_route]").val();
    let search_instructions = $("input[name=search_instructions]").val();


    // let presc_form = $('#presc-data-form').serialize();

    var p_type = $("input[name='p_search_type']:checked").val();
    var edit_id = $("input[name='edit_id']").val();

    if (edit_id != '' && edit_id != 0) {
        deleteMedicineRow(edit_id);
        $('#FavTableList tr#' + edit_id + '').remove();
    }
    var url = $('#base_url').val() + "/prescription/save-prescription-favourite";
    save_starts = 1;
    $('.save_btn_bg').prop({ disable: 'true' });
    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: {
            groups: groups,
            search_item_code_hidden: search_item_code_hidden,
            search_item_name_hidden: search_item_name_hidden,
            search_dose: search_dose,
            search_freq_value_hidden: search_freq_value_hidden,
            search_freq_name_hidden: search_freq_name_hidden,
            search_duration: search_duration,
            search_quantity: search_quantity,
            search_route: search_route,
            search_instructions: search_instructions,
            user_id: user_id,
            _token: _token
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"]("Saved.");
                $('input[name="favitemid[]"]').val(data.id);
                $('input[name="selected_edit_id[]"]').val(data.id);
                clearmedfields();

            }


        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('.save_btn_bg').prop({ disable: 'false' });
        }
    });

}




//medicine search listing(edit row)
var edit_row_timeout = null;
var edit_row_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_name[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_string = $(this).val();
    var patient_id = $('#patient_id').val();
    var search_type = $("input[name='m_search_type']:checked").val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_string == "" || edit_row_search_string.length < 3) {
        edit_row_last_search_string = '';
        return false;
    } else {
        var med_list = $(tr).find('.medicine-list-div-row-listing');
        $(med_list).show();
        clearTimeout(edit_row_timeout);
        edit_row_timeout = setTimeout(function () {
            if (edit_row_search_string == edit_row_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: edit_row_search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#MedicationTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data.medicine_list;
                    let res_data = "";

                    var search_list = $(tr).find('#ListMedicineSearchDataRowListing-' + row_id);




                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;

                            res_data += '<tr><td>' + item_desc + '</td><td>' + generic_name + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    edit_row_last_search_string = edit_row_search_string;
                    $(".presc_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});

//when double click listing medicine search result (edit)
$(document).on('dblclick', '.list-medicine-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let sel_row_td = $(tr).closest('.medicine-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let name = $(tr).find('input[name="list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="list_med_code_hid[]"]').val();

    let price = $(tr).find('input[name="list_med_price_hid[]"]').val();

    let cur_row_itm_code = $(sel_row_tr).find('input[name="selected_item_code[]"]').val();

    //same item already exists
    let alredy_exist = $('input[name="selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == code && $(obj).val() != cur_row_itm_code) {
            return code;
        }
    });

    if (alredy_exist.length > 0) {
        Command: toastr["error"]("Medicine already exist.");
        return false;
    }

    if (name != '' && code != '') {
        $(sel_row_td).removeClass('outside-medicine');
        $(sel_row_td).find('span[class="med_name"]').html(name);
        $(sel_row_td).find('input[name="selected_item_code[]"]').val(code);
        $(sel_row_td).find('input[name="selected_item_name[]"]').val(name);
        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('value', name);
        $(sel_row_td).find('input[name="selected_item_price[]"]').val(price);

        $(sel_row_td).find(".medicine-list-div-row-listing").hide();
    }

});


//close medicine search listing (edit)
$(document).on('click', '.medicine-list-div-row-listing > .close_btn_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".medicine-list-div-row-listing").hide();
});


//frequency search (edit row)
var edit_row_freq_timeout = null;
var edit_row_freq_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_frequency[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_freq_string = $(this).val();
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_freq_string == "" || edit_row_search_freq_string.length < 2) {
        edit_row_freq_last_search_string = '';
        return false;
    } else {
        var freq_list = $(tr).find('.frequency-list-div-row-listing');
        $(freq_list).show();
        clearTimeout(edit_row_freq_timeout);
        edit_row_freq_timeout = setTimeout(function () {
            if (edit_row_search_freq_string == edit_row_freq_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/frequency-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_freq_string: edit_row_search_freq_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#FrequencyTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var freq_search_list = $(tr).find('#ListFrequencySearchDataRowListing-' + row_id);



                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let frequency = response[i].frequency;
                            let frequency_value = response[i].frequency_value;

                            res_data += '<tr><td>' + frequency + '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' + i + '" value="' + frequency_value + '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' + i + '" value="' + frequency + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(freq_search_list).html(res_data);
                    edit_row_freq_last_search_string = edit_row_search_freq_string;
                    $(".freq_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.freq_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close frequency search
$(document).on('click', '.frequency-list-div-row-listing > .close_btn_freq_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".frequency-list-div-row-listing").hide();
});

//when select frequency
$(document).on('dblclick', '.list-frequency-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.frequency-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
    let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();

    if (name != '' && value != '') {
        $(sel_row_td).find('span[class="frequency"]').html(name);
        $(sel_row_td).find('input[name="selected_frequency_value[]"]').val(value);
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').val(name);
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').attr('value', name);

        $(sel_row_td).find(".frequency-list-div-row-listing").hide();
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').focus();
    }

});

//route search (edit row)
var edit_row_route_timeout = null;
var edit_row_route_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_route[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_route_string = $(this).val();
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_route_string == "" || edit_row_search_route_string.length < 2) {
        edit_row_route_last_search_string = '';
        return false;
    } else {
        var route_list = $(tr).find('.route-list-div-row-listing');
        $(route_list).show();
        clearTimeout(edit_row_route_timeout);
        edit_row_route_timeout = setTimeout(function () {
            if (edit_row_search_route_string == edit_row_route_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/route-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_route_string: edit_row_search_route_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#RouteTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var route_search_list = $(tr).find('#ListRouteSearchDataRowListing-' + row_id);

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let route = response[i].route;

                            res_data += '<tr><td>' + route + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(route_search_list).html(res_data);
                    edit_row_route_last_search_string = edit_row_search_route_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close route search
$(document).on('click', '.route-list-div-row-listing > .close_btn_route_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".route-list-div-row-listing").hide();
});

//when select route
$(document).on('dblclick', '.list-route-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.route-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let route = $(tr).text();

    if (route != '') {
        $(sel_row_td).find('input[name="selected_item_route[]"]').val(route);

        $(sel_row_td).find(".route-list-div-row-listing").hide();
        $(sel_row_td).find('input[name="selected_item_route[]"]').focus();
    }

});


//instruction search (edit row)
var edit_row_instruction_timeout = null;
var edit_row_instruction_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_remarks[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_instruction_string = $(this).val();
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_instruction_string == "" || edit_row_search_instruction_string.length < 2) {
        edit_row_instruction_last_search_string = '';
        return false;
    } else {
        var instruction_list = $(tr).find('.instruction-list-div-row-listing');
        $(instruction_list).show();
        clearTimeout(edit_row_instruction_timeout);
        edit_row_instruction_timeout = setTimeout(function () {
            if (edit_row_search_instruction_string == edit_row_instruction_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/instruction-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_instruction_string: edit_row_search_instruction_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#InstructionTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var instruction_search_list = $(tr).find('#ListInstructionSearchDataRowListing-' + row_id);

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let instruction = response[i].instruction;

                            res_data += '<tr><td>' + instruction + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(instruction_search_list).html(res_data);
                    edit_row_instruction_last_search_string = edit_row_search_instruction_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close instruction search
$(document).on('click', '.instruction-list-div-row-listing > .close_btn_instruction_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".instruction-list-div-row-listing").hide();
});

//when select route
$(document).on('dblclick', '.list-instruction-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.instruction-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let instruction = $(tr).text();

    if (instruction != '') {
        $(sel_row_td).find('input[name="selected_item_remarks[]"]').val(instruction);

        $(sel_row_td).find(".instruction-list-div-row-listing").hide();
    }

});




//Route search
var route_timeout = null;
var route_last_search_string = '';
$('#search_route').on('keyup', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchRoute(obj);

});

$('#search_route').on('focus', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchRoute(obj, 1);
});

function searchRoute(obj, all = 0) {
    var search_route_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var route_list = $('.route-list-div');

    if ((search_route_string == "" || search_route_string.length < 2) && all == 0) {
        route_last_search_string = '';
        return false;
    } else {
        var route_list = $('.route-list-div');
        $(route_list).show();
        clearTimeout(route_timeout);
        route_timeout = setTimeout(function () {
            if (search_route_string == route_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/route-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_route_string: search_route_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#RouteTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var route_search_list = $('#ListRouteSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let route = response[i].route;

                            res_data += '<tr><td>' + route + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(route_search_list).html(res_data);
                    route_last_search_string = search_route_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}

//close route search
$(document).on('click', '.close_btn_route_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".route-list-div").hide();
});


//when select route
$(document).on('dblclick', '#ListRouteSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let route = $(tr).text();

    if (route != '') {
        $('input[name="search_route"]').val(route);

        $(".route-list-div").hide();
        $('#search_instructions').focus();
    }

});


//Instruction search
var instruction_timeout = null;
var instruction_last_search_string = '';
$('#search_instructions').on('keyup', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchInstructions(obj);

});

$('#search_instructions').on('focus', function () {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);

    searchInstructions(obj, 1);
});

function searchInstructions(obj, all = 0) {
    var search_instruction_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var instruction_list = $('.instruction-list-div');

    if ((search_instruction_string == "" || search_instruction_string.length < 2) && all == 0) {
        instruction_last_search_string = '';
        return false;
    } else {
        var instruction_list = $('.instruction-list-div');
        $(instruction_list).show();
        clearTimeout(instruction_timeout);
        instruction_timeout = setTimeout(function () {
            if (search_instruction_string == instruction_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/instruction-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_instruction_string: search_instruction_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#InstructionTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var instruction_search_list = $('#ListInstructionSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let instruction = response[i].instruction;

                            res_data += '<tr><td>' + instruction + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(instruction_search_list).html(res_data);
                    instruction_last_search_string = search_instruction_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}


//when select instruction
$(document).on('dblclick', '#ListInstructionSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let instruction = $(tr).text();

    if (instruction != '') {
        $('input[name="search_instructions"]').val(instruction);

        $(".instruction-list-div").hide();
    }

});

//close instruction search
$(document).on('click', '.close_btn_instruction_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".instruction-list-div").hide();
});




document.onkeydown = function (evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        $(".frequency-list-div").hide();
        $(".route-list-div").hide();
        $(".medicine-list-div").hide();
    }
};


function savePrescriptionFavourite() {
    let _token = $('#c_token').val();
    let search_medicine = $("input[name=search_medicine]").val();

    if (search_medicine != '') {
        addNewMedicine();
    }

    if ($('#FavTableList tbody > tr').find('input[name="selected_item_code[]"]').length == 0) {
        //no medicine in the list
    } else {
        saveDoctorPrescriptionsFav();
    }

}




function addGroup() {

    $('input[name="groupname"]').val('');
    $("#group_add_modal").modal('show');

}


function SaveGroup() {

    var user_name = $("input[name=search_user_id_hidden]").val();
    let group_name = $('#groupname').val();
    let _token = $('#c_token').val();

    if (group_name != '') {
        var url = $('#base_url').val() + "/prescription/save-new-group";
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: 'group_name=' + group_name + '&_token=' + _token + '&user_name=' + user_name,
            beforeSend: function () {

            },
            success: function (data) {
                if (data) {
                    Command: toastr["success"]("Saved.");
                    $('#fav_groups #hiderow').hide();
                    $("#fav_groups").append('<tr id=' + data.id + '><td id=' + data.id + ' style="height:30px;cursor: pointer;" onclick="listMedicineByGroup(' + data.id + ')">' + group_name + '<input type="hidden" class="" name="del_grp_id[]" value=' + data.id + '><button style="float: right" class="btn  btn-danger delete-group-row" type="button"><i class="fa fa-trash-o"></i></button></td></tr>');

                }
            },
            complete: function () {
                $("#group_add_modal").modal('hide');
            }
        });
    } else {
        toastr.warning("Type Group Name");
    }

}


$(document).on('click', '.delete-medicine-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="favitemid[]"]').val();

        if (edit_id != '' && edit_id != 0) {
            deleteMedicineRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});

//delete single row from db
function deleteMedicineRowFromDb(id) {
    var url = $('#base_url').val() + "/prescription/delete-medicine-row";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}


function deleteMedicineRow(id) {
    var url = $('#base_url').val() + "/prescription/delete-medicine-row";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {

            }
        },
        complete: function () {

        }
    });
}


// medicine edit
$(document).on('click', '.edit-medicine-row', function () {

    let tr = $(this).closest('tr');
    let edit_id = $(tr).find('input[name="favitemid[]"]').val();
    let fav_medicine_name = $(tr).find('input[name="fav_medicine_name[]"]').val();
    let fav_medicine_code = $(tr).find('input[name="fav_medicine_code[]"]').val();
    let fav_dose = $(tr).find('input[name="fav_dose[]"]').val();
    let fav_quantity = $(tr).find('input[name="fav_quantity[]"]').val();
    let fav_frequency = $(tr).find('input[name="fav_frequency[]"]').val();
    let frequency_value = $(tr).find('input[name="fav_frequency_value[]"]').val();
    let fav_duration = $(tr).find('input[name="fav_duration[]"]').val();
    let fav_route = $(tr).find('input[name="fav_route[]"]').val();
    let fav_notes = $(tr).find('input[name="fav_notes[]"]').val();
    if (fav_medicine_name != '' && edit_id != '') {

        $('input[name="search_medicine"]').val(fav_medicine_name);
        $('input[name="edit_id"]').val(edit_id);
        $('input[name="search_freq_value_hidden"]').val(frequency_value);
        $('input[name="search_dose"]').val(fav_dose);
        $('input[name="search_quantity"]').val(fav_quantity);
        $('input[name="search_freq_name_hidden"]').val(fav_frequency);
        $('input[name="search_frequency"]').val(fav_frequency);
        $('input[name="search_duration"]').val(fav_duration);
        $('input[name="search_route"]').val(fav_route);
        $('input[name="search_instructions"]').val(fav_notes);

    }
});


$(document).on('click', '.close_btn_fav_med_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".favorite-medicine-list-popup").hide();
});




//user search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_user"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var user_list = $('.user-list-div');

    $(".favorite-pres-list-popup").hide();
    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(user_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/frequency/user-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: search_string,

                },
                beforeSend: function () {
                    $('#UserTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    //alert(data);
                    let response = data;
                    let res_data = "";


                    var search_list = $('#ListUserSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let user_name = response[i].user_name;
                            let user_id = response[i].user_id;

                            res_data += '<tr><td>' + user_name + '</td><input type="hidden" name="list_user_name_hid[]" id="list_user_name_hid-' + i + '" value="' + user_name + '"><input type="hidden" name="list_user_id_hid[]" id="list_user_id_hid-' + i + '" value="' + user_id + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".user_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.user_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});


$(document).on('dblclick', '#ListUserSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".favorite-pres-list-popup").show();
    let _token = $('#c_token').val();
    let tr = $(this);
    let name = $(tr).find('input[name="list_user_name_hid[]"]').val();
    let user_id = $(tr).find('input[name="list_user_id_hid[]"]').val();

    if (name != '' && user_id != '') {
        $('input[name="search_user"]').val(name);
        $('input[name="search_user_id_hidden"]').val(user_id);

        var url = $('#base_url').val() + "/prescription/medicine-favorite-groups";
        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: "load_groups=" + 1 + "&_token=" + _token + '&user_id=' + user_id,
            beforeSend: function () {
                $('#fav_groups').html('<div class="col-md-12 text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></div>');
            },
            success: function (data) {
                if (data.status == 1) {

                    $("#fav_groups").html(data.html);

                }
                $('#search_med_type').show();
                $('#prescription_list').show();

            },
            complete: function () {
                let group_length = $("#fav_groups td").length;

                if (group_length > 0) {
                    let groupid = $('#fav_groups td:first').attr('id');
                    if (groupid == undefined) {
                        grp_id = 0;
                    } else {
                        grp_id = groupid;
                    }
                    listMedicineByGroup(grp_id);
                }

            }
        });

        $(".user-list-div").hide();

    }

});



// list medicines by groups
function listMedicineByGroup(id) {
    clearmedfields();

    var user_id = $("input[name=search_user_id_hidden]").val();
    $('input[name="get_grp_id"]').val(id);
    $('#fav_groups tr#' + id + '').addClass('highlight').siblings().removeClass('highlight');

    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/prescription/medicine-favorite-list";
    $.ajax({
        type: "POST",
        url: url,
        data: 'load_group_items=1&group_id=' + id + '&_token=' + _token + '&user_id=' + user_id,
        success: function (response) {
            if (response.status == 1) {
                $("#ListMedicineFavorites").html(response.html);
            }
            $("body").LoadingOverlay("hide");
            $('input[name="search_medicine"]').focus();
        },
        complete: function (e) {
            $("body").LoadingOverlay("hide");
        }
    });


}


// user search box closing
$(document).on('click', '.close_btn_user_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".user-list-div").hide();
});


$(document).on('click', '.del-med-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="selected_edit_id[]"]').val();

        if (edit_id != '' && edit_id != 0) {
            deleteMedicineRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});


$(document).on('click', '.edit-med-row', function () {
    let tr = $(this).closest('tr');
    let edit_id = $(tr).find('input[name="selected_edit_id[]"]').val();
    let fav_medicine_name = $(tr).find('input[name="selected_item_name[]"]').val();
    let fav_medicine_code = $(tr).find('input[name="selected_item_code[]"]').val();
    let fav_dose = $(tr).find('input[name="selected_item_dose[]"]').val();
    let fav_quantity = $(tr).find('input[name="selected_item_quantity[]"]').val();
    let fav_frequency = $(tr).find('input[name="selected_item_frequency[]"]').val();
    let frequency_value = $(tr).find('input[name="selected_frequency_value[]"]').val();
    let fav_duration = $(tr).find('input[name="selected_item_duration[]"]').val();
    let fav_route = $(tr).find('input[name="selected_item_route[]"]').val();
    let fav_notes = $(tr).find('input[name="selected_item_remarks[]"]').val();
    if (fav_medicine_name != '' && edit_id != '') {

        $('input[name="search_medicine"]').val(fav_medicine_name);
        $('input[name="edit_id"]').val(edit_id);
        $('input[name="search_freq_value_hidden"]').val(frequency_value);
        $('input[name="search_dose"]').val(fav_dose);
        $('input[name="search_quantity"]').val(fav_quantity);
        $('input[name="search_freq_name_hidden"]').val(fav_frequency);
        $('input[name="search_frequency"]').val(fav_frequency);
        $('input[name="search_duration"]').val(fav_duration);
        $('input[name="search_route"]').val(fav_route);
        $('input[name="search_instructions"]').val(fav_notes);

    }
});


$(document).on('click', '.delete-group-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let del_grp_id = $(tr).find('input[name="del_grp_id[]"]').val();

        //  $('table#FavTableList tr.'+del_grp_id+'').remove();
        if (del_grp_id != '' && del_grp_id != 0) {
            deleteGroupRowFromDb(del_grp_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});


function deleteGroupRowFromDb(id) {
    var url = $('#base_url').val() + "/prescription/group-delete-item";
    let _token = $('#c_token').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {

            if (data != '' && data != undefined && data != 0) {
                $('table#FavTableList tr.' + id + '').remove();
                $("#td_1").show();
                $("input[name=get_grp_id]").val("");
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}
