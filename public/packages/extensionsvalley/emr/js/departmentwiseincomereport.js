$(document).ready(function () {
    setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $(".multiselect ").css("width", "100%");
    }, 300);
    $('#date_from').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('#date_to').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

});

document.getElementById("date_from").blur();
document.getElementById("date_to").blur();



function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});



function getReportData() {
    var url = $('#base_url').val() + "/report/getDepartmentWiseIncome";

    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var speciality = $('#speciality').val();
    var insurance = 0;
    if ($('#insurance').is(":checked")) {
        insurance = 1;
    }
    var param = { from_date: from_date, to_date: to_date ,speciality : speciality, insurance : insurance };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#search_results').attr('disabled', true);
            $('#searchresultspin').removeClass('fa fa-search');
            $('#searchresultspin').addClass('fa fa-spinner');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#search_results').attr('disabled', false);
            $('#searchresultspin').removeClass('fa fa-spinner');
            $('#searchresultspin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}

function search_clear() {
    var current_date = $('#current_date').val();
    $('#date_from').val(current_date);
    $('#date_to').val(current_date);
    $('#speciality').val('');
    $('#speciality').select2();
    $("#insurance").prop( "checked", false );
    getReportData();
}