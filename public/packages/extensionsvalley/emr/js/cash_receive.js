function changePaymentMode(mode) {
    $('#payment_mode').val(mode);
    $('#machine_bank').val('');
    $('#bank').val('');
}

var i = 0;
var collection = [];
var collected_amount = 0;
var amount_total = 0;
var balance = 0;

function addNewPaymentMode() {

    var payment_mode = $('#payment_mode').val();
    var payment_mode_name = $('#payment_mode option:selected').text();
    var machine_bank = $('#machine_bank').val();
    var machine_bank_name = $('#machine_bank option:selected').text();
    var bank = $('#bank').val();
    var bank_name = $('#bank option:selected').text();
    var amount = $('#amount').val();
    var card_no = $('#card_no').val();
    var phone_no = $('#phone_no').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();
    var amount_to_be_paid = parseFloat($('#amount_to_be_paid').val());
    var html = '';
    var html = '';
    var append_flag = 0;
    var current_balance_amount = $('#balance_amount').val();
    if (current_balance_amount == '') {
        current_balance_amount = 0;
    }
    current_balance_amount = parseFloat(current_balance_amount);

    if (amount == 0 || amount == '') {
        return false;
    } else {
        amount = parseFloat(amount);
    }
    if (current_balance_amount == 0) {
        current_balance_amount = amount_to_be_paid;
    }
    // console.log(current_balance_amount);

    if (parseFloat(amount) > parseFloat(current_balance_amount)) {
        Command: toastr["warning"]('Amount cannot be greater than current balance amount');
        return false;
    }

    if (payment_mode == '') {
        Command: toastr["warning"]('Please select payment mode!');
        return false;
    }
    // if (payment_mode == '2') {
    //     if (card_no == '') {
    //         Command: toastr["warning"]('Please enter card number!');
    //         return false;
    //     }
    // }

    var PaymentModeMandatory = checkPaymentModeMandatory(payment_mode);
    if (PaymentModeMandatory == false) {
        return false;
    }

    html += '<tr class="payment_mode_row" id="payment_mode_row_' + i + '">';
    html += '<td class="payment_mode">' + payment_mode_name + '</td>';
    html += '<td class="amount">' + amount + '</td>';
    html += '<td class="machine_bank_name">' + machine_bank_name + '</td>';
    html += '<td class="bank_name">' + bank_name + '</td>';
    html += '<td class="card_no">' + card_no + '</td>';
    html += '<td class="exp_date">' + exp_year + '/' + exp_month + '</td>';
    html += '<td class="phone_no">' + phone_no + '</td>';
    html += '<td><button type="button" onclick="deleteCurrentRow(this);" class="btn btn-danger deleteCurrentRow"><i class="fa fa-trash"></i></button></td>';
    html += '</tr>';


    if ($('#payment_mode_table tr').length > 1) {


        $("#payment_mode_table tbody tr").each(function () {
            var mode = $(this).find('td.payment_mode');
            if (mode.html() == payment_mode_name) {
                append_flag = 1;
                var current_amount = $(this).find('td.amount').html();
                var new_amount = parseFloat(current_amount) + parseFloat(amount);
                $(this).find('td.amount').html(new_amount);
            }
        });

        if (append_flag == 0) {
            $('#payment_mode_table').append(html);
        }


    } else {
        $('#payment_mode_table').append(html);
        append_flag = 1;
    }



    collected_amount_old = parseFloat($('#collected_amount').val());
    collected_amount = collected_amount_old + amount;
    $('#collected_amount').val(collected_amount);
    if($('#patient_payback').val()==0){
        $('#collected_amount_hidden').val(collected_amount);
    }
    

    // alert(amount);
    // return;

    balance_amount = amount_to_be_paid - amount;
    amount_total = amount_total + amount;
    balance = current_balance_amount- amount;
    checkBalance();

}

function checkPaymentModeMandatory(payment_mode) {
    var payment_mode_detail = $('#payment_mode_detail').val();
    var card_no = $('#card_no').val();
    var bank = $('#bank').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();

    payment_mode_detail = atob(payment_mode_detail);
    payment_mode_detail = JSON.parse(payment_mode_detail);
    if (payment_mode_detail[payment_mode].is_cardno_mandatory != false) {
        if (card_no == '') {
            Command: toastr["warning"]('Please enter card number!');
            return false;
        }
    }
    if (payment_mode_detail[payment_mode].is_bank_mandatory != false) {
        if (bank == '') {
            Command: toastr["warning"]('Please select bank!');
            return false;
        }
    }
    if (payment_mode_detail[payment_mode].is_expirydate_mandatory != false) {
        if (exp_month == '' || exp_year == '') {
            Command: toastr["warning"]('Please select expiry date!');
            return false;
        }
    }

}


function checkContentLength(id, length) {
    var value = $('#' + id).val();
    if (value.length > length) {
        value = value.substring(0, length);
        $('#' + id).val(value);
    }
    if (id == 'exp_month') {
        if (value > 12 || value < 1) {
            $('#' + id).val('');
        }
    }
    if (id == 'exp_year') {
        if (value > 2099) {
            $('#' + id).val('');
        }
    }
}

function deleteCurrentRow(obj) {
    $(obj).closest('tr').remove();
    var current_amount = $(obj).closest('tr').find('td.amount').html();
    current_amount = parseFloat(current_amount);
    var amount = $('#amount').val();
    amount = parseFloat(amount);
    $('#amount').val(parseFloat(amount) + parseFloat(current_amount));

    var collected_amount = $('#collected_amount').val();
    collected_amount = parseFloat(collected_amount);
    if(collected_amount!=0){
        $('#collected_amount').val(parseFloat(collected_amount) - parseFloat(current_amount));
        $('#collected_amount_hidden').val(parseFloat(collected_amount) - parseFloat(current_amount));
    }

    var balance_amount = $('#balance_amount').val();
    balance_amount = parseFloat($('#amount_to_pay').val())- parseFloat($('#collected_amount').val());
    //alert(current_amount);
    $('#balance_amount').val(balance_amount);

    amount_total = amount_total - current_amount;
    balance = balance_amount;

    var current_payment_mode = $(obj).closest('tr').find('td.payment_mode').html();
    if (current_payment_mode == 'ADVANCE') {
        var advance_balance = $('#advance_balance').val();
        $('#advance_balance').val(parseFloat(advance_balance) + current_amount);
    }
    checkBalance();
}

$('#received_amount').on('keyup', function () {
    var received_amount = $('#received_amount').val();
    var amount_to_be_paid = $('#amount_to_be_paid').val();
    var balance = parseFloat(amount_to_be_paid) - parseFloat(received_amount);
    $('#return_amount').val(balance);
});

function save_bill_payment(save_type) {
    addNewPaymentMode();
    var payment_mode_array = [];
    var amount_array = [];
    var machine_bank_array = [];
    var bank_array = [];
    var card_no_array = [];
    var exp_date_array = [];
    var phone_no_array = [];
    var i = 0;
    var token = $('#c_token').val();
    var is_discharge = $('#is_discharge').val();
    $("#payment_mode_table tbody tr").each(function () {
        var payment_mode = $(this).find('td.payment_mode').html();
        var amount = $(this).find('td.amount').html();
        var machine_bank = $(this).find('td.machine_bank').html();
        var bank = $(this).find('td.bank').html();
        var card_no = $(this).find('td.card_no').html();
        var exp_date = $(this).find('td.exp_date').html();
        var phone_no = $(this).find('td.phone_no').html();

        payment_mode_array[i] = payment_mode != '' ? payment_mode : '';
        amount_array[i] = amount != '' ? amount : 0;
        machine_bank_array[i] = machine_bank != '' ? machine_bank : '';
        bank_array[i] = bank != '' ? bank : '';
        card_no_array[i] = card_no != '' ? card_no : '';
        exp_date_array[i] = exp_date != '' ? exp_date : '';
        phone_no_array[i] = phone_no != '' ? phone_no : '';
        i++;
    });

    var collected_amount = parseFloat($('#collected_amount').val());
    var amount_to_pay = parseFloat($('#amount_to_pay').val());
    if (collected_amount > amount_to_pay) {
        Command: toastr["warning"]('Collected amount is greater than amount to be paid!');
        return false;
    }
    else if (collected_amount < amount_to_pay) {
        Command: toastr["warning"]('Collected amount is less than amount to be paid!');
        return false;
    }
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
    var url = $('#base_url').val() + "/cash_receive/save_cash_receive";
    var bill_amount = $('#total_bill_amount').val();
    var bill_head_id = $('#bill_head_id').val();
    var net_amount = $('#total_net_amount').val();
    var amount_to_be_paid = $('#amount_to_be_paid').val();
    var uhid = $('#uhid').val();
    var is_already_paid = $('#is_already_paid').val();
    var bill_no = $('#bill_no').val();
    var bill_tag = $('#bill_type').val();
    var new_bill_date = $('#new_bill_date').val();
    var edited_advance_amount = parseInt($('#edited_advance_amount').val());
    var dataparams = {
        '_token': token,
        'counter': $('#counter').val(),
        'payment_mode_array': payment_mode_array,
        'amount_array': amount_array,
        'machine_bank_array': machine_bank_array,
        'bank_array': bank_array,
        'card_no_array': card_no_array,
        'exp_date_array': exp_date_array,
        'phone_no_array': phone_no_array,
        'received_amount': $('#received_amount').val(),
        'return_amount': $('#return_amount').val(),
        'collected_amount': $('#collected_amount').val(),
        'balance_amount': $('#balance_amount').val(),
        'amount_total': $('#amount_total').val(),
        'balance': $('#balance').val(),
        'save_type': save_type,
        'bill_head_id': bill_head_id,
        'bill_amount': bill_amount,
        'net_amount': net_amount,
        'already_paid_amount': $('#already_paid_amount').val(),
        'amount_to_be_paid': $('#amount_to_be_paid').val(),
        'advance_balance': $('#advance_balance').val(),
        'location_id': location_id,
        'uhid': uhid,
        'is_discharge': is_discharge,
        'is_already_paid':is_already_paid,
        'bill_tag':bill_tag,
        'bill_no':bill_no,
        'new_bill_date':new_bill_date,
        'edited_advance_amount':edited_advance_amount,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#modalCashReciveBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            var obj = JSON.parse(html);
            // console.log(obj.message[0].visit_id);

            if (obj.status == '1') {
                Command: toastr["success"]('Bill Received Successfully!');
                $('#modalCashRecive').modal('hide');
                $('.search_btn').trigger('click');
                if (save_type == 1) {
                    //--------------Save and print---------------
                    PrintCashRecive(amount_to_be_paid, net_amount, bill_head_id, save_type, obj.receipt, payment_mode_array, bill_amount, uhid, location_id);
                    
                }else if(save_type == 2){
                    PrintSrBill();
                }else {
                    paymentCompleted();
                }

            }
            else {
                var str_msg = '';
                var arr = obj.message;
                for (var i = 0; i < arr.length; i++) {
                    for (var key in arr[i]) {
                        var value = arr[i][key];
                        str_msg += key + " = " + value + '<br>';
                    }
                }


                bootbox.confirm({
                    title: "Error!",
                    message: str_msg,

                    callback: function () {

                    }
                });

            }

        },

        complete: function () {
            $('#modalCashReciveBody').LoadingOverlay("hide");
            var blade_id = $('#blade_id').val();
            if (blade_id == '2') {
                resetAppointmentForm(1, 2, 0);
            }
        },


    });

}

function PrintCashRecive(amount_to_be_paid, net_amount, bill_head_id, save_type, receipt, payment_mode_array, bill_amount, location_id) {
    var url = $('#base_url').val() + "/cash_receive/print_cash_receive";
    var dataparams = {
        '_token': $('#c_token').val(),
        'receipt': receipt,
        'save_type': save_type,
        'payment_mode_array': payment_mode_array,
        'bill_amount': bill_amount,
        'amount_to_be_paid': amount_to_be_paid,
        'net_amount': net_amount,
        'bill_head_id': bill_head_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {

        },
        success: function (html) {

            print_data(html);

            paymentCompleted();

        },
        complete: function () {

        },
    });

}


function print_data(htmlData) {
    //event.stopPropagation();

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    mywindow.document.write('<style>@page{size:portrait;margin:30;margin-left:35px;text-align:center;}@media print {p{margin-top:20px; color:black; }}</style>');

    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:14px !important} .titleclass{text-align:center;padding-top:5px;} .padding-top{padding-top:10px !important;}</style>');

    mywindow.document.write(htmlData + '<script>setTimeout(function(){window.print();window.close(); },1000)</script>');

    return true;
}


function loadCashCollection(bill_id_array, is_discharge = 0,is_already_paid=0,from_reg_ren=0) {
    if (bill_id_array.length > 0) {

        balance = 0;
        i = 0;
        collection = [];
        collected_amount = 0;
        amount_total = 0;

        var token = $("#c_token").val();
        var base_url = $("#base_url").val();
        var dataparams = {
            '_token': token,
            'bill_head_id': bill_id_array,
            'is_discharge': is_discharge,
            'is_already_paid':is_already_paid,
            'from_reg_ren':from_reg_ren,
        };
        $.ajax({
            type: "POST",
            url: base_url + "/cash_receive/cash_receive",
            data: dataparams,
            beforeSend: function () {
                $('#modalCashRecive').modal('show');
                $('#modalCashReciveBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#modalCashReciveBody').html(data);

            },
            complete: function () {
                $('#modalCashReciveBody').LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            },
        });
    }
}

function adjustFromAdvance() {
    var available_advance = parseInt($('#advance_balance').val());
    var advance_adjusting_amt = parseInt($('#advance_adjusting').val());
    var amount_to_pay = parseInt($('#amount').val());
    var balance = 0;
    if (advance_adjusting_amt <= 0 || advance_adjusting_amt == '' || isNaN(advance_adjusting_amt)) {
        Command: toastr["warning"]("No amount available for adjustment.")
        return;
    }
    if (advance_adjusting_amt > amount_to_pay) {
        Command: toastr["warning"]("Advance Adjusting Amount Should Be Less Than Amount To Be Paid");
        $('#advance_adjusting').val('');
        return;
    }
    if (advance_adjusting_amt > available_advance) {
        Command: toastr["warning"]("Advance Adjusting Amount Should Be Less Than Available Advance");
        $('#advance_adjusting').val('');
        return;

    }

    $('#amount').val(advance_adjusting_amt);

    balance = parseInt(available_advance - advance_adjusting_amt);
    $('#advance_balance').val(balance);

    changePaymentMode('8');
    addNewPaymentMode();

    $('#advance_adjusting').val();
}
    function checkBalance(){
        var collected_amount_it=$('#collected_amount_hidden').val().trim();
        var amount_to_pay=$('#amount_to_pay').val();
    console.log(collected_amount_it);
   
        if(parseFloat(collected_amount_it ||collected_amount_it==0)){
            
            var org_collected_amount=$('#collected_amount').val();
            console.log(collected_amount_it);
          
            var balance=parseFloat(amount_to_pay - collected_amount_it);
        
            var org_balance=parseFloat(amount_to_pay - org_collected_amount);
        

            if (Math.sign(balance) === -1) {
                if((Math.sign(org_balance) != -1)){
                   
                    $('#amount').val(org_balance);
                    $('#balance_amount').val(org_balance);
                }else{
                  
                    $('#amount').val(0);
                    $('#balance_amount').val(0);
                }
                console.log(org_balance);
            
                var payback=Math.abs(balance);
                $('#patient_payback').val(payback);
            }else{
                 console.log(org_balance);

                $('#amount').val(balance);
                $('#balance_amount').val(balance);
                $('#patient_payback').val(0);
        
            
            
        }
        }else{
            //console.log('am in');
            
           $('#balance_amount').val(amount_to_pay);
            $('#amount').val(amount_to_pay);
            $('#patient_payback').val(0);
            $('.deleteCurrentRow').click();
        }
    
    }

$(document).on('keyup','.collection_amt',function(){
    checkBalance();
})
    
function PrintSrBill() {
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var printBillId = JSON.parse(atob($('#bill_head_id').val()))[0];
    var bill_tag = 'CONS';
    var package = $('#package_id_hidden').val() ? $('#package_id_hidden').val() : 0;
    var is_duplicate = $("#is_duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var include_hospital_header = 1;
    var param = { is_duplicate: is_duplicate, include_hospital_header: include_hospital_header, package: package, printBillId: printBillId, location_id: location_id, location_name: location_name, bill_tag: bill_tag };
    var base_url = $('#base_url').val();
    if (!printBillId && !bill_tag) {
        toastr.warning('Please select bill....');
        return;
    }
    var url = base_url + "/service_bill/print_bill_detail";
    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function () {
           // $('#popup' + printBillId).hide();
            // $('#print_list_btn').attr('disabled', true);
            // $('#print_list_spin').removeClass('fa fa-print');
            // $('#print_list_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write('<style>@page{size:A5 landscape;} .patient_head{ font-size:10px !important; } </style>' + response + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

        },
        complete: function () {
            // $('#print_status').val('');
            // $('#print_bill_id').val(' ');
            // $('#search_bill_tag').val(' ').select2();
            // $('#package_id').val(' ');
            // $('#print_list_btn').attr('disabled', false);
            // $('#print_list_spin').removeClass('fa fa-spinner fa-spin');
            // $('#print_list_spin').addClass('fa fa-print');
            // $('#serv_print_config_modal').modal('toggle');
        },

    });
}