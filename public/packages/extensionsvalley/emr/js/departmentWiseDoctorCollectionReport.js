$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});


function DepartmentWiseDoctorCollection() {

    var base_url = $('#base_url').val();
    var url = base_url + "/report/getDepartmentWiseDoctorCollectionReportData";

    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var paid_status = $('#paid_status').val();
    var payment_type = $('#payment_type').val();
    var doctor_id = $('#doctor_id').val();
    var department_id = $('#department_id').val();

    var parm = { from_date: from_date, to_date: to_date, paid_status: paid_status, payment_type: payment_type, doctor_id: doctor_id, department_id: department_id };
    $.ajax({
        type: "POST",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');
        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });

}

function datarst() {

    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    $('#paid_status').val('').select2();
    $('#payment_type').val('').select2();
    $('#doctor_id').val('').select2();
    $('#department_id').val('').select2();
    DepartmentWiseDoctorCollection();

}
