$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

var base_url = $("#base_url").val();
var token = $("#c_token").val();

function getReportData() {
    var url = base_url + "/flexy_report/DepartmentwisecollectionrohinihospitalData";
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var doctor_id = $("#doctor_id").val();
    var param = { from_date: from_date, to_date, to_date ,doctor_id:doctor_id}
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });
}

function getDischargeDetail(discharge_id) {
    var to_date = $("#to_date").val();
    var from_date = $("#from_date").val();
    var base_url = $('#base_url').val();
    var uhid = $('#dischrage_uhid' + discharge_id).html();
    var patient_name = $('#dischrage_patientname' + discharge_id).html();
    var url = base_url + '/flexy_report/getDischargeDetail';
    var param = {
        from_date: from_date,
        to_date: to_date,
        discharge_id: discharge_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getDischargeDetailBtn' + discharge_id).attr('disabled', true);
            $('#getDischargeDetailSpin' + discharge_id).removeClass('fa fa-list');
            $('#getDischargeDetailSpin' + discharge_id).addClass('fa fa-spinner fa-spin');
            $('#collectionInnerDetailModelHeader').html(patient_name + ' ( ' + uhid + ' ) ');
        },
        success: function (data) {
            $('#collectionInnerDetailModelDiv').html(data);
            $("#collectionInnerDetailModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);
        },
        complete: function () {
            $('#getDischargeDetailBtn' + discharge_id).attr('disabled', false);
            $('#getDischargeDetailSpin' + discharge_id).removeClass('fa fa-spinner fa-spin');
            $('#getDischargeDetailSpin' + discharge_id).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getDrillDownData(from_type, list_type, list_from,col_type) {
    var to_date = $("#to_date").val();
    var from_date = $("#from_date").val();
    var base_url = $('#base_url').val();
    var doctor_id = $('#doctor_id').val();
    var url = base_url + '/flexy_report/DoctorwiseprocedureincomerohiniDataDeatail';
    var param = {
        from_type: from_type,
        to_date: to_date,
        from_date: from_date,
        list_type: list_type,
        col_type: col_type,
        doctor_id:doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#patient_detail_data_list' + list_type + list_from).attr('disabled', true);
            $('#patient_detail_data_spin' + list_type + list_from).removeClass('fa fa-list');
            $('#patient_detail_data_spin' + list_type + list_from).addClass('fa fa-spinner fa-spin');
            $('#collectionDetailModelHeader').html(from_type);
        },
        success: function (data) {
            $('#collectionDetailModelDiv').html(data);
            $("#collectionDetailModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);
        },
        complete: function () {
            $('#patient_detail_data_list' + list_type + list_from).attr('disabled', false);
            $('#patient_detail_data_spin' + list_type + list_from).removeClass('fa fa-spinner fa-spin');
            $('#patient_detail_data_spin' + list_type + list_from).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function search_clear() {

    var current_date = $('#current_date').val();
    //$(".datapicker").select2('val', '');
    // $(".datapicker").trigger("change");
    $(".datapicker").val([]).change();

    $('#admission_date_from').val(current_date);
    $('#admission_date_to').val(current_date);
    $('#op_no_hidden').val('');
    $('#op_no').val('');
    $('input:checkbox').removeAttr('checked');
}
