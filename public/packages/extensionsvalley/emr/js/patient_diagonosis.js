$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    }

                    else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getPatientDiagonosisData() {

    var url = route_json.SearchPatientDiagonosis;
    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var flag = 1;
    var diagonosises = $('#diagonosis_hidden').val();

    var status = $('#checkbox1').is(":checked");
    if (status) {
        if (!from_date || !to_date) {
            toastr.warning("Please enter From Date And To Date");
            flag = 0;
        }
    } else if (diagonosises == '0') {
        toastr.warning("Please Enter Diagonosis");
        flag = 0;
    }
    var parm = {
        from_date: from_date, status: status, to_date: to_date, diagonosises: diagonosises
    };
    if (flag == 1) {
        $.ajax({
            type: "POST",
            url: url,
            data: parm,
            beforeSend: function () {
                $('#ResultDataContainer').css('display', 'block');
                $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (html) {
                $('#ResultsViewArea').html(html);
                $('#print_results').removeClass('disabled');
                $('#csv_results').removeClass('disabled');
            },
            complete: function () {
                $('#ResultsViewArea').LoadingOverlay("hide");
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                $('#ResultDataContainer').css('display', 'hide');
                return;
            }
        });
    }
}

function hideShowDateFilters() {
    var status = $('#checkbox1').is(":checked");
    if (status) {
        var current_date = $('#current_date').val();
        $('#date_from').val(current_date);
        $('#date_to').val(current_date);
        $('#date_from').attr('disabled', false);
        $('#date_to').attr('disabled', false);
    } else {
        $('#date_from').val('');
        $('#date_to').val('');
        $('#date_from').attr('disabled', true);
        $('#date_to').attr('disabled', true);
    }


}
function datarst() {

    $('.filter_label1').hide();
    $('#date_from').hide();
    $('#date_to').hide();
    $('.ajaxSearchBox').hide();
    $('#diagonosis_hidden').val('');
    $('#diagonosis').val('');
}
