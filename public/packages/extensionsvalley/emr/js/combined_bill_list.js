$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $('#bill_no').focus();
    var bill_type = $('#bill_type').val();

    // if (bill_type == 'PH') {
    //     getPharmacyBillList();
    // }
    getCombinedBillList();

});
$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
$(document).on('click', '.advanceSearchBtn', function (event) {
    advancePatientSearch(1);
});
//----Hidden Filed Search--------------------------------------------------------------------
var token = $("#c_token").val();
var base_url = $("#base_url").val();
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("").hide();
            $('#' + input_id + '_hidden').val('');
        } else {
            var url = base_url + "/billlist/setAjaxSearch";
            var bill_no = $('#bill_no').val();
            var bill_type = $('#bill_type').val();
            var param = { bill_no: bill_no, search_key: search_key, search_key_id: input_id, bill_type: bill_type };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function fillSearchDetials(id, name, serach_key_id) {
    name = htmlDecode(name);
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function addserviceBill() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/service_bill";
    document.location.href = url;
}

function getCombinedBillList() {
    var bill_type = $('#bill_type').val();
        $('#bill_tag_field').css('display', 'block');
        // $('#ip_field').css('display', 'none');
        $('#ip_field').css('display', 'block');
        $('#addnew').css('display', 'block');
        $('#addnew').css('display', 'block');
        $('#pharmacybilllistResetBtn').css('display', 'block');
    var include_in_discharge = $('#include_in_discharge').is(":checked");
    var paid_bill = $('#paid_bill').is(":checked");
    var Unpaid_bill = $('#Unpaid_bill').is(":checked");
    var cancelled_bill = $('#cancelled_bill').is(":checked");
    var full_return_bill = $('#full_return_bill').is(":checked");
    var partially_return_bill = $('#partially_return_bill').is(":checked");
    var Pending_request_bill = $('#Pending_request_bill').is(":checked");
    var bill_no = $('#bill_no').val();
    var bill_tag = $('#bill_tag').val();
    var payment_type = $('#payment_type').val();
    var prepared_by = $('#prepared_by').val();
    var location = $('#location').val();
    var item_desc = $('#item_desc_hidden').val();
    var visit_type = $('#visit_type').val();
    var patient = $('#patient_hidden').val();
    var uhid = $('#patient_uhid').val();
    var ip_no = $('#ip_no').val();
    var company = $('#company').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var params = {
        Pending_request_bill: Pending_request_bill,
        partially_return_bill: partially_return_bill,
        full_return_bill: full_return_bill,
        cancelled_bill: cancelled_bill,
        Unpaid_bill: Unpaid_bill,
        paid_bill: paid_bill,
        include_in_discharge: include_in_discharge,
        bill_no: bill_no,
        bill_tag: bill_tag,
        payment_type: payment_type,
        prepared_by: prepared_by,
        location: location,
        item_desc: item_desc,
        visit_type: visit_type,
        patient: patient,
        uhid: uhid,
        ip_no: ip_no,
        company: company,
        from_date: from_date,
        to_date: to_date,
        bill_type: bill_type
    };
    var url = base_url + "/combined_bill_list/getCombinedBillList";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $('#pharmacy_bill_data').html(' ');
            $('#pharmacy_bill_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#pharmacybilllistBtn').attr('disabled', true);
            $("#pharmacybilllistspin").removeClass("fa fa-search");
            $("#pharmacybilllistspin").addClass("fa fa-spinner fa-spin");

        },
        success: function (data) {
            if (data) {
                $('#pharmacy_bill_data').html(data);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            }

        },
        complete: function () {
            $('#pharmacybilllistBtn').attr('disabled', false);
            $('#pharmacy_bill_data').LoadingOverlay("hide");
            $("#pharmacybilllistspin").removeClass("fa fa-spinner fa-spin");
            $("#pharmacybilllistspin").addClass("fa fa-search");

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}
$('#bill_noAjaxDiv').on('click', 'li', function () {
    var bill_type = $('#bill_type').val();
    if (bill_type == 'PH') {
        getDetailsFromBillNum();
    }
});

function getDetailsFromBillNum() {
    var url = base_url + "/billlist/getDetailsFromBillNum";
    var bill_no = $('#bill_no').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { bill_no: bill_no },
        beforeSend: function () {
            $('#filter_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (data) {
            if (data) {
                $('#payment_type').val(data[0].payment_type).select2();
                $('#prepared_by').val(data[0].created_by).select2();
                $('#location').val(data[0].location).select2();
                $('#company').val(data[0].company_id).select2();
                $('#patient').val(data[0].patient_name);
                $('#patient_hidden').val(data[0].patient_id);
                $('#uhid').val(data[0].uhid);
                $('#ip_no').val(data[0].ip_no);
                $('#visit_type').val(data[0].visit_status);
            }

        },
        complete: function () {
            $('#filter_area').LoadingOverlay("hide")

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });


}

function reset() {
    $('.reset').val('');
    $('#location').val('').select2();
    $('#prepared_by').val('').select2();
    $('#payment_type').val('').select2();
    $('#company').val('').select2();
    $('#bill_no').focus();
    $('.checkit').prop('checked', false);
    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    $('#bill_tag').val('');
    $('#item_desc_hidden').val('');
    getPharmacyBillList();
}

function displayBillDetails(id, bill_tag, package_id) {
    var head_id = $('#head_id_hidden').val();
    if (head_id != id) {
        $('#popup' + head_id).hide();
    }
    $('#head_id_hidden').val(id);
    $('#bill_tag_hidden').val(bill_tag);
    $('#package_id_hidden').val(package_id);
    if ($('#popup' + id).is(":visible")) {

        $('#popup' + id).hide();
    } else if ($('#popup' + id).is(":hidden")) {
        $('#popup' + id).show();
    }

    $('#ppclose' + id).click(function () {
        $('#popup' + id).hide();
    });


}

function displayCancelBillStatus(head_id) {
    $('#cancelBillSetup').modal('show');
    $('#head_id_hidden').val(head_id);
    var url = base_url + "/billlist/displayCancelBillStatus";
    $.ajax({
        type: "POST",
        url: url,
        data: { head_id: head_id },
        beforeSend: function () {
            $('#cancelBillSetupBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#cancel_bill').val(' ');
            $('#cancel_patient').val(' ');
            $('#cancel_paytype').val(' ');
            $('#cancel_by').val(' ');
            $('#Cancel_reason').val(' ');

        },
        success: function (data) {
            if (data) {
                $('#cancel_bill').val(data[0].bill_no);
                $('#cancel_patient').val(data[0].patient_name);
                $('#cancel_paytype').val(data[0].payment_type);
                var user_name = $('#user_name').val();
                $('#cancel_by').val(user_name);

            }

        },
        complete: function () {
            $('#cancelBillSetupBody').LoadingOverlay("hide")

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

function cancelBill() {
    var Cancel_reason = $('#Cancel_reason').val();
    var head_id = $('#head_id_hidden').val();
    if (head_id != 0) {
        var url = base_url + "/billlist/cancelBill";
        $.ajax({
            type: "POST",
            url: url,
            data: { head_id: head_id, Cancel_reason: Cancel_reason },
            beforeSend: function () {
                $('#Cancel').attr('disabled', true);
                $('#Cancel_spin').removeClass('fa fa-trash');
                $('#Cancel_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (data) {
                    toastr.success('Bill cancelled successfully.');
                    $('#cancel' + head_id).hide();
                    $('#cancelBillSetup').modal('hide');
                    $('#cancel_bg' + head_id).addClass('cancelled-bg');
                    $('#paid_bg' + head_id).removeClass('unpaid-bg');
                    $('#paid_bg' + head_id).removeClass('pending-requested-bg');
                    $('#popup' + head_id).hide();

                }


            },
            complete: function () {
                $('#Cancel').attr('disabled', false);
                $('#Cancel_spin').removeClass('fa fa-spinner fa-spin');
                $('#Cancel_spin').addClass('fa fa-trash');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            },
        });
    }


}
$('.checkit').click(function () {
    var name = $(this).attr('name');

    if ($(this).hasClass('checked')) {
        $(this).prop('checked', false);
        $(this).removeClass('checked');
        getPharmacyBillList();
    } else {
        $('input[name="' + name + '"]').removeClass('checked');
        $(this).addClass('checked');
        getPharmacyBillList();
    }
});

function printData() {
    $('#print_config_modal').modal('toggle');
}

function printBillDetails(bill_id = 0) {

    if (bill_id == 0) {
        bill_id = $("#head_id_hidden").val();
    }
    var include_hospital_header = $("#showTitle").prop('checked') ? 1 : 0;
    var is_duplicate = $("#duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var printMode = $('input[name=printMode]:checked').val();

    var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var url = $('#base_url').val() + "/pharmacy/printBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
            location_code: location_code,
            include_hospital_header: include_hospital_header,
            is_duplicate: is_duplicate,
            printMode: printMode
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {

            if (data.status == 1) {
                if (printMode == 1) {
                    var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data.print_data + '<script>setTimeout(function(){window.print();window.close()},1000)</script>');
                } else {
                    var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 landscape;} .patient_head{ font-size:10px !important; } </style>' + data.print_data + '<script>setTimeout(function(){window.print();window.close()},1000)</script>');
                }
            } else {
                toastr.success('Bill print success.');
            }

            // window.location.reload();
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}

function displayPendingBillStatus(head_id) {
    $('#pendinBill').modal('show');
    $('#head_id_hidden').val(head_id);
    var url = base_url + "/billlist/displayPendingBillStatus";
    $.ajax({
        type: "POST",
        url: url,
        data: { head_id: head_id },
        beforeSend: function () {
            $('#pendingBillSetupBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $('#pending_bill').val(' ');
            $('#pending_patient').val(' ');
            $('#pending_paytype').val(' ');
            $('#pending_by').val(' ');
            $('#Pending_reason').val(' ');

        },
        success: function (data) {
            if (data) {
                $('#pending_bill').val(data[0].bill_no);
                $('#pending_patient').val(data[0].patient_name);
                $('#pending_paytype').val(data[0].payment_type);
                var user_name = $('#user_name').val();
                $('#pending_by').val(user_name);

            }

        },
        complete: function () {
            $('#pendingBillSetupBody').LoadingOverlay("hide")

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}



function pendingBill() {
    var Pending_reason = $('#Pending_reason').val();
    var head_id = $('#head_id_hidden').val();
    if (head_id != 0) {
        var url = base_url + "/billlist/pendingBill";
        $.ajax({
            type: "POST",
            url: url,
            data: { head_id: head_id, Pending_reason: Pending_reason },
            beforeSend: function () {
                $('#pending').attr('disabled', true);
                $('#pending_spin').removeClass('fa fa-save');
                $('#pending_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (data) {
                    if (data != 0) {
                        toastr.success('Bill pending requested successfully.');
                    } else {
                        toastr.success('Pending done successfully ');
                    }

                    $('#pending' + head_id).hide();
                    $('#pendinBill').modal('hide');
                    $('#paid_bg' + head_id).removeClass('paid-bg');
                    $('#paid_bg' + head_id).addClass('pending-requested-bg');

                    $('#popup' + head_id).hide();

                }
            },
            complete: function () {
                $('#pending').attr('disabled', false);
                $('#pending_spin').removeClass('fa fa-spinner fa-spin');
                $('#pending_spin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            },
        });
    }


}

$(document).on('click', '#print_serv_bill', function (event) {
    if( $('#package_id_hidden').val().trim()!=0){
        $('#detail_summary_print').show();
    }else{
        $('#detail_summary_print').hide();

    }
    $('#serv_print_config_modal').modal('toggle');
});

function PrintBill() {
    var printMode = $('input[name=printMode]:checked').val();
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var printBillId = $('#head_id_hidden').val();
    var bill_tag = $('#bill_tag_hidden').val();
    var package = $('#package_id_hidden').val() ? $('#package_id_hidden').val() : 0;
    var is_duplicate = $("#is_duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var include_hospital_header = $("#titleShow").prop('checked') ? 1 : 0;
    var detail_print = $("#detail_print").prop('checked') ? 1 : 0;
    var summary_print = $("#summary_print").prop('checked') ? 1 : 0;
    var param = { detail_print:detail_print,summary_print:summary_print,is_duplicate: is_duplicate, include_hospital_header: include_hospital_header, package: package, printBillId: printBillId, location_id: location_id, location_name: location_name, bill_tag: bill_tag };
    var base_url = $('#base_url').val();
    if (!printBillId && !bill_tag) {
        toastr.warning('Please select bill....');
        return;
    }
    var url = base_url + "/service_bill/print_bill_detail";
    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function () {
            $('#popup' + printBillId).hide();
            $('#print_list_btn').attr('disabled', true);
            $('#print_list_spin').removeClass('fa fa-print');
            $('#print_list_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');

            if (printMode == 1) {
                winPrint.document.write('<style>@page{size:A5 portrait;} .patient_head{ font-size:10px !important; } </style>' + response + '<script>setTimeout(function(){window.print();},1000)</script>');
            } else {
            winPrint.document.write('<style>@page{size:A5 landscape;} .patient_head{ font-size:10px !important; } </style>' + response + '<script>setTimeout(function(){window.print();},1000)</script>');
            }

        },
        complete: function () {
            $('#print_status').val('');
            $('#print_bill_id').val(' ');
            $('#search_bill_tag').val(' ').select2();
            $('#package_id').val(' ');
            $('#print_list_btn').attr('disabled', false);
            $('#print_list_spin').removeClass('fa fa-spinner fa-spin');
            $('#print_list_spin').addClass('fa fa-print');
            $('#serv_print_config_modal').modal('toggle');
        },

    });
}

function reprintRegistrationBill(head_id, visit_id) {
    var url = base_url + "/master/reprintRegistrationBill";
    $.ajax({
        type: "POST",
        url: url,
        data: { visit_id: visit_id },
        beforeSend: function () {
            $('#popup' + head_id).hide();
            $('#pharmacy_bill_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if (data) {
                toastr.success("Print successfully queued");
            } else {
                toastr.warning("Bill Not Found");
            }
        },
        complete: function () {
            $('#print_status').val('');
            $('#print_bill_id').val(' ');
            $('#search_bill_tag').val(' ').select2();
            $('#package_id').val(' ');
            $('#pharmacy_bill_data').LoadingOverlay("hide")

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}

$('#pay_bill_btn').on('click', function () {
    bill_id_array = [];
    $(".check_bill").each(function () {
        if ($(this).is(":checked")) {
            var uhid = $(this).attr('data-uhid');
            var payment_type = $(this).attr('data-payment-type');

            var bill_id = $(this).val();
            bill_id_array.push(bill_id);
        }
    });
   // loadCashCollection(bill_id_array);
    loadCashCollection(bill_id_array, 0, 0, 1);

});

function checkSameUhid(uhid, id) {
    $(".check_bill").each(function () {
        if ($(this).is(":checked")) {
            var checked_uhid = $(this).attr('data-uhid');
            if (uhid != checked_uhid) {
                $('.checked_bill' + id).prop('checked', false);
                toastr.warning('Please select same bill of patient')
                return;

            }
        }
    });
}

function paymentCompleted() {

};
