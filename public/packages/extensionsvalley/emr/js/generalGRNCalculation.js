function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    return number.toFixed(parseInt(decimalConfiguration));
}

function validateText(row_id) {
    $(".active_class" + row_id).attr('disabled', false);
    $(".active_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        } else if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        } else if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        } else if (!val) {
            $(this).val(0);
            flag = 1;
        }
        if ($(this).hasClass("percentage_class" + row_id)) {
            if (parseFloat(val) > 100) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            }
        }
        if (flag == 0) {
            $(this).val(checkIsNaN(val));
        }
    });

    $(".number_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        }
        if (!val) {
            $(this).val(0);
            flag = 1;
        }
    });
}

function insertGeneralGrnCalculation(item_list, tax_array, charge_type, row_id, from_type) {

    $.each(item_list, function (index, value) {
        var bill_detail_id = value.bill_detail_id ? value.bill_detail_id : 0;
        var item_id = value.item_id ? value.item_id : 0;
        var item_id_array = bill_detail_id + '::' + item_id;
        var item_type = value.item_type ? value.item_type : 0;
        var hsn_code = value.hsn_code ? (value.hsn_code).trim() : '';
        var item_code = value.item_code ? value.item_code : '';
        var rate = value.grn_rate ? value.grn_rate : 0;
        var unit_mrp = value.unit_mrp ? value.unit_mrp : 0;
        var uom_val = value.uom_val ? value.uom_val : 0;
        var sales_rate_mrp_check = value.sales_rate_mrp_check ? value.sales_rate_mrp_check : 0;
        var is_free = value.is_free ? value.is_free : 0;
        var grn_qty = value.grn_qty ? value.grn_qty : 0;
        var free_qty = value.grn_free_qty ? value.grn_free_qty : 0;
        var batch_no = value.batch_no ? value.batch_no : '';
        var expiry_date = value.expiry_date ? value.expiry_date : '';
        var item_unit = value.grn_unit ? value.grn_unit : 0;
        var po_id = value.po_id ? value.po_id : 0;
        var discount_type = value.discount_type ? value.discount_type : 0;
        var grn_tax_total_rate = value.grn_tax_total_rate ? value.grn_tax_total_rate : 0;
        var item_desc = value.item_desc ? (value.item_desc).trim() : '';
        var tot_rate = grn_qty * rate;
        var tax_single_item = [];

        if (parseInt(from_type) == 1) {
            tax_single_item = tax_array[item_id] ? tax_array[item_id] : [];
        } else if (parseInt(from_type) == 2) {
            tax_single_item = tax_array[item_id_array] ? tax_array[item_id_array] : [];
        }

        var charge_single_item = charge_type[item_id_array] ? charge_type[item_id_array] : [];
        var tax_details = calculateListNetTotal(tot_rate, tax_single_item, charge_single_item);

        var cgst_per = tax_details['cgst_per'];
        var cgst_amt = tax_details['cgst_amt'];
        var sgst_per = tax_details['sgst_per'];
        var sgst_amt = tax_details['sgst_amt'];
        var igst_per = tax_details['igst_per'];
        var igst_amt = tax_details['igst_amt'];
        var discount_per = tax_details['discount_per'];
        var discount_amt = tax_details['discount_amt'];

        var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
        var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
        var total_othercharges = 0;
        var total_discount = parseFloat(discount_amt);
        var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(total_discount);

        var item_rates = getItemPrice(rate, grn_qty, free_qty, uom_val, net_amt, total_tax_amt);

        var selling_price = item_rates.selling_price;
        var unit_rate = item_rates.unit_rate;
        var unit_cost = item_rates.unit_cost;
        var total_qty = item_rates.total_qty;
        var tot_free_qty = item_rates.tot_free_qty;
        var all_total_qty = item_rates.all_total_qty;
        var unit_cost_with_out_tax = item_rates.unit_cost_with_out_tax;

        item_array[row_id] = {
            bill_detail_id: bill_detail_id,
            po_id: po_id,
            item_id: item_id,
            item_type: item_type,
            hsn_code: hsn_code,
            batch_no: batch_no,
            expiry_date: expiry_date,
            item_code: item_code,
            item_desc: item_desc,
            grn_qty: grn_qty,
            item_unit: item_unit,
            unit_cost: unit_cost,
            unit_cost_with_out_tax: unit_cost_with_out_tax,
            uom_val: uom_val,
            free_qty: free_qty,
            is_free: is_free,
            total_qty: total_qty,
            tot_free_qty: checkIsNaN(tot_free_qty),
            all_total_qty: all_total_qty,
            sales_rate_mrp_check: sales_rate_mrp_check,
            unit_mrp: checkIsNaN(unit_mrp),
            item_rate: checkIsNaN(rate),
            unit_rate: checkIsNaN(unit_rate),
            selling_price: checkIsNaN(selling_price),
            cgst_per: checkIsNaN(cgst_per),
            cgst_amt: checkIsNaN(cgst_amt),
            sgst_per: checkIsNaN(sgst_per),
            sgst_amt: checkIsNaN(sgst_amt),
            igst_per: checkIsNaN(igst_per),
            igst_amt: checkIsNaN(igst_amt),
            total_tax_perc: checkIsNaN(grn_tax_total_rate),
            total_tax_amt: checkIsNaN(total_tax_amt),
            discount_per: checkIsNaN(discount_per),
            discount_amt: checkIsNaN(discount_amt),
            total_discount: checkIsNaN(total_discount),
            discount_type: discount_type,
            total_othercharges: checkIsNaN(total_othercharges),
            totalRate: checkIsNaN(tot_rate),
            netRate: checkIsNaN(net_amt)
        }
        updateGrnList(row_id);
        row_id++;
    });
    return row_id;
}



function updateGrnList(row_id) {
    var batch = '';
    var expiry_date = '';
    var grn_qty = 0;
    var free_qty = 0;
    var tot_qty = 0;
    var unit_rate = 0;
    var total_tax_amt = 0;
    var sgst_amt = 0;
    var cgst_amt = 0;
    var discount_per = 0;
    var discount_amt = 0;
    var discounttype = 0;
    var netRate = 0;
    var total_tax_perc = 0;
    var is_igst = $('#is_igstcheck').is(":checked");
    if (item_array[row_id]) {
        batch = item_array[row_id].batch_no ? item_array[row_id].batch_no : '';
        expiry_date = item_array[row_id].expiry_date ? item_array[row_id].expiry_date : '';
        grn_qty = item_array[row_id].grn_qty ? item_array[row_id].grn_qty : 0;
        free_qty = item_array[row_id].free_qty ? item_array[row_id].free_qty : 0;
        tot_qty = item_array[row_id].all_total_qty ? item_array[row_id].all_total_qty : 0;
        unit_rate = item_array[row_id].unit_rate ? item_array[row_id].unit_rate : 0;
        discounttype = item_array[row_id].discount_type ? item_array[row_id].discount_type : 0;
        total_tax_perc = item_array[row_id].total_tax_perc ? item_array[row_id].total_tax_perc : 0;
        total_tax_amt = item_array[row_id].total_tax_amt ? item_array[row_id].total_tax_amt : 0;
        sgst_amt = item_array[row_id].sgst_amt ? item_array[row_id].sgst_amt : 0;
        cgst_amt = item_array[row_id].cgst_amt ? item_array[row_id].cgst_amt : 0;
        discount_per = item_array[row_id].discount_per ? item_array[row_id].discount_per : 0;
        discount_amt = item_array[row_id].discount_amt ? item_array[row_id].discount_amt : 0;
        netRate = item_array[row_id].netRate ? item_array[row_id].netRate : 0;
    }

    $('#batch_row' + row_id).val(batch);
    $('#expiry_date' + row_id).val(expiry_date);
    $('#grn_qty' + row_id).val(checkIsNaN(grn_qty));
    $('#free_qty' + row_id).val(checkIsNaN(free_qty));
    $('#tot_qty_' + row_id).val(checkIsNaN(tot_qty));
    if (parseInt(discounttype) != 0 && discounttype) {
        if (parseInt(discounttype) == 1) {
            $('#discount_type' + row_id).val(1);
            $('#tot_dic_amt_' + row_id).val(checkIsNaN(discount_per));
        } else if (parseInt(discounttype) == 2) {
            $('#discount_type' + row_id).val(2);
            $('#tot_dic_amt_' + row_id).val(checkIsNaN(discount_amt));
        }
    }
    if (parseInt(total_tax_perc) != 0 && total_tax_perc) {
        $('#tax_type' + row_id).val(parseInt(total_tax_perc));
    }

    if (!is_igst) {
        $('#sgst_tax' + row_id).val(checkIsNaN(sgst_amt));
        $('#cgst_tax' + row_id).val(checkIsNaN(cgst_amt));
    } else {
        $('#sgst_tax' + row_id).val(checkIsNaN(0));
        $('#cgst_tax' + row_id).val(checkIsNaN(0));
    }

    $('#unit_rate' + row_id).val(checkIsNaN(unit_rate));
    $('#tot_tax_amt_' + row_id).val(checkIsNaN(total_tax_amt));
    $('#net_rate' + row_id).val(checkIsNaN(netRate));
    getAllItemsTotals();
}



function calculateListNetTotal(tot_rate, tax_array, charge_type) {
    var tot_amt = 0.0;
    detailtax_array = [];
    detailtax_array['cgst_per'] = 0.0;
    detailtax_array['cgst_amt'] = 0.00;
    detailtax_array['sgst_per'] = 0.0;
    detailtax_array['sgst_amt'] = 0.00;
    detailtax_array['igst_per'] = 0.0;
    detailtax_array['igst_amt'] = 0.00;
    detailtax_array['discount_per'] = 0.00;
    detailtax_array['discount_amt'] = 0.00;

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        charge_type_val = 1;
        if (charge_type[parseInt(index)]) {
            charge_type_val = charge_type[parseInt(index)];
        }
        if (parseInt(index) == 1) {
            detailtax_array['discount_per'] = value;
            detailtax_array['discount_amt'] = amount;
            tot_amt -= parseFloat(amount);
            tot_rate = parseFloat(tot_rate) - parseFloat(amount);
        }
    });

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        if (parseInt(index) == 11) {
            detailtax_array['Freight_Charges_per'] = value;
            detailtax_array['flightcharge_amt'] = amount;
            tot_amt += parseFloat(amount);
        }

        if (parseInt(index) == 10) {
            detailtax_array['cgst_per'] = value;
            detailtax_array['cgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 9) {
            detailtax_array['sgst_per'] = value;
            detailtax_array['sgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 12) {
            detailtax_array['igst_per'] = value;
            detailtax_array['igst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
    });

    console.log(detailtax_array);
    return detailtax_array;
}


function calculateListAmount(row_id, from_type) {
    validateText(row_id);
    var qty = $('#grn_qty' + row_id).val();
    var uom_vlaue = $('#uom_vlaue' + row_id).val();
    var free_qty = $('#free_qty' + row_id).val();
    var item_price = $('#unit_rate' + row_id).val();
    var discount_amt = 0.0;
    var discount_per = 0.0;
    var without_discount = 0.0;
    var total_qty = parseFloat(uom_vlaue) * ((parseFloat(qty) + parseFloat(free_qty)));
    if (!item_price) {
        item_price = 0;
        $('#unit_rate' + row_id).val(0);
    }
    var discountAmountType = $('#discount_type' + row_id).val();
    $('#tot_qty_' + row_id).val(checkIsNaN(total_qty));

    var gst_amt = 0.0;
    var amount = 0.0;
    var tot_rate = qty * item_price;
    var discount_amt = $('#tot_dic_amt_' + row_id).val();
    if (!discount_amt || (parseFloat(discount_amt) > parseFloat(tot_rate))) {
        discount_amt = 0;
    }
    gst_amt = $('#tot_dic_amt_' + row_id).val();
    if (!gst_amt) {
        gst_amt = 0;
    }
    if (parseInt(discountAmountType) == 1) {
        if (parseFloat(gst_amt) > 100) {
            gst_amt = 0;
            discount_per = 0.0;
            $('#tot_dic_amt_' + row_id).val(checkIsNaN(0));
        }
        amount = (gst_amt * tot_rate) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        discount_amt = amount;
        discount_per = gst_amt;
    } else if (parseInt(discountAmountType) == 2) {
        if (parseFloat(gst_amt) >= parseFloat(tot_rate)) {
            gst_amt = 0;
            $('#tot_dic_amt_' + row_id).val(checkIsNaN(0));
        }
        discount_amt = gst_amt;
        if (tot_rate == '' || parseFloat(tot_rate) == 0) {
            amount = 0.0;
        } else {
            amount = (100 * gst_amt) / tot_rate;
            if (amount === Infinity) {
                amount = 0;
            }
        }
        discount_per = amount;
    }
    var without_discount = parseFloat(tot_rate) - parseFloat(discount_amt);

    gst_amt = $('#tax_type' + row_id).val();
    if (parseFloat(gst_amt) > 100) {
        gst_amt = 0;
        $('#tax_type' + row_id).val('');
    }
    if (!gst_amt) {
        gst_amt = 0;
    }
    amount = (gst_amt * without_discount) / 100;
    if (amount === Infinity) {
        amount = 0;
    }
    $('#tot_tax_amt_' + row_id).val(checkIsNaN(amount));
    var is_igst = $('#is_igstcheck').is(":checked");
    if (!is_igst) {
        item_taxsplit=(checkIsNaN(amount) / 2);
        $('#sgst_tax' + row_id).val(checkIsNaN(item_taxsplit));
        $('#cgst_tax' + row_id).val(checkIsNaN(item_taxsplit));
    } else {
        $('#sgst_tax' + row_id).val(checkIsNaN(0));
        $('#cgst_tax' + row_id).val(checkIsNaN(0));
    }
    var net_rate = parseFloat(without_discount) + parseFloat(amount);
    $('#net_rate' + row_id).val(checkIsNaN(net_rate));
    updateItemArray(row_id, net_rate, amount, discount_amt, discount_per);
}



function updateItemArray(row_id, net_rate, total_tax, discount_amt, discount_per) {
    var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
    var po_id = $('#po_detail_id_hidden' + row_id).val();
    var item_id = $('#item_id_hidden' + row_id).val();
    var batch = $('#batch_row' + row_id).val();
    var expiry_date = $('#expiry_date' + row_id).val();
    var item_code = $('#item_code_hidden' + row_id).val();
    var item_desc = $('#item_desc_' + row_id).val();
    var grn_qty = $('#grn_qty' + row_id).val();
    var free_qty = $('#free_qty' + row_id).val();
    var item_unit = $('#uom_select_id_' + row_id).val();
    var item_rate = $('#unit_rate' + row_id).val();
    var uom_value = $('#uom_vlaue' + row_id).val();
    var tot_rate = grn_qty * item_rate;
    var is_free = $('#is_freecheck' + row_id).is(":checked");
    var is_igst = $('#is_igstcheck' + row_id).is(":checked");
    var tax_type = $('#tax_type' + row_id).val();
    var discount_type = $('#discount_type' + row_id).val();
    var sgst_amt = 0;
    var cgst_amt = 0;
    var sgst_per = 0;
    var cgst_per = 0;
    var igst_per = 0;
    var igst_amt = 0;
    if (!is_igst) {
        if (tax_type) {
            var tax_division = $('#tax_type' + row_id + ' option:selected').attr('attt-id');
            sgst_per = checkIsNaN(tax_division);
            cgst_per = checkIsNaN(tax_division);
            sgst_amt = checkIsNaN(total_tax) / 2;
            cgst_amt = checkIsNaN(total_tax) / 2;
        }
    } else {
        if (tax_type) {
            igst_per = tax_type;
            igst_amt = $('#tot_tax_amt_' + row_id).val();
        }
    }

    var item_full_rates = getItemPrice(item_rate, grn_qty, free_qty, uom_value, net_rate, total_tax);

    var unit_rate = item_full_rates.unit_rate;
    var unit_cost = item_full_rates.unit_cost;
    var total_qty = item_full_rates.total_qty;
    var tot_free_qty = item_full_rates.tot_free_qty;
    var all_total_qty = item_full_rates.all_total_qty;
    var unit_cost_with_out_tax = item_full_rates.unit_cost_with_out_tax;
    var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
    var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
    var total_othercharges = 0;
    var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(discount_amt);

    item_array[row_id] = {
        bill_detail_id: bill_detail_id,
        po_id: po_id,
        item_id: item_id,
        item_type: 0,
        hsn_code: '',
        batch_no: batch,
        expiry_date: expiry_date,
        item_code: item_code,
        item_desc: item_desc,
        grn_qty: grn_qty,
        item_unit: item_unit,
        unit_cost: unit_cost,
        unit_cost_with_out_tax: unit_cost_with_out_tax,
        uom_val: uom_value,
        free_qty: free_qty,
        is_free: is_free ? 1 : 0,
        total_qty: total_qty,
        tot_free_qty: checkIsNaN(tot_free_qty),
        all_total_qty: all_total_qty,
        sales_rate_mrp_check: 0,
        discount_type: discount_type,
        grn_mrp: 0,
        unit_mrp: 0,
        item_rate: checkIsNaN(item_rate),
        unit_rate: checkIsNaN(unit_rate),
        cgst_per: checkIsNaN(cgst_per),
        cgst_amt: checkIsNaN(cgst_amt),
        sgst_per: checkIsNaN(sgst_per),
        sgst_amt: checkIsNaN(sgst_amt),
        igst_per: checkIsNaN(igst_per),
        igst_amt: checkIsNaN(igst_amt),
        total_tax_perc: checkIsNaN(total_tax_perc),
        total_tax_amt: checkIsNaN(total_tax_amt),
        discount_per: checkIsNaN(discount_per),
        discount_amt: checkIsNaN(discount_amt),
        total_othercharges: checkIsNaN(total_othercharges),
        totalRate: checkIsNaN(tot_rate),
        netRate: checkIsNaN(net_amt)
    }
    getAllItemsTotals();
}


function getItemPrice(rate, grn_qty, free_qty, uom_unit, net_amt, total_tax_amt) {
    var unit_cost = 0;
    var unit_cost_with_out_tax = 0;
    var return_data = {}
    if (!uom_unit) {
        uom_unit = 0;
    }


    var tot_unit_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * uom_unit;
    var unit_rate = 0;
    if (parseInt(uom_unit) != 0) {
        unit_rate = (rate / uom_unit);
    }


    if (!tot_unit_qty || parseFloat(tot_unit_qty) == 0) {
        tot_unit_qty = 0;
    } else {
        unit_cost = (net_amt) / tot_unit_qty;
    }

    if (parseFloat((parseFloat(grn_qty) * parseFloat(uom_unit))) != 0) {
        unit_cost_with_out_tax = (parseFloat(unit_cost)) - ((parseFloat(total_tax_amt) / ((parseFloat(grn_qty)) * parseFloat(uom_unit))));
    }


    var total_qty = parseFloat(grn_qty) * parseFloat(uom_unit);
    var tot_free_qty = parseFloat(free_qty) * parseFloat(uom_unit);
    var all_total_qty = parseFloat(tot_unit_qty);

    return_data = {
        unit_rate: unit_rate,
        unit_cost: unit_cost,
        total_qty: total_qty,
        tot_free_qty: tot_free_qty,
        all_total_qty: all_total_qty,
        unit_cost_with_out_tax: unit_cost_with_out_tax
    }
    return return_data;
}



function getAllItemsTotals() {
    $cgst_amt_total = 0.0;
    $sgst_amt_total = 0.0;
    $igst_amt_total = 0.0;
    $discount_amt_total = 0.0;
    var total_array = [];

    var bill_discount = 0.0;
    var total_gross = 0.0;
    var total_othercharges = 0.0;

    $.each(item_array, function (index, value) {
        $cgst_amt_total = parseFloat($cgst_amt_total) + parseFloat(value.cgst_amt);
        $sgst_amt_total = parseFloat($sgst_amt_total) + parseFloat(value.sgst_amt);
        $igst_amt_total = parseFloat($igst_amt_total) + parseFloat(value.igst_amt);
        $discount_amt_total = parseFloat($discount_amt_total) + parseFloat(value.discount_amt);
        total_gross = parseFloat(total_gross) + parseFloat(value.totalRate);
    });

    var round_off = $('#round_off').val();
    if (!round_off) {
        $('#round_off').val(0);
        round_off = 0.0;
    }
    var total_discount = parseFloat($discount_amt_total) + parseFloat(bill_discount);
    var total_tax = parseFloat($cgst_amt_total) + parseFloat($sgst_amt_total) + parseFloat($igst_amt_total);
    var net_amount = (parseFloat(total_gross) + parseFloat(total_tax) + parseFloat(total_othercharges)) - parseFloat(total_discount);

    var bill_discount_type = $('#bill_discount_type').val();
    var bill_discount_value = $('#bill_discount_value').val();
    if (!bill_discount_value) {
        bill_discount_value = 0.0;
    }

    if (parseFloat(net_amount) > 0) {
        if (parseInt(bill_discount_type) == 1) {
            if (bill_discount_value > 100) {
                bill_discount_value = 0.0;
            }
            bill_discount = (net_amount * bill_discount_value) / 100;
        } else if (parseInt(bill_discount_type) == 2) {
            if (bill_discount_value > net_amount) {
                bill_discount_value = 0.0;
            }
            bill_discount = bill_discount_value;
        }
    } else {
        bill_discount_value = 0.0;
    }
    $('#bill_discount_value').val(checkIsNaN(bill_discount_value));
    $('#tot_bill_discount_amt').html(checkIsNaN(bill_discount));
    net_amount = (parseFloat(net_amount) + parseFloat(round_off)) - (parseFloat(bill_discount));

    total_array['total_gross'] = checkIsNaN(total_gross);
    total_array['total_dis'] = checkIsNaN(total_discount);
    total_array['total_tax'] = checkIsNaN(total_tax);
    total_array['total_net'] = checkIsNaN(net_amount);
    total_array['bill_discount'] = checkIsNaN(bill_discount);

    if ($('#ITEMDISC_amot')) {
        $('#ITEMDISC_amot').html(checkIsNaN($discount_amt_total));
    }
    var is_igst = $('#is_igstcheck').is(":checked");
    if (is_igst) {
        if ($('#SGST_amot')) {
            $('#SGST_amot').html(checkIsNaN(0));
        }
        if ($('#CGST_amot')) {
            $('#CGST_amot').html(checkIsNaN(0));
        }
        if ($('#IGST_amot')) {
            $('#IGST_amot').html(checkIsNaN(total_tax));
        }
    } else {
        if ($('#SGST_amot')) {
            $('#SGST_amot').html(checkIsNaN($sgst_amt_total));
        }
        if ($('#CGST_amot')) {
            $('#CGST_amot').html(checkIsNaN($cgst_amt_total));
        }
        if ($('#IGST_amot')) {
            $('#IGST_amot').html(checkIsNaN(0));
        }
    }
    if ($('#gross_amount_hd').val()) {
        $('#gross_amount_hd').val(checkIsNaN(total_gross));
    }
    if ($('#discount_hd').val()) {
        $('#discount_hd').val(checkIsNaN(total_discount));
    }
    if ($('#tot_tax_amt_hd').val()) {
        $('#tot_tax_amt_hd').val(checkIsNaN(total_tax));
    }
    if ($('#net_amount_hd').val()) {
        $('#net_amount_hd').val(checkIsNaN(net_amount));
    }

    if ($('#tot_gross_footer')) {
        $('#tot_gross_footer').html(checkIsNaN(total_gross));
    }
    if ($('#tot_tax_footer')) {
        $('#tot_tax_footer').html(checkIsNaN(total_tax));
    }
    if ($('#tot_dicount_footer')) {
        $('#tot_dicount_footer').html(checkIsNaN(total_discount));
    }
    if ($('#tot_net_amt_footer')) {
        $('#tot_net_amt_footer').html(checkIsNaN(net_amount));
    }
    return total_array;
}
