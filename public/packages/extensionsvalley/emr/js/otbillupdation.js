$(window).load(function(){
    $("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });
    $("input[data-attr='date']").trigger("focus");
    $(".serachOTUpdationBills").trigger("focus");
})

window.inprogress_status = false;
$(document).ready(function(){
    
    $(document).on('click', '.serachOTUpdationBills', function(event) { 
        fetchOTUpdationBills();
    });
    
    $(document).on('click', '.check_all', function(event) { 
        if($(".check_all:checked").length > 0){
            $(".checkbox_updation").prop('checked', true);
            if(($('.show_procecessed:checked').length > 0)){
                $(".revertOTBillsDiv").show();
            } else {
                $(".processOTBillsDiv").show();
            }
        } else {
            $(".checkbox_updation").prop('checked', false);
            $(".processOTBillsDiv").hide();
            $(".revertOTBillsDiv").hide();
        }
    });
   
    $(document).on('click', '.processOTBills', function(event) { 
        if(window.inprogress_status === false){
            window.inprogress_status = true;
            processOTBills();
        }
    });
    
    $(document).on('click', '.revertOTBills', function(event) { 
        revertProcessedOTBills();
    });
   
    $(document).on('click', '.checkbox_updation', function(event) {
        if($(".checkbox_updation:checked").length > 0){
            if(($('.show_procecessed:checked').length > 0)){
                $(".revertOTBillsDiv").show();
            } else {
                $(".processOTBillsDiv").show();
            }
            
        } else {
            $(".processOTBillsDiv").hide();
            $(".revertOTBillsDiv").hide();
        }
        if($(".checkbox_updation:not(:checked)").length == 0){
            $(".check_all").prop('checked', true);
        } else {
            $(".check_all").prop('checked', false);
        }
    });





    function fetchOTUpdationBills(){
        var from_date = $('.from_date').val();
        var to_date = $('.to_date').val();
        var show_procecessed = ($('.show_procecessed:checked').length > 0) ? 1 : 0;

        if(moment(from_date).isAfter(to_date)){
            toastr.error("Please check the Date Range !!");
            return;
        }

        if($(".check_all:checked").length > 0){
            $(".check_all").attr("checked", false);
        }

        $(".processOTBillsDiv").hide();
        $(".revertOTBillsDiv").hide();

        let _token = $('#c_token').val();
        // var url = "/otbilling/fetchOTUpdationBills";
        var url = $("#base_url").val()+"/otbilling/fetchOTUpdationBills";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                from_date :from_date,
                to_date :to_date,
                show_procecessed :show_procecessed,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".bill_updation_body").empty();
                $(".serachOTUpdationBills").find('i').removeClass('fa-search').addClass('fa-spinner').addClass('fa-spin');
                $(".bill_updation_body").append("<tr><td colspan='7' style='text-align: center;'><i class='fa fa-spinner fa-spin' style='font-size: 20px;'></i></td></tr>")
            },
            success: function (response) {
                console.log(response);
                $(".serachOTUpdationBills").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-search');
                
                
                if(response.code === 100){
                    $(".bill_updation_body").empty();

                    var grand_total = 0;
                    $.each(response.data, function(key, val){
                        $(".bill_updation_body").append("<tr><td colspan='7' style='color:#36A693;'><b>"+moment(key).format('DD-MMM-YYYY')+"</b></td></tr>");
                        
                        var total = 0
                        $.each(val, function(key1, val1){
                            total = parseFloat(total) + parseFloat(val1.total_amount);
                            $(".bill_updation_body").append("<tr><td>"+(key1+1)+"</td><td>"+val1.bill_no+"</td><td>"+val1.bill_tag+"</td><td>"+val1.doctor+"</td><td>"+val1.patient+"</td><td>"+val1.total_amount+"</td><td><input type='checkbox' class='checkbox_updation' value='"+val1.bill_id+"'></td></tr>");
                        });
                        $(".bill_updation_body").append("<tr><td colspan='5' style='color:#b71100;'><b>Total Amount :</b></td><td colspan='2' style='color:#b71100;'><b>"+total.toFixed(2)+"</b></td></tr>");
                        grand_total = parseFloat(grand_total) + parseFloat(total);

                    });

                    $(".bill_updation_body").append("<tr><td colspan='5' style='color:#c59017;'><b>Grand Total :</b></td><td colspan='2' style='color:#c59017;'><b>"+grand_total.toFixed(2)+"</b></td></tr>");
                    
                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { }
        });
    }
    
    function processOTBills(){

        var bill_details = [];
        $(".checkbox_updation:checked").each(function(key, val){
            bill_details.push($(val).val());
        })

        let _token = $('#c_token').val();
        // var url = "/otbilling/processOTBills";
        var url = $("#base_url").val()+"/otbilling/processOTBills";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                bill_details :bill_details,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".processOTBills").find('i').removeClass('fa-check').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (response) {
                console.log(response);
                $(".processOTBills").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-check');
                
                
                if(response.code === 100){

                    if(response.error_bill_list.length > 0){
                        toastr.error("OT Bill configuration does not exist for the following Bill Tags..!! \n"+ response.error_bill_list.join(','));
                    }
                    if(response.succuss_bill_list.length > 0){
                        toastr.success("The following bills are successfully processed ..!! \n"+ response.succuss_bill_list.join(','));
                        fetchOTUpdationBills();
                    }
                    

                    
                   
                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { 
                window.inprogress_status = false;
            }
        });
    }
    
    
    function revertProcessedOTBills(){

        var bill_details = [];
        $(".checkbox_updation:checked").each(function(key, val){
            bill_details.push($(val).val());
        })

        let _token = $('#c_token').val();
        // var url = "/otbilling/revertProcessedOTBills";
        var url = $("#base_url").val()+"/otbilling/revertProcessedOTBills";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                bill_details :bill_details,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".revertOTBills").find('i').removeClass('fa-check').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (response) {
                console.log(response);
                $(".revertOTBills").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-check');
                
                
                if(response.code === 100){

                    toastr.success("The bills are successfully reverted ..!! ");
                    fetchOTUpdationBills();
                   
                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { }
        });
    }
});