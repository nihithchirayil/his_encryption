$(document).ready(function() {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
});

$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});
//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val(0);
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();
        var ward1 = $('#brand_name_hidden').val();
        var ward2 = $('#chemical_name_hidden').val();
        var ward3   = $('#generic_name_hidden').val();
        var ward4   = $('#therapeutic_category_hidden').val();
        var ward5   = $('#therapeutic_subcategory_hidden').val();
        var ward6   = $('#manufacturer_name_hidden').val();

        var param={search_key:search_key,from_type:input_id,ward1: ward1, ward1:ward1,ward2:ward2,ward3:ward3,ward4:ward4,ward5:ward5,ward6:ward6};


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {

            var url = route_json.SearchPharmacyCatageory;
            $.ajax({
                type: "GET",
                url: url,
                data: param,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-spin fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
//------------------------------------------------------------releative select box--------------------------------------------------------
$("#ward").on('change', function() {
    var ward = $('#ward').val();
    // alert(ward);
    var load = $('#warning1').val();
    //alert(ward);

    if (ward == '') {
        var ward = 'a';

    }


    if (ward) {

        var url = route_json.ajaxRoomTypes;

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: { 'ward': ward },
            beforeSend: function() {

                if (load == undefined) {
                    $('#room_type').after('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                }


            },
            success: function(html) {


                if (html) {

                    $("#room_type").empty();

                    //$("#room_type").html('<option value="">Select  Room Type</option>');
                    $.each(html, function(key, value) {
                        ($("#room_type").append('<option value="' + value.id + '">' + value.room_name + '</option>'));
                    });
                    $('#room_type').select2();
                } else {
                    $("#room_type").empty();
                }

            },

            error: function() {
                Command: toastr["warning"]("Please  select a proper item !");


            },
            complete: function() {

                $('#warning1').remove();

            }

        });

    } else {
        $('#ward').focus();
        $("#room_type").empty();
    }

});


function getReportData() {
    var url = route_json.getPharmacyItemFormulary;
    var brand_name=$('#brand_name_hidden').val();
    var chemical_name=$('#chemical_name_hidden').val();
    var generic_name=$('#generic_name_hidden').val();
    var therapeutic_category=$('#therapeutic_category_hidden').val();
    var therapeutic_subcategory=$('#therapeutic_subcategory_hidden').val();
    var manufacturer_name=$('#manufacturer_name_hidden').val();
    var param={brand_name:brand_name,chemical_name:chemical_name,generic_name:generic_name,therapeutic_category:therapeutic_category,therapeutic_subcategory:therapeutic_subcategory,manufacturer_name:manufacturer_name,from_type:1};

    //-------filters---------------------------------------------

    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function() {
            $('#searchpharmacydatabtn').attr('disabled', true);
            $('#searchpharmacydataspin').removeClass('fa fa-search');
            $('#searchpharmacydataspin').addClass('fa fa-spinner fa-spin');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function() {

            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#searchpharmacydatabtn').attr('disabled', false);
            $('#searchpharmacydataspin').removeClass('fa fa-spinner fa-spin');
            $('#searchpharmacydataspin').addClass('fa fa-search');
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}

function occupancy() {
    var url = route_json.currentoccupanciesreport;

    //-------filters---------------------------------------------

    var filters_list = new Object();

    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function() {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function() {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function() {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}




//---------generate csv------------------------------------------------------

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv(from_type=2) {
    var url = route_json.getPharmacyItemFormulary;
    var brand_name=$('#brand_name_hidden').val();
    var chemical_name=$('#chemical_name_hidden').val();
    var generic_name=$('#generic_name_hidden').val();
    var therapeutic_category=$('#therapeutic_category_hidden').val();
    var therapeutic_subcategory=$('#therapeutic_subcategory_hidden').val();
    var manufacturer_name=$('#manufacturer_name_hidden').val();
    var param={brand_name:brand_name,chemical_name:chemical_name,generic_name:generic_name,therapeutic_category:therapeutic_category,therapeutic_subcategory:therapeutic_subcategory,manufacturer_name:manufacturer_name,from_type:from_type};

    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function() {
           
            if(from_type==2){
                $('#excelpharmacydatabtn').attr('disabled', true);
                $('#excelpharmacydataspin').removeClass('fa fa-file-excel-o');
                $('#excelpharmacydataspin').addClass('fa fa-spinner fa-spin');
            }else if(from_type==3){
                $('#printItemFormulary').attr('disabled', true);
                $('#printItemFormularySpin').removeClass('fa fa-print');
                $('#printItemFormularySpin').addClass('fa fa-spinner fa-spin');
            }
          
        },
        success: function(html) {
            console.log(html);
            $('#ResultsViewExcel').html(html);
        },
        complete: function() {
            $('#excelpharmacydatabtn').attr('disabled', false);
            if(from_type==2){
                $('#excelpharmacydatabtn').attr('disabled', false);
                $('#excelpharmacydataspin').removeClass('fa fa-spinner fa-spin');
                $('#excelpharmacydataspin').addClass('fa fa-file-excel-o');
            }else if(from_type==3){
                $('#printItemFormulary').attr('disabled', false);
                $('#printItemFormularySpin').removeClass('fa fa-spinner fa-spin');
                $('#printItemFormularySpin').addClass('fa fa-print');
            }
          
            if(from_type==2){
                exceller();
            }else if(from_type==3){
                print_generate('result_container_print');
            }
          
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

         });





};
function exceller(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}

//---------generate csv ends--------------------------------------------------


//---------print report-------------------------------------------------------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}




function print_generate(printData) {
   
    var $table = $('table.theadfix_wrapper');
    $table.floatThead('destroy');
    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

      var msglist = document.getElementById(printData);
      showw = showw + msglist.innerHTML;
    //showw = showw + printData;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size: A4 porttrait;margin:0;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{ size: A4 landscape;margin:0;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }

}

function search_clear() {
    $('.hidden_search').val('');
    $('.filters').val('');
}
