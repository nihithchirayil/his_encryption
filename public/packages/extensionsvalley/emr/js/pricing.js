
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
   
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_pricing_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
      
        data.pricing_name = $("#pricing_name").val();
        data.company = $("#company").val();
        data.effective_date = $(".effective_date").val();
        data.date1 = $(".date1").val();
        data.pricing_type = $(".pricing_type").val();
        data.expiry_date = $(".expiry_date").val();
        data.date2 = $(".date2").val();
        data.show_price_discount = $(".show_price_discount").val();
        data.billing_use_only = $(".billing_use_only").val();
        data.allow_pay_from_cash = $(".allow_pay_from_cash").val();
        data.status = $(".status").val();
        if($(".status").prop('checked') == true){
            // alert("chcked");
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".show_price_discount").prop('checked') == true){
            // alert("chcked");
            data.show_price_discount =1;
        }else{
            data.show_price_discount =0;
        }
        if($(".effective_date").prop('checked') == true){
            // alert("chcked");
            data.effective_date =1;
        }else{
            data.effective_date =0;
        }
        if($(".expiry_date").prop('checked') == true){
            // alert("chcked");
            data.expiry_date =1;
        }else{
            data.expiry_date =0;
        }
       console.log(data.expiry_date);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($(".pricing_name_add").val().trim() == ''){
            Command: toastr["error"]("Please enter pricing Name");
            return;
        }
        if($(".effective_date_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Effecive date");
            return;
        }
        if($(".expiry_date_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Expiry date");
            return;
        }
        data.pricing_id = $(".reference_id").val();
        data.pricing_name = $("#pricing_name_add").val();
        data.company = $("#company_add").val();
        data.effective_date = $(".effective_date_add").val();
        data.pricing = $("#pricing_type_add").val();
        data.expiry_date = $(".expiry_date_add").val();
        data.show_price_discount = $(".show_price_discount_add").val();
        data.billing_use_only = $(".billing_use_only_add").val();
        data.allow_pay_from_cash = $(".allow_pay_from_cash_add").val();
        data.status = $(".status_add").val();
// alert(data.allow_pay_from_cash);
       
        if($(".status_add").prop('checked') == true){
            // alert("chcked");
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".show_price_discount_add").prop('checked') == true){
            // alert("chcked");
            data.show_price_discount =1;
        }else{
            data.show_price_discount =0;
        }
        if($(".billing_use_only_add").prop('checked') == true){
            // alert("chcked");
            data.billing_use_only =1;
        }else{
            data.billing_use_only =0;
        }
        if($(".allow_pay_from_cash_add").prop('checked') == true){
            // alert("chcked");
            data.allow_pay_from_cash =1;
        }else{
            data.allow_pay_from_cash =0;
        }
        // if($(".esi_add").prop('checked') == true){
        //     // alert("chcked");
        //     data.is_esi ='t';
        // }else{
        //     data.is_esi ='f';
        // }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"]("Pricing Name Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }
                   else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
   
    function editItem(element){
       
        let pricing_id = $(element).parents('tr').attr('data-id');
        let company = $(element).parents('tr').attr('data-company');
        let pricing_name = $(element).parents('tr').attr('data-pricing');
        let effective_date = $(element).parents('tr').attr('data-effective_date');
        let pricing = $(element).parents('tr').attr('data-pricing_type');
        let expiry_date = $(element).parents('tr').attr('data-expiry_date');
        let show_price_discount = $(element).parents('tr').attr('data-discount');
        let billing_use_only = $(element).parents('tr').attr('data-billing');
        let allow_pay_from_cash = $(element).parents('tr').attr('data-pay_from_cash');
        let status1 = $(element).parents('tr').attr('data-status');
// alert(effective_date);
        if(status1==1){
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", true);

            } else{
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", false);
                   }
            if(show_price_discount==1){
             //alert("CheckBox checked.");
                        $(".show_price_discount_add").prop("checked", true);
                
            } else{
            //alert("CheckBox checked.");
                         $(".show_price_discount_add").prop("checked", false);
                   }
           if(billing_use_only==1){
            //alert("CheckBox checked.");
                       $(".billing_use_only_add").prop("checked", true);
               
           } else{
           //alert("CheckBox checked.");
                        $(".billing_use_only_add").prop("checked", false);
                  }
           if(allow_pay_from_cash==1){
             //alert("CheckBox checked.");
                        $(".allow_pay_from_cash_add").prop("checked", true);
                
            } else{
            //alert("CheckBox checked.");
                         $(".allow_pay_from_cash_add").prop("checked", false);
                   }
        $(".reference_id").val(pricing_id);
        $("#pricing_name_add").val(pricing_name);
        $("#company_add").val(company);
        $("#company_add").select2().trigger('change'); 
      $("#effective_date_add").val( moment(effective_date).format('YYYY-MM-DD'));
   
        $("#pricing_type_add").val(pricing);
        $("#expiry_date_add").val( moment(expiry_date).format('YYYY-MM-DD'));

        $(".show_price_discount_add").val(show_price_discount);
        $(".billing_use_only_add").val(billing_use_only);
        $(".allow_pay_from_cash_add").val(allow_pay_from_cash);
        $(".status_add").val(status1);
      
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $("#add").html('Edit Pricing');

    }

  
    function resetForm(){
        $(".reference_id").val('');
        $(".pricing_name_add").val('');
        $(".select2").val('').trigger('change');
        $(".pricing_type_add").val('');
        $(".effective_date_add").val('');
        $(".expiry_date_add").val('');
        $("#add").html('Add Pricing');
        $('#status_add').removeAttr('checked');
        $('#billing_use_only_add').removeAttr('checked');
        $('#allow_pay_from_cash_add').removeAttr('checked');
        $('#show_price_discount_add').removeAttr('checked');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

