$(document).ready(function(){
    $('.bill_date').val(moment().format('DD-MMM-YYYY HH:mm:ss'));
    $("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });


    //----UHID search--------------------------------------------------------------------
    $('.patient_uhid').keyup(function (event) {

        var keycheck = /[a-z0-9A-Z ]/; // now only accept alphabets, number and space noe need to change it.
        if(event.keyCode == '96') { event.keyCode = '48'; }
        var value = event.key; //get the charcode and convert to char
        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            // if ($('#op_no_hidden').val() != "") {
            //     $('#op_no_hidden').val('');
            // }
            var op_no = $(this).val();
            op_no = op_no.replace("/[^\w\s-_\.]/gi");
            op_no = op_no.trim();
            if (op_no == "") {
                $(".OpAjaxDiv").html("");
                $(".OpAjaxDiv").hide();
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "op_no=" + op_no,
                    beforeSend: function () {

                        $(".OpAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $(".OpAjaxDiv").html(html).show();
                        $(".OpAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        //  $('#loading_image').hide();
                    }
                });
            }

        } else if (event.keyCode === 13) {
            ajaxlistenter('OpAjaxDiv', event);
            return false;
        } else if (event.keyCode === 38){
            ajax_list_key_down('OpAjaxDiv', "UP");
        } else if (event.keyCode === 40){
            ajax_list_key_down('OpAjaxDiv', "DOWN");
        }
    });

     //----UHID search--------------------------------------------------------------------
     $('.patient_uhid_search').keyup(function (event) {

        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        if(event.keyCode == '96') { event.keyCode = '48'; }
        var value = event.key; //get the charcode and convert to char
        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            // if ($('#op_no_hidden').val() != "") {
            //     $('#op_no_hidden').val('');
            // }
            var op_no = $(this).val();
            op_no = op_no.replace("/[^\w\s-_\.]/gi");
            op_no = op_no.trim();
            if (op_no == "") {
                $(".OpAjaxDivSearch").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "op_no=" + op_no +"&search=true",
                    beforeSend: function () {

                        $(".OpAjaxDivSearch").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $(".OpAjaxDivSearch").html(html).show();
                        $(".OpAjaxDivSearch").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        //  $('#loading_image').hide();
                    }
                });
            }

        } else if (event.keyCode === 13) {
            ajaxlistenter('OpAjaxDivSearch', event);
            return false;
        } else if (event.keyCode === 38){
            ajax_list_key_down('OpAjaxDivSearch', "UP");
        } else if (event.keyCode === 40){
            ajax_list_key_down('OpAjaxDivSearch', "DOWN");
        }
    });

});

$(document).on('click', '.saveButton', function(event) {
    var confirm = window.confirm("Are you sure want to continue ?");
    if (confirm == true) {
        saveOTBill(0);
    }
});

$(document).on('click', '.bill_cancel', function(event) {
    bootbox.prompt("Are you sure want to cancel this bill ? If so, please enter the reason.", function(result){
        console.log(result);
        if(result){
            cancelBill(result);
        }
    });
});

$(document).on('keyup', '#discount_percentage_status', function(event) {

    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault(); //stop character from entering input
    }

    var discount = $(this).val();
    if(discount == ""){
        $("#discount_percentage_status").val(0);
    }

    if(parseInt(discount) > 100){
        $("#discount_percentage_status").val(100);
    }

    var surgery_charge_service_code = $("#surgery_charge_service_code").val();
    var surgery_charge = $(".item-amount-"+surgery_charge_service_code).val();

    if(parseInt(surgery_charge) > 0 && parseInt(discount) > 0){
        var discount_amount = surgery_charge * discount/100;
        $(".item-amount-"+surgery_charge_service_code).parents('tr').find('.discount_percentage_amount').val(discount_amount);
    } else {
        var discount_amount = 0;
        $(".item-amount-"+surgery_charge_service_code).parents('tr').find('.discount_percentage_amount').val(discount_amount);
    }

    calculateItemWiseNetAmount();


});

$(document).on('click', '.saveAndPrintButton', function(event) {
    var confirm = window.confirm("Are you sure want to continue ?");
    if (confirm == true) {
        saveOTBill(1);
    }
});

$(document).on('click', '.viewBills', function(event) {
    $("#ot_bills_modal").modal('show');
    window.bill1 = $(".bill1_body").html();
    window.bill2 = $(".bill2_body").html();
    window.bill3 = $(".bill3_body").html();
    window.bill4 = $(".bill4_body").html();
    window.bill5 = $(".bill5_body").html();
    window.bill6 = $(".bill6_body").html();
});

$(document).on('click', '.searchBills', function(event) {
    billSearch(1);
});

$(document).on('click', '.print_bill', function(event) {

    printBill();


});

$(document).on('click', '.clearSearch', function(event) {
    $(".bill_no_search").val('');
    $('.patient_uhid_search').val('');
    $(".from_date").val('');
    $(".to_date").val('');
    $(".searchBillContent").html('');
});

$('.searchBillContent').on('click', '.pagination a', function (e) {
    billSearch($(this).attr('href').split('page=')[1]);
    e.preventDefault();
});

$(document).on('click', '.pop_closebtn', function(event) {
    event.stopPropagation();
    calculateTotal();
    $(this).parent().parent().find(".pop").hide();
});

$(document).on('keyup change', '.package_item_amount', function(event) {
    var package_item_amount = 0;
    $(this).parents('.pop').find('.package_item_amount').each(function() {
        if($(this).val()>0) {
            package_item_amount += parseFloat($(this).val());
        }
    });
    package_item_amount = package_item_amount.toFixed(2);
    $(this).parents('.package').next('td').find('.amount').val(package_item_amount);
    calculateTotal();

});

$('#ot_bill_tag_five').change(function(){
    ot_bill_tag_five();
});



function ajaxlistenter(dom, event) {
    $('.'+dom).find('li.liHover')[0].click();
}

function ajax_list_key_down(dom, type) {
    var current = $('.'+dom).find('li.liHover')[0];
    if(type === "DOWN"){
        $('.'+dom).find('li.liHover').next().addClass('liHover');
    }
    if(type === "UP"){
        $('.'+dom).find('li.liHover').prev().addClass('liHover');
    }
    $(current).removeClass('liHover');
}

function fillOpDetials(patient_id, op_id, id, patient_name, current_visit_id, admitting_doctor_id, consulting_doctor_id, gender, age, share_holder_status) {
    $('.patient_uhid').val(op_id);
    $('.patient_uhid').attr('patient_id', patient_id);
    $('.patient_uhid').attr('current_visit_id', current_visit_id);
    $('.patient_name').val(patient_name.replaceAll("  ", ""));
    $('#admitting_doctor').val(admitting_doctor_id);
    $('#surgeon').val(consulting_doctor_id);
    $('#gender').val(gender);
    $('.age').val(age);
    // $('#current_visit_id').val(current_visit_id);
    $('.OpAjaxDiv').hide();
    if(parseInt(share_holder_status) === 1){
        $("#discount_percentage_status").val(50);
        $("#discount_percentage_status").attr('disabled', true);
    } else {
        $("#discount_percentage_status").val(20);
        $("#discount_percentage_status").attr('disabled', false);
    }
    fetchPaymentType(op_id);
}

function fetchPaymentType(uhid, payment_type_id = false){
    let _token = $('#c_token').val();
    var url = $("#base_url").val()+"/otbilling/fetchPaymentTypesByUHID";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            uhid :uhid,
            _token :_token
        },
        beforeSend: function () {

        },
        success: function (response) {
            $(".payment_type_li").empty();

            var payment_type = '';
            payment_type = payment_type+'<select class="form-control select2" title="Payment Type" id="payment_type" style="color:#555555; padding:2px 12px; " name="payment_type">';
            payment_type = payment_type+'<option value="" >Payment Type</option>';
            $.each(response.data.payment_types, function(key, val){
                payment_type = payment_type+'<option value="'+val.id+'" data-payment-code="'+val.code+'" >'+val.name+'</option>';
            });
            payment_type = payment_type+'</select>';
            $(".payment_type_li").html(payment_type);

            if(payment_type_id){
                $("#payment_type").val(payment_type_id)
            } else {
                if(response.data.is_insured == 1){
                    $("#payment_type").find('option[data-payment-code="insurence"]').attr('selected', true);
                }
                // else if(response.data.visit_status == 'IP' && response.data.is_insured == 0){
                //     $("#payment_type").find('option[data-payment-code="ipcredit"]').attr('selected', true);
                // }
                else {
                    $("#payment_type").find('option[data-payment-code="cash/Card"]').attr('selected', true);
                }
            }

        },
        complete: function () { }
    });
}

function fillOpDetialsSearch(patient_id, op_id, id, patient_name, current_visit_id, admitting_doctor_id, consulting_doctor_id, gender, age) {
    $('.patient_uhid_search').val(op_id);
    $('.patient_uhid_search').attr('patient_id', patient_id);
    $('.patient_name_search').val(patient_name.replaceAll("  ", ""));
    $('.OpAjaxDivSearch').hide();
}


function displaySubPackages(head_id,show_status, check = '0'){

    if(head_id != '') {
        if (($('#popupDiv' + head_id).html().trim()) == '') {
            var url = "";
            $.ajax({
                type: "GET",
                async: false,
                url: url,
                data: 'ot_group_head_id=' + head_id,
                success: function (html) {
                    $('#popupDiv' + head_id).html(html).show();
                    if(check == '1' && !$('#head-id-' + head_id).prop("checked")){
                        selectOTItem('head-id-'+head_id, head_id);
                    }

                }
            });
        } else if(show_status == '1') {
            $('#popupDiv' + head_id).show();
            if(check == '1' && !$('#head-id-' + head_id).prop("checked")){
                selectOTItem('head-id-'+head_id, head_id);
            }
        }
    }
}

 //----------function bill tag change---------------------------------
 function ot_bill_tag_five(bill_details) {

    var bill_tag = $('#ot_bill_tag_five').val();
    if(bill_tag != ""){

        var url = '';
        $.ajax({
            type: "GET",
            url: url,
            data: "bill_tag=" + bill_tag,
            beforeSend: function () {
                $("#fixTableOTBills5").html('<tbody><tr><td style="width: 3%; padding:0 0 0 0; text-align: center;" colspan="9"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr></tbody>');
            },
            success: function (data) {
                $("#fixTableOTBills5").html(data);
                if(bill_details){
                    $.each(bill_details, function(key, value){
                        $("input[value='"+value.service_code+"']").trigger('click');
                        $("input[value='"+value.service_code+"']").attr("data-bill-detail-id", value.bill_detail_id);
                        $("input[value='"+value.service_code+"']").attr("data-actual-billed-price", parseInt(value.net_amount).toFixed(2));
                        $(".item-amount-"+value.service_code ).val( parseInt(value.net_amount).toFixed(2));
                        $("#head-id-"+value.batch).attr("data-bill-detail-id", value.bill_detail_id);
                    });
                }
                calculateTotal();
            },
            complete: function () {
            }
        });
    }
}

function calculateTotal(){
    var grand_total = 0.0;

    $('.deleteID').each(function () {
        if (this.checked) {
            var item_value = $('#field_selling_price-' + this.id).val();
            if(item_value != 0){
                grand_total += parseFloat(item_value);
            }
        }
    });

    grand_total = grand_total.toFixed(2);
    $('.net_amount').val(grand_total);
}

//---------Item select Total Caluculation function-----------------------
function checkBoxClick(head_id, type){
    if(type){
        checkForItemRemoved(head_id);
    } else {
        checkForItemRemoved('head-id-'+head_id);
        displaySubPackages(head_id,'0');
    }
    calculateItemWiseNetAmount();
    // calculateTotal();
}

function selectOTItem(id,head_id){
    var item_checkbox = $('#' + id).prop("checked");
    $('#' + id).prop("checked", !item_checkbox);
    checkForItemRemoved(id);
    if(head_id != '0') {
        displaySubPackages(head_id,'0');
    }
    calculateTotal();
}

window.removedItemsBills = [];
window.removedItemsBill1 = [];
function checkForItemRemoved(id){
    if($(".bill_no").val() != ""){ // checking edit mode or not
        var temp_head = $('#' + id).attr('data-head-id');
        if($('#' + id).prop("checked") == true){ // checking checkbox is checked or or not
            if($('.nav-tabs li.active a').html() == "Bill1"){

                $("#popupDiv"+temp_head).find('.package_item_amount').each(function(key, val){
                    if($(val).attr('data-bill-detail-id')) {
                        var bill_detail_id = $(val).attr('data-bill-detail-id');
                        if(window.removedItemsBill1.indexOf(bill_detail_id) != -1){
                            window.removedItemsBill1.splice(window.removedItemsBill1.indexOf(bill_detail_id), 1);
                        }
                    }
                });
            } else {
                // var service_code = $('#' + id).attr('data-service-code');
                if($("input[value='"+id+"']").attr('data-bill-detail-id')) {
                    var bill_detail_id = $("input[value='"+id+"']").attr('data-bill-detail-id');
                    if(window.removedItemsBills.indexOf(bill_detail_id) != -1){
                        window.removedItemsBills.splice(window.removedItemsBills.indexOf(bill_detail_id), 1);
                    }
                }
            }

        } else {
            if($('.nav-tabs li.active a').html() == "Bill1"){
                $("#popupDiv"+temp_head).find('.package_item_amount').each(function(key, val){
                    if($(val).attr('data-bill-detail-id')) {
                        var bill_detail_id = $(val).attr('data-bill-detail-id');
                        if(window.removedItemsBill1.indexOf(bill_detail_id) < 0){
                            window.removedItemsBill1.push(bill_detail_id);
                        }
                    }
                });
            } else {
                // var service_code = $('#' + id).attr('data-service-code');
                if($("input[value='"+id+"']").attr('data-bill-detail-id')) {
                    var bill_detail_id = $("input[value='"+id+"']").attr('data-bill-detail-id');
                    if(window.removedItemsBills.indexOf(bill_detail_id) < 0){
                        window.removedItemsBills.push(bill_detail_id);
                    }
                }
            }

        }

    }
}

function saveOTBill(print_config){

    var request_json = {};
    request_json.bill_head_id           = $(".bill_no").attr('data-bill-head-id') ? $(".bill_no").attr('data-bill-head-id') : 0;
    request_json.bill_no                = $(".bill_no").val() ? $(".bill_no").val(): 0;
    request_json.patient_id             = $(".patient_uhid").attr('patient_id');
    request_json.patient_name           = $(".patient_name").val();
    request_json.age                    = $(".age").val();
    request_json.gender_id              = $("#gender").val();
    request_json.gender_name            = $( "#gender option:selected" ).text();
    request_json.visit_id               = $(".patient_uhid").attr('current_visit_id');
    request_json.admitting_doctor_id    = $('#admitting_doctor').val();
    request_json.admitting_doctor_name  = $( "#admitting_doctor option:selected" ).text();
    request_json.consulting_doctor_id   = $('#surgeon').val();
    request_json.consulting_doctor_name = $( "#surgeon option:selected" ).text();
    request_json.anesthesia_doctor_id   = $('#anesthesia_doctor').val();
    request_json.anesthesia_doctor_name = $( "#anesthesia_doctor option:selected" ).text();
    request_json.net_amount             = $('.net_amount').val();
    request_json.payment_type           = $('#payment_type').val();
    request_json.payment_type_name      = $('#payment_type option:selected').text();
    request_json.discount_percentage    = $('#discount_percentage_status').val();
    request_json.bill1                  = {};
    request_json.bills                  = {};
    request_json.bills.bill2            = [];
    request_json.bills.bill3            = [];
    request_json.bills.bill4            = [];
    request_json.bills.bill5            = [];
    request_json.bills.bill6            = [];


    if(print_config == 1){
        $(".saveAndPrintButton").attr('disabled', true);
    } else {
        $(".saveButton").attr('disabled', true);
    }


    $('.bill_one_check').each(function () {
        var that = this;
        if (that.checked) {
            var temp_head = $(that).attr("data-head-id");
            console.log(temp_head, "temppppp");

            $("#popupDiv"+temp_head).find('.package_item_amount').each(function () {
                var temp            = {};
                temp.service_code   = $(this).attr('data-service-code');
                temp.head_id        = $(this).attr('data-head-id');
                temp.price          = $(this).val();
                temp.detail_id      = $(this).attr('data-detail-id'); // ot_bill_group_detail_id
                var package_item    = this;

                if($(that).attr('data-bill-detail-id') && $(that).attr('data-bill-detail-id') != ''){
                    temp.bill_detail_id = $(package_item).attr('data-bill-detail-id'); // bill_detail id
                    var actual_billed_price = $(package_item).attr('data-actual-billed-price');
                    // comparing current price and actual billed price
                    if(parseInt(actual_billed_price) != parseInt(temp.price)){
                        temp.detail_status = 2;
                    } else {
                        temp.detail_status = 0;
                    }
                } else {
                    temp.bill_detail_id = 0;
                    if(request_json.bill_no != ""){
                        temp.detail_status = 1;
                    } else {
                        temp.detail_status = 0;
                    }

                }


                if(request_json.bill1[temp.head_id]){
                    request_json.bill1[temp.head_id].push(temp);
                } else{
                    request_json.bill1[temp.head_id] = [];
                    request_json.bill1[temp.head_id].push(temp);
                }
            });

        }
    });

    window.removedItemsBill1.forEach(function(val){
        var temp = {};
        temp.detail_status = 3;
        temp.bill_detail_id = val;
        var head_id = $("input[data-bill-detail-id='"+val+"']").attr('data-head-id');
        if(request_json.bill1[head_id]){
            request_json.bill1[head_id].push(temp);
        } else{
            request_json.bill1[head_id] = [];
            request_json.bill1[head_id].push(temp);
        }

    })


    $('.bill_type_check').each(function () {
        if (this.checked) {
            var temp            = {};
            temp.service_code   = $(this).attr('data-service-code');
            temp.price          = $(this).parents('tr').find('.item-amount-'+$(this).attr('data-service-code')).val();
            temp.head_id        = $(this).attr('data-head-id');
            temp.detail_id      = $(this).attr('data-detail-id'); // ot_bill_group_detail_id
            temp.bill_detail_id = $("input[data-service-code='"+temp.service_code+"']").attr('data-bill-detail-id'); // bill_detail id
            var bill_check = this;

            if($(this).attr('data-bill-detail-id') && $(this).attr('data-bill-detail-id') != ''){
                var actual_billed_price = $(bill_check).attr('data-actual-billed-price');
                // comparing current price and actual billed price
                if(parseInt(actual_billed_price) != parseInt(temp.price)){
                    temp.detail_status = 2;
                } else {
                    temp.detail_status = 0;
                }
            } else {
                temp.bill_detail_id = 0;
                if(request_json.bill_no != ""){
                    temp.detail_status = 1;
                } else {
                    temp.detail_status = 0;
                }

            }

            if($(this).attr('data-bill-type') == '2'){
                request_json.bills.bill2.push(temp);
            }else if($(this).attr('data-bill-type') == '3'){
                request_json.bills.bill3.push(temp);
            }else if($(this).attr('data-bill-type') == '4'){
                temp.bill_tag   = $('#ot_bill_tag_four').val();
                request_json.bills.bill4.push(temp);
            } else if($(this).attr('data-bill-type') == '5'){
                temp.bill_tag   = $('#ot_bill_tag_five').val();
                request_json.bills.bill5.push(temp);
            }else if($(this).attr('data-bill-type') == '6'){
                temp.bill_tag   = $('#ot_bill_tag_six').val();
                request_json.bills.bill6.push(temp);
            }
        }
    });

    window.removedItemsBills.forEach(function(val){
        var temp = {};
        temp.detail_status = 3;
        temp.bill_detail_id = val;

        var bill_type = $('.nav-tabs li.active').find('a').html();
        if(bill_type == 'Bill2'){
            request_json.bills.bill2.push(temp);
        }else if(bill_type == 'Bill3'){
            request_json.bills.bill3.push(temp);
        }else if(bill_type == 'Bill4'){
            temp.bill_tag   = $('#ot_bill_tag_four').val();
            request_json.bills.bill4.push(temp);
        } else if(bill_type == 'Bill5'){
            temp.bill_tag   = $('#ot_bill_tag_five').val();
            request_json.bills.bill5.push(temp);
        }  else if(bill_type == 'Bill6'){
            temp.bill_tag   = $('#ot_bill_tag_six').val();
            request_json.bills.bill6.push(temp);
        }

    })
    console.log(request_json)
    var validation_response = validateRequestData(request_json);
    if (!validation_response){
        $(".saveButton").attr('disabled', false);
        $(".saveAndPrintButton").attr('disabled', false);
        return;
    }



    let _token = $('#c_token').val();

    // var url = "otbilling/saveOTBill";
    var url = $("#base_url").val()+"/otbilling/saveOTBill";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_data :request_json,
            _token :_token
        },
        dataType: "json" ,
        beforeSend: function () {

            if(print_config == 1){
                $(".saveAndPrintButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            } else {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            }


        },
        success: function (response) {
            if(response.code === 100){
                if(print_config == 1){
                    $(".saveAndPrintButton").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-save');
                } else {
                    $(".saveButton").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-save');
                }

                toastr.success("Bill Generated..!");
                if(print_config == 1){
                    window.bill_ids_for_print = response.data.bill_ids;
                    printBill();
                } else {
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000)
                }


            } else {
                toastr.error("Something went wrong..!");
                $(".saveButton").attr('disabled', false);
                $(".saveAndPrintButton").attr('disabled', false);
            }
        },
        complete: function () { }
    });

}

function validateRequestData(request){
    if(!request.patient_id){
        toastr.error("Please select a patient");
        return false;
    }
    // if(!request.admitting_doctor_id){
    //     toastr.error("Please select admitting doctor");
    //     return false;
    // }
    if(!request.consulting_doctor_id){
        toastr.error("Please select consulting doctor");
        return false;
    }
    // if(!request.anesthesia_doctor_id){
    //     toastr.error("Please select anesthesia doctor");
    //     return false;
    // }
    if(!request.payment_type){
        toastr.error("Please select a payment");
        return false;
    }
    if(request.bills.bill4.length > 0 && !$('#ot_bill_tag_four').val()){
        toastr.error("Please select a bill tag for OT Bill");
        return false;
    }
    if(request.bills.bill5.length > 0 && !$('#ot_bill_tag_five').val()){
        toastr.error("Please select a bill tag for Bill5");
        return false;
    }
    if($('input:checkbox:checked').length == 0){
        toastr.error("Please select a service");
        return false;
    }
    return true;
}



function cancelBill(reason){

    var bill_id = $(".bill_no").attr("data-bill-head-id");
    let _token = $('#c_token').val();
    // var url = "/otbilling/cancelBill";
    var url = $("#base_url").val()+"/otbilling/cancelBill";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id :bill_id,
            reason :reason,
            _token :_token
        },
        dataType: "json" ,
        beforeSend: function () {
            $(".bill_cancel").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (response) {
            console.log(response);
            if(response.code === 100){
                $(".bill_cancel").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                toastr.success("Bill Cancelled..!");
                window.location.reload();
            } else {
                toastr.error("Something went wrong..!");
            }
        },
        complete: function () { }
    });
}

function billSearch(page_no){
    var bill_no = $(".bill_no_search").val();
    var uhid = $('.patient_uhid_search').val();
    var from_date = $(".from_date").val();
    var to_date = $(".to_date").val();
    var paid_status = $("#search_paid_status").val();
    var payment_type = $("#search_payment_type").val();
    var url = "";
    $.ajax({
        type: "GET",
        url: url,
        data: "&bill_no=" + bill_no +"&uhid=" +uhid+"&payment_type=" +payment_type+"&paid_status=" +paid_status +"&from_date=" +from_date +"&to_date=" +to_date + "&page=" + page_no,
        beforeSend: function () {
            $(".searchBills i").removeClass('fa-search').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (response) {
            $(".searchBills i").removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-search');
            $('.searchBillContent').html(response);
        },
        complete: function () { }
    });
}

function fetchBillDetails(bill_id){
    window.bill_ids_for_print = [];

    let _token = $('#c_token').val();
    var url = $("#base_url").val()+"/otbilling/fetchBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id :bill_id,
            _token :_token
        },
        dataType: "json" ,
        beforeSend: function () {

        },
        success: function (response) {
            console.log(response);

            if(response.code === 100){
                if(response.data.length > 0){

                    if(response.data[0].paid_status == 0){
                        $('.bill_cancel').show();
                        // $('.print_bill').show();
                        $('#admitting_doctor').attr("disabled", false);
                        $('#anesthesia_doctor').attr("disabled", false);
                        $('#surgeon').attr("disabled", false);
                        $(".saveButton").show();
                        $(".saveAndPrintButton").show();
                        $("#discount_percentage_status").attr("disabled", false);
                    }else{
                        $(".saveButton").hide();
                        $(".saveAndPrintButton").hide();
                        $('.bill_cancel').hide();
                        // $('.print_bill').hide();
                        $('#admitting_doctor').attr("disabled", true);
                        $('#anesthesia_doctor').attr("disabled", true);
                        $('#surgeon').attr("disabled", true);
                        $("#discount_percentage_status").attr("disabled", true);
                    }
                    // temporary purpose
                    $("#discount_percentage_status").attr("disabled", true);
                    $('.print_bill').show();
                    $('.deleteID').prop("checked", false)
                    $('.bill_tabs').show();
                    $("#ot_bills_modal").modal('hide');
                    $(".bill_date").val(moment(response.data[0].bill_datetime).format('DD-MMM-YYYY HH:mm:ss'))
                    $(".patient_uhid").val(response.data[0].uhid)
                    $(".patient_uhid").attr('patient_id', response.data[0].patient_id);
                    $(".patient_uhid").attr('current_visit_id', response.data[0].visit_id);
                    $(".patient_name").val(response.data[0].patient_name)
                    $(".age").val(response.data[0].patient_age)
                    $("#gender").val(response.data[0].gender)
                    $("#surgeon").val(response.data[0].consulting_doctor_id)
                    $("#discount_percentage_status").val(response.data[0].discount_percentage)
                    $("#anesthesia_doctor").val(response.data[0].anesthesia_doctor_id)
                    $("#admitting_doctor").val(response.data[0].admitting_doctor_id)
                    $(".bill_no").val(response.data[0].bill_no)
                    // $("#payment_type").val(response.data[0].payment_type_id)
                    $(".bill_no").attr("data-bill-head-id", response.data[0].bill_head_id)
                    $(".abc").attr('disabled', true);

                    if(!window.bill_ids_for_print.includes(parseInt(response.data[0].bill_head_id))){
                        window.bill_ids_for_print.push(parseInt(response.data[0].bill_head_id));
                    }

                    $('.bill_tabs').hide();
                    $("."+response.data[0].ot_bill_group_code).show();
                    $("."+response.data[0].ot_bill_group_code).trigger('click');
                    if(response.data[0].ot_bill_group_code == "B1"){


                        var batches = response.data.map(x => x.batch);
                        batches = [...new Set(batches)]; // removing duplicate batch numbers

                        $.each(batches, function(key, value){
                            selectOTItem('head-id-'+value,value);
                        })

                        $.each(response.data, function(key, value){
                            $('#popupDiv' + value.batch).find("input[data-service-code='"+value.service_code+"']").val(parseInt(value.actual_selling_price).toFixed(2));
                            $('#popupDiv' + value.batch).find("input[data-service-code='"+value.service_code+"']").attr("data-bill-detail-id", value.bill_detail_id);
                            $('#popupDiv' + value.batch).find("input[data-service-code='"+value.service_code+"']").attr("data-actual-billed-price", parseInt(value.actual_selling_price).toFixed(2));
                            $("#head-id-"+value.batch).attr("data-bill-detail-id", value.bill_detail_id);
                        })
                        $(".package_item_amount").trigger('keyup');

                    } else {

                        if(response.data[0].ot_bill_group_code == "B2" || response.data[0].ot_bill_group_code == "B3" || response.data[0].ot_bill_group_code == "B6"){
                            $.each(response.data, function(key, value){
                                $("input[value='"+value.service_code+"']").trigger('click');
                                $("input[value='"+value.service_code+"']").attr("data-bill-detail-id", value.bill_detail_id);
                                $("input[value='"+value.service_code+"']").attr("data-actual-billed-price", parseInt(value.actual_selling_price).toFixed(2));
                                $(".item-amount-"+value.service_code ).val( parseInt(value.actual_selling_price).toFixed(2));
                                $("#head-id-"+value.batch).attr("data-bill-detail-id", value.bill_detail_id);
                            });

                        }

                        if(response.data[0].ot_bill_group_code == "B4"){
                            $.each(response.data, function(key, value){
                                $("input[value='"+value.service_code+"']").trigger('click');
                                $("input[value='"+value.service_code+"']").attr('disabled', true);
                                $("input[value='"+value.service_code+"']").attr("data-bill-detail-id", value.bill_detail_id);
                                $("input[value='"+value.service_code+"']").attr("data-actual-billed-price", parseInt(value.actual_selling_price).toFixed(2));
                                $(".item-amount-"+value.service_code ).val( parseInt(value.actual_selling_price).toFixed(2));
                                $("#head-id-"+value.batch).attr("data-bill-detail-id", value.bill_detail_id);
                            });

                            $("#ot_bill_tag_four").val(response.data[0].bill_tag_id);
                        }
                        if(response.data[0].ot_bill_group_code == "B5"){
                            $("#ot_bill_tag_five").val(response.data[0].bill_tag_id);
                            ot_bill_tag_five(response.data);
                        }
                    }

                    var surgery_charge_service_code = $("#surgery_charge_service_code").val();
                    var surgery_charge = $(".item-amount-"+surgery_charge_service_code).val();
                    var discount_percentage = $("#discount_percentage_status").val()

                    if(parseInt(surgery_charge) > 0 && parseInt(discount_percentage) > 0){
                        var discount_amount = surgery_charge * discount_percentage/100;
                        $(".item-amount-"+surgery_charge_service_code).parents('tr').find('.discount_percentage_amount').val(discount_amount);
                    }

                    calculateItemWiseNetAmount();
                    fetchPaymentType(response.data[0].uhid, response.data[0].payment_type_id);

                } else {
                    toastr.success("Invalid records..!");
                }

            } else {
                toastr.success("Something went wrong..!");
            }
        },
        complete: function () { }
    });
}

function printBill(){
    if(window.bill_ids_for_print.length > 0){

        var url = $("#base_url").val()+"/otbilling/printOTBill";
        let _token = $('#c_token').val();
        $.ajax({
            async: true,
            type: "POST",
            url: url,
            data: {
                bill_id : window.bill_ids_for_print,
                _token :_token
            },
            beforeSend: function () {
                $(".print_bill").find('i').removeClass('fa-print').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (response) {
                $(".print_bill").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-print');

                var mywindow = window.open('', 'my div', 'height=3508,width=2480');
                mywindow.document.write(response+'<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');

                setTimeout(function(){
                    window.location.reload();
                }, 1000)
            },
            complete: function () { }
        });

    }
}


var timeout = null;
function fetchAddtionalSurgeryCharge(){


    var bill_tag = $("#ot_bill_tag_four").val();
    var surgery_charge_service_code = $("#surgery_charge_service_code").val();
    var cssd_charge_service_code = $("#cssd_charge_service_code").val();
    var asst_service_charge_service_code = $("#asst_service_charge_service_code").val();
    var url = $("#base_url").val()+"/otbilling/fetchAddtionalSurgeryCharge";
    var surgery_charge = $(".item-amount-"+surgery_charge_service_code).val();
    var discount_percentage = $("#discount_percentage_status").val()
    var local_anesthesia_charge_service_code = $("#local_anesthesia_charge_service_code").val();



    if (bill_tag && bill_tag != "" && surgery_charge_service_code && surgery_charge_service_code != "") {

        if(parseInt(surgery_charge) > 0 && parseInt(discount_percentage) > 0){
            var discount_amount = surgery_charge * discount_percentage/100;
            $(".item-amount-"+surgery_charge_service_code).parents('tr').find('.discount_percentage_amount').val(discount_amount);
        }

        if(parseInt(surgery_charge) > 0){
            calculateItemWiseNetAmount();
        }

        clearTimeout(timeout);
        timeout = setTimeout(function () {
            let _token = $('#c_token').val();
            $.ajax({
                async: true,
                type: "POST",
                url: url,
                data: {
                    bill_tag : bill_tag,
                    surgery_charge : surgery_charge,
                    _token :_token
                },
                beforeSend: function () {

                },
                success: function (response) {
                    console.log(response)
                    if(response.data.cssd_charge ){
                        $(".item-amount-"+cssd_charge_service_code).val(response.data.cssd_charge);
                    } else {
                        $(".item-amount-"+cssd_charge_service_code).val(0);
                    }
                    if(response.data.asst_surgeon_charge ){
                        $(".item-amount-"+asst_service_charge_service_code).val(response.data.asst_surgeon_charge);
                    } else {
                        $(".item-amount-"+asst_service_charge_service_code).val(0);
                    }
                    if(response.data.local_anesthesia_charge ){
                        $(".item-amount-"+local_anesthesia_charge_service_code).val(response.data.local_anesthesia_charge);
                    } else {
                        $(".item-amount-"+local_anesthesia_charge_service_code).val(0);
                    }
                    calculateItemWiseNetAmount();
                },
                complete: function () { }
            });

        }, 500)
    } else {
        return false;
    }
}


function calculateItemWiseNetAmount(){
    $(".bill_amount").each(function(key, value){
        var amount = $(this).val();
        var discount = $(value).parents('tr').find('.discount_percentage_amount').val()
        if(amount){
            var bill_wise_net_amount = amount-discount;
            $(value).parents('tr').find('.bill_wise_net_amount').val(parseFloat(bill_wise_net_amount).toFixed(2));
        }
    });
    $(".bill_wise_net_amount").each(function(key, value){
        var amount = $(this).val();
        if(amount && parseInt(amount) > 0){
            if($(value).parents('tr').find('.bill_type_check').prop('checked') == false){
                $(value).parents('tr').find('.bill_type_check').trigger('click');
            }
        } else if(parseInt(amount) == 0 && $(value).parents('tr').find('.bill_type_check').prop('checked') == true){
            $(value).parents('tr').find('.bill_type_check').trigger('click');
        }
    });
    calculateTotal();

}
