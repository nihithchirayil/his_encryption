$(document).ready(function () {

    var current_visit_type = $("#current_visit_type").val();
    if(current_visit_type == "OP"){
        $(".ip_action_button").hide();
    }
    $("#investigationServiceDate").datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $(document).on('click', '.bg-orange', function (event) {
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $(document).on('click', '.op_btn', function (event) {
        $(".op_slide_box").addClass('open');
    });

    $(document).on('click', '.slide_close_btn', function (event) {
        $(".op_slide_box").removeClass('open');
    });



    $(document).on('click', '.ip_btn', function (event) {
        $(".ip_slide_box").addClass('open');
    });

    $(document).on('click', '.slide_close_btn', function (event) {
        $(".ip_slide_box").removeClass('open');
    });


    document.querySelectorAll('.select_button li').forEach(function (li) {
        li.addEventListener('click', function () {
          this.classList.toggle('active');
        });
    });



    setTimeout(function () {
        var patient_id = $('#patient_id').val();
        //alert(patient_id);
        fetch_all_allergy(patient_id);
        addNewMedicine();
    }, 500);

    $('.select2').select2();

    $('.modal').on('shown.bs.modal', function (e) {
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $('.selectsearch').select2();

    $(document).on('click', '.combined_view_date_list ul li a', function () {
        $('.combined_view_date_list ul li a').removeClass('active')
        $(this).addClass('active')
    })

    $(document).on('click', '.combined_view_date_list ul li', function () {
        var disset = $(this).attr('id')
        $(this)
            .closest('.combined_view_wrapper')
            .find('tr#' + disset)
            .addClass('active')
    })


    $(document).on('click', '.combined_view_date_list ul li a', function () {
        var disset = $(this).attr("id");
        $('.combined_view_date_list ul li a').removeClass("active");
        $('.table_header_bg_grey').removeClass("tr_highlight");
        $(this).addClass("active");
        $(this).closest('.combined_view_wrapper').find("tr." + disset).addClass('tr_highlight');
    });

    /*********** COMBINED VIEW *******************/

    $('.modal').on('shown.bs.modal', function (e) {
        $(".theadfix_wrapper").floatThead('reflow');
    });



    $(document).on('click', '.filter_expand', function () {
        $(this).closest('#filter_area').toggleClass('col-md-8 col-md-12');
        $('.collapse_content').toggleClass('hidden');
    });
    $(document).on('click', '.expand_preview_btn', function () {
        $(this).addClass('collapse_preview_btn');
        $(this).removeClass('expand_preview_btn');
        $(this).toggleClass('fa-expand fa-minus');
        $(".preview_left_box").toggleClass('col-md-12 col-md-6');
        $('.preview_container_wrapper').addClass('expand');
        $('.preview_right_box').removeClass('hidden');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });


    $(document).on('click', '.collapse_preview_btn', function () {
        $(this).removeClass('collapse_preview_btn');
        $(this).addClass('expand_preview_btn');
        $(this).toggleClass('fa-minus fa-expand');
        $(".preview_left_box").toggleClass('col-md-6 col-md-12');
        $('.preview_container_wrapper').removeClass('expand');
        $('.preview_right_box').addClass('hidden');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });

    $(document).on('click', '.expand_prescription_btn', function () {
        $(this).addClass('collapse_prescription_btn');
        $(this).removeClass('expand_prescription_btn');
        $(this).toggleClass('fa-expand fa-minus');
        $('.prescription_wrapper').addClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });

    $(document).on('click', '.collapse_prescription_btn', function () {
        $(this).addClass('expand_prescription_btn');
        $(this).removeClass('collapse_prescription_btn');
        $(this).toggleClass('fa-minus fa-expand');
        $('.prescription_wrapper').removeClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });

    $(document).on('click', '.expand_rightbox_btn', function () {
        $(this).addClass('collapse_rightbox_btn');
        $(this).removeClass('expand_rightbox_btn');
        $(this).toggleClass('fa-expand fa-minus');
        $('.right_wrapper_box').addClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });


    $(document).on('click', '.collapse_rightbox_btn', function () {
        $(this).addClass('expand_rightbox_btn');
        $(this).removeClass('collapse_rightbox_btn');
        $(this).toggleClass('fa-minus fa-expand');
        $('.right_wrapper_box').removeClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });

    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });

    $(document).on('click', '.notes_sec_list ul li', function () {
        var disset = $(this).attr("id");
        $('.notes_sec_list ul li').removeClass("active");
        $(this).addClass("active");
        $(this).closest('.notes_box').find(".note_content").css("display", "none");
        $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
    });

    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });


    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });

    $('.datepicker').datetimepicker({
        format: 'MMMM-DD-YYYY'
    });
    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
    $('.date_time_picker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });




    $('#next_review_date').datetimepicker({
        format: 'MMM-DD-YYYY',
        //minDate: new Date(),
    }).on('dp.change', function (e) {
        takeNextAppointment();
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

    $("#poplink").popover({
        html: true,
        placement: "right",
        trigger: "hover",
        title: function () {
            return $(".pop-title").html();
        },
        content: function () {
            return $(".pop-content").html();
        }
    });



    var patient_ip_status = $('#patient_ip_status').val();
    if (patient_ip_status == 3) {

        $('#btn_discharge_initiate_container').css("display", "inline");
        $('#btn_sent_for_biling').css("display", "none");
        $('#btn_revert_discharge').css("display", "none");
        $('#btn_revert_sent_for_biling').css("display", "none");

    } else if (patient_ip_status == 5) {
        $('#btn_discharge_initiate_container').css("display", "none");
        $('#btn_sent_for_biling').css("display", "inline");
        $('#btn_revert_discharge').css("display", "inline");

    } else if (patient_ip_status == 6) {
        $('#btn_discharge_initiate_container').css("display", "none");
        $('#btn_sent_for_biling').css("display", "none");
        $('#btn_revert_discharge').css("display", "none");
        $('#btn_revert_sent_for_biling').css("display", "inline");

    }
    else {
        $('#btn_discharge_initiate_container').css("display", "none");
        $('#btn_sent_for_biling').css("display", "inline");
        $('#btn_sent_for_biling').attr("disabled", "disabled");
        $('#btn_revert_discharge').css("display", "none");
        $('#btn_revert_sent_for_biling').css("display", "none");
    }


    $('.date_floatinput').datetimepicker({
        format: "MMM-DD-YYYY hh:mm a",
    });

    $("#death_mark_form > input[name='death_date_time']").datetimepicker({
        format: "MMM-DD-YYYY HH:mm",
    });

    var fav_form_id = $('#fav_form_id').val();
    loadForm(fav_form_id, 0);
    $("#forms option:selected").val(fav_form_id);


    //-----------promt to save assement data remainder-----------------------------

    $('#assesment_draft_mode').val(0);
    $(document).on('change', "#tab1 input[type=text],#tab1 input[type=checkbox],#tab1 input[type=radio],#tab1 input[type=date],#tab1 textarea", function (event) {
        $('#assesment_draft_mode').val(1);
        //Event.stop(event);
    });

    $(document).on('click', ".vertical_tab >li,.btn-nurse-task,.nav_title>a,.user-profile,.action-redirection,.side-menu>li>a", function (event) {

        var assesment_draft_mode = $('#assesment_draft_mode').val();
        if (assesment_draft_mode == 1) {


            cuteAlert({
                type: "question",
                title: "Assesment Changes Save Remainder",
                message: "Your Assesment changes not saved would you like to save?",
                confirmText: "Save",
                cancelText: "Discard"
            }).then((e) => {
                if (e == "confirm") {
                    window.save_encounter_status = "TRUE";
                    saveClinicalTemplate(1);
                    $('#assesment_draft_mode').val(0);

                } else {
                    $('#assesment_draft_mode').val(0);

                }
            })

        }

    });


    //-----------promt to save investigation data remainder-----------------------------

    $('#investigation_draft_mode').val(0);
    $(document).on('change', "#tab2 input[type=text],#tab2 input[type=checkbox],#tab2 input[type=radio],#tab2 input[type=date],#tab2 textarea", function () {

        $('#investigation_draft_mode').val(1);


    });

    $(document).on('click', ".vertical_tab >li,.btn-nurse-task,.nav_title>a,.user-profile,.action-redirection,.side-menu>li>a", function (event) {
        var investigation_draft_mode = $('#investigation_draft_mode').val();
        if (investigation_draft_mode == 1) {


            cuteAlert({
                type: "question",
                title: "Investigation Save Remainder",
                message: "Your Investigation changes not saved would you like to save?",
                confirmText: "Save",
                cancelText: "Discard"
            }).then((e) => {
                if (e == "confirm") {
                    saveInvestigation();
                    $('#investigation_draft_mode').val(0);

                } else {
                    $('#investigation_draft_mode').val(0);

                }
            })

        }

    });



    //-----------promt to save prescription data remainder-----------------------------

    $('#prescription_changes_draft_mode').val(0);
    $(document).on('change', "#drug input[type=text],#tab3 input[type=checkbox],#tab3 input[type=radio],#tab3 input[type=date],#tab3 textarea", function () {
        $('#prescription_changes_draft_mode').val(1);


    });

    $(document).on('click', ".vertical_tab >li,.btn-nurse-task,.nav_title>a,.user-profile,.action-redirection,.side-menu>li>a", function (event) {
        var prescription_changes_draft_mode = $('#prescription_changes_draft_mode').val();
        if (prescription_changes_draft_mode == 1) {


            cuteAlert({
                type: "question",
                title: "Prescription Save Remainder",
                message: "Your Prescription changes not saved would you like to save?",
                confirmText: "Save",
                cancelText: "Discard"
            }).then((e) => {
                if (e == "confirm") {
                    saveDoctorPrescriptions(1);
                    $('#prescription_changes_draft_mode').val(0);

                } else {
                    $('#prescription_changes_draft_mode').val(0);

                }
            })

        }

    });



    //-----------promt to save iv chart data remainder-----------------------------

    $('#iv_fluid_draft_mode').val(0);
    $(document).on('change', "#tab5 input[type=text],#tab5 input[type=checkbox],#tab5 input[type=radio],#tab5 input[type=date],#tab5 textarea", function () {
        iv_fluid_draft_mode = 1;
        $('#iv_fluid_draft_mode').val(1);


    });

    $(document).on('click', ".vertical_tab >li,.btn-nurse-task,.nav_title>a,.user-profile,.action-redirection,.side-menu>li>a", function (event) {

        var iv_fluid_draft_mode = $('#iv_fluid_draft_mode').val();
        if (iv_fluid_draft_mode == 1) {


            cuteAlert({
                type: "question",
                title: "Iv changes Save Remainder",
                message: "Your Iv changes not saved would you like to save?",
                confirmText: "Save",
                cancelText: "Discard"
            }).then((e) => {
                if (e == "confirm") {
                    saveIvChart();
                    $('#iv_fluid_draft_mode').val(0);

                } else {
                    $('#iv_fluid_draft_mode').val(0);

                }
            })

        }

    });


    //-----------promt to intake output return data remainder-----------------------------

    $('#intake_output_draft_mode').val(0);
    $(document).on('change', "#tab8 input[type=text],#tab8 input[type=checkbox],#tab8 input[type=radio],#tab8 input[type=date],#tab8 textarea", function () {

        $('#intake_output_draft_mode').val(1);

    });

    $(document).on('click', ".vertical_tab >li,.btn-nurse-task,.nav_title>a,.user-profile,.action-redirection,.side-menu>li>a", function (event) {

        var intake_output_draft_mode = $('#intake_output_draft_mode').val();
        if (intake_output_draft_mode == 1) {


            cuteAlert({
                type: "question",
                title: "Intake Output Save Remainder",
                message: "Your Intake Output changes not saved would you like to save?",
                confirmText: "Save",
                cancelText: "Discard"
            }).then((e) => {
                if (e == "confirm") {
                    SaveIntake();
                    SaveOutPut();
                    $('#intake_output_draft_mode').val(0);

                } else {
                    $('#intake_output_draft_mode').val(1);
                }
            })

        }

    });


    $(".theadfix_wrapper").floatThead('reflow');


    $(".pop-history-btn").popover({
        html: true,
        placement: "right",
        trigger: "hover",

        content: function () {
            return $(".pop-content-history").html();
        }
    });

    $(".pop-history-btn").mouseenter(function () {
        $(".pos_stat_hover ").css('position', 'static');
    });


    $(".pop-history-btn").mouseleave(function () {
        $(".pos_stat_hover ").css('position', 'relative');
    });

});

$(document).on('click', '.combined_view_date_list ul li a', function () {
    var disset = $(this).attr("id");
    $('.combined_view_date_list ul li a').removeClass("active");
    $('.table_header_bg_grey').removeClass("tr_highlight");
    $(this).addClass("active");
    $(this).closest('.combined_view_wrapper').find("tr." + disset).addClass('tr_highlight');
});


//----------------Additional Room management------------------------------------------
$(document).on('click', '#btn_additional_room_mgmt', function () {
    $('#nursing_modal').modal('toggle');
    var url = $('#base_url').val() + "/nursing/selectAdditionalRoomMgmt";
    var visit_id = $("#visit_id").val();
    var nursing_station = $('#nursing_station').val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id + '&nursing_station=' + nursing_station,
        beforeSend: function () {
            $('#nursing_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#nursing_modal_body").html('');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $("#nursing_modal_body").html(obj.room_types_list);
            $("#nursing_modal_body").append(obj.requestHistory);

        },
        complete: function () {
            $('#nursing_modal_body').LoadingOverlay("hide");
            $('#nursing_modal_title').html("Additional Room Management");
        }
    });
});

function saveAdditionalBedTransferRequest() {
    var room_type = $("#additional_bed_trasnfer_room_type").val() ? $("#additional_bed_trasnfer_room_type").val(): 0;
    var nursing_station = $("#additional_bed_trasnfer_nursing_station").val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var user_id = $("#user_id").val();

    if (nursing_station != '') {
        var url = $('#base_url').val() + "/nursing/saveAdditionalBedTransferRequest";
        $.ajax({
            type: "GET",
            url: url,
            data: 'room_type=' + room_type +
                '&visit_id=' + visit_id +
                '&nursing_station=' + nursing_station +
                '&user_id=' + user_id +
                '&patient_id=' + patient_id,
            beforeSend: function () {
                $('#nursing_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if (data == 1) {
                    Command: toastr["success"]("Success!");
                }
                else if (data == 2) {
                    Command: toastr["warning"]("Already pending additional room transfer request!");
                    return;
                }
                else if (data == 3) {
                    Command: toastr["warning"]("Already have additional room!");
                    return;
                }
                else {
                    Command: toastr["error"]("Something went wrong!");
                    return;
                }
                selectAdditionalRoomHistory();
            },
            complete: function () {
                $('#nursing_modal_body').LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr["warning"]("Please select nursing station.");
    }
}
function selectAdditionalRoomHistory() {
    var visit_id = $("#visit_id").val();
    var url = $('#base_url').val() + "/nursing/selectAdditionalRoomHistory";
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () { },
        success: function (data) {
            $("#additional_room_request_history").html(data);
        },
        complete: function () { }
    });
}
function deleteAdditionalRoomTransferReq(req_id) {
    var visit_id = $("#visit_id").val();
    var url = $('#base_url').val() + "/nursing/deleteAdditionalRoomTransferReq";
    var param = { visit_id: visit_id, req_id: req_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () { },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Deleted Successfully!");
            }
            else {
                Command: toastr["error"]("Someting went wrong!");
            }
        },
        complete: function () {
            selectAdditionalRoomHistory();
        }
    });
}
//-------------------END------------------------------------------------------
//----------------Room and bed management------------------------------------------
$(document).on('click', '#btn_room_mgmt', function () {

    $('#nursing_modal').modal('toggle');
    var url = $('#base_url').val() + "/nursing/selectRoomAndBedMgmt";
    var visit_id = $("#visit_id").val();
    var nursing_station = $('#nursing_station').val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id + '&nursing_station=' + nursing_station,
        beforeSend: function () {
            $('#nursing_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            $("#nursing_modal_body").html('');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $("#nursing_modal_body").html(obj.room_types_list);
            $("#nursing_modal_body").append(obj.requestHistory);
        },
        complete: function () {
            $('#nursing_modal_body').LoadingOverlay("hide");
            $('#nursing_modal_title').html("Room And Bed Management");
        }
    });
});

function selectRoomRequestHistory() {
    var visit_id = $("#visit_id").val();
    var url = $('#base_url').val() + "/nursing/selectRoomRequestHistory";
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () { },
        success: function (data) {
            $("#request_history").html(data);
        },
        complete: function () { }
    });
}

function deleteRoomTransferReq(req_id) {
    var visit_id = $("#visit_id").val();
    var url = $('#base_url').val() + "/nursing/deleteRoomTransferReq";
    var param = { visit_id: visit_id, req_id: req_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () { },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Deleted Successfully!");
            }
            else {
                Command: toastr["error"]("Someting went wrong!");
            }
        },
        complete: function () {
            selectRoomRequestHistory();
        }
    });
}

function saveBedTransferRequest() {
    var room_type = $("#bed_trasnfer_room_type").val() ? $("#bed_trasnfer_room_type").val(): 0;
    var nursing_station = $("#bed_trasnfer_nursing_station").val();
    var retain_status = 0;
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var user_id = $("#user_id").val();
    if ($("#retain_status").is(':checked')) {
        retain_status = 1;
    }

    if (nursing_station != '') {
        var url = $('#base_url').val() + "/nursing/saveBedTransferRequest";
        $.ajax({
            type: "GET",
            url: url,
            data: 'room_type=' + room_type +
                '&retain_status=' + retain_status +
                '&visit_id=' + visit_id +
                '&nursing_station=' + nursing_station +
                '&user_id=' + user_id +
                '&patient_id=' + patient_id,
            beforeSend: function () {
                $('#nursing_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                //console.log(data);
                if (data == 1) {
                    Command: toastr["success"]("Success!");
                }
                else if (data == 2) {
                    Command: toastr["warning"]("Already pending room transfer request!");
                    return;
                }
                else {
                    Command: toastr["error"]("Something went wrong!");
                    return;
                }
                // $("#nursing_modal_body").html(data);
                selectRoomRequestHistory();
            },
            complete: function () {
                $('#nursing_modal_body').LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr["warning"]("Please select nursing station.");
    }
}


//-------------Room and bed management ends---------------------------------------

//-------------Patient private notes----------------------------------------------
$(document).on('click', '#btn_private_notes', function () {

    $('#nursing_modal').modal('toggle');
    var url = $('#base_url').val() + "/nursing/selectPrivateNotes";
    var patient_id = $("#patient_id").val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id,
        beforeSend: function () {
            $('#nursing_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $("#nursing_modal_body").html(data);
        },
        complete: function () {
            $('#nursing_modal_body').LoadingOverlay("hide");
            $('#nursing_modal_title').html("Private Notes");
        }
    });
});
//-------------Patient private notes ends-----------------------------------------
//====================discharge==================================================================================

//------------discharge initialte-------------------------------------------------
$(document).on('click', '#btn_discharge_initiate', function () {
    var url = $('#base_url').val() + "/nursing/initaiteDischarge";
    var visit_id = $("#visit_id").val();
    var discharge_remarks = $('#discharge_remarks').val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id + '&discharge_remarks=' + discharge_remarks,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                $("#btn_sent_for_biling").css("display", "inline");
                $("#btn_revert_discharge").css("display", "inline");
                $("#btn_discharge_initiate").css("display", "none");
                Command: toastr["success"]("Discharge Initiated!");
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {

        }
    });
});
//------------discharge initialte ends-----------------------------------------

//------------Sent for billing-------------------------------------------------
$(document).on('click', '#btn_sent_for_biling', function () {
    var url = $('#base_url').val() + "/nursing/sendForBilling";
    var visit_id = $("#visit_id").val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {

                $('#btn_discharge_initiate_container').css("display", "none");
                $('#btn_sent_for_biling').css("display", "none");
                $('#btn_revert_discharge').css("display", "none");
                $('#btn_revert_sent_for_biling').css("display", "inline");



                Command: toastr["success"]("Sent Fot Billing!");
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {
            $('#nursing_modal_body').LoadingOverlay("hide");
            $('#nursing_modal_title').html("Private Notes");
        }
    });
});
//------------Sent for billing ends--------------------------------------------


$(document).on("click", ".radiologyResultsBtn", function(){
    fetchRadiologyResults();
});


$(document).on("change", "#seach_service_id", function(){
    fetchRadiologyResults();
});

function fetchRadiologyResults(){

    let patient_id = $('#patient_id').val();
    let seach_service_id = $('#seach_service_id').val();
    var url = $('#base_doumenturl').val() + "/emr/radiologyResults";
    if (patient_id) {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                patient_id: patient_id,
                seach_service_id:seach_service_id
            },
            beforeSend:function(){
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("body").LoadingOverlay("hide");
                $("#radiology_results_data").html(data.html);
                $("#seach_service_id").empty();
                $("#seach_service_id").append('<option value="">Select Service</option>');
                $.each(data.service_list, function(key, val){
                    $("#seach_service_id").append('<option value="'+val.id+'">'+val.service_desc+'</option>');
                })

                if(seach_service_id){
                    $("#seach_service_id").val(seach_service_id);
                }

                $("#radiology_results_modal").modal('show');
            },
            error: function () {
                $("body").LoadingOverlay("hide");
                console.log('failed to fetch results..!')
            }
        });
    }
}


function fetchRadiologyUploadsData(){

    let patient_id = $('#patient_id').val();
    var url = $('#base_doumenturl').val() + "/emr/fetchRadiologyUploadsData";
    if (patient_id) {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                patient_id: patient_id,
            },
            beforeSend:function(){
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("body").LoadingOverlay("hide");
                $("#radiology_upload_data").html(data.html);
            },
            error: function () {
                $("body").LoadingOverlay("hide");
                console.log('failed to fetch radiology uploads..!')
            }
        });
    }
}

$(document).on("click", ".pacsViewerBtn", function(){
    let pacs_viewer_prefix = $('#pacs_viewer_prefix').val();
    var accession_no = $(this).attr('data-accession-no');
    if (pacs_viewer_prefix && accession_no) {
        $("#pacs_viewer_iframe").attr('src', pacs_viewer_prefix+accession_no);
    }

    $("#radiology_pacs_viewer_iframe_modal").modal('show');
});

$(document).on("click", ".pacsViewReportBtn", function(){
    let pacs_report_prefix = $('#pacs_report_prefix').val();
    var accession_no = $(this).attr('data-accession-no');

    window.open(pacs_report_prefix+accession_no, '_blank');
    // if (pacs_report_prefix && accession_no) {
    //     $("#pacs_view_report_iframe").attr('src', pacs_report_prefix+accession_no);
    // }

    // $("#radiology_pacs_view_report_iframe_modal").modal('show');
});

//------------Revert Discharge-------------------------------------------------
$(document).on('click', '#btn_revert_discharge', function () {
    var url = $('#base_url').val() + "/nursing/revertDischarge";
    var visit_id = $("#visit_id").val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $("#btn_sent_for_biling").css("display", "none");
                $("#btn_sent_for_biling").removeAttr("disabled");
                $("#btn_revert_discharge").css("display", "none");
                $("#btn_discharge_initiate").css("display", "inline");
                Command: toastr["success"]("Discharge Reverted!");
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {
            $('#nursing_modal_body').LoadingOverlay("hide");
            $('#nursing_modal_title').html("Private Notes");
        }
    });
});
//------------Revert Discharge ends--------------------------------------------
//------------Revert sent for billing-------------------------------------------------
$(document).on('click', '#btn_revert_sent_for_biling', function () {
    var url = $('#base_url').val() + "/nursing/revertSentForBilling";
    var visit_id = $("#visit_id").val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'visit_id=' + visit_id,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $("#btn_sent_for_biling").css("display", "inline");
                $("#btn_sent_for_biling").removeAttr("disabled");
                $("#btn_revert_discharge").css("display", "none");
                $("#btn_discharge_initiate").css("display", "none");
                $("#btn_revert_sent_for_biling").css("display", "none");
                Command: toastr["success"]("Discharge Reverted!");
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {
            $('#nursing_modal_body').LoadingOverlay("hide");
            $('#nursing_modal_title').html("Private Notes");
        }
    });
});
//------------Revert sent for billing--------------------------------------------

//====================discharge ends=============================================================================


function saveNurseEncounterData() {
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let investigation_head_id = $('#investigation_head_id').val();

    if (encounter_id == "" || visit_id == "") {
        Command: toastr["error"]("Error.! Visit And Encounter Not Found.");
        return false;
    }

    //save as drafts when not saved
    //prescription
    if ($('#medicine-listing-table tbody > tr').find('input[name="selected_item_code[]"]').length == 0) {
        //no medicine in the list
    } else {
        // saveDoctorPrescriptions();
    }
    //assessment
    saveClinicalTemplate(1);

    //return;
    //convert drafts
    // if (patient_id != '') {
    //     var url = $('#base_url').val() + "/emr/save-doctors-encounter-datas";
    //     $.ajax({
    //         type: "POST",
    //         url: url,
    //         data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id + '&investigation_head_id=' + investigation_head_id,
    //         beforeSend: function () {
    //             $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
    //         },
    //         success: function (data) {
    //             if (data.status = 1) {
    //                 Command: toastr["success"]("Saved Successfully.");
    //                 window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
    //             } else if (data.status = 2) {

    //             } else {
    //                 Command: toastr["error"]("Error.");
    //             }
    //         },
    //         complete: function () {
    //             enableSaveBtn();
    //             $("body").LoadingOverlay("hide");
    //         }
    //     });
    // }
}

// function saveInvestigation() {
//     let _token = $('#c_token').val();
//     let patient_id = $('#patient_id').val();
//     let visit_id = $('#visit_id').val();
//     let encounter_id = $('#encounter_id').val();
//     let investigation_head_id = $('#investigation_head_id').val();
//     var location_id = $('#location_id').val();

//     if (encounter_id == "" || visit_id == "") {
//         Command: toastr["error"]("Error.! Visit And Encounter Not Found.");
//         return false;
//     }

//     if (patient_id != '') {
//         var url = $('#base_url').val() + "/emr/updateInvestigationStatus";
//         $.ajax({
//             type: "POST",
//             url: url,
//             data: '_token=' + _token + '&investigation_head_id=' + investigation_head_id + '&location_id=' + location_id + '&visit_id=' + visit_id,
//             beforeSend: function () {
//                 $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
//             },
//             success: function (data) {
//                 //console.log(data);
//                 $("body").LoadingOverlay("hide");
//                 if (data == 1) {
//                     Command: toastr["success"]("Saved Successfully.");
//                     // if ($("#print_investigation").is(':checked')) {
//                     //      printInvestigationHistoryList(investigation_head_id);
//                     // }
//                 }
//                 else {
//                     Command: toastr["error"]("Error.");
//                 }

//                 $('#investigation_draft_mode').val(0);
//             },
//             complete: function () {
//                 location.reload();
//                 $('#investigation_draft_mode').val(0);
//                 // window.onbeforeunload = null;
//             }
//         });
//     }
// }

function sortTable(tableId, n, typeid) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (typeid == 1) {
                if (dir == "asc") {
                    if (parseInt(x.innerHTML.toLowerCase()) > parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML.toLowerCase()) < parseInt(y.innerHTML.toLowerCase())) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}


var public_sort_var = 2;

function myFunction(search_type, InputId, tableId) {
    var searchType = $('input[name=' + search_type + ']:checked').val();
    var serch_method_conf = "anywhere";
    var input, filter, table, tr, td, i;
    input = document.getElementById(InputId);
    filter = input.value.toUpperCase();
    table = document.getElementById(tableId);
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[searchType];

        if (td) {
            if (serch_method_conf == "anywhere") {
                if (td.innerHTML.toUpperCase().match(filter)) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } else {
                if (td.innerHTML.toUpperCase().startsWith(filter)) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
        $('.theadscroll').perfectScrollbar("update");
    }

    if (searchType == 5) {
        sortTable('myTable', 5, public_sort_var);
        if (public_sort_var == 1) {
            public_sort_var = 2;
        } else {
            public_sort_var = 1;
        }
    }

}

function applyFilter(classname) {
    if (classname == 'all') {
        $("#myTable tbody tr").show();
    } else {
        $("#myTable tbody tr").hide();
        $("#myTable tbody tr." + classname).show();
    }
}

$("#poplink").popover({
    html: true,
    placement: "right",
    trigger: "hover",
    title: function () {
        return $(".pop-title").html();
    },
    content: function () {
        return $(".pop-content").html();
    }
});



function goToSelectedPatient(patient_id) {
    window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
}

$(document).on('DOMMouseScroll mousewheel', '.Scrollable', function (ev) {
    var $this = $(this),
        scrollTop = this.scrollTop,
        scrollHeight = this.scrollHeight,
        height = $this.innerHeight(),
        delta = (ev.type == 'DOMMouseScroll' ?
            ev.originalEvent.detail * -40 :
            ev.originalEvent.wheelDelta),
        up = delta > 0;

    var prevent = function () {
        ev.stopPropagation();
        ev.preventDefault();
        ev.returnValue = false;
        return false;
    }

    if (!up && -delta > scrollHeight - height - scrollTop) {
        // Scrolling down, but this will take us past the bottom.
        $this.scrollTop(scrollHeight);
        return prevent();
    } else if (up && delta > scrollTop) {
        // Scrolling up, but this will take us past the top.
        $this.scrollTop(0);
        return prevent();
    }
});

//====================Intake Output==============================================

function add_new_intake(obj) {
    var current_date = $('#search_date_web').val();
    var current_time = $('#search_time_web').val();
    var current_row = obj.closest('tr').rowIndex;
    var table = document.getElementById("intake_border");
    let row_count = table.rows.length;
    let row_id = row_count + 1;
    if (current_row + 1 == row_count) {
        if (parseInt(row_count) > 0) {
            let last_id = $('#intake_border tr:last').attr('row-id');
            if (last_id != undefined) {
                row_id = parseInt(last_id) + 1;
            }
        }
        var row = table.insertRow(row_count);
        row.setAttribute("row-id", row_id, 0);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);


        cell1.innerHTML = "<div class='col-md-7 padding_xs' style='text-align:center'><input type='text' id='io_date_" + row_id + "' name='io_date[]' value='" + current_date + "' class='form-control datepicker' data-attr='date' placeholder='Date'></div><div class='col-md-5 padding_xs' style='text-align:center'><input type='text' value='" + current_time + "' id='io_time_" + row_id + "' name='io_time[]' onclick='add_new_intake(this);' class='form-control timepicker' data-attr='date' placeholder='Time'></div>";


        cell2.innerHTML = "<input type='text' onclick='add_new_intake(this);' onblur='calculate_intake(" + row_id + ");' id='intake_iv_" + row_id + "'  name='intake_iv[]'  class='form-control int_type'>";

        cell3.innerHTML = "<input type='text' onclick='add_new_intake(this);' onblur='calculate_intake(" + row_id + ");'  id='intake_oral_" + row_id + "'  name='intake_oral[]'  class='form-control int_type'>";

        cell4.innerHTML = "<input type='text' readonly id='intake_total_" + row_id + "'  name='intake_total[]'  class='form-control int_type'>";

        cell5.innerHTML = "<button type='button' class='btn btn-sm bg-red del-intake-list-row' name='delete_intake[]' id='delete_intake_" + row_id + " '><i class='fa fa-trash'></i></button>";

        $('.datepicker').datetimepicker({
            format: 'MMMM-DD-YYYY'
        });
        $('.timepicker').datetimepicker({
            format: 'hh:mm A'
        });
    }

}

function add_out_put(obj) {
    var current_date = $('#search_date_web').val();
    var current_time = $('#search_time_web').val();
    var current_row = obj.closest('tr').rowIndex;
    var table = document.getElementById("output_border");
    let row_count = table.rows.length;
    let row_id = row_count + 1;
    if (current_row + 1 == row_count) {
        if (parseInt(row_count) > 0) {
            let last_id = $('#output_border tr:last').attr('row-id');
            if (last_id != undefined) {
                row_id = parseInt(last_id) + 1;
            }
        }
        var row = table.insertRow(row_count);
        row.setAttribute("row-id", row_id, 0);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);


        cell1.innerHTML = "<div class='col-md-7 padding_xs' style='text-align:center'><input type='text' id='out_date_" + row_id + "' name='out_date[]' value='" + current_date + "' class='form-control datepicker' data-attr='date' placeholder='Date'></div><div class='col-md-5 padding_xs' style='text-align:center'><input type='text' value='" + current_time + "' id='out_time_" + row_id + "' name='out_time[]' onclick='add_out_put(this);' class='form-control timepicker' data-attr='date' placeholder='Time'></div>";


        cell2.innerHTML = "<input type='text' onclick='add_out_put(this);' onblur='calculate_output(" + row_id + ");' id='output_urine_" + row_id + "'  name='output_urine[]'  class='form-control int_type'>";

        cell3.innerHTML = "<input type='text' onclick='add_out_put(this);' onblur='calculate_output(" + row_id + ");' id='output_others_" + row_id + "'  name='output_others[]'  class='form-control int_type'>";

        cell4.innerHTML = "<input type='text' readonly id='output_total_" + row_id + "'  name='output_total[]'  class='form-control int_type'>";

        cell5.innerHTML = "<button type='button' class='btn btn-sm bg-red del-ouput-list-row' name='delete_ouput[]' id='delete_ouput_" + row_id + " '><i class='fa fa-trash'></i></button>";

        $('.datepicker').datetimepicker({
            format: 'MMMM-DD-YYYY'
        });
        $('.timepicker').datetimepicker({
            format: 'hh:mm A'
        });
    }

}
// document.getElementsByClassName("intake").click(function(){
//     add_new_intake();
// });

$(document).on('keypress', '.int_type', function (evt) {
    var theEvent = evt || window.event;
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
});

function calculate_intake(i) {
    var iv = ($('#intake_iv_' + i).val() != '') ? $('#intake_iv_' + i).val() : 0;
    var oral = ($('#intake_oral_' + i).val() != '') ? $('#intake_oral_' + i).val() : 0;
    //alert(i);

    $('#intake_total_' + i).val(parseInt(iv) + parseInt(oral));
}

function calculate_output(i) {
    var output_urine = ($('#output_urine_' + i).val() != '') ? $('#output_urine_' + i).val() : 0;
    var output_others = ($('#output_others_' + i).val() != '') ? $('#output_others_' + i).val() : 0;
    $('#output_total_' + i).val(parseInt(output_urine) + parseInt(output_others));

}
$(document).on('click', '.del-tapperdose-list-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        $(tr).remove();

    }
});

$(document).on('click', '.del-intake-list-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let del_id = $(tr).find('input[name="intake_id[]"]').val();
        //alert(del_id);
        if (del_id != '' && del_id != 0 && del_id != undefined) {
            deleteIntakeRowFromDb(del_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }

    }
});

//delete single row from db
function deleteIntakeRowFromDb(id) {
    var url = $('#base_url').val() + "/nursing/intake-delete-item";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}

$(document).on('click', '.del-ouput-list-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let del_id = $(tr).find('input[name="output_id[]"]').val();
        if (del_id != '' && del_id != 0 && del_id != undefined) {
            deleteOutputRowFromDb(del_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }

    }
});


function deleteOutputRowFromDb(id) {
    var url = $('#base_url').val() + "/nursing/output-delete-item";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {

        }
    });
}

function SaveIntake() {
    let _token = $('#c_token').val();
    var dataParam = $('#io_chart').serialize();
    let visit_id = $('#visit_id').val();
    let patient_id = $('#patient_id').val();
    let encounter_id = $('#encounter_id').val();
    var url = $('#base_url').val() + "/nursing/SaveIntake";

    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: dataParam + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id,
        beforeSend: function () {
            $('#intake_border').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {

            $('#intake_border').LoadingOverlay("hide");
            console.log(data);
            if (data == 1) {
                Command: toastr["success"]("Saved Successfully.");
            }
            else {
                Command: toastr["error"]("Please check internet connection!");
            }
            intake_output_draft_mode = 0;
        },
        complete: function () {

        }
    });

}


function SaveOutPut() {
    let _token = $('#c_token').val();
    var dataParam = $('#io_chart').serialize();
    let visit_id = $('#visit_id').val();
    let patient_id = $('#patient_id').val();
    let encounter_id = $('#encounter_id').val();
    var url = $('#base_url').val() + "/nursing/SaveOutPut";

    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: dataParam + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&encounter_id=' + encounter_id,
        beforeSend: function () {
            $('#output_border').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#output_border').LoadingOverlay("hide");
            console.log(data);
            if (data == 1) {
                Command: toastr["success"]("Saved Successfully.");
            }
            else {
                Command: toastr["error"]("Please check internet connection!");
            }
            intake_output_draft_mode = 0;
        },
        complete: function () {

        }
    });

}
//====================Intake Output Ends==========================================

//----------------Advance Detail------------------------------------------
$(document).on('click', '#btn_advnc_dtail', function () {

    $('#advance_detail').modal('toggle');
    var url = $('#base_url').val() + "/nursing/advanceDetail";
    var patient_id = $("#patient_id").val();
    // alert(patient_id);
    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id,
        beforeSend: function () {
            $('#advance_detail_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $("#advance_detail_body").html(obj.pending_bills_list);
            //  $("#advance_detail_body").append(obj.requestHistory);
        },
        complete: function () {
            $('#advance_detail_body').LoadingOverlay("hide");
            $('#advance_detail_title').html("Patient Advance Detail");
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
});

function saveAdmitDctr() {
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();

    if (doctor_id != '') {
        var url = $('#base_url').val() + "/nursing/saveAdmitDctr";
        $.ajax({
            type: "GET",
            url: url,
            data: 'doctor_id=' + doctor_id +
                '&visit_id=' + visit_id +
                '&patient_id=' + patient_id,

            beforeSend: function () {
                $('#chnge_dctr_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                //console.log(data);
                if (data == 1) {
                    Command: toastr["success"]("Success!");
                }
                else {
                    Command: toastr["error"]("Something went wrong!");
                    return;
                }

            },
            complete: function () {
                $('#chnge_dctr_body').LoadingOverlay("hide");
            }
        });
    } else {
        Command: toastr["warning"]("Please select doctor.");
    }
}

function showPainScore() {
    $('#modal_painScore').modal('toggle');
}

// function medicine_return(){
//     var patient_id = $('#patient_id').val();
//     var visit_id = $('#visit_id').val();
//     var url = $('#base_url').val() + "/nursing/medicineReturn";
//     $.ajax({
//         type: "GET",
//         url: url,
//         data: $('form#frm-score').serialize()+'&visit_id='+visit_id+'&patient_id='+patient_id,
//         beforeSend: function () {

//         },
//         success: function (data) {
//             Command: toastr["error"](data);
//             console.log(data);return;
//           if(data == 1){

//           }
//           else{
//           }
//         },
//         complete: function () {

//         }
//     });
// }

function saveScore(action) {
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/nursing/savePainScore";
    $.ajax({
        type: "GET",
        url: url,
        data: $('form#frm-score').serialize() + '&visit_id=' + visit_id + '&patient_id=' + patient_id,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]('Saved Successfully.');
                $('#modal_painScore').modal('hide');

            }
            else {
                Command: toastr["error"]('Details not saved..');
            }
        },
        complete: function () {

        }
    });
}

function markDeath() {
    let confirmed_doctor_id_hidden = $("#confirmed_doctor_id_hidden").val();
    let death_date_time = $("#death_date_time").val();
    let death_reason = $("#death_reason").val().trim();

    let visit_id = $("#visit_id").val();
    let patient_id = $("#patient_id").val();

    if (confirmed_doctor_id_hidden > 0 && death_date_time != '') {

        var url = $('#base_url').val() + "/nursing/deathMarking";
        $.ajax({
            type: "GET",
            url: url,
            data: 'confirmed_doctor_id_hidden=' + confirmed_doctor_id_hidden + '&death_date_time=' + death_date_time + '&death_reason=' + death_reason + '&visit_id=' + visit_id + '&patient_id=' + patient_id,
            beforeSend: function () {
                $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (response) {
                if (response == 1) {
                    //success
                    toastr.success("Death Marked.");
                } else if (response == 2) {
                    //Already Marked
                    toastr.warning("Already Marked !.");
                } else {
                    //Error
                    toastr.danger("Error !.");
                }
            },
            complete: function () {
                $("body").LoadingOverlay("hide");
            }
        });


    } else {
        toastr.warning("Please Fill All Fields. Error !.");
    }

}

function LabResultsTrend() {
    var patient_id = $('#patient_id').val();
    var encounter_id = $('#encounter_id').val();
    if (patient_id != '') {
        var url = $('#base_url').val() + "/nursing/labResultTrends";
        $.ajax({
            url: url,
            type: "GET",
            data: "patient_id=" + patient_id + "&encounter_id=" + encounter_id,
            beforeSend: function () {

            },
            success: function (data) {
                // console.log(data);
                $('#lab_restuls_data').html(data);
            },
            complete: function () {
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            }
        });
    }
}

function getLabSubtestResult(id) {
    var url = $('#base_url').val() + "/nursing/getLabSubtestResult";
    $.ajax({
        url: url,
        type: "GET",
        data: "result_id=" + id,
        beforeSend: function () {

        },
        success: function (data) {
            $('#modal_lab_sub_results').modal('toggle');
            $('#lab_sub_restuls_data').html(data);
        },
        complete: function () {

        }
    });
}

function savePrescription() {
    var encounter_id = $('#encounter_id').val();

    if ($('#iv-medicine-listing-table tbody > tr').find('input[name="iv_selected_item_code[]"]').length != 0) {
        saveDoctorConsumablePrescriptions();
    }

    saveDoctorPrescriptions();

    // var url = $('#base_url').val() + "/nursing/updatePrescriptionStatus";
    // $.ajax({
    //     url: url,
    //     type: "GET",
    //     data: "encounter_id=" + encounter_id,
    //     beforeSend: function() {

    //     },
    //     success: function(data) {
    //         if (data == 1) {
    //             //toastr.success("Prescription saved successfully!");
    //         }
    //         $('#prescription_changes_draft_mode').val(0);
    //     },
    //     complete: function() {

    //     }
    // });
}

function add_medicine_return() {

    var visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/nursing/addMedicineReturn";
    $.ajax({
        url: url,
        type: "GET",
        data: "visit_id=" + visit_id,
        beforeSend: function () {
            $('#modal_add_medicine_return').modal('show');
            $('#add_medication_return_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (data) {
            $('#add_medication_return_data').html(data);
            $('#add_medication_return_data').LoadingOverlay("hide");
        },
        complete: function () {

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

        }
    });
}

function getYellowSheetServices() {
    var visit_id = $('#visit_id').val();
    window.open($('#base_url').val() + "/master/mapYellowSheetPatient/" + visit_id, '_self');
}

function checkReturnQty(obj, available) {
    var current_qty = $(obj).val();
    //console.log(current_qty+'####'+available);
    if (parseInt(current_qty) > parseInt(available)) {
        Command: toastr["error"]("Return quantity should not greater than available quantity!");
        $(obj).val('');
        $(obj).focus();
    }
}

function backToRetain(visit_id, current_bed_id, user_id) {
    var url = $('#base_url').val() + "/nursing/backToRetain";
    $.ajax({
        url: url,
        type: "GET",
        data: "visit_id=" + visit_id +
            '&current_bed_id=' + current_bed_id +
            '&user_id=' + user_id,
        beforeSend: function () {
            $('#nursing_modal').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#nursing_modal').LoadingOverlay("hide");
            if (data == 1) {
                Command: toastr["success"]("Patient sent back to retianed bed!");
                $('#nursing_modal').modal('hide');
                $('#btn_back_to_dashboard').trigger('click');
            } else if(data == 2){
                Command: toastr["error"]("Bed transfer already requested. Please cancel the transfer request to continue..!");
            }

            $('#nursing_modal').modal('hide');
        },
        complete: function () {

        }
    });
}

function saveMedicationReturn() {
    let _token = $('#c_token').val();
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var location_id = $('#location_id').val();
    var user_id = $('#user_id').val();
    var dataParam = $('#medication_return_form').serialize();
    var url = $('#base_url').val() + "/nursing/SaveMedicationReturn";
    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: dataParam + '&_token=' + _token + '&patient_id=' + patient_id + '&visit_id=' + visit_id + '&location_id=' + location_id + '&user_id=' + user_id,

        beforeSend: function () {
            $('#add_medication_return_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#add_medication_return_data').LoadingOverlay("hide");
            if (data == 1) {
                Command: toastr["success"]("Medicine return intent requested successfully!");
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {
            // $('#modal_add_medicine_return').modal('hide');
            add_medicine_return();
        }
    });

}

function LoadMedicineReturnResults() {
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    if (visit_id != '') {
        var url = $('#base_url').val() + "/nursing/LoadMedicineReturnResults";
        $.ajax({
            url: url,
            type: "GET",
            data: "patient_id=" + patient_id + "&visit_id=" + visit_id,
            beforeSend: function () {

            },
            success: function (data) {
                $('#medicine_return_intent_results').html(data);
            },
            complete: function () {

            }
        });
    }
}

function editMedicineReturn(available_quantity, mrid, req_qty) {
    $('#med_edit_return_qty').val(req_qty);
    $('#medicine_return_current_qty').val(req_qty);
    $('#medicine_return_edit_id').val(mrid);
    $('#medicine_return_edit_available').val(available_quantity);
    $('#modal_add_medicine_edit_return').modal('toggle');
}

function checkReturnEditQty() {
    var available_qty = $('#medicine_return_edit_available').val();
    var return_qty = $('#med_edit_return_qty').val();
    var total_qty = parseInt(available_qty) + parseInt(return_qty);
    console.log(available_qty + '###' + return_qty + '###' + total_qty);

    if (parseInt(return_qty) > parseInt(total_qty)) {
        Command: toastr["error"]("Return quantity should not greater than available quantity!");
        $('#med_edit_return_qty').val('');
        $('#med_edit_return_qty').focus();
        return;
    }
}

function update_medicine_return() {
    var medicine_return_edit_id = $('#medicine_return_edit_id').val();
    var med_edit_return_qty = $('#med_edit_return_qty').val();
    //checkReturnEditQty();
    var visit_id = $('#visit_id').val();
    if (visit_id != '') {
        var url = $('#base_url').val() + "/nursing/updateMedicineReturn";
        $.ajax({
            url: url,
            type: "GET",
            data: "medicine_return_edit_id=" + medicine_return_edit_id + "&med_edit_return_qty=" + med_edit_return_qty,
            beforeSend: function () {

            },
            success: function (data) {
                if (data == 1) {
                    Command: toastr["success"]("Medicine return intent requested successfully!");
                    $('#modal_add_medicine_edit_return').modal('hide');
                    LoadMedicineReturnResults();
                } else {
                    Command: toastr["error"]("Something went wrong!");
                }
            },
            complete: function () {

            }
        });
    }
}

function deleteMedicineReturn(available_quantity, mrid, req_qty) {
    var medicine_return_edit_id = $('#medicine_return_edit_id').val();
    var url = $('#base_url').val() + "/nursing/deleteMedicineReturn";
    $.ajax({
        url: url,
        type: "GET",
        data: "medicine_return_edit_id=" + mrid,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                Command: toastr["success"]("Medicine return deleted successfully!");
                LoadMedicineReturnResults();
            } else {
                Command: toastr["error"]("Something went wrong!");
            }
        },
        complete: function () {

        }
    });
}

function gotoMedicationChart() {
    var patient_id = $('#patient_id').val();
    var url = $('#base_url').val() + "/nursing/MedicationChartReport/?patient_id=" + patient_id;
    window.open(url, '_self');
}

$('#btn_dis_summary').click(function () {
    var patient_id = $('#patient_id').val();
    var url = $('#base_url').val() + "/nursing/showPatientSummaryList";
    $.ajax({
        url: url,
        type: "GET",
        data: "patient_id=" + patient_id,
        beforeSend: function () {
            $('#summary_list_modal').modal('show');
            $('#summary_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_list_data').LoadingOverlay("hide");
            $('#summary_list_data').html(data);
        },
        complete: function () {

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    });
});

function showSummary(summary_id) {
    var url = $('#base_url').val() + "/nursing/showSummary";
    $.ajax({
        url: url,
        type: "GET",
        data: "summary_id=" + summary_id,
        beforeSend: function () {
            $('#summary_list_modal').modal('hide');
            $('#summary_view_modal').modal('show');
            $('#summary_view_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#summary_view_data').LoadingOverlay("hide");
            $('#summary_view_data').html(data);
        },
        complete: function () {

        }
    });
}

/*********** COMBINED VIEW *******************/
function combinedView() {
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_type = $('input[name="visittype"]:checked').val();
    let sort = $('input[name="sortresult"]:checked').val();

    if (patient_id != '') {
        $('#combined_view_modal').modal('show');
        var url = $('#base_url').val() + "/emr/load-combined-view";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&patient_id=' + patient_id + '&visit_type=' + visit_type + '&sort=' + sort,
            beforeSend: function () {
                $('#combined-view-loader').removeClass('hide');
            },
            success: function (data) {
                if (data.status = 1) {
                    $('.combined_view_wrapper').html(data.html);


                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                } else {
                    Command: toastr["error"]("Error.");
                }

                $('#btn_my-visit').hide();
                $('#all-visit').trigger('click');
            },
            complete: function () {
                $('#combined-view-loader').addClass('hide');
                $('h3').css('font-size','14px');
            }
        });
    }
}


function showIvFluidChart() {
    var visit_id = $("#visit_id").val();
    var url = $('#base_url').val() + "/nursing/showIvFluidData";
    $('#iv_chart_content').html('');
    $.ajax({
        type: "GET",
        url: url,
        data: "visit_id=" + visit_id,
        beforeSend: function () {
            $('#iv_chart_content').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            //console.log(html);
            $('#iv_chart_content').html(html);
            $('.date_floatinput').datetimepicker({
                format: "MMM-DD-YYYY HH:mm A",
            });

            $('.iv_search_rate').on('focus', function () {
                event.preventDefault();
                var obj = $(this);
                var current_id = $(this).attr('id');
                searchIvRateInIvList(current_id, obj, 1);
            });

            $('.iv_search_rate_change').on('focus', function () {
                event.preventDefault();
                var obj = $(this);
                var current_id = $(this).attr('id');
                searchIvRateChange(obj, 1);
            });

        },
        complete: function () {
            $('#iv_chart_content').LoadingOverlay("hide");

            $('.datepicker').datetimepicker({
                format: 'MMMM-DD-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });

        },
    });
}

function searchIvRateChange(obj, all = 0) {
    var search_rate_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var rate_list = $('.iv-rate-list-div_change');
    var iv_rate_last_search_string = '';
    if ((search_rate_string == "" || search_rate_string.length < 2) && all == 0) {
        iv_rate_last_search_string = '';
        return false;
    } else {
        var rate_list = $('.iv-rate-list-div_change');
        $(rate_list).show();
        //clearTimeout(iv_rate_timeout);
        iv_rate_timeout = setTimeout(function () {
            if (search_rate_string == iv_rate_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/rate-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_rate_string: search_rate_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#IvRateTable_change > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var rate_search_list = $('#ListIvRateSearchData_change');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let rate = response[i].iv_rate;

                            res_data += '<tr><td>' + rate + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(rate_search_list).html(res_data);
                    iv_rate_last_search_string = search_rate_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
}

//when select Iv rate
$(document).on('dblclick', '#ListIvRateSearchData_change tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let rate = $(tr).text();

    if (rate != '') {
        $('input[name="iv_search_rate_change"]').val(rate);

        $(".iv-rate-list-div_change").hide();
    }

});

//close Iv rate search
$(document).on('click', '.close_btn_iv_rate_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".iv-rate-list-div_change").hide();
    $(".iv-rate-list-div").hide();
});

function searchIvRateInIvList(current_id, obj, all = 0) {
    var incr_value = current_id.split('_')[0];
    var search_rate_string = $(obj).val();
    var patient_id = $('#patient_id').val();
    var rate_list = $('#' + incr_value + '_iv-rate-list-div');
    var iv_rate_last_search_string = '';
    if ((search_rate_string == "" || search_rate_string.length < 2) && all == 0) {
        iv_rate_last_search_string = '';
        return false;
    } else {
        var rate_list = $('#' + incr_value + '_iv-rate-list-div');
        $(rate_list).show();
        // clearTimeout(iv_rate_timeout);
        iv_rate_timeout = setTimeout(function () {
            if (search_rate_string == iv_rate_last_search_string && all == 0) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/rate-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_rate_string: search_rate_string,
                    patient_id: patient_id,
                    all: all
                },
                beforeSend: function () {
                    $('#' + incr_value + '_IvRateTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    console.log(data);

                    let response = data;
                    let res_data = "";

                    var rate_search_list = $('#' + incr_value + '_ListIvRateSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let rate = response[i].iv_rate;

                            res_data += '<tr><td>' + rate + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(rate_search_list).html(res_data);
                    iv_rate_last_search_string = search_rate_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                    $(document).on('dblclick', '#' + incr_value + '_ListIvRateSearchData tr', function (event) {
                        event.preventDefault();
                        /* Act on the event */

                        let tr = $(this);
                        let rate = $(tr).text();
                        if (rate != '') {
                            $('#' + incr_value + '_iv_search_rate').val(rate);

                            $('#' + incr_value + '_iv-rate-list-div').hide();
                        }

                    });

                }
            });

        }, 200)

    }
}

function toggleChangeVolume(increment, id, iv_chart_id) {
    $('#change_voume_modal').modal('toggle');
    var change_rate = $('#' + increment + '_iv_search_rate').val();
    $('#iv_search_rate_change').val(change_rate);

    var start_at = $('#' + increment + '_stop_at').val();
    $('#start_at_change').val(start_at);
    $('#iv_chart_id_chagne').val(iv_chart_id);
}

function saveChangeVolume() {
    var change_rate = $('#iv_search_rate_change').val();
    change_rate = window.btoa(change_rate);
    var start_at = $('#start_at_change').val();
    // var stop_at = $('#stop_at_change').val();
    var iv_chart_id = $('#iv_chart_id_chagne').val();

    var url = $('#base_url').val() + "/nursing/saveIvChangeVolume";
    $.ajax({
        type: "GET",
        url: url,
        data: "change_rate=" + change_rate + '&start_at=' + start_at + '&iv_chart_id=' + iv_chart_id,

        beforeSend: function () {
            $('#change_volume_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            //console.log(html);
            if (html == 1) {
                $('#change_voume_modal').modal('hide');
            }
            showIvFluidChart();
        },
        complete: function () {
            $('#change_volume_data').LoadingOverlay("hide");

        },
    });
}


/*-------------- Billing Report----------------- */

function billingReport() {
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();


    if (patient_id != '') {
        $('#billing_report_modal').modal('show');
        var url = $('#base_url').val() + "/nursing/load-billing-report";
        $.ajax({
            type: "POST",
            url: url,
            data: '_token=' + _token + '&patient_id=' + patient_id,
            beforeSend: function () {
                $('#billing-report-loader').removeClass('hide');
            },
            success: function (data) {
                if (data.status = 1) {
                    $('.billing_report_wrapper').html(data.html);

                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                } else {
                    Command: toastr["error"]("Error.");
                }
            },
            complete: function () {
                $('#billing-report-loader').addClass('hide');
            }
        });
    }
}

$(document).on('click', '.combined_view_date_list ul li', function () {
    setTimeout(function () {
        $("#combined_view_modal").scrollTop(0);
    }, 50)


});


function createDischargeSummary() {

    window.location.href = $('#base_url').val() + "/summary/dischargesummary";
}


$(document).on('click', 'body', function (event) {
    let dont_show_toastr = true;
    if ($(event.target).parents('#tab1').length == 0 && $('#assesment_draft_mode').val() == 1) {
        saveClinicalTemplate(1, dont_show_toastr);
    }
})

function show_io_report() {
    var visit_id = $("#visit_id").val();
    var url = $('#base_url').val() + "/nursing/showIoReport";
    $('#iv_chart_content').html('');
    $.ajax({
        type: "GET",
        url: url,
        data: "visit_id=" + visit_id,
        beforeSend: function () {
            $('#io_report_modal').modal('toggle');
            $('#io_report_modal_body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#io_report_modal_body').html(html);

        },
        complete: function () {
            $('#io_report_modal_body').LoadingOverlay("hide");

        },
    });
}

function updateSeenstatus(visit_id) {
    var url = $('#base_url').val() + "/emr/updateSeen";
    var visit_id = $(visit_id).val();
    $.ajax({
        url: url,
        type: "GET",
        data: "visit_id=" + visit_id,
        beforeSend: function () {

        },
        success: function (data) {
            //alert(data);return;
        },
        complete: function () {

        }
    });
}

function docImgPreview(docID,storage_type='server'){
    if(docID == 0 || docID == undefined){
      Command: toastr["error"]("Error.!");
      return false;
    }
    var url = $('#base_url').val() + "/emr/preview-document";
    var id = docID;
    $.ajax({
        type: "GET",
        url: url,
        data: "id=" +id+'&storage_type='+storage_type,
        beforeSend: function () {
            if(storage_type != 'local'){
                $('#view_document_modal').modal('show');
                $('#document_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            }
        },
        success: function (response) {
            if(storage_type == 'local'){
                window.open(response, '_blank');
            }else{
                $('#myframe').attr("src",response);
            }
        },
        complete: function () {
            if(storage_type != 'local'){
                $('#document_data').LoadingOverlay("hide");
            }
        }
    });
}
