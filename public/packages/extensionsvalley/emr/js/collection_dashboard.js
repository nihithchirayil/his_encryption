$(document).ready(function (){
    getCustomDates(1)
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});

function getCustomDates(from_type) {
    var date_data = $('#date_datahidden').val();
    var obj = JSON.parse(date_data);
    $('#daterange_hidden').val(from_type);
    if (from_type == '1') {
        $('#from_date').val(obj.current_date);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.current_date);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Today');
        getAllGraphs();
    } else if (from_type == '2') {
        $('#from_date').val(obj.yeasterday);
        $('#to_date').val(obj.yeasterday);
        $('#from_datadis').html(obj.yeasterday);
        $('#to_datadis').html(obj.yeasterday);
        $('#range_typedata').html('Yesterday');
        getAllGraphs();
    } else if (from_type == '3') {
        $('#from_date').val(obj.week_last_sunday);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.week_last_sunday);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Week Till Date');
        getAllGraphs();
    } else if (from_type == '4') {
        $('#from_date').val(obj.lastweek_from);
        $('#to_date').val(obj.lastweek_to);
        $('#from_datadis').html(obj.lastweek_from);
        $('#to_datadis').html(obj.lastweek_to);
        $('#range_typedata').html('Last Week');
        getAllGraphs();
    } else if (from_type == '5') {
        $('#from_date').val(obj.firstday_thismonth);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.firstday_thismonth);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Month Till Date');
        getAllGraphs();
    } else if (from_type == '6') {
        $('#from_date').val(obj.firstday_lastmonth);
        $('#to_date').val(obj.lastday_lastmonth);
        $('#from_datadis').html(obj.firstday_lastmonth);
        $('#to_datadis').html(obj.lastday_lastmonth);
        $('#range_typedata').html('Last Month');
        getAllGraphs();
    } else if (from_type == '7') {
        $("#customdatapopmodel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

}

function getAllGraphs() {
   // $('.colum_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
    setTimeout(function () {
        getAllTotalCollection(1);
    }, 1000);
    setTimeout(function () {
        getAllTotalCollection(2);
    }, 2000);
    setTimeout(function () {
        getAllTotalCollection(3);
    }, 3000);
    setTimeout(function () {
        getAllTotalCollection(4);
    }, 4000);
    setTimeout(function () {
        getAllTotalCollection(5);
    }, 5000);
    setTimeout(function () {
        getAllTotalCollection(6);
    }, 6000);
    setTimeout(function () {
        getAllTotalCollection(7);
    }, 7000);
    setTimeout(function () {
        getAllTotalCollection(8);
    }, 8000);
    setTimeout(function () {
        getAllTotalCollection(9);
    }, 9000);
    setTimeout(function () {
        getAllTotalCollection(10);
    }, 10000);
    setTimeout(function () {
        getAllTotalCollection(11);
    }, 11000);
    setTimeout(function () {
        getAllTotalCollection(12);
    }, 12000);
    setTimeout(function () {
        getAllTotalCollection(13);
    }, 13000);
    setTimeout(function () {
        getAllTotalCollection(14);
    }, 14000);


}

function getAllTotalCollection(from_type){
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var date_range = $('#daterange_hidden').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/collection_dashboard/getAllTotalCollection";
    var token = $('#c_token').val();
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type, date_range: date_range };
    $.ajax({
        type: "POST",
        url: url,
       // async: false,
        data: param,
        beforeSend: function () {
            $('#list_data' + from_type).removeClass('fa fa-list');
            $('#list_data' + from_type).addClass('fa fa-spinner fa-spin');
            $('#total_collection' + from_type).html("Please Wait!!!");
            //$('.colum_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');


            if(from_type == 1){
                $('#total_collection_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 2){
                $('#consultation_total_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 3){
                $('#pharmacy_total_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 4){
                $('#lab_total_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 5){
                $('#radiology_total_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 6){
                $('#lab_data_container').html('<h2 style="text-align:center;"><i style="color:blue;" class="fa fa-spinner fa-spin"></i></h2>');
            }else if(from_type == 7){
                $('#pharmacy_consumable_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#pharmacy_non_consumable_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 8){
                $('#service_total_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 9){
                $('#rent_total_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 10){
                $('#xray_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#usg_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#ct_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 11){
                $('#op_pharmacy_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#ip_pharmacy_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#gp_pharmacy_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 12){
                $('#op_lab_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#ip_lab_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#gp_lab_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 13){
                $('#op_service_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#ip_service_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#gp_service_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }else if(from_type == 14){
                $('#mlc_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
                $('#decc_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            }
            // else if(from_type == 15){
            //     $('#abg_val').html('<i style="color:white;" class="fa fa-spinner fa-spin"></i>');
            // }

        },
        success: function (data) {
                if(from_type !=6){
                    var obj = JSON.parse(data);
                }


            //---total collection------
            if(from_type == 1){
                $('#total_collection_val').html('<i class="fa fa-inr"></i> '+obj.total_collection);
            }else if(from_type == 2){
                $('#consultation_total_val').html('<i class="fa fa-inr"></i> '+obj.consultation_total);
            }else if(from_type == 3){
                $('#pharmacy_total_val').html('<i class="fa fa-inr"></i> '+obj.pharmacy_total);
            }else if(from_type == 4){
                $('#lab_total_val').html('<i class="fa fa-inr"></i> '+obj.lab_total);
            }else if(from_type == 5){
                $('#radiology_total_val').html('<i class="fa fa-inr"></i> '+obj.radiology_total);
            }else if(from_type == 6){
                console.log(data);
                $('#lab_data_container').html(data);
            }else if(from_type == 7){
                $('#pharmacy_consumable_val').html('<i class="fa fa-inr"></i> '+obj['CONSUMABLE']);
                $('#pharmacy_non_consumable_val').html('<i class="fa fa-inr"></i> '+obj['NON_CONSUMABLE']);
            }else if(from_type == 8){
                $('#service_total_val').html('<i class="fa fa-inr"></i> '+obj.services_total);
            }else if(from_type == 9){
                $('#rent_total_val').html('<i class="fa fa-inr"></i> '+obj.rent_total);
            }else if(from_type == 10){
                $('#xray_val').html('<i class="fa fa-inr"></i> '+obj['XRAY']);
                $('#usg_val').html('<i class="fa fa-inr"></i> '+obj['USG']);
                $('#ct_val').html('<i class="fa fa-inr"></i> '+obj['CT_MRI']);
            }else if(from_type == 11){
                $('#op_pharmacy_val').html('<i class="fa fa-inr"></i> '+obj['OP']);
                $('#ip_pharmacy_val').html('<i class="fa fa-inr"></i> '+obj['IP']);
                $('#gp_pharmacy_val').html('<i class="fa fa-inr"></i> '+obj['GP']);
            }else if(from_type == 12){
                $('#op_lab_val').html('<i class="fa fa-inr"></i> '+obj['OP']);
                $('#ip_lab_val').html('<i class="fa fa-inr"></i> '+obj['IP']);
                $('#gp_lab_val').html('<i class="fa fa-inr"></i> '+obj['GP']);
            }else if(from_type == 13){
                $('#op_service_val').html('<i class="fa fa-inr"></i> '+obj['OP']);
                $('#ip_service_val').html('<i class="fa fa-inr"></i> '+obj['IP']);
                $('#gp_service_val').html('<i class="fa fa-inr"></i> '+obj['GP']);
            }else if(from_type == 14){
                $('#mlc_val').html('<i class="fa fa-inr"></i> '+obj['MLC']);
                $('#decc_val').html('<i class="fa fa-inr"></i> '+obj['DECC']);
            }else if(from_type == 15){
                $('#abg_val').html('<i class="fa fa-inr"></i> '+obj['ABG']);
            }

        },
        complete: function () {
            $('#list_data' + from_type).removeClass('fa fa-spinner fa-spin');
            $('#list_data' + from_type).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getCustomDateRange() {
    var from_date = $('#from_date').val();
    var todate = $('#to_date').val();
    $('#range_typedata').html('Custom Date Range Selection');
    $('#from_datadis').html(from_date);
    $('#to_datadis').html(todate);
    getAllGraphs();
    $("#customdatapopmodel").modal('toggle');
}



