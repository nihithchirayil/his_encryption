$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    setTimeout(function () {
        $(".select2").select2();
        searchQuotation();
        $('.ajaxSearchBox').hide();

    }, 800);

});
var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();


$('#po_number').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('po_idAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var po_no = $(this).val();
        if (!po_no) {
            $("#po_idAjaxDiv").html("").hide();
            $('#po_id_hidden').val('');
        } else {
            var url = base_url + "/purchase/po_number_search";
            var param = {
                _token: token,
                po_no: po_no
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#po_idAjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#po_idAjaxDiv").html(html).show();
                    $("#po_idAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('po_idAjaxDiv', event);
    }
});


$('#supplier_number').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('supplier_idAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $(this).val();
        if (!vendor_name) {
            $("#supplier_idAjaxDiv").html("").hide();
            $('#supplier_id_hidden').val('');
        } else {
            var url = base_url + "/purchase/vendor_search";
            var param = {
                vendor_name: vendor_name
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#supplier_idAjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#supplier_idAjaxDiv").html(html).show();
                    $("#supplier_idAjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('supplier_idAjaxDiv', event);
    }
});



//----------- Item Detail search------------
$('#item_desc').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('item_desc_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#item_desc_hidden').val() != "") {
            $('#item_desc_hidden').val('');
            $("#item_desc_AjaxDiv").hide();
        }
        var item_desc = $(this).val();
        if (item_desc == "") {
            $("#item_desc_AjaxDiv").html("").hide();
        } else {
            var url = base_url + "/grn/ProductNameSearch";

            $.ajax({
                type: "POST",
                url: url,
                data: "item_desc=" + item_desc,
                beforeSend: function () {
                    $("#item_desc_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#item_desc_AjaxDiv").html(html).show();
                    $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('item_desc_AjaxDiv', event);
    }
});


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function removeAllWhiteSpace(s) {
    s = s.replace(/(\d)[\s.]+(?=\d)/g, '$1');
    s = s.replace(/(?<=\d)[\s.]+(?=\d)/g, '');
    return s;
}

function fillitem_desc(item_code, item_desc) {
    item_desc = htmlDecode(item_desc);
    $('#item_desc_hidden').val(item_code);
    $('#item_desc').val(item_desc);
    $('#item_desc_AjaxDiv').hide();
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#supplier_number').val(vendor_name);
    $('#supplier_id_hidden').val(vendor_code);
    $('#supplier_idAjaxDiv').hide();
    searchQuotation();
}

function fillPoNumber(id, po_number) {
    po_number = htmlDecode(po_number);
    $('#po_number').val(po_number);
    $('#po_id_hidden').val(id);
    $('#po_idAjaxDiv').hide();
    searchQuotation();
}

function searchQuotation() {
    var url = base_url + "/purchase/searchPOList";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();
    var item_code = $('#item_desc_hidden').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            vendor: vendor,
            location: location,
            po_dated: po_dated,
            from_date: from_date,
            to_date: to_date,
            item_code: item_code,
            po_status: po_status
        },
        beforeSend: function () {
            $('#searchQuotationBtn').attr('disabled', true);
            $('#searchQuotationSpin').removeClass('fa fa-search');
            $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchQuotationBtn').attr('disabled', false);
            $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchQuotationSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function addWindowLoad(to_url, po_id, copy) {
    var url = base_url + "/purchase/" + to_url + '/' + po_id + '/' + copy;
    document.location.href = url;
}




function searchQuotation() {
    var url = base_url + "/purchase/searchPOList";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();
    var item_desc = $('#item_desc_hidden').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            vendor: vendor,
            location: location,
            po_dated: po_dated,
            from_date: from_date,
            to_date: to_date,
            item_desc: item_desc,
            po_status: po_status
        },
        beforeSend: function () {
            $('#searchQuotationBtn').attr('disabled', true);
            $('#searchQuotationSpin').removeClass('fa fa-search');
            $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchQuotationBtn').attr('disabled', false);
            $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchQuotationSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function downloadExcelPrintPreview(po_id) {
    var url = base_url + "/purchase/getPurchaseDetailPrint";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            po_id: po_id,
        },
        beforeSend: function () {
            $('#downloadExcelPrintPreviewBtn' + po_id).attr('disabled', true);
            $('#downloadExcelPrintPreviewSpin' + po_id).removeClass('fa fa-file-excel-o');
            $('#downloadExcelPrintPreviewSpin' + po_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#getPrintPreviewModelDiv').html(obj.html);
            $("#getPrintPreviewModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            $('#downloadExcelPrintPreviewBtn' + po_id).attr('disabled', false);
            $('#downloadExcelPrintPreviewSpin' + po_id).removeClass('fa fa-spinner fa-spin');
            $('#downloadExcelPrintPreviewSpin' + po_id).addClass('fa fa-file-excel-o');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}




function downloadPoListExcel() {
    var url = base_url + "/purchase/downloadPoListExcel";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();
    var item_desc = $('#item_desc_hidden').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            vendor: vendor,
            location: location,
            po_dated: po_dated,
            from_date: from_date,
            to_date: to_date,
            po_status: po_status,
            item_desc: item_desc
        },
        beforeSend: function () {
            $('#downloadPoListExcelBtn').attr('disabled', true);
            $('#downloadPoListExcelSpin').removeClass('fa fa-file-excel-o');
            $('#downloadPoListExcelSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            if (html.trim() != 'NODATA') {
                html = removeAllWhiteSpace(html)
                var download_file = base_url + '/packages/uploads/Purchase/' + html;
                $('#downloadfile').attr('href', download_file);
                $('#downloadfile')[0].click();
            } else {
                toastr.warning("No records found");
            }
        },
        complete: function () {
            $('#downloadPoListExcelBtn').attr('disabled', false);
            $('#downloadPoListExcelSpin').removeClass('fa fa-spinner fa-spin');
            $('#downloadPoListExcelSpin').addClass('fa fa-file-excel-o');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}



function downloadDetailPoListExcel() {
    var url = base_url + "/purchase/downloadDetailPoListExcel";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();
    var item_desc = $('#item_desc_hidden').val();
    if (from_date && to_date) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                po_id: po_id,
                vendor: vendor,
                location: location,
                po_dated: po_dated,
                from_date: from_date,
                to_date: to_date,
                po_status: po_status,
                item_desc: item_desc
            },
            beforeSend: function () {
                $('#downloadDetailPoListExcelBtn').attr('disabled', true);
                $('#downloadDetailPoListExcelSpin').removeClass('fa fa-table');
                $('#downloadDetailPoListExcelSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                if (html.trim() != 'NODATA') {
                    html = removeAllWhiteSpace(html)
                    var download_file = base_url + '/packages/uploads/Purchase/' + html;
                    $('#downloadfile').attr('href', download_file);
                    $('#downloadfile')[0].click();
                } else {
                    toastr.warning("No records found");
                }
            },
            complete: function () {
                $('#downloadDetailPoListExcelBtn').attr('disabled', false);
                $('#downloadDetailPoListExcelSpin').removeClass('fa fa-spinner fa-spin');
                $('#downloadDetailPoListExcelSpin').addClass('fa fa-table');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        toastr.warning("Please select from date and to date");
    }
}



function downloadPoListExcel() {
    var url = base_url + "/purchase/downloadPoListExcel";
    var po_id = $('#po_id_hidden').val();
    var vendor = $('#supplier_id_hidden').val();
    var location = $('#deprt_list').val();
    var po_dated = $('#po_dated').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var po_status = $('#po_status').val();
    var item_desc = $('#item_desc_hidden').val();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            po_id: po_id,
            vendor: vendor,
            location: location,
            po_dated: po_dated,
            from_date: from_date,
            to_date: to_date,
            po_status: po_status,
            item_desc: item_desc
        },
        beforeSend: function () {
            $('#downloadPoListExcelBtn').attr('disabled', true);
            $('#downloadPoListExcelSpin').removeClass('fa fa-file-excel-o');
            $('#downloadPoListExcelSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            if (html.trim() != 'NODATA') {
                html = removeAllWhiteSpace(html)
                var download_file = base_url + '/packages/uploads/Purchase/' + html;
                $('#downloadfile').attr('href', download_file);
                $('#downloadfile')[0].click();
            } else {
                toastr.warning("No records found");
            }
        },
        complete: function () {
            $('#downloadPoListExcelBtn').attr('disabled', false);
            $('#downloadPoListExcelSpin').removeClass('fa fa-spinner fa-spin');
            $('#downloadPoListExcelSpin').addClass('fa fa-file-excel-o');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function copyPOData(po_id) {
    var url = base_url + "/purchase/checkCopyPOData";
    var param = {
        po_id: po_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#copyPODataBtn' + po_id).attr('disabled', true);
            $('#copyPODataSpin' + po_id).removeClass('fa fa-copy');
            $('#copyPODataSpin' + po_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (parseInt(data) == 1) {
                addWindowLoad('quotationPurchaseOrder', po_id, 1);
            } else {
                toastr.warning("Please wait for one minute.")
            }
        },
        complete: function () {
            $('#copyPODataBtn' + po_id).attr('disabled', false);
            $('#copyPODataSpin' + po_id).removeClass('fa fa-spinner fa-spin');
            $('#copyPODataSpin' + po_id).addClass('fa fa-copy');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}
