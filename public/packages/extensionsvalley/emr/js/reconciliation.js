var token = $("#c_token").val();
var base_url = $("#base_url").val();
$(document).ready(function() {
    $(".datepicker").datetimepicker({ format: 'MMM-DD-YYYY' });
    var today = moment().format('MMM-DD-YYYY');
    $("#from_date").val(today);
    $("#to_date").val(today);

});

$(document).on("click", "#searchBtn", function(){
    fetchLedgerBookingEntries(1);
})

function fetchLedgerBookingEntries(check_head_exist){
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var ledger_bank_id = $("#ledger_bank_id").val();
    var voucher_type = $("#voucher_type").val();
    var url = base_url + "/accounts/fetchLedgerBookingEntries";
    var param = { 
        _token: token,
        from_date: from_date,
        to_date: to_date,
        ledger_bank_id:ledger_bank_id,
        voucher_type:voucher_type,
        check_head_exist:check_head_exist.toString()
    };

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });

            $('#searchBtn').attr('disabled', true);
            $('#searchSpin').removeClass('fa fa-search');
            $('#searchSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if(data.status == 1){
                $('#searchDataDiv').html(data.html);
                fetchReconcilitationBankStmt();
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            } else if(data.status == 2){
                $(".select_ledger_head_entry_table_body").empty();
                $(".select_ledger_head_entry_table_body").append("<tr class='particular_item' data-from-date='"+data.from_date+"' data-to-date='"+data.to_date+"'><td>"+data.from_date+"</td><td>"+data.to_date+"</td><td>"+data.created_by+"</td></tr>");
                $("#select_ledger_head_entry").modal({backdrop: 'static', keyboard: false});
            }
            
        },
        complete: function () {
            $('#searchBtn').attr('disabled', false);
            $('#searchSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchSpin').addClass('fa fa-search');
            $("#searchDataDiv").LoadingOverlay("hide");

        },
    });
}

$(document).on("click", ".select_ledger_head_entry_table_body tr", function(){
    var from_date = $(this).attr('data-from-date');
    var to_date = $(this).attr('data-to-date');
    from_date = moment(from_date).format('MMM-DD-YYYY');
    to_date = moment(to_date).format('MMM-DD-YYYY');

    $("#from_date").val(from_date);
    $("#to_date").val(to_date);
    $("#select_ledger_head_entry").modal('hide');
    fetchLedgerBookingEntries(0);

})

$("#uploadBankStatement").on("submit", function (e) {
    e.preventDefault();
    var image_vai = validateDoc("uploadExcelID");
    var ledger_bank_id = $("#ledger_bank_id").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    if (image_vai) {
        if(ledger_bank_id) {
            if(from_date && to_date) {
                var url = base_url + "/accounts/uploadBankStatement";
                var formdata = new FormData(this);
                formdata.append('ledger_bank_id', ledger_bank_id);
                formdata.append('from_date', from_date);
                formdata.append('to_date', to_date);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formdata,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $("#uploadExcelButton").attr("disabled", true);
                        $("#uploadExcelButtonSpin").removeClass("fa fa-upload");
                        $("#uploadExcelButtonSpin").addClass("fa fa-spinner fa-spin");
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            fetchReconcilitationBankStmt();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $("#uploadExcelButton").attr("disabled", false);
                        $("#uploadExcelButtonSpin").removeClass("fa fa-spinner fa-spin");
                        $("#uploadExcelButtonSpin").addClass("fa fa-upload");
                    },
                    error: function () {
                        toastr.error(
                            "Error Please Check Your Internet Connection and Try Again"
                        );
                    },
                });
            } else {
                bootbox.alert("Please select a date.");
            }
        } else {
            bootbox.alert("Please select bank");
        }
    } else {
        bootbox.alert(
            "<strong>File criteria.</strong><br>Extensions: .xlsx <br>Size : Maximum 5 MB"
        );
    }
});


function validateDoc(input_id) {
    var fileInput = document.getElementById(input_id);
    if (fileInput) {
        var filePath = fileInput.value;
        if (filePath) {
            var allowedExtensions = /(\.(xlsx|xls))$/i;
            var file_size = fileInput.size;
            if (!allowedExtensions.exec(filePath) || file_size > 5150) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function fetchReconcilitationBankStmt(){
    var ledger_bank_id = $("#ledger_bank_id").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/accounts/fetchReconcilitationBankStmt";
    var param = {
        _token: token,
        ledger_bank_id:ledger_bank_id,
        from_date:from_date,
        to_date:to_date,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#excelUploadData").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#excelUploadData').html(data);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
        complete: function () {
            $("#excelUploadData").LoadingOverlay("hide");

        },
    });
}

$(document).on('click', '.ledger_items', function(){


    var voucher_type = $(this).attr('data-voucher-type');
    var voucher_no = $(this).attr('data-voucher-no');
    if($(this).hasClass('activeItem')){
        $("#searchDataDiv").find('tr[data-voucher-no="'+voucher_no+'"]').removeClass('activeItem');
    } else {
        $(".ledger_items").removeClass('activeItem');
        $("#searchDataDiv").find('tr[data-voucher-no="'+voucher_no+'"]').addClass('activeItem');
    }

    var total_amount = 0.00;
    $("#searchDataDiv").find('tr.activeItem').each(function(key, val){
        if(voucher_type == "Receipt"){
            if($(val).attr('data-cr-dr') == 'cr') {
                total_amount = total_amount + parseFloat($(val).attr("data-amount"));
            } else if($(val).attr('data-cr-dr') == 'dr') {
                total_amount = total_amount - parseFloat($(val).attr("data-amount"));
            }
        } else if(voucher_type == "Payment"){
            if($(val).attr('data-cr-dr') == 'cr') {
                total_amount = total_amount - parseFloat($(val).attr("data-amount"));
            } else if($(val).attr('data-cr-dr') == 'dr') {
                total_amount = total_amount + parseFloat($(val).attr("data-amount"));
            }
        } else {
            total_amount = total_amount + parseFloat($(val).attr("data-amount"));
        }
    });
    total_amount = total_amount.toFixed(2);
    $(".total_ledger_amount").val(total_amount);


    if($("#excelUploadData").find('tr.activeItem').length == 0){
        

        if($(".show_all_statement_items").prop("checked")){
            $(".show_all_statement_items").prop("checked", false);
        }
        
        
        if($("#excelUploadData").find('tr[data-amount="'+total_amount+'"]').length > 0){
            $('.stmt_items').hide();
            $("#excelUploadData").find('tr[data-amount="'+total_amount+'"]').show();
            
            $("#searchDataDiv").find('tr.activeItem').each(function(key, val){
                var ledger_name = $(val).find('.ledger_name').html();
    
                (function(string1){
                    $("#excelUploadData").find('tr[data-amount="'+total_amount+'"]').each(function(key, val){
                        var particular_name = $(val).find('.particular_name').html();
                        var match_percentage = checkMatchPercent(string1, particular_name);
                        if($(val).attr('data-match-percentage')){
                            var old_per = $(val).attr('data-match-percentage');
                            if(parseFloat(old_per) < match_percentage){
                                $(val).attr('data-match-percentage', match_percentage);
                            }
                        } else {
                            $(val).attr('data-match-percentage', match_percentage);
                        }
                    });
                })(ledger_name)
            });
    
            sortTable("bank_stmt_detais_table");
        } else {
            if($(".reconceiled_class").css("display") != "none"){
                $('.stmt_items').show();
            } else {
                $('.stmt_items').show();
                $(".reconceiled_class").hide();
                $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
            }

            showNoStatementsMessage();
        }
    }

    calculateBalanceAmount();
    
})

$(document).on('click', '.stmt_items', function(){
    if($("#searchDataDiv").find('tr.activeItem').length > 1){
        $(".stmt_items").removeClass('activeItem');
        $(this).addClass('activeItem');
    } else {
        if($(this).hasClass('activeItem')){
            $(this).removeClass('activeItem');
        } else {
            $(this).addClass('activeItem');
        }
    }

    var total_amount = 0;
    $("#excelUploadData").find('tr.activeItem').each(function(key, val){
        total_amount = total_amount + parseFloat($(val).attr('data-amount'));
    });
    total_amount = total_amount.toFixed(2);
    $(".total_statement_amount").val(total_amount);
    calculateBalanceAmount();
    
})

$(document).on("click", '.show_all_statement_items', function(){
    if($(this).prop("checked")){
        $('.stmt_items').show();
    } else {
        if($("#searchDataDiv").find('tr.activeItem').length > 1){
            $($("#searchDataDiv").find('tr.activeItem')[0]).click();
        }
    }
});

function showMatchedMessage(){
    $(".match_found_div").show();
    setTimeout(function(){
        $(".match_found_div").hide();
    }, 2000);
}

function showNoStatementsMessage(){
    $(".no_stmt_found_div").show();
    setTimeout(function(){
        $(".no_stmt_found_div").hide();
    }, 2000);
}

function calculateBalanceAmount(){
    var total_ledger_amount = $(".total_ledger_amount").val()
    var total_statement_amount = $(".total_statement_amount").val();

    var balance_amount = total_ledger_amount - total_statement_amount;
    $(".balance_amount").val(balance_amount);

    if(balance_amount == 0 && parseInt(total_ledger_amount) > 0){
        showMatchedMessage();
    }
}

$(document).on("click", "#show_hide_recon_entries_btn", function(){
    if($(".reconceiled_class").css("display") == "none"){
        $(".reconceiled_class").show();
        $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
    } else {
        $(".reconceiled_class").hide();
        $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }
    
});

$(document).on("change", ".filter_stmt_items", function(){
    var value= $(".filter_stmt_items").val();
    if(value == ""){
        $(".stmt_items").show();
    } else if(value == "cr"){
        $(".cr_class").show();
        $(".dr_class").hide();
    } else if(value == "dr"){
        $(".dr_class").show();
        $(".cr_class").hide();
    }
})

$(document).on("click", ".show_reconciliation_summary_btn", function(){
    var ledger_bank_id = $("#ledger_bank_id").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();

    if(ledger_bank_id &&  from_date && to_date){
        var url = base_url + "/accounts/fetchReconciliationSummary";
        var param = {
            _token: token,
            ledger_bank_id:ledger_bank_id,
            from_date:from_date,
            to_date:to_date,
        };

        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $(".show_reconciliation_summary_btn").attr('disabled', true);
                $(".show_reconciliation_summary_btn").find("i").removeClass('fa-list').addClass("fa-spinner fa-spin");
            },
            success: function (data) {
                $(".show_summary_modal_body").html(data.html);
                $("#show_summary_modal").modal('show');

                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 15
                });
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                $(".show_reconciliation_summary_btn").attr('disabled', false);
                $(".show_reconciliation_summary_btn").find("i").removeClass("fa-spinner fa-spin").addClass('fa-list');
            },
        });
    } else {
        toastr.error("Invalid parameters.")
    }
    
})

$(document).on("click", ".auto_reconcile_btn", function(){
    var confirm = window.confirm("Are you sure want to auto reconcile ?");
    if(confirm){
        var ledger_bank_id = $("#ledger_bank_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        if(!ledger_bank_id){
            toastr.error('please select any ledger');
            return;
        }
        if(!from_date){
            toastr.error('please select from date');
            return;
        }
        if(!to_date){
            toastr.error('please select to date');
            return;
        }

        var url = base_url + "/accounts/autoReconciliation";
        var param = {
            _token: token,
            ledger_bank_id:ledger_bank_id,
            from_date:from_date,
            to_date:to_date,
        };

        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $(".auto_reconcile_btn").attr('disabled', true);
                $(".auto_reconcile_btn").find("i").removeClass('fa-cogs').addClass("fa-spinner fa-spin");
            },
            success: function (data) {
                if(data.status == 1){
                    toastr.success('Auto Reconcilitation Completed!');
                    $("#searchBtn").click();
                } else {
                    toastr.error(data.message);
                }
            },
            complete: function () {
                $(".auto_reconcile_btn").attr('disabled', false);
                $(".auto_reconcile_btn").find("i").removeClass("fa-spinner fa-spin").addClass('fa-cogs');
            },
        });
    }
});

$(document).on("click", ".mark_as_match_btn", function(){
    var balance_amount = $(".balance_amount").val();
    balance_amount = parseFloat(balance_amount).toFixed(2);
    if(balance_amount != 0.00){
        toastr.error('Invalid match.');
        return;
    }
    if($("#leger_details_table").find('tr.activeItem').length == 0){
        toastr.error('Please select any ledger entry.');
        return;
    }
    if($("#bank_stmt_detais_table").find('tr.activeItem').length == 0){
        toastr.error('Please select any statement.');
        return;
    }
    var confirm = window.confirm("Are you sure want to save ?");
    if(confirm){

        var ledger_ids = [];
        $("#searchDataDiv").find('tr.activeItem').each(function(key, val){
            var ledger_id = $(val).attr('data-ledger-id');
            ledger_ids.push(ledger_id);
        });
        var statement_ids = [];
        $("#excelUploadData").find('tr.activeItem').each(function(key, val){
            var statement = $(val).attr('data-stmt-id');
            statement_ids.push(statement);
        });

        var ledger_bank_id = $("#ledger_bank_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        var url = base_url + "/accounts/saveReconciliation";
        var param = {
            _token: token,
            ledger_bank_id:ledger_bank_id,
            from_date:from_date,
            to_date:to_date,
            ledger_ids:ledger_ids,
            statement_ids:statement_ids,
            match_status: 1,
            missmatch_reason:''
        };

        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $(".mark_as_match_btn").attr('disabled', true);
                $(".mark_as_match_btn").find("i").removeClass('fa-thumbs-up').addClass("fa-spinner fa-spin");
            },
            success: function (data) {
                toastr.success('success');
                $(".ledger_items.activeItem").hide();
                $(".stmt_items.activeItem").hide();
                $(".ledger_items.activeItem").removeClass('activeItem');
                $(".stmt_items.activeItem").removeClass('activeItem');
                if($(".reconceiled_class").css("display") == "none"){
                    $(".reconceiled_class").show();
                    $("#show_hide_recon_entries_btn").find('i').removeClass('fa-eye').addClass('fa-eye-slash');
                } else {
                    $(".reconceiled_class").hide();
                    $("#show_hide_recon_entries_btn").find('i').removeClass('fa-eye-slash').addClass('fa-eye');
                }
                fetchReconcilitationBankStmt();

            },
            complete: function () {
                $(".mark_as_match_btn").attr('disabled', false);
                $(".mark_as_match_btn").find("i").removeClass("fa-spinner fa-spin").addClass('fa-thumbs-up');
            },
        });
    }
});


$(document).on("click", ".mark_as_missmatch_btn", function(){
    if($("#leger_details_table").find('tr.activeItem').length == 0 && $("#bank_stmt_detais_table").find('tr.activeItem').length == 0){
        toastr.error('Please select any ledger entry or statement');
        return;
    }
    var prompt = window.prompt("Please enter the reason/remarks.");
    if(prompt){

        var ledger_ids = [];
        $("#searchDataDiv").find('tr.activeItem').each(function(key, val){
            var ledger_id = $(val).attr('data-ledger-id');
            ledger_ids.push(ledger_id);
        });
        var statement_ids = [];
        $("#excelUploadData").find('tr.activeItem').each(function(key, val){
            var statement = $(val).attr('data-stmt-id');
            statement_ids.push(statement);
        });

        var ledger_bank_id = $("#ledger_bank_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        var url = base_url + "/accounts/saveReconciliation";
        var param = {
            _token: token,
            ledger_bank_id:ledger_bank_id,
            from_date:from_date,
            to_date:to_date,
            ledger_ids:ledger_ids,
            statement_ids:statement_ids,
            match_status: 2,
            missmatch_reason:prompt
        };

        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $(".mark_as_missmatch_btn").attr('disabled', true);
                $(".mark_as_missmatch_btn").find("i").removeClass('fa-thumbs-down').addClass("fa-spinner fa-spin");
            },
            success: function (data) {
                toastr.success('success');
                $(".ledger_items.activeItem").hide();
                $(".stmt_items.activeItem").hide();
                $(".ledger_items.activeItem").removeClass('activeItem');
                $(".stmt_items.activeItem").removeClass('activeItem');
                if($(".reconceiled_class").css("display") == "none"){
                    $(".reconceiled_class").show();
                    $("#show_hide_recon_entries_btn").find('i').removeClass('fa-eye').addClass('fa-eye-slash');
                } else {
                    $(".reconceiled_class").hide();
                    $("#show_hide_recon_entries_btn").find('i').removeClass('fa-eye-slash').addClass('fa-eye');
                }
                fetchReconcilitationBankStmt();
            },
            complete: function () {
                $(".mark_as_missmatch_btn").attr('disabled', false);
                $(".mark_as_missmatch_btn").find("i").removeClass("fa-spinner fa-spin").addClass('fa-thumbs-down');
            },
        });
    }
});

function checkMatchPercent(s1, s2){
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();
    var words1 = s1.split(" ");
    var words2 = s2.split(" ");

    var diffs1 = words2.filter(x => !words1.includes(x));
    var diffs2 = words1.filter(x => !words2.includes(x));

    var diffsLength = (diffs1.toString()+diffs2.toString()).length;
    var wordsLength = (words1.toString()+words2.toString()).length;

    if(!wordsLength) return 0;

    var differenceRate = ( diffsLength / wordsLength );
    var similarityRate = 1 - differenceRate;
    return similarityRate*100;
}

function sortTable(table) {
    var rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    switching = true;
    // Set the sorting direction to ascending:
    dir = "desc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = $("#" + table + " tbody").find('tr');
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = $(rows[i]).attr('data-match-percentage') ? $(rows[i]).attr('data-match-percentage') : 0;
        y = $(rows[i + 1]).attr('data-match-percentage') ? $(rows[i + 1]).attr('data-match-percentage') : 0;
        /* Check if the two rows should switch place,
        based on the direction, asc or desc: */
        if (dir == "asc") {
          if (parseFloat(x) > parseFloat(y)) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
            if (parseFloat(x) < parseFloat(y)) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        // Each time a switch is done, increase this count by 1:
        switchcount ++;
      } else {
        /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }