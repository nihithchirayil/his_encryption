function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    return number.toFixed(parseInt(decimalConfiguration));
}

function validateText(row_id) {
    $(".active_class" + row_id).attr('disabled', false);
    $(".active_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        } else if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        } else if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        } else if (!val) {
            $(this).val(0);
            flag = 1;
        }
        if ($(this).hasClass("percentage_class" + row_id)) {
            if (parseFloat(val) > 100) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            }
        }
        if (flag == 0) {
            // val = parseFloat(val).toFixed(2);
            $(this).val(checkIsNaN(val));
        }
    });

    $(".number_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        }
        if (!val) {
            $(this).val(0);
            flag = 1;
        }
    });
}



function calculatePopupData(row_id) {

    detailtax_array = [];
    detailtax_array['cgst_per'] = 0.0;
    detailtax_array['cgst_amt'] = 0.00;
    detailtax_array['sgst_per'] = 0.0;
    detailtax_array['sgst_amt'] = 0.00;
    detailtax_array['igst_per'] = 0.0;
    detailtax_array['igst_amt'] = 0.00;
    detailtax_array['discount_per'] = 0.00;
    detailtax_array['discount_amt'] = 0.00;
    detailtax_array['special_discount_per'] = 0.00;
    detailtax_array['special_discount_amt'] = 0.00;
    detailtax_array['Freight_Charges_per'] = 0.0;
    detailtax_array['flightcharge_amt'] = 0.00;
    detailtax_array['other_charges_per'] = 0.0;
    detailtax_array['othercharge_amt'] = 0.00;
    detailtax_array['discounttype1'] = 1;
    detailtax_array['discounttype2'] = 1;

    if ($('#charge_percentage32' + row_id).val()) {
        detailtax_array['special_discount_per'] = $('#charge_percentage32' + row_id).val();
        detailtax_array['special_discount_amt'] = $('#charge_amount32' + row_id).val();
        detailtax_array['discounttype2'] = $('#popdiscount_type32' + row_id).val();
    }
    if ($('#charge_percentage1' + row_id).val()) {
        detailtax_array['discount_per'] = $('#charge_percentage1' + row_id).val();
        detailtax_array['discount_amt'] = $('#charge_amount1' + row_id).val();
        detailtax_array['discounttype1'] = $('#popdiscount_type1' + row_id).val();
    }
    if ($('#charge_percentage9' + row_id).val()) {
        detailtax_array['sgst_per'] = $('#charge_percentage9' + row_id).val();
        detailtax_array['sgst_amt'] = $('#charge_amount9' + row_id).val();
    }
    if ($('#charge_percentage10' + row_id).val()) {
        detailtax_array['cgst_per'] = $('#charge_percentage10' + row_id).val();
        detailtax_array['cgst_amt'] = $('#charge_amount10' + row_id).val();
    }
    if ($('#charge_percentage12' + row_id).val()) {
        detailtax_array['igst_per'] = $('#charge_percentage12' + row_id).val();
        detailtax_array['igst_amt'] = $('#charge_amount12' + row_id).val();
    }
    if ($('#charge_percentage11' + row_id).val()) {
        detailtax_array['Freight_Charges_per'] = $('#charge_percentage11' + row_id).val();
        detailtax_array['flightcharge_amt'] = $('#charge_amount11' + row_id).val();
    }
    if ($('#charge_percentage3' + row_id).val()) {
        detailtax_array['other_charges_per'] = $('#charge_percentage3' + row_id).val();
        detailtax_array['othercharge_amt'] = $('#charge_amount3' + row_id).val();
    }
    return detailtax_array;
}


function getPopupRowId() {
    var rowid_array = [];
    $('.calculationSelectRowID').each(function () {
        rowid_array.push(parseInt($(this).val()));
    });
    return rowid_array;
}




function validatePopUpdata(row_id, from_type) {
    var flag = true;
    if ($('#batch_name_model' + row_id).length) {
        var batch = $('#batch_name_model' + row_id).val();
        var expiry_date = $('#expiry_date_model' + row_id).val();
        var grn_qty = $('#grn_qty' + row_id).val();
        var rate = $('#rate' + row_id).val();
        var grn_mrp = $('#mrp' + row_id).val();
        var unit_mrp = $('#unit_mrp' + row_id).val();
        var uom_val = $('#uom_val_model' + row_id).val();
        var selling_price = $('#selling_price' + row_id).val();
        var free_item_id = $('#free_item_id' + row_id).is(":checked");
        var batch_status = $('#batch_needed' + row_id).val();
        var is_free = free_item_id ? 1 : 0;

        var free_qty = $('#free_qty' + row_id).val();

        if (!grn_mrp) {
            grn_mrp = 0;
        }
        if (!selling_price) {
            selling_price = 0;
        }
        if (!batch) {
            toastr.warning("Batch is Empty !!");
            $('#batch_name_model' + row_id).focus();
            flag = false;
        } else if (!expiry_date && parseInt(batch_status) == 1) {
            toastr.warning("Expiry date is Empty !!");
            $('#expiry_date_model' + row_id).focus();
            flag = false;
        } else if (parseInt(grn_qty) == 0) {
            toastr.warning("Please enter GRN qty");
            $('#grn_qty' + row_id).focus();
            flag = false;
        } else if ((parseFloat(rate) == 0) && parseFloat(is_free) == 0) {
            toastr.warning("Rate is Empty !!");
            $('#rate' + row_id).focus();
            flag = false;
        } else if (parseFloat(grn_mrp) == 0 && parseInt(batch_status) == 1) {
            toastr.warning("MRP is Empty !!");
            $('#mrp' + row_id).focus();
            flag = false;
        } else if (parseFloat(grn_mrp) < parseFloat(rate) && parseInt(batch_status) == 1) {
            toastr.warning("MRP is Less than Rate !!");
            $('#mrp' + row_id).focus();
            flag = false;
        } else if ((parseFloat(unit_mrp) < parseFloat(selling_price)) && parseInt(batch_status) == 1) {
            toastr.warning("Unit MRP is Less than Selling Price !!");
            $('#selling_price' + row_id).focus();
            flag = false;
        } else if (parseFloat(selling_price) == 0 && parseInt(batch_status) == 1) {
            toastr.warning("Selling Price cannot be empty !!");
            $('#selling_price' + row_id).focus();
            flag = false;
        } else {
            if (parseFloat(from_type) == 1) {
                var grn_status = $('#grnSavePosttype').val();
                if (parseInt(grn_id) != 0 && (parseInt(grn_status) == 2) || (parseInt(grn_status) == 4)) {
                    var tot_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * parseFloat(uom_val);
                    checkStockProductQty(tot_qty, row_id, 1);
                    flag = false;
                }
            }

        }
    }
    return flag;
}

function getLastDayofMonth(expiry_date) {

    if (expiry_date_format == 'MM-YYYY') {
        var arr = expiry_date.split('-');
        var year = arr[1] ? arr[1] : 0;
        var month = arr[0] ? arr[0] : 0;
        var lastDay = new Date(year, month, 0).getDate();
        expiry_date = lastDay + '-' + month + '-' + year;
    } else if (expiry_date_format == 'YYYY') {
        var year = arr[0] ? arr[0] : 0;
        var month = 1;
        var lastDay = new Date(year, month, 0).getDate();
        expiry_date = lastDay + '-' + month + '-' + year;
    }
    return expiry_date;
}


function calculatePopupCharges(from_type) {
    rowid_array = getPopupRowId();
    var flag = true;
    var newBatchItem = [];
    var row_id = 0;
    var main_row_id = 0;
    $.each(rowid_array, function (index, value) {
        row_id = value;
        var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
        if (!bill_detail_id || bill_detail_id == 'undefined') {
            bill_detail_id = 0;
        }
        var newBatch = $('#newBatchItem' + row_id).val();
        var batch = $('#batch_name_model' + row_id).val();
        var expiry_date = $('#expiry_date_model' + row_id).val();
        expiry_date = getLastDayofMonth(expiry_date);
        var grn_qty = $('#grn_qty' + row_id).val();
        var rate = $('#rate' + row_id).val();
        var grn_mrp = $('#mrp' + row_id).val();
        var hsn_code = $('#hsn_code_model' + row_id).val();
        var unit_mrp = $('#unit_mrp' + row_id).val();
        var uom_val = $('#uom_val_model' + row_id).val();
        var unit_check_box = $('#unit_check_box' + row_id).is(":checked");
        var sales_rate_mrp_check = unit_check_box ? 1 : 0;
        var selling_price = $('#selling_price' + row_id).val();
        var free_item_id = $('#free_item_id' + row_id).is(":checked");
        var batch_status = $('#batch_needed' + row_id).val();
        var po_id = $('#popPODetailID' + row_id).val();
        var is_free = free_item_id ? 1 : 0;
        var free_qty = $('#free_qty' + row_id).val();
        var item_unit = $('#uom_select_pop_id_' + row_id).val();
        var item_desc = $('#item_descpopup').val();
        var item_code = $('#item_codepopup').val();
        var item_id = $('#popupItemid' + row_id).val();
        flag = validatePopUpdata(row_id, from_type);
        if (flag) {
            var tax_details = calculatePopupData(row_id);
            var cgst_per = tax_details['cgst_per'];
            var cgst_amt = tax_details['cgst_amt'];
            var sgst_per = tax_details['sgst_per'];
            var sgst_amt = tax_details['sgst_amt'];
            var igst_per = tax_details['igst_per'];
            var igst_amt = tax_details['igst_amt'];
            var discount_per = tax_details['discount_per'];
            var discount_amt = tax_details['discount_amt'];
            var special_discount_per = tax_details['special_discount_per'];
            var special_discount_amt = tax_details['special_discount_amt'];
            var Freight_Charges_per = tax_details['Freight_Charges_per'];
            var flightcharge_amt = tax_details['flightcharge_amt'];
            var other_charges_per = tax_details['other_charges_per'];
            var othercharge_amt = tax_details['othercharge_amt'];
            var discounttype1 = tax_details['discounttype1'];
            var discounttype2 = tax_details['discounttype2'];
            var tot_rate = grn_qty * rate;


            var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
            var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
            var total_othercharges = parseFloat(othercharge_amt) + parseFloat(flightcharge_amt);
            var total_discount = parseFloat(discount_amt) + parseFloat(special_discount_amt);
            var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(total_discount);

            var item_rates = getItemPrice(grn_mrp, rate, grn_qty, free_qty, uom_val, net_amt, total_tax_amt);
            if (parseInt(sales_rate_mrp_check) == 0) {
                selling_price = item_rates.selling_price;
            }
            var unit_rate = item_rates.unit_rate;
            var unit_cost = item_rates.unit_cost;
            var total_qty = item_rates.total_qty;
            var tot_free_qty = item_rates.tot_free_qty;
            var all_total_qty = item_rates.all_total_qty;
            var unit_cost_with_out_tax = item_rates.unit_cost_with_out_tax;

            item_array[row_id] = {
                bill_detail_id: bill_detail_id,
                po_id: po_id,
                item_id: item_id,
                item_type: batch_status,
                hsn_code: hsn_code,
                batch_no: batch,
                expiry_date: expiry_date,
                item_code: item_code,
                item_desc: item_desc,
                grn_qty: grn_qty,
                item_unit: item_unit,
                unit_cost: unit_cost,
                unit_cost_with_out_tax: unit_cost_with_out_tax,
                uom_val: uom_val,
                free_qty: free_qty,
                is_free: is_free,
                total_qty: total_qty,
                tot_free_qty: checkIsNaN(tot_free_qty),
                all_total_qty: all_total_qty,
                sales_rate_mrp_check: sales_rate_mrp_check,
                grn_mrp: checkIsNaN(grn_mrp),
                unit_mrp: checkIsNaN(unit_mrp),
                item_rate: checkIsNaN(rate),
                unit_rate: checkIsNaN(unit_rate),
                selling_price: checkIsNaN(selling_price),
                cgst_per: checkIsNaN(cgst_per),
                cgst_amt: checkIsNaN(cgst_amt),
                sgst_per: checkIsNaN(sgst_per),
                sgst_amt: checkIsNaN(sgst_amt),
                igst_per: checkIsNaN(igst_per),
                igst_amt: checkIsNaN(igst_amt),
                total_tax_perc: checkIsNaN(total_tax_perc),
                total_tax_amt: checkIsNaN(total_tax_amt),
                discount_per: checkIsNaN(discount_per),
                discount_amt: checkIsNaN(discount_amt),
                discounttype1: parseInt(discounttype1),
                discounttype2: parseInt(discounttype2),
                total_discount: checkIsNaN(total_discount),
                special_discount_per: checkIsNaN(special_discount_per),
                special_discount_amt: checkIsNaN(special_discount_amt),
                othercharge_per: checkIsNaN(other_charges_per),
                othercharge_amt: checkIsNaN(othercharge_amt),
                flightcharge_per: checkIsNaN(Freight_Charges_per),
                flightcharge_amt: checkIsNaN(flightcharge_amt),
                total_othercharges: checkIsNaN(total_othercharges),
                totalRate: checkIsNaN(tot_rate),
                netRate: checkIsNaN(net_amt)
            }
            if (parseInt(newBatch) == 1) {
                newBatchItem.push(parseInt(row_id));
            } else {
                main_row_id = row_id;
            }
        } else {
            return false;
        }
    });

    if (flag) {
        $('#item_chargesmodel_div').html('');
        $('#item_charges_model').modal('toggle');
        setTimeout(function () {
            updateGrnList(main_row_id, 2);
        }, 1500);
    }

    if (flag && newBatchItem.length == 0) {
        getSelectedRowFocus();
    } else if (flag && newBatchItem.length != 0) {
        createBatchRows(newBatchItem);
    }
}

function editGRNRecalculate(item_list, tax_array, charge_type, row_id, from_type) {
    $.each(item_list, function (index, value) {
        var bill_detail_id = value.bill_detail_id ? value.bill_detail_id : 0;
        var item_id = value.item_id ? value.item_id : 0;
        var item_id_array = bill_detail_id + '::' + item_id;
        var item_type = value.item_type ? value.item_type : 0;
        var hsn_code = value.hsn_code ? (value.hsn_code).trim() : '';
        var item_code = value.item_code ? value.item_code : '';
        var rate = value.grn_rate ? value.grn_rate : 0;
        var unit_mrp = value.unit_mrp ? value.unit_mrp : 0;
        var grn_mrp = value.grn_mrp ? value.grn_mrp : 0;
        var uom_val = value.uom_val ? value.uom_val : 0;
        var sales_rate_mrp_check = value.sales_rate_mrp_check ? value.sales_rate_mrp_check : 0;
        var selling_price = value.sales_rate ? value.sales_rate : 0;
        var is_free = value.is_free ? value.is_free : 0;
        var grn_qty = value.grn_qty ? value.grn_qty : 0;
        var free_qty = value.grn_free_qty ? value.grn_free_qty : 0;
        var batch_no = value.batch_no ? value.batch_no : '';
        var expiry_date = value.expiry_date ? value.expiry_date : '';
        var item_unit = value.grn_unit ? value.grn_unit : 0;
        var po_id = value.po_id ? value.po_id : 0;
        var item_desc = value.item_desc ? (value.item_desc).trim() : '';
        var tot_rate = grn_qty * rate;

        var tax_single_item = tax_array[item_id_array] ? tax_array[item_id_array] : [];
        var charge_single_item = charge_type[item_id_array] ? charge_type[item_id_array] : [];
        var tax_details = calculateListNetTotal(tot_rate, tax_single_item, charge_single_item);
        var cgst_per = tax_details['cgst_per'];
        var cgst_amt = tax_details['cgst_amt'];
        var sgst_per = tax_details['sgst_per'];
        var sgst_amt = tax_details['sgst_amt'];
        var igst_per = tax_details['igst_per'];
        var igst_amt = tax_details['igst_amt'];
        var discount_per = tax_details['discount_per'];
        var discount_amt = tax_details['discount_amt'];
        var special_discount_per = tax_details['special_discount_per'];
        var special_discount_amt = tax_details['special_discount_amt'];
        var Freight_Charges_per = tax_details['Freight_Charges_per'];
        var flightcharge_amt = tax_details['flightcharge_amt'];
        var other_charges_per = tax_details['other_charges_per'];
        var othercharge_amt = tax_details['othercharge_amt'];
        var discounttype1 = tax_details['discounttype1'];
        var discounttype2 = tax_details['discounttype2'];

        var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
        var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
        var total_othercharges = parseFloat(othercharge_amt) + parseFloat(flightcharge_amt);
        var total_discount = parseFloat(discount_amt) + parseFloat(special_discount_amt);
        var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(total_discount);

        var item_rates = getItemPrice(grn_mrp, rate, grn_qty, free_qty, uom_val, net_amt, total_tax_amt);
        if (parseInt(sales_rate_mrp_check) == 0) {
            selling_price = item_rates.selling_price;
        }
        var unit_rate = item_rates.unit_rate;
        var unit_cost = item_rates.unit_cost;
        var total_qty = item_rates.total_qty;
        var tot_free_qty = item_rates.tot_free_qty;
        var all_total_qty = item_rates.all_total_qty;
        var unit_cost_with_out_tax = item_rates.unit_cost_with_out_tax;

        item_array[row_id] = {
            bill_detail_id: bill_detail_id,
            po_id: po_id,
            item_id: item_id,
            item_type: item_type,
            hsn_code: hsn_code,
            batch_no: batch_no,
            expiry_date: expiry_date,
            item_code: item_code,
            item_desc: item_desc,
            grn_qty: grn_qty,
            item_unit: item_unit,
            unit_cost: unit_cost,
            unit_cost_with_out_tax: unit_cost_with_out_tax,
            uom_val: uom_val,
            free_qty: free_qty,
            is_free: is_free,
            total_qty: total_qty,
            tot_free_qty: checkIsNaN(tot_free_qty),
            all_total_qty: all_total_qty,
            sales_rate_mrp_check: sales_rate_mrp_check,
            grn_mrp: checkIsNaN(grn_mrp),
            unit_mrp: checkIsNaN(unit_mrp),
            item_rate: checkIsNaN(rate),
            unit_rate: checkIsNaN(unit_rate),
            selling_price: checkIsNaN(selling_price),
            cgst_per: checkIsNaN(cgst_per),
            cgst_amt: checkIsNaN(cgst_amt),
            sgst_per: checkIsNaN(sgst_per),
            sgst_amt: checkIsNaN(sgst_amt),
            igst_per: checkIsNaN(igst_per),
            igst_amt: checkIsNaN(igst_amt),
            total_tax_perc: checkIsNaN(total_tax_perc),
            total_tax_amt: checkIsNaN(total_tax_amt),
            discount_per: checkIsNaN(discount_per),
            discount_amt: checkIsNaN(discount_amt),
            discounttype1: parseInt(discounttype1),
            discounttype2: parseInt(discounttype2),
            total_discount: checkIsNaN(total_discount),
            special_discount_per: checkIsNaN(special_discount_per),
            special_discount_amt: checkIsNaN(special_discount_amt),
            othercharge_per: checkIsNaN(other_charges_per),
            othercharge_amt: checkIsNaN(othercharge_amt),
            flightcharge_per: checkIsNaN(Freight_Charges_per),
            flightcharge_amt: checkIsNaN(flightcharge_amt),
            total_othercharges: checkIsNaN(total_othercharges),
            totalRate: checkIsNaN(tot_rate),
            netRate: checkIsNaN(net_amt)
        }
        updateGrnList(row_id, from_type);
        row_id++;
    });
    return row_id;
}


function insertFullListItems(item_list, tax_array, charge_type, row_id, from_type) {
    $.each(item_list, function (index, value) {
        var item_id = value.item_id ? value.item_id : 0;
        var item_type = value.item_type ? value.item_type : 0;
        var hsn_code = value.hsn_code ? (value.hsn_code).trim() : '';
        var item_code = value.item_code ? value.item_code : '';
        var rate = value.grn_rate ? value.grn_rate : 0;
        var unit_mrp = value.unit_mrp ? value.unit_mrp : 0;
        var grn_mrp = value.grn_mrp ? value.grn_mrp : 0;
        var uom_val = value.uom_val ? value.uom_val : 0;
        var sales_rate_mrp_check = value.sales_rate_mrp_check ? value.sales_rate_mrp_check : 0;
        var selling_price = value.sales_rate ? value.sales_rate : 0;
        var is_free = value.is_free ? value.is_free : 0;
        var grn_qty = value.grn_qty ? value.grn_qty : 0;
        var free_qty = value.grn_free_qty ? value.grn_free_qty : 0;
        var bill_detail_id = value.bill_detail_id ? value.bill_detail_id : 0;
        var batch_no = value.batch_no ? value.batch_no : '';
        var expiry_date = value.expiry_date ? value.expiry_date : '';
        var item_unit = value.grn_unit ? value.grn_unit : 0;
        var po_id = value.po_id ? value.po_id : 0;
        var item_desc = value.item_desc ? (value.item_desc).trim() : '';
        var tot_rate = grn_qty * rate;

        var tax_single_item = tax_array[item_id] ? tax_array[item_id] : [];
        var charge_single_item = charge_type[item_id] ? charge_type[item_id] : [];
        var tax_details = calculateListNetTotal(tot_rate, tax_single_item, charge_single_item);
        var cgst_per = tax_details['cgst_per'];
        var cgst_amt = tax_details['cgst_amt'];
        var sgst_per = tax_details['sgst_per'];
        var sgst_amt = tax_details['sgst_amt'];
        var igst_per = tax_details['igst_per'];
        var igst_amt = tax_details['igst_amt'];
        var discount_per = tax_details['discount_per'];
        var discount_amt = tax_details['discount_amt'];
        var special_discount_per = tax_details['special_discount_per'];
        var special_discount_amt = tax_details['special_discount_amt'];
        var Freight_Charges_per = tax_details['Freight_Charges_per'];
        var flightcharge_amt = tax_details['flightcharge_amt'];
        var other_charges_per = tax_details['other_charges_per'];
        var othercharge_amt = tax_details['othercharge_amt'];
        var discounttype1 = tax_details['discounttype1'];
        var discounttype2 = tax_details['discounttype2'];

        var total_tax_perc = parseFloat(cgst_per) + parseFloat(sgst_per) + parseFloat(igst_per);
        var total_tax_amt = parseFloat(cgst_amt) + parseFloat(sgst_amt) + parseFloat(igst_amt);
        var total_othercharges = parseFloat(othercharge_amt) + parseFloat(flightcharge_amt);
        var total_discount = parseFloat(discount_amt) + parseFloat(special_discount_amt);
        var net_amt = parseFloat(tot_rate) + parseFloat(total_tax_amt) + parseFloat(total_othercharges) - parseFloat(total_discount);

        var item_rates = getItemPrice(grn_mrp, rate, grn_qty, free_qty, uom_val, net_amt, total_tax_amt);
        if (parseInt(sales_rate_mrp_check) == 0) {
            selling_price = item_rates.selling_price;
        }
        var unit_rate = item_rates.unit_rate;
        var unit_cost = item_rates.unit_cost;
        var total_qty = item_rates.total_qty;
        var tot_free_qty = item_rates.tot_free_qty;
        var all_total_qty = item_rates.all_total_qty;
        var unit_cost_with_out_tax = item_rates.unit_cost_with_out_tax;

        item_array[row_id] = {
            bill_detail_id: bill_detail_id,
            po_id: po_id,
            item_id: item_id,
            item_type: item_type,
            hsn_code: hsn_code,
            batch_no: batch_no,
            expiry_date: expiry_date,
            item_code: item_code,
            item_desc: item_desc,
            grn_qty: grn_qty,
            item_unit: item_unit,
            unit_cost: unit_cost,
            unit_cost_with_out_tax: unit_cost_with_out_tax,
            uom_val: uom_val,
            free_qty: free_qty,
            is_free: is_free,
            total_qty: total_qty,
            tot_free_qty: tot_free_qty,
            all_total_qty: all_total_qty,
            sales_rate_mrp_check: sales_rate_mrp_check,
            grn_mrp: checkIsNaN(grn_mrp),
            unit_mrp: checkIsNaN(unit_mrp),
            item_rate: checkIsNaN(rate),
            unit_rate: checkIsNaN(unit_rate),
            selling_price: checkIsNaN(selling_price),
            cgst_per: checkIsNaN(cgst_per),
            cgst_amt: checkIsNaN(cgst_amt),
            sgst_per: checkIsNaN(sgst_per),
            sgst_amt: checkIsNaN(sgst_amt),
            igst_per: checkIsNaN(igst_per),
            igst_amt: checkIsNaN(igst_amt),
            total_tax_perc: checkIsNaN(total_tax_perc),
            total_tax_amt: checkIsNaN(total_tax_amt),
            discount_per: checkIsNaN(discount_per),
            discount_amt: checkIsNaN(discount_amt),
            discounttype1: parseInt(discounttype1),
            discounttype2: parseInt(discounttype2),
            total_discount: checkIsNaN(total_discount),
            special_discount_per: checkIsNaN(special_discount_per),
            special_discount_amt: checkIsNaN(special_discount_amt),
            othercharge_per: checkIsNaN(other_charges_per),
            othercharge_amt: checkIsNaN(othercharge_amt),
            flightcharge_per: checkIsNaN(Freight_Charges_per),
            flightcharge_amt: checkIsNaN(flightcharge_amt),
            total_othercharges: checkIsNaN(total_othercharges),
            totalRate: checkIsNaN(tot_rate),
            netRate: checkIsNaN(net_amt)
        }
        updateGrnList(row_id, from_type);
        row_id++;
    });
    return row_id;
}


function updateGrnList(row_id, from_type) {
    var batch = '';
    var expiry_date = '';
    var grn_qty = 0;
    var free_qty = 0;
    var tot_qty = 0;
    var unit_rate = 0;
    var unit_cost = 0;
    var unit_mrp = 0;
    var sales_price = 0;
    var total_tax_amt = 0;
    var total_discount = 0;
    var netRate = 0;
    if (item_array[row_id]) {
        batch = item_array[row_id].batch_no ? item_array[row_id].batch_no : '';
        expiry_date = item_array[row_id].expiry_date ? item_array[row_id].expiry_date : '';
        grn_qty = item_array[row_id].grn_qty ? item_array[row_id].grn_qty : 0;
        free_qty = item_array[row_id].free_qty ? item_array[row_id].free_qty : 0;
        tot_qty = item_array[row_id].all_total_qty ? item_array[row_id].all_total_qty : 0;
        unit_rate = item_array[row_id].unit_rate ? item_array[row_id].unit_rate : 0;
        unit_cost = item_array[row_id].unit_cost ? item_array[row_id].unit_cost : 0;
        unit_mrp = item_array[row_id].unit_mrp ? item_array[row_id].unit_mrp : 0;
        sales_price = item_array[row_id].selling_price ? item_array[row_id].selling_price : 0;
        total_tax_amt = item_array[row_id].total_tax_amt ? item_array[row_id].total_tax_amt : 0;
        total_discount = item_array[row_id].total_discount ? item_array[row_id].total_discount : 0;
        netRate = item_array[row_id].netRate ? item_array[row_id].netRate : 0;
    }

    $('#batch_row' + row_id).val(batch);
    $('#expiry_date' + row_id).val(expiry_date);
    $('#grn_qty' + row_id).val(checkIsNaN(grn_qty));
    $('#free_qty' + row_id).val(checkIsNaN(free_qty));
    $('#tot_qty_' + row_id).val(checkIsNaN(tot_qty));
    $('#unit_rate' + row_id).val(checkIsNaN(unit_rate));
    $('#unit_cost' + row_id).val(checkIsNaN(unit_cost));
    $('#unit-mrp_' + row_id).val(checkIsNaN(unit_mrp));
    $('#sales_price_' + row_id).val(checkIsNaN(sales_price));
    $('#tot_tax_amt_' + row_id).val(checkIsNaN(total_tax_amt));
    $('#tot_dic_amt_' + row_id).val(checkIsNaN(total_discount));
    $('#net_cost_' + row_id).val(checkIsNaN(netRate));

    if (parseInt(from_type) == 1) {
        setTimeout(function () {
            chargesModel(row_id);
        }, 1000);

    }
    if (parseInt(from_type) != 3) {
        getAllItemsTotals();
    }
}


function getItemPrice(mrp, rate, grn_qty, free_qty, uom_unit, net_amt, total_tax_amt) {
    var unit_cost = 0;
    var unit_cost_with_out_tax = 0;
    var return_data = {}
    if (!uom_unit) {
        uom_unit = 0;
    }


    var tot_unit_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * uom_unit;
    var tot_grn_qty = parseFloat(grn_qty) * uom_unit;
    var selling_price = 0;
    var unit_rate = 0;
    if (parseInt(uom_unit) != 0) {
        unit_rate = (rate / uom_unit);
    }


    if (!tot_unit_qty || parseFloat(tot_unit_qty) == 0) {
        tot_unit_qty = 0;
    } else {
        unit_cost = (net_amt) / tot_unit_qty;
    }

    if (parseFloat((parseFloat(grn_qty) * parseFloat(uom_unit))) != 0) {
        unit_cost_with_out_tax = (parseFloat(unit_cost)) - ((parseFloat(total_tax_amt) / ((parseFloat(grn_qty)) * parseFloat(uom_unit))));
    }

    if (!tot_grn_qty || parseFloat(tot_grn_qty) == 0) {
        tot_grn_qty = 0;
    } else {
        selling_price = (mrp * grn_qty) / tot_grn_qty;
    }

    var total_qty = parseFloat(grn_qty) * parseFloat(uom_unit);
    var tot_free_qty = parseFloat(free_qty) * parseFloat(uom_unit);
    var all_total_qty = parseFloat(tot_unit_qty);

    return_data = {
        unit_rate: unit_rate,
        selling_price: selling_price,
        unit_cost: unit_cost,
        total_qty: total_qty,
        tot_free_qty: tot_free_qty,
        all_total_qty: all_total_qty,
        unit_cost_with_out_tax: unit_cost_with_out_tax
    }
    return return_data;
}



function calculateAmount(row_id, item_id, status_code = '', from_type = 1) {
    var qty = $('#grn_qty' + row_id).val();
    var item_price = $('#rate' + row_id).val();
    if (!item_price) {
        item_price = 0;
        $('#rate' + row_id).val(0);
    }
    var discountAmountType1 = $('#popdiscount_type1' + row_id).val();
    var discountAmountType2 = $('#popdiscount_type32' + row_id).val();


    validateText(row_id);

    if ((qty != '0' || !qty) && (qty != '0' || !qty)) {
        var gst_amt = 0.0;
        var amount = 0.0;
        var tot_rate = qty * item_price;
        var discount_amt = 0.0;
        var discount_amt = $('#charge_amount1' + row_id).val();
        if (!discount_amt || (parseFloat(discount_amt) > parseFloat(tot_rate))) {
            discount_amt = 0;
        }
        if (parseInt(from_type) == 2) {
            var dis_amt = (parseFloat(discount_amt) / parseFloat(tot_rate)) * 100;
        }

        if (parseInt(discountAmountType1) == 1) {
            gst_amt = $('#charge_percentage1' + row_id).val();
            $('#charge_percentage1' + row_id).val(checkIsNaN(gst_amt));
            if (!gst_amt) {
                gst_amt = 0;
            }
            amount = (gst_amt * tot_rate) / 100;
            if (amount === Infinity) {
                amount = 0;
            }
            $('#charge_amount1' + row_id).val(checkIsNaN(amount));
            discount_amt = amount;
        } else if (parseInt(discountAmountType1) == 2) {
            gst_amt = $('#charge_amount1' + row_id).val();
            $('#charge_amount1' + row_id).val(checkIsNaN(gst_amt));
            discount_amt = gst_amt;
            if (!gst_amt) {
                gst_amt = 0;
            }
            if (tot_rate == '' || parseFloat(tot_rate) == 0) {
                amount = 0.0;
            } else {
                amount = (100 * gst_amt) / tot_rate;
                if (amount === Infinity) {
                    amount = 0;
                }
            }
            $('#charge_percentage1' + row_id).val(checkIsNaN(amount));
        }


        var without_discount = parseFloat(tot_rate) - parseFloat(discount_amt);

        var discount_amt = $('#charge_amount32' + row_id).val();
        if (!discount_amt || (parseFloat(discount_amt) > parseFloat(without_discount))) {
            discount_amt = 0;
        }
        if (parseInt(from_type) == 2) {
            var dis_amt = (parseFloat(discount_amt) / parseFloat(without_discount)) * 100;
            $('#charge_percentage32' + row_id).val(checkIsNaN(dis_amt));
        }

        if (parseInt(discountAmountType2) == 1) {
            gst_amt = $('#charge_percentage32' + row_id).val();
            $('#charge_percentage32' + row_id).val(checkIsNaN(gst_amt));
            if (!gst_amt) {
                gst_amt = 0;
            }
            amount = (gst_amt * without_discount) / 100;
            if (amount === Infinity) {
                amount = 0;
            }
            $('#charge_amount32' + row_id).val(checkIsNaN(amount));
            discount_amt = amount;
        } else if (parseInt(discountAmountType2) == 2) {
            gst_amt = $('#charge_amount32' + row_id).val();
            $('#charge_amount32' + row_id).val(checkIsNaN(gst_amt));
            discount_amt = gst_amt;
            if (!gst_amt) {
                gst_amt = 0;
            }
            if (without_discount == '' || parseFloat(without_discount) == 0) {
                amount = 0.0;
            } else {
                amount = (100 * gst_amt) / without_discount;
                if (amount === Infinity) {
                    amount = 0;
                }
            }
            $('#charge_percentage32' + row_id).val(checkIsNaN(amount));
        }
        without_discount = parseFloat(without_discount) - parseFloat(discount_amt);


        gst_amt = $('#charge_percentage9' + row_id).val();
        if (!gst_amt) {
            gst_amt = 0;
        }
        amount = (gst_amt * without_discount) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        if (parseInt(status_code) == 9) {
            $('#charge_percentage10' + row_id).val(gst_amt);
            $('#charge_amount10' + row_id).val(checkIsNaN(amount));
        }
        $('#charge_amount9' + row_id).val(checkIsNaN(amount));

        gst_amt = $('#charge_percentage10' + row_id).val();
        if (!gst_amt) {
            gst_amt = 0;
        }
        amount = (gst_amt * without_discount) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        if (parseInt(status_code) == 10) {
            $('#charge_percentage9' + row_id).val(gst_amt);
            $('#charge_amount9' + row_id).val(checkIsNaN(amount));
        }
        $('#charge_amount10' + row_id).val(checkIsNaN(amount));

        var sgst_per = $('#charge_percentage9' + row_id).val();
        var cgst_per = $('#charge_percentage10' + row_id).val();
        if (parseFloat(sgst_per) > 0 || parseFloat(cgst_per) > 0) {
            amount = 0;
            $('#charge_percentage12' + row_id).val(checkIsNaN(amount));
            $('#charge_percentage12' + row_id).attr('readonly', true);
        } else {
            $('#charge_percentage12' + row_id).attr('readonly', false);
        }

        gst_amt = $('#charge_percentage12' + row_id).val();
        if (!gst_amt) {
            gst_amt = 0;
        }
        amount = (gst_amt * without_discount) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        $('#charge_amount12' + row_id).val(checkIsNaN(amount));

        gst_amt = $('#charge_percentage13' + row_id).val();
        if (!gst_amt) {
            gst_amt = 0;
        }
        amount = (gst_amt * without_discount) / 100;
        if (amount === Infinity) {
            amount = 0;
        }
        $('#charge_amount13' + row_id).val(checkIsNaN(amount));

        var fight_charge = $('#charge_amount11' + row_id).val();
        if (!fight_charge) {
            fight_charge = 0;
            $('#charge_amount11' + row_id).val(0);
        }

        if (without_discount == '' || parseFloat(without_discount) == 0) {
            amount = 0.0;
        } else {
            amount = (fight_charge / without_discount) * 100;
        }

        $('#charge_percentage11' + row_id).val(checkIsNaN(amount));
        $('#charge_amount11' + row_id).val(checkIsNaN(fight_charge));

        var other_charge = $('#charge_amount3' + row_id).val();
        if (!other_charge) {
            other_charge = 0;
            $('#charge_amount3' + row_id).val(0);
        }
        amount = (other_charge / without_discount) * 100;


        if (without_discount == '' || parseFloat(without_discount) == 0) {
            amount = 0.0;
        } else {
            amount = (other_charge / without_discount) * 100;
        }

        $('#charge_percentage3' + row_id).val(checkIsNaN(amount));
        $('#charge_amount3' + row_id).val(checkIsNaN(other_charge));
        taxCalculation(row_id);
        if (parseInt(status_code) == 1 || parseInt(status_code) == 32) {
            calculateAmount(row_id, item_id, 100, from_type);
        }

    } else {
        $('.active_class').val(0);
        if (item_array[row_id]) {
            $('#charge_percentage9' + row_id).val(item_array[row_id].sgst_per ? item_array[row_id].sgst_per : 0);
            $('#charge_percentage10' + row_id).val(item_array[row_id].cgst_per ? item_array[row_id].cgst_per : 0);
            $('#charge_percentage12' + row_id).val(item_array[row_id].igst_per ? item_array[row_id].igst_per : 0);
            $('#charge_percentage1' + row_id).val(item_array[row_id].discount_per ? item_array[row_id].discount_per : 0);
            if ($('#charge_percentage32' + row_id)) {
                $('#charge_percentage32' + row_id).val(item_array[row_id].special_discount_per ? item_array[row_id].special_discount_per : 0);
            }
        }
    }
    $('#charge_percentage12' + row_id).attr('readonly', true);
}


function editSellingPriceCheck(row_id) {
    var status = $("#unit_check_box" + row_id).is(":checked");
    if (status) {
        $('#selling_price' + row_id).attr('readonly', false);
    } else {
        $('#selling_price' + row_id).attr('readonly', true);
    }
    taxCalculation(row_id);
}

function itemIsFree(row_id, item_id) {
    var status = $("#free_item_id" + row_id).is(":checked");
    if (status) {
        $('#rate' + row_id).attr('readonly', true);
        $('#rate' + row_id).val(0);
        $('#free_qty' + row_id).attr('readonly', true);
        $('#free_qty' + row_id).val(0);
    } else {
        var org_row_id = $('#row_idpopup').val();
        $('#rate' + row_id).attr('readonly', false);
        $('#free_qty' + row_id).attr('readonly', false);
        if (item_array[org_row_id].item_rate) {
            $('#rate' + row_id).val(item_array[org_row_id].item_rate ? item_array[org_row_id].item_rate : 0);
        }
        if (item_array[org_row_id].free_qty) {
            $('#free_qty' + row_id).val(item_array[org_row_id].free_qty ? item_array[org_row_id].free_qty : 0);
        }
        if (item_array[org_row_id].discount_per) {
            $('#charge_percentage1' + row_id).val(item_array[org_row_id].discount_per ? item_array[org_row_id].discount_per : 0);
        }
        if (item_array[org_row_id].special_discount_per) {
            $('#charge_percentage32' + row_id).val(item_array[org_row_id].special_discount_per ? item_array[org_row_id].special_discount_per : 0);
        }

    }
    calculateAmount(row_id, item_id);
}

function taxCalculation(row_id) {
    var req_qty = $('#grn_qty' + row_id).val();
    var item_price = $('#rate' + row_id).val();

    var tot_rate = req_qty * item_price;
    var tot_amt = 0.0;
    $('.amount_class' + row_id).each(function () {
        gst_amt = $(this).val();
        item_type = $(this).attr("data-code");
        if (item_type == 'GA') {
            tot_amt -= parseFloat(gst_amt);
        } else {
            tot_amt += parseFloat(gst_amt);
        }
    });

    var net_rate = tot_rate + tot_amt;
    var grn_mrp = $('#mrp' + row_id).val();
    var free_qty = $('#free_qty' + row_id).val();
    var uom_val = $('#uom_val_model' + row_id).val();

    var item_rates = getItemPrice(grn_mrp, item_price, req_qty, free_qty, uom_val, net_rate, 0);
    var check_status = $("#unit_check_box" + row_id).is(":checked");
    if (!check_status) {
        $('#selling_price' + row_id).val(checkIsNaN(item_rates.selling_price));
    }
    $('#unit_mrp' + row_id).val(checkIsNaN(item_rates.selling_price));
    $('#unit_rate' + row_id).val(checkIsNaN(item_rates.unit_rate));
    $('#unit_cost' + row_id).val(checkIsNaN(item_rates.unit_cost));
    $('#tot_rate_cal' + row_id).val(checkIsNaN(tot_rate));
    $('#charges_tot_pop' + row_id).val(checkIsNaN(net_rate));
    $('#all_total_qty_model' + row_id).val(checkIsNaN(item_rates.all_total_qty));
}

function calculateListNetTotal(tot_rate, tax_array, charge_type) {
    var tot_amt = 0.0;
    detailtax_array = [];
    detailtax_array['cgst_per'] = 0.0;
    detailtax_array['cgst_amt'] = 0.00;
    detailtax_array['sgst_per'] = 0.0;
    detailtax_array['sgst_amt'] = 0.00;
    detailtax_array['igst_per'] = 0.0;
    detailtax_array['igst_amt'] = 0.00;
    detailtax_array['discount_per'] = 0.00;
    detailtax_array['discount_amt'] = 0.00;
    detailtax_array['special_discount_per'] = 0.00;
    detailtax_array['special_discount_amt'] = 0.00;
    detailtax_array['Freight_Charges_per'] = 0.0;
    detailtax_array['flightcharge_amt'] = 0.00;
    detailtax_array['other_charges_per'] = 0.0;
    detailtax_array['othercharge_amt'] = 0.00;
    detailtax_array['discounttype1'] = 1;
    detailtax_array['discounttype2'] = 1;

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        charge_type_val = 1;
        if (charge_type[parseInt(index)]) {
            charge_type_val = charge_type[parseInt(index)];
        }
        if (parseInt(index) == 1) {
            detailtax_array['discount_per'] = value;
            detailtax_array['discount_amt'] = amount;
            detailtax_array['discounttype1'] = charge_type_val;
            tot_amt -= parseFloat(amount);
            tot_rate = parseFloat(tot_rate) - parseFloat(amount);
        }
        if (parseInt(index) == 32) {
            detailtax_array['special_discount_per'] = value;
            detailtax_array['special_discount_amt'] = amount;
            detailtax_array['discounttype2'] = charge_type_val;
            tot_amt -= parseFloat(amount);
            tot_rate = parseFloat(tot_rate) - parseFloat(amount);
        }
    });

    $.each(tax_array, function (index, value) {
        var amount = (value * tot_rate) / 100;
        if (parseInt(index) == 11) {
            detailtax_array['Freight_Charges_per'] = value;
            detailtax_array['flightcharge_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 3) {
            detailtax_array['other_charges_per'] = value;
            detailtax_array['othercharge_amt'] = amount;
            tot_amt += parseFloat(amount);
        }

        if (parseInt(index) == 10) {
            detailtax_array['cgst_per'] = value;
            detailtax_array['cgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 9) {
            detailtax_array['sgst_per'] = value;
            detailtax_array['sgst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
        if (parseInt(index) == 12) {
            detailtax_array['igst_per'] = value;
            detailtax_array['igst_amt'] = amount;
            tot_amt += parseFloat(amount);
        }
    });

    return detailtax_array;
}

function getAllItemsTotals() {
    $cgst_amt_total = 0.0;
    $sgst_amt_total = 0.0;
    $igst_amt_total = 0.0;
    $discount_amt_total = 0.0;
    $special_discount_amt_total = 0.0;
    $flightcharge_amt_total = 0.0;
    $othercharge_amt_total = 0.0;
    var total_array = [];

    var bill_discount = 0.0;
    var total_gross = 0.0;
    var total_othercharges = 0.0;

    $.each(item_array, function (index, value) {
        $cgst_amt_total = parseFloat($cgst_amt_total) + parseFloat(value.cgst_amt);
        $sgst_amt_total = parseFloat($sgst_amt_total) + parseFloat(value.sgst_amt);
        $igst_amt_total = parseFloat($igst_amt_total) + parseFloat(value.igst_amt);
        $discount_amt_total = parseFloat($discount_amt_total) + parseFloat(value.discount_amt);
        $special_discount_amt_total = parseFloat($special_discount_amt_total) + parseFloat(value.special_discount_amt);
        $flightcharge_amt_total = parseFloat($flightcharge_amt_total) + parseFloat(value.flightcharge_amt);
        $othercharge_amt_total = parseFloat($othercharge_amt_total) + parseFloat(value.othercharge_amt);
        total_gross = parseFloat(total_gross) + parseFloat(value.totalRate);
        total_othercharges = parseFloat(total_othercharges) + parseFloat(value.total_othercharges);
    });

    var round_off = $('#round_off').val();
    if (!round_off) {
        $('#round_off').val(0);
        round_off = 0.0;
    }
    var total_discount = parseFloat($discount_amt_total) + parseFloat($special_discount_amt_total) + parseFloat(bill_discount);
    var total_tax = parseFloat($cgst_amt_total) + parseFloat($sgst_amt_total) + parseFloat($igst_amt_total);
    var net_amount = (parseFloat(total_gross) + parseFloat(total_tax) + parseFloat(total_othercharges)) - parseFloat(total_discount);

    var bill_discount_type = $('#bill_discount_type').val();
    var bill_discount_value = $('#bill_discount_value').val();
    if (!bill_discount_value) {
        bill_discount_value = 0.0;
    }

    if (parseFloat(net_amount) > 0) {
        if (parseInt(bill_discount_type) == 1) {
            if (bill_discount_value > 100) {
                bill_discount_value = 0.0;
            }
            bill_discount = (net_amount * bill_discount_value) / 100;
        } else if (parseInt(bill_discount_type) == 2) {
            if (bill_discount_value > net_amount) {
                bill_discount_value = 0.0;
            }
            bill_discount = bill_discount_value;
        }
    } else {
        bill_discount_value = 0.0;
    }
    $('#bill_discount_value').val(checkIsNaN(bill_discount_value));
    $('#tot_bill_discount_amt').html(checkIsNaN(bill_discount));
    net_amount = (parseFloat(net_amount) + parseFloat(round_off)) - (parseFloat(bill_discount));

    total_array['total_gross'] = checkIsNaN(total_gross);
    total_array['total_dis'] = checkIsNaN(total_discount);
    total_array['total_tax'] = checkIsNaN(total_tax);
    total_array['total_net'] = checkIsNaN(net_amount);
    total_array['bill_discount'] = checkIsNaN(bill_discount);

    if ($('#SCHEMADISC_amot')) {
        $('#SCHEMADISC_amot').html(checkIsNaN($special_discount_amt_total));
    }
    if ($('#ITEMDISC_amot')) {
        $('#ITEMDISC_amot').html(checkIsNaN($discount_amt_total));
    }
    if ($('#SGST_amot')) {
        $('#SGST_amot').html(checkIsNaN($sgst_amt_total));
    }
    if ($('#CGST_amot')) {
        $('#CGST_amot').html(checkIsNaN($cgst_amt_total));
    }
    if ($('#IGST_amot')) {
        $('#IGST_amot').html(checkIsNaN($igst_amt_total));
    }
    if ($('#FRCHG_amot')) {
        $('#FRCHG_amot').html(checkIsNaN($flightcharge_amt_total));
    }
    if ($('#OTHCHG_amot')) {
        $('#OTHCHG_amot').html(checkIsNaN($othercharge_amt_total));
    }
    if ($('#gross_amount_hd').val()) {
        $('#gross_amount_hd').val(checkIsNaN(total_gross));
    }
    if ($('#discount_hd').val()) {
        $('#discount_hd').val(checkIsNaN(total_discount));
    }
    if ($('#tot_tax_amt_hd').val()) {
        $('#tot_tax_amt_hd').val(checkIsNaN(total_tax));
    }
    if ($('#tot_other_amt_hd').val()) {
        $('#tot_other_amt_hd').val(checkIsNaN(total_othercharges));
    }

    if ($('#net_amount_hd').val()) {
        $('#net_amount_hd').val(checkIsNaN(net_amount));
    }

    if ($('#tot_tax_footer')) {
        $('#tot_tax_footer').html(checkIsNaN(total_tax));
    }
    if ($('#tot_dicount_footer')) {
        $('#tot_dicount_footer').html(checkIsNaN(total_discount));
    }
    if ($('#tot_net_amt_footer')) {
        $('#tot_net_amt_footer').html(checkIsNaN(net_amount));
    }
    return total_array;
}
