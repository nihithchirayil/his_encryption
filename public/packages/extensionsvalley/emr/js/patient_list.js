
$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    window.search_url = $(".table_body_contents").attr('data-search-url');
    getPatientList(window.search_url);

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        if($(e.target).parent().hasClass('active') == false){
            var url = $(this).attr("href");
            $('.table_body_contents').attr('data-action-url', url);
            getPatientList(url);
        }
    });
    $(document).on('click', '.PatientSearchBtn', function (e) {
        getPatientList(window.search_url);
    });
});

function getPatientList(url) {
    var base_url = $('#base_url').val();
    // var url = base_url + "/master/fullPatientList";
    var patient_name = $('#searchpatient_name').val();
    var uhid = $('#searchpatient_uhid').val();
    var ipno = $('#searchpatient_ipno').val();
    var type = $('#searchpatient_type').val();
    var mobile = $('#searchpatient_mobile').val();
    var email = $('#searchpatient_email').val();
    var address = $('#searchpatient_address').val();
    var area = $('#searchpatient_area').val();
    var pincode = $('#searchpatient_pincode').val();
    var country = $('#searchpatient_country').val();
    var state = $('#searchpatient_state').val();
    var patient_id_hidden = $('#searchpatient_name_hidden').val();
    
    var param = {
        patient_name: patient_name,
        uhid: uhid,
        area: area,
        country: country,
        state: state,
        ipno: ipno,
        type: type,
        email: email,
        mobile: mobile,
        address: address,
        patient_id_hidden: patient_id_hidden,
        pincode: pincode
    };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function() {
            $('.PatientSearchBtn').attr('disabled', true);
            $('.PatientSearchBtn').find('i').removeClass('fa fa-search');
            $('.PatientSearchBtn').find('i').addClass('fa fa-spinner fa-spin');   
        },
        success: function(res) {
            $('#listPatientDataDivs').html(res);
        },
        complete: function() {
            $('.theadscroll').perfectScrollbar("update");
            $('.PatientSearchBtn').attr('disabled', false);
            $('.PatientSearchBtn').find('i').removeClass('fa fa-spinner fa-spin');
            $('.PatientSearchBtn').find('i').addClass('fa fa-search');
        }
    });
}

function resetAdvancePatientSearch() {
    $('#searchpatient_name').val('');
    $('#searchpatient_uhid').val('');
    $('#searchpatient_ipno').val('');
    $('#searchpatient_type').val('All').select2();
    $('#searchpatient_mobile').val('');
    $('#searchpatient_email').val('');
    $('#searchpatient_address').val('');
    $('#searchpatient_area').val('');
    $('#searchpatient_pincode').val('');
    $('#searchpatient_country').val('');
    $('#searchpatient_state').val('');
    $('#searchpatient_name_hidden').val('');
    window.search_url = $(".table_body_contents").attr('data-search-url');
    getPatientList(window.search_url);
}

function editPatientData(obj, patient_id) {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/editPatientBasicData";
    $('#hidden_patient_id').val(patient_id);
    var param = {
        patient_id: patient_id
    };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function() {
            $(obj).find('i').re
            $(obj).find('i').removeClass('fa fa-edit').addClass('fa fa-spinner fa-spin');
        },
        success: function(res) {
            $('#getPatientListModelBody').html(res);
        },
        complete: function() {
            $("#txt_patient_state").select2();
            $(obj).find('i').removeClass('fa fa-spinner fa-spin').addClass('fa fa-edit');
            $('.theadscroll').perfectScrollbar("update");
            $("#getPatientListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.txt_patient_date').datetimepicker({
                format: 'MMM-DD-YYYY',
            });            
            $('#tabs-nav li').click(function() {
                $('#tabs-nav li').removeClass('active');
                $(this).addClass('active');
                $('.tab-content1').hide();

                var activeTab = $(this).find('a').attr('href');
                $(activeTab).fadeIn();
                return false;
            });
        }
    });
}
/*upload image and save data*/
$("#savePatientDetailsForm").on('submit', (function(value) {
    value.preventDefault();
    var patient_id = $('#hidden_patient_id').val();
    var patient_email = $('#txt_patient_email').val();
    if (patient_email) {
        var email_check = isEmail();
        if (!email_check) {
            flag = 0;
            return false;
        }
    }
    var url = $('#base_url').val() + "/master/updatePatientDetails";
    var param = new FormData(this);
    param.append('patient_id', patient_id);
    $.ajax({
        url: url,
        type: "POST",
        async: true,
        data: param,
        contentType: false,
        cache: false,
        processData: false,
        functionName: 'documentUpload',
        crossDomain: true,
        beforeSend: function() {
            $('#btnSave_patientDetails').attr('disabled', true);
            $('#btnSave_patientDetails').find('i').removeClass('fa fa-save');
            $('#btnSave_patientDetails').find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            if (data.status == 1) {
                toastr.success("Patient Information Updated Successfully");
                var url_action = $(".table_body_contents").attr('data-action-url');
                getPatientList(url_action);
            }
        },
        error: function() {
            
        },
        complete: function() {
            $('#btnSave_patientDetails').attr('disabled', false);
            $('#btnSave_patientDetails').find('i').removeClass('fa fa-spinner fa-spin');
            $('#btnSave_patientDetails').find('i').addClass('fa fa-save');
        }
    });
}));


function getPatientAddress(from_type) {
    var url = $('#base_url').val() + "/patient_register/getPatientAddress";
    var patient_country = $("#txt_patient_country").val();
    var patient_state = $("#txt_patient_state").val();
    var param = { from_type: from_type, patient_country: patient_country, patient_state: patient_state };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (from_type == '1') {
                $("#txt_patient_state").html(data);
                $("#txt_patient_state").select2();
          
            } 
        },
        complete: function () {
            $("#patientappoinment_div").LoadingOverlay("hide");          
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
// EMAIL VALIDATION
function isEmail() {
    var flag = true;
    var email = $('#txt_patient_email').val();
    if (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        flag = regex.test(email);
    }
    if (!flag) {
        toastr.warning('Please enter valid email');
        $('#txt_patient_email').focus();
    }
    return flag
}
// SET GENDER SELECT SALUTATION
function getPatientGender() {
    var title_sex = $('#txt_patientsalutation').find(':selected').data('sex');
    var gender = $("#txt_patient_gender option[data-code='" + title_sex + "']").val();
    $("#txt_patient_gender").val(gender);
}

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    var search_key_id = '';

    if (input_id == 'searchpatient_uhid') {
        search_key_id = 'uhid';
    } else if (input_id == 'searchpatient_name') {
        search_key_id = 'patient';
    }
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();       

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + "/master/AjaxSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: '&search_key=' + search_key + '&search_key_id=' + search_key_id,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }
            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }       
});

function fillSearchDetials(id, name, serach_key_id) {
    var serach_key_type_id = '';
    if (serach_key_id == 'uhid') {
        serach_key_type_id = 'searchpatient_uhid';
    } else {
        serach_key_type_id = 'searchpatient_name';
    }        
    $('#' + serach_key_type_id + '_hidden').val(id);
    $('#' + serach_key_type_id).val(htmlDecode(name));
    $('#' + serach_key_type_id).attr('title', (name));
    $("#" + serach_key_type_id + "AjaxDiv").hide();
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

$(document).mouseup(function(e) {
    var container = $(".ajaxSearchBox");  
    // Check if the click event was triggered on the div or one of its children
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      // Close the div
      container.hide();
    }
  });