var token = $("#c_token").val();
var base_url = $("#base_url").val();

$(document).ready(function() {


    getEditBillData();

    $('#billPackage').attr('disabled', true);

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    var today = $('#current_date').val();
    $('#from_date').val(today);
    $('#to_date').val(today);
    $('.bill_date').val(moment().format('DD-MMM-YYYY HH:mm:ss'));
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

});

var ser_id = new Array();
$(document).on('click', '.advanceSearchBtn', function(event) {
    advancePatientSearch(1);

});

// hidden search--------------------
$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();




        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $('#base_url').val()

            var url = base_url + "/service_bill/ajaxsearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();

                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        // ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetial(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();

    var convert_head_id = $('#convert_head_id').val();
    if (!convert_head_id) {
        getBillData();
    }
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
});

function billTagSearch() {
    var uhid = $('#patient_uhid').val();
    var bill_tag = $('#search_bill_tag').val();
    if (uhid) {
        if (bill_tag == 'PB') {
            $('#billPackage').attr('disabled', false);
            $('#billPackage').prop('checked', true);
            packageLIst();
        } else {
            $('#billPackage').prop('checked', false);
            $('#billPackage').attr('disabled', true);
            serachServiceList();
        }
    }


}

function getEditBillData() {

    var head_id = $('#edit_head_id').val();
    var bill_no = $('#edit_bill_no').val();
    var uhid = $('#edit_uhid').val();
    var bill_tag = $('#edit_bill_tag').val();
    var package_id = $('#edit_package_id').val();
    var paid_status = $('#paid_status').val();
    var cancelled_status = $('#cancelled_status').val();
    if (paid_status == 1 || cancelled_status == 3) {
        $('#serviceListDiv').css('pointer-events', 'none');
        $('#service_bill_save').css('pointer-events', 'none');
        $('#ser_bill_detail_div_1').css('pointer-events', 'none');
        $('#editpricing').css('pointer-events', 'none');
    }
    // console.log(head_id);

    // console.log(package_id);


    if (head_id) {
        $('#search_bill_tag').val(bill_tag);
        $('#patient_uhid').val(uhid);
        $('#bill_head_ids').val(head_id);
        $('#patient_uhid').attr('disabled', true);
        $('.advanceSearchBtn').attr('disabled', true);
        $('#bill_tag').css('pointer-events', 'none');
        $('#editpricing').css('pointer-events', 'none');

        getBillData();
    }


}

function getBillData(intend = 0) {
    var uhid = $('#patient_uhid').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/getbill_no_data";
    var param = { uhid: uhid }
        // console.log(uhid);

    if ($('.serv_list').is(':checked')) {
        toastr.warning("Please remove all items");
        $('#patient_uhid').val(' ');
        return;
    }
    if ($('.package_select').is(':checked')) {
        toastr.warning("Please remove all items");
        $('#patient_uhid').val(' ');
        return;
    }
    $('#search_bill_tag').attr('disabled', false);
    $('#billPackage').attr('disabled', false);


    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            });
        },
        success: function(data) {
            // console.log(data);
            $('#phone_number').val(data[0].patient_phone);
            $('#patient_name').val(data[0].patient_name);
            var edit_company_id = $('#hidden_company_id').val();
            var edit_pricing_id = $('#hidden_pricing_id').val();
            if (edit_company_id) {
                $('#company').val(edit_company_id).select2();
            } else {
                $('#company').val(data[0].company_id).select2();
            }
            if (edit_pricing_id) {
                $('#pricing').val(edit_pricing_id).select2();
            } else {
                $('#pricing').val(data[0].pricing_id).select2();
            }
            // $('#company').val(data[0].company_id).select2();
            // $('#pricing').val(data[0].pricing_id).select2();
            // $('#bill_location').val(data[0].location);
            $('#visit_type').val(data[0].visit_status);
            $('#consulting_doctor').val(data[0].consulting_doctor_name);
            $('#admitting_doctor_id').val(data[0].admitting_doctor_id);
            $('#patient_id').val(data[0].patient_id);
            $("#op_no_hidden").val(data[0].patient_id);
            $('#pricing_id').val(data[0].pricing_id);
            $('#visit_id').val(data[0].visit_id);
            $('#bill_head_ids').val(data[0].bill_head_id);
            $('#package_id').val(data[0].package_id);
            $('#patient_age').val(data[0].age);
            $('#admitting_doctor_name').val(data[0].admitting_doctor_name);
            $('#consulting_doctor_name').val(data[0].consulting_doctor_id).select2();
            // console.log(data[0].consulting_doctor_id);
            $('#company_id').val(data[0].company_id);
            $('#consulting_doctor_id').val(data[0].consulting_doctor_id);
            $('#insured_status').val(data[0].insured_status);
            $('#roomType').val(data[0].room_type).select2();
            // console.log(data[0].room_type);
            $('#Ipnumber').val(data[0].ip_number);

            // console.log(data[0].insured_status);


            // if (data[0].insured_status == 1) {
            //     $('#paymentType').val(3).select2();
            //     // $('#paymentType').select2().attr('disabled', true);
            // }
            var patient_id = $('#patient_id').val();
            if (patient_id) {
                clearamount();
            }
            var bill_head_id = $('#edit_head_id').val();
            if (!bill_head_id) {
                paymentType();
            }

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('#editpricing').attr('disabled', false);
            $('#billPackage').attr('disabled', false);
            $('#DocFrmOut').attr('disabled', false);
            var pricing_id = $('#pricing_id').val();
            var bill_tag = $('#search_bill_tag').val();
            if (bill_tag == 'PB') {
                $('#billPackage').attr('disabled', false);
                $('#billPackage').prop('checked', true);
                packageLIst();
            } else {
                if (pricing_id && bill_tag) {
                    serachServiceList(intend);
                }
            }
            // advanceAmount();
            // editPricing();


        },
    });


}


// function advanceAmount() {
//     var visit_id = $('#patient_id').val();
//     var base_url = $('#base_url').val();
//     var url = base_url + "/service_bill/advanceAmount";
//     var param = { visit_id: visit_id }
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: param,
//         beforeSend: function() {

//         },
//         success: function(data) {
//             // console.log(data);
//             $("#advanceAmount").val(data[0].patient_advance);
//         },
//         complete: function() {},
//     });

// }


function serachServiceList(intend = 0) {
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var pricing_id = '';
    var bill_tag = $('#search_bill_tag').val();
    var package_id = $('#package_id').val();
    var base_url = $('#base_url').val();
    var uhid = $('#uhid').val();
    var room_type = $('#roomType').val();
    var edit_head_id = $('#edit_head_id').val();
    var convert_head_id = $('#convert_head_id').val();
    var payment_type = $('#paymentType').val();
    var edit_pricing_id = $('#hidden_pricing_id').val();
    pricing_id = $('#pricing_id').val();
    // console.log(patient_id);
    // console.log(pricing_id);
    // if (!edit_pricing_id) {
    //     pricing_id = $('#pricing_id').val();
    // } else {
    //     pricing_id = $('#hidden_pricing_id').val();
    // }

    if (patient_id && visit_id) {

        var url = base_url + "/service_bill/serviceListing";
        var param = { convert_head_id: convert_head_id, uhid: uhid, package_id: package_id, patient_id: patient_id, visit_id: visit_id, pricing_id: pricing_id, bill_tag: bill_tag, room_type: room_type, edit_head_id: edit_head_id }
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function() {
                $("body").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: "#337AB7",
                });
            },
            success: function(data) {

                $('#serviceListDiv').html(data);


                var payment_type = $('#paymentType').val();
                // console.log(payment_type);
                if (payment_type == 'insurance') {
                    $('#PayNow').fadeOut();
                } else {
                    $('#PayNow').fadeIn();
                }


            },
            complete: function() {
                editBillHeadDetails();
                if (intend != 0) {

                    for (i = 0; i < intend.service_list.length; i++) {
                        console.log(intend.service_list[i].id);
                        $("#add_" + intend.service_list[i].id).prop('checked', true);
                        $('#add_'+intend.service_list[i].id).attr('data_detail_id',intend.service_list[i].detail_id);
                    }
                    setTimeout(function() { sumFunction(); }, 1000);
                }

                $("body").LoadingOverlay("hide");
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

            },
        });

    }

}

function updatePricing() {
    if ($("#search_bill_tag").val() == "PB") {
        packageLIst();
    } else {
        serachServiceList();
    }
}

function popupDiv(id) {
    $(".popDiv").hide();
    $('#ppclose' + id).click(function() {
        $('#popup' + id).hide();
    });

    if ($('#popup' + id).is(":visible")) {

        $('#popup' + id).hide();
    } else if ($('#popup' + id).is(":hidden")) {
        $('#popup' + id).show();
        // console.log(id);
    }
}



function addNewBill() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/service_bill";
    document.location.href = url;
}

function packageLIst() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/packageDetails";

    var uhid = $('#patient_uhid').val();
    if (!uhid) {
        toastr.warning('Please select uhid');
        return;
    }
    if ($('.serv_list').is(':checked')) {
        toastr.warning('Please remove service Item');
        return;
    }

    if ($('#billPackage').is(':checked')) {
        $('#search_bill_tag').val("PB");
        $('#discountPer').prop('disabled', true);
        $('#discountAmt').prop('disabled', true);


        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function() {
                $("body").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: "#337AB7",

                });
                $('#conDocDiv').css('display', 'block');
                $('#admitDocDiv').css('display', 'none');
                $('#selectPkageName').html(' ');
            },
            success: function(data) {
                // console.log(data);
                $('#serviceListDiv').html(data);
                $('#consulting_doctor').attr('readonly', false);

            },
            complete: function() {
                $("body").LoadingOverlay("hide");
                editpackageDetails();
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
        });

    } else {
        toastr.warning(" No Package");
        return;
    }


}

function pkgpopupDiv(id) {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/packageList";
    $('#package_id').val(id);
    // var edit_package_id = $('#edit_package_id').val();
    // if (edit_package_id != id) {
    var param = { id: id }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            });
            $('.selectPkageName').html(' ');
        },
        success: function(data) {
            // console.log(data);
            $('#serviceListDiv').html(data);
            pkg_name = $('#pkgName').val();
            $('#serviceListDiv').append(pkg_name);

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            sumFunction();
        },
    });

    // }
}






function SaveDataJson() {

    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var bill_head_id = $('#edit_head_id').val();
    var edit_bill_no = $('#edit_bill_no').val();
    var id = $('.bill_details').attr('data-id');
    var detail_id = $('#bd_id_' + id).val();
    var bill_tag = $('#search_bill_tag').val();
    var uhid = $('#patient_uhid').val();
    if (location_id == 0 || location_id == '') {
        toastr.warning('Location is not defined');
        return false;
    }
    if (uhid == "" || uhid == " ") {
        toastr.warning('Please Fill UHID');
        return false;
    }
    if (bill_tag == "" || bill_tag == " ") {
        toastr.warning('Please Fill Bill Tag');
        return false;
    }
    if ($('#billPackage').is(':checked') == true) {
        $('.serv_list:checked').each(function() {
            toastr.warning('Please select  Package');
            $('.serv_list').prop('checked', false);
            return false;
        });
    }
    if ($('#billPackage').is(':checked') == false) {


        if ($('.serv_list').is(':checked') == false) {
            toastr.warning('Please add items');
            return false;
        }
    }
    if (!$('#consulting_doctor_name').val()) {
        toastr.warning('Please select consulting doctor');
        return false;
    }

    // console.log(detail_id);


    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/saveServiceBill";
    var jsonArray = {};


    if (edit_bill_no) {
        jsonArray.bill_no = $('#edit_bill_no').val();
    } else {
        jsonArray.bill_no = 0;
    }
    if (bill_head_id) {
        jsonArray.bill_head_id = $('#edit_head_id').val();

    } else {
        jsonArray.bill_head_id = 0;
    }
    jsonArray.uhid = $('#patient_uhid').val();
    jsonArray.visit_status = $('#visit_type').val();
    jsonArray.visit_id = $('#visit_id').val();
    jsonArray.patient_name = $('#patient_name').val();
    jsonArray.patient_age = $('#patient_age').val();
    jsonArray.admitting_doctor_id = $('#admitting_doctor_id').val();
    jsonArray.phone_number = $('#phone_number').val();
    jsonArray.admitting_doctor_name = $('#admitting_doctor_name').val();
    jsonArray.pricing_id = $('#pricing').val();
    jsonArray.company_id = $('#company').val();
    jsonArray.patient_id = $('#patient_id').val();
    jsonArray.bill_tag = $('#search_bill_tag').val();

    jsonArray.location = location_id;



    if ($('#DocFrmOut').is(':checked')) {
        jsonArray.consulting_doctor_name = $('#docFrmOutside').val();
    } else {
        jsonArray.consulting_doctor_name = $('#consulting_doctor_name').val();
    }

    jsonArray.consulting_doctor_id = $('#consulting_doctor_id').val();
    jsonArray.payment_type = $('#paymentType').val();
    jsonArray.netAmount = $('#netAmount').val();
    jsonArray.discount_type = 0;
    if ($('#discountAmt').is(':checked') || $('#discountPer').is(':checked')) {
        if ($('#discountAmt').is(':checked')) {
            jsonArray.discount_type = 2;
        } else if ($('#discountPer').is(':checked')) {
            jsonArray.discount_type = 1;
        }

        jsonArray.discount = $('#inPercenAmount').val();
        jsonArray.discount_amount = $('#inPercenAmount').val();
    } else {
        jsonArray.discount = $('#discount_val').val();
        jsonArray.discount_amount = $('#discount_val').val();
    }


    var bill_total_discount = $('#inPercenAmount').val();

    jsonArray.bill_amount = $('#billAmount').val();
    jsonArray.package_id = 0;
    


    if ($("#billPackage").is(':checked')) {

        var pkg_id = $('#package_id').val();
        // console.log(pkg_id);
        jsonArray.package_id = pkg_id;


    }
    jsonArray.service_items = [];
    var i = 1;
    $('.serv_list:checked').each(function() {

        var id = $(this).attr('data-id');
        var servDisc = $(this).attr('data-serv-desc');
        var bd_id = $('#edit_bd_id').val();
        var discount_type = 0;
        // console.log(discount_type);

        var temp = {};


        temp.id = $(this).attr('data-id');
        temp.bill_tag = $('#search_bill_tag').val();

        temp.package_id = $('#package_id').val() ? $('#package_id').val() : 0;

        temp.service_id = $(this).attr('data-id');
        temp.dataDepCode = $(this).attr('data-dep-code');
        temp.subDepCode = $(this).attr('data-subdep');
        temp.servCode = $(this).attr('data-serv-code');
        temp.servDisc = $(this).attr('data-serv-desc');
        temp.pricingDisc = $(this).attr('data-pricing-disc');
        temp.pricingDiscPer = $(this).attr('data-pridisc-per');
        temp.detail_id = 0;
        var isdiscountable = $(this).attr('data-isdiscountable');
        if (isdiscountable) {
            temp.isDiscoutale = 1;
        } else {
            temp.isDiscoutale = 0;
        }
        temp.isinsurexemption = $(this).attr('data-exemption');
        var is_refundable = $(this).attr('data-is-refundable');
        if (is_refundable) {
            temp.is_refundable = 1;
        } else {
            temp.is_refundable = 0;
        }
        var priceEditable = $(this).attr('data-is-is_price_editable');
        
        temp.intent_detail_id = $(this).attr('data_detail_id')>0?$(this).attr('data_detail_id'):0;
        if (priceEditable) {
            temp.priceEditable = 1;
        } else {
            temp.priceEditable = 0;
        }
        var qty = $('#qty_' + id).val();
        temp.qty = qty;
        if ($("#perType" + id).is(":checked")) {
            discount_type = 1;
        }
        if ($("#amtType" + id).is(":checked")) {
            discount_type = 2;
        }

        temp.discount_type = discount_type; //calculation discount wise
        // temp.amount = $(this).attr('data-price');
        temp.amount = $('#amount_' + id).val();
        var discamt = $('#discount' + id).val();

        if (discount_type == 1) {
            temp.discuntInPer = discamt;
            temp.sDiscunt = temp.amount * (discamt / 100) * qty;
            $price = $('#price_amount' + id).val();
        } else if (discount_type == 2) {
            temp.discuntInPer = 0;
            temp.sDiscunt = $('#discount' + id).val();
            $price = $('#price_amount' + id).val();
        } else {
            $price = $('#amount_' + id).val() * qty;
            temp.sDiscunt = 0;
            temp.discuntInPer = 0;
        }



        temp.price = $price;
        temp.total_price = $price;

        if ($('#discountAmt').is(':checked') || $('#discountPer').is(':checked')) {
            var total_bill_discount = $('#billAmount').val();
            var total_discount = $('#inPercenAmount').val();
            var split_discount = (parseFloat(total_discount) / parseFloat(total_bill_discount)) * 100;
            // console.log(split_discount);
            temp.splitDiscountAmt = (temp.price * split_discount) / 100;
        } else {
            temp.splitDiscountAmt = 0;
        }

        temp.detail_status = 1;
        temp.sl_no = i;
        temp.intend_id = 0;




        $('.bill_details').each(function() {

            var bd_id = $(this).attr('data-id');
            var bd_qty = $(this).attr('data-qty');
            var bd_netamount = $(this).attr('data-net-amount');
            var item_id = $(this).attr('data-item-id');
            var intend_id = $(this).attr('data-intend-id');
            var bill_amount = $('#edit_bill_amount').val();

            if (bill_head_id) {
                if ($('input:checked')) {

                    if (item_id == id) {
                        // console.log(id);
                        // console.log(item_id);
                        temp.intend_id = intend_id > 0 ? intend_id : 0;
                        temp.detail_id = bd_id;
                        if ((parseInt(bill_amount) != parseInt(temp.total_price)) || (parseInt(bd_qty) != parseInt(temp.qty))) {
                            temp.detail_status = 2;
                        } else {
                            temp.detail_status = 0;
                            temp.detail_id = bd_id;

                        }
                    }
                    //  else {
                    //     temp.detail_id = 0;
                    // }

                }


            }

        })

        i++;
        // console.log(temp.detail_id);

        jsonArray.service_items.push(temp);
        // console.log(servDisc);
    });
    //package bill_details adding...........


    if ($("#billPackage").is(':checked')) {
        var package_id = $('#package_id').val();
        // console.log(package_id);
        var edit_package_id = $('#edit_package_id').val();
        if (!edit_package_id) {
            if (package_id == "" || package_id == " ") {
                toastr.warning('Select package');
                return;
            }
            var i = i;


            $('.package_select').each(function() {
                var head_id = $('#package_id').val();

                var temp = {};
                var servid = $(this).attr('data-id');
                // console.log(id);
                temp.id = $(this).attr('data-id');
                temp.bill_tag = $('#search_bill_tag').val();
                temp.package_id = $(this).attr('data-id');
                temp.service_id = $(this).attr('pkg-service-id');
                temp.amount = $(this).attr('pkg-amt');
                temp.servDisc = $(this).attr('pkg-serv-disc');
                temp.qty = $('#qty_' + servid).val();
                temp.pricingDisc = 0;
                temp.pricingDiscPer = 0;
                temp.detail_id = 0;
                temp.detail_status = 1;
                temp.discuntInPer = 0;
                temp.sDiscunt = 0;
                temp.discount_type = 1;
                temp.total_price = temp.qty * temp.amount;
                var isdiscountable = $(this).attr('pkg-discountable');
                if (isdiscountable) {
                    temp.isDiscoutale = 1;
                } else {
                    temp.isDiscoutale = 0;
                }
                temp.isinsurexemption = 0;
                var is_refundable = $(this).attr('pkg-refundable');
                if (is_refundable) {
                    temp.is_refundable = 1;
                } else {
                    temp.is_refundable = 0;
                }
                var priceEditable = $(this).attr('pkg-price-editable');
                if (priceEditable) {
                    temp.priceEditable = 1;
                } else {
                    temp.priceEditable = 0;
                }
                temp.price = 0;
                if ($('#discountAmt').is(':checked') || $('#discountPer').is(':checked')) {
                    var total_bill_discount = $('#billAmount').val();
                    var total_discount = $('#inPercenAmount').val();
                    var split_discount = (parseFloat(total_discount) / parseFloat(total_bill_discount)) * 100;
                    // console.log(split_discount);
                    temp.splitDiscountAmt = (temp.price * split_discount) / 100;
                } else {
                    temp.splitDiscountAmt = 0;
                }
                temp.sl_no = i;
                temp.intent_detail_id = 0;



                $('.bill_details').each(function() {

                    var bd_id = $(this).attr('data-id');
                    var bd_qty = $(this).attr('data-qty');
                    var bd_netamount = $(this).attr('data-net-amount');
                    var item_id = $(this).attr('data-item-id');


                    if (bill_head_id) {
                        if ($('input:checked')) {

                            if (item_id == servid) {
                                // console.log(id);
                                // console.log(item_id);
                                temp.detail_id = bd_id;
                                if ((parseInt(bd_netamount) != parseInt(temp.total_price)) || (parseInt(bd_qty) != parseInt(temp.qty))) {
                                    temp.detail_status = 2;
                                } else {
                                    temp.detail_status = 0;
                                    temp.detail_id = bd_id;
                                }
                            } else {
                                // temp.detail_id = 1;
                            }

                        }


                    }
                })
                i++;
                jsonArray.service_items.push(temp);





            })
        }
        var edit_package_id = $('#edit_package_id').val();

        if (edit_package_id) {
            var i = 1;
            $('.bill_details').each(function() {

                var head_id = $('#package_id').val();
                var temp = {};
                var servid = $(this).attr('data-id');
                // console.log(id);
                temp.id = $(this).attr('data-id');
                temp.bill_tag = $('#search_bill_tag').val();
                temp.package_id = $(this).attr('data-pkg-id');
                temp.service_id = $(this).attr('data-item-id');
                temp.amount = $(this).attr('data-net-amount');
                temp.servDisc = $(this).attr('data-item-disc');
                temp.qty = $(this).attr('data-qty');
                temp.pricingDisc = 0;
                temp.pricingDiscPer = 0;
                temp.detail_id = $(this).attr('data-id');
                temp.detail_status = 0;
                temp.discuntInPer = 0;
                temp.sDiscunt = 0;
                temp.discount_type = 1;
                temp.total_price = temp.qty * temp.amount;
                var isdiscountable = $(this).attr('pkg-discountable');
                if (isdiscountable) {
                    temp.isDiscoutale = 1;
                } else {
                    temp.isDiscoutale = 0;
                }
                temp.isinsurexemption = 0;
                var is_refundable = $(this).attr('pkg-refundable');
                if (is_refundable) {
                    temp.is_refundable = 1;
                } else {
                    temp.is_refundable = 0;
                }
                var priceEditable = $(this).attr('pkg-price-editable');
                if (priceEditable) {
                    temp.priceEditable = 1;
                } else {
                    temp.priceEditable = 0;
                }
                temp.price = 0;
                if ($('#discountAmt').is(':checked') || $('#discountPer').is(':checked')) {
                    var total_bill_discount = $('#billAmount').val();
                    var total_discount = $('#inPercenAmount').val();
                    var split_discount = (parseFloat(total_discount) / parseFloat(total_bill_discount)) * 100;
                    // console.log(split_discount);
                    temp.splitDiscountAmt = (temp.price * split_discount) / 100;
                } else {
                    temp.splitDiscountAmt = 0;
                }
                temp.sl_no = i;
                temp.intent_detail_id = 0;

                $('.bill_details').each(function() {

                    var bd_id = $(this).attr('data-id');
                    var bd_qty = $(this).attr('data-qty');
                    var bd_netamount = $(this).attr('data-net-amount');
                    var item_id = $(this).attr('data-item-id');


                    // if (bill_head_id) {
                    //     if ($('input:checked')) {

                    //         if (item_id == servid) {
                    //             // console.log(id);
                    //             // console.log(item_id);
                    //             temp.detail_id = bd_id;
                    //             if ((parseInt(bd_netamount) != parseInt(temp.total_price)) || (parseInt(bd_qty) != parseInt(temp.qty))) {
                    //                 temp.detail_status = 2;
                    //             } else {
                    //                 temp.detail_status = 0;
                    //                 temp.detail_id = bd_id;
                    //             }
                    //         } else {
                    //             // temp.detail_id = 1;
                    //         }

                    //     }


                    // }
                })
                i++;
                jsonArray.service_items.push(temp);

            })
        }


    }


    $('.bill_details').each(function() {
        var item_id = $(this).attr('data-item-id');
        var bd_id = $(this).attr('data-id');
        var i = 1;
        var temp = {};

        $(":checkbox:not(:checked)").each(function() {

            var id = $(this).attr('data-id');
            if (item_id == id) {

                // console.log(id);

                temp.id = $(this).attr('data-id');
                temp.detail_id = bd_id;
                temp.bill_tag = $('#search_bill_tag').val();
                temp.package_id = $('#package_id').val();
                temp.service_id = $(this).attr('data-id');
                temp.dataDepCode = $(this).attr('data-dep-code');
                temp.subDepCode = $(this).attr('data-subdep');
                temp.servCode = $(this).attr('data-serv-code');
                temp.servDisc = $(this).attr('data-serv-desc');
                temp.servAmtDisc = $('#discount' + id).val();
                temp.pricingDisc = $(this).attr('data-pricing-disc');
                temp.pricingDiscPer = $(this).attr('data-pridisc-per');
                var isdiscountable = $(this).attr('data-isdiscountable');
                if (isdiscountable) {
                    temp.isDiscoutale = 1;
                } else {
                    temp.isDiscoutale = 0;
                }
                temp.isinsurexemption = $(this).attr('data-exemption');
                var is_refundable = $(this).attr('data-is-refundable');
                if (is_refundable) {
                    temp.is_refundable = 1;
                } else {
                    temp.is_refundable = 0;
                }
                var priceEditable = $(this).attr('data-is-is_price_editable');
                if (priceEditable) {
                    temp.priceEditable = 1;
                } else {
                    temp.priceEditable = 0;
                }
                var qty = $('#qty_' + id).val();
                temp.qty = qty;
                // temp.amount = $(this).attr('data-price');
                temp.amount = $('#amount_' + id).val();

                temp.splitDiscountAmt = 0;
                $total_price = $('#amount_' + id).val() * qty;
                temp.price = $total_price;
                temp.detail_status = 3;
                temp.sDiscunt = 0;
                temp.discount_type = 0;
                temp.total_price = 0;
                temp.sl_no = i;

                // console.log('3');
            }
            i++;


        })
        jsonArray.service_items.push(temp);

    });


    console.log(jsonArray);
    $.ajax({
        type: "POST",
        url: url,
        data: {
            jsonArray: jsonArray
        },
        dataType: "json",
        beforeSend: function() {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
            $('#printAndSave').attr('disabled', true);
        },
        success: function(response) {

            bill_id_array = [];
            $('#print_bill_id').val(response.data.bill_ids);
            var bill_id = response.data.bill_ids;

            var edit_bill_no = $('#edit_bill_no').val();
            var bill_no = '';
            if (response.data.bill_no) {
                bill_no = response.data.bill_no;
            } else if (edit_bill_no) {
                bill_no = edit_bill_no;
            }
            bill_id_array.push(bill_id[0]);
            var name = $('#patient_name').val();
            $('#bill_saved_name').html("<h4 class='modal-title' style='color: black;'>" + name + " BILL SAVED </h4>");
            $('#bill_saved_bill_no').html("<h4 class='modal-title' style='color: black;'>BILL NO : " + bill_no + " </h4>");


            var edit_package_id = $('#edit_package_id').val();
            var package_id = $('#package_id').val();
            if (edit_package_id > 0 || package_id > 0) {
                $('#pkgName').val('');
            }
            var payment_type = $('#paymentType').val();
            // console.log(payment_type);
            if (payment_type == 'insurance') {
                $('#PayNow').fadeOut();
            } else {
                $('#PayNow').fadeIn();

            }
            var patientPayableAmount = $('#patientPayable').val() ? parseInt($('#patientPayable').val()) : 0;
            if ((payment_type == 'insurance' || payment_type == 'credit') && patientPayableAmount == 0) {
                $('#PayNow').hide();

                if (bill_no) {
                    $('#serv_print_config_modal').modal('toggle');
                }

            } else {
                $('#PayNow').show();
                loadCashCollection(bill_id_array);

            }

        },
        complete: function() {
            // $('#serv_print_config_modal').modal('toggle');
            // clearDetails();
            $('#printAndSave').attr('disabled', false);
            // $('#PayNow').on('click', function() {
            //     loadCashCollection(bill_id_array);
            // });

            $("body").LoadingOverlay("hide");


        },
        error: function() {
            Command: toastr["error"]("Network Error");
            $('#printAndSave').attr('disabled', false);
        }
    });
}

function clearDetails() {

    $('#edit_bill_no').val('');


    $('#edit_head_id').val('');
    $('#edit_uhid').val('');
    $('#edit_bill_tag').val('');

    $('#phone_number').val('');
    $('#patient_name').val('');
    $('#company').val('').select2();
    $('#pricing').val('').select2();

    $('#visit_type').val('');
    $('#roomType').val('');
    $('#consulting_doctor').val('');
    $('#admitting_doctor_id').val('');
    $('#patient_id').val('');
    $('#pricing_id').val('');
    $('#visit_id').val('');
    $('#bill_head_ids').val('');

    $('#patient_age').val('');
    $('#admitting_doctor_name').val('');
    $('#consulting_doctor_name').val('').select2();
    $('#company_id').val('');
    $('#consulting_doctor_id').val('');
    $('#insured_status').val('');
    $('#roomType').val('').select2();
    $('#Ipnumber').val('');
    $(":checkbox").attr("checked", false);
    $('#patient_uhid').val('');

    $('#billAmount').val('');
    $('#itemTotal').val('');
    $('#discount_val').val('');
    $('#pricingDiscount').val('');
    $('#patientPayable').val('');
    $('#payorPayable').val('');
    $('#netAmount').val('');
    $('#advanceAmount').val('');
    $('.outstanding_amount').val('');
    $('#docFrmOutside').val('');
    $('#op_no_hidden').val('');

    $('#dicAmnt').val('');
    $('#inPercentg').val('');
    $('#inPercenAmount').val('');
    $('#patient_uhid_hidden').val('');
    $('#uhid_hidden').val('');
    $('#discountAmt').attr('disabled', false);
    $('#discountPer').attr('disabled', false);
    // $('#search_bill_tag').val(' ').select2();
    $('#convert_head_id').val('');
    $('#convert_code').val('');
    $('#service_id').val('');
    $('#intend_type').val('');
    // $('#print_status').val(' ');
    // $('#print_bill_id').val(' ');

}

function autosum(id, pricing_discount) {

    //discount type checking
    if ($("#amtType" + id).is(':checked') || $("#perType" + id).is(':checked')) {
        $('#discount' + id).removeAttr('disabled');
    }

    $("#amtType" + id).click(function() {

        if ($("#amtType" + id).is(':checked')) {
            $('#discount' + id).removeAttr('disabled');
            $('#discountPer').attr('disabled', true);
            $('#discountAmt').attr('disabled', true);
            $('#dicAmnt').attr('disabled', true);
            $('#perType' + id).attr('checked', false);

        } else {
            $('#discountPer').attr('disabled', false);
            $('#discountAmt').attr('disabled', false);
            $('#dicAmnt').attr('disabled', false);
            $('#discount' + id).val(0);
            $('#price_amount' + id).val(0);
            $('#discount' + id).attr('disabled', true);
        }
    })
    $("#perType" + id).click(function() {

        if ($("#perType" + id).is(':checked')) {
            $('#discount' + id).attr('disabled', false);
            $('#discountPer').attr('disabled', true);
            $('#discountAmt').attr('disabled', true);
            $('#dicAmnt').attr('disabled', true);
            $('#amtType' + id).attr('checked', false);

        } else {
            $('#discountPer').attr('disabled', false);
            $('#discountAmt').attr('disabled', false);
            $('#dicAmnt').attr('disabled', false);
            $('#discount' + id).val(0);
            $('#price_amount' + id).val(0);
            $('#discount' + id).attr('disabled', true);
        }
    })


    if ($('.discamount').is(':checked')) {
        $('#discountPer').attr('disabled', true);
        $('#discountAmt').attr('disabled', true);
    } else {
        $('#discountPer').attr('disabled', false);
        $('#discountAmt').attr('disabled', false);
    }



    //actual qty actual Amount
    var qty = $('#qty_' + id).val();
    var Amount = $('#amount_' + id).val();
    var price_amount = parseFloat(qty) * parseFloat(Amount);
    $('#price_amount' + id).val(price_amount);


    if ($("#perType" + id).is(":checked")) //  add discount amount in percentage
    {


        var discount = parseFloat($('#discount' + id).val());
        if (discount < 0 || discount > 100) {
            toastr.warning("Value should be between 0 - 100");
            $('#discount' + id).val('')
            return;
        }

        var disAmt = Amount * (discount / 100);

        var item_amount = Amount - disAmt;
        var total_amount = item_amount * qty;
        console.log(total_amount);
        $('#price_amount' + id).val(parseFloat(Math.round(total_amount))); //total= actual amount - discount
        // $('#amount_' + id).val(parseFloat(total_amount));

    }
    if ($("#amtType" + id).is(":checked")) // add discount in amount  Rs
    {


        var total_amt = $('#price_amount' + id).val();
        var discount = parseFloat($('#discount' + id).val());
        if (discount < 0 || discount > total_amt) {
            toastr.warning("Value should be between 0 and" + total_amt);
            $('#discount' + id).val('')
            return;
        }
        var disAmt = discount;

        var item_amount = Amount - disAmt;
        var total_amount = item_amount * qty;
        $('#price_amount' + id).val(parseFloat(total_amount)); //total price 
        // $('#amount_' + id).val(parseFloat(total_amount));
    }


    $('#p_discount' + id).val(pricing_discount); //pricing discount
    $('#qty' + id).val(qty);
    sumFunction();
}

//package bill sum calculation.................................... 
function pkgsum() {
    if ($("#billPackage").is(':checked')) {
        $('.package_select:checked').each(function() {
            var pkg_id = $(this).attr('pkg-id');
            // console.log(pkg_id);
            var qty = $('#qty' + pkg_id).val();
            var Amount = $('#Amt' + pkg_id).val();
            var total_amount = Amount * qty;
            $('#totalAmt' + pkg_id).val(parseFloat(total_amount))

            pkgpopupDiv(pkg_id);
        })
        sumFunction();


    }
}

$(document).on('click', '.serv_list', function() {
    if ($('.serv_list:checked').length == 0 && $("#patient_uhid").val() != '') {
        $("#search_bill_tag").attr('disabled', true);
        $("#patient_uhid").attr('disabled', true);
        $("#out_side_patient").attr('disabled', true);
    } else {
        $("#search_bill_tag").attr('disabled', true);
        $("#patient_uhid").attr('disabled', true);
        $("#out_side_patient").attr('disabled', true);
    }
})


function sumFunction() {
    //service bill amount sum........

    var con_doctor_id = $('#consulting_doctor_name').val();
    var add_doctor_id = $('#admitting_doctor_name').val();
    var notdisc_sum = 0;
    var sum = 0;
    var discount = 0;
    var total_discount = 0;
    var pricing_disc = 0;
    var discount_val = 0;
    var discount_type = 0;



    $('.serv_list:checked').each(function() {


        if ($('#billPackage').is(':checked') == true) {
            $('.serv_list:checked').each(function() {
                toastr.warning('Please select  Package');
                $('.serv_list').prop('checked', false);
                return;
            });
        }
        var id = $(this).attr('data-id');
        var discountable = $(this).attr('data-isdiscountable');



        if ($("#perType" + id).is(":checked")) {
            discount_type = 1;
        }
        if ($("#amtType" + id).is(":checked")) {
            discount_type = 2;
        }
        var qty = $('#qty_' + id).val();
        var pricingDisc = $(this).attr('data-pricing-disc');
        // var pricingDiscPer = $(this).attr('data-pridisc-per')


        var amount = $('#amount_' + id).val() * qty; //one item total amount 


        if (discount_type == 1) { //discount percentage
            var discount = parseFloat($('#discount' + id).val()) > 0 ? parseFloat($('#discount' + id).val()) : 0;

            // if (!discount) {
            //     var discount = 0;
            // }

            var discount_price = amount * (discount / 100); //calculate discount amount  
        } else {
            var discount_price = parseFloat($('#discount' + id).val()) > 0 ? parseFloat($('#discount' + id).val()) : 0;
            // if (!discount_price) {
            //     var discount_price = 0;
            // }
        }


        discount_val = discount_val + parseFloat(discount_price); //total discount

        pricing_disc = pricing_disc + parseFloat(pricingDisc); //total pricing

        var discountable = $(this).attr('data-isdiscountable');
        if (discountable) {
            var service_amount = amount; //total price ( actual amount - discount ) of one item
            sum = sum + parseFloat(service_amount); //total bill amount 

        } else {
            var not_disc_service_amount = amount; //total price ( actual amount - discount ) of one item
            notdisc_sum = notdisc_sum + parseFloat(not_disc_service_amount); //total bill amount 
        }

        var uhid = $('#patient_uhid').val();
        var bill_tag = $('#search_bill_tag').val();


        if (!uhid) {
            toastr.warning('Please select uhid');
            $('#add_' + id).prop('checked', false);
            return;
        } else if (!bill_tag) {
            toastr.warning('Please select Bill tag');
            $('#add_' + id).prop('checked', false);
            return;
        }



        // $('#price_amount' + id).val(parseFloat(service_amount));
        // $('#p_discount' + id).val(parseFloat(pricingDisc));
        // // $('#discount' + id).val(parseFloat(discount_price));
        // $('#qty' + id).val(parseFloat(qty));


        var docrequired = $(this).attr('data-doc-required');
        if (docrequired) {
            if (!con_doctor_id) {
                toastr.warning("Please Select Doctor");
                $('#add_' + id).prop(':checked', false);
                return;
            }
        }
    })
    if ($("#billPackage").is(':checked')) { //package amount sum
        $('.package_select').each(function() {
            var pkg_id = $(this).attr('data-id');
            var qty = $('#qty_' + pkg_id).val();
            var Amount = $('#pAmount' + pkg_id).val();
            var total_amount = Amount * qty;
            sum = sum + total_amount;
            notdisc_sum = 0;

        })

    }


    var payment_type = $('#paymentType').val();


    $('#discount_val').val(parseFloat(discount_val)); // total bill item discount
    $("#billAmount").val(parseFloat(sum) + parseFloat(notdisc_sum)); //total bill_amount
    $("#itemTotal").val(parseFloat(sum) + parseFloat(notdisc_sum)); //total bill_amount
    $('#pricingDiscount').val(parseFloat(pricing_disc)); //total bill pricing discount
    total_discount = parseFloat(discount_val) //total bill discount
    var total_amount = (parseFloat(sum) + parseFloat(notdisc_sum)) - parseFloat(total_discount) //total bill amount = sum - total_discount 
    $('#totalDisc').val(total_discount);
    if (payment_type == 'cash/Card' || payment_type == 'ipcredit') {
        $('#patientPayable').val(parseFloat(total_amount));
        $('#netAmount').val(parseFloat(total_amount));
        $('#payorPayable').val(parseFloat(0));
    } else if (payment_type == "credit") {
        $('#patientPayable').val(parseFloat(0));
        $('#payorPayable').val(parseFloat(total_amount));
        $('#netAmount').val(parseFloat(total_amount));
    } else if (payment_type == "insurance") {
        $('#patientPayable').val(parseFloat(0));
        $('#payorPayable').val(parseFloat(total_amount));
        $('#netAmount').val(parseFloat(total_amount));

    }

}

$("input[name='itemDiscount']").keyup(function() {
    number = $("input[name='itemDiscount']").val()
    if (number <= 0 || number >= 100) {
        $("input[name='itemDiscount']").val("");
        alert("0 - 100");
    }
});




function billListDetails() {
    var base_url = $('#base_url').val();
    // var url = base_url + "/service_bill/billHeadListing";
    var url = base_url + "/billlist/billlist/1";
    document.location.href = url;



}

function PatientInvestigation() {
    if ($('.serv_list').is(':checked')) {
        toastr.warning('Please Remove items');
    } else {

        var base_url = $('#base_url').val();
        $('#patientinvestigation-lg').modal('show');
        $('#convert_head_id').val(1);
        var patient_uhid = $('#patient_uhid').val();
        $('#conuhid').val(patient_uhid);
        $('#conuhid_hidden').val(patient_uhid);
        patientInvestiGationList();

        // var url = base_url + "/service_bill/patientInvestigation";
        // document.location.href = url;
    }
}



function editBillHeadDetails() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/editBillDetails";
    var bill_head_id = $('#edit_head_id').val();
    var package_id = $('#edit_package_id').val();
    var pricing_id = $('#hidden_pricing_id').val();
    var company_id = $('#hidden_company_id').val();
    var payment_type = $('#hidden_payment_type')

    param = { bill_head_id: bill_head_id };
    if (bill_head_id) {
        $.ajax({
            type: "post",
            url: url,
            data: param,
            beforeSend: function() {
                // $('#company').val(company_id).select2();
            },
            success: function(data) {
                if ($('#editpricing').is(':checked') == false) {
                    editPricing();
                }
                for (i = 0; i < data.length; i++) {
                    $('#edit_bd_id').val(data[i].id);
                    $('#edit_bd_netamount').val(data[i].net_amount);
                    $('#edit_bd_qty').val(data[i].qty);
                    $('#amount_' + data[i].item_id).val(data[i].actual_selling_price);
                    $('#price_amount' + data[i].item_id).val(data[i].net_amount);
                    $('#qty_' + data[i].item_id).val(data[i].qty);
                    $('#qty' + data[i].item_id).val(data[i].qty);
                    $('#add_' + data[i].item_id).prop('checked', true);
                    $('#add_' + data[i].item_id).attr('data_detail_id',data[i].intend_id );
                    // console.log(data[i].discount_type);
                    if (data[i].discount_type == 1) {

                        $('#perType' + data[i].item_id).prop('checked', true);


                        var discount = data[0].discount;
                        var qty = data[0].qty;
                        var actual_value = data[0].actual_selling_price * qty;
                        // console.log(actual_value);
                        // console.log(discount);

                        var dicPer = discount * 100 / actual_value;
                        // console.log(dicPer);
                        $('#discount' + data[i].item_id).val(dicPer);
                    } else if (data[i].discount_type == 2) {
                        $('#amtType' + data[i].item_id).prop('checked', true);


                        $('#discount' + data[i].item_id).val(data[i].discount_amount);
                    }

                }



            },
            complete: function() {

                var paid_status = $('#paid_status').val()
                var cancelled_status = $('#cancelled_status').val()
                if (paid_status == 1 || cancelled_status == 3) {
                    $('#serviceListDiv').css('pointer-events', 'none');
                    $('#service_bill_save').css('pointer-events', 'none');
                    $('#ser_bill_detail_div_1').css('pointer-events', 'none');
                }
                sumFunction();
                if ($('.serv_list').is(':checked')) {
                    $('#billPackage').prop('disabled', true);
                }
                var outsidePStatus = $('#edit_outside_status').val();
                if (outsidePStatus == 1) {
                    $('#out_side_patient').prop('checked', true);
                } else {
                    $('#out_side_patient').prop('disabled', true);
                }

                var bill_amount = $('#edit_actual_amount').val();

                var discount_type = $('#edit_discount_type').val();
                var discount_val = $('#edit_discount_val').val();
                var net_amount = $('#edit_bill_amount').val();



                if (discount_type == 1) {
                    $('#discountPer').prop('checked', true);
                    // $('#dicAmnt').val(discount_val);
                    $('#inPercenAmount').val(discount_val);
                    var amountInPer = (discount_val / bill_amount) * 100;

                    $('#inPercentgDisc').val(Math.round(amountInPer));
                    $('#dicAmnt').val(Math.round(amountInPer));

                    $('#patientPayable').val(net_amount);
                    $('#netAmount').val(net_amount);
                } else if (discount_type == 2) {
                    $('#discountAmt').prop('checked', true);

                    $('#dicAmnt').val(discount_val);
                    $('#inPercenAmount').val(discount_val);
                    var amountInPer = (discount_val / bill_amount) * 100;

                    $('#inPercentgDisc').val(Math.round(amountInPer));

                    $('#patientPayable').val(net_amount);
                    $('#netAmount').val(net_amount);
                }
                discountAdd();
            }
        });
    }

}



function refreshsearchItm() {
    $('#patient_uhid').val(' ');
    $('#patient').val(' ');
    $('#billno').val(' ');
    $('#from_date').val(' ');
    $('#to_date').val(' ');
    $('#docname').val(' ');
    $('#billtag').val(' ').select2();
    $('#pytype').val(' ').select2();
    $('#paid_from_date').val(' ');
    $('#paid_to_date').val(' ');

}


function getPackageDetails(id) {
    var popupDiv = document.getElementById("popup" + id);
    popupDiv.classList.toggle("show");
}
$(document).on('click', function(event) {

    $(this).parent().parent().find(".popupDiv").hide();
});

function addDiscountAmt() {

    if ($('#discountAmt').is(':checked')) {

        $('#discountPer').attr('disabled', true);
        var patient_payable = $('#patientPayable').val() ? $('#patientPayable').val() : 0;
        var discAmt = $('#dicAmnt').val() ? $('#dicAmnt').val() : 0;
        var billAmt = $('#billAmount').val() ? $('#billAmount').val() : 0;
        var itemDisc = $('#discount_val').val() ? $('#discount_val').val() : 0;
        var actual_bill_amt = billAmt - itemDisc;

        if ((billAmt - discAmt) < 0) {
            toastr.warning("discount not possible");
            $('#dicAmnt').val(0);
            $('#inPercentgDisc').val(0);
            $('#inPercenAmount').val(0);
            discountAdd();
            return;
        }
        var discInAmt = discAmt;
        var discInPer = (discAmt / actual_bill_amt) * 100;
        $('#inPercentgDisc').val(Math.round(discInPer));

        $('#inPercenAmount').val(discInAmt);
        // console.log(patient_payable);
        // console.log(discAmt);
        var total_pay_amount = actual_bill_amt - discAmt;
        // console.log(total_pay_amount);
        var payment_type = $('#paymentType').val()
        if (payment_type == 'cash/Card' || payment_type == 'ipcredit') {
            $('#patientPayable').val(parseFloat(total_pay_amount));
            $('#netAmount').val(parseFloat(total_pay_amount));
            $('#payorPayable').val(parseFloat(0));
        } else {
            $('#patientPayable').val(parseFloat(0));
            $('#payorPayable').val(parseFloat(total_pay_amount));
            $('#netAmount').val(parseFloat(total_pay_amount));
        }




    } else {
        $('#discountPer').removeAttr('disabled');

        var discount_val = $('#discount_val').val();
        var billAmt = $('#billAmount').val();
        var actualAmount = billAmt - discount_val;

        $('#dicAmnt').val(' ');
        $('#inPercentgDisc').val(' ');
        $('#inPercenAmount').val(' ');
        $('#patientPayable').val(actualAmount);
        $('#payorPayable').val(0);
        $('#netAmount').val(actualAmount);
    }
}

function discountAmtPer() {

    if ($('#discountPer').is(':checked')) {

        $('#discountAmt').attr('disabled', true);
        var patient_payable = $('#patientPayable').val() ? $('#patientPayable').val() : 0;
        var discAmt = $('#dicAmnt').val() ? $('#dicAmnt').val() : 0;
        var billAmt = $('#billAmount').val() ? $('#billAmount').val() : 0;
        var itemDisc = $('#discount_val').val() ? $('#discount_val').val() : 0;
        var actual_bill_amt = billAmt - itemDisc;


        var discInAmt = actual_bill_amt * (discAmt / 100);
        if ((billAmt - discInAmt) < 0) {
            toastr.warning("discount not possible");
            $('#dicAmnt').val(0);
            $('#inPercentgDisc').val(0);
            $('#inPercenAmount').val(0);

            discountAdd();
            return;
        }
        var discInPer = discAmt;
        $('#inPercentgDisc').val(discInPer);
        $('#inPercenAmount').val(discInAmt);
        // console.log(patient_payable);
        // console.log(discAmt);
        var total_pay_amount = actual_bill_amt - discInAmt;
        // console.log(total_pay_amount);
        var payment_type = $('#paymentType').val()
        if (payment_type == 'cash/Card' || payment_type == 'ipcredit') {
            $('#patientPayable').val(parseFloat(total_pay_amount));
            $('#netAmount').val(parseFloat(total_pay_amount));
            $('#payorPayable').val(parseFloat(0));
        } else {
            $('#patientPayable').val(parseFloat(0));
            $('#payorPayable').val(parseFloat(total_pay_amount));
            $('#netAmount').val(parseFloat(total_pay_amount));
        }



    } else {
        $('#discountAmt').removeAttr('disabled');
        var discount_val = $('#discount_val').val();
        var billAmt = $('#billAmount').val();
        var actualAmount = billAmt - discount_val;
        $('#dicAmnt').val(' ');
        $('#inPercentgDisc').val(' ');
        $('#inPercenAmount').val(' ');
        $('#patientPayable').val(actualAmount);
        $('#payorPayable').val(0);
        $('#netAmount').val(actualAmount);
    }


}

function discountAdd() {
    if ($('#discountAmt').is(':checked')) {
        addDiscountAmt();
    } else if ($('#discountPer').is(':checked')) {
        discountAmtPer();
    } else {
        $('#inPercentgDisc').val(" ");
        $('#inPercenAmount').val(" ");
        $('#dicAmnt').val(" ");
    }



}


function checkPricing() {
    if ($('#editpricing').is(':checked')) {
        $('#company').attr('disabled', false);
        $('#pricing').attr('disabled', false);
        $('#paymentType').attr('disabled', false);
        $('#updatePricing').attr('disabled', false);

    } else {
        $('#company').attr('disabled', true);
        $('#pricing').attr('disabled', true);
        $('#updatePricing').attr('disabled', true);
        $('#paymentType').attr('disabled', true);
    }
}

function editPricing() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/editPricing";
    var company_id = $('#company').val();
    var uhid = $('#patient_uhid').val();
    var pricing_id = $('#pricing_id').val();
    var edit_pricing = $('#hidden_pricing_id').val();
    // console.log(company_id);
    var param = {
        company_id: company_id,
    }
    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function() {
            $('#pricing').empty();
        },
        success: function(data) {
            // console.log(data);

            if (data) {


                $.each(data, function(key, value) {
                    ($("#pricing").append('<option value="' + value.id + '">' + value.pricing_name + '</option>'));
                })
            }
        },
        complete: function() {
            changePricingId();

            if (edit_pricing) {
                $('#pricing').val(edit_pricing).select2();
                // console.log(1);
            } else {
                $('#pricing').val(pricing_id).select2();
            }
            paymentType();
        }


    });
}

function changePricingId() {
    var pricing_id = $('#pricing').val();
    $('#pricing_id').val(pricing_id);
}

function paymentType() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/editPymentType";
    var company_id = $('#company').val();
    var visit_status = $('#visit_type').val();
    var visit_id = $('#visit_id').val();

    // console.log(company_id);
    // console.log(visit_status);
    var param = {
        company_id: company_id,
        visit_status: visit_status,
        visit_id: visit_id,
    }


    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function() {
            $('#paymentType').empty();
        },
        success: function(data) {
            // console.log(data);
            if (data) {
                if (data[0].company_type == 3 && data[0].visit_status == 'OP') {
                    $('#paymentType').append('<option value="insurance">Insurance</option>');
                    $('#paymentType').append('<option value="cash/Card">Cash/Card</option>');
                } else if (data[0].company_type == 3 && data[0].visit_status == 'IP') {
                    $('#paymentType').append('<option value="insurance">Insurance</option>');
                    $('#paymentType').append('<option value="cash/Card">Cash/Card</option>');
                    $('#paymentType').append('<option value="ipcredit">IP Credit</option>');

                } else if (data[0].company_type == 2 && data[0].visit_status == 'IP') {
                    $('#paymentType').append('<option value="credit">Company Credit</option>');
                    $('#paymentType').append('<option value="cash/Card">Cash/Card</option>');
                    $('#paymentType').append('<option value="ipcredit">IP Credit</option>');

                } else if (data[0].company_type == 2 && data[0].visit_status == 'OP') {
                    $('#paymentType').append('<option value="credit">Company Credit</option>');
                    $('#paymentType').append('<option value="cash/Card">Cash/Card</option>');

                } else if (data[0].company_type == 1 && data[0].visit_status == 'IP') {
                    $('#paymentType').append('<option value="cash/Card">Cash/Card</option>');
                    $('#paymentType').append('<option value="ipcredit">IP Credit</option>');

                } else if (data[0].company_type == 1 && data[0].visit_status == 'OP') {
                    $('#paymentType').append('<option value="cash/Card">Cash/Card</option>');

                }
                $('#paymentType').val($('#paymentType').find('option:first').val());
            }
        },
        complete: function() {
            changePricingId();
            sumFunction();
            var paytype = '';
            paytype = $('#hidden_payment_type').val();
            if (paytype && $("#company").is(":disabled")) {
                // if (paytype == 'insurance') {
                //     paytype = 3;
                // } else if (paytype == 'cash/Card') {
                //     paytype = 1;
                // } else if (paytype == 'credit') {
                //     paytype = 4;
                // } else if (paytype == 'ipcredit') {
                //     paytype = 4;
                // }

                $('#paymentType').val(paytype);
            }
            // var insurance = $('#insured_status').val();
            // if (insurance == 1) {
            //     $('#paymentType').val(3);
            // } else {
            //     $('#paymentType').val(1);
            // }

            // var package_id = $('#edit_package_id').val();
            // if (package_id > 0) {
            //     $('#billPackage').prop('checked', true);
            //     packageLIst();
            // } else {
            //     var bill_tag = $('#search_bill_tag').val();
            // }
            discountAdd();
        }
    });
}

function clearamount() {
    $('#discount_val').val(' ');
    $('#totalTax').val(' ');
    $('#billAmount').val(' ');
    $('#itemTotal').val(' ');
    $('#dicAmnt').val(' ');
    $('#patientPayable').val(' ');
    $('#payorPayable').val(' ');
    $('#netAmount').val(' ');
}






function editpackageDetails() {
    var base_url = $('#base_url').val();
    var url = base_url + "/service_bill/getPkgeDetails";
    var bill_head_id = $('#edit_head_id').val();
    var package_id = $('#edit_package_id').val();

    param = { bill_head_id: bill_head_id, package_id: package_id };
    if (bill_head_id && package_id > 0) {
        $.ajax({
            type: "post",
            url: url,
            data: param,
            beforeSend: function() {

            },
            success: function(data) {
                $('.package_select:selected').attr('disabled', true);
                $('#addPack_' + data[0].package_id).prop('checked', true);
                $('#addPack_' + data[0].package_id).attr('disabled', false);
                $('#billAmount').val(data[0].net_amount_wo_roundoff);
                $('#itemTotal').val(data[0].net_amount_wo_roundoff);
                $('#patientPayable').val(data[0].net_amount_wo_roundoff);
                $('#netAmount').val(data[0].net_amount_wo_roundoff);
                $('.package_select').attr('disabled', true);

                // $(".package_select").click(function() {

                //     if ($(".package_select").is(':checked')) {
                //         $('.package_select').attr('disabled', true);
                //         $('#addPack_' + data[0].package_id).attr('disabled', false);

                //     } else {
                //         $('.package_select').attr('disabled', false);
                //     }
                // })



                pkgpopupDiv(data[0].package_id);

            },
            complete: function() {

                sumFunction();
                paymentType();

            }


        });
    }

}

function Docfromout() {
    if ($('#DocFrmOut').is(':checked')) {
        $('#docFrmOutside').prop('readonly', false);
    } else {
        $('#docFrmOutside').prop('readonly', true);



    }
}

$("#item_search_btn").click(function() {
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
});

function searchbyName() {
    var input, filter, text
    $(".search_service_class").css("display", "none");
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();

    if (filter) {
        $(".search_service_class:contains(" + filter + ")").css("display", "block");
    } else {
        $(".search_service_class").css("display", "block");
    }
}

function printAndSave() {
    $('#printAndSave').attr('disabled', true);
    var printBillId = $('#print_bill_id').val();
    var bill_tag = $('#search_bill_tag').val();
    if (!printBillId && !bill_tag) {
        toastr.warning('Please select bill....');
        return;
    }
    $('#print_status').val(1);
    SaveDataJson();
}

function PrintBill() {

    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
    var printBillId = $('#print_bill_id').val();
    var bill_tag = $('#search_bill_tag').val();
    var package = $('#package_id').val() ? $('#package_id').val() : 0;
    var is_duplicate = $("#duplicate").prop('checked') ? '(DUPLICATE)' : '';
    var include_hospital_header = $("#showTitle").prop('checked') ? 1 : 0;
    var param = { is_duplicate: is_duplicate, include_hospital_header: include_hospital_header, package: package, printBillId: printBillId, location_id: location_id, location_name: location_name, bill_tag: bill_tag };
    var base_url = $('#base_url').val();
    if (!printBillId && !bill_tag) {
        toastr.warning('Please select bill....');
        return;
    }
    var url = base_url + "/service_bill/print_bill_detail";
    $.ajax({
        type: "post",
        url: url,
        data: param,
        beforeSend: function() {

            // $("#searchlist_div").LoadingOverlay("show", {
            //     background: "rgba(255, 255, 255, 0.7)",
            //     imageColor: "#337AB7",
            // })
        },
        success: function(response) {

            var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write('<style>@page{size:A5 landscape;} .patient_head{ font-size:10px !important; } </style>' + response + '<script>setTimeout(function(){window.print();window.close();},1000)</script>');
            //window.close();

        },
        complete: function() {
            // $('#print_status').val('');
            // $('#print_bill_id').val(' ');
            // $('#search_bill_tag').val(' ');
            // $('#package_id').val(' ');
            // $("#searchlist_div").LoadingOverlay("hide");
        },
    });
}

function convert_bill_details() {
    if ($('.serv_list').is(':checked')) {
        toastr.warning('Please Remove items');
    } else {
        if ($('.convert').is(':checked')) {
            var base_url = $('#base_url').val();
            var url = base_url + "/service_bill/convertbill";
            var convert = {};
            convert.head_id = [];
            var serv_ids = [];
            var detail_ids=[];

            var ctype = "";
            $('.convert:checked').each(function() {
                convert.ctype = $(this).attr('data-investigation_type');
                id = $(this).attr('data-head_id');
                convert.head_id.push(id);
                convert.convert_uhid = $(this).attr('data-uhid');

            });
            $('#search_bill_tag').val(convert.ctype);
            $('#convert_head_id').val(1);
            $('#patient_uhid').val(convert.convert_uhid);
            $('#convert_head').val(convert.head_id);


            var param = { convert: convert }

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    //    getBillData();

                },
                success: function(data) {
                    getBillData(data);
                    console.log(data.service_list);
                    for (i = 0; i < data.service_list.length; i++) {
                        console.log(data.service_list[i].id);
                        serv_ids.push(data.service_list[i].id);
                        detail_ids.push(data.service_list[i].detail_id);
                        


                    }
                    $('#convert_head_id').val(serv_ids);
                    $('#hidden_convert_detail_ids').val(detail_ids);

                },
                complete: function() {
                    $('#patientinvestigation-lg').modal('hide');

                    // patientInvestiGationList();
                    // clearInvestigation();
                }

            });
        } else {

            toastr.warning('Please Select Details');


        }
    }



}

// function checkConvertItem() {
//     var convert_id = $('#convert_head_id').val();
//     var base_url = $('#base_url').val();
//     var url = base_url + "/service_bill/checkPatientInvestigation";
//     var param = { convert_id: convert_id }
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: param,
//         beforeSend: function() {},
//         success: function(data) {
//             // console.log(data[0]);
//             // console.log(data.length);
//             for (i = 0; i < data.length; i++) {
//                 $("#add_" + data[i].id).prop('checked', true);
//                 // console.log(data[i].id);
//             }
//             // console.log(data);
//         },
//         complete: function() {

//             sumFunction();
//         }


//     });


// }




$(document).on("click", "#out_side_patient", function(e) {
    if ($("#out_side_patient").prop("checked")) {
        showOutsidePatientRegForm();
    }
});

function outsidePatientRegistrationSuccess(patient_details) {

    if (patient_details.external_doctor_check == 1) {
        $("#DocFrmOut").prop("checked", true);
        $("#docFrmOutside").val(patient_details.doctor_name);
        $("#DocFrmOut").attr('disabled', true);
    } else {
        $("#DocFrmOut").prop("checked", false);
        $("#docFrmOutside").val(patient_details.doctor_name);
        $("#DocFrmOut").attr('disabled', false);
    }
    $('#patient_uhid').val(patient_details.uhid);
    $('#patient_uhid_hidden').val(patient_details.uhid);
    $('#patient_name').val(patient_details.patient_name);
    getBillData();

}
// $('.close').click(function() {
//     $('#out_side_patient').prop('checked', false);

// })


function paymentCompleted() {
    // addNewBill();
    // $("#PayNow").hide();
    $('#serv_print_config_modal').modal('toggle');
}
$('#modalCashReciveBody').on('hidden.bs.modal', function() {
    window.location.reload();
})
$('#serv_print_config_modal').on('hidden.bs.modal', function() {
    $('#print_status').val('');
    $('#print_bill_id').val(' ');
    $('#search_bill_tag').val(' ');
    $('#package_id').val(' ');
    window.location.reload();
})
function selectedPatientFromAdvanceSearch(patient_details){ 
    getBillData();
}
$('#modalCashRecive').on('hidden.bs.modal', function() {
    window.location.reload();
})