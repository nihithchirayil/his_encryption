function addNewInvResultEntry(){
    var table = document.getElementById("investigation_result_entry_tbody");
    let row_count = table.rows.length;
    let row_id = row_count + 1;
    var last_text_boxValue = $('#investigation_result_entry_tbody tr:last input[name="investiagation_entry_date[]"]').val();
    if (!last_text_boxValue) {
        last_text_boxValue = '';
    }
    var row = table.insertRow(row_count);
    row.setAttribute("row-id", row_id, 0);

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);


    cell1.innerHTML = '<input type="textbox" name="investiagation_entry_date[]" id="investiagation_entry_date_' + row_id + '" value="'+ last_text_boxValue +'" class="form-control bottom-border-text borderless_textbox datepicker"/>';
    cell2.innerHTML = '<input type="textbox" name="investiagation_entry[]" id="investiagation_entry_' + row_id + '" class="form-control bottom-border-text borderless_textbox" autocomplete="off"/>';
    cell3.innerHTML = '<input type="textbox" name="investiagation_entry_result[]" id="investiagation_entry_result_' + row_id + '" class="form-control bottom-border-text borderless_textbox" autocomplete="off"/>';
    cell4.innerHTML = '<i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>';

    $(".datepicker").datetimepicker({
        format: 'MMM-DD-YYYY'
    });

}

function removeInvResultEntry(obj){
    $(obj).parents('tr').remove();
}

function saveInvestigationResultEntry(){
    var patient_id = $('#patient_id').val();
    var visit_id = $('#visit_id').val();
    var doctor_id = $('#doctor_id').val();
    if(patient_id ==''){
        toastr.warning("Select patient!");
        return false;
    }
    var investiagation_entry_date = [];
    var investiagation_entry = [];
    var investiagation_entry_result = [];

    var table = document.getElementById("investigation_result_entry_tbody");
    let row_count = table.rows.length;

    for(let i=1;i<=row_count;i++){
        if($('#investiagation_entry_date_'+i).val()!=''
            || $('#investiagation_entry_'+i).val()!=''
            || $('#investiagation_entry_result_'+i).val()!=''
        )
        {
            investiagation_entry_date.push($('#investiagation_entry_date_'+i).val());
            investiagation_entry.push($('#investiagation_entry_'+i).val());
            investiagation_entry_result.push($('#investiagation_entry_result_'+i).val());
        }
    }

    var dataparams = {
        patient_id:patient_id,
        visit_id:visit_id,
        doctor_id:doctor_id,
        investiagation_entry_date:investiagation_entry_date,
        investiagation_entry:investiagation_entry,
        investiagation_entry_result:investiagation_entry_result,
        row_count:row_count,
    }

    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/saveInvestigationResultEntry";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $(".datepicker").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            // $('#investigation_result_entry_tbody').html('');
            fetchInvestigationResultEntry();
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}

//----for history tab---------------------------------
function fetchInvestigationResultEntry(){
    var visit_id = $('#visit_id').val();
    var patient_id = $('#patient_id').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/fetchInvestigationResultEntry";
    var dataparams = {
        visit_id:visit_id,
        patient_id:patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("#inv_res_entry_his_container").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if(data!=0){
                $("#inv_res_entry_his_container").html(data);
            }
        },
        complete: function () {
            $("#inv_res_entry_his_container").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            $(".datepicker").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
          fetchPatientLocalDetails(patient_id) ;
        
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}


function fetchPatientInvResultEntry(){
    var visit_id = $('#visit_id').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/fetchPatientInvResultEntry";
    var dataparams = {
        visit_id:visit_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
        },
        success: function (data) {
            //console.log(data);
            if(data!=0){
                $("#investigation_result_entry_tbody").html(data);
            }
        },
        complete: function () {
            $("#inv_res_entry_his_container").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            $(".datepicker").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            fetchInvestigationResultEntry();
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}


//// FOR NURSING MODULE  
function fetchPatientInvResultEntryNursing() {
    var visit_id = $('#poc_visit_id').val();
    var base_url = $("#base_url").val();
    var url = base_url + "/nursing_new/fetchPatientInvestigationResultEntryNs";
    var dataparams = {visit_id:visit_id};
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $("#patient_investigation_result_min").modal({
                backdrop: 'static',
                keyboard: false
            });     
            $("#patient_investigation_result_min_content").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });          
        },
        success: function (data) {
            $("#patient_investigation_result_min_content").html(data); 
        },
        complete: function () {
            $("#patient_investigation_result_min_content").LoadingOverlay("hide");
            $(".datepicker_poc").datetimepicker({
                format: 'MMM-DD-YYYY'
            }); 
            setTimeout(function () {
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30    
                });              
            }, 100);
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });
}

function saveInvestigationResultEntryNs() {
    var patient_id = $('#poc_patient_id').val();
    var visit_id = $('#poc_visit_id').val();
    var doctor_id = $('#poc_doctor_id').val();
    if(patient_id == ''){
        toastr.warning("Select patient!");
        return false;
    }

    var investiagation_entry_date = [];
    var investiagation_entry = [];
    var investiagation_entry_result = [];

    var table = document.getElementById("investigation_result_entry_tbody");
    let row_count = table.rows.length;

    for(let i=1; i<=row_count; i++) {
        if($('#investiagation_entry_date_'+i).val()!=''
            || $('#investiagation_entry_'+i).val()!=''
            || $('#investiagation_entry_result_'+i).val()!=''
        )
        {
            investiagation_entry_date.push($('#investiagation_entry_date_'+i).val());
            investiagation_entry.push($('#investiagation_entry_'+i).val());
            investiagation_entry_result.push($('#investiagation_entry_result_'+i).val());
        }
    }

    var dataparams = {
        patient_id:patient_id,
        visit_id:visit_id,
        doctor_id:doctor_id,
        investiagation_entry_date:investiagation_entry_date,
        investiagation_entry:investiagation_entry,
        investiagation_entry_result:investiagation_entry_result,
        row_count:row_count,
    }

    var base_url = $("#base_url").val();
    var url = base_url + "/emr_lite/saveInvestigationResultEntry";
    $.ajax({
        type: "POST",
        url: url,
        data:dataparams,
        beforeSend: function () {
            $('#save_invs_result_btn').find('i').addClass('fa fa-spinner fa-spin').removeClass('fa-save');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('POC added.');
                $('#patient_investigation_result_min').modal('hide');
            }
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('#save_invs_result_btn').find('i').addClass('fa-save').removeClass('fa-spinner fa-spin');
            $(".datepicker_poc").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
        },
        error: function () {
            toastr.error("Error please check your internet connection");
        },
    });

}

function addNewInvResultEntryNs(){
    var table = document.getElementById("investigation_result_entry_tbody");
    let row_count = table.rows.length;
    let row_id = row_count + 1;
    var last_text_boxValue = $('#investigation_result_entry_tbody tr:last input[name="investiagation_entry_date[]"]').val();
    if (!last_text_boxValue) {
        last_text_boxValue = '';
    }
    var row = table.insertRow(row_count);
    row.setAttribute("row-id", row_id, 0);

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = '<input type="textbox" name="investiagation_entry_date[]" id="investiagation_entry_date_' + row_id + '" value="'+ last_text_boxValue +'" class="form-control bottom-border-text borderless_textbox datepicker_poc"/>';
    cell2.innerHTML = '<input type="textbox" name="investiagation_entry[]" id="investiagation_entry_' + row_id + '" class="form-control bottom-border-text borderless_textbox" autocomplete="off"/>';
    cell3.innerHTML = '<input type="textbox" name="investiagation_entry_result[]" id="investiagation_entry_result_' + row_id + '" class="form-control bottom-border-text borderless_textbox" autocomplete="off"/>';
    cell4.innerHTML = '<i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>';

    $(".datepicker_poc").datetimepicker({
        format: 'MMM-DD-YYYY'
    });

}
