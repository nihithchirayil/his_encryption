$('#surgery_checklist_modal').on('shown.bs.modal', function () {
    $(function () {
        $("#progressbar li:first-child a").tab("show");
    });
    $(document).on("click", "#progressbar li", function (e) {
        $(this).find('a').tab('show');
    });
    $('.set_date_picker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    if ($("#op_vitals_tbody tr").length == 0) {
        addRowOPVital();
    }
    if ($("#op_pack_tbody tr").length == 0) {
        addRowInNursing();
    }

})
var op_vital_row_id = 1;
var op_nursing_row_id = 1;


var base_url = $('#base_url').val()
var token = $('#c_token').val()

function validate(from_type) {
    $(".nav-tabs > .active").next("li").find("a").tab("show");
}

function getPreviousTab() {
    $(".nav-tabs > .active").prev("li").find("a").tab("show");
}

function getPatientChecklist(head_id, surgery_id, surgery_detail_id, patient_id) {
    var url = base_url + "/ot_checklists/getOtChecklist";
    $('#head_id').val(head_id);
    $('#surgery_id_hidden').val(surgery_id);
    $('#surgery_detail_id_hidden').val(surgery_detail_id);
    $('#patient_id_hidden').val(patient_id);
    $('#list_type').val(1);
    $.ajax({
        url: url,
        type: "POST",
        data: { head_id: head_id, surgery_detail_id: surgery_detail_id, patient_id: patient_id },
        beforeSend: function () {
            $('.checklist_btn').attr('disabled', true);
            $('#patient_checklist_spin' + head_id + surgery_id).removeClass('fa fa-check-square-o');
            $('#patient_checklist_spin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');

            $('#save_checklistbtn').show();
            $('#save_ans_checklistbtn').hide();
            $('#save_operation_notes_btn').hide();

        },
        success: function (data) {
            var obj = JSON.parse(data);
            //console.log(obj);
            $('#surgery_checklist').html(obj.checklist_data);

            $('#parameters_approved_status').val(obj.parameters_approved_status);
            $('#pre_op_approved_status').val(obj.pre_op_approved_status);
            $('#sign_in_approved_status').val(obj.sign_in_approved_status);
            $('#signout_approved_status').val(obj.signout_approved_status);
            $('#timeout_approved_status').val(obj.timeout_approved_status);
            $('#nursing_record_approved_status').val(obj.nursing_record_approved_status);
            $('#signout_approved_status').val(obj.signout_approved_status);

            $('.modal-body').css('min-height', '520px');
            $('#checkListDataHeader').html(obj.patient_title + ' ' + obj.patient_name + ' ( ' + obj.patient_uhid + ' ) ');
            $("#surgery_checklist_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

            //-----if checklist approved then remove save btn-----------------
            var parameters_approved_status = $('#parameters_approved_status').val();
            var pre_op_approved_status = $('#pre_op_approved_status').val();
            var sign_in_approved_status = $('#sign_in_approved_status').val();
            var timeout_approved_status = $('#timeout_approved_status').val();
            var nursing_record_approved_status = $('#nursing_record_approved_status').val();
            var signout_approved_status = $('#signout_approved_status').val();
            var surgery_approval_access = $('#surgery_approval_access').val();



            if(parameters_approved_status == 1){
                $('.btn_approve_parameters').css("display", "none");
            }
            if(pre_op_approved_status == 1){
                $('.btn_approve_op_vital').css("display", "none");
            }
            if(sign_in_approved_status == 1){
                $('.btn_approve_signin').css("display", "none");
            }
            if(timeout_approved_status == 1){
                $('.btn_approve_timeout').css("display", "none");
            }
            if(nursing_record_approved_status == 1){
                $('.btn_approve_nursing_record').css("display", "none");
            }
            if(signout_approved_status == 1){
                $('.btn_approve_signout').css("display", "none");
            }




            if (parameters_approved_status == 1 && surgery_approval_access == 1) {
                $('.btn_save_parameters').css("display", "block");
            } else if (parameters_approved_status != 1 && surgery_approval_access == 1) {
                $('.btn_save_parameters').css("display", "block");
                $('.btn_approve_parameters').css("display", "block");
            } else if (parameters_approved_status == 1 && surgery_approval_access == 0) {
                $('.btn_save_parameters').css("display", "none");
                $('.btn_approve_parameters').css("display", "none");
            }


            if (pre_op_approved_status == 1 && surgery_approval_access == 1) {
                $('.btn_save_op_vital').css("display", "block");
                $('.btn_approve_op_vital').css("display", "block");
            } else if (pre_op_approved_status != 1 && surgery_approval_access == 1) {
                $('.btn_save_op_vital').css("display", "block");
                $('.btn_approve_op_vital').css("display", "block");
            } else if (pre_op_approved_status == 1 && surgery_approval_access == 0) {
                $('.btn_save_op_vital').css("display", "none");
                $('.btn_approve_op_vital').css("display", "none");
            }

            if (sign_in_approved_status == 1 && surgery_approval_access == 1) {
                $('.btn_save_signin').css("display", "block");
                $('.btn_approve_signin').css("display", "block");
            } else if (sign_in_approved_status != 1 && surgery_approval_access == 1) {
                $('.btn_save_signin').css("display", "block");
                $('.btn_approve_signin').css("display", "block");
            } else if (sign_in_approved_status == 1 && surgery_approval_access == 0) {
                $('.btn_save_signin').css("display", "none");
                $('.btn_approve_signin').css("display", "none");
            }

            if (timeout_approved_status == 1 && surgery_approval_access == 1) {
                $('.btn_save_timeout').css("display", "block");
                $('.btn_approve_timeout').css("display", "block");
            } else if (timeout_approved_status != 1 && surgery_approval_access == 1) {
                $('.btn_save_timeout').css("display", "block");
                $('.btn_approve_timeout').css("display", "block");
            } else if (timeout_approved_status == 1 && surgery_approval_access == 0) {
                $('.btn_save_timeout').css("display", "none");
                $('.btn_approve_timeout').css("display", "none");
            }

            if (nursing_record_approved_status == 1 && surgery_approval_access == 1) {
                $('.btn_save_nursing_record').css("display", "block");
                $('.btn_approve_nursing_record').css("display", "block");
            } else if (nursing_record_approved_status != 1 && surgery_approval_access == 1) {
                $('.btn_save_nursing_record').css("display", "block");
                $('.btn_approve_nursing_record').css("display", "block");
            } else if (nursing_record_approved_status == 1 && surgery_approval_access == 0) {
                $('.btn_save_nursing_record').css("display", "none");
                $('.btn_approve_nursing_record').css("display", "none");
            }

            if (signout_approved_status == 1 && surgery_approval_access == 1) {
                $('.btn_save_signout').css("display", "block");
                $('.btn_approve_signout').css("display", "block");
            } else if (signout_approved_status != 1 && surgery_approval_access == 1) {
                $('.btn_save_signout').css("display", "block");
                $('.btn_approve_signout').css("display", "block");
            } else if (signout_approved_status == 1 && surgery_approval_access == 0) {
                $('.btn_save_signout').css("display", "none");
                $('.btn_approve_signout').css("display", "none");
            }


            $('.checklist_btn').attr('disabled', false);
            $('#patient_checklist_spin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#patient_checklist_spin' + head_id + surgery_id).addClass('fa fa-check-square-o');

            fetchSurgeryDetails(obj.surgery_checklist_data);
        },
        complete: function () {



            $(".number_only").on("input", function(evt) {
                var self = $(this);
                self.val(self.val().replace(/[^0-9\.]/g, ''));
                if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
                {
                  evt.preventDefault();
                }
              });

            $(document).on("input", ".number_only", function() {
                this.value = this.value.match(/^\d+\.?\d{0,2}/);});








            $('.select2').select2();
        },
    });
}


function fetchSurgeryDetails(surgery_checklist_data) {
    $('.select2').select2();
    //----fetching parameters data--------------------------------------------------------
    if(surgery_checklist_data.length >0){


        var parameters_data = surgery_checklist_data[0]['parameters_data'];
        parameters_data = JSON.parse(parameters_data);

        if(parameters_data != null){
            if (typeof parameters_data.vital_sign_recorded !== 'undefined') {
                $("input[name=vital_sign_recorded][value=" + parameters_data.vital_sign_recorded + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.name_of_procedure !== 'undefined') {
                $("input[name=name_of_procedure][value=" + parameters_data.name_of_procedure + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.name_of_procedure_text !== 'undefined') {
                $('#name_of_procedure_text').val(parameters_data.name_of_procedure_text);
            }
            if (typeof parameters_data.nbm_time !== 'undefined') {
                $('#nbm_time').val(parameters_data.nbm_time);
            }
            if (typeof parameters_data.nbm !== 'undefined') {
                $("input[name=nbm][value=" + parameters_data.nbm + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.am_care !== 'undefined') {
                $("input[name=am_care][value=" + parameters_data.am_care + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.am_care_status !== 'undefined') {
                $("input[name=am_care_status][value=" + parameters_data.am_care_status + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.urinary_output_time !== 'undefined') {
                $('#urinary_output_time').val(parameters_data.urinary_output_time);
            }
            if (typeof parameters_data.on_catheter !== 'undefined' && parameters_data.on_catheter == 1) {
                $("input[name=on_catheter]").attr('checked', 'checked');
            }
            if (typeof parameters_data.urine_amount !== 'undefined' && parameters_data.urine_amount != '') {
                $('#urine_amount').val(parameters_data.urine_amount);
            }

            if (typeof parameters_data.urinary_output_status !== 'undefined') {
                $("input[name=urinary_output_status][value=" + parameters_data.urinary_output_status + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.bowl_wash !== 'undefined' && parameters_data.bowl_wash == 1) {
                $("input[name=bowl_wash]").attr('checked', 'checked');
            }
            if (typeof parameters_data.laxatives !== 'undefined' && parameters_data.laxatives == 1) {
                $("input[name=laxatives]").attr('checked', 'checked');
            }
            if (typeof parameters_data.stool_passed !== 'undefined' && parameters_data.stool_passed == 1) {
                $("input[name=stool_passed]").attr('checked', 'checked');
            }
            if (typeof parameters_data.enema_status !== 'undefined') {
                $("input[name=enema_status][value=" + parameters_data.enema_status + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.skin_preparation !== 'undefined') {
                $("input[name=skin_preparation][value=" + parameters_data.skin_preparation + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.care_hair_nail !== 'undefined') {
                $("input[name=care_hair_nail][value=" + parameters_data.care_hair_nail + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.care_hair_nail !== 'undefined') {
                $("input[name=care_hair_nail][value=" + parameters_data.care_hair_nail + "]").attr('checked', 'checked');
            }

            if (typeof parameters_data.pre_op_medication_time !== 'undefined' && parameters_data.pre_op_medication_time != '') {
                $('#pre_op_medication_time').val(parameters_data.pre_op_medication_time);
            }
            if (typeof parameters_data.pre_op_medication_status !== 'undefined') {
                $("input[name=pre_op_medication_status][value=" + parameters_data.pre_op_medication_status + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.medication_profile_chart !== 'undefined') {
                $("input[name=medication_profile_chart][value=" + parameters_data.medication_profile_chart + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.allergies !== 'undefined') {
                $("input[name=allergies][value=" + parameters_data.allergies + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.required_blood_group !== 'undefined' && parameters_data.required_blood_group != '') {
                $('#required_blood_group').val(parameters_data.required_blood_group);
            }
            if (typeof parameters_data.covid_test_report_status !== 'undefined') {
                $("input[name=covid_test_report_status][value=" + parameters_data.covid_test_report_status + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.serology !== 'undefined') {
                $("input[name=serology][value=" + parameters_data.serology + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.relevant_investigation !== 'undefined') {
                $("input[name=relevant_investigation][value=" + parameters_data.relevant_investigation + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.contact_lenses !== 'undefined') {
                $("input[name=contact_lenses][value=" + parameters_data.contact_lenses + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.dentures_implants !== 'undefined') {
                $("input[name=dentures_implants][value=" + parameters_data.dentures_implants + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.jewellery_removed !== 'undefined') {
                $("input[name=jewellery_removed][value=" + parameters_data.jewellery_removed + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.consent !== 'undefined') {
                $("input[name=consent][value=" + parameters_data.consent + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.high_risk_consent !== 'undefined') {
                $("input[name=high_risk_consent][value=" + parameters_data.high_risk_consent + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.anaesthesia_pac !== 'undefined') {
                $("input[name=anaesthesia_pac][value=" + parameters_data.anaesthesia_pac + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.consultation_medical_fitness !== 'undefined') {
                $("input[name=consultation_medical_fitness][value=" + parameters_data.consultation_medical_fitness + "]").attr('checked', 'checked');
            }
            if (typeof parameters_data.marking_of_site !== 'undefined') {
                $("input[name=marking_of_site][value=" + parameters_data.marking_of_site + "]").attr('checked', 'checked');
            }

            if (typeof parameters_data.parameters_remarks !== 'undefined') {
                $("#parameters_remarks").val(parameters_data.parameters_remarks);
            }

            if (typeof parameters_data.handover_given_by !== 'undefined') {
                $("#handover_given_by").val(parameters_data.handover_given_by).select2();
            }
            if (typeof parameters_data.handover_taken_by !== 'undefined') {
                $("#handover_taken_by").val(parameters_data.handover_taken_by).select2();
            }
        }

        //console.log(surgery_checklist_data);

        //----fetching pre op vitals data--------------------------------------------------------

        var pre_op_vitals_data = surgery_checklist_data[0]['pre_op_vitals_data'];
        pre_op_vitals_data = JSON.parse(pre_op_vitals_data);
        if(pre_op_vitals_data != null){
            if(typeof pre_op_vitals_data['pre_operative_notes'] !== 'undefined'){
                $('#pre_operative_notes').val(pre_op_vitals_data['pre_operative_notes']);
            }
            $('#op_vital_handover_given_by').val(pre_op_vitals_data['op_vital_handover_given_by']).select2();
            $('#op_vital_handover_taken_by').val(pre_op_vitals_data['op_vital_handover_taken_by']).select2();

            pre_op_vitals_data = pre_op_vitals_data['op_vitals_params'];
            var j = 1;
            for (var i = 0; i < pre_op_vitals_data.length; i++) {

                $('#op_vitals_tbody').append('<tr class="vital_class" id="' + j + '_row_idvital" ><td class="common_td_rules "><input onchange="checkTDIsLast(' + j + ',\'vital\')" type="text" class="form-control timepicker" name="time_op" onclick="" value="' + pre_op_vitals_data[i].time_op + '"></td><td class="common_td_rules"><input type="text" class="form-control number_only op ' + j + 'vital" onclick="checkTDIsLast(' + j + ',\'vital\')" name="temp_op" value="' + pre_op_vitals_data[i].temp_op + '"></td><td class="common_td_rules"><input onclick="" class="form-control number_only  op' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].pulse_op + '" name="pulse_op"></td><td class="common_td_rules"><input onclick="" class="form-control number_only op' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].resp_op + '" name="resp_op"></td><td class="common_td_rules"><input onclick="" class="form-control op ' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].bp_op + '" name="bp_op"></td><td class="common_td_rules"><input onclick="" class="form-control number_only op' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].spo2_op + '" name="spo2_op"></td> <td class="common_td_rules"><input onclick="" class="form-control number_only op' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].rbs_op + '" name="rbs_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].pain_op + '" name="pain_op"></td> <td class="common_td_rules"><input onclick="" class="form-control op' + j + 'vital" type="text" value="' + pre_op_vitals_data[i].op_remark + '" name="op_remark"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + j + ',\'vital\')"><i class="fa fa-trash red" ></i></button></td></tr>');
                j++;

            }
        }

        //----fetching sign in data-----------------------------------------------------

        var sign_in_data = surgery_checklist_data[0]['sign_in_data'];
        sign_in_data = JSON.parse(sign_in_data);

        if(sign_in_data != null){
            if (typeof sign_in_data.patient_identity_verified !== 'undefined') {
                $("input[name=patient_identity_verified][value=" + sign_in_data.patient_identity_verified + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.correct_procedure !== 'undefined') {
                $("input[name=correct_procedure][value=" + sign_in_data.correct_procedure + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.site_marked !== 'undefined') {
                $("input[name=site_marked][value=" + sign_in_data.site_marked + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.availability_of_required_medical !== 'undefined') {
                $("input[name=availability_of_required_medical][value=" + sign_in_data.availability_of_required_medical + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.availability_and_functionality_of_required !== 'undefined') {
                $("input[name=availability_and_functionality_of_required][value=" + sign_in_data.availability_and_functionality_of_required + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.anesthesia_safety_check_completed !== 'undefined') {
                $("input[name=anesthesia_safety_check_completed][value=" + sign_in_data.anesthesia_safety_check_completed + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.difficlty_airways !== 'undefined') {
                $("input[name=difficlty_airways][value=" + sign_in_data.difficlty_airways + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.blood_product_reserved !== 'undefined') {
                $("input[name=blood_product_reserved][value=" + sign_in_data.blood_product_reserved + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.risk_of_blood_loss !== 'undefined') {
                $("input[name=risk_of_blood_loss][value=" + sign_in_data.risk_of_blood_loss + "]").attr('checked', 'checked');
            }
            if (typeof sign_in_data.antibiotic_prophylaxis !== 'undefined') {
                $("input[name=antibiotic_prophylaxis][value=" + sign_in_data.antibiotic_prophylaxis + "]").attr('checked', 'checked');
            }

            if (typeof sign_in_data.antibiotic_prophylaxis_drug_name !== 'undefined' && sign_in_data.               antibiotic_prophylaxis_drug_name != '') {
                $("#antibiotic_prophylaxis_drug_name").val(sign_in_data.antibiotic_prophylaxis_drug_name);
            }
            if (typeof sign_in_data.antibiotic_given_time !== 'undefined' && sign_in_data.antibiotic_given_time != '') {
                $("#antibiotic_given_time").val(sign_in_data.antibiotic_given_time);
            }
            if (typeof sign_in_data.signin_completed_by !== 'undefined' && sign_in_data.signin_completed_by != '') {
                $("#signin_completed_by").val(sign_in_data.signin_completed_by).select2();
            }
            if (typeof sign_in_data.signin_completed_at !== 'undefined' && sign_in_data.signin_completed_at != '') {
                $("#signin_completed_at").val(sign_in_data.signin_completed_at);
            }
        }

        //-----fetching timeout data---------------------------
        var time_out_data = surgery_checklist_data[0]['time_out_data'];
        time_out_data = JSON.parse(time_out_data);

        if(time_out_data != null){
            if (typeof time_out_data.correct_patient !== 'undefined') {
                $("input[name=correct_patient][value=" + time_out_data.correct_patient + "]").attr('checked', 'checked');
            }
            if (typeof time_out_data.correct_site_and_side !== 'undefined') {
                $("input[name=correct_site_and_side][value=" + time_out_data.correct_site_and_side + "]").attr('checked', 'checked');
            }
            if (typeof time_out_data.agreement_on_procedure !== 'undefined') {
                $("input[name=agreement_on_procedure][value=" + time_out_data.agreement_on_procedure + "]").attr('checked', 'checked');
            }
            if (typeof time_out_data.availability_of_implants !== 'undefined') {
                $("input[name=availability_of_implants][value=" + time_out_data.availability_of_implants + "]").attr('checked', 'checked');
            }
            if (typeof time_out_data.relevant_investigation_and_images !== 'undefined') {
                $("input[name=relevant_investigation_and_images][value=" + time_out_data.relevant_investigation_and_images + "]").attr('checked', 'checked');
            }
            if (typeof time_out_data.pre_counts_completed !== 'undefined') {
                $("input[name=pre_counts_completed][value=" + time_out_data.pre_counts_completed + "]").attr('checked', 'checked');
            }

            if (typeof time_out_data.timeout_surgeon !== 'undefined' && time_out_data.timeout_surgeon != '') {
                $("#timeout_surgeon").val(time_out_data.timeout_surgeon);
            }
            if (typeof time_out_data.timeout_anaesthetist !== 'undefined' && time_out_data.timeout_anaesthetist != '') {
                $("#timeout_anaesthetist").val(time_out_data.timeout_anaesthetist);
            }
            if (typeof time_out_data.timeout_scrub_nurse !== 'undefined' && time_out_data.timeout_scrub_nurse != '') {
                $("#timeout_scrub_nurse").val(time_out_data.timeout_scrub_nurse).select2();
            }
            if (typeof time_out_data.timeout_nurse !== 'undefined' && time_out_data.timeout_nurse != '') {
                $("#timeout_nurse").val(time_out_data.timeout_nurse).select2();
            }
            if (typeof time_out_data.timeout_completed_by !== 'undefined' && time_out_data.timeout_completed_by != '') {
                $("#timeout_completed_by").val(time_out_data.timeout_completed_by).select2();
            }
            if (typeof time_out_data.timeout_completed_at !== 'undefined' && time_out_data.timeout_completed_at != '') {
                $("#timeout_completed_at").val(time_out_data.timeout_completed_at);
            }
            if (typeof time_out_data.timeout_technician !== 'undefined' && time_out_data.timeout_technician != '') {
                $("#timeout_technician").val(time_out_data.timeout_technician);
            }
            if (typeof time_out_data.timeout_others !== 'undefined' && time_out_data.timeout_others != '') {
                $("#timeout_others").val(time_out_data.timeout_others);
            }
        }


        //----fetching nursing record data------------------------------------------

        var nursing_record_data = surgery_checklist_data[0]['nursing_record_data'];
        nursing_record_data = JSON.parse(nursing_record_data);
        if(nursing_record_data != null){
            if (typeof nursing_record_data.ns_record_procedure_name !== 'undefined' && nursing_record_data.ns_record_procedure_name != '') {
                $("#ns_record_procedure_name").val(nursing_record_data.ns_record_procedure_name);
            }
            if (typeof nursing_record_data.ns_record_procedure_start_time !== 'undefined' && nursing_record_data.ns_record_procedure_start_time != '') {
                $("#ns_record_procedure_start_time").val(nursing_record_data.ns_record_procedure_start_time);
            }
            if (typeof nursing_record_data.ns_record_procedure_end_time !== 'undefined' && nursing_record_data.ns_record_procedure_end_time != '') {
                $("#ns_record_procedure_end_time").val(nursing_record_data.ns_record_procedure_end_time);
            }
            if (typeof nursing_record_data.ns_record_specimen !== 'undefined' && nursing_record_data.ns_record_specimen != '') {
                $("#ns_record_specimen").val(nursing_record_data.ns_record_specimen);
            }
            if (typeof nursing_record_data.ns_record_lab !== 'undefined' && nursing_record_data.ns_record_lab != '') {
                $("#ns_record_lab").val(nursing_record_data.ns_record_lab);
            }
            if (typeof nursing_record_data.ns_record_no_specimen !== 'undefined' && nursing_record_data.ns_record_no_specimen != '') {
                $("#ns_record_no_specimen").val(nursing_record_data.ns_record_no_specimen);
            }
            if (typeof nursing_record_data.ns_record_collector !== 'undefined' && nursing_record_data.ns_record_collector != '') {
                $("#ns_record_collector").val(nursing_record_data.ns_record_collector);
            }
            if (typeof nursing_record_data.ns_implants !== 'undefined' && nursing_record_data.ns_implants != '') {
                $("#ns_implants").val(nursing_record_data.ns_implants);
            }
            var j = 1;
            console.log(nursing_record_data.op_pack);
            for (i = 0; i < nursing_record_data.op_pack.length; i++) {
                $('#op_pack_tbody').append('<tr class="vital_class" id="' + j + '_row_idnursing" ><td class="common_td_rules "><input onchange="checkTDIsLast(' + j + ',\'nursing\')" type="text" class="form-control " name="type_op" onclick="" value="' + nursing_record_data.op_pack[i].type_op + '"></td><td class="common_td_rules"><input onclick="" class="form-control op' + j + 'nursing" type="text" name="site_op" value="' + nursing_record_data.op_pack[i].site_op + '"></td><td class="common_td_rules"><input onclick="" class="form-control op' + j + 'nursing" type="text" value="' + nursing_record_data.op_pack[i].size_op + '" name="size_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + j + 'nursing" type="text" value="' + nursing_record_data.op_pack[i].amount_op + '" oninput="" name="amount_op"></td><td class="common_td_rules"><input onclick="" class="form-control datetimepicker op' + j + 'nursing" type="text" value="' + nursing_record_data.op_pack[i].date_time_op + '" name="date_time_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + j + 'nursing" type="text" value="' + nursing_record_data.op_pack[i].sign_op + '" name="sign_op"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + j + ',\'nursing\')"><i class="fa fa-trash red" ></i></button></td></tr>');
                j++;
                $('.timepicker').datetimepicker({
                    format: 'hh:mm A'
                });
                $('.datetimepicker').datetimepicker({
                    format: 'MMM-DD-YYYY hh:mm A'
                });
                j++;
            }
        }

        //----fetching sign out data------------------------------------------

        var sign_out_data = surgery_checklist_data[0]['sign_out_data'];
        sign_out_data = JSON.parse(sign_out_data);

        if(sign_out_data != null){
            $('#preSpong').val(sign_out_data.preSpong);
            $('#preGauze').val(sign_out_data.preGauze);
            $('#prePeanut_Gauze').val(sign_out_data.prePeanut_Gauze);
            $('#preRoller_Gauze').val(sign_out_data.preRoller_Gauze);
            $('#preVassel_Loop').val(sign_out_data.preVassel_Loop);
            $('#preInstrument').val(sign_out_data.preInstrument);
            $('#preNeedle').val(sign_out_data.preNeedle);
            $('#preCirculatory_Nurse').val(sign_out_data.preCirculatory_Nurse);
            $('#preScrub_Nurse').val(sign_out_data.preScrub_Nurse);
            $('#ReliSpong').val(sign_out_data.ReliSpong);
            $('#ReliGauze').val(sign_out_data.ReliGauze);
            $('#ReliPeanut_Gauze').val(sign_out_data.ReliPeanut_Gauze);
            $('#ReliRoller_Gauze').val(sign_out_data.ReliRoller_Gauze);
            $('#ReliVassel_Loop').val(sign_out_data.ReliVassel_Loop);
            $('#ReliInstrument').val(sign_out_data.ReliInstrument);
            $('#ReliNeedle').val(sign_out_data.ReliNeedle);
            $('#ReliCirculatory_Nurse').val(sign_out_data.ReliCirculatory_Nurse);
            $('#ReliScrub_Nurse').val(sign_out_data.ReliScrub_Nurse);
            $('#AtclSpong').val(sign_out_data.AtclSpong);
            $('#AtclGauze').val(sign_out_data.AtclGauze);
            $('#AtclPeanut_Gauze').val(sign_out_data.AtclPeanut_Gauze);
            $('#AtclRoller_Gauze').val(sign_out_data.AtclRoller_Gauze);
            $('#AtclVassel_Loop').val(sign_out_data.AtclVassel_Loop);
            $('#AtclInstrument').val(sign_out_data.AtclInstrument);
            $('#AtclNeedle').val(sign_out_data.AtclNeedle);
            $('#AtclCirculatory_Nurse').val(sign_out_data.AtclCirculatory_Nurse);
            $('#AtclScrub_Nurse').val(sign_out_data.AtclScrub_Nurse);
            $('#FinalSpong').val(sign_out_data.FinalSpong);
            $('#FinalGauze').val(sign_out_data.FinalGauze);
            $('#FinalPeanut_Gauze').val(sign_out_data.FinalPeanut_Gauze);
            $('#FinalRoller_Gauze').val(sign_out_data.FinalRoller_Gauze);
            $('#FinalVassel_Loop').val(sign_out_data.FinalVassel_Loop);
            $('#FinalInstrument').val(sign_out_data.FinalInstrument);
            $('#FinalNeedle').val(sign_out_data.FinalNeedle);
            $('#FinalCirculatory_Nurse').val(sign_out_data.FinalCirculatory_Nurse);
            $('#FinalScrub_Nurse').val(sign_out_data.FinalScrub_Nurse);


            if (typeof sign_out_data.count_correct !== 'undefined') {
                $("input[name=count_correct][value=" + sign_out_data.count_correct + "]").attr('checked', 'checked');
            }

            if (typeof sign_out_data.surgeon_inform !== 'undefined') {
                $("input[name=surgeon_inform][value=" + sign_out_data.surgeon_inform + "]").attr('checked', 'checked');
            }

            if (typeof sign_out_data.antibiotic_given_time != 'undefined' && sign_out_data.antibiotic_given_time != '') {
                $("#antibiotic_given_time").val(sign_out_data.antibiotic_given_time);
            }
            if (typeof sign_out_data.action_taken_remarks != 'undefined' && sign_out_data.action_taken_remarks != '') {
                $("#action_taken_remarks").val(sign_out_data.action_taken_remarks);
            }
            if (typeof sign_out_data.sign_out_post_notes != 'undefined' && sign_out_data.sign_out_post_notes != '') {
                $("#sign_out_post_notes").val(sign_out_data.sign_out_post_notes);
            }


            if (typeof sign_out_data.signout_handover_given_by != 'undefined' && sign_out_data.signout_handover_given_by != '') {
                $("#signout_handover_given_by").val(sign_out_data.signout_handover_given_by).select2();
            }

            if (typeof sign_out_data.signout_handover_taken_by != 'undefined' && sign_out_data.signout_handover_taken_by != '') {
                $("#signout_handover_taken_by").val(sign_out_data.signout_handover_taken_by).select2();
            }

            if (typeof sign_out_data.signout_handover_taken_at != 'undefined' && sign_out_data.signout_handover_taken_at != '') {
                $("#signout_handover_taken_at").val(sign_out_data.signout_handover_taken_at);
            }



            if (typeof sign_out_data.sign_out_antibiotic_given_time != 'undefined' && sign_out_data.sign_out_antibiotic_given_time != '') {
                $("#sign_out_antibiotic_given_time").val(sign_out_data.sign_out_antibiotic_given_time);
            }
        }

        $(".number_only").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^0-9\.]/g, ''));
            if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
            {
              evt.preventDefault();
            }
        });





    }


}


function addRowOPVital() {
    $('#op_vitals_tbody').append('<tr class="vital_class" id="' + op_vital_row_id + '_row_idvital" ><td class="common_td_rules "><input onchange="checkTDIsLast(' + op_vital_row_id + ',\'vital\')" type="text" class="form-control timepicker" name="time_op" onclick=""></td><td class="common_td_rules"><input type="text" class="form-control number_only op ' + op_vital_row_id + 'vital" onclick="checkTDIsLast(' + op_vital_row_id + ',\'vital\')" name="temp_op"></td><td class="common_td_rules"><input onclick="" class="form-control number_only  op' + op_vital_row_id + 'vital" type="text" value="" name="pulse_op"></td><td class="common_td_rules"><input onclick="" class="form-control number_only op' + op_vital_row_id + 'vital" type="text" value="" name="resp_op"></td><td class="common_td_rules"><input onclick="" class="form-control op ' + op_vital_row_id + 'vital" type="text" value="" name="bp_op"></td><td class="common_td_rules"><input onclick="" class="form-control number_only op' + op_vital_row_id + 'vital" type="text" value="" name="spo2_op"></td> <td class="common_td_rules"><input onclick="" class="form-control number_only op' + op_vital_row_id + 'vital" type="text" value="" name="rbs_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + op_vital_row_id + 'vital" type="text" value="" name="pain_op"></td> <td class="common_td_rules"><input onclick="" class="form-control op' + op_vital_row_id + 'vital" type="text" value="" name="op_remark"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + op_vital_row_id + ',\'vital\')"><i class="fa fa-trash red" ></i></button></td></tr>');
    op_vital_row_id++;
    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });

    $(".number_only").on("input", function(evt) {
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
        {
          evt.preventDefault();
        }
      });
}

function addRowInNursing() {
    $('#op_pack_tbody').append('<tr class="vital_class" id="' + op_nursing_row_id + '_row_idnursing" ><td class="common_td_rules "><input onchange="checkTDIsLast(' + op_nursing_row_id + ',\'nursing\')" type="text" class="form-control " name="type_op" onclick=""></td><td class="common_td_rules"><input onclick="" class="form-control op' + op_nursing_row_id + 'nursing" type="text" value="" name="site_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + op_nursing_row_id + 'nursing" type="text" value="" name="size_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + op_nursing_row_id + 'nursing" type="text" value="" oninput="" name="amount_op"></td><td class="common_td_rules"><input onclick="" class="form-control datetimepicker op' + op_nursing_row_id + 'nursing" type="text" value="" name="date_time_op"></td><td class="common_td_rules"><input onclick="" class="form-control op' + op_nursing_row_id + 'nursing" type="text" value="" name="sign_op"></td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + op_nursing_row_id + ',\'nursing\')"><i class="fa fa-trash red" ></i></button></td></tr>');
    op_nursing_row_id++;
    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });

    $(".number_only").on("input", function(evt) {
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
        {
          evt.preventDefault();
        }
    });

}

$(".number_only").on("input", function(evt) {
    var self = $(this);
    self.val(self.val().replace(/[^0-9\.]/g, ''));
    if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
    {
      evt.preventDefault();
    }
  });

function removeTableData(op_vital_row_id, from_type) {
    var value = $('.op' + op_vital_row_id + '' + from_type).filter(function () {
        return this.value != '';
    });
    if (value.length != 0) {
        bootbox.confirm({
            message: "Are your sure to remove this ?",
            buttons: {
                'confirm': {
                    label: "Remove",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#' + op_vital_row_id + '_row_id' + from_type).remove();
                }
            }
        });
    } else {
        $('#' + op_vital_row_id + '_row_id' + from_type).remove();
    }
}

function checkTDIsLast(id, from_type) {
    if (!$('#' + id + '_row_id' + from_type).closest('tr').next().length) {
        if (from_type == 'nursing') {
            addRowInNursing();
        }
        if (from_type == 'vital') {
            addRowOPVital();
        }

    }
}

function getAnesthesiaRecord(head_id, surgery_id, surgery_detail_id) {
    var url = base_url + "/ot_checklists/getAnesthesiaRecord";
    $('#head_id').val(head_id);
    $('#surgery_id_hidden').val(surgery_id);
    $('#surgery_detail_id_hidden').val(surgery_detail_id);
    $('#list_type').val(2);
    $.ajax({
        url: url,
        type: "POST",
        data: { head_id: head_id, surgery_detail_id: surgery_detail_id },
        beforeSend: function () {
            $('#anesthesia_checklist_btn' + head_id + surgery_id).attr('disabled', true);
            $('#anesthesia_checklist_spin' + head_id + surgery_id).removeClass('fa fa-low-vision');
            $('#anesthesia_checklist_spin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');
            $('#save_checklistbtn').hide();
            $('#save_operation_notes_btn').hide();
            $('#save_ans_checklistbtn').show();
        },
        success: function (data) {
            var obj = JSON.parse(data);
            // console.log(obj.checklist_data);
            $('#surgery_checklist').html(obj.checklist_data);
            $('.modal-body').css('min-height', '520px');
            $('#checkListDataHeader').html(obj.patient_title + ' ' + obj.patient_name + '(' + obj.patient_uhid + ')');
            $("#surgery_checklist_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#anesthesia_checklist_btn' + head_id + surgery_id).attr('disabled', false);
            $('#anesthesia_checklist_spin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#anesthesia_checklist_spin' + head_id + surgery_id).addClass('fa fa-low-vision');

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

            setTimeout(function () {
                fetchAnaesthesiaChecKList(surgery_detail_id);
            }, 2000);

        },
    });
}

function getOperationNotes(head_id, surgery_id, surgery_detail_id) {
    var url = base_url + "/ot_checklists/getOperationNotes";
    $('#head_id').val(head_id);
    $('#surgery_id_hidden').val(surgery_id);
    $('#surgery_detail_id_hidden').val(surgery_detail_id);
    $('#list_type').val(3);
    $.ajax({
        url: url,
        type: "POST",
        data: { head_id: head_id, surgery_detail_id: surgery_detail_id },
        beforeSend: function () {
            $('.operation_notes').attr('disabled', true);
            $('#operation_notes_spin' + head_id + surgery_id).removeClass('fa fa-file');
            $('#operation_notes_spin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');

            $('#save_checklistbtn').hide();
            $('#save_operation_notes_btn').show();
            $('#save_ans_checklistbtn').hide();

        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#surgery_checklist').html(obj.checklist_data);
            $('.modal-body').css('min-height', '436px');
            $('#checkListDataHeader').html(obj.patient_title + ' ' + obj.patient_name + '(' + obj.patient_uhid + ')');
            $("#surgery_checklist_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
                // $('.hide_class').hide();
                // addNewMedicine();
            }, 400);
        },
        complete: function () {
            $('.operation_notes').attr('disabled', false);
            $('#operation_notes_spin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#operation_notes_spin' + head_id + surgery_id).addClass('fa fa-file');
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

            fetchOperationNotes(surgery_detail_id);
            addNewMedicine();
            $('.hide_class').hide();

        },
    });
}

function HistopathologyRequestForm(head_id, surgery_id, patient_id, surgery_detail_id) {
    var url = base_url + "/ot_schedule/HistopathologyRequestForm";
    $('#head_id').val(head_id);
    $('#surgery_id_hidden').val(surgery_id);
    $('#patient_id_hidden').val(patient_id);
    $('#surgery_detail_id_hidden').val(surgery_detail_id);
    var patient_name = $('#SurgeryRequestPatient' + head_id + surgery_id).html();
    var surgery_name = $('#SurgeryRequestSurgery' + head_id + surgery_id).html();
    $.ajax({
        url: url,
        type: "POST",
        data: { head_id: head_id, surgery_id: surgery_id, patient_id: patient_id,surgery_detail_id:surgery_detail_id },
        beforeSend: function () {
            $('#HistopathologyRequestFormBtn' + head_id + surgery_id).attr('disabled', true);
            $('#HistopathologyRequestFormSpin' + head_id + surgery_id).removeClass('fa fa-flask');
            $('#HistopathologyRequestFormSpin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');
            $('#histopathogy_data_modal_header').html(patient_name + '(' + surgery_name + ')');
        },
        success: function (data) {
            $('#histopathogy_data_modal_div').html(data);
            $("#histopathogy_data_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#HistopathologyRequestFormBtn' + head_id + surgery_id).attr('disabled', false);
            $('#HistopathologyRequestFormSpin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#HistopathologyRequestFormSpin' + head_id + surgery_id).addClass('fa fa-flask');
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });
        },
    });
}


function MicrobiologyInvestigation(head_id, surgery_id, patient_id, surgery_detail_id) {
    var url = base_url + "/ot_schedule/MicrobiologyInvestigation";
    $('#head_id').val(head_id);
    $('#surgery_id_hidden').val(surgery_id);
    $('#patient_id_hidden').val(patient_id);
    $('#surgery_detail_id_hidden').val(surgery_detail_id);
    var patient_name = $('#SurgeryRequestPatient' + head_id + surgery_id).html();
    var surgery_name = $('#SurgeryRequestSurgery' + head_id + surgery_id).html();
    $.ajax({
        url: url,
        type: "POST",
        data: { head_id: head_id, surgery_id: surgery_id, patient_id: patient_id,surgery_detail_id:surgery_detail_id },
        beforeSend: function () {
            $('#MicrobiologyInvestigationBtn' + head_id + surgery_id).attr('disabled', true);
            $('#MicrobiologyInvestigationSpin' + head_id + surgery_id).removeClass('fa fa-microchip');
            $('#MicrobiologyInvestigationSpin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');
            $('#microbiology_investigation_data_header').html(patient_name + '(' + surgery_name + ')');
        },
        success: function (data) {
            $('#microbiology_investigation_data_modal_div').html(data);
            $("#microbiology_investigation_data_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });
        },
        complete: function () {
            $('#MicrobiologyInvestigationBtn' + head_id + surgery_id).attr('disabled', false);
            $('#MicrobiologyInvestigationSpin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#MicrobiologyInvestigationSpin' + head_id + surgery_id).addClass('fa fa-microchip');
            $('#AntibioticTimeDate').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
        },
    });
}


function saveHistopathogyData() {
    var url = base_url + "/ot_schedule/saveHistopathology";
    var head_id = $('#head_id').val();
    var histopathology_id = $('#histopathology_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    var clinical_diagnosis = $('#clinical_diagnosis').val();
    var clinical_history = $('#clinical_history').val();
    var nature_of_specimen = $('#nature_of_specimen').val();
    var investigations_done = $('#investigations_done').val();
    var histo_assistant = $('#histo_assistant').val();
    var histo_nurse = $('#histo_nurse').val();
    var histo_scheduled_date = $('#histo_scheduled_date').val();
    var histo_scheduled_time = $('#histo_scheduled_time').val();
    var histo_to = $('#histo_to').val();
    var histo_operation_type = $('#histo_operation_type').val();
    $.ajax({
        url: url,
        type: "POST",
        data: { histopathology_id: histopathology_id, head_id: head_id, surgery_detail_id: surgery_detail_id, surgery_id: surgery_id, patient_id: patient_id, clinical_diagnosis: clinical_diagnosis, clinical_history: clinical_history, nature_of_specimen: nature_of_specimen, investigations_done: investigations_done,histo_assistant,histo_nurse,histo_scheduled_date,histo_scheduled_time,histo_to,histo_operation_type},
        beforeSend: function () {
            $('#saveHistopathogyDataBtn').attr('disabled', true);
            $('#saveHistopathogyDataSpin').removeClass('fa fa-save');
            $('#saveHistopathogyDataSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                toastr.success("Histopathogy data successfully updated");
                $("#histopathogy_data_modal").modal('toggle');
            }
        },
        complete: function () {
            $('#saveHistopathogyDataBtn').attr('disabled', false);
            $('#saveHistopathogyDataSpin').removeClass('fa fa-spinner fa-spin');
            $('#saveHistopathogyDataSpin').addClass('fa fa-save');
        },
    });
}


function saveMicrobiologyInvestigation() {
    var url = base_url + "/ot_schedule/saveMicrobiologyInvestigation";
    var mocrobilogy_id = $('#microbiology_id').val();
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    var ShortHistroyoftheCase = $('#ShortHistroyoftheCase').val();
    var ProvisionalDiagnosis = $('#ProvisionalDiagnosis').val();
    var NatureSpecimenCollection = $('#NatureSpecimenCollection').val();
    var TimeCollectionSpecimen = $('#TimeCollectionSpecimen').val();
    var InvestigationsRequired = $('#InvestigationsRequired').val();
    var AntibioticTimeDate = $('#AntibioticTimeDate').val();
    var AntibioticType = $('#AntibioticType').val();
    $.ajax({
        url: url,
        type: "POST",
        data: {
            mocrobilogy_id: mocrobilogy_id, head_id: head_id, surgery_detail_id: surgery_detail_id, surgery_id: surgery_id, patient_id: patient_id, ShortHistroyoftheCase: ShortHistroyoftheCase, ProvisionalDiagnosis: ProvisionalDiagnosis, NatureSpecimenCollection: NatureSpecimenCollection, TimeCollectionSpecimen: TimeCollectionSpecimen, InvestigationsRequired: InvestigationsRequired, AntibioticTimeDate: AntibioticTimeDate, AntibioticType: AntibioticType
        },
        beforeSend: function () {
            $('#saveMicrobiologyInvestigationBtn').attr('disabled', true);
            $('#saveMicrobiologyInvestigationBtnSpin').removeClass('fa fa-save');
            $('#saveMicrobiologyInvestigationBtnSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                toastr.success("Histopathogy data successfully updated");
                $("#microbiology_investigation_data_modal").modal('toggle');
            }
        },
        complete: function () {
            $('#saveMicrobiologyInvestigationBtn').attr('disabled', false);
            $('#saveMicrobiologyInvestigationBtnSpin').removeClass('fa fa-spinner fa-spin');
            $('#saveMicrobiologyInvestigationBtnSpin').addClass('fa fa-save');
        },
    });
}


function gotoSurgeryBilling(head_id) {
    var url = base_url + "/surgerybilling/BillCreation/" + head_id;
    window.location.href = url;
}

function saveSurgeryChecklistParameters() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgeryChecklistParameters";

    //---ot parameters--------------------------------------------------

    var vital_sign_recorded = $('input[name="vital_sign_recorded"]:checked').val();
    var name_of_procedure = $('input[name="name_of_procedure"]:checked').val();
    var name_of_procedure_text = $('#name_of_procedure_text').val();
    var nbm_time = $('#nbm_time').val();
    var nbm = $('input[name="nbm"]:checked').val();
    var am_care = $('input[name="am_care"]:checked').val();
    var am_care_status = $('input[name="am_care_status"]:checked').val();
    var urinary_output_time = $('#urinary_output_time').val();
    var on_catheter = 0;
    if ($('#on_catheter').is(':checked')) {
        on_catheter = 1;
    }
    var urine_amount = $('#urine_amount').val();
    var urinary_output_status = $('input[name="urinary_output_status"]:checked').val();
    var bowl_wash = 0;
    if ($('#bowl_wash').is(':checked')) {
        bowl_wash = 1;
    }
    var laxatives = 0;
    if ($('#laxatives').is(':checked')) {
        laxatives = 1;
    }
    var stool_passed = 0;
    if ($('#stool_passed').is(':checked')) {
        stool_passed = 1;
    }
    var enema_status = $('input[name="enema_status"]:checked').val();
    var skin_preparation = $('input[name="skin_preparation"]:checked').val();
    var care_hair_nail = $('input[name="care_hair_nail"]:checked').val();
    var pre_op_medication_time = $('#pre_op_medication_time').val();
    var pre_op_medication_status = $('input[name="pre_op_medication_status"]:checked').val();
    var medication_profile_chart = $('input[name="medication_profile_chart"]:checked').val();
    var allergies = $('input[name="allergies"]:checked').val();
    var required_blood_group = $('#required_blood_group').val();
    var required_blood_group_status = $('#required_blood_group_status').val();
    var covid_test_report_status = $('input[name="covid_test_report_status"]:checked').val();
    var serology = $('input[name="serology"]:checked').val();
    var relevant_investigation = $('input[name="relevant_investigation"]:checked').val();
    var contact_lenses = $('input[name="contact_lenses"]:checked').val();
    var dentures_implants = $('input[name="dentures_implants"]:checked').val();
    var jewellery_removed = $('input[name="jewellery_removed"]:checked').val();
    var consent = $('input[name="consent"]:checked').val();
    var high_risk_consent = $('input[name="high_risk_consent"]:checked').val();
    var anaesthesia_pac = $('input[name="anaesthesia_pac"]:checked').val();
    var consultation_medical_fitness = $('input[name="consultation_medical_fitness"]:checked').val();
    var marking_of_site = $('input[name="marking_of_site"]:checked').val();
    var parameters_remarks = $('#parameters_remarks').val();
    var handover_given_by = $('#handover_given_by').val();
    var handover_taken_by = $('#handover_taken_by').val();

    var ot_parameters = {
        'vital_sign_recorded': vital_sign_recorded,
        'name_of_procedure': name_of_procedure,
        'name_of_procedure_text': name_of_procedure_text,
        'nbm_time': nbm_time,
        'nbm': nbm,
        'am_care': am_care,
        'am_care_status': am_care_status,
        'urinary_output_time': urinary_output_time,
        'on_catheter': on_catheter,
        'urine_amount': urine_amount,
        'urinary_output_status': urinary_output_status,
        'bowl_wash': bowl_wash,
        'laxatives': laxatives,
        'stool_passed': stool_passed,
        'enema_status': enema_status,
        'skin_preparation': skin_preparation,
        'care_hair_nail': care_hair_nail,
        'pre_op_medication_time': pre_op_medication_time,
        'pre_op_medication_status': pre_op_medication_status,
        'medication_profile_chart': medication_profile_chart,
        'allergies': allergies,
        'required_blood_group': required_blood_group,
        'required_blood_group_status': required_blood_group_status,
        'covid_test_report_status': covid_test_report_status,
        'serology': serology,
        'relevant_investigation': relevant_investigation,
        'contact_lenses': contact_lenses,
        'dentures_implants': dentures_implants,
        'jewellery_removed': jewellery_removed,
        'consent': consent,
        'high_risk_consent': high_risk_consent,
        'anaesthesia_pac': anaesthesia_pac,
        'consultation_medical_fitness': consultation_medical_fitness,
        'marking_of_site': marking_of_site,
        'parameters_remarks': parameters_remarks,
        'handover_given_by':handover_given_by,
        'handover_taken_by': handover_taken_by,
    }
    ot_parameters = JSON.stringify(ot_parameters);

    var dataparams = {
        'ot_parameters': ot_parameters,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                // $('#surgery_checklist_modal').modal('toggle');
                $('#op_vitals_li').trigger('click');

                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });

}
function saveSurgeryOpVitals() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgeryOpVitals";


    //----checklist vital params-----------------------------------
    var op_vitals_params = [];
    var time_op = '';
    var temp_op = '';
    var pulse_op = '';
    var resp_op = '';
    var bp_op = '';
    var spo2_op = '';
    var rbs_op = '';
    var pain_op = '';
    var op_remark = '';
    var vitalparams = [];
    var pre_operative_notes = $('#pre_operative_notes').val();
    var op_vital_handover_given_by = $('#op_vital_handover_given_by').val();
    var op_vital_handover_taken_by = $('#op_vital_handover_taken_by').val();

    $('#op_vitals_tbody tr').each(function () {

        time_op = $(this).find('input[name="time_op"]').val();
        temp_op = $(this).find('input[name="temp_op"]').val();
        pulse_op = $(this).find('input[name="pulse_op"]').val();
        resp_op = $(this).find('input[name="resp_op"]').val();
        bp_op = $(this).find('input[name="bp_op"]').val();
        spo2_op = $(this).find('input[name="spo2_op"]').val();
        rbs_op = $(this).find('input[name="rbs_op"]').val();
        pain_op = $(this).find('input[name="pain_op"]').val();
        op_remark = $(this).find('input[name="op_remark"]').val();
        // if(time_op!='' && temp_op != '' && pulse_op != '' && resp_op != '' && bp_op != '' && spo2_op != '' && rbs_op != '' && pain_op != '' && op_remark != ''){
        vitalparams = {
            'time_op': time_op,
            'temp_op': temp_op,
            'pulse_op': pulse_op,
            'resp_op': resp_op,
            'bp_op': bp_op,
            'spo2_op': spo2_op,
            'rbs_op': rbs_op,
            'pain_op': pain_op,
            'op_remark': op_remark,
        };
        op_vitals_params.push(vitalparams);
        // }

    });
    var vital_params = [];
    vital_params = { 'op_vitals_params': op_vitals_params, 'pre_operative_notes': pre_operative_notes,'op_vital_handover_given_by':op_vital_handover_given_by, 'op_vital_handover_taken_by':op_vital_handover_taken_by };
    vital_params = JSON.stringify(vital_params);


    var dataparams = {
        'vital_params': vital_params,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                // $('#surgery_checklist_modal').modal('toggle');
                $('#sign_in_li').trigger('click');
                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });

}

function saveSurgerySignIn() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgerySignIn";


    //----checklist sign in params-----------------------------------
    var sign_in_params = [];
    var patient_identity_verified = $('input[name="patient_identity_verified"]:checked').val();
    var correct_procedure = $('input[name="correct_procedure"]:checked').val();
    var site_marked = $('input[name="site_marked"]:checked').val();
    var availability_of_required_medical = $('input[name="availability_of_required_medical"]:checked').val();
    var availability_and_functionality_of_required = $('input[name="availability_and_functionality_of_required"]:checked').val();
    var anesthesia_safety_check_completed = $('input[name="anesthesia_safety_check_completed"]:checked').val();
    var difficlty_airways = $('input[name="difficlty_airways"]:checked').val();
    var blood_product_reserved = $('input[name="blood_product_reserved"]:checked').val();
    var risk_of_blood_loss = $('input[name="risk_of_blood_loss"]:checked').val();
    var antibiotic_prophylaxis = $('input[name="antibiotic_prophylaxis"]:checked').val();
    var antibiotic_prophylaxis_drug_name = '';
    var antibiotic_given_time = '';
    antibiotic_prophylaxis_drug_name = $('#antibiotic_prophylaxis_drug_name').val();
    antibiotic_given_time = $('#antibiotic_given_time').val();
    signin_completed_by = $('#signin_completed_by').val();
    signin_completed_at = $('#signin_completed_at').val();

    sign_in_params = {
        'patient_identity_verified': patient_identity_verified,
        'correct_procedure': correct_procedure,
        'site_marked': site_marked,
        'availability_of_required_medical': availability_of_required_medical,
        'availability_and_functionality_of_required': availability_and_functionality_of_required,
        'anesthesia_safety_check_completed': anesthesia_safety_check_completed,
        'difficlty_airways': difficlty_airways,
        'blood_product_reserved': blood_product_reserved,
        'risk_of_blood_loss': risk_of_blood_loss,
        'antibiotic_prophylaxis': antibiotic_prophylaxis,
        'antibiotic_prophylaxis_drug_name': antibiotic_prophylaxis_drug_name,
        'antibiotic_given_time': antibiotic_given_time,
        'signin_completed_by': signin_completed_by,
        'signin_completed_at': signin_completed_at,
    };

    sign_in_params = JSON.stringify(sign_in_params);


    var dataparams = {
        'sign_in_params': sign_in_params,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                // $('#surgery_checklist_modal').modal('toggle');
                $('#time_out_li').trigger('click');

                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });

}
function saveSurgeryTimeOut() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgeryTimeOut";


    //----checklist timeout params-----------------------------------

    var timeoutparams = [];
    var correct_patient = $('input[name="correct_patient"]:checked').val();
    var correct_site_and_side = $('input[name="correct_site_and_side"]:checked').val();
    var agreement_on_procedure = $('input[name="agreement_on_procedure"]:checked').val();
    var correct_patient_position = $('input[name="correct_patient_position"]:checked').val();
    var availability_of_implants = $('input[name="availability_of_implants"]:checked').val();
    var relevant_investigation_and_images = $('input[name="relevant_investigation_and_images"]:checked').val();
    var pre_counts_completed = $('input[name="pre_counts_completed"]:checked').val();
    var timeout_surgeon = $('#timeout_surgeon').val();
    var timeout_anaesthetist = $('#timeout_anaesthetist').val();
    var timeout_scrub_nurse = $('#timeout_scrub_nurse').val();
    var timeout_nurse = $('#timeout_nurse').val();
    var timeout_completed_by = $('#timeout_completed_by').val();
    var timeout_completed_at = $('#timeout_completed_at').val();
    var timeout_technician = $('#timeout_technician').val();
    var timeout_others = $('#timeout_others').val();
    timeoutparams = {
        'correct_patient': correct_patient,
        'correct_site_and_side': correct_site_and_side,
        'agreement_on_procedure': agreement_on_procedure,
        'correct_patient_position': correct_patient_position,
        'availability_of_implants': availability_of_implants,
        'relevant_investigation_and_images': relevant_investigation_and_images,
        'pre_counts_completed': pre_counts_completed,
        'timeout_surgeon': timeout_surgeon,
        'timeout_anaesthetist': timeout_anaesthetist,
        'timeout_scrub_nurse': timeout_scrub_nurse,
        'timeout_nurse': timeout_nurse,
        'timeout_completed_by': timeout_completed_by,
        'timeout_completed_at': timeout_completed_at,
        'timeout_technician': timeout_technician,
        'timeout_others': timeout_others,
    };

    timeoutparams = JSON.stringify(timeoutparams);


    var dataparams = {
        'timeoutparams': timeoutparams,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                // $('#surgery_checklist_modal').modal('toggle');
                $('#nursing_record_li').trigger('click');
                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });

}
function saveSurgeryNursingRecord() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgeryNursingRecord";


    //-----------checklist nursing record-----------------------------------
    nursing_record_params = [];
    var ns_record_procedure_name = $('#ns_record_procedure_name').val();
    var ns_record_procedure_start_time = $('#ns_record_procedure_start_time').val();
    var ns_record_procedure_end_time = $('#ns_record_procedure_end_time').val();
    var ns_record_specimen = $('#ns_record_specimen').val();
    var ns_record_lab = $('#ns_record_lab').val();
    var ns_record_no_specimen = $('#ns_record_no_specimen').val();
    var ns_record_collector = $('#ns_record_collector').val();
    var ns_implants = $('#ns_implants').val();

    var op_pack = [];
    var op_pack_params = [];

    $('#op_pack_tbody tr').each(function () {
        type_op = $(this).find('input[name="type_op"]').val();
        site_op = $(this).find('input[name="site_op"]').val();
        size_op = $(this).find('input[name="size_op"]').val();
        amount_op = $(this).find('input[name="amount_op"]').val();
        date_time_op = $(this).find('input[name="date_time_op"]').val();
        sign_op = $(this).find('input[name="sign_op"]').val();

        if (type_op != '' || site_op != '' || size_op != '' || amount_op != '' || date_time_op != '' || sign_op != '') {
            op_pack_params = {
                'type_op': type_op,
                'site_op': site_op,
                'size_op': size_op,
                'amount_op': amount_op,
                'date_time_op': date_time_op,
                'sign_op': sign_op,
            };
            op_pack.push(op_pack_params);
        }

    });
    nursing_record_params = {
        'ns_record_procedure_name': ns_record_procedure_name,
        'ns_record_procedure_start_time': ns_record_procedure_start_time,
        'ns_record_procedure_end_time': ns_record_procedure_end_time,
        'ns_record_specimen': ns_record_specimen,
        'ns_record_lab': ns_record_lab,
        'ns_record_no_specimen': ns_record_no_specimen,
        'ns_record_collector': ns_record_collector,
        'ns_implants': ns_implants,
        'op_pack': op_pack
    };
    nursing_record_params = JSON.stringify(nursing_record_params);


    var dataparams = {
        'nursing_record_params': nursing_record_params,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                // $('#surgery_checklist_modal').modal('toggle');
                $('#sign_out_li').trigger('click');
                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });

}
function saveSurgerySignOut() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgerySignOut";


    //----checklist signout------------------------
    checklist_signout_params = [];
    var preSpong = $('#preSpong').val();
    var preGauze = $('#preGauze').val();
    var prePeanut_Gauze = $('#prePeanut_Gauze').val();
    var preRoller_Gauze = $('#preRoller_Gauze').val();
    var preVassel_Loop = $('#preVassel_Loop').val();
    var preInstrument = $('#preInstrument').val();
    var preNeedle = $('#preNeedle').val();
    var preCirculatory_Nurse = $('#preCirculatory_Nurse').val();
    var preScrub_Nurse = $('#preScrub_Nurse').val();
    var ReliSpong = $('#ReliSpong').val();
    var ReliGauze = $('#ReliGauze').val();
    var ReliPeanut_Gauze = $('#ReliPeanut_Gauze').val();
    var ReliRoller_Gauze = $('#ReliRoller_Gauze').val();
    var ReliVassel_Loop = $('#ReliVassel_Loop').val();
    var ReliInstrument = $('#ReliInstrument').val();
    var ReliNeedle = $('#ReliNeedle').val();
    var ReliCirculatory_Nurse = $('#ReliCirculatory_Nurse').val();
    var ReliScrub_Nurse = $('#ReliScrub_Nurse').val();
    var AtclSpong = $('#AtclSpong').val();
    var AtclGauze = $('#AtclGauze').val();
    var AtclPeanut_Gauze = $('#AtclPeanut_Gauze').val();
    var AtclRoller_Gauze = $('#AtclRoller_Gauze').val();
    var AtclVassel_Loop = $('#AtclVassel_Loop').val();
    var AtclInstrument = $('#AtclInstrument').val();
    var AtclNeedle = $('#AtclNeedle').val();
    var AtclCirculatory_Nurse = $('#AtclCirculatory_Nurse').val();
    var AtclScrub_Nurse = $('#AtclScrub_Nurse').val();
    var FinalSpong = $('#FinalSpong').val();
    var FinalGauze = $('#FinalGauze').val();
    var FinalPeanut_Gauze = $('#FinalPeanut_Gauze').val();
    var FinalRoller_Gauze = $('#FinalRoller_Gauze').val();
    var FinalVassel_Loop = $('#FinalVassel_Loop').val();
    var FinalInstrument = $('#FinalInstrument').val();
    var FinalNeedle = $('#FinalNeedle').val();
    var FinalCirculatory_Nurse = $('#FinalCirculatory_Nurse').val();
    var FinalScrub_Nurse = $('#FinalScrub_Nurse').val();

    var count_correct = $('input[name="count_correct"]:checked').val();
    var surgeon_inform = $('input[name="surgeon_inform"]:checked').val();
    var sign_out_antibiotic_given_time = $('#sign_out_antibiotic_given_time').val();
    var antibiotic_given_time = $('#antibiotic_given_time').val();
    var signin_completed_by = $('#signin_completed_by').val();
    var signin_completed_at = $('#signin_completed_at').val();
    var action_taken_remarks = $('#action_taken_remarks').val();
    var sign_out_post_notes = $('#sign_out_post_notes').val();
    var signout_handover_given_by = $('#signout_handover_given_by').val();
    var signout_handover_taken_by = $('#signout_handover_taken_by').val();
    var signout_handover_taken_at = $('#signout_handover_taken_at').val();

    checklist_signout_params = {
        'preSpong': preSpong,
        'preGauze': preGauze,
        'prePeanut_Gauze': prePeanut_Gauze,
        'preRoller_Gauze': preRoller_Gauze,
        'preVassel_Loop': preVassel_Loop,
        'preInstrument': preInstrument,
        'preNeedle': preNeedle,
        'preCirculatory_Nurse': preCirculatory_Nurse,
        'preScrub_Nurse': preScrub_Nurse,

        'ReliSpong': ReliSpong,
        'ReliGauze': ReliGauze,
        'ReliPeanut_Gauze': ReliPeanut_Gauze,
        'ReliRoller_Gauze': ReliRoller_Gauze,
        'ReliVassel_Loop': ReliVassel_Loop,
        'ReliInstrument': ReliInstrument,
        'ReliNeedle': ReliNeedle,
        'ReliCirculatory_Nurse': ReliCirculatory_Nurse,
        'ReliScrub_Nurse': ReliScrub_Nurse,
        'AtclSpong': AtclSpong,
        'AtclGauze': AtclGauze,
        'AtclPeanut_Gauze': AtclPeanut_Gauze,
        'AtclRoller_Gauze': AtclRoller_Gauze,
        'AtclVassel_Loop': AtclVassel_Loop,
        'AtclInstrument': AtclInstrument,
        'AtclNeedle': AtclNeedle,
        'AtclCirculatory_Nurse': AtclCirculatory_Nurse,
        'AtclScrub_Nurse': AtclScrub_Nurse,
        'FinalSpong': FinalSpong,
        'FinalGauze': FinalGauze,
        'FinalPeanut_Gauze': FinalPeanut_Gauze,
        'FinalRoller_Gauze': FinalRoller_Gauze,
        'FinalVassel_Loop': FinalVassel_Loop,
        'FinalInstrument': FinalInstrument,
        'FinalNeedle': FinalNeedle,
        'FinalCirculatory_Nurse': FinalCirculatory_Nurse,
        'FinalScrub_Nurse': FinalScrub_Nurse,
        'count_correct': count_correct,
        'surgeon_inform': surgeon_inform,
        'antibiotic_given_time': antibiotic_given_time,
        'signin_completed_by': signin_completed_by,
        'signin_completed_at': signin_completed_at,
        'action_taken_remarks': action_taken_remarks,
        'sign_out_post_notes': sign_out_post_notes,
        'signout_handover_given_by': signout_handover_given_by,
        'signout_handover_taken_by': signout_handover_taken_by,
        'signout_handover_taken_at': signout_handover_taken_at,
        'sign_out_antibiotic_given_time': sign_out_antibiotic_given_time,
    };

    checklist_signout_params = JSON.stringify(checklist_signout_params);


    var dataparams = {
        'checklist_signout_params': checklist_signout_params,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                $('#surgery_checklist_modal').modal('toggle');
                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });

}





function saveSurgeryChecKList() {
    var head_id = $('#head_id').val();
    var surgery_id = $('#surgery_id_hidden').val();
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    var list_type = $('#list_type').val();
    var url = base_url + "/ot_checklists/saveSurgeryChecKList";

    //---ot parameters--------------------------------------------------

    var vital_sign_recorded = $('input[name="vital_sign_recorded"]:checked').val();
    var name_of_procedure = $('input[name="name_of_procedure"]:checked').val();
    var name_of_procedure_text = $('#name_of_procedure_text').val();
    var nbm_time = $('#nbm_time').val();
    var nbm = $('input[name="nbm"]:checked').val();
    var am_care = $('input[name="am_care"]:checked').val();
    var am_care_status = $('input[name="am_care_status"]:checked').val();
    var urinary_output_time = $('#urinary_output_time').val();
    var on_catheter = 0;
    if ($('#on_catheter').is(':checked')) {
        on_catheter = 1;
    }
    var urine_amount = $('#urine_amount').val();
    var urinary_output_status = $('input[name="urinary_output_status"]:checked').val();
    var bowl_wash = 0;
    if ($('#bowl_wash').is(':checked')) {
        bowl_wash = 1;
    }
    var laxatives = 0;
    if ($('#laxatives').is(':checked')) {
        laxatives = 1;
    }
    var stool_passed = 0;
    if ($('#stool_passed').is(':checked')) {
        stool_passed = 1;
    }
    var enema_status = $('input[name="enema_status"]:checked').val();
    var skin_preparation = $('input[name="skin_preparation"]:checked').val();
    var care_hair_nail = $('input[name="care_hair_nail"]:checked').val();
    var pre_op_medication_time = $('#pre_op_medication_time').val();
    var pre_op_medication_status = $('input[name="pre_op_medication_status"]:checked').val();
    var medication_profile_chart = $('input[name="medication_profile_chart"]:checked').val();
    var allergies = $('input[name="allergies"]:checked').val();
    var required_blood_group = $('#required_blood_group').val();
    var required_blood_group_status = $('#required_blood_group_status').val();
    var covid_test_report_status = $('input[name="covid_test_report_status"]:checked').val();
    var serology = $('input[name="serology"]:checked').val();
    var relevant_investigation = $('input[name="relevant_investigation"]:checked').val();
    var contact_lenses = $('input[name="contact_lenses"]:checked').val();
    var dentures_implants = $('input[name="dentures_implants"]:checked').val();
    var jewellery_removed = $('input[name="jewellery_removed"]:checked').val();
    var consent = $('input[name="consent"]:checked').val();
    var high_risk_consent = $('input[name="high_risk_consent"]:checked').val();
    var anaesthesia_pac = $('input[name="anaesthesia_pac"]:checked').val();
    var consultation_medical_fitness = $('input[name="consultation_medical_fitness"]:checked').val();
    var marking_of_site = $('input[name="marking_of_site"]:checked').val();

    var ot_parameters = {
        'vital_sign_recorded': vital_sign_recorded,
        'name_of_procedure': name_of_procedure,
        'name_of_procedure_text': name_of_procedure_text,
        'nbm_time': nbm_time,
        'nbm': nbm,
        'am_care': am_care,
        'am_care_status': am_care_status,
        'urinary_output_time': urinary_output_time,
        'on_catheter': on_catheter,
        'urine_amount': urine_amount,
        'urinary_output_status': urinary_output_status,
        'bowl_wash': bowl_wash,
        'laxatives': laxatives,
        'stool_passed': stool_passed,
        'enema_status': enema_status,
        'skin_preparation': skin_preparation,
        'care_hair_nail': care_hair_nail,
        'pre_op_medication_time': pre_op_medication_time,
        'pre_op_medication_status': pre_op_medication_status,
        'medication_profile_chart': medication_profile_chart,
        'allergies': allergies,
        'required_blood_group': required_blood_group,
        'required_blood_group_status': required_blood_group_status,
        'covid_test_report_status': covid_test_report_status,
        'serology': serology,
        'relevant_investigation': relevant_investigation,
        'contact_lenses': contact_lenses,
        'dentures_implants': dentures_implants,
        'jewellery_removed': jewellery_removed,
        'consent': consent,
        'high_risk_consent': high_risk_consent,
        'anaesthesia_pac': anaesthesia_pac,
        'consultation_medical_fitness': consultation_medical_fitness,
        'marking_of_site': marking_of_site,
    }
    ot_parameters = JSON.stringify(ot_parameters);

    //----checklist vital params-----------------------------------
    var op_vitals_params = [];
    var time_op = '';
    var temp_op = '';
    var pulse_op = '';
    var resp_op = '';
    var bp_op = '';
    var spo2_op = '';
    var rbs_op = '';
    var pain_op = '';
    var op_remark = '';
    var vitalparams = [];
    var pre_operative_notes = $('#pre_operative_notes').val();
    var op_vital_handover_given_by = $('#op_vital_handover_given_by').val();
    var op_vital_handover_taken_by = $('#op_vital_handover_taken_by').val();
    $('#op_vitals_tbody tr').each(function () {

        time_op = $(this).find('input[name="time_op"]').val();
        temp_op = $(this).find('input[name="temp_op"]').val();
        pulse_op = $(this).find('input[name="pulse_op"]').val();
        resp_op = $(this).find('input[name="resp_op"]').val();
        bp_op = $(this).find('input[name="bp_op"]').val();
        spo2_op = $(this).find('input[name="spo2_op"]').val();
        rbs_op = $(this).find('input[name="rbs_op"]').val();
        pain_op = $(this).find('input[name="pain_op"]').val();
        op_remark = $(this).find('input[name="op_remark"]').val();
        // if(time_op!='' && temp_op != '' && pulse_op != '' && resp_op != '' && bp_op != '' && spo2_op != '' && rbs_op != '' && pain_op != '' && op_remark != ''){
        vitalparams = {
            'time_op': time_op,
            'temp_op': temp_op,
            'pulse_op': pulse_op,
            'resp_op': resp_op,
            'bp_op': bp_op,
            'spo2_op': spo2_op,
            'rbs_op': rbs_op,
            'pain_op': pain_op,
            'op_remark': op_remark,
        };
        op_vitals_params.push(vitalparams);
        // }

    });
    var vital_params = [];
    vital_params = { 'op_vitals_params': op_vitals_params, 'pre_operative_notes': pre_operative_notes,'op_vital_handover_given_by':op_vital_handover_given_by, 'op_vital_handover_taken_by':op_vital_handover_taken_by};
    vital_params = JSON.stringify(vital_params);


    //----checklist sign in params-----------------------------------
    var sign_in_params = [];
    var patient_identity_verified = $('input[name="patient_identity_verified"]:checked').val();
    var correct_procedure = $('input[name="correct_procedure"]:checked').val();
    var site_marked = $('input[name="site_marked"]:checked').val();
    var availability_of_required_medical = $('input[name="availability_of_required_medical"]:checked').val();
    var availability_and_functionality_of_required = $('input[name="availability_and_functionality_of_required"]:checked').val();
    var anesthesia_safety_check_completed = $('input[name="anesthesia_safety_check_completed"]:checked').val();
    var difficlty_airways = $('input[name="difficlty_airways"]:checked').val();
    var blood_product_reserved = $('input[name="blood_product_reserved"]:checked').val();
    var risk_of_blood_loss = $('input[name="risk_of_blood_loss"]:checked').val();
    var antibiotic_prophylaxis = $('input[name="antibiotic_prophylaxis"]:checked').val();
    var antibiotic_prophylaxis_drug_name = '';
    var antibiotic_given_time = '';
    antibiotic_prophylaxis_drug_name = $('#antibiotic_prophylaxis_drug_name').val();
    antibiotic_given_time = $('#antibiotic_given_time').val();
    signin_completed_by = $('#signin_completed_by').val();
    signin_completed_at = $('#signin_completed_at').val();

    sign_in_params = {
        'patient_identity_verified': patient_identity_verified,
        'correct_procedure': correct_procedure,
        'site_marked': site_marked,
        'availability_of_required_medical': availability_of_required_medical,
        'availability_and_functionality_of_required': availability_and_functionality_of_required,
        'anesthesia_safety_check_completed': anesthesia_safety_check_completed,
        'difficlty_airways': difficlty_airways,
        'blood_product_reserved': blood_product_reserved,
        'risk_of_blood_loss': risk_of_blood_loss,
        'antibiotic_prophylaxis': antibiotic_prophylaxis,
        'antibiotic_prophylaxis_drug_name': antibiotic_prophylaxis_drug_name,
        'antibiotic_given_time': antibiotic_given_time,
        'signin_completed_by': signin_completed_by,
        'signin_completed_at': signin_completed_at,
    };

    sign_in_params = JSON.stringify(sign_in_params);

    //----checklist timeout params-----------------------------------

    var timeoutparams = [];
    var correct_patient = $('input[name="correct_patient"]:checked').val();
    var correct_site_and_side = $('input[name="correct_site_and_side"]:checked').val();
    var agreement_on_procedure = $('input[name="agreement_on_procedure"]:checked').val();
    var correct_patient_position = $('input[name="correct_patient_position"]:checked').val();
    var availability_of_implants = $('input[name="availability_of_implants"]:checked').val();
    var relevant_investigation_and_images = $('input[name="relevant_investigation_and_images"]:checked').val();
    var pre_counts_completed = $('input[name="pre_counts_completed"]:checked').val();
    var timeout_surgeon = $('#timeout_surgeon').val();
    var timeout_anaesthetist = $('#timeout_anaesthetist').val();
    var timeout_scrub_nurse = $('#timeout_scrub_nurse').val();
    var timeout_nurse = $('#timeout_nurse').val();
    var timeout_completed_by = $('#timeout_completed_by').val();
    var timeout_completed_at = $('#timeout_completed_at').val();
    var timeout_technician = $('#timeout_technician').val();
    var timeout_others = $('#timeout_others').val();
    timeoutparams = {
        'correct_patient': correct_patient,
        'correct_site_and_side': correct_site_and_side,
        'agreement_on_procedure': agreement_on_procedure,
        'correct_patient_position': correct_patient_position,
        'availability_of_implants': availability_of_implants,
        'relevant_investigation_and_images': relevant_investigation_and_images,
        'pre_counts_completed': pre_counts_completed,
        'timeout_surgeon': timeout_surgeon,
        'timeout_anaesthetist': timeout_anaesthetist,
        'timeout_scrub_nurse': timeout_scrub_nurse,
        'timeout_nurse': timeout_nurse,
        'timeout_completed_by': timeout_completed_by,
        'timeout_completed_at': timeout_completed_at,
        'timeout_technician': timeout_technician,
        'timeout_others': timeout_others,
    };

    timeoutparams = JSON.stringify(timeoutparams);

    //-----------checklist nursing record-----------------------------------
    nursing_record_params = [];
    var ns_record_procedure_name = $('#ns_record_procedure_name').val();
    var ns_record_procedure_start_time = $('#ns_record_procedure_start_time').val();
    var ns_record_procedure_end_time = $('#ns_record_procedure_end_time').val();
    var ns_record_specimen = $('#ns_record_specimen').val();
    var ns_implants = $('#ns_implants').val();
    var ns_record_lab = $('#ns_record_lab').val();
    var ns_record_no_specimen = $('#ns_record_no_specimen').val();
    var ns_record_collector = $('#ns_record_collector').val();

    var op_pack = [];
    var op_pack_params = [];

    $('#op_pack_tbody tr').each(function () {
        type_op = $(this).find('input[name="type_op"]').val();
        site_op = $(this).find('input[name="site_op"]').val();
        size_op = $(this).find('input[name="size_op"]').val();
        amount_op = $(this).find('input[name="amount_op"]').val();
        date_time_op = $(this).find('input[name="date_time_op"]').val();
        sign_op = $(this).find('input[name="sign_op"]').val();

        if (type_op != '' || site_op != '' || size_op != '' || amount_op != '' || date_time_op != '' || sign_op != '') {
            op_pack_params = {
                'type_op': type_op,
                'site_op': site_op,
                'size_op': size_op,
                'amount_op': amount_op,
                'date_time_op': date_time_op,
                'sign_op': sign_op,
            };
            op_pack.push(op_pack_params);
        }

    });
    nursing_record_params = {
        'ns_record_procedure_name': ns_record_procedure_name,
        'ns_record_procedure_start_time': ns_record_procedure_start_time,
        'ns_record_procedure_end_time': ns_record_procedure_end_time,
        'ns_record_specimen': ns_record_specimen,
        'ns_record_lab': ns_record_lab,
        'ns_record_no_specimen': ns_record_no_specimen,
        'ns_record_collector': ns_record_collector,
        'op_pack': op_pack,
        'ns_implants': ns_implants
    };
    nursing_record_params = JSON.stringify(nursing_record_params);


    //----checklist signout------------------------
    checklist_signout_params = [];
    var preSpong = $('#preSpong').val();
    var preGauze = $('#preGauze').val();
    var prePeanut_Gauze = $('#prePeanut_Gauze').val();
    var preRoller_Gauze = $('#preRoller_Gauze').val();
    var preVassel_Loop = $('#preVassel_Loop').val();
    var preInstrument = $('#preInstrument').val();
    var preNeedle = $('#preNeedle').val();
    var preCirculatory_Nurse = $('#preCirculatory_Nurse').val();
    var preScrub_Nurse = $('#preScrub_Nurse').val();
    var ReliSpong = $('#ReliSpong').val();
    var ReliGauze = $('#ReliGauze').val();
    var ReliPeanut_Gauze = $('#ReliPeanut_Gauze').val();
    var ReliRoller_Gauze = $('#ReliRoller_Gauze').val();
    var ReliVassel_Loop = $('#ReliVassel_Loop').val();
    var ReliInstrument = $('#ReliInstrument').val();
    var ReliNeedle = $('#ReliNeedle').val();
    var ReliCirculatory_Nurse = $('#ReliCirculatory_Nurse').val();
    var ReliScrub_Nurse = $('#ReliScrub_Nurse').val();
    var AtclSpong = $('#AtclSpong').val();
    var AtclGauze = $('#AtclGauze').val();
    var AtclPeanut_Gauze = $('#AtclPeanut_Gauze').val();
    var AtclRoller_Gauze = $('#AtclRoller_Gauze').val();
    var AtclVassel_Loop = $('#AtclVassel_Loop').val();
    var AtclInstrument = $('#AtclInstrument').val();
    var AtclNeedle = $('#AtclNeedle').val();
    var AtclCirculatory_Nurse = $('#AtclCirculatory_Nurse').val();
    var AtclScrub_Nurse = $('#AtclScrub_Nurse').val();
    var FinalSpong = $('#FinalSpong').val();
    var FinalGauze = $('#FinalGauze').val();
    var FinalPeanut_Gauze = $('#FinalPeanut_Gauze').val();
    var FinalRoller_Gauze = $('#FinalRoller_Gauze').val();
    var FinalVassel_Loop = $('#FinalVassel_Loop').val();
    var FinalInstrument = $('#FinalInstrument').val();
    var FinalNeedle = $('#FinalNeedle').val();
    var FinalCirculatory_Nurse = $('#FinalCirculatory_Nurse').val();
    var FinalScrub_Nurse = $('#FinalScrub_Nurse').val();

    var count_correct = $('input[name="count_correct"]:checked').val();
    var surgeon_inform = $('input[name="surgeon_inform"]:checked').val();
    var sign_out_antibiotic_given_time = $('#sign_out_antibiotic_given_time').val();
    var antibiotic_given_time = $('#antibiotic_given_time').val();
    var signin_completed_by = $('#signin_completed_by').val();
    var signin_completed_at = $('#signin_completed_at').val();
    var action_taken_remarks = $('#action_taken_remarks').val();
    var sign_out_post_notes = $('#sign_out_post_notes').val();
    var signout_handover_given_by = $('#signout_handover_given_by').val();
    var signout_handover_taken_by = $('#signout_handover_taken_by').val();
    var signout_handover_taken_at = $('#signout_handover_taken_at').val();

    checklist_signout_params = {
        'preSpong': preSpong,
        'preGauze': preGauze,
        'prePeanut_Gauze': prePeanut_Gauze,
        'preRoller_Gauze': preRoller_Gauze,
        'preVassel_Loop': preVassel_Loop,
        'preInstrument': preInstrument,
        'preNeedle': preNeedle,
        'preCirculatory_Nurse': preCirculatory_Nurse,
        'preScrub_Nurse': preScrub_Nurse,

        'ReliSpong': ReliSpong,
        'ReliGauze': ReliGauze,
        'ReliPeanut_Gauze': ReliPeanut_Gauze,
        'ReliRoller_Gauze': ReliRoller_Gauze,
        'ReliVassel_Loop': ReliVassel_Loop,
        'ReliInstrument': ReliInstrument,
        'ReliNeedle': ReliNeedle,
        'ReliCirculatory_Nurse': ReliCirculatory_Nurse,
        'ReliScrub_Nurse': ReliScrub_Nurse,
        'AtclSpong': AtclSpong,
        'AtclGauze': AtclGauze,
        'AtclPeanut_Gauze': AtclPeanut_Gauze,
        'AtclRoller_Gauze': AtclRoller_Gauze,
        'AtclVassel_Loop': AtclVassel_Loop,
        'AtclInstrument': AtclInstrument,
        'AtclNeedle': AtclNeedle,
        'AtclCirculatory_Nurse': AtclCirculatory_Nurse,
        'AtclScrub_Nurse': AtclScrub_Nurse,
        'FinalSpong': FinalSpong,
        'FinalGauze': FinalGauze,
        'FinalPeanut_Gauze': FinalPeanut_Gauze,
        'FinalRoller_Gauze': FinalRoller_Gauze,
        'FinalVassel_Loop': FinalVassel_Loop,
        'FinalInstrument': FinalInstrument,
        'FinalNeedle': FinalNeedle,
        'FinalCirculatory_Nurse': FinalCirculatory_Nurse,
        'FinalScrub_Nurse': FinalScrub_Nurse,
        'count_correct': count_correct,
        'surgeon_inform': surgeon_inform,
        'antibiotic_given_time': antibiotic_given_time,
        'signin_completed_by': signin_completed_by,
        'signin_completed_at': signin_completed_at,
        'action_taken_remarks': action_taken_remarks,
        'sign_out_post_notes': sign_out_post_notes,
        'signout_handover_given_by': signout_handover_given_by,
        'signout_handover_taken_by': signout_handover_taken_by,
        'signout_handover_taken_at': signout_handover_taken_at,
        'sign_out_antibiotic_given_time': sign_out_antibiotic_given_time,
    };

    checklist_signout_params = JSON.stringify(checklist_signout_params);

    var dataparams = {
        'checklist_signout_params': checklist_signout_params,
        'nursing_record_params': nursing_record_params,
        'timeoutparams': timeoutparams,
        'sign_in_params': sign_in_params,
        'vital_params': vital_params,
        'ot_parameters': ot_parameters,
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'surgery_id': surgery_id,

    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_checklistbtn').attr('disabled', true);
            $('#save_checklistspin').removeClass('fa fa-save');
            $('#save_checklistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                $('#surgery_checklist_modal').modal('toggle');
                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }
            }
        },
        complete: function () {
            $('#save_checklistbtn').attr('disabled', false);
            $('#save_checklistspin').removeClass('fa fa-spinner fa-spin');
            $('#save_checklistspin').addClass('fa fa-save');
        },
    });
}



var medicine_list = [];
var medicine_row_id = 1;
var consumables_list = [];
var consumables_row_id = 1;


function medicineIndent(head_id, surgery_id) {
    var url = base_url + "/ot_checklists/medicineIndent";
    $.ajax({
        url: url,
        type: "POST",
        data: { head_id: head_id, surgery_id: surgery_id },
        beforeSend: function () {
            $('#medicineIndent_btn').attr('disabled', true);
            $('#medicineIndent_spin' + head_id + surgery_id).removeClass('fa fa-medkit');
            $('#medicineIndent_spin' + head_id + surgery_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            $("#surgery_checklist_modal").attr('data-request-id', head_id);
            $("#surgery_checklist_modal").attr('data-surgery-id', surgery_id);
            $('#medicinelist').empty();
            $('#consumableslist').empty();
            $("#surgery_checklist").find('.modal-body').css('min-height', '600px !important');
            $("#surgery_checklist").html(response.data.html);
            $.each(response.data.medicine_detail, function (key, val) {
                fillMedicineData(val.item_code, val.item_desc, "medicine_serach", "0", val.amount, val.generic_name, val.quantity);
            });

            $.each(response.data.consumable_detail, function (key, val) {
                fillMedicineData(val.item_code, val.item_desc, "consumables_search", "0", val.amount,'', val.quantity);
            });

            $("#surgery_checklist_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#surgery_checklist_modal").find(".modal-footer").hide();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        complete: function () {
            $('#medicineIndent_btn').attr('disabled', true);
            $('#medicineIndent_spin' + head_id + surgery_id).removeClass('fa fa-spinner fa-spin');
            $('#medicineIndent_spin' + head_id + surgery_id).addClass('fa fa-medkit');
        },
    });
}

$('#surgery_checklist_modal').on('hidden.bs.modal', function () {
    $("#surgery_checklist_modal").find(".modal-footer").show();
});


function fillMedicineData(code, desc, serach_key_id, stock, price, generic_name, quantity = 1) {
    $('#' + serach_key_id + '_hidden').val(code);
    $('#' + serach_key_id).val('');
    $('.ajaxSearchBox').hide();
    if (serach_key_id == 'medicine_serach') {
        addMedicineListData(code, desc, price, generic_name, quantity);
    } else if (serach_key_id == 'consumables_search') {
        addConsumablesListData(code, desc, price, quantity);
    }
}


function addMedicineListData(code, desc, price, generic_name, quantity) {
    $('#medicinelist').append('<tr class="Medicine_class" id="Medicine_row_id' + medicine_row_id + '"><td class="common_td_rules Medicine_count_class"><td class="common_td_rules">' + desc + '</td><td class="common_td_rules">' + generic_name + '</td><td class="common_td_rules"><input class="form-control" type="hidden" value="' + code + '" name="medicine_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="medicine_price"><input class="form-control" oninput="validateNumber(this)" value="' + quantity + '" autocomplete="off" onkeyup="getListTotalQty(' + medicine_row_id + ',\'Medicine\',' + price + ')" type="text" id="Medicine_qty' + medicine_row_id + '" name="medicine_qty"></td> <td class="td_common_numeric_rules"> ' + price + ' </td><td id="Medicine_price_total' + medicine_row_id + '" class="Medicine_price td_common_numeric_rules"> ' + price + ' </td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + medicine_row_id + ', \'' + code + '\', \'Medicine\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    medicine_row_id++;
    medicine_list.push(code);
    calculateTableDataPrice('Medicine');
}

function addConsumablesListData(code, desc, price, quantity) {
    $('#consumableslist').append('<tr class="Consumables_class" id="Consumables_row_id' + consumables_row_id + '"><td class="common_td_rules Consumables_count_class"><td class="common_td_rules">' + desc + '</td><td class="common_td_rules"><input class="form-control" type="hidden" value="' + code + '" name="consumables_serive_code"><input class="form-control" type="hidden" value="' + price + '" name="consumables_price"><input class="form-control" oninput="validateNumber(this)" value="' + quantity + '" autocomplete="off" onkeyup="getListTotalQty(' + consumables_row_id + ',\'Consumables\',' + price + ')" type="text" id="Consumables_qty' + consumables_row_id + '" name="Consumables_qty"></td><td class="td_common_numeric_rules"> ' + price + ' </td><td id="Consumables_price_total' + consumables_row_id + '" class="Consumables_price td_common_numeric_rules"> ' + price + ' </td><td><button type="button" class="btn btn-default" onclick="removeTableData(' + consumables_row_id + ', \'' + code + '\', \'Consumables\')"><i class="fa fa-trash red" ></i></button></td> </tr>');
    consumables_row_id++;
    consumables_list.push(code);
    calculateTableDataPrice('Consumables');
}


function calculateTableDataPrice(from_type) {
    var total = 0.0;
    if ('.' + from_type + '_price') {
        $('.' + from_type + '_price').each(function (index) {
            var price = $(this).html();
            total += parseFloat(price);
        });
        $('#' + from_type + '_charge').html(number_format(total, 2, '.', ''));
    }
    if ('.' + from_type + '_class') {
        row_ct = 1;
        $('.' + from_type + '_class').each(function (i) {
            $(this).find('.' + from_type + '_count_class').text(row_ct);
            row_ct++;
        });
    }
}


function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}


function getPreviousTab() {
    $(".nav-tabs > .active").prev("li").find("a").tab("show");
}


function gotoNextTab() {
    $(".nav-tabs > .active").next("li").find("a").tab("show");
}

function saveSurgeryIndent() {

    var bill_details = {};
    var medicine_details = [];

    $("#medicinelist").find('tr').each(function (key, val) {
        var medicine = {};
        medicine.product_code = $(val).find('input[name="medicine_serive_code"]').val();
        medicine.quantity = $(val).find('input[name="medicine_qty"]').val();
        medicine_details.push(medicine);
    })
    bill_details.medicine_details = medicine_details;

    var consumable_details = [];

    $("#consumableslist").find('tr').each(function (key, val) {
        var consumable = {};
        consumable.product_code = $(val).find('input[name="consumables_serive_code"]').val();
        consumable.quantity = $(val).find('input[name="Consumables_qty"]').val();
        consumable_details.push(consumable);
    })
    bill_details.consumable_details = consumable_details;

    var head_id = $("#surgery_checklist_modal").attr('data-request-id');
    var surgery_id = $("#surgery_checklist_modal").attr('data-surgery-id');

    if (bill_details.medicine_details.length > 0 || bill_details.consumable_details.length > 0) {
        var url = base_url + "/ot_checklists/saveMedicineIndent";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                request_json: bill_details,
                head_id: head_id,
                surgery_id: surgery_id,
                _token: token
            },
            dataType: "json",
            beforeSend: function () {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function (response) {
                $("body").LoadingOverlay("hide");
                if (response.status == 1) {
                    toastr.success('Successfully saved.');
                    $("#surgery_checklist_modal").modal("hide");
                    searchOTSchedule();
                    // window.location.reload();
                }
            },
            complete: function () { }
        });
    }
}

//-----saving parameters in ot checklist------------------------------------
function saveNursingSurgeryChecKList() {
    var patient_id = $('#patient_id').val();
    var surgery_request_id = $('#surgery_request_id').val();
    if (surgery_request_id == '') {
        toastr.warning('Please select surgery!');
        return false;
    }
    var vital_sign_recorded = $('input[name="vital_sign_recorded"]:checked').val();
    var name_of_procedure = $('input[name="name_of_procedure"]:checked').val();
    var name_of_procedure_text = $('#name_of_procedure_text').val();
    var nbm_time = $('#nbm_time').val();
    var nbm = $('input[name="nbm"]:checked').val();
    var am_care = $('input[name="am_care"]:checked').val();
    var am_care_status = $('input[name="am_care_status"]:checked').val();
    var urinary_output_time = $('#urinary_output_time').val();
    var on_catheter = 0;
    if ($('#on_catheter').is(':checked')) {
        on_catheter = 1;
    }
    var urine_amount = $('#urine_amount').val();
    var urinary_output_status = $('input[name="urinary_output_status"]:checked').val();
    var bowl_wash = 0;
    if ($('#bowl_wash').is(':checked')) {
        bowl_wash = 1;
    }
    var laxatives = 0;
    if ($('#laxatives').is(':checked')) {
        laxatives = 1;
    }
    var stool_passed = 0;
    if ($('#stool_passed').is(':checked')) {
        stool_passed = 1;
    }
    var enema_status = $('input[name="enema_status"]:checked').val();
    var skin_preparation = $('input[name="skin_preparation"]:checked').val();
    var care_hair_nail = $('input[name="care_hair_nail"]:checked').val();
    var pre_op_medication_time = $('#pre_op_medication_time').val();
    var pre_op_medication_status = $('input[name="pre_op_medication_status"]:checked').val();
    var medication_profile_chart = $('input[name="medication_profile_chart"]:checked').val();
    var allergies = $('input[name="allergies"]:checked').val();
    var required_blood_group = $('#required_blood_group').val();
    var required_blood_group_status = $('#required_blood_group_status').val();
    var covid_test_report_status = $('input[name="covid_test_report_status"]:checked').val();
    var serology = $('input[name="serology"]:checked').val();
    var relevant_investigation = $('input[name="relevant_investigation"]:checked').val();
    var contact_lenses = $('input[name="contact_lenses"]:checked').val();
    var dentures_implants = $('input[name="dentures_implants"]:checked').val();
    var jewellery_removed = $('input[name="jewellery_removed"]:checked').val();
    var consent = $('input[name="consent"]:checked').val();
    var high_risk_consent = $('input[name="high_risk_consent"]:checked').val();
    var anaesthesia_pac = $('input[name="anaesthesia_pac"]:checked').val();
    var consultation_medical_fitness = $('input[name="consultation_medical_fitness"]:checked').val();
    var marking_of_site = $('input[name="marking_of_site"]:checked').val();
    var parameters_remarks = $('#parameters_remarks').val();
    var handover_given_by = $('#handover_given_by').val();
    var handover_taken_by = $('#handover_taken_by').val();

    var ot_parameters = {
        'vital_sign_recorded': vital_sign_recorded,
        'name_of_procedure': name_of_procedure,
        'name_of_procedure_text': name_of_procedure_text,
        'nbm_time': nbm_time,
        'nbm': nbm,
        'am_care': am_care,
        'am_care_status': am_care_status,
        'urinary_output_time': urinary_output_time,
        'on_catheter': on_catheter,
        'urine_amount': urine_amount,
        'urinary_output_status': urinary_output_status,
        'bowl_wash': bowl_wash,
        'laxatives': laxatives,
        'stool_passed': stool_passed,
        'enema_status': enema_status,
        'skin_preparation': skin_preparation,
        'care_hair_nail': care_hair_nail,
        'pre_op_medication_time': pre_op_medication_time,
        'pre_op_medication_status': pre_op_medication_status,
        'medication_profile_chart': medication_profile_chart,
        'allergies': allergies,
        'required_blood_group': required_blood_group,
        'required_blood_group_status': required_blood_group_status,
        'covid_test_report_status': covid_test_report_status,
        'serology': serology,
        'relevant_investigation': relevant_investigation,
        'contact_lenses': contact_lenses,
        'dentures_implants': dentures_implants,
        'jewellery_removed': jewellery_removed,
        'consent': consent,
        'high_risk_consent': high_risk_consent,
        'anaesthesia_pac': anaesthesia_pac,
        'consultation_medical_fitness': consultation_medical_fitness,
        'marking_of_site': marking_of_site,
        'parameters_remarks': parameters_remarks,
        'handover_given_by': handover_given_by,
        'handover_taken_by': handover_taken_by,
    }
    ot_parameters = JSON.stringify(ot_parameters);
    var dataparams = {
        'patient_id': patient_id,
        'surgery_request_id': surgery_request_id,
        'ot_parameters': ot_parameters,
    };

    var url = base_url + "/ot_checklists/saveNsOtCheckList";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            if (response == 1) {
                toastr.success('Successfully saved.');
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });

}

function getSurgeryChecklistData(surgery_detail_id) {
    $('#surgery_request_id').val(surgery_detail_id);
}

function PrintOtCheckList(surgery_detail_id) {
    var url = base_url + "/ot_checklists/PrintOtCheckList";
    var dataparams = { 'surgery_detail_id': surgery_detail_id };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#ot_checklist_print_modal').modal('show');
            $('#ot_checklist_print_data').html('');
            $("#ot_checklist_print_data").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            if (response != 0) {
                $('#ot_checklist_print_data').html(response);
            } else {
                toastr.error('No record found!');
            }
        },
        complete: function () {
            $("#ot_checklist_print_data").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}
function PrintAnsCheckList(surgery_detail_id) {
    var url = base_url + "/ot_checklists/PrintAnsCheckList";
    var dataparams = { 'surgery_detail_id': surgery_detail_id };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#ot_checklist_print_modal').modal('show');
            $("#ot_checklist_print_data").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {

            if (response == 0) {
                toastr.error('No internet connection.');
            }else if (response == 2) {
                toastr.warning('No entry found.');
                $('#ot_checklist_print_modal').modal('hide');
            } else {
                $('#ot_checklist_print_data').html(response);
            }

        },
        complete: function () {
            $("#ot_checklist_print_data").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}

function PrintOperationNotes(surgery_detail_id,patient_id){
    var url = base_url + "/ot_checklists/PrintOperationNotes";
    var dataparams = {surgery_detail_id: surgery_detail_id,patient_id:patient_id};
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#ot_checklist_print_modal').modal('show');
            $("#ot_checklist_print_data").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {

            if (response == 0) {
                toastr.error('No internet connection.');
            }else if (response == 2) {
                toastr.warning('No entry found.');
                $('#ot_checklist_print_modal').modal('hide');
            } else {
                $('#ot_checklist_print_data').html(response);
            }

        },
        complete: function () {
            $("#ot_checklist_print_data").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}


function PrintHistoPathology(head_id,surgery_detail_id){
    var url = base_url + "/ot_checklists/PrintHistoPathology";
    var dataparams = {surgery_detail_id: surgery_detail_id,head_id:head_id};
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {

            $("#ot_checklist_print_data").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            $('#ot_checklist_print_modal').modal('show');
            if (response == 0) {
                toastr.error('No internet connection.');
            }else if (response == 2) {
                toastr.warning('No entry found.');
                $('#ot_checklist_print_modal').modal('hide');
            } else {
                $('#ot_checklist_print_data').html(response);
            }

        },
        complete: function () {
            $("#ot_checklist_print_data").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}



//---------print report--------------
function takePrintOtChecklist() {
    $('#print_config_modal').modal('toggle');
}

function print_generate(printData) {

    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');
    $('#total_row').css('display', '');
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;



    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }

    mywindow.document.write(
        '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
    );
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.pagebreak { page-break-before: always !important;page-break-after: always !important;}</style>'
    );
    mywindow.document.write(
        '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
    );
    mywindow.document.write(
        '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
    );
    mywindow.document.write(
        '<style> .col-md-4 {width:25% !important;float:left !important;clear:right !important;} .text-center {text-align:center;} .pagebreak{page-break-after: always !important;} h5{font-size: 14px;} .checklist_head_text{color:white !important;} .checklist_head_text_new{color:white !important;}   .no-padding{padding:0px !important;} .col-md-6{width:50% !important;float:left !important;clear:right !important;} .col-md-12{width:100% !important;float:left !important;clear:right !important;} .col-md-2{width:20% !important;float:left !important;clear:right !important;}.col-md-1{width:10% !important;float:left !important;clear:right !important;}</style>'
    );
    var font_awesome_path = $('#font_awsome_css_path').val();
    mywindow.document.write(
        '<link href="' + font_awesome_path + '" rel="stylesheet">'
    );

    mywindow.document.write(showw);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
        InitTheadScroll();
        $('#total_row').css('display', 'none');
    }, 1000);
    return true;
}




function saveAnaesthetiaPreAnaesthetic() {
    var surgery_detail_id = $('#anaesthesia_surgery_req_detail_id').val();
    var surgery_head_id = $('#anaesthesia_surgery_req_head_id').val();
    var patient_id = $('#anaesthesia_patient_id').val();
    if (surgery_detail_id == '') {
        toastr.warning('Select surgery!');
        return false;
    }
    //----pre anesthetic assesment----------------------------------------

    var name_of_surgery = $('#name_of_surgery').val();
    var date_and_time_of_surgery = $('#date_and_time_of_surgery').val();
    var surgeon_name = $('#pre_ans_surgeon_name').val();
    var history_hypertension = $('input[name="history_hypertension"]:checked').val();
    var diabetes = $('input[name="diabetes"]:checked').val();
    var asthma = $('input[name="asthma"]:checked').val();
    var epilepsy = $('input[name="epilepsy"]:checked').val();
    var allergy = $('input[name="allergy"]:checked').val();
    var i_h_d = $('input[name="i_h_d"]:checked').val();
    var previous_surgeory_and_anesthesia = $('#previous_surgeory_and_anesthesia').val();
    var thyroid_condition = $('input[name="thyroid_condition"]:checked').val();
    var hyper_history_of_tb = $('#hyper_history_of_tb').val();
    var anesthesia_medications = $('#anesthesia_medications').val();
    var other_relevant_history = $('#other_relevant_history').val();
    var other_relevant_history = $('#other_relevant_history').val();
    var icterus = $('input[name="icterus"]:checked').val();
    var pallor = $('input[name="pallor"]:checked').val();
    var lymphadenopathy = $('#lymphadenopathy').val();
    var edema = $('input[name="edema"]:checked').val();
    var airway_dentition = $('#airway_dentition').val();
    var thyroid_desc = $('#thyroid_desc').val();
    var spine = $('#spine').val();
    var mallampattl_grade = $('#mallampattl_grade').val();
    var tm_distance = $('#tm_distance').val();
    var anesthesia_pulse = $('#anesthesia_pulse').val();
    var anesthesia_bp = $('#anesthesia_bp').val();
    var anesthesia_temprature = $('#anesthesia_temprature').val();
    var anesthesia_weight = $('#anesthesia_weight').val();
    var cvs = $('#cvs').val();
    var respiratory_system = $('#respiratory_system').val();
    var git = $('#git').val();
    var cns = $('#cns').val();
    var other_relevant_findings = $('#other_relevant_findings').val();

    //---investigation-------------------------------------------------------
    var inv_total_cols = document.getElementById('pre_anaesthetic_inv_table').rows[0].cells.length;
    var pre_inv_time = [];
    var pre_inv_hb = [];
    var pre_inv_platelets = [];
    var pre_inv_blood_sugar = [];
    var pre_inv_pp = [];
    var pre_inv_random = [];
    var pre_inv_hba1c = [];
    var pre_inv_blood_grouping = [];
    var pre_inv_bleeding_time = [];
    var pre_inv_clotting_time = [];
    var pre_inv_pt_control = [];
    var pre_inv_pt_test = [];
    var pre_inv_pt_inr = [];
    var pre_inv_apt_control = [];
    var pre_inv_apt_test = [];
    var pre_inv_apt_others = [];
    var pre_inv_creatine = [];
    var pre_inv_blood_urea = [];
    var pre_inv_cue = [];
    var pre_inv_na = [];
    var pre_inv_k = [];
    var pre_inv_serum_electrolytes_others = [];
    var pre_inv_hbsag = [];
    var pre_inv_hcv = [];
    var pre_inv_hiv = [];
    var pre_inv_xray_chest = [];
    var pre_inv_ecg = [];
    var pre_inv_echo = [];
    var pre_inv_lft = [];
    var pre_inv_tsh = [];
    var pre_inv_t3 = [];
    var pre_inv_t4 = [];

    for (var j = 1; j < inv_total_cols; j++) {
        pre_inv_time.push($('#pre_inv_time_' + j).val());
        pre_inv_hb.push($('#pre_inv_hb_' + j).val());
        pre_inv_platelets.push($('#pre_inv_platelets_' + j).val());
        pre_inv_blood_sugar.push($('#pre_inv_blood_sugar_' + j).val());
        pre_inv_pp.push($('#pre_inv_pp_' + j).val());
        pre_inv_random.push($('#pre_inv_random_' + j).val());
        pre_inv_hba1c.push($('#pre_inv_hba1c_' + j).val());
        pre_inv_blood_grouping.push($('#pre_inv_blood_grouping_' + j).val());
        pre_inv_bleeding_time.push($('#pre_inv_bleeding_time_' + j).val());
        pre_inv_clotting_time.push($('#pre_inv_clotting_time_' + j).val());
        pre_inv_pt_control.push($('#pre_inv_pt_control_' + j).val());
        pre_inv_pt_test.push($('#pre_inv_pt_test_' + j).val());
        pre_inv_pt_inr.push($('#pre_inv_pt_inr_' + j).val());
        pre_inv_apt_control.push($('#pre_inv_apt_control_' + j).val());
        pre_inv_apt_test.push($('#pre_inv_apt_test_' + j).val());
        pre_inv_apt_others.push($('#pre_inv_apt_others_' + j).val());
        pre_inv_creatine.push($('#pre_inv_creatine_' + j).val());
        pre_inv_blood_urea.push($('#pre_inv_blood_urea_' + j).val());
        pre_inv_cue.push($('#pre_inv_cue_' + j).val());
        pre_inv_na.push($('#pre_inv_na_' + j).val());
        pre_inv_k.push($('#pre_inv_k_' + j).val());
        pre_inv_serum_electrolytes_others.push($('#pre_inv_serum_electrolytes_others_' + j).val());
        pre_inv_hbsag.push($('#pre_inv_hbsag_' + j).val());
        pre_inv_hcv.push($('#pre_inv_hcv_' + j).val());
        pre_inv_hiv.push($('#pre_inv_hiv_' + j).val());
        pre_inv_xray_chest.push($('#pre_inv_xray_chest_' + j).val());
        pre_inv_ecg.push($('#pre_inv_ecg_' + j).val());
        pre_inv_echo.push($('#pre_inv_echo_' + j).val());
        pre_inv_lft.push($('#pre_inv_lft_' + j).val());
        pre_inv_tsh.push($('#pre_inv_tsh_' + j).val());
        pre_inv_t3.push($('#pre_inv_t3_' + j).val());
        pre_inv_t4.push($('#pre_inv_t4_' + j).val());
    }

    var pre_ans_inv_data = {
        'pre_inv_time':pre_inv_time,
        'pre_inv_hb':pre_inv_hb,
        'pre_inv_platelets':pre_inv_platelets,
        'pre_inv_blood_sugar':pre_inv_blood_sugar,
        'pre_inv_pp':pre_inv_pp,
        'pre_inv_random':pre_inv_random,
        'pre_inv_hba1c':pre_inv_hba1c,
        'pre_inv_blood_grouping':pre_inv_blood_grouping,
        'pre_inv_bleeding_time':pre_inv_bleeding_time,
        'pre_inv_clotting_time':pre_inv_clotting_time,
        'pre_inv_pt_control':pre_inv_pt_control,
        'pre_inv_pt_test':pre_inv_pt_test,
        'pre_inv_pt_inr':pre_inv_pt_inr,
        'pre_inv_apt_control':pre_inv_apt_control,
        'pre_inv_apt_test':pre_inv_apt_test,
        'pre_inv_apt_others':pre_inv_apt_others,
        'pre_inv_creatine':pre_inv_creatine,
        'pre_inv_blood_urea':pre_inv_blood_urea,
        'pre_inv_cue':pre_inv_cue,
        'pre_inv_na':pre_inv_na,
        'pre_inv_k':pre_inv_k,
        'pre_inv_serum_electrolytes_others':pre_inv_serum_electrolytes_others,
        'pre_inv_hbsag':pre_inv_hbsag,
        'pre_inv_hcv':pre_inv_hcv,
        'pre_inv_hiv':pre_inv_hiv,
        'pre_inv_xray_chest':pre_inv_xray_chest,
        'pre_inv_ecg':pre_inv_ecg,
        'pre_inv_echo':pre_inv_echo,
        'pre_inv_lft':pre_inv_lft,
        'pre_inv_tsh':pre_inv_tsh,
        'pre_inv_t3':pre_inv_t3,
        'pre_inv_t4':pre_inv_t4,
    };


    var other_specific_investigations = $('#other_specific_investigations').val();
    var pre_op_openion = $('#pre_op_openion').val();
    var review_pac = $('#review_pac').val();
    var asa_category = $('#asa_category').val();
    var type_of_anaesthesia_planned = $('#type_of_anaesthesia_planned').val();
    var completed_by = $('#completed_by').val();
    var completed_date = $('#completed_date').val();
    var completed_time = $('#completed_time').val();


    var pre_anaesthetic_data={
        'name_of_surgery':name_of_surgery,
        'date_and_time_of_surgery':date_and_time_of_surgery,
        'surgeon_name':surgeon_name,
        'history_hypertension':history_hypertension,
        'diabetes':diabetes,
        'asthma':asthma,
        'epilepsy':epilepsy,
        'allergy':allergy,
        'i_h_d':i_h_d,
        'previous_surgeory_and_anesthesia':previous_surgeory_and_anesthesia,
        'thyroid_condition':thyroid_condition,
        'hyper_history_of_tb':hyper_history_of_tb,
        'anesthesia_medications':anesthesia_medications,
        'other_relevant_history':other_relevant_history,
        'other_relevant_history':other_relevant_history,
        'icterus':icterus,
        'pallor':pallor,
        'lymphadenopathy':lymphadenopathy,
        'edema':edema,
        'airway_dentition':airway_dentition,
        'thyroid_desc':thyroid_desc,
        'spine':spine,
        'mallampattl_grade':mallampattl_grade,
        'tm_distance':tm_distance,
        'anesthesia_pulse':anesthesia_pulse,
        'anesthesia_bp':anesthesia_bp,
        'anesthesia_temprature':anesthesia_temprature,
        'anesthesia_weight':anesthesia_weight,
        'cvs':cvs,
        'respiratory_system':respiratory_system,
        'git':git,
        'cns':cns,
        'other_relevant_findings':other_relevant_findings,
        'pre_ans_inv_data':pre_ans_inv_data,
        'other_specific_investigations':other_specific_investigations,
        'pre_op_openion':pre_op_openion,
        'review_pac':review_pac,
        'asa_category':asa_category,
        'type_of_anaesthesia_planned':type_of_anaesthesia_planned,
        'completed_by':completed_by,
        'completed_date':completed_date,
        'completed_time':completed_time,
    };

    pre_anaesthetic_data = JSON.stringify(pre_anaesthetic_data);
    var url = base_url + "/ot_schedule/saveAnaesthetiaPreAnaesthetic";
    var dataparams = {
        'pre_anaesthetic_data': pre_anaesthetic_data,
        'surgery_detail_id': surgery_detail_id,
        'surgery_head_id': surgery_head_id,
        'patient_id': patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#surgery_checklist").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            if (response != 0) {
                toastr.success('Saved Successfully.');
            } else {
                toastr.error('No internet connection.');
            }
        },
        complete: function () {
            $("#surgery_checklist").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}
function saveAnaesthetiaRecord() {
    var surgery_detail_id = $('#anaesthesia_surgery_req_detail_id').val();
    var surgery_head_id = $('#anaesthesia_surgery_req_head_id').val();
    var patient_id = $('#anaesthesia_patient_id').val();
    if (surgery_detail_id == '') {
        toastr.warning('Select surgery!');
        return false;
    }

    //-----anesthesia record data------------
    var ans_record_anaesthesiologist_1 = $('#ans_record_anaesthesiologist_1').val();
    var ans_record_anaesthesiologist_2 = $('#ans_record_anaesthesiologist_2').val();
    var ans_record_surgery = $('#ans_record_surgery').val();
    var ans_record_surgery_date = $('#ans_record_surgery_date').val();
    var ans_record_surgeon = $('#ans_record_surgeon').val();
    var ans_record_type_anaesthesia = $('input[name="ans_record_type_anaesthesia"]:checked').val();
    var ans_record_nurse = $('#ans_record_nurse').val();
    var ans_record_premedication = $('#ans_record_premedication').val();
    var ans_record_assesment_before_induction = $('#ans_record_assesment_before_induction').val();
    var ans_record_hr = $('#ans_record_hr').val();
    var ans_record_bp = $('#ans_record_bp').val();
    var ans_record_spo2 = $('#ans_record_spo2').val();
    var ans_record_rr = $('#ans_record_rr').val();
    var ans_record_general_condition = $('#ans_record_general_condition').val();
    var ans_record_iv_access = $('#ans_record_iv_access').val();
    var ans_record_site = $('#ans_record_site').val();
    var ans_record_cannula_guage = $('#ans_record_cannula_guage').val();
    var ans_record_regional_anaesthesia_technique = $('#ans_record_regional_anaesthesia_technique').val();
    var ans_record_approach = $('#ans_record_approach').val();
    var ans_record_needle = $('#ans_record_needle').val();
    var ans_record_catheter_fixed_at = $('#ans_record_catheter_fixed_at').val();
    var ans_record_space = $('#ans_record_space').val();
    var ans_record_drug = $('#ans_record_drug').val();
    var ans_record_onset_of_action = $('#ans_record_onset_of_action').val();
    var ans_record_sensory_level = $('#ans_record_sensory_level').val();
    var ans_record_motor_level = $('#ans_record_motor_level').val();
    var ans_record_sensory_level = $('#ans_record_sensory_level').val();
    var ans_record_general_ans_preoxygenation = $('#ans_record_general_ans_preoxygenation').val();
    var ans_record_induction = $('#ans_record_induction').val();
    var ans_record_inhalational = $('#ans_record_inhalational').val();
    var ans_record_inhalational_other = $('#ans_record_inhalational_other').val();
    var ans_record_type_inhalation_type = $('input[name="ans_record_type_inhalation_type"]:checked').val();
    var ans_record_et_nasal = $('#ans_record_et_nasal').val();
    var ans_record_cuff = $('#ans_record_cuff').val();
    var ans_record_other_info = $('#ans_record_other_info').val();
    var ans_record_ventilation_controlled = $('#ans_record_ventilation_controlled').val();
    var ans_record_ventilator_mv = $('#ans_record_ventilator_mv').val();
    var ans_record_rr2 = $('#ans_record_rr2').val();
    var ans_record_tv = $('#ans_record_tv').val();
    var ans_record_ie_ratio = $('#ans_record_ie_ratio').val();
    var ans_record_patient_position = $('#ans_record_patient_position').val();
    var ans_record_technique_applied = $('#ans_record_technique_applied').val();
    var ans_record_removal_at = $('#ans_record_removal_at').val();
    var ans_record_other_note = $('#ans_record_other_note').val();
    var ans_record_time_of_induction = $('#ans_record_time_of_induction').val();
    var ans_record_time_of_incision = $('#ans_record_time_of_incision').val();

    var anasthesia_record_data = {
        'ans_record_anaesthesiologist_1': ans_record_anaesthesiologist_1,
        'ans_record_anaesthesiologist_2': ans_record_anaesthesiologist_2,
        'ans_record_surgery': ans_record_surgery,
        'ans_record_surgery_date': ans_record_surgery_date,
        'ans_record_surgeon': ans_record_surgeon,
        'ans_record_type_anaesthesia': ans_record_type_anaesthesia,
        'ans_record_nurse': ans_record_nurse,
        'ans_record_premedication': ans_record_premedication,
        'ans_record_assesment_before_induction': ans_record_assesment_before_induction,
        'ans_record_hr': ans_record_hr,
        'ans_record_bp': ans_record_bp,
        'ans_record_spo2': ans_record_spo2,
        'ans_record_rr': ans_record_rr,
        'ans_record_general_condition': ans_record_general_condition,
        'ans_record_iv_access': ans_record_iv_access,
        'ans_record_site': ans_record_site,
        'ans_record_cannula_guage': ans_record_cannula_guage,
        'ans_record_regional_anaesthesia_technique': ans_record_regional_anaesthesia_technique,
        'ans_record_approach': ans_record_approach,
        'ans_record_needle': ans_record_needle,
        'ans_record_catheter_fixed_at': ans_record_catheter_fixed_at,
        'ans_record_space': ans_record_space,
        'ans_record_drug': ans_record_drug,
        'ans_record_onset_of_action': ans_record_onset_of_action,
        'ans_record_sensory_level': ans_record_sensory_level,
        'ans_record_motor_level': ans_record_motor_level,
        'ans_record_sensory_level': ans_record_sensory_level,
        'ans_record_general_ans_preoxygenation': ans_record_general_ans_preoxygenation,
        'ans_record_induction': ans_record_induction,
        'ans_record_inhalational': ans_record_inhalational,
        'ans_record_type_inhalation_type': ans_record_type_inhalation_type,
        'ans_record_inhalational_other': ans_record_inhalational_other,
        'ans_record_et_nasal': ans_record_et_nasal,
        'ans_record_cuff': ans_record_cuff,
        'ans_record_other_info': ans_record_other_info,
        'ans_record_ventilation_controlled': ans_record_ventilation_controlled,
        'ans_record_ventilator_mv': ans_record_ventilator_mv,
        'ans_record_rr2': ans_record_rr2,
        'ans_record_tv': ans_record_tv,
        'ans_record_ie_ratio': ans_record_ie_ratio,
        'ans_record_patient_position': ans_record_patient_position,
        'ans_record_technique_applied': ans_record_technique_applied,
        'ans_record_removal_at': ans_record_removal_at,
        'ans_record_other_note': ans_record_other_note,
        'ans_record_time_of_induction': ans_record_time_of_induction,
        'ans_record_time_of_incision': ans_record_time_of_incision,
    };

    anasthesia_record_data = JSON.stringify(anasthesia_record_data);


    var url = base_url + "/ot_schedule/saveAnaesthetiaRecord";
    var dataparams = {
        'anasthesia_record_data': anasthesia_record_data,
        'surgery_detail_id': surgery_detail_id,
        'surgery_head_id': surgery_head_id,
        'patient_id': patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#surgery_checklist").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            if (response != 0) {
                toastr.success('Saved Successfully.');
            } else {
                toastr.error('No internet connection.');
            }
        },
        complete: function () {
            $("#surgery_checklist").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}
function saveAnaesthetiaChecklist() {
    var surgery_detail_id = $('#anaesthesia_surgery_req_detail_id').val();
    var surgery_head_id = $('#anaesthesia_surgery_req_head_id').val();
    var patient_id = $('#anaesthesia_patient_id').val();
    if (surgery_detail_id == '') {
        toastr.warning('Select surgery!');
        return false;
    }

    //----------checklist data---------------------
    var total_cols = document.getElementById('anasthetia_checklist_table').rows[0].cells.length;
    var chk_time = [];
    var chk_oxygen = [];
    var chk_nitrous_oxide = [];
    var chk_halothane = [];
    var chk_sevofiurane = [];
    var chk_anaesthesia_drugs = [];
    var chk_anaesthesia_drugs1 = [];
    var chk_anaesthesia_drugs2 = [];
    var chk_anaesthesia_drugs3 = [];
    var chk_anaesthesia_drugs4 = [];
    var chk_anaesthesia_drugs5 = [];
    var chk_antibiotics1 = [];
    var chk_fluids1 = [];
    var chk_fluids2 = [];
    var chk_fluids3 = [];
    var chk_fluids4 = [];
    var chk_crystalloids = [];
    var chk_colloids = [];
    var chk_invasive_lines = [];
    var chk_invasive_lines2 = [];
    var chk_blood_and_blood_products = [];
    var chk_monitoring_sys = [];
    var chk_monitoring_dia = [];
    var chk_other_monitors = [];
    var chk_other_monitors1 = [];
    var chk_other_monitors2 = [];
    var chk_other_monitors3 = [];
    var chk_urine_output = [];
    var chk_blood_loss = [];
    var chk_total_iv_fliud_given = [];
    var chk_reversal = [];
    var chk_other_monitors_text_1 = '';
    var chk_other_monitors_text_2 = '';
    var chk_other_monitors_text_3 = '';
    var chk_time_of_completion = '';
    var chk_time_of_extubation = '';
    var adequate_resp_efforts = '';
    var eye_opening = '';
    var obeying_commands = '';
    var sustained_head_lift = '';
    var tongue_protrusion = '';
    var airway_reflexes = '';
    var after_giving_thorough = '';

    var post_extubation_pr = '';
    var post_extubation_rr = '';
    var post_extubation_spo2 = '';
    var post_extubation_bp = '';

    for (var i = 1; i < total_cols; i++) {
        chk_time.push($('#chk_time_' + i).val());
        chk_oxygen.push($('#chk_oxygen_' + i).val());
        chk_nitrous_oxide.push($('#chk_nitrous_oxide_' + i).val());
        chk_halothane.push($('#chk_halothane_' + i).val());
        chk_sevofiurane.push($('#chk_sevofiurane_' + i).val());
        chk_anaesthesia_drugs.push($('#chk_anaesthesia_drugs_' + i).val());
        chk_anaesthesia_drugs1.push($('#chk_anaesthesia_drugs1_' + i).val());
        chk_anaesthesia_drugs2.push($('#chk_anaesthesia_drugs2_' + i).val());
        chk_anaesthesia_drugs3.push($('#chk_anaesthesia_drugs3_' + i).val());
        chk_anaesthesia_drugs4.push($('#chk_anaesthesia_drugs4_' + i).val());
        chk_anaesthesia_drugs5.push($('#chk_anaesthesia_drugs5_' + i).val());
        chk_antibiotics1.push($('#chk_antibiotics1_' + i).val());
        chk_fluids1.push($('#chk_fluids1_' + i).val());
        chk_fluids2.push($('#chk_fluids2_' + i).val());
        chk_fluids3.push($('#chk_fluids3_' + i).val());
        chk_fluids4.push($('#chk_fluids4_' + i).val());
        chk_crystalloids.push($('#chk_crystalloids_' + i).val());
        chk_colloids.push($('#chk_colloids_' + i).val());
        chk_invasive_lines.push($('#chk_invasive_lines_' + i).val());
        chk_invasive_lines2.push($('#chk_invasive_lines2_' + i).val());
        chk_blood_and_blood_products.push($('#chk_blood_and_blood_products_' + i).val());
        chk_monitoring_sys.push($('#chk_monitoring_sys_' + i).val());
        chk_monitoring_dia.push($('#chk_monitoring_dia_' + i).val());
        chk_other_monitors.push($('#chk_other_monitors_' + i).val());
        chk_other_monitors1.push($('#chk_other_monitors1_' + i).val());
        chk_other_monitors2.push($('#chk_other_monitors2_' + i).val());
        chk_other_monitors3.push($('#chk_other_monitors3_' + i).val());
        chk_urine_output.push($('#chk_urine_output_' + i).val());
        chk_blood_loss.push($('#chk_blood_loss_' + i).val());
        chk_total_iv_fliud_given.push($('#chk_total_iv_fliud_given_' + i).val());
        chk_reversal.push($('#chk_reversal_' + i).val());
    }


        chk_time_of_completion = $('#chk_time_of_completion').val();
        chk_time_of_extubation = $('#chk_time_of_extubation').val();

        chk_other_monitors_text_1 = $('#chk_other_monitors_text_1').val();
        chk_other_monitors_text_2 = $('#chk_other_monitors_text_2').val();
        chk_other_monitors_text_3 = $('#chk_other_monitors_text_3').val();

        adequate_resp_efforts = $('input[name="adequate_resp_efforts"]:checked').val();
        eye_opening = $('input[name="eye_opening"]:checked').val();
        obeying_commands = $('input[name="obeying_commands"]:checked').val();
        sustained_head_lift = $('input[name="sustained_head_lift"]:checked').val();
        tongue_protrusion = $('input[name="tongue_protrusion"]:checked').val();
        airway_reflexes = $('input[name="airway_reflexes"]:checked').val();
        after_giving_thorough = $('input[name="after_giving_thorough"]:checked').val();

        post_extubation_pr = $('#post_extubation_pr').val();
        post_extubation_rr = $('#post_extubation_rr').val();
        post_extubation_spo2 = $('#post_extubation_spo2').val();
        post_extubation_bp = $('#post_extubation_bp').val();

    var anaesthesia_cheklist_data = {
        'chk_time': chk_time,
        'chk_oxygen': chk_oxygen,
        'chk_nitrous_oxide': chk_nitrous_oxide,
        'chk_halothane': chk_halothane,
        'chk_sevofiurane': chk_sevofiurane,
        'chk_anaesthesia_drugs': chk_anaesthesia_drugs,
        'chk_anaesthesia_drugs1': chk_anaesthesia_drugs1,
        'chk_anaesthesia_drugs2': chk_anaesthesia_drugs2,
        'chk_anaesthesia_drugs3': chk_anaesthesia_drugs3,
        'chk_anaesthesia_drugs4': chk_anaesthesia_drugs4,
        'chk_anaesthesia_drugs5': chk_anaesthesia_drugs5,
        'chk_antibiotics1': chk_antibiotics1,
        'chk_fluids1': chk_fluids1,
        'chk_fluids2': chk_fluids2,
        'chk_fluids3': chk_fluids3,
        'chk_fluids4': chk_fluids4,
        'chk_crystalloids': chk_crystalloids,
        'chk_colloids': chk_colloids,
        'chk_invasive_lines': chk_invasive_lines,
        'chk_invasive_lines2': chk_invasive_lines2,
        'chk_blood_and_blood_products': chk_blood_and_blood_products,
        'chk_monitoring_sys': chk_monitoring_sys,
        'chk_monitoring_dia': chk_monitoring_dia,
        'chk_other_monitors': chk_other_monitors,
        'chk_other_monitors1': chk_other_monitors1,
        'chk_other_monitors2': chk_other_monitors2,
        'chk_other_monitors3': chk_other_monitors3,
        'chk_urine_output': chk_urine_output,
        'chk_blood_loss': chk_blood_loss,
        'chk_total_iv_fliud_given': chk_total_iv_fliud_given,
        'chk_reversal': chk_reversal,
        'chk_time_of_completion': chk_time_of_completion,
        'chk_time_of_extubation': chk_time_of_extubation,
        'chk_other_monitors_text_1': chk_other_monitors_text_1,
        'chk_other_monitors_text_2': chk_other_monitors_text_2,
        'chk_other_monitors_text_3': chk_other_monitors_text_3,
        'adequate_resp_efforts': adequate_resp_efforts,
        'eye_opening': eye_opening,
        'obeying_commands': obeying_commands,
        'sustained_head_lift': sustained_head_lift,
        'tongue_protrusion': tongue_protrusion,
        'airway_reflexes': airway_reflexes,
        'after_giving_thorough': after_giving_thorough,
        'post_extubation_pr': post_extubation_pr,
        'post_extubation_rr': post_extubation_rr,
        'post_extubation_spo2': post_extubation_spo2,
        'post_extubation_bp': post_extubation_bp,
    };
    anaesthesia_cheklist_data = JSON.stringify(anaesthesia_cheklist_data);

    var url = base_url + "/ot_schedule/saveAnaesthetiaChecklist";
    var dataparams = {
        'anaesthesia_cheklist_data': anaesthesia_cheklist_data,
        'surgery_detail_id': surgery_detail_id,
        'surgery_head_id': surgery_head_id,
        'patient_id': patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#surgery_checklist").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            if (response != 0) {
                toastr.success('Saved Successfully.');
            } else {
                toastr.error('No internet connection.');
            }
        },
        complete: function () {
            $("#surgery_checklist").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}
function saveImmediatePostOp() {
    var surgery_detail_id = $('#anaesthesia_surgery_req_detail_id').val();
    var surgery_head_id = $('#anaesthesia_surgery_req_head_id').val();
    var patient_id = $('#anaesthesia_patient_id').val();
    if (surgery_detail_id == '') {
        toastr.warning('Select surgery!');
        return false;
    }

    //-----post op assesment----------------------------------------
    var imm_post_op_level_of_consciouness = $('#imm_post_op_level_of_consciouness').val();
    var imm_post_op_coherence = $('#imm_post_op_coherence').val();
    var imm_post_op_reflexes = $('#imm_post_op_reflexes').val();
    var imm_post_op_head_lift = $('#imm_post_op_head_lift').val();
    var imm_post_op_pr = $('#imm_post_op_pr').val();
    var imm_post_op_bp = $('#imm_post_op_bp').val();
    var imm_post_op_rr = $('#imm_post_op_rr').val();
    var imm_post_op_spop2 = $('#imm_post_op_spop2').val();
    var imm_post_op_sa_ea_sensory_level = $('#imm_post_op_sa_ea_sensory_level').val();
    var imm_post_op_motor_level = $('#imm_post_op_motor_level').val();
    var imm_post_op_patient_transferred_to = $('input[name="imm_post_op_patient_transferred_to"]:checked').val();
    var imm_post_op_other_notes_if_any = $('#imm_post_op_other_notes_if_any').val();
    var imm_post_op_monitoring_drug_shifting = $('#imm_post_op_monitoring_drug_shifting').val();
    var imm_post_op_duration_of_shifting = $('#imm_post_op_duration_of_shifting').val();
    var imm_post_op_post_shifting_vitals = $('#imm_post_op_post_shifting_vitals').val();
    var imm_post_op_vitals_stable = $('input[name="imm_post_op_vitals_stable"]:checked').val();
    var imm_post_op_patient_able_to_walk = $('input[name="imm_post_op_patient_able_to_walk"]:checked').val();
    var imm_post_op_vomiting = $('input[name="imm_post_op_vomiting"]:checked').val();
    var imm_post_op_pain = $('input[name="imm_post_op_pain"]:checked').val();
    var imm_post_op_bleeding = $('input[name="imm_post_op_bleeding"]:checked').val();
    var imm_post_op_condition_discharge = $('input[name="imm_post_op_condition_discharge"]:checked').val();
    var imm_post_op_discharge_criteria = $('input[name="imm_post_op_discharge_criteria"]:checked').val();
    var imm_post_op_activity = $('input[name="imm_post_op_activity"]:checked').val();
    var imm_post_op_breathing = $('input[name="imm_post_op_breathing"]:checked').val();
    var imm_post_op_circulation = $('input[name="imm_post_op_circulation"]:checked').val();
    var imm_post_op_consciousness = $('input[name="imm_post_op_consciousness"]:checked').val();
    var imm_post_op_o2_saturation = $('input[name="imm_post_op_o2_saturation"]:checked').val();
    var imm_post_op_total_score = $('#imm_post_op_total_score').val();
    var imm_post_op_doctor_sign = $('#imm_post_op_doctor_sign').val();
    var imm_post_op_date_and_time = $('#imm_post_op_date_and_time').val();

    var post_op_assessment_data = {
        'imm_post_op_level_of_consciouness': imm_post_op_level_of_consciouness,
        'imm_post_op_coherence': imm_post_op_coherence,
        'imm_post_op_reflexes': imm_post_op_reflexes,
        'imm_post_op_head_lift': imm_post_op_head_lift,
        'imm_post_op_pr': imm_post_op_pr,
        'imm_post_op_bp': imm_post_op_bp,
        'imm_post_op_rr': imm_post_op_rr,
        'imm_post_op_spop2': imm_post_op_spop2,
        'imm_post_op_sa_ea_sensory_level': imm_post_op_sa_ea_sensory_level,
        'imm_post_op_motor_level': imm_post_op_motor_level,
        'imm_post_op_patient_transferred_to': imm_post_op_patient_transferred_to,
        'imm_post_op_other_notes_if_any': imm_post_op_other_notes_if_any,
        'imm_post_op_monitoring_drug_shifting': imm_post_op_monitoring_drug_shifting,
        'imm_post_op_duration_of_shifting': imm_post_op_duration_of_shifting,
        'imm_post_op_post_shifting_vitals': imm_post_op_post_shifting_vitals,
        'imm_post_op_vitals_stable': imm_post_op_vitals_stable,
        'imm_post_op_patient_able_to_walk': imm_post_op_patient_able_to_walk,
        'imm_post_op_vomiting': imm_post_op_vomiting,
        'imm_post_op_pain': imm_post_op_pain,
        'imm_post_op_bleeding': imm_post_op_bleeding,
        'imm_post_op_condition_discharge': imm_post_op_condition_discharge,
        'imm_post_op_discharge_criteria': imm_post_op_discharge_criteria,
        'imm_post_op_activity': imm_post_op_activity,
        'imm_post_op_breathing': imm_post_op_breathing,
        'imm_post_op_circulation': imm_post_op_circulation,
        'imm_post_op_consciousness': imm_post_op_consciousness,
        'imm_post_op_o2_saturation': imm_post_op_o2_saturation,
        'imm_post_op_total_score': imm_post_op_total_score,
        'imm_post_op_doctor_sign': imm_post_op_doctor_sign,
        'imm_post_op_date_and_time': imm_post_op_date_and_time,
    };

    post_op_assessment_data = JSON.stringify(post_op_assessment_data);

    var url = base_url + "/ot_schedule/saveImmediatePostOp";
    var dataparams = {
        'post_op_assessment_data': post_op_assessment_data,
        'surgery_detail_id': surgery_detail_id,
        'surgery_head_id': surgery_head_id,
        'patient_id': patient_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#surgery_checklist").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (response) {
            if (response != 0) {
                toastr.success('Saved Successfully.');
            } else {
                toastr.error('No internet connection.');
            }
        },
        complete: function () {
            $("#surgery_checklist").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        }
    });
}





function fetchAnaesthesiaChecKList(surgery_detail_id) {
    var url = base_url + "/ot_checklists/fetchAnaesthesiaChecKList";

    $.ajax({
        url: url,
        type: "POST",
        data: { surgery_detail_id: surgery_detail_id },
        beforeSend: function () {

        },
        success: function (data) {

            if (data == 0) {
                toastr.error('No internet connection.');
            } else {

                var obj = JSON.parse(data);
                pre_anaesthetic_data = null;
                if(obj.length >0 && obj[0].pre_anaesthetic_data !== 'undefined'){
                    pre_anaesthetic_data = obj[0].pre_anaesthetic_data;
                    pre_anaesthetic_data = JSON.parse(pre_anaesthetic_data);
                }

                //-----fetch pre_anaesthetic_data --------------
                if(pre_anaesthetic_data != null){
                    if (typeof pre_anaesthetic_data.name_of_surgery !== 'undefined') {
                        $('#name_of_surgery').val(pre_anaesthetic_data.name_of_surgery);
                    }
                    if (typeof pre_anaesthetic_data.date_and_time_of_surgery !== 'undefined') {
                        $('#date_and_time_of_surgery').val(pre_anaesthetic_data.date_and_time_of_surgery);
                    }
                    if (typeof pre_anaesthetic_data.surgeon_name !== 'undefined') {
                        $('#surgeon_name').val(pre_anaesthetic_data.surgeon_name);
                    }

                    if (typeof pre_anaesthetic_data.history_hypertension !== 'undefined') {
                        $("input[name=history_hypertension][value=" + pre_anaesthetic_data.history_hypertension + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.diabetes !== 'undefined') {
                        $("input[name=diabetes][value=" + pre_anaesthetic_data.diabetes + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.asthma !== 'undefined') {
                        $("input[name=asthma][value=" + pre_anaesthetic_data.asthma + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.epilepsy !== 'undefined') {
                        $("input[name=epilepsy][value=" + pre_anaesthetic_data.epilepsy + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.allergy !== 'undefined') {
                        $("input[name=allergy][value=" + pre_anaesthetic_data.allergy + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.i_h_d !== 'undefined') {
                        $("input[name=i_h_d][value=" + pre_anaesthetic_data.i_h_d + "]").attr('checked', 'checked');
                    }

                    if (typeof pre_anaesthetic_data.previous_surgeory_and_anesthesia !== 'undefined') {
                        $('#previous_surgeory_and_anesthesia').val(pre_anaesthetic_data.previous_surgeory_and_anesthesia);
                    }

                    if (typeof pre_anaesthetic_data.thyroid_condition !== 'undefined') {
                        $("input[name=thyroid_condition][value=" + pre_anaesthetic_data.thyroid_condition + "]").attr('checked', 'checked');
                    }

                    if (typeof pre_anaesthetic_data.hyper_history_of_tb !== 'undefined') {
                        $('#hyper_history_of_tb').val(pre_anaesthetic_data.hyper_history_of_tb);
                    }

                    if (typeof pre_anaesthetic_data.anesthesia_medications !== 'undefined') {
                        $('#anesthesia_medications').val(pre_anaesthetic_data.anesthesia_medications);
                    }

                    if (typeof pre_anaesthetic_data.other_relevant_history !== 'undefined') {
                        $('#other_relevant_history').val(pre_anaesthetic_data.other_relevant_history);
                    }
                    if (typeof pre_anaesthetic_data.icterus !== 'undefined') {
                        $("input[name=icterus][value=" + pre_anaesthetic_data.icterus + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.pallor !== 'undefined') {
                        $("input[name=pallor][value=" + pre_anaesthetic_data.pallor + "]").attr('checked', 'checked');
                    }
                    if (typeof pre_anaesthetic_data.edema !== 'undefined') {
                        $("input[name=edema][value=" + pre_anaesthetic_data.edema + "]").attr('checked', 'checked');
                    }
                    $('#lymphadenopathy').val(pre_anaesthetic_data.lymphadenopathy);
                    $('#airway_dentition').val(pre_anaesthetic_data.airway_dentition);
                    $('#thyroid_desc').val(pre_anaesthetic_data.thyroid_desc);
                    $('#spine').val(pre_anaesthetic_data.spine);
                    $('#mallampattl_grade').val(pre_anaesthetic_data.mallampattl_grade);
                    $('#tm_distance').val(pre_anaesthetic_data.tm_distance);
                    $('#anesthesia_pulse').val(pre_anaesthetic_data.anesthesia_pulse);
                    $('#anesthesia_bp').val(pre_anaesthetic_data.anesthesia_bp);
                    $('#anesthesia_temprature').val(pre_anaesthetic_data.anesthesia_temprature);
                    $('#anesthesia_weight').val(pre_anaesthetic_data.anesthesia_weight);
                    $('#cvs').val(pre_anaesthetic_data.cvs);
                    $('#respiratory_system').val(pre_anaesthetic_data.respiratory_system);
                    $('#git').val(pre_anaesthetic_data.git);
                    $('#cns').val(pre_anaesthetic_data.cns);
                    $('#other_relevant_findings').val(pre_anaesthetic_data.other_relevant_findings);

                    var investigation_data = pre_anaesthetic_data.pre_ans_inv_data;
                    if(investigation_data !== 'undefined'){
                        var pre_ans_inv_colum_count = 0;
                        pre_ans_inv_colum_count = investigation_data.pre_inv_time.length;
                        inv_currnet_col_count = $('#pre_anaesthetic_inv_table tr:first td').length;
                        inv_currnet_col_count = inv_currnet_col_count-1;

                        if(pre_ans_inv_colum_count > inv_currnet_col_count){
                            var inv_col_diff = pre_ans_inv_colum_count-inv_currnet_col_count;
                            for(var l=0; l<inv_col_diff; l++){
                                addNewColumInvestigation();
                            }

                        }


                        if(investigation_data != null){
                            for (var m = 0; m < pre_ans_inv_colum_count; m++) {
                                var n = m+1;
                                $('#pre_inv_time_' + n).val(investigation_data.pre_inv_time[m]);
                                $('#pre_inv_hb_' + n).val(investigation_data.pre_inv_hb[m]);
                                $('#pre_inv_platelets_' + n).val(investigation_data.pre_inv_platelets[m]);
                                $('#pre_inv_blood_sugar_' + n).val(investigation_data.pre_inv_blood_sugar[m]);
                                $('#pre_inv_pp_' + n).val(investigation_data.pre_inv_pp[m]);
                                $('#pre_inv_random_' + n).val(investigation_data.pre_inv_random[m]);
                                $('#pre_inv_hba1c_' + n).val(investigation_data.pre_inv_hba1c[m]);
                                $('#pre_inv_blood_grouping_' + n).val(investigation_data.pre_inv_blood_grouping[m]);
                                $('#pre_inv_bleeding_time_' + n).val(investigation_data.pre_inv_bleeding_time[m]);
                                $('#pre_inv_clotting_time_' + n).val(investigation_data.pre_inv_clotting_time[m]);
                                $('#pre_inv_pt_control_' + n).val(investigation_data.pre_inv_pt_control[m]);
                                $('#pre_inv_pt_test_' + n).val(investigation_data.pre_inv_pt_test[m]);
                                $('#pre_inv_pt_inr_' + n).val(investigation_data.pre_inv_pt_inr[m]);
                                $('#pre_inv_apt_control_' + n).val(investigation_data.pre_inv_apt_control[m]);
                                $('#pre_inv_apt_test_' + n).val(investigation_data.pre_inv_apt_test[m]);
                                $('#pre_inv_apt_others_' + n).val(investigation_data.pre_inv_apt_others[m]);
                                $('#pre_inv_creatine_' + n).val(investigation_data.pre_inv_creatine[m]);
                                $('#pre_inv_blood_urea_' + n).val(investigation_data.pre_inv_blood_urea[m]);
                                $('#pre_inv_cue_' + n).val(investigation_data.pre_inv_cue[m]);
                                $('#pre_inv_na_' + n).val(investigation_data.pre_inv_na[m]);
                                $('#pre_inv_k_' + n).val(investigation_data.pre_inv_k[m]);
                                $('#pre_inv_serum_electrolytes_others_' + n).val(investigation_data.pre_inv_serum_electrolytes_others[m]);
                                $('#pre_inv_hbsag_' + n).val(investigation_data.pre_inv_hbsag[m]);
                                $('#pre_inv_hcv_' + n).val(investigation_data.pre_inv_hcv[m]);
                                $('#pre_inv_hiv_' + n).val(investigation_data.pre_inv_hiv[m]);
                                $('#pre_inv_xray_chest_' + n).val(investigation_data.pre_inv_xray_chest[m]);
                                $('#pre_inv_ecg_' + n).val(investigation_data.pre_inv_ecg[m]);
                                $('#pre_inv_echo_' + n).val(investigation_data.pre_inv_echo[m]);
                                $('#pre_inv_lft_' + n).val(investigation_data.pre_inv_lft[m]);
                                $('#pre_inv_tsh_' + n).val(investigation_data.pre_inv_tsh[m]);
                                $('#pre_inv_t3_' + n).val(investigation_data.pre_inv_t3[m]);
                                $('#pre_inv_t4_' + n).val(investigation_data.pre_inv_t4[m]);
                            }
                        }

                    }

                    $('#other_specific_investigations').val(pre_anaesthetic_data.other_specific_investigations);
                    $('#pre_op_openion').val(pre_anaesthetic_data.pre_op_openion);
                    $('#review_pac').val(pre_anaesthetic_data.review_pac);
                    $('#asa_category').val(pre_anaesthetic_data.asa_category);
                    $('#type_of_anaesthesia_planned').val(pre_anaesthetic_data.type_of_anaesthesia_planned);
                    $('#completed_by').val(pre_anaesthetic_data.completed_by);
                    $('#completed_date').val(pre_anaesthetic_data.completed_date);
                    $('#completed_time').val(pre_anaesthetic_data.completed_time);
                }

                //-----ans record data-----------------------------
                anasthesia_record_data = null;
                if(obj.length >0 && obj[0].anasthesia_record_data !== 'undefined'){
                    anasthesia_record_data = obj[0].anasthesia_record_data;
                    anasthesia_record_data = JSON.parse(anasthesia_record_data);
                }

                if(anasthesia_record_data != null){
                    $('#ans_record_anaesthesiologist_1').val(anasthesia_record_data.ans_record_anaesthesiologist_1);
                    $('#ans_record_anaesthesiologist_2').val(anasthesia_record_data.ans_record_anaesthesiologist_2);
                    $('#ans_record_surgery').val(anasthesia_record_data.ans_record_surgery);
                    $('#ans_record_surgery_date').val(anasthesia_record_data.ans_record_surgery_date);
                    $('#ans_record_surgeon').val(anasthesia_record_data.ans_record_surgeon);

                    if (typeof anasthesia_record_data.ans_record_type_anaesthesia !== 'undefined') {
                        $("input[name=ans_record_type_anaesthesia][value=" + anasthesia_record_data.ans_record_type_anaesthesia + "]").attr('checked', 'checked');
                    }




                    $('#ans_record_nurse').val(anasthesia_record_data.ans_record_nurse);
                    $('#ans_record_premedication').val(anasthesia_record_data.ans_record_premedication);
                    $('#ans_record_assesment_before_induction').val(anasthesia_record_data.ans_record_assesment_before_induction);
                    $('#ans_record_hr').val(anasthesia_record_data.ans_record_hr);
                    $('#ans_record_bp').val(anasthesia_record_data.ans_record_bp);
                    $('#ans_record_spo2').val(anasthesia_record_data.ans_record_spo2);
                    $('#ans_record_rr').val(anasthesia_record_data.ans_record_rr);
                    $('#ans_record_general_condition').val(anasthesia_record_data.ans_record_general_condition);
                    $('#ans_record_iv_access').val(anasthesia_record_data.ans_record_iv_access);
                    $('#ans_record_site').val(anasthesia_record_data.ans_record_site);
                    $('#ans_record_cannula_guage').val(anasthesia_record_data.ans_record_cannula_guage);
                    $('#ans_record_regional_anaesthesia_technique').val(anasthesia_record_data.ans_record_regional_anaesthesia_technique);
                    $('#ans_record_approach').val(anasthesia_record_data.ans_record_approach);
                    $('#ans_record_needle').val(anasthesia_record_data.ans_record_needle);
                    $('#ans_record_catheter_fixed_at').val(anasthesia_record_data.ans_record_catheter_fixed_at);
                    $('#ans_record_space').val(anasthesia_record_data.ans_record_space);
                    $('#ans_record_drug').val(anasthesia_record_data.ans_record_drug);
                    $('#ans_record_onset_of_action').val(anasthesia_record_data.ans_record_onset_of_action);
                    $('#ans_record_sensory_level').val(anasthesia_record_data.ans_record_sensory_level);
                    $('#ans_record_motor_level').val(anasthesia_record_data.ans_record_motor_level);
                    $('#ans_record_general_ans_preoxygenation').val(anasthesia_record_data.ans_record_general_ans_preoxygenation);
                    $('#ans_record_induction').val(anasthesia_record_data.ans_record_induction);
                    $('#ans_record_inhalational').val(anasthesia_record_data.ans_record_inhalational);
                    $('#ans_record_inhalational_other').val(anasthesia_record_data.ans_record_inhalational_other);

                    if (typeof anasthesia_record_data.ans_record_type_inhalation_type !== 'undefined') {
                        $("input[name=ans_record_type_inhalation_type][value=" + anasthesia_record_data.ans_record_type_inhalation_type + "]").attr('checked', 'checked');
                    }
                    $('#ans_record_et_nasal').val(anasthesia_record_data.ans_record_et_nasal);
                    $('#ans_record_cuff').val(anasthesia_record_data.ans_record_cuff);
                    $('#ans_record_other_info').val(anasthesia_record_data.ans_record_other_info);
                    $('#ans_record_ventilation_controlled').val(anasthesia_record_data.ans_record_ventilation_controlled);
                    $('#ans_record_ventilator_mv').val(anasthesia_record_data.ans_record_ventilator_mv);
                    $('#ans_record_rr2').val(anasthesia_record_data.ans_record_rr2);
                    $('#ans_record_tv').val(anasthesia_record_data.ans_record_tv);
                    $('#ans_record_ie_ratio').val(anasthesia_record_data.ans_record_ie_ratio);
                    $('#ans_record_patient_position').val(anasthesia_record_data.ans_record_patient_position);
                    $('#ans_record_technique_applied').val(anasthesia_record_data.ans_record_technique_applied);
                    $('#ans_record_removal_at').val(anasthesia_record_data.ans_record_removal_at);
                    $('#ans_record_other_note').val(anasthesia_record_data.ans_record_other_note);
                    $('#ans_record_time_of_induction').val(anasthesia_record_data.ans_record_time_of_induction);
                    $('#ans_record_time_of_incision').val(anasthesia_record_data.ans_record_time_of_incision);
                }




                //----immediate post op---------------------------

                post_op_assessment_data = null;
                if(obj.length >0 && obj[0].post_op_assessment_data !== 'undefined'){
                    post_op_assessment_data = obj[0].post_op_assessment_data;
                    post_op_assessment_data = JSON.parse(post_op_assessment_data);
                }

                if(post_op_assessment_data !=null){
                    $('#imm_post_op_level_of_consciouness').val(post_op_assessment_data.imm_post_op_level_of_consciouness);
                    $('#imm_post_op_coherence').val(post_op_assessment_data.imm_post_op_coherence);
                    $('#imm_post_op_reflexes').val(post_op_assessment_data.imm_post_op_reflexes);
                    $('#imm_post_op_head_lift').val(post_op_assessment_data.imm_post_op_head_lift);
                    $('#imm_post_op_pr').val(post_op_assessment_data.imm_post_op_pr);
                    $('#imm_post_op_bp').val(post_op_assessment_data.imm_post_op_bp);
                    $('#imm_post_op_rr').val(post_op_assessment_data.imm_post_op_rr);
                    $('#imm_post_op_spop2').val(post_op_assessment_data.imm_post_op_spop2);
                    $('#imm_post_op_sa_ea_sensory_level').val(post_op_assessment_data.imm_post_op_sa_ea_sensory_level);
                    $('#imm_post_op_motor_level').val(post_op_assessment_data.imm_post_op_motor_level);


                    if (typeof post_op_assessment_data.imm_post_op_patient_transferred_to !== 'undefined') {
                        $("input[name=imm_post_op_patient_transferred_to][value=" + post_op_assessment_data.imm_post_op_patient_transferred_to + "]").attr('checked', 'checked');
                    }


                    $('#imm_post_op_other_notes_if_any').val(post_op_assessment_data.imm_post_op_other_notes_if_any);
                    $('#imm_post_op_monitoring_drug_shifting').val(post_op_assessment_data.imm_post_op_monitoring_drug_shifting);
                    $('#imm_post_op_duration_of_shifting').val(post_op_assessment_data.imm_post_op_duration_of_shifting);

                    if (typeof post_op_assessment_data.imm_post_op_vitals_stable !== 'undefined') {
                        $("input[name=imm_post_op_vitals_stable][value=" + post_op_assessment_data.imm_post_op_vitals_stable + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_patient_able_to_walk !== 'undefined') {
                        $("input[name=imm_post_op_patient_able_to_walk][value=" + post_op_assessment_data.imm_post_op_patient_able_to_walk + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_vomiting !== 'undefined') {
                        $("input[name=imm_post_op_vomiting][value=" + post_op_assessment_data.imm_post_op_vomiting + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_pain !== 'undefined') {
                        $("input[name=imm_post_op_pain][value=" + post_op_assessment_data.imm_post_op_pain + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_bleeding !== 'undefined') {
                        $("input[name=imm_post_op_bleeding][value=" + post_op_assessment_data.imm_post_op_bleeding + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_condition_discharge !== 'undefined') {
                        $("input[name=imm_post_op_condition_discharge][value=" + post_op_assessment_data.imm_post_op_condition_discharge + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_discharge_criteria !== 'undefined') {
                        $("input[name=imm_post_op_discharge_criteria][value=" + post_op_assessment_data.imm_post_op_discharge_criteria + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_activity !== 'undefined') {
                        $("input[name=imm_post_op_activity][value=" + post_op_assessment_data.imm_post_op_activity + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_breathing !== 'undefined') {
                        $("input[name=imm_post_op_breathing][value=" + post_op_assessment_data.imm_post_op_breathing + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_circulation !== 'undefined') {
                        $("input[name=imm_post_op_circulation][value=" + post_op_assessment_data.imm_post_op_circulation + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_consciousness !== 'undefined') {
                        $("input[name=imm_post_op_consciousness][value=" + post_op_assessment_data.imm_post_op_consciousness + "]").attr('checked', 'checked');
                    }
                    if (typeof post_op_assessment_data.imm_post_op_o2_saturation !== 'undefined') {
                        $("input[name=imm_post_op_o2_saturation][value=" + post_op_assessment_data.imm_post_op_o2_saturation + "]").attr('checked', 'checked');
                    }
                    $('#imm_post_op_total_score').val(post_op_assessment_data.imm_post_op_total_score);
                    $('#imm_post_op_doctor_sign').val(post_op_assessment_data.imm_post_op_doctor_sign);
                    $('#imm_post_op_date_and_time').val(post_op_assessment_data.imm_post_op_date_and_time);
                }




                //-------anaesthesia_cheklist_data---------------------------------

                var ans_colum_count = 0;
                var col_diff = 0;
                anaesthesia_cheklist_data = null;
                if(obj.length >0 && obj[0].anaesthesia_cheklist_data !== 'undefined'){
                    anaesthesia_cheklist_data = obj[0].anaesthesia_cheklist_data;
                    anaesthesia_cheklist_data = JSON.parse(anaesthesia_cheklist_data);
                    ans_colum_count = anaesthesia_cheklist_data.chk_time.length;

                }

                currnet_col_count = $('#anasthetia_checklist_table tr:first td').length;
                currnet_col_count = currnet_col_count-1;

                if(ans_colum_count > currnet_col_count){
                    var col_diff = ans_colum_count-currnet_col_count;
                    for(var k=0; k<col_diff; k++){
                        addAnsChekListNewColum();
                    }

                }

                if(anaesthesia_cheklist_data != null){
                    for (var i = 0; i < ans_colum_count; i++) {
                        var j = i+1;
                        $('#chk_time_' + j).val(anaesthesia_cheklist_data.chk_time[i]);
                        $('#chk_oxygen_' + j).val(anaesthesia_cheklist_data.chk_oxygen[i]);
                        $('#chk_nitrous_oxide_' + j).val(anaesthesia_cheklist_data.chk_nitrous_oxide[i]);
                        $('#chk_halothane_' + j).val(anaesthesia_cheklist_data.chk_halothane[i]);
                        $('#chk_sevofiurane_' + j).val(anaesthesia_cheklist_data.chk_sevofiurane[i]);
                        $('#chk_anaesthesia_drugs_' + j).val(anaesthesia_cheklist_data.chk_anaesthesia_drugs[i]);
                        $('#chk_anaesthesia_drugs1_' + j).val(anaesthesia_cheklist_data.chk_anaesthesia_drugs1[i]);
                        $('#chk_anaesthesia_drugs2_' + j).val(anaesthesia_cheklist_data.chk_anaesthesia_drugs2[i]);
                        $('#chk_anaesthesia_drugs3_' + j).val(anaesthesia_cheklist_data.chk_anaesthesia_drugs3[i]);
                        $('#chk_anaesthesia_drugs4_' + j).val(anaesthesia_cheklist_data.chk_anaesthesia_drugs4[i]);
                        $('#chk_anaesthesia_drugs5_' + j).val(anaesthesia_cheklist_data.chk_anaesthesia_drugs5[i]);
                        $('#chk_antibiotics1_' + j).val(anaesthesia_cheklist_data.chk_antibiotics1[i]);
                        $('#chk_fluids1_' + j).val(anaesthesia_cheklist_data.chk_fluids1[i]);
                        $('#chk_fluids2_' + j).val(anaesthesia_cheklist_data.chk_fluids2[i]);
                        $('#chk_fluids3_' + j).val(anaesthesia_cheklist_data.chk_fluids3[i]);
                        $('#chk_fluids4_' + j).val(anaesthesia_cheklist_data.chk_fluids4[i]);
                        $('#chk_crystalloids_' + j).val(anaesthesia_cheklist_data.chk_crystalloids[i]);
                        $('#chk_colloids_' + j).val(anaesthesia_cheklist_data.chk_colloids[i]);
                        $('#chk_invasive_lines_' + j).val(anaesthesia_cheklist_data.chk_invasive_lines[i]);
                        $('#chk_invasive_lines2_' + j).val(anaesthesia_cheklist_data.chk_invasive_lines2[i]);
                        $('#chk_blood_and_blood_products_' + j).val(anaesthesia_cheklist_data.chk_blood_and_blood_products[i]);
                        $('#chk_monitoring_sys_' + j).val(anaesthesia_cheklist_data.chk_monitoring_sys[i]);
                        $('#chk_monitoring_dia_' + j).val(anaesthesia_cheklist_data.chk_monitoring_dia[i]);
                        $('#chk_other_monitors_' + j).val(anaesthesia_cheklist_data.chk_other_monitors[i]);
                        $('#chk_other_monitors1_' + j).val(anaesthesia_cheklist_data.chk_other_monitors1[i]);
                        $('#chk_other_monitors2_' + j).val(anaesthesia_cheklist_data.chk_other_monitors2[i]);
                        $('#chk_other_monitors3_' + j).val(anaesthesia_cheklist_data.chk_other_monitors3[i]);
                        $('#chk_urine_output_' + j).val(anaesthesia_cheklist_data.chk_urine_output[i]);
                        $('#chk_blood_loss_' + j).val(anaesthesia_cheklist_data.chk_blood_loss[i]);
                        $('#chk_total_iv_fliud_given_' + j).val(anaesthesia_cheklist_data.chk_total_iv_fliud_given[i]);
                        $('#chk_reversal_' + j).val(anaesthesia_cheklist_data.chk_reversal[i]);
                    }
                    $('#chk_time_of_completion').val(anaesthesia_cheklist_data.chk_time_of_completion);
                    $('#chk_time_of_extubation').val(anaesthesia_cheklist_data.chk_time_of_extubation);
                    $('#chk_other_monitors_text_1').val(anaesthesia_cheklist_data.chk_other_monitors_text_1);
                    $('#chk_other_monitors_text_2').val(anaesthesia_cheklist_data.chk_other_monitors_text_2);
                    $('#chk_other_monitors_text_3').val(anaesthesia_cheklist_data.chk_other_monitors_text_3);

                    if (typeof anaesthesia_cheklist_data.adequate_resp_efforts !== 'undefined') {
                        $("input[name=adequate_resp_efforts][value=" + anaesthesia_cheklist_data.adequate_resp_efforts + "]").attr('checked', 'checked');
                    }
                    if (typeof anaesthesia_cheklist_data.eye_opening !== 'undefined') {
                        $("input[name=eye_opening][value=" + anaesthesia_cheklist_data.eye_opening + "]").attr('checked', 'checked');
                    }
                    if (typeof anaesthesia_cheklist_data.obeying_commands !== 'undefined') {
                        $("input[name=obeying_commands][value=" + anaesthesia_cheklist_data.obeying_commands + "]").attr('checked', 'checked');
                    }
                    if (typeof anaesthesia_cheklist_data.sustained_head_lift !== 'undefined') {
                        $("input[name=sustained_head_lift][value=" + anaesthesia_cheklist_data.sustained_head_lift + "]").attr('checked', 'checked');
                    }
                    if (typeof anaesthesia_cheklist_data.tongue_protrusion !== 'undefined') {
                        $("input[name=tongue_protrusion][value=" + anaesthesia_cheklist_data.tongue_protrusion + "]").attr('checked', 'checked');
                    }
                    if (typeof anaesthesia_cheklist_data.airway_reflexes !== 'undefined') {
                        $("input[name=airway_reflexes][value=" + anaesthesia_cheklist_data.airway_reflexes + "]").attr('checked', 'checked');
                    }
                    if (typeof anaesthesia_cheklist_data.after_giving_thorough !== 'undefined') {
                        $("input[name=after_giving_thorough][value=" + anaesthesia_cheklist_data.after_giving_thorough + "]").attr('checked', 'checked');
                    }


                    $('#post_extubation_pr').val(anaesthesia_cheklist_data.post_extubation_pr);
                    $('#post_extubation_rr').val(anaesthesia_cheklist_data.post_extubation_rr);
                    $('#post_extubation_spo2').val(anaesthesia_cheklist_data.post_extubation_spo2);
                    $('#post_extubation_bp').val(anaesthesia_cheklist_data.post_extubation_bp);


                }



                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            }


        },
        complete: function () {
            $('.timepicker').datetimepicker({
                format: 'hh:mm A',
            });
            $('.datetimepicker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        },
    });
}

function saveOperationNotes(print_status=0) {
    var surgery_detail_id = $('#or_surgery_req_detail_id').val();
    var patient_id = $('#or_patient_id').val();

    var or_surgeon = $('#or_surgeon').val();
    var or_scheduled_date = $('#or_scheduled_date').val();
    var or_anaesthesia_type = $('#or_anaesthesia_type').val();
    var or_assistant = $('#or_assistant').val();
    var or_scheduled_time = $('#or_scheduled_time').val();
    var or_anaesthetist = $('#or_anaesthetist').val();
    var or_nurse = $('#or_nurse').val();
    var or_to = $('#or_to').val();
    var or_operation_type = $('#or_operation_type').val();
    var or_pre_operative_diagnosis = $('#or_pre_operative_diagnosis').val();
    var or_post_operative_diagnosis = $('#or_post_operative_diagnosis').val();
    var or_incision = $('#or_incision').val();
    var or_findings = $('#or_findings').val();
    var or_procedure = $('#or_procedure').val();
    var or_instructions = $('#or_instructions').val();
    var or_drain = $('#or_drain').val();
    var or_procedure_done = $('#or_procedure_done').val();
    var or_hpe = $('input[name="or_hpe"]:checked').val();

    var operation_record_params = {
        'or_surgeon': or_surgeon,
        'or_scheduled_date': or_scheduled_date,
        'or_anaesthesia_type': or_anaesthesia_type,
        'or_assistant': or_assistant,
        'or_scheduled_time': or_scheduled_time,
        'or_anaesthetist': or_anaesthetist,
        'or_nurse': or_nurse,
        'or_to': or_to,
        'or_operation_type': or_operation_type,
        'or_pre_operative_diagnosis': or_pre_operative_diagnosis,
        'or_post_operative_diagnosis': or_post_operative_diagnosis,
        'or_incision': or_incision,
        'or_findings': or_findings,
        'or_instructions': or_instructions,
        'or_procedure': or_procedure,
        'or_drain': or_drain,
        'or_procedure_done': or_procedure_done,
        'or_hpe': or_hpe,
    };

    operation_record_params = JSON.stringify(operation_record_params);


    //--------additional or params----------------------
    var or_surgeon2 = $('#or_surgeon2').val();
    var or_scheduled_date2 = $('#or_scheduled_date2').val();
    var or_anaesthesia_type2 = $('#or_anaesthesia_type2').val();
    var or_assistant2 = $('#or_assistant2').val();
    var or_scheduled_time2 = $('#or_scheduled_time2').val();
    var or_anaesthetist2 = $('#or_anaesthetist2').val();
    var or_nurse2 = $('#or_nurse2').val();
    var or_to2 = $('#or_to2').val();
    var or_operation_type2 = $('#or_operation_type2').val();
    var or_pre_operative_diagnosis2 = $('#or_pre_operative_diagnosis2').val();
    var or_post_operative_diagnosis2 = $('#or_post_operative_diagnosis2').val();
    var or_incision2 = $('#or_incision2').val();
    var or_findings2 = $('#or_findings2').val();
    var or_procedure2 = $('#or_procedure2').val();
    var or_instructions2 = $('#or_instructions2').val();
    var or_drain2 = $('#or_drain2').val();
    var or_procedure_done2 = $('#or_procedure_done2').val();
    var or_hpe2 = $('input[name="or_hpe2"]:checked').val();


    var additional_operation_record_params = {
        'or_surgeon2': or_surgeon2,
        'or_scheduled_date2': or_scheduled_date2,
        'or_anaesthesia_type2': or_anaesthesia_type2,
        'or_assistant2': or_assistant2,
        'or_scheduled_time2': or_scheduled_time2,
        'or_anaesthetist2': or_anaesthetist2,
        'or_nurse2': or_nurse2,
        'or_to2': or_to2,
        'or_operation_type2': or_operation_type2,
        'or_pre_operative_diagnosis2': or_pre_operative_diagnosis2,
        'or_post_operative_diagnosis2': or_post_operative_diagnosis2,
        'or_incision2': or_incision2,
        'or_findings2': or_findings2,
        'or_instructions2': or_instructions2,
        'or_procedure2': or_procedure2,
        'or_drain2': or_drain2,
        'or_procedure_done2': or_procedure_done2,
        'or_hpe2': or_hpe2,
    };

    additional_operation_record_params = JSON.stringify(additional_operation_record_params);




    var url = base_url + "/ot_checklists/saveOperationNotes";

    var dataparams = {
        'patient_id': patient_id,
        'print_status': print_status,
        'surgery_detail_id': surgery_detail_id,
        'operation_record_params': operation_record_params,
        'additional_operation_record_params': additional_operation_record_params,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
            $('#save_operation_notes_btn').attr('disabled', true);
            $('#save_operation_notes_spin').removeClass('fa fa-save');
            $('#save_operation_notes_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != 0) {
                toastr.success('Updated Successfully');
                $('#surgery_checklist_modal').modal('toggle');
                if (list_type == '1') {
                    $('#patient_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '2') {
                    $('#anesthesia_checklist_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '3') {
                    $('#operation_notes_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '4') {
                    $('#gotoSurgeryBilling_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                } else if (list_type == '5') {
                    $('#medicineIndent_btn' + head_id + surgery_id).removeClass('btn-default').addClass('btn-success');
                }

                if(print_status == 1){
                    PrintOperationNotes(surgery_detail_id,patient_id);
                }
            }
        },
        complete: function () {
            $('#save_operation_notes_btn').attr('disabled', false);
            $('#save_operation_notes_spin').removeClass('fa fa-spinner fa-spin');
            $('#save_operation_notes_spin').addClass('fa fa-save');
        },
    });

}

function fetchOperationNotes(surgery_detail_id) {
    var url = base_url + "/ot_checklists/fetchOperationNotes";
    var dataparams = {
        'surgery_detail_id': surgery_detail_id,
    };
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {
        },
        success: function (data) {
            if (data != 0) {

                var or_data = data['resultData'];
                if(or_data !=''){
                    obj = JSON.parse(or_data);
                    // console.log(obj.is_finalized);
                    var operation_record_params = obj[0]['operation_details_json'];
                    operation_record_params = JSON.parse(operation_record_params);
                    $('#or_surgeon').val(operation_record_params['or_surgeon']);
                    $('#or_scheduled_date').val(operation_record_params['or_scheduled_date']);
                    $('#or_anaesthesia_type').val(operation_record_params['or_anaesthesia_type']);
                    $('#or_assistant').val(operation_record_params['or_assistant']);
                    $('#or_scheduled_time').val(operation_record_params['or_scheduled_time']);
                    $('#or_anaesthetist').val(operation_record_params['or_anaesthetist']);
                    $('#or_nurse').val(operation_record_params['or_nurse']);
                    $('#or_to').val(operation_record_params['or_to']);
                    $('#or_operation_type').val(operation_record_params['or_operation_type']);
                    $('#or_pre_operative_diagnosis').val(operation_record_params['or_pre_operative_diagnosis']);
                    $('#or_post_operative_diagnosis').val(operation_record_params['or_post_operative_diagnosis']);
                    $('#or_incision').val(operation_record_params['or_incision']);
                    $('#or_findings').val(operation_record_params['or_findings']);
                    $('#or_instructions').val(operation_record_params['or_instructions']);
                    $('#or_procedure').val(operation_record_params['or_procedure']);
                    $('#or_drain').val(operation_record_params['or_drain']);
                    $('#or_procedure_done').val(operation_record_params['or_procedure_done']);
                    if (typeof operation_record_params['or_hpe'] !== 'undefined') {
                        $("input[name=or_hpe][value=" + operation_record_params['or_hpe'] + "]").attr('checked', 'checked');
                    }

                    if(obj[0].is_finalized == 1){
                        $('#save_operation_notes_btn0').hide();
                        $('#save_operation_notes_btn1').hide();
                    }else{
                        $('#save_operation_notes_btn0').show();
                        $('#save_operation_notes_btn1').show();
                    }


                    //----additional or params-----------------------
                    var additional_operational_data = obj[0]['additional_operational_data'];
                    if(additional_operational_data !=''){
                        $('.additional_operaion_note').show();
                        additional_operational_data = JSON.parse(additional_operational_data);
                        $('#or_surgeon2').val(additional_operational_data['or_surgeon2']);
                        $('#or_scheduled_date2').val(additional_operational_data['or_scheduled_date2']);
                        $('#or_anaesthesia_type2').val(additional_operational_data['or_anaesthesia_type2']);
                        $('#or_assistant2').val(additional_operational_data['or_assistant2']);
                        $('#or_scheduled_time2').val(additional_operational_data['or_scheduled_time2']);
                        $('#or_anaesthetist2').val(additional_operational_data['or_anaesthetist2']);
                        $('#or_nurse2').val(additional_operational_data['or_nurse2']);
                        $('#or_to2').val(additional_operational_data['or_to2']);
                        $('#or_operation_type2').val(additional_operational_data['or_operation_type2']);
                        $('#or_pre_operative_diagnosis2').val(additional_operational_data['or_pre_operative_diagnosis2']);
                        $('#or_post_operative_diagnosis2').val(additional_operational_data['or_post_operative_diagnosis2']);
                        $('#or_incision2').val(additional_operational_data['or_incision2']);
                        $('#or_findings2').val(additional_operational_data['or_findings2']);
                        $('#or_instructions2').val(additional_operational_data['or_instructions2']);
                        $('#or_procedure2').val(additional_operational_data['or_procedure2']);
                        $('#or_drain2').val(additional_operational_data['or_drain2']);
                        $('#or_procedure_done2').val(additional_operational_data['or_procedure_done2']);
                        if (typeof additional_operational_data['or_hpe2'] !== 'undefined') {
                            $("input[name=or_hpe2][value=" + additional_operational_data['or_hpe2'] + "]").attr('checked', 'checked');
                        }
                    }


                    if(obj[0].is_finalized == 1){
                        $('#save_operation_notes_btn0').hide();
                        $('#save_operation_notes_btn1').hide();
                    }else{
                        $('#save_operation_notes_btn0').show();
                        $('#save_operation_notes_btn1').show();
                    }

                }
                var doctor_params = data['doctorData']
                var signatureImage = data['signatureImage'];
                // console.log(doctor_params[0]['doctor_name']);
                // return;


                $('#label_or_dr_name').html(doctor_params[0]['doctor_name']);
                $('#label_or_dr_licenseno').html(doctor_params[0]['license_no']);
                // $('#label_or_dr_sign').html(signatureImage);
                if(signatureImage !=''){
                    var signatureImage = "data:image/jpeg;base64,"+atob(signatureImage);
                    var imgElement = $("<img>");
                    imgElement.attr("src", signatureImage);
                    imgElement.attr("alt", "Signature");
                    $("#dr_signature").attr('src',signatureImage);
                    $("#dr_signature").css('display','block');
                }else{
                    $("#dr_signature").css('display','none');
                }


            }
        },
        complete: function () {

        },
    });
}



$('#service_item').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('service_itemAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#service_item').val();
        if (search_key == "") {
            $('#service_itemAjaxDiv').hide();
            $("#service_item_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var surgery_id = $('#surgery_id').val();
            var param = { _token: token, surgery_id: surgery_id, search_key: search_key, search_key_id: 'service_item' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#service_itemAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#service_itemAjaxDiv").html(html).show();
                    $("#service_itemAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('service_itemAjaxDiv', event);
    }
});


$('#medicine_serach').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('medicine_serachAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#medicine_serach').val();
        if (search_key == "") {
            $('#medicine_serachAjaxDiv').hide();
            $("#medicine_serach_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(medicine_list);
            var medicine_search = $('input[name="medicine_name_search"]:checked').val();
            var param = { _token: token, medicine_search: medicine_search, list_array: listArrayString, search_key: search_key, search_key_id: 'medicine_serach' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#medicine_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#medicine_serachAjaxDiv").html(html).show();
                    $("#medicine_serachAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('medicine_serachAjaxDiv', event);
    }
});



$('#consumables_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('consumables_serachAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#consumables_search').val();
        if (search_key == "") {
            $('#consumables_serachAjaxDiv').hide();
            $("#consumables_search_hidden`").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(consumables_list);
            var medicine_search = $('input[name="medicine_name_search"]:checked').val();
            var param = { _token: token, medicine_search: medicine_search, list_array: listArrayString, search_key: search_key, search_key_id: 'consumables_search' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#consumables_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#consumables_serachAjaxDiv").html(html).show();
                    $("#consumables_serachAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('consumables_serachAjaxDiv', event);
    }
});


$('#equipment_list').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('equipment_listAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#equipment_list').val();
        if (search_key == "") {
            $('#equipment_listAjaxDiv').hide();
            $("#equipment_list_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(equipment_list);
            var param = { _token: token, list_array: listArrayString, search_key: search_key, search_key_id: 'equipment_list' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#equipment_listAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#equipment_listAjaxDiv").html(html).show();
                    $("#equipment_listAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('equipment_listAjaxDiv', event);
    }
});


$('#instrument_list').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('instrument_listAjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var search_key = $('#instrument_list').val();
        if (search_key == "") {
            $('#instrument_listAjaxDiv').hide();
            $("#instrument_list_hidden").val('');
        } else {
            var url = base_url + "/surgery/ajaxSearch";
            var listArrayString = JSON.stringify(instrument_list);
            var param = { _token: token, list_array: listArrayString, search_key: search_key, search_key_id: 'instrument_list' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#instrument_listAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#instrument_listAjaxDiv").html(html).show();
                    $("#instrument_listAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('instrument_listAjaxDiv', event);
    }
});

function ApproveSurgeryChecklist(form_type) {
    var surgery_detail_id_hidden = $('#surgery_detail_id_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (surgery_detail_id_hidden == '') {
        toastr.warning('Select surgery!');
        return false;
    }

    var dataparams = {
        'patient_id': patient_id,
        'surgery_detail_id': surgery_detail_id_hidden,
        'form_type':form_type,
    };
    var url = base_url + "/ot_checklists/ApproveSurgeryChecklist";
    $.ajax({
        url: url,
        type: "POST",
        data: dataparams,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == 1) {
                toastr.success('Approved Successfully');
                $('#surgery_checklist_modal').modal('toggle');
            } else if (data == 2) {
                toastr.warning('Checklist Not saved!');
            } else {
                toastr.error('Error!');
            }
        },
        complete: function () {

        },
    });
}

function addAnsChekListNewColum(){
    var table = $("#anasthetia_checklist_table");
    var row_id = "ans_1";
    var row = $("#" + row_id, table);
    var num_columns = $("td", row).length;
    $('#ans_1').append('<td class="common_td_rules" style="text-align:center;min-width:150px !important;"><input type="text" id="chk_time_'+ num_columns +'" class="form-control timepicker" placeholder="" /></td>');

    $('#ans_2').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_oxygen_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_3').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_nitrous_oxide_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_4').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_halothane_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_5').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_sevofiurane_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_6').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_anaesthesia_drugs_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_7').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_anaesthesia_drugs1_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_8').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_anaesthesia_drugs2_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_9').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_anaesthesia_drugs3_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_10').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_anaesthesia_drugs4_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_11').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_anaesthesia_drugs5_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_12').append('<td class="common_td_rules" style="text-align:center"><select class="form-control select2" id="chk_antibiotics1_'+num_columns+'" style="color:#555555; padding:4px 12px;" name="chk_antibiotics1_'+num_columns+'"><option selected="selected" value=""></option><option value="AMIKAMAC 500MG INJ">AMIKAMAC 500MG INJ</option><option value="AUGMENTIN 1.2G INJ">AUGMENTIN 1.2G INJ</option><option value="CEFAZOLIN 500MG INJECTION(NOSTOF)">CEFAZOLIN 500MG INJECTION (NOSTOF)</option><option value="CEFGLOBE S FORTE 3GM INJ	">CEFGLOBE S FORTE 3GM INJ	</option><option value="CIPROFLOXACIN INJECTION 100ML">CIPROFLOXACIN INJECTION 100ML</option><option value="E STAPH 1GM INJ	">E STAPH 1GM INJ	</option><option value="E STAPH 500MG INJ">E STAPH 500MG INJ</option><option value="KEPIME SB 1.5GM	">KEPIME SB 1.5GM	</option><option value="ZOCEF 1.5GM INJ">ZOCEF 1.5GM INJ</option><option value="ZOCEF 750MG INJ	">ZOCEF 750MG INJ	</option><option value="ZONAMAX ES 1.5GM INJ">ZONAMAX ES 1.5GM INJ</option></select></td>');

    $('#ans_13').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_fluids1_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_14').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_fluids2_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_15').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_fluids3_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_16').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_fluids4_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_17').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_crystalloids_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_18').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_colloids_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_19').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_invasive_lines_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_20').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_invasive_lines2_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_21').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_blood_and_blood_products_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_22').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_monitoring_sys_'+num_columns+'" class="form-control" style="width:55px !important;float:left;" placeholder="" />/<input type="text" id="chk_monitoring_dia_'+num_columns+'" class="form-control" style="width:55px !important;float:right;" placeholder="" /></td>');

    $('#ans_23').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_other_monitors_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_24').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_other_monitors1_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_25').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_other_monitors2_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_26').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_other_monitors3_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_27').append(' <td class="common_td_rules" style="text-align:center"><input type="text" id="chk_urine_output_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_28').append(' <td class="common_td_rules" style="text-align:center"><input type="text" id="chk_blood_loss_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_29').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_total_iv_fliud_given_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#ans_30').append('<td class="common_td_rules" style="text-align:center"><input type="text" id="chk_reversal_'+num_columns+'" class="form-control" placeholder="" /></td>');
    $('.timepicker').datetimepicker({
        format: 'hh:mm A',
    });
}

function addNewColumInvestigation(){
    var table = $("#pre_anaesthetic_inv_table");
    var row_id = "ans_1";
    var row = $("#" + row_id, table);
    var num_columns = $("td", row).length;

    $('#pre_inv_1').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_time_'+num_columns+'" class="form-control datepicker" placeholder="" /></td>');

    $('#pre_inv_2').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_hb_'+num_columns+'" class="form-control" placeholder=""/><br><input type="text" id="pre_tlc_hb_'+num_columns+'" class="form-control" placeholder=""/><br><input type="text" id="pre_inv_platelets_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_3').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_blood_sugar_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_pp_'+num_columns+'" class="form-control" placeholder=""/><br><input type="text" id="pre_inv_random_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_hba1c_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_4').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_blood_grouping_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_bleeding_time_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_clotting_time_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_5').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_pt_control_'+num_columns+'" class="form-control" placeholder="" style=""/><br><input type="text" id="pre_inv_pt_test_'+num_columns+'" class="form-control" placeholder="" style=""/><br><input type="text" id="pre_inv_pt_inr_'+num_columns+'" class="form-control" placeholder="" style=""/><br><input type="text" id="pre_inv_apt_control_'+num_columns+'" class="form-control" placeholder="" style=""/><br><input type="text" id="pre_inv_apt_test_'+num_columns+'" class="form-control" placeholder="" style=""/><br><input type="text" id="pre_inv_apt_others_'+num_columns+'" class="form-control" placeholder="" style=""/></td>');

    $('#pre_inv_6').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_blood_grouping_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_bleeding_time_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_clotting_time_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_7').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_creatine_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_blood_urea_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_cue_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_8').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_na_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_k_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_serum_electrolytes_others_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_9').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_hbsag_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_hcv_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_hiv_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_xray_chest_'+num_columns+'" class="form-control" placeholder="" /></td>');

    $('#pre_inv_10').append('<td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_ecg_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_echo_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_lft_'+num_columns+'" class="form-control" placeholder="" /><br></td>');

    $('#pre_inv_11').append(' <td class="common_td_rules" style="text-align:center;"><input type="text" id="pre_inv_tsh_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_t3_'+num_columns+'" class="form-control" placeholder="" /><br><input type="text" id="pre_inv_t4_'+num_columns+'" class="form-control" placeholder="" /><br></td>');

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
}

function showAdditionalOrNotes(){
    $('.additional_operaion_note').toggle();
}
