$(document).ready(function () {
    base_url = $('#base_url').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    setTimeout(function () {
        $(".select2").select2();
        searchQuotation();
    }, 800);

});
base_url = '';

function searchNewVendor() {
    var token = $('#token_hiddendata').val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var vendor_name = $('#searchnewvendor').val();
        if (vendor_name == "") {
            $('#ajaxVendorSearchBox').hide();
            $("#vendoritem_id_hidden").val(0);
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, vendor_name: vendor_name, row_id: 1 },
                beforeSend: function () {
                    $("#ajaxVendorSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxVendorSearchBox").html(html).show();
                    $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}


function searchContractNo() {
    var token = $('#token_hiddendata').val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxContractSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var contractno = $('#searchContract').val();
        if (contractno == "") {
            $('#ajaxContractSearchBox').hide();
            $("#contract_id_hidden").val(0);
        } else {
            var url = base_url + "/quotation/contractno_search";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, contractno: contractno },
                beforeSend: function () {
                    $("#ajaxContractSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxContractSearchBox").html(html).show();
                    $("#ajaxContractSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown('ajaxContractSearchBox', event);
    }
}


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#searchnewvendor').val(vendor_name);
    $('#vendoritem_id_hidden').val(id);
    $('#ajaxVendorSearchBox').hide();
}

function fillcontract_no(id, contract_no) {
    vendor_name = htmlDecode(contract_no);
    $('#searchContract').val(contract_no);
    $('#contract_id_hidden').val(id);
    $('#ajaxContractSearchBox').hide();
}

function searchQuotation() {
    var contract_id = $('#contract_id_hidden').val();
    var item_supplier = $('#item_supplier').val();
    var to_date = $('#to_date').val();
    var from_date = $('#from_date').val();
    var vendor_id = $('#vendoritem_id_hidden').val();
    var status = $('#status').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/searchVendorContractList";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, contract_id: contract_id, item_supplier: item_supplier, vendor_id: vendor_id, to_date: to_date, from_date: from_date, status: status },
        beforeSend: function () {
            $('#searchQuotationBtn').attr('disabled', true);
            $('#searchQuotationSpin').removeClass('fa fa-search');
            $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchQuotationBtn').attr('disabled', false);
            $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchQuotationSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function listContractItems(head_id) {
    var quotation_no = $('#contract_no_data' + head_id).html();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getVendorContractItems";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, contract_id: head_id, from_type: 2 },
        beforeSend: function () {
            $('#quotationListHeader').html(quotation_no);
            $('#listContractItemsBtn' + head_id).attr('disabled', true);
            $('#listContractItemsSpin' + head_id).removeClass('fa fa-list');
            $('#listContractItemsSpin' + head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#quotationListDiv").html(data.html);
            $("#quotationListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#listContractItemsBtn' + head_id).attr('disabled', false);
            $('#listContractItemsSpin' + head_id).removeClass('fa fa-spinner fa-spin');
            $('#listContractItemsSpin' + head_id).addClass('fa fa-list');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function addWindowLoad(to_url, rfq_id) {
    var url = base_url + "/quotation/" + to_url + '/' + rfq_id;
    document.location.href = url;
}
