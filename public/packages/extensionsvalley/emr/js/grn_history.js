$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    getReportData();
});

$(document).on("click", function (event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $('#base_url').val();
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});


function getFiltersData(flag) {
    var grn_number = $('#grn_number_hidden').val();
    var invoice_from = $('#invoice_date_from').val();
    var invoice_to = $('#invoice_date_to').val();
    var created_from = $('#created_date_from').val();
    var created_to = $('#created_date_to').val();
    var approved_date_from = $('#approved_date_from').val();
    var approved_date_to = $('#approved_date_to').val();
    var vendor_invoice_no = $('#vendor_invoice_no').val();
    var vendor_id = $('#vendor_id').val();
    var to_location = $('#to_location').val();
    var grn_type = $('#grn_type').val();
    var created_by = $('#created_by_hidden').val();
    var approved_by = $('#approved_by_hidden').val();
    var purchase_category = $('#purchase_category').val();
    var grn_status = $('#status').val();

    if (flag == 1 && (!created_from || !created_to) && (!invoice_from || !invoice_to) && (!approved_date_from || !approved_date_to)) {
        flag = 0;
    } else {
        flag = 1;
    }

    var param = {
        grn_number: grn_number, invoice_from: invoice_from, invoice_to: invoice_to, approved_date_from: approved_date_from,
        approved_date_to: approved_date_to, created_from: created_from, created_to: created_to, vendor_invoice_no: vendor_invoice_no,
        vendor_id: vendor_id, to_location: to_location, grn_type: grn_type, created_by: created_by, approved_by: approved_by,
        purchase_category: purchase_category, grn_status: grn_status
    };
    if (flag == 1) {
        return param;
    } else {
        return false;
    }
}


function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
    getReportData();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
//------------------------------------------------------------releative select box--------------------------------------------------------

function getReportData() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/get_grn_history";
    var param = getFiltersData(0);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#search_results').attr('disabled', true);
            $('#search_results_Spin').removeClass('fa fa-search');
            $('#search_results_Spin').addClass('fa fa-spinner fa-spin');
            $('#pagination_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#pagination_data').html(html);
        },
        complete: function () {
            $('#pagination_data').LoadingOverlay("hide");
            $('#search_results').attr('disabled', false);
            $('#search_results_Spin').removeClass('fa fa-spinner fa-spin');
            $('#search_results_Spin').addClass('fa fa-search');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });

}

function getGrnConsolidated() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/getGrnConsolidated";
    var param = getFiltersData(1);
    if (param) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#getConsolidatedBtn').attr('disabled', true);
                $('#getConsolidatedSpin').removeClass('fa fa-list');
                $('#getConsolidatedSpin').addClass('fa fa-spinner fa-spin');
                $('#getGrnDetalisHeader').html('Consolidated Data');
            },
            success: function (data) {
                $('#ResultDataContainer').html(data);
                $("#getGrnDetalisModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $('#ResultDataContainer').css('display', 'block');
            },
            complete: function () {
                $('#getConsolidatedBtn').attr('disabled', false);
                $('#getConsolidatedSpin').removeClass('fa fa-spinner fa-spin');
                $('#getConsolidatedSpin').addClass('fa fa-list');
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
            }

        });
    } else {
        toastr.warning("Please Enter Invoice/Created/Approved From and To Date");
    }
}


function getGrnDetalis() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/getGrnDetalis";
    var param = getFiltersData(1);
    if (param) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#grnDetailsBtn').attr('disabled', true);
                $('#grnDetailsSpin').removeClass('fa fa-list');
                $('#grnDetailsSpin').addClass('fa fa-spinner fa-spin');
                $('#getGrnDetalisHeader').html('Purchase History Report');
            },
            success: function (data) {
                $('#ResultDataContainer').html(data);
                $("#getGrnDetalisModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $('#ResultDataContainer').css('display', 'block');
            },
            complete: function () {
                $('#grnDetailsBtn').attr('disabled', false);
                $('#grnDetailsSpin').removeClass('fa fa-spinner fa-spin');
                $('#grnDetailsSpin').addClass('fa fa-list');
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    } else {
        toastr.warning("Please Enter Invoice/Created/Approved From and To Date");
    }

}

function getGrnAnalysis() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/getGrnAnalysis";
    var param = getFiltersData(1);
    if (param) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#getGrnAnalysisBtn').attr('disabled', true);
                $('#getGrnAnalysisSpin').removeClass('fa fa-list');
                $('#getGrnAnalysisSpin').addClass('fa fa-spinner fa-spin');
                $('#getGrnDetalisHeader').html('GRN Analysis Report');
            },
            success: function (data) {
                $('#ResultDataContainer').html(data);
                $("#getGrnDetalisModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $('#ResultDataContainer').css('display', 'block');
            },
            complete: function () {
                $('#getGrnAnalysisBtn').attr('disabled', false);
                $('#getGrnAnalysisSpin').removeClass('fa fa-spinner fa-spin');
                $('#getGrnAnalysisSpin').addClass('fa fa-list');
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    } else {
        toastr.warning("Please Enter Invoice/Created/Approved From and To Date");
    }
}


function getSearchPreview() {
    var base_url = $('#base_url').val();
    var url = base_url + "/purchase/getGrnListPreview";
    var param = getFiltersData(1);
    if (param) {
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#getlistpreviewBtn').attr('disabled', true);
                $('#getlistpreviewSpin').removeClass('fa fa-eye');
                $('#getlistpreviewSpin').addClass('fa fa-spinner fa-spin');
                $('#getGrnDetalisHeader').html('GRN Preview');
            },
            success: function (data) {
                $('#ResultDataContainer').html(data);
                $("#getGrnDetalisModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $('#ResultDataContainer').css('display', 'block');
            },
            complete: function () {
                $('#getlistpreviewBtn').attr('disabled', false);
                $('#getlistpreviewSpin').removeClass('fa fa-spinner fa-spin');
                $('#getlistpreviewSpin').addClass('fa fa-eye');
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    } else {
        toastr.warning("Please Enter Invoice/Created/Approved From and To Date");
    }
}

function search_clear() {
    $('#grn_number').val('');
    $('#grn_number_hidden').val('');
    $('#invoice_date_from').val('');
    $('#invoice_date_to').val('');
    $('#created_date_from').val('');
    $('#created_date_to').val('');
    $('#approved_date_from').val('');
    $('#approved_date_to').val('');
    $('#vendor_invoice_no').val('');
    $('#vendor_id').val('').select2();
    $('#to_location').val('').select2();
    $('#grn_type').val('').select2();
    $('#created_by').val('');
    $('#created_by_hidden').val('');
    $('#approved_by').val('');
    $('#approved_by_hidden').val('');
    $("#purchase_category").val([]).change().select2();
    $("#status").val([]).change().select2();
    getReportData();
}

