function addStockRequestNew() {
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addDeptStockRequests";
    var action_url = main_uri + route_path;
    document.location.href = action_url;
}

function stockItemEditLoadData(list, stock_id) {
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addDeptStockRequests/" + stock_id;
    var action_url = main_uri + route_path;
    document.location.href = action_url;
}

function reload_fn() {
    window.location.reload();
}
