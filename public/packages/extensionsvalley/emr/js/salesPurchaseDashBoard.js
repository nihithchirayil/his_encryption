$(document).ready(function () {
    getCustomDates(1)
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});
var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();
var company_code = $('#company_code').val();

function getAllGraphs() {
    setTimeout(function () {
        getAllTotalSalesPurchaseCollection('sales');
    }, 200);
    setTimeout(function () {
        getAllTotalSalesPurchaseCollection('purchase');
    }, 400);
    setTimeout(function () {
        getLocationwiseCollection();
    }, 600);
    setTimeout(function () {
        getPharamacyDeatils('supplier_deatils_value', 0);
    }, 800);
    setTimeout(function () {
        getPharamacyDeatils('supplier_deatils_number', 0);
    }, 1000);
    setTimeout(function () {
        getPharamacyDeatils('expiry_item_deatils', 0);
    }, 1200);
    setTimeout(function () {
        getPharamacyDeatils('fast_moving_items', 1);
    }, 1400);
    setTimeout(function () {
        getPharamacyDeatils('nearby_expiry_items', 1);
    }, 1600);
}

function getCustomDates(from_type) {
    var date_data = $('#date_datahidden').val();
    var obj = JSON.parse(date_data);
    $('#daterange_hidden').val(from_type);
    if (from_type == '1') {
        $('#from_date').val(obj.current_date);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.current_date);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Today');
        getAllGraphs();
    } else if (from_type == '2') {
        $('#from_date').val(obj.yeasterday);
        $('#to_date').val(obj.yeasterday);
        $('#from_datadis').html(obj.yeasterday);
        $('#to_datadis').html(obj.yeasterday);
        $('#range_typedata').html('Yesterday');
        getAllGraphs();
    } else if (from_type == '3') {
        $('#from_date').val(obj.week_last_sunday);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.week_last_sunday);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Week Till Date');
        getAllGraphs();
    } else if (from_type == '4') {
        $('#from_date').val(obj.lastweek_from);
        $('#to_date').val(obj.lastweek_to);
        $('#from_datadis').html(obj.lastweek_from);
        $('#to_datadis').html(obj.lastweek_to);
        $('#range_typedata').html('Last Week');
        getAllGraphs();
    } else if (from_type == '5') {
        $('#from_date').val(obj.firstday_thismonth);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.firstday_thismonth);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Month Till Date');
        getAllGraphs();
    } else if (from_type == '6') {
        $('#from_date').val(obj.firstday_lastmonth);
        $('#to_date').val(obj.lastday_lastmonth);
        $('#from_datadis').html(obj.firstday_lastmonth);
        $('#to_datadis').html(obj.lastday_lastmonth);
        $('#range_typedata').html('Last Month');
        getAllGraphs();
    } else if (from_type == '7') {
        $("#customdatapopmodel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

}

function getCustomDateRange() {
    var from_date = $('#from_date').val();
    var todate = $('#to_date').val();
    $('#range_typedata').html('Custom Date Range Selection');
    $('#from_datadis').html(from_date);
    $('#to_datadis').html(todate);
    getAllGraphs();
    $("#customdatapopmodel").modal('toggle');
}


function getAllTotalSalesPurchaseCollection(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var date_range = $('#daterange_hidden').val();
    var url = base_url + "/dashboard/getAllTotalSalesPurchaseCollection";
    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        from_type: from_type,
        date_range: date_range
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#totalCollection" + from_type).LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            if (from_type == 'sales') {
                $('#total_collection1').html(data.total_collection);
                $('#total_collection2').html(data.sales_cost);
                $('#total_collection3').html(data.margin);
            } else if (from_type == 'purchase') {
                $('#total_collection4').html(data.company_credit);
                $('#total_collection5').html(data.discount_amt);
                $('#total_collection6').html(data.pharmacy_purchase);
            }


        },
        complete: function () {
            $("#totalCollection" + from_type).LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getLocationwiseCollection() {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/dashboard/getLocationwiseCollection";
    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#collection_deatils").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });

        },
        success: function (data) {
            $('#collection_deatils').html(data);
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1500);
        },
        complete: function () {
            $("#collection_deatils").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getFullDataDrillDown(header_name, from_type, day_status) {
    var day_count = 0;
    if (parseInt(day_status) == 1) {
        day_count = $('#' + from_type + 'no').val();
    }
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/dashboard/getPharamacyDeatils";
    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        from_type: from_type,
        day_count: day_count,
        limit: 0,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getCountsInDetailModelDiv").html('');
            $('#listbtn' + from_type).attr('disabled', true);
            $('#listspin' + from_type).removeClass('fa fa-bar-chart');
            $('#listspin' + from_type).addClass('fa fa-spinner fa-spin');
            $('#getCountsInDetailModelHeader').html(header_name);
        },
        success: function (data) {
            $("#getCountsInDetailModelDiv").html(data);
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $("#getCountsInDetailModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);
        },
        complete: function () {
            $('#listbtn' + from_type).attr('disabled', false);
            $('#listspin' + from_type).removeClass('fa fa-spinner fa-spin');
            $('#listspin' + from_type).addClass('fa fa-bar-chart');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}





function getSalesFullTotal(header_name, from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/dashboard/getCreditDiscoutPurchaseDetailList";
    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        from_type: from_type,
        limit: 0,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getCountsInDetailModelDiv").html('');
            $('#listbtn' + from_type).attr('disabled', true);
            $('#listspin' + from_type).removeClass('fa fa-bar-chart');
            $('#listspin' + from_type).addClass('fa fa-spinner fa-spin');
            $('#getCountsInDetailModelHeader').html(header_name);

        },
        success: function (data) {
            $("#getCountsInDetailModelDiv").html(data);
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $("#getCountsInDetailModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);
        },
        complete: function () {
            $('#listbtn' + from_type).attr('disabled', false);
            $('#listspin' + from_type).removeClass('fa fa-spinner fa-spin');
            $('#listspin' + from_type).addClass('fa fa-bar-chart');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getPharamacyDeatils(from_type, day_status) {
    var day_count = 0;
    if (parseInt(day_status) == 1) {
        day_count = $('#' + from_type + 'no').val();
    }
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/dashboard/getPharamacyDeatils";
    var param = {
        _token: token,
        from_date: from_date,
        to_date: to_date,
        from_type: from_type,
        day_count: day_count,
        limit: 10,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#" + from_type).LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });

        },
        success: function (data) {
            $('#' + from_type).html(data);
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);
        },
        complete: function () {
            $("#" + from_type).LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
