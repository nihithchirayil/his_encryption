var limit = 20;
var offset=0;
var offset1=0;
var offset2=0;
var total_rec=0
var total_rec1=0
var total_rec2=0;
var current_latitude='';
var current_longitude='';
$(document).ready(function () {
    getLocation();
    $(function () {
        $("#progressbar li:first-child a").tab("show");
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    var table_company_code = $("#table_company_code").val();
    if (table_company_code == 'DAYAH') {
        $('.datepicker').datetimepicker({
            format: 'MMM-DD-YYYY',
            minDate: '04-01-2022',
        });
    } else {
        $('.datepicker').datetimepicker({
            format: 'MMM-DD-YYYY',
        });
    }
    offset=0;
    offset1=0;
    offset2=0;
    searchList(15,0);


});
$("#progressbar li").click(function (e) {
    $(this).find('a').tab('show');
});
function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } 
  }
  
  function showPosition(position) {
   
    current_latitude=position.coords.latitude;
    current_longitude=position.coords.longitude;
  }


function searchList(limit = 15,from_type=0) {
    if(from_type==4){
        offset=0;
        offset1=0;
        offset2=0;
    }
    $('.ajaxSearchBox').hide();
    var url = $('#base_url').val()+'/accounts/vendorOnlinePaymentData';
    var ledger_id_head = $("#ledger_name_hidden").val();
    var from_date_invoice = $("#from_date").val();
    var to_date_invoice = $("#to_date").val();
    var param1 = {ledger_id: ledger_id_head, bill_date_from: from_date_invoice,from_type:from_type,
        bill_date_to: to_date_invoice, limit: limit, offset: offset,offset1:offset1,offset2:offset2};
    $.ajax({
        type: "POST",
        url: url,
        data: param1,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner');
            $('#loadSpin').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (msg) {
            var obj = JSON.parse(msg);
            console.log(obj);
            msg = obj.viewData ?? '';
            msg1 = obj.viewData1 ?? '';
            msg2 = obj.viewData2 ?? '';
            if(obj.total_rec){
                total_rec = obj.total_rec;
            }
            if(obj.total_rec1){
                total_rec1 = obj.total_rec1;
            }
            if(obj.total_rec2){
                total_rec2 = obj.total_rec2; 
            }
            if (offset == 0 && obj.viewData) {
               
                    $('#common_list_div').html(msg);
                }else if(obj.viewData){
                    $('#receipt_table').append(msg);
                }
            if (offset1 == 0 && obj.viewData1) {
               
                    $('#common_list_div1').html(msg1);
                }else if(obj.viewData1){
                    $('#receipt_table1').append(msg1);
                }
            if (offset2 == 0 && obj.viewData2) {
               
                $('#common_list_div2').html(msg2);
                }else if(obj.viewData2){
                    $('#receipt_table2').append(msg2);

                }
               
               
            
        },
        complete: function () {
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner');
            $('#searchdataspin').addClass('fa fa-search');
            $('#loadSpin').LoadingOverlay("hide");


            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');


        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#ledger_name_hidden').val() != "") {
            $('#ledger_name_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var datastring = '';



        if (search_key == "") {
            $("#AjaxDiv").html("");
        } else {
            var url = $('#base_url').val()+'/accounts/searchLedgerMaster';
            $.ajax({
                type: "GET",
                url: url,
                data: 'type=1&is_header=1&ledger_desc=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#AjaxDiv").html("No results found!").show();

                        $("#AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#AjaxDiv").html(html).show();
                        $("#AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down('AjaxDiv', event);
    }
});

function fillParty(e,ledger_name,id,ledger_group_id,ledger_code){
    $('#ledger_name_hidden').val(id);
    $('#ledger_name').val(ledger_name);
    $("#AjaxDiv").hide();

}  

function payDetails(e,amount,vendor_id,detail_id,type=1){
    var url = $('#base_url').val()+'/accounts/onlinePaymentDetails';
    $.ajax({
        type: "POST",
        url: url,
        data: {vendor_id:vendor_id,detail_id:detail_id},
        beforeSend: function () {
           $('.onlinePayment').attr('disabled',true);
           $(e).find('i').removeClass('fa fa-money');   
           $(e).find('i').addClass('fa fa-spin fa-spinner');   
        },
        success: function (html) {
            $('#readyToSave').html('');
            if (html) {
           
            $('#setTheHeader').html(html.ledger_name);
            $('#paymentDetailsBody').html(html.data);
            $('#paymentDetailsModal').modal('show');
            if(type==2){
                $('#readyToSave').append('<button type="button" id="savePayDetails" class="btn btn-warning" onclick="onlinePaymentRequest(this,'+amount+','+vendor_id+','+detail_id+','+type+')" ><i class="fa fa-money"></i> Cancel</button>')
                $('#add_transfer_details').hide();
                $('#paymentDetailsBody').css('height','146px');

            }else{
                $('#readyToSave').append('<button type="button" id="savePayDetails" class="btn btn-warning" onclick="onlinePaymentRequest(this,'+amount+','+vendor_id+','+detail_id+','+type+')" ><i class="fa fa-money"></i> Pay('+amount+')</button>')
                $('#add_transfer_details').show();
                $('#paymentDetailsBody').css('height','232px');


            }
            } 

        },

        complete: function () {
            $('.onlinePayment').attr('disabled',false);
            $(e).find('i').removeClass('fa fa-spin fa-spinner');   

            $(e).find('i').addClass('fa fa-money');   
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        },

    });
}
function paidDetails(e,vendor_id,detail_id){
    var url = $('#base_url').val()+'/accounts/onlinePaymentPaidDetails';
    $.ajax({
        type: "POST",
        url: url,
        data: {vendor_id:vendor_id,detail_id:detail_id},
        beforeSend: function () {
           $('#paidView').attr('disabled',true);
           $(e).find('i').removeClass('fa fa-money');   
           $(e).find('i').addClass('fa fa-spin fa-spinner');   
        },
        success: function (html) {
            $('#readyToSave').html('');
            if (html) {
            $('#paymentDetailsBody').css('height','274px');
            $('#setTheHeader').html(html.ledger_name);
            $('#paymentDetailsBody').html(html.data);
            $('#paymentDetailsModal').modal('show');
           
            } 

        },

        complete: function () {
            $('#paidView').attr('disabled',false);
            $(e).find('i').removeClass('fa fa-spin fa-spinner');   

            $(e).find('i').addClass('fa fa-money');   
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        },

    });
}

function onlinePaymentRequest(e,amount,vendor_id,detail_id,type){
  
    var txt='';
    if(type==2){
        txt='Cancel';
        btn_txt='Yes';
    }else{
        if(checkValidation()==false){
            return;
        }
        txt='Pay';
        btn_txt='Pay';
    }
    var remitter=$('#remitter').val();
    var beneficiary=$('#beneficiary').val();
    var type_of_pay=$('#type_of_pay').val();
    var payment_remarks=$('#payment_remarks').val();
    bootbox.confirm({
        message: "Are You Sure You want to "+txt+" ?",
        buttons: {
            confirm: {
                label: btn_txt,
                className: "btn-warning",
                default: "true",
            },
            cancel: {
                label: "No",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                var url = $('#base_url').val()+'/accounts/onlinePaymentRequest';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { payment_remark:payment_remarks,latitude:current_latitude,longitude:current_longitude,amount:amount,vendor_id:vendor_id,detail_id:detail_id,type:type,remitter:remitter,beneficiary:beneficiary,type_of_pay:type_of_pay},
                    beforeSend: function () {
                       $('#savePayDetails').attr('disabled',true);
                       $(e).find('i').removeClass('fa fa-money');   
                       $(e).find('i').addClass('fa fa-spin fa-spinner');   
                    },
                    success: function (html) {
                        if (html == 1) {
                          toastr.success('Success.');
                          $('#paymentDetailsModal').modal('hide');
                          offset=0;
                          offset1=0;
                          offset2=0;
                          searchList(15,0);


                        }else if(html==2){
                            toastr.warning('On transition.');
                        } 
    
                    },
    
                    complete: function () {
                        $('#savePayDetails').attr('disabled',false);
                        $(e).find('i').removeClass('fa fa-spin fa-spinner');   
                        $(e).find('i').addClass('fa fa-money');   
                    },
                    error: function () {
                        Command: toastr["error"]("Network Error!");
                        return;
                    },
    
                });
            }
        },
    });
}

function checkValidation(){
    var remitter=$('#remitter').val();
    var beneficiary=$('#beneficiary').val();
    var type_of_pay=$('#type_of_pay').val();
    var flag=true;
    var payment_remarks=$('#payment_remarks').val();
    if(!remitter){
         toastr.warning('please select remitter.');
         flag=false;
    }else if(!beneficiary){
        toastr.warning('please select beneficiary.');
        flag=false;
    }else if(!type_of_pay){
        toastr.warning('please select payment type.');
        flag=false;
    }else if(!payment_remarks){
        toastr.warning('please add remarks.');
        flag=false;
    }

    return flag;
}
function resetPage(){
    var current_date=$('#current_date').val();
    $('.reset').val('');
    $('#to_date').val(current_date);
    $('#from_date').val(current_date);
    searchList(20,4)
}
