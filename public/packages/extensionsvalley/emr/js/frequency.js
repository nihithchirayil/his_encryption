
function InsertFrequency(e){ 

if($('input[name="chk[]"]:checked').length > 0){
    var value = e.value;
    var frequency_id =  e.id;
    var get_freq_name = ($("#"+frequency_id).attr('class'));
    var user_id = $("input[name=search_user_id_hidden]").val();
   
       if(user_id == 0){
          Command: toastr["error"]("Select User.");
      }else{
                  let url = $('#base_url').val();
                  let _token = $('#c_token').val();
                $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
                  $.ajax({
                      type: "POST",
                      async:false,
                      url: url + "/frequency/update_frequency",
                      data: {

                         frequency_id: frequency_id,
                            user_id: user_id,
                          _token: _token
                      },
                      beforeSend: function () {

                      },
                       success: function (data) {

                           if (data.status == 1 ) { 

                                   if($('input[name="chk[]"]:checked').length > 0){
                                         
                                      $('#selected_freq_table').append("<tr style='padding:0px !important;height:27px;'><td style='text-align:left;padding:0px !important;'>&nbsp;&nbsp;"+get_freq_name+" </td><td style='padding:0px !important;text-align:center'>"+value+"</td><td  style='text-align: center;padding:0px !important;'><input type='hidden'  name='frequency_id[]' value="+data.id+"><button type='button' style='padding-top:3px;' class='btn btn-sm btn-danger delete-frequency-row'><i class='fa fa-trash'></i> Remove</button></td></tr>");
                                      $('#hiderow').hide();
                                      Command: toastr["success"]("Frequency Updated.");
                                      }
                                             
                                    }
                                    else{
                                       Command: toastr["error"]("Frequency Already Exist.");
                                    }

                      },
                complete: function () {
                   $("body").LoadingOverlay("hide");
                }
                      });
                }
              }
    }




function save_frequency_details(){

    var frequency_name = $('#frequency_name').val();
    var frequency_value = $('#frequency_value').val();
    var user_id = $("input[name=search_user_id_hidden]").val();


            let url = $('#base_url').val();
            let _token = $('#c_token').val();

     if (!(frequency_value >= 48 || frequency_value <= 57)) {
            Command: toastr["error"]("Only Numbers Allowed.");
            return false;
          }
          else if(frequency_name =='' || frequency_value =='' || user_id==''){
             Command: toastr["error"]("Please Fill All Details.");
          }
          else{
           $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
            $.ajax({
                type: "POST",
                async:false,
                url: url + "/frequency/save_frequency",
                data: {
                   frequency_name: frequency_name,
                   frequency_value: frequency_value,
                   user_id: user_id,
                    _token: _token
                },
                beforeSend: function () {

                },
                 success: function (data) {
                     if (data.status == 1 ) {

                $('#common_freq_table').append("<tr  style='padding:0px !important;height:27px;' id="+frequency_name+"><td style='text-align:left;padding:0px !important;'>&nbsp; <input id="+data.frequency_id+" name='chk[]' value="+frequency_name+" type='checkbox' data-id='undefined'>&nbsp;&nbsp; <label>"+frequency_name+"</label></td><td style='padding:0px !important;text-align:center;'>"+frequency_value+"</td></tr>");
                $('#selected_freq_table').append("<tr style='padding:0px !important;height:27px;'><td style='text-align:left;padding:0px !important;'> &nbsp;&nbsp;"+frequency_name+"<input type='hidden' name='frequency_id[]' value="+data.id+"></td><td style='padding:0px !important;text-align:center'>"+frequency_value+"</td><td style='text-align: center;padding:0px !important;'><button type='button' style='padding-top:3px;' class='btn btn-sm btn-danger delete-frequency-row'><i class='fa fa-trash'></i> Remove</button></td></tr>");
               
                  Command: toastr["success"]("Frequency Saved.");
                  $('#frequency_name').val('');
                  $('#frequency_value').val('');
                  $('#selected_freq_table #hiderow').hide();
                 }
                  else{
                        Command: toastr["error"]("Frequency Already Exist.");
                                }
                  },
              complete: function () {
                 $("body").LoadingOverlay("hide");
              }
                });
        }
    }


$('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });





//user search
var timeout = null;
var last_search_string = '';
$(document).on('keyup', 'input[name="search_user"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val();
    var user_list = $('.user-list-div');

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        $(user_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/frequency/user-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: search_string,
                   
                },
                beforeSend: function () {
                    $('#UserTable > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {
                    //alert(data);
                    let response = data;
                    let res_data = "";


                    var search_list = $('#ListUserSearchData');


                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let user_name = response[i].user_name;
                             let user_id = response[i].user_id;

                            res_data += '<tr><td>' + user_name + '</td><input type="hidden" name="list_user_name_hid[]" id="list_user_name_hid-' + i + '" value="' + user_name + '"><input type="hidden" name="list_user_id_hid[]" id="list_user_id_hid-' + i + '" value="' + user_id + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    last_search_string = search_string;
                    $(".user_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.user_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});


$(document).on('dblclick', '#ListUserSearchData tr', function (event) {
    event.preventDefault();
    /* Act on the event */
    let _token = $('#c_token').val();
    let tr = $(this);
    let name = $(tr).find('input[name="list_user_name_hid[]"]').val();
    let user_id = $(tr).find('input[name="list_user_id_hid[]"]').val();

    if (name != '' && user_id != '') {
        $('input[name="search_user"]').val(name);
        $('input[name="search_user_id_hidden"]').val(user_id);

        var url = $('#base_url').val()+"/frequency/frequency-list-by-user";
		    $.ajax({
		      type: "POST",
		      url: url,
		      async: false,
		     data: 'load_group_items=1&user_id=' +user_id+'&_token='+_token,
		      beforeSend: function () {
		        $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
		      },
		        success: function (data) {
		                if(data.status == 1){

		                   $("#frequency-listing-table").html(data.html);

		                }

		              },
		              complete: function () {
		                $("body").LoadingOverlay("hide");
		              }
		    });


        $(".user-list-div").hide();
       
    }

});



$(document).on('click', '.close_btn_user_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".user-list-div").hide();
});


$(document).on('click', '.delete-frequency-row', function () {
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="frequency_id[]"]').val();
     
        if (edit_id != '' && edit_id != 0) {
            deleteFrequencyRowFromDb(edit_id);
            $(tr).remove();
 } else {
            $(tr).remove();
        }
      }
});

//delete single row from db
function deleteFrequencyRowFromDb(id) {
    var url = $('#base_url').val() + "/frequency/delete-frequency-row";
    let _token = $('#c_token').val();
     $("body").LoadingOverlay("show", { background  : "rgba(89, 89, 89, 0.6)",imageColor : '#337AB7'});
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {

        },
        success: function (data) { //console.log(data);
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {
           $("body").LoadingOverlay("hide");
        }
    });
}
