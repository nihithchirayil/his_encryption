$(document).ready(function () {
    setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 10 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + "/report/advanceCollectionReport";
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function getcashReportData() {
    var url = route_json.cashcollectioncloseresult;
    var filters_list = new Object();
    // var chk = document.getElementById("detail"); //to check the  checkbox
    // if (chk.checked) {
    //     var check_box = 1;
    // } else {
    //     var check_box = 0;
    // }
    var check_box = 0;
    filters_list['detail'] = check_box; //pushing the value into the list;
    // var chk = document.getElementById("excludeot_bill"); //to check the  checkbox
    // if (chk.checked) {
    //     var check_box = 1;
    // } else {
    //     var check_box = 0;
    // }
    filters_list['excludeot_bill'] = check_box; //pushing the value into the list;
    var filters_value = '';
    var filters_id = '';
    // var chk = document.getElementById("exclude_dialysis_trust"); //to check the  checkbox
    // if (chk.checked) {
    //     var check_box = 1;
    // } else {
    //     var check_box = 0;
    // }
    filters_list['exclude_dialysis_trust'] = check_box; //pushing the value into the list;
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}

function reset_filter() {
    var current_date = $('#current_date').val();
    $("#user").val([]).change();
    $('#cash_collected_date_from').val(current_date);
    // $('#cash_collected_date_to').val(current_date);
    $('#op_no_hidden').val('');
    $('#op_no').val('');
    $('input:checkbox').removeAttr('checked');
    getcashReportData();
}

function showAdvDetails(e, bill_type, type, usr_id) {
    $('.adv_refund_color').css('background', 'none');
    if (bill_type == 'ADVANCE' || bill_type == 'ADVANCE ADJUST') {

        if ($('#adv_' + type + '_' + usr_id).is(':visible')) {
            $('#adv_' + type + '_' + usr_id).fadeOut();
            $(e).css('background', 'none');

        } else {
            $('.adv_refund').hide();
            $(e).css('background', 'darkseagreen');
            $('#adv_' + type + '_' + usr_id).fadeIn()
        }
    }
}

$(document).on("click", ".cash_close_amout", function () {
    $(this).trigger('keyup');
});

var timeout = null;
$(document).on("keyup", ".cash_close_amout", function () {
    var $input = $(this);
    var value = $input.val();
    var net_amount = $input.data('net-amount');
    var difference_amt = 0;
    var html = '';
    if (value != '') {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            difference_amt = (parseFloat(net_amount) - parseFloat(value)).toFixed(2);
            if (parseFloat(difference_amt) > 0) {
                html = '<i class="fa fa-arrow-down" style="color: red;"></i> ' + Math.abs(difference_amt) + ' (Short)';
            } else {
                if (difference_amt != 0) {
                    html = '<i class="fa fa-arrow-up" style="color: green;"></i> ' + Math.abs(difference_amt) + ' (Excess)';
                } else {
                    html = parseInt(difference_amt);
                }
            }
            $input.closest('tr').find('.cash_difference_amt').attr('data-difference-amt', Math.abs(difference_amt));
            $input.closest('tr').find('.cash_difference_amt').html(html);
        }, 500);
    } else {
        $input.closest('tr').find('.cash_difference_amt').html('');
    }
});

$(document).on("click", ".cash_close_edit", function () {
    $(this).removeClass('cash_close_edit');
    $(this).addClass('cash_close_save');
    $(this).closest('tr').find('.cash_close_save_edit_btn i').addClass('fa-save');
    $(this).closest('tr').find('.cash_close_save_edit_btn i').removeClass('fa-pencil');
    $(this).closest('tr').find('.cash_close_amout').attr('readonly', false);
});

$(document).on("click", ".cash_close_save", function () {
    var tr = $(this).closest('tr');
    var user_id = $(this).attr('data-user-id');
    var net_amount = tr.find('.cash_close_amout').attr('data-net-amount');
    var close_amount = tr.find('.cash_close_amout').val();
    var difference_amt = tr.find('.cash_difference_amt').attr('data-difference-amt');
    var bill_date = $('#cash_collected_date_from').val();

    if (close_amount != '') {
        var ins_data = {};
        ins_data.user_id = user_id;
        ins_data.net_amount = net_amount;
        ins_data.close_amount = close_amount;
        ins_data.bill_date = bill_date;
        ins_data.difference_amt = difference_amt;

        var url = route_json.cashcollectionclosesave;
        $.ajax({
            type: "POST",
            url: url,
            data: { ins_data: JSON.stringify(ins_data) },
            beforeSend: function () {
                tr.find('.cash_close_save i').removeClass('fa-save').addClass('fa-spinner fa-spin');
            },
            success: function (data) {
                if (data == 1) {
                    Command: toastr["success"]("Saved Successfully!");
                } else {
                    Command: toastr["error"]("Network Error!");
                }
            },
            complete: function () {
                tr.find('.cash_close_save i').addClass('fa-save').removeClass('fa-spinner fa-spin');
                getcashReportData();
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });

    } else {
        Command: toastr["warning"]("Please enter closing amount.");
    }
});


function printReportData() {
    $('#print_config_modal').modal('toggle');
}


function print_generate(printData) {

    var showw = "";
    var printMode = $('input[name=printMode]:checked').val();
    var printhead = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;
    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }
    mywindow.document.write('<style>.table.no-border, .table.no-border td, .table.no-border th { border: 0; } .table_sm th, .table_sm td { font-size: 12px !important; padding: 1px 5px!important; } .table_sm td span { line-height: 18px !important; } .table_lg th, .table_lg td { padding: 10px 5px !important; font-size: 12px; } .table_head.tth { font-size: 12px !important; padding: 1px !important; } .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td { border: 1px solid #CCC !important; } .table-col-bordered>thead>tr>th, .table-col-bordered>tbody>tr>th, .table-col-bordered>tfoot>tr>th, .table-col-bordered>thead>tr>td, .table-col-bordered>tbody>tr>td, .table-col-bordered>tfoot>tr>td { border-right: 1px solid #CCC !important; border-top: none; } .headerclass{text-align:left !important;background-color:#000 !important;color:black !important;font-size:12.5px !important} th { background-color:#d9dfe0 !important;}.common_td_rules{ text-align: left !important; overflow: hidden !important;}.td_common_numeric_rules{ border-left: solid 1px #bbd2bd !important; text-align: right !important; } .hide_input{ display: none; }</style>');
    mywindow.document.write(showw + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
    return true;
}

function exceller_template_without_header(excel_name) {
    var template_date = $('#exceller_templatedata').val();
    var uri = 'data:application/vnd.ms  -excel;base64,',
        template = atob(template_date),
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    var toExcel = document.getElementById("result_data_table").innerHTML;

    var ctx = {
        worksheet: name || '',
        table: toExcel
    };
    var link = document.createElement("a");
    link.download = excel_name + ".xls";
    link.href = uri + base64(format(template, ctx))
    link.click();
}