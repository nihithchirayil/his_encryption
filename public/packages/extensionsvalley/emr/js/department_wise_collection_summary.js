$(document).ready(function () {
    $('.date-picker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    $('.select2').select2();
});

function getSummaryData()
{
    var url = route_json.SearchDepartmentWiseCollection;
    var from_date = $('#summary_date_from').val();
    var to_date = $('#summary_date_to').val();
    var payment_type = $('#payment_type').val();
    var paid_status = $('#paid_status').val();
    var doctor = $('#doctor').val();
    
    var parm = { from_date: from_date, to_date: to_date, payment_type: payment_type, paid_status: paid_status, doctor: doctor };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }
    });
}

function resetFilter() {
    var current_date = $('#current_date').val();
    $('#summary_date_from').val(current_date);
    $('#summary_date_to').val(current_date);
    getSummaryData();
}

