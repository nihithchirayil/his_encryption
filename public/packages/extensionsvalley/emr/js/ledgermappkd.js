$(document).ready(function () {
    searchLeadger();
});
var base_url = $('#base_url').val();
var token = $('#hidden_filetoken').val();

$('#ledger_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxledgerSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

        var ledger_name = $(this).val();
        if (!ledger_name) {
            $('#ledger_name_hidden').val('');
            $("#ajaxledgerSearchBox").hide();
        } else {
            var url = base_url + "/item/pkdledgermapsearch";
            console.log(url);
            var param = { key: 'ledger_name', searchkey: ledger_name, type: '1' };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxledgerSearchBox").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxledgerSearchBox").html(html).show();
                    $("#ajaxledgerSearchBox").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxledgerSearchBox', event);
    }
});

$('#ledger_nameinput').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxledgerSearchBox1');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

        var ledger_name = $(this).val();
        if (!ledger_name) {
            $('#ledger_name_hiddeninput').val('');
            $("#ajaxledgerSearchBox1").hide();
        } else {
            var url = base_url + "/item/pkdledgermapsearch";
            console.log(url);
            var param = { key: 'ledger_name', searchkey: ledger_name, type: '2' };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxledgerSearchBox1").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxledgerSearchBox1").html(html).show();
                    $("#ajaxledgerSearchBox1").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxledgerSearchBox1', event);
    }
});

$('#dept_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxdeptSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

        var dept_name = $(this).val();
        if (!dept_name) {
            $('#dept_hidden').val('');
            $("#ajaxdeptSearchBox").hide();
        } else {
            var url = base_url + "/item/pkdledgermapsearch";
            console.log(url);
            var param = { key: 'dept_name', searchkey: dept_name, type: '1' };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxdeptSearchBox").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxdeptSearchBox").html(html).show();
                    $("#ajaxdeptSearchBox").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxdeptSearchBox', event);
    }
});
$('#dept_nameinput').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxdeptSearchBoxinput');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

        var dept_name = $(this).val();
        if (!dept_name) {
            $('#specility_hiddeninput').val('');
            $("#ajaxdeptSearchBoxinput").hide();
        } else {
            var url = base_url + "/item/pkdledgermapsearch";
            console.log(url);
            var param = { key: 'dept_name', searchkey: dept_name, type: '2' };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxdeptSearchBoxinput").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxdeptSearchBoxinput").html(html).show();
                    $("#ajaxdeptSearchBoxinput").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxdeptSearchBoxinput', event);
    }
});

$('#speciality').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxdeptSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

        var speciality = $(this).val();
        if (!speciality) {
            $('#specility_hidden').val('');
            $("#ajaxspecialitySearchBox").hide();
        } else {
            var url = base_url + "/item/pkdledgermapsearch";
            console.log(url);
            var param = { key: 'speciality', searchkey: speciality, type: '1' };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxspecialitySearchBox").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxspecialitySearchBox").html(html).show();
                    $("#ajaxspecialitySearchBox").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxspecialitySearchBox', event);
    }
});
$('#specialityinput').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxspecialitySearchBoxinput');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

        var speciality = $(this).val();
        if (!speciality) {
            $('#specility_hidden').val('');
            $("#ajaxspecialitySearchBoxinput").hide();
        } else {
            var url = base_url + "/item/pkdledgermapsearch";
            console.log(url);
            var param = { key: 'speciality', searchkey: speciality, type: '2' };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajaxspecialitySearchBoxinput").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#ajaxspecialitySearchBoxinput").html(html).show();
                    $("#ajaxspecialitySearchBoxinput").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('ajaxspecialitySearchBoxinput', event);
    }
});
function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillLedgerValues(id, name, key, type = 0) {
    if (key == 'ledger_name' && type == '1') {

        name = htmlDecode(name);
        $('#ledger_name_hidden').val(id);
        $('#ledger_name').val(name);
        $('#ajaxledgerSearchBox').hide();
    } else if (key == 'ledger_name' && type == '2') {

        name = htmlDecode(name);
        $('#ledger_name_hiddeninput').val(id);
        $('#ledger_nameinput').val(name);
        $('#ajaxledgerSearchBox1').hide();
    } else if (key == 'dept_name' && type == '2') {
        // console.log(type);
        name = htmlDecode(name);
        $('#dept_hiddeninput').val(id);
        $('#dept_nameinput').val(name);
        $('#ajaxdeptSearchBoxinput').hide();
    } else if (key == 'dept_name' && type == '1') {

        name = htmlDecode(name);
        $('#dept_hidden').val(id);
        $('#dept_name').val(name);
        $('#ajaxdeptSearchBox').hide();
    } else if (key == 'speciality' && type == '1') {

        name = htmlDecode(name);
        $('#specility_hidden').val(id);
        $('#speciality').val(name);
        $('#ajaxspecialitySearchBox').hide();
    } else if (key == 'speciality' && type == '2') {

        name = htmlDecode(name);
        $('#specility_hiddeninput').val(id);
        $('#specialityinput').val(name);
        $('#ajaxspecialitySearchBoxinput').hide();
    }

}
function searchLeadger() {
    var url = base_url + "/item/loadledgermapping";
    var ledger_id = $('#ledger_name_hidden').val();
    var dept_id = $('#dept_hidden').val();
    var speciality_id = $('#specility_hidden').val();

    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, ledger_id: ledger_id, dept_id: dept_id, speciality_id: speciality_id },
        beforeSend: function () {
            $('#ledgerseracch').attr('disabled', true);
            $('#ledgersearchspin').removeClass('fa fa-search');
            $('#ledgersearchspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#ledgerseracch').attr('disabled', false);
            $('#ledgersearchspin').removeClass('fa fa-spinner fa-spin');
            $('#ledgersearchspin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function searchLeadgerInsert() {
    var url = base_url + "/item/saveledgermapping";
    var ledger_id = $('#ledger_name_hiddeninput').val();
    var dept_id = $('#dept_hiddeninput').val();
    var speciality_id = $('#specility_hiddeninput').val();
    var mapingid =  $('#mapingid').val();

    if (ledger_id) {
        if (dept_id) {
            if (speciality_id) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, ledger_id: ledger_id, dept_id: dept_id, speciality_id: speciality_id,mapingid: mapingid},
                    beforeSend: function () {
                        $('#savedata').attr('disabled', true);
                        $('#ledgersearchspin').removeClass('fa fa-search');
                        $('#ledgersearchspin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if(parseInt(data)==1){
                            toastr.success('Success');
                            searchLeadger();

                            $('#ledger_name_hiddeninput').val('');
                            $('#ledger_nameinput').val('');

                            $('#dept_nameinput').val('');
                            $('#dept_hiddeninput').val('');

                            $('#specialityinput').val('');
                            $('#specility_hiddeninput').val('');

                        }
                    },
                    complete: function () {
                        $('#savedata').attr('disabled', false);
                        $('#ledgersearchspin').removeClass('fa fa-spinner fa-spin');
                        $('#ledgersearchspin').addClass('fa fa-search');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            } else {
               toastr.warning('Please select Speciality.');
               $('#specialityinput').focus();
            }

        } else {
            toastr.warning('Please select Department.');
             $('#dept_nameinput').focus();

        }
    } else {
        toastr.warning('Please select Ledger.');
        $('#ledger_nameinput').focus();

    }




}
//----Hidden Filed Search--------------------------------------------------------------------



function getReportData() {
    var url = route_json.admitHistoryReportResults;

    var filters_list = new Object();

    var exclude_dischrage_patient = 0;
    var chk = document.getElementById("exclude_dischrage_patient"); //to check the  checkbox
    if (chk.checked) {
        exclude_dischrage_patient = 1;
    }
    filters_list['exclude_dischrage_patient'] = exclude_dischrage_patient; //pushing the value into the list;

    var chk = document.getElementById("checkbox"); //to check the  checkbox
    if (chk.checked) {
        var check_box = 1;
    } else {
        var check_box = 0;
    }
    filters_list['checkbox'] = check_box; //pushing the value into the list;
    var chk = document.getElementById("occu"); //to check the  checkbox
    if (chk.checked) {
        var check_box = 1;
    } else {
        var check_box = 0;
    }
    filters_list['occu'] = check_box; //pushing the value into the list;
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "POST",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });

}

function getmapingid(map_id) {
    map_id=atob(map_id)

    var url = base_url + "/item/fetchledgermapping";

    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, map_id: map_id },
        beforeSend: function () {
            $('#ledgerseracch').attr('disabled', true);
            $('#ledgersearchspin').removeClass('fa fa-search');
            $('#ledgersearchspin').addClass('fa fa-spinner fa-spin');
            $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {

            $("#searchDataDiv").LoadingOverlay("hide");

            $('#mapingid').val(data[0].mapping_id);

            $('#ledger_name_hiddeninput').val(data[0].ledger_id);
            $('#ledger_nameinput').val(data[0].ledger_name);

            $('#dept_nameinput').val(data[0].dept_name);
            $('#dept_hiddeninput').val(data[0].dept_id);

            $('#specialityinput').val(data[0].speciality);
            $('#specility_hiddeninput').val(data[0].speciality_id);

        },
        complete: function () {
            $('#ledgerseracch').attr('disabled', false);
            $('#ledgersearchspin').removeClass('fa fa-spinner fa-spin');
            $('#ledgersearchspin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function search_clear() {

    $('#ledger_name_hidden').val('');
    $('#ledger_name').val('');

    $('#dept_hidden').val('');
    $('#dept_name').val('');

    $('#specility_hidden').val('');
    $('#speciality').val('');
    searchLeadger();

}

function search_clear_insert() {

    $('#ledger_name_hiddeninput').val('');
    $('#ledger_nameinput').val('');

    $('#dept_nameinput').val('');
    $('#dept_hiddeninput').val('');

    $('#specialityinput').val('');
    $('#specility_hiddeninput').val('');

}

