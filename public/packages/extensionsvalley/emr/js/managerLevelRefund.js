$(document).ready(function () {
    base_url = $('#base_url').val();
    $('#patient_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('patient_idAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if ($('#patient_id_hidden').val() != "") {
                $('#patient_id_hidden').val('');
            }
            var patient_name = $(this).val();
            patient_name = patient_name.trim();
            if (patient_name == "") {
                $("#patient_idAjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "patient_name_search=" + patient_name,
                    beforeSend: function () {
                        $("#patient_idAjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        //  alert(html); return;
                        $("#patient_idAjaxDiv").html(html).show();
                        $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("patient_idAjaxDiv", event);
        }
    });

    //-----------Bill No search------------
    $('#bill_no').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('bill_no_AjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if ($('#bill_no_hidden').val() != "") {
                $('#bill_no_hidden').val('');
            }
            var bill_no = $(this).val();
            if (bill_no == "") {
                $("#bill_no_AjaxDiv").html("");
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "bill_no=" + bill_no,
                    beforeSend: function () {
                        $("#bill_no_AjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        $("#bill_no_AjaxDiv").html(html).show();
                        $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                    },
                    error: function () {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            }

        } else {
            ajax_list_key_down("bill_no_AjaxDiv", event);
        }
    });

    $('#cashier_name').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('cashier_nameAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var cashier_name = $(this).val();
            if (cashier_name == "") {
                $("#cashier_nameAjaxDiv").html("");
                $('#cashier_id_hidden').val(0);
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "cashier_name=" + cashier_name,
                    beforeSend: function () {
                        $("#cashier_nameAjaxDiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function (html) {
                        // alert(html); return;
                        $("#cashier_nameAjaxDiv").html(html).show();
                        $("#cashier_nameAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        } else {
            ajax_list_key_down("cashier_nameAjaxDiv", event);
        }
    });
    var timeout = null;
    $(document).on('keyup', '#cashierSub_name', function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('cashierSub_nameAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var cashier_name = $(this).val();
            if (cashier_name == "") {
                $("#cashierSub_nameAjaxDiv").html("");
                $('#cashierSub_id_hidden').val(0);
                $('#cashierSub_nameAjaxDiv').hide();
            } else {
                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    var url = '';
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: "cashier_name=" + cashier_name,
                        beforeSend: function () {
                            $("#cashierSub_nameAjaxDiv").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function (html) {
                            // alert(html); return;
                            $("#cashierSub_nameAjaxDiv").html(html).show();
                            $("#cashierSub_nameAjaxDiv").find('li').first().addClass('liHover');
                        }
                    });
                }, 500)
            }

        } else {
            ajax_list_key_down("cashierSub_nameAjaxDiv", event);
        }
    });


    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    searchBill();

});

base_url = '';

function fillPatientValues(uhid, patient_name, from_type = 0) {
    $('#patient_uhid_hidden').val(uhid);
    $('#patient_name').val(patient_name);
    $('#patient_idAjaxDiv').hide();
    if (from_type != 1) {
        $('#bill_no').val('');
        $('#bill_id_hidden').val('');
    }
}

function filterCashierName(id, cashierName) {
    $('#cashier_id_hidden').val(id);
    $('#cashier_name').val(cashierName);
    $('#cashierSub_id_hidden').val(id);
    $('#cashierSub_name').val(cashierName);
    $('#cashier_nameAjaxDiv').hide();
    $('#cashierSub_nameAjaxDiv').hide();
}

/* For BillNo Search, filling values */
function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
    $('#bill_no').val(bill_no);
    $('#patient_uhid_hidden').val(uhid);
    $('#bill_id_hidden').val(bill_id);
    $('#bill_no_AjaxDiv').hide();
    if (from_type != 1) {
        fillPatientValues(uhid, patient_name, 1);
    }
}

function searchBill() {
    var bill_no = $('#bill_no').val();
    var bill_id = $('#bill_id_hidden').val();
    var patient_name = $('#patient_name').val();
    var uhid = $('#patient_uhid_hidden').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var url = base_url + "/purchase/searchRefundBill";
    var token = $('#token_hiddendata').val();

    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, bill_no: bill_no, bill_id: bill_id, patient_name: patient_name, uhid: uhid, from_date: from_date, to_date: to_date },
        beforeSend: function () {
            $('#select_cashierdatadiv').hide();
            $('#pending_bil_list').html('Please Wait!!!');
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#pending_bil_list').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#select_cashierdatadiv').show();
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
            $('#searchdataspin').addClass('fa fa-search');
            $('#bill_no_AjaxDiv').hide();
            $('#patient_idAjaxDiv').hide();
        },
        error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}


function getDetailBills(bill_id, obj) {
    var casher_id = $('#cashier_id_hidden').val();
    var url = base_url + "/purchase/getDeatilBills";
    var token = $('#token_hiddendata').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, bill_id: bill_id },
        beforeSend: function () {
            var bill_no = $('#bill_nolist' + bill_id).html();
            $('#detailBillsModelheader').html(bill_no.trim());
            $(obj).attr('disabled', true);
            $(obj).find('i').removeClass('fa fa-list');
            $(obj).find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#detailBillsModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#detailBillsModelDiv').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $(obj).attr('disabled', false);
            $(obj).find('i').removeClass('fa fa-spinner fa-spin');
            $(obj).find('i').addClass('fa fa-list');
            $('#refundBillDataBtn').attr('onClick', 'refundBill(' + bill_id + ')');
            var casher_id = $('#cashier_id_hidden').val();
            var cashier_name = $('#cashier_name').val();
            var payment_accounting_date = $('#payment_accounting_date').val();
            var customMinDate = moment(payment_accounting_date);
            $('#cashierSub_name').val(cashier_name);
            $('#cashierSub_id_hidden').val(casher_id);
            $("#custom_bill_refund_date").datetimepicker({
                format: 'MMM-DD-YYYY',
                minDate: customMinDate,
                maxDate: moment()
            });
            $('#custom_bill_refund_date_time').datetimepicker({
                format: 'HH:mm'
            });
        },
        error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });    
}



function refundBill(bill_id) {
    var casher_id = $('#cashierSub_id_hidden').val();
    var custom_refund_date = $('#custom_bill_refund_date').val();
    var custom_bill_refund_time = $('#custom_bill_refund_date_time').val();    
    var url = base_url + "/purchase/refundBillManager";
    var token = $('#token_hiddendata').val();
    var column_array = getCheckedColumns();
    if (custom_refund_date == '' || custom_refund_date == null) {
        toastr.warning("Please Select Refund Date");
        return false;
    }
    if (custom_bill_refund_time == '' || custom_bill_refund_time == null) {
        toastr.warning("Please Select Refund Time");
        return false;
    }
    if (column_array.length != 0) {
        if (casher_id != '0') {
            bootbox.confirm({
                message: "Are you sure you want to Refund this Bill ?",
                buttons: {
                    'confirm': {
                        label: "Refund",
                        className: 'btn-primary',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-warning'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var column_arraystring = JSON.stringify(getCheckedColumns());
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: { _token: token, column_arraystring: column_arraystring, bill_id: bill_id, casher_id: casher_id, custom_refund_date: custom_refund_date, custom_bill_refund_time: custom_bill_refund_time },
                            beforeSend: function () {
                                $('#refundBillDataBtn').attr('disabled', true);
                                $('#refundBillDataSpin').removeClass('fa fa-undo');
                                $('#refundBillDataSpin').addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                if (data) {
                                    data = data.trim();
                                }
                                if (data == 'no_bills') {
                                    toastr.warning("No Items For Return");
                                }else if (data == 'error_in_return_insert') {
                                    toastr.warning("No Bills To Return");
                                }else if (data == 'error_in_refund_insert') {
                                    toastr.warning("No Bills To Refund");
                                }else if (data == 'error_in_date') {
                                    toastr.warning("Return date not selected");
                                }else {
                                    $("#detailBillsModel").modal('toggle');
                                    toastr.success("Successfully Refund<br> Refund Number:" + data);
                                    searchBill();
                                }
                            },
                            complete: function () {
                                $('#refundBillDataBtn').attr('disabled', false);
                                $('#refundBillDataSpin').removeClass('fa fa-spinner fa-spin');
                                $('#refundBillDataSpin').addClass('fa fa-undo');
                            },
                            error: function () {
                                toastr.error("Please Check Internet Connection");
                            }
                        });
                    }
                }
            });
        } else {
            toastr.warning("Please Select a Cashier");
            $('#cashierSub_name').focus();
        }
    } else {
        toastr.warning("Please Select Any Item");
        $('#cashier_name').focus();
    }
}

function getCheckedColumns() {
    detail_array = [];
    $('.billDetailId').each(function (index) {
        var status = $(this).is(":checked");
        if (status) {
            detail_array.push($(this).val());
        }
    });
    return detail_array;
}

function checkallBills() {
    balance_amtot = 0.0;
    var status = $('#checkallBills').is(":checked");
    if (status) {
        $('.billDetailId').prop('checked', true);
        $('.balance_amt').each(function (index) {
            var balance_amt = $(this).html().trim();
            balance_amtot += parseFloat(balance_amt);
        });
    } else {
        $('.billDetailId').prop('checked', false);
    }
    $('#refund_amtspan').html(Math.round(balance_amtot * 100) / 100);
}


function checkDeatilBills(list_id) {
    var balance_amtot = parseFloat($('#refund_amtspan').html().trim());
    var balance_amt = $('#balance_amt' + list_id).html().trim();
    var status = $('#checkBillsDetails' + list_id).is(":checked");
    if (status) {
        balance_amtot += parseFloat(balance_amt);
    } else {
        balance_amtot -= parseFloat(balance_amt);
    }
    $('#refund_amtspan').html(Math.round(balance_amtot * 100) / 100);
    allSelectedStatus();
}

function allSelectedStatus() {
    var full_check = $('#checkboxdata').val();
    var k = 0;
    $('.billDetailId').each(function (index) {
        var status = $(this).is(":checked");
        if (status) {
            k++;
        }
    });
    if (full_check == k) {
        $('.checkallBills').prop('checked', true);
    } else {
        $('.checkallBills').prop('checked', false);

    }
}

function resetfilter() {
    var current_date = $('#current_date').val();
    var last_month_date = $('#last_month_date').val();
    $('#patient_uhid_hidden').val('');
    $('#patient_name').val('');
    $('#patient_idAjaxDiv').hide();
    $('#bill_no').val('');
    $('#bill_id_hidden').val('');
    $('#bill_no_AjaxDiv').hide();
    $('#select_cashierdatadiv').hide();
    $('#from_date').val(last_month_date);
    $('#to_date').val(current_date);
    searchBill();
}
