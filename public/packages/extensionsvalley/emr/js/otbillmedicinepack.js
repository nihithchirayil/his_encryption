$(window).load(function(){
    $("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });
    $("input[data-attr='date']").trigger("focus");
    $(".getReport").trigger("focus");
    $(".report_date").html("Report Date : " + moment().format('DD-MMM-YYYY HH:mm:ss'));
})

$(document).ready(function(){



    $(document).on('click', '.getReport', function(event) {
        getMedicinePackReport();
    });
    $(document).on('click', '.printReport', function(event) {
        printReport();
    });


    function getMedicinePackReport(){
        var from_date = $('.from_date').val();
        var to_date = $('.to_date').val();
        var doctor = $('.doctor').val();
        var anesthesia_doctor = $('.anesthesia_doctor').val();
        $(".report_date_range").html('Report From: '+ from_date +' - '+to_date);
        if(moment(from_date).isAfter(to_date)){
            toastr.error("Please check the Date Range !!");
            return;
        }

        let _token = $('#c_token').val();
        // var url = "/otbilling/getMedicinePackReport";
        var url = $("#base_url").val()+"/otbilling/getMedicinePackReport";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                from_date :from_date,
                to_date :to_date,
                doctor :doctor,
                anesthesia_doctor:anesthesia_doctor,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".otbill_medicinepack_body").empty();
                $(".getReport").find('i').removeClass('fa-search').addClass('fa-spinner').addClass('fa-spin');
                $(".otbill_medicinepack_body").append("<tr><td colspan='10' style='text-align: center;'><i class='fa fa-spinner fa-spin' style='font-size: 20px;'></i></td></tr>")
            },
            success: function (response) {
                console.log(response);
                $(".getReport").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-search');


                if(response.code === 100){
                    $(".otbill_medicinepack_body").empty();

                    var amount_GRAND_total = OT_GRAND_TOTAL = ANAST_GRAND_TOTAL = ANS_HSP_GRAND_TOTAL = GF_GRAND_TOTAL = MED_PACK_GRAND_TOTAL = Overtime_GRAND_TOTAL = SUR_SH_GRAND_TOTAL = 0;

                    $.each(response.data, function(key, val){
                        $(".otbill_medicinepack_body").append("<tr style='background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif'><th colspan='10'>"+key+"</th></tr>");

                        var amount_total = OT_TOTAL = ANAST_TOTAL = ANS_HSP_TOTAL = GF_TOTAL = MED_PACK_TOTAL = Overtime_TOTAL = SUR_SH_TOTAL = 0;
                        $.each(val, function(key1, val1){
                            // total = parseFloat(total) + parseFloat(val1.total_amount);
                            var OT,ANAST,ANS_HSP,GF,MED_PACK,Overtime, SUR_SH  ='';

                            OT = val1.OT ? val1.OT : 0.00;
                            ANAST = val1.ANAST ? val1.ANAST: 0.00;
                            ANS_HSP = val1.ANS_HSP ? val1.ANS_HSP: 0.00;
                            GF = val1.GF ? val1.GF: 0.00;
                            MED_PACK = val1.MED_PACK ? val1.MED_PACK: 0.00;
                            Overtime = val1.Overtime ? val1.Overtime: 0.00;
                            SUR_SH = val1.SUR_SH ? val1.SUR_SH: 0.00;

                            OT_TOTAL = parseFloat(OT_TOTAL)+ parseFloat(OT);
                            ANAST_TOTAL = parseFloat(ANAST_TOTAL)+ parseFloat(ANAST);
                            ANS_HSP_TOTAL = parseFloat(ANS_HSP_TOTAL)+ parseFloat(ANS_HSP);
                            GF_TOTAL = parseFloat(GF_TOTAL)+ parseFloat(GF);
                            MED_PACK_TOTAL = parseFloat(MED_PACK_TOTAL)+ parseFloat(MED_PACK);
                            Overtime_TOTAL = parseFloat(Overtime_TOTAL)+ parseFloat(Overtime);
                            SUR_SH_TOTAL = parseFloat(SUR_SH_TOTAL)+ parseFloat(SUR_SH);
                            amount_total = parseFloat(amount_total)+ parseFloat(val1.total_amount);

                            $(".otbill_medicinepack_body").append("<tr><td colspan='10'><b>"+moment(val1.bill_datetime).format('DD-MMM-YYYY HH:mm:ss')+"</b></td></tr><tr><td>"+val1.bill_no+"</td><td>"+val1.uhid+"</td><td colspan='7' >"+val1.doctor+"</td><td  >"+val1.anesthesia_doctor+"</td></tr><tr><td colspan='2' >"+val1.patient+"</td><td>"+val1.total_amount+"</td><td>"+OT+"</td><td>"+MED_PACK+"</td><td>"+ANAST+"</td><td>"+GF+"</td><td>"+Overtime+"</td><td>"+SUR_SH+"</td><td>"+ANS_HSP+"</td>    </tr>");
                        });
                        $(".otbill_medicinepack_body").append("<tr><td colspan='2' style='color:#b71100;'><b>Sub Total:</b></td><td style='color:#b71100;'><b>"+amount_total.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+OT_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+MED_PACK_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+ANAST_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+GF_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+Overtime_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+SUR_SH_TOTAL.toFixed(2)+"</b></td><td style='color:#b71100;'><b>"+ANS_HSP_TOTAL.toFixed(2)+"</b></td></tr>");
                        OT_GRAND_TOTAL = parseFloat(OT_TOTAL)+ parseFloat(OT_GRAND_TOTAL);
                        ANAST_GRAND_TOTAL = parseFloat(ANAST_TOTAL)+ parseFloat(ANAST_GRAND_TOTAL);
                        ANS_HSP_GRAND_TOTAL = parseFloat(ANS_HSP_TOTAL)+ parseFloat(ANS_HSP_GRAND_TOTAL);
                        GF_GRAND_TOTAL = parseFloat(GF_TOTAL)+ parseFloat(GF_GRAND_TOTAL);
                        MED_PACK_GRAND_TOTAL = parseFloat(MED_PACK_TOTAL)+ parseFloat(MED_PACK_GRAND_TOTAL);
                        Overtime_GRAND_TOTAL = parseFloat(Overtime_TOTAL)+ parseFloat(Overtime_GRAND_TOTAL);
                        SUR_SH_GRAND_TOTAL = parseFloat(SUR_SH_TOTAL)+ parseFloat(SUR_SH_GRAND_TOTAL);
                        amount_GRAND_total = parseFloat(amount_total)+ parseFloat(amount_GRAND_total);

                    });

                    $(".otbill_medicinepack_body").append("<tr style='background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif'><th colspan='2'>Grand Total:</th><th>"+amount_GRAND_total.toFixed(2)+"</th><th>"+OT_GRAND_TOTAL.toFixed(2)+"</th><th>"+MED_PACK_GRAND_TOTAL.toFixed(2)+"</th><th>"+ANAST_GRAND_TOTAL.toFixed(2)+"</th><th>"+GF_GRAND_TOTAL.toFixed(2)+"</th><th>"+Overtime_GRAND_TOTAL.toFixed(2)+"</th><th>"+SUR_SH_GRAND_TOTAL.toFixed(2)+"</th><th>"+ANS_HSP_GRAND_TOTAL.toFixed(2)+"</th></tr>");

                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { }
        });
    }

    function printReport(){
        var printMode = $('input[name=printMode]:checked').val();
        var showw = "";
        var mywindow = window.open('', 'my div', 'height=3508,width=2480');
        var msglist = document.getElementById('printData');
        showw = showw + msglist.innerHTML;

        if(printMode == 1){
            mywindow.document.write('<style>td,th{padding:3px;}tabele,td,th{border:1px solid #000;font-size:12px;}@media  print table,td,tr{page-break-inside:auto;width:100%; margin:0;}</style>');

        }else{
            mywindow.document.write('<style>td,th{padding:3px;}tabele,td,th{border:1px solid #000;font-size:12px;}@media  print table,td,tr{page-break-inside:auto;width:100%; margin:0;}</style>');

        }

        mywindow.document.write(showw);
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print()
        mywindow.close();
        return true;
    }
});

