
$(document).ready(function(){
   
   $('#baby_details_tab_body').html('');
   $('#progressbar').html('');
   
    babySearchList();
})
var baby_array=[];
var baby_counter=[];
$(document).on('click','#progressbar li',function (e) {
    $(this).find('a').tab('show');
});

function addBornBabyDetails(patient_id){
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    if(location_id==0){
        location_id=$('.select_default_location_modal_data').val();
        if(location_id==0 || !location_id){
               toastr.warning('Please select a location.')
               $('.select_default_location_modal ').modal('show');
               return;
        }
    }
    var url=$('#base_url').val()+'/newborn/motherDetails';
    var token=$('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {token:token,patient_id:patient_id},
        beforeSend: function () {
            $('#baby_reg_mag').attr('disabled', true);
            $('#baby_reg_mag_spin').removeClass('fa fa-child');
            $('#baby_reg_mag_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var obj=JSON.parse(data);
            console.log(obj);
            if (data) {
                $('#mother_uhid_1').html(obj.mother_uhid);
                $('#mother_name_1').html(obj.mother_name);    
                $('#add_mother_uhid').val(obj.mother_uhid);
                $('#add_mother_name').val(obj.mother_name);
                $('#add_mother_age').val(obj.mother_age);
                $('#add_admit_doc').val(obj.admitting_doctor);
                $('#add_admit_doc_id').val(obj.admitting_doctor_id);
                $('#add_admit_date').val(obj.admitting_date);
                $('#bed_id').val(obj.bed_id);
                $('#add_baby_details_count').val(obj.baby_count);
                $('#baby_count').val(obj.baby_count);
                $('#new_born_baby_modal').modal('show');
               resetBabyTabLI();
              
            } 

          
        },
        complete: function () {
            $('#baby_reg_mag').attr('disabled', false);
            $('#baby_reg_mag_spin').removeClass('fa fa-spinner fa-spin');
            $('#baby_reg_mag_spin').addClass('fa fa-child');
            $("#new_born_baby_modal").modal({
                backdrop: 'static',
                keyboard: false
            });

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

   
}
function resetBabyTabLI(){
   // $('#add_baby_details_count').val(0);\
    var baby_count=$('#baby_count').val();
    $('#add_baby_details_count').val(baby_count);
    $('#baby_details_tab_body').html('');
    $('#progressbar').html('');
    baby_array=[]; 
    baby_counter=[];
    addNewBabyDetailsDiv();
    babySearchList();
}
function setBedList(e){
 var url=$('#base_url').val()+'/newborn/setBedList';
 var token=$('#c_token').val();
 bed_type=$(e).val();
 if(bed_type){
    $.ajax({
        type: "POST",
        url: url,
        data: {token:token,bed_type:bed_type},
        beforeSend: function () {
        $('#bed_selection_box').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var obj=JSON.parse(data);
            if (data) {

                $(e).closest('.col-md-6').next().find('.bed_selection').empty();
                $(e).closest('.col-md-6').next().find('.bed_selection').html('<option value="">Select Bed</option>');
                $.each(obj, function (key, value) {
                    ($(e).closest('.col-md-6').next().find('.bed_selection').append('<option value="' + value.id + '">' + value.room_name + '</option>'));
                });
            } 

          
        },
        complete: function () {
            $('#bed_selection_box').LoadingOverlay("hide");

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
 } 
}
$(document).on('keyup','.add_baby_birth_length_cm',function(){
    convert_to_inches(this);  
})
$(document).on('keyup','.add_baby_birth_weight_kg',function(){
    convert_to_pounds(this);  
})
$(document).on('keyup','.add_baby_birth_length_inch',function(){
    convert_to_cm(this);  
})
$(document).on('keyup','.add_baby_birth_weight_pound',function(){
    convert_to_Kg(this);  
})
$(document).on('keyup','.add_baby_head_cer_inc',function(){
    convert_to_cm_cr(this);  
})
$(document).on('keyup','.add_baby_head_cer_cm',function(){
    convert_to_inches_cr(this);  
})
function convert_to_pounds(e){
   var kilo=$(e).val().trim();
	if(kilo){
    var n_kilo=parseFloat(kilo * 2.2).toFixed(3);
    $(e).closest('.col-md-6').next().find('.add_baby_birth_weight_pound').val(n_kilo);
    }else{
        $(e).closest('.col-md-6').next().find('.add_baby_birth_weight_pound').val('');
    }
}
function convert_to_Kg(e){
   var pound=$(e).val().trim();
	if(pound){
    var n_pound=parseFloat(pound / 2.2).toFixed(3);
    $(e).closest('.col-md-6').prev().find('.add_baby_birth_weight_kg').val(n_pound);
    }else{
        $(e).closest('.col-md-6').prev().find('.add_baby_birth_weight_kg').val('');
    }
}
function convert_to_cm_cr(e){
   var inc=$(e).val().trim();
	if(inc){
         var inc_t=parseFloat(inc / 0.4).toFixed(3);
        $(e).closest('.col-md-6').prev().find('.add_baby_head_cer_cm').val(inc_t);
    }else{
        $(e).closest('.col-md-6').prev().find('.add_baby_head_cer_cm').val('');
    }
}
function convert_to_inches_cr(e){
   var cm=$(e).val().trim();
	if(cm){
         var cmt=parseFloat(cm * 0.4).toFixed(3);
        $(e).closest('.col-md-6').next().find('.add_baby_head_cer_inc').val(cmt);
    }else{
        $(e).closest('.col-md-6').next().find('.add_baby_head_cer_inc').val('');
    }
}
function convert_to_cm(e){
   var inc=$(e).val().trim();
	if(inc){
         var inc_t=parseFloat(inc / 0.4).toFixed(3);
        $(e).closest('.col-md-6').prev().find('.add_baby_birth_length_cm').val(inc_t);
    }else{
        $(e).closest('.col-md-6').prev().find('.add_baby_birth_length_cm').val('');
    }
}
function convert_to_inches(e){
   var cm=$(e).val().trim();
	if(cm){
         var cmt=parseFloat(cm * 0.4).toFixed(3);
        $(e).closest('.col-md-6').next().find('.add_baby_birth_length_inch').val(cmt);
    }else{
        $(e).closest('.col-md-6').next().find('.add_baby_birth_length_inch').val('');
    }
}


function addNewBabyDetailsDiv(){
   var url=$('#base_url').val()+'/newborn/setBabyDetailsLiAndTabs';
   var  token=$('#c_token').val();
    var mother_name=$('#add_mother_name').val();
    var patient_id=$('#patient_id').val();
    var baby_count=$('#add_baby_details_count').val();
    var inc_baby_count=parseInt(baby_count) + 1;
    var flag=true;
    flag=ValidatBeforSaveorAddd();
    if(flag){
        if(mother_name){
            $.ajax({
                type: "POST",
                url: url,
                data: {token:token,baby_count:inc_baby_count,mother_name:mother_name,patient_id:patient_id},
                beforeSend: function () {
                 $('#addBabyDetailsBOX').attr('disabled', true);
                 $('#addBabyDetailsBOX_btn').removeClass('fa fa-plus');
                 $('#addBabyDetailsBOX_btn').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    var obj=JSON.parse(data);
                    if (data) {
                      $('#progressbar').append(obj.created_li);
                      $('#baby_details_tab_body').append(obj.created_tabs);
                      $('#add_baby_details_count').val(inc_baby_count);
                      baby_counter.push(inc_baby_count);
                      $('.baby_datepicker').datetimepicker({
                         format: 'MMM-DD-YYYY',
                     });
                     $('.baby_timepicker').datetimepicker({
                         format: 'hh:mm A',
                     });
                      
                    } 
        
                  
                },
                complete: function () {
                 $('#addBabyDetailsBOX').attr('disabled', false);
                 $('#addBabyDetailsBOX_btn').removeClass('fa fa-spinner fa-spin');
                 $('#addBabyDetailsBOX_btn').addClass('fa fa-plus');
                 $("#progressbar li:last-child a").tab("show");
               
        
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
         } 
    }else{
     toastr.warning('Incomplete details.');
    }
    
   
   }

   function removeThisDViv(tab,val){
    $('#'+tab).remove();
    $('#'+tab+'_tab').remove();
    var count_data=$('#add_baby_details_count').val();
    $('#add_baby_details_count').val(parseInt(count_data)-1);
    $("#progressbar li:last-child a").tab("show");
    var index = baby_counter.indexOf(val);
    if (index > -1) { 
        baby_counter.splice(index, 1); 
      }
      

   }
   function stringifiedBadyDetails(){
    var flag=true
    baby_counter.forEach(function(count){
        baby_array.push({
            'name':$('#add_baby_name'+count).val(),
            'gender':$('#add_baby_gender'+count).val(),
            'birth_date':$('#add_baby_birth_date'+count).val(),
            'birth_time':$('#add_baby_birth_time'+count).val(),
            'baby_weight_kg':$('#add_baby_birth_weight_kg'+count).val(),
            'baby_weight_pound':$('#add_baby_birth_weight_pound'+count).val(),
            'baby_length_cm':$('#add_baby_birth_length_cm'+count).val(),
            'baby_length_inch':$('#add_baby_birth_length_inch'+count).val(),
            'mode_of_delivery':$('#mode_of_delivery'+count).val(),
            'bed_preference':$('#bed_preference'+count).val(),
            'bed_selection':$('#bed_selection'+count).val(),
            'baby_consulting_doc':$('#baby_consulting_doc'+count).val(),
            'add_baby_head_cer_cm':$('#add_baby_head_cer_cm'+count).val(),
            'add_baby_head_cer_inc':$('#add_baby_head_cer_inc'+count).val(),
           })
      
    })
  

    
}


function saveNewBabyDetails(){
    var url=$('#base_url').val()+'/newborn/saveNewBabyDetails';
    var token=$('#c_token').val();
    var flag=true;
    var mother_uhid=$('#add_mother_uhid').val();
    var doctor_id=$('#add_admit_doc_id').val();
    var actual_admission_date=$('#add_admit_date').val();
    var current_bed_id=$('#bed_id').val();
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    if(location_id==0){
        location_id=$('.select_default_location_modal_data').val();
        if(location_id==0 || !location_id){
               toastr.warning('Please select a location.')
               $('.select_default_location_modal ').modal('show');
               return;
        }
    }
    flag=ValidatBeforSaveorAddd();
   if(flag){
    stringifiedBadyDetails();
    if(parseInt(baby_array.length) > 0){
        $.ajax({
            type: "POST",
            url: url,
            data: {token:token,mother_uhid:mother_uhid,doctor_id:doctor_id,actual_admission_date:actual_admission_date,current_bed_id:current_bed_id,
                location_id:location_id,baby_details_string:JSON.stringify(baby_array)},
            beforeSend: function () {
             $('#save_new_born').attr('disabled', true);
             $('#save_new_born_spin').removeClass('fa fa-save');
             $('#save_new_born_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
            if(data){
                toastr.success('Success.');
                var baby_count=$('#baby_count').val();
                var ttl_cnt=parseInt(baby_count) + parseInt(baby_array.length);
                $('#baby_count').val(ttl_cnt);
                resetBabyTabLI();
              

            }
    
              
            },
            complete: function () {
             $('#save_new_born').attr('disabled', false);
             $('#save_new_born_spin').removeClass('fa fa-spinner fa-spin');
             $('#save_new_born_spin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }else{
        toastr.warning('Nothing to save.');
        return;
    }
   }else{
     toastr.warning('Incomplete details.');
   }
    
} 

$(document).on('keydown','.searchables',function(e){
    console.log($(this).val().trim());
    if (e.keyCode == 13 && $(this).val().trim()!='') {  
        babySearchList();
    }
   
});
$(document).on('blur','.searchables_date',function(e){
    
        babySearchList();
    
   
});
function resetSearch(){
    $('.searchables_date').val('');
    $('.searchables').val('');
    babySearchList()
}
function babySearchList(){
    var url=$('#base_url').val()+'/newborn/babySearchList';
    var token=$('#c_token').val();
    var patient_id=$('#patient_id').val();
    var from_date=$('#new_born_from_date').val().trim();
    var to_date=$('#new_born_to_date').val().trim();
    var baby_uhid=$('#new_born_baby_uhid').val().trim();
    var baby_name=$('#new_born_baby_name').val().trim();
   
    $.ajax({
           type: "POST",
           url: url,
           data: {token:token,patient_id:patient_id,from_date:from_date,to_date:to_date,baby_uhid:baby_uhid,baby_name:baby_name},
           beforeSend: function () {
            $('#birthRegistrationHistoryTable').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
           },
           success: function (data) {
               if (data) {
                 $('#birthRegistrationHistoryTable').html(data);
               } 
   
             
           },
           complete: function () {
            $('#birthRegistrationHistoryTable').LoadingOverlay("hide");
           },
           error: function () {
               toastr.error("Error Please Check Your Internet Connection");
           }
       });
    
}

function updateBbyDetails(id){
    var url=$('#base_url').val()+'/newborn/updateBbyDetails';
   var token=$('#c_token').val();
   $('#baby_edit_id').val(id);
    token=$('#c_token').val();
    $('#add_baby_details').hide();
    $('#add_baby_details_edit').show();
    $.ajax({
           type: "POST",
           url: url,
           data: {token:token,id:id},
           beforeSend: function () {
            $('.reset_baby_details').val('')
            $('#add_baby_details_edit').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
           },
           success: function (data) {
               if (data) {
                $('#addBabyDetailsBOX').hide();
                 $('#add_baby_details_edit').html(data);
                 baby_counter=[];
                 baby_counter.push(1);
                 document.getElementById('save_new_born').setAttribute('onclick','saveBbyEditedDetails()');
                 $('#save_new_born').html('<i class="fa fa-save" id="save_new_born_spin"></i> Update');
                 $('.baby_datepicker').datetimepicker({
                    format: 'MMM-DD-YYYY',
                });
                $('.baby_timepicker').datetimepicker({
                    format: 'hh:mm A',
                });
                $('#resetBabyTabLI_int').show();
              

               } 
   
             
           },
           complete: function () {
            $('#add_baby_details_edit').LoadingOverlay("hide");
            //var bed_preference_hidden=$('#bed_preference_hidden').val();
            var bed_id_hidden=$('#bed_id_hidden').val();
            if(bed_id_hidden){
               $('#bed_selection1').val(bed_id_hidden);     
            } 
           // setBedList1(bed_preference_hidden);
           },
           error: function () {
               toastr.error("Error Please Check Your Internet Connection");
           }
       });  
}

function resetBabyTabLI_int(){
   $('#add_baby_details').show();
   $('#add_baby_details_edit').hide();
   $('#add_baby_details_edit').val('');
   $('#addBabyDetailsBOX').show();
   $('#resetBabyTabLI_int').hide();
   
   document.getElementById('save_new_born').setAttribute('onclick','saveNewBabyDetails()')
   $('#save_new_born').html('<i class="fa fa-save" id="save_new_born_spin"></i> Save');

    resetBabyTabLI(); 
}
function saveBbyEditedDetails(){
    var url=$('#base_url').val()+'/newborn/saveBbyEditedDetails';
    var token=$('#c_token').val();
    var mother_uhid=$('#add_mother_uhid').val();
    var flag=true;
    flag=ValidatBeforSaveorAddd();
    if(flag){
        stringifiedBadyDetails();
   
        $.ajax({
               type: "POST",
               url: url,
               data: {token:token,mother_uhid:mother_uhid,id:$('#baby_edit_id').val(),baby_details_string:JSON.stringify(baby_array)},
               beforeSend: function () {
                $('#save_new_born').attr('disabled', true);
                $('#save_new_born_spin').removeClass('fa fa-save');
                $('#save_new_born_spin').addClass('fa fa-spinner fa-spin');
               },
               success: function (data) {
                   if (data) {
                       toastr.success('Success.'); 
                       resetBabyTabLI_int();
                   } 
       
                 
               },
               complete: function () {
                $('#save_new_born').attr('disabled', false);
                 $('#save_new_born_spin').removeClass('fa fa-spinner fa-spin');
                 $('#save_new_born_spin').addClass('fa fa-save');
               },
               error: function () {
                   toastr.error("Error Please Check Your Internet Connection");
               }
           });
    }else{
       toastr.warning('Incomplete details.');
    }
         
}

function ValidatBeforSaveorAddd(){
    var flag=true;
    baby_counter.forEach(function(count){
    if($('#add_baby_name'+count).val().trim() !='' && $('#add_baby_gender'+count).val().trim()!='' && $('#add_baby_birth_date'+count).val().trim() !='' &&
    $('#add_baby_birth_time'+count).val().trim() !='' && $('#add_baby_birth_weight_kg'+count).val().trim() !='' && $('#add_baby_birth_weight_pound'+count).val().trim() !=''
     && $('#add_baby_birth_length_cm'+count).val().trim()!='' && $('#add_baby_birth_length_inch'+count).val().trim()!=''
     && $('#add_baby_head_cer_cm'+count).val().trim() !='' &&  $('#add_baby_head_cer_inc'+count).val().trim() !='' && 
     $('#mode_of_delivery'+count).val()!='' &&  $('#bed_preference'+count).val()!='' &&  $('#bed_preference'+count).val()!='' 
     && $('#bed_selection'+count).val() !='' && $('#baby_consulting_doc'+count).val()!='' ){
           flag=true;
    }else{
        flag=false;
        return false;
    }
    })

   return flag;
}
function setBedList1(type=0){
    var url=$('#base_url').val()+'/newborn/setBedList';
    var token=$('#c_token').val();
  
    if(type !=0){
       $.ajax({
           type: "POST",
           url: url,
           data: {token:token,bed_type:type},
           beforeSend: function () {
           $('#bed_selection_box').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
           },
           success: function (data) {
               var obj=JSON.parse(data);
               if (data) {
   
                   $('#bed_selection1').empty();
                   $('#bed_selection1').html('<option value="">Select Bed</option>');
                   $.each(obj, function (key, value) {
                       ($('#bed_selection1').append('<option value="' + value.id + '">' + value.room_name + '</option>'));
                   });
               } 
              var bed_id_hidden=$('#bed_id_hidden').val();
              if(bed_id_hidden){
                 $('#bed_selection1').val(bed_id_hidden);     
              } 
             
           },
           complete: function () {
               $('#bed_selection_box').LoadingOverlay("hide");
   
           },
           error: function () {
               toastr.error("Error Please Check Your Internet Connection");
           }
       });
    } 
   }