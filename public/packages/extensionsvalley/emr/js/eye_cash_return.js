$(document).ready(function() {

});


//---numeric only validation----------------
$(".number_only").keydown(function(event) {
    // Allow only backspace and delete
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190) {
        // let it happen, don't do anything
    } else {
        // Ensure that it is a number and stop the keypress
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
        }
    }
});

function changePaymentMode(mode) {
    $('#payment_mode').val(mode);
    $('#machine_bank').val('');
    $('#bank').val('');
}


function checkContentLength(id, length) {
    var value = $('#' + id).val();
    if (value.length > length) {
        value = value.substring(0, length);
        $('#' + id).val(value);
    }
    if (id == 'exp_month') {
        if (value > 12 || value < 1) {
            $('#' + id).val('');
        }
    }
    if (id == 'exp_year') {
        if (value > 2099) {
            $('#' + id).val('');
        }
    }
}

function save_return_payment(save_type) {

    var payment_mode = $('#payment_mode').val();
    var return_no = $('#return_no').val();
    var uhid = $('#uhid').val();
    var visit_id = $('#visit_id').val();
    var return_amount = $('#return_amount').val();
    var counter_id = 0;
    var payment_type = $('#payment_type').val();
    var advance_type = $('#advance_type').val();
    var unit_id = $('#unit_id').val();
    var return_type = $('#return_type').val();
    var return_id = $('#return_id').val();
    var card_no = $('#card_no').val();
    var bank = $('#bank').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();
    var token = $('#c_token').val();

    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
    var url = $('#base_url').val() + "/eye_cash_return/save_cash_return";

    var dataparams = {
        '_token': token,
        'payment_mode': payment_mode,
        'return_no': return_no,
        'uhid': uhid,
        'visit_id': visit_id,
        'return_amount': return_amount,
        'counter_id': counter_id,
        'payment_type': payment_type,
        'advance_type': advance_type,
        'location_code': location_id,
        'unit_id': unit_id,
        'return_type': return_type,
        'return_id': return_id,
        'card_no': card_no,
        'bank': bank,
        'exp_month': exp_month,
        'exp_year': exp_year,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function() {
            $('#modalCashReturn').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            //console.log(html);return;
            var obj = JSON.parse(html);

            if (obj.status == '1') {
                Command: toastr["success"]('Returned Successfully!');
                $('#modalCashReturn').modal('hide');
                $('.search_btn').trigger('click');
                makePayButtonDisable(return_id);


            }
            else {
                Command: toastr["warning"](obj.message);
            }

        },

        complete: function() {
            $('#modalCashReturn').LoadingOverlay("hide");
            cashReturnComplete();
        },


    });

}


function loadCashReturn(return_id) {
    if (return_id != '') {
        var token = $("#c_token").val();
        var base_url = $("#base_url").val();
        var dataparams = {
            '_token': token,
            'return_id': return_id
        };
        $.ajax({
            type: "POST",
            url: base_url + "/eye_cash_return/cash_return",
            data: dataparams,
            beforeSend: function() {
                $('#modalCashReturn').modal('show');
                $('#modalCashReturnBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function(data) {
                $('#modalCashReturnBody').html(data);
            },
            complete: function() {
                $('#modalCashReturnBody').LoadingOverlay("hide");                
            },
            error: function() {
                toastr.error("Error!! Please check your internet connection!");
            },
        });
    }
}

function makePayButtonDisable(return_id) {
    $('#pay_button' + return_id).fadeOut();
    $('#paid' + return_id).removeClass('unpaid-bg');
    $('#paid' + return_id).addClass('paid-bg');
    $('#pay_button').fadeOut();
}
function reloadWindow(){
    var is_return_screen=$('#return_screen').val();
    if(is_return_screen==1){
        window.location.reload();
        $('#modalCashReturn').modal('hide');
    }else{
        $('#modalCashReturn').modal('hide');
    }
    
}