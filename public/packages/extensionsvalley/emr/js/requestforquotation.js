$(document).ready(function () {
    base_url = $('#base_url').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2();
    checkEditStatus();
    var rfq_id = $('#rfq_id').val();
    if (rfq_id != '0') {
        $('#location').attr("disabled", true);
    }
    $('.datepicker').blur();
    getLocationDefaultValue();
});

base_url = '';
var charge_item = {};
var item_array = {};
var vendor_array = new Array();
var vendor_editstatus = 0;
var previous_location = $('#location').val();
var request_array = new Array();



function changeLocation() {
    getLocationDefaultValue();
    var check_tems = addQuotationItems(2);
    var temp_loc = previous_location;
    if (check_tems) {
        if (request_array.length != 0) {
            bootbox.confirm({
                message: "Items added to this location would be lost.Are you sure you want you continue ?",
                buttons: {
                    'confirm': {
                        label: "Continue",
                        className: 'btn-warning',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $('#row_count_id').val(0);
                        $('#row_body_data').html('');
                        getNewRowInserted();
                        previous_location = $('#location').val();
                    } else {
                        $("#location").val(temp_loc);
                        $("#location").select2();
                    }
                }
            });
        }
    }

}

function getLocationDefaultValue() {
    var location = $('#location').val();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/getLocationDefaultValue";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, location: location },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#location_defaultvalue').val(data);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
        }
    });
}

function checkEditStatus() {
    var rfq_id = $('#rfq_id').val();
    var token = $('#token_hiddendata').val();
    if (rfq_id != '0') {
        var url = base_url + "/quotation/getRequestQuotationItems";
        vendor_editstatus = 1;
        $.ajax({
            type: "POST",
            url: url,
            data: { _token: token, rfq_id: rfq_id },
            beforeSend: function () {
                $('#addQuotationRowbtn').attr('disabled', true);
                $('#addQuotationRowSpin').removeClass('fa fa-plus');
                $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                $('#row_body_data').html(html);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30

                });
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function () {
                $('#addQuotationRowbtn').attr('disabled', false);
                $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addQuotationRowSpin').addClass('fa fa-plus');
                var row_count = $('#editdatalistmaxrow').val();
                $('#row_count_id').val(parseInt(row_count));
            }
        });
        var row_count = $('#editvendoristmaxrow').val();
        $('#vendorrow_count_id').val(parseInt(row_count));
    } else {
        getNewRowInserted();
    }
}


function getNewRowInserted() {
    var url = base_url + "/quotation/addQuotationRow";
    var token = $('#token_hiddendata').val();
    var row_count = $('#row_count_id').val();
    $('#row_count_id').val(parseInt(row_count) + 1);
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, row_count: row_count },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            $('#row_body_data').append(html);
            row_ct = 1;
            $(".row_class").each(function (i) {
                $(this).find('.row_count_class').text(row_ct); // row Count Re-arenge
                row_ct++;
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            $('.form-control').css("border-color", "#fff");
        }
    });
}

function addNewVendor() {
    var url = base_url + "/quotation/addNewVendor";
    var token = $('#token_hiddendata').val();
    var row_count = $('#vendorrow_count_id').val();
    $('#vendorrow_count_id').val(parseInt(row_count) + 1);
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, row_count: row_count },
        beforeSend: function () {
            $('#addNewVendorRowbtn').attr('disabled', true);
            $('#addNewVendorRowSpin').removeClass('fa fa-list');
            $('#addNewVendorRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            $('#add_new_vendor').append(html);
            row_ct = 1;
            $(".vendorrow_class").each(function (i) {
                $(this).find('.vendorrow_count_class').text(row_ct); // row Count Re-arenge
                row_ct++;
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        },
        complete: function () {
            $('#addNewVendorRowbtn').attr('disabled', false);
            $('#addNewVendorRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addNewVendorRowSpin').addClass('fa fa-list');
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}

function addQuotationItems(from_type) {
    request_array = new Array();
    var flag = true;
    $('.form-control').css("border-color", "#fff");
    $('#row_body_data tr').each(function () {
        var item_id = $(this).find("input[name='item_id_hidden[]']").val();
        if (item_id != '') {
            var row_id = $(this).find("input[name='row_id_hidden[]']").val();
            var uom_val = $('#uom_select_id_' + row_id + ' option').data("uom_value");
            var request_qty = $(this).find("input[name='request_qty[]']").val();
            var comments = $(this).find("input[name='comments[]']").val();
            if (from_type == 1) {
                if (request_qty == 0 || !request_qty) {
                    flag = false;
                    $(this).find("input[name='request_qty[]']").css("border-color", "#ff0000");
                }
            }
            if (flag == true) {
                request_array.push({
                    'item_id': item_id
                    , 'row_id': row_id
                    , 'uom_val': uom_val
                    , 'request_qty': request_qty
                    , 'comments': comments
                });
            }
        }
    });
    return flag;
}

function email_validate(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function getItemVendors(from_type) {
    vendor_array = new Array();
    var flag = true;
    $('.form-control').css("border-color", "#fff");
    $('.check_vendor').each(function (index) {
        var status = $(this).is(":checked");
        var vendor_type = 1;
        if (status) {
            var vendor_id = $(this).val();
            if (vendor_id != '') {
                var row_id = $('#VendorRowDatatd' + vendor_id).html();
                var vendorEmail = $('#VendorEmailtd' + vendor_id).val();
                if (from_type == '1') {
                    if (vendorEmail) {
                        status = email_validate(vendorEmail);
                        if (!status) {
                            $('#VendorEmailtd' + vendor_id).css("border-color", "#ff0000");
                            flag = false;
                        }
                    } else {
                        $('#VendorEmailtd' + vendor_id).css("border-color", "#ff0000");
                        flag = false;
                    }
                }
                if (flag == true) {
                    vendor_array.push({
                        'vendor_id': vendor_id
                        , 'row_id': row_id
                        , 'vendor_type': vendor_type
                        , 'vendorEmail': vendorEmail
                    });
                }
            }
        }
    });

    $('#add_new_vendor tr').each(function () {
        var vendor_id = $(this).find("input[name='vendoritem_id_hidden[]']").val();
        var vendor_type = 2;
        if (vendor_id != '') {
            var vendorEmail = $(this).find("input[name='vendorEmail[]']").val();
            var row_id = $(this).find("input[name='vendorrow_id_hidden[]']").val();
            if (from_type == '1') {
                if (vendorEmail) {
                    var status = email_validate(vendorEmail);
                    if (!status) {
                        $(this).find("input[name='vendorEmail[]']").css("border-color", "#ff0000");
                        flag = false;
                    }
                } else {
                    $(this).find("input[name='vendorEmail[]']").css("border-color", "#ff0000");
                    flag = false;
                }
            }
            if (flag == true) {
                vendor_array.push({
                    'vendor_id': vendor_id
                    , 'row_id': row_id
                    , 'vendor_type': vendor_type
                    , 'vendorEmail': vendorEmail
                });
            }
        }
    });
    return flag;
}

function addMoreVendors() {
    $('#add_new_vendor_thead').show();
    $('#add_new_vendor').show();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30

    });
    setTimeout(function () {
        $('.theadfix_wrapper').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
    }, 400);
}


function addVendor() {
    var check_tems = addQuotationItems(1);
    var closing_date = $('#closing_date').val();
    if (closing_date) {
        var token = $('#token_hiddendata').val();
        if (check_tems) {
            if (request_array.length != 0) {
                var fetchitem = JSON.stringify(request_array);
                fetchitem = encodeURIComponent(fetchitem);
                var url = base_url + "/quotation/mapVendorDetails";
                var row_count = $('#vendorrow_count_id').val();
                var rfq_id = $('#rfq_id').val();
                $('#row_count_id').val(parseInt(row_count) + 1);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, fetchitem: fetchitem, rfq_id: rfq_id, vendor_editstatus: vendor_editstatus },
                    beforeSend: function () {
                        $('#addVendorBtn').attr('disabled', true);
                        $('#addVendorSpin').removeClass('fa fa-forward');
                        $('#addVendorSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        $("#addvendorDiv").html(data);
                        $("#addVendorModelData").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function () {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    },
                    complete: function () {
                        $('#addVendorBtn').attr('disabled', false);
                        $('#addVendorSpin').removeClass('fa fa-spinner fa-spin');
                        $('#addVendorSpin').addClass('fa fa-forward');
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                        if (vendor_editstatus == 1) {
                            vendor_editstatus = 0;
                        }
                    }
                });
            } else {
                toastr.warning("Please Enter Any Item");
            }
        }
    } else {
        toastr.warning("Please Enter Closing Date");
        $('#closing_date').focus();
    }
}


function updateVendorEmail(uniquie_id, from_type) {
    var token = $('#token_hiddendata').val();
    var vendor_id = '';
    var vendor_email = '';
    if (from_type == '1') {
        vendor_id = uniquie_id;
        vendor_email = $('#VendorEmailtd' + uniquie_id).val();
    } else if (from_type == '2') {
        vendor_id = $('#vendoritem_id_hidden' + uniquie_id).val();
        vendor_email = $('#vendoremail' + uniquie_id).val();
    }
    if (vendor_id && vendor_email) {
        var status = email_validate(vendor_email);
        if (status) {
            var url = base_url + "/quotation/updateVendorEmail";
            var param = { _token: token, vendor_id: vendor_id, vendor_email: vendor_email };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                },
                success: function (data) {
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                },
            });
        }
    }
}


function searchItemCode(id, event, row_id) { //alert(id);return;
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var ajax_div = $('#' + id).next().attr('id');
    var token = $('#token_hiddendata').val();
    if (value.match(keycheck)) {
        var item_code = $('#' + id).val();
        if (item_code == "") {
            $("#" + ajax_div).html("");
        } else {
            var location_defaultvalue = $('#location_defaultvalue').val();
            var url = base_url + "/quotation/QuotationItemSearch";
            var param = { _token: token, item_code: item_code, location_defaultvalue: location_defaultvalue, row_id: row_id };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#addQuotationRowbtn').attr('disabled', true);
                    $('#addQuotationRowSpin').removeClass('fa fa-plus');
                    $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $('#addQuotationRowbtn').attr('disabled', false);
                    $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
                    $('#addQuotationRowSpin').addClass('fa fa-plus');
                }, error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}


function searchNewVendor(id, event, row_id) {
    var token = $('#token_hiddendata').val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var location = $('#location').val();
        var vendor_name = $('#' + id).val();
        if (vendor_name == "") {
            $("#" + ajax_div).html("");
        } else {
            var url = base_url + "/purchase/vendor_search";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, vendor_name: vendor_name, row_id: row_id },
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function removeRow(row_id) {
    bootbox.confirm({
        message: "Are You Sure You want to delete ?",
        buttons: {
            'confirm': {
                label: "Delete",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $("#row_data_" + row_id).remove();
                row_ct = 1;
                $(".row_class").each(function (i) {
                    $(this).find('.row_count_class').text(row_ct);
                    row_ct++;
                });
            }
        }
    });
}


function removevenderRow(row_id) {
    bootbox.confirm({
        message: "Are You Sure You want to delete ?",
        buttons: {
            'confirm': {
                label: "Delete",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $("#vendorrow_data_" + row_id).remove();
                row_ct = 1;
                $(".vendorrow_class").each(function (i) {
                    $(this).find('.vendorrow_count_class').text(row_ct);
                    row_ct++;
                });
            }
        }
    });
}


function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}

function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');
    var selected = list_items.filter('.liHover');
    if (event.keyCode === 13) {
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    list_items.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('liHover');
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
    vendor_name = htmlDecode(vendor_name);
    $('#newvendor_' + row_id).val(vendor_name);
    $('#vendorcode' + row_id).val(vendor_code);
    $('#vendoremail' + row_id).val(email);
    $('#vendoritem_id_hidden' + row_id).val(id);
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#' + itemCodeListDivId).hide();
    if ($(".vendorrow_class:last").find("input[name='vendoritem_id_hidden[]']").val() != '') {
        addNewVendor();
    }
}

function getItemDescID(itemID) {
    var flag = 1;
    $(".itemdesc_id").each(function (i) {
        var itemID_get = $(this).val();
        if (parseInt(itemID) == parseInt(itemID_get)) {
            flag = 0;
        }
    });
    return flag;
}

function fillItemValues(list, item_code, item_desc, item_id, uom_name, uom_id, uom_value, row_id) {

    var duplicate_check = getItemDescID(item_id);
    if (duplicate_check == 1) {
        var itemCodeListDivId = $(list).parent().attr('id');
        var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
        $('#' + itemCodeTextId).val(item_desc);
        $('#' + itemCodeListDivId).hide();
        $('#' + itemCodeTextId).closest("tr").find("input[name='item_id_hidden[]']").val(item_id);
        fetchUOMData(item_id, row_id, uom_id);
        $('#itemdesc_idhidden' + row_id).val(item_id);
    } else {

        bootbox.confirm({
            message: "This item is already added.<br>Are you sure you want to add ?",
            buttons: {
                'confirm': {
                    label: "Add",
                    className: 'btn-warning',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var itemCodeListDivId = $(list).parent().attr('id');
                    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
                    $('#' + itemCodeTextId).val(item_desc);
                    $('#' + itemCodeListDivId).hide();
                    $('#' + itemCodeTextId).closest("tr").find("input[name='item_id_hidden[]']").val(item_id);
                    fetchUOMData(item_id, row_id, uom_id);
                    $('#itemdesc_idhidden' + row_id).val(item_id);
                }
            }
        });
    }
}

function fetchUOMData(item_id, row_id, uom_id) {
    var url = base_url + "/quotation/getItemUom";
    var item_code = $('#item_code_hidden' + row_id).val();
    var token = $('#token_hiddendata').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, item_id: item_id, uom_id: uom_id, item_code: item_code },
        beforeSend: function () {
            $('#addQuotationRowbtn').attr('disabled', true);
            $('#addQuotationRowSpin').removeClass('fa fa-plus');
            $('#addQuotationRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            response = JSON.parse(response);
            $('#uom_select_id_' + row_id).html(response.option);
            $('#request_qty' + row_id).focus();
        },
        complete: function () {
            if ($(".row_class:last").find("input[name='item_code_hidden[]']").val() != '') {
                getNewRowInserted();
            }
            $('#addQuotationRowbtn').attr('disabled', false);
            $('#addQuotationRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addQuotationRowSpin').addClass('fa fa-plus');
            $('#item_desc_' + row_id).prop("readonly", true);
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function checkallvendor() {
    var status = $('#checkAllVendor').is(":checked");
    if (status) {
        $('.check_vendor').prop('checked', true);
    } else {
        $('.check_vendor').prop('checked', false);
    }
}

function completeRequestQuotation(from_type) {
    bootbox.confirm({
        message: "Are You Sure You want to Generate RFQ ? <br> You Wont able to make any modification after Generate",
        buttons: {
            'confirm': {
                label: "Complete",
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var check_tems = addQuotationItems(1);
                if (check_tems) {
                    var location = $('#location').val();
                    var rfq_id = $('#rfq_id').val();
                    if (from_type == '2') {
                        rfq_id = 0;
                    }
                    var token = $('#token_hiddendata').val();
                    if (request_array.length != 0) {
                        var vendorstatus = getItemVendors(1);
                        if (vendorstatus) {
                            if (vendor_array.length != 0) {
                                var fetchitem = JSON.stringify(request_array);
                                fetchitem = encodeURIComponent(fetchitem);
                                var vendorDetails = JSON.stringify(vendor_array);
                                vendorDetails = encodeURIComponent(vendorDetails);
                                var url = base_url + "/quotation/saveRequestQuotation";
                                var closing_date = $('#closing_date').val();
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: { _token: token, closing_date: closing_date, base_url: base_url, rfq_id: rfq_id, status: 1, location: location, fetchitem: fetchitem, vendorDetails: vendorDetails },
                                    beforeSend: function () {
                                        $('#completeRequestQuotationbtn').attr('disabled', true);
                                        $('#completeRequestQuotationSpin').removeClass('fa fa-save');
                                        $('#completeRequestQuotationSpin').addClass('fa fa-spinner fa-spin');
                                    },
                                    success: function (data) {
                                        toastr.success("RFQ Saved Sucessfully");
                                        var obj = JSON.parse(data);
                                        var rfq_no = obj.rfq_no;
                                        var rfq_id = obj.rfq_id;
                                        confirmQuoatationNo(rfq_no, rfq_id);
                                    },
                                    complete: function () {
                                        $('#completeRequestQuotationbtn').attr('disabled', false);
                                        $('#completeRequestQuotationSpin').removeClass('fa fa-spinner fa-spin');
                                        $('#completeRequestQuotationSpin').addClass('fa fa-save');
                                    },
                                    error: function () {
                                        toastr.error('Please check your internet connection and try again');
                                    }
                                });
                            } else {
                                toastr.warning("Please Enter Any Vendor");
                            }
                        }
                    } else {
                        toastr.warning("Please Enter Any Item");
                    }
                }
            }
        }
    });
}


function saveRequestQuotation(from_type) {
    bootbox.confirm({
        message: "Are You Sure You want to make RFQ Draft Mode ?",
        buttons: {
            'confirm': {
                label: "Draft",
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var check_tems = addQuotationItems(1);
                if (check_tems) {
                    var location = $('#location').val();
                    var token = $('#token_hiddendata').val();
                    var rfq_id = $('#rfq_id').val();
                    if (from_type == '2') {
                        rfq_id = 0;
                    }
                    if (request_array.length != 0) {
                        var vendorstatus = getItemVendors(2);
                        if (vendorstatus) {
                            if (vendor_array.length != 0) {
                                var fetchitem = JSON.stringify(request_array);
                                fetchitem = encodeURIComponent(fetchitem);
                                var vendorDetails = JSON.stringify(vendor_array);
                                vendorDetails = encodeURIComponent(vendorDetails);
                                var url = base_url + "/quotation/saveRequestQuotation";
                                var closing_date = $('#closing_date').val();
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: { _token: token, closing_date: closing_date, base_url: base_url, rfq_id: rfq_id, status: 0, location: location, fetchitem: fetchitem, vendorDetails: vendorDetails },
                                    beforeSend: function () {
                                        $('#saveRequestQuotationbtn').attr('disabled', true);
                                        $('#saveRequestQuotationspin').removeClass('fa fa-save');
                                        $('#saveRequestQuotationspin').addClass('fa fa-spinner fa-spin');
                                    },
                                    success: function (data) {
                                        var obj = JSON.parse(data);
                                        var rfq_no = obj.rfq_no;
                                        var rfq_id = obj.rfq_id;
                                        toastr.success("RFQ Saved Sucessfully");
                                        confirmQuoatationNo(rfq_no, rfq_id);
                                    },
                                    complete: function () {
                                        $('#saveRequestQuotationbtn').attr('disabled', false);
                                        $('#saveRequestQuotationspin').removeClass('fa fa-spinner fa-spin');
                                        $('#saveRequestQuotationspin').addClass('fa fa-save');
                                    },
                                    error: function () {
                                        toastr.error('Please check your internet connection and try again');
                                    }
                                });
                            } else {
                                toastr.warning("Please Enter Any Vendor");
                            }
                        }
                    } else {
                        toastr.warning("Please Enter Any Item");
                    }
                }
            }
        }
    });
}

function viewRFQ(rfq_no, head_id) {
    var token = $('#token_hiddendata').val();
    var url = base_url + "/quotation/listQuotationItems";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, head_id: head_id },
        beforeSend: function () {
            $('#quotationListHeader').html(rfq_no);
        },
        success: function (data) {
            $("#quotationListDiv").html(data);
            $("#quotationListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {

        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function closeRFQView() {
    url = base_url + "/quotation/listRquestforQuotation";
    document.location.href = url;
}

function confirmQuoatationNo(rfq_no, rfq_id) {
    var url = '';
    $update_string = 'Saved';
    if (!rfq_no) {
        rfq_no = $('#rfq_no').val();
        $update_string = 'Updated';
    }
    bootbox.confirm({
        message: "RFQ Successfully " + $update_string + " <br> RFQ No. :" + rfq_no,
        buttons: {
            'confirm': {
                label: "RFQ List",
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'View RFQ',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                url = base_url + "/quotation/listRquestforQuotation";
                document.location.href = url;
            } else {
                viewRFQ(rfq_no, rfq_id);
            }
        }
    });
}
