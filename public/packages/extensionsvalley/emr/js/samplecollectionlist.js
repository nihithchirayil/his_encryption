$(document).ready(function() {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "Select", maximumSelectionSize: 6 });

    if ($('#auto_acknowledge').val() == 1) {
        $('#aknowledge_sample').fadeOut();
    } else {
        $('#aknowledge_sample').fadeIn();
    }
    getSampleList(1);
    tinyMceCreate('labProfileData', 460);
    if ($('#is_lab_admin').val() == 0) {
        $('.SamplefinalizeDataBtn').attr('disabled', true);
        $('.SampledefinalizeDataBtn').attr('disabled', true);
    } else {
        $('.SamplefinalizeDataBtn').attr('disabled', false);
        $('.SampledefinalizeDataBtn').attr('disabled', false);
    }

});
$(document).on("click", function(event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});
$(document).on('click', '.advanceSearchBtn', function(event) {
    advancePatientSearch(1);
});
//----Hidden Filed Search--------------------------------------------------------------------
var token = $("#c_token").val();
var base_url = $("#base_url").val();
var finalize_patient = 0;
var finalize_array = [];
var sample_status = [];
var status_array = [];
var sample_no_array = [];
var sample_mail_data = ' ';
var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;


function tinyMceCreate(text_area, tinymce_height) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help',
        toolbar: 'undo redo | bold italic underline strikethrough |fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | Custom Bullet ',
        browser_spellcheck: true,
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,
        importcss_append: true,
        height: tinymce_height,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
    });
}

$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var set_service_id = $('#set_service_id').val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = base_url + "/sampleCollection/setAjaxSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'set_service_id=' + set_service_id + '&search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});


function getSampleList(from_type) {
    $('.checkit').attr('disabled', true);
    var paid_bill = $('#paid').is(":checked");
    var Unpaid_bill = $('#unpaid').is(":checked");
    var cash = $('#cash').is(":checked");
    var credit = $('#credit').is(":checked");
    var insurance = $('#insurance').is(":checked");
    var credit_company = $('#credit_company').is(":checked");
    var bill_no = $('#bill_no').val();
    var test = $('#test_hidden').val();
    var department = $('#department').val();
    var sub_department = $('#sub_department').val();
    var report_type = $('#report_type').val();
    var payment_type = $('#payment_type').val();
    var sample = $('#sample').val();
    var sample_status_string = JSON.stringify(sample_status);
    var sample_no = $('#sample_no').val();
    var visit_type = $('#visit').val();
    var patient = $('#patient_name').val();
    var uhid = $('#patient_uhid').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var finalize_array_string = JSON.stringify(finalize_array);
    var params = {
        credit_company: credit_company,
        cash: cash,
        credit: credit,
        incurance: insurance,
        test: test,
        department: department,
        Unpaid_bill: Unpaid_bill,
        paid_bill: paid_bill,
        sub_department: sub_department,
        bill_no: bill_no,
        report_type: report_type,
        sample: sample,
        payment_type: payment_type,
        sample_status: sample_status_string,
        sample_no: sample_no,
        visit_type: visit_type,
        uhid: uhid,
        from_date: from_date,
        to_date: to_date,
        patient: patient,
        finalize_array_string: finalize_array_string,
    };
    var url = base_url + "/sampleCollection/getSampleList";
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function() {
            $('#list_container').html(' ');
            var modal1 = $('#labNumericBillsModel').is(':visible');
            var modal2 = $('#labProfileBillsModel').is(':visible');
            if (!modal1 && !modal2) {
                $('#list_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            }
            $('#search_sample').attr('disabled', true);
            $("#sample_spin").removeClass("fa fa-search");
            $("#sample_spin").addClass("fa fa-spinner fa-spin");
            if (from_type == '1') {
                finalize_patient = 0;
                finalize_array = [];
                status_array = [];
                sample_no_array = [];
            }
        },
        success: function(data) {
            if (data) {
                $('#list_container').html(data);
            }
        },
        complete: function() {
            $('#search_sample').attr('disabled', false);
            $('#list_container').LoadingOverlay("hide");
            $("#sample_spin").removeClass("fa fa-spinner fa-spin");
            $("#sample_spin").addClass("fa fa-search");
            //  checkFinazlePatient(finalize_patient);
            $('.finalizle_test').prop('checked', false);
            $('.checkit').attr('disabled', false);


        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}



function reset() {
    $('.reset').val('');
    $('.filters').val('');
    $('.select2').val('All').select2();
    $('#payment_type').val('All');
    $('.checkit').prop('checked', false);
    $('#add_class').removeClass()
    var current_date = $('#current_date').val();
    $('#from_date').val(current_date);
    $('#to_date').val(current_date);
    getSampleList(1);
}

function displayDetails(id) {
    var head_id = $('#head_id_hidden').val();
    if (head_id != id) {
        $('#popup' + head_id).hide();
    }
    $('#head_id_hidden').val(id);
    if ($('#popup' + id).is(":visible")) {

        $('#popup' + id).hide();
    } else if ($('#popup' + id).is(":hidden")) {
        $('#popup' + id).show();
    }

    $('#ppclose' + id).click(function() {
        $('#popup' + id).hide();
    });


}

function displayCancelStatus(head_id, sample_id, detail_id, sampstatus) {
    bootbox.confirm({
        message: "Are you sure,you want to de-collect this Sample?",
        buttons: {
            confirm: {
                label: "De-collect",
                className: "btn-warning",
                default: "true",
            },
            cancel: {
                label: "Cancel",
                className: "btn-danger",
            },
        },
        callback: function(result) {
            if (result) {
                $('#cancelSetup').modal('show');
                $('#head_id_hidden').val(head_id);
                $('#sample_id_hidden').val(sample_id);
                $('#bill_detail_id').val(detail_id);
                $('#sampstatus').val(sampstatus);
            }
        },
    });
}


function cancelReport() {
    var flag = true;
    var sampstatus = $('#sampstatus').val();
    if (sampstatus == 6) {
        if (confirm('Finalized sample will be De-collected?')) {
            flag = true;
        } else {
            flag = false;
        }
    }
    if (flag) {
        var head_id = $('#head_id_hidden').val();
        var sample_id = $('#sample_id_hidden').val();
        var detail_id = $('#bill_detail_id').val();

        var cancelled_reason = $('#Cancel_reason').val();
        if (head_id != 0) {
            var url = base_url + "/sampleCollection/cancelReport";
            $.ajax({
                type: "POST",
                url: url,
                data: { head_id: head_id, sample_id: sample_id, detail_id: detail_id, cancelled_reason: cancelled_reason },
                beforeSend: function() {
                    $('#Cancel').attr('disabled', true);
                    $('#Cancel_spin').removeClass('fa fa-trash');
                    $('#Cancel_spin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    if (data) {
                        toastr.success('De-collected successfully.');
                        getSampleList(1);
                        $('#cancel' + head_id).hide();
                        $('#cancelSetup').modal('hide');
                        $('#paid_bg' + head_id).removeClass(["cash_card_bg", "credit_bg", "company_credit_bg", "insurance_bg"]);
                        $('#cancel_bg' + head_id).addClass('cancelled-bg');
                        $('#popup' + head_id).hide();
                        $('#head_id_hidden').val('');
                        $('#sample_id_hidden').val('');
                        $('#bill_detail_id').val('');
                        $('#Cancel_reason').val('');
                        $('#sampstatus').val('');
                    }
                },
                complete: function() {
                    $('#Cancel').attr('disabled', false);
                    $('#Cancel_spin').removeClass('fa fa-spinner fa-spin');
                    $('#Cancel_spin').addClass('fa fa-trash');
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });
        }
    }
}
$('.checkit').click(function() {
    var name = $(this).attr('name');
    if ($(this).hasClass('checked')) {
        $(this).prop('checked', false);
        $(this).removeClass('checked');
        getSampleList(1);
    } else {
        $('input[name="' + name + '"]').removeClass('checked');
        $(this).addClass('checked');
        getSampleList(1);
    }
});
$("#department").change(function() {
    getRelativeSubDepart();
});

function getRelativeSubDepart() {
    var dept = $('#department').val();
    if (dept != 'All') {
        var url = base_url + "/sampleCollection/getRelativeSubDepart";

        $.ajax({
            type: "POST",
            url: url,
            data: { dept: dept },
            beforeSend: function() {
                $("#sub_box").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: "#009869"
                });
                $("#sub_department").val(' ').select2();
            },
            success: function(data) {
                if (data) {
                    $("#sub_department").html('<option value="All">Select</option>');
                    $.each(data, function(key, value) {
                        $("#sub_department").append(
                            "<option value=" +
                            value.id +
                            "> " +
                            value.name +
                            "</option>"
                        );
                    });
                }
            },
            complete: function() {
                $("#sub_box").LoadingOverlay("hide");

            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }

}

function finalizle_test(billdetailid, patient_id, sample_status, sample_no) {
    checkFinazlePatient(patient_id);
    var flag = 0;
    $('.finalizle_test' + patient_id).each(function(index) {
        if ($(this).is(":checked")) {
            flag = 1;
            return;
        }
    });
    var status = $('#finalizle_test' + billdetailid).is(":checked");
    var check = finalize_array.includes(billdetailid);
    if (status) {
        if (!check) {
            finalize_patient = patient_id;
            finalize_array.push(billdetailid);
            status_array.push(sample_status);
            sample_no_array.push(sample_no);
        }
    } else {
        var pos = finalize_array.indexOf(billdetailid);
        var pos_status = status_array.indexOf(sample_status);
        var pos_sample_no = sample_no_array.indexOf(sample_no);
        if (pos_sample_no > -1) {
            sample_no_array.splice(pos, 1);
        }
        if (pos_status > -1) {
            status_array.splice(pos, 1);
        }
        if (pos > -1) {
            finalize_array.splice(pos, 1);
        }
    }
   

    if (flag == 0 && (finalize_array.length) == 0) {
        $('.finalizle_test').attr('disabled', false);
        finalize_array = [];
        sample_status = [];
        status_array = [];
        sample_no_array = [];
        finalize_patient = 0;
    }

}


function convertToRtf(plain) {
    plain = plain.replace(/\n/g, "\\par\n");
    return "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang2057{\\fonttbl{\\f0\\fnil\\fcharset0 Microsoft Sans Serif;}}\n\\viewkind4\\uc1\\pard\\f0\\fs17 " + plain + "\\par\n}";
}

function convertToPlain(rtf) {
    rtf = rtf.replace(/\\par[d]?/g, "");
    return rtf.replace(/\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?/g, "").trim();
}

function resultEntry(lab_id, sample_id, bill_detail_id, samplestatus, limit_stage, report_code, from_type = 1) {
    $('#sample_id_hidden').val(sample_id);
    $('#report_code').val(report_code);
    $('#lab_id').val(lab_id);
    $('#bill_detail_id').val(bill_detail_id);
    $('#limit_stage').val(limit_stage);
    setButtonType(samplestatus)
    tinymce.get('labProfileData').setContent('');
    var url = base_url + "/sampleCollection/sampleResultEntry";
    $.ajax({
        type: "POST",
        url: url,
        data: { lab_id: lab_id, sample_id: sample_id, bill_detail_id: bill_detail_id, samplestatus: samplestatus, report_code: report_code },
        beforeSend: function() {
            $('#list_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {
            var obj = JSON.parse(data);
            if(obj.sizeOfDetailArray <=0){
                toastr.warning('Unavailable Service !');
                return;
            }else{
                if (report_code == 'T') {
                    tinymce.get('labProfileData').setContent(convertToPlain(obj.actual_result));
                    $('#labProfileBillsModelHeader').html(obj.title);
                    $('#labProfileBillsModelDiv').html(obj.html);
                    if (from_type != 2) {
                        $("#labProfileBillsModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                    var sample_no = $('#textsample_no_hidden').val();
                    $('#sample_number_hidden').val(sample_no);
                    var doctor_id = $('#textdoctor_id_hidden').val();
                    $('#doctor_id_hidden').val(doctor_id);
                } else {
                    $('#labNumericBillsModelHeader').html(obj.title);
                    $('#labNumericBillsModeDiv').html(obj.html);
                    if (from_type != 2) {
                        $("#labNumericBillsModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
    
                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                    var sample_no = $('#nummbericsample_no_hidden').val();
                    $('#sample_number_hidden').val(sample_no);
                    var doctor_id = $('#nummbericdoctor_id_hidden').val();
                    $('#doctor_id_hidden').val(doctor_id);
                }
            }
            
        },
        complete: function() {
            $('#list_container').LoadingOverlay("hide");
            $('#popup' + lab_id).hide();
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}


function getLabDetailsData() {
    var lab_id = $('#lab_id_hidden').val();
    var url = base_url + "/sampleCollection/getLabDetailsData";
    $.ajax({
        type: "POST",
        url: url,
        data: { lab_id: lab_id },
        beforeSend: function() {
            $('#list_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {
            var obj = JSON.parse(data);
            $('#lab_details_data').html(obj.html);
        },
        complete: function() {
            $('#list_container').LoadingOverlay("hide");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getCheckedSamples() {
    sample_status = [];
    $('.sample_status').each(function(index) {
        var status = $(this).is(":checked");
        if (status) {
            sample_status.push($(this).val());
        }
    });
    getSampleList(1);
}

function checkFinazlePatient(patient_id) {
    if (patient_id != 0) {
        $('.finalizle_test').attr('disabled', true);
        $('.finalizle_test' + patient_id).attr('disabled', false);
    }
}

function getSampleDataList() {
    var report_code = $('#report_code').val();
    var result_array = new Array();

    if (report_code == 'F') {
        $('#getSampleResultDataList tr').each(function() {
            var resultDetailID = $(this).find("input[name='resultDetailID']").val();
            var actual_result = $(this).find("input[name='actual_result']").val();
            var bill_detail_id = $(this).find("input[name='bill_detail_id']").val();
            var methodology = $(this).find("select[name='methodology']").val();
            var detail_comments = $(this).find("input[name='detail_comments']").val();
            var detail_order = $(this).find("input[name='detail_order']").val();
            result_array.push({
                'resultDetailID': resultDetailID,
                'actual_result': actual_result,
                'bill_detail_id': bill_detail_id,
                'methodology': methodology,
                'detail_comments': detail_comments,
                'detail_order': detail_order,
            });
        });
    } else if (report_code == 'T') {
        var actual_result = tinymce.get('labProfileData').getContent();
        var resultDetailID = $('#resultDetailID_hidden').val();
        var bill_detail_id = $('#bill_detail_id_hidden').val();

        result_array.push({
            'resultDetailID': resultDetailID,
            'actual_result': convertToRtf(actual_result),
            'bill_detail_id': bill_detail_id,
            'methodology': 0,
            'detail_comments': '',
            'detail_order': 0,
        });
    } else {
        $('#getSampleResultDataList tr').each(function() {
            var resultDetailID = $(this).find("input[name='resultDetailID']").val();
            var actual_result = $(this).find("input[name='actual_result']").val();
            var bill_detail_id = $(this).find("input[name='bill_detail_id']").val();
            var methodology = $(this).find("select[name='methodology']").val();
            var detail_comments = $(this).find("input[name='detail_comments']").val();
            var detail_order = $(this).find("input[name='detail_order']").val();
            result_array.push({
                'resultDetailID': resultDetailID,
                'actual_result': actual_result,
                'bill_detail_id': bill_detail_id,
                'methodology': methodology,
                'detail_comments': detail_comments,
                'detail_order': detail_order,
            });
        });
    }
    return result_array;
}
var confirm_password = '<label for="" class="" id="confirm_password">Please confirm your login password before finalizing.</label><br><input type="password" id="con_password" class="form-control">';
var report_initiation_type = 6; //its for finalized represtation
function confirmSampleCollectionResultEntry(from_type) {
    var limit_stage = $('#limit_stage').val();
    if (limit_stage == 1 && (from_type == 2 || from_type === 3 || from_type == 4)) {
        toastr.warning('Please make sure selected sample is paid.');
        return;
    }
    var sample_id = $('#sample_id_hidden').val();
    var bill_detail_id = $('#bill_detail_id').val();
    if (from_type == 2) {
        report_initiation_type = 5;
        confirmSample(sample_id, bill_detail_id);
    } else if (from_type == 3) {
        report_initiation_type = 6;
        confirmSample(sample_id, bill_detail_id);
    } else if (from_type == 4) {
        report_initiation_type = 3;
        confirmSample(sample_id, bill_detail_id);
    } else if (from_type == 5) {
        bootbox.confirm({
            message: confirm_password,
            buttons: {
                confirm: {
                    label: 'Save & Finalize',
                    className: "btn-primary",
                    default: "true",
                },
                cancel: {
                    label: "Cancel",
                    className: "btn-danger",
                },
            },
            callback: function(result) {
                if (result) {
                    saveSampleCollectionResultEntry(from_type);
                }
            },
        });
    } else {
        saveSampleCollectionResultEntry(from_type)
    }

}

function saveSampleCollectionResultEntry(from_type) {
    var url = base_url + "/sampleCollection/saveSampleCollectionResultEntry";
    var report_code = $('#report_code').val();
    var password = '';
    if (from_type == 5) {
        password = $('#con_password').val();
        if (password == '') {
            toastr.warning('Please enter your login password.');
            if (report_code != 'F') {
                $('.SamplesaveandfinalizeDataBtnN').click();
            } else {
                $('.SamplesaveandfinalizeDataBtnF').click();
            }
            return;
        }
    }
    var uhid = $('#result_entrypatient_uhid').html();
    var sample_id = $('#sample_id_hidden').val();
    var sample_no = $('#sample_number_hidden').val();
    var lab_id = $('#lab_id').val();
    var doctor_id = $('#doctor_id_hidden').val();
    var bill_detail_id = $('#bill_detail_id').val();
    var sample_data_string = getSampleDataList();
    if(report_code=='F'||report_code=='T'){
        var lab_notes = $('#lab_notes_'+ report_code).val();
        var lab_remarks = $('#lab_remarks_'+ report_code).val();
        var phlebotomist_comments = $('#phlebotomist_comments_'+ report_code).val();
    }else{
        var lab_notes = $('#lab_notes').val();
        var lab_remarks = $('#lab_remarks').val();
        var phlebotomist_comments = $('#phlebotomist_comments').val(); 
    }
   
    var params = { password: password, lab_id: lab_id, uhid: uhid, lab_notes: lab_notes, lab_remarks: lab_remarks, phlebotomist_comments: phlebotomist_comments, report_code: report_code, sample_id: sample_id, bill_detail_id: bill_detail_id, sample_no: sample_no, doctor_id: doctor_id, from_type: from_type, sample_data_string: sample_data_string };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function() {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {
            if (data.mesg == 101) {
                toastr.warning("Invalid password.")
            } else if (data) {
                toastr.success('' + data.mesg + '');
                var samplestatus = $('#samplestatus_hidden').val();
                var limit_stage = $('#limit_stage').val();
                resultEntry(lab_id, sample_id, bill_detail_id, samplestatus, limit_stage, report_code, 2)
                getSampleList(1);
                if (from_type == 1 && report_initiation_type == 1) {
                    $('.modal').modal('hide');
                }

            }
        },
        complete: function() {
            $('body').LoadingOverlay("hide");
        },
    });
}

function confirmSample(sample_id = 0, bill_detail_id = 0) {
    if (sample_id != 0 && sample_no != 0) {
        finalize_array = [];
        sample_no_array = [];
        sample_no_array.push(sample_id);
        finalize_array.push(bill_detail_id);
    }
    if (report_initiation_type != 6) {
        initiateResultByOneStage(); //de-finalize
    } else {
        var set_box = bootbox.confirm({
            message: confirm_password,
            buttons: {
                confirm: {
                    label: 'Finalize',
                    className: "btn-primary",
                    default: "true",
                },
                cancel: {
                    label: "Cancel",
                    className: "btn-danger",
                },
            },
            callback: function(result) {
                if (result) {
                    initiateResultByOneStage();
                }
            },
        });
        set_box.on('shown.bs.modal', function() {
            $("#con_password").focus();

        });
    }

}


function initiateResultByOneStage() {
    var password = '   ';
    if (report_initiation_type == 6) {
        password = $('#con_password').val();
        if (password == '') {
            toastr.warning('Please enter your login password.');
            $('#finalised_btn').click();
            return;
        }
    }
    var url = base_url + "/sampleCollection/initiateResultByOneStage";
    $.ajax({
        type: "POST",
        url: url,
        data: { token: token, sample_id: sample_no_array[0], finalize_array: finalize_array, password: password, report_initiation_type: report_initiation_type },
        beforeSend: function() {
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data) {
            if (data) {
                if (data.mesg == 101) {
                    toastr.warning('Invalid password.');
                    return;
                } else {
                    toastr.success('' + data.mesg + '');
                    $('.modal').modal('hide');
                    getSampleList(1)
                    return;
                }

            }
        },
        complete: function() {
            $('body').LoadingOverlay("hide");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });

}
$('#viewconfirmsample').click(function() {
    setSampleView();
});
$('#print_result').click(function() {
    if (status_array.length > 0) {
        var status1 = checkIfAllSameStatus(status_array);
        var status2 = checkIfAllSameStatus(sample_no_array);

        if (status1) {
            if (status2) {

                if (status_array[0] == 6) {
                    //  status_array[0] = 7;
                    $('#print_config_modal').modal('toggle');

                } else {
                    toastr.warning('Please make sure sample is finalized.');
                    return;
                }
            } else {
                toastr.warning('Please make sure you selected samples with same sample number.');
                return;
            }

        } else {
            toastr.warning('Please make sure you selected samples with same status.');
            return;
        }

    }
});
// $('#viewandmailreport').click(function() {
//     setSampleView(2);
// });

function setSampleView(view_type = 0) {
    if (status_array.length > 0) {
        var status1 = checkIfAllSameStatus(status_array);
        var status2 = checkIfAllSameStatus(sample_no_array);

        if (status1) {
            if (status2) {
                if (view_type == 1) {

                    var include_hospital_header = $("#showTitle").prop('checked') ? 1 : 0;


                } else if (view_type == 2) {
                    status_array[0] = 7;
                    $('#setmodalhead').html('VIEW & MAIL REPORT');
                }

                setButtonType(status_array[0]);
                if (status_array[0] == 5 || status_array[0] == 6 || status_array[0] == 3 || status_array[0] == 7) {
                    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
                    var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
                    var url = base_url + "/sampleCollection/viewOfFinalizedResult";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: { include_hospital_header: include_hospital_header, token: token, sample_no: sample_no_array[0], finalize_array: finalize_array, location_name: location_name, view_type: view_type },
                        beforeSend: function() {
                            if (view_type == 1) {
                                $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                                $('#print_sample').attr('disabled', true);
                                $('#print_spin').removeClass('fa fa-print');
                                $('#print_spin').addClass('fa fa-spinner fa-spin');
                            } else {
                                $('#viewconfirmsample').attr('disabled', true);
                                $('#viewconfirmsample_spin').removeClass('fa fa-eye');
                                $('#viewconfirmsample_spin').addClass('fa fa-spinner fa-spin');
                            }

                        },
                        success: function(data) {

                            if (data) {
                                if (view_type == 1) {
                                    PrintSample(data);
                                } else {
                                    if (view_type == 2) {

                                    }
                                    $('#viewandconfirmresult_box').html(data);
                                    $('#viewandconfirmresult').modal('show');
                                    $('.theadscroll').perfectScrollbar({
                                        wheelPropagation: true,
                                        minScrollbarLength: 30
                                    });
                                }
                            }


                        },
                        complete: function() {
                            if (view_type == 1) {
                                $('#print_sample').attr('disabled', false);
                                $('#print_spin').removeClass('fa fa-spinner fa-spin');
                                $('#print_spin').addClass('fa fa-print');
                                $('body').LoadingOverlay("hide");
                            } else {
                                $('#viewconfirmsample').attr('disabled', false);
                                $('#viewconfirmsample_spin').removeClass('fa fa-spinner fa-spin');
                                $('#viewconfirmsample_spin').addClass('fa fa-eye');
                            }
                        },

                        error: function() {
                            toastr.error("Error Please Check Your Internet Connection");
                        },
                    });
                } else {
                    toastr.warning("Make sure sample result is entered.");
                    return;
                }



            } else {
                toastr.warning('Please make sure you selected samples with same sample number.');
                return;
            }

        } else {
            toastr.warning('Please make sure you selected samples with same status.');
            return;
        }

    }
}

function setButtonType(type) {
    if (type == 6) {
        if ($('#is_lab_admin').val() == 0) {
            $('#finalised_btn').attr('disabled', true);
        } else {
            $('#finalised_btn').attr('disabled', false);
        }
        $('#finalised_btn').attr('disabled', false);
        $('#finalised_btn').html('<i id="finalised_spin" class="fa fa-check padding_sm"></i>De-finalize');
        report_initiation_type = 3;
        $('.SampleentryDataBtn').fadeOut();
        $('.SamplecertifyDataBtn').fadeOut();
        $('.SamplefinalizeDataBtn').fadeOut();
        $('.SampledefinalizeDataBtn').fadeIn();
        $('.SamplesaveandfinalizeDataBtn').fadeOut();

    } else if (type == 5) {
        if ($('#is_lab_admin').val() == 0) {
            $('#finalised_btn').attr('disabled', true);
        } else {
            $('#finalised_btn').attr('disabled', false);
        }
        $('#finalised_btn').html('<i id="finalised_spin" class="fa fa-check padding_sm"></i>Finalize');
        report_initiation_type = 6;
        $('.SampleentryDataBtn').fadeIn();
        $('.SamplecertifyDataBtn').fadeOut();
        $('.SampledefinalizeDataBtn').fadeOut();
        $('.SamplefinalizeDataBtn').fadeIn();
        $('.SamplesaveandfinalizeDataBtn').fadeOut();

    } else if (type == 3) {
        $('#finalised_btn').attr('disabled', false);
        $('#finalised_btn').html('<i id="finalised_spin" class="fa fa-check padding_sm"></i>Provisionally Certified');
        report_initiation_type = 5;
        $('.SampleentryDataBtn').fadeIn();
        $('.SamplecertifyDataBtn').fadeIn();
        $('.SampledefinalizeDataBtn').fadeOut();
        $('.SamplefinalizeDataBtn').fadeOut();
        $('.SamplesaveandfinalizeDataBtn').fadeOut();

    } else if (type == 1) {
        report_initiation_type = 1;
        $('#finalised_btn').attr('disabled', false);
        $('.SamplesaveandfinalizeDataBtn').fadeOut();
        $('.SampleentryDataBtn').fadeIn();
        $('.SamplecertifyDataBtn').fadeOut();
        $('.SampledefinalizeDataBtn').fadeOut();
        $('.SamplefinalizeDataBtn').fadeOut();
    } else if (type == 7) {
        // $('#finalised_btn').attr('disabled', false);
        // $('#finalised_btn').html('<i id="mail_sample" class="fa fa-mail padding_sm"></i>Send Mail');
        // $('#finalised_btn').attr('onclick', 'emailSample()');

    }
}

function checkIfAllSameStatus(arr) {
    return arr.every((v, i, a) => v === a[0]);
}
$('#aknowledge_sample').click(function() {

    aknowledgeSample();

});

function aknowledgeSample() {
    // if (status_array.length > 0) {
    //     var status1 = checkIfAllSameStatus(status_array);
    //     var status2 = checkIfAllSameStatus(sample_no_array);

    //     if (status1) {
    //         if (status2) {

    if (status_array[0] == 0) {
        var url = base_url + "/sampleCollection/aknowledgeSample";
        $.ajax({
            type: "POST",
            url: url,
            data: { token: token, sample_id: sample_no_array[0], finalize_array: finalize_array, status_array: status_array },
            beforeSend: function() {
                $('#aknowledge_sample').attr('disabled', true);
                $('#aknowledge_spin').removeClass('fa fa-eye');
                $('#aknowledge_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function(data) {
                if (data) {
                    toastr.success("Sample acknowledged successfully.");
                    getSampleList(1);

                }
            },
            complete: function() {
                $('#aknowledge_sample').attr('disabled', false);
                $('#aknowledge_spin').removeClass('fa fa-spinner fa-spin');
                $('#aknowledge_spin').addClass('fa fa-eye');
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
                return;
            },
        });
    }


    // } else {
    //     toastr.warning('Please make sure you selected samples with same sample number.');
    //     return;
    // }

    // } else {
    //     toastr.warning('Please make sure you selected samples with same status.');
    //     return;
    // }

}

$('#payment_type').change(function() {
    var type = $("#payment_type").val()
    if (type == 'cash/Card') {
        var set_class = 'round_blue';
    } else if (type == 'ipcredit') {
        var set_class = 'lime';
    } else if (type == 'insurance') {
        var set_class = 'pink';
    } else if (type == 'credit') {
        var set_class = 'round_yellow';
    }
    $('#add_class').removeClass()
    $('#add_class').addClass(set_class);
})



function PrintSample(data) {
    if (data) {
        var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
        winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data + '<script>setTimeout(function(){window.print();window.close();},1000)</script>');
        //;
    }
    // window.location.reload();
}

$('#sample_print_close').click(function() {
    reset_checked_data();
})

function reset_checked_data() {
    $('.finalizle_test').prop('checked', false);
    $('.finalizle_test').attr('disabled', false);
    finalize_array = [];
    sample_status = [];
    status_array = [];
    sample_no_array = [];
    finalize_patient = 0;
}

function checkUserPassword() {
    bootbox.confirm({
        message: confirm_password,
        buttons: {
            confirm: {
                label: 'FINALIZE',
                className: "btn-primary",
                default: "true",
            },
            cancel: {
                label: "Cancel",
                className: "btn-danger",
            },
        },
        callback: function(result) {
            initiateResultByOneStage(result);
        },
    });
}

$('#labNumericBillsModel').on('keyup', 'input', function(e) {
    if (e.which == 39)
        $(this).closest('td').next().find('input').focus();
    else if (e.which == 37)
        $(this).closest('td').prev().find('input').focus();
    else if (e.which == 40)
        $(this).closest('tr').next().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
    else if (e.which == 38)
        $(this).closest('tr').prev().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
});

function viewLabResult(bill_detail_id,sample_no,uhid){
    var url = base_url + "/sampleCollection/viewLabResult";
    $.ajax({
        type: "POST",
        url: url,
        data: { bill_detail_id: bill_detail_id, token: token,sample_no:sample_no, uhid: uhid,location_name: location_name},
        beforeSend: function() {
           
                $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            

        },
        success: function(data) {

            if (data) {
               console.log(data);
                    $('#viewandconfirem_box').html(data);
                    $('#viewaonlyresult').modal('show');
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                
            }


        },
        complete: function() {
                $('body').LoadingOverlay("hide");
      
        },

        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });

}