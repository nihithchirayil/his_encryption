$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.select2').select2();
});


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(name);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});


function getsubdepartments() {

    var url = route_json.getSubdepartments;
    var department_id = '';

    department_id = $('#department_id').val();
    var parm = { department_id: department_id };
    console.log(department_id);
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#search_results').attr('disable', true);
            $('#search_results_spin').removeClass('fa fa-search');
            $('#search_results_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#subdepatmentdiv').html(data)
        },

        complete: function () {
            $('#search_results').attr('disable', false);
            $('#search_results_spin').removeClass('fa fa-spinner fa-spin');
            $('#search_results_spin').addClass('fa fa-search');
            $('#subdepartment_id').select2();
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });
}


function getServiceMasterItemsList() {

    //var url = route_json.getServiceItemsList;
    var url = $('#base_url').val() + "/report/getServiceMasterItemsList";
    var subdepartment_id = '';

    subdepartment_id = $('#subdepartment_id').val();
    var parm = { subdepartment_id: subdepartment_id };
    //console.log(subdepartment_id);
    //return;
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#search_results').attr('disable', true);
            $('#search_results_spin').removeClass('fa fa-search');
            $('#search_results_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#itemsdiv').html(data)
        },

        complete: function () {
            $('#search_results').attr('disable', false);
            $('#search_results_spin').removeClass('fa fa-spinner fa-spin');
            $('#search_results_spin').addClass('fa fa-search');
            $('#item_list').select2();
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });
}


function DepartmentWiseProcedureIncomeData() {

    var url = route_json.DepartmentWiseProcedureIncomeData;

    //-------filters---------------------------------------------

    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();


    var parm = { from_date: from_date, to_date: to_date };


    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            console.log(html);
            $('#ResultsViewArea').html(html);

            $('#print_results').removeClass('disabled');

            $('#csv_results').removeClass('disabled');


        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}

function depatment_change() {
    var status = $('#is_rob').is(":checked");
    if (status) {
        $('#nondepartment_data_div').hide();
        $('#department_data_div').show();
        getsubdepartments(1);
    } else {
        $('#nondepartment_data_div').show();
        $('#department_data_div').hide();
        getsubdepartments(2);
    }
}


function getReportData() {

    var base_url = $('#base_url').val();
    var token = $('#c_token').val();
    var url = base_url + '/report/DepartmentWiseProcedureIncomeDetailData';

    var filters_list = new Object();


    var filters_value = '';
    var filters_id = '';
    filters_list['_token'] = token;
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'admission_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'admission_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "POST",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}
//-------filters-----------------------------------------

function search_clear() {
    location.reload(true);
    $('#is_rob').attr('checked', false);
    var current_date = $('#current_date').val();
    $('#admission_date_from').val(current_date);
    $('#admission_date_to').val(current_date);
    $('#paid_status').val('');
    $('#department_id').val('');
    $('#department_id').select2();
    $('#nondepartment_id').val('');
    $('#nondepartment_id').select2();
}
