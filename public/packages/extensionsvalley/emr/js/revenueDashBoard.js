$(document).ready(function () {
    getCustomDates(1)
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});
var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();
var company_code = $('#company_code').val();
function getAllGraphs() {
    if (company_code == 'ROHINI') {
        setTimeout(function () {
            getAllTotalCollection(1);
        }, 1000);
        setTimeout(function () {
            getAllTotalCollection(2);
        }, 1500)
        setTimeout(function () {
            getAllTotalCollection(3);
        }, 2000);
        setTimeout(function () {
            getAllTotalCollection(4);
        }, 2500);
        setTimeout(function () {
            getAllTotalCollection(15);
        }, 3000);
        setTimeout(function () {
            getAllTotalCollection(5);
        }, 3500);
        setTimeout(function () {
            getAllTotalCollection(6);
        }, 4000);
        setTimeout(function () {
            getAllTotalCollection(7);
        }, 4500);
        setTimeout(function () {
            getAllTotalCollection(8);
        }, 5000);
        setTimeout(function () {
            getAllTotalCollection(9);
        }, 5500);
        setTimeout(function () {
            getAllTotalCollection(10);
        }, 6000);
        setTimeout(function () {
            getAllTotalCollection(11);
        }, 6500);
        setTimeout(function () {
            getAllTotalCollection(12);
        }, 7000);
        setTimeout(function () {
            getAllTotalCollection(13);
        }, 7500);
        setTimeout(function () {
            getAllTotalCollection(23);
        }, 8000);
        setTimeout(function () {
            getAllTotalCollection(24);
        }, 8500);
        setTimeout(function () {
            getAllTotalCollection(22);
        }, 9000);
        setTimeout(function () {
            getAllTotalCollection(14);
        }, 9500);
        setTimeout(function () {
            getAllTotalCollection(25);
        }, 10000);
        setTimeout(function () {
            getOPIPcount('opdata');
        }, 10500);
        setTimeout(function () {
            getOPIPcount('ipdata');
        }, 11000);
        setTimeout(function () {
            getAllTotalCollection(50);
        }, 11500);
        setTimeout(function () {
            getPharmacyMargin(51);
        }, 12000);
        setTimeout(function () {
            getAllTotalCollection(52);
        }, 2000);
    } else {
        setTimeout(function () {
            getAllTotalCollection(1);
        }, 200);
        setTimeout(function () {
            getAllTotalCollection(2);
        }, 400);
        setTimeout(function () {
            getAllTotalCollection(3);
        }, 600);
        setTimeout(function () {
            getAllTotalCollection(4);
        }, 800);
        setTimeout(function () {
            getAllTotalCollection(15);
        }, 1000);
        setTimeout(function () {
            getAllTotalCollection(5);
        }, 1200);
        setTimeout(function () {
            getAllTotalCollection(6);
        }, 1400);
        setTimeout(function () {
            getAllTotalCollection(7);
        }, 1600);
        setTimeout(function () {
            getAllTotalCollection(8);
        }, 1800);
        setTimeout(function () {
            getAllTotalCollection(9);
        }, 2000);        
        setTimeout(function () {
            getAllTotalCollection(10);
        }, 2200);
        setTimeout(function () {
            getAllTotalCollection(11);
        }, 2400);
        setTimeout(function () {
            getAllTotalCollection(12);
        }, 2600);
        setTimeout(function () {
            getAllTotalCollection(13);
        }, 2800);
        setTimeout(function () {
            getAllTotalCollection(23);
        }, 3000);
        setTimeout(function () {
            getAllTotalCollection(24);
        }, 3200);
        setTimeout(function () {
            getAllTotalCollection(22);
        }, 3400);
        setTimeout(function () {
            getAllTotalCollection(14);
        }, 3600);
        setTimeout(function () {
            getAllTotalCollection(25);
        }, 3800);
        setTimeout(function () {
            getOPIPcount('opdata');
        }, 4000);
        setTimeout(function () {
            getOPIPcount('ipdata');
        }, 4200);
        setTimeout(function () {
            getAllTotalCollection(50);
        }, 4400);
        setTimeout(function () {
            getPharmacyMargin(51);
        }, 4600);
        setTimeout(function () {
            getAllTotalCollection(52);
        }, 2000);
    }

}

function getCustomDates(from_type) {
    var date_data = $('#date_datahidden').val();
    var obj = JSON.parse(date_data);
    $('#daterange_hidden').val(from_type);
    if (from_type == '1') {
        $('#from_date').val(obj.current_date);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.current_date);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Today');
        getAllGraphs();
    } else if (from_type == '2') {
        $('#from_date').val(obj.yeasterday);
        $('#to_date').val(obj.yeasterday);
        $('#from_datadis').html(obj.yeasterday);
        $('#to_datadis').html(obj.yeasterday);
        $('#range_typedata').html('Yesterday');
        getAllGraphs();
    } else if (from_type == '3') {
        $('#from_date').val(obj.week_last_sunday);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.week_last_sunday);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Week Till Date');
        getAllGraphs();
    } else if (from_type == '4') {
        $('#from_date').val(obj.lastweek_from);
        $('#to_date').val(obj.lastweek_to);
        $('#from_datadis').html(obj.lastweek_from);
        $('#to_datadis').html(obj.lastweek_to);
        $('#range_typedata').html('Last Week');
        getAllGraphs();
    } else if (from_type == '5') {
        $('#from_date').val(obj.firstday_thismonth);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.firstday_thismonth);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Month Till Date');
        getAllGraphs();
    } else if (from_type == '6') {
        $('#from_date').val(obj.firstday_lastmonth);
        $('#to_date').val(obj.lastday_lastmonth);
        $('#from_datadis').html(obj.firstday_lastmonth);
        $('#to_datadis').html(obj.lastday_lastmonth);
        $('#range_typedata').html('Last Month');
        getAllGraphs();
    } else if (from_type == '7') {
        $("#customdatapopmodel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

}

function getPharmacyMargin(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getPurchaseMargins";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#list_data' + from_type).removeClass('fa fa-list');
            $('#list_data' + from_type).addClass('fa fa-spinner fa-spin');
            $('#total_collection' + from_type).html("Please Wait!!!");
        },
        success: function (data) {
            $('#total_collection' + from_type).html(data);

        },
        complete: function () {
            $('#list_data' + from_type).removeClass('fa fa-spinner fa-spin');
            $('#list_data' + from_type).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getCustomDateRange() {
    var from_date = $('#from_date').val();
    var todate = $('#to_date').val();
    $('#range_typedata').html('Custom Date Range Selection');
    $('#from_datadis').html(from_date);
    $('#to_datadis').html(todate);
    getAllGraphs();
    $("#customdatapopmodel").modal('toggle');
}

function getTotalCollection(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getTotalCollection";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getTotalcollectionbtn').attr('disabled', true);
            $('#getTotalcollectionspin').removeClass('fa fa-bar-chart');
            $('#getTotalcollectionspin').addClass('fa fa-spinner fa-spin');
            $('#getmodelgraphsheader').html('Total Collections');
        },
        success: function (data) {
            if (parseInt(data) != 0) {
                var obj = JSON.parse(data);
                var cash = parseInt(obj.cash);
                var card = parseInt(obj.card);
                var other = parseInt(obj.other);
                var toal_collection = parseInt(obj.toal_collection);
                Highcharts.chart('getmodelgraphsdiv', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        type: 'pie',
                        plotShadow: false,
                        height: 500
                    },
                    title: {
                        text: '<strong>Total Collection ' + toal_collection + '</strong>'
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },

                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                        },
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            formatter: function () {
                                return this.y;
                            }
                        },
                        center: ['50%', '50%'],
                        startAngle: -90,
                        endAngle: 360,
                        size: '100%'
                    },

                    series: [{
                        type: 'pie',
                        data: [
                            {
                                name: 'Cash',
                                y: cash,
                                send_data: 'cash',
                                color: '#48754c',

                            },
                            {
                                name: 'Card',
                                y: card,
                                send_data: 'card',
                                color: '#f59042',

                            }, {
                                name: 'Other',
                                y: other,
                                send_data: 'other',
                                color: '#FFFF55',

                            },

                        ],
                        showInLegend: false
                    }]
                });
                $("#getmodelgraphs").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                toastr.warning("No Result Found");
            }
        },
        complete: function () {
            $('#getTotalcollectionbtn').attr('disabled', false);
            $('#getTotalcollectionspin').removeClass('fa fa-spinner fa-spin');
            $('#getTotalcollectionspin').addClass('fa fa-bar-chart');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getAllTotalCollection(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var date_range = $('#daterange_hidden').val();
    var url = base_url + "/master/getAllTotalCollection";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type, date_range: date_range };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#list_data' + from_type).removeClass('fa fa-list');
            $('#list_data' + from_type).addClass('fa fa-spinner fa-spin');
            $('#total_collection' + from_type).html("Please Wait!!!");
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#total_collection' + from_type).html(obj.total);
            if (obj.total2 == '0' || obj.diff == '0') {
                $('#count_bottom' + from_type).html("<i title='" + obj.total2 + "' class='blue'><i class='fa fa-sort-asc'></i> 0%</i>");
            } else if (obj.increase_value == 'neg') {
                $('#count_bottom' + from_type).html("<i title='" + obj.total2 + "' class='red'><i class='fa fa-sort-desc'></i>" + obj.diff + "% Decrease</i>");
            } else if (obj.increase_value == 'pos') {
                $('#count_bottom' + from_type).html("<i title='" + obj.total2 + "' class='green'><i class='fa fa-sort-asc'></i>" + obj.diff + "% Increase</i>");
            }

        },
        complete: function () {
            $('#list_data' + from_type).removeClass('fa fa-spinner fa-spin');
            $('#list_data' + from_type).addClass('fa fa-list');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getlabDetails() {
    getLabRadiologyCollection('lab');
    setTimeout(function () {
        getLabRadiologyCollection('labsub');
    }, 800);
}

function getIpOpTotalCollection() {
    getLabRadiologyCollection('total_op_ip_card');
    setTimeout(function () {
        getLabRadiologyCollection('total_op_ip_tags');
    }, 800);
}
function getPharmacyCollection() {
    getLabRadiologyCollection('pharmacy_collection');
    setTimeout(function () {
        getGenericWiseProductCount();
    }, 800);
}
function getLabRadiologyCollection(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getRadiologyCollection";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    var high_chartdiv = 'getmodelgraphsdiv';
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            if ($('#getlabbtn' + from_type)) {
                $('#getlabbtn' + from_type).attr('disabled', true);
                $('#getlabspin' + from_type).removeClass('fa fa-bar-chart');
                $('#getlabspin' + from_type).addClass('fa fa-spinner fa-spin');
            } else {
                $("#ip_op_statusdataipdata").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            }
            var header_string = '';
            if (from_type == 'radio') {
                header_string = 'Radiology Collections';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'lab') {
                header_string = 'Lab Collections';
                $('#dualgraph_modelheader').html(header_string);
                high_chartdiv = 'dualgraph_modeldiv1';
            } else if (from_type == 'labsub') {
                header_string = 'Lab Collections';
                $('#dualgraph_modelheader').html(header_string);
                high_chartdiv = 'dualgraph_modeldiv2';
            } else if (from_type == 'total_op_ip_card') {
                header_string = 'Cardwise Data';
                $('#dualgraph_modelheader').html(header_string);
                high_chartdiv = 'dualgraph_modeldiv1';
            } else if (from_type == 'total_op_ip_tags') {
                header_string = 'Total Collection';
                $('#dualgraph_modelheader').html(header_string);
                high_chartdiv = 'dualgraph_modeldiv2';
            } else if (from_type == 'ip_admission') {
                header_string = 'IP Admission';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'other_admission') {
                header_string = 'Other Admission';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'op_collection') {
                header_string = 'OP Collection';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'ip_collection') {
                header_string = 'IP Collection';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'casualty_collection') {
                header_string = 'Casualty Collection';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'cosmetic') {
                header_string = 'Cosmetic';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'pharmacy_purchase') {
                header_string = 'Pharmacy Purchase';
                $('#getmodelgraphsheader').html(header_string);
            } else if (from_type == 'pharmacy_collection') {
                header_string = 'Pharmacy Collection';
                $('#dualgraph_modelheader').html(header_string);
                high_chartdiv = 'dualgraph_modeldiv2';

            } else if (from_type == 'total_bil_collection') {
                header_string = 'Total Bill Amount';
                $('#dualgraph_modelheader').html(header_string);
            } else if (from_type == 'discharge_collection') {
                header_string = 'Discharge Collection';
                $('#getmodelgraphsheader').html(header_string);
            }
        },       
        success: function (data) {
            if (parseInt(data) != 0) {
                var lable_string = 'Total Collection ';
                if (from_type == 'ip_admission' || from_type == 'other_admission') {
                    lable_string = 'Total Admission ';
                }
                var obj = JSON.parse(data);
                var total_collection = obj.net_total;
                Highcharts.chart(high_chartdiv, {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        type: 'pie',
                        plotShadow: false,
                        height: 500
                    },
                    title: {
                        text: '<strong>' + lable_string + total_collection + '</strong>'
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },

                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                        },
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            formatter: function () {
                                return this.y;
                            }
                        },
                        center: ['50%', '50%'],
                        startAngle: -90,
                        endAngle: 360,
                        size: '100%'
                    },

                    series: [{
                        type: 'pie',
                        allowPointSelect: false,
                        pointPadding: 0,
                        groupPadding: 0.03,
                        borderWidth: 0,
                        showInLegend: false,
                        shadow: false,
                        name: 'Count in Amount',
                        data: obj.data_send,
                    }]
                });
                if (from_type == 'lab' || from_type == 'labsub' || from_type == 'total_op_ip_card' || from_type == 'total_op_ip_tags' || from_type == 'pharmacy_collection_genericwise' || from_type == 'pharmacy_collection') {
                    $("#dualgraph_model").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                } else {
                    $("#getmodelgraphs").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            } else {
                toastr.warning("No Result Found");
            }

        },
        complete: function () {
            if ($('#getlabbtn' + from_type)) {
                $('#getlabbtn' + from_type).attr('disabled', false);
                $('#getlabspin' + from_type).removeClass('fa fa-spinner fa-spin');
                $('#getlabspin' + from_type).addClass('fa fa-bar-chart');
            } else {
                $("#ip_op_statusdataipdata").LoadingOverlay("hide");
            }
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getCompanyCreditSplitup(from_type, list_type, from_text) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getCompanyCreditSplitup";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getlabbtn' + from_type).attr('disabled', true);
            $('#getlabspin' + from_type).removeClass('fa fa-bar-chart');
            $('#getlabspin' + from_type).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (list_type == '1') {
                $('#otbill_groupsheader').html(from_text);
                if (data) {
                    $('#otbill_groupsdiv').html(data);
                    $("#otbill_groupsmodel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            } else if (list_type == '2') {
                $('#getmodelgraphsheader').html(from_text);
                if (parseInt(data) != 0) {
                    var obj = JSON.parse(data);
                    var total_collection = obj.net_total;
                    Highcharts.chart('getmodelgraphsdiv', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            type: 'pie',
                            plotShadow: false,
                            height: 500
                        },
                        title: {
                            text: '<strong>Total Collection ' + total_collection + '</strong>'
                        },
                        exporting: {
                            enabled: false
                        },
                        credits: {
                            enabled: false
                        },

                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                            },
                            dataLabels: {
                                enabled: true,
                                distance: -20,
                                formatter: function () {
                                    return this.y;
                                }
                            },
                            center: ['50%', '50%'],
                            startAngle: -90,
                            endAngle: 360,
                            size: '100%'
                        },

                        series: [{
                            type: 'pie',
                            allowPointSelect: false,
                            pointPadding: 0,
                            groupPadding: 0.03,
                            borderWidth: 0,
                            showInLegend: false,
                            shadow: false,
                            name: 'Count in Amount',
                            data: obj.data_send,
                        }]
                    });
                    $("#getmodelgraphs").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                } else {
                    toastr.warning("No Result Found");
                }
            }

        },
        complete: function () {
            $('#getlabbtn' + from_type).attr('disabled', false);
            $('#getlabspin' + from_type).removeClass('fa fa-spinner fa-spin');
            $('#getlabspin' + from_type).addClass('fa fa-bar-chart');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getOTCollection() {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getOTCollection";
    var param = { _token: token, from_date: from_date, to_date: to_date };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getFullOtCollectionbtn').attr('disabled', true);
            $('#getFullOtCollectionspin').removeClass('fa fa-bar-chart');
            $('#getFullOtCollectionspin').addClass('fa fa-spinner fa-spin');
            $('#getmodelgraphsheader').html('OT Collections');
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var ot_instrument_bill = parseInt(obj.ot_instrument_bill);
            var sterrad = parseInt(obj.sterrad);
            var dr_roy_package = parseInt(obj.dr_roy_package);
            var surgery = parseInt(obj.surgery);
            var other_poc = parseInt(obj.other_poc);
            var total = parseInt(obj.total);
            if (parseInt(total) != 0) {
                Highcharts.chart('getmodelgraphsdiv', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        type: 'pie',
                        plotShadow: false,
                        height: 500
                    },
                    title: {
                        text: '<strong>Total Collection ' + total + '</strong>'
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },

                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                        },
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            formatter: function () {
                                return this.y;
                            }
                        },
                        center: ['50%', '50%'],
                        startAngle: -90,
                        endAngle: 360,
                        size: '100%'
                    },

                    series: [{
                        type: 'pie',
                        allowPointSelect: false,
                        showInLegend: false,
                        pointPadding: 0,
                        groupPadding: 0.03,
                        borderWidth: 0,
                        shadow: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    getOtDetalis(this.send_data);
                                }
                            }
                        },
                        type: 'pie',
                        name: 'Count in Amount',
                        data: [
                            {
                                name: 'OT Instrument Bill (' + ot_instrument_bill + ')',
                                y: ot_instrument_bill,
                                send_data: 'ot_instrument_bill',
                                color: '#48754c',
                            },
                            {
                                name: 'Sterrad (' + sterrad + ')',
                                y: sterrad,
                                send_data: 'sterrad',
                                color: '#f59042',
                            },
                            {
                                name: 'Dr Roy Package (' + dr_roy_package + ')',
                                y: dr_roy_package,
                                send_data: 'dr_roy_package',
                                color: '#ffff00',
                            },
                            {
                                name: 'Surgery (' + surgery + ')',
                                y: surgery,
                                send_data: 'surgery',
                                color: '#008080',
                            },
                            {
                                name: 'Other Procedure (' + other_poc + ')',
                                y: other_poc,
                                send_data: 'other_poc',
                                color: '#CBC3E3',
                            }

                        ]
                    }]
                });
                $("#getmodelgraphs").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                toastr.warning("No Result Found");
            }

        },
        complete: function () {
            $('#getFullOtCollectionbtn').attr('disabled', false);
            $('#getFullOtCollectionspin').removeClass('fa fa-spinner fa-spin');
            $('#getFullOtCollectionspin').addClass('fa fa-bar-chart');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}


function getOtDetalis(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getOtDetalis";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getmodelgraphsdiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            if (from_type == 'ot_instrument_bill') {
                $('#otbill_groupsheader').html('OT Instrument Bill');
            } else if (from_type == 'sterrad') {
                $('#otbill_groupsheader').html('Sterrad');
            } else if (from_type == 'dr_roy_package') {
                $('#otbill_groupsheader').html('Dr Roy Package');
            } else if (from_type == 'surgery') {
                $('#otbill_groupsheader').html('Surgery');
            } else if (from_type == 'other_poc') {
                $('#otbill_groupsheader').html('Other Procedure');
            }
        },
        success: function (data) {
            $('#otbill_groupsdiv').html(data);
            $("#otbill_groupsmodel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $("#getmodelgraphsdiv").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getOPIPcount(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getIPOPGraphs";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#ip_op_statusdata" + from_type).LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#ip_op_statusdata' + from_type).html("Please Wait!!!");
        },
        success: function (data) {
            if (from_type == 'opdata') {
                var obj = JSON.parse(data);
                var new_count = parseInt(obj.new_count);
                var renew_count = parseInt(obj.renew_count);
                var followup_count = parseInt(obj.followup_count);
                var tot_count = parseInt(obj.tot_count);

                Highcharts.chart('ip_op_statusdata' + from_type, {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        height: 300
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '<strong>' + tot_count + '</strong>',
                        align: 'center',
                        verticalAlign: 'middle',
                        y: -18,
                        x: -0
                    },
                    legend: {
                        x: 0,
                        y: -15
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -20,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                },
                                formatter: function () {
                                    return this.y;
                                }
                            },

                            showInLegend: true,

                            center: ['50%', '50%'],
                            startAngle: -90,
                            endAngle: 360,
                            size: '100%'
                        },

                        series: {
                            pointPadding: 0,
                            groupPadding: 0.03,
                            borderWidth: 0,
                            showInLegend: true,

                            shadow: false,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        getOTPatientDetalis(this.send_data);
                                    }
                                }
                            }
                        }
                    },

                    series: [{
                        type: 'pie',
                        name: 'Count in Percentage',
                        innerSize: '50%',

                        data: [
                            {
                                name: 'Registration',
                                y: new_count,
                                send_data: 'reg',
                                color: '#48754c',
                                dataLabels: {
                                    distance: -15,
                                    enabled: true
                                }
                            },
                            {
                                name: 'Follow up',
                                y: followup_count,
                                send_data: 'follow_up',
                                color: '#f59042',
                                dataLabels: {
                                    distance: -15,
                                    enabled: true
                                }
                            }, {
                                name: 'Renewal',
                                y: renew_count,
                                send_data: 'renewal',
                                color: '#FFFF55',
                                dataLabels: {
                                    distance: -15,
                                    enabled: true
                                }
                            },

                        ]
                    }]
                });
            } else if (from_type == 'ipdata') {

                var obj = JSON.parse(data);
                var icu_count = parseInt(obj.icd);
                var other_count = parseInt(obj.other);
                var tot_count = parseInt(obj.tot_count);

                Highcharts.chart('ip_op_statusdata' + from_type, {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        height: 285
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '<strong>' + tot_count + '</strong>',
                        align: 'center',
                        verticalAlign: 'middle',
                        y: -18,
                        x: -0
                    },
                    legend: {
                        x: 0,
                        y: -15
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -20,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                },
                                formatter: function () {
                                    return this.y;
                                }
                            },

                            showInLegend: true,

                            center: ['50%', '50%'],
                            startAngle: -90,
                            endAngle: 360,
                            size: '100%'
                        },

                        series: {
                            pointPadding: 0,
                            groupPadding: 0.03,
                            borderWidth: 0,
                            showInLegend: true,

                            shadow: false,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        getLabRadiologyCollection(this.send_data);
                                    }
                                }
                            }
                        }
                    },

                    series: [{
                        type: 'pie',
                        name: 'Count in Percentage',
                        innerSize: '50%',

                        data: [
                            {
                                name: 'Others',
                                y: other_count,
                                send_data: 'other_admission',
                                color: '#48754c',
                                dataLabels: {
                                    distance: -15,
                                    enabled: true
                                }
                            },
                            {
                                name: 'ICU',
                                y: icu_count,
                                send_data: 'ip_admission',
                                color: '#f59042',
                                dataLabels: {
                                    distance: -15,
                                    enabled: true
                                }
                            }

                        ]
                    }]
                });
            }
        },
        complete: function () {
            $("#ip_op_statusdata" + from_type).LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}


function getOTPatientDetalis(from_type) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getOTPatientDetalis";
    var param = { _token: token, from_date: from_date, to_date: to_date, from_type: from_type };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#ip_op_statusdataopdata").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            if (from_type == 'follow_up') {
                $('#otbill_groupsheader').html('Follow Up');
            } else if (from_type == 'renewal') {
                $('#otbill_groupsheader').html('Renewal');
            } else if (from_type == 'reg') {
                $('#otbill_groupsheader').html('Registration');
            }
        },
        success: function (data) {
            $('#otbill_groupsdiv').html(data);
            $("#otbill_groupsmodel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $("#ip_op_statusdataopdata").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getGenericWiseProductCount() {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/getGenericWiseProductCount";
    var param = { _token: token, from_date: from_date, to_date: to_date };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#dualgraph_modeldiv1").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });

        },
        success: function (data) {
            $('#dualgraph_modeldiv1').html(data);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 1000);



        },
        complete: function () {
            $("#dualgraph_modeldiv1").LoadingOverlay("hide");

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}
