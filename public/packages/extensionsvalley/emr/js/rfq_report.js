$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    //-----------Bill No search------------
    $('#rfq_number').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var base_url = $('#base_url').val();
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#rfq_id_hidden').val() != "") {
                $('#rfq_id_hidden').val('');
                $("#rfq_idAjaxDiv").html("").hide();
            }
            var rfq_no = $(this).val();
            if (rfq_no == "") {
                $("#rfq_idAjaxDiv").html("").hide();
            } else {
                var url = base_url + "/reports/searchRFQNumber";
                var param = { rfq_no: rfq_no };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#rfq_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        // alert(html); return;
                        $("#rfq_idAjaxDiv").html(html).show();
                        $("#rfq_idAjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

function fillrfq_no(id, rfq_number) {
    $('#rfq_number').val(rfq_number);
    $('#rfq_id_hidden').val(id);
    $('#rfq_idAjaxDiv').hide();
}


function getReportData() {
    var rfq_number = $('#rfq_number').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/reports/rfq_report_get";
    var from_date = $("#fromdate").val();
    var to_date = $("#todate").val();
    var rfq_id = $('#rfq_id_hidden').val();
    var location = $('#location').val();
    var param = { from_date: from_date, rfq_number: rfq_number, to_date: to_date, rfq_id: rfq_id, location: location };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#search_results').attr('disabled', true);
            $('#serachResultSpin').removeClass('fa fa-search');
            $('#serachResultSpin').addClass('fa fa-spinner');
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#search_results').attr('disabled', false);
            $('#serachResultSpin').removeClass('fa fa-spinner');
            $('#serachResultSpin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");

        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }
    });
}

function listQuotationItems(head_id) {
    var base_url = $('#base_url').val();
    var quotation_no = $('#quotation_no_data' + head_id).html();
    var token = $('#token_hiddendata').val();
    var url = base_url + "/purchase/listQuotationItems";
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, head_id: head_id },
        beforeSend: function () {
            $('#quotationListHeader').html(quotation_no);
            $('#listQuotationItemsBtn' + head_id).attr('disabled', true);
            $('#listQuotationItemsSpin' + head_id).removeClass('fa fa-list');
            $('#listQuotationItemsSpin' + head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#quotationListDiv").html(data);
            $("#quotationListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $('#listQuotationItemsBtn' + head_id).attr('disabled', false);
            $('#listQuotationItemsSpin' + head_id).removeClass('fa fa-spinner fa-spin');
            $('#listQuotationItemsSpin' + head_id).addClass('fa fa-list');
        }
    });
}


function search_clear() {
    var current_date = $('#current_date').val();
    $('#fromdate').val(current_date);
    $('#todate').val(current_date);
    $('#rfq_number').val('');
    $('#rfq_idAjaxDiv').hide();
    $('#location').val('');
    $('#location').select2();
    getReportData();
}
