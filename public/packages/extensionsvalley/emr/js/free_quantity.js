$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
    });

    $(".select2").select2();
    document.onkeyup = KeyCheck;
    $('.expiry_date').datetimepicker({
        format: 'DD-MM-YYYY',
        minDate: new Date()
    });
    checkFreeStockEditStatus();
});

$(function () {
    var focusedElement;
    $(document).on('focus', 'input', function () {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function () {
            focusedElement.select();
        }, 100);
    });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        window.location.reload();
    }
});


var base_url = $('#base_url').val();
var freestockid = $('#freestockid').val();
var token = $('#hidden_filetoken').val();
var decimalConfiguration = $('#decimal_configuration').val();
var item_array = {};
var delete_item = [];


function checkFreeStockEditStatus() {
    if (parseInt(freestockid) != 0) {
        var url = base_url + '/free_stock/getFreeStockDetalis';
        var param = {
            freestockid: freestockid
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#row_body_data").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                var obj = JSON.parse(data);
                $("#row_body_data").html(obj.html);
                editUpdateItemArray(obj.po_result);
                updateFreeStockHead(obj.po_head);
            },
            complete: function () {
                $("#row_body_data").LoadingOverlay("hide");
                row_ct = 1;
                $(".row_class").each(function (i) {
                    $(this).find('.row_count_class').text(row_ct); // row Count Re-arenge
                    row_ct++;
                });
                getAllItemsTotals();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        getNewRowInserted();
    }
}


function updateFreeStockHead(head_data) {
    $.each(head_data, function (index, value) {
        $('#round_off').val(value.round_off);
        $('#adjustment_id').val(value.adjustment_head_id);
        $('#free_stock_date').val(value.sales_date);
        $('#location').val(value.location_code);
        $('#item_type').val(value.item_type);
        $('#freestock_reason').val(value.reason);
        $('#freestock_remarks').val(value.remarks);
    });
}


function editUpdateItemArray(item_list) {
    row_id = 1;
    $.each(item_list, function (index, value) {
        var item_code = value.item_code ? value.item_code : '';
        if (item_code && item_code != 'undefined') {
            var detail_id = value.adj_detail_id ? value.adj_detail_id : 0;
            var item_id = value.item_id ? value.item_id : 0;
            var item_desc = value.item_desc ? value.item_desc : 0;
            var batch_no = value.batch ? value.batch : 0;
            var expiry_date = value.expiry ? value.expiry : 0;
            var grn_qty = value.stock_qty ? value.stock_qty : 0;
            var uom_value = value.uom_val ? value.uom_val : 0;
            var item_unit = value.uom ? value.uom : 0;
            var selling_price = value.sales_price ? value.sales_price : 0;
            var mrp = value.unit_mrp ? value.unit_mrp : 0;
            var item_rate = value.item_rate ? value.item_rate : 0;
            var tax_per = value.unit_tax ? value.unit_tax : 0;
            var tot_tax_amt = value.tot_tax_amt ? value.tot_tax_amt : 0;
            var net_cost = value.net_cost ? value.net_cost : 0;

            var tot_rate = grn_qty * item_rate;
            var price_details = getItemPrice(item_rate, grn_qty, 0, uom_value, net_cost, tot_tax_amt, selling_price, tax_per);
            var unit_cost = price_details['unit_cost'] ? price_details['unit_cost'] : 0;
            var unit_tax_amount = price_details['unit_tax_amount'] ? price_details['unit_tax_amount'] : 0;
            var all_total_qty = price_details['all_total_qty'] ? price_details['all_total_qty'] : 0;
            var unit_cost_with_out_tax = price_details['unit_cost_with_out_tax'] ? price_details['unit_cost_with_out_tax'] : 0;
            var unit_cost_with_out_tax = price_details['unit_cost_with_out_tax'] ? price_details['unit_cost_with_out_tax'] : 0;
            var unit_rate = price_details['unit_rate'] ? price_details['unit_rate'] : 0;
            var sellingprice_without_tax = parseFloat(selling_price) - parseFloat(unit_tax_amount);

            item_array[row_id] = {
                detail_id: detail_id,
                item_id: item_id,
                batch_no: batch_no,
                expiry_date: expiry_date,
                item_code: item_code,
                item_desc: item_desc,
                grn_qty: checkIsNaN(grn_qty),
                item_unit: item_unit,
                unit_cost: checkIsNaN(unit_cost),
                unit_tax_amount: checkIsNaN(unit_tax_amount),
                unit_cost_with_out_tax: checkIsNaN(unit_cost_with_out_tax),
                uom_val: checkIsNaN(uom_value),
                all_total_qty: checkIsNaN(all_total_qty),
                mrp: checkIsNaN(mrp),
                item_rate: checkIsNaN(item_rate),
                unit_rate: checkIsNaN(unit_rate),
                selling_price: checkIsNaN(selling_price),
                sellingprice_without_tax: sellingprice_without_tax,
                total_tax_perc: checkIsNaN(tax_per),
                total_tax_amt: checkIsNaN(tot_tax_amt),
                totalRate: checkIsNaN(tot_rate),
                netRate: checkIsNaN(net_cost)
            }
        }
        row_id++;
    });
    $('#row_count_id').val(row_id);
    console.log(item_array);
    updateFreeStockList();
}


function updateFreeStockList() {
    $.each(item_array, function (index, value) {
        $('#unit_rate' + index).val(value.item_rate);
        $('#unit_cost' + index).val(value.unit_cost);
        $('#unit-mrp_' + index).val(value.mrp);
        $('#sales_price_' + index).val(value.selling_price);
        $('#tax_type_' + index).val(parseInt(value.total_tax_perc));
        $('#tot_tax_amt_' + index).val(value.unit_tax_amount);
        $('#tot_tax_amt_new_' + index).val(value.total_tax_amt);
        $('#net_cost_' + index).val(value.netRate);
    });
}



function getSelectedRowFocus() {
    if ($(".row_class:last").find("input[name='item_desc[]']").val() == '') {
        setTimeout(function () {
            $(".row_class:last").find("input[name='item_desc[]']").focus();
        }, 1000);
    }
}

function KeyCheck(e) {
    var KeyID = (window.event) ? event.keyCode : e.keyCode;
    if (KeyID == 113) {
        purchase_history();
    }
    if (KeyID == 27) {
        $('#item_chargesmodel_div').html('');
        $('#item_charges_model').modal('hide');
    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function closePoPopup() {
    $('#poCheckBox').prop('checked', false); // Unchecks it
    $('#po_list_model').modal('toggle');
}


function getNewRowInserted() {
    if ($(".row_class:last").find("input[name='item_desc[]']").val() != '') {
        var url = base_url + '/free_stock/AddNewRow';
        var row_count = $('#row_count_id').val();
        $('#row_count_id').val(parseInt(row_count) + 1);
        $.ajax({
            type: "POST",
            url: url,
            data: {
                row_count: row_count,
                add_row: 1,
                _token: token
            },
            beforeSend: function () {
                $('#addNewGrnRow').attr('disabled', true);
                $('#addNewGrnRowSpin').removeClass('fa fa-plus');
                $('#addNewGrnRowSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {
                $('#row_body_data').append(html);
                row_ct = 1;
                $(".row_class").each(function (i) {
                    $(this).find('.row_count_class').text(row_ct); // row Count Re-arenge
                    row_ct++;
                });
            },
            complete: function () {
                $('#addNewGrnRow').attr('disabled', false);
                $('#addNewGrnRowSpin').removeClass('fa fa-spinner fa-spin');
                $('#addNewGrnRowSpin').addClass('fa fa-plus');
                serialNoCalculation('row_class', 'row_count_class');
                $('.expiry_date').datetimepicker({
                    format: 'DD-MM-YYYY',
                });
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }
}

function validateText(row_id) {
    $(".active_class" + row_id).attr('disabled', false);
    $(".active_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        } else if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        } else if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        } else if (!val) {
            $(this).val(0);
            flag = 1;
        }
        if ($(this).hasClass("percentage_class" + row_id)) {
            if (parseFloat(val) > 100) {
                $(this).val(0);
                $(this).blur();
                flag = 1;
            }
        }
        if (flag == 0) {
            $(this).val(checkIsNaN(val));
        }
    });

    $(".number_class" + row_id).each(function (index) {
        var flag = 0;
        var val = $(this).val();
        if (isNaN(val)) {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'NaN') {
            $(this).val(0);
            flag = 1;
        }
        if (val == 'Infinity') {
            $(this).val(0);
            flag = 1;
        }
        if (!val) {
            $(this).val(0);
            flag = 1;
        }
    });
}


function calculateMrpAmount(row_id) {
    var mrp = $('#unit-mrp_' + row_id).val();
    $('#sales_price_' + row_id).val(mrp);
    calculateListAmount(row_id);
}



function listItemArray(row_id) {
    var item_code = $('#item_desc_' + row_id).attr('attr-code');
    if (item_code && item_code != 'undefined') {
        var detail_id = $('#item_desc_' + row_id).attr('attr-detailid');
        var item_id = $('#item_desc_' + row_id).attr('attr-id');
        var item_desc = $('#item_desc_' + row_id).val();
        var batch_no = $('#batch_row' + row_id).val();
        var expiry_date = $('#expiry_date' + row_id).val();
        var grn_qty = $('#tot_qty_' + row_id).val();
        var uom_value = $('#uom_select_id_' + row_id + ' :selected').attr('data-uom_value');
        var item_unit = $('#uom_select_id_' + row_id).val();
        var selling_price = $('#sales_price_' + row_id).val();
        var mrp = $('#unit-mrp_' + row_id).val();
        var item_rate = $('#unit_rate' + row_id).val();
        var tax_per = $('#tax_type_' + row_id).val();
        var tot_tax_amt = $('#tot_tax_amt_new_' + row_id).val();
        var net_cost = $('#net_cost_' + row_id).val();
        var tot_rate = grn_qty * item_rate;

        var price_details = getItemPrice(item_rate, grn_qty, 0, uom_value, net_cost, tot_tax_amt, selling_price, tax_per);
        var unit_cost = price_details['unit_cost'] ? price_details['unit_cost'] : 0;
        var unit_tax_amount = price_details['unit_tax_amount'] ? price_details['unit_tax_amount'] : 0;
        var all_total_qty = price_details['all_total_qty'] ? price_details['all_total_qty'] : 0;
        var unit_cost_with_out_tax = price_details['unit_cost_with_out_tax'] ? price_details['unit_cost_with_out_tax'] : 0;
        var unit_rate = price_details['unit_rate'] ? price_details['unit_rate'] : 0;
        var sellingprice_without_tax = parseFloat(selling_price) - parseFloat(unit_tax_amount);

        item_array[row_id] = {
            detail_id: detail_id,
            item_id: item_id,
            batch_no: batch_no,
            expiry_date: expiry_date,
            item_code: item_code,
            item_desc: item_desc,
            grn_qty: checkIsNaN(grn_qty),
            item_unit: item_unit,
            unit_cost: checkIsNaN(unit_cost),
            unit_tax_amount: checkIsNaN(unit_tax_amount),
            unit_cost_with_out_tax: checkIsNaN(unit_cost_with_out_tax),
            uom_val: checkIsNaN(uom_value),
            all_total_qty: checkIsNaN(all_total_qty),
            mrp: checkIsNaN(mrp),
            item_rate: checkIsNaN(item_rate),
            unit_rate: checkIsNaN(unit_rate),
            selling_price: checkIsNaN(selling_price),
            sellingprice_without_tax: sellingprice_without_tax,
            total_tax_perc: checkIsNaN(tax_per),
            total_tax_amt: checkIsNaN(tot_tax_amt),
            totalRate: checkIsNaN(tot_rate),
            netRate: checkIsNaN(net_cost)
        }
    }
    getAllItemsTotals();
}

function calculateListAmount(row_id) {
    validateText(row_id);
    var qty = $('#tot_qty_' + row_id).val();
    var uom_value = $('#uom_select_id_' + row_id + ' :selected').attr('data-uom_value');
    $('#convention_factor' + row_id).val(checkIsNaN(uom_value));
    var free_qty = 0;
    var item_price = $('#unit_rate' + row_id).val();
    var sales_price = $('#sales_price_' + row_id).val();

    if (!item_price) {
        item_price = 0;
        $('#unit_rate' + row_id).val(0);
    }
    var tot_rate = qty * item_price;
    var tax_per = $('#tax_type_' + row_id).val();
    if (parseFloat(tax_per) > 100) {
        tax_per = 0;
        $('#tax_type_' + row_id).val('');
    }
    if (!tax_per) {
        tax_per = 0;
    }
    var tax_amount = (tax_per * tot_rate) / 100;
    if (tax_amount === Infinity) {
        tax_amount = 0;
    }
    $('#tot_tax_amt_new_' + row_id).val(checkIsNaN(tax_amount));
    var net_rate = parseFloat(tot_rate) + parseFloat(tax_amount);
    $('#net_cost_' + row_id).val(checkIsNaN(net_rate));
    var price_details = getItemPrice(item_price, qty, free_qty, uom_value, net_rate, tax_amount, sales_price, tax_per);
    var unit_cost = price_details['unit_cost'] ? price_details['unit_cost'] : 0;
    var unit_tax_amount = price_details['unit_tax_amount'] ? price_details['unit_tax_amount'] : 0;
    var all_total_qty = price_details['all_total_qty'] ? price_details['all_total_qty'] : 0;
    $('#all_total_qty' + row_id).val(checkIsNaN(all_total_qty));
    $('#unit_cost' + row_id).val(checkIsNaN(unit_cost));
    $('#tot_tax_amt_' + row_id).val(checkIsNaN(unit_tax_amount));
    listItemArray(row_id);
}


function getItemPrice(rate, grn_qty, free_qty, uom_unit, net_amt, total_tax_amt, sales_price, tax_per) {
    var unit_cost = 0;
    var unit_cost_with_out_tax = 0;
    var return_data = {}
    if (!uom_unit) {
        uom_unit = 0;
    }
    var tot_unit_qty = (parseFloat(grn_qty) + parseFloat(free_qty)) * uom_unit;
    var unit_rate = 0;
    if (parseInt(uom_unit) != 0) {
        unit_rate = (rate / uom_unit);
    }


    if (!tot_unit_qty || parseFloat(tot_unit_qty) == 0) {
        tot_unit_qty = 0;
    } else {
        unit_cost = (net_amt) / tot_unit_qty;
    }

    if (parseFloat((parseFloat(grn_qty) * parseFloat(uom_unit))) != 0) {
        unit_cost_with_out_tax = (parseFloat(unit_cost)) - ((parseFloat(total_tax_amt) / ((parseFloat(grn_qty)) * parseFloat(uom_unit))));
    }


    var total_qty = parseFloat(grn_qty) * parseFloat(uom_unit);
    var tot_free_qty = parseFloat(free_qty) * parseFloat(uom_unit);
    var all_total_qty = parseFloat(tot_unit_qty);

    var unit_sales_price = parseFloat(sales_price) / (1 + (tax_per / 100));
    unit_tax_amount = parseFloat(sales_price) - parseFloat(unit_sales_price);

    return_data = {
        unit_rate: unit_rate,
        unit_cost: unit_cost,
        total_qty: total_qty,
        unit_tax_amount: unit_tax_amount,
        tot_free_qty: tot_free_qty,
        all_total_qty: all_total_qty,
        unit_cost_with_out_tax: unit_cost_with_out_tax
    }
    return return_data;
}


function getAllItemsTotals() {
    var total_array = [];
    var total_tax_amt = 0.0;
    var total_gross = 0.0;

    $.each(item_array, function (index, value) {
        total_tax_amt = parseFloat(total_tax_amt) + parseFloat(value.total_tax_amt);
        total_gross = parseFloat(total_gross) + parseFloat(value.totalRate);
    });

    var round_off = $('#round_off').val();
    if (!round_off) {
        $('#round_off').val(0);
        round_off = 0.0;
    }
    var net_amount = (parseFloat(total_gross) + parseFloat(total_tax_amt) + parseFloat(round_off));

    var total_tax_amt_split = parseFloat(total_tax_amt) / 2;

    if ($('#SGST_amot')) {
        $('#SGST_amot').html(checkIsNaN(total_tax_amt_split));
    }
    if ($('#CGST_amot')) {
        $('#CGST_amot').html(checkIsNaN(total_tax_amt_split));
    }
    if ($('#gross_amount_hd').val()) {
        $('#gross_amount_hd').val(checkIsNaN(total_gross));
    }
    if ($('#tot_tax_amt_hd').val()) {
        $('#tot_tax_amt_hd').val(checkIsNaN(total_tax_amt));
    }

    if ($('#net_amount_hd').val()) {
        $('#net_amount_hd').val(checkIsNaN(net_amount));
    }

    return total_array;
}

function searchItemCode(id, event, row_id) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = $('#' + id).next().attr('id');
    var item_code = htmlDecode($('#' + id).val());
    var search_type = $('input[name="search_type_item"]:checked').val() ? $(
        'input[name="search_type_item"]:checked').val() : 'item_desc';
    if (event.keyCode == 13) {
        ajaxlistenter(ajax_div);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (item_code) {
            var list_status = true;
            var location = $('#location').val();
            $('#grn_listrow_id').val(row_id);
            var url = base_url + "/grn/productSearch";
            var param = {
                item_code: item_code,
                location: location,
                search_type: search_type,
                list_status: list_status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                    if (search_type && search_type == 'barcode') {
                        if ($("#" + ajax_div).find('li').length == 1) {
                            $("#" + ajax_div).find('li').first().click();
                        }
                    }
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            $("#" + ajax_div).html('').hide();
        }
    } else {
        ajax_list_key_down(ajax_div, event);
    }
}

function fillItemValues(list, item_code, item_desc, item_id, uom_name, uom_id, uom_value) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#' + itemCodeTextId).attr('attr-code', item_code);
    $('#' + itemCodeTextId).attr('attr-id', item_id);
    $('#' + itemCodeTextId).val(htmlDecode(item_desc));
    $('#' + itemCodeTextId).attr('disabled', true);
    $('#' + itemCodeListDivId).hide();
    var row_id = $('#grn_listrow_id').val();
    fetchUOMData(item_id, row_id, uom_id);
}


function fetchUOMData(item_id, row_id, uom_id) {
    var url = base_url + "/grn/searchItemUOM";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            item_id: item_id,
            uom_id: uom_id,
        },
        beforeSend: function () {
            $('#addNewGrnRow').attr('disabled', true);
            $('#addNewGrnRowSpin').removeClass('fa fa-plus');
            $('#addNewGrnRowSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (response) {
            var obj = JSON.parse(response);
            $('#uom_select_id_' + row_id).html(obj.option);
        },
        complete: function () {
            $('#addNewGrnRow').attr('disabled', false);
            $('#addNewGrnRowSpin').removeClass('fa fa-spinner fa-spin');
            $('#addNewGrnRowSpin').addClass('fa fa-plus');
            setTimeout(function () {
                $('#batch_row' + row_id).focus();
                var uom_value = $('#uom_select_id_' + row_id + ' :selected').attr('data-uom_value');
                $('#convention_factor' + row_id).val(checkIsNaN(uom_value));
            }, 500);
            getNewRowInserted();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function serialNoCalculation(tr_class, td_class) {
    row_ct = 1;
    $("." + tr_class).each(function (i) {
        $(this).find('.' + td_class).text(row_ct);
        row_ct++;
    });
}


function reloadFreeStock() {
    var freestockid = $('#freestockid').val();
    bootbox.confirm({
        message: "All changes would be lost, Are you sure you reload ?",
        buttons: {
            'confirm': {
                label: "Continue",
                className: 'btn-primary',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var url = base_url + "/free_stock/addEditFreeStock";
                if (parseInt(freestockid) != 0) {
                    url = base_url + "/free_stock/addEditFreeStock/" + freestockid;
                }
                window.location.href = url;
            }
        }
    });
}


function blockSpecialChar(e) {
    var k = e.keyCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));

}

function item_history(row_id) {
    $('#hdn_item_history').val(row_id);
}


$("#item_search_btn").click(function () {
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
    $('#issue_search_box').focus();
});

function searchProducts() {
    console.log("eeee");
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("main_row_tbl");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


function removeRow(row_id) {
    var item_code = $('#item_desc_' + row_id).attr('attr-code');
    if (item_code) {
        var item_desc = $('#item_desc_' + row_id).val();
        bootbox.confirm({
            message: "Are you sure, you want delete " + item_desc + " ?",
            buttons: {
                'confirm': {
                    label: "Delete",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var detail_id = $('#item_desc_' + row_id).attr('attr-detailid');
                    if (item_array[row_id]) {
                        delete item_array[row_id];
                    }
                    if (parseInt(detail_id) != 0) {
                        delete_item.push(detail_id);
                    }
                    $('#row_data_' + row_id).remove();
                    serialNoCalculation('row_class', 'row_count_class');
                    getAllItemsTotals();
                }
            }
        });
    } else {
        $('#row_data_' + row_id).remove();
    }
}


function purchase_history() {
    var row_id = $('#hdn_item_history').val();
    var item_code = $('#item_desc_' + row_id).attr('attr-code');
    var item_desc = $('#item_desc_' + row_id).val();
    if (item_code && item_code != 'undefined') {
        var url = base_url + "/grn/lastGRNHistory";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                item_code: item_code,
            },
            beforeSend: function () {
                $('#purchase_historymodel_header').html(item_desc);
                $("#row_body_data").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                if (data != 1) {
                    setTimeout(function () {
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 500);
                    $('#history_dl_list').html(data);
                    $("#purchase_historymodel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            },
            complete: function () {
                $("#row_body_data").LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });

    } else {
        toastr.warning("Please Select A Item !!");
    }

}



function postString(post_type) {
    var title = '';
    if (post_type == 1)
        title = "Free Stock Saved Successfuly";
    if (post_type == 2)
        title = "Free Stock Approved Successfuly";
    if (post_type == 3)
        title = "Free Stock Rejected Successfuly";
    if (post_type == 4)
        title = "Free Stock Closed Successfuly";
    return title;
}

function showBillNo(post_type) {
    var free_stock_no = $('#free_stock_no').val();
    var url = base_url + "/free_stock/listFreeStock";
    var title = postString(post_type);
    if (!free_stock_no)
        free_stock_no = $("#free_stock_no").html();
    bootbox.alert({
        title: title,
        message: "Free Stock No. :" + free_stock_no,
        callback: function () {
            window.location.href = url;
        }
    });
}


function checkIsNaN(number) {
    if (isNaN(number)) {
        number = 0;
    }
    number = parseFloat(number);
    return number.toFixed(parseInt(decimalConfiguration));
}




function validateGRNData() {
    var item_type = $('#item_type').val();
    $('.form-control').removeClass('validate_data');
    var flag = true;
    var message = '';
    var return_array = {};
    var validate_item_batch = [];
    $.each(item_array, function (index, value) {
        if (parseFloat(value.netRate) == 0 && parseInt(value.is_free) != 1 && value.item_code) {
            $('#net_cost_' + index).addClass('validate_data');
            flag = false;
            message = "Net amount canot be zero";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }
        var itemcode_batch = value.item_code + value.batch_no;
        if (!validate_item_batch.includes(itemcode_batch)) {
            validate_item_batch.push(itemcode_batch);
        } else {
            $('#batch_row' + index).addClass('validate_data');
            flag = false;
            message = "Duplicate Item Found";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }

        if (parseFloat(value.selling_price) > parseFloat(value.mrp)) {
            $('#sales_price_' + index).addClass('validate_data');
            flag = false;
            message = "Selling price is greater than MRP";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }

        if (parseInt(item_type) == 1 && !value.expiry_date && value.item_code) {
            $('#expiry_date' + index).addClass('validate_data');
            flag = false;
            message = "Expiry Date is mandatory for Pharmacy Items";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }

        if (parseInt(item_type) == 1 && !value.batch_no && value.item_code) {
            $('#batch_row' + index).addClass('validate_data');
            flag = false;
            message = "Batch No is mandatory for Pharmacy Items";
            return_array = {
                status: flag,
                message: message
            }
            return return_array;
        }
    });
    return_array = {
        status: flag,
        message: message
    }
    return return_array;
}


function saveFreeStockEntry(postType, fromType) {
    var url = base_url + "/free_stock/saveFreeStock";
    var item_array_string = JSON.stringify(item_array);
    var item_string = encodeURIComponent(item_array_string);
    var deleted_adjustment = JSON.stringify(delete_item);
    var item_type = $('#item_type').val();
    var location = $('#location').val();
    var free_stock_date = $('#free_stock_date').val();
    var freestock_reason = $('#freestock_reason').val();
    var freestock_remarks = $('#freestock_remarks').val();
    var freestockid = $('#freestockid').val();
    var adjustment_id = $('#adjustment_id').val();
    var free_stock_no = $('#free_stock_no').val();

    var total_rate = $('#gross_amount_hd').val();
    var total_tax = $('#tot_tax_amt_hd').val();
    var round_off = $('#round_off').val();
    var net_amount = $('#net_amount_hd').val();

    var array_len = Object.keys(item_array).length;
    var validate_grn = validateGRNData();
    if (validate_grn.status) {
        if (location) {
            if (item_type) {
                if (parseInt(array_len) != 0) {
                    var string_data = "";
                    if (parseInt(fromType) == 1) {
                        string_data = "Save"
                    } else if (parseInt(fromType) == 2) {
                        string_data = "Save &amp; Print"
                    } else if (parseInt(fromType) == 3) {
                        string_data = "Approve"
                    } else if (parseInt(fromType) == 4) {
                        string_data = "Approve &amp; Print"
                    } else if (parseInt(fromType) == 5) {
                        string_data = "Cancel"
                    } else if (parseInt(fromType) == 6) {
                        string_data = "Close Bill"
                    }
                    var status = 0;
                    bootbox.confirm({
                        message: "Are you sure, you want " + string_data + " ?",
                        buttons: {
                            'confirm': {
                                label: 'Yes',
                                className: 'btn-danger',
                                default: 'true'
                            },
                            'cancel': {
                                label: 'No',
                                className: 'btn-warning'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: {
                                        _token: token,
                                        adjustment_id: adjustment_id,
                                        freestockid: freestockid,
                                        item_type: item_type,
                                        location: location,
                                        free_stock_date: free_stock_date,
                                        freestock_reason: freestock_reason,
                                        freestock_remarks: freestock_remarks,
                                        free_stock_no: free_stock_no,
                                        postType: postType,
                                        item_string: item_string,
                                        deleted_adjustment: deleted_adjustment,
                                        total_rate: total_rate,
                                        total_tax: total_tax,
                                        round_off: round_off,
                                        net_amount: net_amount
                                    },
                                    beforeSend: function () {
                                        $('.disable_on_save').prop('disabled', true);
                                        $('#saveFreeStockEntrySpin' + fromType).removeClass('fa fa-save');
                                        $('#saveFreeStockEntrySpin' + fromType).addClass('fa fa-spinner fa-spin');
                                    },
                                    success: function (data) {
                                        status = data.status;
                                        if (parseInt(data.status) == 1) {
                                            toastr.success(data.message);
                                            $('#free_stock_no').val(data.free_stock_no);
                                            showBillNo(postType);
                                        } else {
                                            toastr.warning(data.message);
                                        }
                                    },

                                    complete: function () {
                                        $('.disable_on_save').prop('disabled', false);
                                        $('#saveFreeStockEntrySpin' + fromType).removeClass('fa fa-spinner fa-spin');
                                        $('#saveFreeStockEntrySpin' + fromType).addClass('fa fa-save');
                                    },
                                    error: function () {
                                        toastr.error("Error Please Check Your Internet Connection");
                                    }
                                });
                            }
                        }
                    });

                } else {
                    toastr.warning("Please Select any Item !!");
                }

            } else {
                toastr.warning("Please select any item type");
                $('#item_type').focus();
            }
        } else {
            toastr.warning("Please select any Supply Location");
            $('#location').focus();
        }
    } else {
        toastr.warning(validate_grn.message);
    }
}
