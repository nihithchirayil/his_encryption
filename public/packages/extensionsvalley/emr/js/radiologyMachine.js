$(document).ready(function () {
    
    searchRadiology();
    $(".select2").select2();
    
}); 

function searchRadiology() {
    var radiology_id = $('#radiology_hidden').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/searchRadiology";
    var param = {
        radiology_id: radiology_id,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#searchDataDiv").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })


        },
        success: function (data) {
            $("#searchDataDiv").html(data);
        },
        complete: function () {

            $("#searchDataDiv").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
    });
}

function saveMachine() {

    var Machine_name = $('#name').val();
    if(!Machine_name){
        toastr.warning('Please enter radiology machine name.');
        return;
    }
    var start_time=$('#start_tm').val();
    var end_time=$('#end_tm').val();
    var status = $('#status').val();
    var machine_Id = $('#hidden_id').val();
    var modality=$('#search_modality').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveRadiologyMachine";
    
    if(!start_time){
        toastr.warning('Please enter start time.');
        return;
    }
    if(!end_time){
        toastr.warning('Please enter end time.');
        return;
    }
    if(start_time>end_time){
        toastr.warning('Please check time you have entered.');
        return;
    }
    if(!modality){
        toastr.warning('Please select modatity.');
        return;
    }
    var param = {
        Machine_name: Machine_name,
        status: status,
        modality: modality,
        start_time:start_time,
        end_time:end_time,
        machine_Id: machine_Id
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#savebtnBtn").attr("disabled",true);
            $("#savebtnSpin").removeClass("fa fa-save");
            $("#savebtnSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function (data) {
            if (data.status == 101) {
                toastr.warning("" + data.message + "");
            
                 } 
                 else if(data){
                    toastr.success("" + data.message + "");
                    searchRadiology();
                    clearItem();
                 }
                 
        },
        complete: function () {
            $("#savebtnBtn").attr("disabled",false);
            $("#savebtnSpin").removeClass("fa fa-spinner fa-spin");
            $("#savebtnSpin").addClass("fa fa-save");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
            

        },
    });
}
function clearItem() {
    $('#hidden_id').val('');
    $('#name').val('');
    $('#search_modality').val('');
    $('#start_tm').val('');
    $('#end_tm').val('');

}
function editItem(id,name,start_time,end_time,modatity_id,status) {
    $('#hidden_id').val(id);
    $('#name').val(name);
    $('#start_tm').val(start_time);
    $('#end_tm').val(end_time);
    $('#search_modality').val(modatity_id);
    $('#status').val(status);
    $('#saveBtn').html('Update');
    
}
function deleteItem(id) {
    var message_show = '<p>Are you sure you want to delete? </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deleteRadiology";
                var param = {
                    id: id
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#searchDataDiv").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        }
                        else {
                            toastr.error(obj.message);
                        }
                    },
                    complete: function () {
                        $("#searchDataDiv").LoadingOverlay("hide");
                        searchRadiology();
                    },
                });
            }
        }
    });
}
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();



        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $('#base_url').val();
            var url = base_url + "/master/ajaxSearchRadiology";
            console.log(base_url);
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();

                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        // ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
    // doctorSearchList();

}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
});