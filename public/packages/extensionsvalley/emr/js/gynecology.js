$(document).ready(function (){
    initTinymce();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.date_time_picker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });
    expandTinymce();
    //$('.trimister').toggle();

    $(document).on("click", "#conception_natural_label", function(){
        $("#assisted_container").addClass('collapse').removeClass('in');
    });

    $(document).on("keyup", ".pregnancy", function(){
        var len = 0;
        $('.pregnancy').each(function(key, val){
            if($(val).val() == '') {
                len = len+1;
            }
        })
        if(len == 0)
            addNewPregnancyHistroyRow();

    });

    $(document).on('click', ".trimester_head", function(){
        var element = $(this).attr('data-element');
        var that = this;
        if($("#"+element).css("display") == 'block'){
            $("#"+element).hide();
        } else {
            $('#trimester1').hide();
            $('#trimester2').hide();
            $('#trimester3').hide();
            $("#"+element).show();
        }

        $([document.documentElement, document.body]).animate({
            scrollTop: $(that).offset().top
        }, 100);

    });

    $(document).on('blur', '.next_review_date', function(){
        takeNextAppointment();
    })

    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');

});

$(window).load(function(){
    fetchGynecologyDetails();

    $("ul.nav-tabs a").click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).tab('show');
    });

    $(document).on('keypress', '.gen_exam_height, .gen_exam_weight, .gen_exam_pulse, .gen_exam_bmi, .gen_exam_respiration, .gen_exam_temperature', function (e) {
        var keyCode = e.keyCode || e.which;

        //Regex for Valid Characters i.e. Numbers.
        var regex = /^[0-9]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            e.preventDefault();
        }

        return true;
    });
})

$('.select_button li').click(function () {
    $(this).toggleClass('active')
})

$("#poplink").popover({
    html: true,
    placement: "right",
    trigger: "hover",

    content: function () {
        return $(".pop-content").html();
    }
});

var acc = document.getElementsByClassName("accordion");
var investigation_future_date = 0;
var i;

for (i = 0; i < acc.length; i++) {
acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
    panel.style.display = "none";
    } else {
    panel.style.display = "block";
    }
});
}


function initTinymce(){

    tinymce.init({
        selector: 'textarea.texteditor11',
        max_height: 90,
        autoresize_min_height: '90',
        themes: "modern",
        content_css : 'tinymce_body',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
        imagetools_cors_hosts: ['picsum.photos'],
        // menubar: 'file edit view insert format tools table help',
        menubar: false,
        statusbar: false,
        toolbar: false,
        // toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        // toolbar_sticky: true,
        browser_spellcheck: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,

        importcss_append: true,

        height: 600,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',

        branding: false,
        statusbar: false,
        forced_root_block: '',
        setup: function(editor) {

            editor.ui.registry.addButton('Ucase', {
                text: 'A^',
                onAction: function() {
                    TextToUpperCase();
                },
            });
            editor.ui.registry.addButton('Lcase', {
                text: 'a^',
                onAction: function() {
                    TextToLowerCase();
                },
            });
            editor.ui.registry.addButton('Icase', {
                text: 'I^',
                onAction: function() {
                    TextToInterCase();
                },
            });
            editor.ui.registry.addButton('Ccase', {
                text: 'C^',
                onAction: function() {
                    FirstLetterToInterCase();
                },
            });

        },


    });
}

function expandTinymce(){
    $('.accordion').trigger('click');
    $('.tiny_block_toggle').trigger('click');
}


function getFormattedJSON1(){
    let gynecology_json = {};
    gynecology_json.pregnancy_history = {};
    gynecology_json.menstural_history = [];
    gynecology_json.pregnancy_chart = {};
    gynecology_json.trimester_content = {};
    gynecology_json.trimester_content.first_trimester = {};
    gynecology_json.trimester_content.second_trimester = {};
    gynecology_json.trimester_content.third_trimester = {};
    gynecology_json.medical_history = $('#medical_history_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.surgical_history = $('#surgical_history_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.family_history = $('#family_history_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.outside_investigation = $('#outside_investigation_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.preceding_complaints = $('#preceding_complaints_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.patient_diagnosis = $('#patient_diagnosis_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.findings_and_impressions = $('#findings_and_impressions_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.diet_remarks = $('#diet_remarks_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.presenting_complaints = $('#presenting_complaints_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.general_and_systemic_examination = $('#general_and_systemic_examination_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.followup_advise = $('#followup_advise_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.anomaly_scan_status = $('#anomaly_scan_status_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.growth_scan = $('#growth_scan_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.growth_fetal_position = $('#growth_fetal_position_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.lmp_date = $("#lmp_date").val();
    gynecology_json.edd_based_on_lmp_date = $("#edd_based_on_lmp_date").val() ? $("#edd_based_on_lmp_date").val(): null;
    gynecology_json.edd_based_on_uss = $("#edd_based_on_uss").val() ? $("#edd_based_on_uss").val() : null;
    gynecology_json.active_week = $("body").attr('data-active-week') ? $("body").attr('data-active-week') : 0;
    gynecology_json.active_trimester = $("body").attr('data-active-trimester') ? $("body").attr('data-active-trimester') :0;


    // pregnancy history
    gynecology_json.pregnancy_history.data = [];
    gynecology_json.pregnancy_history.gravida = $("#gravida").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.para = $("#para").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.abortion = $("#abortion").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.live = $("#live").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.ectopic = $("#ectopic").is(":checked") ? 1 : 0;

    $(".preg_history_body").find('tr').each(function(key, val){
        var temp_pregnancy_arr = {};
        temp_pregnancy_arr.pregnancy = $(val).find('.pregnancy').val() ? $(val).find('.pregnancy').val() : '';
        temp_pregnancy_arr.pregnancy_dob = $(val).find('.pregnancy_dob').val() ? $(val).find('.pregnancy_dob').val() : '';
        temp_pregnancy_arr.pregnancy_gender = $(val).find('.pregnancy_gender').val() ? $(val).find('.pregnancy_gender').val() : '';
        temp_pregnancy_arr.pregnancy_birth_weight = $(val).find('.pregnancy_birth_weight').val() ? $(val).find('.pregnancy_birth_weight').val() : '';
        temp_pregnancy_arr.pregnancy_remarks = $(val).find('.pregnancy_remarks').val() ? $(val).find('.pregnancy_remarks').val() : '';
        if(temp_pregnancy_arr.pregnancy != '')
            gynecology_json.pregnancy_history.data.push(temp_pregnancy_arr);
    });

    gynecology_json.pregnancy_history.conception = $('input[name=conception_options]:checked').val();
    gynecology_json.pregnancy_history.et_date = $('#et_date').val() ? $('#et_date').val() : '';
    gynecology_json.pregnancy_history.assisted_text = $('#assisted_text').val() ? $('#assisted_text').val(): '';
    if(gynecology_json.pregnancy_history.conception == 1)
        gynecology_json.pregnancy_history.assisted_text = '';

    gynecology_json.pregnancy_history.conception_options = [];
    $("#preg_history_group_items").find('li.active').each(function(key,val){
        var selected_conception_option = $(val).attr("data-value");
        gynecology_json.pregnancy_history.conception_options.push(selected_conception_option);
    });

    if($.inArray("Other", gynecology_json.pregnancy_history.conception_options))
        gynecology_json.pregnancy_history.conception_option_other_text = $("#conception_option_other_text").val() ? $("#conception_option_other_text").val() : '';

    gynecology_json.pregnancy_history.inj_high_date = $("#inj_high_date").val() ? $("#inj_high_date").val() : '';

    // menstural history
    $(".menstural_history_table_body").find('tr').each(function(key, val){
        var temp_menstural_arr = {};
        temp_menstural_arr.menstural_lmp_date = $(val).find('.menstural_lmp_date').val() ? $(val).find('.menstural_lmp_date').val() : '';
        temp_menstural_arr.menstural_type = $(val).find('.menstural_type').val() ? $(val).find('.menstural_type').val() : '';
        temp_menstural_arr.menstural_duration_of_cycle = $(val).find('.menstural_duration_of_cycle').val() ? $(val).find('.menstural_duration_of_cycle').val() : '';
        if(temp_menstural_arr.menstural_lmp_date != '')
            gynecology_json.menstural_history.push(temp_menstural_arr);
    });


    // pregnancy chart

    $(".pregnancy_chart_table_body").find('tr').each(function(key, val){
        var key_name = $(val).attr('data-name');
        gynecology_json.pregnancy_chart[key_name] = {};
        gynecology_json.pregnancy_chart[key_name].past_status = $(val).find('.past_status').parent().hasClass('off') ? 0 : 1;
        gynecology_json.pregnancy_chart[key_name].present_status = $(val).find('.present_status').parent().hasClass('off') ? 0 : 1;
    });

    gynecology_json.edd_based_on_lmp_date = $("#edd_based_on_lmp_date").val() ?  $("#edd_based_on_lmp_date").val() : '';
    gynecology_json.edd_based_on_uss = $("#edd_based_on_uss").val() ?  $("#edd_based_on_uss").val() : '';


    $(".week_detail").each(function(key, val){
        var week_number = parseInt($(val).attr('data-week-number'));
        if(week_number >= 1 && week_number <= 12){
            gynecology_json.trimester_content.first_trimester[week_number]  = {};
        } else if(week_number >= 13 && week_number <= 26){
            gynecology_json.trimester_content.second_trimester[week_number]  = {};
        } else if(week_number >= 26 && week_number <= 40){
            gynecology_json.trimester_content.third_trimester[week_number]  = {};
        }

        $(val).find('.days_detail').each(function(key1, val1){
            var visit_date = $(val1).attr('data-visit-date');

            var general_examination = {};
            general_examination.height = $(val1).find('.gen_exam_height').val() ? $(val1).find('.gen_exam_height').val() : "";
            general_examination.weight = $(val1).find('.gen_exam_weight').val() ? $(val1).find('.gen_exam_weight').val() : "";
            general_examination.pulse = $(val1).find('.gen_exam_pulse').val() ? $(val1).find('.gen_exam_pulse').val() : "";
            general_examination.bmi = $(val1).find('.gen_exam_bmi').val() ? $(val1).find('.gen_exam_bmi').val() : "";
            general_examination.respiration = $(val1).find('.gen_exam_respiration').val() ? $(val1).find('.gen_exam_respiration').val() : "";
            general_examination.temperature = $(val1).find('.gen_exam_temperature').val() ? $(val1).find('.gen_exam_temperature').val() : "";
            general_examination.pallor = $(val1).find('.gen_exam_pallor').val() ? $(val1).find('.gen_exam_pallor').val() : "";
            general_examination.oadema = $(val1).find('.gen_exam_oadema').val() ? $(val1).find('.gen_exam_oadema').val() : "";
            general_examination.breast = $(val1).find('.gen_exam_breast').val() ? $(val1).find('.gen_exam_breast').val() : "";
            general_examination.abdomen = $(val1).find('.gen_exam_abdomen').val() ? $(val1).find('.gen_exam_abdomen').val() : "";
            general_examination.thyroid = $(val1).find('.gen_exam_thyroid').find('.bg-radio_grp.active').find('input').val();
            general_examination.acne = $(val1).find('.gen_exam_acne').find('.bg-radio_grp.active').find('input').val();
            general_examination.striae = $(val1).find('.gen_exam_striae').find('.bg-radio_grp.active').find('input').val();
            general_examination.hirsutism = $(val1).find('.gen_exam_hirsutism').find('.bg-radio_grp.active').find('input').val();
            general_examination.galactorrhea = $(val1).find('.gen_exam_galactorrhea').find('.bg-radio_grp.active').find('input').val();

            var systemic_examination = {};
            systemic_examination.cvs = $(val1).find('.sys_exam_cvs').val() ? $(val1).find('.sys_exam_cvs').val() : "";
            systemic_examination.cns = $(val1).find('.sys_exam_cns').val() ? $(val1).find('.sys_exam_cns').val() : "";
            systemic_examination.resp_system = $(val1).find('.sys_exam_resp_system').val() ? $(val1).find('.sys_exam_resp_system').val() : "";


            var pelvic_examination = {};
            pelvic_examination.introitus = $(val1).find('.introitus').find('.bg-radio_grp.active').find('input').val();
            pelvic_examination.cx = $(val1).find('.cx').find('.bg-radio_grp.active').find('input').val();
            pelvic_examination.uterus_size = $(val1).find('.uterus_size').find('.bg-radio_grp.active').find('input').val();
            pelvic_examination.mobility = $(val1).find('.mobility').find('.bg-radio_grp.active').find('input').val();
            pelvic_examination.adnexa = $(val1).find('.adnexa').find('.bg-radio_grp.active').find('input').val();
            pelvic_examination.rv_av = $(val1).find('.rv_av').val() ? $(val1).find('.rv_av').val() : "";

            var investigations = [];
            $(val1).find('.investigation_container').find(".form-check-input").each(function(key2, val2){
                if($(val2).is(":checked")){
                    var temp_inv = {};
                    temp_inv.service_code = $(val2).val();
                    investigations.push(temp_inv);
                }
            });

            var uss = [];
            $(val1).find('.uss_container').find(".form-check-input").each(function(key2, val2){
                if($(val2).is(":checked")){
                    var temp_uss = {};
                    temp_uss.service_code = $(val2).val();
                    uss.push(temp_uss);
                }
            });

            var obstetric_examination = [];
            $(val1).find('.obtetric_examination_table_body').find('tr').each(function(key2, val2){
                var temp_obstetric_examination = {};

                temp_obstetric_examination.obstetric_date = $(val2).find('.obstetric_date').val() ? $(val2).find('.obstetric_date').val() : "";
                temp_obstetric_examination.gestation_age = $(val2).find('.gestation_age').val() ? $(val2).find('.gestation_age').val() : "";
                temp_obstetric_examination.height = $(val2).find('.height').val() ? $(val2).find('.height').val() : "";
                temp_obstetric_examination.weight = $(val2).find('.weight').val() ? $(val2).find('.weight').val() : "";
                temp_obstetric_examination.bp = $(val2).find('.bp').val() ? $(val2).find('.bp').val() : "";
                temp_obstetric_examination.fundal_height = $(val2).find('.fundal_height').val() ? $(val2).find('.fundal_height').val() : "";
                temp_obstetric_examination.presentation = $(val2).find('.presentation').val() ? $(val2).find('.presentation').val() : "";
                temp_obstetric_examination.engaged_or_not_engaged = $(val2).find('.engaged_or_not_engaged').val() ? $(val2).find('.engaged_or_not_engaged').val() : "";
                temp_obstetric_examination.fhs = $(val2).find('.fhs').val() ? $(val2).find('.fhs').val() : "";
                temp_obstetric_examination.remarks = $(val2).find('.remarks').val() ? $(val2).find('.remarks').val() : "";
                if(temp_obstetric_examination.obstetric_date != "")
                    obstetric_examination.push(temp_obstetric_examination);
            })


            if(week_number >= 1 && week_number <= 12){
                gynecology_json.trimester_content.first_trimester[week_number][visit_date] = {};
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].general_examination = general_examination;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].systemic_examination = systemic_examination;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].pelvic_examination = pelvic_examination;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].investigations = investigations;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].uss = uss;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].obstetric_examination = obstetric_examination;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].scan_details = $('#'+$(val1).find('.scan_details').attr('id')+'_ifr')[0].contentDocument.body.innerHTML;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].treatment_plan = $('#'+$(val1).find('.treatment_plan').attr('id')+'_ifr')[0].contentDocument.body.innerHTML;
                gynecology_json.trimester_content.first_trimester[week_number][visit_date].next_review_date = $(val1).find('.next_review_date').val() ? $(val1).find('.next_review_date').val() : '';

            } else if(week_number >= 13 && week_number <= 26){
                gynecology_json.trimester_content.second_trimester[week_number][visit_date] = {};
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].general_examination = general_examination;
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].systemic_examination = systemic_examination;
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].pelvic_examination = pelvic_examination;
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].investigations = investigations;
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].uss = uss;
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].obstetric_examination = obstetric_examination;
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].scan_details = $(val1).find('.scan_details').attr('id') ? $('#'+$(val1).find('.scan_details').attr('id')+'_ifr')[0].contentDocument.body.innerHTML : '';
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].treatment_plan = $(val1).find('.treatment_plan').attr('id') ? $('#'+$(val1).find('.treatment_plan').attr('id')+'_ifr')[0].contentDocument.body.innerHTML : '';
                gynecology_json.trimester_content.second_trimester[week_number][visit_date].next_review_date = $(val1).find('.next_review_date').val() ? $(val1).find('.next_review_date').val() : '';
            } else if(week_number >= 26 && week_number <= 40){
                gynecology_json.trimester_content.third_trimester[week_number][visit_date] = {};
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].general_examination = general_examination;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].systemic_examination = systemic_examination;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].pelvic_examination = pelvic_examination;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].investigations = investigations;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].uss = uss;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].obstetric_examination = obstetric_examination;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].scan_details = $('#'+$(val1).find('.scan_details').attr('id')+'_ifr')[0].contentDocument.body.innerHTML;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].treatment_plan = $('#'+$(val1).find('.treatment_plan').attr('id')+'_ifr')[0].contentDocument.body.innerHTML;
                gynecology_json.trimester_content.third_trimester[week_number][visit_date].next_review_date = $(val1).find('.next_review_date').val() ? $(val1).find('.next_review_date').val() : '';
            }
        });
    });

    return gynecology_json;
}


function getFormattedJSON(){
    let gynecology_json = {};
    gynecology_json.pregnancy_history = {};
    gynecology_json.menstural_history = [];
    gynecology_json.pregnancy_chart = {};
    gynecology_json.current_visit_details = {};
    gynecology_json.medical_history = $('#medical_history_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.surgical_history = $('#surgical_history_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.family_history = $('#family_history_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.outside_investigation = $('#outside_investigation_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.preceding_complaints = $('#preceding_complaints_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.patient_diagnosis = $('#patient_diagnosis_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.findings_and_impressions = $('#findings_and_impressions_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.diet_remarks = $('#diet_remarks_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.presenting_complaints = $('#presenting_complaints_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.general_and_systemic_examination = $('#general_and_systemic_examination_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.followup_advise = $('#followup_advise_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.anomaly_scan_status = $('#anomaly_scan_status_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.growth_scan = $('#growth_scan_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.growth_fetal_position = $('#growth_fetal_position_ifr')[0].contentDocument.body.innerHTML;
    gynecology_json.lmp_date = $("#lmp_date").val();
    gynecology_json.edd_based_on_lmp_date = $("#edd_based_on_lmp_date").val() ? $("#edd_based_on_lmp_date").val(): null;
    gynecology_json.edd_based_on_uss = $("#edd_based_on_uss").val() ? $("#edd_based_on_uss").val() : null;
    gynecology_json.active_week = $("body").attr('data-active-week') ? $("body").attr('data-active-week') : 0;
    gynecology_json.active_trimester = $("body").attr('data-active-trimester') ? $("body").attr('data-active-trimester') :0;


    // pregnancy history
    gynecology_json.pregnancy_history.data = [];
    gynecology_json.pregnancy_history.gravida = $("#gravida").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.para = $("#para").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.abortion = $("#abortion").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.live = $("#live").is(":checked") ? 1 : 0;
    gynecology_json.pregnancy_history.ectopic = $("#ectopic").is(":checked") ? 1 : 0;

    $(".preg_history_body").find('tr').each(function(key, val){
        var temp_pregnancy_arr = {};
        temp_pregnancy_arr.pregnancy = $(val).find('.pregnancy').val() ? $(val).find('.pregnancy').val() : '';
        temp_pregnancy_arr.pregnancy_dob = $(val).find('.pregnancy_dob').val() ? $(val).find('.pregnancy_dob').val() : '';
        temp_pregnancy_arr.pregnancy_gender = $(val).find('.pregnancy_gender').val() ? $(val).find('.pregnancy_gender').val() : '';
        temp_pregnancy_arr.pregnancy_birth_weight = $(val).find('.pregnancy_birth_weight').val() ? $(val).find('.pregnancy_birth_weight').val() : '';
        temp_pregnancy_arr.pregnancy_remarks = $(val).find('.pregnancy_remarks').val() ? $(val).find('.pregnancy_remarks').val() : '';
        if(temp_pregnancy_arr.pregnancy != '')
            gynecology_json.pregnancy_history.data.push(temp_pregnancy_arr);
    });

    gynecology_json.pregnancy_history.conception = $('input[name=conception_options]:checked').val();
    gynecology_json.pregnancy_history.et_date = $('#et_date').val() ? $('#et_date').val() : '';
    gynecology_json.pregnancy_history.assisted_text = $('#assisted_text').val() ? $('#assisted_text').val(): '';
    if(gynecology_json.pregnancy_history.conception == 1)
        gynecology_json.pregnancy_history.assisted_text = '';

    gynecology_json.pregnancy_history.conception_options = [];
    $("#preg_history_group_items").find('li.active').each(function(key,val){
        var selected_conception_option = $(val).attr("data-value");
        gynecology_json.pregnancy_history.conception_options.push(selected_conception_option);
    });

    if($.inArray("Other", gynecology_json.pregnancy_history.conception_options))
        gynecology_json.pregnancy_history.conception_option_other_text = $("#conception_option_other_text").val() ? $("#conception_option_other_text").val() : '';

    gynecology_json.pregnancy_history.inj_high_date = $("#inj_high_date").val() ? $("#inj_high_date").val() : '';

    // menstural history
    $(".menstural_history_table_body").find('tr').each(function(key, val){
        var temp_menstural_arr = {};
        temp_menstural_arr.menstural_lmp_date = $(val).find('.menstural_lmp_date').val() ? $(val).find('.menstural_lmp_date').val() : '';
        temp_menstural_arr.menstural_type = $(val).find('.menstural_type').val() ? $(val).find('.menstural_type').val() : '';
        temp_menstural_arr.menstural_duration_of_cycle = $(val).find('.menstural_duration_of_cycle').val() ? $(val).find('.menstural_duration_of_cycle').val() : '';
        if(temp_menstural_arr.menstural_lmp_date != '')
            gynecology_json.menstural_history.push(temp_menstural_arr);
    });


    // pregnancy chart

    $(".pregnancy_chart_table_body").find('tr').each(function(key, val){
        var key_name = $(val).attr('data-name');
        gynecology_json.pregnancy_chart[key_name] = {};
        gynecology_json.pregnancy_chart[key_name].past_status = $(val).find('.past_status').parent().hasClass('off') ? 0 : 1;
        gynecology_json.pregnancy_chart[key_name].present_status = $(val).find('.present_status').parent().hasClass('off') ? 0 : 1;
    });

    gynecology_json.edd_based_on_lmp_date = $("#edd_based_on_lmp_date").val() ?  $("#edd_based_on_lmp_date").val() : '';
    gynecology_json.edd_based_on_uss = $("#edd_based_on_uss").val() ?  $("#edd_based_on_uss").val() : '';

    gynecology_json.current_visit_details = {};
    $(".days_detail.active").each(function(key1, val1){
        var visit_date = $(val1).attr('data-visit-date');
        gynecology_json.visit_date = visit_date;

        var general_examination = {};
        general_examination.height = $(val1).find('.gen_exam_height').val() ? $(val1).find('.gen_exam_height').val() : "";
        general_examination.weight = $(val1).find('.gen_exam_weight').val() ? $(val1).find('.gen_exam_weight').val() : "";
        general_examination.pulse = $(val1).find('.gen_exam_pulse').val() ? $(val1).find('.gen_exam_pulse').val() : "";
        general_examination.bmi = $(val1).find('.gen_exam_bmi').val() ? $(val1).find('.gen_exam_bmi').val() : "";
        general_examination.respiration = $(val1).find('.gen_exam_respiration').val() ? $(val1).find('.gen_exam_respiration').val() : "";
        general_examination.temperature = $(val1).find('.gen_exam_temperature').val() ? $(val1).find('.gen_exam_temperature').val() : "";
        general_examination.pallor = $(val1).find('.gen_exam_pallor').val() ? $(val1).find('.gen_exam_pallor').val() : "";
        general_examination.oadema = $(val1).find('.gen_exam_oadema').val() ? $(val1).find('.gen_exam_oadema').val() : "";
        general_examination.breast = $(val1).find('.gen_exam_breast').val() ? $(val1).find('.gen_exam_breast').val() : "";
        general_examination.abdomen = $(val1).find('.gen_exam_abdomen').val() ? $(val1).find('.gen_exam_abdomen').val() : "";
        general_examination.thyroid = $(val1).find('.gen_exam_thyroid').find('.bg-radio_grp.active').find('input').val();
        general_examination.acne = $(val1).find('.gen_exam_acne').find('.bg-radio_grp.active').find('input').val();
        general_examination.striae = $(val1).find('.gen_exam_striae').find('.bg-radio_grp.active').find('input').val();
        general_examination.hirsutism = $(val1).find('.gen_exam_hirsutism').find('.bg-radio_grp.active').find('input').val();
        general_examination.galactorrhea = $(val1).find('.gen_exam_galactorrhea').find('.bg-radio_grp.active').find('input').val();
        general_examination.date = visit_date;

        var systemic_examination = {};
        systemic_examination.cvs = $(val1).find('.sys_exam_cvs').val() ? $(val1).find('.sys_exam_cvs').val() : "";
        systemic_examination.cns = $(val1).find('.sys_exam_cns').val() ? $(val1).find('.sys_exam_cns').val() : "";
        systemic_examination.resp_system = $(val1).find('.sys_exam_resp_system').val() ? $(val1).find('.sys_exam_resp_system').val() : "";


        var pelvic_examination = {};
        pelvic_examination.introitus = $(val1).find('.introitus').find('.bg-radio_grp.active').find('input').val();
        pelvic_examination.cx = $(val1).find('.cx').find('.bg-radio_grp.active').find('input').val();
        pelvic_examination.uterus_size = $(val1).find('.uterus_size').find('.bg-radio_grp.active').find('input').val();
        pelvic_examination.mobility = $(val1).find('.mobility').find('.bg-radio_grp.active').find('input').val();
        pelvic_examination.adnexa = $(val1).find('.adnexa').find('.bg-radio_grp.active').find('input').val();
        pelvic_examination.rv_av = $(val1).find('.rv_av').val() ? $(val1).find('.rv_av').val() : "";

        var investigations = [];
        $(val1).find('.investigation_container').find(".form-check-input").each(function(key2, val2){
            if($(val2).is(":checked")){
                var temp_inv = {};
                temp_inv.service_code = $(val2).val();
                investigations.push(temp_inv);
            }
        });

        var uss = [];
        $(val1).find('.uss_container').find(".form-check-input").each(function(key2, val2){
            if($(val2).is(":checked")){
                var temp_uss = {};
                temp_uss.service_code = $(val2).val();
                uss.push(temp_uss);
                investigations.push(temp_uss);
            }
        });

        var obstetric_examination = [];
        $(val1).find('.obtetric_examination_table_body').find('tr').each(function(key2, val2){
            var temp_obstetric_examination = {};

            temp_obstetric_examination.obstetric_date = $(val2).find('.obstetric_date').val() ? $(val2).find('.obstetric_date').val() : moment().format('MMM-DD-YYYY');
            temp_obstetric_examination.gestation_age = $(val2).find('.gestation_age').val() ? $(val2).find('.gestation_age').val() : "";
            temp_obstetric_examination.height = $(val2).find('.height').val() ? $(val2).find('.height').val() : "";
            temp_obstetric_examination.weight = $(val2).find('.weight').val() ? $(val2).find('.weight').val() : "";
            temp_obstetric_examination.bp = $(val2).find('.bp').val() ? $(val2).find('.bp').val() : "";
            temp_obstetric_examination.fundal_height = $(val2).find('.fundal_height').val() ? $(val2).find('.fundal_height').val() : "";
            temp_obstetric_examination.presentation = $(val2).find('.presentation').val() ? $(val2).find('.presentation').val() : "";
            temp_obstetric_examination.engaged_or_not_engaged = $(val2).find('.engaged_or_not_engaged').val() ? $(val2).find('.engaged_or_not_engaged').val() : "";
            temp_obstetric_examination.fhs = $(val2).find('.fhs').val() ? $(val2).find('.fhs').val() : "";
            temp_obstetric_examination.remarks = $(val2).find('.remarks').val() ? $(val2).find('.remarks').val() : "";
            if(temp_obstetric_examination.gestation_age.trim() != "")
                obstetric_examination.push(temp_obstetric_examination);
        });



        gynecology_json.current_visit_details.general_examination = general_examination;
        gynecology_json.current_visit_details.systemic_examination = systemic_examination;
        gynecology_json.current_visit_details.pelvic_examination = pelvic_examination;
        gynecology_json.current_visit_details.investigations = investigations;
        gynecology_json.current_visit_details.uss = uss;
        gynecology_json.current_visit_details.obstetric_examination = obstetric_examination;
        gynecology_json.current_visit_details.scan_details = $('#'+$(val1).find('.scan_details').attr('id')+'_ifr')[0].contentDocument.body.innerHTML;
        gynecology_json.current_visit_details.treatment_plan = $('#'+$(val1).find('.treatment_plan').attr('id')+'_ifr')[0].contentDocument.body.innerHTML;
        gynecology_json.current_visit_details.next_review_date = $(val1).find('.next_review_date').val() ? $(val1).find('.next_review_date').val() : '';

    });

    return gynecology_json;
}

function addNewPregnancyHistroyRow(){
    $('.preg_history_body').append('<tr><td> <input type="text" autocomplete="off" name="pregnancy_name[]" autofocus="" value="" class="form-control table_text pregnancy" placeholder="" id="" style=""></td><td><input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker table_text pregnancy_dob" placeholder="MMM-DD-YYYY" id="" style=""></td><td><select class="form-control table_text pregnancy_gender" id="gender" style="font-weight:700;border:none;" name="gender"><option value="">Select Gender</option><option value="1">Male</option><option value="2">Female</option><option value="3">Transgender</option></select></td><td><input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text pregnancy_birth_weight" placeholder="" id="" style=""></td><td><input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text pregnancy_remarks" placeholder="" id="" style=""></td></tr>');

    setTimeout(function(){
        $('.datepicker').datetimepicker({
            format: 'MMM-DD-YYYY'
        });
    }, 1000)
}

function addNewMensturalHistroyRow(){
    var len = 0;
    $('.menstural_lmp_date').each(function(key, val){
        if($(val).val() == '') {
            len = len+1;
        }
    })
    if(len == 0){
        $('.menstural_history_table_body').append('<tr><td><input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker table_text menstural_lmp_date" placeholder="MMM-DD-YYYY" id="" style="" required onblur="addNewMensturalHistroyRow()"></td><td><select class="form-control table_text" id="menstural_types" style="font-weight:700;border:none;" name="gender"><option value="">Select Type</option><option value="1">Regular</option><option value="2">Irregular</option></select></td><td><input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text menstural_duration_of_cycle" placeholder="" id="" style=""></td></tr>');

        setTimeout(function(){
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });
        }, 1000)

    }

}

function saveGynecologyDetails(){
    var json_data = getFormattedJSON();
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    var doctor_id = $("#doctor_id").val();
    var encounter_id = $("#encounter_id").val();
    json_data.patient_id = patient_id;
    json_data.visit_id = visit_id;
    json_data.encounter_id = encounter_id;
    json_data.doctor_id = doctor_id;

    var json_data = JSON.stringify(json_data);
    json_data = escape(json_data);

    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/gynecology/saveGynecologyDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: "json_data="+json_data+"&_token="+_token,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"](data.message);
                window.location.reload();
            } else {
                Command: toastr["error"](data.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}


function fetchGynecologyDetails(){
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    var doctor_id = $("#doctor_id").val();
    let _token = $('#c_token').val();
    var request_data = {};
    request_data.patient_id = patient_id;
    request_data.visit_id = visit_id;
    request_data.doctor_id = doctor_id;
    request_data._token = _token;

    var url = $('#base_url').val() + "/gynecology/fetchGynecologyDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: request_data,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data.status == 1) {

                if(Object.keys(data.gynecology_data).length > 0){

                    var trimester_content = JSON.parse(data.gynecology_data.trimester_content);
                    if(Object.keys(trimester_content.first_trimester).length == 0){
                        $('div[data-element=trimester1]').hide();
                        $('#trimester1').hide();
                    }
                    if(Object.keys(trimester_content.second_trimester).length == 0){
                        $('div[data-element=trimester2]').hide();
                        $('#trimester2').hide();
                    }
                    if(Object.keys(trimester_content.third_trimester).length == 0){
                        $('div[data-element=trimester3]').hide();
                        $('#trimester3').hide();
                    }
                    $("#trimester1").html(data.first_trimester_html);
                    $("#trimester2").html(data.second_trimester_html);
                    $("#trimester3").html(data.third_trimester_html);



                    $("ul.nav-tabs a").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).tab('show');
                    });

                    initTinymce();

                    $('.datepicker').datetimepicker({
                        format: 'MMM-DD-YYYY'
                    });

                    // disabling all input fields of previous visits
                    $(".days_detail").each(function(key, val){
                        if($(val).attr('data-visit-date') != data.current_date ){
                            $(val).find('input').attr('disabled', true);
                            $(val).find('textarea').attr('disabled', true);
                            $(val).find('.showPrescriptionFormBtn').hide();
                            $(val).find('div[class*="gen_exam_"]').css('pointer-events', 'none');
                            $(val).find('div[class*="pelvic_exam_"]').css('pointer-events', 'none');
                        }
                    });

                    if(data.active_week){
                        var active_week = parseInt(data.active_week);
                        $('body').attr('data-active-week', active_week);
                        $('body').attr('data-active-trimester', data.active_trimester);
                        if($('div[data-week-number='+active_week+']').length > 0){
                            var id = $('div[data-week-number='+active_week+']').attr('id');
                            $("a[href='#"+id+"']").click();
                            $('div[data-week-number='+active_week+']').find(".days_head").find('li').last().find('a').click();
                        }

                        if(data.active_trimester == 1 ){
                            $('div[data-element=trimester1]').show();
                            $('#trimester1').show();
                        } else if( data.active_trimester == 2){
                            $('div[data-element=trimester2]').show();
                            $('#trimester2').show();
                        } else if( data.active_trimester == 3){
                            $('div[data-element=trimester3]').show();
                            $('#trimester3').show();
                        }

                        fetchVitalsIntoGynec();

                    }

                    $('#medical_history_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.medical_history;
                    $('#surgical_history_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.surgical_history;
                    $('#family_history_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.family_history;
                    $('#outside_investigation_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.outside_investigation;
                    $('#preceding_complaints_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.preceding_complaints;
                    $('#patient_diagnosis_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.patient_diagnosis;
                    $('#findings_and_impressions_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.findings_and_impressions;
                    $('#diet_remarks_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.diet_remarks;
                    $('#presenting_complaints_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.presenting_complaints;
                    $('#general_and_systemic_examination_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.general_and_systemic_examination;
                    $('#followup_advise_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.followup_advise;
                    $('#anomaly_scan_status_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.anomaly_scan_status;
                    $('#growth_scan_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.growth_scan;
                    $('#growth_fetal_position_ifr')[0].contentDocument.body.innerHTML = data.gynecology_data.growth_fetal_position;

                    $("#lmp_date").val(moment(data.gynecology_data.lmp_date).format('MMM-DD-YYYY'));
                    $("#edd_date").val(moment(data.gynecology_data.edd_based_on_lmp_date).format('MMM-DD-YYYY'));
                    $("#edd_based_on_lmp_date").val(moment(data.gynecology_data.edd_based_on_lmp_date).format('MMM-DD-YYYY'));
                    if(data.gynecology_data.edd_based_on_uss)
                        $("#edd_based_on_uss").val(moment(data.gynecology_data.edd_based_on_uss).format('MMM-DD-YYYY'));

                    var pregnancy_history = JSON.parse(data.gynecology_data.pregnancy_history);
                    if(pregnancy_history.gravida == 1) { $("#gravida").prop("checked", true); }
                    if(pregnancy_history.para == 1) { $("#para").prop("checked", true); }
                    if(pregnancy_history.abortion == 1) { $("#abortion").prop("checked", true); }
                    if(pregnancy_history.live == 1) { $("#live").prop("checked", true); }
                    if(pregnancy_history.ectopic == 1) { $("#ectopic").prop("checked", true); }
                    if(pregnancy_history.inj_high_date)
                        $("#inj_high_date").val(moment(pregnancy_history.inj_high_date).format('MMM-DD-YYYY'));
                    if(pregnancy_history.et_date)
                        $("#et_date").val(moment(pregnancy_history.et_date).format('MMM-DD-YYYY'));
                    $("#assisted_text").val(pregnancy_history.assisted_text);
                    $("#conception_option_other_text").val(pregnancy_history.conception_option_other_text);
                    $('input[name=conception_options][value='+pregnancy_history.conception+']').trigger('click');

                    $.each(pregnancy_history.conception_options, function(key, val){
                        $('#preg_history_group_items').find('li[data-value='+val+']').trigger('click');
                    })

                    $.each(pregnancy_history.data, function(key, val){
                        var gender_male, gender_female, gender_other = '';
                        if(val.pregnancy_gender == 1){
                            gender_male = ' selected ';
                        } else if(val.pregnancy_gender == 2){
                            gender_female = ' selected ';
                        } else if(val.pregnancy_gender == 3){
                            gender_other = ' selected ';
                        }
                        $('.preg_history_body').prepend('<tr><td> <input type="text" autocomplete="off" name="pregnancy_name[]" autofocus="" value="'+val.pregnancy+'" class="form-control table_text pregnancy" placeholder="" id="" style=""></td><td><input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="'+moment(val.pregnancy_dob).format('MMM-DD-YYYY')+'" class="form-control datepicker table_text pregnancy_dob" placeholder="MMM-DD-YYYY" id="" style=""></td><td><select class="form-control table_text pregnancy_gender" id="gender" style="font-weight:700;border:none;" name="gender"><option value="">Select Gender</option><option '+gender_male+' value="1">Male</option><option '+gender_female+' value="2">Female</option><option '+gender_other+' value="3">Transgender</option></select></td><td><input type="text" autocomplete="off" name="" autofocus="" value="'+val.pregnancy_birth_weight+'" class="form-control table_text pregnancy_birth_weight" placeholder="" id="" style=""></td><td><input type="text" autocomplete="off" name="" autofocus="" value="'+val.pregnancy_remarks+'" class="form-control table_text pregnancy_remarks" placeholder="" id="" style=""></td></tr>');
                    })


                    var menstural_history = JSON.parse(data.gynecology_data.menstural_history);
                    $.each(menstural_history, function(key, val){
                        var type_regular, type_irregular = '';
                        if(val.menstural_type == 1){
                            type_regular = ' selected ';
                        } else if(val.menstural_type == 2){
                            type_irregular = ' selected ';
                        }

                        $('.menstural_history_table_body').prepend('<tr><td><input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="'+moment(val.menstural_lmp_date).format('MMM-DD-YYYY')+'" class="form-control datepicker table_text menstural_lmp_date" placeholder="MMM-DD-YYYY" id="" style="" required onblur="addNewMensturalHistroyRow()"></td><td><select class="form-control table_text" id="menstural_types" style="font-weight:700;border:none;" name="menstural_types"><option value="">Select Type</option><option '+type_regular+'  value="1">Regular</option><option '+type_irregular+' value="2">Irregular</option></select></td><td><input type="text" autocomplete="off" name="" autofocus="" value="'+val.menstural_duration_of_cycle+'" class="form-control table_text menstural_duration_of_cycle" placeholder="" id="" style=""></td></tr>');
                    })


                    var pregnancy_chart = JSON.parse(data.gynecology_data.pregnancy_chart);
                    $.each(pregnancy_chart, function(key, val){
                        if(val.present_status == 0)
                            $(".pregnancy_chart_table_body").find('tr[data-name='+key+']').find('.present_status').parent().trigger('click');
                        if(val.past_status == 0)
                            $(".pregnancy_chart_table_body").find('tr[data-name='+key+']').find('.past_status').parent().trigger('click');
                    });

                } else {

                    $("#trimester1").html(data.first_trimester_html);
                    $("#trimester2").html(data.second_trimester_html);
                    $("#trimester3").html(data.third_trimester_html);

                    $("ul.nav-tabs a").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).tab('show');
                    });

                    initTinymce();

                    $('.datepicker').datetimepicker({
                        format: 'MMM-DD-YYYY'
                    });

                    if(data.active_week){
                        var active_week = parseInt(data.active_week);
                        $('body').attr('data-active-week', active_week);
                        $('body').attr('data-active-trimester', data.active_trimester);
                        if($('div[data-week-number='+active_week+']').length > 0){
                            var id = $('div[data-week-number='+active_week+']').attr('id');
                            $("a[href='#"+id+"']").click();
                            $('div[data-week-number='+active_week+']').find(".days_head").find('li').last().find('a').click();
                        }

                        if(data.active_trimester == 1 ){
                            $('div[data-element=trimester1]').show();
                            $('#trimester1').show();
                        } else if( data.active_trimester == 2){
                            $('div[data-element=trimester2]').show();
                            $('#trimester2').show();
                        } else if( data.active_trimester == 3){
                            $('div[data-element=trimester3]').show();
                            $('#trimester3').show();
                        }

                    }
                }
            } else {
                Command: toastr["error"]("Failed to fetch details");
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });
}

function calculateEDDBasedOnLMP(){
    var lmp_date = $("#lmp_date").val();
    var edd_date = moment(lmp_date, "MMM-DD-YYYY").add(280, 'days');
    edd_date = moment(edd_date).format('MMM-DD-YYYY')
    $("#edd_date").val(edd_date);
    $("#edd_based_on_lmp_date").val(edd_date);

    var message_show = 'Are you sure you want save ?';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Save',
                className: 'btn-success',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function(result) {
            if (result) {
                saveGynecologyDetails();
            }
        }
    });
}

function fetchInvestigationHistory (visit_date){
    var hard_reload = false;
    if(!visit_date){
        hard_reload = true;
        visit_date = $(".days_detail.active").attr('data-visit-date');
    }
    if($(".days_detail.active").find('.investigationHistoryContainer').find('.popover').css('display') == 'block' && hard_reload == false){
        $(".days_detail.active").find('.investigationHistoryContainer').find('.popover').hide();
        $(".days_detail.active").find('.investigationHistoryContainer').find('.popover-content').empty();
    } else {
        let _token = $('#c_token').val();
        let patient_id = $('#patient_id').val();
        var url = $('#base_url').val() + "/gynecology/fetchInvestigationHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "visit_date="+visit_date+"&patient_id="+patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".days_detail.active").find('.fetchInvestigationIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {
                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                        $(".days_detail.active").find('.investigationHistoryContainer').find('.popover').hide();
                        $(".days_detail.active").find('.investigationHistoryContainer').find('.popover-content').empty();
                    } else {
                        $(".days_detail.active").find('.investigationHistoryContainer').find('.popover-content').html(data.history);
                        $(".days_detail.active").find('.investigationHistoryContainer').find('.popover').show();

                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }

                }
            },
            complete: function () {
                $(".days_detail.active").find('.fetchInvestigationIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-list');
            }
        });
    }
}


function fetchUSSHistory (visit_date){
    var hard_reload = false;
    if(!visit_date){
        hard_reload = true;
        visit_date = $(".days_detail.active").attr('data-visit-date');
    }
    if($(".days_detail.active").find('.ussHistoryContainer').find('.popover').css('display') == 'block' && hard_reload == false){
        $(".days_detail.active").find('.ussHistoryContainer').find('.popover').hide();
        $(".days_detail.active").find('.ussHistoryContainer').find('.popover-content').empty();
    } else {
        let _token = $('#c_token').val();
        let patient_id = $('#patient_id').val();
        var url = $('#base_url').val() + "/gynecology/fetchUSSHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "visit_date="+visit_date+"&patient_id="+patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".days_detail.active").find('.fetchUSSIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {
                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                        $(".days_detail.active").find('.ussHistoryContainer').find('.popover').hide();
                        $(".days_detail.active").find('.ussHistoryContainer').find('.popover-content').empty();
                    } else {
                        $(".days_detail.active").find('.ussHistoryContainer').find('.popover-content').html(data.history);
                        $(".days_detail.active").find('.ussHistoryContainer').find('.popover').show();

                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }

                }
            },
            complete: function () {
                $(".days_detail.active").find('.fetchUSSIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-list');
            }
        });
    }
}



function fetchPrescriptionHistory (visit_date){
    var hard_reload = false;
    if(!visit_date){
        hard_reload = true;
        visit_date = $(".days_detail.active").attr('data-visit-date');
    }
    if($(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover').css('display') == 'block' && hard_reload == false){
        $(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover').hide();
        $(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover-content').empty();
    } else {
        let _token = $('#c_token').val();
        let patient_id = $('#patient_id').val();
        var url = $('#base_url').val() + "/gynecology/fetchPrescriptionHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "visit_date="+visit_date+"&patient_id="+patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".days_detail.active").find('.fetchPrescriptionIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {

                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                        $(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover').hide();
                        $(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover-content').empty();
                    } else {
                        $(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover-content').html(data.history);
                        $(".days_detail.active").find('.PrescriptionHistoryContainer').find('.popover').show();

                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }
                }
            },
            complete: function () {
                $(".days_detail.active").find('.fetchPrescriptionIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-list');
            }
        });
    }
}

$(document).on("click", '.deleteInvestigationButton', function(){
    var that = this;
    var investigation_detail_id = $(this).attr('data-investigation-detail-id');
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/gynecology/deleteInvestigation";
    $.ajax({
        type: "POST",
        url: url,
        async:true,
        data: "investigation_detail_id="+investigation_detail_id+"&_token="+_token,
        beforeSend: function () {
            $(that).find('.deleteInvestigationIcon').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"]("Success");
                fetchInvestigationHistory();
            }
        },
        complete: function () {
            $(that).find('.deleteInvestigationIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
        }
    });
})

$(document).on("click", '.deleteUSSButton', function(){
    var that = this;
    var investigation_detail_id = $(this).attr('data-investigation-detail-id');
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/gynecology/deleteUSS";
    $.ajax({
        type: "POST",
        url: url,
        async:true,
        data: "investigation_detail_id="+investigation_detail_id+"&_token="+_token,
        beforeSend: function () {
            $(that).find('.deleteUSSIcon').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"]("Success");
                fetchUSSHistory();
            }
        },
        complete: function () {
            $(that).find('.deleteUSSIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
        }
    });
})

$(document).on("click", '.deletePrescriptionButton', function(){
    var that = this;
    var prescription_detail_id = $(this).attr('data-prescription-detail-id');
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/gynecology/deletePrescription";
    $.ajax({
        type: "POST",
        url: url,
        async:true,
        data: "prescription_detail_id="+prescription_detail_id+"&_token="+_token,
        beforeSend: function () {
            $(that).find('.deletePrescriptionIcon').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
        },
        success: function (data) {
            if (data.status == 1) {
                Command: toastr["success"]("Success");
                fetchPrescriptionHistory();
            }
        },
        complete: function () {
            $(that).find('.deletePrescriptionIcon').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-trash');
        }
    });
})

$(document).on("click", '.showPrescriptionFormBtn', function(){
    $(".gynec_prescription_modal").modal('show');
    if($("#medicine-listing-table").find('tr').length == 0){
        addNewMedicine();
    }
});

$(document).on("click", '.saveGynecPrescriptionBtn', function(){
    $(".gynec_prescription_modal").modal('hide');
});

function fetchVitalsIntoGynec(){
    var patient_id = $('#patient_id').val();
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/gynecology/fetchVitalsIntoGynec";
    $.ajax({
        type: "POST",
        url: url,
        async:true,
        data: "patient_id="+patient_id+"&_token="+_token,
        beforeSend: function () {

        },
        success: function (data) {
            if (data.status == 1) {
                var active_trimester = $('body').attr('data-active-trimester');
                $("#trimester"+active_trimester).find(".days_detail.active").find('.gen_exam_height').val(data.data.vitals.height ? data.data.vitals.height : '');
                $("#trimester"+active_trimester).find(".days_detail.active").find('.gen_exam_weight').val(data.data.vitals.weight ? data.data.vitals.weight : '');
                $("#trimester"+active_trimester).find(".days_detail.active").find('.gen_exam_pulse').val(data.data.vitals.pulse ? data.data.vitals.pulse : '');
                $("#trimester"+active_trimester).find(".days_detail.active").find('.gen_exam_bmi').val(data.data.vitals.bmi ? data.data.vitals.bmi : '');
                $("#trimester"+active_trimester).find(".days_detail.active").find('.gen_exam_respiration').val(data.data.vitals.respiration ? data.data.vitals.respiration : '');
                $("#trimester"+active_trimester).find(".days_detail.active").find('.gen_exam_temperature').val(data.data.vitals.temp ? data.data.vitals.temp : '');
            }
        },
        complete: function () {

        }
    });
}

function fetchObstetricsHistory(){
    if($(".days_detail.active").find('.obstetricsHistoryContainer').find('.popover').css('display') == 'block'){
        $(".days_detail.active").find('.obstetricsHistoryContainer').find('.popover').hide();
        $(".days_detail.active").find('.obstetricsHistoryContainer').find('.popover-content').empty();
    } else {
        var patient_id = $('#patient_id').val();
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/gynecology/fetchObstetricsHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "patient_id="+patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".days_detail.active").find('.fetchObsterticsIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                if (data.status == 1) {

                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                    } else {
                        $(".days_detail.active").find('.obstetricsHistoryContainer').find('.popover-content').html(data.history);
                        $(".days_detail.active").find('.obstetricsHistoryContainer').find('.popover').show();

                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }


                }
            },
            complete: function () {
                $(".days_detail.active").find('.fetchObsterticsIcon').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-list');
            }
        });
    }

}


function fetchGeneralExaminationHistory(){
    if($(".days_detail.active").find('.generalExaminationHistoryContainer').find('.popover').css('display') == 'block'){
        $(".days_detail.active").find('.generalExaminationHistoryContainer').find('.popover').hide();
        $(".days_detail.active").find('.generalExaminationHistoryContainer').find('.popover-content').empty();
    } else {
        var patient_id = $('#patient_id').val();
        let _token = $('#c_token').val();
        var url = $('#base_url').val() + "/gynecology/fetchGeneralExaminationHistory";
        $.ajax({
            type: "POST",
            url: url,
            async:true,
            data: "patient_id="+patient_id+"&_token="+_token,
            beforeSend: function () {
                $(".days_detail.active").find('.fetchGeneralExaminationIcon').removeClass('fa-list').addClass('fa-spinner').addClass('fa-spin');
            },
            success: function (data) {
                console.log(data)
                if (data.status == 1) {

                    if(data.count == 0){
                        Command: toastr["info"]('No records found');
                    } else {
                        $(".days_detail.active").find('.generalExaminationHistoryContainer').find('.popover-content').html(data.history);
                        $(".days_detail.active").find('.generalExaminationHistoryContainer').find('.popover').show();

                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }
                }
            },
            complete: function () {
                $(".days_detail.active").find('.fetchGeneralExaminationIcon').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-list');
            }
        });
    }

}

function clickToTop(){
    window.scrollTo(0,0);
}


function takeNextAppointment() {
    var next_review_date = $('.next_review_date').val();
    var url = $('#base_url').val() + "/emr/selectDrAppointments";
    var visit_id = $("#visit_id").val();

    if (next_review_date) {
        $.ajax({
            type: "GET",
            url: url,
            data: 'visit_id=' + visit_id + '&next_review_date=' + next_review_date,
            beforeSend: function () {
                $('#appointment_modal').modal('show');
                $('#next_appointment_list_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#next_appointment_list_data').html(data);
                $('#next_appointment_list_data').LoadingOverlay("hide");
            },
            complete: function () {
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });


            }
        });
    }


}

