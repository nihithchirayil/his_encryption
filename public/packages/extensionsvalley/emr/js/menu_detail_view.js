$(document).on('ready', function(){
    $('.select2').select2({
        dropdownParent: $('#change_menu_parent_modal')
    });
    
    fetchAllMenuList();
});

$(document).on('click', '.moveMenuBtn', function(){
    $(".copy_menu_permission").hide();
    $("#change_menu_parent_modal").modal('show');
    $("#change_menu_parent_modal").attr('data-action-type', 'move');
});

$(document).on('click', '.copyMenuBtn', function(){
    $("#change_menu_parent_modal").modal('show');
    $("#change_menu_parent_modal").attr('data-action-type', 'copy');
    $(".copy_menu_permission").show();
});

$(document).on('click', '.select_check', function(){
    var parent_id = $(this).attr('data-menuid');
    if($(this).prop("checked")){
        $("input[data-parentid='"+parent_id+"']").prop('checked', true);
        $("#parient_"+parent_id).find('.select_check').prop('checked', true);
    } else {
        $("input[data-parentid='"+parent_id+"']").prop('checked', false);
        $("#parient_"+parent_id).find('.select_check').prop('checked', false);
    }
    
});

$(document).on('click', '.saveMenuChangesBtn', function(){
    let url = $('#base_url').val();
    let _token = $('#c_token').val();

    var action_type = $("#change_menu_parent_modal").attr('data-action-type');
    var menu_ids = [];
    $(".select_check:checked").each(function(key, val){
        var menu_id = $(val).attr('data-menuid');
        menu_ids.push(menu_id);
    })

    var parent_id = $(".parent_id_select").val();
    var access_control_status = $(".access_control_status").prop('checked') ? "on" : "off";
    var parent_menu_status = $(".parent_menu_status").prop('checked') ? "on" : "off";

    var data = {};
    data.action_type = action_type;
    data.menu_ids = menu_ids;
    data.parent_id = parent_id;
    data.access_control_status = access_control_status;
    data.parent_menu_status = parent_menu_status;
    data._token = _token;

    $.ajax({
        type: "POST",
        url: url + "/master/saveMenuDetail",
        data: data,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (data) {
            if (data.status == 1) {
                $("#change_menu_parent_modal").modal('hide');
                $(".parent_id_select").select2('destroy'); 
                $(".parent_id_select").val(0);
                $('.parent_id_select').select2({
                    dropdownParent: $('#change_menu_parent_modal')
                });
                $(".access_control_status").prop('cheked', true);
                fetchAllMenuList();
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });

});

function fetchAllMenuList() {

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + "/master/fetchAllMenuList",
        data: {
            _token: _token
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        },
        success: function (data) {
            if (data) {
                $("#aclListBody").html(data);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        }
    });

}

function menudrop(id,action) {
    $('#' + action + '_' + id).hide();
    if(action == 'plus') {
        $('#minus_' + id).show();
        $('#parient_' + id).slideDown();
    } else {
        $('#plus_' + id).show();
        $('#parient_' + id).slideUp();
    }
}