$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }

        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function DialysisReportTrustData() {

    var url = route_json.DialysisReportTrustData;

    //-------filters---------------------------------------------

    var from_date = $('#date_from').val();
    var to_date = $('#date_to').val();
    var summary = 0;
    if ($("#summary").is(':checked') == true) {
        $("#summary_hid").val("1");
        summary = 1;
    } else {
        summary = 0;
        $("#summary_hid").val("0");
    }
    var parm = { from_date: from_date, to_date: to_date, summary_hid: summary };
    $.ajax({
        type: "GET",
        url: url,
        data: parm,
        beforeSend: function () {
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },

        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;

        }
    });

}


// function RestOfBillsDetailCollectionData() {

//     var url = route_json.RestOfBillsCollectionDetailData;

//     //-------filters---------------------------------------------

//     var from_date = $('#date_from').val();
//     var to_date = $('#date_to').val();
//     var depatment_list = $('#depatment_list').val();


//     var parm = { from_date: from_date, to_date: to_date, depatment_list: depatment_list };


//     $.ajax({
//         type: "GET",
//         url: url,
//         data: parm,
//         beforeSend: function () {
//             $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

//         },
//         success: function (html) {
//             console.log(html);
//             $('#ResultsViewArea').html(html);

//             $('#print_results').removeClass('disabled');

//             $('#csv_results').removeClass('disabled');


//         },

//         complete: function () {
//             $('#ResultsViewArea').LoadingOverlay("hide");
//             $('#ResultDataContainer').css('display', 'block');

//             $('.theadscroll').perfectScrollbar({
//                 wheelPropagation: true,
//                 minScrollbarLength: 30

//             });
//             // setTimeout(function() {
//             //     $('.theadfix_wrapper').floatThead({
//             //         position: 'absolute',
//             //         scrollContainer: true
//             //     });
//             // }, 400);
//         },
//         error: function () {
//             Command: toastr["error"]("Network Error!");
//             $('#ResultDataContainer').css('display', 'hide');
//             return;

//         }
//     });


// function datarst() {

// var current_date = $('#current_date').val();

// $("#depatment_list").select2('val', '');
// $('#date_from').val(current_date);

// }
