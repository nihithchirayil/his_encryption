var base_url = $('#base_url').val();
$(document).ready(function () {

    setTimeout(function () {
        // clearselect2();
        $(".select2").select2({
            placeholder: "",
            maximumSelectionSize: 10
        });
    }, 2000);

    //fixed header script ends here
    setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
    }, 300);
    //$(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $("input[data-attr='date']").datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $("input[data-attr='datetime']").datetimepicker({
        format: 'MMM-DD-YYYY hh:mm:A'
    });
});
$("#consolidated").click(function () {
    if($("#consolidated").prop('checked') == true){
         consolidated  = 1;
         $('#item_desc_search').attr("disabled",true);
         $('#item_desc_search').val('');
         $('#item_id').val('');

     }else{
         consolidated = 0;
         $('#item_desc_search').attr("disabled",false);
     }

});
$('#patient_uhid').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('patient_uhid_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if ($('#patient_uhid_hidden').val() != "") {
            $('#patient_uhid_hidden').val('');
            $("#patient_uhid_AjaxDiv").hide();
        }
        var patient_uhid = $(this).val();
        if (patient_uhid == "") {
            $("#patient_uhid_AjaxDiv").html("").hide();
        } else {
            var url = $('#base_url').val() + "/flexy_report/ajax_consulting_doctor";

            $.ajax({
                type: "GET",
                url: url,
                data: "patient_uhid=" + patient_uhid,
                beforeSend: function () {
                    $("#patient_uhid_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#patient_uhid_AjaxDiv").html(html).show();
                    $("#patient_uhid_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('patient_uhid_AjaxDiv', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function filluhidSearchDetials(id, name) {
    name = htmlDecode(name);
    $('#patient_uhid_hidden').val(id);
    $('#patient_uhid').val(name);
    $('#patient_uhid_AjaxDiv').hide();
}
$('#item_desc_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('itemDescSearchBox');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var item_desc = $('#item_desc_search').val();
        if (item_desc == "") {
            $('#itemDescSearchBox').hide();
            $("#item_id").val('');
        } else {
            var url = base_url + "/purchase/searchItemDesc";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    item_desc: item_desc
                },
                beforeSend: function () {
                    $("#itemDescSearchBox").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#itemDescSearchBox").html(html).show();
                    $("#itemDescSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {},
            });
        }
    } else {
        ajax_list_key_down('itemDescSearchBox', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillitem_desc(item_id, item_desc) {
    item_desc = htmlDecode(item_desc);
    $('#item_id').val(item_id);
    $('#item_desc_search').val(item_desc);
    $('#itemDescSearchBox').hide();
}

function downloadExcelData() {
    var base_url = $('#base_url').val();
    var url = base_url + '/flexy_report/LocationwisepharmacysalesdetailreportDataExcel';
    var paid_bills_only = $('#paid_bills_only').is(":checked");
    var unpaid_bills_only = $('#unpaid_bills_only').is(":checked");
    var settled_bills_only = $('#settled_bills_only').is(":checked");
    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';


    $('.filters').each(function () {
        filters_id = this.id;

        filters_value = $('#' + filters_id).val();
        if (Array.isArray(filters_value)) {

            var filter_aarry = jQuery.grep(filters_value, function (n, i) {
                return (n !== "" && n != null);
            });


            if (filter_aarry.length == 0) {
                filters_value = '';
            }

            if (filter_aarry.find(function (element) {
                    return element == 'All';
                })) {
                filters_value = '';
            }
        }
        filters_id = filters_id.replace('_hidden', '');

        if (filters_value && filters_value !== "" && filters_value !== "All" && filters_value !== [] &&
            filters_value !== null && filters_value != 'All') {
            filters_list[filters_id] = filters_value;
        }
    });
    filters_list['paid_bills_only'] = paid_bills_only;
    filters_list['unpaid_bills_only'] = unpaid_bills_only;
    filters_list['settled_bills_only'] = settled_bills_only;
    $.ajax({
        url: url,
        type: 'POST',
        data: filters_list,
        beforeSend: function () {
            $('#csvResultsBtn').attr('disabled', true);
            $('#csvResultsSpin').removeClass('fa fa-file-excel-o');
            $('#csvResultsSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                toastr.success("Successfully queued in generation");
            }
        },
        complete: function () {
            $('#csvResultsBtn').attr('disabled', false);
            $('#csvResultsSpin').removeClass('fa fa-spinner fa-spin');
            $('#csvResultsSpin').addClass('fa fa-file-excel-o');
        }
    });
}



function generatedExcelData(report_name, from_type) {
    var base_url = $('#base_url').val();
    var url = base_url + '/flexy_report/generatedReportExcelData';
    var param = {
        report_name: report_name
    }
    $.ajax({
        url: url,
        type: 'POST',
        data: param,
        beforeSend: function () {
            $('#generatedExcelDataModelDiv').html("Please Wait!!!!!!!!!");
            $('#generatedExcelDataBtn').attr('disabled', true);
            $('#generatedExcelDataSpin').removeClass('fa fa-list');
            $('#generatedExcelDataSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#generatedExcelDataModelDiv').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            if (parseInt(from_type) == 1) {
                $("#generatedExcelDataModel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }



        },
        complete: function () {
            $('#generatedExcelDataBtn').attr('disabled', false);
            $('#generatedExcelDataSpin').removeClass('fa fa-spinner fa-spin');
            $('#generatedExcelDataSpin').addClass('fa fa-list');
        }
    });
}




function getResultData() {
    var base_url = $('#base_url').val();
    var url = base_url + '/flexy_report/LocationwisepharmacysalesdetailreportData';
    var paid_bills_only = $('#paid_bills_only').is(":checked");
    var unpaid_bills_only = $('#unpaid_bills_only').is(":checked");
    var settled_bills_only = $('#settled_bills_only').is(":checked");
    var doctor_name_search = $('#doctor_name_search').val();
    var visit_status = $('#visit_type').val();
    var schedule = $('#schedule').val();
    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';
    var item_select = $('#item_select').val();

    $('.filters').each(function () {
        filters_id = this.id;

        filters_value = $('#' + filters_id).val();
        if (Array.isArray(filters_value)) {

            var filter_aarry = jQuery.grep(filters_value, function (n, i) {
                return (n !== "" && n != null);
            });


            if (filter_aarry.length == 0) {
                filters_value = '';
            }

            if (filter_aarry.find(function (element) {
                    return element == 'All';
                })) {
                filters_value = '';
            }
        }
        filters_id = filters_id.replace('_hidden', '');

        if (filters_value && filters_value !== "" && filters_value !== "All" && filters_value !== [] &&
            filters_value !== null && filters_value != 'All') {
            filters_list[filters_id] = filters_value;
        }
    });
    filters_list['paid_bills_only'] = paid_bills_only;
    filters_list['unpaid_bills_only'] = unpaid_bills_only;
    filters_list['settled_bills_only'] = settled_bills_only;
    filters_list['doctor_name_search'] = doctor_name_search;
    filters_list['visit_status'] = visit_status;
    filters_list['schedule'] = schedule;
    filters_list['item_id'] = item_select;
    var consolidated =0;
    if($("#consolidated").prop('checked') == true){
        consolidated  = 1;

    }else{
        consolidated = 0;
    }
    filters_list['consolidated'] = consolidated;
    $.ajax({
        url: url,
        type: 'POST',
        data: filters_list,
        beforeSend: function () {
            $('#ResultDataContainer').show();

            $('#ResultDataContainer').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('.ResultDataContainerBody').show();
            $('#ResultsViewArea').html(data);

        },
        complete: function () {
            $('#ResultDataContainer').LoadingOverlay("hide");

            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        }
    });
}

//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var base_url = $('#base_url').val();
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
            $("#" + input_id + "AjaxDiv").hide();

        } else {
            var url = base_url + '/flexy/FlexyReportProgressiveSearch';
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html(
                        '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {

                    $("#" + input_id + "AjaxDiv").html(html).show();
                    $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {

                },
                error: function () {
                    bootbox.alert('nw error');
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}


/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});


$("#category").on('change', function () {
    var category = $('#category').val();
    if (category == '') {
        var category = 'a';

    }

    if (category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#s2id_sub_category').append(
                    '<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>'
                );
                $("#sub_category").prop("disabled", true); //Disable
            },
            success: function (html) {
                if (html) {
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub catogery</option>');
                    $.each(html, function (key, value) {
                        $("#sub_category").append('<option value="' + key + '">' +
                            value + '</option>');
                    });
                } else {
                    $("#sub_category").empty();
                }
            },

            error: function () {
                Command: toastr["error"](
                    "Please check your internet connection and try again!");
                $("#sub_category").prop("disabled", false);
            },
            complete: function () {
                $('#warning1').remove();
                $("#sub_category").prop("disabled", false);
            }

        });

    } else {
        $("#category").focus();
        $("#sub_category").empty();
    }

});


//---------generate csv---------------
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {
        type: "text/csv"
    });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function printReportData() {
    $('#print_config_modal').modal('toggle');
}

function print_generate(printData) {

    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');
    $('#total_row').css('display', '');
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;



    if (printMode == 1) {
        mywindow.document.write(
            '<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>'
        );
    } else {
        mywindow.document.write(
            '<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>'
        );
    }

    mywindow.document.write(
        '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
    );
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
    );
    mywindow.document.write(
        '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
    );
    mywindow.document.write(
        '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
    );

    mywindow.document.write(showw);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
        InitTheadScroll();
        $('#total_row').css('display', 'none');
    }, 1000);


    return true;
}

function InitTheadScroll() {
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });
}



$("button[type='reset']").on('click', function (evt) {
    $(".select2").val([]).select2();
    $(".filters").val('');
    $('#ResultsViewArea').html('');
    $("body").LoadingOverlay("show", {
        background: "rgba(255, 255, 255, 0.7)",
        imageColor: '#337AB7'
    });
    window.location.reload();

});

function clearselect2() {
    $('select').select2({
        placeholder: '',
        allowClear: true,
    });
}

function exceller_template_without_header(excel_name) {
    var template_date = $('#exceller_templatedata').val();
    var uri = 'data:application/vnd.ms  -excel;base64,',
        template = atob(template_date),
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    var toExcel = document.getElementById("result_data_table").innerHTML;

    var ctx = {
        worksheet: name || '',
        table: toExcel
    };
    var link = document.createElement("a");
    link.download = excel_name + ".xls";
    link.href = uri + base64(format(template, ctx))
    link.click();
}
$(document).on('change', '#paid_bills_only', function () {
    if ($(this).is(':checked')) {
        $('#unpaid_bills_only').prop('checked', false);
        $('#settled_bills_only').prop('checked', false);
    }
})
$(document).on('change', '#unpaid_bills_only', function () {
    if ($(this).is(':checked')) {
        $('#paid_bills_only').prop('checked', false);
        $('#settled_bills_only').prop('checked', false);
    }
})
$(document).on('change', '#settled_bills_only', function () {
    if ($(this).is(':checked')) {
        $('#paid_bills_only').prop('checked', false);
        $('#unpaid_bills_only').prop('checked', false);
    }
})
