function manageDocs(patient_id,doctor_id,encounter_id,visit_id) {
    $('#manageDocsForm')[0].reset();
    $('.uploadedImg').html('');
    $('#visit_id').val(visit_id);
    $('#patient_id').val(patient_id);
    $('#encounter_id').val(encounter_id);
    $('#doctor_id').val(doctor_id);
    $('#manageDocs').modal('show');
    loadUploadImages();
}
var k = 0;
function add_new_row_files(){
    let docType = "";
    let text = "";
    let value = "";
    $('select[name="document_type_hidden"] option').each(function() {
        value = $(this).val();
        text = $(this).text();
        docType += '<option value="'+value+'">'+text+'</option>';
    });
    var documentTypeSelect = '<select class="form-control" required="required" name="document_type[]">\n\
    '+docType+'</select>';
    var title = '<input type="text" name="title[]" class="form-control" id="description" placeholder="Title">';
    var description = '<input type="text" name="description[]" class="form-control" id="description" placeholder="Description">';
    $("#roww").append("<div  class='remove_"+k+" col-sm-12 no-padding'>\n\
    <div class='col-md-3 padding_sm'>\n\
    <input class='inputfile inputfile-1 hidden' id='file_upload" + k + "' data-multiple-caption='{count} files selected'  data-show-upload='false' name='file_upload[]' type='file' >\n\
    <label class='no-margin btn-block' for='file_upload" + k + "'><i class='ion ion-ios-folder' style='font-size: 18px;'></i> <span>Choose a file ...</span></label>\n\
    </div>\n\
    <div class='col-md-2 padding_sm'>\n\
    "+documentTypeSelect+"\
    </div>\n\
    <div class='col-md-3 padding_sm'>\n\
    "+description+"\
    </div>\n\
    <div class='col-md-2 padding_sm'>\n\
    <button class='btn btn-danger btn-block' onclick='remove_upload_file("+k+");' type='button' ><i class='fa fa-trash-o'></i>" +" Clear"+ "</button>\n\
    </div>\n\
    </div>\n\
    ")
    k++;
}
function remove_upload_file(k){
    if(k == 'default')
    {
        let options = "";
        let text = "";
        let value = "";
        $('select[name="document_type_hidden"] option').each(function() {
            value = $(this).val();
            text = $(this).text();
            options += '<option value="'+value+'">'+text+'</option>';
        });
      $(".remove_00").replaceWith('<div class="remove_00 col-sm-12 no-padding"><div class="col-md-3 padding_sm"><input type="file" name="file_upload[]" id="f_upload" class="inputfile inputfile-1 hidden" data-show-upload="false" data-multiple-caption="{count} files selected"><label class="no-margin btn-block" for="f_upload"><i class="ion ion-ios-folder" style="font-size: 18px;"></i> <span id="uploadspan_documentsdocumenttab">Choose a file ...</span></label><span style="color: #d14;"></span></div><div class="col-md-2 padding_sm"><select name="document_type[]" required="required" class="form-control">'+options+'</select></div><div class="col-md-2 padding_sm"><input type="text" placeholder="Title" id="description" class="form-control" name="title[]"></div><div class="col-md-3 padding_sm"><input type="text" placeholder="Description" id="description" class="form-control" name="description[]"></div><div class="col-md-2 padding_xs"><input type="hidden" name="patient_id" id="patient_id" value="{{$pid}}"><input type="hidden" name="encounter_id" id="encounter_id" value="{{$encounter_id}}"><!--  <button class="btn btn-success btn-block" type="submit" id="doc_file_upload"><i class="fa fa-upload"></i> Upload</button> --></div><div class="col-md-2 padding_sm"><button class="btn btn-danger btn-block" onclick="remove_upload_file(\''+k+'\');" type=button"><i class="fa fa-trash-o"></i> Remove</button></div></div>');
    }
    else
    {  $('.remove_'+k).remove();}
}

function clear_upload_file(){
    $(".document_type_upload").val('');
    $(".description_upload").val('');
    $('#manageDocsForm')[0].reset();
    // $('#uploadspan_documentsdocumenttab').html('Choose a file ...');
    $('.uploadedImg').hide();
}

/*upload image*/
$("#manageDocsForm").on('submit', (function (value) {

    value.preventDefault();
    var upload_cnt = 0;
    var type_cnt = 0;
    if( $('.document_type_upload').val()==''){

        toastr.warning('Please select type.');
        return;
    }
    if( $('.description_upload').val().trim()==''){
        toastr.warning('Please add description.');
        return;
    }
    if( $('.uploadedImg').html().trim()==''){
        toastr.warning('Please add file.');
        return;
    }
        $("#doc_file_upload").attr("disabled","disabled");
        var url = $('#base_url').val() + "/master/patientDocumentUpload";
        var param = new FormData(this);
        var visit_id = $("#visit_id").val();
        var patient_id = $("#patient_id").val();
        var encounter_id = $("#encounter_id").val();
        var doctor_id = $("#doctor_id").val();
        param.append('visit_id', visit_id);
        param.append('patient_id', patient_id);
        param.append('encounter_id', encounter_id);
        param.append('doctor_id', doctor_id);
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            data: param,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                 $('#upload_file_doc').removeClass('fa fa-upload');
                 $('#upload_file_doc').addClass('fa fa-spin fa-spinner');
            },
            success: function (data) {
                $("#doc_file_upload").attr("enabled","enabled");
                if(data.status == 1){
                    Command: toastr["success"]('Saved Successfully');
                    $('#manageDocsForm')[0].reset();
                    $('.uploadedImg').html('');
                    $('#roww').html('');
                    loadUploadImages();
                    // $('#uploadspan_documentsdocumenttab').html('Choose a file ...');
                    $('.uploadedImg').hide();

                }else{
                    Command: toastr["error"]('File upload failed. Please upload file less than 10 MB');
                }
            },
            error: function () {
                $("#doc_file_upload").attr("enabled","enabled");
                Command: toastr["warning"]('Error.!');
            },
            complete: function () {
                $("#doc_file_upload").removeAttr("disabled");
                $('#upload_file_doc').removeClass('fa fa-spin fa-spinner');
                $('#upload_file_doc').addClass('fa fa-upload');
            }
        });

}));


//load history
function loadUploadImages(page) {
    $('#document-data-list').html('');
    var url = $('#base_url').val() + "/master/loadDocumentList";
    var patient_id = $('#patient_id').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            document_list: 1,
            page: page,
            patient_id: patient_id,
            _token: _token,
        },
        beforeSend: function () {
            $('#document-data-list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                $('#document-data-list').html(data);
            }
        },
        complete: function () {
            $('#document-data-list').LoadingOverlay('hide');
        }
    });
}

//documen pagination
$(document).on('click', '.document_pagination .pagination li a', function (event) {
    event.preventDefault();
    $('li').removeClass('active');
    $(this).parent('li').addClass('active');
    var page = $(this).attr('href').split('page=')[1];
    loadUploadImages(page);
});

function docImgDelete(docID,e){
    if(docID == 0){
      Command: toastr["error"]("You don't have access to remove the document!");
      return false;

    }
    bootbox.confirm({
        title: 'Delete Document',
        size: 'small',
        message: 'Are you sure, you want to delete this document?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                style:'margin-left: 5px;'
            },
            'confirm': {
                label: 'Yes',
            }
        },

        callback: function(result) {
            if (result) {
                var url = $('#base_url').val() + "/master/deleteDocument";
                $.ajax({
                url: url,
                type: "GET",
                async: true,
                data: 'doc_id=' + docID,
                beforeSend: function(){
                    $('#spin_trash'+docID).removeClass('fa fa-trash');
                    $('#spin_trash'+docID).addClass('fa  fa-spin fa-spinner');
                },
                success: function (data) {
                    if(data == 1){
                        Command: toastr["success"]('Document Deleted.');
                        $(e).parent('td').parent('tr').remove();
                    }else{
                        Command: toastr["error"]('Error.!');
                    }
                },
                complete: function () {
                    $('#spin_trash'+docID).addClass('fa fa-trash');
                    $('#spin_trash'+docID).removeClass('fa  fa-spin fa-spinner');
                },
                error: function () {
                    Command: toastr["warning"]('Error.!');
                }
                });
            }
        }
    });
}
function docImgPreview(docID,storage_type='server'){
    if(docID == 0 || docID == undefined){
      Command: toastr["error"]("Error.!");
      return false;
    }
    var url = $('#base_url').val() + "/emr/preview-document";
    var id = docID;
    $.ajax({
        type: "GET",
        url: url,
        data: "id=" +id+'&storage_type='+storage_type,
        beforeSend: function () {
            if(storage_type != 'local'){
                $('#view_document_modal').modal('show');
                $('#document_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            }
        },
        success: function (response) {
            if(storage_type == 'local'){
                window.open(response, '_blank');
            }else{
                $('#myframe').attr("src",response);
            }
        },
        complete: function () {
            if(storage_type != 'local'){
                $('#document_data').LoadingOverlay("hide");
            }
        }
    });
}

$(document).on("change", '.document_upload', function(event){
    var filename = event.currentTarget.files[0].name;
    // $('#uploadspan_documentsdocumenttab').html(filename);
    $('.uploadedImg').attr('href', URL.createObjectURL(event.currentTarget.files[0]));
    $('.uploadedImg').html(filename);
    $('.uploadedImg').show();
});

