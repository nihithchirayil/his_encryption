$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

    //-----------Bill No search------------
    $('#item_desc').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            if ($('#item_desc_hidden').val() != "") {
                $('#item_desc_hidden').val('');
                $("#item_desc_AjaxDiv").hide();
            }
            var item_desc = $(this).val();
            if (item_desc == "") {
                $("#item_desc_AjaxDiv").html("").hide();
            } else {
                var url = '';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "item_desc=" + item_desc,
                    beforeSend: function () {
                        $("#item_desc_AjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#item_desc_AjaxDiv").html(html).show();
                        $("#item_desc_AjaxDiv").find('li').first().addClass('liHover');
                    }
                });
            }

        }
    });

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

function fillitem_desc(item_code, item_desc) {
    $('#item_desc_hidden').val(item_code);
    $('#item_desc').val(item_desc);
    $('#item_desc_AjaxDiv').hide();
}


function getReportData() {
    var base_url = $('#base_url').val();
    var url = base_url + "/reports/getItemStockTaxWise";
    var from_date = $("#fromdate").val();
    var item_desc = $('#item_desc_hidden').val();
    var location = $('#location').val();
    var show_location = $('#show_loc').is(':checked') ? 1 : 0;
    var general_store = $('#gen_str').is(':checked') ? 1 : 0;
    var param = { from_date: from_date, item_desc: item_desc, location: location,show_location:show_location,general_store:general_store };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#search_results').attr('disabled', true);
            $('#serachResultSpin').removeClass('fa fa-search');
            $('#serachResultSpin').addClass('fa fa-spinner');
            $('#ResultDataContainer').css('display', 'block');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#search_results').attr('disabled', false);
            $('#serachResultSpin').removeClass('fa fa-spinner');
            $('#serachResultSpin').addClass('fa fa-search');
            $('#ResultsViewArea').LoadingOverlay("hide");

        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }
    });
}

function search_clear() {

    var current_date = $('#current_date').val();

    $('#fromdate').val(current_date);
    $('#todate').val(current_date);
    $('#item_desc_hidden').val('');
    $('#item_desc').val('');
    $('#item_desc_AjaxDiv').hide();
    $('#location').val('');
    $('#location').select2();
}
