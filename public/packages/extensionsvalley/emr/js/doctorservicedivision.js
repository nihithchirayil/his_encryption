


$(document).on('click', '.delete-doctor-service', function () { 
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="dctr_service_id[]"]').val();

        if (edit_id != '' && edit_id != 0) {
            deleteServiceRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});

//delete single row from db
function deleteServiceRowFromDb(id) {  
    var url = $('#base_url').val() + "/doctorservice/delete-doctor-service";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {
             $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {
             $("body").LoadingOverlay("hide");
        }
    });
}


function deleteServiceRow(id) {  
    var url = $('#base_url').val() + "/doctorservice/delete-doctor-service";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {
           
        },
        success: function (data) {
          
        },
        complete: function () {
           
        }
    });
}



$(document).on('click', '.edit-doctor-service', function () { 

         $('#doctor_service_division_modal').modal('show');
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="dctr_service_id[]"]').val();
        let fav_service_desc =  $(tr).find('input[name="fav_service_desc[]"]').val();
        let fav_percentage =  $(tr).find('input[name="fav_percentage[]"]').val();
        let fav_doctor_name =  $(tr).find('input[name="fav_doctor_name[]"]').val();
    
        if (fav_doctor_name != '' && edit_id != '') {
           
           $("#confirmed_doctor option[value="+ fav_doctor_name +"]").attr('selected', 'selected');
           $("#service option[value="+ fav_service_desc +"]").attr('selected', 'selected');
           $('input[name="percentage"]').val(fav_percentage);
            $('input[name="edit_dctr_service"]').val(edit_id);
          
        } 
});


function addDctrService() {
    let edit_dctr_service = $("#edit_dctr_service").val();
    let confirmed_doctor = $("#confirmed_doctor").val();
    let service = $("#service").val();
    let percentage = $("#percentage").val();
   
  

        if (!(percentage >= 48 || percentage <= 57)) {
             toastr.warning("Only Numbers Allowed.");
          }
          else if(confirmed_doctor == 0 || service == 0|| percentage == ''){
            toastr.warning("Please Fill All Details.");
          }
  
        else{

              if (edit_dctr_service != '' && edit_dctr_service != 0) {
            deleteServiceRow(edit_dctr_service);
        }
        var url =  $('#base_url').val() + "/doctorservice/save-doctor-service";
        $.ajax({
            type: "GET",
            url: url,
            data: 'confirmed_doctor=' + confirmed_doctor + '&service=' + service + '&percentage=' + percentage ,
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function(response) { 
                if (response.status == 1) {
                  
                    Command: toastr["success"]("Saved.");
                     window.location.href = $('#base_url').val() + "/doctorservice/doctor-service-division/";
               
                } else {
                  
                    Command: toastr["error"]("Error.");
                }
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }
    
}


$("#doctor_id").on('change', function() {
    var doctor_id = $("#doctor_id").val();
   
    if (doctor_id) {
        var url = $('#base_url').val() + "/doctorservice/get-doctor-service";
        var param = { doctor_id: doctor_id};
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
               
                $('#doctorservicedatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function(data) {
                $('#doctorservicedatadiv').html(data);
                setTimeout(function() {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function() {
               
                $('#doctorservicedatadiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } 
 });


$("#service_id").on('change', function() {
    var service_id = $("#service_id").val();
   
    if (service_id) {
        var url = $('#base_url').val() + "/doctorservice/get-doctor-service";
        var param = { service_id: service_id};
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
               
                $('#doctorservicedatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function(data) {
                $('#doctorservicedatadiv').html(data);
                setTimeout(function() {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function() {
               
                $('#doctorservicedatadiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } 
 });





