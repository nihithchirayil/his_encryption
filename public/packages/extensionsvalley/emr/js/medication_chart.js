$(document).ready(function () {
    var current_visit_id = $('#visit_id').val();
    if (current_visit_id != 0) {
        getMedicationReport();
    }
    $('[data-toggle="tooltip"]').tooltip();
});

var add_med = 0;
function addMedicineGiven(medication_detail_id, frequency_value, frequency = 0) {
    if(add_med == 0){
        add_med = 1;
        var url = $('#base_url').val() + "/nursing/addMedicineGiven";
        var visit_id = $('#visit_id').val();
        var patient_id = $('#patient_id').val();
        var checkStatus = 0;
        var giventime = '';
        var expected_time = '';


        if ($('#check_' + medication_detail_id).is(':checked')) {
            checkStatus = 1;
        } else {
            checkStatus = 0;
        }

        givendate = $('#given_date_' + medication_detail_id).val();
        giventime = $('#given_time_' + medication_detail_id).val();
        $('#btn_medicine_given_' + medication_detail_id).attr('disabled', true);
            $.ajax({
                type: "POST",
                url: url,
                async:true,
                data: 'medication_detail_id=' + medication_detail_id +
                    '&visit_id=' + visit_id +
                    '&patient_id=' + patient_id +
                    '&given_time=' + giventime +
                    '&given_date=' + givendate +
                    '&expected_time=' + expected_time +
                    '&frequency_value=' + frequency_value +
                    '&checkStatus=' + checkStatus,
                beforeSend: function () {
                    $('#medication_given_container_' + medication_detail_id).LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });


                    $('#fa-medicine_given_' + medication_detail_id).removeClass('fa-medkit');
                    $('#fa-medicine_given_' + medication_detail_id).addClass('fa-spinner fa-spin');


                },
                success: function (data) {

                    $('#medication_given_container_' + medication_detail_id).LoadingOverlay("hide");
                    $('#fa-medicine_given_' + medication_detail_id).removeClass('fa-spinner fa-spin');
                    $('#fa-medicine_given_' + medication_detail_id).addClass('fa-medkit');
                    //console.log(data);
                    if (data == 0) {
                        Command: toastr["error"]("Error.! Something went wrong!.");
                    }
                    else if (data == 3) {
                        Command: toastr["warning"]("Course completed !");
                    }
                    else {
                        var obj = JSON.parse(data);

                        $('#given_time_' + medication_detail_id).css('color', '#a8a8a8 !important');
                        $('#given_date_' + medication_detail_id).css('color', '#a8a8a8 !important');

                        $('#given_time_' + medication_detail_id).val(obj.increment_time);
                        $('#given_date_' + medication_detail_id).val(obj.increment_date);

                        if (frequency != 'STAT') {
                            Command: toastr["info"]("Note ! Current Medication is given at " + obj.inserted_time + ".\n Next course should be given at " + obj.next_expected_time);
                        }

                        $('#medication_given_container_' + medication_detail_id).append(obj.medicine_given_view);

                        if (frequency == 'STAT') {
                            stopMedicine(medication_detail_id);
                        }
                    }
                },
                complete: function () {
                    $('#btn_medicine_given_' + medication_detail_id).attr('disabled', false);
                    add_med = 0;
                }
            });
    }





}

function updateMedicineGiven(course_id, frequency_value, medication_detail_id, timerstatus) {
    var url = $('#base_url').val() + "/nursing/updateMedicineGiven";
    var visit_id = $('#visit_id').val();
    var patient_id = $('#patient_id').val();
    var checkStatus = 0;
    var giventime = '';
    var expected_time = '';
    if ($('#' + course_id).is(':checked')) {
        checkStatus = 1;
    } else {
        checkStatus = 0;
    }
    //alert(checkStatus); return;
    entryArray = course_id.split("_");
    var medicine_approved_id = entryArray[0];
    var course_id_val = entryArray[1];

    if (timerstatus == 1) {
        giventime = $('#timepicker_' + course_id).val();
        checkStatus = 1;
        //alert(giventime);
    }
    expected_time = $('#timepicker_' + course_id).attr("data-expected");

    $.ajax({
        type: "GET",
        url: url,
        data: 'medicine_approved_id=' + medicine_approved_id +
            '&course_id=' + course_id_val +
            '&frequency_value=' + frequency_value +
            '&visit_id=' + visit_id +
            '&patient_id=' + patient_id +
            '&medication_detail_id=' + medication_detail_id +
            '&giventime=' + giventime +
            '&expected_time=' + expected_time +
            '&checkStatus=' + checkStatus,
        beforeSend: function () {
            $('#timepicker_' + course_id).addClass('loadinggif');
        },
        success: function (data) {
            console.log(data);
            if (data == 0) {
                Command: toastr["error"]("Error.! Something went wrong!.");
            }
            else if (data == 2) {
                $('#timepicker_' + course_id).css('background-color', 'white');
                $('#timepicker_' + course_id).val('');
            } else {
                var obj = JSON.parse(data);
                var course = course_id[course_id.length - 1];
                course = parseInt(course) + 1;
                var next_course_id = course_id.slice(0, -1) + course;

                $('#timepicker_' + course_id).val(obj.inserted_time);
                $('#timepicker_' + course_id).css('background-color', '#7AECD0');
                $('#timepicker_' + course_id).removeClass('timepickerclass_default');

                //-----next couse time assigning---------
                $('#timepicker_' + next_course_id).val(obj.increment_sloat_time);
                $('#timepicker_' + next_course_id).addClass('timepickerclass_default');

                Command: toastr["info"]("Note ! Next course should be given at " + obj.increment_sloat_time);

            }
        },
        complete: function () {

        }
    });
}

function saveIvChart() {
    var dataparams = $("#frm-ivchart").serialize();
    var user_id = $('#user_id').val();
    $.ajax({
        type: "POST",
        url: $('#base_url').val() + "/nursing/saveIvChart",
        data: dataparams + '&user_id=' + user_id,
        async: false,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
            if (data == 0) {
                Command: toastr["error"]("Error! Something went wrong!");
            }
            else {
                Command: toastr["success"]("Saved Successfully!");
                showIvFluidChart();
            }
            $('#iv_fluid_draft_mode').val(0);
        },
    });
}

function IvReorder() {
    Command: toastr["error"]("Error.! Something went wrong!");
}

//----UHID search--------------------------------------------------------------------
$('#op_no').keyup(function (event) {

    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#op_no_hidden').val() != "") {
            $('#op_no_hidden').val('');
        }
        var op_no = $(this).val();
        op_no = op_no.replace("/[^\w\s-_\.]/gi");
        op_no = op_no.trim();
        if (op_no == "") {
            $("#OpAjaxDiv").html("");
        } else {
            var url = '';
            $.ajax({
                type: "GET",
                url: url,
                data: "op_no=" + op_no,
                beforeSend: function () {

                    $("#OpAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {

                    $("#OpAjaxDiv").html(html).show();
                    $("#OpAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                    //  $('#loading_image').hide();
                }
            });
        }

    } else {
        ajax_list_key_down('OpAjaxDiv', event);
    }
});


function fillOpDetials(patient_id, op_id, id, patient_name, current_visit_id) {
    $('#op_no_hidden').val(patient_id);
    $('#op_no').val(op_id);
    $('#label_patient_name').html(patient_name);
    $('#visit_id').val(current_visit_id);
    $('#current_visit_id').val(current_visit_id);
    $('#OpAjaxDiv').hide();
}

/* setting for enter key press in ajaxDiv listing */
$("#op_no").on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('OpAjaxDiv');
        return false;
    }
});

//----------UHID search ends-----------------------------------------------------------

function getMedicationReport() {
    var current_visit_id = $('#current_visit_id').val();
    var from_op_list = $('#is_from_op_list').val();
    if (current_visit_id == '') {
        Command: toastr["warning"]("Please select patient!");
        $('#op_no').focus();
        return;
    }
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/getMedicationReport",
        data: "current_visit_id=" + current_visit_id + "&from_op_list=" + from_op_list,
        beforeSend: function () {
            $('#medication_report_data').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#medication_report_data').LoadingOverlay("hide");
            if (html == 0) {
                // Command: toastr["warning"]("No approved medications found!");
            } else {
                $('#medication_report_data').html(html);
            }
        },
        complete: function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var left = $('.theadscroll').width() * 3;
            $('.theadscroll').scrollLeft(left);
            $('.left_col').find('.theadscroll').scrollLeft(0);
            resetBootstrapTable();
        }
    });
}

function stopMedicine(medicine_detail_id) {

    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/stopMedicine",
        data: "medicine_detail_id=" + medicine_detail_id,
        beforeSend: function () {
            $('body').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            // console.log(html);
            $('body').LoadingOverlay("hide");
            if (html == 1) {
                Command: toastr["success"]("Medication stopped!");
            }
            else {
                Command: toastr["warning"]("Something went wrong!");
            }
        },
        complete: function () {
            showMedicationChart();
        }
    });
}

function showMedicationChart() {
    var visit_id = $('#visit_id').val();
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/showMedicationChart",
        data: "visit_id=" + visit_id,
        beforeSend: function () {
            $('#medication_chart_content').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (html) {
            $('#medication_chart_content').LoadingOverlay("hide");
            $('#medication_chart_content').html(html);
        },
        complete: function () {
            $('.datepicker').datetimepicker({
                format: 'MMMM-DD-YYYY'
            });

            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });

            $("[data-toggle='popover']").popover('toggle');
            $("[data-toggle='popover']").popover('hide');
        }
    });
}



function deleteGivenMedication(medication_chart_id) {
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/deleteGivenMedication",
        data: "medication_chart_id=" + medication_chart_id,
        beforeSend: function () {

            $('#medication_given_container_' + medication_chart_id).LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });

        },
        success: function (html) {
            $('#medication_given_container_' + medication_chart_id).LoadingOverlay("hide");
            if (html == 1) {
                Command: toastr["success"]("Deleted!");
                $("#chart_id_" + medication_chart_id).remove();

            }
            else {
                Command: toastr["warning"]("Something went wrong!");
            }
        },
        complete: function () {

        }
    });
}

function editGivenMedication(medication_chart_id, change_date, change_time) {
    $('#modalChangeMedicationTime').modal('toggle');
    $('#med_chart_change_date').val(change_date);
    $('#med_chart_change_time').val(change_time);
    $('#change_med_chart_id').val(medication_chart_id);
}

function editGivenMedicationSave() {

    var med_chart_change_date = $('#med_chart_change_date').val();
    var med_chart_change_time = $('#med_chart_change_time').val();
    var medication_chart_id = $('#change_med_chart_id').val();

    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/nursing/editGivenMedication",
        data: "medication_chart_id=" + medication_chart_id + '&med_chart_change_date=' + med_chart_change_date + '&med_chart_change_time=' + med_chart_change_time,
        beforeSend: function () {

            $('#modalChangeMedicationTime').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });

        },
        success: function (html) {
            //console.log(html);return;
            $('#modalChangeMedicationTime').LoadingOverlay("hide");
            if (html != 0) {
                Command: toastr["success"]("Updated Successfully!");
                $('#modalChangeMedicationTime').modal('hide');
                //$( "#chart_id_"+medication_chart_id ).remove();
                obj = JSON.parse(html);
                var medication_detail_id = obj.medication_detail_id;
                $("#chart_id_" + medication_chart_id).html('');
                $("#chart_id_" + medication_chart_id).html(obj.medicine_given_view);

            }
            else {
                Command: toastr["warning"]("Something went wrong!");
            }
        },
        complete: function () {

        }
    });
}
