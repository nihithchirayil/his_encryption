$(window).load(function(){
    $("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });
    $("input[data-attr='date']").trigger("focus");
    $(".getReport").trigger("focus");
    $(".report_date").html("Report Date : " + moment().format('DD-MMM-YYYY HH:mm:ss'));
})

$(document).ready(function(){
    


    $(document).on('click', '.getReport', function(event) { 
        getOTPaymentsReport();
    });
    $(document).on('click', '.printReport', function(event) { 
        printReport();
    });
    

    function getOTPaymentsReport(){
        var from_date = $('.from_date').val();
        var to_date = $('.to_date').val();
        var doctor = $('.doctor').val();

        if(moment(from_date).isAfter(to_date)){
            toastr.error("Please check the Date Range !!");
            return;
        }

        let _token = $('#c_token').val();
        var url = $("#base_url").val()+"/otbilling/fetchOTPaymentsReport";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                from_date :from_date,
                to_date :to_date,
                _token :_token
            },
            dataType: "json" ,
            beforeSend: function () {
                $(".otbill_payments_body").empty();
                $(".getReport").find('i').removeClass('fa-search').addClass('fa-spinner').addClass('fa-spin');
                $(".otbill_payments_body").append("<tr><td colspan='3' style='text-align: center;'><i class='fa fa-spinner fa-spin' style='font-size: 20px;'></i></td></tr>")
            },
            success: function (response) {
                console.log(response);
                $(".getReport").find('i').removeClass('fa-spin').removeClass('fa-spinner').addClass('fa-search');
                
                
                if(response.code === 100){
                    $(".otbill_payments_body").empty();

                    var grand_total = 0;
                    if(response.data.bill_details.length > 0){

                        var bill_total = 0;
                        $.each(response.data.bill_details, function(key, val){
                            var i = key+1;
                            bill_total = bill_total + parseFloat(val.total_amount);
                            
                            $(".otbill_payments_body").append('<tr><td>'+i+'</td><td>'+val.bill_tag_name+'</td><td>'+val.total_amount+'</td><tr>');
                        });
                        grand_total = grand_total + bill_total;
                        
                        $(".otbill_payments_body").append('<tr style="color: #b71100;"><td colspan="2" style="text-align:right;"><b>Total<b></td><td><b>'+bill_total.toFixed(2)+'</b></td><tr>');
                    }

                    

                    if(response.data.debit_collection_details.length > 0){
                        $(".otbill_payments_body").append('<tr><td colspan="3"> </td><tr><tr style="text-align:center;"><td colspan="3"> <b>Debit Colletion</b> </td><tr><tr><td colspan="3"> </td><tr>');
                    
                        var collection_total = 0;
                        $.each(response.data.debit_collection_details, function(key, val){
                            var i = key+1;
                            collection_total = collection_total + parseFloat(val.total_amount);
                            
                            $(".otbill_payments_body").append('<tr><td>'+i+'</td><td>'+val.bill_tag_name+'</td><td>'+val.total_amount+'</td><tr>');
                        });
                        grand_total = grand_total + collection_total;
                        $(".otbill_payments_body").append('<tr style="color: #b71100;"><td colspan="2" style="text-align:right;"><b>Total<b></td><td><b>'+collection_total.toFixed(2)+'</b></td><tr>');
                    }

                    
                    $(".otbill_payments_body").append('<tr style="color:#c59017;"><td colspan="2" style="text-align:right;"><b>Grand Total<b></td><td><b>'+grand_total.toFixed(2)+'</b></td><tr>');

                    
                } else {
                    toastr.error("Something went wrong..!");
                }
            },
            complete: function () { }
        });
    }

    function printReport(){
        var printMode = $('input[name=printMode]:checked').val();
        var showw = "";
        var mywindow = window.open('', 'my div', 'height=3508,width=2480');
        var msglist = document.getElementById('printData');
        showw = showw + msglist.innerHTML;
        
        if(printMode == 1){
            mywindow.document.write('<style>td,th{padding:3px;}tabele,td,th{border:1px solid #000;font-size:12px;}@media  print table,td,tr{page-break-inside:auto;width:100%; margin:0;}</style>');            
    
        }else{
            mywindow.document.write('<style>td,th{padding:3px;}tabele,td,th{border:1px solid #000;font-size:12px;}@media  print table,td,tr{page-break-inside:auto;width:100%; margin:0;}</style>');
    
        }
    
        mywindow.document.write(showw);
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
    
        mywindow.print()
        mywindow.close();
        return true;
    }
});

