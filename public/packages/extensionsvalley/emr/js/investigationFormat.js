
$(document).ready(function () {
    // listInvestigations();
    subDepartmentlist();

});

function listInvestigations() {

    var departmentId = $('#department').val();
    var subDepartmentId = $('#subdepartment').val();
    var formatCode = $('#formatCode').val();
    var testName = $('#testName').val();

    var base_url = $('#base_url').val();
    var url = base_url + "/master/serviceItemSearch";
    var param = {
        departmentId: departmentId,
        subDepartmentId: subDepartmentId,
        formatCode: formatCode,
        testName: testName

    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#search_service").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })


        },
        success: function (data) {
            console.log(data);
            $(".testsView").html(data);

        },
        complete: function () {

            $("#search_service").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
    });


}

function subDepartmentlist() {

    var departmentId = $('#department').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/listSubDepartment";
    var param = {
        departmentId: departmentId
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#subdepartment').empty();
            // $("#subdepartment").LoadingOverlay("show", {
            //     background: "rgba(255, 255, 255, 0.7)",
            //     imageColor: "#337AB7",
            // })

        },
        success: function (data) {
            console.log(data);
            var S = "Select Sub Department"
            $("#subdepartment").append('<option value="">' + S + '</option>')
            $.each(data, function (key, value) {
                ($("#subdepartment").append('<option value="' + value.id + '">' + value.name + '</option>'));
            })
        },
        complete: function () {
            // $("#subdepartment").LoadingOverlay("hide");
            listInvestigations();
        },
    });


}
function getServiceDetail(id, short_name, sampleId, pathologyReportTitle, noOfSamples, noOfQty, unit, vacutainer, InstrucForBillPers, is_blood_test, send_to_machine, is_confidential, is_poc, is_outsource, decimal_point, report_processing_time, lab_service_order, billing_instruction, report_Type, subdepartment_id,lab_remarks,lab_notes,decimal_point) {
    $('#edit_serv_id').val(id);
    $('#printOrder').val(lab_service_order);
    $('#shortName').val(short_name);
    $('#sampleName').val(sampleId);
    $('#pathologyReportTitle').val(pathologyReportTitle);
    $('#reportFormat').val(report_Type);
    $('#noOfSamples').val(noOfSamples);
    $('#noOfQty').val(noOfQty);
    $('#unit').val(unit);
    $('#vacutainer').val(vacutainer);
    $('#InstrucForBillPers').val(billing_instruction);
    // $('#subdepartment').val(subdepartment_id);
    $('#remarks').val(lab_remarks);
    $('#notes').val(lab_notes);
    $('#decimalPoint').val(decimal_point);

    var service_name = $('#testName' + id).html();
    $('.findings_of_id').html(service_name);

    if (is_blood_test == true) {
        $('#bloodTest').prop('checked', true);
    } else {
        $('#bloodTest').prop('checked', false);
    }
    if (is_confidential == true) {
        $('#confidental').prop('checked', true);
    } else {
        $('#confidental').prop('checked', false);
    }
    if (is_outsource == true) {
        $('#outSource').prop('checked', true);
    } else {
        $('#outSource').prop('checked', false);
    }
    if (is_poc == true) {
        $('#poc').prop('checked', true);
    } else {
        $('#poc').prop('checked', false);
    }
    if (send_to_machine == true) {
        $('#sendMachine').prop('checked', true);
    } else {
        $('#sendMachine').prop('checked', false);
    }
    $('#decimalPoint').val(decimal_point);
    getSubTestDetails(id);
}

function getSubTestDetails(id) {

    var base_url = $('#base_url').val();
    var url = base_url + "/master/subTestedItem";
    var param = {
        id: id
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#subTestDetails").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            $("#subTestDetails").html(data);
        },
        complete: function () {
            $("#subTestDetails").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
    });


}

function departmentList() {
    $('#orderForLabModal').modal('show');

    var base_url = $('#base_url').val();
    var url = base_url + "/master/subDepartment";
    var department = $('#department').val();
    var param = {
        department: department,
    }
    console.log(department);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#printOrderLabDepart").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            console.log(data);
            $("#printOrderLabDepart").html(data);

        },
        complete: function () {
            $("#printOrderLabDepart").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
    });


}

$("#item_search_btn").click(function () {
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
});
$("#finding_search_btn").click(function () {
    $("#finding_name").toggle();
    $("#find_search_btn_text").toggle();
});

function searchbyName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("fixTable");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function searchbyServiceName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("testName");
    filter = input.value.toUpperCase();
    table = document.getElementById("fixTable_id");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function searchByTestName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("test_setup_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("fixTable_test_setup");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function testSetupDetails() {
    $('#testSetupModal').modal('show');
    var base_url = $('#base_url').val();
    var url = base_url + "/master/testSetup";
    var department_id = $('#department').val();
    var param = {
        department_id: department_id

    }

    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $("#testSetup").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            console.log(data);
            $("#testSetup").html(data);

        },
        complete: function () {
            $("#testSetup").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
    });
}

function saveSubTest() {
    var edit_id = $('#edit_serv_id').val();
    if (edit_id == '') {
        toastr.warning('Please select service_item.');
        return;
    }
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveSubTest";
    var subTestName = $('#subTestName').val();
    if (subTestName == '') {
        toastr.warning('Please enter sub test name.');
        return;
    }
    var subReportType = $('#subReportType').val();
    if (subReportType == '') {
        toastr.warning('Please select sub report type.');
        return;
    }
    var subPrintType = $('#SubPrintType').val();
    if (subPrintType == '') {
        toastr.warning('Please select print order.');
        return;
    }
    var sub_test_status = $('#sub_test_status').is(':checked');
    var service_ids = $('#edit_serv_id').val();
    if (sub_test_status == true) {
        var sub_test_status = 1;
    } else {
        var sub_test_status = 0;
    }
    var service_id = $('#printOrder').val();
    var param = {
        subTestName: subTestName,
        subReportType: subReportType,
        subPrintType: subPrintType,
        sub_test_status: sub_test_status,
        service_id: service_id,
        service_ids: service_ids
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $("#subTestDetails").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.warning(obj.message);
            }


        },
        complete: function () {
            $("#subTestDetails").LoadingOverlay("hide");
            getSubTestDetails(service_ids);
            $('#subTestName').val('');
            $('#subReportType').val('');
            $('#SubPrintType').val('');

        },
    });
}

function subTestDelete(id, service_id) {
    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deleteSubTest";

                var param = {
                    id: id,
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,

                    beforeSend: function () {
                        $("#subTestDetails").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })

                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        }
                        else {
                            toastr.warning(obj.message);
                        }
                    },
                    complete: function () {
                        $("#subTestDetails").LoadingOverlay("hide");
                        getSubTestDetails(service_id);
                        $('#subTestName').val('');
                        $('#subReportType').val('');
                        $('#SubPrintType').val('');
                    },
                });
            }
        }
    });

}

var service = '0';
var machine_convert_chk = [];
var machine_convert_unchk = [];

function checkConvert(serviceid) {

    var status = $('#machine_' + serviceid).is(":checked");
    var check = machine_convert_chk.includes(serviceid);

    if (status) {
        if (!check) {
            service = serviceid;
            machine_convert_chk.push(service);
        }
    } else {
        machine_convert_unchk.push(serviceid);
        var pos = machine_convert_chk.indexOf(service);
        if (pos > -1) {
            machine_convert_chk.splice(pos, 1);
        }
    }
    console.log(machine_convert_chk);
    console.log(machine_convert_unchk);
}


function mappingToMachineTest() {

    var base_url = $('#base_url').val();
    var url = base_url + "/master/converToMachineTest";
    var param = {
        machine_convert_chk: machine_convert_chk,
        machine_convert_unchk: machine_convert_unchk
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $("#testSetup").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        }
                        else {
                            toastr.warning(obj.message);
                        }
        },
        complete: function () {
            $("#testSetup").LoadingOverlay("hide");

            $('#subTestName').val('');
            $('#subReportType').val('');
            $('#SubPrintType').val('');

        },
    });
}

function saveservice_item() {
    var edit_id = $('#edit_serv_id').val();
    if (edit_id == '') {
        toastr.warning('Please select service_item.');
        return;
    }
    var sample_name = $('#sampleName').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveServiceItem";
    var department = $('#department').val();
    var subdepartment = $('#subdepartment').val();
    var testName = $('#testName' + edit_id).html();
    var printOrder = $('#printOrder').val();
    var shortName = $('#shortName').val();
    var pathologyReportTitle = $('#pathologyReportTitle').val();
    var noOfSamples = $('#noOfSamples').val();
    var noOfQty = $('#noOfQty').val();
    var unit = $('#unit').val();
    var vacutainer = $('#vacutainer').val();
    var reportFormat = $('#reportFormat').val();
    var remarks = $('#remarks').val();
    var notes = $('#notes').val();
    var bloodTest = $('#bloodTest').is(':checked');
    var sendMachine = $('#sendMachine').is(':checked');
    if (sendMachine == true) {
        sendMachine = 1;
    } else {
        sendMachine = 0;
    }
    var poc = $('#poc').is(':checked');
    var confidental = $('#confidental').is(':checked');
    var outSource = $('#outSource').is(':checked');
    var InstrucForBillPers = $('#InstrucForBillPers').val();
    var instrucForPhelo = $('#instrucForPhelo').val();
    var lab_notes = $('#notes').val();
    var param = {
        edit_id: edit_id,
        sendMachine: sendMachine,
        department: department,
        subdepartment: subdepartment,
        testName: testName,
        printOrder: printOrder,
        shortName: shortName,
        pathologyReportTitle: pathologyReportTitle,
        noOfSamples: noOfSamples,
        noOfQty: noOfQty,
        unit: unit,
        vacutainer: vacutainer,
        reportFormat: reportFormat,
        remarks: remarks,
        notes: notes,
        bloodTest: bloodTest,
        poc: poc,
        confidental: confidental,
        outSource: outSource,
        InstrucForBillPers: InstrucForBillPers,
        instrucForPhelo: instrucForPhelo,
        sample_name: sample_name,
        lab_notes: lab_notes,
    }

    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.warning(obj.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");



        },
    });
}


function chengePriceOrde(code) {

    var base_url = $('#base_url').val();
    var url = base_url + "/master/chengePriceOrder";

    var printOrder = $('.printOrder' + code).val();
    console.log(printOrder);
    var param = {
        printOrder: printOrder,
        code: code,
    }

    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $("#printOrderLabDepart").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.warning(obj.message);
            }
        },
        complete: function () {
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            $("#printOrderLabDepart").LoadingOverlay("hide");
        },
    });
}

function chengeLabServOrder(id) {
    var labServicOrder = $('#chengeLabServOrder' + id).val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/chengeLabServiceOrder";
    var param = {
        labServicOrder: labServicOrder,
        id: id,
    }
    console.log(param);
    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $("#testSetup").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {

            try{
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.warning(obj.message);
            }
        }catch (err){
            console.log('error', err);
        }
        },
        complete: function () {
            
            $("#testSetup").LoadingOverlay("hide");



        },
    });
}

function createViewFormat(subtest_id=0) {
    var edit_head_id = $('#edit_serv_id').val();
    if (edit_head_id == '') {
        toastr.warning('Please select service item.');
        return;
    }
    var format_type = $('#reportFormat').val();
    console.log(format_type);
    if (format_type == 'N') {
        $('#numericFormatModal').modal('show');
        numericFormatList(subtest_id);
    } else if (format_type == 'T') {
        $('#TextFormatModel').modal('show');
        tinyMceCreate('textFIndings', 460);
        getTextFindings();
    }
    else if (format_type == 'G') {
        $('#profileFormatModal').modal('show');
        investigationFormatList(subtest_id);
    } else if (format_type == 'F') {
        $('#findingFormatModal').modal('show');
        findingDetailList();
        selectedFindingList();
    }
}

function serviceListing() {

    var base_url = $('#base_url').val();
    var url = base_url + "/master/ListingServiceItems";

    $.ajax({
        type: "POST",
        url: url,

        beforeSend: function () {
            $("#profileList").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            $('#profileList').html(data);
            console.log(data)
        },
        complete: function () {
            $("#profileList").LoadingOverlay("hide");



        },
    });
}
function getTextFindings() {
    var service_id = $('#edit_serv_id').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/getTextFindings";
    var param = {
        service_id: service_id,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {

            $("#text_format").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })

        },
        success: function (data) {
            console.log(data);
            try {
            if(data.length>0){
            var obj = JSON.parse(data);
            if(obj.status==1){
            tinymce.get('textFIndings').setContent(obj.format_value);
            }else{
            tinymce.get('textFIndings').setContent('');
            }
        }
        } catch (err) {
            //  SyntaxError: Unexpected end of JSON input
            console.log('error', err);
          }
        
        },
        complete: function () {
            $("#text_format").LoadingOverlay("hide");
        },
    });
}

function tinyMceCreate(text_area, tinymce_height) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        plugins: 'emoticons hr image link lists charmap table ',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help',
        toolbar: 'undo redo | bold italic underline strikethrough |fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | Custom Bullet ',
        browser_spellcheck: true,
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,
        importcss_append: true,
        height: tinymce_height,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
    });
}
function findingDetailList() {
    var service_id = $('#edit_serv_id').val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/findingListingDetails";
    $.ajax({
        type: "POST",
        url: url,

        beforeSend: function () {
            $(".findingTableBody").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            $('#findingDetails').html(data);
        },
        complete: function () {
            $(".findingTableBody").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
    });
}
function selectedFindingList() {
    var service_id = $('#edit_serv_id').val();
    if (!service_id) {
        toastr.warning('Please select service');
        return;
    }
    var base_url = $('#base_url').val();
    var url = base_url + "/master/selectedFIndingDetails";
    var param = {
        service_id: service_id
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,

        beforeSend: function () {
            $(".selectFindingTableBody").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
            resetBootstrapTable();
        },
        success: function (data) {
            $('#selectFIndings').html(data);
        },
        complete: function () {
            $(".selectFindingTableBody").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            resetBootstrapTable();
        },
    });

}

function searchByFindings() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("finding_name");
    filter = input.value.toUpperCase();
    table = document.getElementById("finding_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
function addFindingsToServ(findings_id, finding_name) {
    var service_id = $('#edit_serv_id').val();
    var param = {
        findings_id: findings_id,
        finding_name: finding_name,
        service_id: service_id,
    }
    var base_url = $('#base_url').val();
    var url = base_url + "/master/addFindingsToService";

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            selectedFindingList();
            console.log(data)
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.warning(obj.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        },
    });
}
function convertToRtf(plain) {
    plain = plain.replace(/\n/g, "\\par\n");
    return "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang2057{\\fonttbl{\\f0\\fnil\\fcharset0 Microsoft Sans Serif;}}\n\\viewkind4\\uc1\\pard\\f0\\fs17 " + plain + "\\par\n}";
}
function addtextfindings() {
    var service_id = $('#edit_serv_id').val();
    var format_val = tinymce.get('textFIndings').getContent();
    var format_val = convertToRtf(format_val);
    var base_url = $('#base_url').val();
    var url = base_url + "/master/addTextFindings";
    var param = {
        service_id: service_id,
        format_val: format_val
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            console.log(data)
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.warning(obj.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('#TextFormatModel').modal('hide');

        },
    });
}
function addIsPanic(id) {
    var isPanic = '';
    if ($('.is_panic' + id).is(':checked')) {
        isPanic = 1;
    } else {
        isPanic = 0;
    }
    var base_url = $('#base_url').val();
    var url = base_url + "/master/addIsPanic";
    var param = {
        isPanic: isPanic,
        id: id,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            console.log(data);
            
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.error(obj.message);
            }
        },
        complete: function () {

            $("body").LoadingOverlay("hide");
        },
    });
}
function deleteFindings(id) {
    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deleteFindings";
                var param = {
                    id: id,
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })
                    },
                    success: function () {
                    },
                    complete: function () {
                        selectedFindingList();
                        $("body").LoadingOverlay("hide");
                    },
                });

            }
        }
    });

}
function numericFormatList(sub_test_id=0) {
    $('#edit_sub_test_id').val(sub_test_id);
    var service_id = $('#edit_serv_id').val();
    if (service_id == '') {
        toastr.warning('Please select service.')
        return;
    }
    var base_url = $('#base_url').val();
    var url = base_url + "/master/numericFormatListing";
    param = {
        service_id: service_id,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            console.log(data);
            $('#numericListView').html(data);
        },
        complete: function () {


            $("body").LoadingOverlay("hide");
        },
    });
}

function saveNumericFormat() {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveNumericFormatLIst";
    var service_id = $('#edit_serv_id').val();
    var sub_test_id = $('#edit_sub_test_id').val()>0?$('#edit_sub_test_id').val():0;
    var machine_name = $('#machine_name').val();
    var gender = $('#gender_numeric').val();
    
    var from_year = $('#from_year').val() > 0 ? $('#from_year').val() : 0;
    if (from_year != 0) {
        var from_year = from_year * 365.2425;
    }
    var from_month = $('#from_month').val() > 0 ? $('#from_month').val() : 0;
    if (from_month != 0) {
        var one_month_day = 365.2425 / 12;
        var from_month = from_month * parseInt(one_month_day);
    }
    var from_date = $('#from_date').val() > 0 ? $('#from_date').val() : 0;
    if (from_date != 0) {
        from_date = $('#from_date').val();
    }

    var date = parseInt(from_year) + parseInt(from_month) + parseInt(from_date);
    var date_from = parseInt(date);
    var n_to_year = $('#to_year').val() > 0 ? $('#to_year').val() : 0;
    if (n_to_year != 0) {
        var n_to_year = n_to_year * 365.2425;
    }
    var n_to_month = $('#to_month').val() > 0 ? $('#to_month').val() : 0;
    if (n_to_month != 0) {
        var n_to_month = n_to_month * parseInt(one_month_day);
    }
    var to_date = $('#to_date').val() > 0 ? $('#to_date').val() : 0;
    var dateto = parseInt(n_to_year) + parseInt(n_to_month) + parseInt(to_date);
    var date_to = parseInt(dateto);
    console.log(date_to);
    console.log(date_from);


    var minimum_value = $('#minimum_range').val();
    var maximum_value = $('#maximum_range').val();
    var panic_minimum_value = $('#panic_minimum_value').val();
    var panic_maximum_value = $('#panic_maximum_value').val();
    var normalRangeDescrip = $('#normalRangeDescrip').val();
    var param = {
        service_id: service_id,
        machine_name: machine_name,
        gender: gender,
        minimum_value: minimum_value,
        maximum_value: maximum_value,
        panic_minimum_value: panic_minimum_value,
        panic_maximum_value: panic_maximum_value,
        normalRangeDescrip: normalRangeDescrip,
        service_id: service_id,
        date_from: date_from,
        date_to: date_to,
        sub_test_id:sub_test_id,

    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.error(obj.message);
            }
            numericFormatList();
        },
        complete: function () {


            $("body").LoadingOverlay("hide");
        },
    });
}
function investigationFormatList(subtest_id) {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/investigationListData";
    var service_id = $('#edit_serv_id').val();
    $('#edit_sub_test_id').val(subtest_id);
    var param = {
        service_id: service_id
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            $('#profileDataList').html(data);

        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            resetBootstrapTable();
        },
    });

}
$('.hidden_search').keyup(function (event) {
    
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();
        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            
            var base_url = $('#base_url').val()

            var url = base_url + "/master/formListAjaxsearch";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function () {
                    
                    // $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                    resetBootstrapTable();
                    if(service_ids){
                        if(service_ids.length>0){
                            $.each(service_ids, function(key, value) {
                                $('#service_ids'+value).prop('checked', true);
                            });
                        }
                        }

                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        // ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;

    }
    
});

var service_ids = [];
function selectService(){
    $('.service_ids:checked').each(function () {
    var id = $(this).attr('data-id') > 0 ? $(this).attr('data-id').trim() : 0;
    if (service_ids.includes(id) === false) service_ids.push(id);
    });
    $(".service_ids:not(:checked)").each(function() {
        var id = $(this).attr('data-id') > 0 ? $(this).attr('data-id').trim() : 0;
        if(service_ids.includes(id)===true) service_ids.splice( $.inArray(id, service_ids), 1 );

    })
}
// function checkService(value,serv_id,name){
//     if(value==serv_id){
//         return true;
//        }else{
//         return false
//        }
// }
function checkService(){
 var flag=false;
 var name = '';
$('.select_service').each(function(){
    var serv_id = $(this).attr('data-select-id').trim();
    var resultId=service_ids.indexOf(serv_id);
    if(resultId != -1){
        var serviceId=service_ids[resultId];
      name = $(this).attr('data-select'+serviceId);
     flag=true;
     return false;
    }else{ 
    flag=false;
    }
});
$result=[];
$result['flag']=flag;
$result['name']=name;
return $result;
}

function saveProfileFormat() {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveProfileFormat";
    // var service_ids = [];
    var service_id = $('#edit_serv_id').val();
    var subTestId = $("#edit_sub_test_id").val() > 0 ? $("#edit_sub_test_id").val() : 0;
    var report_type = $('#subReportType').val();
    var is_exit=checkService();
    console.log(is_exit['flag']);
    if (is_exit['flag']){
        toastr.warning(is_exit['name']+'already exist.');
        return false;
    }
    var param = {
        service_ids: service_ids,
        service_id: service_id,
        subTestId: subTestId,
        report_type: report_type,
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            console.log(data);
            if(data){
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.error(obj.message);
            }
        }
        },
        complete: function () {
            investigationFormatList(id = 0);
            $("body").LoadingOverlay("hide");
            service_ids = [];
            $result = [];
            $('#profile_service_item').val('');
            

        },
    });
}
function deleteProfile(id) {

    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deleteProfile";
                var param = {
                    id: id
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        }
                        else {
                            toastr.error(obj.message);
                        }
                    },
                    complete: function () {
                        investigationFormatList();
                        $("body").LoadingOverlay("hide");
                    },
                });
            }
        }
    });

}



function deleteNumericFormat(id) {
    var message_show = '<p>Are you sure you want to delete. </p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                var base_url = $('#base_url').val();
                var url = base_url + "/master/deleteNumericFormat";
                var param = {
                    id: id
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: "#337AB7",
                        })
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            toastr.success(obj.message);
                        }
                        else {
                            toastr.error(obj.message);
                        }
                    },
                    complete: function () {
                        $("body").LoadingOverlay("hide");
                        numericFormatList();
                    },
                });

            }
        }
    });

}
function orderUpdate(id) {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/orderUpdate";
    var updateOrder = $('#order' + id).val();
    var param = {
        id: id,
        updateOrder: updateOrder,
    }

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#337AB7",
            })
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == 1) {
                toastr.success(obj.message);
            }
            else {
                toastr.error(obj.message);
            }
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
            investigationFormatList(id = 0);
        },
    });
}
$('#numericFormatModal').on('hidden.bs.modal', function() {
    numericFormClear(); 
})
function numericFormClear(){
    $('#machine_name').val('');
    $('#gender_numeric').val('');
    $('#from_year').val('');
    $('#to_year').val('');
    $('#from_month').val('');
    $('#to_month').val('');
    $('#from_date').val('');
    $('#minimum_range').val('');
    $('#maximum_range').val('');
    $('#panic_minimum_value').val('');
    $('#panic_maximum_value').val('');
    $('#to_date').val('');
    $('#normalRangeDescrip').val('');
}


