function drugReaction(){
    var patient_id = $("#patient_id").val();
    var visit_id = $('#visit_id').val();
    var url = $('#base_url').val() + "/drug_reaction/drug_reaction";

    $.ajax({
        url: url,
        type: "GET",
        data: {
            patient_id:patient_id,
            visit_id:visit_id
        },
        beforeSend:function(){
            $('#drug_reaction_modal').modal('show');
            $('#drug_reaction_modal_content').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data){
            $('#drug_reaction_modal_content').html(data);
            $('.select2').select2();
        },
        complete: function(){
            $('#drug_reaction_modal_content').LoadingOverlay("hide");
            $('.start_date').datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });

            listDrugReaction();
            //if($("#suspected_drug").val()!= ''){
                $("#suspected_drug").change(changeDrugDetail).change();
            //}

        },
    });
}

function saveDrugReaction(){
    var dataparams = $('#add_drug_reaction').serialize();
    let _token = $('#c_token').val();
    var url = $('#base_url').val() + "/drug_reaction/save_drug_reaction";
    var patient_id = $('#patient_id').val();
    var suspected_drug = $('#suspected_drug').val();

    if(suspected_drug == ''){
        Command: toastr["warning"]('Please select suspected drug!');
        return false;
    }

    $.ajax({
        url: url,
        type: "POST",
        data: '_token=' + _token + dataparams+'&patient_id='+patient_id,
        beforeSend:function(){
            $('#drug_reaction_modal_content').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data){
            var obj = JSON.parse(data);
            if(obj.status == 'success'){
                Command: toastr["success"](obj.message);
                listDrugReaction();
                $('#add_drug_reaction')[0].reset();
            }else{
                Command: toastr["error"](obj.message);
            }

        },
        complete: function(){
            $('#drug_reaction_modal_content').LoadingOverlay("hide");

        },
    });
}
function changeDrugDetail(){
   // console.log('dddd');
  //  if($('#drug_reaction_edit_id').val() == ''){
        var obj = $(this);
        var drug_detail_id = obj.val();
        var drug_detail = $('#drug_reaction_detail_array').val();
            drug_detail = atob(drug_detail);
            drug_detail = JSON.parse(drug_detail);
        var medicine_id = drug_detail[drug_detail_id].item_id;
        var dose = drug_detail[drug_detail_id].dose;
        var batch = drug_detail[drug_detail_id].batch;
        var expiry = drug_detail[drug_detail_id].expiry;
        $('#drug_reaction_item_id').val(medicine_id);
        $('#drug_reaction_dose').val(dose);
        $('#drug_reaction_batch_no').val(batch);
        $('#drug_reaction_expiry_date').val(expiry);
  //  }

}

function delete_drug_reaction_data(id){
    var url = $('#base_url').val() + "/drug_reaction/delete_drug_reaction";
    $.ajax({
        url: url,
        type: "GET",
        data: {
            id:id,
        },
        beforeSend:function(){
            $('#reaction_list_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data){
            var obj = JSON.parse(data);
            if(obj.status == 'success'){
                Command: toastr["success"](obj.message);
                $('#drug_reaction_table_row_'+id).remove();

            }else{
                Command: toastr["error"](obj.message);
            }
        },
        complete: function(){
            $('#reaction_list_container').LoadingOverlay("hide");
            //if($("#suspected_drug").val()!= ''){
                $("#suspected_drug").change(changeDrugDetail).change();
            //}
        },
    });
}

function listDrugReaction(){
    var patient_id = $('#patient_id').val();
    var url = $('#base_url').val() + "/drug_reaction/list_drug_reaction";
    $.ajax({
        url: url,
        type: "GET",
        data: {
            patient_id:patient_id,
        },
        beforeSend:function(){
            $('#reaction_list_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data){
            $('#reaction_list_container').html(data);
        },
        complete: function(){
            $('#reaction_list_container').LoadingOverlay("hide");
            $(".tooltip-drug-details").popover({ trigger: "hover" });

            //if($("#suspected_drug").val()!= ''){
                $("#suspected_drug").change(changeDrugDetail).change();
            //}

        }

    });
}

function edit_drug_reaction_data(id){
    var url = $('#base_url').val() + "/drug_reaction/edit_drug_reaction";
    $.ajax({
        url: url,
        type: "GET",
        data: {
            id:id,
        },
        beforeSend:function(){
            $('#drug_reaction_modal_content').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(data){
            var obj = JSON.parse(data);
            //console.log(obj.data.medication_detail_id);
            if(obj.status == 'success'){
                $('#drug_reaction_edit_id').val(obj.data.id);
                $('#suspected_drug').val(obj.data.medication_detail_id).trigger('change');
                $('#drug_reaction_dose').val(obj.data.dose);
                $('#drug_reaction_batch_no').val(obj.data.batch_no);
                $('#drug_reaction_expiry_date').val(obj.data.expiry_date);
                $('#drug_reaction_start_date').val(obj.data.started_date);
                $('#drug_reaction_start_time').val(obj.data.started_time);
                $('#drug_reaction_description').val(obj.data.description);
            }else{
                Command: toastr["error"](obj.message);

            }

        },
        complete: function(){
            $('#drug_reaction_modal_content').LoadingOverlay("hide");
            if($("#suspected_drug").val()!= ''){
                $("#suspected_drug").change(changeDrugDetail).change();
            }

        },
    });
}

//if($("#suspected_drug").val()!= ''){
    $("#suspected_drug").change(changeDrugDetail).change();
//}

