$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2();
    searchDatasetList();
    tinyMceCreate('dataset_query', 350, 0);
});
var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();



function searchDataset(event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(event.keyCode);
    var dataset_name = $('#search_dataset_name').val();
    if (event.keyCode == 13) {
        ajaxlistenter('ajax_search_dataset');
        return false;
    } else if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        if (dataset_name) {
            var url = base_url + "/form_template/search_dataset_name";
            var param = { _token: token, dataset_name: dataset_name };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#ajax_search_dataset").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajax_search_dataset").html(html).show();
                    $("#ajax_search_dataset").find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }, error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            $("#ajax_search_dataset").html('').hide();
            $("#search_dataset_id").val(0);
        }
    } else {
        ajax_list_key_down('ajax_search_dataset', event);
    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillItemValues(id, dataset_name) {
    $('#search_dataset_name').val(htmlDecode(dataset_name));
    $("#ajax_search_dataset").html('').hide();
    $("#search_dataset_id").val(id);
}

function tinyMceCreate(text_area, tinymce_height, read_only) {
    tinymce.init({
        selector: '#' + text_area,
        max_height: tinymce_height,
        autoresize_min_height: '90',
        imagetools_cors_hosts: ['picsum.photos'],
        browser_spellcheck: false,
        autosave_ask_before_unload: false,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: false,
        contextmenu: false,
        toolbar: false,
        menubar: false,
        importcss_append: false,
        height: 400,
        image_caption: false,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
        readonly: read_only,
    });
}

function searchDatasetList() {
    var url = base_url + "/form_template/searchDatasetList";
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var search_dataset_id = $('#search_dataset_id').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { _token: token, from_date: from_date, to_date: to_date, search_dataset_id: search_dataset_id },
        beforeSend: function () {
            $('#searchDatasetListBtn').attr('disabled', true);
            $('#searchDatasetListSpin').removeClass('fa fa-search');
            $('#searchDatasetListSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchDataDiv').html(data);
        },
        complete: function () {
            $('#searchDatasetListBtn').attr('disabled', false);
            $('#searchDatasetListSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchDatasetListSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error('Please check your internet connection and try again');
        }
    });
}


function verfyDataset() {
    $html_data = "";
    var filters = false;
    $('.dataset_filter').each(function (i, obj) {
        if ($(this).val()) {
            filters = true;
            $html_data += "<div class='col-md-2 padding_sm'><strong> " + $(this).val() + " : </strong></div><div class='col-md-10 padding_sm'><input type='text' autocomplete='off' class='form-control set_filtersdata' id='set_filtersdata" + $(this).attr('id') + "' name='set_filtersdata' value=''></></div><div class='clearfix'></div>";
        }
    });

    if (filters) {
        $('#addToFiltersModelDiv').html($html_data);
        $("#addToFiltersModel").modal({
            backdrop: 'static',
            keyboard: false
        });
    } else {
        validateQuery(1);
    }

}

function addToFilter() {
    validateQuery(2);
}

function validateQuery(from_type) {
    var filter_data = new Array();
    if (parseInt(from_type) == 2) {
        $('.set_filtersdata').removeClass('bg-red');
        var j = 1;
        $('.set_filtersdata').each(function (i, obj) {
            if ($(this).val()) {
                filter_data.push({
                    filter_id: 'filter' + j,
                    filter_value: $(this).val(),
                });
            } else {
                $(this).addClass('bg-red');
                return false;
            }
            j++;
        });
    }
    var filter_data_string = JSON.stringify(filter_data);
    var url = base_url + "/form_template/verfyDataset";
    var dataset_id = $('#dataset_id').val();
    var dataset_name = $('#dataset_name').val();
    var dataset_query_text = tinymce.get('dataset_query').getContent({ format: 'text' });
    if (dataset_name) {
        if (dataset_query_text) {
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, dataset_name: dataset_name, dataset_id: dataset_id, dataset_query_text: dataset_query_text, filter_data_string: filter_data_string },
                beforeSend: function () {
                    $('#verfyDatasetBtn').attr('disabled', true);
                    $('#verfyDatasetSpin').removeClass('fa fa-check');
                    $('#verfyDatasetSpin').addClass('fa fa-spinner fa-spin');
                    $('#saveDatasetBtn').attr('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.status) == 1) {
                        toastr.success(data.message);
                        $('#dataset_selection_data').val(data.dataset_keys);
                        $('#saveDatasetBtn').attr('disabled', false);
                    } else {
                        toastr.warning(data.message);
                    }
                    if (parseInt(from_type) == 2) {
                        $('#addToFiltersModel').modal('toggle');
                    }
                },
                complete: function () {
                    $('#verfyDatasetBtn').attr('disabled', false);
                    $('#verfyDatasetSpin').removeClass('fa fa-spinner fa-spin');
                    $('#verfyDatasetSpin').addClass('fa fa-check');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        } else {
            toastr.warning("Please enter any query");
        }
    } else {
        toastr.warning("Please enter Dataset Name");
    }
}

function saveDataset() {
    var url = base_url + "/form_template/saveDataset";
    var dataset_query_html = tinymce.get('dataset_query').getContent();
    var dataset_id = $('#dataset_id').val();
    var dataset_name = $('#dataset_name').val();
    var dataset_status = $('#dataset_status').is(":checked");
    var is_master = $('#is_master_dataset').is(":checked");
    var dataset_selection_data = $('#dataset_selection_data').val();
    var dataset_filter1 = $('#dataset_filter1').val();
    var dataset_filter2 = $('#dataset_filter2').val();
    var dataset_filter3 = $('#dataset_filter3').val();
    var dataset_filter4 = $('#dataset_filter4').val();
    var dataset_filter5 = $('#dataset_filter5').val();
    var dataset_filter6 = $('#dataset_filter6').val();
    var dataset_query_text = tinymce.get('dataset_query').getContent({ format: 'text' });
    if (dataset_name) {
        if (dataset_query_text) {
            if (dataset_selection_data) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, dataset_id: dataset_id, dataset_name: dataset_name, is_master: is_master, dataset_status: dataset_status, dataset_query_html: dataset_query_html, dataset_selection_data: dataset_selection_data, dataset_query_text: dataset_query_text, dataset_filter1: dataset_filter1, dataset_filter2: dataset_filter2, dataset_filter3: dataset_filter3, dataset_filter4: dataset_filter4, dataset_filter5: dataset_filter5, dataset_filter6: dataset_filter6 },
                    beforeSend: function () {
                        $('#saveDatasetBtn').attr('disabled', true);
                        $('#saveDatasetSpin').removeClass('fa fa-save');
                        $('#saveDatasetSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            $('#dataset_id').val(data.dataset_id);
                            $("#dataSetmodel").modal('toggle');
                            searchDatasetList();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $('#saveDatasetBtn').attr('disabled', false);
                        $('#saveDatasetSpin').removeClass('fa fa-spinner fa-spin');
                        $('#saveDatasetSpin').addClass('fa fa-save');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            } else {
                toastr.warning("Please enter any selection data");
            }
        } else {
            toastr.warning("Please enter any query");
        }
    } else {
        toastr.warning("Please enter Dataset Name");
    }
}

function addEditDataSet(set_id, set_name) {
    $('#dataset_id').val(set_id);
    var url = base_url + "/form_template/addEditDataSet";
    if (parseInt(set_id) != 0) {
        $.ajax({
            type: "POST",
            url: url,
            data: { _token: token, set_id: set_id },
            beforeSend: function () {
                $('#updateDataSetBtn' + set_id).attr('disabled', true);
                $('#updateDataSetSpin' + set_id).removeClass('fa fa-edit');
                $('#updateDataSetSpin' + set_id).addClass('fa fa-spinner fa-spin');
                $('#dataset_status').prop('checked', false);
                $('#is_master_dataset').prop('checked', false);
            },
            success: function (data) {
                $('#dataset_name').val(data.dataset_name);
                $('#dataset_selection_data').val(data.dataset_selection_data);
                $('#dataset_filter1').val(data.dataset_filter1);
                $('#dataset_filter2').val(data.dataset_filter2);
                $('#dataset_filter3').val(data.dataset_filter3);
                $('#dataset_filter4').val(data.dataset_filter4);
                $('#dataset_filter5').val(data.dataset_filter5);
                $('#dataset_filter6').val(data.dataset_filter6);
                tinymce.get('dataset_query').setContent(data.data_query_html);
                if (parseInt(data.status) == 1) {
                    $('#dataset_status').prop('checked', true);
                }
                if (parseInt(data.is_master) == 1) {
                    $('#is_master_dataset').prop('checked', true);
                }

                $('#addDataSetHeader').html(atob(set_name));
                $("#dataSetmodel").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            complete: function () {
                $('#updateDataSetBtn' + set_id).attr('disabled', false);
                $('#updateDataSetSpin' + set_id).removeClass('fa fa-spinner fa-spin');
                $('#updateDataSetSpin' + set_id).addClass('fa fa-edit');
            },
            error: function () {
                toastr.error('Please check your internet connection and try again');
            }
        });
    } else {
        $('#dataset_name').val('');
        $('#dataset_selection_data').val('');
        $('#dataset_filter1').val('');
        $('#dataset_filter2').val('');
        $('#dataset_filter3').val('');
        $('#dataset_filter4').val('');
        $('#dataset_filter5').val('');
        $('#dataset_filter6').val('');
        tinymce.get('dataset_query').setContent('');
        $('#dataset_status').prop('checked', true);
        $('#is_master_dataset').prop('checked', false);
        $('#addDataSetHeader').html(atob(set_name));
        $("#dataSetmodel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
}

function resetFilters() {
    $('#from_date').val('');
    $('#to_date').val('');
    $('#search_dataset_name').val('');
    $('#search_dataset_id').val(0);
    searchDatasetList();
}


function deleteDataset(set_id, dataset_name) {
    dataset_name = atob(dataset_name);
    var url = base_url + "/form_template/deleteDataSet";
    bootbox.confirm({
        message: "Are you sure,you want to delete " + dataset_name + " ?",
        buttons: {
            'confirm': {
                label: "Yes",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { _token: token, set_id: set_id },
                    beforeSend: function () {
                        $('#deleteDataSetBtn' + set_id).attr('disabled', true);
                        $('#deleteDataSetSpin' + set_id).removeClass('fa fa-trash');
                        $('#deleteDataSetSpin' + set_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        $('#datasetDataRow' + set_id).remove();
                    },
                    complete: function () {
                        $('#deleteDataSetBtn' + set_id).attr('disabled', false);
                        $('#deleteDataSetSpin' + set_id).removeClass('fa fa-spinner fa-spin');
                        $('#deleteDataSetSpin' + set_id).addClass('fa fa-trash');
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        }
    });
}
