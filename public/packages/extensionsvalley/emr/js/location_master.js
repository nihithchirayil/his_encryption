
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }
        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        window.search_url = $(".table_body_contents").attr('data-search-url');
        getFilteredItems(window.search_url);

        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            if($(e.target).parent().hasClass('active') == false){
                var url = $(this).attr("href");
                getFilteredItems(url);
            }
            
        });
        
        $(document).on('click', '.searchBtn', function (e) {
            getFilteredItems(window.search_url);
        });
        $(document).on("click", function(event){
            var $trigger = $(".ajaxSearchBox");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ajaxSearchBox").hide();
            }            
        });

    });
   
    //----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_location_master_search";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});

    function getFilteredItems(url){
        // alert('sads');
        // return;
        var data = {};
      
        data.location = $("#location").val();
        data.location_type = $("#location_type").val();
        data.floor = $("#floor").val();
        data.status = $(".status").val();
       
              $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                 $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $(".table_body_contents").LoadingOverlay("hide");
                $(".table_body_contents").html(data.html);
                $(".page-item").attr("disabled", false);
                $(".page-item.active").attr("disabled", true);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            },
            complete: function () {
                
            }
        });

    }

    function saveForm(){
        var url = $(".table_body_contents").attr('data-action-url');
        var data = {};

        if($("#location_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Location Name");
            return;
        }
        if($("#location_code_add").val().trim() == ''){
            Command: toastr["error"]("Please enter Code");
            return;
        }
        if($("#location_type_add").val().trim() == ''){
            Command: toastr["error"]("Please enter  Location Type");
            return;
        }
   

        data.location_id = $(".reference_id").val();
        data.location = $("#location_add").val();
        data.location_type = $("#location_type_add").val();
        data.location_code = $("#location_code_add").val();
        data.floor = $("#floor_add").val();
        data.contact_no = $("#contact_no_add").val();
        data.contact_person = $("#contact_person_add").val();
        data.is_nursing_station = $("#is_nursing_station_add").val();
        data.deals_pharmacy_item = $("#deals_pharmacy_item_add").val();
        data.deals_materials_item = $("#deals_materials_item_add").val();
        data.status = $(".status_add").val();
// alert(data.allow_pay_from_cash);
       
        if($(".status_add").prop('checked') == true){
            // alert("chcked");
            data.status =1;
        }else{
            data.status =0;
        }
        if($(".is_nursing_station_add").prop('checked') == true){
            // alert("chcked");
            data.is_nursing_station =1;
        }else{
            data.is_nursing_station =0;
        }
        if($(".deals_pharmacy_item_add").prop('checked') == true){
            // alert("chcked");
            data.deals_pharmacy_item =1;
        }else{
            data.deals_pharmacy_item =0;
        }
        if($(".deals_materials_item_add").prop('checked') == true){
            // alert("chcked");
            data.deals_materials_item =1;
        }else{
            data.deals_materials_item =0;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".saveButton").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.saveButton').attr('disabled', true);

            },
            success: function (data) {  
                if(data==1){
                    Command: toastr["error"](" Code Already Exit!");
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
    
                    }
                  
                   else{
                    $(".saveButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.saveButton').attr('disabled', false);
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                    resetForm();
                    }
            },
            complete: function () {

            }
        });
    }
   
    function editItem(element){
     

        let location_id = $(element).parents('tr').attr('data-id');
        let location = $(element).parents('tr').attr('data-location_name');
        let location_code = $(element).parents('tr').attr('data-code');
        let location_type = $(element).parents('tr').attr('data-type');
        let floor = $(element).parents('tr').attr('data-floor');
        let contact_no = $(element).parents('tr').attr('data-contact_no');
        let contact_person = $(element).parents('tr').attr('data-contact_person');
        let is_nursing_station = $(element).parents('tr').attr('data-nursing_station');
        let deals_pharmacy_item = $(element).parents('tr').attr('data-pharmacy_item');
        let deals_materials_item = $(element).parents('tr').attr('data-materials_item');
        let status = $(element).parents('tr').attr('data-status');
// alert(effective_date);
        if(status==1){
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", true);

            } else{
            //alert("CheckBox checked.");
                        $(".status_add").prop("checked", false);
                   }
                  
         if(is_nursing_station==1){
          //alert("CheckBox checked.");
                      $(".is_nursing_station_add").prop("checked", true);
          } else{
          //alert("CheckBox checked.");
                      $(".is_nursing_station_add").prop("checked", false);
                 }
      if(deals_pharmacy_item==1){
         //alert("CheckBox checked.");
                     $(".deals_pharmacy_item_add").prop("checked", true);
         } else{
         //alert("CheckBox checked.");
                     $(".deals_pharmacy_item_add").prop("checked", false);
                }
     if(deals_materials_item==1){
      //alert("CheckBox checked.");
                  $(".deals_materials_item_add").prop("checked", true);
      } else{
      //alert("CheckBox checked.");
                  $(".deals_materials_item_add").prop("checked", false);
             }
        $(".reference_id").val(location_id);
        $("#location_add").val(location);
        $("#location_type_add").val(location_type);
        $("#location_type_add").select2().trigger('change'); 
        $("#location_code_add").val(location_code);
        $("#floor_add").val(floor);
        $("#contact_no_add").val(contact_no);
        $("#contact_person_add").val(contact_person);
        $("#is_nursing_station_add").val(is_nursing_station);
        $("#deals_pharmacy_item_add").val(deals_pharmacy_item);
        $("#deals_materials_item_add").val(deals_materials_item);
        $(".status_add").val(status);
      
        $(".saveButton").html('<i class="fa fa-save"></i> Update');
       
        $("#add").html('Edit Location');

    }
    function revert(element){
     
        // let confirm = window.confirm('Are you sure want to restore ?');
        // if(confirm){
            let data = {};
            let url = $(".table_body_contents").attr('data-restore-url');
            let location_id = $(element).parents('tr').attr('data-id');
            data.location_id = location_id;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function () {
                    $("#restore").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                },
                success: function (data) {  
                    $("#restore").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-undo');
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                },
                complete: function () { 

                }
            });
        // }
        
    }
    function deleteItem(element){

        let confirm = window.confirm('Are you sure want to delete ?');
        if(confirm){
            let data = {};
            let url = $(".table_body_contents").attr('data-delete-url');
            let location_id = $(element).parents('tr').attr('data-id');
            data.location_id = location_id;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function () {
                    $(".deleteButton").find('i').removeClass('fa-trash').addClass('fa-spinner').addClass('fa-spin');
                },
                success: function (data) {  
                    $(".deleteButton").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash');
                    Command: toastr["success"]("success");
                    getFilteredItems(window.search_url);
                },
                complete: function () { 

                }
            });
        }
        
    }

  
    function resetForm(){
        $(".reference_id").val('');
        $("#location_add").val('');
        $("#location_type_add").val('').trigger('change');
        $("#location_code_add").val('');
        $("#floor_add").val('');
        $("#contact_no_add").val('');
        $("#contact_person_add").val('');
        $("#add").html('Add Location');
        $('#status_add').removeAttr('checked');
        $('#is_nursing_station_add').removeAttr('checked');
        $('#deals_pharmacy_item_add').removeAttr('checked');
        $('#deals_materials_item_add').removeAttr('checked');
        $(".saveButton").html('<i class="fa fa-save"></i> Save');
    }

