$(document).ready(function(){
    $("#time_taken").datetimepicker({
        format: 'MMM-DD-YYYY hh:mm a',
        maxDate: new Date()
        // .setHours(23, 59, 59),
        // widgetPositioning:{
        //     horizontal: 'auto',
        //     vertical: 'bottom'
        // }
    });
});
function clear_vital_vlues() {
    $("#vital_batch").val('0');
    $("#weight_value").val('');
    $("#temperature_value").val('');
    $("#temperature_value_f").val('');
    $("#height_value").val('');
    $("#bp_systolic").val('');
    $("#bp_diastolic").val('');
    $("#pulse").val('');
    $("#respiration").val('');
    $("#head").val('');
    $("#oxygen").val('');
    $("#bmi").val('');
    $("#spirometry").val('');
    $("#time_taken").val(moment().format('MMM-DD-YYYY hh:mm A'));
    $("#update_vital").text('Save');

    $("#weight_value").focus();
}

$( "#temperature_value").keyup(function() {
    vital_value_c = $("#temperature_value").val();
    if(vital_value_c != ''){
        vital_value_f = ((vital_value_c * (9 / 5)) + 32).toFixed(2);
        $('#temperature_value_f').val(vital_value_f);
    }else{
        $('#temperature_value_f').val('');
    }
});


$( "#temperature_value_f").keyup(function() {
    vital_value_f = $("#temperature_value_f").val();
    if(vital_value_f != ''){
        vital_value_c = ((vital_value_f - 32) * (5 / 9)).toFixed(2);
        $('#temperature_value').val(vital_value_c);
    }else{
        $('#temperature_value').val('');
    }
});

function edit_vital_values(batch) {

    $("#vital_batch").val(batch);
    $("#weight_value").val($("#weight_value").attr('value'));
    $("#temperature_value").val($("#temperature_value").attr('value'));
    $("#temperature_value_f").val($("#temperature_value_f").attr('value'));
    $("#height_value").val($("#height_value").attr('value'));
    $("#bp_systolic").val($("#bp_systolic").attr('value'));
    $("#bp_diastolic").val($("#bp_diastolic").attr('value'));
    $("#pulse").val($("#pulse").attr('value'));
    $("#respiration").val($("#respiration").attr('value'));
    $("#head").val($("#head").attr('value'));
    $("#oxygen").val($("#oxygen").attr('value'));
    $("#bmi").val($("#bmi").attr('value'));
    $("#spirometry").val($("#spirometry").attr('value'));
    $("#time_taken").val(moment().format('MMM-DD-YYYY hh:mm A'));
}
function saveVitals() {
    var vital_value = {};
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    let patient_id = $('#patient_id').val();
    let visit_id = $('#visit_id').val();
    let encounter_id = $('#encounter_id').val();
    let time_taken = $('#time_taken').val();
    let vital_batch = $('#vital_batch').val();
    if ($("#weight").val() == 1) {
        if ($("#weight_value").val() != '') {
            vital_value["1"] = $("#weight_value").val();
            vital_value["2"] = (($("#weight_value").val()) / 2.205).toFixed(2);
        }

    }
    if ($("#weight").val() == 2) {
        if ($("#weight_value").val() != '') {
            vital_value["2"] = $("#weight_value").val();
            vital_value["1"] = (($("#weight_value").val()) * 2.205).toFixed(2);
        }

    }



    if ($("#temperature").val() == 9) {
        if ($("#temperature_value_f").val() != '') {
            vital_value["9"] = $("#temperature_value_f").val();
            vital_value["10"] = (($("#temperature_value_f").val() - 32) * (5 / 9)).toFixed(2);
        }

    }
    if ($("#temperature").val() == 10) {
        if ($("#temperature_value").val() != '') {
            vital_value["10"] = $("#temperature_value").val();
            vital_value["9"] = (($("#temperature_value").val() * (9 / 5)) + 32).toFixed(2);
        }

    }


    if ($("#height").val() == 3) {
        if ($("#height_value").val() != '') {
            vital_value["3"] = $("#height_value").val();
            vital_value["4"] = (($("#height_value").val()) * (30.48)).toFixed(2);
        }

    }
    if ($("#height").val() == 4) {
        if ($("#height_value").val() != '') {
            vital_value["4"] = $("#height_value").val();
            vital_value["3"] = ((($("#height_value").val()) / 30.48).toFixed(2));
            ;
        }

    }
    if ($("#bp_systolic").val() != '') {
        vital_value["5"] = $("#bp_systolic").val();
    }
    if ($("#bp_diastolic").val() != '') {
        vital_value["6"] = $("#bp_diastolic").val();
    }
    if ($("#pulse").val() != '') {
        vital_value["7"] = $("#pulse").val();
    }
    if ($("#respiration").val() != '') {
        vital_value["8"] = $("#respiration").val();
    }
    if ($("#temperature_location").val() != '') {
        vital_value["11"] = $("#temperature_location").val();
    }

    if ($("#oxygen").val() != '') {
        vital_value["12"] = $("#oxygen").val();
    }
    if ($("#head").val() != '') {
        vital_value["13"] = $("#head").val();
    }
    if ($("#bmi").val() != '') {
        vital_value["14"] = $("#bmi").val();
    }
    if ($("#spirometry").val() != '') {
        vital_value["20"] = $("#spirometry").val();
    }

    var vitals_length = Object.keys(vital_value).length;
    if(vitals_length < 2){
        toastr.info('Enter patient vitals')
        return;
    }

    $.ajax({
        type: "POST",
        url: url + "/emr/add_vital",
        data: {
            vital_array: vital_value,
            patient_id: patient_id,
            visit_id: visit_id,
            encounter_id: encounter_id,
            time_taken: time_taken,
            vital_batch: vital_batch,
            _token: _token
        },
        beforeSend: function () {
             $('#vital_tble').html('<tr><td colspan="5" class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></td></tr>');
        },
        success: function (data) {
            console.log(data);
            if(data){
                $("#vital_entry_details").load(" #vital_entry_details");
                Command: toastr["success"]("Save.");
                $('#vital_batch').val(data);
                $("#edit_vital_modal_btn").show();
                $("#editvitalModal").modal('hide');
                $("#weight_value").attr('value', $("#weight_value").val());
                $("#temperature_value").attr('value', $("#temperature_value").val());
                $("#temperature_value_f").attr('value', $("#temperature_value_f").val());
                $("#height_value").attr('value', $("#height_value").val());
                $("#bp_systolic").attr('value', $("#bp_systolic").val());
                $("#bp_diastolic").attr('value', $("#bp_diastolic").val());
                $("#pulse").attr('value', $("#pulse").val());
                $("#respiration").attr('value', $("#respiration").val());
                $("#head").attr('value', $("#head").val());
                $("#oxygen").attr('value', $("#oxygen").val());
                $("#bmi").attr('value', $("#bmi").val());
                $("#spirometry").attr('value', $("#spirometry").val());
            }

            if( typeof fetchVitalsIntoGynec == 'function'){
                fetchVitalsIntoGynec();
            }
        },
        complete: function () {
          //  $('#editvitalModal').modal('toggle');
        }
    });
}

//==============================================================================
function weightSelect(e) {
    var weight_val = e.value;
    if (weight_val == 1) {
        if ($("#weight_value").val() != '') {
            var weight_result = (($("#weight_value").val()) * 2.205).toFixed(2);
            $("#weight_value").val(weight_result);
        }

    }

    if (weight_val == 2) {
        if ($("#weight_value").val() != '') {
            var weight_result = (($("#weight_value").val()) / 2.205).toFixed(2);
            $("#weight_value").val(weight_result);
        }

    }
}
function temperatureSelect(e) {
    var temp_val = e.value;
    if (temp_val == 9) {
        if ($("#temperature_value").val() != '') {
            var temp_result = (($("#temperature_value").val() * (9 / 5)) + 32).toFixed(2);
            $("#temperature_value").val(temp_result);
        }

    }
    if (temp_val == 10) {
        if ($("#temperature_value").val() != '') {
            var temp_result = (($("#temperature_value").val() - 32) * (5 / 9)).toFixed(2);
             $("#temperature_value").val(temp_result);
        }

    }
}
function heightSelect(e) {
     var height_val = e.value;
    if (height_val == 3) {
        if ($("#height_value").val() != '') {
            var height_result = (($("#height_value").val()) / 30.48).toFixed(2);
            $("#height_value").val(height_result);
        }

    }
    if (height_val == 4) {
        if ($("#height_value").val() != '') {
            var height_result = (($("#height_value").val()) * (30.48)).toFixed(2);
            $("#height_value").val(height_result);
        }

    }
}

function show_vital_history(){
    let patient_id = $('#patient_id').val();
    let lstVitalItems = $("#lstVitalItems").val();
    let from_date = $("#vital_graph_from_date").val();
    let to_date = $("#vital_graph_to_date").val();
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
        $.ajax({
        type: "POST",
        url: url + "/emr/show_vital_history",
        dataType: "json",
        data: {
            patient_id: patient_id,
            vital_mster_id: lstVitalItems,
            from_date:from_date,
            to_date:to_date,
            _token: _token
        },
        beforeSend: function () {
        $('#vital_his_table').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
        $('#vital_graph').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');
        },
        success: function (data) {
            //console.log(data);
// vital graph chart starts here
var datas = data;
       var chart = Highcharts.chart('vital_graph', {
            chart: {
            type: 'spline'
            },
            title: {
            text: 'Patient Vitals Chart'
             },

            xAxis: {
            type: 'datetime',
             categories: datas[datas.length-1].time_taken,


            },
            yAxis: {
            title: {
            text: 'Vital Values'
            },
            labels: {
            formatter: function () {
            return this.value;
            }
            }
            },
            tooltip: {
            split: true,
            },

            plotOptions: {
            spline: {
            marker: {
            enabled: true
            },
            dataLabels: {
            enabled: true
            },
            enableMouseTracking: true
            }
            },
            series: data
});

    vital_history_table(patient_id,lstVitalItems,from_date,to_date);
    $("#vital_graph_modal").modal('show');
        },
        complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}
function vital_history_table(patient_id,lstVitalItems,from_date,to_date) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
                type: "POST",
                url: url + "/emr/show_vital_history_table",
                data: {
                    patient_id:patient_id,
                    history_table:'table',
                    vital_mster_id: lstVitalItems,
                    from_date:from_date,
                    to_date:to_date,
                    _token: _token
                },
                beforeSend: function () {
                },
                success: function (data) {
                     $('#vital_his_table').html(data);
                },
            });
              }

$('#weight_value,#height_value,#height_value').on('blur',function(){
    let w_val = '';
    let w_type = '';
    let h_val = '';
    let h_type = '';

    w_val = $('#weight_value').val();
    h_val = $('#height_value').val();
    if(h_val == ""){
        h_val = $('#height_value').val();
    }
    w_type = $('#weight').val();
    h_type = $('#height').val();

    if(w_val != '' && h_val != '' && w_type != ''){
        calculateBmi(w_val,w_type,h_val,h_type);
    }
});

function calculateBmi(weight,wtype,height,htype){

    //convert weight to LBS to KG (weight in KG)
    let weight_type = "";
    let weight_kg = "";
    weight_type = wtype;
    //1 LBS 2 KG
    if(weight_type == "1"){
        weight_kg = Math.round((parseFloat(weight)/2.2046));
    }else{
        weight_kg = Math.round(parseFloat(weight));
    }

    //convert height to (F/I) to M
    //3 INCH 4 CM
    let height_type = "";
    let height_cm = "";
    let height_m = "";
    height_type = htype;
    if(height_type == "3"){
      let vital_value_feet = height;

       //inch to meter
       height_m = parseFloat(vital_value_feet)/3.2808;

    }else{
        height_cm = parseFloat(height);
        height_m = parseFloat(height_cm/100);//convert to meter
    }

    if(weight_kg != '' && height_m != ''){
        let bmi_val = '';
        bmi_val = parseFloat(weight_kg/(height_m*height_m));
        $('input[name="bmi"]').val(bmi_val.toFixed(2));
    }

}
