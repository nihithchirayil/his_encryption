$(document).on('click', '#pending_bills_btn', function(event) {
    fetchPatientPendingBills(true);
});


function fetchPatientPendingBills(show_flag){
    var patient_id = $("#op_no_hidden").val();
    var url = $('#base_url').val() + "/pharmacy/getPendingBills";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if (data.count > 0) {
                
                if(show_flag == true){
                    $("#pending_bills_modal").modal('show');
                    $("#pending_bills_list_data").html(data.result);
                } else {
                    if($("#pending_bill_show_config").length > 0){
                        if ($("#pending_bill_show_config").val().toString() == '1') {
                            $("#pending_bills_modal").modal('show');
                            $("#pending_bills_list_data").html(data.result);
                        }
                    } else {
                        $("#pending_bills_modal").modal('show');
                        $("#pending_bills_list_data").html(data.result);
                    }
                }
                
                $(".outstanding_amount").val(data.outstanding_amount);

                var $table = $('table.theadfix_wrapper');

                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }
                });

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            } else {
                Command: toastr["success"]("No pending bills..!");
            }

        },
        complete: function() {
            $("body").LoadingOverlay("hide");

            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            }, 500);
        }
    });
}