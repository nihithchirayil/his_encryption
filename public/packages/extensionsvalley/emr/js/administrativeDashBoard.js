$(document).ready(function () {
    getAllGraphs()
    getCustomDates(1);
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});

var base_url = $('#base_url').val();
var token = $('#c_token').val();

function getAllGraphs() {
    setTimeout(function () {
        getOtUtilization();
    }, 200);
    setTimeout(function () {
        //  getIcuOccupancy();
    }, 300);
    setTimeout(function () {
        //getWardOccupancy();
    }, 400);
    setTimeout(function () {
        // getRoomOccupancy();
    }, 500);
    setTimeout(function () {
        getTatOutleries();
    }, 600);
    setTimeout(function () {
        //  getIpSatisfaction();
    }, 600);
    setTimeout(function () {
        //  getOpSatisfaction();
    }, 700);
    setTimeout(function () {
        //  getEmployeeSatisfaction();
    }, 600);

}

function getCustomDates(from_type) {
    var date_data = $('#date_datahidden').val();
    var obj = JSON.parse(date_data);
    $('#daterange_hidden').val(from_type);
    if (from_type == '1') {
        $('#from_date').val(obj.firstday_thismonth);
        $('#to_date').val(obj.current_date);
        $('#from_datadis').html(obj.firstday_thismonth);
        $('#to_datadis').html(obj.current_date);
        $('#range_typedata').html('Current Month');
        getAdministrativeDashboardData();
    } else if (from_type == '2') {
        $('#from_date').val(obj.firstday_lastmonth);
        $('#to_date').val(obj.lastday_lastmonth);
        $('#from_datadis').html(obj.firstday_lastmonth);
        $('#to_datadis').html(obj.lastday_lastmonth);
        $('#range_typedata').html('Previous Month');
        getAdministrativeDashboardData();
    } else if (from_type == '3') {
        $('#from_date').val(obj.firstday_last3month);
        $('#to_date').val(obj.lastday_previousmonth);
        $('#from_datadis').html(obj.firstday_last3month);
        $('#to_datadis').html(obj.lastday_previousmonth);
        $('#range_typedata').html('Last 3 Months');
        getAdministrativeDashboardData();
    } else if (from_type == '4') {
        $('#from_date').val(obj.firstday_last6month);
        $('#to_date').val(obj.lastday_previousmonth);
        $('#from_datadis').html(obj.firstday_last6month);
        $('#to_datadis').html(obj.lastday_previousmonth);
        $('#range_typedata').html('Last 6 Months');
        getAdministrativeDashboardData();
    } else if (from_type == '5') {
        $('#from_date').val(obj.startday_lastyear);
        $('#to_date').val(obj.endday_lastyear);
        $('#from_datadis').html(obj.startday_lastyear);
        $('#to_datadis').html(obj.endday_lastyear);
        $('#range_typedata').html('Last 1 Year');
        getAdministrativeDashboardData();
    } else if (from_type == '7') {
        $("#customdatapopmodel").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

}

function getCustomDateRange() {
    var from_date = $('#from_date').val();
    var todate = $('#to_date').val();
    $('#range_typedata').html('Custom Date Range Selection');
    $('#from_datadis').html(from_date);
    $('#to_datadis').html(todate);
    getAdministrativeDashboardData();
    $("#customdatapopmodel").modal('toggle');
}

function getAdministrativeDashboardData() {

    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var param = { _token: token, from_date: from_date, to_date: to_date };

    var url = base_url + "/master/getAdministrativeDashboardData";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            obj = JSON.parse(html);
            console.log(obj.SatisfactionIndex);
            getIcuOccupancy(obj.icuOccupancyData.not_occupied, obj.icuOccupancyData.occupied);
            getRoomOccupancy(obj.RoomOccupancyData.not_occupied, obj.RoomOccupancyData.occupied);
            getWardOccupancy(obj.WardOccupancyData.not_occupied, obj.WardOccupancyData.occupied);
            getIpSatisfaction(obj.SatisfactionIndex.ip_satisfaction);
            getOpSatisfaction(obj.SatisfactionIndex.op_satisfaction);
            getEmployeeSatisfaction(obj.SatisfactionIndex.employee_satisfaction);
            $('#overall_compliance').html(obj.SatisfactionIndex.overall_compliance + '%');
            $('#mortality_count').html(obj.mortality + '%');
            $('#sentinalevent_count').html(obj.sentinalevent);
            $('#AvgDischargeTime').html(obj.AvgDischargeTime);
            $('#CriticalDowntime').html(obj.CriticalDowntime);

            $('#vat_value').html(obj.InfectionControlParams['vap']);
            $('#pressure_ulcer_rate_value').html(obj.InfectionControlParams['pressure_ulcer_rate']);
            $('#surgical_site_infection_value').html(obj.InfectionControlParams['surgical_site_infection']);
            $('#hand_hygiene_rate_value').html(obj.InfectionControlParams['hand_hygiene_rate']);
            $('#catheter_associated_uti_value').html(obj.InfectionControlParams['catheter_associated_uti']);
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}

function getOtUtilization() {
    Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                    [0, color],
                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        })
    });

    // Build the chart
    Highcharts.chart('ot_utilization', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: 'Utilized', y: 40 },
                { name: 'Not Utilized', y: 60 },
            ]
        }]
    });
}
function getIcuOccupancy(not_occupied, occupied) {

    Highcharts.chart('icu_occupancy', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: 'Occupied', y: occupied },
                { name: 'Vacant', y: not_occupied },
            ]
        }]
    });
}
function getWardOccupancy(not_occupied, occupied) {

    // Build the chart
    Highcharts.chart('ward_occupancy', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: 'Occupied', y: occupied },
                { name: 'Vacant', y: not_occupied },
            ]
        }]
    });
}
function getRoomOccupancy(not_occupied, occupied) {

    // Build the chart
    Highcharts.chart('room_occupancy', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: 'Occupied', y: occupied },
                { name: 'Vacant', y: not_occupied },
            ]
        }]
    });
}
function getTatOutleries() {

    // Build the chart
    Highcharts.chart('tat_outleries', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: 'N', y: 51 },
                { name: 'Y', y: 49 },
            ]
        }]
    });
}
function getIpSatisfaction(ip_satisfaction) {

    // Build the chart
    Highcharts.chart('ip_satisfaction', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { y: ip_satisfaction, name: '85% Satisfied' },
                { y: 100 - ip_satisfaction, name: '15% Dissatisfied' },
            ]
        }]
    });
}
function getOpSatisfaction(op_satisfaction) {

    // Build the chart
    Highcharts.chart('op_satisfaction', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: '85% Satisfied', y: op_satisfaction },
                { name: '15% Dissatisfied', y: 100 - op_satisfaction },
            ]
        }]
    });
}
function getEmployeeSatisfaction(employee_satisfaction) {

    // Build the chart
    Highcharts.chart('employee_satisfaction', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: 125
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                },
                // showInLegend: true,

                center: ['50%', '50%'],
                startAngle: -90,
                endAngle: 360,
                size: '100%'
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: '85% Satisfied', y: employee_satisfaction },
                { name: '15% Dissatisfied', y: 100 - employee_satisfaction },
            ]
        }]
    });
}

function getOtUtilizationDetail() {

    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var param = { _token: token, from_date: from_date, to_date: to_date };

    var url = base_url + "/master/getOtUtilizationDetail";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#drilldownmodel').modal('show');
            $("#drilldownmodel").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            $("#drilldownmodel").LoadingOverlay("hide");
            $("#drilldownbody").html(html);
            $('#drilldowntitle').html('Ot Utlization Detail');

        },
        complete: function () {

        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });

}

