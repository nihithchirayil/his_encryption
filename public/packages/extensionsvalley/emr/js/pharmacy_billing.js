var token = $("#c_token").val();
var base_url = $("#base_url").val();

$(document).on('ready', function() {
    addNewRow();
    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

    $('.theadfix_wrapper').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

    // $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $("#op_no").focus();

});

$(document).on('click', '.addNewRowBtn', function(event) {
    addNewRow();

});

$(document).on('click', '.advanceSearchBtn', function(event) {
    advancePatientSearch(1);
});

$(document).on('click', '#select_prescription_btn', function(event) {
    showIndentScreen();
});

$(document).on('keyup', function(e) {
    if (e.key == "Escape") {
        $(".medicine-list-div-row-listing").hide();
        $("#pending_bills_modal").modal('hide');
        $("#op_noAjaxDiv").hide();

    }
});


$(document).on("click", "#pharmacy_bill_list_btn", function(e) {
    var base_url = $('#base_url').val();
    var url = base_url + "/billlist/billlist";
    document.location.href = url;
});

$(document).on('click', '.filter_option', function(event) {
    if(window.tr){
        var element = $(window.tr).find('.item_description');
        last_search_string = '';
        searchPharmacyItem(element.val(), element);
    }

});

$(window).load(function() {
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    if (location_id != '') {
        $("#billing_location").val(location_id);
    }

    if (parseInt($("#bill_id").val()) > 0) {
        fetchBillDetails($("#bill_id").val());
    }
    $(".select2").select2()

    $(".presc_from_date").val($("#current_date").val());
    $(".presc_to_date").val($("#current_date").val());
    $("#op_no").focus();
})

$(document).on('input', '.number_class', function() {
    let cur_obj = $(this);
    let cur_obj_value = $(this).val();
    $(cur_obj).val(cur_obj_value.replace(/[^0-9]/g, ""));
});
$(document).on('keyup', '.quantity', function() {
    //var item_code = $(this).parents('tr.row_class').find('.item_price ').attr("data-item-code");
    var item_code = $(this).parents('tr.row_class').find('.item_description').attr("data-item-code");
    console.log(item_code);
    var location_code = $("#billing_location").val();
    var qty = $(this).val();
    var current_batch_stock = $(this).parents('tr.row_class').find('.item_description').attr('data-stock');
    var current_batch = $(this).parents('tr.row_class').find('.batch').val();
    if (qty == 0) {
        qty = '';
        $(this).val(0);
        $(this).parents('tr.row_class').find('.discount_amount ').val(0);
        $('.discount_value').val('');
    }


    if ((!current_batch_stock || parseInt(qty) > parseInt(current_batch_stock)) && qty) {
        checkStockData(item_code, location_code, qty, current_batch);
    }
    // calculateTotalAmount();
    $(this).closest('tr').find('.item_discount_amount').trigger('keyup');
    setDisAmt();

});

$(document).on('click', '#out_side_doctor', function(event) {
    if ($("#out_side_doctor").prop('checked')) {
        $("#outside_doctor_name").removeAttr('disabled');
        $("#outside_doctor_name").removeAttr('readonly');
        $("#doctor").attr('disabled', true);
        $("#doctor").attr('readonly', true);
        $("#outside_doctor_name").focus();
    } else {
        $("#outside_doctor_name").attr('disabled', true);
        $("#outside_doctor_name").attr('readonly', true);
        $("#doctor").removeAttr('disabled');
        $("#doctor").removeAttr('readonly');
    }
});

$(document).on('click', '.medicine-list-div-row-listing > .close_btn_med_search', function(event) {
    event.preventDefault();
    $(".medicine-list-div-row-listing").hide();
});

$(document).on('click', '.list-medicine-search-data-row-listing > tr', function(event) {
    event.preventDefault();
    var batch = $(event.currentTarget).attr('data-batch-no');
    var item_code = $(event.currentTarget).attr('data-item-code');
    var location_code = $("#billing_location").val();
    var base_url = $('#base_url').val();
    var url = base_url + "/pharmacy/batchWiseItemList";
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    var no_gst_for_ip_patient = $("#no_gst_for_ip_patient").val();
    var visit_status = $("#visit_status").val();
    $.ajax({
        url: url,
        type: "POST",
        data: {
            visit_id:visit_id,
            patient_id:patient_id,
            batch:batch,
            item_code:item_code,
            location_code:location_code
        },
        beforeSend:function (){
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });

        },
        success: function(data) {
            if(data){
             var obj = JSON.parse(data);
           //  console.log(obj);
             if ($(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']").length > 0) {
                if($(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']").parents('tr').find('.batch').val() == obj['item_details'][0].batch_no){
                    $('.medicine-list-div-row-listing').hide();
                    bootbox.alert('Item already exists.');
                    $(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']").parents('tr').find('.batch[value="'+obj['item_details'][0].batch_no+'"]').addClass('alerts-border');

                    setTimeout(function() {
                        $(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']").removeClass('alerts-border');
                        var end = $($(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']")[0]).val().length;
                        $(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']")[0].setSelectionRange(end, end);
                        $($(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']")[0]).focus();
                        $(window).scrollTop($(".item_description[data-item-code='" + obj['item_details'][0].item_code + "']").position().top);
                    }, 3000)

                    return;
                }


            }
          //   console.log(obj);
            var unit_tax_amount = obj['item_details'][0].unit_selling_price * obj['item_details'][0].unit_tax_rate;
            if(unit_tax_amount > 0){
                unit_tax_amount = unit_tax_amount / 100;
                unit_tax_amount = parseFloat(unit_tax_amount).toFixed(2)
            }
            var unit_selling_price = obj['item_details'][0].unit_selling_price;
            var unit_tax_rate = obj['item_details'][0].unit_tax_rate;
            if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                unit_selling_price = (parseFloat(unit_selling_price) + parseFloat(unit_tax_amount)).toFixed(2);
                unit_tax_rate = parseFloat(0).toFixed(2);
                unit_tax_amount = parseFloat(0).toFixed(2);
            }
               $(window.tr).find('.batch').val(obj['item_details'][0].batch_no);
               $(window.tr).find('.generic_name').val(obj['item_details'][0].generic_name);
               $(window.tr).find('.expiry_date').val(obj['item_details'][0].expiry_date);
               $(window.tr).find('.rack').val(obj['item_details'][0].rack);
               $(window.tr).find('.mrp').val(obj['item_details'][0].unit_mrp);
               $(window.tr).find('.discount_amount').val(obj['item_details'][0].ret_disc_amount);
               $(window.tr).find('.discount_amount').attr('discount-type', obj['item_details'][0].ret_disc_type);
               $(window.tr).find('.discount_amount').attr('discount-percentage', obj['item_details'][0].ret_disc_per);
               $(window.tr).find('.item_price').val(unit_selling_price);
               $(window.tr).find('.item_price').attr('actual-selling-price', unit_selling_price);
               $(window.tr).find('.tax').val(unit_tax_rate);
            //    var unit_tax_amount = obj['item_details'][0].unit_selling_price * obj['item_details'][0].unit_tax_rate;
            //    if(unit_tax_amount > 0){
            //        unit_tax_amount = unit_tax_amount / 100;
            //        unit_tax_amount = parseFloat(unit_tax_amount).toFixed(2)
            //    }

               $(window.tr).find('.tax_amount').val(unit_tax_amount);
               $(window.tr).find('.total_amount').val();
               $(window.tr).find('.item_description').val(obj['item_details'][0].item_desc);
               $(window.tr).find('.item_description').attr('data-item-code', obj['item_details'][0].item_code);
               $(window.tr).find('.item_description').attr('data-stock', obj['item_details'][0].stock);
               $(window.tr).find('.quantity').focus();
               delete window.tr;
               $(".medicine-list-div-row-listing").hide();
              // getLocationWiseItemStock(obj['item_details'][0].item_code);
               addNewRow();
               let response = obj['stock_details'];
               if (response.length > 0) {
                   var item_desc = $($(".item_description[data-item-code='" + item_code + "']")[0]).val();
                   $(".location_details_body").append('<tr><td colspan="2" style="text-align:center;" ><span><b>' + item_desc + '</b></span></td></tr>');
                   $.each(response, function(key, value) {
                       $(".location_details_body").append('<tr><td style="text-align:left;" ><span>' + value.location_name + '</span></td><td><span>' + value.stock + '</span></td></tr>');
                   });
               } else {
                   $(".location_details_body").append('<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>');
               }
                    }
        },
        complete: function(){
            $("body").LoadingOverlay("hide");

        }
    });
    // var discount_amount = $(event.currentTarget).attr('data-discount-amount');
    // var discount_percentage = $(event.currentTarget).attr('data-discount-percentage');
    // var actual_selling_price = $(event.currentTarget).attr('data-actual-selling-price');
    // var discount_type = $(event.currentTarget).attr('data-discount-type');
    // var genreric_name = $(event.currentTarget).attr('data-genreric-name');
    // var tax_rate = $(event.currentTarget).attr('data-unit-tax-rate');
    // var tax_amount = $(event.currentTarget).attr('data-unit-tax-amount');
    // var rack = $(event.currentTarget).attr('data-rack-name');
    // var expiry_date = $(event.currentTarget).attr('data-expiry-date');
    // var price = $(event.currentTarget).attr('data-effective-price');
    // var mrp = $(event.currentTarget).attr('data-unit-mrp');
    // var data_stock = $(event.currentTarget).attr('data-stock');
    // var is_discountable = $(event.currentTarget).attr('data-is_discountable');







});


function addNewRow() {
    var row_length = 0;
    var medicine_length = 0;
    $('.item_description').each(function(key, value) {
        if (!$(value).val())
            row_length = row_length + 1;
        else
            medicine_length = medicine_length + 1;
    });
    if (row_length > 0)
        return;

    if (medicine_length > 0 && $("#op_no").val()) {
        $("#op_no").attr('disabled', true);
        $("#out_side_patient").attr('disabled', true);
        $("#billing_location").attr('disabled', true);
    } else {
        $("#op_no").attr('disabled', false);
        $("#out_side_patient").attr('disabled', false);
        $("#billing_location").attr('disabled', false);
    }

    var item_wise_discount = $('#item_wise_discount').val();
    var disabled = '';
    if (parseInt(item_wise_discount) == 0) {
        disabled = 'disabled';
    } 

    var discount = '<td><select id="item_discount_type" class="form-control item_discount_type" '+ disabled +'><option value="">Discount Type</option><option value="1">Percentage(%)</option><option value="2">Amount</option></select></td><td><input class="form-control item_discount_amount number_class" type="text" value="" attr-item-discount="0" '+ disabled +'></td>';

    var row_html = '<tr style="background: #FFF;" class="row_class"><td class="serial_no">1</td><td><input class="form-control item_description" type="text" value=""></td><td><input class="form-control batch" readonly disabled type="text" value=""></td><td><input class="form-control generic_name" readonly disabled  type="text" value=""></td><td><input class="form-control expiry_date" readonly disabled type="text" value=""></td><td><input class="form-control rack" readonly disabled type="text" value=""></td><td><input class="form-control mrp number_class" readonly disabled type="text" value=""></td><td><input class="form-control quantity number_class" type="text" value=""></td><td><input class="form-control discount_amount number_class" readonly disabled type="text" value=""></td><td><input class="form-control item_price number_class" readonly disabled type="text" value="0"></td><td><input class="form-control tax number_class" readonly disabled type="text" value="0"></td><td><input class="form-control tax_amount number_class" readonly disabled type="text" value="0"></td>'+ discount +'<td class="HideIpPatient"><input class="form-control total_tax_amount number_class" readonly disabled type="text" value="0"></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="0"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
    $(".pharmacy_billing_table_body").append(row_html);
    resetSerialNumbers();

    var visit_status = $("#visit_status").val();
    var no_gst_for_ip_patient = $("#no_gst_for_ip_patient").val();
    if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
        $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').hide();
    } else {
        $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').show();
    }

}

function deleteRow(button) {
    var medicine_length = 0;
    $('.item_description').each(function(key, value) {
        if ($(value).val())
            medicine_length = medicine_length + 1;
    });

    if (medicine_length > 0 && $("#op_no").val()) {
        $("#op_no").attr('disabled', true);
        $("#out_side_patient").attr('disabled', true);
        $("#billing_location").attr('disabled', true);
    } else {
        $("#op_no").attr('disabled', false);
        $("#out_side_patient").attr('disabled', false);
        $("#billing_location").attr('disabled', false);
    }

    if ($("#bill_id").val() != "0") {
        $(button).parents('tr.row_class').removeClass('row_class').addClass('row_class_removed').hide();
    } else {
        $(button).parents('tr.row_class').remove();
    }

  calculateTotalAmount();
            resetSerialNumbers();
            setDisAmt();
}

function resetSerialNumbers() {
    $('tr.row_class').find(".serial_no").each(function(key, val) {
        $(this).html(key + 1);
    })
}


var timeout = null;
var last_search_string = '';
$(document).on('keyup', '.item_description', function(event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        return false;
    }
    /* Act on the event */
    var obj = $(this);
    var search_string = $(this).val().trim();
    searchPharmacyItem(search_string, obj);

});

var searchMedicineAjax = '';
function searchPharmacyItem(search_string, obj) {
    if($('#billing_location').val()=='' || $('#billing_location').val()==null){
        toastr.warning('Please select a billing location.');
        return;
    }
    if($('#op_no').val()=='' || $('#op_no').val()==null){
        toastr.warning('Please select a patinet.');
        return;
    }
    var out_of_stock = ($("#out_of_stock").prop("checked")) ? 1 : 0;
    var another_batch_item = ($("#another_batch_item").prop("checked")) ? 1 : 0;
    var generic_name_wise = ($("#generic_name_wise").prop("checked")) ? 1 : 0;
    var contain_search = ($("#contain_search").prop("checked")) ? 1 : 0;
    var patient_id = $("#op_no_hidden").val();
    var billing_location = $("#billing_location").val();
    let tr = $(obj).closest('tr');
    window.tr = tr;
    let row_id = $(obj).closest('tr').html();
    var no_gst_for_ip_patient = $("#no_gst_for_ip_patient").val();
    var visit_status = $("#visit_status").val();
    if (search_string == "") {
        $(tr).find('.batch').val("");
        $(tr).find('.generic_name').val("");
        $(tr).find('.expiry_date').val("");
        $(tr).find('.rack').val("");
        $(tr).find('.mrp').val(0);
        $(tr).find('.quantity').val('');
        $(tr).find('.item_price').val(0);
        $(tr).find('.tax').val(0);
        $(tr).find('.tax_amount').val(0);
        $(tr).find('.total_amount').val(0);
        $('.medicine-list-div-row-listing').hide();
        $(tr).find('.item_description').attr('data-item-code', '');
        $(tr).find('.item_description').attr('data-stock', '');
        $(tr).find('.item_price').attr('actual-selling-price', '');
        $(tr).find('.discount_amount').attr('discount-type', '');
        $(tr).find('.discount_amount').attr('discount-percentage', '');
    }

    if (search_string == "" || search_string.length < 3) {
        last_search_string = '';
        return false;
    } else {
        var med_list = $('.medicine-list-div-row-listing');
        $('.list-medicine-search-data-row-listing').empty();
        $(med_list).show();
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            if (search_string == last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/pharmacy/searchPharmacyItem";
            searchMedicineAjax = $.ajax({
                type: "POST",
                url: url,
                data: {
                    patient_id: patient_id,
                    billing_location: billing_location,
                    search_string: search_string,
                    out_of_stock: out_of_stock,
                    another_batch_item: another_batch_item,
                    generic_name_wise: generic_name_wise,
                    contain_search: contain_search
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                    $('.item_description').blur();
                },
                success: function(data) {

                    let response = data.medicine_list;
                    let res_data = "";

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            var item_desc = response[i].item_desc;
                            var item_code = response[i].item_code;
                            var generic_name = response[i].generic_name;
                            var stock = response[i].stock;
                            var current_batch_stock = response[i].current_batch_stock;
                            var unit_tax_rate = response[i].unit_tax_rate;
                            var unit_tax_amount = response[i].unit_tax_amount;
                            unit_tax_amount = Math.round(unit_tax_amount * 100) / 100
                            var ret_effect_price = response[i].ret_effect_price;
                            var ret_disc_amount = response[i].ret_disc_amount;
                            var expiry_date = response[i].expiry_date;
                            var rack_name = response[i].rack_name;
                            var unit_mrp = response[i].unit_mrp;
                            var batch_no = response[i].batch_no;
                            var ret_disc_type = response[i].ret_disc_type;
                            var ret_disc_percentage = response[i].ret_disc_per;
                            var ret_selling_price = response[i].ret_selling_price;
                            var is_discountable = response[i].is_discountable;
                            var is_returnable = response[i].returnable_ind;
                            console.log(is_returnable);
                            if(is_returnable==1){
                                   $tr_color="<i class='fa fa-check green'></i>";
                            }else{
                                $tr_color="<i class='fa fa-times red'></i>";
                            }
                            var nostock = '';
                            if (!stock) {
                                nostock = 'no-stock';
                            }
                            var price_for_ip_patient = ret_effect_price;
                            if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                                price_for_ip_patient = (parseFloat(price_for_ip_patient) + parseFloat(unit_tax_amount)).toFixed(2);
                            }

                            res_data += '<tr style="background:'+$tr_color+'"  data-is_discountable="'+is_discountable+'" data-actual-selling-price="' + ret_selling_price + '" data-discount-type="' + ret_disc_type + '" data-item-code="' + item_code + '" data-item-desc="' + item_desc + '" data-genreric-name="' + generic_name + '" data-stock="' + current_batch_stock + '" data-unit-tax-rate="' + unit_tax_rate + '" data-rack-name="' + rack_name + '" data-unit-tax-amount="' + unit_tax_amount + '" data-effective-price="' + ret_effect_price + '" data-expiry-date="' + expiry_date + '" data-unit-mrp="' + unit_mrp + '" data-batch-no="' + batch_no + '" data-discount-amount="' + ret_disc_amount + '" data-discount-percentage="' + ret_disc_percentage + '"><td class="' + nostock + '">' + item_desc + '</td><td>' + generic_name + '</td><td class="search_list_batch_no">' + batch_no + '</td><td>' + stock + '</td><td>' + price_for_ip_patient + '</td><td>' + $tr_color + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="4">No Data Found..!</td></tr>';
                    }

                    $(".list-medicine-search-data-row-listing").html(res_data);
                    last_search_string = search_string;
                    if ($("#another_batch_item").prop("checked")) {
                        $(".search_list_batch_no").show();
                    } else {
                        $(".search_list_batch_no").hide();
                    }
                    // $(".theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                    $('.theadfix_wrapper').floatThead("reflow");
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }
            });
        }, 1000)

    }
}

function getLocationWiseItemStock(item_code) {
    var url = $('#base_url').val() + "/pharmacy/getLocationWiseItemStock";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            item_code: item_code
        },
        beforeSend: function() {
            $(".location_details_body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $(".location_details_body").empty();
            let response = data.stock_details;
            if (response.length > 0) {
                var item_desc = $($(".item_description[data-item-code='" + item_code + "']")[0]).val();
                $(".location_details_body").append('<tr><td colspan="2" style="text-align:center;" ><span><b>' + item_desc + '</b></span></td></tr>');
                $.each(data.stock_details, function(key, value) {
                    $(".location_details_body").append('<tr><td style="text-align:left;" ><span>' + value.location_name + '</span></td><td><span>' + value.stock + '</span></td></tr>');
                });
            } else {
                $(".location_details_body").append('<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>');
            }
        },
        complete: function() {
            $(".location_details_body").LoadingOverlay("hide");
        }
    });
}

function calculateTotalAmount() {
    var item_total = 0.00;
    var total_tax = 0.00;
    var net_amount = 0.00;
    var bill_amount = 0.00;
    var total_discount = 0.00;

    $(".pharmacy_billing_table_body").find('tr.row_class').each(function(key, value) {
        var item = $(value).find('.item_description').val();
        var quantity = $(value).find('.quantity').val();
        var tax_amount = $(value).find('.tax_amount').val();
        var price = $(value).find('.item_price').val();
        var discount = $(value).find('.discount_amount').val();
        var item_discount = $(value).find('.item_discount_amount').attr('attr-item-discount');

        if (quantity && price && item) {

            quantity = parseInt(quantity);
            tax_amount = parseFloat(tax_amount);
            price = parseFloat(price);

            total_tax = parseFloat(total_tax) + parseFloat(tax_amount * quantity);
            item_total = parseFloat(item_total) + parseFloat(price * quantity);
            // item_total = parseFloat(item_total) + parseFloat((price * quantity) - item_discount);
            total_discount = parseFloat(total_discount) + parseFloat(discount);
            var total_tax_amount = parseFloat(tax_amount * quantity).toFixed(2);
            var total_amount = (parseFloat((price + tax_amount) * quantity) - parseFloat(discount)).toFixed(4);
            total_amount =(parseFloat(total_amount) - parseFloat(item_discount)).toFixed(4);
            total_discount = parseFloat(total_discount) + parseFloat(item_discount);
            $(value).find('.total_amount').val(total_amount);
            $(value).find('.total_tax_amount').val(total_tax_amount);

        } else {
            $(value).find('.total_amount').val(0.00);
            $(value).find('.total_tax_amount').val(0.00);
        }
    });

    total_tax = parseFloat(total_tax).toFixed(4);
    item_total = parseFloat(item_total).toFixed(4);
    bill_amount = (parseFloat(item_total) + parseFloat(total_tax)).toFixed(4);
    net_amount = (parseFloat(bill_amount) - parseFloat(total_discount)).toFixed(4);
    total_discount = parseFloat(total_discount).toFixed(2);

    $(".total_tax").val(total_tax);
    $(".item_total").val(net_amount);
    $(".bill_amount").val(bill_amount);
    $(".net_amount").val(net_amount);
    $(".total_discount").val(total_discount);
    $(".total_discount").attr("attr-items-total-dis", total_discount);
}


$('.hidden_search').keyup(function(event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = "";
            $.ajax({
                type: "GET",
                url: "",
                data: 'search_key=' + search_key + '&search_key_id=' + input_id,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();

                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});



function getPatientDetailsByUHID(uhid, clear_rows = true){
    var url = $('#base_url').val() + "/pharmacy/getPatientDetailsByUHID";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            uhid: uhid
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if ($('#select_prescription_modal').hasClass('in')) {
                $("#patient_uhid").val(data.patient_details.uhid);
                $("#patient_uhid_hidden").val(data.patient_details.patient_id);
                $("#patient_uhidAjaxDiv").hide();
                $("#patient_name_presc").val(data.patient_details.patient_name);
                getIndentList();
            } else {
                $("#op_noAjaxDiv").hide();
                $("#patient_id").val(data.patient_details.patient_id);
                $("#op_no").val(data.patient_details.uhid);
                $("#op_no_hidden").val(data.patient_details.patient_id);
                $("#patient_name_bill").html(data.patient_details.patient_name);
                $("#ip_number").html(data.patient_details.admission_no);
                $("#visit_status").val(data.visit_status);
                if (data.visit_status == 'IP') {
                    $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').hide();
                } else {
                    $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').show();
                }

                if(data.patient_details.doctor_id != ""){
                    $("#doctor").val(parseInt(data.patient_details.doctor_id)).trigger('change');
                } else {
                    $("#doctor").val(data.patient_details.doctor_id).trigger('change');
                }
                $("#current_room").html(data.patient_details.room_name);
                $("#company").val(data.patient_details.company_id);
                $("#pricing").val(data.patient_details.pricing_id);
                $(".patient_advance").val(parseFloat(data.patient_details.patient_advance).toFixed(4));
                $("#visit_id").val(data.patient_details.visit_id);
                $("#room_type").val(data.patient_details.room_type_id);
                $("#payment_type").empty();

                $.each(data.payment_types, function(key, value){
                    $("#payment_type").append('<option value="' + key + '">' + value + '</option>');
                });
                if(data.default_payment_type != ''){
                    $('#payment_type option[value="opcredit"]').attr("selected",true);
                }
                fetchPatientPendingBills(false);
                getPatientAllergy(data.patient_details.patient_id);

                if(clear_rows == true){
                    $(".pharmacy_billing_table_body").empty();
                }

                addNewRow();

            }
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}



function fillSearchDetialsPharmacy(id, uhid, name, gender, age, phone, ip_number, serach_key_id, doctor_id) {
    getPatientDetailsByUHID(uhid);
}

function getPaymentTypesAndDoctor(patient_id,pay_type='') {
    var url = $('#base_url').val() + "/pharmacy/getPaymentTypes";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $("#payment_type").empty();
            $.each(data.payment_types, function(key, value) {
                $("#payment_type").append('<option value="' + key + '">' + value + '</option>');
            });

            if (Object.keys(data.payment_types).length > 0) {
                $("#payment_type").val($("#payment_type").find('option:first').val()).trigger('change');
            }

            if(data.auto_select_doctor_id && data.auto_select_doctor_id != ""){
                $("#doctor").val(data.auto_select_doctor_id).trigger('change');
            }
            $("#payment_type").val(pay_type);
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}


var timeout1 = null;

function checkStockData(item_code, location_code, qty, current_batch) {

    var item_wise_discount = $('#item_wise_discount').val();
    var disabled = '';
    if (parseInt(item_wise_discount) == 0) {
        disabled = 'disabled';
    }

    var patient_id = $("#op_no_hidden").val();
    var billing_location = $("#billing_location").val();
    var no_gst_for_ip_patient = $("#no_gst_for_ip_patient").val();
    var visit_status = $("#visit_status").val();

    clearTimeout(timeout1);
    timeout1 = setTimeout(function() {

        var url = $('#base_url').val() + "/pharmacy/checkStockData";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                patient_id: patient_id,
                billing_location: billing_location,
                item_code: item_code,
                location_code: location_code,
                qty: qty
            },
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
            },
            success: function(data) {
                // console.log(total_stock);
                // console.log(qty);
                if (data.result.length > 0) {
                    var total_stock = data.result[0].r_total_stock;
                    var item_desc = data.result[0].item_desc;

                    if (parseInt(qty) > parseInt(total_stock)) {
                        if (parseInt($("#bill_id").val()) > 0) {
                            var old_quantity = $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').find('.quantity').attr('data-old-quantity');
                            $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').find('.quantity').val(old_quantity);
                        } else {
                            Command: toastr["error"]("Item out of stock");
                            $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').find('.quantity').val('').trigger('keyup');
                        }
                    } else {

                        var current_batch_out_of_stock = false;
                        $.each(data.result, function(key1, val1) {
                            if ((parseInt(qty) > parseInt(val1.r_stock)) && (current_batch == val1.r_batch_no)) {
                                current_batch_out_of_stock = true;
                            }
                        });

                        if (current_batch_out_of_stock == true) {
                            bootbox.confirm({
                                message: 'Insufficient stock for ' + item_desc.toUpperCase() + '. \n Do you want to add from another batch ?',
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-success',
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                callback: function(result) {
                                    if (result) {
                                        $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').remove();
                                        $(".item_description").each(function(key, val) {
                                            if ($(val).val() == "") {
                                                $(val).parents('tr.row_class').remove();
                                            }
                                        });

                                        $.each(data.result, function(key, val) {
                                            if (parseInt(val.r_qty) > 0) {
                                                var r_tax = val.r_tax;
                                                var r_tax_amount = val.r_tax_amount;
                                                var ret_effect_price = val.ret_effect_price;
                                                var ret_selling_price = val.ret_selling_price;
                                                if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                                                    ret_effect_price = (parseFloat(ret_effect_price) + parseFloat(r_tax_amount)).toFixed(2);
                                                    ret_selling_price = (parseFloat(ret_selling_price) + parseFloat(r_tax_amount)).toFixed(2);
                                                    r_tax = parseFloat(0).toFixed(2);
                                                    r_tax_amount = parseFloat(0).toFixed(2);
                                                }
                                                var discount = '<td><select id="item_discount_type" class="form-control item_discount_type" ' + disabled + '><option value="">Discount Type</option><option value="1">Percentage(%)</option><option value="2">Amount</option></select></td><td><input class="form-control item_discount_amount number_class" type="text" value="" attr-item-discount="0" ' + disabled + '></td>';

                                                var total_tax_amt = parseFloat(parseInt(val.r_qty) * parseFloat(val.r_tax_amount)).toFixed(2);
                                                var row_html = '<tr style="background: #FFF;" class="row_class"><td class="serial_no"></td><td><input class="form-control item_description" type="text" value="' + val.item_desc + '" data-item-code="' + val.r_item_code + '" data-stock="' + val.r_stock + '"></td><td><input class="form-control batch" readonly disabled type="text" value="' + val.r_batch_no + '"></td><td><input class="form-control generic_name" readonly disabled  type="text" value="' + val.generic_name + '"></td><td><input class="form-control expiry_date" readonly disabled type="text" value="' + val.r_expiry_date + '"></td><td><input class="form-control rack" readonly disabled type="text" value="' + val.rack_name + '"></td><td><input class="form-control mrp number_class" readonly disabled type="text" value="' + val.r_mrp + '"></td><td><input class="form-control quantity number_class" type="text" value="' + val.r_qty + '"></td><td><input class="form-control discount_amount number_class" readonly disabled type="text" value="' + val.ret_disc_amount + '" discount-type="' + val.ret_disc_type + '" discount-percentage="' + val.ret_disc_per + '"></td><td><input class="form-control item_price number_class" readonly disabled type="text" value="' + ret_effect_price + '" actual-selling-price="' + ret_selling_price + '"></td><td><input class="form-control tax number_class" readonly disabled type="text" value="' + r_tax + '"></td><td><input class="form-control tax_amount number_class" readonly disabled type="text" value="' + r_tax_amount + '"></td>' + discount + '<td class="HideIpPatient"><input class="form-control total_tax_amount number_class" readonly disabled type="text" value="' + val.total_tax_amt + '"></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="0"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
                                                $(".pharmacy_billing_table_body").append(row_html);
                                            }
                                        });

                                        if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                                            $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').hide();
                                        } else {
                                            $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').show();
                                        }
                                        addNewRow();
                                        calculateTotalAmount();
                                    }
                                }
                            });
                        }



                    }


                } else {
                    if (parseInt($("#bill_id").val()) > 0) {
                        var old_quantity = $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').find('.quantity').attr('data-old-quantity');
                        $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').find('.quantity').val(old_quantity);
                    } else {
                        Command: toastr["error"]("Item out of stock");
                        $(".item_description[data-item-code='" + item_code + "']").parents('tr.row_class').find('.quantity').val('').trigger('keyup');
                    }
                }
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

            }
        });
    }, 500);

}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function resetPharmacyBill() {
    window.location.reload();
}

function checkPharmacyBillFormValidation() {
    var op_no_hidden = $("#op_no_hidden").val();
    var payment_type = $("#payment_type").val();
    var billing_location = $("#billing_location").val();
    var out_side_doctor = $("#out_side_doctor").prop('checked');
    var mess = 0;
    $('.row_class').each(function() {
        var qty_number = $(this).find('.quantity').val();
        var item_desc = $(this).find('.item_description').val();
        if (!qty_number && item_desc) {
            mess = 1;
        }
    });
    if (mess != 0) {
        Command: toastr['error']('Please check item quantity');
        return false;
    }
    if (!op_no_hidden) {
        Command: toastr['error']('Please select any patient');
        return false;
    }
    if (!payment_type) {
        Command: toastr['error']('Please select payment type');
        return false;
    }
    if (!billing_location) {
        Command: toastr['error']('Please select location');
        return false;
    }
    if (!out_side_doctor) {
        if ($("#doctor").val() == "") {
            Command: toastr['error']('Please select doctor');
            return false;
        }
    } else {
        if ($("#outside_doctor_name").val() == "") {
            Command: toastr['error']('Please enter doctor name');
            return false;
        }
    }

    var medicine_count = 0;
    $(".pharmacy_billing_table_body").find('tr.row_class').each(function(key, val) {
        if ($(val).find('.item_description').val()) {
            medicine_count = medicine_count + 1;
        }
    });

    if(medicine_count== 0){
        Command: toastr['error']('Please select any medicine');
        return false;
    }

    return true;

}


function savePharmacyBill() {
    //calculateTotalAmount();
   // setDisAmt();
    var validation = checkPharmacyBillFormValidation();
    if (validation == false) {
        return;
    }
    var message_show = '<p>Are you sure you want to save this bill. </p> <p> Net amount : ' + $(".net_amount").val() + '</p>';
    bootbox.confirm({
        message: message_show,
        buttons: {
            'confirm': {
                label: 'Save',
                className: 'btn-success',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-primary'
            }
        },
        callback: function(result) {
            if (result) {
                var data = {};
                data.bill_id = $("#bill_id").val();
                data.patient_id = $("#op_no_hidden").val();
                data.payment_type = $("#payment_type").val();
                data.billing_location = $("#billing_location").val();
                data.out_side_doctor = $("#out_side_doctor").prop('checked');
                data.doctor_name = '';
                data.doctor_id = 0;
                if (data.out_side_doctor) {
                    data.doctor_name = $("#outside_doctor_name").val();
                } else {
                    data.doctor_id = $("#doctor").val();
                }

                data.pharmacy_items = [];
                $(".pharmacy_billing_table_body").find('tr').each(function(key, val) {
                    if ($(val).find('.item_description').val()) {
                        var item = {};
                        if (data.bill_id) {
                            if ($(val).attr('data-detail-id') && ($(val).find('.item_description').attr('data-item-code') != $(val).find('.item_description').attr('data-old-item-code'))) {
                                item.detail_status = 2;
                                item.detail_id = $(val).attr('data-detail-id');
                            } else if ($(val).attr('data-detail-id') && ($(val).find('.quantity').attr('data-old-quantity') != $(val).find('.quantity').val())) {
                                item.detail_status = 2;
                                item.detail_id = $(val).attr('data-detail-id');
                            } else if (!$(val).attr('data-detail-id')) {
                                item.detail_status = 1;
                                item.detail_id = $(val).attr('data-detail-id');
                            } else if ($(val).attr('data-detail-id')) {
                                // item.detail_status = 0;
                                item.detail_status = 2;
                                item.detail_id = $(val).attr('data-detail-id');
                            }

                            if ($(val).hasClass('row_class_removed')) {
                                item.detail_status = 3;
                            }
                        }
                        data.indent_head_id = $(val).attr('data-indent-head-id') ? $(val).attr('data-indent-head-id') : 0;
                        item.indent_id = $(val).attr('data-indent-id') ? $(val).attr('data-indent-id') : 0;
                        item.item_description = $(val).find('.item_description').val();
                        item.item_code = $(val).find('.item_description').attr("data-item-code");
                        item.serial_no = $(val).find('.serial_no').html();
                        item.batch = $(val).find('.batch').val();
                        item.generic_name = $(val).find('.generic_name').val();
                        item.expiry_date = $(val).find('.expiry_date').val();
                        item.rack = $(val).find('.rack').val();
                        item.mrp = $(val).find('.mrp').val();
                        item.discount_amount = $(val).find('.discount_amount').val();
                        item.discount_type = $(val).find('.discount_amount').attr("discount-type");
                        item.discount_percentage = $(val).find('.discount_amount').attr("discount-percentage");
                        item.item_price = $(val).find('.item_price').val();
                        item.item_actual_price = $(val).find('.item_price').attr('actual-selling-price');
                        item.tax = $(val).find('.tax').val();
                        item.tax_amount = $(val).find('.tax_amount').val();
                        item.total_amount = $(val).find('.total_amount').val();
                        item.quantity = $(val).find('.quantity').val();
                        item.item_discount_type = $(val).find('#item_discount_type').val();
                        item.item_discount_amount = $(val).find('.item_discount_amount').val();
                        item.item_discount_value = $(val).find('.item_discount_amount').attr('attr-item-discount');
                        data.pharmacy_items.push(item);
                    }
                });

                var discount_amount = ($(".total_discount").val()).trim();
                discount_amount = (discount_amount != '') ? discount_amount : 0;
                var items_discount_amount = $(".total_discount").attr("attr-items-total-dis");
                var head_discount = (parseFloat(discount_amount) - parseFloat(items_discount_amount)).toFixed(4);
                data.item_total = $(".item_total").val();
                data.total_tax = $(".total_tax").val();
                data.net_amount = $(".net_amount").val();
                data.bill_amount = $(".bill_amount").val();
                data.discount_reason = $("#discount_narration").val();
                data.discount_type = $("#discount_type").val();
                data.discount_value = $(".discount_value").val();
                data.discount_amount = head_discount;
                data.items_discount_amount = items_discount_amount;

                var url = $('#base_url').val() + "/pharmacy/savePharmacyBill";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        pharmacy_bill_data: data
                    },
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                    },
                    success: function(data) {
                        console.log('success');
                        if (data.status == 1) {
                            var bill_details = data.data.bill_details ? data.data.bill_details : {};
                            if (Object.keys(bill_details).length > 0) {
                                var result_status = bill_details.Result;
                                var message_status = bill_details.message;
                                if (result_status == "1") {
                                    var bill_no = bill_details.bill_no ? bill_details.bill_no : '';
                                    if (parseInt($("#bill_id").val()) > 0) {
                                        bill_no = $("#bill_no").val();
                                    }
                                    var bill_head_id = bill_details.bill_head_id ? bill_details.bill_head_id : '';
                                    showSaveMessageModal(bill_head_id, bill_no);
                                } else {
                                    $.each(message_status, function(key11, value11){
                                        var msg = Object.values(value11)[0];
                                        Command: toastr['error'](msg);
                                    });

                                }

                            }


                        } else {
                            var item = data.data.out_of_stock_item;
                            var item_desc = item.item_description;
                            var item_code = item.item_code;
                            var batch = item.batch;

                            Command: toastr['error'](data.message);

                            bootbox.alert(item_desc.toUpperCase() + ' does not have enough stock in ' + batch + '. Please check the stock exists in another batch.')
                            $(".item_description[data-item-code='" + item_code + "']").addClass('alerts-border');
                            setTimeout(function() {
                                $(".item_description[data-item-code='" + item_code + "']").removeClass('alerts-border');
                                var end = $($(".item_description[data-item-code='" + item_code + "']")[0]).val().length;
                                $(".item_description[data-item-code='" + item_code + "']")[0].setSelectionRange(end, end);
                                $($(".item_description[data-item-code='" + item_code + "']")[0]).focus();
                                $(window).scrollTop($(".item_description[data-item-code='" + item_code + "']").position().top);
                            }, 3000);
                        }
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }
                });
            }
        }
    });

}

function fetchBillDetails(bill_id) {
    var item_wise_discount = $('#item_wise_discount').val();
    var disabled = '';
    if (parseInt(item_wise_discount) == 0) {
        disabled = 'disabled';
    }
    var no_gst_for_ip_patient = $("#no_gst_for_ip_patient").val();
    var url = $('#base_url').val() + "/pharmacy/fetchBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {

            getPaymentTypesAndDoctor(data.bill_head.patient_id,data.bill_head.payment_type);
            $("#op_no").val(data.bill_head.uhid).attr('disabled', true);
            $("#op_no_hidden").val(data.bill_head.patient_id).attr('disabled', true);
            $("#patient_id").val(data.bill_head.patient_id);
            if (data.bill_head.is_outside_patient == 1) {
                $("#out_side_patient").prop('checked', true);
            }
            $("#out_side_patient").attr('disabled', true);
            $("#patient_name_bill").html(data.bill_head.patient_name);
            $("#ip_number").html(data.bill_head.ip_no);
            $("#payment_type").val(data.bill_head.payment_type);
            $("#billing_location").val(data.bill_head.location).attr('disabled', true);
            $("#doctor").val(data.bill_head.consulting_doctor_id).trigger('change');
            $(".pharmacy_bill_no").html(" - "+data.bill_head.bill_no);
            if (data.bill_head.is_ext_doctor) {
                $("#out_side_doctor").prop('checked', true);
                $("#outside_doctor_name").val(data.bill_head.consulting_doctor_name);
                $("#outside_doctor_name").attr("disabled", false);
                $("#doctor").attr("disabled", true);
            }

            $("#current_room").html(data.bill_head.room_name);
            $("#company").val(data.bill_head.company_id);
            $("#pricing").val(data.bill_head.pricing_id);
            $(".patient_advance").val(parseFloat(data.bill_head.patient_advance).toFixed(2));
            $("#visit_id").val(data.bill_head.visit_id);
            $("#visit_status").val((data.bill_head.current_visit_status).trim());
            $("#room_type").val(data.bill_head.room_type_id);
            $("#select_prescription_btn").attr('disabled', true);
            getPatientAllergy(data.bill_head.patient_id);

            $(".pharmacy_billing_table_body").empty();
            $(".advanceSearchBtn").attr('disabled', true);
            $('#op_no').attr('disabled', true);
            $('#discount_narration').val(data.bill_head.discount_reason);
            $('#discount_type').val(data.bill_head.discount_type);
            if(data.bill_head.discount_type==1){
                $('.discount_value').val(data.bill_head.discount);
            }else if(data.bill_head.discount_type==2){
                $('.discount_value').val(data.bill_head.discount_amount);
            }




            fetchPatientPendingBills(false);
            var is_editable='readonly disabled'
            $.each(data.bill_detail, function(key, val) {
                // if(val.pricing_discount_amount){
                //     is_editable='';
                // }
                var discount_value = '';
                if (val.discount_type == 1) {
                    discount_value = val.discount;
                } else if (val.discount_type == 2) {
                    discount_value = val.discount_amount;
                }

                var discount = '<td><select id="item_discount_type" class="form-control item_discount_type" '+ disabled +'><option value="">Discount Type</option><option value="1" ' + ((val.discount_type == 1) ? 'selected' : '') + '>Percentage(%)</option><option value="2" ' + ((val.discount_type == 2) ? 'selected' : '') + '>Amount</option></select></td><td><input class="form-control item_discount_amount number_class" type="text" value="' + discount_value + '" attr-item-discount="' + val.discount_amount + '" '+ disabled +'></td>';

                var row_html = '<tr style="background: #FFF;" class="row_class" data-indent-id="' + val.intend_id + '" data-indent-head-id="' + val.intend_head_id + '" data-detail-id="' + val.detail_id + '"><td class="serial_no">' + val.sl_no + '</td><td><input class="form-control item_description" type="text" value="' + val.item_desc + '" data-item-code="' + val.item_code + '" data-old-item-code="' + val.item_code + '" data-stock=""></td><td><input class="form-control batch" readonly disabled type="text" value="' + val.batch + '"></td><td><input class="form-control generic_name" readonly disabled  type="text" value="' + val.generic_name + '"></td><td><input class="form-control expiry_date" readonly disabled type="text" value="' + val.expiry + '"></td><td><input class="form-control rack" readonly disabled type="text" value="' + val.rack + '"></td><td><input class="form-control mrp number_class" readonly disabled type="text" value="' + val.unit_mrp + '"></td><td><input class="form-control quantity number_class" type="text" data-old-quantity="' + parseInt(val.qty) + '" value="' + parseInt(val.qty) + '"></td><td><input class="form-control discount_amount number_class" '+is_editable+' type="text" value="' + val.pricing_discount_amount + '" discount-type="' + val.pricing_disc_type + '" discount-percentage="' + val.pricing_discount_percentage + '" ></td><td><input class="form-control item_price number_class" readonly disabled type="text" value="' + val.selling_price_wo_tax + '" actual-selling-price="' + val.ret_selling_price + '"></td><td><input class="form-control tax number_class" readonly disabled type="text" value="' + val.tax_percent + '"></td><td><input class="form-control tax_amount number_class" readonly disabled type="text" value="' + val.unit_tax_amount + '"></td>' + discount + '<td class="HideIpPatient"><input class="form-control total_tax_amount number_class" readonly disabled type="text" value="' + val.net_amount + '"></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value="' + val.net_amount + '"></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
                $(".pharmacy_billing_table_body").append(row_html);
            });

            var visit_status = $("#visit_status").val();
            if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').hide();
            } else {
                $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').show();
            }
            if (data.bill_head.cancelled_status == 3 || data.bill_head.paid_status == 1) {
                $(".savePharmacyBillBtn").remove();
                $(".addNewRowBtn").remove();
                $(".deleteRowBtn").remove();
                $("#doctor").attr('disabled', true);
                $("#payment_type").attr('disabled', true);
                $(".item_description").attr('disabled', true);
                $(".quantity").attr('disabled', true);
                $("#out_side_doctor").attr('disabled', true);
                $(".filter_option").attr('disabled', true);
                $(".item_discount_type").attr('disabled', true);
                $(".item_discount_amount").attr('disabled', true);
            } else {
                addNewRow();
            }

            calculateTotalAmount();
            resetSerialNumbers();
            setDisAmt();



        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}

$(document).on('click', ".print_config_close_btn", function() {
    $("#print_config_modal").modal('hide');
})

function showSaveMessageModal(bill_id, bill_no) {
    $(".save_message_bill_no").html(bill_no);
    $("#bill_id").val(bill_id);
    if (($("#payment_type").val() == "cash/Card" || $("#payment_type").val() =='opcredit')  && parseInt($(".net_amount").val()) > 0) {
        payBill();
    } else {
        if($("#enable_dotmatrix_printout_clinic").val() == "1"){
            var bill_id = $('#bill_id').val();
            var base_url = $('#base_url').val();
            if (!bill_id) {
                window.location.reload();
            } else {
                var url = base_url + '/pharmacy/pharmacyBilling';
                window.location.href = url;
            }
        } else {
            $("#print_config_modal").modal('show');
        }
    }
}

function printBillDetails(bill_id = 0) {

    if (bill_id == 0) {
        bill_id = $("#bill_id").val();
    }
    var include_hospital_header = $("#include_hospital_header").prop('checked') ? 1 : 0;
    var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
    var printMode = $('input[name=printMode]:checked').val();
    var url = $('#base_url').val() + "/pharmacy/printBillDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            bill_id: bill_id,
            location_code: location_code,
            include_hospital_header: include_hospital_header,
            printMode: printMode
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            if(data.status == 1){
                if (printMode == 1) {
                    var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + data.print_data + '<script>setTimeout(function(){window.print(); window.close();  },1000)</script>');
                } else {
                    var winPrint = window.open('', '', 'left=0,top=0,width=3508,height=2480,toolbar=0,scrollbars=0,status=0');
                    winPrint.document.write('<style>@page{size:A4 landscape;} .patient_head{ font-size:10px !important; } </style>' + data.print_data + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');
                }
            } else {
                toastr.success('Bill print success.');
            }

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
        }
    });
}


function showIndentScreen() {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
    $("#select_prescription_modal").modal('show');
    getIndentList();
}

function fetchPrescriptionDetailData(presc_id) {
    var location_code = $("#billing_location").val();
    var url = $('#base_url').val() + "/pharmacy/fetchPrescriptionDetailData";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            presc_id: presc_id,
            billing_location: location_code
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $(".prescription_detail_data").html(data.prescription_detail_data);
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}

function getIndentList() {
    var patient_id = $("#patient_uhid_hidden").val();
    var doctor_id = $("#doctor_presc").val();
    var nursing_location = $("#nursing_location").val();
    var pharmacy_location = $("#pharmacy_location").val();
    var presc_visit_type = $("#presc_visit_type").val();
    var presc_status = $("#presc_status").val();
    var presc_from_date = $(".presc_from_date").val();
    var presc_to_date = $(".presc_to_date").val();
    var search_by_date = $("#search_by_date").prop("checked") ? "1" : "2";
    var pending_or_partial = $("#pending_or_partial").prop("checked") ? "1" : "2";
    var url = $('#base_url').val() + "/pharmacy/getIndentList";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id,
            doctor_id: doctor_id,
            nursing_location: nursing_location,
            pharmacy_location: pharmacy_location,
            presc_visit_type: presc_visit_type,
            presc_status: presc_status,
            presc_from_date: presc_from_date,
            presc_to_date: presc_to_date,
            search_by_date: search_by_date,
            pending_or_partial: pending_or_partial,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            window.selected_patient_id_for_convertions = 0;
            $(".prescription_head_data").html(data.prescription_head_data);
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}


//print prescription view
function printPescriptionView(head_id) {
    if (head_id != '' && head_id != undefined) {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            async: false,
            url: url + "/emr/print-prescription-details",
            data: {
                head_id: head_id,
                _token: _token
            },
            beforeSend: function() {},
            success: function(data) {
                let res = JSON.parse(data);
                var winPrint = window.open('', '', 'left=0,top=0,width=1020,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(res.html + '<script>setTimeout(function(){window.print(); window.close(); },1000)</script>');




            },
            complete: function() {

            }
        });
    }
}


function convertToBill() {
    if($('#billing_location').val()=='' || $('#billing_location').val()==null){
        toastr.warning('Please select a billing location.');
        return;
    }
    var prescriptions_to_convert = [];
    var patient_id = '';
    var uhid = '';
    $(".presc_select_check").each(function(key, val) {
        if ($(val).prop("checked")) {
            var presc_id = $(val).parents('tr').attr('data-presc-id');
            prescriptions_to_convert.push(presc_id);
            patient_id = $(val).parents('tr').attr('data-patient-id');
            uhid = $(val).parents('tr').attr('data-patient-uhid');
        }
    });

    if (prescriptions_to_convert.length == 0) {
        Command: toastr['danger']('Please select prescriptions to convert.');
        return;
    }

    if (patient_id == "") {
        Command: toastr['danger']('Please select prescriptions to convert.');
        return;
    }

    var item_wise_discount = $('#item_wise_discount').val();
    var disabled = '';
    if (parseInt(item_wise_discount) == 0) {
        disabled = 'disabled';
    }

    var location_code = $("#billing_location").val();
    var no_gst_for_ip_patient = $("#no_gst_for_ip_patient").val();

    var url = $('#base_url').val() + "/pharmacy/getPrescriptionListToConvert";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            presc_list: prescriptions_to_convert,
            patient_id: patient_id,
            billing_location: location_code
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
          //  console.log(data);
            window.selected_patient_id_for_convertions = 0;
            $("#select_prescription_modal").modal('hide');
            var patient_details = data.patient_details;
            getPatientDetailsByUHID(patient_details.uhid, false);
            var visit_status = data.patient_details.current_visit_status;
            $("#visit_status").val(visit_status.trim());

            $(".pharmacy_billing_table_body").empty();

            $.each(data.out_of_item_desc, function(key, val) {
                if (data.out_of_item_desc.length > 0) {
                    toastr.warning(val.item_desc + ' is out of stock');
               //     console.log(val.item_desc);
                }
            });


            $.each(data.item_list, function(key, val) {

                var r_tax = val.r_tax;
                var r_tax_amount = val.r_tax_amount;
                var ret_effect_price = val.ret_effect_price;
                var ret_selling_price = val.ret_selling_price;
                if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                    ret_effect_price = (parseFloat(ret_effect_price) + parseFloat(r_tax_amount)).toFixed(2);
                    ret_selling_price = (parseFloat(ret_selling_price) + parseFloat(r_tax_amount)).toFixed(2);
                    r_tax = parseFloat(0).toFixed(2);
                    r_tax_amount = parseFloat(0).toFixed(2);
                }

                var discount = '<td><select id="item_discount_type" class="form-control item_discount_type" ' + disabled + '><option value="">Discount Type</option><option value="1">Percentage(%)</option><option value="2">Amount</option></select></td><td><input class="form-control item_discount_amount number_class" type="text" value="" attr-item-discount="0" ' + disabled + '></td>';

                var row_html = '<tr style="background: #FFF;" class="row_class" data-detail-id="" data-indent-id="' + val.indent_id + '" data-indent-head-id="' + val.indent_head_id + '"><td class="serial_no"></td><td><input class="form-control item_description" type="text" value="' + val.item_desc + '" data-item-code="' + val.r_item_code + '" data-old-item-code="" data-stock=""></td><td><input class="form-control batch" readonly disabled type="text" value="' + val.r_batch_no + '"></td><td><input class="form-control generic_name" readonly disabled  type="text" value="' + val.generic_name + '"></td><td><input class="form-control expiry_date" readonly disabled type="text" value="' + val.r_expiry_date + '"></td><td><input class="form-control rack" readonly disabled type="text" value="' + val.rack_name + '"></td><td><input class="form-control mrp number_class" readonly disabled type="text" value="' + val.r_mrp + '"></td><td><input class="form-control quantity number_class" type="text" data-old-quantity="" value="' + parseInt(val.r_qty) + '"></td><td><input class="form-control discount_amount number_class" readonly disabled type="text" value="' + val.ret_disc_amount + '" discount-type="' + val.ret_disc_type + '" discount-percentage="' + val.ret_disc_per + '" ></td><td><input class="form-control item_price number_class" readonly disabled type="text" value="' + ret_effect_price + '" actual-selling-price="' + ret_selling_price + '"></td><td><input class="form-control tax number_class" readonly disabled type="text" value="' + r_tax + '"></td><td><input class="form-control tax_amount number_class" readonly disabled type="text" value="' + r_tax_amount + '"></td>'+ discount +'<td class="HideIpPatient"><input class="form-control total_tax_amount number_class" readonly disabled type="text" value="0"></td><td><input class="form-control total_amount number_class" readonly disabled type="text" value=""></td><td><a class="btn btn-sm deleteRowBtn text-red" style="border: 1px solid #b7b5b5; background: #f3f2f2;" onclick="deleteRow(this)"><i class="fa fa-trash"></i></a></td></tr>';
                $(".pharmacy_billing_table_body").append(row_html);

            });
            var visit_status = $("#visit_status").val();
            if (no_gst_for_ip_patient == 1 && visit_status.trim() == 'IP') {
                $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').hide();
            } else {
                $('#pharmacy_billing_table th.HideIpPatient, #pharmacy_billing_table td.HideIpPatient').show();
            }

            calculateTotalAmount();
            resetSerialNumbers();
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        }
    });
}


function showSubtituteItemModal(id, head_id, item_code, item_desc, generic_name, quantity) {
    $("#sub_item_desc").val(item_desc);
    $("#sub_item_desc").attr("data-item-code", item_code);
    $("#sub_item_desc").attr("data-presc-id", id);
    $("#sub_item_desc").attr("data-presc-head-id", head_id);
    $("#substitute_item_generic").val(generic_name);
    $("#substitute_item_qty").val(quantity);
    $("#substitute_item_modal").modal('show');
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
}

var timeout1 = null;
var last_search_string1 = '';
$(document).on('keyup', '#substitute_item_desc', function(event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        return false;
    }
    /* Act on the event */
    var search_string = $(this).val().trim();
    searchPharmacySubstituteItem(search_string);

});


function searchPharmacySubstituteItem(search_string) {
    var generic_name_wise = ($("#filter_by_generic").prop("checked")) ? 1 : 0;
    var patient_id = $("#patient_id").val();
    var billing_location = $("#billing_location").val();

    if (search_string == "") {
        $('#substitute_item_desc_table').html('<tr class="text-center"><td colspan="3">No Data Found..!</td></tr>');
    }

    if (search_string == "" || search_string.length < 3) {
        last_search_string1 = '';
        return false;
    } else {
        clearTimeout(timeout1);
        timeout1 = setTimeout(function() {
            if (search_string == last_search_string1) {
                return false;
            }
            var url = $('#base_url').val() + "/pharmacy/searchPharmacyItem";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    patient_id: patient_id,
                    billing_location: billing_location,
                    search_string: search_string,
                    out_of_stock: 0,
                    another_batch_item: 0,
                    generic_name_wise: generic_name_wise,
                    contain_search: 0
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
                },
                success: function(data) {

                    let response = data.medicine_list;
                    let res_data = "";

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            var item_desc = response[i].item_desc;
                            var item_code = response[i].item_code;
                            var generic_name = response[i].generic_name;
                            var stock = response[i].stock;
                            // var current_batch_stock = response[i].current_batch_stock;
                            var nostock = '';
                            if (!stock) {
                                nostock = 'no-stock';
                            }

                            res_data += '<tr data-item-code="' + item_code + '" data-item-desc="' + item_desc + '" data-genreric-name="' + generic_name + '" data-stock="' + stock + '"><td>' + stock + '</td><td class="' + nostock + '">' + item_desc + '</td><td><button class="btn btn-sm btn-primary" type="button" onclick="replaceMedicineItem(this);"><i class="fa fa-plus"></i></button></td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="3">No Data Found..!</td></tr>';
                    }

                    $(".substitute_item_desc_table").html(res_data);
                    last_search_string = search_string;

                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }, 500)

    }
}

function replaceMedicineItem(btn) {
   // console.log(btn);

    var presc_id = $("#sub_item_desc").attr("data-presc-id");
    var presc_head_id = $("#sub_item_desc").attr("data-presc-head-id");
    var old_item_code = $("#sub_item_desc").attr("data-item-code");
    var new_item_code = $(btn).parents('tr').attr('data-item-code');
    var new_item_desc = $(btn).parents('tr').attr('data-item-desc');

    var url = $('#base_url').val() + "/pharmacy/replaceMedicineItem";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            presc_id: presc_id,
            old_item_code: old_item_code,
            new_item_code: new_item_code,
            new_item_desc: new_item_desc
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
           // console.log(data);
            Command: toastr["success"]("Successfully substituted.");
            $("#substitute_item_modal").modal('hide');
            fetchPrescriptionDetailData(presc_head_id);
        },
        complete: function() {
            $("body").LoadingOverlay("hide");

        }
    });

}

window.selected_patient_id_for_convertions = 0;
$(document).on("click", ".presc_select_check", function(e) {
    var patient_id = $(e.currentTarget).parents('tr').attr('data-patient-id');
    if (window.selected_patient_id_for_convertions != 0) {
        if (patient_id != window.selected_patient_id_for_convertions) {
            Command: toastr["error"]("Please select prescription of same patient to convert.");
            $(e.currentTarget).prop('checked', false);
            return;
        }
    } else {
        window.selected_patient_id_for_convertions = parseInt(patient_id);
    }

    if ($(".presc_select_check:checked").length == 0) {
        window.selected_patient_id_for_convertions = 0;
    }

});

$(document).on("click", "#pending_or_partial", function(e) {
    if ($(e.currentTarget).prop('checked')) {
        $("#presc_status").attr('disabled', true);
        $("#presc_status").attr('readonly', true);
    } else {
        $("#presc_status").removeAttr('disabled');
        $("#presc_status").removeAttr('readonly');
    }
});

$(document).on("click", "#out_side_patient", function(e) {
    if ($("#out_side_patient").prop("checked")) {
        $("#op_no").prop('disabled', true);
        $(".advanceSearchBtn").prop('disabled', true);
        showOutsidePatientRegForm();
    } else {
        $("#op_no").prop('disabled', false);
        $(".advanceSearchBtn").prop('disabled', false);
    }
});


function outsidePatientRegistrationSuccess(patient_details) {
    getPatientDetailsByUHID(patient_details.uhid);
    $("#op_no").prop('disabled', true);

    if (patient_details.external_doctor_check == 1) {
        $("#out_side_doctor").prop("checked", true);
        $("#outside_doctor_name").val(patient_details.doctor_name);
        $("#doctor").attr('disabled', true);
    } else {
        $("#out_side_doctor").prop("checked", false);
        $("#outside_doctor_name").val('');
        $("#doctor").attr('disabled', false);
    }
}

function payBill() {
    var bill_ids = [];
    bill_ids.push($("#bill_id").val());
    loadCashCollection(bill_ids);
}


$('#print_config_modal').on('hidden.bs.modal', function() {
    var bill_id = $('#bill_id').val();
    var base_url = $('#base_url').val();
    if (!bill_id) {
        window.location.reload();
    } else {
        var url = base_url + '/pharmacy/pharmacyBilling';
        window.location.href = url;
    }


});

function getPatientAllergy(id) {


    var url = $('#base_url').val() + "/pharmacy/getPatientAllergy";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: id,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            $('#getAllrgyDetails').html(data);
        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        }
    });

}

function paymentCompleted() {
    if($("#enable_dotmatrix_printout_clinic").val() == "1"){
        var bill_id = $('#bill_id').val();
        var base_url = $('#base_url').val();
        if (!bill_id) {
            window.location.reload();
        } else {
            var url = base_url + '/pharmacy/pharmacyBilling';
            window.location.href = url;
        }
    } else {
        $("#print_config_modal").modal('show');
    }
    // $("#print_config_modal").modal('show');
    // var base_url = $('#base_url').val();
    // var url = base_url + '/pharmacy/pharmacyBilling';
    // window.location.href = url;
}

$(document).on('keydown', function(event) {
    if ($('.medicine-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-medicine-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                if ($(".list-medicine-search-data-row-listing").find('tr.active_medicine').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_medicine').is(':first-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_medicine');
                    $(".list-medicine-search-data-row-listing").find('tr').last().addClass('active_medicine');
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_medicine').prev().addClass('active_medicine');
                    $(".list-medicine-search-data-row-listing").find('tr.active_medicine').last().removeClass('active_medicine');
                }
            } else if (event.which == 40) { // down
                if ($(".list-medicine-search-data-row-listing").find('tr.active_medicine').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_medicine').is(':last-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_medicine');
                    $(".list-medicine-search-data-row-listing").find('tr').first().addClass('active_medicine');
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_medicine').next().addClass('active_medicine');
                    $(".list-medicine-search-data-row-listing").find('tr.active_medicine').first().removeClass('active_medicine');
                }
            } else if (event.which == 13) {
                $(".list-medicine-search-data-row-listing").find('tr.active_medicine').trigger('click');
            } else if (event.which == 8) {
                $('.medicine-list-div-row-listing').hide();
                $(window.tr).find('.item_description').focus();
            }
        }
    } else if($('#op_noAjaxDiv').css('display') == 'block'){
        if (event.which == 13) {
            $("#op_noAjaxDiv").find('li.liHover').trigger('click');
        }
    }
})

$(document).on('focus', '.item_description', function(e){
    window.tr = $(e.currentTarget).parents('tr.row_class');
})
$('#modalCashRecive').on('hidden.bs.modal', function() {
    if($("#enable_dotmatrix_printout_clinic").val() == "1"){
        var bill_id = $('#bill_id').val();
        var base_url = $('#base_url').val();
        if (!bill_id) {
            window.location.reload();
        } else {
            var url = base_url + '/pharmacy/pharmacyBilling';
            window.location.href = url;
        }
    } else {
        $("#print_config_modal").modal('show');
    }
})

$(document).on("click", ".page-link", function (e) {
    e.preventDefault();
    var url = $(this).attr("href");
    var patient_id = $("#patient_uhid_hidden").val();
    var doctor_id = $("#doctor_presc").val();
    var nursing_location = $("#nursing_location").val();
    var pharmacy_location = $("#pharmacy_location").val();
    var presc_visit_type = $("#presc_visit_type").val();
    var presc_status = $("#presc_status").val();
    var presc_from_date = $(".presc_from_date").val();
    var presc_to_date = $(".presc_to_date").val();
    var search_by_date = $("#search_by_date").prop("checked") ? "1" : "2";
    var pending_or_partial = $("#pending_or_partial").prop("checked") ? "1" : "2";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id,
            doctor_id: doctor_id,
            nursing_location: nursing_location,
            pharmacy_location: pharmacy_location,
            presc_visit_type: presc_visit_type,
            presc_status: presc_status,
            presc_from_date: presc_from_date,
            presc_to_date: presc_to_date,
            search_by_date: search_by_date,
            pending_or_partial: pending_or_partial,
        },
        beforeSend: function() {
            $("body").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function(data) {
            window.selected_patient_id_for_convertions = 0;
            $(".prescription_head_data").html(data.prescription_head_data);
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        },
        complete: function() {
            $("body").LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });

    return false;

});
$(document).on('keyup', '.discount_amount', function() {
    var dis_at=parseFloat($(this).val()) ?? 0;
    var qty=parseInt($(this).parents('tr.row_class').find('.quantity').val()) ?? 0;
    //console.log(dis_at);
    var qty_amt=dis_at;
    var total=parseFloat($(this).parents('tr.row_class').find('.total_amount ').val());
    if(qty_amt <= total ){
        calculateTotalAmount();
    }else{
        toastr.warning('Amount must me less then selling price.');
        $(this).val(0);
    }
    //setDisAmt();

});
$(document).on('keyup', '.discount_value', function() {
    setDisAmt(1);
});
$(document).on('change', '#discount_type', function() {
    setDisAmt();
});
function setDisAmt(from_type=0){
    $('.total_discount').val(0);
    var item_discount = $('.total_discount').attr("attr-items-total-dis");
    var type =$('#discount_type').val();
    if(!type && from_type==1){
        toastr.warning('Please select discount type.');
        $('.discount_value').val('');

    }
    var dis=$('.discount_value').val();
    var net_amount_in=$('.item_total ').val().trim();
    var amt=0;
    if(dis && type){
        if(type==1){
           if(parseFloat(dis) <= parseFloat(100)){
            dis_t=(parseFloat(dis/100)*parseFloat(net_amount_in)).toFixed(4);
            ttl_dis=$('.total_discount').val();
            overall_dis=(parseFloat(ttl_dis) + parseFloat(dis_t)).toFixed(4);
            amt= (parseFloat(net_amount_in) - parseFloat(dis_t)).toFixed(4);
            $('.net_amount').val(amt);
            overall_dis = (parseFloat(overall_dis) + parseFloat(item_discount)).toFixed(4);
       //     $('#discount_value_hidden').val(dis_t);
            $('.total_discount').val(overall_dis);
           }else{
            toastr.warning('Given discount must me less than or equal to 100.');
           }
        }else if(type==2){

            if(parseFloat(net_amount_in) >= parseFloat(dis)){
                amt= (parseFloat(net_amount_in) - parseFloat(dis)).toFixed(4);
               // console.log(amt);
                ttl_dis=$('.total_discount').val();
                overall_dis=(parseFloat(ttl_dis) + parseFloat(dis)).toFixed(4);

                $('.net_amount').val(amt);
             //   $('#discount_value_hidden').val(ttl_dis);
                overall_dis = (parseFloat(overall_dis) + parseFloat(item_discount)).toFixed(4);
                $('.total_discount').val(overall_dis);
            }else{
                toastr.warning('Given discount amount must me less than or equal to net amount.');
            }

        }
    }else{

        calculateTotalAmount();
    }
}

$(document).on('change', '#item_discount_type', function() {
    $(this).closest('tr').find('.item_discount_amount').trigger('keyup');
});

$(document).on('keyup', '.item_discount_amount', function () {
    $(this).closest('tr').find('.item_discount_amount').attr("attr-item-discount", 0);
    calculateTotalAmount();
    // calculateItemTotal($(this).closest('tr'));

    var type = $(this).closest('tr').find('#item_discount_type').val();
    var value = ($(this).val()).trim();
    if (value != '' && type == '') {
        toastr.warning('Please select discount type.');
        $(this).val('');
    }

    value = (value != '') ? value : 0;
    var total_amount = $(this).closest('tr').find('.total_amount').val();

    if (type == 1) {
        if (parseFloat(value) <= parseFloat(100)) {
            dis_t = (parseFloat(value / 100) * parseFloat(total_amount)).toFixed(4);
            amt = (parseFloat(total_amount) - parseFloat(dis_t)).toFixed(4);
            $(this).closest('tr').find('.total_amount').val(amt);
            $(this).attr("attr-item-discount", dis_t);
        } else {
            $(this).val('');
            $(this).trigger('keyup');
            toastr.warning('Given discount must me less than or equal to 100.');
        }
    } else if (type == 2) {
        if (parseFloat(total_amount) >= parseFloat(value)) {
            amt = (parseFloat(total_amount) - parseFloat(value)).toFixed(4);
            $(this).closest('tr').find('.total_amount').val(amt);
            $(this).attr("attr-item-discount", value);
        } else {
            $(this).val('');
            $(this).trigger('keyup');
            toastr.warning('Given discount amount must me less than or equal to net amount.');
        }
    }
    calculateTotalAmount();
    setDisAmt();
});

// function calculateItemTotal(row)
// {
//     console.log(row);
//     var value = (row.find('.item_discount_amount').val()).trim();
//     value = (value != '') ? value : 0;
//     var type = row.find('#item_discount_type').val();
//     var total_amount = row.find('.total_amount').val();
//     alert(value +' ___'+total_amount)

//     if (type == 1) {
//         if (parseFloat(value) <= parseFloat(100)) {
//             dis_t = (parseFloat(value / 100) * parseFloat(total_amount)).toFixed(4);
//             amt = (parseFloat(total_amount) - parseFloat(dis_t)).toFixed(4);
//             alert(amt)
//             row.find('.total_amount').val(amt);
//             row.find('.item_discount_amount').attr("attr-item-discount", dis_t);
//         } else {
//             $(this).val('');
//             toastr.warning('Given discount must me less than or equal to 100.');
//         }
//     } else if (type == 2) {
//         if (parseFloat(total_amount) >= parseFloat(value)) {
//             amt = (parseFloat(total_amount) - parseFloat(value)).toFixed(4);
//             alert(amt)
//             row.find('.total_amount').val(amt);
//             row.find('.item_discount_amount').attr("attr-item-discount", value);
//         } else {
//             $(this).val('');
//             toastr.warning('Given discount amount must me less than or equal to net amount.');
//         }
//     }
//     calculateTotalAmount();
// }