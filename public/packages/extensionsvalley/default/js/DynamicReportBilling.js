$(document).ready(function () {
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    setTimeout(function () {
        // $(body).addClass('sidebar-collapse');
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        $("input[data-attr='date']").datetimepicker({ format: 'YYYY-MM-DD' });
    }, 300);
    $('tbody').sortable();
    //$("input[data-attr='date']").datetimepicker({ format: 'YYYY-MM-DD' });
    $(document).on('click', '.filter_expand', function () {
        $(this).toggleClass('fa-expand fa-minus');
        $(this).closest('#filter_area').toggleClass('col-md-8 col-md-12');
        $('.collapse_content').toggleClass('hidden');
    });

    $("input[name='favourite_type']").click(function () {
        var typeValue = $(this).val();
        if (typeValue == 2) {
            $('#favourite_user_role_section').css('display', 'block');
            $('#favourite_user_section').css('display', 'none');
        } else if (typeValue == 3) {
            $('#favourite_user_role_section').css('display', 'none');
            $('#favourite_user_section').css('display', 'block');
        } else {
            $('#favourite_user_role_section').css('display', 'none');
            $('#favourite_user_section').css('display', 'none');
        }
    });

    if ($('#report_id_hidden').val() != '') {
        $('.avlailable_check').each(function () {
            $(this).trigger("click");
        });
    }

});



function initialize_WebDataRocksGraph(json_data) {
    //var pivot = '';
    pivot = new WebDataRocks({
        container: "#wdr-graph_component",
        toolbar: true,
        customizeCell: customizeCellFunction,
        beforetoolbarcreated: customizeToolbar,
        report: {
            dataSource: {
                data: json_data
            },
            slice: {
                expands: {
                    expandAll: true
                }
            },
            options: {
                //                grid: {
                //                    type: "flat"
                //                }
            }
        },
        reportcomplete: function () {

            webdatarocks.setOptions({
                //                grid: {
                //                    type: "flat"
                //                }
            });

            //pivot.off("reportcomplete");
            pivotTableReportComplete = true;
            createGoogleChart();

        }
    });

    var pivotTableReportComplete = false;
    var googleChartsLoaded = false;

    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(onGoogleChartsLoaded);

    function onGoogleChartsLoaded() {
        googleChartsLoaded = true;
        if (pivotTableReportComplete) {
            createGoogleChart();
        }
    }

    function createGoogleChart() {
        if (googleChartsLoaded) {

            pivot.googlecharts.getData({
                type: "bar"
            },
                drawChart,
                drawChart
            );
        }
    }

    function drawChart(_data) {
        var data = google.visualization.arrayToDataTable(_data.data);
        var report_name = $('#report_name').val();
        var options = {
            title: report_name,
            legend: {
                position: 'center'
            },
            is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('googlechart-container'));
        chart.draw(data, options);
    }


}
function initialize_WebDataRocks(json_data) {
    //var pivot = '';
    pivot = new WebDataRocks({
        container: "#wdr-component",
        toolbar: true,
        customizeCell: customizeCellFunction,
        beforetoolbarcreated: customizeToolbar,
        report: {
            dataSource: {
                data: json_data
            },
            slice: {
                expands: {
                    expandAll: true
                }
            },
            options: {
                grid: {
                    type: "flat"
                }
            }
        },
        reportcomplete: function () {

            webdatarocks.setOptions({
                grid: {
                    type: "flat"
                }
            });

            pivot.off("reportcomplete");
            webdatarocks.off('reportchange');
            pivotTableReportComplete = true;
            //createGoogleChart();

        }
    });
    pivot = '';

}







function customizeToolbar(toolbar) {
    var tabs = toolbar.getTabs(); // get all tabs from the toolbar
    toolbar.getTabs = function () {
        delete tabs[0]; // delete the first tab
        delete tabs[1]; // delete the second tab
        delete tabs[2]; // delete the third tab
        return tabs;
    }
}

function customizeCellFunction(cellBuilder, cellData) {
    if (cellData.type == "value") {
        if (cellData.rowIndex == 1) {
            // cellBuilder.addClass("alter3");
        } else {
            if (cellData.rowIndex % 2 == 0) {
                cellBuilder.addClass("alter1");
            } else {
                cellBuilder.addClass("alter2");
            }
        }
    }
}


var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}



//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        console.log(datastring);
        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.dynamicBillingReportSearch;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {

                    $("#" + input_id + "AjaxDiv").html(html).show();
                    $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    bootbox.alert('nw error');
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});


//---------------get results------------------------------------------------------------------------------
function getDynamicReportData() {

    //-------selected fileds-------------------------------------

    var selected_fields_list = [];
    var selected_id = '';
    var selected_value = '';
    var selected_key = '';
    $('#selected_fields_list > tr').each(function () {
        selected_id = this.id;
        // selected_value = $('#'+selected_id).find('span').html();
        // selected_key = selected_id.replace('selected_','');
        selected_fields_list.push(selected_id);
    });

    //console.log(selected_fields_list);
    //return;

    //-------order by fileds-------------------------------------

    var orderby_fields_list = new Object();
    var orderby_id = '';
    var orderby_value = '';
    var orderby_key = '';
    $('#orderby_fields_list > tr>td').each(function () {
        orderby_id = this.id;
        orderby_value = $('#' + orderby_id).find('select').val();
        orderby_key = orderby_id.replace('orderby_', '');
        orderby_fields_list[orderby_key] = orderby_value;
    });

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;

        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }
        if (filters_id == 'payment_accounting_date_from') {
            filters_id = 'payment_accounting_date';
        }
        if (filters_id == 'payment_accounting_date_to') {
            filters_id = 'payment_accounting_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    //    console.log(filters_list);
    //    return;
    //    console.log(orderby_fields_list);
    //    console.log(selected_fields_list);

    var url = route_json.getDynamicReportData;
    var param = { process_type: 1, summery_type: 2, have_cashcollection: 0, columns: selected_fields_list, filters: filters_list, orderby: orderby_fields_list };
    param = JSON.stringify(param);
    // console.log(param); return;
    $.ajax({

        type: "GET",
        url: url,
        data: { data: param },
        beforeSend: function () {
            $("#wdr-component").html('<span style="width:100%;text-align: center;float: right;"><i class="fa fa fa-refresh fa-spin" style="font-size:22px;margin:auto;"></i></span>');

        },
        success: function (html) {
            // console.log(html); return;
            var obj = JSON.parse(html);
            console.log(obj.resultData);
            if (obj.returnStatus == 0) {
                $('#wdr-component').html("<span style='font-size:11px;color:red;padding:10px;'>" + obj.resultData + "</span>");
            } else {
                initialize_WebDataRocks(obj.resultData);

                setTimeout(function () { initialize_WebDataRocksGraph(obj.resultData); }, 3000);

                $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                $(".filter_expand").trigger("click");


                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $("#btn_graph").removeClass("disabled");
            }

        },
        complete: function () {
        },
        error: function () {

        }

    });



    //console.log(selected_fields_list);
    // console.log(orderby_fields_list);
    //console.log(filters_list);

}



function addOrRemoveToSelected(id, item_value, item_key, colum_name) {
    var htmlSelected = "<tr id='" + colum_name + "'><td id='selected_" + item_key + "'><span>" + item_value + "</span><button type='button' style='float:right;' class='btn light_purple_bg btn-xs btn_add_to_order_by' onclick='AddToOrderBy(\"" + item_key + "\",\"" + item_value + "\")'><i class='fa fa-arrow-down' aria-hidden='true'></i></button> </td></tr>";

    if ($('#' + id).is(":checked")) {
        $('#selected_fields_list').append(htmlSelected);
    } else {
        $("#selected_" + item_key).parent('tr').remove();
        $("#orderby_" + item_key).parent('tr').remove();
    }

}

function AddToOrderBy(item_key, item_value) {
    var htmlOrderBy = "<tr><td id='orderby_" + item_key + "'><span>" + item_value + "</span><button style='float:right;margin-left: 2px;' onclick='RemoveFromOrderBy(\"" + item_key + "\")' class='btn light_purple_bg btn-xs btn_add_to_order_by'><i class='fa fa-close'></i></button><select class='order_by_select' id='order_by_select_" + item_key + "'><option title='Ascending' value='ASC'>ASC</option><option title='Descending' value='DESC'>DESC</option></select></td></tr>";
    $("#orderby_" + item_key).parent('tr').remove();
    $('#orderby_fields_list').append(htmlOrderBy);
}

function RemoveFromOrderBy(item_key) {
    $("#orderby_" + item_key).parent('tr').remove();
}

function saveAddToFavourite() {
    var report_name = $('#report_name').val();
    var selected_fields_list = [];
    var selected_id = '';

    if (report_name == '') {
        Command: toastr["warning"]('Enter Report Name!');
        $('#report_name').focus();
        return;
    }
    var favourite_type = $('input[name="favourite_type"]:checked').val();
    var user_roles = $('#user_roles').val();
    var favourite_users = $('#favourite_users').val();

    //-------selected filelds ---------------------------------------------
    $('#selected_fields_list > tr >td').each(function () {
        selected_id = this.id;
        selected_id = selected_id.replace('selected_', '');
        selected_fields_list.push(selected_id);
    });

    //-------order by fileds---------------------------------------------
    var orderby_fields_list = new Object();
    var orderby_id = '';
    var orderby_value = '';
    var orderby_key = '';
    $('#orderby_fields_list > tr>td').each(function () {
        orderby_id = this.id;
        orderby_value = $('#' + orderby_id).find('select').val();
        orderby_key = orderby_id.replace('orderby_', '');
        orderby_fields_list[orderby_key] = orderby_value;
    });


    //-------filters---------------------------------------------
    var filters_list = [];
    var filters_value = '';
    var filters_id = '';
    $('.filters').each(function () {
        filters_id = this.id;
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }
        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list.push(filters_id);
        }


    });
    var url = route_json.dynamicBillingAddtoFavourite;

    var param = {
        report_name: report_name,
        favourite_type: favourite_type,
        user_roles: user_roles,
        favourite_users: favourite_users,
        columns: selected_fields_list,
        filters: filters_list,
        orderby: orderby_fields_list
    };
    param = JSON.stringify(param);
    $.ajax({
        type: "GET",
        url: url,
        data: { data: param },
        beforeSend: function () {
            $("#favouriteModal").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            if (html == 1) {
                Command: toastr["success"]('Report Added Into Favorite Successfully!');
                $("#favouriteModal").modal('toggle');
            } else {
                Command: toastr["error"]('Something Went Wrong!');
            }

            $("#favouriteModal").LoadingOverlay("hide");
        },
        complete: function () {
        },
        error: function () {

        }
    });
}

function getReportsList() {
    $('#reportListModal').modal('toggle');
    var url = route_json.showDynamicReports;
    $.ajax({
        type: "GET",
        url: url,
        data: 'showreport=1',
        beforeSend: function () {
            $("#reportListModal").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (html) {
            $("#report_list_data").html(html);
            $("#reportListModal").LoadingOverlay("hide");
        },
        complete: function () {
        },
        error: function () {

        }
    });
}

function goToReport(report_id) {
    var url = route_json.dynamicBillingReport;
    report_id = encodeURIComponent(window.btoa(report_id));
    var str = "?report_id=" + report_id;
    window.location.href = url + str;
}

function showDynamicReportDataGraph() {
    $('#graph_container').css('display', '');
    $("#googlechart-container").toggle();
}

