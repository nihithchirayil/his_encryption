
$(document).ready(function () {
    $('#email').focus();
    $(document).on('click', '.op_btn', function (event) {
        $(".op_slide_box").addClass('open');
    });

    $(document).on('click', '.slide_close_btn', function (event) {
        $(".op_slide_box").removeClass('open');
    });



    $(document).on('click', '.ip_btn', function (event) {
        $(".ip_slide_box").addClass('open');
    });

    $(document).on('click', '.slide_close_btn', function (event) {
        $(".ip_slide_box").removeClass('open');
    });


    $(document).on('click', '.smooth_scroll', function (event) {
        event.preventDefault();

        $('.theadscroll').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });



    $('.modal').on('shown.bs.modal', function (e) {
        $(".theadfix_wrapper").floatThead('reflow');
    });

    //$(".selectsearch").select2();

    $(document).on('click', '.combined_view_date_list ul li a', function () {
        $('.combined_view_date_list ul li a').removeClass("active");
        $(this).addClass("active");
    });


    $(document).on('click', '.combined_view_date_list ul li', function () {
        var disset = $(this).attr("id");
        $(this).closest('.combined_view_wrapper').find("tr#" + disset).addClass('active');

    });




    // vital graph chart starts here


    Highcharts.chart('vital_graph', {

        title: {
            text: 'Patient Vitals Chart'
        },

        credits: {
            enabled: false
        },
        // exporting: {
        //     enabled: false
        // },

        subtitle: {
            text: 'Patient Name (55/F) - RRMMGG'
        },

        yAxis: {
            title: {
                text: 'Vital Values'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2010 to 2017'
            }
        },

        legend: {
            layout: 'horizontal',
            //align: 'right',
            verticalAlign: 'bottom'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },

        series: [{
            name: 'BP Systolic mmHg',
            data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
        }, {
            name: 'BP Diastolic mmHg',
            data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
        }, {
            name: 'Pulse / Min',
            data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
        }, {
            name: 'Respiration / Min',
            data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
        }, {
            name: 'Temperature F',
            data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });

    // vital graph chart ends here



    $(document).on('click', '.filter_expand', function () {
        $(this).closest('#filter_area').toggleClass('col-md-8 col-md-12');
        $('.collapse_content').toggleClass('hidden');
    });
    $(document).on('click', '.expand_preview_btn', function () {
        $(this).addClass('collapse_preview_btn');
        $(this).removeClass('expand_preview_btn');
        $(this).toggleClass('fa-expand fa-minus');
        $(".preview_left_box").toggleClass('col-md-12 col-md-6');
        $('.preview_container_wrapper').addClass('expand');
        $('.preview_right_box').removeClass('hidden');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });


    $(document).on('click', '.collapse_preview_btn', function () {
        $(this).removeClass('collapse_preview_btn');
        $(this).addClass('expand_preview_btn');
        $(this).toggleClass('fa-minus fa-expand');
        $(".preview_left_box").toggleClass('col-md-6 col-md-12');
        $('.preview_container_wrapper').removeClass('expand');
        $('.preview_right_box').addClass('hidden');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });



    $(document).on('click', '.expand_prescription_btn', function () {
        $(this).addClass('collapse_prescription_btn');
        $(this).removeClass('expand_prescription_btn');
        $(this).toggleClass('fa-expand fa-minus');
        $('.prescription_wrapper').addClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });

    $(document).on('click', '.collapse_prescription_btn', function () {
        $(this).addClass('expand_prescription_btn');
        $(this).removeClass('collapse_prescription_btn');
        $(this).toggleClass('fa-minus fa-expand');
        $('.prescription_wrapper').removeClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });




    $(document).on('click', '.expand_rightbox_btn', function () {
        $(this).addClass('collapse_rightbox_btn');
        $(this).removeClass('expand_rightbox_btn');
        $(this).toggleClass('fa-expand fa-minus');
        $('.right_wrapper_box').addClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });


    $(document).on('click', '.collapse_rightbox_btn', function () {
        $(this).addClass('expand_rightbox_btn');
        $(this).removeClass('collapse_rightbox_btn');
        $(this).toggleClass('fa-minus fa-expand');
        $('.right_wrapper_box').removeClass('expand');

        setTimeout(function () {
            $(".theadfix_wrapper").floatThead('reflow');
        }, 400);
    });




    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });

    $(document).on('click', '.notes_sec_list ul li', function () {
        var disset = $(this).attr("id");
        $('.notes_sec_list ul li').removeClass("active");
        $(this).addClass("active");
        $(this).closest('.notes_box').find(".note_content").css("display", "none");
        $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
    });




    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });


    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });

    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('.date_time_picker').datetimepicker();


    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    //       $('.fixed_header').floatThead({
    //     position: 'absolute',
    //     scrollContainer: true
    // });

});
