function dragstart_handler(ev) {
    // Change the source element's background color to signify drag has started
    // ev.currentTarget.style.border = "dashed";
    // Add the id of the drag source element to the drag data payload so
    // it is available when the drop event is fired
    ev.dataTransfer.setData("text", ev.target.id);
    // Tell the browser both copy and move are possible
    ev.effectAllowed = "copyMove";
}

function dragover_handler(ev) {
    // Change the target element's border to signify a drag over event
    // has occurred
    //  ev.currentTarget.style.background = "lightblue";
    ev.preventDefault();
}

// $(document).on('drop','.dest_copy',function(e){
//     drop_handler(e);
//  });

function drop_handler(ev) {
    ev.preventDefault();
    var id = ev.dataTransfer.getData("text");

    if (id.indexOf("copy") != -1) {
        //document.getElementById(id).remove();
        return false;
    }

    // Get the id of drag source element (that was added to the drag data
    // payload by the dragstart event handler)
    // Only Move the element if the source and destination ids are both "move"
    if (id == "src_move" && ev.target.id == "dest_move")
        ev.target.appendChild(document.getElementById(id));
    // Copy the element if the source and destination ids are both "copy"

    // if (ev.target.classList.contains('dest_copy')) {

        //  var nodeCopy = document.getElementById(id).cloneNode(true);
        var nodeCopy = document.getElementById(id).cloneNode(true);

        renderTemplateComponent(id, ev);
        sortdiv();
    // }
}


function sortdiv() {
    $('.dest_copy').sortable({
        placeholder: "ui-state-highlight",
        helper: 'clone',
        // axis: "y",
        cursor: "pointer",
        items: '.sortable',
        dropOnEmpty: true,
    });
}

function dragend_handler(ev) {
    // Restore source's border
    //   ev.target.style.border = "solid black";
    // Remove all of the drag data
    ev.dataTransfer.clearData();
}

function myFunction(){
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("html_components");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("li").find('div')[0];
        console.log(a);
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function renderTemplateComponent(id, ev) {
    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "GET",
        url: url + "/form_template/renderTemplateComponent",
        data: {
            component_id: id,
        },
        beforeSend: function () {
            $(".template-container").LoadingOverlay("show", {
                background: "rgba(0,0,0,0)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $(".template-container").LoadingOverlay("hide");
            var obj = JSON.parse(data);
            if (obj.id_name == 'table_component') {
                renderTableContent(ev.target);
            } else {
                $(ev.target).append(obj.viewData);
            }
            if (obj.control_id == 5 || obj.control_id == 6) {
                // $('#' + obj.id_name).select2();
            }
            if (obj.control_id == 8) {
                $('#' + obj.id_name).datetimepicker({
                    format: 'MMM-DD-YYYY'
                });
            }
            if (obj.control_id == 9) {
                $('#' + obj.id_name).datetimepicker({
                    format: 'hh:mm A'
                });
            }
            if (obj.control_id == 10) {
                $('#' + obj.id_name).datetimepicker({
                    format: 'MMM-DD-YYYY hh:mm A',
                });
            }
            if (obj.control_id == 13) {
                initialize_tinymce(obj.id_name);
            }
            if (obj.control_id == 15) {
                $('.contenteditable').attr('contenteditable', 'true');
            }

            if (obj.id_name == 'image_editor') {
                $('.sfx-canvas').css('display','none');
                $('.sfx-canvas').css('width','100%');
                $('.sfx-canvas').css('height','100%');
            }


            $('.fa-times-circle').on('mouseenter', function () {
                $(this).parent().css('opacity', 0.5);
            });

            $('.fa-times-circle').on('mouseleave', function () {
                $(this).parent().css('opacity', 1);
            });

        },
        complete: function () { }
    });
}

function renderTableContent(target) {
    bootbox.confirm("<form id='table_data' action=''>\
    Rows:<input type='number' name='table_rows' style='width: 80px; margin: 10px;' /> \
    Columns:<input type='number' name='table_columns' style='width: 80px; margin: 10px;' />\
    </form>", function (result) {
        if (result) {
            var rows = parseInt($("input[name='table_rows']").val());
            rows = (rows > 0) ? rows : 0;
            var columns = parseInt($("input[name='table_columns']").val());
            columns = (columns > 0) ? columns : 0;

            if (rows > 0 && columns > 0) {
                var tds = '';
                var table = '<div class="col-md-12 sortable" draggable="true" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);"> <i class="fa fa-times-circle" aria-hidden="true"></i> <table class="table table-bordered static_table_component">';
                for (var i = 1; i <= rows; i++) {
                    table += '<tr class="static_table_component_tr_' + i + '">';
                    tds = '';
                    for (var j = 1; j <= columns; j++) {
                        tds += '<td class="control-group dest_copy" ondragover="dragover_handler(event);" draggable="true" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);" ></td>'
                    }
                    table += tds;
                    table += '</tr>';
                }
                table += '</table> </div>';
                $(target).append(table);
            }
        }
    });
}

function initialize_tinymce(id) {
    tinymce.init({
        selector: 'textarea#' + id,
        branding: false,
        statusbar: false,
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
}


$(document).on("click", ".fa-times-circle", function () {
    var data_id_get = $(this).attr('attr_data_id');
    if (data_id_get && data_id_get != 'undefined') {
        if (control_master[data_id_get]) {
            delete control_master[data_id_get];
        }
    }
    $(this).parent().remove();
});

function resetTemplate() {
    $('#template_list').val('0').trigger('change');
    $('.template-container').html('');
}

function removeDuplicates(arr) {
    return arr.filter((item,
        index) => arr.indexOf(item) === index);
}


function getControlMapping() {
    var control_mapping = {};
    $(".template-container .data_id_control_id").each(function (index) {
        if (($(this).attr('attr_control_id') != 'undefined') && ($(this).attr('attr_dataset_id') != 'undefined')) {
            control_mapping[$(this).attr('attr_control_id')] = {
                dataset_id: $(this).attr('attr_dataset_id'),
                datapoint: $(this).attr('attr_data_point'),
            }
        }
    });
    console.log(control_mapping);
    return control_mapping;
}

function saveTemplate() {
    var template_name = $('#template_name').val();
    tinymce.remove(".tiny_class");
    var name_array = [];
    var id_array = [];
    var type_array = [];
    var input_array = {};
    input_array["text"] = [];
    input_array["textarea"] = [];
    input_array["checkbox"] = [];
    input_array["radio"] = [];
    input_array["select"] = [];
    input_array["multiselect"] = [];
    input_array["datepicker"] = [];
    input_array["timepicker"] = [];
    input_array["datetimepicker"] = [];
    input_array["progressive_text"] = [];
    input_array["progressive_hidden"] = [];
    input_array["tiny"] = [];
    input_array["table"] = [];
    input_array["image_editor"] = [];
    input_array["image_editor_images"] = [];



    $('.template-container').find('.text-form-input').each(function () {
        // name_array.push($(this).prop("name"));
        input_array["text"].push($(this).prop("name"));
    });
    // return false;

    $('.template-container').find('.textarea-form-input').each(function () {
        input_array["textarea"].push($(this).prop("name"));
    });


    $(".template-container .check-form-input input[type='hidden']").each(function () {
        input_array["checkbox"].push($(this).prop("name"));
    });
    input_array["checkbox"] = removeDuplicates(input_array["checkbox"]);


    $('.template-container').find('.radio-form-input').each(function () {
        input_array["radio"].push($(this).prop("name"));
    });
    input_array["radio"] = removeDuplicates(input_array["radio"]);


    $('.template-container').find('.select-form-input').each(function () {
        input_array["select"].push($(this).prop("name"));
    });
    $('.template-container').find('.multiselect-form-input').each(function () {
        input_array["multiselect"].push($(this).prop("name"));
    });
    $('.template-container').find('.datepicker-form-input').each(function () {
        input_array["datepicker"].push($(this).prop("name"));
    });
    $('.template-container').find('.timepicker-form-input').each(function () {
        input_array["timepicker"].push($(this).prop("name"));
    });
    $('.template-container').find('.datetimepicker-form-input').each(function () {
        input_array["datetimepicker"].push($(this).prop("name"));
    });
    $('.template-container').find('.progressive-form-input-text').each(function () {
        input_array["progressive_text"].push($(this).prop("name"));
    });
    $('.template-container').find('.progressive-form-input-hidden').each(function () {
        input_array["progressive_hidden"].push($(this).prop("name"));
    });
    $('.template-container').find('.tiny-form-input').each(function () {
        input_array["tiny"].push($(this).prop("name"));
    });
    $('.template-container').find('.table-form-input').each(function () {
        input_array["table"].push($(this).prop("name"));
    });
    $('.template-container').find('#image_canvas_container').each(function () {
        input_array["image_editor"].push('canvas_img');
    });
    $('.template-container').find('.image_editor_images').each(function () {
        input_array["image_editor_images"].push($(this).attr('src'));
    });

    //----clearing image container canvas components----------------------
    if ($('.template-container').find('#image_canvas_container').length) {
        $('#image_canvas_container').html('');
    }
    var template_container_data = $('.template-container').html();


    var is_emr_lite = $('#template_in_emr_lite').is(':checked');

    input_array = JSON.stringify(input_array);
    console.log(input_array);
    // return false;

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    var template_id = $("#template_list").val();
    var control_master_string = JSON.stringify(getControlMapping());
    if (template_name) {
        $.ajax({
            type: "POST",
            url: url + "/form_template/saveTemplate",
            data: {
                token: _token,
                template_name: template_name,
                template_container_data: template_container_data,
                template_id: template_id,
                is_emr_lite: is_emr_lite,
                control_master_string: control_master_string,
                input_array: input_array
            },
            beforeSend: function () {
                $("body").LoadingOverlay("show", {
                    background: "rgba(0,0,0,0)",
                    imageColor: '#337AB7'
                });
            },
            success: function (data) {
                $("body").LoadingOverlay("hide");
                var obj = JSON.parse(data);

                if (obj.status == 1) {
                    toastr.info('Rended complete successfully!');
                    setTimeout(function () {
                        var url = $('#base_url').val() + "/form_template/templateBuilder";
                        window.history.pushState('', '', url);
                        location.reload();
                    }, 1000);
                } else {
                    toastr.error('error!');
                    console.log(obj.message);
                }
            },
            complete: function () { }
        });
    } else {
        toastr.warning("Please Enter Template Name");
        $('#template_name').focus();
    }
}

$(document).on("change", "#template_list", function () {
    var template_id = $("#template_list").val();
    if (template_id == 0) {
        $('#template_name').val('');
        $('.dest_copy').html('');
    }

    if (template_id != '0') {
        let url = $('#base_url').val();
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: url + "/form_template/fetchTemplateData",
            data: {
                token: _token,
                template_id: template_id,
            },
            beforeSend: function () {
                tinymce.remove(".tiny_class");
                $('.template-container').html('');
                $('#template_name').val('');
                $("body").LoadingOverlay("show", {
                    background: "rgba(0,0,0,0)",
                    imageColor: '#337AB7'
                });
                $('#template_in_emr_lite').prop('checked', false);
            },
            success: function (data) {
                $("body").LoadingOverlay("hide");
                var obj = JSON.parse(data);
                if (obj.status == 1) {
                    var template_data = obj.templates;
                    $('#template_name').val(template_data.name);
                    $('.template-container').html(template_data.html);
                    if (template_data.is_emr_lite == '1') {
                        $('#template_in_emr_lite').prop('checked', true);
                    } else {
                        $('#template_in_emr_lite').prop('checked', false);
                    }
                    toastr.success('Fetched successfully!');
                    initialize_html_controls();
                } else {
                    toastr.error('error!');
                }
            },
            complete: function () { }
        });
    }
});

function initialize_html_controls() {
    tinymce.init({
        selector: '.tiny_class',
        branding: false,
        statusbar: false,
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });
    $('.datetimepicker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A',
    });

    $('.sfx-canvas').css('display','none');
    $('.sfx-canvas').css('width','100%');
    $('.sfx-canvas').css('height','100%');
}

$(document).on("click", 'img.pain_scale_smiley', function (e) {
    $(e.target).parents('.pain_scale_radio').find("input[type='radio']").click();
    $('.pain_scale_smiley').css('opacity', '.5');
    $(e.target).css('opacity', '1');
});

$(document).on("click", 'button.txt_box_collapsible', function () {
    if ($(this).find("i").hasClass('fa-arrow-down')) {
        $(this).find("i").removeClass('fa-arrow-down').addClass('fa-arrow-up');
    } else {
        $(this).find("i").removeClass('fa-arrow-up').addClass('fa-arrow-down');
    }
});

function ReloadHtmlControls(){
    let url = $('#base_url').val();
    $.ajax({
        type: "GET",
        url: url + "/form_template/ReloadHtmlControls",
        beforeSend: function () {
            $("#control_block").LoadingOverlay("show", {
                background: "rgba(0,0,0,0)",
                imageColor: '#337AB7'
            });

            $('#fa_reset_controls').removeClass('fa fa-refresh');
            $('#fa_reset_controls').addClass('fa fa-spin fa-spinner');
        },
        success: function (data) {
            $('#fa_reset_controls').removeClass('fa fa-spin fa-spinner');
            $('#fa_reset_controls').addClass('fa fa-refresh');

            $("#control_block").LoadingOverlay("hide");
            $('#control_block').html(data);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },

    });
}
