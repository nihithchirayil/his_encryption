$(document).ready(function () {
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

    $('.nav-tabs li').click(function() {
        $('.nav-tabs li').removeClass('active');
        $(this).addClass('active');
        $('.tab-content .tab-pane').hide();
        var selectedTab = $(this).find('a').attr('href');
        $(selectedTab).show();
        $('#template_list').trigger('change');
    });
});

$(document).on("change", "#template_list", function() {
    var template_id = $("#template_list").val();
    var selectedTab = $('.nav-tabs li.active a').attr('href');
    
    if (template_id != '0') {
        let _token = $('#c_token').val();
        $.ajax({
            type: "POST",
            url: '',
            data: {
                token: _token,
                template_id: template_id,
                active_tab: selectedTab,
            },
            beforeSend: function () {
                $('#template_mapping_content').html('');
                $('#speciality_mapping_content').html('');
                $("body").LoadingOverlay("show", { background: "rgba(0,0,0,0)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("body").LoadingOverlay("hide");
                if (selectedTab == '#tab_speciality') {
                    $('#speciality_mapping_content').html(data);
                } else if (selectedTab == '#tab_doctor') {
                $('#template_mapping_content').html(data);
                }
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            complete: function () {
            }
        });
    }
});

function saveTemplateMaping(obj, active_tab) {
    var checked = 0;
    if ($(obj).is(':checked') == true) {
        checked = 1; 
    }
    var doctor_id = 0;
    var speciality_id = 0;
    if (active_tab == '#tab_doctor') {
        doctor_id = $(obj).attr('doctor_id');
    } else if (active_tab == '#tab_speciality') {
        speciality_id = $(obj).attr('speciality_id');
    }
    var template_id = $("#template_list").val();

    let url = $('#base_url').val();
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url + '/form_template/saveDoctorTemplateMapping',
        data: {
            token: _token, template_id: template_id, checked: checked, doctor_id: doctor_id, speciality_id: speciality_id, active_tab: active_tab
        },
        beforeSend: function () {
            $("body").LoadingOverlay("show", { background: "rgba(0,0,0,0)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("body").LoadingOverlay("hide");
            // console.log(data)
            if (data.status == '1') {
                toastr.success(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        complete: function () {
        }
    });
}