$(document).ready(function(){
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
    $('.select2').select2();
    initialize_tinymce('tiny_class');

    $('.datepicker').datetimepicker({
        format: 'MMMM-DD-YYYY'
    });

    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });

    $('.date_time_picker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });

});

function initialize_tinymce(class_name){
    tinymce.init({
        selector: 'textarea.'+class_name,
        branding: false,
        statusbar: false,
        height:250,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
}


function saveDynamicTemplate(){
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    var encounter_id = $("#encounter_id").val();
    var doctor_id = $('#doctor_id').val();
    var base_url = $('#base_url').val();
    let _token = $('#c_token').val();
    var dynamic_template_id = $('#select_dynamic_template').val();
    var patient_name=$("#patient_name").val();
var age=$("#age").val();
var fc00184=$("#fc00184").val();
var fc00185=$("#fc00185").val();
var fc00186=$("#fc00186").val();
var fc00187=$("#fc00187").val();
var fc00190=$("#fc00190").val();
var fc00191=$("#fc00191").val();
var fc00192=$("#fc00192").val();
var fc00193=$("#fc00193").val();
var fc00194=$("#fc00194").val();
var fc00195=$("#fc00195").val();
var fc00196=$("#fc00196").val();
var fc00197=$("#fc00197").val();
var fc00198=$("#fc00198").val();
var fc00199=$("#fc00199").val();
var fc00200=$("#fc00200").val();
var fc00201=$("#fc00201").val();
var fc00202=$("#fc00202").val();
var fc00203=$("#fc00203").val();
var fc00204=$("#fc00204").val();
var fc00206=$("#fc00206").val();
var fc00207=$("#fc00207").val();
var fc00208=$("#fc00208").val();
var fc00209=$("#fc00209").val();
var fc00210=$("#fc00210").val();
var fc00211=$("#fc00211").val();
var fc00212=$("#fc00212").val();
var fc00213=$("#fc00213").val();
var fc00214=$("#fc00214").val();
var fc00215=$("#fc00215").val();
var fc00216=$("#fc00216").val();
var fc00217=$("#fc00217").val();
var fc00218=$("#fc00218").val();
var fc00219=$("#fc00219").val();
var fc00220=$("#fc00220").val();
var fc00221=$("#fc00221").val();
var fc00222=$("#fc00222").val();
var fc00223=$("#fc00223").val();


var input_data = {
'patient_name':patient_name,
'age':age,
'fc00184':fc00184,
'fc00185':fc00185,
'fc00186':fc00186,
'fc00187':fc00187,
'fc00190':fc00190,
'fc00191':fc00191,
'fc00192':fc00192,
'fc00193':fc00193,
'fc00194':fc00194,
'fc00195':fc00195,
'fc00196':fc00196,
'fc00197':fc00197,
'fc00198':fc00198,
'fc00199':fc00199,
'fc00200':fc00200,
'fc00201':fc00201,
'fc00202':fc00202,
'fc00203':fc00203,
'fc00204':fc00204,
'fc00206':fc00206,
'fc00207':fc00207,
'fc00208':fc00208,
'fc00209':fc00209,
'fc00210':fc00210,
'fc00211':fc00211,
'fc00212':fc00212,
'fc00213':fc00213,
'fc00214':fc00214,
'fc00215':fc00215,
'fc00216':fc00216,
'fc00217':fc00217,
'fc00218':fc00218,
'fc00219':fc00219,
'fc00220':fc00220,
'fc00221':fc00221,
'fc00222':fc00222,
'fc00223':fc00223,
};



var id_inputs = ["patient_name","age","fc00184","fc00185","fc00186","fc00187","fc00190","fc00191","fc00192","fc00193","fc00194","fc00195","fc00196","fc00197","fc00198","fc00199","fc00200","fc00201","fc00202","fc00203","fc00204","fc00206","fc00207","fc00208","fc00209","fc00210","fc00211","fc00212","fc00213","fc00214","fc00215","fc00216","fc00217","fc00218","fc00219","fc00220","fc00221","fc00222","fc00223"];



var name_inputs = [];



    input_data = JSON.stringify(input_data);
    var dataparams = {
        '_token':_token,
        'patient_id':patient_id,
        'visit_id':visit_id,
        'encounter_id':encounter_id,
        'doctor_id':doctor_id,
        'input_data':input_data,
        'dynamic_template_id':dynamic_template_id,
        'input_array':id_inputs,
        'name_array':name_inputs,
    };

    url = base_url +"/dynamic_template/semenfreezingSave";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
           // $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
           return data;
        },
        complete: function () {
           // $('body').LoadingOverlay("hide");
        }
    });
}

function fetchCustomFormDetails() {
    var datas = {};
    var template = {};
    $('#dynamic_template_data').find(':input').each(function () {
        var id = $(this).attr('id');
        var type = $(this).attr('type');
        var value = $(this).val();
        if (id) {
            datas[id] = value.trim();
        }
    });
    template['datas'] = datas;

    var vitals = {};
    var vital_value = {};
    if ($('#dynamic_template_data').find('#static_component_vital_form').length) {
        if ($("#static_component_vital_form #weight").val() == 1) {
            if ($("#static_component_vital_form #weight_value").val() != '') {
                vital_value["1"] = $("#static_component_vital_form #weight_value").val();
                vital_value["2"] = (($("#static_component_vital_form #weight_value").val()) / 2.205).toFixed(2);
            }
        }
        if ($("#static_component_vital_form #weight").val() == 2) {
            if ($("#static_component_vital_form #weight_value").val() != '') {
                vital_value["2"] = $("#static_component_vital_form #weight_value").val();
                vital_value["1"] = (($("#static_component_vital_form #weight_value").val()) * 2.205).toFixed(2);
            }
        }

        if ($("#static_component_vital_form #temperature").val() == 9) {
            if ($("#static_component_vital_form #temperature_value").val() != '') {
                vital_value["9"] = $("#static_component_vital_form #temperature_value").val();
                vital_value["10"] = (($("#static_component_vital_form #temperature_value").val() - 32) * (5 / 9)).toFixed(2);
            }
        }
        if ($("#static_component_vital_form #temperature").val() == 10) {
            if ($("#static_component_vital_form #temperature_value").val() != '') {
                vital_value["10"] = $("#static_component_vital_form #temperature_value").val();
                vital_value["9"] = (($("#static_component_vital_form #temperature_value").val() * (9 / 5)) + 32).toFixed(2);
            }
        }

        if ($("#static_component_vital_form #height").val() == 3) {
            if ($("#static_component_vital_form #height_value").val() != '') {
                vital_value["3"] = $("#static_component_vital_form #height_value").val();
                vital_value["4"] = (($("#static_component_vital_form #height_value").val()) * (30.48)).toFixed(2);
            }

        }
        if ($("#static_component_vital_form #height").val() == 4) {
            if ($("#static_component_vital_form #height_value").val() != '') {
                vital_value["4"] = $("#static_component_vital_form #height_value").val();
                vital_value["3"] = ((($("#static_component_vital_form #height_value").val()) / 30.48).toFixed(2));
                ;
            }

        }
        if ($("#static_component_vital_form #bp_systolic").val() != '') {
            vital_value["5"] = $("#static_component_vital_form #bp_systolic").val();
        }
        if ($("#static_component_vital_form #bp_diastolic").val() != '') {
            vital_value["6"] = $("#static_component_vital_form #bp_diastolic").val();
        }
        if ($("#static_component_vital_form #pulse").val() != '') {
            vital_value["7"] = $("#static_component_vital_form #pulse").val();
        }
        if ($("#static_component_vital_form #respiration").val() != '') {
            vital_value["8"] = $("#static_component_vital_form #respiration").val();
        }
        if ($("#static_component_vital_form #temperature_location").val() != '') {
            vital_value["11"] = $("#static_component_vital_form #temperature_location").val();
        }

        if ($("#static_component_vital_form #oxygen").val() != '') {
            vital_value["12"] = $("#static_component_vital_form #oxygen").val();
        }
        if ($("#static_component_vital_form #head").val() != '') {
            vital_value["13"] = $("#static_component_vital_form #head").val();
        }
        if ($("#static_component_vital_form #bmi").val() != '') {
            vital_value["14"] = $("#static_component_vital_form #bmi").val();
        }
        if ($("#static_component_vital_form #waist").val() != '') {
            vital_value["20"] = $("#static_component_vital_form #waist").val();
        }
        if ($("#static_component_vital_form #hip").val() != '') {
            vital_value["21"] = $("#static_component_vital_form #hip").val();
        }
        if ($("#static_component_vital_form input[name='visibility_status']:checked").val() == 1) {
            if ($("#static_component_vital_form #blood_sug").val().trim()) {
                vital_value["22"] = $("#static_component_vital_form #blood_sug").val();
            }
        } else if ($("#static_component_vital_form input[name='visibility_status']:checked").val() == 2) {
            if ($("#static_component_vital_form #blood_sug").val().trim()) {
                vital_value["23"] = $("#static_component_vital_form #blood_sug").val();
            }
        }

        vitals['vital_data'] = vital_value;
        vitals['vital_date'] = $('#static_component_vital_form #time_taken').val();
        vitals['batch_no'] = $('#static_component_vital_form #latest_vital_batch').val();
        vitals['vital_remarks'] = $('#static_component_vital_form #remarks').val();
    }
    template['vitals'] = vitals;

    var allergies = {};
    if ($('#dynamic_template_data').find('#static_component_allergy_form').length) {
        var values_allergy = $("#static_component_allergy_form input[name='alergylist_medicine_code_hidden[]']").map(function () {
            var tt = $(this).val();
            if (tt != '') {
                return $(this).val();
            }
        }).get();
        var allergy_ct = values_allergy.length;
        var other_allergies = $("#static_component_allergy_form #otherAllergies").val().trim();
        if (values_allergy.length > 0 || other_allergies != '') {
            var allergy_arr = [];
            $('#static_component_allergy_form #MedicineAllergyListData').find('tr').each(function (key, val) {
                var allergic_obj = {};
                if ($(val).find("input[name='alergylist_medicine_code_hidden[]']").val() != '') {
                    allergic_obj.allergymedicine = $(val).find("input[name='allergymedicine[]']").val();
                    allergic_obj.medicine_code = $(val).find("input[name='alergylist_medicine_code_hidden[]']").val();
                    allergic_obj.alrg_type = $(val).find("input[name='alrg_type[]']").val();

                    allergy_arr.push(allergic_obj);
                }
            })
            allergies['allergies'] = allergy_arr;
            allergies['other_allergies'] = other_allergies;
        }
    }
    template['allergies'] = allergies;

    return JSON.stringify(template);
}
