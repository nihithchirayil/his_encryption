$(document).ready(function(){
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');
    $('.select2').select2();
    initialize_tinymce('tiny_class');

    $('.datepicker').datetimepicker({
        format: 'MMMM-DD-YYYY'
    });

    $('.timepicker').datetimepicker({
        format: 'hh:mm A'
    });

    $('.date_time_picker').datetimepicker({
        format: 'MMM-DD-YYYY hh:mm A'
    });

    fetchPatientHeader();

});

function initialize_tinymce(class_name){
    tinymce.init({
        selector: 'textarea.'+class_name,
        branding: false,
        statusbar: false,
        height:250,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
}


function saveDynamicTemplate(){
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    var encounter_id = $("#encounter_id").val();
    var doctor_id = $('#doctor_id').val();
    var base_url = $('#base_url').val();
    let _token = $('#c_token').val();
    var dynamic_template_id = $('#select_dynamic_template').val();

var id10 = $('#id10').val();
var id11 = $('#id11').val();
var id12 = $('#id12').val();
var id13 = $('#id13').val();
var id14 = $('#id14').val();
var id15 = $('#id15').val();
var id16 = $('#id16').val();
var id17 = $('#id17').val();

var fc00153=$("#fc00153").val();
var fc00154=$("#fc00154").val();
var fc00155=$("#fc00155").val();
var fc00156=$("#fc00156").val();
var fc00157=$("#fc00157").val();
var fc00158=$("#fc00158").val();
var fc00159=$("#fc00159").val();
var fc00160=$("#fc00160").val();
var fc00161=$("#fc00161").val();
var fc00162=$("#fc00162").val();
var fc00089=$("#fc00089").val();
var fc00090=$("#fc00090").val();
var fc00091=$("#fc00091").val();
var fc00092=$("#fc00092").val();
var fc00105=$("#fc00105").val();
var fc00106=$("#fc00106").val();
var fc00109=$("#fc00109").val();
var fc00108=$("#fc00108").val();
var fc00046=$("#fc00046").val();
var fc00111=$("#fc00111").val();
var fc00112=$("#fc00112").val();
var fc00084=$("#fc00084").val();
var fc00113=$("#fc00113").val();
var fc00047=$("#fc00047").val();
var fc00122=$("#fc00122").val();
var fc00118=$("#fc00118").val();
var fc00119=$("#fc00119").val();
var fc00120=$("#fc00120").val();
var fc00121=$("#fc00121").val();
var fc00123=$("#fc00123").val();
var fc00124=$("#fc00124").val();
var fc00125=$("#fc00125").val();
var fc00126=$("#fc00126").val();
var fc00127=$("#fc00127").val();
var fc00128=$("#fc00128").val();
var fc00129=$("#fc00129").val();
var fc00130=$("#fc00130").val();
var fc00131=$("#fc00131").val();
var fc00132=$("#fc00132").val();
var fc00133=$("#fc00133").val();
var fc00134=$("#fc00134").val();
var fc00135=$("#fc00135").val();
var fc00136=$("#fc00136").val();
var fc00137=$("#fc00137").val();
var fc00138=$("#fc00138").val();
var fc00139=$("#fc00139").val();
var fc00140=$("#fc00140").val();
var fc00141=$("#fc00141").val();
var fc00142=$("#fc00142").val();
var fc00143=$("#fc00143").val();
var fc00011=$("#fc00011").val();
var fc00093=$("#fc00093").val();
var fc00029=$("#fc00029").val();
var fc00030=$("#fc00030").val();
var fc00080=$("#fc00080").val();
var fc00101=$("#fc00101").val();
var fc00095=$("#fc00095").val();
var fc00099=$("#fc00099").val();
var fc00098=$("#fc00098").val();
var fc00100=$("#fc00100").val();
var fc00103=$("#fc00103").val();
var fc00102=$("#fc00102").val();
var fc00144=$("#fc00144").val();
var fc00145=$("#fc00145").val();
var fc00107=$("#fc00107").val();
var fc00110=$("#fc00110").val();


var input_data = {

'id10':id10,
'id11':id11,
'id12':id12,
'id13':id13,
'id14':id14,
'id15':id15,
'id16':id16,
'id17':id17,

'fc00153':fc00153,
'fc00154':fc00154,
'fc00155':fc00155,
'fc00156':fc00156,
'fc00157':fc00157,
'fc00158':fc00158,
'fc00159':fc00159,
'fc00160':fc00160,
'fc00161':fc00161,
'fc00162':fc00162,
'fc00089':fc00089,
'fc00090':fc00090,
'fc00091':fc00091,
'fc00092':fc00092,
'fc00105':fc00105,
'fc00106':fc00106,
'fc00109':fc00109,
'fc00108':fc00108,
'fc00046':fc00046,
'fc00111':fc00111,
'fc00112':fc00112,
'fc00084':fc00084,
'fc00113':fc00113,
'fc00047':fc00047,
'fc00122':fc00122,
'fc00118':fc00118,
'fc00119':fc00119,
'fc00120':fc00120,
'fc00121':fc00121,
'fc00123':fc00123,
'fc00124':fc00124,
'fc00125':fc00125,
'fc00126':fc00126,
'fc00127':fc00127,
'fc00128':fc00128,
'fc00129':fc00129,
'fc00130':fc00130,
'fc00131':fc00131,
'fc00132':fc00132,
'fc00133':fc00133,
'fc00134':fc00134,
'fc00135':fc00135,
'fc00136':fc00136,
'fc00137':fc00137,
'fc00138':fc00138,
'fc00139':fc00139,
'fc00140':fc00140,
'fc00141':fc00141,
'fc00142':fc00142,
'fc00143':fc00143,
'fc00011':fc00011,
'fc00093':fc00093,
'fc00029':fc00029,
'fc00030':fc00030,
'fc00080':fc00080,
'fc00101':fc00101,
'fc00095':fc00095,
'fc00099':fc00099,
'fc00098':fc00098,
'fc00100':fc00100,
'fc00103':fc00103,
'fc00102':fc00102,
'fc00144':fc00144,
'fc00145':fc00145,
'fc00107':fc00107,
'fc00110':fc00110,
};



var id_inputs = ["","","","","","","","","fc00153","fc00154","fc00155","fc00156","fc00157","fc00158","fc00159","fc00160","fc00161","fc00162","fc00089","fc00090","fc00091","fc00092","fc00105","fc00106","fc00109","fc00108","fc00046","fc00111","fc00112","fc00084","fc00113","fc00047","fc00122","fc00118","fc00119","fc00120","fc00121","fc00123","fc00124","fc00125","fc00126","fc00127","fc00128","fc00129","fc00130","fc00131","fc00132","fc00133","fc00134","fc00135","fc00136","fc00137","fc00138","fc00139","fc00140","fc00141","fc00142","fc00143","fc00011","fc00093","fc00029","fc00030","fc00080","fc00101","fc00095","fc00099","fc00098","fc00100","fc00103","fc00102","fc00144","fc00145","fc00107","fc00110"];



var name_inputs = [];



    input_data = JSON.stringify(input_data);
    var dataparams = {
        '_token':_token,
        'patient_id':patient_id,
        'visit_id':visit_id,
        'encounter_id':encounter_id,
        'doctor_id':doctor_id,
        'input_data':input_data,
        'dynamic_template_id':dynamic_template_id,
        'input_array':id_inputs,
        'name_array':name_inputs,
    };

    url = base_url +"/dynamic_template/caserecordSave";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
           // $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
           return data;
        },
        complete: function () {
           // $('body').LoadingOverlay("hide");
        }
    });
}

function fetchCustomFormDetails() {
    var datas = {};
    var template = {};
    $('#dynamic_template_data').find(':input').each(function () {
        var id = $(this).attr('id');
        var type = $(this).attr('type');
        var value = $(this).val();
        if (id) {
            datas[id] = value.trim();
        }
    });
    template['datas'] = datas;

    var vitals = {};
    var vital_value = {};
    if ($('#dynamic_template_data').find('#static_component_vital_form').length) {
        if ($("#static_component_vital_form #weight").val() == 1) {
            if ($("#static_component_vital_form #weight_value").val() != '') {
                vital_value["1"] = $("#static_component_vital_form #weight_value").val();
                vital_value["2"] = (($("#static_component_vital_form #weight_value").val()) / 2.205).toFixed(2);
            }
        }
        if ($("#static_component_vital_form #weight").val() == 2) {
            if ($("#static_component_vital_form #weight_value").val() != '') {
                vital_value["2"] = $("#static_component_vital_form #weight_value").val();
                vital_value["1"] = (($("#static_component_vital_form #weight_value").val()) * 2.205).toFixed(2);
            }
        }

        if ($("#static_component_vital_form #temperature").val() == 9) {
            if ($("#static_component_vital_form #temperature_value").val() != '') {
                vital_value["9"] = $("#static_component_vital_form #temperature_value").val();
                vital_value["10"] = (($("#static_component_vital_form #temperature_value").val() - 32) * (5 / 9)).toFixed(2);
            }
        }
        if ($("#static_component_vital_form #temperature").val() == 10) {
            if ($("#static_component_vital_form #temperature_value").val() != '') {
                vital_value["10"] = $("#static_component_vital_form #temperature_value").val();
                vital_value["9"] = (($("#static_component_vital_form #temperature_value").val() * (9 / 5)) + 32).toFixed(2);
            }
        }

        if ($("#static_component_vital_form #height").val() == 3) {
            if ($("#static_component_vital_form #height_value").val() != '') {
                vital_value["3"] = $("#static_component_vital_form #height_value").val();
                vital_value["4"] = (($("#static_component_vital_form #height_value").val()) * (30.48)).toFixed(2);
            }

        }
        if ($("#static_component_vital_form #height").val() == 4) {
            if ($("#static_component_vital_form #height_value").val() != '') {
                vital_value["4"] = $("#static_component_vital_form #height_value").val();
                vital_value["3"] = ((($("#static_component_vital_form #height_value").val()) / 30.48).toFixed(2));
                ;
            }

        }
        if ($("#static_component_vital_form #bp_systolic").val() != '') {
            vital_value["5"] = $("#static_component_vital_form #bp_systolic").val();
        }
        if ($("#static_component_vital_form #bp_diastolic").val() != '') {
            vital_value["6"] = $("#static_component_vital_form #bp_diastolic").val();
        }
        if ($("#static_component_vital_form #pulse").val() != '') {
            vital_value["7"] = $("#static_component_vital_form #pulse").val();
        }
        if ($("#static_component_vital_form #respiration").val() != '') {
            vital_value["8"] = $("#static_component_vital_form #respiration").val();
        }
        if ($("#static_component_vital_form #temperature_location").val() != '') {
            vital_value["11"] = $("#static_component_vital_form #temperature_location").val();
        }

        if ($("#static_component_vital_form #oxygen").val() != '') {
            vital_value["12"] = $("#static_component_vital_form #oxygen").val();
        }
        if ($("#static_component_vital_form #head").val() != '') {
            vital_value["13"] = $("#static_component_vital_form #head").val();
        }
        if ($("#static_component_vital_form #bmi").val() != '') {
            vital_value["14"] = $("#static_component_vital_form #bmi").val();
        }
        if ($("#static_component_vital_form #waist").val() != '') {
            vital_value["20"] = $("#static_component_vital_form #waist").val();
        }
        if ($("#static_component_vital_form #hip").val() != '') {
            vital_value["21"] = $("#static_component_vital_form #hip").val();
        }
        if ($("#static_component_vital_form input[name='visibility_status']:checked").val() == 1) {
            if ($("#static_component_vital_form #blood_sug").val().trim()) {
                vital_value["22"] = $("#static_component_vital_form #blood_sug").val();
            }
        } else if ($("#static_component_vital_form input[name='visibility_status']:checked").val() == 2) {
            if ($("#static_component_vital_form #blood_sug").val().trim()) {
                vital_value["23"] = $("#static_component_vital_form #blood_sug").val();
            }
        }

        vitals['vital_data'] = vital_value;
        vitals['vital_date'] = $('#static_component_vital_form #time_taken').val();
        vitals['batch_no'] = $('#static_component_vital_form #latest_vital_batch').val();
        vitals['vital_remarks'] = $('#static_component_vital_form #remarks').val();
    }
    template['vitals'] = vitals;

    var allergies = {};
    if ($('#dynamic_template_data').find('#static_component_allergy_form').length) {
        var values_allergy = $("#static_component_allergy_form input[name='alergylist_medicine_code_hidden[]']").map(function () {
            var tt = $(this).val();
            if (tt != '') {
                return $(this).val();
            }
        }).get();
        var allergy_ct = values_allergy.length;
        var other_allergies = $("#static_component_allergy_form #otherAllergies").val().trim();
        if (values_allergy.length > 0 || other_allergies != '') {
            var allergy_arr = [];
            $('#static_component_allergy_form #MedicineAllergyListData').find('tr').each(function (key, val) {
                var allergic_obj = {};
                if ($(val).find("input[name='alergylist_medicine_code_hidden[]']").val() != '') {
                    allergic_obj.allergymedicine = $(val).find("input[name='allergymedicine[]']").val();
                    allergic_obj.medicine_code = $(val).find("input[name='alergylist_medicine_code_hidden[]']").val();
                    allergic_obj.alrg_type = $(val).find("input[name='alrg_type[]']").val();

                    allergy_arr.push(allergic_obj);
                }
            })
            allergies['allergies'] = allergy_arr;
            allergies['other_allergies'] = other_allergies;
        }
    }
    template['allergies'] = allergies;

    return JSON.stringify(template);
}

function fetchPatientHeader(){
    url = base_url +"/emr/fetchPatientDetails";
    var dataparams = {
        'patient_id':$('#patient_id').val(),
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
        },
        success: function (data) {
           if(data!=0){
                var obj = JSON.parse(data);
                $('#id10').val(obj.uhid);
                $('#id11').val(obj.patient_name);
                $('#id12').val(obj.age+'/'+obj.gender);
                $('#id13').val(obj.phone);
                $('#id14').val(obj.address);
                $('#id15').val(obj.area);
                $('#id16').val(obj.visit_date);
                $('#id17').val(obj.doctor_name);
           }
        },
        complete: function () {
        }
    });
}
