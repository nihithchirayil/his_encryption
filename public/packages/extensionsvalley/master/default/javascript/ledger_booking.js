batch_array = {};
$(document).ready(function () {
    window.onload = function() {
    var typ_vl = localStorage.getItem("type_val");
var exp_pay_type =localStorage.getItem("exp_pay_type");
var ses_bill_entry_date = localStorage.getItem("bill_entry_date");
if(typ_vl != null){
$("#v_type_1").val(typ_vl);
$("#entry_date").val(ses_bill_entry_date);
$("#type").val(exp_pay_type);
$('#v_type_1').val(typ_vl).change();
}
localStorage.removeItem("type_val");
localStorage.removeItem("exp_pay_type");
localStorage.removeItem("bill_entry_date");
}
calculate_table_total('amount','total_amount_by_bill');
calculate_table_total('amnt_cd','total_amount_cd');
   if (window.location.href.indexOf("add_ledger_booking_entry") > -1)
    {
         insertDefaultBankDetails(1);
    }
    $("#amount_1").focus();
    setTimeout(function(){
    $('#ledger_item_desc-1').focus();
            },300);

      setTimeout(function(){
        var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('?');
//  if(sURLVariables[1] != 'undefined'){
//      var roww_id = (/id=(\d+)/.exec(sPageURL)[1]);
//      $("#edit_ledger_booking_"+roww_id).css("background-color","#edb2b2");
//      $('table tr#edit_ledger_booking_'+roww_id).find('input:first').focus();
//  }
            },300);
$(document).bind('keydown', function(e) {
  if(e.ctrlKey && (e.which == 83)) { // SAVE AND CONTINUE +s
    e.preventDefault();
    saveForm(0);
    return false;
  }else if(e.ctrlKey && (e.which == 80) && !e.altKey){ //purchase +p
      e.preventDefault();
      $('#v_type_1').val(1).change();
      return false;
  }else if(e.ctrlKey && e.altKey && (e.which == 80)){//payment +alt+p
      e.preventDefault();
      $('#v_type_1').val(12).change();
      return false;
  }else if(e.ctrlKey &&(e.which == 76)){//sales +l
      e.preventDefault();
      $('#v_type_1').val(2).change();
      return false;
  }else if(e.ctrlKey && (e.which == 74)){//journel +j
      e.preventDefault();
      $('#v_type_1').val(3).change();
      return false;
  }else if(e.ctrlKey && (e.which == 82) && !e.shiftKey){ //Receipt +r
      e.preventDefault();
      $('#v_type_1').val(5).change();
      return false;
  }else if(e.ctrlKey && (e.which == 84)){//Credit Note Import  +t
      e.preventDefault();
      $('#v_type_1').val(6).change();
      return false;
  }else if(e.ctrlKey && (e.which == 73)){// Sales import +i
      e.preventDefault();
      $('#v_type_1').val(7).change();
      return false;
  }else if(e.ctrlKey && (e.which == 77)&& !e.shiftKey){// Receipt Import +m
      e.preventDefault();
      $('#v_type_1').val(8).change();
      return false;
  }else if(e.ctrlKey && (e.which == 79)){//Contra +o
      e.preventDefault();
      $('#v_type_1').val(9).change();
      return false;
  }else if(e.ctrlKey && (e.which == 69)){//credit note +e
      e.preventDefault();
      $('#v_type_1').val(10).change();
      return false;
  }else if(e.ctrlKey && (e.which == 68)){//debit note +d
      e.preventDefault();
      $('#v_type_1').val(11).change();
      return false;
  }else if(e.ctrlKey && (e.which == 65)){//advance search popup +a
      e.preventDefault();
     editAdvanceSearch();
      setTimeout(function(){
        $("#ledger_item_desc-11").focus();
    },300);
      return false;
  }else if(e.altKey && (e.which == 65)){//advance search fn alt+a
      e.preventDefault();
      AdvanceEditSearch();
      return false;
  }else if(e.altKey && (e.which == 65)){//save exit alt+s
      e.preventDefault();
      saveForm(1);
      return false;
  }else if(e.ctrlKey && (e.which == 8)){//cancel alt+backspace
      e.preventDefault();
      backToList();
      return false;
  }
});
});
function calculate_table_total(cls, id) {
    var ttl_amnt = 0;
    $("." + cls).each(function () {
        var amnt = $(this).val();
        if (amnt != '') {
            ttl_amnt = parseFloat(ttl_amnt) + parseFloat(amnt);
        }

    });
    $("#" + id).val(ttl_amnt.toFixed(2));
    if ($("#total_amount_cd").val() != '' && $("#total_amount_by_bill").val() != '') {
        var cur_difference = parseFloat($("#total_amount_by_bill").val()) - parseFloat($("#total_amount_cd").val());
        $("#ttl_diff").val(cur_difference.toFixed(2));
        $("#ttl_dif_label").html('');
        $("#ttl_dif_label").html(cur_difference.toFixed(2));
    }

}
$('input[name="ledger[]"]').on('blur', function() {
        if(this.value==''){
        $('#payment_receipt_tbody_two tr').each(function () {
        if ($(this).find('input[name="ledger_cd[]"]').val() ==''){
       var focus_id =($(this).find('input[name="ledger_cd[]"]').attr("id"));
       $("#"+focus_id).focus();
    }
    });

    }
});



function select_item_desc(id, event, is_bank) {
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
    if (is_bank == 1) {
        var ajax_div = 'search_ledger_item_box-' + id;
        var ledger_desc = $('#ledger_item_desc-' + id).val();
    } else {
        var ajax_div = 'search_ledger_item_box_cd-' + id;
        var ledger_desc = $('#ledger_item_desc_cd-' + id).val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
             var url = $("#ins_base_url").val();
            $.ajax({
                type: "GET",
                url: url + "/master/searchLedger",
                data: 'ledger_desc=' + ledger_desc + '&search_ledger_desc=1&is_bank=' + is_bank,
                beforeSend: function () {
                    $("#" + ajax_div).css('top', '');
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();

                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).css('margin-left', '310px');
                    if(parseFloat($("#" + ajax_div+" li").size()) > 15){
                    $("#" + ajax_div).css('top', '-12vh');
                    $("#" + ajax_div).css('position', 'absolute');
                    }else{
                         $("#" + ajax_div).css('top', '');
                    }

                    $("#" + ajax_div).css('z-index', '1004');
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}


/**
 *
 * @param {type} e
 * @param {type} ledger_name
 * @param {type} id
 * @param {type} account_no
 * @param {type} branch
 * @param {type} current_balance
 * @returns {undefined}
 * FOR IS BANK FILL DETAILS
 */
function fillLedgerValues(e, ledger_name, id,ledger_group_id,opening_balence,is_asset) {
    var select_type = $("#type").val();
    if (select_type == 1) {
        var cr_dr_val = 'dr';
    } else {
        var cr_dr_val = 'cr';
    }
    var cls = "'amount'";
    var ids = "'total_amount_by_bill'";
    $(e).closest("tr").find("input[name='ledger[]']").val(ledger_name.replace(/&amp;/g, '&'));
    $(e).closest("tr").find("input[name='ledger_id[]']").val(id);
    $(e).closest("tr").find("input[name='group_id[]']").val(ledger_group_id);
    if(is_asset==1){
       $(e).parents('tr').find('.opening_balance_val').html(opening_balence);
    }
    $(e).parents("tr").find("input[name='amnt[]']").focus();
    $(e).closest("div").hide();
    var divElements = document.querySelectorAll('.search_ledger_item_box');
    var row_id = divElements.length;
    var row_id = parseFloat(row_id) + 1;
    response = '<tr><td ><input type="text" value="" name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,1)" id="ledger_item_desc-' + row_id + '">\n\
<input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-' + row_id + '">\n\
<div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-' + row_id + '" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;\n\
margin: -2px 0px 0px 0px;overflow-y: auto; width: 28%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"> \n\
</div></td><td><span class="opening_balance_val"></span></td>\n\
    <td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>\n\
    <td  style="display: none"><input type="text" value="" name="account_no[]" readonly="" class="form-control" id="account_no_' + row_id + '"></td>\n\
    <td  style="display: none"><input type="text" value="" name="branch[]" autocomplete="off"  readonly=""  class="form-control" id="branch_' + row_id + '">\n\
<input type="hidden" value="" name="group_id[]" autocomplete="off"  readonly=""  class="form-control" id="group_' + row_id + '"></td>\n\
    <td  style="display: none"><input type="text" value="" name="balance[]" autocomplete="off"   readonly=""  id="balance_' + row_id + '" class="form-control"  ></td>\n\
    <td ><input type="text" value="" name="amnt[]" class="form-control amount" onkeyup="number_validation(this);calculate_table_total(' + cls + ',' + ids + ');" id="amnt_' + row_id + '"> </td>\n\
    <td ><input type="text" value="" name="naration_head[]" class="form-control" id="naration_head_' + row_id + '"> </td>\n\
    <td style="display: none"> <input type="text" value="' + cr_dr_val + '" name="dr_cr[]" class="form-control inc_exp"  readonly=""  id="dr_cr_' + row_id + '"> </td>\n\
    <td>\n\
    <div class="container">\n\
    <textarea autocomplete="off" placeholder="Eg:bill1,bill2" onclick="autoheight(this.id);widhAdjust(2);" onkeyup="autoheight(this.id);widhAdjust(2);" onblur="widhAdjust(1)" class="reference_no" wrap="off" cols="35" rows="1"  name="refe_no[]" id="refe_no_' + row_id + '"  ></textarea></div>\n\
    <input type="hidden" value="" id="refe_details_' + row_id + '" name="refe_details[]" autocomplete="off"  class="form-control">\n\
    </td>\n\
    <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';
    var row_ck = 0;
        $('#payment_receipt_tbody tr').each(function () {
        if ($(this).find('input[name="ledger[]"]').val() ==''){
            row_ck = 1;
        }
    });
    if(row_ck == 0){
    $('#payment_receipt_tbody').append(response);
    $(".theadfix_wrapper").floatThead('reflow');
    }
$('input[name="ledger[]"]').on('blur', function() {
        if(this.value==''){
            $("#ledger_item_desc_cd-1").focus();
//        $('#payment_receipt_tbody_two tr').each(function () {
//        if ($(this).find('input[name="ledger_cd[]"]').val() !=''){
//       var focus_id =($(this).find('input[name="ledger_cd[]"]').attr("id"));
//       $("#"+focus_id).focus();
//    }
//    });

    }
});
previousLedgerHistory(id,cr_dr_val);
}
function pending_ammount_calc(e){
    var pending_amnt = $("#ttl_diff").val();
    $(e).closest("tr").find("input[name='amnt_cd[]']").val(pending_amnt);
    calculate_table_total('amnt_cd','total_amount_cd');
}
function fillLedgerValuesOthr(e, ledger_name, id,ledger_group_id,opening_balence,is_asset) {


    $(e).closest("tr").find("input[name='ledger_cd[]']").val(ledger_name.replace(/&amp;/g, '&'));
    $(e).closest("tr").find("input[name='ledger_id_cd[]']").val(id);
    $(e).closest("tr").find("input[name='group_id_cd[]']").val(ledger_group_id);
    $(e).closest("div").hide();
    if(is_asset==1){
       $(e).parents('tr').find('.opening_balance_val').html(opening_balence);
    }
    $(e).parents("tr").find("input[name='amnt_cd[]']").focus();
    fill_newrow();
    }
    function fill_newrow(){
    var cls = "'amnt_cd'";
    var ids = "'total_amount_cd'";
    //var select_types = $('input[name="type"]:checked').val();
    var select_types = $("#type").val();
    if (select_types == 1) {
        var cr_dr_vals = 'cr';
    } else {
        var cr_dr_vals = 'dr';
    }
    var divElements = document.querySelectorAll('.search_ledger_item_box');
    var row_id = divElements.length;
    var row_id = parseFloat(row_id) + 1;
    response = '<tr><td ><input type="text" value="" name="ledger_cd[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,0)" id="ledger_item_desc_cd-' + row_id + '" onblur="pending_ammount_calc(this)">\n\
<input type="hidden" value="" name="ledger_id_cd[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden_cd-' + row_id + '">\n\
<div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box_cd-' + row_id + '" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;\n\
margin: -2px 0px 0px 0px;overflow-y: auto; width: 27%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"> \n\
</div></td><td><span class="opening_balance_val"></span></td>\n\
<td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>\n\
<td ><input type="text" value="" name="amnt_cd[]" class="form-control amnt_cd" onkeyup="number_validation(this);calculate_table_total(' + cls + ',' + ids + ');" id="amnt_cd_' + row_id + '"> </td>\n\
<td ><input type="hidden" value="" name="naration[]" autocomplete="off"   id="naration_' + row_id + '" class="form-control"  >\n\
<input type="hidden" value="" id="group_id_cd_' + row_id + '" name="group_id_cd[]" autocomplete="off"  class="form-control">\n\
<input type="hidden" value="' + cr_dr_vals + '" name="dr_cr_cd[]" class="form-control inc_exp_cd"  readonly=""  id="dr_cr_cd_' + row_id + '"></td>\n\
<td style="display: none">  </td>\n\
    <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';

         var ck = 0;
        $('#payment_receipt_tbody_two tr').each(function () {
        if ($(this).find('input[name="ledger_cd[]"]').val() ==''){
            ck = 1;
        }
    });
    if(ck == 0){
            $('#payment_receipt_tbody_two').append(response);
    $(".datepicker").datetimepicker({
        format: 'DD-MM-YYYY'
    });
    }

}
$("#pay_rec_id_add").on('click', '.delete_return_entry_added', function () {
    $(this).closest('tr').remove();
});
$("#payment_receipt_tbody").on('click', '.delete_return_entry_added', function () {
    $(this).closest('tr').remove();
});
$("#refere_table_list").on('click', '.delete_return_entry_added_modal', function () {
    $(this).closest('tr').remove();
});
$("#pay_rec_id_2").on('click', '.delete_return_entry_added', function () {
    $(this).closest('tr').remove();
});
function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function delete_booking_entry(id) {
    if (confirm("Are you sure you want to Delete ?")) {
        var params = {delete_ledger_list: 1, detail_id: id};
         var url = $("#ins_base_url").val();
        $.ajax({
            type: "GET",
                url: url + "/master/deleteLedger",
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                if (data == 1) {
                    $.LoadingOverlay("hide");
                    $('#edit_ledger_booking_' + id).remove();
                    toastr.success('Deleted Succcessfuly !!');
                }
            },
            complete: function () {

            }
        });
    }
}



/**
 *
 * @param {type} k
 * @param {type} ref
 * VIEW THE MODAL
 */
function search_ref_no(k, ref = 0) {
    $("#search_ref_modal").modal('show');
    var ref_apend = '';
    var kl = 1;
    var btch = batch_array[k];
        if ($.type(btch) != "undefined" || btch != undefined) {
            for (var i = 0; i < btch.length; i++) {
                var obj = btch[i];
                var ref_no = obj.ref_no;
                var ref_date = obj.ref_date;
                ref_apend += ' <tr>\n\
            <td><input type="text" value="' + ref_no + '" name="multi_ref_no[]"  class="form-control multi_ref_no"  onblur="insert_multi_row()"></td>\n\
            <td><input type="text" value="' + ref_date + '" name="multi_ref_date[]"  class="form-control multi_ref_dt" id="multi_ref_date'+kl+'" ></td>\n\
            <td><i class="fa fa-trash-o delete_return_entry_added_modal" style="cursor:pointer"></i></td>\n\
            </tr>';
                kl++;
            }
        }else if (ref != '') {
        var JSONObject = JSON.parse(ref);
        for (var i = 0; i < JSONObject.length; i++) {

            var ref_no = JSONObject[i]["ref_no"];
            var ref_date = JSONObject[i]["ref_dt"];
            if (ref_no != '') {
                ref_apend += ' <tr>\n\
            <td><input type="text" value="' + ref_no + '" name="multi_ref_no[]"  class="form-control multi_ref_no"  onblur="insert_multi_row()"></td>\n\
            <td><input type="text" value="' + ref_date + '" name="multi_ref_date[]"  class="form-control multi_ref_dt" id="multi_ref_date'+kl+'"></td>\n\
            <td><i class="fa fa-trash-o delete_return_entry_added_modal" style="cursor:pointer"></i></td>\n\
            </tr>';
                kl++;
            }
        }
    }


    ref_apend += ' <tr>\n\
            <td><input type="text" value="" name="multi_ref_no[]"  class="form-control multi_ref_no"  onblur="insert_multi_row()"></td>\n\
            <td><input type="text" value="" name="multi_ref_date[]"  class="form-control multi_ref_dt" id ="multi_ref_date'+kl+'"></td>\n\
            <td><i class="fa fa-trash-o delete_return_entry_added_modal" style="cursor:pointer"></i></td>\n\
            </tr>';
    $("#refere_table_list_body").html(ref_apend);

    $("#bill_id").val('');
    $("#search_ref_modal_id").val(k);
    $("#append_div").html('');
        $('#refere_table_list_body').find(".multi_ref_dt").datetimepicker({
        format: 'DD-MM-YYYY'
    });
}
/**
 *
 * APPENDING ROW TO THE MODAL BOX
 */
function insert_multi_row() {
    var apnd = '';
    var divElements = document.querySelectorAll('.multi_ref_no');
    var ij = divElements.length;
    ij++;
    apnd += ' <tr>\n\
            <td><input type="text" value="" name="multi_ref_no[]"  class="form-control multi_ref_no" id="multi_ref_no' + ij + '" onblur="insert_multi_row()"></td>\n\
            <td><input type="text" value="" name="multi_ref_date[]"  class="form-control multi_ref_dt" id="multi_ref_date' + ij + '" ></td>\n\
            <td><i class="fa fa-trash-o delete_return_entry_added_modal" style="cursor:pointer"></i></td>\n\
            </tr>';

    $("#refere_table_list_body").append(apnd);
            $('#refere_table_list_body').find(".multi_ref_dt").datetimepicker({
        format: 'DD-MM-YYYY'
    });
}
/**
 *
 * SAVING MULTI REFERCNCE DATA IN JSON FORMAT TO VIEW AGAIN WHEN CLICKING THE ICON
 */
function saveMultiReference() {
    var abc = '';
    var kl = 1;

    var insert_row = $("#search_ref_modal_id").val();
    batch_array[insert_row] = [];
    $(".multi_ref_no").each(function () {
        var ref_no = $(this).val();
        if (ref_no != '' && ref_no != null && $.type(ref_no) != "undefined") {
            if ($.type($("#multi_ref_date" + kl).val()) != "undefined") {
                var ref_date = $("#multi_ref_date" + kl).val();
            } else {
                var ref_date = '-';
            }
            abc += '[' + ref_no + ',' + ref_date + '],';
            console.log(ref_date);
            batch_array[insert_row].push({ref_no: ref_no, ref_date: ref_date});
            console.log(batch_array);
        }

        kl++;
    });


    $("#refe_details_" + insert_row).val(abc);
    var ids = $("#search_ref_modal_id").val();
    $("#refe_no_" + ids).val("multiple");
    $("#refe_dt_" + ids).val("multiple");
}


/**
 *
 * @param {type} val
 * WHEN CHANGING BOOKING TYPE LABELS CHANGED EACH OTHER FROM DEBIT ACCOUNT TO CREDIT ACCOUNT AND VICEVERSA
 */
function changeBookingType(val) {
    if (val == 1) {
        $("#title_show").html('<b>Debit Account</b>');
        $("#title_show_btm").html('<b>Credit Account</b>');
        $(".inc_exp").val('dr');
        $(".inc_exp_cd").val('cr');
        insertDefaultBankDetails(1);
    } else if(val == 2) {
        $("#title_show").html('<b>CreditAccount</b>');
        $("#title_show_btm").html('<b>Debit Account</b>');
        $(".inc_exp").val('cr');
        $(".inc_exp_cd").val('dr');
        insertDefaultBankDetails(2);
    }
}
function insertDefaultBankDetails(val){
    if(val==1){
    var json_bank = JSON.parse($("#bank_recep").val());
    }else{
    var json_bank = JSON.parse($("#bank_hidden_payment").val());
    }
//    $("#ledger_item_desc-1").val(json_bank[0].ledger_name);
//    $("#ledger_item_desc_hidden-1").val(json_bank[0].id);
//    $("#account_no_1").val(json_bank[0].account_no);
//    $("#branch_1").val(json_bank[0].branch);
//    $("#balance_1").val(json_bank[0].current_balance);
//    $("#group_id_1").val(json_bank[0].ledger_group_id);
}
/**
 *
 * @returns {Boolean}
 * ADDIG BOOKING ENTRY TO DB
 */
function saveForm(save_type) {

    var db_total = $("#total_amount_by_bill").val();
    var cd_total = $("#total_amount_cd").val();
    var ttl_diff = parseFloat(db_total) - parseFloat(cd_total);
    $("#ttl_diff").val(ttl_diff);
    if ($("#type").val() == '') {
        bootbox.alert("Please select voucher Type");
        return false;
    }
    localStorage.setItem("type_val", $("#v_type_1").val());
    localStorage.setItem("exp_pay_type", $("#type").val());
    localStorage.setItem("bill_entry_date", $("#entry_date").val());
    if (parseFloat(cd_total) != parseFloat(db_total)) {
        bootbox.alert("Debit and Credit totals are not equal");
        return false;
    }
     var filled_item_desc_count = $('input[name="ledger_cd[]"]').filter((i, el) => el.value.trim() != '').length;
   var filled_amount_count = $('input[name="amnt_cd[]"]').filter((i, el) => el.value.trim() != '').length;
   if(parseFloat(filled_amount_count) != parseFloat(filled_item_desc_count)){
       bootbox.alert("Plese fill ledger details for each amount entry");return;;
   }
   if(parseFloat(filled_amount_count) == 0 && parseFloat(filled_item_desc_count)==0 ){
       bootbox.alert("No data selected for update");return;
   }

    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/master/save_ledger_booking",
        data: $("#form").serialize(),
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            if(save_type==1){
            $('#add_share_holder_spin').removeClass('fa fa-save');
            $('#add_share_holder_spin').addClass('fa fa-spinner fa-spin');
        }else{
            $('#add_share_holder_spin_t').removeClass('fa fa-thumbs-up');
            $('#add_share_holder_spin_t').addClass('fa fa-spinner fa-spin');
        }
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            if (data == 1) {
                toastr.success('Added Successfully...');
            } else {
                toastr.error('Error Occured Please try again later...');
            }
            if(save_type==1){
                backToList();
            }else{
                pageRefresh();
            }
        },
        complete: function () {
            $('#add_share_holder_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin').addClass('fa fa-save');

        }
    });
}
/**
 *
 * FOR UPDATE LEDGER BOOKING
 */
function saveEditForm(save_type) {
    var from_type = $("#from_type").val();
    var db_total = $("#total_amount_by_bill").val();
    var cd_total = $("#total_amount_cd").val();
    var ttl_diff = parseFloat(db_total) - parseFloat(cd_total);
    $("#ttl_diff").val(ttl_diff);
    if ($("#type").val() == '') {
       bootbox.alert("Please select voucher Type");
        return false;
    }
    localStorage.setItem("type_val", $("#v_type_1").val());
    localStorage.setItem("exp_pay_type", $("#type").val());
    localStorage.setItem("bill_entry_date", $("#entry_date").val());
    if (parseFloat(cd_total) != parseFloat(db_total)) {
        bootbox.alert("Debit and Credit totals are not equal");
        return false;
    }
   var filled_item_desc_count = $('input[name="ledger_cd[]"]').filter((i, el) => el.value.trim() != '').length;
   var filled_amount_count = $('input[name="amnt_cd[]"]').filter((i, el) => el.value.trim() != '').length;
   if(parseFloat(filled_amount_count) != parseFloat(filled_item_desc_count)){
       bootbox.alert("Plese fill ledger details for each amount entry");return;;
   }
   if(parseFloat(filled_amount_count)==0 && parseFloat(filled_item_desc_count)==0 ){
       bootbox.alert("No data selected for update");return;
   }

    var url = $("#ins_base_url").val();
    $.ajax({
        type: "POST",
        url: url + "/master/update_ledger_booking_entry",
        data: $("#form1").serialize(),
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            if(save_type==1){
            $('#add_share_holder_spin').removeClass('fa fa-save');
            $('#add_share_holder_spin').addClass('fa fa-spinner fa-spin');
        }else{
            $('#add_share_holder_spin_t').removeClass('fa fa-thumbs-up');
            $('#add_share_holder_spin_t').addClass('fa fa-spinner fa-spin');        }
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            if (data == 1) {
                toastr.success('Updated Successfully...');
            } else {
                toastr.error('Error Occured Please check details entered..');
            }
            if(from_type == 0){
           if(save_type==1){
                backToList();
            }else{
                window.location.href = url + "/master/add_ledger_booking_entry";
            }
          }else{
              $("#editLeaderDetailModel").modal('hide');
          }
        },
        complete: function () {
            $('#add_share_holder_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_share_holder_spin').addClass('fa fa-save');
        }
    });
}
function backToList() {
    var url = $("#ins_base_url").val();
    localStorage.removeItem("type_val");
    localStorage.removeItem("exp_pay_type");
    localStorage.removeItem("bill_entry_date");
    window.location = url + "/master/ledger_booking_entry_list";
}
        /**
         *
         * @param {type} e
         * @returns {undefined}
         * SHOWS MODAL BOX FOR NEW LEDGER ADD
         */
function saveNewLedger(e){
    var led_id = $(e).parents("tr").find('.ledger_item_desc').attr('id');
    var led_name = $(e).parents("tr").find('.ledger_item_desc').val();
    var row_ids = (led_id).split('-');
    var url = $("#ins_base_url").val();
    $.ajax({
        type: "GET",
        url: url + "/master/list_ledger_master",
        data: {data:1},
        beforeSend: function () {
             $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            $('#add_new_ledger_spin').removeClass('fa fa-plus');
            $('#add_new_ledger_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                var html_content = $(data).find('#ledger_master_form_whole');
                $("#append_ledger_master").html(html_content);
                $("#ledger_name_update").val(led_name);
                $("#led_cancel_btn").hide();
                $("#from_booking_entry").val('1');
                $("#row_ids").val(led_id);
                 $('#ledger_master_form_whole').removeClass('col-md-4 padding_sm');
                $("#search_ref_modal").modal('show');
                $.LoadingOverlay("hide");
            }
        },
        complete: function () {
            $('#add_new_ledger_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_new_ledger_spin').addClass('fa fa-plus');
            $(".ajaxSearchBox").hide();
        }
    });
}
   function select_group_asset(id, event) {
    var url = $("#ins_base_url").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        var ajax_div = 'group_asset_name_box-1';
        var ledger_desc = $('#group_asset_name').val();

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: url + "/master/list_asset_group",
                data: 'search_group_asset=' + ledger_desc,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillAssetGroupValues(e, ledger_name, id,typ,is_asset) {
      event.preventDefault();
      $("#group_asset_name").val(ledger_name.replace(/&amp;/g, '&'));
      $("#group_asset_id").val(id);
      $("#is_asset_etry").val(is_asset);
      if(is_asset == 0){
    $(".credit_div").show();
 }else{
        $(".credit_div").hide();
        $("#credit_days").val('');
        $("#credit_amount").val('');
        $("input[type=radio][name=pl_type]").prop('checked', false);
    }
}
function showCreditData(e){
    if($(e).val() > 0){
        $(".credit_div").show();
    }else{
        $(".credit_div").hide();
        $("#credit_days").val('');
        $("#credit_amount").val('');
        $("input[type=radio][name=pl_type]").prop('checked', false);
    }
}
function saveLedgerMaster(){
   var list_url = $("#ins_base_url").val();

   var url = $('#ledger_master_form').attr('action');
   var led_id = $("#row_ids").val();
   var row_ids = (led_id).split('-');
   if($("#ledger_name_update").val()==''){
       alert("Please enter Ledger name");
       return false;
   }
   if($("#group_asset_id").val()=='' || $("#group_asset_name").val()==''){
       alert("Please select group");
       return false;
   }
  $.ajax({
        type: "GET",
        url: url ,
        data: $("#ledger_master_form").serialize(),
        beforeSend: function () {
             if($("#from_booking_entry").val() == 1){
                 $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
             }
            $('#add_new_ledger_spin').removeClass('fa fa-save');
            $('#add_new_ledger_spin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var json_data = JSON.parse(data);
            if (json_data.success == 2) {
                 toastr.error('Default Payment Bank already selected !...');
            }
             if (json_data.success == 3) {
                 toastr.error('Default Receive Bank already selected !...');
            }
            if (json_data.success == 4) {
                 toastr.error('Ledger name exist !...');
            }
            if (json_data.success == 0) {
                 toastr.error('Error occured !...');
            }
            if (json_data.success == 1) {
                toastr.success(json_data.message);
                if($("#from_booking_entry").val() == 1){
                    var row_idss = row_ids[1];
                    var up_below_part = row_ids[0]
                    if(up_below_part == 'ledger_item_desc_cd'){
                    $("#ledger_item_desc_cd-"+row_idss).val(json_data.ledger_name);
                    $("#ledger_item_desc_hidden_cd-"+row_idss).val(json_data.ledger_id);
                    $("#group_id_cd_"+row_idss).val(json_data.group_id);
                    fill_newrow();

                    }else{
                    $("#ledger_item_desc-"+row_idss).val(json_data.ledger_name);
                    $("#ledger_item_desc_hidden-"+row_idss).val(json_data.ledger_id);
                    $("#group_id_"+row_idss).val(json_data.group_id);
                     fillLedgerValues('',json_data.ledger_name,json_data.ledger_id,json_data.group_id);
                }
                    $.LoadingOverlay("hide");
                    $("#search_ref_modal").modal('hide');
                }else{
                window.location = list_url + "/master/list_ledger_master";
            }
            }
        },
        complete: function () {
            $('#add_new_ledger_spin').removeClass('fa fa-spinner fa-spin');
            $('#add_new_ledger_spin').addClass('fa fa-save');
        }
    });
}
function selectExpenseIncome(val){
    var url = $("#ins_base_url").val();
    var generated_vouchr_text = $("#generated_vouchr_text").val();
    $.ajax({
                type: "GET",
                url: url + "/master/select_expense_income",
                data: 'search_expense_income=' + val+'&generated_vouchr_text='+generated_vouchr_text,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    var json_data1 = JSON.parse(data);
                    $.LoadingOverlay("hide");
                    changeBookingType(json_data1.v_type);
                    $("#type").val(json_data1.v_type);
                    if(json_data1.v_no != 0){
                    $("#generated_vouchr_label").html('<b>'+json_data1.v_no+'</b>');
                     $("#generated_vouchr_text").val(json_data1.v_no);
                 }
                 setTimeout(function(){
                    $("#ledger_item_desc-1").focus();
                },300);
                },
                complete: function () {
                }
            });
}
function pageRefresh(){
window.location.reload();
}
function widhAdjust(v){
    if(v==1){
         $("#th_ref").css("width","11%");
         $("#th_nar").css("width","37%");
         $(".reference_no").css("height", "26px");
    }else{
    $("#th_nar").css("width","11%");
    $("#th_ref").css("width","37%");
    }
    console.log(v);
    $(".theadfix_wrapper").floatThead('reflow');
}
function previousLedgerHistory(id,cr_dr_val){
     var url = $("#ins_base_url").val();
        $.ajax({
                type: "GET",
                url: url + "/master/select_previous_booking",
                data: 'ledger_id=' + id+'&cr_dr='+cr_dr_val,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                var json_data = JSON.parse(data);
                $.LoadingOverlay("hide");
                if(parseFloat(json_data.cnt) > 0){
                    for(var i=0; i < 1;i++){
                        $('#payment_receipt_tbody_two tr').each(function () {
                            if ($(this).find('input[name="ledger_cd[]"]').val() ==''){
                                var ledger_name_id =($(this).find('input[name="ledger_cd[]"]').attr("id"));
                                var ledger_id =($(this).find('input[name="ledger_id_cd[]"]').attr("id"));
                                var ledger_group_id =($(this).find('input[name="group_id_cd[]"]').attr("id"));
                                $("#"+ledger_name_id).val(json_data.dtls[i].ledger_name);
                                $("#"+ledger_id).val(json_data.dtls[i].ledger_id);
                                $("#"+ledger_group_id).val(json_data.dtls[i].group_id);
                            }
                        });
                        fill_newrow();
                    }
                }
                },
                complete: function () {
                }
            });
}
function AdvanceEditSearch(){
    var url = $("#ins_base_url").val();
    var ledger_name = $("#ledger_item_desc-11").val();
    if(ledger_name !=''){
    var ledger_id = $("#ledger_item_desc_hidden-11").val();
    }else{
        var ledger_id = '';
    }
    var amount = $("#amount_adv_edit").val();
    var narration = $("#narration_adv_edit").val();
    var voucher_no = $("#voucherno_adv_edit").val();
    var from_adv_date = $("#from_entry_date_adv").val();
    var to_adv_date = $("#to_entry_date_adv").val();
    var v_type_adv = $("#v_type_adv").val();
    var ref_adv_edit = $("#ref_adv_edit").val();
    var params = {ledger_id: ledger_id, amount: amount,narration:narration,voucher_no:voucher_no,from_adv_date:from_adv_date,to_adv_date:to_adv_date,
    v_type_adv:v_type_adv,ref_adv_edit:ref_adv_edit};
        $.ajax({
                type: "GET",
                url: url + "/master/advanced_ledger_edit_list",
                data: params,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                 $("#edit_apend_modal").html(data);
                $("#adv_search_modal").modal('show');
                $.LoadingOverlay("hide");
                $(".theadfix_wrapper").floatThead('reflow');
                },
                complete: function () {
                }
            });

}
 function select_item_desc_adv(id, event, is_bank) {
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
    if (is_bank == 1) {
    var ajax_div = 'search_ledger_item_box-' + id;
    var ledger_desc = $('#ledger_item_desc-' + id).val();
    } else {
    var ajax_div = 'search_ledger_item_box_cd-' + id;
    var ledger_desc = $('#ledger_item_desc_cd-' + id).val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


    if (ledger_desc == "") {
    $("#" + ajax_div).html("");
    } else {
    var url = $("#ins_base_url").val();
    $.ajax({
    type: "GET",
                url: url + "/master/searchLedger",
            data: 'ledger_desc=' + ledger_desc + '&search_ledger_desc=1&is_adv=' + 1,
            beforeSend: function () {
            $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#" + ajax_div).html(html).show();
            $("#" + ajax_div).find('li').first().addClass('liHover');
            },
            complete: function () {
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            }
    });
    }

    } else {
    ajaxProgressiveKeyUpDown(ajax_div, event);
    }
    }
    function fillLedgerValues_adv(e, ledger_name, id, ledger_group_id) {
    $("#ledger_item_desc-11").val(ledger_name.replace(/&amp;/g, '&'));
    $("#ledger_item_desc_hidden-11").val(id);
    }
   function refreshAllData(){
    localStorage.removeItem("type_val");
    localStorage.removeItem("exp_pay_type");
    localStorage.removeItem("bill_entry_date");
    window.location.reload();
    }
    function editAdvanceSearch(){
$('#fill_advanced_edit_tbody').find('input:text, select')
                    .each(function () {
                        $(this).val('');
                    });

        $("#adv_search_modal").modal('show');
        setTimeout(function(){
        $("#ledger_item_desc-11").focus();
    },300);
    }
