var insurance_array = new Array();
var pricing_details = new Array();
var patient_more_details = new Array();
var insurance_count = new Array();
var insurance_details = new Array();
var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
$(document).ready(function () {
    $('#patientGeneralPricingBtn').trigger('click');
    getPatientAddress(1);
    //checkProWantOrNot();
});

// function getPatientInsurance() {
//     var url = base_url + "/patient_register/getPatientInsurance";
//     var patient_id = $("#patient_id_hidden").val();
//     var insurance_array_string = JSON.stringify(insurance_array);
//     var company_id = $('#commpany_id_hidden').val();
//     var pricing_id = $('#pricing_id_hidden').val();
//     var param = { patient_id: patient_id, company_id: company_id, pricing_id: pricing_id, insurance_array_string: insurance_array_string };
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: param,
//         beforeSend: function () {
//             $('#patientInsuranceBtn').attr('disabled', true);
//             $('#patientInsuranceSpin').removeClass('fa fa-umbrella');
//             $('#patientInsuranceSpin').addClass('fa fa-spinner fa-spin');
//             $('#patientInsuranceModelHeader').html('Patient Insurance Details');
//             $('#patient_modeldata_from_type').val(1);
//         },
//         success: function (data) {
//             $('#patientInsuranceModelDiv').html(data);
//             $("#patientInsuranceModel").modal({
//                 backdrop: 'static',
//                 keyboard: false
//             });
//         },
//         complete: function () {
//             $('#patientInsuranceBtn').attr('disabled', false);
//             $('#patientInsuranceSpin').removeClass('fa fa-spinner fa-spin');
//             $('#patientInsuranceSpin').addClass('fa fa-umbrella');
//             setTimeout(function () {
//                 $('.insurance_datepicker').datetimepicker({
//                     format: 'MMM-DD-YYYY',
//                 });
//                 var secondary_status = $('#patient_secondary_insurance').is(":checked");
//                 if (secondary_status) {
//                     $('.secondary_insurance').prop('disabled', false);
//                 } else {
//                     $('.secondary_insurance').prop('disabled', true);
//                 }

//                 var tertiary_status = $('#patient_tertiary_insurance').is(":checked");
//                 if (tertiary_status) {
//                     $('.tertiary_insurance').prop('disabled', false);
//                 } else {
//                     $('.tertiary_insurance').prop('disabled', true);
//                 }
//                 getInsurancePricing('primary');
//                 getInsurancePricing('secondary');
//                 getInsurancePricing('tertiary');
//             }, 400);
//         },
//         error: function () {
//             toastr.error("Error Please Check Your Internet Connection");
//         }
//     });
// }


function getAllPricingDetails() {
    var url = base_url + "/patient_register/getAllPricingDetails";
    var patient_id = $("#patient_id_hidden").val();
    var pricing_details_string = JSON.stringify(pricing_details);
    var company_id = $('#commpany_id_hidden').val();
    var pricing_id = $('#pricing_id_hidden').val();
    var param = { patient_id: patient_id, company_id: company_id, pricing_id: pricing_id, pricing_details_string: pricing_details_string };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#patientAllPricingDetailsBtn').attr('disabled', true);
            $('#patientAllPricingDetailsSpin').removeClass('fa fa-money');
            $('#patientAllPricingDetailsSpin').addClass('fa fa-spinner fa-spin');
            $('#patientInsuranceModelHeader').html(' Pricing Details');
            $('#patient_modeldata_from_type').val(2);
        },
        success: function (data) {
            $('#patientInsuranceModelDiv').html(data);
            $("#patientInsuranceModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#sponsor_expiry_date').datetimepicker({
                format: 'MMM-DD-YYYY',
            });
            $('#patientAllPricingDetailsBtn').attr('disabled', false);
            $('#patientAllPricingDetailsSpin').removeClass('fa fa-spinner fa-spin');
            $('#patientAllPricingDetailsSpin').addClass('fa fa-money');
            getInsurancePricing('sponsor');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function getPatientMoreDetails() {

    var url = base_url + "/patient_register/getPatientMoreDetails";
    var patient_id = $("#patient_id_hidden").val();
    var patient_more_details_string = JSON.stringify(patient_more_details);
    var param = { patient_id: patient_id, patient_more_details_string: patient_more_details_string };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#patientMoreDetailsBtn').attr('disabled', true);
            $('#patientMoreDetailsSpin').removeClass('fa fa-info');
            $('#patientMoreDetailsSpin').addClass('fa fa-spinner fa-spin');
            $('#patientInsuranceModelHeader').html('Patient More Information');
            $('#patient_modeldata_from_type').val(3);
        },
        success: function (data) {
            $('#patientInsuranceModelDiv').html(data);
            $("#patientInsuranceModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#patientMoreDetailsBtn').attr('disabled', false);
            $('#patientMoreDetailsSpin').removeClass('fa fa-spinner fa-spin');
            $('#patientMoreDetailsSpin').addClass('fa fa-info');
            setTimeout(function () {
                $('#patient_passport_expiry').datetimepicker({
                    format: 'MMM-DD-YYYY',
                });
            }, 400);
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });


}

function getInsurancePricing(from_type) {
    var url = base_url + "/patient_register/getInsurancePricing";
    var insurance_company = $("#" + from_type + "_insurance_company").val();
    var insurance_pricing = $("#" + from_type + "_insurance_pricing_hidden").val();
    var html_string = "";
    if (insurance_company != '') {
        var param = { insurance_company: insurance_company, insurance_pricing: insurance_pricing };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                $("#patientInsuranceModelDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("#" + from_type + "_insurance_pricing").html(data);
            },
            complete: function () {
                $("#patientappoinment_div").LoadingOverlay("hide");
                $("#patientInsuranceModelDiv").LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        html_string = "<option value=''>Select</option>";
        $("#" + from_type + "_insurance_pricing").html(html_string);
    }

}

function getInsurancePricing_ins(from_type) {
    var url = base_url + "/patient_register/getInsurancePricing";
    var insurance_company = $("#insurance_company" + from_type).val();
    var insurance_pricing = $("#insurance_pricing_hidden" + from_type).val();
    var html_string = "";
    if (insurance_company != '') {
        var param = { insurance_company: insurance_company, insurance_pricing: insurance_pricing };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $("#patientInsuranceModelDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                $("#insurance_pricing" + from_type).html(data);
            },
            complete: function () {
                $("#patientInsuranceModelDiv").LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        html_string = "<option value=''>Select</option>";
        $("#" + from_type + "_insurance_pricing").html(html_string);
    }

}

function saveModelData() {
    var model_from_type = $('#patient_modeldata_from_type').val();
    if (model_from_type == '1') {
        savePatientInsurance();
    } else if (model_from_type == '2') {
        savePricingDetails();
    } else if (model_from_type == '3') {
        saveOtherDetails();
    }
}


function saveOtherDetails() {
    patient_more_details = new Array();
    patient_more_details.push({
        'patient_blood_grp': $('#patient_blood_grp').val(),
        'patient_MaritalStatus': $('#patient_MaritalStatus').val(),
        'patient_relation': $('#patient_relation').val(),
        'patient_relationname': $('#patient_relationname').val(),
        'patient_occupation': $('#patient_occupation').val(),
        'patient_idproof_type': $('#patient_idproof_type').val(),
        'patient_id_number': $('#patient_id_number').val(),
        'patient_reference_type': $('#patient_reference_type').val(),
        'patient_hospital_reference': $('#patient_hospital_reference').val(),
        'patient_doctor_reference': $('#patient_doctor_reference').val(),
        'patient_refferal_remarks': $('#patient_refferal_remarks').val(),
        'vip_patient_status': $('#vip_patient_status').is(":checked"),
        'vip_patient_comments': $('#vip_patient_comments').val(),
        'international_patient_status': $('#international_patient_status').is(":checked"),
        'patient_passport_no': $('#patient_passport_no').val(),
        'patient_passport_expiry': $('#patient_passport_expiry').val(),
        'patient_organ_donation': $('input[name=patient_organ_donation]:checked').val(),
        'patient_special_notes': $('#patient_special_notes').val(),
        'co_type': $('#co_type').val(),
        'co_name': $('#co_name').val(),
        'co_mobile': $('#co_mobile').val(),
    });
    toastr.success("Patient More Information Added Successfully");
    $('#patientInsuranceModel').modal('toggle');
}


function savePricingDetails() {
    var sponsor_insurance_company = $('#sponsor_insurance_company').val();
    var sponsor_insurance_pricing = $('#sponsor_insurance_pricing').val();
    if (sponsor_insurance_company && sponsor_insurance_pricing) {
        pricing_details = new Array();
        pricing_details.push({
            'sponsor_insurance_company': $('#sponsor_insurance_company').val(),
            'sponsor_insurance_pricing': $('#sponsor_insurance_pricing').val(),
            'sponsor_expiry_date': $('#sponsor_expiry_date').val(),
        });
        toastr.success("Pricing Details Added Successfully");
        $('#patientInsuranceModel').modal('toggle');
    } else {
        toastr.warning("Please Select Insurance Company and Insurance Pricing");
    }
}


function savePatientInsurance() {
    var primary_insurance_company = $("#primary_insurance_company").val();
    var primary_insurance_pricing = $("#primary_insurance_pricing").val();
    var primary_authorisation_letter_no = '';
    var primary_insurance_id_no = '';
    var primary_credit_limit = '';
    var primary_card_type = '';
    var primary_color_of_card = '';
    var primary_issue_date = '';
    var primary_expiry_date = '';
    var primary_dependent_relation = '';
    if (primary_insurance_company != '') {
        if (primary_insurance_pricing != '') {
            primary_authorisation_letter_no = $('#primary_authorisation_letter_no').val();
            primary_insurance_id_no = $('#primary_insurance_id_no').val();
            primary_credit_limit = $('#primary_credit_limit').val();
            primary_card_type = $('#primary_card_type').val();
            primary_color_of_card = $('#primary_color_of_card').val();
            primary_issue_date = $('#primary_issue_date').val();
            primary_expiry_date = $('#primary_expiry_date').val();
            primary_dependent_relation = $('#primary_dependent_relation').val();
        } else {
            toastr.warning("Please Select Primary Insurance Pricing");
            return false;
        }
    } else {
        toastr.warning("Please Select Primary Insurance Company");
        return false;
    }
    insurance_array = new Array();
    insurance_array.push({
        'primary_insurance_company': primary_insurance_company,
        'primary_insurance_pricing': primary_insurance_pricing,
        'primary_authorisation_letter_no': primary_authorisation_letter_no,
        'primary_insurance_id_no': primary_insurance_id_no,
        'primary_credit_limit': primary_credit_limit,
        'primary_card_type': primary_card_type,
        'primary_color_of_card': primary_color_of_card,
        'primary_issue_date': primary_issue_date,
        'primary_expiry_date': primary_expiry_date,
        'primary_dependent_relation': primary_dependent_relation,
    });
    toastr.success("Insurance Successfully Added");
    $('#patientInsuranceModel').modal('toggle');
}


function checkInsuranceStatus(from_type) {
    var status = true;
    if (from_type == '1') {
        status = $('#patient_secondary_insurance').is(":checked");
        if (status) {
            $('.secondary_insurance').prop('disabled', false);
        } else {
            $('.secondary_insurance').prop('disabled', true);
        }
    } else if (from_type == '2') {
        status = $('#patient_tertiary_insurance').is(":checked");
        if (status) {
            $('.tertiary_insurance').prop('disabled', false);
        } else {
            $('.tertiary_insurance').prop('disabled', true);
        }
    }
}


function getPatientAddress(from_type) {
    var url = base_url + "/patient_register/getPatientAddress";
    var patient_country = $("#patient_country").val();
    var patient_state = $("#patient_state").val();
    var company_state = $("#company_state").val();
    var company_district = $("#company_district").val();
    var param = { from_type: from_type, company_state: company_state, company_district: company_district, patient_country: patient_country, patient_state: patient_state };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (from_type == '1') {
                $("#patient_state").html(data);
                $("#patient_state").select2();
                $("#patient_district").html("<option value=''>Select</option>");
                $("#patient_district").select2();
            } else if (from_type == '2') {
                $("#patient_district").html(data);
                $("#patient_district").select2();
            }
        },
        complete: function () {
            $("#patientappoinment_div").LoadingOverlay("hide");
            if (from_type == '1') {
                getPatientAddress(2);
            }
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getPricingDetalis(pricing_id) {
    var url = base_url + "/patient_register/getPricingDetalis";
    var patient_pricing = $("#" + pricing_id).val();
    var param = { patient_pricing: patient_pricing };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#pricing_company').val(obj.pricing_name);
            $('#company_pricing').val(obj.company_name);
            $('#company_type_hidden').val(obj.company_type);
            $('#commpany_id_hidden').val(obj.company_id);
            $('#pricing_id_hidden').val(patient_pricing);
        },
        complete: function () {
            $("#patientappoinment_div").LoadingOverlay("hide");
            var doctor_id = $('#patient_doctor').val();
            if (doctor_id) {
                getDoctorChageSplitUp(patient_pricing);
            }
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function changeIsFreeStatus() {
    if ($('#is_reg_free')) {
        if ($('#is_reg_free').is(':checked')) {
            $('#doctor_charges_hidden').html(0.0);
        } else {
            var charges_amt = $('#charges_hidden').val();
            $('#doctor_charges_hidden').html(parseFloat(charges_amt));
        }
    }
}

function createPatientRegistration() {
    location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
    if (location_code && location_code != 0) {
        var is_free_visit = false;
        if ($('#add_area').is(':checked')) {
            var patient_area = $('#add_patient_area').val();
        } else {
            var patient_area = $('#patient_area').val();
        }
        if ($('#is_reg_free')) {
            if ($('#is_reg_free').is(':checked')) {
                is_free_visit = true;
            }
        }

        var status = $('input[name=patient_status]:checked').val();
        var appointment_id = $('#appointment_id').val();
        var string = "Are you sure you want to take Registration ?";
        if (status == '2') {
            string = "Are you sure you want to take Renewal ?";
        }
        var url = base_url + "/patient_register/createPatientRegistration";
        var patient_id = $('#patient_id_hidden').val();
        var doctor_charges = $('#doctor_charges_hidden').html();
        var visit_date = $('#patient_visit_date').val();
        var start_time = $('#slot_start_time').val();
        var doctor = $('#patient_doctor_hidden').val();
        var uhid = $('#patient_uhid').val();
        var patient_name = $('#patient_name').val();
        var patient_dob = $('#patient_dob').val();
        var patient_age = $('#age_years').val();
        var patient_gender = $('#patient_gender').val();
        var patient_phone = $('#patient_phone').val();
        var alternate_mobile_no = $('#alternate_phone').val();
        var patient_email = $('#patient_email').val();
        var patient_country = $('#patient_country').val();
        var patient_state = $('#patient_state').val();
        var patient_district = $('#patient_district').val();

        var patient_pincode = $('#patient_pincode').val();
        var patient_remarks = $('#patient_remarks').val();
        var pricing_id = $('#pricing_id_hidden').val();
        var commpany_id = $('#commpany_id_hidden').val();
        var patient_address = $('#patient_address').val();
        var patient_salutation = $('#patientsalutation').val();
        var patient_token = $('#patient_token').val();
        var consulting_date_from = $('#consulting_date_from').val();
        var session_id = $('#session_id').val();
        var booked_status = $('#booked_status').val();
        var patient_vulnerable = $('#patient_vulnerable').val();
        var split_amt = $('#split_hidden').val();
        var discount_type = $('#discount_type').val();
        var discount_narration = $('#discount_narration').val();
        var discount = $('.discount_value').val();
        var image_url=$('#pt_profile_url').val();

        var is_video_consultation = 0;
        if ($('#is_video_consultation').is(":checked")) {
            is_video_consultation = 1;
        }

        var flag = 1;
        var alternatePhoneFlag = true;
        if(alternate_mobile_no != ''){
            alternatePhoneFlag = checkAlternateMobileNumber();
        }
        var branch_id = localStorage.getItem('branch_id') ? parseInt(localStorage.getItem('branch_id')) : 0;
        // var phone_flag = checkMobileNo();
        // var phone_flag = isValidIndianMobileNumber();
        var phone_flag = mobileNumberNormalValidation();

        var patient_dobnotknown = false;
        if (status == '2') {
            if (!patient_id) {
                toastr.warning('Please select any Patient');
                $('#patient_uhid').focus();
                return false;
            }
        }
        if (!doctor || doctor == '') {
            toastr.warning('Please Select Doctor');
            var string = "<div class='col-md-12 padding_sm text-center red' style='margin-top: 150px'><h3> Please Select Doctor </h3> </div>";
            $('#doctor_appointmentslot_div').html(string);
            flag = 0;
            return false;
        } else if (!start_time) {
            toastr.warning('Please select any Slot');
            flag = 0;
            return false;
        } else if (!visit_date) {
            toastr.warning('Please select any Booking Date');
            flag = 0;
            return false;
        } else if (!patient_salutation || patient_salutation == '') {
            toastr.warning('Please select any Salutation');
            flag = 0;
            return false;
        } else if (!patient_name) {
            toastr.warning('Please select any Patient Name');
            $('#patient_name').focus();
            flag = 0;
            return false;
        } else if (!patient_dob) {
            toastr.warning('Please Enter Patient DOB');
            $('#patient_dob').focus();
            flag = 0;
            return false;
        } else if (!phone_flag) {
            toastr.warning('Please enter valid mobile number');
            $('#patient_phone').focus();
            flag = 0;
            return false;
        } else if (!patient_gender || patient_gender == '') {
            toastr.warning('Please select any Gender');
            flag = 0;
            return false;
        } else if (!alternatePhoneFlag) {
            $('#alternate_phone').focus();
            flag = 0;
            return false;
        }
        else if (!pricing_id) {
            toastr.warning('Please select any Company');
            flag = 0;
            return false;
        } else if (!commpany_id) {
            toastr.warning('Please select any Pricing');
            flag = 0;
            return false;
        } else if (!doctor_charges) {
            toastr.warning('Doctor Charges Cannot be Empty');
            flag = 0;
            return false;
        } else if (patient_email) {
            var email_check = isEmail();
            if (!email_check) {
                flag = 0;
                return false;
            }
        }
        if (ValidatBeforSaveorAddd()) {
            stringifiedInsuranceDetails()
            var insurance_array_string = JSON.stringify(insurance_details);
        }
        var patient_more_details_string = JSON.stringify(patient_more_details);
        var pricing_details_string = JSON.stringify(pricing_details);
        var doctor_name = $("#patient_doctor").text();

        var param = {
            _token: token,
            patient_id: patient_id,
            is_free_visit: is_free_visit,
            visit_date: visit_date,
            start_time: start_time,
            patient_age: patient_age,
            patient_country: patient_country,
            patient_district: patient_district,
            patient_state: patient_state,
            patient_area: patient_area,
            patient_pincode: patient_pincode,
            patient_remarks: patient_remarks,
            pricing_id: pricing_id,
            commpany_id: commpany_id,
            doctor: doctor,
            uhid: uhid,
            patient_name: patient_name,
            patient_dob: patient_dob,
            branch_id: branch_id,
            patient_dobnotknown: patient_dobnotknown,
            patient_gender: patient_gender,
            patient_phone: patient_phone,
            alternate_mobile_no:alternate_mobile_no,
            patient_email: patient_email,
            patient_address: patient_address,
            patient_salutation: patient_salutation,
            patient_token: patient_token,
            consulting_date_from: consulting_date_from,
            doctor_charges: doctor_charges,
            session_id: session_id,
            booked_status: booked_status,
            appointment_id: appointment_id,
            patient_more_details_string: patient_more_details_string,
            pricing_details_string: pricing_details_string,
            insurance_array_string: insurance_array_string,
            patient_vulnerable: patient_vulnerable,
            location_code: location_code,
            split_amt: split_amt,
            discount_type:discount_type,
            is_video_consultation: is_video_consultation,
            discount_narration:discount_narration,
            discount:discount,
            image_url:image_url
        };
        if (flag == 1) {
            bootbox.confirm({
                message: string,
                buttons: {
                    confirm: {
                        label: 'Save',
                        className: 'btn-success',
                        default: "true",
                    },
                    cancel: {
                        label: "Cancel",
                        className: "btn-danger",
                    },
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: param,
                            beforeSend: function () {
                                // $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                                // $("#doctor_appointmentslot_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                                $('#createPatientRegistraionBtn').attr('disabled', true);
                                $('#createPatientRegistraionSpin').removeClass('fa fa-save');
                                $('#createPatientRegistraionSpin').addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                var obj = JSON.parse(data);
                                console.log(obj);
                                window.op_pres = obj.print_data;
                                if (parseInt(obj.result) == 1) {
                                    resetAppointmentForm(1, 2, 0);
                                    var string = "Registration";
                                    if (parseInt(status) == 2) {
                                        string = "Renewal";
                                    }
                                    window.appointment_details = {
                                        "uhid": obj.uhid,
                                        "patient_name": patient_name,
                                        "patient_token": patient_token,
                                        "visit_date": visit_date,
                                        "doctor_name": doctor_name,
                                        "string": string,
                                        "bill_head_id": obj.bill_head_id,
                                        "start_time": start_time,
                                        "patient_pay": obj.patient_pay

                                    };
                                    if (parseInt(obj.patient_pay) == 1) {
                                        $("#bill_id").val(obj.bill_head_id);
                                        bill_id_array = [];
                                        var bill_id = $("#bill_id").val();
                                        bill_id_array.push(bill_id);
                                        if($('#op_credit_status').val()!=1){
                                            loadCashCollection(bill_id_array, 0, 0, 1);
                                        }
                                    } else {
                                        paymentCompleted();
                                    }
                                    $("#is_video_consultation").prop("checked", false);

                                } else {
                                    if (parseInt(obj.result) == 3) {
                                        toastr.warning(obj.message);

                                    } else {
                                        toastr.error("Error Please Check Your Internet Connection");
                                    }
                                }
                                $('#progressbar').html('');
                                $('#details_tab_body').html('');
                                insurance_array = new Array();
                                pricing_details = new Array();
                                patient_more_details = new Array();
                                insurance_count = new Array();
                                insurance_details = new Array();
                                $('#pt_profile_url').val('');
                                $('.pt_profile_body').html('');

                            },
                            complete: function () {
                                // $("#patientappoinment_div").LoadingOverlay("hide");
                                // $("#doctor_appointmentslot_div").LoadingOverlay("hide");
                                $('#createPatientRegistraionBtn').attr('disabled', false);
                                $('#createPatientRegistraionSpin').removeClass('fa fa-spinner fa-spin');
                                $('#createPatientRegistraionSpin').addClass('fa fa-save');
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });
                    }
                }
            });
        }
    } else {
        toastr.warning("Please select any location");
    }
}

function paymentCompleted() {
    if (window.appointment_details) {
        obj = window.appointment_details;
    }
    if ($('#printOpPrescription').is(':checked')) {
        var winPrint = window.open('', '', 'left=0,top=0,width=2480,height=3508,toolbar=0,scrollbars=0,status=0');
        winPrint.document.write('<style>@page{size:A4 portrait;}  </style>' + window.op_pres + '<script>setTimeout(function(){window.print();window.close();},1000)</script>');

    }
}

function getPatientInsurance() {

    $("#patientSetInsuranceModel").modal({
        backdrop: 'static',
        keyboard: false
    });
    var patient_hidden_id = $('#patient_id_hidden').val();
    if (patient_hidden_id && patient_hidden_id != 0 && $('#progressbar li').length == 0) {
        fetchExistingInsuranceDetails(patient_hidden_id);
    } else {
        if ($('#progressbar li').length == 0) {
            addnewInsuranceDiv();
        }
    }


}

function patientAreaStatus() {
    if ($('#add_area').is(':checked')) {
        $('#area_text').show();
        $('#area_select').hide();
    } else {
        $('#area_text').hide();
        $('#area_select').show();
    }
}
function addnewInsuranceDiv() {

    if (ValidatBeforSaveorAddd() || $('#progressbar li').length == 0) {
        var url = base_url + "/patient_register/addnewInsuranceDiv";
        var insurance_div_count = $('#insurance_div_count').val();
        var param = { insurance_div_count: insurance_div_count };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#add_insurance_btn').attr('disabled', true);
                $('#add_insurance_spin').removeClass('fa fa-plus');
                $('#add_insurance_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                $('#progressbar').append(data.created_li);
                $('#details_tab_body').append(data.created_tabs);
                $(document).on('click', '#progressbar li', function (e) {
                    $(this).find('a').tab('show');
                });
                $("#progressbar li:last-child a").tab("show");
                $('#insurance_div_count').val(parseInt(insurance_div_count) + 1);
                insurance_count.push(parseInt(insurance_div_count));

            },
            complete: function () {
                $('#add_insurance_btn').attr('disabled', false);
                $('#add_insurance_spin').removeClass('fa fa-spinner fa-spin');
                $('#add_insurance_spin').addClass('fa fa-plus');
                setTimeout(function () {
                    $('.insurance_exp_datepicker').datetimepicker({
                        format: 'MMM-DD-YYYY',
                        minDate: new Date()
                    });
                    $('.insurance_issue_datepicker').datetimepicker({
                        format: 'MMM-DD-YYYY',
                        maxDate: new Date()
                    });
                }, 400);
                $(document).on('keydown', '.principal_search', function (event) {
                    var input_id = '';
                    input_id = $(this).attr('id');
                    if (event.keyCode === 13) {
                        ajaxlistenter(input_id + 'AjaxDiv');
                        return false;
                    }
                });
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning('Incomplete Details.');
    }

}
$(document).on('change', '.insurance_type', function () {
    insurance_type(this);
})
function insurance_type(e) {
    var insurance_type = $(e).data('type_id');
    var insurance_name = $(e).find(':selected').text();
    $('#' + insurance_type + '-tab').html(insurance_name);
}
function stringifiedInsuranceDetails() {
    var flag = true;

    insurance_count.forEach(function (count) {
        insurance_details.push({
            'insurance_company': $('#insurance_company' + count).val(),
            'insurance_pricing': $('#insurance_pricing' + count).val(),
            'primary_authorisation_letter_no': $('#primary_authorisation_letter_no' + count).val(),
            'primary_insurance_id_no': $('#primary_insurance_id_no' + count).val(),
            'primary_credit_limit': $('#primary_credit_limit' + count).val(),
            'primary_card_type': $('#primary_card_type' + count).val(),
            'primary_color_of_card': $('#primary_color_of_card' + count).val(),
            'primary_expiry_date': $('#primary_expiry_date' + count).val(),
            'primary_issue_date': $('#primary_issue_date' + count).val(),
            'insurance_type': $('#insurance_type' + count).val(),
            'principle_member_uhid': $('#principle_member_uhid' + count + '_hidden').val(),
            'principle_member_pr': $('#principle_member_uhid' + count + '_name').val(),
            'primary_dependent_relation': $('#primary_dependent_relation' + count).val(),
            'detail_id': $('#ins_detail_id' + count).val(),
            'insurance_status': $('#insurance_status' + count).is(':checked') ? 1 : 0,
        })

    })





}
function saveInsurance() {
    if (ValidatBeforSaveorAddd()) {
        toastr.success("Successfully Added");
        insurance_count.forEach(function (count) {

            if ($('#insurance_type' + count).val() == 1) {
                var insurance_company = $('#insurance_company' + count).val();
                var insurance_pricing = $('#insurance_pricing' + count).val();
                var insurance_company_name = $('#insurance_company' + count).find(":selected").text();
                var insurance_pricing_name = $('#insurance_pricing' + count).find(":selected").text();
                $('#company_pricing').val(insurance_company_name);
                $('#pricing_company').val(insurance_pricing_name);
                $('#commpany_id_hidden').val(insurance_company);
                $('#pricing_id_hidden').val(insurance_pricing);
                //  $('#company_type_hidden').val(insurance_pricing_name);
            }


        })
        $('#patientSetInsuranceModel').modal('hide');
    } else {
        toastr.warning("Incomplete Details.");

    }

}
// function savePricing(){
//         toastr.success("Successfully Added");
//         $('#patientInsuranceModel').modal('hide');
// }
function ValidatBeforSaveorAddd() {
    var flag = true;
    insurance_count.forEach(function (count) {
        if ($('#insurance_company' + count).val() != '' && $('#insurance_pricing' + count).val() != '' &&
            $('#primary_expiry_date' + count).val() != '' && $('#insurance_type' + count).val() != '') {
            flag = true;
        } else {
            flag = false;
            return false;
        }
    })

    return flag;
}
function removeThisDViv(tab, val) {
    if ($('#progressbar li').length != 1) {
        $('#' + tab).remove();
        $('#' + val + '_tab').remove();
        var count_data = $('#insurance_div_count').val();
        $('#insurance_div_count').val(parseInt(count_data) - 1);
        $("#progressbar li:last-child a").tab("show");
        var index = insurance_count.indexOf(val);
        if (index > -1) {
            insurance_count.splice(index, 1);
        }
    }


}
function fetchExistingInsuranceDetails(patient_id = 0) {
    var url = base_url + "/patient_register/fetchExistingInsuranceDetails";
    var param = { patient_id: patient_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            console.log(data.count);
            $('.add_insurance_detail_div').html(data.html);
            $('#insurance_div_count').val(parseInt(data.count) + 1);
            var inc_arr = data.insurance_count;
            inc_arr.forEach(function (val) {
                insurance_count.push(
                    (val));
            })
            if (data.count == 0) {
                addnewInsuranceDiv();
            }
            $('.insurance_exp_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
                minDate: new Date()
            });
            $('.insurance_issue_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
                maxDate: new Date()
            });
            $(document).on('keydown', '.principal_search', function (event) {
                var input_id = '';
                input_id = $(this).attr('id');
                if (event.keyCode === 13) {
                    ajaxlistenter(input_id + 'AjaxDiv');
                    return false;
                }
            });
        },
        complete: function () {
            $("#patientappoinment_div").LoadingOverlay("hide");
            $(document).on('click', '#progressbar li', function (e) {
                $(this).find('a').tab('show');
            });
            $("#progressbar li:last-child a").tab("show");

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}
//----Hidden Filed Search--------------------------------------------------------------------

$(document).on('keyup', '.hidden_search', function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + '/patient_register/ajaxsearch_speciality_doctor_inReg';
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});
$(document).on('keyup', '.principal_search', function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = $('#base_url').val() + '/patient_register/ajaxUhidSearch';
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetialsIns(id, uhid, patient_name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(uhid);
    $('#' + serach_key_id + '_name').val(patient_name);
    $('#' + serach_key_id).attr('title', uhid);
    $("#" + serach_key_id + "AjaxDiv").hide();
}


/* setting for enter key press in ajaxDiv listing */

function checkProWantOrNot() {

    var doc_charge_hidden = $('#charges_hidden').val();
    if ($('#doctor_charges_hidden').html().trim() != '' && $('#doctor_charges_hidden').html().trim() != 0.00) {

        if ($('#patient_name').val().trim() != '') {
            if ($('#dis_prov').is(':checked')) {
                $('#discount_pro').fadeIn();
                $('#dis_amt').fadeIn();
            } else {
                $('#discount_pro').fadeOut();
                $('#dis_amt').fadeOut();
                $('#doctor_charges_hidden').html(doc_charge_hidden);
                $('#split_hidden').val('0.00');
                $('.discount_value').val('');
                $('#discount_type').val('');
                $('#discount_narration').val('');
            }
        } else {
            $('#dis_prov').prop('checked', false);
            toastr.warning('Please select a patient.');
        }

    } else {
        $('#dis_prov').prop('checked', false);
        toastr.warning('No charge to discount.');
    }

}

$(document).on('keyup', '.discount_value', function () {
    setDiscount();
})
$(document).on('change', '#discount_type', function () {
    setDiscount();
})

function setDiscount() {
    var dis_type = $('#discount_type').val();
    var dis_val = $('.discount_value').val().trim();
    var doc_charge = $('#doctor_charges_hidden').html().trim();
    var doc_charge_hidden = $('#charges_hidden').val();
    var final_amt = 0;
    var split_amt = 0;
    if (dis_type) {
        if (dis_val) {
            if (dis_type == 1) {
                console.log(dis_val);
                if (parseFloat(dis_val) <= 100) {
                    split_amt = parseFloat((dis_val / 100) * doc_charge_hidden).toFixed(2);
                    final_amt = parseFloat(doc_charge_hidden - (dis_val / 100) * doc_charge_hidden).toFixed(2);
                    $('#doctor_charges_hidden').html(final_amt);
                    $('#split_hidden').val(split_amt);
                } else {
                    toastr.warning('Percentage must be less than 100.');
                    $('.discount_value').val('0.00');
                    $('#split_hidden').val('0.00');
                    $('#doctor_charges_hidden').html(doc_charge_hidden);
                }


            } else if (dis_type == 2) {
                if (parseFloat(dis_val) <= parseFloat(doc_charge_hidden)) {
                    split_amt = dis_val;
                    final_amt = parseFloat(doc_charge_hidden - dis_val).toFixed(2);
                    $('#doctor_charges_hidden').html(final_amt);
                    $('#split_hidden').val(split_amt);
                } else {
                    toastr.warning('Discount amount must be less than total charge.');
                    $('.discount_value').val('');
                    $('#split_hidden').val('0.00');
                    $('#doctor_charges_hidden').html(doc_charge_hidden);
                }

            }
        } else {
            $('#doctor_charges_hidden').html(doc_charge_hidden);
            $('#split_hidden').val('0.00');
            $('.discount_value').val('');
        }

    } else {
        toastr.warning('Please select discount type');
        $('#doctor_charges_hidden').val('');
        $('#doctor_charges_hidden').html(doc_charge_hidden);

    }

}
function userProfile() {
    var url = $('#base_url').val() + "/patient_register/getUserData";
    var patient_id = $('#patient_id_hidden').val();
    var patient_name = $('#patient_name').val();
    var status = $('input[name=patient_status]:checked').val();
    if (patient_id || patient_name) {
        if (patient_id) {
            var params = { patient_id: patient_id };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function () {
                    $('#image_pt_id').val(patient_id);
                    $('#userProfileBtn').attr('disabled', true);
                    $('#userProfileSpin').removeClass('fa fa-user');
                    $('#userProfileSpin').addClass('fa fa-spinner fa-spin');
                    var patient_name = $('#patient_name').val();
                    $('#pt_name').html(patient_name.toUpperCase());
                },
                success: function (data) {
                    $("#pt_profile").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    var patient_name = $('#base_url').val() + '/packages/extensionsvalley/patient_profile/' + data;
                    $('.pt_profile_body').html("<img src='" + patient_name + "' alt='patient Profile' width='300' height='300' style='margin:-7px;'>");
                },
                complete: function () {
                    $('#userProfileBtn').attr('disabled', false);
                    $('#userProfileSpin').removeClass('fa fa-spinner fa-spin');
                    $('#userProfileSpin').addClass('fa fa-user');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });

        } else {

            var url = $('#base_url').val() + '/packages/extensionsvalley/patient_profile/default_img.png';
            var img_url = url.replace("public/", "");
            $('#pt_name').html(patient_name);
            $("#pt_profile").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.pt_profile_body').html("<img src='" + img_url + "' alt='patient Profile' width='300' height='300' style='margin:-7px;'>");

        }

    }else{
        if(status == '1'){

            toastr.warning('Please enter patient name.');
        } else {
            toastr.warning('Please select any patient.');
        }
    }

}
$("#submitpatient_image").on("submit", function (e) {
    e.preventDefault();
    var image_vai = validateDoc("pt_image");
    if (image_vai) {
        var url = base_url + "/patient_register/uploadPatientImage";
        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $("#submitPtImageBtn").attr("disabled", true);
                $("#submitPtImageSpin").removeClass("fa fa-save");
                $("#submitPtImageSpin").addClass("fa fa-spinner fa-spin");
            },
            success: function (data) {
                if (parseInt(data.status) == 100) {
                    toastr.success("Success.");
                    document.getElementById('submitpatient_image').reset();
                    $("#pt_profile").modal('toggle');
                    $("#pt_profile_url").val(data.filename);
                } else if (parseInt(data.status) == 102) {
                    bootbox.alert(
                        "<strong>File criteria.</strong><br>Extensions: jpg,jpeg,png,gif <br>Size : Maximum 5 MB"
                    );
                }
            },
            complete: function () {
                $("#submitPtImageBtn").attr("disabled", false);
                $("#submitPtImageSpin").removeClass("fa fa-spinner fa-spin");
                $("#submitPtImageSpin").addClass("fa fa-save");
            },
            error: function () {
                toastr.error(
                    "Error Please Check Your Internet Connection and Try Again"
                );
            },
        });
    } else {
        bootbox.alert(
            "<strong>File criteria.</strong><br>Extensions: jpg,jpeg,png,gif <br>Size : Maximum 5 MB"
        );
    }
});


function validateDoc(input_id) {
    var fileInput = document.getElementById(input_id);
    if (fileInput) {
        var filePath = fileInput.value;
        if (filePath) {
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;

            var file_size = fileInput.size;
            if (!allowedExtensions.exec(filePath) || file_size > 5150) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function checkAlternateMobileNumber(element){

    var pattern = /^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}$/;
    var mobileNumber = $('#alternate_phone').val()? $('#alternate_phone').val() : '';
    var length = mobileNumber.length;
    var valid;
    if (length > 10){
        valid = pattern.test(mobileNumber);
    } else if(length < 10){
        valid = false;
    } else if(length  == 10){
        valid = true;
    }
    if (!valid) {
        toastr.warning('Please enter valid mobile number');
    }

    return valid;
}


function previousHistory() {
    var url =$('#base_url').val()+"/patient_register/getpatientDetails";
    var patient_id = $('#patient_id_hidden').val();
    if(patient_id){
            var params = { patient_id: patient_id };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function () {
                    $('#userPreviousHistoryBtn').attr('disabled', true);
                    $('#userPreviousHistorySpin').removeClass('fa fa-info');
                    $('#userPreviousHistorySpin').addClass('fa fa-spinner fa-spin');
                    var patient_name = $('#patient_name').val();
                    $('#pt_name_details').html(patient_name.toUpperCase()+' - Previous Visits');
                },
                success: function (data) {
                    $("#pt_details").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#set_previous_history').html(data);



                    var $table = $('table.theadfix_wrapper');

                    $table.floatThead({
                        scrollContainer: function ($table) {
                            return $table.closest('.theadscroll');
                        }

                    });

                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });


                    $('.fixed_header').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });


                    setTimeout(function(){
                        $(".theadscroll").perfectScrollbar('update');
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 250 )


                },
                complete: function () {


                    $('#userPreviousHistoryBtn').attr('disabled', false);
                    $('#userPreviousHistorySpin').removeClass('fa fa-spinner fa-spin');
                    $('#userPreviousHistorySpin').addClass('fa fa-info');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });

    }else{

            toastr.warning('Please select any patient.');
    }

}


