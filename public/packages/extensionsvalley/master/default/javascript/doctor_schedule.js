$(document).ready(function () {
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30

    });
    setTimeout(function () {
        $('.theadfix_wrapper').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
    }, 400);
    getDoctorList();
});

var base_url = $('#base_url').val();
var token = $('#token').val();


function getDoctorList() {
    var url = base_url + "/doctor_schedule/getDoctorList";
    var doctor_id = $('#doctor_id').val();
    var speciality_id = $('#speciality_id').val();
    var param = { _token: token, speciality_id: speciality_id, doctor_id: doctor_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getDoctrorListBtn').attr('disabled', true);
            $('#getDoctrorListSpin').removeClass('fa fa-search');
            $('#getDoctrorListSpin').addClass('fa fa-spinner fa-spin');
            $("#getDoctrorListData").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#getDoctrorListData').html(data);
        },
        complete: function () {
            $('#getDoctrorListBtn').attr('disabled', false);
            $('#getDoctrorListSpin').removeClass('fa fa-spinner fa-spin');
            $('#getDoctrorListSpin').addClass('fa fa-search');
            $("#getDoctrorListData").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function addNewSpeciality() {
    var url = base_url + "/doctor_schedule/addNewDoctorSpeciality";
    var speciality = $('#speciality_name_add').val();
    var param = { _token: token, speciality: speciality };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#addNewDoctorSpecialitybtn').attr('disabled', true);
            $('#addNewDoctorSpecialitySpin').removeClass('fa fa-plus');
            $('#addNewDoctorSpecialitySpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                fillSearchDetalis(data, speciality, 'speciality_name_add');
            }
        },
        complete: function () {
            $('#addNewDoctorSpecialitybtn').attr('disabled', false);
            $('#addNewDoctorSpecialitySpin').removeClass('fa fa-spinner fa-spin');
            $('#addNewDoctorSpecialitySpin').addClass('fa fa-plus');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function checkDoctorUserName() {
    var url = base_url + "/doctor_schedule/checkDoctorUserName";
    var user_name = $('#add_doctor_user_name').val();
    var doctor_id = $('#doctor_list_id').val();
    if (user_name) {
        var param = { _token: token, user_name: user_name, doctor_id: doctor_id };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveSalaryMasterBtn').attr('disabled', true);
                $('#saveSalaryMasterSpin').removeClass('fa fa-save');
                $('#saveSalaryMasterSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (parseInt(data) != 0) {
                    toastr.warning("Username already used.Please try another");
                    $('#add_doctor_user_name').val('');
                    $('#add_doctor_user_name').focus();
                } else {
                    toastr.success("Valid username");
                }
            },
            complete: function () {
                $('#saveSalaryMasterBtn').attr('disabled', false);
                $('#saveSalaryMasterSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveSalaryMasterSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }

}

function updateAllRow(section_id, list_type, class_name_id) {
    var list_val = $('#' + list_type + '_' + section_id + '_' + class_name_id).val();
    for ($i = class_name_id; $i < 7; $i++) {
        $('#' + list_type + '_' + section_id + '_' + $i).val(list_val);
    }
}
var doctor_schedule = {};

function validateDoctorSchedule() {
    doctor_schedule = {};
    var flag = true;
    var from_morning_time = '';
    var to_morning_time = '';
    var from_evening_time = '';
    var to_evening_time = '';
    var i = 0;
    $('.doctor_schedule').css("background", "#fff");
    $(".doctor_schedule").each(function (index, value) {
        var week_name = $(this).attr('attr-name');
        var from_type = $(this).attr('attr-id');
        var increment_id = $(this).attr('attr-increment');
        var list_val = this.value;
        if (from_type == 'from_morning') {
            from_morning_time = list_val;
            to_morning_time = list_val;
        }
        if (from_type == 'to_morning') {
            to_morning_time = list_val;
        }

        if (from_type == 'from_evening') {
            from_evening_time = list_val;
            to_evening_time = list_val;
        }
        if (from_type == 'to_evening') {
            to_evening_time = list_val;
        }


        if (from_morning_time && !to_morning_time) {
            $('#to_morning_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter morning to time");
            flag = false;
            return false;
        } else if (!from_morning_time && to_morning_time) {
            $('#from_morning_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter morning from time");
            flag = false;
            return false;
        } else if (from_morning_time > to_morning_time) {
            $('#from_morning_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("start morning time is greater than end time");
            flag = false;
            return false;
        } else if (from_evening_time && !to_evening_time) {
            $('#to_evening_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter evening to time");
            flag = false;
            return false;
        } else if (!from_evening_time && to_evening_time) {
            $('#from_evening_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter evening from time");
            flag = false;
            return false;
        } else if (from_evening_time > to_evening_time) {
            $('#from_evening_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("start evening time is greater than end time");
            flag = false;
            return false;
        }
        i++;
        if (flag) {
            doctor_schedule[i] = {
                week_name: week_name,
                from_type: from_type,
                list_val: list_val,
            }
        }
    });
    return flag;
}

function isEmail() {
    var flag = true;
    var email = $('#add_doctor_email').val();
    if (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        flag = regex.test(email);
    }
    if (!flag) {
        toastr.warning('Please enter valid email');
        $('#add_doctor_email').focus();
    }
    return flag
}


function editDoctorMaster(doctor_id) {
    var url = base_url + "/doctor_schedule/editDoctorMaster";
    var param = { _token: token, doctor_id: doctor_id };
    $('#doctor_list_id').val(doctor_id);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('.editDoctorMasterRow').removeClass('bg-green');
            $("#getDoctrorListData").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var doctor_detalis = obj.doctor_master;
            var schedule_master = obj.schedule_master;
            var consultation_amt = obj.consultation_amt;
            var renewal_amt = obj.renewal_amt;
            var doctor_username = obj.doctor_username;
            $('#add_doctor_name').val(doctor_detalis[0].doctor_name ? doctor_detalis[0].doctor_name : '');
            $('#speciality_name_add').val(doctor_detalis[0].speciality_name ? doctor_detalis[0].speciality_name : '');
            $('#speciality_id_add').val(doctor_detalis[0].speciality ? doctor_detalis[0].speciality : '');
            $('#add_doctor_email').val(doctor_detalis[0].email ? doctor_detalis[0].email : '');
            $('#add_doctor_phone').val(doctor_detalis[0].phone ? doctor_detalis[0].phone : '');
            $('#add_doctor_qualification').val(doctor_detalis[0].qualification ? doctor_detalis[0].qualification : '');
            $('#renewal_days').val(doctor_detalis[0].free_followup_days ? doctor_detalis[0].free_followup_days : '');
            $('#renewal_times').val(doctor_detalis[0].free_followup_no ? doctor_detalis[0].free_followup_no : '');
            $('#op_consultation').val(consultation_amt ? consultation_amt : '');
            $('#op_renewal_consultation').val(renewal_amt ? renewal_amt : '');
            $('#add_doctor_user_name').val(doctor_username ? doctor_username : '');
            $('#add_doctor_user_name').prop("readonly", true);

            for (i = 0; i < schedule_master.length; i++) {
                if (parseInt(schedule_master[i].repeats_on_sunday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_0').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_0').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_0').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_0').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_0').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_0').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_0').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_0').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }
                } else if (parseInt(schedule_master[i].repeats_on_monday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_1').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_1').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_1').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_1').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_1').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_1').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_1').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_1').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }

                } else if (parseInt(schedule_master[i].repeats_on_tuesday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_2').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_2').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_2').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_2').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_2').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_2').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_2').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_2').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }

                } else if (parseInt(schedule_master[i].repeats_on_wednesday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_3').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_3').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_3').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_3').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_3').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_3').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_3').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_3').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }
                } else if (parseInt(schedule_master[i].repeats_on_thursday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_4').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_4').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_4').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_4').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_4').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_4').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_4').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_4').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }
                } else if (parseInt(schedule_master[i].repeats_on_friday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_5').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_5').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_5').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_5').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_5').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_5').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_5').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_5').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }
                } else if (parseInt(schedule_master[i].repeats_on_saturday) == 1) {
                    if (schedule_master[i].schedule_description == 'Morning_Shift') {
                        $('#from_morning_6').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_morning_6').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_morning_6').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_morning_6').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    } else if (schedule_master[i].schedule_description == 'Evening_Shift') {
                        $('#from_evening_6').val(schedule_master[i].from_time ? schedule_master[i].from_time : '');
                        $('#to_evening_6').val(schedule_master[i].to_time ? schedule_master[i].to_time : '');
                        $('#walkin_evening_6').val(schedule_master[i].walkin_count ? schedule_master[i].walkin_count : '');
                        $('#dur_evening_6').val(schedule_master[i].slot_duration ? schedule_master[i].slot_duration : '');
                    }
                }
            }

        },
        complete: function () {
            $('#editDoctorMasterRow' + doctor_id).addClass('bg-green');
            $("#getDoctrorListData").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}



function saveSalaryMaster() {
    var url = base_url + "/doctor_schedule/saveSalaryMaster";
    var doctor_name = $('#add_doctor_name').val();
    var speciality_id = $('#speciality_id_add').val();
    var doctor_email = $('#add_doctor_email').val();
    var doctor_phone = $('#add_doctor_phone').val();
    var doctor_qualification = $('#add_doctor_qualification').val();
    var op_consultation = $('#op_consultation').val();
    var renewal_days = $('#renewal_days').val();
    var renewal_times = $('#renewal_times').val();
    var doctor_id = $('#doctor_list_id').val();
    var user_name = $('#add_doctor_user_name').val();
    var password = $('#add_doctor_password').val();
    var renewal_charge = $('#op_renewal_consultation').val();
    var flag = true;
    if (doctor_name) {
        if (speciality_id) {
            if (doctor_email) {
                flag = isEmail();
            }
            if (flag) {
                flag = validateDoctorSchedule();
                if (flag) {
                    if (op_consultation) {
                        if (renewal_days) {
                            if (renewal_times) {
                                if (user_name) {
                                    if (password || doctor_id) {
                                        bootbox.confirm({
                                            message: "Are you sure you want to create doctor schedule ?",
                                            buttons: {
                                                confirm: {
                                                    label: "Yes",
                                                    className: "btn-success",
                                                    default: "true",
                                                },
                                                cancel: {
                                                    label: "No",
                                                    className: "btn-warning",
                                                },
                                            },
                                            callback: function (result) {
                                                if (result) {
                                                    var doctor_schedule_string = JSON.stringify(doctor_schedule);
                                                    var param = {
                                                        _token: token,
                                                        doctor_id: doctor_id,
                                                        doctor_name: doctor_name,
                                                        speciality_id: speciality_id,
                                                        doctor_email: doctor_email,
                                                        doctor_phone: doctor_phone,
                                                        user_name: user_name,
                                                        password: password,
                                                        doctor_qualification: doctor_qualification,
                                                        doctor_schedule_string: doctor_schedule_string,
                                                        op_consultation: op_consultation,
                                                        renewal_days: renewal_days,
                                                        renewal_times: renewal_times,
                                                        renewal_charge: renewal_charge
                                                    };
                                                    $.ajax({
                                                        type: "POST",
                                                        url: url,
                                                        data: param,
                                                        beforeSend: function () {
                                                            $('#saveSalaryMasterBtn').attr('disabled', true);
                                                            $('#saveSalaryMasterSpin').removeClass('fa fa-search');
                                                            $('#saveSalaryMasterSpin').addClass('fa fa-spinner fa-spin');
                                                        },
                                                        success: function (data) {
                                                            if (parseInt(data) == 1) {
                                                                toastr.success("Doctor master successfully updated");
                                                                resetSalaryMaster();
                                                            } else {
                                                                toastr.error("Error Please Check Your Internet Connection");
                                                            }
                                                        },
                                                        complete: function () {
                                                            $('#saveSalaryMasterBtn').attr('disabled', false);
                                                            $('#saveSalaryMasterSpin').removeClass('fa fa-spinner fa-spin');
                                                            $('#saveSalaryMasterSpin').addClass('fa fa-search');
                                                        },
                                                        error: function () {
                                                            toastr.error("Error Please Check Your Internet Connection");
                                                        }
                                                    });
                                                }
                                            },
                                        });
                                    } else {
                                        toastr.warning("Please enter password");
                                        $('#add_doctor_password').focus();
                                    }
                                } else {
                                    toastr.warning("Please enter user name");
                                    $('#add_doctor_user_name').focus();
                                }

                            } else {
                                toastr.warning("Please enter renewal times");
                                $('#renewal_times').focus();
                            }
                        } else {
                            toastr.warning("Please enter renewal days");
                            $('#renewal_days').focus();
                        }
                    } else {
                        toastr.warning("Please enter OP consultation");
                        $('#op_consultation').focus();
                    }
                }
            }
        } else {
            toastr.warning("Please enter speciality");
            $('#speciality_id_add').focus();
        }
    } else {
        toastr.warning("Please enter doctor name");
        $('#add_doctor_name').focus();
    }
}

function resetForm() {
    $('#doctor_name').val('');
    $('#doctor_id').val('');
    $('#speciality_name').val('');
    $('#speciality_id').val('');
    getDoctorList();
}

function resetSalaryMaster() {
    $('#add_doctor_name').val('');
    $('#speciality_name_add').val('');
    $('#speciality_id_add').val('');
    $('#add_doctor_email').val('');
    $('#add_doctor_phone').val('');
    $('#add_doctor_qualification').val('');
    $('#op_consultation').val('');
    $('#renewal_days').val('');
    $('#renewal_times').val('');
    $('.doctor_schedule').val('');
    $('#doctor_list_id').val('');
    $('#add_doctor_user_name').val('');
    $('#add_doctor_user_name').prop("readonly", false);
    $('#add_doctor_password').val('');
    $('#op_renewal_consultation').val('');
    resetForm();
}

$('#doctor_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('doctor_name_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var doctor_name = $(this).val();
        if (!doctor_name) {
            $('#doctor_id').val('');
            $("#doctor_name_AjaxDiv").hide();
        } else {
            var url = base_url + "/doctor_schedule/doctorSearchCriteria";
            var param = { list_name: doctor_name, list_type: 'doctor_name' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#doctor_name_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#doctor_name_AjaxDiv").html(html).show();
                    $("#doctor_name_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('doctor_name_AjaxDiv', event);
    }
});


$('#speciality_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('speciality_name_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var speciality_name = $(this).val();
        if (!speciality_name) {
            $('#speciality_id').val('');
            $("#speciality_name_AjaxDiv").hide();
        } else {
            var url = base_url + "/doctor_schedule/doctorSearchCriteria";
            var param = { list_name: speciality_name, list_type: 'speciality_name' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#speciality_name_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#speciality_name_AjaxDiv").html(html).show();
                    $("#speciality_name_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('speciality_name_AjaxDiv', event);
    }
});


$('#speciality_name_add').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('speciality_name_add_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var speciality_name_add = $(this).val();
        if (!speciality_name_add) {
            $('#speciality_id_add').val('');
            $("#speciality_name_add_AjaxDiv").hide();
        } else {
            var url = base_url + "/doctor_schedule/doctorSearchCriteria";
            var param = { list_name: speciality_name_add, list_type: 'speciality_name_add' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#speciality_name_add_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#speciality_name_add_AjaxDiv").html(html).show();
                    $("#speciality_name_add_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('speciality_name_add_AjaxDiv', event);
    }
});


function fillSearchDetalis(list_id, list_name, from_type) {
    if (from_type == 'doctor_name') {
        $('#doctor_name').val(htmlDecode(list_name));
        $('#doctor_id').val(list_id);
        $("#doctor_name_AjaxDiv").hide();
    } else if (from_type == 'speciality_name') {
        $('#speciality_name').val(htmlDecode(list_name));
        $('#speciality_id').val(list_id);
        $("#speciality_name_AjaxDiv").hide();
    } else if (from_type == 'speciality_name_add') {
        $('#speciality_name_add').val(htmlDecode(list_name));
        $('#speciality_id_add').val(list_id);
        $("#speciality_name_add_AjaxDiv").hide();
    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
