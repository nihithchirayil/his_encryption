$(document).ready(function () {
    base_url = $('#base_url').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});
base_url = '';

function processTallyData() {
    var servieDate = $("#servieDate").val();
    if (servieDate) {
        var url = base_url + "/master/postIncomeDetailsData";
        var param = { servieDate: servieDate };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#postdatadiv').html('');
                $('#processdatabtn').attr('disabled', true);
                $('#processdataspin').removeClass('fa fa-list');
                $('#processdataspin').addClass('fa fa-spinner fa-spin');
                $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if (data != 1) {
                    $('#postdatadiv').html(data);
                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);

                    $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                } else {
                    toastr.warning('Already Posted');
                }
            },
            complete: function () {
                $('#processdatabtn').attr('disabled', false);
                $('#processdataspin').removeClass('fa fa-spinner fa-spin');
                $('#processdataspin').addClass('fa fa-list');
                $('#postdatadiv').LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Service Date");
        $("#servieDate").focus();
    }
}

function insertTallyData() {
    var servieDate = $("#servieDate").val();
    var url = base_url + "/master/insertIncomeDetails";
    var param = { servieDate: servieDate };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#nextsequencedatabtn').attr('disabled', true);
            $('#nextsequencedataspin').removeClass('fa fa-list');
            $('#nextsequencedataspin').addClass('fa fa-spinner fa-spin');
            $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            $('#postdatadiv').html(data);
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('#nextsequencedatabtn').attr('disabled', false);
            $('#nextsequencedataspin').removeClass('fa fa-spinner fa-spin');
            $('#nextsequencedataspin').addClass('fa fa-list');
            $('#postdatadiv').LoadingOverlay("hide");
            $('#generate_csvbtn').attr('disabled', false);
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#postTallyData tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++) {
            var innertext = cols[j].innerText.replace(/,/g, ' ');
            row.push(innertext);
        }

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var servieDate = $("#servieDate").val();
    servieDate = moment(servieDate).format('YYYY-MM-DD');
    var html = document.querySelector("#postTallyData").html;
    export_table_to_csv(html, "Incomefile" + servieDate + ".csv");
}

function exceller(type, fn, dl) {
    var servieDate = $("#servieDate").val();
    servieDate = moment(servieDate).format('YYYY-MM-DD');
    var type = 'xlsx';
    var elt = document.getElementById('postTallyData');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('Incomefile' + servieDate + '.' + (type || 'xlsx')));
}
function postInsToSoftware() {
    var servieDate = $("#servieDate").val();
    var url = base_url + "/master/insertIncomeDetailsSW";
    var param = { servieDate: servieDate };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#postDataToSwSpin').attr('disabled', true);
            $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
             toastr.success("Successfully Posted");
            setTimeout(function () {
               window.location.reload();
            }, 400);
        },
        complete: function () {

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
