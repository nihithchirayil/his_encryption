var is_edit = 0;
function goToSelectedPatient(patient_id) {
    $('body').LoadingOverlay("show", {
        background: "rgba(255, 255, 255, 0.7)",
        imageColor: '#337AB7'
    });
    window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
}



$(document).ready(function () {

    //----set default doctor in locaal storage-----
    var url = new URL(window.location.href);
    var doctor_id = url.searchParams.get("doctor_id");
    localStorage.setItem('selected_dr_id', doctor_id);

    //-----set practice location---------------------
    var practice_location = localStorage.getItem('practice_location');
    var practice_location_name = localStorage.getItem('practice_location_name');

    if (practice_location == null) {
        selectPracticeLocation(doctor_id);
    } else {
        $('#practice_location').select2("val", practice_location);
        $('#current_practice_location').html(practice_location_name);
    }

    searchPatient();
    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });
    $('#menu_toggle').on('click', function () {
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.date_time_picker').datetimepicker();


});
/**
 * OP NUMBER SEARCH STARTS
 */
$('#op_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv").html("");
            $("#op_no_search_hidden").val("");
        } else {
            try {
                var url = '';
                var param = { op_no_search: op_no_search, op_no_search_prog: 1 };
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#op_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#op_no_searchCodeAjaxDiv").html(html).show();
                        $("#op_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('op_no_searchCodeAjaxDiv', event);
    }
});
$('#op_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_id(list, patient_id, uhid) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#op_no_search_hidden').val(patient_id);
    $('#op_no_search').val(uhid);
    $('#' + itemCodeListDivId).hide();
}
/**
 *
 * OP NUMBER SEARCH ENDS
 */


function searchPatient() {
    var file_token = $('#hidden_filetoken').val();
    var url = $("#ins_base_url").val();
    url = url + "/patient_register/patientList";
    var op_no_search_hidden = $("#op_no_search_hidden").val();
    var op_no_search = $("#op_no_search").val();
    var practice_location = $("#practice_location").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var phone_no = $("#phone_no").val();

    var params = { _token: file_token, op_no_search_hidden: op_no_search_hidden, op_no_search: op_no_search, practice_location: practice_location, from_date: from_date, to_date: to_date, phone_no: phone_no, is_ajax: 1 };
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner');
            $('#patientListDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $('#patientListDiv').html('');
            $('#patientListDiv').html(msg);
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }

            });
        },
        complete: function () {
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner');
            $('#searchdataspin').addClass('fa fa-search');
            $('#patientListDiv').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

function PatientRenew(patient_id) {
    var doctor_id = localStorage.getItem('selected_dr_id');
    $.ajax({
        type: "GET",
        url: $('#base_url').val() + "/patient_register/patientRenew",
        data: { patient_id: patient_id, doctor_id: doctor_id },
        beforeSend: function () {
            $('#patientListDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (response) {
            // $('#patientListDiv').LoadingOverlay("hide");
            var obj = JSON.parse(response);
            if (obj.status == 'success') {
                toastr.success(obj.message);
                window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
            } else {
                toastr.error(obj.message);
            }

        },
    });
}

function goto_add_new_patient() {
    $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
    window.location.href = $('#base_url').val() + "/patient_register/patientRegister";
}

// function selectPracticeLocation() {
//     $doctor_id = localStorage.getItem('selected_dr_id');
//     $('#practice_location_modal').modal({
//         show: true,
//         keyboard: false,
//         backdrop: 'static'
//     });

//     $.ajax({
//         type: "GET",
//         url: $('#base_url').val() + "/patient_register/getPracticeLocation",
//         data: { doctor_id: $doctor_id },
//         beforeSend: function () {
//             $('#practice_location_modal').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
//         },
//         success: function (data) {
//             $('#practice_location_modal').LoadingOverlay("hide");
//             $('#practice_location_modal_data').html(data);
//         }
//     });
// }

// function set_practice_location(practice_location_id, name) {
//     $.ajax({
//         type: "GET",
//         url: $('#base_url').val() + "/patient_register/setPracticeLocation",
//         data: { practice_location_id: practice_location_id },
//         beforeSend: function () {
//             $('#practice_location_modal').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
//         },
//         success: function (data) {
//             $('#practice_location_modal').LoadingOverlay("hide");
//             $('#practice_location_modal').modal('hide');
//             $('#practice_location').val(data);
//             $('#practice_location_hidden').val(data);
//             localStorage.setItem('practice_location', data);
//             localStorage.setItem('practice_location_name', name);
//             $('#practice_location').select2("val", data);
//             $('#current_practice_location').html(name);
//             searchPatient();
//         }

//     });
// }

function clear_list_search() {
    $("#practice_location").val(null).trigger("change");
    $('#op_no_search').val('');
    $('#op_no_search_hidden').val('');
    $('#phone_no').val('');
}

