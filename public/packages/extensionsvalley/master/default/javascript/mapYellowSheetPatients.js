$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    base_url = $('#base_url').val();
    var admission_date = $('#patient_admission_date').val();
    var discharge_date = $('#patient_discharge_date').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate: admission_date,
        maxDate: discharge_date
    });

    loadTabData();
    $("#doctorSearchName").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#doctormasterServicesDiv tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

function searchProcudureList() {
    var value = $('#procedureSearch').val().toLowerCase();
    $("#yellowSheetServicesDiv tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

document.body.addEventListener('click', function (event) {
    $("#fav_grp_div").hide();
});
base_url = '';
list_flag = 1;

function loadTabData() {
    if (list_flag == 1) {
        getYelloewSheetData()
    } else if (list_flag == 2) {
        getDoctorSheetData();
    } else if (list_flag == 3) {
        getYellowSheetHourlyService();
    }

}

function getYelloewSheetData() {
    getYellowSheetServices(1);
    getBiliingYellowSheetServices();
    AlltotalChecked();
    list_flag = 1;
}

function getDoctorSheetData() {
    getDoctorMasterList(1);
    getDoctorMasterList(2);
    list_flag = 2;
}

function getYellowSheetHourlyService() {
    getYellowSheetHourlyServiceEntry();
    getYellowSheetHourlyServiceList();
    list_flag = 3;

}

function searchpatientMaster(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = 'fav_grp_div';
    var url = base_url + "/master/searchPatientMaster";

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var patientMaster = $('#' + id).val();
        if (patientMaster == "") {
            $("#" + ajax_div).html("").hide();
            $('#patientMasterID').val('');
        } else {
            var param = {
                patientMaster: patientMaster
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#" + ajax_div).LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    }).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $("#" + ajax_div).LoadingOverlay("hide");
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function fillGroupDetails(obj, patient_id) {
    window.location.href = base_url + "/master/mapYellowSheetPatient/" + patient_id;
}

function getYellowSheetServices(from_type) {
    var patient_id = $("#patient_id").val();
    if (patient_id != '0') {
        var doctor_id = $("#doctor_id").val();
        var visit_id = $("#visit_id").val();
        var servieDate = $("#servieDate").val();
        var servieDate = $("#servieDate").val();
        var procedure_type_id = $("#procedure_type_id").val();
        var url = base_url + "/master/getYellowSheetServices";
        var param = {
            visit_id: visit_id,
            servieDate: servieDate,
            doctor_id: doctor_id,
            patient_id: patient_id,
            procedure_type_id: procedure_type_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#yellowSheetServicesDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('#yellowSheetServicesDiv').html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                $('#yellowSheetServicesDiv').LoadingOverlay("hide");
                setTimeout(function () {
                    searchProcudureList()
                }, 1000);

            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient");
    }


}


function getYellowSheetReport(from_type) {
    var patient_id = $("#patient_id").val();
    if (patient_id != '0') {
        var doctor_id = $("#doctor_id").val();
        var visit_id = $("#visit_id").val();
        var admission_date = $("#patient_admission_date").val();
        var discharge_date = $("#patient_discharge_date").val();
        var servieDate = $("#servieDate").val();
        var url = base_url + "/master/getYellowSheetServicesReport";
        var param = {
            visit_id: visit_id,
            discharge_date: discharge_date,
            admission_date: admission_date,
            servieDate: servieDate,
            doctor_id: doctor_id,
            patient_id: patient_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {

                $('#yellowSheetServicesReportDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('#yellowSheetServicesReportDiv').html(data);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var left = $('.theadscroll').width() * 3;
                $('.theadscroll').scrollLeft(left);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                    var objDiv = document.getElementById("yellowsheetReportScroll");
                    objDiv.scrollLeft = objDiv.scrollWidth;
                    var objDiv = document.getElementById("DoctorListReportScroll");
                    objDiv.scrollLeft = objDiv.scrollWidth;
                }, 400);
            },
            complete: function () {

                $('#yellowSheetServicesReportDiv').LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient");
    }

}

function getBiliingYellowSheetServices() {
    var patient_id = $("#patient_id").val();
    if (patient_id != '0') {
        var doctor_id = $("#doctor_id").val();
        var visit_id = $("#visit_id").val();
        var servieDate = $("#servieDate").val();
        var url = base_url + "/master/getBiliingYellowSheetServices";
        var param = {
            visit_id: visit_id,
            servieDate: servieDate,
            doctor_id: doctor_id,
            patient_id: patient_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#billingyellowSheetServicesDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('#billingyellowSheetServicesDiv').html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            },
            complete: function () {
                $('#billingyellowSheetServicesDiv').LoadingOverlay("hide");
                $(".doc_data").select2("destroy").select2();
                AlltotalChecked();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }

}

function addYellowSheetBillingServices(sheet_id, billing_sheet_id, price = 0) {
    var checked_status = $('#addBillisngServicesbtn' + sheet_id).is(":checked");
    var yellow_sheet_price = $('#yellow_sheet_price_' + sheet_id).html();
    var doctor_id = $("#doctor_id").val();
    if (price == 0) {
        bootbox.prompt({
            title: 'Enter Service Price!',
            inputType: 'number',
            callback: function (result) {

                if (result === null || result.trim() === '') {
                    // User clicked on "Cancel" or didn't enter a price
                    $('#addBillisngServicesbtn' + sheet_id).prop("checked", false);
                    return;
                }

                yellow_sheet_price = parseFloat(result);

                if (isNaN(yellow_sheet_price) || yellow_sheet_price <= 0) {
                    toastr.warning("Invalid price entered. Please enter a valid positive number.");
                    $('#addBillisngServicesbtn' + sheet_id).prop("checked", false);
                    return;
                }

                if (doctor_id != '0') {
                    var visit_id = $("#visit_id").val();
                    var patient_id = $("#patient_id").val();
                    var servieDate = $("#servieDate").val();
                    var url = base_url + "/master/saveBillingYellowSheet";
                    var param = {
                        checked_status: checked_status,
                        billing_sheet_id: billing_sheet_id,
                        servieDate: servieDate,
                        sheet_id: sheet_id,
                        visit_id: visit_id,
                        doctor_id: doctor_id,
                        patient_id: patient_id,
                        yellow_sheet_price: yellow_sheet_price
                    };
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: param,
                        beforeSend: function () {
                            $('#addBillingServicesbtn' + sheet_id).attr('disabled', true);
                        },
                        success: function (data) {
                            if (data != '0') {
                                if (checked_status) {
                                    $('#addBillisngServicesbtn' + sheet_id).attr('onclick', 'addYellowSheetBillingServices(' + sheet_id + ',' + data + ');');
                                } else {
                                    $('#addBillisngServicesbtn' + sheet_id).attr('onclick', 'addYellowSheetBillingServices(' + sheet_id + ',' + 0 + ');');
                                }
                            } else {
                                toastr.warning("Bill Already Generated");
                                if (checked_status) {
                                    $('#addBillisngServicesbtn' + sheet_id).prop("checked", false);
                                } else {
                                    $('#addBillisngServicesbtn' + sheet_id).prop("checked", true);
                                }
                            }

                        },
                        complete: function () {
                            $('#addBillingServicesbtn' + sheet_id).attr('disabled', false);
                            getYelloewSheetData();
                        },
                        error: function () {
                            toastr.error("Error Please Check Your Internet Connection");
                        }
                    });
                } else {
                    toastr.warning("Please Select Any Doctors");
                }

            }
        });
    } else {
        if (doctor_id != '0') {
            var visit_id = $("#visit_id").val();
            var patient_id = $("#patient_id").val();
            var servieDate = $("#servieDate").val();
            var url = base_url + "/master/saveBillingYellowSheet";
            var param = {
                checked_status: checked_status,
                billing_sheet_id: billing_sheet_id,
                servieDate: servieDate,
                sheet_id: sheet_id,
                visit_id: visit_id,
                doctor_id: doctor_id,
                patient_id: patient_id,
                yellow_sheet_price: yellow_sheet_price
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#addBillingServicesbtn' + sheet_id).attr('disabled', true);
                },
                success: function (data) {
                    if (data != '0') {
                        if (checked_status) {
                            $('#addBillisngServicesbtn' + sheet_id).attr('onclick', 'addYellowSheetBillingServices(' + sheet_id + ',' + data + ');');
                        } else {
                            $('#addBillisngServicesbtn' + sheet_id).attr('onclick', 'addYellowSheetBillingServices(' + sheet_id + ',' + 0 + ');');
                        }
                    } else {
                        toastr.warning("Bill Already Generated");
                        if (checked_status) {
                            $('#addBillisngServicesbtn' + sheet_id).prop("checked", false);
                        } else {
                            $('#addBillisngServicesbtn' + sheet_id).prop("checked", true);
                        }
                    }

                },
                complete: function () {
                    $('#addBillingServicesbtn' + sheet_id).attr('disabled', false);
                    getYelloewSheetData();
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Any Doctors");
        }
    }

}


function getCheckedColumns() {
    var multi_select_list = new Array();
    var doctor_id = 0;
    var time = '';
    var sheet_id = 0;
    $('#previousYelloeSheetData tr').each(function () {
        doctor_id = 0;
        time = '';
        sheet_id = 0;
        if ($.type($(this).find('input[name="checkPreviousPatientProcedures"]').val()) != "undefined") {
            status = $(this).find('input[name="checkPreviousPatientProcedures"]').is(":checked");
            if (status == 'true') {
                sheet_id = $(this).find('input[name="checkPreviousPatientProcedures"]').val();
                if ($.type($(this).find('select[name="previousSheetDoctor"]').val()) != "undefined") {
                    doctor_id = $(this).find('select[name="previousSheetDoctor"]').val();
                }

                if ($.type($(this).find('input[name="privousYellowSheetTimePicker"]').val()) != "undefined") {
                    time = $(this).find('input[name="privousYellowSheetTimePicker"]').val();
                }
                multi_select_list.push({
                    'doctor_id': doctor_id,
                    'time': time,
                    'sheet_id': sheet_id
                });
            }

        }
    });
    return multi_select_list;
}

function addPreviousYellowSheet() {
    var sheet_id = JSON.stringify(getCheckedColumns());
    if (sheet_id != '[]') {
        var visit_id = $("#visit_id").val();
        var patient_id = $("#patient_id").val();
        var servieDate = $("#servieDate").val();
        var url = base_url + "/master/savePreviousBillingYellowSheet";
        var param = {
            servieDate: servieDate,
            sheet_id: sheet_id,
            visit_id: visit_id,
            patient_id: patient_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#savePreviousServicesBtn').attr('disabled', true);
                $('#savePreviousServicesSpin').removeClass('fa fa-save');
                $('#savePreviousServicesSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (data != '0') {
                    toastr.success("Successfully Updated");
                    $("#addPreviousServicesModel").modal('toggle');
                    getYelloewSheetData();
                } else {
                    toastr.warning("Bill Already Generated");
                }
            },
            complete: function () {
                $('#savePreviousServicesBtn').attr('disabled', false);
                $('#savePreviousServicesSpin').removeClass('fa fa-spinner fa-spin');
                $('#savePreviousServicesSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Data");
    }
}


function getPreviousYellowSheetSevices(from_type) {
    var doctor_id = $("#doctor_id").val();
    var preivousservieDate = '';
    if (from_type == '2') {
        preivousservieDate = $('#copyfrom_previous').val();
    }
    if (doctor_id != '0') {
        var visit_id = $("#visit_id").val();
        var patient_id = $("#patient_id").val();
        var servieDate = $("#servieDate").val();
        var url = base_url + "/master/getPreviousYellowSheetSevices";
        var param = {
            preivousservieDate: preivousservieDate,
            servieDate: servieDate,
            visit_id: visit_id,
            doctor_id: doctor_id,
            patient_id: patient_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#addPreviousServicesBtn').attr('disabled', true);
                $('#addPreviousServicesSpin').removeClass('fa fa-plus');
                $('#addPreviousServicesSpin').addClass('fa fa-spinner fa-spin');
                $('#addPreviousServicesModelDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                if (data != '0') {
                    if (from_type == '1') {
                        $("#addPreviousServicesModel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                    $('#addPreviousServicesModelDiv').html(data);
                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);

                    $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                } else {
                    toastr.warning("Bill Already Generated");
                }

            },
            complete: function () {
                $('#addPreviousServicesBtn').attr('disabled', false);
                $('#addPreviousServicesSpin').removeClass('fa fa-spinner fa-spin');
                $('#addPreviousServicesSpin').addClass('fa fa-plus');
                $('#addPreviousServicesModelDiv').LoadingOverlay("hide");
                var admission_date = $('#patient_admission_date').val();
                $('#copyfrom_previous').datetimepicker({
                    format: 'MMM-DD-YYYY',
                    minDate: new Date(admission_date),
                    maxDate: new Date(),
                });
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Doctors");
    }
}

function checkAllProcedures() {
    var checked_status = $('#checkAllPatientProcedures').is(":checked");
    if (checked_status) {
        $('.procedure_check').prop('checked', true);
    } else {
        $('.procedure_check').prop('checked', false);
    }
}

function removeYellowSheetBillingServices(sheet_id, yellowsheetid, log_id) {
    var url = base_url + "/master/deleteYellowSheetBillingServices";
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#servieDate").val();
    var visit_time = $("#yellowsheettime_picker" + sheet_id).val();
    var param = {
        servieDate: servieDate,
        visit_time: visit_time,
        visit_id: visit_id,
        doctor_id: doctor_id,
        patient_id: patient_id,
        sheet_id: sheet_id,
        log_id: log_id
    };
    bootbox.confirm({
        message: 'Are You Sure You Want To Delete ?',
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning pull-right save_align'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#removeBillingServicesbtn' + sheet_id).attr('disabled', true);
                        $('#removeBillingServicesspin' + sheet_id).removeClass('fa fa-trash-o');
                        $('#removeBillingServicesspin' + sheet_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success("Successfully removed");
                            $('#billingyellowsheetrow' + sheet_id).remove();
                            $('#addBillisngServicesbtn' + yellowsheetid).attr('onclick', 'addYellowSheetBillingServices(' + yellowsheetid + ',' + 0 + ');');
                            $('#addBillisngServicesbtn' + yellowsheetid).prop("checked", false);
                        } else {
                            toastr.warning(data.message);
                        }

                    },
                    complete: function () {
                        $('#removeBillingServicesbtn' + sheet_id).attr('disabled', false);
                        $('#removeBillingServicesspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
                        $('#removeBillingServicesspin' + sheet_id).addClass('fa fa-trash-o');
                        AlltotalChecked();
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        }
    });
}


function updateYellowSheetBillingServices(sheet_id) {
    var doctor_id = $("#yellowsheetdoctor_select" + sheet_id).val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#servieDate").val();
    var visit_time = $("#yellowsheettime_picker" + sheet_id).val();
    var url = base_url + "/master/updateYellowSheetBillingServices";
    if (doctor_id) {
        if (visit_time) {
            var param = {
                servieDate: servieDate,
                visit_id: visit_id,
                patient_id: patient_id,
                sheet_id: sheet_id,
                doctor_id: doctor_id,
                visit_time: visit_time
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', true);
                    $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-repeat');
                    $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (data != '0') {
                        toastr.success("Successfully Updated");
                    } else {
                        toastr.warning("Bill Already Generated");
                    }
                },
                complete: function () {
                    $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', false);
                    $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
                    $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-repeat');
                    AlltotalChecked();
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Any Time");
            $("#yellowsheettime_picker" + sheet_id).focus();
        }
    } else {
        toastr.warning("Please Select Any Doctor");
        $("#yellowsheetdoctor_select" + sheet_id).focus();
    }
}

function repeatYellowSheetBillingServices(sheet_id) {
    var url = base_url + "/master/copyYellowSheetBillingServices";
    var param = {
        sheet_id: sheet_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', true);
            $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-repeat');
            $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != '0') {
                toastr.success("Successfully Added");
                getYelloewSheetData();
            } else {
                toastr.warning("Bill Already Generated");
            }
        },
        complete: function () {
            $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', false);
            $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
            $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-repeat');
            AlltotalChecked();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getDoctorMasterList(from_type) {
    var patient_id = $("#patient_id").val();
    if (patient_id != '0') {

        var url = base_url + "/master/getDoctorMasterList";
        var setdatadiv = 'doctormasterServicesDiv';
        if (from_type == '2') {
            setdatadiv = 'billingdoctormasterServicesDiv';
        }

        var visit_id = $("#visit_id").val();
        var servieDate = $("#servieDate").val();
        var param = {
            servieDate: servieDate,
            from_type: from_type,
            visit_id: visit_id,
            patient_id: patient_id
        };

        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#' + setdatadiv).LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('#' + setdatadiv).html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                $('#' + setdatadiv).LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient");
    }
}

function check_doctorservice(doctor_id, from_type, service_mode, billing_status) {
    var checked_status = $('#check_doctorservice' + doctor_id + from_type + service_mode).is(":checked");
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#servieDate").val();
    var url = base_url + "/master/saveDoctorBillingService";
    var param = {
        billing_status: billing_status,
        servieDate: servieDate,
        visit_id: visit_id,
        patient_id: patient_id,
        service_mode: service_mode,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#check_doctorservice' + doctor_id + 1 + service_mode).attr('disabled', true);
            if ($('#check_doctorservice' + doctor_id + 2 + service_mode)) {
                $('#check_doctorservice' + doctor_id + 2 + service_mode).attr('disabled', true);
            }
        },
        success: function (data) {
            if (data != '0') {
                if (checked_status) {
                    $('#check_doctorservice' + doctor_id + 1 + service_mode).attr('onclick', 'check_doctorservice(' + doctor_id + ',' + 1 + ',' + service_mode + ',' + data + ');');
                    if ($('#check_doctorservice' + doctor_id + 2 + service_mode)) {
                        $('#check_doctorservice' + doctor_id + 2 + service_mode).attr('onclick', 'check_doctorservice(' + doctor_id + ',' + 2 + ',' + service_mode + ',' + data + ');');
                        $('#check_doctorservice' + doctor_id + 2 + service_mode).prop("checked", true);
                    }
                    $('#check_doctorservice' + doctor_id + 1 + service_mode).prop("checked", true);
                } else {
                    $('#check_doctorservice' + doctor_id + 1 + service_mode).attr('onclick', 'check_doctorservice(' + doctor_id + ',' + 1 + ',' + service_mode + ',' + 0 + ');');
                    if ($('#check_doctorservice' + doctor_id + 2 + service_mode)) {
                        $('#check_doctorservice' + doctor_id + 2 + service_mode).attr('onclick', 'check_doctorservice(' + doctor_id + ',' + 2 + ',' + service_mode + ',' + 0 + ');');
                        $('#check_doctorservice' + doctor_id + 2 + service_mode).prop("checked", false);
                    }
                    $('#check_doctorservice' + doctor_id + 1 + service_mode).prop("checked", false);
                }
            } else {
                toastr.warning("Bill Already Generated");
                if (checked_status) {
                    $('#check_doctorservice' + doctor_id + from_type + service_mode).prop("checked", false);
                } else {
                    $('#check_doctorservice' + doctor_id + from_type + service_mode).prop("checked", true);
                }
            }
        },
        complete: function () {
            $('#check_doctorservice' + doctor_id + 1 + service_mode).attr('disabled', false);
            if ($('#check_doctorservice' + doctor_id + 2 + service_mode)) {
                $('#check_doctorservice' + doctor_id + 2 + service_mode).attr('disabled', false);
            }
            getDoctorMasterList(2);
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getYellowSheetHourlyServiceEntry(from_type) {
    var patient_id = $("#patient_id").val();
    var visit_id = $("#visit_id").val();
    if (patient_id != '0') {
        var url = base_url + "/master/getYellowSheetHourlyServices";
        var param = {
            hourly_service: 1,
            patient_id: patient_id,
            visit_id: visit_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {

                $('#yellowSheetHourlyServiceDiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('#yellowSheetHourlyServicetbody').html(data);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var left = $('.theadscroll').width() * 3;
                $('.theadscroll').scrollLeft(left);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });

                }, 400);
            },
            complete: function () {

                $('#yellowSheetHourlyServiceDiv').LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient");
    }

}
$('.hourly-service-input').on('keyup', function () {
    calculateHourlyServiceCharge(this); // Pass the element itself
});

function calculateHourlyServiceCharge(inputElement) {
    var service_id = $(inputElement).attr('id').split('_')[2];
    var service_charge = $(inputElement).val();

    var service_rate = $("#service_rate_" + service_id).html();
    var calculated_price = parseFloat(service_rate) * parseFloat(service_charge);
    $("#calculated_service_td_" + service_id).html(calculated_price);
}

function saveHourlyService(e, id) {
    var patient_id = $("#patient_id").val();
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var servieDate = $("#servieDate").val();
    var service_id = id;
    var qnty = $("#hourly_service_" + id).val();
    if (parseFloat(qnty) == 0 || qnty == '') {
        toastr.error("Please select quantity");
        return false;
    }
    var total_charge = $("#calculated_service_td_" + service_id).html();
    var param = {
        service_id: service_id,
        qnty: qnty,
        total_charge: total_charge,
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id,
        servieDate: servieDate
    };
    var url = base_url + "/master/saveHourlyServiceData";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("btn-hourly").attr('disabled', true);
            $('#btn_save_hourly_spin' + service_id).removeClass('fa fa-save');
            $('#btn_save_hourly_spin' + service_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data > 1) {
                toastr.success("Saved Successfully");
            }
        },
        complete: function () {
            $("btn-hourly").attr('disabled', false);
            $('#btn_save_hourly_spin' + service_id).removeClass('fa fa-spinner fa-spin');
            $('#btn_save_hourly_spin' + service_id).addClass('fa fa-save');
            getYellowSheetHourlyServiceList();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function selectProcedureType(e) {
    var service_id = $(e).val();
    var url = base_url + "/master/getYellowSheetHourlyServices";
    var param = {
        hourly_service: 1,
        procedure_type_id: service_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#yellowSheetHourlyServiceDiv').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            $('#yellowSheetHourlyServicetbody').html(data);
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var left = $('.theadscroll').width() * 3;
            $('.theadscroll').scrollLeft(left);
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

            }, 400);
        },
        complete: function () {

            $('#yellowSheetHourlyServiceDiv').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function getYellowSheetHourlyServiceList(from_type) {
    var patient_id = $("#patient_id").val();
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var servieDate = $("#servieDate").val();
    if (patient_id != '0') {
        var url = base_url + "/master/getYellowSheetHourlyServiceList";
        var param = {
            patient_id: patient_id,
            doctor_id: doctor_id,
            visit_id: visit_id,
            servieDate: servieDate
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {

                $('#yellow_sheet_hourly_list_div').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function (data) {
                $('#yellow_sheet_hourly_list_div').html(data);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                var left = $('.theadscroll').width() * 3;
                $('.theadscroll').scrollLeft(left);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
            },
            complete: function () {
                $('#yellow_sheet_hourly_list_div').LoadingOverlay("hide");
                AlltotalHourly();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient");
    }

}

function updateQnty(qty, id, price) {
    var url = base_url + "/master/updateHourlyQty";
    var patient_id = $("#patient_id").val();
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var servieDate = $("#servieDate").val();
    var param = {
        qty: qty,
        id: id,
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id,
        servieDate: servieDate,
        price: price
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#yellow_sheet_hourly_list_div').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data > 1) {
                toastr.success("Successfully Updated");
            }
        },
        complete: function () {
            $('#yellow_sheet_hourly_list_div').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function changePrice(id) {
    var qty_val = $("#update_qty_" + id).val();
    var price = $("#hourly_price_" + id).val();
    var calculated_price = parseFloat(qty_val) * parseFloat(price);
    $("#ttl_charge_updated_" + id).html(calculated_price);
    AlltotalHourly();
}

function updateYellowSheetBillingServicesHourly(sheet_id) {
    var doctor_id = $("#yellowsheetdoctorhourly_select" + sheet_id).val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#servieDate").val();
    var visit_time = $("#yellowsheettime_picker" + sheet_id).val();
    var url = base_url + "/master/updateYellowSheetBillingServices";
    if (doctor_id) {
        if (visit_time) {
            var param = {
                servieDate: servieDate,
                visit_id: visit_id,
                patient_id: patient_id,
                sheet_id: sheet_id,
                doctor_id: doctor_id,
                visit_time: visit_time
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#yellow_sheet_hourly_list_div').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function (data) {
                    if (data != '0') {
                        toastr.success("Successfully Updated");
                    } else {
                        toastr.warning("Bill Already Generated");
                    }
                },
                complete: function () {
                    $('#yellow_sheet_hourly_list_div').LoadingOverlay("hide");
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Any Time");
            $("#yellowsheettime_picker" + sheet_id).focus();
        }
    } else {
        toastr.warning("Please Select Any Doctor");
        $("#yellowsheetdoctor_select" + sheet_id).focus();
    }
}

function updateHourlQnty(id) {
    var url = base_url + "/master/updateHourlyQty";
    var patient_id = $("#patient_id").val();
    var doctor_id = $("#yellowsheetdoctorhourly_select" + id).val();
    var visit_id = $("#visit_id").val();
    var servieDate = $("#servieDate").val();
    var qty = $("#update_qty_" + id).val();
    var price = $("#hourly_price_" + id).val();
    var param = {
        qty: qty,
        id: id,
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id,
        servieDate: servieDate,
        price: price
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#saveBillingServicesdetailbtn" + id).attr('disabled', true);
            $('#saveBillingServicesdetailspin' + id).removeClass('fa fa-save');
            $('#saveBillingServicesdetailspin' + id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data == 1) {
                toastr.success("Successfully Updated");
            }
        },
        complete: function () {
            $("#saveBillingServicesdetailbtn" + id).attr('disabled', false);
            $('#saveBillingServicesdetailspin' + id).removeClass('fa fa-spinner fa-spin');
            $('#saveBillingServicesdetailspin' + id).addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function removeYellowSheetBillingServicesHourly(sheet_id, yellowsheetid, log_id) {
    var url = base_url + "/master/deleteYellowSheetBillingServices";
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#servieDate").val();
    var visit_time = $("#update_service_time_hourly_" + sheet_id).val();
    var param = {
        servieDate: servieDate,
        visit_time: visit_time,
        visit_id: visit_id,
        doctor_id: doctor_id,
        patient_id: patient_id,
        sheet_id: sheet_id,
        log_id: log_id
    };
    bootbox.confirm({
        message: 'Are You Sure You Want To Delete ?',
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning pull-right save_align'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#removeBillingServicesbtn' + sheet_id).attr('disabled', true);
                        $('#removeBillingServicesspinhourly' + sheet_id).removeClass('fa fa-trash-o');
                        $('#removeBillingServicesspinhourly' + sheet_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data != '0') {
                            toastr.success("Successfully Removed");
                            $('#addBillisngServicesbtn' + yellowsheetid).attr('onclick', 'addYellowSheetBillingServices(' + yellowsheetid + ',' + 0 + ');');
                            $('#billingyellowsheetrowhourly' + sheet_id).remove();
                        } else {
                            toastr.warning("Bill Already Generated");
                        }

                    },
                    complete: function () {
                        $('#removeBillingServicesbtnhourly' + sheet_id).attr('disabled', false);
                        $('#removeBillingServicesspinhourly' + sheet_id).removeClass('fa fa-spinner fa-spin');
                        $('#removeBillingServicesspinhourly' + sheet_id).addClass('fa fa-trash-o');
                        AlltotalHourly();
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        }
    });
}

function AlltotalHourly() {
    var total = 0;
    $('[id^="ttl_charge_updated_"]').each(function () {
        var value = parseFloat($(this).text());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $("#all_total_hourly").html('<b>' + total + '</b>');
}

function changePriceChecked(value, id) {
    var url = base_url + "/master/updateHourlyQty";
    var patient_id = $("#patient_id").val();
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var servieDate = $("#servieDate").val();
    if (value == '' || value == 0) {
        toastr.warning("Please enter a valid price!");
        return false;
    }
    var param = {
        qty: '1',
        id: id,
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id,
        servieDate: servieDate,
        price: value
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#billingyellowSheetServicesDiv').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
        },
        success: function (data) {
            if (data > 1) {
                toastr.success("Successfully Updated");
            }
        },
        complete: function () {
            $('#billingyellowSheetServicesDiv').LoadingOverlay("hide");
            AlltotalChecked();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function AlltotalChecked() {
    var total = 0;
    $('[id^="update_price_procedure_"]').each(function () {
        var value = parseFloat($(this).val());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $("#all_total_checked").html('<b>' + total + '</b>');
}

function updateBillYellowSheetfn() {
    var url = base_url + "/master/updateBillYellowSheetfn";
    var patient_id = $("#patient_id").val();
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var servieDate = $("#servieDate").val();
    var param = {
        patient_id: patient_id,
        doctor_id: doctor_id,
        visit_id: visit_id,
        servieDate: servieDate,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#updateBillYellowSheetfnBtn').attr('disabled', true);
            $('#updateBillYellowSheetfnSpin').removeClass('fa fa-save');
            $('#updateBillYellowSheetfnSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (parseInt(data.status) == 1) {
                toastr.success(data.message);
            } else {
                toastr.warning(data.message);
            }
        },
        complete: function () {
            $('#updateBillYellowSheetfnBtn').attr('disabled', false);
            $('#updateBillYellowSheetfnSpin').removeClass('fa fa-spinner fa-spin');
            $('#updateBillYellowSheetfnSpin').addClass('fa fa-save');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
