$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    searchGrnReceiveList();
});

var base_url = $('#base_url').val();
var token = $('#c_token').val();



function searchGrnReceiveList() {
    var url = base_url + "/master/searchGrnReceiveList";
    var grn_from_date = $('#grn_from_date').val();
    var grn_to_date = $('#grn_to_date').val();
    var approve_from_date = $('#approve_from_date').val();
    var approve_to_date = $('#approve_to_date').val();
    var bill_no = $('#bill_no_search').val();
    var grn_no = $('#grn_no_search').val();
    var received_only = $('#received_only').is(':checked');
    var param = {
        _token: token,
        grn_from_date: grn_from_date,
        grn_to_date: grn_to_date,
        approve_from_date: approve_from_date,
        approve_to_date: approve_to_date,
        bill_no: bill_no,
        grn_no: grn_no,
        received_only: received_only
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#pagination_list_datadiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('#search_grnbtn').attr('disabled', true);
            $('#search_grnSpin').removeClass('fa fa-search');
            $('#search_grnSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#pagination_list_datadiv').html(data);
        },
        complete: function () {
            $("#pagination_list_datadiv").LoadingOverlay("hide");
            $('#search_grnbtn').attr('disabled', false);
            $('#search_grnSpin').removeClass('fa fa-spinner fa-spin');
            $('#search_grnSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function save_receive_grn(track_id) {
    var grn_no = $('#grn_nolistdata' + track_id).html();
    bootbox.confirm({
        message: "Are you sure you want to receive GRN No. " + grn_no + " ?",
        buttons: {
            confirm: {
                label: "Receive",
                className: "btn-success",
                default: "true",
            },
            cancel: {
                label: "Cancel",
                className: "btn-warning",
            },
        },
        callback: function (result) {
            if (result) {
                var url = base_url + "/master/save_receive_grn";
                var grn_comments = $('#receive_grn_comments' + track_id).val();
                var param = {
                    _token: token,
                    track_id: track_id,
                    grn_comments: grn_comments
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#receive_grn_btn' + track_id).attr('disabled', true);
                        $('#receive_grn_spin' + track_id).removeClass('fa fa-save');
                        $('#receive_grn_spin' + track_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data == 'received') {
                            toastr.warning('Already Received');
                        } else {
                            toastr.success('Successfully Received');
                        }

                        $("#receive_grn_model").modal('toggle');
                        searchGrnReceiveList();
                    },
                    complete: function () {
                        $('#receive_grn_btn' + track_id).attr('disabled', false);
                        $('#receive_grn_spin' + track_id).removeClass('fa fa-spinner fa-spin');
                        $('#receive_grn_spin' + track_id).addClass('fa fa-save');
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        },
    });
}



$('#grn_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxGRNNoSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#grn_no_search').val();
        if (bill_no == "") {
            $('#ajaxGRNNoSearchBox').hide();
            $("#grn_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'grn_receive_no', search_key: bill_no },
                beforeSend: function () {
                    $("#ajaxGRNNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxGRNNoSearchBox").html(html).show();
                    $("#ajaxGRNNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxGRNNoSearchBox', event);
    }
});


$('#bill_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('ajaxBillNoSearchBox');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#bill_no_search').val();
        if (bill_no == "") {
            $('#ajaxBillNoSearchBox').hide();
            $("#bill_id_hidden").val('');
        } else {
            var url = base_url + "/purchase/grnNumberSearch";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, search_key_id: 'bill_receive_no', search_key: bill_no },
                beforeSend: function () {
                    $("#ajaxBillNoSearchBox").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#ajaxBillNoSearchBox").html(html).show();
                    $("#ajaxBillNoSearchBox").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('ajaxBillNoSearchBox', event);
    }
});

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function fillInvoice_no(grn_id, bill_no, from_type) {
    bill_no = htmlDecode(bill_no);
    if (from_type == 'grn_receive_no') {
        $('#grn_no_search').val(bill_no);
        $('#grn_id_hidden').val(grn_id);
        $('#ajaxGRNNoSearchBox').hide();
    } else if (from_type == 'bill_receive_no') {
        $('#bill_no_search').val(bill_no);
        $('#bill_id_hidden').val(grn_id);
        $('#ajaxBillNoSearchBox').hide();
    }
}

function resetSearchData() {
    $('#grn_from_date').val('');
    $("#bill_id_hidden").val('');
    $('#grn_to_date').val('');
    $('#approve_from_date').val('');
    $('#approve_to_date').val('');
    $('#bill_no_search').val('');
    $('#bill_id_hidden').val('');
    $('#ajaxBillNoSearchBox').hide();
    $('#ajaxGRNNoSearchBox').hide();
    $('#grn_no_search').val('');
    $('#grn_id_hidden').val('');
    $('#received_only').prop('checked', false);
    searchGrnReceiveList();
}
