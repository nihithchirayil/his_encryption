function get_str_slug(val, separator1) {
	var str = val;
	var separator = typeof(separator1)!='undefined'?separator1:'-';
    str = str.replace(/[^\w\s]/gi, separator);
    str = str.replace(/[\s]+/gi, separator);
//    var regi_str = '/['+separator+']+/gi';
//    str = str.replace(regi_str, separator);
    str = str.replace(/[-]+/gi, separator);
    str = str.replace(' ', separator);
    return str;
}