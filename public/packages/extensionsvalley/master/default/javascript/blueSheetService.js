$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    var route_value = $('#route_value').val();
    var decode_route = atob(route_value);
    route_json = JSON.parse(decode_route);

    searchBlueSheet();

    $("#searchaddedProcedure").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#procedurelistData tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});





$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    var file_token = $('#hidden_filetoken').val();

    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#serviceMasterID').val() != "") {
            $('#serviceMasterID').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();


        if (search_key == "") {
            $("#serviceMasterSearch_div").html("");
        } else {
            var url = route_json.service_mastersearch;
            var param = { _token: file_token, serviceMaster: search_key };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {

                    $("#serviceMasterSearch_div").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                         $("#serviceMasterSearch_div").html("No results found!").show();
                         $("#serviceMasterSearch_div").find('li').first().addClass('liHover');
                    } else {
                         $("#serviceMasterSearch_div").html(html).show();
                         $("#serviceMasterSearch_div").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillGroupDetails(obj, service_id, service_name) {
    $('#serviceMasterID').val(service_id);
    $('#serviceMasterSearch').val(service_name);
    $("#serviceMasterSearch_div").html("").hide();
}

function searchServiceMaster(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = 'fav_grp_div';
    var url = route_json.service_mastersearch;
    var file_token = $('#hidden_filetoken').val();

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var serviceMaster = $('#' + id).val();
        if (serviceMaster == "") {
            $("#" + ajax_div).html("").hide();
            $('#serviceMasterID').val('');
        } else {
            var param = { _token: file_token, serviceMaster: serviceMaster };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#addbluesheetbtn').attr('disabled', true);
                    $('#addbluesheetspin').removeClass('fa fa-plus');
                    $('#addbluesheetspin').addClass('fa fa-spinner fa-spin');
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('#addbluesheetbtn').attr('disabled', false);
                    $('#addbluesheetspin').removeClass('fa fa-spinner fa-spin');
                    $('#addbluesheetspin').addClass('fa fa-plus');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}


function saveServiceMaster() {
    var service_id = $('#serviceMasterID').val();
    var service_name = $('#serviceMasterSearch').val();
    if (service_id && service_name) {
        var url = route_json.saveBlueServiceMaster;
        var file_token = $('#hidden_filetoken').val();
        var param = { _token: file_token, service_id: service_id, service_name: service_name };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#addbluesheetbtn').attr('disabled', true);
                $('#addbluesheetspin').removeClass('fa fa-plus');
                $('#addbluesheetspin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                $('#serviceMasterID').val('');
                $('#serviceMasterSearch').val('');
                toastr.success("Successfully Updated");
                searchBlueSheet();
            },
            complete: function () {
                $('#addbluesheetbtn').attr('disabled', false);
                $('#addbluesheetspin').removeClass('fa fa-spinner fa-spin');
                $('#addbluesheetspin').addClass('fa fa-plus');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Enter Service Name");
        $('#serviceMasterSearch').focus();
    }

}

function searchBlueSheet() {
    var url = route_json.searchBlueSheet;
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#addbluesheetbtn').attr('disabled', true);
            $('#addbluesheetspin').removeClass('fa fa-plus');
            $('#addbluesheetspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#searchProcedureSearchDiv').html(data);
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);

            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
        },
        complete: function () {
            $('#addbluesheetbtn').attr('disabled', false);
            $('#addbluesheetspin').removeClass('fa fa-spinner fa-spin');
            $('#addbluesheetspin').addClass('fa fa-plus');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function deletebluesheet(sheet_id) {
    var url = route_json.deleteBluesheet;
    var file_token = $('#hidden_filetoken').val();
    var param = { _token: file_token, sheet_id: sheet_id };

    bootbox.confirm({
        message: 'Are You Sure You Want To Delete ?',
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning pull-right save_align'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#deletebluesheetbtn' + sheet_id).attr('disabled', true);
                        $('#deletebluesheetspin' + sheet_id).removeClass('fa fa-trash-o');
                        $('#deletebluesheetspin' + sheet_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data) {
                            $('#bluesheetrow' + sheet_id).remove();
                        }
                    },
                    complete: function () {
                        $('#deletebluesheetbtn' + sheet_id).attr('disabled', false);
                        $('#deletebluesheetspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
                        $('#deletebluesheetspin' + sheet_id).addClass('fa fa-trash-o');
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        }
    });

}
function clearProceudureSearch() {
    $('#serviceMasterID').val('');
    $('#serviceMasterSearch').val('');
    $("#fav_grp_div").html("").hide();
}