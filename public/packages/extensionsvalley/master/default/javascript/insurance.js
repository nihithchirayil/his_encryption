var is_edit = 0;
$(document).ready(function () {

    // $('#menu_toggle').trigger('click');
    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });
    $('#menu_toggle').on('click', function (){
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('.date_time_picker').datetimepicker();
    insurance_settlement_list();
});

$(document).on('click', '.page-link', function (e) {
    e.preventDefault();
    if ($(e.target).parent().hasClass('active') == false) {
        insurance_settlement_list($(this).attr('href').split('page=')[1]);

    }
});

function save_insurance(sett_type) {

    var val = $("#all_save_btn").val();
    var bill_from = $("#bill_from_" + val).val();
    var bill_head_id = $("#bill_head_id_" + val).val();
    var visit_id = $("#visit_id_" + val).val();
    var company_id = $("#company_id_" + val).val();
    var rec_amnt = $("#rec_amnt_" + val).val();
    var settle_amount = $("#settle_amount_entry").val();
    var tds_amount = $("#tds_amount_entry").val();
    var write_off_amount = $("#write_off_amount_entry").val();
    var claim_amount = $("#claim_amount_entry").val();
    var payment_checking = $("#ins_head_id_for_payment_mod").val();
    var pay_patient_adj = $("#pay_patient_adj_amount").val();
    var pay_insurance_adj = $("#pay_insurance_adj_amount").val();
    var pay_other_adj_amount = $("#pay_other_adj_amount").val();
    var bill_tag_ins = $("#bill_tag_ins").val();

    var url = $("#ins_base_url").val();
    url = url + "/master/save_insurance";
    var param = {save_ins: 1, bill_from: bill_from, company_id: company_id, rec_amnt: rec_amnt,
        visit_id: visit_id, bill_head_id: bill_head_id, settle_amount: settle_amount,
        tds_amount: tds_amount, write_off_amount: write_off_amount,claim_amount :claim_amount,pay_insurance_adj:pay_insurance_adj,
        pay_patient_adj:pay_patient_adj,pay_other_adj_amount:pay_other_adj_amount,bill_tag_ins:bill_tag_ins,sett_type:sett_type};
    if($("#ins_head_id_for_payment_mod").val() != 0 ){
        save_ins_payment_row(payment_checking);
    }
    if (sett_type != 5) {
        if ((pay_other_adj_amount == '' || parseFloat(pay_other_adj_amount) == 0 ) && (pay_insurance_adj== '' || parseFloat(pay_insurance_adj) == 0 ) &&  (pay_patient_adj == '' || parseFloat(pay_patient_adj) == 0)) {
            toastr.error("Amount Not entered");
            return;
        }
    }
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            if (sett_type == 5) {
                $('#save_fully_ins_payment_spin_btn').removeClass('fa fa-thumbs-up');
                $('#save_fully_ins_payment_spin_btn').addClass('fa fa-spinner fa-spin');
            } else {
                $('#save_ins_payment_spin_btn').removeClass('fa fa-save');
                $('#save_ins_payment_spin_btn').addClass('fa fa-spinner fa-spin');
            }
        },
        success: function (data) {
            if (data> 0) {
                toastr.success("Successfully Added");
                save_ins_payment_row(data);
            } else {
                toastr.error("Error occured Please check data");
            }
        },
        complete: function () {
            if (sett_type == 5) {
                $('#save_fully_ins_payment_spin_btn').removeClass('fa fa-spinner fa-spin');
                $('#save_fully_ins_payment_spin_btn').addClass('fa fa-thumbs-up');
            } else {
                $('#save_ins_payment_spin_btn').removeClass('fa fa-spinner fa-spin');
                $('#save_ins_payment_spin_btn').addClass('fa fa-save');
            }
            $('#insurance_list_modal').modal('hide');
            var page = $('#table_insurance_settlememtList').attr('table_page_count');
            insurance_settlement_list(page);
        }
    });

}
/**
 * OP NUMBER SEARCH STARTS
 */
$('#op_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv").html("");
            $("#op_no_search_hidden").val("");
        } else {
            try {
                var url = '';
                var param = {op_no_search: op_no_search,op_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#op_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#op_no_searchCodeAjaxDiv").html(html).show();
                        $("#op_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('op_no_searchCodeAjaxDiv', event);
    }
});
$('#op_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_id(list, patient_id, uhid) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#op_no_search_hidden').val(patient_id);
    $('#op_no_search').val(uhid);
    $('#' + itemCodeListDivId).hide();
}
/**
 *
 * OP NUMBER SEARCH ENDS
 */


//**************** IP NUMBER SEARCH STARTS **************************

$('#ip_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var ip_no_search = $(this).val();
        ip_search = ip_no_search.trim();
        if (ip_search == "") {
            $("#ip_no_searchCodeAjaxDiv").html("");
            $("#ip_no_search_hidden").val("");
        } else {
            try {
                var url = '';
                var paramts = {ip_no_search: ip_no_search,ip_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: paramts,
                    beforeSend: function () {
                        $("#ip_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#ip_no_searchCodeAjaxDiv").html(html).show();
                        $("#ip_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('ip_no_searchCodeAjaxDiv', event);
    }
});
$('#ip_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('ip_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_ip_id(list, admission_id,uhid) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#ip_no_search').val(admission_id);
    $('#ip_no_search_hidden').val(uhid);
    $('#' + itemCodeListDivId).hide();
}
//************************* IP NUMBER SEARCH ENDS *********************************************
$('#bill_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var bill_no_search = $(this).val();
        bill_search = bill_no_search.trim();
        if (bill_search == "") {
            $("#bill_no_searchCodeAjaxDiv").html("");
            $("#bill_no_search_hidden").val("");
        } else {
            try {
                var url = '';
                var param = {bill_no_search: bill_no_search,bill_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#bill_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#bill_no_searchCodeAjaxDiv").html(html).show();
                        $("#bill_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    }
});
$('#bill_no').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('bill_no_searchCodeAjaxDiv');
        return false;
    }
});
function fillBillValues(list, bill_no,bill_date, net_amount) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#bill_no_hidden').val(bill_no);
    $('#bill_no').val(bill_no);
    $('#bill_no_searchCodeAjaxDiv').hide();
}

function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function view_insurance(bill_head_id, bill_from,val,balance_amnt,patient_name,uhid,claim_amnt,pat_advance,pat_ins_advance,bill_tag,claim_amount_new) {
    if(balance_amnt <= 0){
        balance_amnt = '';
    }
    $("#pat_name_lbl").html('<b>'+patient_name+'&nbsp;&nbsp;['+ uhid +']</b>');
    $("#claim_lbl").html('<b>'+claim_amnt+'</b>');
    $("#balance_lbl").html('<b>'+balance_amnt+'</b>');
    $("#balance_lbl_txt").val(balance_amnt);
    $("#claim_amount_entry").val(claim_amount_new);
    $("#all_save_btn").val(val);
    $("#view_modal_bill_from").val(bill_from);
    $("#pat_adv").html(pat_advance);
    $("#pat_ins_adv").html(pat_ins_advance);
    $("#bill_tag_ins").val(bill_tag);
    var url = $("#ins_base_url").val();
    url = url + "/master/view_insurance_details";
    var params = {view_insurance_list: 1, bill_head_id: bill_head_id};
    $.ajax({
        type: "GET",
        url: url,
        data: params,
        beforeSend: function () {
            $('#view_ins_spin_' + bill_head_id).removeClass('fa fa-eye');
            $('#view_ins_spin_' + bill_head_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            let res = data;
            let response = '';
            let settlstats = 0;
            var j = 1;
            $('#pay_patient_adj_amount').val('0');
            $('#pay_insurance_adj_amount').val('0');
            $('#pay_other_adj_amount').val('0');
            // response += '<tr id="insurance_head_dtls_entry" style="background-color:#efb6b6">\n\
            //                                 <td></td>\n\
            //                                 <td></td>\n\
            //                                 <td><input type="text" id="settle_amount_entry" value=""  onkeyup="number_validation(this);calculate_settlement_amount();" class="form-control first_entry_calcuation" ></td>\n\
            //                                 <td><input type="text" id="tds_amount_entry" value="" onkeyup="number_validation(this);calculate_settlement_amount();" class="form-control first_entry_calcuation" ></td>\n\
            //                                 <td><input type="text" id="write_off_amount_entry" value="" onkeyup="number_validation(this);calculate_settlement_amount()" class="form-control first_entry_calcuation" ></td>\n\
            //                                 <td></td>\n\
            //                                 <td></td></tr>';
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    let id = res[i].id;
                    let total_settled_amnt = res[i].total_settled_amount;
                    let settled_amount = res[i].settled_amount;
                    let tds_amount = res[i].tds_amount;
                    let write_off_amount = res[i].write_off_amount;
                    let company = res[i].company;
                    let settled_date = res[i].settled_date;
                    let bill_head_id = res[i].bill_head_id;
                    settlstats = res[i].ins_settl_stauts;

                    if (total_settled_amnt > 0) {
                    response += '<tr id="insurance_head_dtls_' + id + '" style="cursor:pointer"><td>' + settled_date + '</td>\n\
                                            <td class="td_common_numeric_rules" id="total_settled_' + id + '" onclick="show_payment_mode(' + id + ',' + total_settled_amnt + ')">' + total_settled_amnt + '</td>\n\
                                            <td class="td_common_numeric_rules" id="settled_amount_' + id + '" onclick="show_payment_mode(' + id + ',' + total_settled_amnt + ')">' + settled_amount + '</td>\n\
                                            <td class="td_common_numeric_rules" id="tds_amount_' + id + '" onclick="show_payment_mode(' + id + ',' + total_settled_amnt + ')">' + tds_amount + '</td>\n\
                                            <td class="td_common_numeric_rules" id="write_off_amount_' + id + '" onclick="show_payment_mode(' + id + ',' + total_settled_amnt + ')">' + write_off_amount + '</td>\n\
                                            <td class="common_td_rules" onclick="show_payment_mode(' + id + ',' + total_settled_amnt + ')">' + company + '</td>\n\
                                            <td><i class="fa fa-trash-o delete_ins_head_entry"  onclick="delete_ins_head_entry(' + id + ',' + bill_head_id + ')"></i></td></tr>';
                }
                    j++;
                }
            } else {
                response += '<tr><td colspan="7" class="text-center"></td></tr>';
            }
            $('#insurance_list_modal_content').html(response);
            if (settlstats == 1) {
                $(".modal-footer").hide();
                $('.delete_ins_head_entry').hide();
            } else {
                $(".modal-footer").show();
                $('.delete_ins_head_entry').show();
            }
        calculate_settlement_amount();
            // setTimeout(function(){
            //     document.getElementById("settle_amount_entry").focus();
            // },2000);
        },
        complete: function () {
            $('#view_ins_spin_' + bill_head_id).removeClass('fa fa-spinner fa-spin');
            $('#view_ins_spin_' + bill_head_id).addClass('fa fa-eye');

            // $("#insurance_list_modal").modal('show');
            $("#insurance_list_modal").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
}
function delete_ins_head_entry(id, bill_head_id) {
    if (confirm("Are you sure you want to Delete ?")) {
        var bill_from = $("#view_modal_bill_from").val();
        var settled_amnt = $("#settled_amount_" + id).html();
        var tds_amnt = $("#tds_amount_" + id).html();
        var write_off_amnt = $("#write_off_amount_" + id).html();
        var url = $("#ins_base_url").val();
        url = url + "/master/delete_insurance_details";
        var params = {delete_insurance_list: 1, ins_head_id: id, bill_head_id: bill_head_id, bill_from: bill_from, settled_amnt: settled_amnt, tds_amnt: tds_amnt, write_off_amnt: write_off_amnt};
        $.ajax({
            type: "GET",
            url: url,
            data: params,
            beforeSend: function () {
            },
            success: function (data) {
                if (data == 1) {
                    is_edit = 1;
                    $('#insurance_head_dtls_' + id).remove();
                    toastr.success('Deleted Succcessfuly !!');
                }
            },
            complete: function () {

            }
        });
    }
}
$('#insurance_list_modal').on('hidden.bs.modal', function () {
    if (is_edit == 1) {
        var page = $('#table_insurance_settlememtList').attr('table_page_count');
        insurance_settlement_list(page);
    }
});
function show_payment_mode(id, total_settled_amnt) {
    $("#balance_payment_amount").val(total_settled_amnt)
    $("#payment_mode_details").show();
    $('#payment_list_modal_content').html('<i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="left: 50%;margin-left: 2em;margin-top: 1em;"></i><span class="sr-only">Loading...</span>');

    $("#total_settled_amnt_for_validation").val(total_settled_amnt);
    $("#ins_head_id_for_payment_mod").val(id);
    var paymentmod_search_vars = '';
    var bank_searchstring = '';
    var payment_mod = $('#cash_payment_mod_hidden').val();
    payment_mod = JSON.parse(payment_mod);
    var bank_hidden_mod = $('#bank_hidden').val();
    bank_hidden_mod = JSON.parse(bank_hidden_mod);

    var url = $("#ins_base_url").val();
    url = url + "/master/view_insurance_details";
    var params = {view_insurance_payment_mode: 1, ins_head_id: id};
    $.ajax({
        type: "GET",
        url: url,
        data: params,
        beforeSend: function () {
        },
        success: function (data) {
            $('#payment_list_modal_content').html('');
            let res = data;
            let response = '';
            var j = 1;
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {

                    let id = res[i].id;
                    let settled_amount = res[i].settled_amount;
                    let check_no = res[i].check_no;
                    let check_date = res[i].check_date;
                    let card_holder_name = res[i].card_holder_name;
                    let payment_mod_value = res[i].payment_mode;
                    let bank_id = res[i].bank;

                    $.each(payment_mod, function (key, val) {
                        var sel = '';
                        if (payment_mod_value == key) {
                            sel = 'selected';
                        }
                        paymentmod_search_vars += "<option " + sel + " value='" + key + "'>" + val + "</option>";
                    });
                    $.each(bank_hidden_mod, function (key, val) {
                        var sel = '';
                        if (bank_id == key) {
                            sel = 'selected';
                        }
                        bank_searchstring += "<option " + sel + " value='" + key + "'>" + val + "</option>";
                    });
                    response += '<tr id="insurance_payment_mode_dtls_' + id + '" ><td><select name="payment_mode_detail" class="form-control">' + paymentmod_search_vars + '</select></td>\n\
                                            <td id="payment_settled_' + id + '"><input type="text" autocomplete="off" value="' + settled_amount + '" name="payment_settled_amnt" class="form-control payment_calc" onkeyup="number_validation(this);calculate_payment_data(this,' + total_settled_amnt + ')"></td>\n\
                                            <td id="bank_' + id + '"><select name="bank_detail" class="form-control">' + bank_searchstring + '</select></td>\n\
                                            <td id="card_check_no_' + id + '"><input type="text" value="' + check_no + '" autocomplete="off" name="card_check_no" class="form-control"> </td>\n\
                                            <td id="check_date_' + id + '"><input type="text" value="' + check_date + '" autocomplete="off" name="check_date" onclick="sample_date_time();" class="form-control sample_date_time"></td>\n\
                                            <td id="card_holder_name_' + id + '"><input type="text" value="' + card_holder_name + '" autocomplete="off" name="card_holder_name" class="form-control"> </td>\n\
                                            <td><i class="fa fa-trash-o" style="cursor:pointer" id="payment_trash_btn_' + id + '" onclick="delete_payment_entry(' + id + ')"></i></td></tr>';
                    j++;

                }
                $('#payment_list_modal_content').html(response);
                $('#payment_list_modal_content').find(".sample_date_time").datetimepicker({
                    format: 'DD-MM-YYYY'
                });
            } else {
                add_ins_payment_row(total_settled_amnt);
            }
        },
        complete: function () {
            $("#payment_mode_details").show();
        }
    });
}
function add_ins_payment_row(total_settled_amnt = '') {
    var total_settled_amnt = $("#total_settled_amnt_for_validation").val();
    var show_balance_payment_amount = $("#show_balance_payment_amount_hidden").val();
    var add_balance = 0;
    if(show_balance_payment_amount == '0' || isNaN(show_balance_payment_amount)){
        show_balance_payment_amount = total_settled_amnt;
    }
    $('.payment_calc').each(function () {
         if (isNaN(parseFloat(this.value))) {
            var add_balance1 = 0;
        }else{
             var add_balance1 = parseFloat($(this).val());
        }
       add_balance = parseFloat(add_balance) + parseFloat(add_balance1);
    });
   var ttl_stled_final1 = (total_settled_amnt - add_balance);
    var ttl_stled_final = parseFloat(show_balance_payment_amount);
    var paymentmod_search_vars = '';
    var bank_searchstring = '';
    var payment_mod = $('#cash_payment_mod_hidden').val();
    payment_mod = JSON.parse(payment_mod);
    var bank_hidden_mod = $('#bank_hidden').val();
    bank_hidden_mod = JSON.parse(bank_hidden_mod);
    $.each(payment_mod, function (key, val) {
        paymentmod_search_vars += "<option value='" + key + "'>" + val + "</option>";
    });
    $.each(bank_hidden_mod, function (key, val) {
        bank_searchstring += "<option  value='" + key + "'>" + val + "</option>";
    });
    response = '<tr  ><td><select name="payment_mode_detail" class="form-control">' + paymentmod_search_vars + '</select></td>\n\
    <td ><input type="text" value="'+ttl_stled_final1+'" autocomplete="off" name="payment_settled_amnt" class="form-control payment_calc" onkeyup="number_validation(this);calculate_payment_data(this,' + total_settled_amnt + ')"  ></td>\n\
    <td ><select name="bank_detail" class="form-control">' + bank_searchstring + '</select></td>\n\
    <td><input type="text" value="" name="card_check_no" autocomplete="off" class="form-control"> </td>\n\
    <td ><input type="text" value="" name="check_date" autocomplete="off" class="form-control sample_date_time"></td>\n\
    <td ><input type="text" value="" name="card_holder_name" autocomplete="off" class="form-control"> </td>\n\
    <td><i class="fa fa-trash-o delete_payment_entry_added" style="cursor:pointer"></i></td></tr>';

    $('#payment_list_modal_content').append(response);
    $('#payment_list_modal_content').find(".sample_date_time").datetimepicker({
        format: 'DD-MM-YYYY'
    });
}
function save_ins_payment_row(data) {

    var file_token = $('#hidden_filetoken').val();
    if(data==0){
    var ins_head_id_for_payment_mod = $("#ins_head_id_for_payment_mod").val();
    }else{
        var ins_head_id_for_payment_mod =data;
    }
    var multi_select_list = new Array();

    var payment_mod = '';
    var amount_detail = '';
    var card_bankname = '';
    var cheque_date = '';
    var card_holder_name = '';
    var card_chequeno = '';

    $('#payment_list_modal_content tr').each(function () {
        if ($.type($(this).find('select[name="payment_mode_detail"]').val()) != "undefined") {
            payment_mod = $(this).find('select[name="payment_mode_detail"]').val();
        }
        if ($.type($(this).find('input[name="payment_settled_amnt"]').val()) != "undefined") {
            amount_detail = $(this).find('input[name="payment_settled_amnt"]').val();
        }
        if ($.type($(this).find('select[name="bank_detail"]').val()) != "undefined") {
            card_bankname = $(this).find('select[name="bank_detail"]').val();
        }
        if ($.type($(this).find('input[name="card_check_no"]').val()) != "undefined") {
            card_chequeno = $(this).find('input[name="card_check_no"]').val();
        }
        if ($.type($(this).find('input[name="check_date"]').val()) != "undefined") {
            cheque_date = $(this).find('input[name="check_date"]').val();
        }
        if ($.type($(this).find('input[name="card_holder_name"]').val()) != "undefined") {
            card_holder_name = $(this).find('input[name="card_holder_name"]').val();
        }


        multi_select_list.push({'payment_mod': payment_mod
            , 'amount_detail': amount_detail
            , 'cheque_date': cheque_date
            , 'card_chequeno': card_chequeno
            , 'card_holder_name': card_holder_name
            , 'card_bankname': card_bankname
        });
    });
    if(amount_detail !=''){
    var payment_data = multi_select_list;
    payment_data = JSON.stringify(payment_data);
    var param = {_token: file_token, payment_data: payment_data, insurancepayment_save: 1, ins_head_id_for_payment_mod: ins_head_id_for_payment_mod};
    var url = $("#ins_base_url").val();
    url = url + "/master/save_insurance_payment";
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#save_ins_payment_spin_btn').removeClass('fa fa-save');
            $('#save_ins_payment_spin_btn').addClass('fa fa-spinner fa-spin');

        },
        success: function (data) {
            if (data > 0) {
                is_edit = 1;
                toastr.success("Payment details Successfully Added");
            } else {
                toastr.error("Error occured Please check data");
            }
        },
        complete: function () {
            $('#save_ins_payment_spin_btn').removeClass('fa fa-spinner fa-spin');
            $('#save_ins_payment_spin_btn').addClass('fa fa-save');

        }
    });
    }else{
         toastr.warning("Payment Entry is not added");
    }
}
function sample_date_time() {
    $('.sample_date_time').datetimepicker({
        format: 'DD-MM-YYYY'
    });
}


function calculate_payment_data(e, total_payment_amnt) {
    var balance_payment_amount = $("#balance_payment_amount").val();
    var class_name = ($(e).attr('class').split(' '));
    class_name = class_name[1];
    var total_amnt = 0;
    $("." + class_name).each(function () {
        if (isNaN(parseFloat(this.value))) {
            var current_val = 0;
        } else {
            var current_val = parseFloat(this.value);
        }
        total_amnt = total_amnt + current_val;
        var balance = parseFloat(balance_payment_amount)-parseFloat(total_amnt);
        //$("#show_balance_payment_amount").html("Balance: "+balance);
        $("#show_balance_payment_amount_hidden").val(balance);
    });
    if (parseFloat(total_payment_amnt) < parseFloat(total_amnt)) {
        $(e).val('');
    }

}
function delete_payment_entry(id) {
    if (confirm("Are you sure you want to Delete ?")) {
        var file_token = $('#hidden_filetoken').val();
        var url = $("#ins_base_url").val();
        url = url + "/master/delete_insurance_payment";
        $.ajax({
            type: "POST",
            url: url,
            data: {_token: file_token, payment_id: id, deletet_payment: 1},
            beforeSend: function () {
                $('#payment_trash_btn_' + id).removeClass('fa fa-trash-o');
                $('#payment_trash_btn_' + id).addClass('fa fa-spinner fa-spin');

            },
            success: function (data) {
                if (data == 1) {
                    toastr.success("Successfully Deleted");
                    $("#insurance_payment_mode_dtls_" + id).remove();
                    is_edit = 1;
                } else {
                    toastr.error("Error occured Please check data");
                }
            },
            complete: function () {

            }
        });
    }
}
$("#payment_entry_table").on('click', '.delete_payment_entry_added', function () {
    $(this).closest('tr').remove();
});
function calculate_settlement_amount(){
    var total_settled = 0;

            var settle_amount = 0;
            var tds =  0;
            var write_off = 0;
            var pay_patient_adj_amount =   $("#pay_patient_adj_amount").val();
            var pay_insurance_adj_amount =   $("#pay_insurance_adj_amount").val();
            var pay_other_adj_amount =   $("#pay_other_adj_amount").val();
            var balance_label = $("#balance_lbl_txt").val();
            $("#balance_lbl_txt").val(balance_label);
            var claim_label = $("#claim_amount_entry").val();
            $("#claim_amount_entry").val(claim_label);
            var rec_amount = $("#balance_lbl_txt").val();
            var show_claim_amount = $("#show_claim_amount").val();
            if(settle_amount ==''){
                settle_amount = 0;
            }
            if(tds ==''){
                tds = 0;
            }
            if(write_off ==''){
                write_off = 0;
            }
            if(pay_patient_adj_amount ==''){
                pay_patient_adj_amount = 0;
            }
            if(pay_insurance_adj_amount ==''){
                pay_insurance_adj_amount = 0;
            }
            if(pay_other_adj_amount ==''){
                pay_other_adj_amount = 0;
            }
        total_settled = parseFloat(settle_amount)+parseFloat(tds)+parseFloat(write_off)+parseFloat(pay_patient_adj_amount)+parseFloat(pay_insurance_adj_amount)+parseFloat(pay_other_adj_amount);
        // $("#balance_lbl").html(rec_amount);

        if(show_claim_amount == 1){
            var calculated_balance_label = parseFloat(rec_amount)-parseFloat(total_settled);
        }else{
            var calculated_balance_label = parseFloat(balance_label)-parseFloat(total_settled);
        }
        //$("#balance_lbl").html('');
        $("#balance_lbl").html(calculated_balance_label);
        show_payment_mode(0,total_settled);
}
function checkAdvance(e){
    if($(e).attr('id') == 'pay_patient_adj_amount'){
       var adv_val =  $("#pat_adv").html();
       var cur_val = $(e).val();
       if(parseFloat(adv_val) < parseFloat(cur_val)){
           $(e).val('');
       }
    }else if($(e).attr('id') == 'pay_insurance_adj_amount'){
       var adv_val =  $("#pat_ins_adv").html();
       var cur_val = $(e).val();
       if(parseFloat(adv_val) < parseFloat(cur_val)){
           $(e).val('');
       }
    }
}

function checkAdvancetxt(e) {
    var adv_val =  $(e).attr('ins_advance');
    var cur_val = $(e).val();
    if(parseFloat(adv_val) < parseFloat(cur_val)){
        $(e).val('');
    }
}

function save_settlement_detail(obj, cunt)
{
    var row = $(obj).closest("tr");
    var bill_from = $("#bill_from_" + cunt).val();
    var bill_head_id = $("#bill_head_id_" + cunt).val();
    var visit_id = $("#visit_id_" + cunt).val();
    var company_id = $("#company_id_" + cunt).val();
    var rec_amnt = $("#rec_amnt_" + cunt).val();
    var bill_tag = $("#bill_tag" + cunt).val();
    var settle_amount = row.find(".settle_amount_entry").val();
    var tds_amount = row.find(".tds_amount_entry").val();
    var write_off_amount = row.find(".write_off_amount_entry").val();
    var pay_insurance_adj = row.find(".pay_insurance_adj_amount").val();
    var claim_amount = row.find(".claim_amount_entry").val();

    var url = $("#ins_base_url").val();
    url = url + "/master/save_insurance";
    var file_token = $('#hidden_filetoken').val();

    var param = {_token: file_token, save_ins: 1, bill_from: bill_from, company_id: company_id, rec_amnt: rec_amnt,visit_id: visit_id, bill_head_id: bill_head_id, settle_amount: settle_amount,
        tds_amount: tds_amount, write_off_amount: write_off_amount, claim_amount : claim_amount, pay_insurance_adj:pay_insurance_adj,bill_tag_ins :bill_tag,sett_type:1 };

    if (settle_amount == '' && tds_amount == '' && write_off_amount == '' && pay_insurance_adj== '') {
        toastr.error("Amount Not entered");
        return false;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
           $(obj).find('i').removeClass('fa fa-save').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data> 0) {
                toastr.success("Successfully Added");
            } else {
                toastr.error("Error occured Please check data");
            }
        },
        complete: function () {
            $(obj).find('i').removeClass('fa fa-spinner fa-spin').addClass('fa fa-save');
            var page = $('#table_insurance_settlememtList').attr('table_page_count');
            insurance_settlement_list(page);
        }
    });
}

function insurance_settlement_list(page =1)
{
    var op_no_search = $("#op_no_search").val();
    var op_no_search_hidden = $("#op_no_search_hidden").val();
    var bill_no = $("#bill_no").val();
    var company = $("#company").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var ip_no_search = $("#ip_no_search").val();
    var ip_no_search_hidden = $("#ip_no_search_hidden").val();
    var settlement_status = $("#settlement_status").is(':checked') ? 1 : 0;
    var file_token = $('#hidden_filetoken').val();

    var param = {_token: file_token,
        op_no_search: op_no_search,
        op_no_search_hidden: op_no_search_hidden,
        bill_no: bill_no,
        company: company,
        from_date: from_date,
        to_date: to_date,
        ip_no_search: ip_no_search,
        ip_no_search_hidden: ip_no_search_hidden,
        settlement_status:settlement_status,
        page: page
    };
   $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
           $('#insurance_settlement_list').find('i').removeClass('fa fa-search').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#insurence_settlement_list').html(data);
        },
        complete: function () {
            $('#insurance_settlement_list').find('i').removeClass('fa fa-spinner fa-spin').addClass('fa fa-search');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            $('#table_insurance_settlememtList').attr('table_page_count', page);
        }
    });
}


function calculate_settlement_amount_txt(obj, cunt) {

    setTimeout(function () {
        var total_settled = 0;
        var row = $(obj).closest("tr");
        var settle_amount = row.find(".settle_amount_entry").val();
        var tds =  row.find(".tds_amount_entry").val();
        var write_off = row.find(".write_off_amount_entry").val();
        var pay_insurance_adj_amount = row.find(".pay_insurance_adj_amount").val();
        var claim_amount_entry = row.find(".claim_amount_entry").val();
        var pay_patient_adj_amount = 0;
        var pay_other_adj_amount = 0;
        var rec_amount = $("#rec_amnt_" + cunt).val();
        var show_claim_amount = $("#show_claim_amount").val();

        if (settle_amount =='') {
            settle_amount = 0;
        }
        if (tds =='') {
            tds = 0;
        }
        if (write_off == '') {
            write_off = 0;
        }
        if (pay_patient_adj_amount =='') {
            pay_patient_adj_amount = 0;
        }
        if (pay_insurance_adj_amount =='') {
            pay_insurance_adj_amount = 0;
        }
        if (pay_other_adj_amount =='') {
            pay_other_adj_amount = 0;
        }
        total_settled = parseFloat(settle_amount)+parseFloat(tds)+parseFloat(write_off)+parseFloat(pay_patient_adj_amount)+parseFloat(pay_insurance_adj_amount)+parseFloat(pay_other_adj_amount);

        // if (show_claim_amount == 1) {
        //     var calculated_balance_label = parseFloat(claim_amount_entry)-parseFloat(total_settled);
        // } else {
        var calculated_balance_label = parseFloat(rec_amount)-parseFloat(total_settled);
        if (isNaN(parseFloat(calculated_balance_label))) {
            calculated_balance_label = rec_amount;
        }
        row.find(".rec_amnt_td").html(calculated_balance_label.toFixed(2));
    }, 800);
}
function save_claim_amount(obj, cunt)
{
    var row = $(obj).closest("tr");
    var bill_from = $("#bill_from_" + cunt).val();
    var bill_head_id = $("#bill_head_id_" + cunt).val();
    var visit_id = $("#visit_id_" + cunt).val();
    var bill_tag = $("#bill_tag" + cunt).val();
    var claim_amount = row.find(".claim_amount_entry").val();

    var url = $("#ins_base_url").val();
    url = url + "/master/save_insurance_claim";
    var file_token = $('#hidden_filetoken').val();

    var param = {_token: file_token, save_claim: 1, bill_from: bill_from, visit_id: visit_id, bill_head_id: bill_head_id,
         claim_amount : claim_amount,bill_tag_ins :bill_tag,sett_type:1 };

    if (claim_amount == '') {
        toastr.error("Amount Not entered");
        return false;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
           $(obj).find('i').removeClass('fa fa-save').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data> 0) {
                toastr.success("Successfully Added");
            } else {
                toastr.error("Error occured Please check data");
            }
            var net_amount = $(".net_amount_" + cunt).html();
            var claim_amount = $("#claim_amount_entry" + cunt).val();
            var calculated_balance_label = parseFloat(net_amount)-parseFloat(claim_amount);
            // if (isNaN(parseFloat(calculated_balance_label))) {
            //     calculated_balance_label = calculated_balance_label;
            // }
            $(".rec_amnt_new_"+cunt).html(calculated_balance_label.toFixed(2));
        },
        complete: function () {
            $(obj).find('i').removeClass('fa fa-spinner fa-spin').addClass('fa fa-save');
            var page = $('#table_insurance_settlememtList').attr('table_page_count');
            insurance_settlement_list(page);
        }
    });
}
