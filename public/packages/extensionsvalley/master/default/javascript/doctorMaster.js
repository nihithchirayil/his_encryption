$(document).ready(function() {
    var token = $("#token_hiddendata").val();
    $('#consultancy_duration').datetimepicker({
        format: 'mm:ss',
    });
    $('#slot_duration').datetimepicker({
        format: 'mm:ss',
    });


});


function isEmail(Email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(Email);

}

var ip;

function getIp() {
    $.getJSON("https://api.ipify.org/?format=json", function() {
        ip = ip;
    });

    return ip;



}

// $('.hidden_search').keyup(function(event) {
//     var input_id = '';
//     var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
//     var value = event.key; //get the charcode and convert to char
//     input_id = $(this).attr('id');
//     //alert(input_id);return;
//     var current;
//     if (value.match(keycheck) || event.keyCode == '8') {
//         if ($('#' + input_id + '_hidden').val() != "") {
//             $('#' + input_id + '_hidden').val('');
//         }
//         var search_key = $(this).val();
//         search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
//         search_key = search_key.trim();



//         if (search_key == "") {
//             $("#" + input_id + "AjaxDiv").html("");
//         } else {
//             var base_url = $('#base_url').val();
//             var url = base_url + "/master/ajaxdoctormaster";
//             console.log(base_url);
//             $.ajax({
//                 type: "POST",
//                 url: url,
//                 data: 'search_key=' + search_key + '&search_key_id=' + input_id,
//                 beforeSend: function() {

//                     $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
//                 },
//                 success: function(html) {
//                     if (html == 0) {
//                         $("#" + input_id + "AjaxDiv").html("No results found!").show();
//                         $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
//                     } else {
//                         $("#" + input_id + "AjaxDiv").html(html).show();
//                         $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');

//                     }
//                 },
//                 complete: function() {
//                     //  $('#loading_image').hide();

//                 },
//                 error: function() {
//                     Command: toastr["error"]("Network Error!");
//                     return;
//                 }

//             });
//         }
//     } else {
//         // ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
//     }
// });

// function fillSearchDetials(id, name, serach_key_id) {
//     $('#' + serach_key_id + '_hidden').val(id);
//     $('#' + serach_key_id).val(name);
//     $('#' + serach_key_id).attr('title', name);
//     $("#" + serach_key_id + "AjaxDiv").hide();
//     // doctorSearchList();

// }

// /* setting for enter key press in ajaxDiv listing */
// $(".hidden_search").on('keydown', function(event) {
//     var input_id = '';
//     input_id = $(this).attr('id');
//     if (event.keyCode === 13) {
//         ajaxListKeyEnter(input_id + 'AjaxDiv');
//         return false;

//     }
// });

function doctorSearchList(doc_id) {

    var base_url = $("#base_url").val();
    var doctorName = doc_id;
    var token = $("#token_hiddendata").val();
    var param = { _token: token, doctorName: doctorName, };

    var url = base_url + "/master/doctorslist";

    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchBtn").attr("disabled", true);
            $("#searchSpin").removeClass("fa fa-search");
            $("#searchSpin").addClass("fa fa-spinner fa-spin");
            $('#searchDataDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function(data) {

            $("#searchDataDiv").html(data);
            $('#searchDataDiv').LoadingOverlay("hide");


        },
        complete: function() {
            $("#searchBtn").attr("disabled", false);
            $("#searchSpin").removeClass("fa fa-spinner fa-spin");
            $("#searchSpin").addClass("fa fa-search");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}




function saveItem() {


    var ip = getIp();



    var id = $('#edit_id').val();
    var doctor_name = $('#docter_name').val();
    if (doctor_name == ' ') {
        toastr.warning("Please Enter Doctor Name");
        return;
    }
    var phone = $('#phone_number').val();
    if (phone == ' ') {
        toastr.warning("Please Enter Phone Number ");
        return;
    }
    var Email = $('#Email').val();
    var emailName = isEmail(Email);
    var email = $.trim(emailName);
    if (email == false) {
        toastr.warning("Please Enter Valid Email Address");
        return;
    }
    var address = $('#address').val();
    var status = $('#status').val();
    var qualification = $('#qualification').val();
    var Speciality = $('#Speciality').val();



    if (Speciality == null) {
        toastr.warning('Please Select Speciality');

        return;
    }
    var license_contry = $('#license_contry').val();
    var license_state = $('#license_state').val();

    var license_no = $('#license_no').val();
    if (license_no == ' ') {
        license_no = 0;
    }

    var opRoomNo = $('#opRoomNo').val();
    if (opRoomNo == ' ') {
        opRoomNo = 0;
    }
    var screeningRoomNO = $('#screeningRoomNO').val();

    if (screeningRoomNO == ' ') {
        screeningRoomNO = 0;
    }
    var firstconsultationitem = $('#firstconsultationitem').val();
    // if (firstconsultationitem == ' ') {
    //     firstconsultationitem =

    //     toastr.warning('Please Enter First Consultation Item');
    //     return;

    // }
    var subseequent_consultation_item = $('#subseequent_consultation_item').val();
    // if (subseequent_consultation_item == ' ') {
    //     toastr.warning('Please Enter First Subseequent Consultation Item');
    //     return;
    // }


    var free_followup_day = $('#free_followup_day').val();
    if (free_followup_day == ' ') {
        free_followup_day = 0;
    }
    var free_followup_no = $('#free_followup_no').val();
    if (free_followup_no == ' ') {
        free_followup_no = 0;
    }
    var same_specl_followup_days = $('#same_specl_followup_days').val();
    if (same_specl_followup_days == 0) {
        same_specl_followup_days = 0;
    }
    var same_specl_followup_no = $('#same_specl_followup_no').val();
    if (same_specl_followup_no == ' ') {
        same_specl_followup_no = 0;
    }
    var consultancy_duration = $('#consultancy_duration').val();

    var gender = $('#gender').val();
    var defualt_op_phar = $('#defualt_op_phar').val();
    var prescription_dep = $('#prescription_dep').val();
    var medication_note = $('#medication_note').val();
    var medication_foot = $('#medication_foot').val();
    var slot_duration = $('#slot_duration').val();
    var token_prefix = $('#token_prefix').val();
    var default_landing = $('#default_landing').val();
    var appointments_use = $('#appointments_only').is(":checked");
    if (appointments_use == false) {
        appointments_only = 0;
    }
    if (appointments_use == true) {
        appointments_only = 1;
    }
    var appointments = $('#hide_appointments').is(":checked");
    if (appointments == true) {
        hide_appointments = 1;
    }
    if (appointments == false) {
        hide_appointments = 0;
    }

    var er = $('#is_er').is(':checked');
    if (er == true) {
        is_er = 1;
    }
    if (er == false) {
        is_er = 0;
    }
    var casuality = $('#is_casuality').is(':checked');
    if (casuality == true) {
        is_casuality = 1;
    }
    if (casuality == false) {
        is_casuality = 0;
    }
    var has_op_right = $('#has_op_right').is(':checked');
    var has_ip_rights = $('#has_ip_rights').is(':checked');
    var free_followup_for_same = $('#free_followup_for_same').is(':checked');

    var mlc = $('#is_mlc').is(':checked');
    if (mlc == true) {
        is_mlc = 1;
    }
    if (mlc == false) {
        is_mlc = 0;
    }

    show_queue_management = 0;
    if ( $('#show_queue_management').is(':checked') == true) {
        show_queue_management = 1;
    }
    var username = $('#username').val();
    if (username == '') {
        toastr.warning('Please Enter User Name');
        return;
    }
    var password = $('#password').val();
    var default_landing_page = $('#default_landing_page').val();

    var base_url = $("#base_url").val();

    var url = base_url + "/master/saveitem";
    var token = $("#token_hiddendata").val();




    param = {
        _token: token,
        id: id,
        doctor_name: doctor_name,
        phone: phone,
        Email: Email,
        address: address,
        qualification: qualification,
        Speciality: Speciality,
        license_contry: license_contry,
        license_state: license_state,
        license_no: license_no,
        status: status,
        opRoomNo: opRoomNo,
        screeningRoomNO: screeningRoomNO,
        firstconsultationitem: firstconsultationitem,
        subseequent_consultation_item: subseequent_consultation_item,
        free_followup_day: free_followup_day,
        free_followup_no: free_followup_no,
        same_specl_followup_days: same_specl_followup_days,
        same_specl_followup_no: same_specl_followup_no,
        consultancy_duration: consultancy_duration,
        gender: gender,
        defualt_op_phar: defualt_op_phar,
        prescription_dep: prescription_dep,
        medication_note: medication_note,
        medication_foot: medication_foot,
        slot_duration: slot_duration,
        token_prefix: token_prefix,
        default_landing: default_landing,
        appointments_only: appointments_only,
        hide_appointments: hide_appointments,
        is_er: is_er,
        is_casuality: is_casuality,
        has_op_right: has_op_right,
        has_ip_rights: has_ip_rights,
        free_followup_for_same: free_followup_for_same,
        is_mlc: is_mlc,
        show_queue_management: show_queue_management,
        username: username,
        password: password,
        default_landing_page: default_landing_page,
        ip: ip

    }


    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchBtn").attr("disabled", true);
            $("#searchSpin").removeClass("fa fa-search");
            $("#searchSpin").addClass("fa fa-spinner fa-spin");
            $('#doc_modal').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function(data) {
            if (data == 1) {
                toastr.error('Allready Exist')
            } else if (data == 2) {
                toastr.error('User Name Already Taken')
            } else {
                $("#searchDataDiv").html(data);
                toastr.success('Details added successfully');
                $('#addDoctorDetails').modal('hide');
                doctorSearchList(id);
                $('status').val();

            }




        },
        complete: function() {
            $("#searchBtn").attr("disabled", false);
            $("#searchSpin").removeClass("fa fa-spinner fa-spin");
            $("#searchSpin").addClass("fa fa-search");

            $('#doc_modal').LoadingOverlay("hide");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },
    });
}


function editItem(element)

{

    var id = $(element).parents('tr').attr('data-id');
    var doctor_name = $(element).parents('tr').attr('data-name');
    var phone = $(element).parents('tr').attr('data-phone');
    var Email = $(element).parents('tr').attr('data-email');
    var address = $(element).parents('tr').attr('data-address');
    var status = $(element).parents('tr').attr('data-status');
    var qualification = $(element).parents('tr').attr('data-qualification');
    var Speciality = $(element).parents('tr').attr('data-speciality');
    var license_contry = $(element).parents('tr').attr('data-license_country');
    var license_state = $(element).parents('tr').attr('data-license_state');
    var license_no = $(element).parents('tr').attr('data-license_no');
    var opRoomNo = $(element).parents('tr').attr('data-op_room_no');
    var screeningRoomNO = $(element).parents('tr').attr('data-screening_room_no');
    var firstconsultationitem = $(element).parents('tr').attr('data-first_consultation_item');
    var subseequent_consultation_item = $(element).parents('tr').attr('data-subsequent_consultation_item');
    var free_followup_day = $(element).parents('tr').attr('data-free_followup_days');
    var free_followup_no = $(element).parents('tr').attr('data-free_followup_no');
    var same_specl_followup_days = $(element).parents('tr').attr('data-same_speciality_followup_days');
    var same_specl_followup_no = $(element).parents('tr').attr('data-same_speciality_followup_no');
    var consultancy_duration = $(element).parents('tr').attr('data-consultation_duration');
    var gender = $(element).parents('tr').attr('data-gender');
    var defualt_op_phar = $(element).parents('tr').attr('data-default_op_pharmacy');
    var prescription_dep = $(element).parents('tr').attr('data-prescription_department_head');
    var medication_note = $(element).parents('tr').attr('data-medication_note');
    var medication_foot = $(element).parents('tr').attr('data-medication_footer_text');
    var slot_duration = $(element).parents('tr').attr('data-slot_duration');
    var token_prefix = $(element).parents('tr').attr('data-token_prefix');
    var default_landing_page = $(element).parents('tr').attr('data-default_landing_page');
    var appointments_only = $(element).parents('tr').attr('data-hide_from_appointments');
    var hide_appointments = $(element).parents('tr').attr('data-hide_from_appointments');
    var is_er = $(element).parents('tr').attr('data-is_er');
    var is_casuality = $(element).parents('tr').attr('data-is_casualty');
    var has_op_right = $(element).parents('tr').attr('data-has_op_rights');
    var has_ip_rights = $(element).parents('tr').attr('data-has_ip_rights');
    var free_followup_for_same = $(element).parents('tr').attr('data-free_followup_for_same_speciality');
    var is_mlc = $(element).parents('tr').attr('data-is_mlc');
    var show_queue_management = $(element).parents('tr').attr('data-show_queue_management');
    var username = $(element).parents('tr').attr('data-username');
    var default_landing = $(element).parents('tr').attr('data-default_landing');



    $('#docter_name').val(doctor_name);
    $('#phone_number').val(phone);
    $('#Email').val(Email);
    $('#address').val(address);
    $('#status').val(status);
    $('#qualification').val(qualification);
    $('#Speciality').val(Speciality);
    $('#license_contry').val(license_contry);
    $('#license_state').val(license_state);
    $('#license_no').val(license_no);
    $('#screeningRoomNO').val(screeningRoomNO);
    $('#firstconsultationitem').val(firstconsultationitem);
    $('#subseequent_consultation_item').val(subseequent_consultation_item);
    $('#free_followup_day').val(free_followup_day);
    $('#free_followup_no').val(free_followup_no);
    $('#same_specl_followup_days').val(same_specl_followup_days);
    $('#same_specl_followup_no').val(same_specl_followup_no);
    $('#consultancy_duration').val(consultancy_duration);
    $('#gender').val(gender);
    $('#defualt_op_phar').val(defualt_op_phar);
    $('#prescription_dep').val(prescription_dep);
    $('#medication_note').val(medication_note);
    $('#medication_foot').val(medication_foot);
    $('#slot_duration').val(slot_duration);
    $('#token_prefix').val(token_prefix);
    $('#default_landing').val(default_landing_page);
    $('#opRoomNo').val(opRoomNo);
    $('#username').val(username);
    $('#password').val('');
    $('#default_landing_page').val(default_landing);
    $('#edit_id').val(id);
    if (appointments_only == 1) {
        $('#appointments_only').prop('checked', true);
    } else {
        $('#appointments_only').prop('checked', false);
    }
    if (hide_appointments == 1) {
        $('#hide_appointments').prop('checked', true);
    } else {
        $('#hide_appointments').prop('checked', false);
    }
    if (is_er == 1) {
        $('#is_er').prop('checked', true);
    } else {
        $('#is_er').prop('checked', false);
    }
    if (is_casuality == 1) {
        $('#is_casuality').prop('checked', true);
    } else {
        $('#is_casuality').prop('checked', false);
    }
    if (has_op_right == 1) {
        $('#has_op_right').prop('checked', true);
    } else {
        $('#has_op_right').prop('checked', false);
    }
    if (has_ip_rights == 1) {
        $('#has_ip_rights').prop('checked', true);
    } else {
        $('#has_ip_rights').prop('checked', false);
    }
    if (free_followup_for_same == 1) {
        $('#free_followup_for_same').prop('checked', true);
    } else {
        $('#free_followup_for_same').prop('checked', false);
    }
    if (is_mlc == 1) {
        $('#is_mlc').prop('checked', true);
    } else {
        $('#is_mlc').prop('checked', false);
    }
    if (show_queue_management == 1) {
        $('#show_queue_management').prop('checked', true);
    } else {
        $('#show_queue_management').prop('checked', false);
    }

}

function resetmodal() {
    $('#edit_id').val(' ');
    $('#docter_name').val(' ');
    $('#phone_number').val(' ');
    $('#Email').val(' ');
    $('#address').val(' ');
    $('#status').val(1);
    $('#qualification').val(' ');
    $('#Speciality').val(0);
    $('#license_contry').val(' ');
    $('#license_state').val(' ');
    $('#license_no').val(' ');
    $('#screeningRoomNO').val(' ');
    $('#firstconsultationitem').val(" ");
    $('#subseequent_consultation_item').val(" ");
    $('#free_followup_day').val(' ');
    $('#free_followup_no').val(' ');
    $('#same_specl_followup_days').val(' ');
    $('#same_specl_followup_no').val(' ');
    $('#consultancy_duration').val(' ');
    $('#gender').val(' ');
    $('#defualt_op_phar').val(' ');
    $('#prescription_dep').val(' ');
    $('#medication_note').val(' ');
    $('#medication_foot').val(' ');
    $('#slot_duration').val(' ');
    $('#token_prefix').val(' ');
    $('#default_landing').val(' ');
    $('#opRoomNo').val(' ');
    $('#edit_id').val(' ');
    $('#appointments_only').val(' ');
    $('#appointments_only').prop('checked', false);
    $('#hide_appointments').val(' ');
    $('#hide_appointments').prop('checked', false);
    $('#is_er').val(' ');
    $('#is_er').prop('checked', false);
    $('#is_mlc').val(' ');
    $('#is_casuality').val(' ');
    $('#has_op_right').val(' ');
    $('#has_ip_rights').val(' ');
    $('#free_followup_for_same').val(' ');
    $('#is_casuality').prop('checked', false);
    $('#has_op_right').prop('checked', false);
    $('#has_ip_rights').prop('checked', false);
    $('#free_followup_for_same').prop('checked', false);
    $('#is_mlc').prop('checked', false);
    $('#show_queue_management').prop('checked', false);
    $('#username').val('');
    $('#password').val('');
    $('#default_landing_page').val('');


}

function activateDoctor(id) {
    var token = $("#token_hiddendata").val();
    var base_url = $("#base_url").val();
    var active = $('#active_doc').is(':checked') ? 1 : 0;
    var url = base_url + '/master/deleteitem'
    params = { _token: token, id: id, active: active }
    $.ajax({
        type: 'POST',
        url: url,
        data: params,
        beforeSend: function() {
            $('#searchDataDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        },
        success: function(data) {
            if (data) {
                if (active == 1) {
                    toastr.success('Doctor status activated.')
                    $('#doctor_name' + id).css('color', '#809f58');
                    $('#addDoctorDetails').on('show.bs.modal', function() {
                        $('#status').val(1);
                    });
                } else if (active == 0) {
                    toastr.success('Doctor status inactivated.')
                    $('#doctor_name' + id).css('color', '#036603');

                    $('#addDoctorDetails').on('show.bs.modal', function() {
                        $('#status').val(0);
                    });
                }
            }
        },
        complete: function() {
            $('#searchDataDiv').LoadingOverlay("hide");



        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        },

    });





}
