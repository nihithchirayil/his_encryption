var limit = 15;
var offset = 0;
var total_rec = 0;
var init_status = 0;
var debounceTimer;
var receive_mrd = {};

$(document).ready(function () {
    var interval;
    $('.form-control').blur(function () {
        setTimeout(function () {
            $('.ajaxSearchBox').hide();
        }, 500);
    });
    setInterval(function () {
        var timer = $('.js-timeout').html();
        timer = timer.split(':');
        var minutes = timer[0];
        var seconds = timer[1];
        seconds -= 1;
        if (minutes < 0) return;
        else if (seconds < 0 && minutes != 0) {
            minutes -= 1;
            seconds = 59;
        } else if (seconds < 10 && length.seconds != 2) {
            seconds = '0' + seconds
        };
        $('.js-timeout').html(minutes + ':' + seconds);
        if (minutes == 0 && seconds == 0) {
            $('.js-timeout').html('0:00');
            searchBill(15, 0, 1, 2);
            $('.js-timeout').html('2:00');
        }
    }, 1000);

    $('#js-startTimer').click(function () {
        $('.js-timeout').text("2:00");
        countdown();
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    searchBill();
    $('#recevie_dispatch_text').html(' Dispatch');
    $('#receiveDispathAllRecordsBtn').attr('data-name', 'Dispatch');
    $('#receiveDispathAllRecordsBtn').attr('data-status', 0);
});


$('#op_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var base_url = $('#base_url').val();
    if ((value.match(keycheck) || event.keyCode == '8') && (event.keyCode != '40' && event.keyCode != '38' && event
            .keyCode != '13' && event.keyCode != '37' && event.keyCode != '39')) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv").html("").hide();
            $("#op_no_search_hidden").val("");
        } else {
            clearTimeout(debounceTimer);
            debounceTimer = setTimeout(function () {
                try {
                    var param = {
                        op_no_search: op_no_search,
                        op_no_search_prog: 1
                    };
                    $.ajax({
                        type: "GET",
                        url: '',
                        data: param,
                        beforeSend: function () {
                            $("#op_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                        },
                        success: function (html) {
                            if (html == 'No Result Found') {
                                toLocationPub = '';
                            }
                            $("#op_no_searchCodeAjaxDiv").html(html).show();
                            $("#op_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                        },
                        complete: function () {
                            if (op_search == "") {
                                $("#op_no_searchCodeAjaxDiv").html("").hide();
                                $("#op_no_search_hidden").val("");
                            }
                        }
                    });
                } catch (err) {
                    document.getElementById("demo").innerHTML = err.message;
                }
            }.bind(this), 300);
        }
    } else {
        ajax_list_key_down('op_no_searchCodeAjaxDiv', event);
    }
});


$('#op_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv');
        return false;
    }
});

$('#txt_seach_scan').on('keydown', function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        var search_uhid = $(this).val();
        var obj = this;
        var mrd_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");
        if (search_uhid) {
            $.ajax({
                type: "POST",
                url: $('#base_url').val() + "/master/updateScanPatientMrdTrack",
                data: 'search_uhid=' + search_uhid,
                beforeSend: function () {
                    $('body').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.6)",
                        imageColor: '#337AB7'
                    });
                },
                success: function (datas) {
                    var data = JSON.parse(datas);
                    if (data.success == '1') {
                        if (parseInt(data.status) == 1) {
                            toastr.info(data.message);
                        } else if (parseInt(data.status) == 2) {
                            toastr.success(data.message);
                        } else {
                            toastr.warning(data.message);
                        }
                    } else {
                        toastr.error(data.message);
                    }
                },
                complete: function () {
                    $('body').LoadingOverlay("hide");
                    $(obj).val('');
                    searchBill();
                }
            });
        }
    }
});

function checkReceiveDispathMrd(mrd_id, patient_id) {
    var check_status = $("#receiveAllData" + mrd_id).is(":checked");
    if (check_status) {
        receive_mrd[parseInt(mrd_id)] = parseInt(patient_id)
    } else {
        delete receive_mrd[parseInt(mrd_id)];
    }
    var array_len = Object.keys(receive_mrd).length;
    if (parseInt(array_len) != 0) {
        $('#receiveDispathAllRecordsBtn').show();
    } else {
        $('#receiveDispathAllRecordsBtn').hide();
    }
}

function receiveDispathAllRecords() {
    var array_len = Object.keys(receive_mrd).length;
    var btn_name = $('#receiveDispathAllRecordsBtn').attr('data-name');
    var mrd_status = $('#receiveDispathAllRecordsBtn').attr('data-status');
    if (parseInt(array_len) != 0) {
        bootbox.confirm({
            message: "Are you sure,you want to " + btn_name + " " + array_len + " records?",
            buttons: {
                'confirm': {
                    label: "Continue",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var receive_mrd_string = JSON.stringify(receive_mrd);
                    $.ajax({
                        type: "POST",
                        url: $('#base_url').val() + "/master/receiveDispathAllRecords",
                        data: {
                            mrd_status: mrd_status,
                            receive_mrd_string: receive_mrd_string
                        },
                        beforeSend: function () {
                            $('#receiveDispathAllRecordsBtn').attr('disabled', true);
                            $('#receiveDispathAllRecordsSpin').removeClass('fa fa-check');
                            $('#receiveDispathAllRecordsSpin').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            if (parseInt(data.status) == 1) {
                                toastr.success(data.message);
                                receive_mrd = {};
                                searchBill();
                                $('#receiveDispathAllRecordsBtn').hide();
                            } else if (parseInt(data.status) == 4) {
                                toastr.warning(data.message);
                                $('#error_dispatchdatamodelDiv').html(data.htmldata);
                                $("#error_dispatchdatamodel").modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            } else {
                                toastr.warning(data.message);
                            }
                        },
                        complete: function () {
                            $('#receiveDispathAllRecordsBtn').attr('disabled', false);
                            $('#receiveDispathAllRecordsSpin').removeClass('fa fa-spinner fa-spin');
                            $('#receiveDispathAllRecordsSpin').addClass('fa fa-check');
                        },
                        error: function () {
                            Command: toastr["error"]("Network Error!");
                            return;
                        }
                    });
                }
            }
        });

    } else {
        toastr.warning("Please select any file");
    }
}

function fillSearchDetials(patient_id, uhid, patient_name) {
    $('#op_no_search_hidden').val(patient_id);
    $('#op_no_search').val(patient_name + '[' + uhid + ']');
    $('#op_no_searchCodeAjaxDiv').hide();
    $('#searchdatabtn').trigger('click');
}

$('#old_uhid_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var base_url = $('#base_url').val();
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var old_uhid_search = $(this).val();
        op_search = old_uhid_search.trim();
        if (op_search == "") {
            $("#old_uhid_searchCodeAjaxDiv").html("");
            $("#old_uhid_search_hidden").val("");
        } else {
            try {
                var url = '';
                var param = {
                    old_uhid_search: old_uhid_search,
                    old_uhid_search_prog: 1
                };
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#old_uhid_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#old_uhid_searchCodeAjaxDiv").html(html).show();
                        $("#old_uhid_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {}
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajax_list_key_down('old_uhid_searchCodeAjaxDiv', event);
    }
});

$('#old_uhid_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('old_uhid_searchCodeAjaxDiv');
        return false;
    }
});

function fillOlduhidSearchDetials(ext_uhid, patient_name) {
    $('#old_uhid_search_hidden').val(ext_uhid);
    $('#old_uhid_search').val(patient_name + '[' + ext_uhid + ']');
    $('#old_uhid_searchCodeAjaxDiv').hide();
    $('#searchdatabtn').trigger('click');
}

$('#searchdatabtn').click(function () {
    offset = 0;
    limit = 15;
    $('#generilistdiv').html("");
    $('#Outlistdiv').html("");
    $('#TodayRecivedlistdiv').html("");
});


function searchBill(limit = 15, offset = 0, init_status = 0, resetReceiceData = 1) {
    if ($('.nav-tabs').find('li.active').find('a').attr("data-attr-id") == 'instore') {
        $('#print_results').removeClass('disabled');
    } else {
        $('#print_results').addClass('disabled');
    }
    var url = $('#base_url').val() + "/master/getMrdTrack";
    var op_no_search_hidden = $('#op_no_search_hidden').val();
    var op_no_search = $('#op_no_search').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var status_update = -1;
    var doctor = $("#doctor").val();
    var shift = $("#shift").val();
    var mrd_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");
    var old_uhid = $('#old_uhid_search_hidden').val();
    if (parseInt(resetReceiceData) != 2) {
        receive_mrd = {};
        $('#receiveDispathAllRecordsBtn').hide();
    }
    var receive_mrd_string = JSON.stringify(receive_mrd);
    $('.ajaxSearchBox').hide();
    var param = {
        shift: shift,
        op_no_search_hidden: op_no_search_hidden,
        doctor: doctor,
        op_no_search: op_no_search,
        from_date: from_date,
        to_date: to_date,
        status_update: status_update,
        limit: limit,
        offset: offset,
        mrd_type: mrd_type,
        old_uhid: old_uhid,
        receive_mrd_string: receive_mrd_string
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner');
            if (mrd_type == 'instore') {
                $('#generilistdiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            } else if (mrd_type == 'instore') {
                $('#Outlistdiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            } else {
                $('#TodayRecivedlistdiv').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            }
        },
        success: function (msg) {
            var obj = JSON.parse(msg);
            msg = obj.viewData;
            total_rec = obj.total_rec;
            if (offset == 0) {
                if (mrd_type == 'instore') {
                    $('#generilistdiv').html(msg);
                } else if (mrd_type == 'outstore') {
                    $('#Outlistdiv').html(msg);
                } else {
                    $('#TodayRecivedlistdiv').html(msg);
                }
            } else {
                if (mrd_type == 'instore') {
                    $('#offsetDetails').html(msg);
                    var mrn = $('#morningLoad').html();
                    var eve = $('#eveningLoad').html();
                    $('#load_data2').find('.table tbody').append(mrn);
                    $('#load_data1').find('.table tbody').append(eve);
                } else if (mrd_type == 'outstore') {
                    $('#load_data3').find('.table tbody').append(msg);
                } else {
                    $('#load_data4').find('.table tbody').append(msg);

                }

            }

            if (mrd_type == 'instore') {
                $('#instorecount').html(obj.total_rec);
            } else if (mrd_type == 'outstore') {
                $('#receivedispatchcount').html(obj.total_rec);
            } else {
                $('#receivedcount').html(obj.total_rec);
            }
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner');
            $('#searchdataspin').addClass('fa fa-search');
            $('#generilistdiv').LoadingOverlay("hide");
            $('#Outlistdiv').LoadingOverlay("hide");
            $('#TodayRecivedlistdiv').LoadingOverlay("hide");
            $("#doctor").val(doctor).select2();

        },
        error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

function updateMrdTrack(id, status, patient_id, from_type = 0) {
    var remarks = $("#remarks_" + id).val();
    $.ajax({
        type: "POST",
        url: $('#base_url').val() + "/master/updateMrdTrack",
        data: 'mrd_id=' + id + '&mrd_status=' + status + '&remarks=' + remarks + '&patient_id=' + patient_id + '&from_type=' + from_type,
        beforeSend: function () {
            $('#generilistdiv').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (datas) {
            var data = JSON.parse(datas);
            if (data.success == '1') {
                toastr.success(data.message);
                if (from_type == 1) {
                    $("#dispatchModal").modal('toggle');
                }
                refresh(2);
            } else if (data.success == '4') {
                $("#dispatchModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#receive_all_document_btn').attr('onclick', 'updateMrdTrack(' + id + ',' + status + ',' + patient_id + ',1)');
                $("#dispatch_modal").html(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        complete: function () {
            $('#generilistdiv').LoadingOverlay("hide");
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);
        }
    });
}

$('.nav-tabs').find('li').find('a').on('click', function () {
    receive_mrd = {};
    var nav_tabs = $(this).attr('data-attr-id');
    $('#receiveDispathAllRecordsBtn').hide();
    if (nav_tabs == 'outstore') {
        $('#recevie_dispatch_text').html(' Receive')
        $('#receiveDispathAllRecordsBtn').attr('data-name', 'Receive');
        $('#receiveDispathAllRecordsBtn').attr('data-status', 1);
    } else if (nav_tabs == 'todays_received') {
        $('#recevie_dispatch_text').html(' Dispatch');
        $('#receiveDispathAllRecordsBtn').attr('data-name', 'Dispatch');
        $('#receiveDispathAllRecordsBtn').attr('data-status', 0);
    } else if (nav_tabs == 'instore') {
        $('#recevie_dispatch_text').html(' Dispatch');
        $('#receiveDispathAllRecordsBtn').attr('data-name', 'Dispatch');
        $('#receiveDispathAllRecordsBtn').attr('data-status', 0);
    }
    setTimeout(function () {
        offset = 0;
        searchBill();
    }, 500);
});

var index; // cell index
var toggleBool; // sorting asc, desc
function sorting(tbody, index) {

    this.index = index;
    if (toggleBool) {
        toggleBool = false;
    } else {
        toggleBool = true;
    }

    var datas = new Array();
    var tbodyLength = tbody.rows.length;
    for (var i = 0; i < tbodyLength; i++) {
        datas[i] = tbody.rows[i];
    }

    // sort by cell[index]
    datas.sort(compareCells);
    for (var i = 0; i < tbody.rows.length; i++) {
        // rearrange table rows by sorted rows
        tbody.appendChild(datas[i]);
    }
}

function compareCells(a, b) {
    var aVal = a.cells[index].innerText;
    var bVal = b.cells[index].innerText;
    aVal = aVal.replace(/\,/g, '');
    bVal = bVal.replace(/\,/g, '');
    if (toggleBool) {
        var temp = aVal;
        aVal = bVal;
        bVal = temp;
    }
    if (aVal.match(/^[0-9]+$/) && bVal.match(/^[0-9]+$/)) {
        return parseFloat(aVal) - parseFloat(bVal);
    } else {
        if (aVal < bVal) {
            return -1;
        } else if (aVal > bVal) {
            return 1;
        } else {
            return 0;
        }
    }
}

function refresh(from_type = 1) {
    if (parseInt(from_type) != 2) {
        var current_date = $('#current_date').val();
        $('#shift').val(0);
        $('#shift').select2();
        $('#doctor').val(-1);
        $('#doctor').select2();
        $('#from_date').val(current_date);
        $('#to_date').val(current_date);
        $('#old_uhid_search').val('');
        $('#old_uhid_search_hidden').val('');
    } else {
        $('#op_no_search').focus();
    }
    $('#op_no_search').val('');
    $('#op_no_search_hidden').val('');
    searchBill(15, 0, 1);
    receive_mrd = {};
    $('#receiveDispathAllRecordsBtn').hide();
}

function fetchPrintDetails() {

    if ($('.nav-tabs').find('li.active').find('a').attr("data-attr-id") == 'instore') {
        var op_no_search_hidden = $('#op_no_search_hidden').val();
        var op_no_search = $('#op_no_search').val();
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var status_update = -1;
        var doctor = $("#doctor").val();
        var shift = $("#shift").val();
        var mrd_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");
        var old_uhid = $('#old_uhid_search_hidden').val();
        var is_checked = $('#is_checked').is(':checked') ? 1 : 0;
        $('.ajaxSearchBox').hide();

        var param = {
            is_checked: is_checked,
            shift: shift,
            op_no_search_hidden: op_no_search_hidden,
            doctor: doctor,
            op_no_search: op_no_search,
            from_date: from_date,
            to_date: to_date,
            status_update: status_update,
            limit: limit,
            offset: offset,
            mrd_type: mrd_type,
            old_uhid: old_uhid
        };
        $.ajax({
            type: "POST",
            url: $('#base_url').val() + "/master/fetchPrintDetails",
            data: param,
            beforeSend: function () {
                $('#print_results').attr('disabled', true);
                $('#print_spin').removeClass('fa fa-print');
                $('#print_spin').addClass('fa fa-spinner fa-spin');
            },
            success: function (html) {

                if (html) {
                    $('#print_hidden').html(html);
                    $('#print_results').removeClass('disabled');
                    searchBill(15, 0);
                    print_generate('result_container_div');

                }

            },
            complete: function () {
                $('#print_results').attr('disabled', false);
                $('#print_spin').removeClass('fa fa-spinner fa-spin');
                $('#print_spin').addClass('fa fa-print');
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            }

        });
    }


}

function print_generate(printData) {
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    printhead = $("#hospital_header").val();
    // showw = showw + atob(printhead);
    var msglist = document.getElementById(printData);
    showw = msglist.innerHTML;
    mywindow.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;}</style>');
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    //mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    }, 1000);
    // mywindow.print()
    // mywindow.close();
    return true;
}

function getVisitDetails(id, patient_id, visit_time) {
    $.ajax({
        type: "POST",
        url: $('#base_url').val() + "/master/getVisitDetails",
        data: {
            patient_id: patient_id,
            visit_time: visit_time,
            mrd_id: id
        },
        beforeSend: function () {
            $('#visit_date_btn' + id).attr('disabled', true);
            $('#visit_date_spin' + id).removeClass('fa fa-info');
            $('#visit_date_spin' + id).addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {

            if (html) {
                $('#todayVisitHistory_data').html(html);
                $('#todayVisitHistory').modal('show');
            }

        },
        complete: function () {
            $('#visit_date_btn' + id).attr('disabled', false);
            $('#visit_date_spin' + id).removeClass('fa fa-spinner fa-spin');
            $('#visit_date_spin' + id).addClass('fa fa-info');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}


function getMrdDoctorList(mrd_id, patient_id) {
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var pat_name = $('#patient_name_list' + mrd_id).html();
    $.ajax({
        type: "POST",
        url: $('#base_url').val() + "/master/getMrdDoctorList",
        data: {
            patient_id: patient_id,
            from_date: from_date,
            to_date: to_date
        },
        beforeSend: function () {
            $('#morevist_doctorlistheader').html(pat_name);
            $('#getMrdDoctorListBtn' + mrd_id).attr('disabled', true);
            $('#getMrdDoctorListSpin' + mrd_id).removeClass('fa fa-user-md');
            $('#getMrdDoctorListSpin' + mrd_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            $('#morevist_doctorlistdiv').html(html);
            $("#morevist_doctorlist").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#getMrdDoctorListBtn' + mrd_id).attr('disabled', false);
            $('#getMrdDoctorListSpin' + mrd_id).removeClass('fa fa-spinner fa-spin');
            $('#getMrdDoctorListSpin' + mrd_id).addClass('fa fa-user-md');
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}
