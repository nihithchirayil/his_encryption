$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    base_url = $('#base_url').val();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
});
base_url = '';



function updateAccountPosting() {
    var post_type = $("#post_type").val();
    var servieDate = $("#servieDate").val();
    if (post_type) {
        if (servieDate) {
            bootbox.confirm({
                message: "Are you sure you want to Update",
                buttons: {
                    'confirm': {
                        label: 'Update',
                        className: 'btn-primary',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-warning'
                    }
                },
                callback: function (result) {
                    if (result) {

                        var url = base_url + "/master/SaveAccountsPosting";
                        var param = { post_type: post_type, servieDate: servieDate };
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: param,
                            beforeSend: function () {
                                $('#updateAccountPostingBtn').attr('disabled', true);
                                $('#updateAccountPostingspin').removeClass('fa fa-save');
                                $('#updateAccountPostingspin').addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                            if(data == 3){
                                toastr.error("Data exist,Please delete from finance");
                                }else{
                                toastr.success("Successfully Updated");
                            }
                            },
                            complete: function () {
                                $('#updateAccountPostingBtn').attr('disabled', false);
                                $('#updateAccountPostingspin').removeClass('fa fa-spinner fa-spin');
                                $('#updateAccountPostingspin').addClass('fa fa-save');
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });


                    }
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
    } else {
        toastr.warning("Please Select Post Type");
        $("#post_type").focus();
    }
}

