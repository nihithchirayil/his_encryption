$(document).ready(function () {
    $(document).on("click", function (event) {
        var $trigger = $(".ajaxSearchBox");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".ajaxSearchBox").hide();
        }
    });
    var current_date = $('#current_date').val();
    $('.patient_dob').datetimepicker({
        format: 'MMM-DD-YYYY',
        maxDate: current_date
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});
$(document).on("click", function (event) {
    closeAjaxSearch(event);
});
$(document).keydown(function (event) {
    if (event.keyCode == 27) {
        closeAjaxSearch(event);
    }
});

function closeAjaxSearch(event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
}
var base_url = $('#base_url').val();
var blade_id = $('#blade_id').val();
var token = $('#token').val();
var block_appointment = new Array();
var unblock_appointment = new Array();

function checkPatientStatus() {
    var status = $('input[name=patient_status]:checked').val();
    var flag = 0;
    $("#is_video_consultation").prop("checked", false);
    if (status == '1') {
        var patient_id = $('#patient_id_hidden').val();
        if (patient_id) {
            flag = 1;
        } else {
            $('#patient_uhid_status').hide();
            $('#renewal_free_div').hide();
            resetAppointmentForm(1, 5, 0);
        }

    } else {
        var patient_name = $('#patient_name').val();
        if (patient_name) {
            flag = 2;
        } else {
            $('#patient_uhid_status').show();
            if ($('#is_reg_free')) {
                $('#is_reg_free').prop('checked', false);
            }
            $('#renewal_free_div').show();
            $('#patient_uhid').focus();
            resetAppointmentForm(2, 5, 0);
        }

    }
    if (flag != 0) {
        bootbox.confirm({
            message: "Changes done would lost.  Are you sure to continue ?",
            buttons: {
                confirm: {
                    label: "Yes",
                    className: "btn-warning",
                    default: "true",
                },
                cancel: {
                    label: "No",
                    className: "btn-danger",
                },
            },
            callback: function (result) {
                if (result) {
                    resetAppointmentForm(flag, 5, 0);
                } else {
                    if (flag == 1) {
                        $('#has_uhid').prop('checked', true);
                        $('#patient_uhid_status').show();
                        $('#patient_uhid').focus();
                        $('#renewal_free_div').show();
                        if ($('#is_reg_free')) {
                            $('#is_reg_free').prop('checked', false);
                        }
                    } else {
                        $('#new_patient').prop('checked', true);
                        $('#patient_uhid_status').hide();
                        $('#renewal_free_div').hide();
                    }
                }
            }

        });
    }

}




//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url = $('#base_url').val();
            var url = base_url + "/patient_register/ajaxsearch_speciality_doctor";
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }

});

function fillSearchDetials_booking(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    // $('#' + serach_key_id).val(speciality);
    $('#' + serach_key_id).attr('title', name);
    // $('#' + serach_key_id).attr('title', speciality);
    $("#" + serach_key_id + "AjaxDiv").hide();
    getDoctorSlots();
}
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
function searchAppointmentData() {
    var value = $('#booked_patient_search').val();
    if (value) {
        value = value.trim().toLowerCase();
    }
    $("#doctor_slots_data_body tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value.trim()) > -1)
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    setTimeout(function () {
        $('.theadfix_wrapper').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
    }, 400);
}

// $("#patient_doctor").on('change', function() {
//     var data = {};

//     data.speciality = $('#speciality').val();
//     data.doctor = $('#patient_doctor').val();
//     var load = $('#warning1').val();

//     if (speciality == '') {
//       var speciality = 'a';

//     }

//  if(speciality) {


//         $.ajax({
//                     type: "GET",
//                     url: '',
//                     data:data,
//                     beforeSend: function () {

//                         $('#s2id_speciality').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
//                         $( "#speciality" ).prop( "disabled", true ); //Disable
//                      },
//                     success: function (html) {

//                                 if(html){
//                     //      var obj=JSON.parse(html);
//                     //    console.log(obj);

//                                        $("#patient_doctor").empty();

//                                         $("#speciality").html('<option value="">Select Doctor</option>');
//                                         $.each(html,function(key,value){
//                                           $("#patient_doctor").append('<option value="'+key+'">'+value+'</option>');
//                                         });
//                                         $('#patient_doctor').select2();

//                                 }else{
//                                         $("#patient_doctor").empty();
//                                 }

//                     },

//                     error: function () {
//                         Command: toastr["error"]("Please check your internet connection and try again!");
//                         $( "#patient_doctor" ).prop( "disabled", false );
//                     },
//                     complete: function () {
//                         $('#warning1').remove();
//                         $( "#patient_doctor" ).prop( "disabled", false );
//                     }

//                 });

//         }
//     else {
//                 $('#speciality').focus();
//                 $("#patient_doctor").empty();
//             }

//     });
//     $("#location").on('click', function() {
//         $('#warning2').remove();
//     });
function addAppoinment(token, from_time, schedule_id) {
    $('.addAppoinment').removeClass('bg-blue');
    $('#addAppoinment' + token).addClass('bg-blue');
    var appointment_edit_status = $('#appointment_edit_status').val();
    if ((blade_id == '2' && appointment_edit_status == '0') || blade_id == '1') {
        $('#slot_start_time').val(from_time);
        $('#patient_token').val(token);
        $('#session_id').val(schedule_id);
    }
}
function getAppoinmentData() {
    var data = {};
    data.doctor = $("#patient_doctor").val();
    console.log(data.doctor);
    data.speciality = $("#speciality").val();
    var url = base_url + "/patient_register/getAppoinmentData";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
        },
        success: function (data) {

        },
        complete: function () {

        }
    });


}

function getDoctorSlots(from_type = 0) {
    var patient_status = $('input[name=patient_status]:checked').val();
    var visit_date = $('#patient_visit_date').val();
    var doctor_id = $('#patient_doctor_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    var patient_name = $('#patient_name').val();
    var patient_phone = $('#patient_phone').val();

    if (from_type != '1') {
        $('#slot_start_time').val('');
        $('#patient_token').val('');
        $('#session_id').val('');
    }
    var current_date = $('#current_date').val();
    if (visit_date == current_date) {
        $('#getbooking_allslots').attr('disabled', false);
    } else {
        $('#getbooking_allslots').attr('disabled', true);
        $('#getbooking_allslots').prop('checked', true);
    }
    var allslots = $('#getbooking_allslots').is(":checked");
    if (visit_date) {
        if (doctor_id) {
            var url = base_url + "/patient_register/getDoctorSlots";
            var param = { _token: token, blade_id: blade_id, patient_name: patient_name, patient_phone: patient_phone, visit_date: visit_date, patient_status: patient_status, patient_id: patient_id, doctor_id: doctor_id, allslots: allslots };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    $("#doctor_appointmentslot_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                    $('#searchAppointmentBtn').attr('disabled', true);
                    $('#searchAppointmentSpin').removeClass('fa fa-list');
                    $('#searchAppointmentSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    var obj = JSON.parse(data);
                    $('#doctor_appointmentslot_div').html(obj.result);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                    $('#doctor_charges_hidden').html(obj.netamount);
                    $('#charges_hidden').val(obj.netamount);
                    if (from_type != '1') {
                        if (parseInt(obj.appointment_count) != 0) {
                            $('#getDoctorChagesModelDiv').html(obj.appointment_list);
                            $('#getDoctorChagesModelHeader').html('Appointment List');
                            $("#getDoctorChagesModel").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                    }
                },
                complete: function () {
                    $("#patientappoinment_div").LoadingOverlay("hide");
                    $("#doctor_appointmentslot_div").LoadingOverlay("hide");
                    searchAppointmentData();
                    $('#searchAppointmentBtn').attr('disabled', false);
                    $('#searchAppointmentSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchAppointmentSpin').addClass('fa fa-list');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            $('#doctor_appointmentslot_div').html('');
            var string = "<div class='col-md-12 padding_sm text-center red' style='margin-top: 150px'><h3> Please select doctor </h3> </div>";
            $('#doctor_appointmentslot_div').html(string);
        }
    } else {
        $('#patient_visit_date').focus();
        toastr.warning('Please select visit date');
    }
}

function resetAppointmentForm(from_type = 1, post_type = 1, btn_click = 0) {
    if (from_type == '1') {
        $('#new_patient').prop('checked', true);
        $('#patient_uhid_status').hide();
        $('#renewal_free_div').hide();
    } else {
        $('#has_uhid').prop('checked', true);
        $('#patient_uhid_status').show();
        $('#patient_uhid').focus();
        $('#renewal_free_div').show();
    }

    if (btn_click == 1) {
        var current_date = $('#current_date').val();
        $('#patient_visit_date').val(current_date);
    }
    if ($('#is_reg_free')) {
        $('#is_reg_free').prop('checked', false);
    }

    $('#slot_start_time').val('');
    $('#patient_id_hidden').val('');
    $('#patient_token').val('');
    $('#session_id').val('');
    $('#appointment_id').val('');
    $('#patient_uhid').val('');
    $('#patient_uhid_AjaxDiv').hide();
    $('#patientsalutation').val('').select2();
    $('#patient_name').val('');
    $('#patient_dob').val('');
    $('#patient_gender').val('').select2();
    $('#patientagein').val('1').select2();
    $('#patient_phone').val('');
    $('#alternate_phone').val('');
    $('#patient_email').val('');
    $('#patient_address').val('');
    $('#age_years').val('');
    $('#age_month').val('');
    $('#age_days').val('');
    $('#patient_age').val('');
    $('#appointment_edit_status').val(0);
    $('#dis_prov').prop('checked', false);
    $('#discount_type').val('');
    $('.discount_value').val('');
    $('#discount_narration').val('');
    $('#discount_pro').fadeOut();
    $('#dis_amt').fadeOut();
    $('#doctor_charges_hidden').html('');
    $('#split_hidden').val('');
    $('#pt_profile_url').val('');
    $('.pt_profile_body').html('');
    if (blade_id == '2') {
        var company_country = $('#company_country').val();
        $('#patient_vulnerable').val('').select2();
        $('#patient_pincode').val('');
        $('#patient_remarks').val('');
        $('#pricing_company').val('');
        $('#company_pricing').val('');
        $('#patient_default_area').val('');
        $('#commpany_id_hidden').val('');
        $('#pricing_id_hidden').val('');
        $('#patientGeneralPricingBtn').trigger('click');
        $('#appointment_title').html('Patient Registration');
        $('#patient_country').val(company_country).select2();
        getPatientAddress(1)
        $('#patient_state').val('').select2();
        $('#patient_district').val('').select2();
        $('#patient_area').val('').select2();
        $('#add_patient_area').val('');
        $('#add_area').attr('checked', false);
        $('#patient_pincode').val('');
        insurance_array = new Array();
        patient_more_details = new Array();
        $('#progressbar').html('');
        $('#details_tab_body').html('');
        insurance_count = new Array();
        insurance_details = new Array();
    } else if (blade_id == '1') {
        $('#appointment_title').html('Add New Appointment');
    }
    if (post_type != '5') {
        getDoctorSlots(0);
    }
}

function editAppointment(appointment_id, token, from_time, schedule_id, from_type, doctor_id = 0) {
    var url = base_url + "/patient_register/editAppointment";
    var param = { appointment_id: appointment_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            if ((from_type == '1' || from_type == '3' || from_type == '5') && blade_id == '1') {
                $('#appointment_title').html('Edit Appointment');
                $("#doctor_appointmentslot_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            } else if (from_type == '2' && blade_id == '1') {
                $('#appointment_title').html('Copy Appointment');
                $('#cloneAppointmentIdBtn' + appointment_id).attr('disabled', true);
                $('#cloneAppointmentIdSpin' + appointment_id).removeClass('fa fa-clone');
                $('#cloneAppointmentIdSpin' + appointment_id).addClass('fa fa-spinner fa-spin');
            } else if (blade_id == '1') {
                $('#appointment_title').html('Add New Appointment');
            } else if (blade_id == '2') {
                $('#appointment_title').html('Patient Registration');
            }
            if (from_type == '5') {
                $('#patient_phone_AjaxDiv').hide();
                $('#patient_alternate_phone_AjaxDiv').hide();
            } else if (from_type == '1') {
                $('#appointment_edit_status').val(1);
            }
        },
        success: function (data) {
            var josn_deatils = JSON.parse(data);
            var obj = josn_deatils.result;
            console.log(obj);
            var pricing_id_get = josn_deatils.pricing_id;
            var patient_id = obj[0].patient_id ? obj[0].patient_id : 0;
            if (patient_id && patient_id != '0') {
                $('#has_uhid').prop('checked', true);
                $('#patient_uhid_status').show();
                $('#renewal_free_div').show();
                if ($('#is_reg_free')) {
                    $('#is_reg_free').prop('checked', false);
                }
                $('#patient_uhid').focus();
            } else {
                $('#new_patient').prop('checked', true);
                $('#patient_uhid_status').hide();
                $('#renewal_free_div').hide();
            }
            if (from_type == '1' || from_type == '3') {
                $('#appointment_id').val(appointment_id);
                $('#slot_start_time').val(from_time);
            } else if (from_type == '2' || from_type == '5') {
                $('#appointment_id').val('');
            }
            if (blade_id == '2') {
                var pin_code = obj[0].pincode ? obj[0].pincode : '';
                if (pin_code) {
                    $('#patient_pincode').val(pin_code);
                    getPostalCodes(obj[0].area);
                } else {
                    $('#patient_pincode').val('');
                    $('#add_area').prop('checked', true);
                    $('#add_patient_area').val(obj[0].area ? obj[0].area : '');
                    patientAreaStatus();
                }
                var pricing_id = obj[0].pricing_id ? obj[0].pricing_id : 0;
                if (parseInt(pricing_id) != 0) {
                    $('#default_pricing_id_edit').val(pricing_id);
                    getPricingDetalis('default_pricing_id_edit');
                } else {
                    getPricingDetalis('default_pricing_id');
                }
                $('#patient_default_area').val(obj[0].area ? obj[0].area : '');

                insurance_array = new Array();
                patient_more_details = new Array();
                insurance_array.push({
                    'primary_insurance_company': obj[0].insurance_company ? obj[0].insurance_company : 0,
                    'primary_insurance_pricing': obj[0].insurance_pricing ? obj[0].insurance_pricing : 0,
                    'primary_authorisation_letter_no': obj[0].authorisation_letter_no ? obj[0].authorisation_letter_no : '',
                    'primary_insurance_id_no': obj[0].insurance_id_no ? obj[0].insurance_id_no : '',
                    'primary_credit_limit': obj[0].credit_limit ? obj[0].credit_limit : '',
                    'primary_card_type': obj[0].card_type ? obj[0].card_type : '',
                    'primary_color_of_card': obj[0].color_of_card ? obj[0].color_of_card : '',
                    'primary_issue_date': obj[0].issue_date ? obj[0].issue_date : '',
                    'primary_expiry_date': obj[0].expiry_date ? obj[0].expiry_date : '',
                    'primary_dependent_relation': obj[0].dependent_relation ? obj[0].dependent_relation : '',
                });

                patient_more_details.push({
                    'patient_blood_grp': obj[0].blood_group ? obj[0].blood_group : '',
                    'patient_MaritalStatus': obj[0].marital_status ? obj[0].marital_status : '',
                    'patient_relation': obj[0].reference_type ? obj[0].reference_type : '',
                    'patient_relationname': obj[0].reference_remarks ? obj[0].reference_remarks : '',
                    'patient_idproof_type': obj[0].id_proof_type ? obj[0].id_proof_type : '',
                    'vip_patient_comments': obj[0].vip_comments ? obj[0].vip_comments : '',
                    'patient_passport_no': obj[0].passport_no ? obj[0].passport_no : '',
                    'patient_passport_expiry': obj[0].passport_expiry ? obj[0].passport_expiry : '',
                    'patient_organ_donation': obj[0].organ_donation ? obj[0].organ_donation : '',
                });
            }
            if (from_type != '5') {
                $('#patient_visit_date').val(obj[0].booking_date ? obj[0].booking_date : '');
                $('#patient_doctor_hidden').val(obj[0].doctor_id ? obj[0].doctor_id : '');
                $('#patient_doctor').val(obj[0].doctor_name ? obj[0].doctor_name : '');
            }
            $('#patient_id_hidden').val(patient_id);
            $('#patient_token').val(token);
            $('#session_id').val(schedule_id);
            $('#patient_uhid').val(obj[0].uhid ? obj[0].uhid : '');
            $('#patient_uhid_AjaxDiv').hide();
            $('#patientsalutation').val(obj[0].salutation ? obj[0].salutation : '').select2();
            $('#patient_name').val(obj[0].patient_name ? obj[0].patient_name : '');
            $('#getpatientdob').prop('checked', false);
            $('#patient_dob').val(obj[0].dob ? obj[0].dob : '');
            $('#patient_gender').val(obj[0].gender ? obj[0].gender : '').select2();
            $('#patient_phone').val(obj[0].mobile_no ? obj[0].mobile_no.trim() : '');
            $('#alternate_phone').val(obj[0].alternate_mobile_no ? obj[0].alternate_mobile_no.trim() : '');
            $('#patient_email').val(obj[0].email ? obj[0].email : '');
            $('#patient_address').val(obj[0].address ? obj[0].address : '');

            var is_video_consultation = obj[0].is_video_consultation ? obj[0].is_video_consultation : 0;
            if (is_video_consultation == 1) {
                $("#is_video_consultation").prop("checked", true);
            }

            var patient_doctor = $('#patient_doctor').val();
            if (patient_doctor || patient_doctor != 'All') {
                getDoctorChageSplitUp(pricing_id_get);
            }
        },
        complete: function () {
            if (from_type == '1' || from_type == '3' || from_type == '5') {
                $("#doctor_appointmentslot_div").LoadingOverlay("hide");
            } else if (from_type == '2') {
                $('#cloneAppointmentIdBtn' + appointment_id).attr('disabled', false);
                $('#cloneAppointmentIdSpin' + appointment_id).removeClass('fa fa-spinner fa-spin');
                $('#cloneAppointmentIdSpin' + appointment_id).addClass('fa fa-clone');
            }
            if (from_type == '3') {
                $("#getDoctorChagesModel").modal('toggle');
                if (doctor_id != 0) {
                    $('#patient_doctor').val(doctor_id).select2();
                    getDoctorSlots(1);
                }
            }
            getAge();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function cancelAppointment(appointment_id) {
    bootbox.confirm({
        message: "Are you sure you want to cancel this appointment ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-warning",
                default: "true",
            },
            cancel: {
                label: "No",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                var url = base_url + "/patient_register/cancelAppointment";
                var param = { appointment_id: appointment_id };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#cancelAppointmentIdBtn' + appointment_id).attr('disabled', true);
                        $('#cancelAppointmentIdSpin' + appointment_id).removeClass('fa fa-trash');
                        $('#cancelAppointmentIdSpin' + appointment_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data == 'Paid') {
                            token.warning("Paied appointment can not be cancelled");
                        } else {
                            toastr.success('Appointment Cancelled Successfully');
                            getDoctorSlots(0);
                        }
                    },
                    complete: function () {
                        $('#cancelAppointmentIdBtn' + appointment_id).attr('disabled', false);
                        $('#cancelAppointmentIdSpin' + appointment_id).removeClass('fa fa-spinner fa-spin');
                        $('#cancelAppointmentIdSpin' + appointment_id).addClass('fa fa-trash');
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        },
    });
}

function createPatientBooking() {
    var location_code = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
    if (location_code && location_code != 0) {
        var appointment_id = $('#appointment_id').val();
        var string = "Are you sure, you want to book this appointment ?";
        if (appointment_id) {
            string = "Are you sure you want to edit this appointment ?";
        }

        var url = base_url + "/patient_register/createPatientBooking";
        var patient_id = $('#patient_id_hidden').val();
        var visit_date = $('#patient_visit_date').val();
        var start_time = $('#slot_start_time').val();
        var doctor = $('#patient_doctor_hidden').val();
        var uhid = $('#patient_uhid').val();
        var patient_name = $('#patient_name').val();
        var patient_dob = $('#patient_dob').val();
        var patient_gender = $('#patient_gender').val();
        var patient_phone = $('#patient_phone').val();
        var patient_email = $('#patient_email').val();
        var patient_address = $('#patient_address').val();
        var patient_salutation = $('#patientsalutation').val();
        var patient_token = $('#patient_token').val();
        var session_id = $('#session_id').val();
        var flag = 1;
        var doctor_name = $("#patient_doctor").val();
        var branch_id = localStorage.getItem('branch_id') ? parseInt(localStorage.getItem('branch_id')) : 0;
        // var phone_flag = checkMobileNo();
        var phone_flag = mobileNumberNormalValidation();

        var status = $('input[name=patient_status]:checked').val();
        if (status == '2') {
            if (!patient_id) {
                toastr.warning('Please select any Patient');
                $('#patient_uhid').focus();
                return false;
            }
        }
        if (!doctor || doctor == 'All') {
            toastr.warning('Please select soctor');
            var string = "<div class='col-md-12 padding_sm text-center red' style='margin-top: 150px'><h3> Please select doctor </h3> </div>";
            $('#doctor_appointmentslot_div').html(string);
            flag = 0;
            return false;
        } else if (!start_time) {
            toastr.warning('Please select any Slot');
            flag = 0;
            return false;
        } else if (!visit_date) {
            toastr.warning('Please select any Appointment Date');
            flag = 0;
            return false;
        } else if (!patient_salutation || patient_salutation == 'All') {
            toastr.warning('Please select any Salutation');
            flag = 0;
            return false;
        } else if (!patient_name) {
            toastr.warning('Please select any Patient Name');
            $('#patient_name').focus();
            flag = 0;
            return false;
        } else if (!patient_dob) {
            toastr.warning('Please Enter Patient DOB');
            $('#patient_dob').focus();
            flag = 0;
            return false;
        } else if (!phone_flag) {
            $('#patient_phone').focus();
            flag = 0;
            return false;
        } else if (!patient_gender || patient_gender == 'All') {
            toastr.warning('Please select any Gender');
            flag = 0;
            return false;
        } else if (patient_email) {
            var email_check = isEmail();
            if (!email_check) {
                flag = 0;
                return false;
            }
        }

        var is_video_consultation = 0;
        if ($('#is_video_consultation').is(":checked")) {
            is_video_consultation = 1;
        }

        var param = {
            _token: token,
            patient_id: patient_id,
            visit_date: visit_date,
            start_time: start_time,
            doctor: doctor,
            uhid: uhid,
            patient_name: patient_name,
            patient_dob: patient_dob,
            branch_id: branch_id,
            patient_gender: patient_gender,
            patient_phone: patient_phone,
            patient_email: patient_email,
            patient_address: patient_address,
            patient_salutation: patient_salutation,
            patient_token: patient_token,
            session_id: session_id,
            is_video_consultation: is_video_consultation,
            appointment_id: appointment_id
        };
        if (flag == 1) {
            bootbox.confirm({
                message: string,
                buttons: {
                    confirm: {
                        label: 'Save',
                        className: 'btn-success',
                        default: "true",
                    },
                    cancel: {
                        label: "Cancel",
                        className: "btn-danger",
                    },
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: param,
                            beforeSend: function () {
                                $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                                $("#doctor_appointmentslot_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                                $('#createPatientBookingBtn').attr('disabled', true);
                                $('#createPatientBookingSpin').removeClass('fa fa-save');
                                $('#createPatientBookingSpin').addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                var obj = JSON.parse(data);
                                if (obj.status == '1') {
                                    var appointment_id = $('#appointment_id').val();
                                    if (appointment_id) {
                                        toastr.success("Appointment update successfully");
                                    } else {
                                        toastr.success("Appointment created");
                                    }
                                    $("#is_video_consultation").prop("checked", false);
                                    confirmPatientRequest(uhid, patient_name, obj.prefix + patient_token, visit_date, doctor_name, 'Booking', 1, 0, start_time, 0);
                                } else {
                                    toastr.warning(obj.message);
                                }
                            },
                            complete: function () {
                                $("#patientappoinment_div").LoadingOverlay("hide");
                                $("#doctor_appointmentslot_div").LoadingOverlay("hide");
                                $('#createPatientBookingBtn').attr('disabled', false);
                                $('#createPatientBookingSpin').removeClass('fa fa-spinner fa-spin');
                                $('#createPatientBookingSpin').addClass('fa fa-save');
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });
                    }
                }


            });
        }
    } else {
        toastr.warning("Please select any location");
    }
}

function selectBlockSlot(token, from_time, to_time, schedule_id) {
    var block_status = $('#blockAppoinment' + token).is(":checked");
    if (block_status) {
        block_appointment[token] = {
            from_time: from_time,
            to_time: to_time,
            schedule_id: schedule_id
        }
    } else {
        delete block_appointment[token];
    }
}

function selectUnblockSlot(token, from_time, to_time, schedule_id) {
    var block_status = $('#unblockAppoinment' + token).is(":checked");
    if (block_status) {
        unblock_appointment[token] = {
            from_time: from_time,
            to_time: to_time,
            schedule_id: schedule_id
        }
    } else {
        delete unblock_appointment[token];
    }
}


function blockAppointment() {
    var blocked_len = Object.keys(block_appointment).length;
    var unblocked_len = Object.keys(unblock_appointment).length;
    var doctor = $('#patient_doctor_hidden').val();
    var visit_date = $('#patient_visit_date').val();
    var branch_id = localStorage.getItem('branch_id') ? parseInt(localStorage.getItem('branch_id')) : 0;
    if (doctor) {
        if (blocked_len != 0 || unblocked_len != 0) {
            bootbox.confirm({
                message: "Are you sure you want to block/unblock appointments.",
                buttons: {
                    confirm: {
                        label: "Yes",
                        className: "btn-warning",
                        default: "true",
                    },
                    cancel: {
                        label: "No",
                        className: "btn-danger",
                    },
                },
                callback: function (result) {
                    if (result) {
                        var url = base_url + "/patient_register/blockAppointment";
                        var block_appointment_string = JSON.stringify(block_appointment);
                        var unblock_appointment_string = JSON.stringify(unblock_appointment);
                        var param = { doctor: doctor, visit_date: visit_date, branch_id: branch_id, unblock_appointment_string: unblock_appointment_string, block_appointment_string: block_appointment_string };
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: param,
                            beforeSend: function () {
                                $('#blockAppointmentBtn').attr('disabled', true);
                                $('#blockAppointmentSpin').removeClass('fa fa-ban');
                                $('#blockAppointmentSpin').addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                if (data) {
                                    toastr.success("Slot status successfully changed");
                                    getDoctorSlots(0);
                                    block_appointment = new Array();
                                    unblock_appointment = new Array();
                                } else {
                                    toastr.error("Error Please Check Your Internet Connection");
                                }
                            },
                            complete: function () {
                                $('#blockAppointmentBtn').attr('disabled', false);
                                $('#blockAppointmentSpin').removeClass('fa fa-spinner fa-spin');
                                $('#blockAppointmentSpin').addClass('fa fa-ban');
                            },
                            error: function () {
                                toastr.error("Error Please Check Your Internet Connection");
                            }
                        });
                    }
                },
            });
        } else {
            toastr.warning("Please select any slot");
        }
    }
}


function getDoctorChageSplitUp(pricing_id) {
    var patient_status = $('input[name=patient_status]:checked').val();
    var doctor_id = $('#patient_doctor_hidden').val();
    var patient_id = $('#patient_id_hidden').val();
    if (doctor_id) {
        var url = base_url + "/patient_register/getDoctorSplitUp";
        var param = { pricing_id: pricing_id, patient_status: patient_status, doctor_id: doctor_id, patient_id: patient_id };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#getDoctorChargesBtn').attr('disabled', true);
                $('#getDoctorChargesSpin').removeClass('fa fa-columns');
                $('#getDoctorChargesSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (pricing_id == '0') {
                    $("#getDoctorChagesModelDiv").html(data);
                    $('#getDoctorChagesModelHeader').html('Charges Split Up');
                    $("#getDoctorChagesModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                } else {
                    $('#doctor_charges_hidden').html(data);
                    $('#charges_hidden').val(data);
                }
            },
            complete: function () {
                $('#getDoctorChargesBtn').attr('disabled', false);
                $('#getDoctorChargesSpin').removeClass('fa fa-spinner fa-spin');
                $('#getDoctorChargesSpin').addClass('fa fa-columns');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please select doctor");
    }

}

function confirmPatientRequest(uhid, patient_name, token_no, visit_date, doctor_name, from_string, from_type, bill_id, start_time, pay_status) {
    var uhid_string = '';
    if (uhid) {
        uhid_string = "<tr><td> UHID </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + uhid + "</td></tr>";
    }
    var message_string = "<table>" + uhid_string + "<tr><td> Patient Name </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + patient_name + "</td></tr><tr><td> Token No </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + token_no + "</td></tr><tr><td> Appointment Time </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td> " + start_time + "</td></tr><tr><td> Appointment Date </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + visit_date + "</td></tr><tr><td> Doctor Name </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td> " + doctor_name + "</td></tr></table>";
    if (from_type == '1') {
        bootbox.alert({
            title: from_string + " Successfully Added ",
            message: message_string,
            callback: function () {
                block_appointment = new Array();
                resetAppointmentForm(1, from_type, 0);
            }
        });
    } else {
        if (pay_status == '1') {
            label_type = 'Print / Pay';
        } else {
            label_type = 'Print'
        }
        bootbox.confirm({
            title: from_string + " Successfully Added ",
            message: message_string,
            buttons: {
                'confirm': {
                    label: label_type,
                    className: 'btn-primary',
                    default: 'true'
                },
                'cancel': {
                    label: 'OK',
                    className: 'btn-succcess'
                }
            },
            callback: function (result) {
                if (result) {
                    printDetails(patient_name, uhid, bill_id);
                }
                resetAppointmentForm(1, from_type, 0);
                block_appointment = new Array();
                insurance_array = new Array();
                pricing_details = new Array();
                patient_more_details = new Array();
            }
        });
    }
}


function printDetails(patient_name, uhid, bill_id) {
    var url = base_url + "/patient_register/printDetails";
    var param = { bill_id: bill_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getPrintRegistrationModelHedder').html(patient_name + " - " + uhid);
            $("#getPrintRegistrationModelDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("#getPrintRegistrationModelDiv").html(data);
            $("#getPrintRegistrationModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $("#getPrintRegistrationModelDiv").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function addWindowLoad(to_url) {
    var url = '';
    if (to_url == 'appointmentlisting') {
        url = base_url + "/master/" + to_url;
    } else {
        url = base_url + "/patient_register/" + to_url;
    }
    document.location.href = url;
}

function confirmPatientRequestNew(uhid, patient_name, token_no, visit_date, doctor_name, from_string, from_type, bill_id, start_time, pay_status) {
    var uhid_string = '';
    if (uhid) {
        uhid_string = "<tr><td> UHID </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + uhid + "</td></tr>";
    }
    var message_string = "<table>" + uhid_string + "<tr><td> Patient Name </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + patient_name + "</td></tr><tr><td> Token No </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + token_no + "</td></tr><tr><td> Appointment Time </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td> " + start_time + "</td></tr><tr><td> Appointment Date </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td>" + visit_date + "</td></tr><tr><td> Doctor Name </td><td>&nbsp;&nbsp; : &nbsp;&nbsp;</td><td> " + doctor_name + "</td></tr></table>";
    if (from_type == '1') {
        bootbox.alert({
            title: from_string + " Successfully Added ",
            message: message_string,
            callback: function () {
                block_appointment = new Array();
                resetAppointmentForm(1, from_type, 0);
            }
        });
    } else {

        bootbox.confirm({
            title: from_string + " Successfully Added ",
            message: message_string,
            buttons: {
                'confirm': {
                    label: 'Print',
                    className: 'btn-primary',
                    default: 'true'
                },
                'cancel': {
                    label: 'OK',
                    className: 'btn-succcess'
                }
            },
            callback: function (result) {
                if (result) {
                    printDetails(patient_name, uhid, bill_id);
                }
                resetAppointmentForm(1, from_type, 0);
                block_appointment = new Array();
                insurance_array = new Array();
                pricing_details = new Array();
                patient_more_details = new Array();
                insurance_count = new Array();
                insurance_details = new Array();


            }
        });
    }
}

function printDetailsNew(patient_name, uhid, bill_id) {
    var url = base_url + "/patient_register/printDetails";
    var param = { bill_id: bill_id };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getPrintRegistrationModelHedder').html(patient_name + " - " + uhid);
            $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $("#getPrintRegistrationModelDiv").html(data);
            $("#getPrintRegistrationModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $("body").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
