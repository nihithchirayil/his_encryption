
$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });

    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

    window.search_url = $(".table_body_contents").attr('data-search-url');
    getFilteredItems(window.search_url);

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        if($(e.target).parent().hasClass('active') == false){
            var url = $(this).attr("href");
            getFilteredItems(url);
        }

    });

    $(document).on('click', '.searchBtn', function (e) {
        getFilteredItems(window.search_url);
    });
    $(document).on("click", function(event){
        var $trigger = $(".ajaxSearchBox");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $(".ajaxSearchBox").hide();
        }
    });

});

function getFilteredItems(url){
    var data = {};
    data.voucher_no = $("#voucher_no").val();
    data.status = $('#select_status').val();
    data.voucher_type = $('#select_voucher').val();
    data.post_type = $('#select_post_type').val();
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
             $(".table_body_contents").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (data) {
            $(".table_body_contents").LoadingOverlay("hide");
            $(".table_body_contents").html(data.html);
            $(".page-item").attr("disabled", false);
            $(".page-item.active").attr("disabled", true);

            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
        },
        complete: function () {

        }
    });
}
function ApproveReq(e,id,type = 1){
    var url = $(".table_body_contents").attr('data-action-url');
    var data = {};
    data.type = type;
    data.id = id;   
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(e).find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            $(e).attr('disabled', true);
        },
        success: function (data) {
            $(e).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
            $(e).attr('disabled', false);
            toastr.success('Successfully Approved');
            getFilteredItems(window.search_url);
        },
        complete: function () {
            $(e).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
            $(e).attr('disabled', false);        }
    });
}
function RejectReq(e,id,type = 2){
    var url = $(".table_body_contents").attr('data-action-url');
    var data = {};
    data.type = type;
    data.id = id;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $(e).find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            $(e).attr('disabled', true);
        },
        success: function (data) {
            $(e).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
            $(e).attr('disabled', false);
            toastr.success('Successfully Rejected');
            getFilteredItems(window.search_url);
        },
        complete: function () {
            $(e).find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
            $(e).attr('disabled', false);
            }
    });
}





