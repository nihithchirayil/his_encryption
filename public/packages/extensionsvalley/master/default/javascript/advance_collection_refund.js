var limit = 25;
var offset = 0;
var total_rec = 0;
var i = 0;
var collection = [];
var collected_amount = 0;
var amount_total = 0;
var balance = 0;
$(document).ready(function () {
var $table = $('table.theadfix_wrapper');
$table.floatThead({
scrollContainer: function ($table) {
return $table.closest('.theadscroll');
}

});
$('.theadscroll').perfectScrollbar({
wheelPropagation: true,
        minScrollbarLength: 30
});
$('.fixed_header').floatThead({
position: 'absolute',
        scrollContainer: true
});
searchList(15,0);
});
var base_url = $("#base_url").val();

function searchList(limit = 25,offset =0){

    var url=base_url+'/advance_colrefnd/advanceCollectionRefundData';
    var patient_id = $("#patient_name_hidden").val();
    var param1 = {patient_id:patient_id,limit:limit,offset:offset};
    $.ajax({
    type: "GET",
        url: url,
        data:param1,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled',true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner');
            $('#common_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (msg) {
            var obj = JSON.parse(msg);
             msg = obj.viewData;
            total_rec = obj.total_rec;
            if(offset == 0){
                $('#common_list_div').html(msg);
            }else{
                $('#receipt_table1').append(msg);

            }
        },
        complete: function () {
            $('#searchdatabtn').attr('disabled',false);
            $('#searchdataspin').removeClass('fa fa-spinner');
            $('#searchdataspin').addClass('fa fa-search');
            $('#common_list_div').LoadingOverlay("hide");


            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');


        },error: function(){
            toastr.error("Please Check Internet Connection");
        }
    });
}

$('.hidden_search').keyup(function (event) {
var input_id = '';
var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
var value = event.key; //get the charcode and convert to char
input_id = $(this).attr('id');
var url = base_url+'/advance_colrefnd/advanceCollectionRefund';
if (value.match(keycheck)  || event.keyCode == '8' || event.keyCode == 46) {
    if ($('#'+input_id+'_hidden').val() != "") {
        $('#'+input_id+'_hidden').val('');
    }
    var search_key = $(this).val();
    search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
    search_key = search_key.trim();

    var datastring = '';
    if (search_key == "") {
        $("#AjaxDiv_"+input_id).html("");
    } else {
        $.ajax({
            type: "GET",
            url: url,
            data: 'op_no_search=' + search_key+'&input_id='+input_id,
            beforeSend: function () {

                $("#AjaxDiv_"+input_id).html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
                if (html == 0) {
                    $("#AjaxDiv_"+input_id).html("No results found!").show();

                    $("#AjaxDiv_"+input_id).find('li').first().addClass('liHover');

                    return "<span>No result found!!</span>";

                } else {
                    $("#AjaxDiv_"+input_id).html(html).show();
                    $("#AjaxDiv_"+input_id).find('li').first().addClass('liHover');

                }

            },

            complete: function () {
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            },

        });
    }
} else {
    ajaxProgressiveKeyUpDown('AjaxDiv_'+input_id, event);
}
});

function fillSearchDetials(id,uhid,name,input_id,current_adv_amnt) {
    $('#'+input_id+"_hidden").val(id);
    if(input_id == 'patient_name_add'){
        $("#current_adv_amnt").val(current_adv_amnt);
        $('#'+input_id).val(name);
        $("#uhid").val(uhid);
    }else{
        $('#'+input_id).val(name+'['+ uhid+']');
    }
 $("#AjaxDiv_"+input_id).hide();
}
function ajaxProgressiveKeyUpDown(ajax_div, event) {
    var item_list = $("#" + ajax_div).find('li');
    var selected = item_list.filter('.liHover');
    if (event.keyCode === 13){
        ajaxProgressiveEnterKey(ajax_div);
    }
    if (event.keyCode != 40 && event.keyCode != 38)
        return;
    item_list.removeClass('liHover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current_item = item_list.eq(0);
        } else {
            current_item = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current_item = item_list.last();
        } else {
            current_item = selected.prev();
        }
    }
    current_item.addClass('liHover');
}
function ajaxProgressiveEnterKey(ajax_div) {
    $("#" + ajax_div).find('li.liHover').trigger('click');
}
//======================================load cash collection modal=================================================

function loadCashCollection() {
        var token = $("#c_token").val();
        var base_url = $("#base_url").val();
        var dataparams = {
            '_token': token,
            'load_cash_collection': 1,
        };
        $.ajax({
            type: "POST",
            url: base_url + "/advance_colrefnd/cash_receive",
            data: dataparams,
            beforeSend: function () {
                $('#modalCashRecive').modal('show');
                $('#modalCashReciveBody').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#modalCashReciveBody').html(data);

            },
            complete: function () {
                $('#modalCashReciveBody').LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            },
        });

}
function fistCalculateBalance(){
var collection_amount = $("#amount").val();
var advance_amount = $("#advance_amount").val();
if(advance_amount != ''){
    var cal_bal = parseFloat(advance_amount)-parseFloat(collection_amount)
}
var single_amnts = 0;
$("#payment_mode_table tbody tr").each(function () {
    single_amnts =  parseFloat(single_amnts) + parseFloat($(this).closest('tr').find('td.amount').html());
 });
 if(isNaN(single_amnts)) {
    var single_amnts = 0;
    }
    if(isNaN(cal_bal)) {
        var cal_bal = 0;
        }
 if(parseFloat(single_amnts) >= 0){
    $("#balance_amount").val(parseFloat(collection_amount)-parseFloat(single_amnts));
 }else if(parseFloat(cal_bal) >= 0 ){
    $("#balance_amount").val(cal_bal);
}

}
function changePaymentMode(mode) {
    $('#payment_mode').val(mode);
    $('#machine_bank').val('');
    $('#bank').val('');
}
function addNewPaymentMode() {
if($('#advance_amount').val() == ''){
    Command: toastr["warning"]('Please enter advance amount');
    return false;
}
if($('#patient_name_add').val() == ''){
    Command: toastr["warning"]('Please enter patient name');
    return false;
}
    var payment_mode = $('#payment_mode').val();
    var payment_mode_name = $('#payment_mode option:selected').text();
    var machine_bank_name = $('#machine_bank option:selected').text();
    var bank_name = $('#bank option:selected').text();
    var amount = $('#amount').val();
    var card_no = $('#card_no').val();
    var phone_no = $('#phone_no').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();
    var amount_to_be_paid = parseFloat($('#advance_amount').val());
    var html = '';
    var append_flag = 0;
    var current_balance_amount = $('#balance_amount').val();

    var single_amnt = 0;
    $("#payment_mode_table tbody tr").each(function () {
       single_amnt =  parseFloat(single_amnt) + parseFloat($(this).closest('tr').find('td.amount').html());
    });
    if(amount_to_be_paid-(parseFloat(single_amnt)+parseFloat(amount)) < 0){
        console.log(amount_to_be_paid);
        console.log("--");
        console.log(single_amnt+amount);
        console.log(amount_to_be_paid-(single_amnt+amount));
        Command: toastr["warning"]('Amount cannot be greater than current balance amount');
        return false;
    }



    if (current_balance_amount == '') {
        current_balance_amount = 0;
    }
    current_balance_amount = parseFloat(current_balance_amount);

    if (amount == 0 || amount == '') {
        return false;
    } else {
        amount = parseFloat(amount);
    }
    if (current_balance_amount == 0) {
        current_balance_amount = amount_to_be_paid;
    }


    if (parseFloat(amount) > parseFloat(current_balance_amount)) {
        Command: toastr["warning"]('Amount cannot be greater than current balance amount');
        return false;
    }

    if (payment_mode == '') {
        Command: toastr["warning"]('Please select payment mode!');
        return false;
    }


    var PaymentModeMandatory = checkPaymentModeMandatory(payment_mode);
    if (PaymentModeMandatory == false) {
        return false;
    }

    html += '<tr class="payment_mode_row" id="payment_mode_row_' + i + '">';
    html += '<td class="payment_mode">' + payment_mode_name + '</td>';
    html += '<td class="amount">' + amount + '</td>';
    html += '<td class="machine_bank_name">' + machine_bank_name + '</td>';
    html += '<td class="bank_name">' + bank_name + '</td>';
    html += '<td class="card_no">' + card_no + '</td>';
    html += '<td class="exp_date">' + exp_year + '/' + exp_month + '</td>';
    html += '<td class="phone_no">' + phone_no + '</td>';
    html += '<td><button type="button" onclick="deleteCurrentRow(this);" class="btn btn-danger deleteCurrentRow"><i class="fa fa-trash"></i></button></td>';
    html += '</tr>';


    if ($('#payment_mode_table tr').length > 1) {


        $("#payment_mode_table tbody tr").each(function () {
            var mode = $(this).find('td.payment_mode');
            if (mode.html() == payment_mode_name) {
                append_flag = 1;
                var current_amount = $(this).find('td.amount').html();
                var new_amount = parseFloat(current_amount) + parseFloat(amount);
                $(this).find('td.amount').html(new_amount);
            }
        });

        if (append_flag == 0) {
            $('#payment_mode_table').append(html);
        }


    } else {
        $('#payment_mode_table').append(html);
        append_flag = 1;
    }
    var single_amnts = 0;
    $("#payment_mode_table tbody tr").each(function () {
       single_amnts =  parseFloat(single_amnts) + parseFloat($(this).closest('tr').find('td.amount').html());
    });
    $("#balance_amount").val(amount_to_be_paid-single_amnts);
    $('#amount').val('');
    $('#card_no').val('');
    $('#phone_no').val('');
    $('#exp_month').val('');
    $('#exp_year').val('');
    $('#payment_mode').val('1');
    $('#machine_bank').val('');
}
function checkPaymentModeMandatory(payment_mode) {
    var payment_mode_detail = $('#payment_mode_detail').val();
    var card_no = $('#card_no').val();
    var bank = $('#bank').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();

    payment_mode_detail = atob(payment_mode_detail);
    payment_mode_detail = JSON.parse(payment_mode_detail);
    if (payment_mode_detail[payment_mode].is_cardno_mandatory != false) {
        if (card_no == '') {
            Command: toastr["warning"]('Please enter card number!');
            return false;
        }
    }
    if (payment_mode_detail[payment_mode].is_bank_mandatory != false) {
        if (bank == '') {
            Command: toastr["warning"]('Please select bank!');
            return false;
        }
    }
    if (payment_mode_detail[payment_mode].is_expirydate_mandatory != false) {
        if (exp_month == '' || exp_year == '') {
            Command: toastr["warning"]('Please select expiry date!');
            return false;
        }
    }

}


function checkContentLength(id, length) {
    var value = $('#' + id).val();
    if (value.length > length) {
        value = value.substring(0, length);
        $('#' + id).val(value);
    }
    if (id == 'exp_month') {
        if (value > 12 || value < 1) {
            $('#' + id).val('');
        }
    }
    if (id == 'exp_year') {
        if (value > 2099) {
            $('#' + id).val('');
        }
    }
}

function deleteCurrentRow(obj) {
    $(obj).closest('tr').remove();
    var current_amount = $(obj).closest('tr').find('td.amount').html();

    var current_amount = 0;
$("#payment_mode_table tbody tr").each(function () {
    current_amount =  parseFloat(current_amount) + parseFloat($(this).closest('tr').find('td.amount').html());
 });
    current_amount = parseFloat(current_amount);
    var amount = $('#advance_amount').val();
    amount = parseFloat(amount);
    $('#amount').val(parseFloat(amount) - parseFloat(current_amount));
    $('#balance_amount').val(parseFloat(amount) - parseFloat(current_amount));
}



//======================================================================================================================================================




function saveCollectionAmount(){
    addNewPaymentMode();
    var url = base_url+'/advance_colrefnd/collectAdvAmount';
    var collection_amount = $("#advance_amount").val();
    var patient_name_add_hidden = $("#patient_name_add_hidden").val();
    var payment_mode_array = [];
    var amount_array = [];
    var machine_bank_array = [];
    var bank_array = [];
    var card_no_array = [];
    var exp_date_array = [];
    var phone_no_array = [];
    var i = 0;
    var token = $('#c_token').val();
    var uhid =$("#uhid").val();
    var amount_chk = 0;
    $("#payment_mode_table tbody tr").each(function () {
        var payment_mode = $(this).find('td.payment_mode').html();
        var amount = $(this).find('td.amount').html();
        var machine_bank = $(this).find('td.machine_bank').html();
        var bank = $(this).find('td.bank').html();
        var card_no = $(this).find('td.card_no').html();
        var exp_date = $(this).find('td.exp_date').html();
        var phone_no = $(this).find('td.phone_no').html();

        payment_mode_array[i] = payment_mode != '' ? payment_mode : '';
        amount_array[i] = amount != '' ? amount : 0;
        machine_bank_array[i] = machine_bank != '' ? machine_bank : '';
        bank_array[i] = bank != '' ? bank : '';
        card_no_array[i] = card_no != '' ? card_no : '';
        exp_date_array[i] = exp_date != '' ? exp_date : '';
        phone_no_array[i] = phone_no != '' ? phone_no : '';
        i++;
        amount_chk = parseFloat(amount_chk)+parseFloat(amount);
    });

    if(parseFloat(amount_chk) != parseFloat(collection_amount)){
        console.log(amount_chk);
        console.log("----")
        console.log(collection_amount)
        Command: toastr["warning"]('Please check amount split up');
            return false;
    }
    var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
       var param1 = {
        '_token': token,
        'counter': $('#counter').val(),
        'payment_mode_array': payment_mode_array,
        'amount_array': amount_array,
        'machine_bank_array': machine_bank_array,
        'bank_array': bank_array,
        'card_no_array': card_no_array,
        'exp_date_array': exp_date_array,
        'phone_no_array': phone_no_array,
        'net_amount': collection_amount,
        'location_id': location_id,
        'uhid': uhid,
       };

       $.ajax({
       type: "GET",
           url: url,
           data:param1,
           beforeSend: function () {
               $('#save_bill_payment').attr('disabled',true);
               $('#save_bill_payment_spn').removeClass('fa fa-save');
               $('#save_bill_payment_spn').addClass('fa fa-spinner fa-spin');
           },
           success: function (message) {
            var msg = JSON.parse(message);
               if(msg.status == 0){
                   toastr.error("Error Occured");
               }
               if(msg.status == 1){
                $('#modalCashRecive').modal('hide');
                   toastr.success(msg.message);
                   searchList();
               }
           },
           complete: function () {
            $('#save_bill_payment').attr('disabled',false);


           },error: function(){
               toastr.error("Please Check Internet Connection");
           }
       });
}
function refundAdvAmountLoad(id,name,adv){
    $("#advance_refund_pt_id_hidden").val(id);
    $("#advance_refund_amnt_hidden").val(adv);
    $("#advance_amount_to_pay").val(adv);
    $("#patient_title").html(name+',Collected amount:- '+adv);
    $("#modalAdvanceRefund").modal('show');
    $(".select2").select2();
}
function checkRefundAmnt(e){
var curamnt = $(e).val();
var rfnd_amnt =  $("#advance_refund_amnt_hidden").val();
if(parseFloat(curamnt) > parseFloat(rfnd_amnt)){
    toastr.error("Amount is greater than Advance amount");
    $(e).val('');
}

}
function saveCollectionRefundAmount(){
 var url = base_url+'/advance_colrefnd/saveCollectionRefundAmount';
 var refund_amount = $("#advance_amount_to_pay").val();
 var rfnd_amnt =  $("#advance_refund_amnt_hidden").val();
 var patient_id = $("#advance_refund_pt_id_hidden").val();
 var payment_type = $("#payment_mode_refund").val();
 var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
 if(refund_amount == '' || refund_amount ==0){
           toastr.error("Please enter valid amount");
           return;
       }
    var param1 = {patient_id:patient_id,refund_amount:refund_amount,location_id:location_id,payment_type:payment_type};
    if(parseFloat(rfnd_amnt) < parseFloat(refund_amount)){
           toastr.error("Given amount greater than the advance amount");
           return;
        }

    $.ajax({
    type: "GET",
        url: url,
        data:param1,
        beforeSend: function () {
            $('#save_refund_payment').attr('disabled',true);
            $('#save_refund_payment_spn').removeClass('fa fa-save');
            $('#save_refund_payment_spn').addClass('fa fa-spinner fa-spin');
        },
        success: function (msg) {
            if(msg == 3){
                toastr.error("Given amount greater than the advance amount");
            }
            if(msg == 1){
                toastr.success("Amount returned successfully");
                $('#modalAdvanceRefund').modal('hide');
                $('#save_refund_payment').attr('disabled',false);
                $('#save_refund_payment_spn').removeClass('fa fa-spinner fa-spin');
                $('#save_refund_payment_spn').addClass('fa fa-save');
                searchList();
            }
        },
        complete: function () {


        },error: function(){
            toastr.error("Please Check Internet Connection");
        }
    });
}
function number_validation(e) {
var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
        val = e.value;
if (!valid) {
    e.value = val.substring(0, val.length - 1);
}
}
