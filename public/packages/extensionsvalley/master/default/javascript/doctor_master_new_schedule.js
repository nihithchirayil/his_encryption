$("#dr_time_sloat_end_time").on('blur', function () {
    var start_time = $('#dr_time_sloat_start_time').val();
    var end_time = $('#dr_time_sloat_end_time').val();
    var start_time = moment(start_time, 'HH:mm:ss a');
    var end_time = moment(end_time, 'HH:mm:ss a');
    var mins = moment.utc(moment(end_time, "HH:mm:ss a").diff(moment(start_time, "HH:mm:ss a"))).format("mm");
    var hours = end_time.diff(start_time, 'hours');
    hours = parseInt(hours) * 60;
    var total_minutes = parseInt(hours) + parseInt(mins);
    if (total_minutes < 0) {
        toastr.warning('End time should be greater than start time!');
        $("#dr_time_sloat_end_time").val('');
    }
});

function checkAllDays() {
    if ($("#dr_time_sloat_week_select_all").prop('checked') == true) {
        $('.time_sloat_weeks').prop('checked', true);
    } else {
        $('.time_sloat_weeks').prop('checked', false);
    }
}

function checkAllWalkinDays() {
    if ($("#dr_time_sloat_walkin_select_all").prop('checked') == true) {
        $('.chk_dr_walkin').prop('checked', true);
    } else {
        $('.chk_dr_walkin').prop('checked', false);
    }
}

//---getting time slots------------------------------

function getAvailableTimeSlots() {
    var start_time = $('#dr_time_sloat_start_time').val();
    var end_time = $('#dr_time_sloat_end_time').val();
    var start_time = moment(start_time, 'HH:mm:ss a');
    var end_time = moment(end_time, 'HH:mm:ss a');
    var mins = moment.utc(moment(end_time, "HH:mm:ss a").diff(moment(start_time, "HH:mm:ss a"))).format("mm");
    var hours = end_time.diff(start_time, 'hours');
    hours = parseInt(hours) * 60;
    var total_minutes = parseInt(hours) + parseInt(mins);
    var duration = parseInt($('#dr_time_sloat_duration').val());

    if (!duration) {
        duration = 0;
        $('#dr_time_sloat_duration').val(0);
    }

    if (duration > total_minutes) {
        toastr.warning('No slots available in this duration!');
    }
    var total_slots = 0;

    if (parseInt(duration) != 0) {
        total_slots = parseInt(total_minutes / duration);
    }

    $('#dr_time_sloat_available_slot').val(total_slots);
    var slot_start = start_time;
    var slot_array = [];

    for (var i = 0; i < total_slots; i++) {
        if (slot_start != start_time) {
            slot_start = moment(slot_start, 'HH:mm:ss a');
        }
        var new_time = moment(slot_start, "DD-MM-YYYY hh:mm:ss A")
            .add(duration, 'minutes')
            .format('HH:mm A');

        slot_array.push(new_time);
        slot_start = new_time;
    }
    var html = '';
    $.each(slot_array, function (key, value) {
        html += '<div class="col-md-3" style="border-radius:3px;padding:2px;font-size:11px;font-weight:600;"><div class="col-md-12 bg-info">' + value + '</div></div>';
    });
    $('#sloat_list_area').html(html);
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30

    });
}


function resetDoctorSchedule() {
    $('#dr_time_sloat_from_date').val('');
    $('#dr_time_sloat_ends_on').val('');
    $('#dr_time_sloat_start_time').val('');
    $('#dr_time_sloat_end_time').val('');
    $('#dr_time_sloat_duration').val(0);
    $('#dr_time_sloat_available_slot').val(0);
    $('#dr_time_sloat_grace_token').val('');
    $('#schedule_id_hidden').val(0);
    $('#sloat_list_area').html('');
    $('.time_sloat_weeks').prop("checked", false);
    $('#dr_time_sloat_week_select_all').prop("checked", false);
    $('#dr_time_sloat_is_ends_on').prop("checked", false);
}


function resetDrWalkin() {
    $('#dr_time_sloat_walkin_count').val('');
    $('#dr_time_sloat_walkin_select_all').prop("checked", false);
    $('.chk_dr_walkin').prop("checked", false);
}

function saveDoctorSchedule() {
    var doctor_id = $('#doctor_id_hidden').val();
    var slot_duration = $('#dr_time_sloat_duration').val();
    if (doctor_id) {
        if (slot_duration && parseInt(slot_duration) != 0) {
            var from_date = $('#dr_time_sloat_from_date').val();
            var from_time = $('#dr_time_sloat_start_time').val();
            var to_date = $('#dr_time_sloat_from_date').val();
            var to_time = $('#dr_time_sloat_end_time').val();
            var ends_on_date = $('#dr_time_sloat_ends_on').val();
            var grace_token = $('#dr_time_sloat_grace_token').val();
            var schedule_id_hidden = $('#schedule_id_hidden').val();
            var parent_schedule_id = 0;
            var is_repeat = 2;
            var repeats = 1;
            var repeats_every = 2;

            if (from_date == '') {
                toastr.warning("enter from date!");
                $('#dr_time_sloat_from_date').focus();
                return false;
            }
            if (to_date == '') {
                toastr.warning("enter to date!");
                $('#dr_time_sloat_from_date').focus();
                return false;
            }
            if (from_time == '') {
                toastr.warning("enter from time!");
                $('#dr_time_sloat_start_time').focus();
                return false;
            }
            if (to_time == '') {
                toastr.warning("enter to time!");
                $('#dr_time_sloat_end_time').focus();
                return false;
            }
            if (slot_duration == '') {
                toastr.warning("enter slot duration!");
                $('#dr_time_sloat_duration').focus();
                return false;
            }


            var repeats_on_sunday = 0;
            if ($('#dr_time_sloat_sun').is(':checked')) {
                repeats_on_sunday = 1;
            }
            var repeats_on_monday = 0;
            if ($('#dr_time_sloat_mon').is(':checked')) {
                repeats_on_monday = 1;
            }
            var repeats_on_tuesday = 0;
            if ($('#dr_time_sloat_tue').is(':checked')) {
                repeats_on_tuesday = 1;
            }
            var repeats_on_wednesday = 0;
            if ($('#dr_time_sloat_wed').is(':checked')) {
                repeats_on_wednesday = 1;
            }
            var repeats_on_thursday = 0;
            if ($('#dr_time_sloat_thu').is(':checked')) {
                repeats_on_thursday = 1;
            }
            var repeats_on_friday = 0;
            if ($('#dr_time_sloat_fri').is(':checked')) {
                repeats_on_friday = 1;
            }
            var repeats_on_saturday = 0;
            if ($('#dr_time_sloat_sat').is(':checked')) {
                repeats_on_saturday = 1;
            }

            var dataparams = {
                'doctor_id': doctor_id,
                'from_date': from_date,
                'from_time': from_time,
                'to_date': to_date,
                'to_time': to_time,
                'slot_duration': slot_duration,
                'ends_on_date': ends_on_date,
                'parent_schedule_id': parent_schedule_id,
                'is_repeat': is_repeat,
                'grace_token': grace_token,
                'repeats': repeats,
                'repeats_every': repeats_every,
                'repeats_on_sunday': repeats_on_sunday,
                'repeats_on_monday': repeats_on_monday,
                'repeats_on_tuesday': repeats_on_tuesday,
                'repeats_on_wednesday': repeats_on_wednesday,
                'repeats_on_thursday': repeats_on_thursday,
                'repeats_on_friday': repeats_on_friday,
                'repeats_on_saturday': repeats_on_saturday,
                'schedule_id_hidden': schedule_id_hidden,
            };
            var url = base_url + "/doctor_master_new/saveDoctorSchedule";
            $.ajax({
                type: "POST",
                url: url,
                data: dataparams,
                beforeSend: function () {
                    $('#saveSalaryMasterBtn').attr('disabled', true);
                    $('#saveDoctorScheduleSpin').removeClass('fa fa-save');
                    $('#saveDoctorScheduleSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (parseInt(data.status) == 1) {
                        toastr.success(data.message);
                        fetchDoctorSchedule();
                        resetDoctorSchedule();
                    } else {
                        toastr.warning(data.message);
                    }
                },
                complete: function () {
                    $('#saveSalaryMasterBtn').attr('disabled', false);
                    $('#saveDoctorScheduleSpin').removeClass('fa fa-spinner fa-spin');
                    $('#saveDoctorScheduleSpin').addClass('fa fa-save');
                }
            });
        } else {
            toastr.warning("Select any Duration");
        }
    } else {
        toastr.warning("Select any doctor!");
    }
}


function fetchDoctorSchedule() {
    var url = base_url + "/doctor_master_new/fetchDoctorSchedule";
    var doctor_id = $('#doctor_id_hidden').val();
    if (doctor_id == 0) {
        toastr.warning("Select doctor!");
        return false;
    }
    var dataparams = {
        'doctor_id': doctor_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#dr_time_sloat_list_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            $("#dr_time_sloat_list_data").html(html);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#dr_time_sloat_list_data").LoadingOverlay("hide");
        }

    });
}

function saveDrWalkin() {
    var url = base_url + "/doctor_master_new/saveDrWalkin";
    var doctor_id = $('#doctor_id_hidden').val();
    var walkin_count = $('#dr_time_sloat_walkin_count').val();
    if (doctor_id) {
        if (walkin_count) {
            var walkin_mon = 0;
            if ($('#dr_time_sloat_walkin_mon').is(':checked')) {
                walkin_mon = 1;
            }
            var walkin_tue = 0;
            if ($('#dr_time_sloat_walkin_tue').is(':checked')) {
                walkin_tue = 1;
            }
            var walkin_wed = 0;
            if ($('#dr_time_sloat_walkin_wed').is(':checked')) {
                walkin_wed = 1;
            }
            var walkin_thu = 0;
            if ($('#dr_time_sloat_walkin_thu').is(':checked')) {
                walkin_thu = 1;
            }
            var walkin_fri = 0;
            if ($('#dr_time_sloat_walkin_fri').is(':checked')) {
                walkin_fri = 1;
            }
            var walkin_sat = 0;
            if ($('#dr_time_sloat_walkin_sat').is(':checked')) {
                walkin_sat = 1;
            }
            var walkin_sun = 0;
            if ($('#dr_time_sloat_walkin_sun').is(':checked')) {
                walkin_sun = 1;
            }

            var dataparams = {
                'doctor_id': doctor_id,
                'walkin_count': walkin_count,
                'walkin_mon': walkin_mon,
                'walkin_tue': walkin_tue,
                'walkin_wed': walkin_wed,
                'walkin_thu': walkin_thu,
                'walkin_fri': walkin_fri,
                'walkin_sat': walkin_sat,
                'walkin_sun': walkin_sun,
            }

            $.ajax({
                type: "POST",
                url: url,
                data: dataparams,
                beforeSend: function () {
                    $('#saveDrWalkinBtn').attr('disabled', true);
                    $('#saveDrWalkinSpin').removeClass('fa fa-save');
                    $('#saveDrWalkinSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (parseInt(data.status) == 1) {
                        toastr.success(data.message);
                        fetchDrWalkin();
                        resetDrWalkin();
                    } else {
                        toastr.warning(data.message);
                    }
                },
                complete: function () {
                    $('#saveDrWalkinBtn').attr('disabled', false);
                    $('#saveDrWalkinSpin').removeClass('fa fa-spinner fa-spin');
                    $('#saveDrWalkinSpin').addClass('fa fa-save');
                }
            });
        } else {
            toastr.warning("Select walkin count!");
        }
    } else {
        toastr.warning("Select any doctor!");
    }
}


function fetchDrWalkin() {
    var url = base_url + "/doctor_master_new/fetchDrWalkin";
    var doctor_id = $('#doctor_id_hidden').val();
    var dataparams = {
        'doctor_id': doctor_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $("#walkin_container_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            $("#walkin_container_data").html(html);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function () {
            $("#walkin_container_data").LoadingOverlay("hide");
        }
    });
}

function delete_dr_wakin_schedule(schedule_id) {
    var url = base_url + "/doctor_master_new/DeleteDrWalkin";

    bootbox.confirm({
        message: "Are you sure,you want to delete ?",
        buttons: {
            'confirm': {
                label: "Continue",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {

                var dataparams = {
                    'schedule_id': schedule_id,
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: dataparams,
                    beforeSend: function () {
                        $('#deletedrwakinscheduleBtn' + schedule_id).attr('disabled', true);
                        $('#deletedrwakinscheduleSpin' + schedule_id).removeClass('fa fa-times');
                        $('#deletedrwakinscheduleSpin' + schedule_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (html) {
                        if (html == 1) {
                            toastr.success("Deleted successfully");
                            $('#rowWalkingDrSchedule' + schedule_id).remove();
                            resetDrWalkin();
                        } else {
                            toastr.error("Error!");
                        }
                    },
                    complete: function () {
                        $('#deletedrwakinscheduleBtn' + schedule_id).attr('disabled', true);
                        $('#deletedrwakinscheduleSpin' + schedule_id).removeClass('fa fa-spinner fa-spin');
                        $('#deletedrwakinscheduleSpin' + schedule_id).addClass('fa fa-times');
                    }
                });
            }
        }
    });
}

function delete_dr_schedule(schedule_id) {
    var url = base_url + "/doctor_master_new/DeleteDrSchedule";
    bootbox.confirm({
        message: "Are you sure,you want to delete ?",
        buttons: {
            'confirm': {
                label: "Continue",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var doctor_id = $('#doctor_id_hidden').val();
                var dataparams = {
                    schedule_id: schedule_id,
                    doctor_id: doctor_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: dataparams,
                    beforeSend: function () {
                        $('#deletedrschedulebtn' + schedule_id).attr('disabled', true);
                        $('#deletedrschedulespin' + schedule_id).removeClass('fa fa-times');
                        $('#deletedrschedulespin' + schedule_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            $('#rowDrSchedule' + schedule_id).remove();
                            resetDoctorSchedule();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $('#deletedrschedulebtn' + schedule_id).attr('disabled', false);
                        $('#deletedrschedulespin' + schedule_id).removeClass('fa fa-spinner fa-spin');
                        $('#deletedrschedulespin' + schedule_id).addClass('fa fa-times');
                    }
                });
            }
        }
    });
}

function edit_dr_schedule(schedule_id) {
    var url = base_url + "/doctor_master_new/EditDrSchedule";
    var dataparams = {
        'schedule_id': schedule_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#editDrScheduleBtn' + schedule_id).attr('disabled', true);
            $('#editDrScheduleSpin' + schedule_id).removeClass('fa fa-edit');
            $('#editDrScheduleSpin' + schedule_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (html) {
            if (html != 0) {
                $('#schedule_id_hidden').val(html[0].id);
                var from_date = html[0].from_date;
                var from_time = html[0].from_time;
                var to_time = html[0].to_time;
                var ends_on_date = html[0].ends_on_date;
                $('#dr_time_sloat_from_date').val(from_date);
                $('#dr_time_sloat_start_time').val(from_time);
                $('#dr_time_sloat_end_time').val(to_time);
                $('#dr_time_sloat_ends_on').val(ends_on_date);
                $('#dr_time_sloat_duration').val(html[0].slot_duration);
                $('#dr_time_sloat_grace_token').val(html[0].grace_token);

                if (ends_on_date) {
                    $('#dr_time_sloat_ends_on').prop('checked', true);
                } else {
                    $('#dr_time_sloat_ends_on').prop('checked', false);
                }

                if (html[0].repeats_on_monday == 1) {
                    $('#dr_time_sloat_mon').prop('checked', true);
                } else {
                    $('#dr_time_sloat_mon').prop('checked', false);
                }
                if (html[0].repeats_on_tuesday == 1) {
                    $('#dr_time_sloat_tue').prop('checked', true);
                } else {
                    $('#dr_time_sloat_tue').prop('checked', false);
                }
                if (html[0].repeats_on_wednesday == 1) {
                    $('#dr_time_sloat_wed').prop('checked', true);
                } else {
                    $('#dr_time_sloat_wed').prop('checked', false);
                }
                if (html[0].repeats_on_thursday == 1) {
                    $('#dr_time_sloat_thu').prop('checked', true);
                } else {
                    $('#dr_time_sloat_thu').prop('checked', false);
                }
                if (html[0].repeats_on_friday == 1) {
                    $('#dr_time_sloat_fri').prop('checked', true);
                } else {
                    $('#dr_time_sloat_fri').prop('checked', false);
                }
                if (html[0].repeats_on_saturday == 1) {
                    $('#dr_time_sloat_sat').prop('checked', true);
                } else {
                    $('#dr_time_sloat_sat').prop('checked', false);
                }
                if (html[0].repeats_on_sunday == 1) {
                    $('#dr_time_sloat_sun').prop('checked', true);
                } else {
                    $('#dr_time_sloat_sun').prop('checked', false);
                }
                getAvailableTimeSlots();

            } else {
                toastr.error("Error!");
            }
        },
        complete: function () {
            $('#editDrScheduleBtn' + schedule_id).attr('disabled', false);
            $('#editDrScheduleSpin' + schedule_id).removeClass('fa fa-spinner fa-spin');
            $('#editDrScheduleSpin' + schedule_id).addClass('fa fa-edit');
        }
    });
}
