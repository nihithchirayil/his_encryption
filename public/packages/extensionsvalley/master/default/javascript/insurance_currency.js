var limit = 100;
var offset = 0;
var total_rec = 0;
var init_status = 0;
var filename_count = 0;
$(document).ready(function () {

    // $('#menu_toggle').trigger('click');
    $(".select_button li").click(function () {
        $(this).toggleClass('active');
    });
    $('#menu_toggle').on('click', function (){
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.ins_datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.date_time_picker').datetimepicker();
    searchInsuranceCurrencyData();
});
function save_insurance(sett_type) {

    var val = $("#all_save_btn").val();
    var bill_from = $("#bill_from_" + val).val();
    var bill_head_id = $("#bill_head_id_" + val).val();
    var visit_id = $("#visit_id_" + val).val();
    var company_id = $("#company_id_" + val).val();
    var rec_amnt = $("#rec_amnt_" + val).val();
    var settle_amount = $("#settle_amount_entry").val();
    var tds_amount = $("#tds_amount_entry").val();
    var write_off_amount = $("#write_off_amount_entry").val();
    var payment_checking = $("#ins_head_id_for_payment_mod").val();
    var pay_patient_adj = $("#pay_patient_adj_amount").val();
    var pay_insurance_adj = $("#pay_insurance_adj_amount").val();
    var pay_other_adj_amount = $("#pay_other_adj_amount").val();
    var bill_tag_ins = $("#bill_tag_ins").val();

    var url = $("#ins_base_url").val();
    url = url + "/master/save_insurance";
    var param = {save_ins: 1, bill_from: bill_from, company_id: company_id, rec_amnt: rec_amnt,
        visit_id: visit_id, bill_head_id: bill_head_id, settle_amount: settle_amount,
        tds_amount: tds_amount, write_off_amount: write_off_amount,pay_insurance_adj:pay_insurance_adj,
        pay_patient_adj:pay_patient_adj,pay_other_adj_amount:pay_other_adj_amount,bill_tag_ins:bill_tag_ins,sett_type:sett_type};
    if($("#ins_head_id_for_payment_mod").val() != 0 ){
        save_ins_payment_row(payment_checking);
    }if (settle_amount == '' && tds_amount == '' && write_off_amount == '' && pay_insurance_adj== '' &&  pay_patient_adj == '') {
        toastr.error("Amount Not entered");
        return;
    }
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            if(sett_type==5){
            $('#save_fully_ins_payment_spin_btn').removeClass('fa fa-thumbs-up');
            $('#save_fully_ins_payment_spin_btn').addClass('fa fa-spinner fa-spin');
        }else{
            $('#save_ins_payment_spin_btn').removeClass('fa fa-save');
            $('#save_ins_payment_spin_btn').addClass('fa fa-spinner fa-spin');
        }
        },
        success: function (data) {
            if (data> 0) {
                toastr.success("Successfully Added");
                save_ins_payment_row(data);
            } else {
                toastr.error("Error occured Please check data");
            }
        },
        complete: function () {
            $('#save_ins_payment_spin_btn').removeClass('fa fa-spinner fa-spin');
            $('#save_ins_payment_spin_btn').addClass('fa fa-save');
            searchInsuranceCurrencyData();
        }
    });

}
/**
 * OP NUMBER SEARCH STARTS
 */
$('#op_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(event.keyCode);
    var url = $("#ins_base_url").val();
    var send_url = url + "/insurance_currency/commonSearch";
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv").html("");
            $("#op_no_search_hidden").val("");
        } else {
            try {
                var url = send_url;
                var param = {op_no_search: op_no_search,op_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url:send_url,
                    data: param,
                    beforeSend: function () {
                        $("#op_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#op_no_searchCodeAjaxDiv").html(html).show();
                        $("#op_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('op_no_searchCodeAjaxDiv', event);
    }
});
$('#op_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_id(list, patient_id, uhid) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#op_no_search_hidden').val(patient_id);
    $('#op_no_search').val(uhid);
    $('#' + itemCodeListDivId).hide();
}
/**
 *
 * OP NUMBER SEARCH ENDS
 */


//**************** IP NUMBER SEARCH STARTS **************************

$('#ip_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(event.keyCode);
    var url = $("#ins_base_url").val();
    var send_url = url + "/insurance_currency/commonSearch";
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var ip_no_search = $(this).val();
        ip_search = ip_no_search.trim();
        if (ip_search == "") {
            $("#ip_no_searchCodeAjaxDiv").html("");
            $("#ip_no_search_hidden").val("");
        } else {
            try {
                var url = '';
                var paramts = {ip_no_search: ip_no_search,ip_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url: send_url,
                    data: paramts,
                    beforeSend: function () {
                        $("#ip_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#ip_no_searchCodeAjaxDiv").html(html).show();
                        $("#ip_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('ip_no_searchCodeAjaxDiv', event);
    }
});
$('#ip_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('ip_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_ip_id(list, admission_id,uhid) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#ip_no_search').val(admission_id);
    $('#ip_no_search_hidden').val(uhid);
    $('#' + itemCodeListDivId).hide();
}
//************************* IP NUMBER SEARCH ENDS *********************************************
$('#bill_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = String.fromCharCode(event.keyCode);
    var url = $("#ins_base_url").val();
    var send_url = url + "/insurance_currency/commonSearch";
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var bill_no_search = $(this).val();
        bill_search = bill_no_search.trim();
        if (bill_search == "") {
            $("#bill_no_searchCodeAjaxDiv").html("");
            $("#bill_no_search_hidden").val("");
        } else {
            try {
                var param = {bill_no_search: bill_no_search,bill_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url: send_url,
                    data: param,
                    beforeSend: function () {
                        $("#bill_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#bill_no_searchCodeAjaxDiv").html(html).show();
                        $("#bill_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    }
});
$('#bill_no').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('bill_no_searchCodeAjaxDiv');
        return false;
    }
});
function fillBillValues(list, bill_no,bill_date, net_amount) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#bill_no_hidden').val(bill_no);
    $('#bill_no').val(bill_no);
    $('#bill_no_searchCodeAjaxDiv').hide();
}

function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid || e.value == 0) {
        e.value = val.substring(0, val.length - 1);
    }
}

function searchInsuranceCurrencyData(limit = 100, offset = 0,){
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    url = url + "/insurance_currency/insuranceSettlementCurrencyList";
    var op_no_search_hidden = $("#op_no_search_hidden").val();
    var ip_no_search_hidden = $("#ip_no_search_hidden").val();
    var bill_no = $("#bill_no").val();
    var company = $("#company").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var param = {_token: file_token,op_no_search_hidden:op_no_search_hidden,ip_no_search_hidden:ip_no_search_hidden,bill_no:bill_no,company:company,from_date:from_date,to_date:to_date,limit,offset}
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            var obj = JSON.parse(msg);
            msg = obj.viewData;
            total_rec = obj.total_rec;
            if (offset == 0) {
                    $('#insurancelistdiv').html(msg);
            } else {
                    $('#load_data').find('.table tbody').append(msg);
            }
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
function viewpreAuth(bill_id,bill_from,uhid,ind){
    filename_count = 0;
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/viewPreAuth";
    var param = {bill_id:bill_id,bill_from:bill_from,ind:ind,uhid:uhid};
    $.ajax({
        type: "POST",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $("#model_pre_auth_div").html(msg)
            $("#pre_auth_modal").modal('show');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");
            $('.ins_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
function viewpreAuthRequest(bill_id,bill_from,patid,comp_id,patient_name,consulting_doctor_id,ind){
    filename_count = 0;
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/viewPreAuthReq";
    var param = {bill_id:bill_id,bill_from:bill_from,ind:ind,patid:patid,comp_id:comp_id,patient_name:patient_name,consulting_doctor_id:consulting_doctor_id};
    $.ajax({
        type: "POST",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $("#model_pre_auth_req_div").html(msg)
            $("#pre_auth_req_modal").modal('show');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");
            $('.ins_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
function viewpreAuthRequestUpdate(bill_id,bill_from,patid,comp_id,patient_name,consulting_doctor_id,ind){
    filename_count = 0;
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/viewPreAuthReqUpdate";
    var param = {bill_id:bill_id,bill_from:bill_from,ind:ind,patid:patid,comp_id:comp_id,patient_name:patient_name,consulting_doctor_id:consulting_doctor_id};
    $.ajax({
        type: "POST",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $("#model_pre_auth_req_div_update").html(msg)
            $("#pre_auth_req_modal_update").modal('show');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");
            $('.ins_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
function viewpreAuthUpdate(bill_id,bill_from,patid,comp_id,patient_name,consulting_doctor_id,ind){
    filename_count = 0;
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/viewpreAuthUpdate";
    var param = {bill_id:bill_id,bill_from:bill_from,ind:ind,patid:patid,comp_id:comp_id,patient_name:patient_name,consulting_doctor_id:consulting_doctor_id};
    $.ajax({
        type: "POST",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $("#model_pre_auth_div_update").html(msg)
            $("#pre_auth_modal_update").modal('show');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");
            $('.ins_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}

$("#savePreAuthDocumentsForm").on('submit', function(e) {
    e.preventDefault();
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/savePreAuthData";

        $.ajax({
            url: send_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('#uploadDocumentBtn1').attr('disabled', true);
                $('#uploadDocumentSpin').removeClass("fa fa-save");
                $('#uploadDocumentSpin').addClass("fa fa-spinner fa-spin");
                $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function(data) {
                if (data) {
                    toastr.success("Successfully Added");
                    searchInsuranceCurrencyData();
                }
            },
            complete: function() {
                $('#uploadDocumentSpin').removeClass("fa fa-spinner fa-spin");
                $('#uploadDocumentSpin').addClass("fa fa-save");
                $('#insurancelistdiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error(
                    "Error Please Check Your Internet Connection and Try Again"
                );
            }
        });


});
$("#savePreAuthReqDocumentsForm").on('submit', function(e) {
    e.preventDefault();
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/savePreAuthDataReq";

        $.ajax({
            url: send_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('#uploadDocumentBtnReq').attr('disabled', true);
                $('#uploadDocumentSpin').removeClass("fa fa-save");
                $('#uploadDocumentSpin').addClass("fa fa-spinner fa-spin");
                $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function(data) {
                if (data) {
                    toastr.success("Successfully Added");
                    searchInsuranceCurrencyData();
                }
            },
            complete: function() {
                $('#uploadDocumentSpin').removeClass("fa fa-spinner fa-spin");
                $('#uploadDocumentSpin').addClass("fa fa-save");
                $('#insurancelistdiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error(
                    "Error Please Check Your Internet Connection and Try Again"
                );
            }
        });


});
function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function viewpreAuthFinal(bill_id,bill_from,uhid,ind){
    filename_count = 0;
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/viewPreAuthFinal";
    var param = {bill_id:bill_id,bill_from:bill_from,ind:ind,uhid:uhid};
    $.ajax({
        type: "POST",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $("#model_pre_auth_div_final").html(msg)
            $("#pre_auth_modal_final").modal('show');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");
            $('.ins_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
$("#saveAuthDocumentsFormFinal").on('submit', function(e) {
    e.preventDefault();
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/savePreAuthDataFinal";

        $.ajax({
            url: send_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('#uploadDocumentBtn2').attr('disabled', true);
                $('#uploadDocumentSpin2').removeClass("fa fa-save");
                $('#uploadDocumentSpin2').addClass("fa fa-spinner fa-spin");
                $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function(data) {
                if (data) {
                    toastr.success("Successfully Added");
                    searchInsuranceCurrencyData();
                }
            },
            complete: function() {
                $('#uploadDocumentSpin2').removeClass("fa fa-spinner fa-spin");
                $('#uploadDocumentSpin2').addClass("fa fa-save");
                $('#insurancelistdiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error(
                    "Error Please Check Your Internet Connection and Try Again"
                );
            }
        });


});
function viewpreAuthUpdateFinal(bill_id,bill_from,patid,comp_id,patient_name,consulting_doctor_id,ind){
    filename_count = 0;
    var url = $("#ins_base_url").val();
    var file_token = $('#hidden_filetoken').val();
    send_url = url + "/insurance_currency/viewpreAuthUpdateFinal";
    var param = {bill_id:bill_id,bill_from:bill_from,ind:ind,patid:patid,comp_id:comp_id,patient_name:patient_name,consulting_doctor_id:consulting_doctor_id};
    $.ajax({
        type: "POST",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#insurancelistdiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (msg) {
            $("#model_pre_auth_div_update_final").html(msg)
            $("#pre_auth_modal_update_final").modal('show');
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);

        },
        complete: function () {
            $('#insurancelistdiv').LoadingOverlay("hide");
            $('.ins_datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });

        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
function fileuploadDiv(e){
    filename_count = parseFloat(filename_count) + parseFloat($(e).length);
    filename_count = parseFloat(filename_count)+1;
    // $("#file_count").html(filename_count+" files selected");
    $(".fileUploaddiv").append('<div class="col-xs-4 padding_sm clear_'+filename_count+'" style="padding-top: 10px !important">'+
        '<div class="fileUpload">'+
            '<input type="file" class="btn-success" name="upload_document[]" id="upload_document" multiple " >'+
        '</div> </div>'+
    '<div class="col-xs-2 padding_sm clear_'+filename_count+'" style="padding-top: 10px !important" id="file_upload_append">'+
            '<span id ="file_count"></span> <span onclick="clearAllFiles('+ filename_count +')" style="color:red;padding-left:10px;cursor:pointer;" id="clear_all_files">Delete</span></div>');
    $("#clear_all_files").show();
}
function clearAllFiles(filename_count){
    $(".clear_"+filename_count).remove();
}
function clearFirstFiles(e){
    $('#upload_document_main').val('');
}
