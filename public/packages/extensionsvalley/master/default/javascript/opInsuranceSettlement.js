$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    searchOpInsuranceSettlement();
    $("#uploadBulkAppoinments").on("submit", function (e) {
        e.preventDefault();
        var file_data = $('#upload_document');
        if (file_data) {
            var image_vai = validateDoc("upload_document");
            if (image_vai) {
                var url = base_url + "/insuranceSettlement/uploadOPInsuSettleForm";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $("#uploadBulkAppoinmetsBtn").attr("disabled", true);
                        $("#uploadBulkAppoinmetsSpin").removeClass("fa fa-upload");
                        $("#uploadBulkAppoinmetsSpin").addClass("fa fa-spinner fa-spin");
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            document.getElementById('uploadBulkAppoinments').reset();
                            searchOpInsuranceSettlement();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $("#uploadBulkAppoinmetsBtn").attr("disabled", false);
                        $("#uploadBulkAppoinmetsSpin").removeClass("fa fa-spinner fa-spin");
                        $("#uploadBulkAppoinmetsSpin").addClass("fa fa-upload");
                    },
                    error: function () {
                        toastr.error(
                            "Error Please Check Your Internet Connection and Try Again"
                        );
                    },
                });
            } else {
                bootbox.alert(
                    "<strong>File criteria.</strong><br>Extensions: .xlsx <br>Size : Maximum 5 MB"
                );
            }
        } else {
            toastr.warning("Please enter a file");
        }
    });
});

var base_url = $('#base_url').val();
var token = $('#token').val();



function searchOpInsuranceSettlement() {
    var url = base_url + "/insuranceSettlement/searchOpInsuranceSettlement";
    var from_date = $('#Settled_from_date').val();
    var to_date = $('#Settled_to_date').val();
    var bill_no_tag = $('#bill_no_tag').val();
    var bill_id_hidden = $('#bill_id_hidden').val();
    var insurance_company = $('#insurance_company').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { from_date: from_date, to_date: to_date, bill_no_tag: bill_no_tag, bill_id_hidden: bill_id_hidden, insurance_company: insurance_company },
        beforeSend: function () {
            $("#searchOpInsuranceSettlementBtn").attr("disabled", true);
            $("#searchOpInsuranceSettlementSpin").removeClass("fa fa-search");
            $("#searchOpInsuranceSettlementSpin").addClass("fa fa-spinner fa-spin");
            $("#OpInsuranceSettlementDiv").LoadingOverlay("show", { background: "rgba(89, 89, 89, 0.6)", imageColor: '#337AB7' });
        },
        success: function (html) {
            $('#OpInsuranceSettlementDiv').html(html);
        },
        complete: function () {
            $("#searchOpInsuranceSettlementBtn").attr("disabled", false);
            $("#searchOpInsuranceSettlementSpin").removeClass("fa-spinner fa-spin");
            $("#searchOpInsuranceSettlementSpin").addClass("fa fa-search");
            $("#OpInsuranceSettlementDiv").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error(
                "Error Please Check Your Internet Connection and Try Again"
            );
        },
    });
}


function removeAllWhiteSpace(s) {
    s = s.replace(/(\d)[\s.]+(?=\d)/g, '$1');
    s = s.replace(/(?<=\d)[\s.]+(?=\d)/g, '');
    return s;
}

function resetsearchForm() {
    var first_date = $('#first_date').val();
    var last_date = $('#last_date').val();
    $('#Settled_from_date').val(first_date);
    $('#Settled_to_date').val(last_date);
    $('#bill_no').val('');
    $('#bill_no_tag').val('');
    $('#bill_id_hidden').val('');
    $('#insurance_company').val('All');
    $('#insurance_company').select2();
    searchOpInsuranceSettlement();
}


$('#bill_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('bill_no_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#bill_no').val();
        if (!bill_no) {
            $('#bill_no_AjaxDiv').hide();
            $("#bill_no_tag").val('');
            $("#bill_id_hidden").val('');
        } else {
            var url = base_url + "/insuranceSettlement/searchInsuranceBillNo";
            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, bill_no: bill_no },
                beforeSend: function () {
                    $("#bill_no_AjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#bill_no_AjaxDiv").html(html).show();
                    $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('bill_no_AjaxDiv', event);
    }
});


function fillBillNo(bill_id, bill_no, bill_tag) {
    $("#bill_id_hidden").val(bill_id);
    $("#bill_no").val(bill_no);
    $("#bill_no_tag").val(bill_tag);
    $('#bill_no_AjaxDiv').hide();
}

function validateDoc(input_id) {
    var fileInput = document.getElementById(input_id);
    if (fileInput) {
        var filePath = fileInput.value;
        if (filePath) {
            var allowedExtensions = /(\.xlsx)$/i;
            var file_size = fileInput.size;
            if (!allowedExtensions.exec(filePath) || file_size > 5150) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function downloadUploadFormat() {
    var url = base_url + "/insuranceSettlement/downloadOPInsuSettleForm";
    var from_date = $('#from_download_format').val();
    var to_date = $('#to_download_format').val();
    $.ajax({
        type: "POST",
        url: url,
        data: { from_date: from_date, to_date: to_date },
        beforeSend: function () {
            $("#downloadUploadFormatBtn").attr("disabled", true);
            $("#downloadUploadFormatSpin").removeClass("fa fa-download");
            $("#downloadUploadFormatSpin").addClass("fa fa-spinner fa-spin");
        },
        success: function (html) {
            if (html.trim() != 'NODATA') {
                html = removeAllWhiteSpace(html)
                var download_file = base_url + '/packages/extensionsvalley/insuranceSettlement/' + html;
                $('#downloadfile').attr('href', download_file);
                $('#downloadfile')[0].click();
            } else {
                toastr.warning("No records found");
            }
        },
        complete: function () {
            $("#downloadUploadFormatBtn").attr("disabled", false);
            $("#downloadUploadFormatSpin").removeClass("fa-spinner fa-spin");
            $("#downloadUploadFormatSpin").addClass("fa fa-download");
        },
        error: function () {
            toastr.error(
                "Error Please Check Your Internet Connection and Try Again"
            );
        },
    });
}




