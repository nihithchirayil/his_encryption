

    $(document).ready(function () {

        setTimeout(function(){
          // clearselect2();
           $(".select2").select2({ placeholder: "", maximumSelectionSize: 10 });
        }, 2000);

        $('.monthpicker').datetimepicker({
                format: 'MMM-YYYY'
        });



//fixed header script ends here
        setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        }, 300);
        //$(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
        $("input[data-attr='datetime']").datetimepicker({ format: 'MMM-DD-YYYY hh:mm:A' });
    });

    function changeTds(obj,id,doctor_id){
        var value = $(obj).next().val();
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/DoctorSalarychangeTds';
        var current_net_amount = $('#tds_span_'+id).parent().parent().find('.net_amount').text();
        var tds_amount = (parseFloat(current_net_amount)*parseFloat(value)/100);
        var balance_amount = parseFloat(current_net_amount) - parseFloat(tds_amount);
        var dataparams = {
            'id': doctor_id,
            'tds': value
        }
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $(".tds").find("[data-attr-id='" + id + "']").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data !=0){
                    // bootbox.alert("Insurance amount updated successfully!");
                    $('.tds_text').css('display', 'none');
                    $('.tds_check').css('display', 'none');
                    $('.tds_span').css('display', 'block');
                    $('#tds_span_'+id).html(value);
                    $('#tds_span_'+id).parent().parent().find('.balance_amount').text(balance_amount);

                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $(".tds").find("[data-attr-id='" + id + "']").LoadingOverlay("hide");
            }
        });
    }

    function changeFixedSalary(obj,id){
        var value = $(obj).next().val();
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/DoctorSalarychangeFixedSalary';
        var previous_amount = $('#fixed_salary_span_'+id).html();
        var current_net_amount = $('#fixed_salary_span_'+id).parent().parent().find('.net_amount').text();
        var new_net_amount = parseFloat(current_net_amount) - parseFloat(previous_amount) + parseFloat(value);
        var dataparams = {
            'id': id,
            'new_amount': value
        }
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $(".fixed_salary").find("[data-attr-id='" + id + "']").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data !=0){
                    // bootbox.alert("Insurance amount updated successfully!");
                    $('.fixed_salary_text').css('display', 'none');
                    $('.fixed_salary_check').css('display', 'none');
                    $('.fixed_salary_span').css('display', 'block');
                    $('#fixed_salary_span_'+id).html(value);
                    $('#fixed_salary_span_'+id).parent().parent().find('.net_amount').text(new_net_amount);

                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $(".fixed_salary").find("[data-attr-id='" + id + "']").LoadingOverlay("hide");
            }
        });
    }

    function changeAdditionalAmount(obj,id){
        var value = $(obj).next().val();
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/DoctorSalarychangeAmountAmount';
        var previous_amount = $('#additional_amount_span_'+id).html();
        var current_net_amount = $('#additional_amount_span_'+id).parent().parent().find('.net_amount').text();
        var new_net_amount = parseFloat(current_net_amount) - parseFloat(previous_amount) + parseFloat(value);
        var dataparams = {
            'id': id,
            'new_amount': value
        }
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $(".additional_amount").find("[data-attr-id='" + id + "']").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data !=0){
                    // bootbox.alert("Insurance amount updated successfully!");
                    $('.additional_amount_text').css('display', 'none');
                    $('.additional_amount_check').css('display', 'none');
                    $('.additional_amount_span').css('display', 'block');
                    $('#additional_amount_span_'+id).html(value);
                    $('#additional_amount_span_'+id).parent().parent().find('.net_amount').text(new_net_amount);

                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $(".additional_amount").find("[data-attr-id='" + id + "']").LoadingOverlay("hide");
            }
        });
    }




    function changeInsuranceAmount(obj,id){
        var value = $(obj).next().val();
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/DoctorSalarychangeInuranceAmount';
        var previous_amount = $('#insurance_amount_span_'+id).html();
        var current_net_amount = $('#insurance_amount_span_'+id).parent().parent().find('.net_amount').text();
        var new_net_amount = parseFloat(current_net_amount) - parseFloat(previous_amount) + parseFloat(value);
        var dataparams = {
            'id': id,
            'new_amount': value
        }
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $(".insurance_amount").find("[data-attr-id='" + id + "']").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data !=0){
                    // bootbox.alert("Insurance amount updated successfully!");
                    $('.insurance_amount_text').css('display', 'none');
                    $('.insurance_amount_check').css('display', 'none');
                    $('.insurance_amount_span').css('display', 'block');
                    $('#insurance_amount_span_'+id).html(value);
                    $('#insurance_amount_span_'+id).parent().parent().find('.net_amount').text(new_net_amount);
                    $(".insurance_amount").find("[data-attr-id='" + id + "']").val(value);

                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $(".insurance_amount").find("[data-attr-id='" + id + "']").LoadingOverlay("hide");
            }
        });
    }

    function remove_service(service_id,service_desc){
        $('.added-services').find("[data-service-id='"+service_id+"']").remove();
        $('#services_list_block').find("[data-attr-desc='"+service_desc+"']").prop('checked', false);
    }

    function add_or_remove_service(service_id,service_desc){
        if($('#service_'+service_id).is(":checked")== false){
            $('.added-services').find('div').each(function(){
                if($(this).attr('data-service-desc') == service_desc){
                    $(this).remove();
                }
            });
        }else{
            var exist = 0
            $('.added-services').find('div').each(function(){
                if($(this).attr('data-service-desc')== service_desc){
                    exist = 1;
                }
            });
            if(exist == 0){
                var html = "<div class='col-md-12 added_service_item' style='font-size: 11px;border-bottom: 1px solid #d8d8d8;font-weight: 600;color:black;' data-service-id='"+service_id+"' data-service-desc='"+service_desc+"' data-service-percentage='0'><input type='hidden' name='added_services[]' value='"+service_id+"'>"+service_desc+"<button type='button' name='remove_service' value='Remove' class='btn pull-right btn-danger' onclick='remove_service(\""+service_id+"\",\""+service_desc+"\");'><i class='fa fa-times-circle' aria-hidden='true'></i></button></div>";
                $('.added-services').append(html);
            }
        }
    }

    function select_sub_department(id){
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/DoctorSalarySelectSubDepartment';
        var dataparams = {
            'department_id':id,
        };
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $('.available-services').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data==0){
                    $('#select_sub_department').html('<option value="">select sub department</option>');
                }else{
                    var obj = atob(data);
                    $('.available-services').LoadingOverlay("hide");
                    $('#select_sub_department').html(obj);
                }
            },
            complete: function () {
                $('.available-services').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            }
        });
    }
    function select_percentage_services(id){
        var base_url = $('#base_url').val();
        var doctor_id = $('#percentage_doctor_id').val();
        var doctor_name = $('#percentage_doctor_name').val();
        var url = base_url+'/flexy_report/DoctorSalarySelectServicesUsingDepartment';
        var added_service_list = [];
        $('.added-services').find('div').each(function(){
            added_service_list.push($(this).attr('data-service-desc'));
        });
        var dataparams = {
            'sub_department_id':$('#select_sub_department').val(),
            'department_id':$('#select_department').val(),
            'doctor_id':doctor_id,
            'doctor_name':doctor_name,
            'added_service_list':added_service_list
        };
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $('.available-services').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data==0){
                    bootbox.alert("No services found!");
                    $('.available-services').LoadingOverlay("hide");
                }else{
                    $('.available-services').html(data);
                }
            },
            complete: function () {
                $('.available-services').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            }
        });
    }

    function add_percentage(){
        var doctor_id = $('#percentage_doctor_id').val();
        var doctor_name = $('#percentage_doctor_name').val();
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/DoctorSalaryAddPercentage';
        var dataparams = {
            'doctor_id': doctor_id,
            'doctor_name': doctor_name,
        };
        $.ajax({
            url: url,
            type: 'GET',
            data: dataparams,
            beforeSend: function () {
                $('#modal_add_service').modal('show');
                $('#add_service_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#add_service_data').html(data);
            },
            complete: function () {
                $('#add_service_data').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });

                $('#select_all_services').change(function(){
                    if($(this).is(":checked")){
                        $("#services_list_block INPUT[type='checkbox']").prop('checked', true);
                    }else{
                        $("#services_list_block INPUT[type='checkbox']").prop('checked', false);
                    }
                });

            }
        });

    }


    function recalculateActualAmount(percentage,net_amount,id){
        var actual_amount = (net_amount*percentage)/100;
        $('#actual_amt_'+id).text(actual_amount);
        $('#dr_percentage_label_'+id).text(percentage);
    }

    function save_percentage(){
        var services = [];
        var percentage = [];
        var doctor_id = $('#proceure_save_doctor_id').val();
        var base_url = $('#base_url').val();
        $('.added_service_item').each(function(){
            services.push($(this).attr('data-service-desc'));
            percentage.push($(this).attr('data-service-percentage'));
        });
            services = btoa(services);
        var url = base_url+'/flexy_report/saveDrSalaryPercentage';
        $.ajax({
            type: "GET",
            url: url,
            data: 'services=' + services + '&doctor_id=' + doctor_id + '&percentage=' + percentage,
            beforeSend: function () {
                $('#add_service_data').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#add_service_data').LoadingOverlay("hide");
                if(data == 1){
                    bootbox.alert("Percentage saved successfully!");
                    $('#modal_add_service').modal('hide');
                    procedure_details(procedure_salary_id,procedure_doctor_id);
                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $('#add_service_data').LoadingOverlay("hide");
            },

        });

    }



    //----Hidden Filed Search--------------------------------------------------------------------

    $('.hidden_search').keyup(function (event) {
        var input_id = '';
        var base_url = $('#base_url').val();
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        input_id = $(this).attr('id');

        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            var department_hidden = $('#department_hidden').val();
            var datastring = '';
            if (input_id == 'sub_department') {
                datastring = '&department_id=' + department_hidden;
            }
            if (input_id == 'scheme') {
                datastring = '&company_id=' + company_hidden;
            }

            if (search_key == "") {
                $("#" + input_id + "AjaxDiv").html("");
                $("#" + input_id + "AjaxDiv").hide();

            } else {
                var url = base_url+'/flexy/FlexyReportProgressiveSearch';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                    beforeSend: function () {

                        $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    },
                    error: function () {
                        bootbox.alert('nw error');
                        return;
                    }

                });
            }
        } else {
            ajax_list_key_down(input_id + 'AjaxDiv', event);
        }
    });

    function fillSearchDetials(id, name, serach_key_id) {
        $('#' + serach_key_id + '_hidden').val(id);
        $('#' + serach_key_id).val(name);
        $('#' + serach_key_id).attr('title', name);
        $("#" + serach_key_id + "AjaxDiv").hide();
    }


    /* setting for enter key press in ajaxDiv listing */
    $(".hidden_search").on('keydown', function (event) {
        var input_id = '';
        input_id = $(this).attr('id');
        if (event.keyCode === 13) {
            ajaxlistenter(input_id + 'AjaxDiv');
            return false;
        }
    });


$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

    if(category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                $( "#sub_category" ).prop( "disabled", true ); //Disable
            },
            success: function (html) {
                if(html){
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub catogery</option>');
                    $.each(html,function(key,value){
                        $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                    });
                }else{
                    $("#sub_category").empty();
                }
            },

            error: function () {
                Command: toastr["error"]("Please check your internet connection and try again!");
                $( "#sub_category" ).prop( "disabled", false );
            },
            complete: function () {
                $('#warning1').remove();
                $( "#sub_category" ).prop( "disabled", false );
            }

        });

    }else{
            $("#category").focus();
            $("#sub_category").empty();
        }

    });


    //---------generate csv---------------
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}



//---------generate csv ends---------


//---------print report--------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}

 function print_generate(printData) {

    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');
    $('#total_row').css('display', '');
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById('consolidated_salary_results');
    showw = showw + msglist.innerHTML;



    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }

    mywindow.document.write(
        '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
    );
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
    );
    mywindow.document.write(
        '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
    );
    mywindow.document.write(
        '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
    );

    mywindow.document.write(showw);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function() {
        mywindow.print();
        mywindow.close();
        InitTheadScroll();
        $('#total_row').css('display', 'none');
    }, 1000);


    return true;
}

function InitTheadScroll() {
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
}

$("button[type='reset']").on('click', function(evt) {
        $(".select2").val([]).select2();
        $(".filters").val('');
        $('#ResultsViewArea').html('');
        $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        window.location.reload();

});

    function clearselect2() {
        $('select').select2({
                placeholder: '',
                allowClear: true,
        });
    }


    function bill_analysis_detail(salary_id,doctor_id){
        var salary_month = $('#month').val();
        var url = '';
        $.ajax({
            type: "GET",
            url: '',
            data: 'is_bill_analysis=1' + '&doctor_id=' + doctor_id + '&salary_month=' + salary_month+'&salary_id='+salary_id,
            beforeSend: function () {

                $('#modal_bill_analysis').modal('show');
                $("#bill_analysis_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                if(html == 0){
                    $("#bill_analysis_data").LoadingOverlay("hide", true);
                    $('#modal_bill_analysis').modal('hide');
                    bootbox.alert("No data found!");
                }else{
                    $('#bill_analysis_data').html(html);
                }
            },
            complete: function () {
                $("#bill_analysis_data").LoadingOverlay("hide", true);
                $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                });
            }
        });
    }

    var procedure_salary_id = 0;
    var procedure_doctor_id = 0;

    function procedure_details(salary_id,doctor_id){
        procedure_salary_id = salary_id;
        procedure_doctor_id = doctor_id;
        var salary_month = $('#month').val();
        var url = '';
        $.ajax({
            type: "GET",
            url: '',
            data: 'is_procedure_details=1' + '&doctor_id=' + doctor_id + '&salary_month=' + salary_month+'&salary_id='+salary_id,
            beforeSend: function () {

                $('#modal_procedure_details').modal('show');
                $("#procedure_details_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                if(html == 0){
                    $("#procedure_details_data").LoadingOverlay("hide", true);
                    $('#modal_procedure_details').modal('hide');
                    bootbox.alert("No data found!");
                }else{
                    $('#procedure_details_data').html(html);
                }
            },
            complete: function () {
                $("#procedure_details_data").LoadingOverlay("hide", true);
                $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                });

                $('.doctor_procedure_percentage').keyup(function(event){
                    if($(this).val()>100){
                        $(this).val(100);
                    }
                });

                $('.doctor_procedure_percentage').keydown(function(event){
                    if(event.keyCode == 8 || event.keyCode == 46)
                        return true;
                    if(event.keyCode >= 96 && event.keyCode <= 105)
                        return true;

                    if(isNaN(parseInt(String.fromCharCode(event.keyCode),10)))
                    return false;
                });
            }
        });
    }

    function op_details(doctor_id){
       // alert('ddd');
       var base_url = $('#base_url').val();
       var token = $('#token').val();
       var url = base_url+'/flexy_report/DoctorSalaryOpDetails';
        var salary_month = $('#month').val();
        var dataparams = {
            'token':token,
            'is_op_details': 1,
            'doctor_id': doctor_id,
            'salary_month': salary_month
        }
        $.ajax({
            type: "POST",
            url: url,
            data: dataparams,
            beforeSend: function () {

                $('#modal_op_details').modal('show');
                $("#op_details_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                if(html == 0){
                    $("#op_details_data").LoadingOverlay("hide", true);
                    $('#modal_op_details').modal('hide');
                    bootbox.alert("No data found!");
                }else{
                    $('#op_details_data').html(html);
                }
            },
            complete: function () {
                $("#op_details_data").LoadingOverlay("hide", true);
                $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                });

                setTimeout(function() {
                    recalculate_op_amount();
                },500);

            }
        });
    }
    function ot_details(doctor_id){
       // alert('ddd');
       var base_url = $('#base_url').val();
       var token = $('#token').val();
       var url = base_url+'/flexy_report/DoctorSalaryOtDetails';
        var salary_month = $('#month').val();
        var dataparams = {
            'token':token,
            'is_ot_details': 1,
            'doctor_id': doctor_id,
            'salary_month': salary_month
        }
        $.ajax({
            type: "POST",
            url: url,
            data: dataparams,
            beforeSend: function () {

                $('#modal_ot_details').modal('show');
                $("#ot_details_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                if(html == 0){
                    $("#ot_details_data").LoadingOverlay("hide", true);
                    $('#modal_ot_details').modal('hide');
                    bootbox.alert("No data found!");
                }else{
                    $('#ot_details_data').html(html);
                }
            },
            complete: function () {
                $("#ot_details_data").LoadingOverlay("hide", true);
                $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                });

            }
        });
    }

    function ip_details(doctor_id){
        var salary_month = $('#month').val();
        var base_url = $('#base_url').val();
        var token = $('#token').val();
        var url = base_url+'/flexy_report/DoctorSalaryIpDetails';
        var dataparams = {
            'token':token,
            'is_op_details': 1,
            'doctor_id': doctor_id,
            'salary_month': salary_month
        }
        $.ajax({
            type: "POST",
            url: url,
            data: dataparams,
            beforeSend: function () {

                $('#modal_ip_details').modal('show');
                $("#ip_details_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                if(html == 0){
                    $("#ip_details_data").LoadingOverlay("hide", true);
                    $('#modal_ip_details').modal('hide');
                    bootbox.alert("No data found!");
                }else{
                    $('#ip_details_data').html(html);
                }
            },
            complete: function () {
                $("#ip_details_data").LoadingOverlay("hide", true);
                $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                });

            }
        });
    }

    function change_percentage(current_percentage,net_amount,id, percentage){
        var url = '';
        var total_amt = $('#total_amt').val();
        var udpated_amt = (net_amount*percentage)/100;
        var total_percentage = $('#total_percentage').val();
        var salary_id = $('#salary_id').val();
        $('#bill_analysis_dr_percentage_label_'+id).val(percentage);

        var new_amt = total_percentage-current_percentage+udpated_amt;
        $.ajax({
            type: "GET",
            url: '',
            data: 'is_change_percentage=1' + '&id=' + id + '&percentage=' + percentage+'&new_amt='+new_amt+'&salary_id='+salary_id,

            beforeSend: function () {

            },
            success: function (html) {
                if(html ==1){
                    bootbox.alert("Bill analysis updated successfully!");
                    getResultData();
                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $('#modal_bill_analysis').modal('hide');
            }

        });
    }

    function UpdateProcedureServices(){
        services_list = [];
        percentage_list = [];
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/UpdateProcedureServices';
        var doctor_id = procedure_doctor_id;
        var salary_month = $('#month').val();
        $('#procedure_services_table tbody tr').find('input[type="text"]').each(function(){
            services_list.push($(this).attr('data-attr-id'));
            percentage_list.push($(this).val());
        });

        var dataparams = {
            'services_list': services_list,
            'percentage_list': percentage_list,
            'doctor_id': doctor_id,
            'salary_month': salary_month,
        };

        $.ajax({
            type: "GET",
            url: url,
            data: dataparams,
            beforeSend: function () {
                $("#procedure_details_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                $("#procedure_details_data").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
                if(html ==1){
                    $("#procedure_details_data").LoadingOverlay("hide");
                    bootbox.alert("Updated successfully!");
                }else{
                    bootbox.alert("Something went wrong!");
                }
            },
            complete: function () {
                $("#procedure_details_data").LoadingOverlay("hide");
            }

        });
    }

    function RoundNum(number){
        var c = number % 1;
        return number-c+(c/1+1.5>>1)*1
    }

    function printDiv(printData) {

        $('#resultDataTable').css('width', '98.5%');
        $(".theadfix_wrapper").floatThead('destroy');
        $('.theadscroll').perfectScrollbar('destroy');
        $('#total_row').css('display', '');
        //$('#print_config_modal').modal('hide');
        var printMode = $('input[name=printMode]:checked').val();
        //alert('dddd'); return;
        var showw = "";

        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);

        var mywindow = window.open('', 'my div', 'height=3508,width=2480');

        var msglist = document.getElementById(printData);
        showw = showw + msglist.innerHTML;



        if (printMode == 1) {
            mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
        } else {
            mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
        }

        mywindow.document.write(
            '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
        );
        mywindow.document.write(
            '<style>#tblQPVE{border-bottom: 1px solid black !important;color:#3b3d3d !important;}</style>');
        mywindow.document.write(
            '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
        mywindow.document.write(
            '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
        );
        mywindow.document.write(
            '<style>thead{background-color:#02967f !important;color:black !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
        );
        mywindow.document.write(
            '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
        );
        mywindow.document.write(
            '<style>.doctor_procedure_percentage {display:none !important;}</style>'
        );
        mywindow.document.write(
            '<style>.doctor_procedure_percentage_label {display:block !important;}</style>'
        );
        mywindow.document.write(
            '<style>.bill_analysis_dr_percentage {display:none !important;}</style>'
        );
        mywindow.document.write(
            '<style>.bill_analysis_dr_percentage_label {display:block !important;}</style>'
        );

        mywindow.document.write(showw);

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        setTimeout(function() {
            mywindow.print();
            mywindow.close();
            InitTheadScroll();
            $('#total_row').css('display', 'none');
            $('#doctor_procedure_percentage_label').css('display', 'none');
            $('#doctor_procedure_percentage').css('display', 'block');
            $('#bill_analysis_dr_percentage_label').css('display', 'none');
            $('#bill_analysis_dr_percentage').css('display', 'block');
        }, 1000);


        return true;
    }

    function recalculate_op_amount(){
       // console.log('ddd');
        var op_amount = $('#op_amount_hidden').val();
        var op_percentage = $('#op_percentage').val();
        var op_calculated_amount = $('#op_calculated_amount').val();
            op_calculated_amount = (parseFloat(op_percentage)*parseFloat(op_amount))/100;
            $('#op_calculated_amount').val(op_calculated_amount);
    }

    function save_calculated_op_amount(){
        var base_url = $('#base_url').val();
        var op_percentage = $('#op_percentage').val();
        var op_calculated_amount = $('#op_calculated_amount').val();
        var doctor_id =  $('#op_doctor_id_hidden').val();
        var salary_month =  $('#op_salary_month_hidden').val();
        var url = base_url+'/flexy_report/recalculateOpAmount';
        var dataparams = {
            'op_percentage':op_percentage,
            'op_calculated_amount':op_calculated_amount,
            'doctor_id':doctor_id,
            'salary_month':salary_month,
        };

        $.ajax({
            type: "GET",
            url: url,
            data: dataparams,
            beforeSend: function () {
                $("#modal_op_details").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {

                if(html == 1){

                    bootbox.alert("Updated successfully!");
                }else{
                    bootbox.alert("Something went wrong!");
                }

            },
            complete: function () {

                $("#modal_op_details").LoadingOverlay("hide");
            }

        });
    }

    function salaryRecalculate(doctor_id){
        var salary_month = $('#month').val();
        var base_url = $('#base_url').val();
        var url = base_url+'/flexy_report/Salaryrecalculate';
        var dataparams = {
            'doctor_id':doctor_id,
            'salary_month':salary_month,
        };
        $.ajax({
            type: "GET",
            url: url,
            data: dataparams,
            beforeSend: function () {
                $('#recalculate_spin_'+doctor_id).addClass('fa-spin');
            },
            success: function (html) {
                if(html == 1){
                    bootbox.alert("Recalculated successfully!");
                }else{
                    bootbox.alert("Something went wrong!");
                }

            },
            complete: function () {
                $('#recalculate_spin_'+doctor_id).removeClass('fa-spin');
            }

        });

    }
