$(document).ready(function () {

    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
});

function checkSumWidth(){
    var sum = 0;
    var current = $('#custom_column_width').val();
    var current_array = current.split(',');
    for(var i=0;i<current_array.length;i++){
        sum = sum + parseInt(current_array[i]);
    }
    $('#sum_width').val(sum);
}


function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function exicuteQuery() {
    var sql = $('#flexy_report_sql').val();
    var report_name = $('#report_name').val();
    var selected = $("input[type='radio'][name='access_type']:checked");
    var access_type = selected.val();
    var group_id = $('select#groups').val();
    var users = $('select#users').val();
    var flexy_report_totals = $('#flexy_report_totals').val();
    var custom_column_width = $('#custom_column_width').val();
    var drill_down_sql = $('#drill_down_sql').val();
    var drill_down_columns = $('#drill_down_columns').val();
    var numeric_colums = $('#numeric_colums').val();
    var report_id = $('#report_id').val();
    var has_drill_down = 0;
    if ($('#has_drill_down').prop("checked") == true) {
        has_drill_down = 1;
    }
    sql = sql.replace(/\r\n|\r|\n/g, " ");
    sql = btoa(htmlDecode(sql));

    if (sql == '') {
        Command: toastr["warning"]('Enter Query!');
    } else {
        var token = $('c_token').val();
        var url = $('#base_url').val() + "/flexy/flexyreportSearch";
        var param = {
            _token: token,
            sql: sql,
            access_type: access_type,
            report_name: report_name,
            group_id: group_id,
            flexy_report_totals: flexy_report_totals,
            custom_column_width: custom_column_width,
            drill_down_sql: drill_down_sql,
            drill_down_columns: drill_down_columns,
            has_drill_down: has_drill_down,
            numeric_colums: numeric_colums,
            report_id: report_id,
            users: users
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,

            beforeSend: function () {
                $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (html) {
                if (html == 0) {
                    Command: toastr["error"]('Something went wrong!');
                } else {
                    Command: toastr["success"]('Saved succesfully!');
                    $('#sql_modal').modal('show');
                    $('#menu_sql').html(html);
                }
            },
            complete: function () {
                $("body").LoadingOverlay("hide");

            }
        });

    }
}

function show_sql(){
    $('#sql_modal').modal('show');
}


// $( "#flexy_report_sql" ).keyup(function() {
//     var search_text = $('#flexy_report_sql').text();
//     if (search_text.indexOf('where') > -1) {
//         return false;
//     } else {

//         if (search_text.indexOf('from') > -1) {
//             var lastCHar = search_text.charAt(search_text.length-1);
//             if(/\s+$/.test(search_text)){

//                 if(!$(".text_table_search").is(":focus" ))
//                     AddSpanForsearchTables();
//             }
//         }
//     }
// });

function AddSpanForsearchTables() {
    var i = 0;
    var numberOfSpans = $('#flexy_report_sql').children('span').length;
    var next_sapn = 1;
    if (numberOfSpans > 0) {
        next_sapn = numberOfSpans + 1;
    }
    var append_html = '<span style="width:50px;background-color:yellow;" contenteditable="enable" id="table_' + next_sapn + '"><input autocomplete="off" class="text_table_search" onkeyup="selectDbTable(this,event);" type="text" id="table_' + next_sapn + '_input" style="border:none;" /><div class="ajaxSearchBox" id="table_' + next_sapn + '_input_ajaxSearchBox" style="display:none;width:150px;"></div></span>';

    $('#flexy_report_sql').append(append_html);
    $('#table_' + next_sapn + '_input').focus();

}

function selectDbTable(span, event) {
    var len = span.value.length;
    if (len == 0) {
        var parent_id = $('#' + span.id).parent().attr('id');
        $('#' + parent_id).remove();
    }
    var key_code = event.keyCode;
    if (key_code == 40 || key_code == 38 || key_code == 13) {
        table_list_key_down(span.id + '_ajaxSearchBox', event);
    } else {
        var table_name = $('#' + span.id).val();
        var url = $('#base_url').val() + "/flexy/getDbTable";
        $.ajax({
            type: "GET",
            url: url,
            data: 'table_name=' + table_name,
            beforeSend: function () {
                $('#table_' + span.id + '_ajaxSearchBox').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (html) {
                $('#table_' + span.id + '_ajaxSearchBox').LoadingOverlay("hide");

                $('#' + span.id + '_ajaxSearchBox').LoadingOverlay("hide");
                $('#' + span.id + '_ajaxSearchBox').css('display', 'block');
                $('#' + span.id + '_ajaxSearchBox').html(html);
            },
            complete: function () {

            }
        });
    }

}

function fetchDbTable(table_name, event) {

}

function table_list_key_down(ajax_div, event) {
    var list_items = $("#" + ajax_div).find('li');

    var selected = list_items.filter('.li_Hover');

    if (event.keyCode != 40 && event.keyCode != 38 && event.keyCode != 13) return;
    list_items.removeClass('li_Hover');
    if (event.keyCode === 40) {
        if (!selected.length || selected.is(':last-child')) {
            current = list_items.eq(0);
        } else {
            current = selected.next();
        }
    } else if (event.keyCode === 38) {
        if (!selected.length || selected.is(':first-child')) {
            current = list_items.last();
        } else {
            current = selected.prev();
        }
    }
    current.addClass('li_Hover');
    if (event.keyCode === 13) {
        $("#" + ajax_div).find('li.li_Hover').trigger('click');
    }
}

function fetchDbTable(table_name, obj) {
    var result = table_name.split('_');
    var alias = '';
    var i = 0;
    for (i = 0; i < result.length; i++) {
        alias += result[i].charAt(0);
    }
    table_name = table_name + ' as ' + alias;
    var div_id = $(obj).parent().parent().parent().attr("id");
    console.log(div_id);
    $('#' + div_id).remove();
    var spanHtml = '<span class="table_name">' + table_name + '</span>';
    $('#flexy_report_sql').append(spanHtml);
    $('#flexy_report_sql').focus();
}

function selectAccessType(type) {
    if (type == 1) {
        $('#group_access').css('display', 'none');
        $('#user_access').css('display', 'none');
    } else if (type == 2) {
        $('#group_access').css('display', 'block');
        $('#user_access').css('display', 'none');
    } else {
        $('#group_access').css('display', 'none');
        $('#user_access').css('display', 'block');
    }
}

function addClassToQuery(obj) {
    var class_name = $(obj).val();
    class_name = '{' + class_name + '}';
    $('#flexy_report_sql').append(class_name);
    insertAtCaret('flexy_report_sql', class_name);
}


function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    if (!txtarea) {
        return;
    }

    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false));
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
    } else if (br == "ff") {
        strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart('character', -txtarea.value.length);
        ieRange.moveStart('character', strPos);
        ieRange.moveEnd('character', 0);
        ieRange.select();
    } else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }

    txtarea.scrollTop = scrollPos;
}

$('#has_drill_down').click(function () {
    if ($(this).prop("checked") == true) {
        $('#drilldown_column_container').css('display', 'block');
        $('#drilldown_column_sql_container').css('display', 'block');
        $('html, body').animate({ scrollTop: $(document).height() }, 'slow');

    } else {
        $('#drilldown_column_container').css('display', 'none');
        $('#drilldown_column_sql_container').css('display', 'none');
    }
});

function show_read_me() {
    $('#read_me_modal').modal('show');
}

function selectReport(id) {
    if (id != 0) {
        var url = $('#base_url').val() + "/flexy/selectReport";
        $.ajax({
            type: "GET",
            url: url,
            data: 'report_id=' + id,
            beforeSend: function () {
                $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function (data) {
                if (data != 0) {
                    var obj = JSON.parse(data);
                    $('#report_name').val(obj.name);
                    $('#report_id').val(obj.id);
                    $('#flexy_report_sql').val(obj.query);
                    $('#numeric_colums').val(obj.numeric_colums);
                    $('#flexy_report_totals').val(obj.flexy_report_totals);
                    $('#custom_column_width').val(obj.custom_column_width);
                    $('#menu_sql').html(obj.menu_sql);

                    if (obj.has_drill_down == 1) {
                        $('#has_drill_down').prop('checked', true);
                    }

                    $('#drill_down_sql').val(obj.drill_down_sql);
                    $('#drill_down_columns').val(obj.drill_down_columns);

                    if (obj.access_type == 1) {
                        $('#public_wise').trigger('click');
                    } else if (obj.access_type == 2) {
                        $('#group_wise').trigger('click');
                        var selectedOptions = obj.groups.split(",");
                        $('select#groups').select2('val', selectedOptions);
                    } else {
                        $('#user_wise').trigger('click');
                        var selectedOptions = obj.users.split(",");
                        $('select#users').select2('val', selectedOptions);
                    }

                } else {
                    Command: toastr["error"]('Something went wrong!');
                }
            },
            complete: function () {
                $('body').LoadingOverlay("hide");
                checkSumWidth();
            }
        });
    }

}


