$(document).ready(function() {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    base_url = $('#base_url').val();
});
base_url = '';

function tableConfiguration() {
    var table_name = $("#table_name").val();
    if (table_name) {
        var url = base_url + "/master/tableColoumnList";
        var param = { table_name: table_name };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $('#tableConfigurationbtn').attr('disabled', true);
                $('#tableConfigurationspin').removeClass('fa fa-cogs');
                $('#tableConfigurationspin').addClass('fa fa-spinner fa-spin');
                $('#tableConfigurationlistDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function(data) {
                $('#tableConfigurationlistDiv').html(data);
                setTimeout(function() {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function() {
                $('#tableConfigurationbtn').attr('disabled', false);
                $('#tableConfigurationspin').removeClass('fa fa-spinner fa-spin');
                $('#tableConfigurationspin').addClass('fa fa-cogs');
                $('#tableConfigurationlistDiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please select Any Table");
        $("#table_name").focus();
    }
}


function getCheckedColumns() {
    column_array = [];
    $('.columnname_check').each(function(index) {
        var status = $(this).is(":checked");
        if (status) {
            column_array.push($(this).val());
        }
    });
    return column_array;
}

function saveTableConfiguration() {
    var table_name = $("#table_name").val();
    var delete_status = $("#delete_status").is(":checked");
    var column_arraystring = JSON.stringify(getCheckedColumns());

    if (table_name) {
        var url = base_url + "/master/saveAuditConfiguration";
        var param = { table_name: table_name, column_arraystring: column_arraystring, delete_status: delete_status };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $('#savetableConfigurationbtn').attr('disabled', true);
                $('#savetableConfigurationspin').removeClass('fa fa-save');
                $('#savetableConfigurationspin').addClass('fa fa-spinner fa-spin');
            },
            success: function(data) {
                if (data) {
                    toastr.success("Successfully Updated");
                }
            },
            complete: function() {
                $('#savetableConfigurationbtn').attr('disabled', false);
                $('#savetableConfigurationspin').removeClass('fa fa-spinner fa-spin');
                $('#savetableConfigurationspin').addClass('fa fa-save');
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please select Any Table");
        $("#table_name").focus();
    }
}