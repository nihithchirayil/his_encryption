$(document).ready(function () {
    getDoctorList();
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $('.select2').select2();
    initTinymce();
});

var base_url = $('#base_url').val();
var token = $('#token').val();


function getDoctorList() {
    var url = base_url + "/doctor_master_new/getDoctorList";
    var doctor_id = $('#doctor_id').val();
    var speciality_id = $('#speciality_id').val();
    var param = {
        _token: token,
        speciality_id: speciality_id,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#getDoctrorListBtn').attr('disabled', true);
            $('#getDoctrorListSpin').removeClass('fa fa-search');
            $('#getDoctrorListSpin').addClass('fa fa-spinner fa-spin');
            $("#getDoctrorListData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            $('#getDoctrorListData').html(data);
        },
        complete: function () {
            $('#getDoctrorListBtn').attr('disabled', false);
            $('#getDoctrorListSpin').removeClass('fa fa-spinner fa-spin');
            $('#getDoctrorListSpin').addClass('fa fa-search');
            $("#getDoctrorListData").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}



function getOpPricing(doctor_id) {
    var url = base_url + "/doctor_master_new/getOpPricing";
    var doctorname = $('#doctorname' + doctor_id).html();
    $('#doctor_id_hidden').val(doctor_id);
    var param = {
        _token: token,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#commonListModelHeader').html('OP Pricing : ' + doctorname)
            $('#opPricincingBtn' + doctor_id).attr('disabled', true);
            $('#opPricincingSpin' + doctor_id).removeClass('fa fa-money');
            $('#opPricincingSpin' + doctor_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#commonListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#commonListModelDiv').html(data);
        },
        complete: function () {
            $('#opPricincingBtn' + doctor_id).attr('disabled', false);
            $('#opPricincingSpin' + doctor_id).removeClass('fa fa-spinner fa-spin');
            $('#opPricincingSpin' + doctor_id).addClass('fa fa-money');
            $('#op_pricing_expirydate').datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            searchOpPricing();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}



function getIpPricing(doctor_id) {
    var url = base_url + "/doctor_master_new/getIpPricing";
    var doctorname = $('#doctorname' + doctor_id).html();
    $('#doctor_id_hidden').val(doctor_id);
    var param = {
        _token: token,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#commonListModelHeader').html('IP Pricing : ' + doctorname)
            $('#ipPricincingBtn' + doctor_id).attr('disabled', true);
            $('#ipPricincingSpin' + doctor_id).removeClass('fa fa-credit-card');
            $('#ipPricincingSpin' + doctor_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#commonListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#commonListModelDiv').html(data);
        },
        complete: function () {
            $('#ipPricincingBtn' + doctor_id).attr('disabled', false);
            $('#ipPricincingSpin' + doctor_id).removeClass('fa fa-spinner fa-spin');
            $('#ipPricincingSpin' + doctor_id).addClass('fa fa-credit-card');
            fetchIppriceingDetalis();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function doctorSchedule(doctor_id) {
    var url = base_url + "/doctor_master_new/adddoctorSchedule";
    var doctorname = $('#doctorname' + doctor_id).html();
    $('#doctor_id_hidden').val(doctor_id);
    var param = {
        _token: token,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#commonListModelHeader').html('Doctor Schedule : ' + doctorname)
            $('#doctorScheduleBtn' + doctor_id).attr('disabled', true);
            $('#doctorScheduleSpin' + doctor_id).removeClass('fa fa-user-md');
            $('#doctorScheduleSpin' + doctor_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#commonListModel").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#commonListModelDiv').html(data);
            fetchDoctorSchedule();
            fetchDrWalkin();
        },
        complete: function () {
            $('#doctorScheduleBtn' + doctor_id).attr('disabled', false);
            $('#doctorScheduleSpin' + doctor_id).removeClass('fa fa-spinner fa-spin');
            $('#doctorScheduleSpin' + doctor_id).addClass('fa fa-user-md');
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A',
            });
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function scheduleEndsOn() {
    var ends_on = $('#dr_time_sloat_is_ends_on').is(":checked");
    if (ends_on) {
        $('#dr_time_sloat_ends_on').prop("readonly", false);
    } else {
        $('#dr_time_sloat_ends_on').prop("readonly", true);
    }
}


function addNewSpeciality() {
    var url = base_url + "/doctor_schedule/addNewDoctorSpeciality";
    var speciality = $('#speciality_name_add').val();
    var param = {
        _token: token,
        speciality: speciality
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#addNewDoctorSpecialitybtn').attr('disabled', true);
            $('#addNewDoctorSpecialitySpin').removeClass('fa fa-plus');
            $('#addNewDoctorSpecialitySpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                fillSearchDetalis(data, speciality, 'speciality_name_add');
            }
        },
        complete: function () {
            $('#addNewDoctorSpecialitybtn').attr('disabled', false);
            $('#addNewDoctorSpecialitySpin').removeClass('fa fa-spinner fa-spin');
            $('#addNewDoctorSpecialitySpin').addClass('fa fa-plus');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}


function checkDoctorUserName() {
    var url = base_url + "/doctor_schedule/checkDoctorUserName";
    var user_name = $('#add_doctor_user_name').val();
    var doctor_id = $('#doctor_id_hidden').val();
    if (user_name) {
        var param = {
            _token: token,
            user_name: user_name,
            doctor_id: doctor_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#saveSalaryMasterBtn').attr('disabled', true);
                $('#saveSalaryMasterSpin').removeClass('fa fa-save');
                $('#saveSalaryMasterSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (parseInt(data) != 0) {
                    toastr.warning("Username already used.Please try another");
                    $('#add_doctor_user_name').val('');
                    $('#add_doctor_user_name').focus();
                } else {
                    toastr.success("Valid username");
                }
            },
            complete: function () {
                $('#saveSalaryMasterBtn').attr('disabled', false);
                $('#saveSalaryMasterSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveSalaryMasterSpin').addClass('fa fa-save');
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }

}

function updateAllRow(section_id, list_type, class_name_id) {
    var list_val = $('#' + list_type + '_' + section_id + '_' + class_name_id).val();
    for ($i = class_name_id; $i < 7; $i++) {
        $('#' + list_type + '_' + section_id + '_' + $i).val(list_val);
    }
}
var doctor_schedule = {};

function validateDoctorSchedule() {
    doctor_schedule = {};
    var flag = true;
    var from_morning_time = '';
    var to_morning_time = '';
    var from_evening_time = '';
    var to_evening_time = '';
    var i = 0;
    $('.doctor_schedule').css("background", "#fff");
    $(".doctor_schedule").each(function (index, value) {
        var week_name = $(this).attr('attr-name');
        var from_type = $(this).attr('attr-id');
        var increment_id = $(this).attr('attr-increment');
        var list_val = this.value;
        if (from_type == 'from_morning') {
            from_morning_time = list_val;
            to_morning_time = list_val;
        }
        if (from_type == 'to_morning') {
            to_morning_time = list_val;
        }

        if (from_type == 'from_evening') {
            from_evening_time = list_val;
            to_evening_time = list_val;
        }
        if (from_type == 'to_evening') {
            to_evening_time = list_val;
        }


        if (from_morning_time && !to_morning_time) {
            $('#to_morning_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter morning to time");
            flag = false;
            return false;
        } else if (!from_morning_time && to_morning_time) {
            $('#from_morning_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter morning from time");
            flag = false;
            return false;
        } else if (from_morning_time > to_morning_time) {
            $('#from_morning_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("start morning time is greater than end time");
            flag = false;
            return false;
        } else if (from_evening_time && !to_evening_time) {
            $('#to_evening_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter evening to time");
            flag = false;
            return false;
        } else if (!from_evening_time && to_evening_time) {
            $('#from_evening_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("Please enter evening from time");
            flag = false;
            return false;
        } else if (from_evening_time > to_evening_time) {
            $('#from_evening_' + increment_id).css("background", "#e94c4cb8");
            toastr.warning("start evening time is greater than end time");
            flag = false;
            return false;
        }
        i++;
        if (flag) {
            doctor_schedule[i] = {
                week_name: week_name,
                from_type: from_type,
                list_val: list_val,
            }
        }
    });
    return flag;
}

function isEmail() {
    var flag = true;
    var email = $('#add_doctor_email').val();
    if (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        flag = regex.test(email);
    }
    if (!flag) {
        toastr.warning('Please enter valid email');
        $('#add_doctor_email').focus();
    }
    same_as_email();
    return flag
}

function timeToNumber(timeString) {
    var totalMinutes = '';
    if (timeString) {
        var timeArray = timeString.split(':');
        totalMinutes = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
    }
    return totalMinutes;
}


function checkRoomStatus(room_status, from_type) {
    var status = $('#' + room_status).is(":checked");
    if (parseInt(from_type) == 1) {
        if (status) {
            $('#op_room_no').prop("readonly", false);
        } else {
            $('#op_room_no').prop("readonly", true);
        }
    } else if (parseInt(from_type) == 2) {
        if (status) {
            $('#ip_room_no').prop("readonly", false);
        } else {
            $('#ip_room_no').prop("readonly", true);
        }
    } else if (parseInt(from_type) == 3) {
        if (status) {
            $('#sp_wise_no_free_days').prop("readonly", false);
            $('#sp_wise_no_free_times').prop("readonly", false);
        } else {
            $('#sp_wise_no_free_days').prop("readonly", true);
            $('#sp_wise_no_free_times').prop("readonly", true);
        }
    }
}


function editDoctorMaster(doctor_id) {
    var url = base_url + "/doctor_master_new/editDoctorMaster";
    $('#doctor_id_hidden').val(doctor_id);
    $('.chkbox').prop('checked', false);
    var param = {
        _token: token,
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('.editDoctorMasterRow').removeClass('bg-blue');
            $("#getDoctrorListData").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var doctor_master = JSON.parse(data.doctor_master);
            var doctor_header = JSON.parse(data.doctor_header);
            $('#doctoruser_id_hidden').val(doctor_master[0].user_id ? doctor_master[0].user_id : 0);
            $('#add_doctor_name').val(doctor_master[0].doctor_name ? doctor_master[0].doctor_name : '');
            $('#doctor_gender').val(doctor_master[0].gender ? doctor_master[0].gender : 0).select2();
            $('#add_doctor_phone').val(doctor_master[0].phone ? doctor_master[0].phone : '');
            $('#token_prefix').val(doctor_master[0].token_prefix ? doctor_master[0].token_prefix : '');
            $('#add_doctor_user_name').val(doctor_master[0].username ? doctor_master[0].username : '');
            $('#add_doctor_login_email').val(doctor_master[0].login_email ? doctor_master[0].login_email : '');
            $('#speciality_name_add').val(doctor_master[0].speciality_name ? doctor_master[0].speciality_name : '');
            $('#speciality_id_add').val(doctor_master[0].speciality ? doctor_master[0].speciality : 0);
            $('#add_doctor_email').val(doctor_master[0].email ? doctor_master[0].email : '');
            $('#doctor_phone').val(doctor_master[0].phone ? doctor_master[0].phone : '');
            $('#add_doctor_address').val(doctor_master[0].address ? doctor_master[0].address : '');
            $('#add_doctor_qualification').val(doctor_master[0].qualification ? doctor_master[0].qualification : '');
            $('#doctor_login_email').val(doctor_master[0].login_email ? doctor_master[0].login_email : '');
            $('#additional_information').val(doctor_master[0].additional_information ? doctor_master[0].additional_information : '');
            $('#medical_licenced').val(doctor_master[0].license_country ? doctor_master[0].license_country : '').select2();
            $('#medical_licenced_state').val(doctor_master[0].license_state ? doctor_master[0].license_state : '').select2();
            $('#license_number').val(doctor_master[0].license_no ? doctor_master[0].license_no : '');
            $('#sp_wise_no_free_days').val(doctor_master[0].same_speciality_followup_days ? doctor_master[0].same_speciality_followup_days : '');
            $('#sp_wise_no_free_times').val(doctor_master[0].same_speciality_followup_no ? doctor_master[0].same_speciality_followup_no : '');
            $('#no_free_days').val(doctor_master[0].free_followup_days ? doctor_master[0].free_followup_days : '');
            $('#no_free_times').val(doctor_master[0].free_followup_no ? doctor_master[0].free_followup_no : '');
            $('#op_room_no').val(doctor_master[0].op_room_no ? doctor_master[0].op_room_no : '');
            $('#ip_room_no').val(doctor_master[0].ip_room_no ? doctor_master[0].ip_room_no : '');
            $('#screening_room_no').val(doctor_master[0].screening_room_no ? doctor_master[0].screening_room_no : '');
            $('#prescription_department_head').val(doctor_master[0].prescription_department_head ? doctor_master[0].prescription_department_head : '');
            var medication_note = doctor_master[0].medication_note ? doctor_master[0].medication_note : '';
            var medication_footer_text = doctor_master[0].medication_footer_text ? doctor_master[0].medication_footer_text : '';
            var login_status = doctor_master[0].login_status ? doctor_master[0].login_status : '';
            var doctor_status = doctor_master[0].doctor_status ? doctor_master[0].doctor_status : '';
            var is_surgeon = doctor_master[0].is_surgeon ? doctor_master[0].is_surgeon : '';
            var hide_from_appointments = doctor_master[0].hide_from_appointments ? doctor_master[0].hide_from_appointments : '';
            var appointments_only = doctor_master[0].appointments_only ? doctor_master[0].appointments_only : '';
            var has_op_rights = doctor_master[0].has_op_rights ? doctor_master[0].has_op_rights : '';
            var has_ip_rights = doctor_master[0].has_ip_rights ? doctor_master[0].has_ip_rights : '';
            var is_mlc = doctor_master[0].is_mlc ? doctor_master[0].is_mlc : '';
            var is_casualty = doctor_master[0].is_casualty ? doctor_master[0].is_casualty : '';
            var speciality_wise_followup = doctor_master[0].free_followup_for_same_speciality ? doctor_master[0].free_followup_for_same_speciality : '';

            tinymce.get("medication_note").setContent(medication_note);
            tinymce.get("medication_footer_text").setContent(medication_footer_text);

            if (login_status) {
                $('#login_active').prop('checked', true);
            }
            if (doctor_status) {
                $('#is_active').prop('checked', true);
            }
            if (is_surgeon) {
                $('#is_surgeon').prop('checked', true);
            }
            if (hide_from_appointments) {
                $('#hide_from_appointments').prop('checked', true);
            }
            if (appointments_only) {
                $('#show_in_appointments_only').prop('checked', true);
            }
            if (has_op_rights) {
                $('#has_op_rights').prop('checked', true);
            }
            if (has_ip_rights) {
                $('#has_ip_rights').prop('checked', true);
            }
            if (is_mlc) {
                $('#is_mlc').prop('checked', true);
            }
            if (is_casualty) {
                $('#is_casuality').prop('checked', true);
            }
            if (speciality_wise_followup) {
                $('#speciality_wise_followup').prop('checked', true);
            }
            tinymce.get("doctor_header").setContent('');
            tinymce.get("doctor_footer").setContent('');
            if (doctor_header && Object.keys(doctor_header).length > 0) {
                tinymce.get("doctor_header").setContent(doctor_header.doctor_headder);
                tinymce.get("doctor_footer").setContent(doctor_header.doctor_footer);
            }

        },
        complete: function () {
            $('.editDoctorMasterRow' + doctor_id).addClass('bg-blue');
            $("#getDoctrorListData").LoadingOverlay("hide");

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
            checkRoomStatus('has_op_rights', 1);
            checkRoomStatus('has_ip_rights', 2);
            checkRoomStatus('speciality_wise_followup', 3);

        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}



function saveSalaryMaster() {
    var url = base_url + "/doctor_master_new/saveDoctorMaster";
    var doctor_id = $('#doctor_id_hidden').val();
    var doctoruser_id = $('#doctoruser_id_hidden').val();
    var doctor_name = $('#add_doctor_name').val();
    var doctor_gender = $('#doctor_gender').val();
    var token_prefix = $('#token_prefix').val();
    var speciality_name = $('#speciality_name_add').val();
    var speciality_id = $('#speciality_id_add').val();
    var doctor_email = $('#add_doctor_email').val();
    var doctor_phone = $('#add_doctor_phone').val();
    var doctor_address = $('#add_doctor_address').val();
    var doctor_qualification = $('#add_doctor_qualification').val();
    var doctor_user_name = $('#add_doctor_user_name').val();
    var doctor_login_email = $('#add_doctor_login_email').val();
    var additional_information = $('#additional_information').val();
    var medical_licenced = $('#medical_licenced').val();
    var medical_licenced_state = $('#medical_licenced_state').val();
    var license_number = $('#license_number').val();
    var login_active = $('#login_active').is(":checked");
    var sp_wise_no_free_days = $('#sp_wise_no_free_days').val();
    var sp_wise_no_free_times = $('#sp_wise_no_free_times').val();
    var no_free_days = $('#no_free_days').val();
    var no_free_times = $('#no_free_times').val();
    var is_surgeon = $('#is_surgeon').is(":checked");
    var hide_from_appointments = $('#hide_from_appointments').is(":checked");
    var show_in_appointments_only = $('#show_in_appointments_only').is(":checked");
    var op_room_no = $('#op_room_no').val();
    var ip_room_no = $('#ip_room_no').val();
    var screening_room_no = $('#screening_room_no').val();
    var has_op_rights = $('#has_op_rights').is(":checked");
    var has_ip_rights = $('#has_ip_rights').is(":checked");
    var is_active = $('#is_active').is(":checked");
    var is_mlc = $('#is_mlc').is(":checked");
    var is_casuality = $('#is_casuality').is(":checked");
    var speciality_wise_followup = $('#speciality_wise_followup').is(":checked");
    var consultation_duration = $('#consultation_duration').val();
    var prescription_department_head = $('#prescription_department_head').val();
    var medication_note = tinymce.get("medication_note").getContent();
    var medication_footer_text = tinymce.get("medication_footer_text").getContent();
    var doctor_header = tinymce.get("doctor_header").getContent();
    var doctor_footer = tinymce.get("doctor_footer").getContent();
    if (doctor_name) {
        if (doctor_gender) {
            if (token_prefix) {
                if (speciality_id) {
                    if (doctor_user_name) {
                        if (doctor_login_email) {
                            var param = {
                                doctor_id: doctor_id,
                                doctoruser_id: doctoruser_id,
                                doctor_name: doctor_name,
                                doctor_gender: doctor_gender,
                                token_prefix: token_prefix,
                                speciality_name: speciality_name,
                                speciality_id: speciality_id,
                                doctor_email: doctor_email,
                                doctor_phone: doctor_phone,
                                doctor_address: doctor_address,
                                doctor_qualification: doctor_qualification,
                                doctor_user_name: doctor_user_name,
                                doctor_login_email: doctor_login_email,
                                additional_information: additional_information,
                                medical_licenced: medical_licenced,
                                medical_licenced_state: medical_licenced_state,
                                license_number: license_number,
                                login_active: login_active,
                                sp_wise_no_free_days: sp_wise_no_free_days,
                                sp_wise_no_free_times: sp_wise_no_free_times,
                                no_free_days: no_free_days,
                                no_free_times: no_free_times,
                                is_surgeon: is_surgeon,
                                hide_from_appointments: hide_from_appointments,
                                show_in_appointments_only: show_in_appointments_only,
                                op_room_no: op_room_no,
                                ip_room_no: ip_room_no,
                                screening_room_no: screening_room_no,
                                has_op_rights: has_op_rights,
                                has_ip_rights: has_ip_rights,
                                is_active: is_active,
                                is_mlc: is_mlc,
                                is_casuality: is_casuality,
                                speciality_wise_followup: speciality_wise_followup,
                                consultation_duration: consultation_duration,
                                doctor_header,
                                doctor_footer,
                                prescription_department_head,
                                medication_note,
                                medication_footer_text
                            }

                            bootbox.confirm({
                                message: "Are you sure you want to create doctor profile ?",
                                buttons: {
                                    confirm: {
                                        label: "Yes",
                                        className: "btn-success",
                                        default: "true",
                                    },
                                    cancel: {
                                        label: "No",
                                        className: "btn-warning",
                                    },
                                },
                                callback: function (result) {
                                    if (result) {
                                        $.ajax({
                                            type: "POST",
                                            url: url,
                                            data: param,
                                            beforeSend: function () {
                                                $('#saveSalaryMasterBtn').attr('disabled', true);
                                                $('#saveSalaryMasterSpin').removeClass('fa fa-search');
                                                $('#saveSalaryMasterSpin').addClass('fa fa-spinner fa-spin');
                                            },
                                            success: function (data) {
                                                if (parseInt(data.status) == 1) {
                                                    toastr.success(data.message);
                                                    resetDoctorProfile();
                                                } else {
                                                    toastr.warning(data.message);
                                                }
                                            },
                                            complete: function () {
                                                $('#saveSalaryMasterBtn').attr('disabled', false);
                                                $('#saveSalaryMasterSpin').removeClass('fa fa-spinner fa-spin');
                                                $('#saveSalaryMasterSpin').addClass('fa fa-search');
                                            },
                                            error: function () {
                                                toastr.error("Error Please Check Your Internet Connection");
                                            }
                                        });
                                    }
                                },
                            });
                        } else {
                            $('#DoctorUserProfileli').tab('show')
                            toastr.warning("Please Enter Doctor Login Email");
                            $('#add_doctor_login_email').focus();
                        }
                    } else {
                        $('#DoctorUserProfileli').tab('show')
                        toastr.warning("Please Enter Doctor User Name");
                        $('#add_doctor_user_name').focus();
                    }
                } else {
                    $('#BasicInformationsli').tab('show')
                    toastr.warning("Please Enter Doctor Speciality");
                    $('#speciality_name_add').focus();
                }
            } else {
                $('#BasicInformationsli').tab('show')
                toastr.warning("Please Enter Doctor Prefix");
                $('#token_prefix').focus();
            }
        } else {
            $('#BasicInformationsli').tab('show')
            toastr.warning("Please Enter Doctor Gender");
            $('#doctor_gender').focus();
        }
    } else {
        $('#BasicInformationsli').tab('show')
        toastr.warning("Please Enter Doctor Name");
        $('#add_doctor_name').focus();
    }

}

function resetForm() {
    $('#doctor_name').val('');
    $('#doctor_id').val('');
    $('#speciality_name').val('');
    $('#speciality_id').val('');
    getDoctorList();
}


function same_as_email() {
    var status = $('#same_as_email').is(":checked");
    if (status) {
        per_email = $('#add_doctor_email').val();
        if (per_email) {
            $('#add_doctor_login_email').val(per_email);
        }
    }
}

function resetDoctorProfile() {
    $('#doctor_id_hidden').val(0);
    $('#doctoruser_id_hidden').val(0);
    $('#schedule_id_hidden').val(0);
    $('#add_doctor_name').val('');
    $('#doctor_gender').val('').select2();
    $('#token_prefix').val('');
    $('#speciality_name_add').val('');
    $('#speciality_id_add').val(0);
    $('#add_doctor_email').val('');
    $('#add_doctor_phone').val('');
    $('#add_doctor_qualification').val('');
    $('#add_doctor_address').val('');
    $('#add_doctor_user_name').val('');
    $('#add_doctor_login_email').val('');
    $('#additional_information').val('');
    $('#medical_licenced').val('').select2();
    $('#license_number').val('');
    $('#sp_wise_no_free_days').val('');
    $('#sp_wise_no_free_times').val('');
    $('#no_free_days').val('');
    $('#no_free_times').val('');
    $('#op_room_no').val('');
    $('#ip_room_no').val('');
    $('#consultation_duration').val('');
    $('#screening_room_no').val('');
    $('#prescription_department_head').val('');

    $('#is_surgeon').prop("checked", false);
    $('#hide_from_appointments').prop("checked", false);
    $('#show_in_appointments_only').prop("checked", false);
    $('#same_as_email').prop("checked", false);
    $('#login_active').prop("checked", false);
    $('#has_op_rights').prop("checked", false);
    $('#has_ip_rights').prop("checked", false);
    $('#is_active').prop("checked", false);
    $('#is_mlc').prop("checked", false);
    $('#is_casuality').prop("checked", false);
    $('#speciality_wise_followup').prop("checked", false);
    checkRoomStatus('has_op_rights', 1);
    checkRoomStatus('has_ip_rights', 2);
    checkRoomStatus('speciality_wise_followup', 3);
    $('.editDoctorMasterRow').removeClass('bg-blue');
    tinymce.get("doctor_header").setContent('');
    tinymce.get("doctor_header").setContent('');
    tinymce.get("medication_note").setContent('');
    tinymce.get("medication_footer_text").setContent('');
}

$('#doctor_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('doctor_name_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var doctor_name = $(this).val();
        if (!doctor_name) {
            $('#doctor_id').val('');
            $("#doctor_name_AjaxDiv").hide();
        } else {
            var url = base_url + "/doctor_schedule/doctorSearchCriteria";
            var param = {
                list_name: doctor_name,
                list_type: 'doctor_name'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#doctor_name_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#doctor_name_AjaxDiv").html(html).show();
                    $("#doctor_name_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('doctor_name_AjaxDiv', event);
    }
});


$('#speciality_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('speciality_name_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var speciality_name = $(this).val();
        if (!speciality_name) {
            $('#speciality_id').val('');
            $("#speciality_name_AjaxDiv").hide();
        } else {
            var url = base_url + "/doctor_schedule/doctorSearchCriteria";
            var param = {
                list_name: speciality_name,
                list_type: 'speciality_name'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#speciality_name_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#speciality_name_AjaxDiv").html(html).show();
                    $("#speciality_name_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('speciality_name_AjaxDiv', event);
    }
});


$('#speciality_name_add').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('speciality_name_add_AjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var speciality_name_add = $(this).val();
        if (!speciality_name_add) {
            $('#speciality_id_add').val('');
            $("#speciality_name_add_AjaxDiv").hide();
        } else {
            var url = base_url + "/doctor_schedule/doctorSearchCriteria";
            var param = {
                list_name: speciality_name_add,
                list_type: 'speciality_name_add'
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#speciality_name_add_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#speciality_name_add_AjaxDiv").html(html).show();
                    $("#speciality_name_add_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('speciality_name_add_AjaxDiv', event);
    }
});


function fillSearchDetalis(list_id, list_name, from_type) {
    list_id = parseInt(list_id);
    if (from_type == 'doctor_name') {
        $('#doctor_name').val(htmlDecode(list_name));
        $('#doctor_id').val(list_id);
        $("#doctor_name_AjaxDiv").hide();
    } else if (from_type == 'speciality_name') {
        $('#speciality_name').val(htmlDecode(list_name));
        $('#speciality_id').val(list_id);
        $("#speciality_name_AjaxDiv").hide();
    } else if (from_type == 'speciality_name_add') {
        $('#speciality_name_add').val(htmlDecode(list_name));
        $('#speciality_id_add').val(list_id);
        $("#speciality_name_add_AjaxDiv").hide();
    }
}

function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function initTinymce(){
    tinymce.init({
        selector: 'textarea.texteditortyne',
        autoresize_min_height: '90',
        themes: "modern",
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
        imagetools_cors_hosts: ['picsum.photos'],
         menubar: 'file edit view insert format tools table help',
        menubar: false,
        statusbar: false,
        toolbar: false,
         toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        // toolbar_sticky: true,
        browser_spellcheck: true,
        autosave_ask_before_unload: true,
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_restore_when_empty: false,
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        image_advtab: true,
        contextmenu: false,
        importcss_append: true,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        statusbar: false,
        forced_root_block: '',
        width: 566, // Set the desired width66
        height: 151, // Set the desired height
        setup: function(editor) {

            editor.ui.registry.addButton('Ucase', {
                text: 'A^',
                onAction: function() {
                    TextToUpperCase();
                },
            });
            editor.ui.registry.addButton('Lcase', {
                text: 'a^',
                onAction: function() {
                    TextToLowerCase();
                },
            });
            editor.ui.registry.addButton('Icase', {
                text: 'I^',
                onAction: function() {
                    TextToInterCase();
                },
            });
            editor.ui.registry.addButton('Ccase', {
                text: 'C^',
                onAction: function() {
                    FirstLetterToInterCase();
                },
            });

        },
    });
}
