$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({
        placeholder: "",
        maximumSelectionSize: 6
    });
    searchmanagePatientInsurance();
});

var base_url = $('#base_url').val();
var token = $('#token').val();

function searchmanagePatientInsurance() {
    var url = base_url + "/insuranceSettlement/searchmanagePatientInsurance";
    var from_date = $('#bill_from_date').val();
    var to_date = $('#bill_to_date').val();
    var patient_id = $('#patient_id_hidden').val();
    var visit_type = $('#visit_type').val();
    var insurance_company = $('#insurance_company').val();
    var insurance_schema = $('#insurance_companyschema').val();
    var insurance_doctor = $('#insurance_doctor').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            from_date: from_date,
            to_date: to_date,
            patient_id: patient_id,
            visit_type: visit_type,
            insurance_company: insurance_company,
            insurance_schema: insurance_schema,
            insurance_doctor: insurance_doctor
        },
        beforeSend: function () {
            $("#searchmanagePatientInsuranceBtn").attr("disabled", true);
            $("#searchmanagePatientInsuranceSpin").removeClass("fa fa-search");
            $("#searchmanagePatientInsuranceSpin").addClass("fa fa-spinner fa-spin");
            $("#managePatientInsuranceDiv").LoadingOverlay("show", {
                background: "rgba(89, 89, 89, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            $('#managePatientInsuranceDiv').html(html);
        },
        complete: function () {
            $("#searchmanagePatientInsuranceBtn").attr("disabled", false);
            $("#searchmanagePatientInsuranceSpin").removeClass("fa-spinner fa-spin");
            $("#searchmanagePatientInsuranceSpin").addClass("fa fa-search");
            $("#managePatientInsuranceDiv").LoadingOverlay("hide");
        },
        error: function () {
            toastr.error(
                "Error Please Check Your Internet Connection and Try Again"
            );
        },
    });
}



function resetsearchForm() {
    var current_date = $('#current_date').val();
    $('#bill_from_date').val(current_date);
    $('#bill_to_date').val(current_date);
    $('#patient_name').val('');
    $('#patient_id_hidden').val(0);
    $('#visit_type').val('').select2();
    $('#insurance_company').val('').select2();
    getCompanyPricing('insurance_company', 0);
    $('#insurance_doctor').val('').select2();
    searchmanagePatientInsurance();
}


$('#patient_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('patient_idAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var patient_name = $('#patient_name').val();
        if (!patient_name) {
            $('.ajaxSearchBox').hide();
            $("#patient_id_hidden").val(0);
        } else {
            var url = base_url + "/insuranceSettlement/searchPatientName";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    patient_name: patient_name,
                    from_type: 1
                },
                beforeSend: function () {
                    $("#patient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#patient_idAjaxDiv").html(html).show();
                    $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('patient_idAjaxDiv', event);
    }
});

$('#dependent_patient_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('dependentpatient_idAjaxDiv');
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var dependent_patient_name = $('#dependent_patient_name').val();
        if (!dependent_patient_name) {
            $('.ajaxSearchBox').hide();
            $("#dependentpatient_id_hidden").val(0);
        } else {
            var url = base_url + "/insuranceSettlement/searchPatientName";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    patient_name: dependent_patient_name,
                    from_type: 2
                },
                beforeSend: function () {
                    $("#dependentpatient_idAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#dependentpatient_idAjaxDiv").html(html).show();
                    $("#dependentpatient_idAjaxDiv").find('li').first().addClass('liHover');
                },
                complete: function () {},
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
    } else {
        ajax_list_key_down('dependentpatient_idAjaxDiv', event);
    }
});

function fillPatientValues(uhid, patient_name, patient_id, from_type) {
    if (parseInt(from_type) == 1) {
        $("#patient_id_hidden").val(patient_id);
        $("#patient_name").val(patient_name);
    } else if (parseInt(from_type) == 2) {
        $("#dependent_patient_name").val(uhid);
        $("#dependentpatient_id_hidden").val(patient_id);
        $("#dependentpatient_name").val(patient_name);
    }
    $('.ajaxSearchBox').hide();
}

function getCompanyPricing(company_name, insurance_schema) {
    var url = base_url + "/insuranceSettlement/getCompanyPricing";
    var company_id = $('#' + company_name).val();
    if (company_id) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                company_id: company_id,
                insurance_schema: insurance_schema
            },
            beforeSend: function () {
                $("#searchmanagePatientInsuranceBtn").attr("disabled", true);
                $("#searchmanagePatientInsuranceSpin").removeClass("fa fa-search");
                $("#searchmanagePatientInsuranceSpin").addClass("fa fa-spinner fa-spin");
            },
            success: function (html) {
                $('#' + company_name + 'schema').html(html);
                $('#' + company_name + 'schema').select2();
            },
            complete: function () {
                $("#searchmanagePatientInsuranceBtn").attr("disabled", false);
                $("#searchmanagePatientInsuranceSpin").removeClass("fa-spinner fa-spin");
                $("#searchmanagePatientInsuranceSpin").addClass("fa fa-search");
            },
            error: function () {
                toastr.error(
                    "Error Please Check Your Internet Connection and Try Again"
                );
            },
        });
    } else {
        $('#' + company_name + 'schema').html("<option value=''>Select</option>");
        $('#' + company_name + 'schema').select2();
    }
}


function editPatientInsurance(patient_id, current_visit_id) {
    var url = base_url + "/insuranceSettlement/editPatientInsurance";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            patient_id: patient_id,
        },
        beforeSend: function () {
            reseteditForm();
            $("#managePatientInsuranceDiv").LoadingOverlay("show", {
                background: "rgba(89, 89, 89, 0.6)",
                imageColor: '#337AB7'
            });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#edit_patient_id_hidden').val(patient_id);
            $('#current_visit_id_hidden').val(current_visit_id);
            $('#edit_insurance_company').val(obj[0].insurance_company).select2();
            getCompanyPricing('edit_insurance_company', obj[0].insurance_pricing);
            $('#edit_policy_id').val(obj[0].authorisation_letter_no);
            $('#edit_insurance_id').val(obj[0].insurance_id_no);
            $('#edit_credit_limit').val(obj[0].credit_limit);
            $('#edit_valid_till').val(obj[0].expiry_date);
            $('#dependent_patient_name').val(obj[0].dependent_uhid);
            $('#dependentpatient_id_hidden').val(obj[0].dependent_patientid);
            $('#dependentpatient_name').val(obj[0].patient_name);
            $('#dependentpatient_relation').val(obj[0].dependent_relation).select2();
        },
        complete: function () {
            $("#managePatientInsuranceDiv").LoadingOverlay("hide");
            $("#editPatientInsuranceBtn" + patient_id).addClass('orange');
        },
        error: function () {
            toastr.error(
                "Error Please Check Your Internet Connection and Try Again"
            );
        },
    });
}


function savePatientInsurance() {
    var url = base_url + "/insuranceSettlement/savePatientInsurance";
    var patient_id = $('#edit_patient_id_hidden').val();
    var current_visit_id = $('#current_visit_id_hidden').val();
    var insurance_company = $('#edit_insurance_company').val();
    var insurance_schema = $('#edit_insurance_companyschema').val();
    var authorisation_letter_no = $('#edit_policy_id').val();
    var insurance_id_no = $('#edit_insurance_id').val();
    var credit_limit = $('#edit_credit_limit').val();
    var expiry_date = $('#edit_valid_till').val();
    var dependent_uhid = $('#dependent_patient_name').val();
    var dependent_patientid = $('#dependentpatient_id_hidden').val();
    var dependent_patient_name = $('#dependentpatient_name').val();
    var dependent_relation = $('#dependentpatient_relation').val();
    if (parseInt(patient_id) != 0) {
        if (insurance_company) {
            if (insurance_schema) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        patient_id: patient_id,
                        current_visit_id: current_visit_id,
                        insurance_company: insurance_company,
                        insurance_schema: insurance_schema,
                        authorisation_letter_no: authorisation_letter_no,
                        insurance_id_no: insurance_id_no,
                        credit_limit: credit_limit,
                        expiry_date: expiry_date,
                        dependent_uhid: dependent_uhid,
                        dependent_patientid: dependent_patientid,
                        dependent_patient_name: dependent_patient_name,
                        dependent_relation: dependent_relation
                    },
                    beforeSend: function () {
                        $("#savePatientInsuranceBtn").attr("disabled", true);
                        $("#savePatientInsuranceSpin").removeClass("fa fa-save");
                        $("#savePatientInsuranceSpin").addClass("fa fa-spinner fa-spin");
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            $('#company_name' + patient_id).html(data.company_name);
                            reseteditForm();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $("#savePatientInsuranceBtn").attr("disabled", false);
                        $("#savePatientInsuranceSpin").removeClass("fa-spinner fa-spin");
                        $("#savePatientInsuranceSpin").addClass("fa fa-save");
                    },
                    error: function () {
                        toastr.error(
                            "Error Please Check Your Internet Connection and Try Again"
                        );
                    },
                });
            } else {
                toastr.warning("Please select any insurance schema");
            }
        } else {
            toastr.warning("Please select any insurance company");
        }
    } else {
        toastr.warning("Please select any patient");
    }
}

function reseteditForm() {
    $('#edit_patient_id_hidden').val(0);
    $('#edit_insurance_company').val('').select2();
    getCompanyPricing('edit_insurance_company', 0);
    $('#edit_policy_id').val('');
    $('#edit_insurance_id').val('');
    $('#edit_credit_limit').val('');
    $('#edit_valid_till').val('');
    $('#dependent_patient_name').val('');
    $('#dependentpatient_id_hidden').val(0);
    $('#dependentpatient_name').val('');
    $('#dependentpatient_relation').val('').select2();
    $('.editPatientInsurance').removeClass('orange');
}
