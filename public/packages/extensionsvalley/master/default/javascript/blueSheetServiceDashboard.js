$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });

});

function selectPatientBlueSheet(e,patient_name,doctor_name,visit_date,patient_id,doctor_id,op_id,visit_id){

$("#patient_id").val(patient_id);
$("#doctor_id").val(doctor_id).select2();
$("#op_id").val(op_id);
$("#visit_id").val(visit_id);
$("#doctor_name").val(doctor_name);
$("#patient_name").val(patient_name);
$("#visit_date").val(visit_date);
getBlueSheetData();
getBiliingBlueSheetServices();
}
function repeatBluesheetData(){
    getBlueSheetData();
    getBiliingBlueSheetServices();
}
function getBlueSheetData(){
   var base_url = $('#base_url').val();
   var visit_id = $("#visit_id").val();
   var doctor_id =$("#doctor_id").val();
   var patient_id = $("#patient_id").val();
   var visit_date = $("#visit_date").val();
    if (patient_id != '0') {

        var url = base_url + "/master/getBlueSheetServices";
        var param = { visit_id: visit_id, servieDate: visit_date, doctor_id: doctor_id, patient_id: patient_id };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#blueSheetServicesDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#blueSheetServicesDiv').html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                $('#blueSheetServicesDiv').LoadingOverlay("hide");
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Patient");
    }


}
function addBlueSheetBillingServices(sheet_id, billing_sheet_id) {
    var checked_status = $('#addBillisngServicesbtn' + sheet_id).is(":checked");
    var doctor_id = $("#doctor_id").val();
    var base_url = $('#base_url').val();
    if (doctor_id != '0') {
        var visit_id = $("#visit_id").val();
        var patient_id = $("#patient_id").val();
        var servieDate = $("#visit_date").val();
        var url = base_url + "/master/saveBillingBlueSheet";
        var param = { checked_status: checked_status, billing_sheet_id: billing_sheet_id, servieDate: servieDate, sheet_id: sheet_id, visit_id: visit_id, doctor_id: doctor_id, patient_id: patient_id };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#addBillingServicesbtn' + sheet_id).attr('disabled', true);
                $('#blueSheetServicesDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if (data != '0') {
                    if (checked_status) {
                        $('#addBillisngServicesbtn' + sheet_id).attr('onclick', 'addBlueSheetBillingServices(' + sheet_id + ',' + data + ');');
                    } else {
                        $('#addBillisngServicesbtn' + sheet_id).attr('onclick', 'addBlueSheetBillingServices(' + sheet_id + ',' + 0 + ');');
                    }
                } else {
                    toastr.warning("Bill Already Generated");
                    if (checked_status) {
                        $('#addBillisngServicesbtn' + sheet_id).prop("checked", false);
                    } else {
                        $('#addBillisngServicesbtn' + sheet_id).prop("checked", true);
                    }
                }

            },
            complete: function () {
                $('#addBillingServicesbtn' + sheet_id).attr('disabled', false);
                $('#blueSheetServicesDiv').LoadingOverlay("hide");
                repeatBluesheetData();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Doctors");
    }
}
function getBiliingBlueSheetServices() {
    var patient_id = $("#patient_id").val();
    var base_url = $('#base_url').val();
    if (patient_id != '0') {
        var doctor_id = $("#doctor_id").val();
        var visit_id = $("#visit_id").val();
        var servieDate = $("#visit_date").val();
        var url = base_url + "/master/getBiliingBlueSheetServices";
        var param = { visit_id: visit_id, servieDate: servieDate, doctor_id: doctor_id, patient_id: patient_id };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
                $('#billingblueSheetServicesDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#billingblueSheetServicesDiv').html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
                $('.datepicker').datetimepicker({
                    format: 'MMMM-DD-YYYY'
                });
            },
            complete: function () {
                $('#billingblueSheetServicesDiv').LoadingOverlay("hide");
                $(".doc_data").select2("destroy").select2();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }

}
function updateBlueSheetBillingServices(sheet_id) {
    var base_url = $('#base_url').val();
    var doctor_id = $("#yellowsheetdoctor_select" + sheet_id).val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#visit_date").val();
    var visit_time = $("#yellowsheettime_picker" + sheet_id).val();
    var url = base_url + "/master/updateBlueSheetBillingServices";
    if (doctor_id) {
        if (visit_time) {
            var param = { servieDate: servieDate, visit_id: visit_id, patient_id: patient_id, sheet_id: sheet_id, doctor_id: doctor_id, visit_time: visit_time };
            $.ajax({
                type: "GET",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', true);
                    $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-repeat');
                    $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (data != '0') {
                        toastr.success("Successfully Updated");
                    } else {
                        toastr.warning("Bill Already Generated");
                    }
                },
                complete: function () {
                    $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', false);
                    $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
                    $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-repeat');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Any Time");
            $("#yellowsheettime_picker" + sheet_id).focus();
        }
    } else {
        toastr.warning("Please Select Any Doctor");
        $("#yellowsheetdoctor_select" + sheet_id).focus();
    }
}
function repeatBlueSheetBillingServices(sheet_id) {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/repeatBlueSheetBillingServices";
    var param = { sheet_id: sheet_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', true);
            $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-repeat');
            $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data != '0') {
                toastr.success("Successfully Added");
                repeatBluesheetData();
            } else {
                toastr.warning("Bill Already Generated");
            }
        },
        complete: function () {
            $('#repeatBillingServicesbtn' + sheet_id).attr('disabled', false);
            $('#repeatBillingServicesspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
            $('#repeatBillingServicesspin' + sheet_id).addClass('fa fa-repeat');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
function removeBlueSheetBillingServices(sheet_id, yellowsheetid) {
    var base_url = $('#base_url').val();
    var url = base_url + "/master/deleteBlueSheetBillingServices";
    var doctor_id = $("#doctor_id").val();
    var visit_id = $("#visit_id").val();
    var patient_id = $("#patient_id").val();
    var servieDate = $("#visit_date").val();
    var visit_time = $("#yellowsheettime_picker" + sheet_id).val();
    var param = { servieDate: servieDate, visit_time: visit_time, visit_id: visit_id, doctor_id: doctor_id, patient_id: patient_id, sheet_id: sheet_id };
    bootbox.confirm({
        message: 'Are You Sure You Want To Delete ?',
        buttons: {
            'confirm': {
                label: 'Delete',
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning pull-right save_align'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#removeBillingServicesbtn' + sheet_id).attr('disabled', true);
                        $('#removeBillingServicesspin' + sheet_id).removeClass('fa fa-trash-o');
                        $('#removeBillingServicesspin' + sheet_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (data != '0') {
                            toastr.success("Successfully Deleted");
                            $('#billingyellowsheetrow' + sheet_id).remove();
                            $('#addBillisngServicesbtn' + yellowsheetid).attr('onclick', 'addBlueSheetBillingServices(' + yellowsheetid + ',' + 0 + ');');
                            $('#addBillisngServicesbtn' + yellowsheetid).prop("checked", false);
                        } else {
                            toastr.warning("Bill Already Generated");
                        }

                    },
                    complete: function () {
                        $('#removeBillingServicesbtn' + sheet_id).attr('disabled', false);
                        $('#removeBillingServicesspin' + sheet_id).removeClass('fa fa-spinner fa-spin');
                        $('#removeBillingServicesspin' + sheet_id).addClass('fa fa-trash-o');
                        repeatBluesheetData();
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        }
    });
}
$("#patient_name_search_tble").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#blueSheetServicesPatientList tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
$("#procedure_name_search_tble").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#blueSheetServicesDiv tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
function selectPatientData(e){
    var service_date = $(e).val();
var base_url = $('#base_url').val();
window.location.href = base_url+"/master/blueSheetDashboard?service_date="+service_date;
}
function datarst(){
    var base_url = $('#base_url').val();
    window.location.href = base_url+"/master/blueSheetDashboard";
}
function saveAll() {
    var visit_id = $("#visit_id").val();
    var base_url = $('#base_url').val();
    var url = base_url + "/master/saveBluesheetData";
    var param = { visit_id: visit_id };
    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchProcedureSearchDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if (data == 1) {
                toastr.success("Successfully Added");
                window.location.reload();
            } else {
                toastr.warning("Bill Already Generated");
            }
        },
        complete: function () {
            $('#searchProcedureSearchDiv').LoadingOverlay("hide");
        },
        error: function () {
            toastr.error("No data to update");
        }
    });
}
