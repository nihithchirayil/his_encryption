$(document).ready(function(){
    advancePatientSearch();
})
var token=$('#c_token').val();
var base_url=$('#base_url').val();

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url ='';
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function ageCalculation() {
    var current_date = $('#current_date').val();
    var dateString = $('#patient_dob').val();
    var yearAge = '';
    var monthAge = '';
    var dateAge = '';
    if (dateString) {

        var now = moment(current_date, 'MMM-DD-YYYY');

        var yearNow = now.format('YYYY');
        var monthNow = now.format('MM');
        var dateNow = now.format('DD');

        var date = moment(dateString, 'MMM-DD-YYYY');
        var yearDob = date.format('YYYY');
        var monthDob = date.format('MM');
        var dateDob = date.format('DD');
        yearAge = parseInt(yearNow) - parseInt(yearDob);

        if (monthNow >= monthDob)
            monthAge = parseInt(monthNow) - parseInt(monthDob);
        else {
            yearAge--;
            monthAge = 12 + parseInt(monthNow) - parseInt(monthDob);
        }

        if (dateNow >= dateDob) {
            dateAge = parseInt(dateNow) - parseInt(dateDob);
        } else {
            monthAge--;
            dateAge = 31 + parseInt(dateNow) - parseInt(dateDob);

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }
    }
    $('#age_years').val(yearAge);
    $('#age_month').val(monthAge);
    $('#age_days').val(dateAge);
}


function dob_unkown() {
    var dob_not_known = $('#dob_not_known').is(":checked");
    if (dob_not_known) {
        $('#patient_age').attr('readonly', false);
    } else {
        $('#patient_age').val('');
        $('#patient_age').attr('readonly', true);
        dobCalculation();
    }
}

function dobCalculation() {
    var age = $('#patient_age').val();
    var dateString = '';
    if (age) {
        var current_date = $('#current_date').val();
        var now = moment(current_date, 'MMM-DD-YYYY');
        var yearage = now.format('YYYY');
        var monthDob = now.format('MMM');
        var dateDob = now.format('DD');
        var yearDob = parseInt(yearage) - parseInt(age);
        dateString = monthDob + '-' + dateDob + '-' + yearDob;
    }
    $('#patient_dob').val(dateString);
    ageCalculation();
}


function advancePatientSearch() {
    $('.ajaxSearchBox').hide();
    var url = base_url + "/master/getPatientList";
    var patient_name = $('#patient').val();
    var uhid = $('#op_no').val();
    var ipno = $('#ip_no').val();
    var type = $('#searchpatient_type').val();
    var mobile = $('#searchpatient_mobile').val();
    var email = $('#searchpatient_email').val();
    var address = $('#searchpatient_address').val();
    var area = $('#searchpatient_area').val();
    var pincode = $('#searchpatient_pincode').val();
    var country = $('#searchpatient_country').val();
    var state = $('#searchpatient_state').val();
    var param = { _token: token, patient_name: patient_name, uhid: uhid, area: area, country: country, state: state, ipno: ipno, type: type, email: email, mobile: mobile, address: address, pincode: pincode };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getPatientListModelDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('.advancePatientSearchBtn').attr('disabled', true);
            $('.advancePatientSearchSpin').removeClass('fa fa-search');
            $('.advancePatientSearchSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#getPatientListModelDiv').html(data);
        },
        complete: function () {
            $("#getPatientListModelDiv").LoadingOverlay("hide");
            $('.advancePatientSearchBtn').attr('disabled', false);
            $('.advancePatientSearchSpin').removeClass('fa fa-spinner fa-spin');
            $('.advancePatientSearchSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function resetAdvancePatientSearch() {
    $('#patient').val('');
    $('#ip_no').val('');
    $('#op_no').val('');
    $('#searchpatient_type').val('All').select2();
    $('#searchpatient_mobile').val('');
    $('#searchpatient_email').val('');
    $('#searchpatient_address').val('');
    $('#searchpatient_area').val('');
    $('#searchpatient_pincode').val('');
    $('#searchpatient_country').val('');
    $('#searchpatient_state').val('');
    advancePatientSearch(2);
}


function isEmail() {
    var flag = true;
    var email = $('#patient_email').val();
    if (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        flag = regex.test(email);
    }
    if (!flag) {
        toastr.warning('Please enter valid email');
        $('#patient_email').focus();
    }
}


function selectPatient(from_type, patient_id) {
    var url = base_url + "/patient_register/selectSinglePatient";
    var param = { _token: token, patient_id: patient_id };
    $('#patient_id_hidden').val(patient_id);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            if (from_type == '1') {
                $("#getPatientListModel").modal('toggle');
            }
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#patient_uhid').val(obj[0].uhid);
            $('#patientsalutation').val(obj[0].title).select2();
            $('#patient_name').val(obj[0].patient_name);
            $('#patient_dob').val(obj[0].dob);
            $('#patient_gender').val(obj[0].gender).select2();
            $('#patient_phone').val(obj[0].phone);
            $('#patient_email').val(obj[0].email);
            $('#patient_address').val(obj[0].address);
        },
        complete: function () {
            $("#patientappoinment_div").LoadingOverlay("hide");
            ageCalculation();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getPatientGender() {
    var title_sex = $('#patientsalutation').find(':selected').data('sex');
    var gender = $("#patient_gender option[data-code='" + title_sex + "']").val();
    $("#patient_gender").val(gender).select2();
}

function checkMobileNo() {
    var length = $('#patient_phone').val().length;
    var flag = true;
    if (length != 10) {
        flag = false;
    }
    if (!flag) {
        toastr.warning('Please enter valid mobile number');
    }
    return flag;
}
