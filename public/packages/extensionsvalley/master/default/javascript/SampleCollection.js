$(document).ready(function () {

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    searchCollection();
});
var base_url = $('#base_url').val();
var token = $('#c_token').val();
var current_date = $('#current_date').val();
var item_array = new Array();



function searchBillNumber() {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('patient_billno_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var bill_no = $('#patient_billno').val();
        if (!bill_no) {
            $('#billno_hidden').val('');
            $("#patient_billno_AjaxDiv").hide();
        } else {
            var url = base_url + "/master/dynamicBillingReportSearch";
            var param = { _token: token, search_key: bill_no, search_key_id: 'bill_no', bill_tag: 'LAB' };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#patient_billno_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#patient_billno_AjaxDiv").html(html).show();
                    $("#patient_billno_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('patient_billno_AjaxDiv', event);
    }
}

function searchCollection() {
    var url = base_url + "/lab/searchSampleCollection";
    var bill_from_date = $('#bill_from_date').val();
    var bill_to_date = $('#bill_to_date').val();
    var patient_id = $('#patient_id_hidden').val();
    var searchSample = $('#searchSample').val();
    var patient_uhid = $('#patient_uhid').val();
    var billno = $('#patient_billno').val();
    var searchLab = $('#searchLab').val();
    var pending_test = $('#pending_test').is(":checked");
    var PatientStatus = $('#searchPatientStatus').val();
    var param = {
        _token: token,
        bill_from_date: bill_from_date,
        bill_to_date: bill_to_date,
        patient_id: patient_id,
        searchSample: searchSample,
        billno: billno,
        patient_uhid: patient_uhid,
        searchLab: searchLab,
        PatientStatus: PatientStatus,
        pending_test: pending_test
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#searchCollectionBtn').attr('disabled', true);
            $('#searchCollectionSpin').removeClass('fa fa-search');
            $('#searchCollectionSpin').addClass('fa fa-spinner fa-spin');
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            $('#patientappoinment_div').html(data);
        },
        complete: function () {
            $('#searchCollectionBtn').attr('disabled', false);
            $('#searchCollectionSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchCollectionSpin').addClass('fa fa-search');
            $("#patientappoinment_div").LoadingOverlay("hide");
        }
    });
}

function addGenearateSample(patient_id, sample_id, item_dept_id, from_type) {
    var url = base_url + "/lab/getSampleDetails";
    var uhid = $('#patient_sample_uhid' + patient_id).html();
    var patient_name = $('#patient_sample_patient_name' + patient_id).html();
    var visit_status = $('#patient_sample_visit_status' + patient_id).html();
    var samplename = $('#patient_sample_samplename' + patient_id).html();
    var bill_from_date = $('#bill_from_date').val();
    var bill_to_date = $('#bill_to_date').val();
    var param = {
        _token: token,
        sample_id: sample_id,
        from_type: from_type,
        uhid: uhid,
        samplename: samplename,
        visit_status: visit_status,
        bill_from_date: bill_from_date,
        bill_to_date: bill_to_date,
        item_dept_id: item_dept_id
    };
    $spin_label = 'fa fa-plus';
    $('#GenarateSampleBtn').show();
    if (from_type == 'history') {
        $spin_label = 'fa fa-history';
        $('#GenarateSampleBtn').hide();
    }
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $('#' + from_type + 'GenearateSampleBtn' + patient_id).attr('disabled', true);
            $('#' + from_type + 'GenearateSampleSpin' + patient_id).removeClass($spin_label);
            $('#' + from_type + 'GenearateSampleSpin' + patient_id).addClass('fa fa-spinner fa-spin');
            $('#getSampleDetailsModelHeader').html(patient_name + ' - ' + uhid);
        },
        success: function (data) {
            $('#getSampleDetailsModelDiv').html(data);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30,
            });
            setTimeout(function () {
                $(".theadfix_wrapper").floatThead({
                    position: "absolute",
                    scrollContainer: true,
                });
            }, 400);
            $("#getSampleDetailsModel").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        complete: function () {
            $('#' + from_type + 'GenearateSampleBtn' + patient_id).attr('disabled', false);
            $('#' + from_type + 'GenearateSampleSpin' + patient_id).removeClass('fa fa-spinner fa-spin');
            $('#' + from_type + 'GenearateSampleSpin' + patient_id).addClass($spin_label);
        }
    });
}


function deleteSampleGenearate(row_id) {
    var desc = $('#tableSampleGenetationItemDesc' + row_id).html();
    bootbox.confirm({
        message: "Are you sure, you want to remove " + desc + " ?",
        buttons: {
            'confirm': {
                label: "Remove",
                className: 'btn-warning',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-success'
            }
        },
        callback: function (result) {
            if (result) {
                $('#tableSapmpleGenetationRow' + row_id).remove();
                row_ct = 1;
                $(".row_class").each(function (i) {
                    $(this).find('.row_count_class').text(row_ct);
                    row_ct++;
                });
            }
        }
    });
}


function getSampleGenetation() {
    item_array = new Array();
    $('#SampleGenetationTbody tr').each(function () {
        var status = $(this).find("input[name='checkSampleData']").is(":checked");
        var labbillsID = $(this).find("input[name='tableSampleGenetationSamplabbillsID']").val();
        if (status && labbillsID) {
            var generated_sample_no = $(this).find("input[name='tableSampleGenetationSampleNo']").val();
            var sample_id = $(this).find("input[name='tableSampleGenetationsampleId']").val();
            var is_outsource = $(this).find("input[name='tableSampleGenetationSampleIsOutsource']").val();
            var patient_id = $(this).find("input[name='tableSampleGenetationSamplePatientId']").val();
            var visit_id = $(this).find("input[name='tableSampleGenetationSampleVisitId']").val();
            var visit_status = $(this).find("input[name='tableSampleGenetationSamplevisitStatus']").val();
            var doctor_id = $(this).find("input[name='tableSampleGenetationSampleDoctorId']").val();
            var location_id = $(this).find("input[name='tableSampleGenetationSampleLocationId']").val();
            var sample_status = $(this).find("input[name='tableSampleGenetationSampleStatus']").val();
            item_array.push({
                'labbillsID': labbillsID,
                'generated_sample_no': generated_sample_no,
                'sample_id': sample_id,
                'is_outsource': is_outsource,
                'patient_id': patient_id,
                'visit_id': visit_id,
                'visit_status': visit_status,
                'doctor_id': doctor_id,
                'location_id': location_id,
                'sample_status': sample_status
            });
        }
    });
    var len = Object.keys(item_array).length;
    return len;
}

function getCheckedColumns() {
    var status = $('#checkAllSampleData').is(":checked");
    if (status) {
        $('.checkSampleData').prop('checked', true);
    } else {
        $('.checkSampleData').prop('checked', false);
    }
}


function GenarateSample() {
    var array_len = getSampleGenetation();
    if (array_len != 0) {
        var test_string = ' samples ?';
        if (array_len == 1) {
            test_string = ' sample ?';
        }
        bootbox.confirm({
            message: "Are you sure, you  want to generate this" + test_string,
            buttons: {
                'confirm': {
                    label: "Generate Sample",
                    className: 'btn-success',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    var url = base_url + "/lab/GenarateSample";

                    var item_array_string = JSON.stringify(item_array);
                    var param = { _token: token, item_array_string: item_array_string };
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: param,
                        beforeSend: function () {
                            $('#GenarateSampleBtn').attr('disabled', true);
                            $('#GenarateSampleSpin').removeClass('fa fa-file');
                            $('#GenarateSampleSpin').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            var obj = JSON.parse(data);
                            if (obj.status == '1') {
                                $('#getSampleDetailsModel').modal('toggle');
                                toastr.success(obj.message);
                                searchCollection();

                            } else {
                                toastr.warning(obj.message);
                            }
                        },
                        complete: function () {
                            $('#GenarateSampleBtn').attr('disabled', false);
                            $('#GenarateSampleSpin').removeClass('fa fa-spinner fa-spin');
                            $('#GenarateSampleSpin').addClass('fa fa-file');
                        }
                    });
                }

            }
        });
    } else {
        toastr.warning('No Sample Selected');
    }
}

function reset_collection() {
    var current_date = $('#current_date').val();
    $('#searchPatientStatus').val('').select2();
    $('#searchLab').val('').select2();
    $('#patient_billno').val('');
    $('#billno_hidden').val('');
    $('#patient_uhid').val('');
    $('#searchSample').val('').select2();
    $('#patientsearch_ipstatus').val('');
    $('#bill_from_date').val(current_date);
    $('#bill_to_date').val(current_date);
    $('#patient_id_hidden').val('');
    $('#pending_test').prop('checked', true);
    searchCollection();
}
