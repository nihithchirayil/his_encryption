var base_url = $('#base_url').val();
var token = $('#token').val();

function searchIpConsultationItem(event, ajax_div, from_type) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var service_name = '';
    var url = "";
    var param = {};
    if (parseInt(from_type) == 1) {
        url = base_url + "/doctor_master_new/serviceSearchCriteria";
        service_name = $('#ip_consultation_item').val();
        service_name = service_name.trim();
        param = {
            service_name_search: service_name
        };
    } else if (parseInt(from_type) == 2) {
        url = base_url + "/doctor_master_new/serviceSearchCriteria";
        service_name = $('#ip_cross_consultation_item').val();
        service_name = service_name.trim();
        param = {
            cross_service_name_search: service_name
        };
    }

    if (event.keyCode == 13) {
        ajaxlistenter(ajax_div);
        return false;
    } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        if (service_name) {
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                },
            });
        } else {
            if (parseInt(from_type) == 1) {
                $('#ip_consultation_item_hidden').val('');
            } else if (parseInt(from_type) == 2) {
                $('#ip_cross_consultation_item_hidden').val('');
            }
            $("#" + ajax_div).html('').hide();
        }
    } else {
        ajax_list_key_down(ajax_div, event);
    }
}

function fillServiceDetails(id, service_desc) {
    $('#ip_consultation_item_hidden').val(id);
    $('#ip_consultation_item').val(service_desc);
    $('#ip_consultation_item').attr('title', service_desc);
    $("#ip_consultation_itemAjaxDiv").hide();
    fetchIpserviceprice(1);
}

function fillCrossServiceDetails(id, service_desc) {
    $('#ip_cross_consultation_item_hidden').val(id);
    $('#ip_cross_consultation_item').val(service_desc);
    $('#ip_cross_consultation_item').attr('title', service_desc);
    $("#ip_cross_consultation_itemAjaxDiv").hide();
    fetchIpserviceprice(0);
}


$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});

function saveIpPricing() {
    var doctor_id = $('#doctor_id_hidden').val();
    var ip_consultation_item_id = $('#ip_consultation_item_id').val();
    var ip_cross_consultation_item_id = $('#ip_cross_consultation_item_id').val();
    var ip_consultation_item = $('#ip_consultation_item_hidden').val();
    var ip_consultation_amount = $('#ip_consultation_amount').val();
    var ip_cross_consultation_item = $('#ip_cross_consultation_item_hidden').val();
    var ip_cross_consultation_amount = $('#ip_cross_consultation_amount').val();
    var url = base_url + "/doctor_master_new/saveIpPricing";

    if (ip_consultation_item) {
        var dataparams = {
            'doctor_id': doctor_id,
            'ip_consultation_item_id': ip_consultation_item_id,
            'ip_cross_consultation_item_id': ip_cross_consultation_item_id,
            'ip_consultation_item': ip_consultation_item,
            'ip_consultation_amount': ip_consultation_amount,
            'ip_cross_consultation_item': ip_cross_consultation_item,
            'ip_cross_consultation_amount': ip_cross_consultation_amount,
        };
        $.ajax({
            type: "POST",
            url: url,
            data: dataparams,
            beforeSend: function () {
                $('#saveIpPricingBtn').attr('disabled', true);
                $('#saveIpPricingSpin').removeClass('fa fa-save');
                $('#saveIpPricingSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function (data) {
                if (parseInt(data.status) == 1) {
                    toastr.success(data.message);
                    $('#ip_consultation_item_id').val(data.doctor_ip_consultation_id);
                    $('#ip_cross_consultation_item_id').val(data.doctor_ipcross_consultation_id);
                    $("#commonListModel").modal('toggle');
                } else {
                    toastr.warning(data.message);
                }
            },
            complete: function () {
                $('#saveIpPricingBtn').attr('disabled', false);
                $('#saveIpPricingSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveIpPricingSpin').addClass('fa fa-save');
            },
        });

    } else {
        toastr.warning("Ip consultation item ");
    }

}



function delete_op_pricing(pricing_id) {
    var url = base_url + "/doctor_master_new/DeleteDrOpPricing";
    bootbox.confirm({
        message: "Are you sure,you want to delete ?",
        buttons: {
            'confirm': {
                label: "Continue",
                className: 'btn-danger',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if (result) {
                var dataparams = {
                    'pricing_id': pricing_id,
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: dataparams,
                    beforeSend: function () {
                        $('#deleteopPricingbtn' + pricing_id).attr('disabled', true);
                        $('#deleteopPricingSpin' + pricing_id).removeClass('fa fa-times');
                        $('#deleteopPricingSpin' + pricing_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function (html) {
                        if (html == 1) {
                            toastr.success("Deleted successfully");
                            $('#rowDrOpPricing' + pricing_id).remove();
                        } else {
                            toastr.error("Error!");
                        }
                    },
                    complete: function () {
                        $('#deleteopPricingbtn' + pricing_id).attr('disabled', true);
                        $('#deleteopPricingSpin' + pricing_id).removeClass('fa fa-spinner fa-spin');
                        $('#deleteopPricingSpin' + pricing_id).addClass('fa fa-times');
                    }
                });
            }
        }
    });
}



function fetchIppriceingDetalis() {
    var doctor_id = $('#doctor_id_hidden').val();
    var url = base_url + "/doctor_master_new/fetchIppriceingDetalis";
    var dataparams = {
        doctor_id: doctor_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#saveOpPricingBtn').attr('disabled', true);
            $('#saveOpPricingSpin').removeClass('fa fa-save');
            $('#saveOpPricingSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            var service_data = JSON.parse(data);
            var is_consult = 0;
            $.each(service_data, function (key, item) {
                is_consult = item.is_consult ? item.is_consult : 0;
                if (parseInt(is_consult) == 1) {
                    $('#ip_consultation_item_id').val(item.pricing_id ? item.pricing_id : 0);
                    $('#ip_consultation_item').val(item.service_desc ? item.service_desc : '');
                    $('#ip_consultation_item_hidden').val(item.service_id ? item.service_id : 0);
                    $('#ip_consultation_amount').val(item.fees ? item.fees : 0);
                } else if (parseInt(is_consult) == 0) {
                    $('#ip_cross_consultation_item_id').val(item.pricing_id ? item.pricing_id : 0);
                    $('#ip_cross_consultation_item').val(item.service_desc ? item.service_desc : '');
                    $('#ip_cross_consultation_item_hidden').val(item.service_id ? item.service_id : 0);
                    $('#ip_cross_consultation_amount').val(item.fees ? item.fees : 0);
                }
            });
        },
        complete: function () {
            $('#saveOpPricingBtn').attr('disabled', false);
            $('#saveOpPricingSpin').removeClass('fa fa-spinner fa-spin');
            $('#saveOpPricingSpin').addClass('fa fa-save');
        },
    });
}

function fetchIpserviceprice(cons_type) {
    var doctor_id = $('#doctor_id_hidden').val();
    var url = base_url + "/doctor_master_new/fetchIpserviceprice";
    if (doctor_id == 0) {
        toastr.warning("Select doctor!");
        return false;
    }
    if (cons_type == 1) {
        var service_id = $('#ip_consultation_item_hidden').val();
    } else {
        var service_id = $('#ip_cross_consultation_item_hidden').val();
    }

    var dataparams = {
        'service_id': service_id,
        'cons_type': cons_type,
        'doctor_id': doctor_id,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            if (cons_type == 1) {
                $("#ip_consultation_amount").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            } else {
                $("#ip_cross_consultation_amount").LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            }

        },
        success: function (html) {
            if (html != '0') {
                if (cons_type == 1) {
                    $('#ip_consultation_amount').val(html);
                } else {
                    $('#ip_cross_consultation_amount').val(html);
                }
            } else {
                if (cons_type == 1) {
                    $('#ip_consultation_amount').val();
                } else {
                    $('#ip_cross_consultation_amount').val();
                }
            }

        },
        complete: function () {
            $("#ip_consultation_amount").LoadingOverlay("hide");
            $("#ip_cross_consultation_amount").LoadingOverlay("hide");
        },
    });
}

$('#op_pricing_company').on('change', function () {
    var company_id = $('#op_pricing_company').val();
    var url = base_url + "/doctor_master_new/SelectPricingOfCompany";
    var doctor_id = $('#doctor_id_hidden').val();
    if (doctor_id == 0) {
        toastr.warning("Select doctor!");
        return false;
    }
    var dataparams = {
        'company_id': company_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#op_pricing_pricing').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {

            if (html != 0) {
                var dropdown = $('#op_pricing_pricing');
                dropdown.empty();
                $.each(html, function (key, item) {
                    dropdown.append(
                        $('<option>', {
                            value: key,
                            text: item
                        }, '</option>'))
                });

            }
            setTimeout(function () {
                fetchOpPricing();
            }, 500);
        },
        complete: function () {
            $('#op_pricing_pricing').LoadingOverlay("hide");
        },
    });
});


$('#op_pricing_company_search').on('change', function () {
    var company_id = $('#op_pricing_company_search').val();
    var url = base_url + "/doctor_master_new/SelectPricingOfCompany";
    var doctor_id = $('#doctor_id_hidden').val();
    if (doctor_id == 0) {
        toastr.warning("Select doctor!");
        return false;
    }
    var dataparams = {
        'company_id': company_id,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#op_pricing_pricing_search').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            if (html != 0) {
                var dropdown = $('#op_pricing_pricing_search');
                dropdown.empty();
                $.each(html, function (key, item) {
                    dropdown.append(
                        $('<option>', {
                            value: key,
                            text: item
                        }, '</option>'))
                });

            }
        },
        complete: function () {
            $('#op_pricing_pricing_search').LoadingOverlay("hide");
        },
    });
});


function setOpPricing() {
    var status = $('#set_op_pricing').is(":checked");
    if (status) {
        $('.op_pricing_service_charges').prop("readonly", false);
    } else {
        $('.op_pricing_service_charges').prop("readonly", true);
    }
}



function getOpServiceData() {
    request_array = {};
    var op_priicing = $('#set_op_pricing').is(":checked");
    if (op_priicing) {
        $('#opservicedata_table tr').each(function () {
            var row_id = $(this).find("input[name='op_pricing_row_id']").val();
            var service_id = $(this).find("input[name='op_pricing_service_id']").val();
            var service_code = $(this).find("input[name='op_pricing_service_code']").val();
            if (service_id && service_code) {
                var reg = $(this).find("input[name='reg']").val();
                var ren = $(this).find("input[name='ren']").val();
                var mlc = $(this).find("input[name='mlc']").val();
                var new_born = $(this).find("input[name='new_born']").val();

                request_array[row_id] = {
                    service_id: service_id,
                    service_code: service_code,
                    REG: reg,
                    REN: ren,
                    mlcreg: mlc,
                    newborn: new_born,
                }
            }
        });
    }
    return request_array;
}



function saveOpPricing() {
    var doctor_id = $('#doctor_id_hidden').val();
    var company_id = $('#op_pricing_company').val();
    var pricing_id = $('#op_pricing_pricing').val();
    var expiry_date = $('#op_pricing_expirydate').val();
    var op_priicing_check = $('#set_op_pricing').is(":checked");
    var op_priicing = getOpServiceData();
    if (parseInt(doctor_id) != 0) {
        if (company_id) {
            if (pricing_id) {
                var op_service_array = JSON.stringify(op_priicing);
                var dataparams = {
                    op_service_array: op_service_array,
                    doctor_id: doctor_id,
                    company_id: company_id,
                    pricing_id: pricing_id,
                    expiry_date: expiry_date,
                    op_priicing_check: op_priicing_check
                }
                var url = base_url + "/doctor_master_new/saveOpPricing";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: dataparams,
                    beforeSend: function () {
                        $('#saveOpPricingBtn').attr('disabled', true);
                        $('#saveOpPricingSpin').removeClass('fa fa-save');
                        $('#saveOpPricingSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        if (parseInt(data.status) == 1) {
                            toastr.success(data.message);
                            searchOpPricing();
                            resetOPPricing();
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    complete: function () {
                        $('#saveOpPricingBtn').attr('disabled', false);
                        $('#saveOpPricingSpin').removeClass('fa fa-spinner fa-spin');
                        $('#saveOpPricingSpin').addClass('fa fa-save');
                    },
                });
            } else {
                toastr.warning("Select pricing!");
            }
        } else {
            toastr.warning("Select company!");
        }

    } else {
        toastr.warning("Select doctor!");
    }
}



function resetOPPricing() {
    $('#op_pricing_company').val('');
    $('#op_pricing_pricing').val('');
    $('#set_op_pricing').prop('checked', false);
    $('.op_pricing_service_charges').prop("readonly", true);
    $('.op_pricing_service_charges').val('');
}

function fetchOpPricing() {
    var doctor_id = $('#doctor_id_hidden').val();
    var company_id = $('#op_pricing_company').val();
    var pricing_id = $('#op_pricing_pricing').val();
    if (doctor_id == 0) {
        toastr.warning("Select doctor!");
        return false;
    }
    if (company_id == '') {
        toastr.warning("Select company!");
        return false;
    }
    if (pricing_id == '') {
        toastr.warning("Select pricing!");
        return false;
    }
    var dataparams = {
        'doctor_id': doctor_id,
        'company_id': company_id,
        'pricing_id': pricing_id,
    };
    var url = base_url + "/doctor_master_new/fetchOpPricing";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#op_service_list_table').LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#337AB7'
            });
        },
        success: function (html) {
            if (html == 0) {
                $('.op_pricing_service_charges ').val('');
            } else {
                var obj = JSON.parse(html);
                for (var i = 1; i <= 4; i++) {
                    if (obj.reg_array[i - 1] != 'null') {
                        $('#reg_' + i).val(obj.reg_array[i - 1]);
                    }
                    if (obj.ren_array[i - 1] != 'null') {
                        $('#ren_' + i).val(obj.ren_array[i - 1]);
                    }
                    if (obj.mlc_array[i - 1] != 'null') {
                        $('#mlc_' + i).val(obj.mlc_array[i - 1]);
                    }
                    if (obj.new_born_array[i - 1] != 'null') {
                        $('#new_born_' + i).val(obj.new_born_array[i - 1]);
                    }

                }
            }
        },
        complete: function () {
            $('#op_service_list_table').LoadingOverlay("hide");
        },
    });
}


function searchOpPricing() {
    var company_id = $('#op_pricing_company_search').val();
    var pricing_id = $('#op_pricing_pricing_search').val();
    var doctor_id = $('#doctor_id_hidden').val();
    var dataparams = {
        doctor_id: doctor_id,
        company_id: company_id,
        pricing_id: pricing_id,
    };

    var url = base_url + "/doctor_master_new/fetchOpPricingSearch";
    $.ajax({
        type: "POST",
        url: url,
        data: dataparams,
        beforeSend: function () {
            $('#searchOpPricingBtn').attr('disabled', true);
            $('#searchOpPricingSpin').removeClass('fa fa-search');
            $('#searchOpPricingSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $("#getOpPricingListData").html(data);
        },
        complete: function () {
            $('#searchOpPricingBtn').attr('disabled', false);
            $('#searchOpPricingSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchOpPricingSpin').addClass('fa fa-search');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
    });

}


function changeOpPricing(from_com, from_pricing) {
    var company_id = $('#' + from_com).val();
    if (company_id) {
        var dataparams = {
            company_id: company_id,
        };
        var url = base_url + "/doctor_master_new/op_pricing_id";
        $.ajax({
            type: "POST",
            url: url,
            data: dataparams,
            beforeSend: function () {
                $('#' + from_pricing).attr('disabled', true);
            },
            success: function (data) {
                $('#' + from_pricing).html(data);
            },
            complete: function () {
                $('#' + from_pricing).attr('disabled', false);
            },
        });
    } else {
        data = "<option value=''>All</option>";
        $('#' + from_pricing).html(data);
    }
}
