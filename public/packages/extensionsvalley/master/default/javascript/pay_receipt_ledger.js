$(document).ready(function () {
    calculate_balance();
    calculate_total();
    if (window.location.href.indexOf("add_pay_receipt") > -1)
    {
         changePaymentType(1);
    }
    $("#ledger_item_desc-1").focus();
    $('#bills').select2();
});
function calculate_total(){
 var ttl_bill_amnt = 0;
    $(".bill_amount").each(function () {
        var bill_amnt = $(this).val();
        if (bill_amnt != '') {
            ttl_bill_amnt = parseFloat(ttl_bill_amnt) + parseFloat(bill_amnt);
        }

    });
     $("#total_amount_by_bill").val(ttl_bill_amnt);
    }
function calculate_balance() {

    var ttl_paid_amnt = 0;
    payment_receipt = $('input[name="payment_receipt"]:checked').val();
    var current_balance = $("#current_balance").val();
    $(".bill_paid_amount").each(function () {
        var paid_amnt = $(this).val();
        if (paid_amnt != '') {
            ttl_paid_amnt = parseFloat(ttl_paid_amnt) + parseFloat(paid_amnt);
        }

    });
    if (payment_receipt == 1) {
        var new_current_balance = parseFloat(current_balance) - ttl_paid_amnt;
    } else {
        var new_current_balance = parseFloat(current_balance) + ttl_paid_amnt;
    }
    $("#paid_total").val(ttl_paid_amnt);
    $("#ttl_dif_label").html(ttl_paid_amnt);

}
var payment_mod = $('#cash_payment_mod_hidden').val();
payment_mod = JSON.parse(payment_mod);
paymentmod_search_vars = '';
$.each(payment_mod, function (key, val) {

    paymentmod_search_vars += "<option  value='" + val.id + "'>" + val.name + "</option>";
});
function select_item_desc(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
    var contra_val = $('input[name="payment_receipt"]:checked').val();
    var ajax_div = 'search_ledger_item_box-' + id;

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var ledger_desc = $('#ledger_item_desc-' + id).val();
        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: "",
                data: 'ledger_desc=' + ledger_desc + '&search_ledger_desc=1&contra_val='+contra_val,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}



function fillLedgerValues(e, ledger_name, id) {
    $(e).closest("tr").find("input[name='payee[]']").val(ledger_name);
    $(e).closest("tr").find("input[name='payee_id[]']").val(id);
    $(e).closest("div").hide();
    setTimeout(function(){
    $(e).parents("tr").find("input[name='referrence_no[]']").focus();
    },200);
    fill_new_row();
}
function fill_new_row(){
var divElements = document.querySelectorAll('.search_ledger_item_box');
    var row_id = divElements.length;
    var row_id = parseFloat(row_id) + 1;
     var contra_val = $('input[name="payment_receipt"]:checked').val();
    response = '<tr><td ><input type="text" value="" name="payee[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event)" id="ledger_item_desc-' + row_id + '">\n\
<input type="hidden" value="" name="payee_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-' + row_id + '">\n\
<div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-' + row_id + '" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;\n\
margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"> \n\
</div></td>\n\
    <td><div class="input-group"><input type="text" value="" name="referrence_no[]" class="form-control" id="edit_bill_no_' + row_id + '"><div class="input-group-btn"><span class="btn btn_green_bg"> <i class="fa fa-search " onclick="search_bill_no(' + row_id + ')" style="cursor:pointer" ></i></span></div></div></td>\n\
    <td style="display: none"><input type="text" value="" name="amount[]" autocomplete="off"  class="form-control bill_amount"  onkeyup="number_validation(this);calculate_total()" id="edit_bill_amount_' + row_id + '"></td>\n\
    <td ><input type="text" value="" name="paid_amnt[]" class="form-control bill_paid_amount" onkeyup="number_validation(this);calculate_balance();" id="edit_bill_paid_amount_' + row_id + '"> </td>\n\
    <td style="display: none"><select name="payment_mode[]" class="form-control">' + paymentmod_search_vars + '</select></td>\n\
    <td ><input type="text" value="" name="naration[]"  class="form-control"></td>\n\
    <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td></tr>';
    if(contra_val != 3){
     var ck = 0;
        $('#payment_receipt_tbody tr').each(function () {
        if ($(this).find('input[name="payee[]"]').val() ==''){
            ck = 1;
        }
    });
    if(ck == 0){
             $('#payment_receipt_tbody').append(response);
    }
    }
    $('#payment_receipt_tbody').find(".datepicker").datetimepicker({
        format: 'DD-MM-YYYY'
    });
    }
$("#pay_rec_id").on('click', '.delete_return_entry_added', function () {
    $(this).closest('tr').remove();
});
function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function deleteLedgerDetails(id,amount) {
    if (confirm("Are you sure you want to Delete ?")) {
        var payment_receipt = $('input[name="payment_receipt"]:checked').val();
        var bank_details = $("#bank_details").val();
        var head_id = $("#head_id").val();
        var params = {delete_ledger_list: 1, detail_id: id,amount:amount,payment_receipt:payment_receipt,bank_details:bank_details,head_id:head_id};
        $.ajax({
            type: "GET",
            url: '',
            data: params,
            beforeSend: function () {
            },
            success: function (data) {
                if (data == 1) {
                    var after_del = $("#paid_total").val();
                    var total_after_del = parseFloat(after_del)-parseFloat(amount);
                    $("#paid_total").val(total_after_del);
                    $("#ttl_dif_label").html(total_after_del);
                    $('#ledger_row_id_' + id).remove();
                    toastr.success('Deleted Succcessfuly !!');
                }
            },
            complete: function () {

            }
        });
    }
}
function search_bill_no(k) {
    $("#search_bill_modal").modal('show');
    $("#bill_id").val('');
    $("#voucher_no").val('');
    $("#bills option:selected").prop("selected", false);
    $("#bills option:selected").removeAttr("selected");
    if($('#edit_bill_no_'+k).val() !=''){
    var selectedOptions = ($('#edit_bill_no_'+k).val().split(','));
        for(var i in selectedOptions) {
        var optionVal = selectedOptions[i];
        $("#bills").find("option[value="+optionVal+"]").prop("selected", "selected");
    }
}
$("#bills").select2().trigger('change');
 $("#search_bill_modal_id").val(k);
}
function searchVoucherNo(id, event) {
    url = $("#ins_base_url").val();
    var bill_type = $("#bill_type").val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = 'search_voucher_no';

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var bill_no = $('#' + id).val();
        if (bill_no == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: url + "/master/select_bill_no",
                data: 'search_voucher_no=' + bill_no ,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillVoucherValues(e, bill_no) {
    $("#voucher_no").val(bill_no);
    var search_bill_modal_id = $("#search_bill_modal_id").val();
    url = $("#ins_base_url").val();
     $.ajax({
                type: "GET",
                url: url + "/master/select_ledger_bill",
                data: 'select_ledger_bill=' + bill_no,
                beforeSend: function () {
                },
                success: function (html) {
                    $("#edit_bill_no_"+search_bill_modal_id).val(html);
                },
                complete: function () {
                  $("#search_bill_modal").modal('hide');
                }
            });

}
//function fillVoucherValues(e, voucher_no) {
//     url = $("#ins_base_url").val();
//    var rowid = $("#search_bill_modal_id").val();
//    $.ajax({
//                type: "GET",
//                url: url + "/master/ledger_details_by_voucher",
//                data: 'voucher_no=' + voucher_no,
//                beforeSend: function () {
//                    $("#search_bill_modal").modal('hide');
//                },
//                success: function (html) {
//                    $("#payment_receipt_tbody").append(html);
//                    $("#search_bill_no").hide();
//                    $("#search_bill_modal").modal('hide');
//                    $("#edit_bill_no_"+rowid).closest('tr').remove();
//        $("#payment_receipt_tbody tr td input:nth-child(0)").filter(function () {
//           if($.trim($(this).val()) == ''){
//              //$(this).closest('tr').remove();
//           }
//        });
//                    //SETTING ID
////                    var divElements = document.querySelectorAll('.ledger_item_desc');
////                    var descElements = document.querySelectorAll('.return_item_desc');
////                    var batch_class = document.querySelectorAll('.batch_wise_info_class');
////                        for (var i = 0; i < divElements.length; i++)
////                        {
////                            divElements[i].id = 'search_return_item_box-' + i;
////                            descElements[i].id = 'ledger_item_desc_hidden-' + i;
////                            batch_class[i].id = 'batch_wise_info_btn-' + i;
////
////                        }
//
//                    calculate_balance();
//                    calculate_total();
//
//                },
//                complete: function () {
//
//                }
//            });
//
//}
function selectCurrentBalance(val){
    url = $("#ins_base_url").val();
     $.ajax({
                type: "GET",
                url: url + "/master/select_current_balance",
                data: 'select_current_balance=' + val,
                beforeSend: function () {
                      $("#current_balance_load").append('<span style="width:300px;text-align: center;" id="current_balance_loader_show"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></span>').show();
                },
                success: function (html) {
                    $("#current_balance").val(html);
                    $("#current_balance_loader_show").remove();
                },
                complete: function () {

                }
            });
}
function validate() {
    var pay_receipt = $('input[name="payment_receipt"]:checked').val();
    var bank_details = $("#bank_details").val();
    var ok = true;
    if (pay_receipt === '' || bank_details === '') {
        ok = false;
        alert("Please select bank and  Type");
    }

    var reqlength = $('.ledger_item_desc').length;
    var value = $('.ledger_item_desc').filter(function () {
        return this.value != '';
    });
        if(value.length == 0){
        toastr.error('No item added for save !!');
         ok = false;
         return false;
    }
    if (value.length>0 && (value.length !== reqlength)) {
//    var value_date = $('.datepicker').filter(function () {
//        return this.value != '';
//    });
//    if (value_date.length>=0 && (value_date.length !== value.length)) {
//        alert('Please fill out all Refference Date fields.');
//         ok = false;
//         return false;
//    }
 var value_bill_paid_amount = $('.bill_paid_amount').filter(function () {
        return this.value != '';
    });
    if (value_bill_paid_amount.length>=0 && (value_bill_paid_amount.length !== value.length)) {
        alert('Please fill out all Amount fields.');
         ok = false;
         return false;
}
}
$('#save_spin_icon').removeClass('fa fa-save');
$('#save_spin_icon').addClass('fa fa-spinner fa-spin');

    saveAfterValidation(ok);
}
function saveAfterValidation(ok){
    if(ok){
        $("#form").submit();
    }
}
function changePaymentType(e){
    if(e == 1){
        var json_default_bank = JSON.parse($("#default_payment_bank").val());
        console.log(json_default_bank);
    }else{
        var json_default_bank = JSON.parse($("#default_rec_bank").val());
        console.log(json_default_bank);
    }
    $("#bank_details").val(json_default_bank.id);
    $("#current_balance").val(json_default_bank.current_balance);
}
function backToList(){
    var url = $("#ins_base_url").val();
    window.location = url + "/master/pay_receipt_ledger_list";
}
$("#go").click(function(){
    var search_bill_modal_id = $("#search_bill_modal_id").val();
      var selMulti = $.map($("#bills option:selected"), function (el, i) {
         return $(el).text();
     });
     $("#edit_bill_no_"+search_bill_modal_id).val(selMulti.join(", "));
     $("#search_bill_modal").modal('hide');
});
