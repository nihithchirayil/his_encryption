function searchPatientUhid() {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('patient_uhid_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var uhid = $('#patient_uhid').val();
        if (!uhid) {
            $('#patient_id_hidden').val('');
            $("#patient_uhid_AjaxDiv").hide();
        } else {
            var url = base_url + "/master/dynamicBillingReportSearch";
            var param = { _token: token, search_key: uhid, search_key_id: 'op_no', patient_type: 1 };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#patient_uhid_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    $("#patient_uhid_AjaxDiv").html(html).show();
                    $("#patient_uhid_AjaxDiv").find('li').first().addClass('liHover');
                }
            });
        }

    } else {
        ajax_list_key_down('patient_uhid_AjaxDiv', event);
    }
}


function searchPatientPhone() {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('patient_phone_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var phone = $('#patient_phone').val();
        if (!phone) {
            $("#patient_phone_AjaxDiv").hide();
        } else {
            var visit_date = $('#patient_visit_date').val();
            var doctor_id = $('#patient_doctor_hidden').val();
            var url = base_url + "/master/dynamicBillingReportSearch";
            var param = { _token: token, search_key: phone, visit_date: visit_date, doctor_id: doctor_id, search_key_id: 'phone', patient_type: 1 };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#patient_phone_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    if (html.trim() != "<li>No results found!</li>") {
                        $("#patient_phone_AjaxDiv").html(html).show();
                        $("#patient_phone_AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#patient_phone_AjaxDiv").hide();
                    }

                }
            });
        }

    } else {
        ajax_list_key_down('patient_phone_AjaxDiv', event);
    }
}


function fillSearchDetials(data_id, data_name, from_type) {
    if (from_type == 'op_no') {
        $('#patient_uhid').val(data_name);
        $("#patient_uhid_AjaxDiv").hide();
        $('#patient_id_hidden').val(data_id);
        var blade_type = $('#blade_type').val();
        if (blade_type != '5') {
            selectPatient(2, data_id);
        }
    } else if (from_type == 'bill_no') {
        $('#billno_hidden').val(data_id);
        $('#patient_billno').val(data_name);
        $("#patient_billno_AjaxDiv").hide();
    }
    $('.ajaxSearchBox').hide();
}


function ageCalculation(dateString = '') {
    var current_date = $('#current_date').val();
    if (dateString == "")
        dateString = $('#patient_dob').val();
    var yearAge = '';
    var monthAge = '';
    var dateAge = '';
    if (dateString) {

        var now = moment(current_date, 'MMM-DD-YYYY');

        var yearNow = now.format('YYYY');
        var monthNow = now.format('MM');
        var dateNow = now.format('DD');

        var date = moment(dateString, 'MMM-DD-YYYY');
        var yearDob = date.format('YYYY');
        var monthDob = date.format('MM');
        var dateDob = date.format('DD');
        yearAge = parseInt(yearNow) - parseInt(yearDob);

        if (monthNow >= monthDob)
            monthAge = parseInt(monthNow) - parseInt(monthDob);
        else {
            yearAge--;
            monthAge = 12 + parseInt(monthNow) - parseInt(monthDob);
        }

        if (dateNow >= dateDob) {
            dateAge = parseInt(dateNow) - parseInt(dateDob);
        } else {
            monthAge--;
            dateAge = 31 + parseInt(dateNow) - parseInt(dateDob);

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }
    }
    $('#age_years').val(yearAge);
    $('#age_month').val(monthAge);
    $('#age_days').val(dateAge);

    if (parseInt(yearAge) > 0) {
        $('#patient_age').val(yearAge);
        $('#patientagein').val(1).select2();
    } else if (parseInt(monthAge) > 0) {
        $('#patient_age').val(monthAge);
        $('#patientagein').val(2).select2();
    } else if (parseInt(dateAge) > 0) {
        $('#patient_age').val(dateAge);
        $('#patientagein').val(3).select2();
    }

    return yearAge + 'Y ' + monthAge + 'M ' + dateAge + 'D';
}

function getagefromdob() {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    var age_val = $('#patient_age').val();
    if (!age_val) {
        age_val = 0;
    }

    var today = new Date();
    var agein = $('#patientagein').val();
    var newDate = new Date();
    if (parseInt(agein) == 1) {
        newDate = new Date(today.setFullYear(today.getFullYear() - parseInt(age_val)));
    } else if (parseInt(agein) == 2) {
        newDate = new Date(today.setMonth(today.getMonth() - parseInt(age_val)));
    } else if (parseInt(agein) == 3) {
        newDate = new Date(today.setDate(today.getDate() - parseInt(age_val)));
    }
    newdate = monthNames[newDate.getMonth()] + '-' + addfullday(newDate.getDate()) + '-' + newDate.getFullYear();
    $('#patient_dob').val(newdate);
}

function addfullday(day_data) {
    if (day_data.toString().length == 1) {
        day_data = '0' + day_data;
        day_data = day_data.toString();
    }
    return day_data;
}


function getAge() {
    var dateString = $('#patient_dob').val();
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    $('#patient_age').val(isNumber(age));
    ageCalculation();
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n) ? n : '';
}


function dob_unkown() {
    var dob_not_known = $('#dob_not_known').is(":checked");
    if (dob_not_known) {
        $('#patient_age').attr('readonly', false);
    } else {
        $('#patient_age').val('');
        $('#patient_age').attr('readonly', true);
        dobCalculation();
    }
}

function dobCalculation() {
    var age = $('#patient_age').val();
    var dateString = '';
    if (age) {
        var current_date = $('#current_date').val();
        var now = moment(current_date, 'MMM-DD-YYYY');
        var yearage = now.format('YYYY');
        var monthDob = now.format('MMM');
        var dateDob = now.format('DD');
        var yearDob = parseInt(yearage) - parseInt(age);
        dateString = monthDob + '-' + dateDob + '-' + yearDob;
    }
    $('#patient_dob').val(dateString);
    ageCalculation();
}


function advancePatientSearch(from_type) {
    if (from_type == '1') {
        $("#getPatientListModel").modal({
            backdrop: 'static',
            keyboard: false
        });
        resetAdvancePatientSearch();
        return false;
    }
    var url = base_url + "/eyeBilling/getPatientList";
    var ip_patient_not_need = $('#ip_patient_not_need').val();
    var patient_name = $('#searchpatient_name').val();
    var uhid = $('#searchpatient_uhid').val();
    var ipno = $('#searchpatient_ipno').val();
    var ip_status = $('#patientsearch_ipstatus').val();
    var type = $('#searchpatient_type').val();
    var mobile = $('#searchpatient_mobile').val();
    var email = $('#searchpatient_email').val();
    var address = $('#searchpatient_address').val();
    var area = $('#searchpatient_area').val();
    var pincode = $('#searchpatient_pincode').val();
    var country = $('#searchpatient_country').val();
    var state = $('#searchpatient_state').val();
    var param = { _token: token, ip_patient_not_need: ip_patient_not_need, ip_status: ip_status, patient_name: patient_name, uhid: uhid, area: area, country: country, state: state, ipno: ipno, type: type, email: email, mobile: mobile, address: address, pincode: pincode };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            $("#getPatientListModelDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            $('.advancePatientSearchBtn').attr('disabled', true);
            $('.advancePatientSearchSpin').removeClass('fa fa-search');
            $('.advancePatientSearchSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            $('#getPatientListModelDiv').html(data);

            $(document).on("click", ".page-link", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                if (url && url != 'undefined') {
                    var patient_name = $('#searchpatient_name').val();
                    var uhid = $('#searchpatient_uhid').val();
                    var ipno = $('#searchpatient_ipno').val();
                    var type = $('#searchpatient_type').val();
                    var mobile = $('#searchpatient_mobile').val();
                    var email = $('#searchpatient_email').val();
                    var ip_status = $('#patientsearch_ipstatus').val();
                    var address = $('#searchpatient_address').val();
                    var ip_patient_not_need = $('#ip_patient_not_need').val();
                    var area = $('#searchpatient_area').val();
                    var pincode = $('#searchpatient_pincode').val();
                    var country = $('#searchpatient_country').val();
                    var state = $('#searchpatient_state').val();
                    var param = {
                        // _token: token,
                        ip_status: ip_status,
                        patient_name: patient_name,
                        ip_patient_not_need: ip_patient_not_need,
                        uhid: uhid,
                        area: area,
                        country: country,
                        state: state,
                        ipno: ipno,
                        type: type,
                        email: email,
                        mobile: mobile,
                        address: address,
                        pincode: pincode
                    };
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: param,
                        beforeSend: function () {
                            $("#getPatientListModelDiv").LoadingOverlay("show", {
                                background: "rgba(255, 255, 255, 0.7)",
                                imageColor: '#337AB7'
                            });
                            $('.advancePatientSearchBtn').attr('disabled', true);
                            $('.advancePatientSearchSpin').removeClass('fa fa-search');
                            $('.advancePatientSearchSpin').addClass('fa fa-spinner fa-spin');
                        },
                        success: function (data) {
                            $('#getPatientListModelDiv').html(data);
                            $('.theadscroll').perfectScrollbar({
                                wheelPropagation: true,
                                minScrollbarLength: 30
                            });
                        },
                        complete: function () {
                            $("#getPatientListModelDiv").LoadingOverlay("hide");
                            $('.advancePatientSearchBtn').attr('disabled', false);
                            $('.advancePatientSearchSpin').removeClass('fa fa-spinner fa-spin');
                            $('.advancePatientSearchSpin').addClass('fa fa-search');
                        },
                        error: function () {
                            toastr.error("Error Please Check Your Internet Connection");
                        }
                    });
                }
                return false;
            });
        },
        complete: function () {
            $("#getPatientListModelDiv").LoadingOverlay("hide");
            $('.advancePatientSearchBtn').attr('disabled', false);
            $('.advancePatientSearchSpin').removeClass('fa fa-spinner fa-spin');
            $('.advancePatientSearchSpin').addClass('fa fa-search');
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });

}

function resetAdvancePatientSearch() {
    $('#searchpatient_name').val('');
    $('#searchpatient_uhid').val('');
    $('#searchpatient_ipno').val('');
    $('#searchpatient_type').val('All').select2();
    $('#searchpatient_mobile').val('');
    $('#searchpatient_email').val('');
    $('#searchpatient_address').val('');
    $('#searchpatient_area').val('');
    $('#searchpatient_pincode').val('');
    $('#searchpatient_country').val('');
    $('#searchpatient_state').val('');
    advancePatientSearch(2);
}


function isEmail() {
    var flag = true;
    var email = $('#patient_email').val();
    if (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        flag = regex.test(email);
    }
    if (!flag) {
        toastr.warning('Please enter valid email');
        $('#patient_email').focus();
    }
    return flag;
}



function selectPatient(from_type, patient_id) {
    var url = base_url + "/patient_register/selectSinglePatient";
    var visit_date = $('#patient_visit_date').val();
    var doctor_id = $('#patient_doctor_hidden').val();
    var blade_id = $('#blade_id').val();
    var patient_phone = $('#patient_phone').val();
    var allslots = $('#getbooking_allslots').is(":checked");
    var param = { _token: token, patient_phone: patient_phone, blade_id: blade_id, allslots: allslots, visit_date: visit_date, doctor_id: doctor_id, patient_id: patient_id };
    $('#patient_id_hidden').val(patient_id);
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function () {
            if (from_type == '1') {
                $("#getPatientListModel").modal('toggle');
            }
            $("#patientappoinment_div").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $('#patient_uhid').val(obj.result[0].uhid);
            $('#patientsalutation').val(obj.result[0].title).select2();
            $('#patient_name').val(obj.result[0].patient_name);
            $('#patient_dob').val(obj.result[0].dob);
            $('#patient_gender').val(obj.result[0].gender).select2();
            $('#patient_phone').val(obj.result[0].phone ? obj.result[0].phone.trim() : '');
            $('#alternate_phone').val(obj.result[0].alternate_mobile_no ? obj.result[0].alternate_mobile_no.trim() : '');
            $('#patient_email').val(obj.result[0].email);
            $('#patient_address').val(obj.result[0].address);
            $('#doctor_charges_hidden').html(obj.total_charge);
            if (blade_id == '1' || blade_id == '2') {
                if (parseInt(obj.appointment_count) != 0) {
                    $('#getDoctorChagesModelDiv').html(obj.appointment_list);
                    $('#getDoctorChagesModelHeader').html('Appointment List');
                    $("#getDoctorChagesModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }
            if (blade_id == '2') {
                if (obj.result[0].pincode) {
                    $('#patient_pincode').val(obj.result[0].pincode);
                    getPostalCodes(obj.result[0].area);
                } else {
                    $('#patient_pincode').val('');
                    $('#add_area').prop('checked', true);
                    $('#add_patient_area').val(obj.result[0].area ? obj.result[0].area : '');
                    patientAreaStatus();
                }
                var pricing_id = obj.result[0].pricing_id ? obj.result[0].pricing_id : 0;
                if (parseInt(pricing_id) != 0) {
                    $('#default_pricing_id_edit').val(pricing_id);
                    getPricingDetalis('default_pricing_id_edit');
                } else {
                    getPricingDetalis('default_pricing_id');
                }

                $('#patient_default_area').val(obj.result[0].area ? obj.result[0].area : '');

                insurance_array = new Array();
                insurance_array.push({
                    'primary_insurance_company': obj.result[0].insurance_company ? obj.result[0].insurance_company : 0,
                    'primary_insurance_pricing': obj.result[0].insurance_pricing ? obj.result[0].insurance_pricing : 0,
                    'primary_authorisation_letter_no': obj.result[0].authorisation_letter_no ? obj.result[0].authorisation_letter_no : '',
                    'primary_insurance_id_no': obj.result[0].insurance_id_no ? obj.result[0].insurance_id_no : '',
                    'primary_credit_limit': obj.result[0].credit_limit ? obj.result[0].credit_limit : '',
                    'primary_card_type': obj.result[0].card_type ? obj.result[0].card_type : '',
                    'primary_color_of_card': obj.result[0].color_of_card ? obj.result[0].color_of_card : '',
                    'primary_issue_date': obj.result[0].issue_date ? obj.result[0].issue_date : '',
                    'primary_expiry_date': obj.result[0].expiry_date ? obj.result[0].expiry_date : '',
                    'primary_dependent_relation': obj.result[0].dependent_relation ? obj.result[0].dependent_relation : '',
                });
                patient_more_details = new Array();
                patient_more_details.push({
                    'patient_blood_grp': obj.result[0].blood_group ? obj.result[0].blood_group : '',
                    'patient_MaritalStatus': obj.result[0].marital_status ? obj.result[0].marital_status : '',
                    'patient_relation': obj.result[0].reference_type ? obj.result[0].reference_type : '',
                    'patient_relationname': obj.result[0].reference_remarks ? obj.result[0].reference_remarks : '',
                    'patient_idproof_type': obj.result[0].id_proof_type ? obj.result[0].id_proof_type : '',
                    'vip_patient_comments': obj.result[0].vip_comments ? obj.result[0].vip_comments : '',
                    'patient_passport_no': obj.result[0].passport_no ? obj.result[0].passport_no : '',
                    'patient_passport_expiry': obj.result[0].passport_expiry ? obj.result[0].passport_expiry : '',
                    'patient_organ_donation': obj.result[0].organ_donation ? obj.result[0].organ_donation : '',
                    'co_name': obj.result[0].co_name ? obj.result[0].co_name : '',
                    'co_type': obj.result[0].co_type ? obj.result[0].co_type : '',
                    'co_mobile': obj.result[0].co_mobile ? obj.result[0].co_mobile : '',
                });
            }
            var calculated_age = ageCalculation(obj.result[0].dob);
            if ($('#is_reg_free')) {
                $('#is_reg_free').prop('checked', false);
            }
            try {
                var patient_details = obj.result[0];
                patient_details.patient_id = patient_id;
                patient_details.calculated_age = calculated_age;
                selectedPatientFromAdvanceSearch(patient_details);
            } catch (e) {
            }

            try {
                fillSearchDetialsPharmacy(patient_id, obj.result[0].uhid, obj.result[0].patient_name, obj.result[0].gender, calculated_age, obj.result[0].phone, '', 'op_no');
            } catch (e) {
            }

            fillSearchDetials(obj.result[0].uhid, 'op_no');
            $('#insurance_div_count').val(1);
            $('#progressbar').html('');
            $('#details_tab_body').html('');

        },
        complete: function () {
            $("#patientappoinment_div").LoadingOverlay("hide");
            ageCalculation();
            getAge();
        },
        error: function () {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}


function getPostalCodes(area) {
    var url = base_url + "/patient_register/getPostCodes";
    var postal_code = $("#patient_pincode").val();
    var company_area = $("#company_area").val();
    var blade_id = $('#blade_id').val();
    if (blade_id == '2') {
        if (postal_code) {
            var param = { postal_code: postal_code, company_area: company_area };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#area_text').hide();
                    $('#area_select').show();
                    $('#add_area').attr('checked', false);
                    $('#getPatientLocationBtn').attr('disabled', true);
                    $('#getPatientLocationSpin').removeClass('fa fa-map-marker');
                    $('#getPatientLocationSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.country) {
                        $('#patient_country').val(obj.country).select2();
                        $('#company_state').val(obj.state);
                    }
                    var area_get = $('#patient_default_area').val();
                    if (obj.area.length != 0) {
                        var area_status = true;
                        if (area_get) {
                            area_status = inArray(area_get, obj.area);
                        }

                        if (area_status) {
                            $('#patient_area').html(obj.string);
                            $('#add_area').prop('checked', false);
                            $('#add_patient_area').val('');
                        } else {
                            $('#add_area').prop('checked', true);
                            $('#add_patient_area').val(area_get);
                        }

                    } else {
                        $('#add_area').prop('checked', true);
                        $('#add_patient_area').val(area_get);
                    }
                    patientAreaStatus();
                },
                complete: function () {
                    $('#patient_state').html("<option value=''>Select</option>");
                    $('#patient_district').html("<option value=''>Select</option>");
                    $('#getPatientLocationBtn').attr('disabled', false);
                    $('#getPatientLocationSpin').removeClass('fa fa-spinner fa-spin');
                    $('#getPatientLocationSpin').addClass('fa fa-map-marker');
                    $("#patient_state").select2();
                    $("#patient_district").select2();
                    if (area) {
                        $("#patient_area").val(area);
                    }
                    $("#patient_area").select2();
                    getPatentLocation();
                    getPatientAddress(1);
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Enter Postal Code");
            $("#patient_pincode").focus();
        }
    }
}

function getPatentLocation() {
    var patient_area = $("#patient_area option:selected").text();
    $('#patient_area_not_known').val(patient_area.trim());
}

function getPatientGender() {
    var title_sex = $('#patientsalutation').find(':selected').data('sex');
    var gender = $("#patient_gender option[data-code='" + title_sex + "']").val();
    $("#patient_gender").val(gender).select2();
}

function checkMobileNo() {
    var length = $('#patient_phone').val().length;
    var flag = true;
    if (length != 10) {
        flag = false;
    }
    if (!flag) {
        toastr.warning('Please enter valid mobile number');
    }
    return flag;
}

function isValidIndianMobileNumber(element) {

    var pattern = /^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}$/;
    var mobileNumber = $('#patient_phone').val() ? $('#patient_phone').val() : '';
    var length = mobileNumber.length;
    var valid;
    if (length > 10) {
        valid = pattern.test(mobileNumber);
    } else if (length < 10) {
        valid = false;
    } else if (length == 10) {
        valid = true;
    }
    if (!valid) {
        toastr.warning('Please enter valid mobile number');
    }

    return valid;
}


function searchPatientAlternatePhone() {
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    if (event.keyCode == 13) {
        ajaxlistenter('patient_alternate_phone_AjaxDiv');
        return false;
     } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
        var phone = $('#alternate_phone').val();
        if (!phone) {
            $("#patient_alternate_phone_AjaxDiv").hide();
        } else {
            var visit_date = $('#patient_visit_date').val();
            var doctor_id = $('#patient_doctor_hidden').val();
            var url = base_url + "/master/dynamicBillingReportSearch";
            var param = { _token: token, search_key: phone, visit_date: visit_date, doctor_id: doctor_id, search_key_id: 'alternate_phone', patient_type: 1 };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#patient_alternate_phone_AjaxDiv").html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function (html) {
                    if (html.trim() != "<li>No results found!</li>") {
                        $("#patient_alternate_phone_AjaxDiv").html(html).show();
                        $("#patient_alternate_phone_AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#patient_alternate_phone_AjaxDiv").hide();
                    }

                }
            });
        }

    } else {
        ajax_list_key_down('patient_alternate_phone_AjaxDiv', event);
    }

}

function addCareofType()
{
    bootbox.prompt({
        title: "Enter C/O Type",
        inputType: 'text',
        buttons: {
            confirm: {
                label: "Save",
                className: 'btn-primary',
                callback: function(){
                }
            },
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
                }
            },
        },
        callback: function (result) {
            if(result != "" && result != undefined){
                saveCareOfType(result);
            }
        }
        });
}

function saveCareOfType(result)
{
    var data = {};
    data.type = result;
    $.ajax({
        type: "POST",
        url: base_url + "/master/saveCareOfType",
        data: data,
        beforeSend: function () {
            $('#co_type').html('');
            $('body').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            console.log(data);
            $('#co_type').html(data);
        },
        complete: function () {
            $('body').LoadingOverlay("hide");
        }
    });
}

function mobileNumberNormalValidation(){
    var mobileNumber = $('#patient_phone').val() ? $('#patient_phone').val() : '';
    var length = mobileNumber.length;
    var valid;
    if (length > 5) {
        valid = true;
    } else {
        valid = false;
    }
    return valid;

}
