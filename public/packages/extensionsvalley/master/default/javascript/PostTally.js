$(document).ready(function () {
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
    blockgreater30days();
});
function blockgreater30days(){
    var block_greater30days = $('#block_greater30days').val() ? $('#block_greater30days').val() : 0;
    if(block_greater30days == 1){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var current_date1 = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day;
            var endDate = $('#servieDate').val();
            var current_date = new Date(current_date1);
            var selected_date = new Date(endDate);
            var diffDate = (current_date - selected_date) / (1000 * 60 * 60 * 24);
            var days = Math.round(diffDate);
            console.log(days);
            if(days > 30){
                $(".hideshowbutton").hide();
                $(".hideshowrequest").show();
            }else{
                $(".hideshowbutton").show();
                $(".hideshowrequest").hide();
            }
    }
}
function servieDate(){
    var post_type = $("#post_type").val();
    var invoice_date = $("#servieDate").val();
    var params = {post_type: post_type,invoice_date : invoice_date};
    var url = $("#base_url").val();
    var block_greater30days = $('#block_greater30days').val() ? $('#block_greater30days').val() : 0;
    if(block_greater30days == 1){
        $.ajax({
            type: "POST",
            url: url + "/master/servieDatepick",
            data: params,
            beforeSend: function () {
            },
            success: function (data) {
                if(data.status == 1){
                    $('.requstbtn').html('Req Pending');
                }else{
                    $('.requstbtn').html('');
                }
                console.log(data.status);
                var updated_status = data.updated_status ? data.updated_status : 0;
                console.log(updated_status)

                    var d = new Date();
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    var current_date1 = d.getFullYear() + '/' +
                        ((''+month).length<2 ? '0' : '') + month + '/' +
                        ((''+day).length<2 ? '0' : '') + day;
                        var endDate = $('#servieDate').val();
                        var current_date = new Date(current_date1);
                        var selected_date = new Date(endDate);
                        var diffDate = (current_date - selected_date) / (1000 * 60 * 60 * 24);
                        var days = Math.round(diffDate);
                        console.log(days);
                        if(updated_status == 1){
                            $(".hideshowbutton").hide();
                            $(".hideshowrequest").show();
                        }else{
                            if(days < 30 || data.status == 2 ){
                                $(".hideshowbutton").show();
                                $(".hideshowrequest").hide();
                            }else{
                                $(".hideshowbutton").hide();
                                $(".hideshowrequest").show();
                            }
                        }

            },
            complete: function () {
            }
        });
    }
}
function requesttoedit(){
    $("#remarks").val('');
    $("#getrequstapproveModel").modal({
                backdrop: 'static',
                keyboard: false
            });
}
function saverequesttoedit(){
    var post_type = $("#post_type").val();
    var invoice_date = $("#servieDate").val();
    var remarks = $("#remarks").val();
    var params = {post_type: post_type,invoice_date : invoice_date,remarks :remarks};
    var url = $("#base_url").val();
        if($("#post_type").val() == ''){
            Command: toastr["warning"]("Please Select Post Type");
            return;
        }
        if($("#remarks").val() == ''){
            Command: toastr["warning"]("Please enter Remarks");
            return;
        }
        $.ajax({
            type: "POST",
            url: url + "/master/requesttoedit",
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                console.log(data);
                if(data == 1){
                    toastr.warning('Request Already Pending!!');
                    return true;
                }
                $.LoadingOverlay("hide");
                toastr.success('Requested Succcessfuly !!');
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
}
var voucherArr = [];
var full_voucher = [];
var base_url = $('#base_url').val();
var token = $('#token').val();
$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var base_url=$('#base_url').val();
            var url = base_url + "/master/ajax_ledger_name_search";
            if($("#checkbox").prop('checked') == true){
                var checkbox =1;
            }else{
                var checkbox =0;
            }
            $.ajax({
                type: "POST",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring + '&checkbox=' + checkbox,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajaxListKeyUpDown(input_id + 'AjaxDiv', event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxListKeyEnter(input_id + 'AjaxDiv');
        return false;
    }
});
function ledger_add_btn(department_name){
   $("#dept_head").html(department_name);
    $('#ledger_map_add_modal').modal('show');
    $('#checkbox').prop('checked', true);
}
function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
function ledgersave(){
    var url = $('#base_url').val()+'/master/save_ledger';
    var data = {};


    if($("#ledger_name_hidden").val().trim() == ''){
        Command: toastr["error"]("Please enter Ledger Name");
        return;
    }

    data.ledger_name = encodeURIComponent($("#ledger_name_hidden").val());
    data.ledger_name1 = encodeURIComponent($("#ledger_name").val());
    data.dept_name = htmlDecode($("#dept_head").html());



    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            $("#save_ledger").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
            $('#save_ledger').attr('disabled', true);

        },
        success: function (data) {

                $("#save_ledger").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                $('#save_ledger').attr('disabled', false);
                Command: toastr["success"]("success");
                $('#ledger_map_add_modal').modal('hide');
                processTallyData();
        },
        complete: function () {

        }
    });
}
$("#ledger_map_add_modal").on('hidden.bs.modal', function() {
    $('#ledger_name').val('');
    $('#ledger_nameAjaxDiv').hide();
    $('#checkbox').prop('checked', true);

});

function processTallyData() {
    voucherArr.splice(0, voucherArr.length);
    full_voucher.splice(0, full_voucher.length);
    var post_type = $("#post_type").val();
    var servieDate = $("#servieDate").val();
    if ($(".hideshowbutton").is(":hidden")) {
        return;
    }
    if (post_type) {
        if (servieDate) {
            var url = base_url + "/master/postTallyData";
            var param = { _token: token, post_type: post_type, servieDate: servieDate };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#postdatadiv').html('');
                    $('#processdatabtn').attr('disabled', true);
                    $('#processdataspin').removeClass('fa fa-list');
                    $('#processdataspin').addClass('fa fa-spinner fa-spin');
                    $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                },
                success: function (data) {
                    if (data != 1) {
                        $('#postdatadiv').html(data);
                        setTimeout(function () {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);

                        $('.theadscroll').perfectScrollbar({
                            minScrollbarLength: 30
                        });
                    } else {
                        toastr.warning('Already Posted');
                    }
                },
                complete: function () {
                    $('#processdatabtn').attr('disabled', false);
                    $('#generate_csvbtn').attr('disabled', false);
                    $('#processdataspin').removeClass('fa fa-spinner fa-spin');
                    $('#processdataspin').addClass('fa fa-list');
                    $('#postdatadiv').LoadingOverlay("hide");
                    $('#multimoddataDiv').hide();
                    $('#normaldataDiv').hide();
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
    } else {
        toastr.warning("Please Select Post Type");
        $("#post_type").focus();
    }
}


function verifyData() {
    var servieDate = $("#servieDate").val();
    if (servieDate) {
        var url = base_url + "/master/verifyTallyData";
        var param = { _token: token, servieDate: servieDate };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                $('#postdatadiv').html('');
                $('#verifydatabtn').attr('disabled', true);
                $('#verifydataspin').removeClass('fa fa-check-square-o');
                $('#verifydataspin').addClass('fa fa-spinner fa-spin');
                $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#postdatadiv').html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                $('#verifydatabtn').attr('disabled', false);
                $('#generate_csvbtn').attr('disabled', false);
                $('#verifydataspin').removeClass('fa fa-spinner fa-spin');
                $('#verifydataspin').addClass('fa fa-check-square-o');
                $('#postdatadiv').LoadingOverlay("hide");
                $('#multimoddataDiv').hide();
                $('#normaldataDiv').hide();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Service Date");
        $("#servieDate").focus();
    }
}

function postDataToTally() {
    var post_type = $("#post_type").val();
    var servieDate = $("#servieDate").val();
    if (post_type) {
        if (servieDate) {
            var url = base_url + "/master/postDataToTally";
            var param = { _token: token, post_type: post_type, servieDate: servieDate };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#postDataToTallyBtn').attr('disabled', true);
                    $('#postDataToTallySpin').removeClass('fa fa-list');
                    $('#postDataToTallySpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    var json_data = JSON.parse(data);
                    if (json_data.status == 1) {
                        toastr.success("Successfully Posted")
                    } else {
                        toastr.warning(json_data.message);
                    }
                },
                complete: function () {
                    $('#postDataToTallyBtn').attr('disabled', false);
                    $('#postDataToTallySpin').removeClass('fa fa-spinner fa-spin');
                    $('#postDataToTallySpin').addClass('fa fa-list');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
    } else {
        toastr.warning("Please Select Post Type");
        $("#post_type").focus();
    }
}
function postDataToSoftware() {
    var post_type = $("#post_type").val();
    var servieDate = $("#servieDate").val();
    if (post_type) {
        if (servieDate) {
            var url = base_url + "/master/postDataToSoftware";
            var param = { _token: token, post_type: post_type, servieDate: servieDate,voucherArray:JSON.stringify(voucherArr),fullVoucher:JSON.stringify(full_voucher)  };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('#postDataToSwBtn').attr('disabled', true);
                    $('#postDataToSwSpin').removeClass('fa fa-calculator');
                    $('#postDataToSwSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    if (data == 1) {
                        toastr.success("Successfully Posted");
                        setTimeout(function () {
                            window.location.reload();
                        }, 300);
                    } else {
                        toastr.error("Error Occured");
                    }
                },
                complete: function () {
                    //                    $('#postDataToTallyBtn').attr('disabled', false);
                    //                    $('#postDataToTallySpin').removeClass('fa fa-spinner fa-spin');
                    //                    $('#postDataToTallySpin').addClass('fa fa-list');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Please Select Service Date");
            $("#servieDate").focus();
        }
    } else {
        toastr.warning("Please Select Post Type");
        $("#post_type").focus();
    }
}
function insertTallyData(from_type) {
    var post_type = $("#post_type").val();
    var servieDate = $("#servieDate").val();
    var batch_array = {};
    var param = {};
    if (post_type) {
        $(".posting_cls").each(function () {
            full_voucher.push($(this).data('ledger_name'));
        });
        if (post_type == 6) {
            $(".tds").each(function () {
                var tds_amount = $(this).val();
                var grn = $(this).attr("id");
                if (grn != undefined) {
                    batch_array[grn] = tds_amount;
                }

            });
            param = { _token: token, from_type: from_type, post_type: post_type, servieDate: servieDate, tds_array: JSON.stringify(batch_array),voucherArray:JSON.stringify(voucherArr),fullVoucher:JSON.stringify(full_voucher) };
        } else {
            param = { _token: token, from_type: from_type, post_type: post_type, servieDate: servieDate,voucherArray:JSON.stringify(voucherArr),fullVoucher:JSON.stringify(full_voucher) };
        }
        var url = base_url + "/master/insertTallyData";

        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function () {
                if (from_type == '1') {
                    $('#nextsequencedatabtn').attr('disabled', true);
                    $('#nextsequencedataspin').removeClass('fa fa-list');
                    $('#nextsequencedataspin').addClass('fa fa-spinner fa-spin');
                } else {
                    $('#insertTallyMultiDataBtn').attr('disabled', true);
                    $('#insertTallyMultiDataSpin').removeClass('fa fa-list');
                    $('#insertTallyMultiDataSpin').addClass('fa fa-spinner fa-spin');
                }
                $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('#postdatadiv').html(data);
                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                if (from_type == '1') {
                    $('#nextsequencedatabtn').attr('disabled', false);
                    $('#nextsequencedataspin').removeClass('fa fa-spinner fa-spin');
                    $('#nextsequencedataspin').addClass('fa fa-list');
                } else {
                    $('#insertTallyMultiDataBtn').attr('disabled', false);
                    $('#insertTallyMultiDataSpin').removeClass('fa fa-spinner fa-spin');
                    $('#insertTallyMultiDataSpin').addClass('fa fa-list');
                }
                $('#postdatadiv').LoadingOverlay("hide");
                $('#multimoddataDiv').show();
                $('#normaldataDiv').show();
            },
            error: function () {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please select Post Type");
        $("#post_type").focus();
    }
}
function deleteFromAcPosting(voucher_no,voucher_no_cls) {
    var servieDate = $("#servieDate").val();
        if (voucher_no && servieDate) {
            var url = base_url + "/master/deleteFromAcPosting";
            var param = { _token: token, voucher_no: voucher_no,servieDate:servieDate };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $('.deleteposting').attr('disabled', true);
                    $('#postdatadiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                },
                success: function (data) {
                    if (data == 1) {
                        toastr.success("Successfully Deleted");
                        $('.'+voucher_no_cls).remove();
                    } else {
                        toastr.warning("Error Please Check Your Internet Connection");
                    }
                },
                complete: function () {
                    $('.deleteposting').attr('disabled', false);
                    $('#postdatadiv').LoadingOverlay("hide");
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        } else {
            toastr.warning("Error Please Check Your Internet Connection");
        }

}
function color_row(e){
    if($(e).prop('checked') == true){
    $(e).closest('tr').css('background-color', '#f7cdcd');
    }else{
        $(e).closest('tr').css('background-color', '');
    }
}
function getVoucherNo(e){
    if($(e).prop('checked') == true){
        voucherArr.push($(e).closest('tr').data('ledger_name'));
    }else{
        var value = $(e).closest('tr').data('ledger_name');
        var index = voucherArr.indexOf(value);
        if (index !== -1) {
            voucherArr.splice(index, 1);
        }
    }
}
