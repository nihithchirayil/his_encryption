var url = $('#base_url').val();
var token = $('#c_token').val();
$(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $(".select2").select2({placeholder: "", maximumSelectionSize: 6});

});
function searchList(tab_id) {
    var send_url = url + "/master/fullUserPermissionData";
    var user_id = $("#user_name_hidden").val();
    var user_names = $("#user_names").val();
    if(user_names == ''){
        toastr.warning("Please select name");
        return false;
    }
    var param1 = {user_id: user_id};
    $.ajax({
        type: "GET",
        url: send_url,
        data: param1,
        beforeSend: function () {
            $('#searchdatabtn').attr('disabled', true);
            $('#searchdataspin').removeClass('fa fa-search');
            $('#searchdataspin').addClass('fa fa-spinner');
            $('#common_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (msg) {
            if (msg != 2) {
                $('#common_list_div').html(msg);
            } else if(msg == '3'){
                toastr.error("Please check user groups");
            }
            else {
                toastr.error("Please Check Internet Connection");
            }
        },
        complete: function () {
            $("#" + tab_id).trigger('click');
            $('#searchdatabtn').attr('disabled', false);
            $('#searchdataspin').removeClass('fa fa-spinner');
            $('#searchdataspin').addClass('fa fa-search');
            $('#common_list_div').LoadingOverlay("hide");


            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30,
                suppressScrollX: true
            });
            $('.theadfix_wrapper').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');


        }, error: function () {
            toastr.error("Please Check Internet Connection");
        }
    });
}
$('.hidden_search').keyup(function (event) {
    var send_url = url + "/master/fullUserPermission";
    var keycheck = 1; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char

    if (keycheck || event.keyCode == '8') {
        if ($('#user_name_hidden').val() != "") {
            $('#user_name_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        if (search_key == "") {
            $("#AjaxDiv").html("");
        } else {
            $.ajax({
                type: "GET",
                url: send_url,
                data: 'user_search=' + search_key,
                beforeSend: function () {

                    $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#AjaxDiv").html("No results found!").show();

                        $("#AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#AjaxDiv").html(html).show();
                        $("#AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down('AjaxDiv', event);
    }
});

function fill_user_details(e, name, id,group_tab=0) {
    $('#user_name_hidden').val(id);
    $('#user_names').val(name);
    if(group_tab==5){
        searchList('tab_id5')
    }else{
        searchList('tab_id10')
    }

    $("#AjaxDiv").hide();
}

function alterLocation(group_id) {
    $(".cur_location_names").css('background-color','#fff');
    var send_url = url + "/master/fullUserLocationAccessList";
    $.ajax({
        type: "GET",
        url: send_url,
        data: 'group_id=' + group_id,
        beforeSend: function () {
            $('#tabs').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'})
        },
        success: function (html) {
            if (html) {
                $("#assign_loc").html(html);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                $('#tabs').LoadingOverlay("hide");
                $("#role_id").val(group_id);
                $("#group_id_loc").val(group_id);
                $("#group_id_bill").val(group_id);
            } else {

            }
            $("#loc_"+group_id).css('background-color','#abc8db');
        },

        complete: function () {
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        },

    });
}
function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
$(document).on('click', '.ass_ug_chk', function () {
    var status = ($(this).prop('checked')) ? 1 : 0;
    var is_ug = 'g';
    var ug_id = $(this).closest("tr").find('.ug_id').val().trim();
    var table_ug_id = $(this).closest("tr").find('.hidden_ass_ug_id').val().trim();
    var ur_gp_id = '';
    ur_gp_id = $('#group_id_loc').val().trim();
    var iD_input = $(this).closest("tr").find('.hidden_ass_ug_id').attr('id');
    var url = $('#base_url').val();
    var send_url = url + "/master/list_location_group";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "id=" + ug_id + "&is_ug=" + is_ug + " &ur_gp_id=" + ur_gp_id + "&ug_id=" + ug_id + "&status=" + status + "&ass_val=g &table_ug_id=" + table_ug_id,
        beforeSend: function () { },
        success: function (data) {
            toastr.success(data.message);
            $("#" + iD_input).val(data.table_ug_id);
        },
        complete: function () { }
    });
});
$(document).on('click', '.ass_ug_chk_bill', function () {
    var status = ($(this).prop('checked')) ? 1 : 0;
    var is_ug = 'g';
    var ug_id = $(this).closest("tr").find('.ug_id_bill').val().trim();
    var table_ug_id = $(this).closest("tr").find('.hidden_ass_ug_id_bill').val().trim();
    var ur_gp_id = '';
    ur_gp_id = $('#group_id_bill').val().trim();
    var iD_input = $(this).closest("tr").find('.hidden_ass_ug_id_bill').attr('id');
    var url = $('#base_url').val();
    var send_url = url + "/admin/bill_tag_group";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "id=" + ug_id + "&is_ug=" + is_ug + " &ur_gp_id=" + ur_gp_id + "&ug_id=" + ug_id + "&status=" + status + "&ass_val=b &table_ug_id=" + table_ug_id,
        beforeSend: function () { },
        success: function (data) {
            toastr.success(data.message);
            $("#" + iD_input).val(data.table_ug_id);
        },
        complete: function () { }
    });
});
function expandCollapse(e, group_id) {
    var disp = $(e).attr('data-display');
    var sz = $(".groups_" + group_id).length;
    if (sz == 0) {
        toastr.warning("No menu to show");
    }
    if (disp == 2) {
        $(".groups_" + group_id).show();
        $(e).attr('data-display', 1);
    } else {
        $(".groups_" + group_id).hide();
        $(e).attr('data-display', 2);
    }
}
function expandCollapseLoc(e, group_id) {
    var disp = $(e).attr('data-display');
    var sz = $(".loc_groups_" + group_id).length;
    if (sz == 0) {
        toastr.warning("No location to show");
    }
    if (disp == 2) {
        $(".loc_groups_" + group_id).show();
        $(e).attr('data-display', 1);
    } else {
        $(".loc_groups_" + group_id).hide();
        $(e).attr('data-display', 2);
    }
}
function expandCollapseBill(e, group_id) {
    var disp = $(e).attr('data-display');
    var sz = $(".bill_tag_groups_" + group_id).length;
    if (sz == 0) {
        toastr.warning("No bill tag to show");
    }
    if (disp == 2) {
        $(".bill_tag_groups_" + group_id).show();
        $(e).attr('data-display', 1);
    } else {
        $(".bill_tag_groups_" + group_id).hide();
        $(e).attr('data-display', 2);
    }
}
function editLoadData(obj, id, acl_key, group_id, worksheet, submit, verify, approve, status) {
    $('#menu_access_id').val(id);
    $('#acl_key').val(acl_key).select2().trigger('change');
    $('#group_id').val(group_id).select2().trigger('change');
    $('#status').val(status);
    if (worksheet == 1) {
        $("#worksheet").prop("checked", true);
    } else {
        $("#worksheet").prop("checked", false);
    }
    if (submit == 1) {
        $("#submit").prop("checked", true);
    } else {
        $("#submit").prop("checked", false);
    }
    if (verify == 1) {
        $("#verify").prop("checked", true);
    } else {
        $("#verify").prop("checked", false);
    }
    if (approve == 1) {
        $("#approve").prop("checked", true);
    } else {
        $("#approve").prop("checked", false);
    }
}
function saveWorkflow() {
    var menu_access_id = $('#menu_access_id').val();
    var acl_key = $('#acl_key').val();
    var group_id = $('#group_id').val();
    var status = $('#status').val();
    var worksheet = 0;
    var submit = 0;
    var verify = 0;
    var approve = 0;
    if ($("#worksheet").prop("checked") == true) {
        worksheet = 1;
    }
    if ($("#approve").prop("checked") == true) {
        approve = 1;
    }
    if ($("#verify").prop("checked") == true) {
        verify = 1;
    }
    if ($("#submit").prop("checked") == true) {
        submit = 1;
    }
    var param = {menu_access_id: menu_access_id, acl_key: acl_key, group_id: group_id, status: status, worksheet: worksheet, submit: submit, verify: verify, approve: approve};
    if (acl_key == '') {
        toastr.warning("Please select menu");
        return;
    } else if (group_id == '') {
        toastr.warning("Please select group");
        return;
    }
    var url = $('#base_url').val();
    var send_url = url + "/master/fullWorkFlowAccessSave";
    $.ajax({
        type: "GET",
        url: send_url,
        data: param,
        beforeSend: function () {
            $('#tabs').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'})
        },
        success: function (data) {
            if (data == 1) {
                toastr.success("Permission updated");
                $(".work_flow_entry").val('');
                $(".work_flow_entry").prop('checked', false);
                $("#role_id").val(group_id);
                $("#group_id_loc").val(group_id);
                $("#group_id_bill").val(group_id);
            } else {
                toastr.warning("Error occured");
            }
        },
        complete: function () {
            //searchList('tab_id2');
            alterWorkFlowTag(group_id);
            $('#tabs').LoadingOverlay("hide");
        }
    });
}

function alterBillTag(group_id) {
    $(".asn_bill_tag_row").css('background-color','#fff');
    var send_url = url + "/master/fullUserBillAccessList";
    $.ajax({
        type: "GET",
        url: send_url,
        data: 'group_id=' + group_id,
        beforeSend: function () {
            $('#tabs').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'})
        },
        success: function (html) {
            if (html) {
                $("#assign_bill_tag").html(html);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                $('#tabs').LoadingOverlay("hide");
                $("#role_id").val(group_id);
                $("#group_id_loc").val(group_id);
                $("#group_id_bill").val(group_id);
            } else {

            }
            $("#bill_tag_tow_"+group_id).css('background-color','#abc8db');
        },

        complete: function () {
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        },

    });
}
function changeAccess(group_id, tab_id) {
    $("#role_id").val(group_id);
    $("#group_id_loc").val(group_id);
    $("#group_id_bill").val(group_id);
    if (tab_id == 'menu') {
        $("#tab_id1").trigger('click');
        searchListByGroup('tab_id1', group_id);
        ;
    } else if (tab_id == 'loc') {
        $("#tab_id3").trigger('click');
        searchListByGroup('tab_id3', group_id);
        ;
    } else if (tab_id == 'bill_tag') {
        $("#tab_id4").trigger('click');
        searchListByGroup('tab_id4', group_id);
        ;
    } else if (tab_id == 'work_flow') {
        $("#tab_id2").trigger('click');
        searchListByGroup('tab_id2', group_id);
    }else if (tab_id == 'user_access') {
        $("#tab_id5").trigger('click');
        alterUserAccess(group_id);
    }

}
function searchListByGroup(tab_id, group_id) {
    var user_id = $("#user_name_hidden").val();
    var send_url = url + "/master/fullUserPermissionDataGroup";
    var user_id = $("#user_name_hidden").val();
                    if (tab_id == 'tab_id3') {
                    alterLocation(group_id);
                } else if (tab_id == 'tab_id1') {
                    alterMenuAccess(group_id);
                }else if (tab_id == 'tab_id4') {
                    alterBillTag(group_id);
                }else if (tab_id == 'tab_id2') {
                    alterWorkFlowTag(group_id);
                }
//    var params1 = {user_id: user_id, group_id: group_id};
//    $.ajax({
//        type: "GET",
//        url: send_url,
//        data: params1,
//        beforeSend: function () {
//            $('#searchdatabtn').attr('disabled', true);
//            $('#searchdataspin').removeClass('fa fa-search');
//            $('#searchdataspin').addClass('fa fa-spinner');
//           $('#tabs').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'})
//        },
//        success: function (msg) {
//            if (msg != 2) {
//                //$('#common_list_div').html(msg);
//                if (tab_id == 'tab_id3') {
//                    alterLocation(group_id);
//                } else if (tab_id == 'tab_id1') {
//                    alterMenuAccess(group_id);
//                }else if (tab_id == 'tab_id4') {
//                    alterBillTag(group_id);
//                }else if (tab_id == 'tab_id2') {
//                    alterWorkFlowTag(group_id);
//                }
//            } else {
//                toastr.error("Please Check Internet Connection");
//            }
//        },
//        complete: function () {
//            $("#" + tab_id).trigger('click');
//            $('#searchdatabtn').attr('disabled', false);
//            $('#searchdataspin').removeClass('fa fa-spinner');
//            $('#searchdataspin').addClass('fa fa-search');
//            $('#tabs').LoadingOverlay("hide");
//
//
//            $('.theadscroll').perfectScrollbar({
//                minScrollbarLength: 30,
//                suppressScrollX: true
//            });
//            $('.theadfix_wrapper').floatThead({
//                position: 'absolute',
//                scrollContainer: true
//            });
//
//            $('.theadscroll').perfectScrollbar("update");
//            $(".theadfix_wrapper").floatThead('reflow');
//
//
//        }, error: function () {
//            toastr.error("Please Check Internet Connection");
//        }
//    });
}
function tab1_click(){
    var role_id = $("#role_id").val();
    var group_id_loc = $("#group_id_loc").val();
    var group_id_bill = $("#group_id_bill").val();
    if(role_id != '' && role_id != undefined){
        alterMenuAccess(role_id);
    }
    if(group_id_loc != '' && group_id_loc != undefined ){
        alterMenuAccess(group_id_loc);
    }
    if(group_id_bill != '' && group_id_bill != undefined ){
        alterMenuAccess(group_id_bill);
    }

}
function tab3_click(){
 var role_id = $("#role_id").val();
    var group_id_loc = $("#group_id_loc").val();
    var group_id_bill = $("#group_id_bill").val();
    if(role_id != '' && role_id != undefined){
        alterLocation(role_id);
    }
    if(group_id_loc != '' && group_id_loc != undefined){
        alterLocation(group_id_loc);
    }
    if(group_id_bill != '' && group_id_bill != undefined){
        alterLocation(group_id_bill);
    }
}
function tab4_click(){
 var role_id = $("#role_id").val();
    var group_id_loc = $("#group_id_loc").val();
    var group_id_bill = $("#group_id_bill").val();
    if(role_id != '' && role_id != undefined){
        alterBillTag(role_id);
    }
    if(group_id_loc != '' && group_id_loc != undefined){
        alterBillTag(group_id_loc);
    }
    if(group_id_bill != '' && group_id_bill != undefined){
        alterBillTag(group_id_bill);
    }
}
function tab2_click(){
    $(".select2").select2({placeholder: "", maximumSelectionSize: 6});
    var role_id = $("#role_id").val();
    var group_id_loc = $("#group_id_loc").val();
    var group_id_bill = $("#group_id_bill").val();
    if(role_id != '' && role_id != undefined){
        alterWorkFlowTag(role_id);
    }
    if(group_id_loc != '' && group_id_loc != undefined){
        alterWorkFlowTag(group_id_loc);
    }
    if(group_id_bill != '' && group_id_bill != undefined){
        alterWorkFlowTag(group_id_bill);
    };
}
function alterWorkFlowTag(group_id) {
    $(".work_flow_row").css('background-color','#fff');
    var send_url = url + "/master/fullUserWorkFlowAccessList";
    $.ajax({
        type: "GET",
        url: send_url,
        data: 'group_id=' + group_id,
        beforeSend: function () {
            $('#tabs').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'})
        },
        success: function (html) {
            if (html) {
                $("#assign_work_flow_tag").html(html);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                $('#tabs').LoadingOverlay("hide");
                $("#role_id").val(group_id);
                $("#group_id_loc").val(group_id);
                $("#group_id_bill").val(group_id);
            } else {

            }
            $("#work_flow_id_"+group_id).css('background-color','#abc8db');
            $(".select2").select2({placeholder: "", maximumSelectionSize: 6});
        },

        complete: function () {
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        },

    });
}
function tab5_click(){
alterUserAccess();
}
function alterUserAccess() {
    var user_name_hidden = $("#user_name_hidden").val();
    var send_url = url + "/master/alterUserAccess";
    $.ajax({
        type: "GET",
        url: send_url,
        data: 'user_name_hidden=' + user_name_hidden,
        beforeSend: function () {
            $('#tabs').LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'})
        },
        success: function (html) {
            if (html) {
                $("#assign_user_groups").html(html);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                $('#tabs').LoadingOverlay("hide");
            } else {

            }

        },

        complete: function () {
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            return;
        },

    });
}
$(document).on('keyup change', '.ass_ug_chk_user', function () {
    var url = $('#base_url').val();
    var send_url = url + "/admin/list_user_group";

    var status = ($(this).prop('checked')) ? 1 : 0;
    var ug_id_user = $(this).closest("tr").find('.ug_id_user').val().trim();
    var ass_val_user = $(this).closest("tr").find('.ass_val_user').val().trim();
    var table_ug_id_user = $(this).closest("tr").find('.hidden_ass_ug_id_user').val().trim();
    var ur_gp_id_user = $('#user_name_hidden').val().trim();
    var iD_input = $(this).closest("tr").find('.hidden_ass_ug_id_user').attr('id');
    var url = "";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "id="+ur_gp_id_user+"&is_ug=u&ur_gp_id=" + ur_gp_id_user + "&ug_id=" + ug_id_user + "&status=" + status + "&ass_val=" + ass_val_user + "&table_ug_id=" + table_ug_id_user,
        beforeSend: function () { },
        success: function (data) {
            toastr.success(data.message);
            $("#" + iD_input).val(data.table_ug_id_user);
        },
        complete: function () { }
    });
});
function addNewUser(e){

    var url = $('#base_url').val();
    var send_url = url + "/admin/listUsers";
    $.ajax({
        type: "GET",
        url: send_url,
        data: {data:1},
        beforeSend: function () {
             $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            $('#newuserbtn').prop('disabled',true);
        },
        success: function (data) {
            if (data) {
                var html_content = $(data).find('#user_add_form_wraper');
                $("#append_user_master").html(html_content);
                $("#saveUserFormBtn").attr("onclick","userOverwrite()");
                $("#user_add_form_wraper").removeClass('col-md-4 padding_sm');
                $("#search_user_modal").modal('show');
                $.LoadingOverlay("hide");
            }
        },
        complete: function () {
            $('#newuserbtn').prop('disabled',false);
        }
    });
}
function userOverwrite(){
        var user_id = $('#user_id').val();
        var group = $('#group_id').val();
        var name = $('#name').val();
        var email = $('#email').val();
        var user_name = $('#user_name').val();
        var password = $('#password').val();
        var password_confirmation = $('#password_confirmation').val();
        var is_cashier = $('#is_cashier').is(":checked");
        var is_nurse = $('#is_nurse').is(":checked");
        var default_location = $('#default_location').val();
        var default_station_id = $('#default_station_id').val();
        var default_store = $('#default_store').val();
        var status = $('#status').val();
        var default_landing_page = $('#default_landing_page').val();
        if (status) {
            if (name) {
                if (email) {
                    if (user_name) {
                        if (password || user_id != '0') {
                            if (password_confirmation || user_id != '0') {
                                var url = base_url + "/admin/saveUserData";
                                var param = {
                                    _token: token, user_id: user_id, name: name, email: email, user_name: user_name, password: password,
                                    is_cashier: is_cashier, is_nurse: is_nurse, default_location: default_location, group: group,
                                    default_station_id: default_station_id, default_store: default_store, status: status,default_landing_page:default_landing_page
                                };
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: param,
                                    beforeSend: function () {
                                        $('#saveUserFormBtn').attr('disabled', true);
                                        $('#saveUserFormSpin').removeClass('fa fa-save');
                                        $('#saveUserFormSpin').addClass('fa fa-spinner');
                                    },
                                    success: function (data) {
                                        if (data) {
                                            fill_user_details(this,name,data,5);
                                            $("#search_user_modal").modal('hide');
                                            toastr.success("User Data Successfully Updated");
                                        } else {
                                            toastr.error("Please Check Internet Connection");
                                        }
                                    },
                                    complete: function () {
                                        $('#saveUserFormBtn').attr('disabled', false);
                                        $('#saveUserFormSpin').removeClass('fa fa-spinner');
                                        $('#saveUserFormSpin').addClass('fa fa-save');
                                    }, error: function () {
                                        toastr.error("Please Check Internet Connection");
                                    }
                                });
                            } else {
                                toastr.warning("Please Enter Confirmation Password");
                                $('#password_confirmation').focus();
                            }
                        } else {
                            toastr.warning("Please Enter Password");
                            $('#password').focus();
                        }
                    } else {
                        toastr.warning("Please Enter User Name");
                        $('#user_name').focus();
                    }
                } else {
                    toastr.warning("Please Enter Email");
                    $('#email').focus();
                }
            } else {
                toastr.warning("Please Enter Name");
                $('#name').focus();
            }
        } else {
            toastr.warning("Please Select Status");
        }
    }
    function saveGroupName(){
        var url = $('#base_url').val();
        var send_url = url + "/master/saveUserGroupSingleScreen";
        var grp_name = $('#grp_name').val();
        var status_grp = $('#status_grp').val();
        if(grp_name){
        $.ajax({
            type: "GET",
            url: send_url,
            data: {grp_name:grp_name,status_grp:status_grp},
            beforeSend: function () {
                 $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                if (data) {
                    if(data == 3){
                        $("#grpModal").modal('hide');
                        $.LoadingOverlay("hide");
                        toastr.success("Group successfully added");
                        window.location.reload();
                    }else{
                        $.LoadingOverlay("hide");
                        toastr.warning("Group name exist");
                    }
                }
            },
            complete: function () {
            }
        });
    }else{
        toastr.warning("Please enter name");
    }
    }
    function searchitem(issue_search_box,item_search_btn_text,item_search_btn_bill){
        $("#"+issue_search_box).toggle();
        $("#"+item_search_btn_text).toggle();
        $("#"+issue_search_box).focus();
        }
        function searchbyName(id,table_id) {
        var input, filter, table, tr, td, i;
        input = document.getElementById(id);
        filter = input.value.toUpperCase();
        table = document.getElementById(table_id);
        tr = table.getElementsByTagName("tr");
        for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
