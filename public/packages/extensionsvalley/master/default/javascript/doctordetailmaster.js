$(function() {
    $("#progressbar li:first-child a").tab("show");
});
$("#progressbar li").click(function(e) {
    $(this)
        .find("a")
        .tab("show");
});
$(document).ready(function() {
    $("#company")
        .val(120)
        .select2();
    var com_id = $("#company").val();
    getcompanypricing(com_id);

    $(".datepicker").datetimepicker({
        format: "MMM-DD-YYYY"
    });
    $(".timepicker").datetimepicker({
        format: "h:mm A"
    });
    $(".select2").select2();
});
$(document).on("click", function(event) {
    var $trigger = $(".ajaxSearchBox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".ajaxSearchBox").hide();
    }
});

//----Hidden Filed Search--------------------------------------------------------------------
var token = $("#c_token").val();
var base_url = $("#base_url").val();
$(".hidden_search").keyup(function(event) {
    var input_id = "";
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr("id");
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == "8") {
        if ($("#" + input_id + "_hidden").val() != "") {
            $("#" + input_id + "_hidden").val("");
        }
        var search_key = $(this).val();
        search_key = search_key.replace("/[^ws-_.]/gi", "");
        search_key = search_key.trim();

        var department_hidden = $("#department_hidden").val();
        var datastring = "";
        if (input_id == "sub_department") {
            datastring = "&department_id=" + department_hidden;
        }
        if (input_id == "scheme") {
            datastring = "&company_id=" + company_hidden;
        }

        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = base_url + "/master/setAjaxSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: "&search_key=" +
                    search_key +
                    "&search_key_id=" +
                    input_id +
                    datastring,
                beforeSend: function() {
                    $("#" + input_id + "AjaxDiv")
                        .html(
                            '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        )
                        .show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv")
                            .html("No results found!")
                            .show();
                        $("#" + input_id + "AjaxDiv")
                            .find("li")
                            .first()
                            .addClass("liHover");
                    } else {
                        $("#" + input_id + "AjaxDiv")
                            .html(html)
                            .show();
                        $("#" + input_id + "AjaxDiv")
                            .find("li")
                            .first()
                            .addClass("liHover");
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }
            });
        }
    } else {
        ajax_list_key_down(input_id + "AjaxDiv", event);
    }
});

function fillSearchDetials(id, name, serach_key_id) {
    $("#" + serach_key_id + "_hidden").val(id);
    $("#" + serach_key_id).val(name);
    $("#" + serach_key_id).attr("title", name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on("keydown", function(event) {
    var input_id = "";
    input_id = $(this).attr("id");
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + "AjaxDiv");
        return false;
    }
});

function getShiftDetails(doc_id) {
    var url = base_url + "/master/getShiftDetails";
    var doc_name = $("#doctor_name" + doc_id).html();
    $("#doctor").val(doc_name);
    $("#doctor_hidden").val(doc_id);
    $(".ser")
        .val("")
        .select2();

    $.ajax({
        type: "POST",
        url: url,
        data: { doc_id: doc_id },
        beforeSend: function() {
            autuSetDoctorDetails();
            $("#shif_filters").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
            $("#shift").empty();
        },
        success: function(data) {
            if (data) {
                $("#shift").html('<option value="">Select Shift</option>');
                $.each(data, function(key, value) {
                    $("#shift").append(
                        '<option value="' +
                        value.id +
                        '">Start From:' +
                        value.fromtime +
                        "-" +
                        value.totime +
                        "</option>"
                    );
                });
            }
        },
        complete: function() {
            $("#shif_filters").LoadingOverlay("hide");
            getPricingListing();
            pricingDetails();
            $('#doc_leave_container').html('')
            $('#blocked_lists').html('')

        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
$("#shift").change(function() {
    var url = base_url + "/master/getShiftDateAndTime";
    var shift_id = $("#shift").val();

    $.ajax({
        type: "POST",
        url: url,
        data: { shift_id: shift_id },
        beforeSend: function() {
            $("#time_selection").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
            $("#shift_frmtime").empty();
            $("#shift_totime").empty();
        },
        success: function(data) {
            if (data) {
                $("#shift_frmtime").val(data[0].fromtime);
                $("#shift_totime").val(data[0].totime);
            }
        },
        complete: function() {
            $("#time_selection").LoadingOverlay("hide");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
});

function saveDocLeaveOrBlock() {
    var doc = $("#doctor_hidden").val();
    if (!doc) {
        toastr.warning("Please select a Doctor.");
        return;
    }
    var leave = $("#leave").is(":checked");
    var block = $("#block").is(":checked");
    var url = base_url + "/master/saveDocLeaveOrBlock";
    var desc = $("#desc")
        .val()
        .trim();
    if (!desc) {
        toastr.warning("Please fill Description.");
        return;
    }
    var all_shift = $('#all_shift').is(':checked') ? 1 : 0;
    var shift = $("#shift").val();
    if (all_shift != 1) {
        var shift_frmtime = $("#shift_frmtime").val();
        if (!shift_frmtime) {
            toastr.warning("Please fill start time.");
            return;
        }
        var shift_totime = $("#shift_totime").val();

        if (!shift_totime) {
            toastr.warning("Please fill End time.");
            return;
        }
    }
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    date1 = new Date(from_date);
    date2 = new Date(to_date);
    if (date1 > date2) {
        toastr.warning("To date must be greater than from date.");
        return;
    }
    $.ajax({
        type: "POST",
        url: url,
        data: {
            shift: shift,
            all_shift: all_shift,
            to_date: to_date,
            from_date: from_date,
            doc: doc,
            leave: leave,
            block: block,
            desc: desc,
            shift_frmtime: shift_frmtime,
            shift_totime: shift_totime
        },
        beforeSend: function() {
            $(".ajaxSearchBox").hide();
            $("#shift_save").attr("disabled", true);
            $("#shift_spin").removeClass("fa fa-save");
            $("#shift_spin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {
            if (data) {
                if (data.status == 101) {
                    toastr.success("" + data.message + "");
                    reset();
                } else {
                    toastr.warning("Execution failed.");
                }
            }
        },
        complete: function() {
            $("#shift_save").attr("disabled", false);
            $("#shift_spin").removeClass("fa fa-spinner fa-spin");
            $("#shift_spin").addClass("fa fa-save");
            viewSlotAndAppointment();
            viewLeaveAndBlockDetails();
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function viewSlotAndAppointment() {
    var doc = $("#doctor_hidden").val();
    if (!doc) {
        toastr.warning("Please select a Doctor.");
        return;
    }
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var url = base_url + "/master/viewSlotAndAppointment";
    $.ajax({
        type: "POST",
        url: url,
        data: { doc: doc, from_date: from_date, to_date: to_date },
        beforeSend: function() {
            $("#search_app").attr("disabled", true);
            $("#listing").hide();
            //$('#doc_leave_container').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });;
            $("#app_spin").removeClass("fa fa-search");
            $("#app_spin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {
            $("#doc_leave_container").html(data);
            $("#doc_leave_container").show();
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $("table.theadfix_wrapper");
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest(".theadscroll");
                }
            });
        },
        complete: function() {
            $("#search_app").attr("disabled", false);
            $("#doc_leave_container").LoadingOverlay("hide");
            $("#app_spin").removeClass("fa fa-spinner fa-spin");
            $("#app_spin").addClass("fa fa-search");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function viewLeaveAndBlockDetails() {
    var doc = $("#doctor_hidden").val();
    if (!doc) {
        toastr.warning("Please select a Doctor.");
        return;
    }
    var from_date = $("#app_from_date").val();
    var to_date = $("#app_to_date").val();
    var url = base_url + "/master/viewLeaveAndBlockDetails";
    $.ajax({
        type: "POST",
        url: url,
        data: { doc: doc, from_date: from_date, to_date: to_date },
        beforeSend: function() {
            $("#blocking").hide();
            // $('#blocked_lists').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });;
            $("#detail_search").attr("disabled", true);
            $("#detail_spin").removeClass("fa fa-search");
            $("#detail_spin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {
            $("#blocked_lists").html(data);
            $(".theadscroll").perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $("table.theadfix_wrapper");
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest(".theadscroll");
                }
            });
        },
        complete: function() {
            $("#blocked_lists").LoadingOverlay("hide");
            $("#detail_search").attr("disabled", false);
            $("#detail_spin").removeClass("fa fa-spinner fa-spin");
            $("#detail_spin").addClass("fa fa-search");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function reset() {
    $("#desc").val(" ");
    $("#shift_frmtime").val(" ");
    $("#shift_totime").val(" ");
    var current_date = $("#current_date").val();
    $("#from_date").val(current_date);
    $("#to_date").val(current_date);
    $("#shift")
        .val("")
        .select2();
    $("#doc_leave_container").html(" ");
    $("#doc_leave_container").html(
        '<h2 id="listing">Slots And Appointment List</h2>'
    );
    $("#blocked_lists").html(" ");
    $("#doc_leave_container").html(
        '<h2 id="blocking">Leave and Block List</h2>'
    );
}

function autuSetDoctorDetails() {
    var doc = $("#doctor").val();
    var doc_hidden = $("#doctor_hidden").val();
    $("#doctorName").val(doc);
    $("#doctorName_hidden").val(doc_hidden);
    doctorSearchList(doc_hidden);
}
$("#company").change(function() {
    var com_id = $("#company").val();
    getcompanypricing(com_id);
});

function getcompanypricing(com_id) {
    var url = base_url + "/master/getcompanypricing";

    $.ajax({
        type: "POST",
        url: url,
        data: { com_id: com_id },
        beforeSend: function() {

            $("#pricing_box").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
            $("#pricing").empty();
        },
        success: function(data) {
            if (data) {
                $("#pricing").html('<option value="">Select Pricing</option>');
                $.each(data, function(key, value) {
                    $("#pricing").append(
                        "<option value=" +
                        value.id +
                        "> " +
                        value.pricing_name +
                        "</option>"
                    );
                });
            }
        },
        complete: function() {
            $("#pricing_box").LoadingOverlay("hide");
            if ($("#company").val() == 120) {
                $("#pricing")
                    .val(67)
                    .select2();
            }
            if ($('#doctor_hidden').val()) {
                pricingDetails();
            }
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function id_function(data) {
    $("#" + data.input_id).val(data.amount);
}
$("#pricing").change(function() {
    pricingDetails();
});

function pricingDetails() {
    $(".inp_reset").val("");
    var doc = $("#doctor_hidden").val();
    if (!doc) {
        toastr.warning("Please select a Doctor.");
        $("#pricing")
            .val("")
            .select2();
        return;
    }
    var company_id = $("#company").val();
    var pricing_id = $("#pricing").val();
    if (!company_id) {
        toastr.warning("Please select a Company.");
        return;
    }
    if (!pricing_id) {
        toastr.warning("Please select a Pricing.");
        return;
    }
    var url = base_url + "/master/getpricingdetails";

    $.ajax({
        type: "POST",
        url: url,
        data: { doc: doc, company_id: company_id, pricing_id: pricing_id },
        beforeSend: function() {
            $("#set_pricing_box").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
            $("#int_reset").val("");
        },
        success: function(data) {
            if (data) {
                console.log(data);
                if (data.length) {
                    data.forEach(id_function);
                }
            }
        },
        complete: function() {
            $("#set_pricing_box").LoadingOverlay("hide");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function savePricing() {
    var insert = [];
    var check = [];
    var status = 0;
    var doc = $("#doctor_hidden").val();
    var company_id = $("#company").val();
    var pricing_id = $("#pricing").val();
    if ($("#checkbox").is(":checked")) {
        status = 1;
    }
    if (!doc) {
        toastr.warning("Please select a Doctor.");
        return;
    }
    if (!company_id) {
        toastr.warning("Please select a Company.");
        return;
    }
    if (!pricing_id) {
        toastr.warning("Please select a Pricing.");
        return;
    }
    $(".inp_reset").each(function() {
        amount = $(this).val() ? $(this).val() : 0;
        type = $(this).attr("data-service_type");
        code = $(this).attr("data-service_code");
        insert.push({
            status: status,
            pricing_id: pricing_id,
            company_id: company_id,
            doctor_id: doc,
            service_type: type,
            amount: amount,
            service_code: code
        });

        if (amount != 0) {
            check.push({ amount });
        }
    });
    if (check.length == 0) {
        toastr.warning("Nothing to save.");
        return;
    }

    var url = base_url + "/master/savePricing";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            insert: insert,
            company_id: company_id,
            pricing_id: pricing_id,
            doctor_id: doc
        },
        beforeSend: function() {
            $("#save_pricing").attr("disabled", true);
            $("#pricing_spin").removeClass("fa fa-save");
            $("#pricing_spin").addClass("fa fa-spinner fa-spin");
        },
        success: function(data) {
            if (data) {
                toastr.success("Pricing added successfully.");
                $("#cmp")
                    .val(company_id)
                    .select2();
                $("#prc")
                    .val(pricing_id)
                    .select2();
                $(".inp_reset").val("");
                $("#company")
                    .val(120)
                    .select2();
                $("#pricing")
                    .val('')
                    .select2();


            }
        },
        complete: function() {
            getPricingListing();

            $("#save_pricing").attr("disabled", false);
            $("#pricing_spin").removeClass("fa fa-spinner fa-spin");
            $("#pricing_spin").addClass("fa fa-save");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function getPricingListing() {
    var doc = $("#doctor_hidden").val();

    var service = $("#ser").val();
    var price = $("#prc").val();
    var company = $("#cmp").val();
    var type = $("#typ").val();
    var url = base_url + "/master/pricinglisting";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            doc: doc,
            service: service,
            price: price,
            company: company,
            type: type
        },
        beforeSend: function() {
            $("#pricing_listing_data").html(" ");
            $("#pricing_search").attr("disabled", true);
            $("#pricing_search_spin").removeClass("fa fa-search");
            $("#pricing_search_spin").addClass("fa fa-spinner fa-spin");
            $("#pricing_listing_data").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: "#009869"
            });
        },
        success: function(data) {
            if (data) {
                $("#pricing_listing_data").html(data);
            }
        },
        complete: function() {
            $("#pricing_listing_data").LoadingOverlay("hide");
            $("#pricing_search").attr("disabled", false);
            $("#pricing_search_spin").removeClass("fa fa-spinner fa-spin");
            $("#pricing_search_spin").addClass("fa fa-search");
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}
$(".ser").change(function() {
    getPricingListing();
});
$("#pricing_search").click(function() {
    getPricingListing();
});

function redirectToAppoinmentlist() {
    var doc = $("#doctor_hidden").val();
    if (doc) {
        var base_url = $("#base_url").val();
        var url = base_url + "/master/appointmentlisting/" + doc;
        window.open(url, "_newtab");
    } else {
        toastr.warning("Please select a Doctor.");
        return;
    }
}

function deletePricing(pricing_id) {
    bootbox.confirm({
        message: "Are You Sure You want to delete this pricing ?",
        buttons: {
            confirm: {
                label: "Delete",
                className: "btn-warning",
                default: "true",
            },
            cancel: {
                label: "Cancel",
                className: "btn-danger",
            },
        },
        callback: function(result) {
            if (result) {
                var url = base_url + "/master/deletePricing";;
                var param = { pricing_id: pricing_id };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#delete_pricing_btn' + pricing_id).attr('disabled', true);
                        $('#delete_pricing' + pricing_id).removeClass('fa fa-trash');
                        $('#delete_pricing' + pricing_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        if (data) {
                            toastr.success('Pricing deleted Successfully');
                            getPricingListing();
                        }
                    },
                    complete: function() {
                        $('#delete_pricing_btn' + pricing_id).attr('disabled', false);
                        $('#delete_pricing' + pricing_id).removeClass('fa fa-spinner fa-spin');
                        $('#delete_pricing' + pricing_id).addClass('fa fa-trash');
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
        },
    });
}
$('#pricing_refresh').click(function() {
    $('.ser ').val('').select2();
    getPricingListing();

})