<?php



namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\KarkinosApiController;
use ExtensionsValley\Apis\KarkinosController;

class KarkinosGetPatientIDJob extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'schedule-get-patient-id-from-karkinos:cron';



    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'schedule get patient id from karkinos';



    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }



    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $get_patient_id_job = (new KarkinosApiController())->getKarkinosPatientIdJob();
    }



}
