<?php



namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Master\AlterEmrDoctorAssesment;

class AlterAssessmentJob extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'alter-assessment-job:cron';



    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'Alter clinical Assessment';



    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }



    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $company_code = \DB::table('company')->value('code');
        if(isset($company_code) && $company_code == 'KEYHOLE'){
            $alter_assessment_job = (new AlterEmrDoctorAssesment())->alterDoctorAssesment();
        }

    }



}
