<?php

   

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\SMSController;

class SendSMSJob extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'send-sms-job:cron';

    

    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'Schedular for sending sms.';

    

    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }

    

    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $sms_job = (new SMSController())->sendSMS();
    }

    

}