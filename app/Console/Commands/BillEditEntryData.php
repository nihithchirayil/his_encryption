<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ExtensionsValley\Apis\BillEditJsonLogController;

class BillEditEntryData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bill-edit-entry-json-log:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedular for parse json log after editing a bill';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $generateExcel = (new BillEditJsonLogController())->ParseJsonLog();
    }
}
