<?php

   

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\PatientHistoryController;

class PatientHistoryJob extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'fetch-patient-history-job:cron';

    

    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'Schedular fetching patient history.';

    

    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }

    

    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $patient_history_job = (new PatientHistoryController())->fetchPatientHistoryData();
    }

    

}