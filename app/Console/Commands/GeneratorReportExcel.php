<?php



namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\GeneratorReportController;

class GeneratorReportExcel extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'genarate-excel-report:cron';



    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'Schedular for Genearating Excel';



    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();
    }



    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $generateExcel = (new GeneratorReportController())->GenarateExcel();
    }
}
