<?php



namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\BestdocConciergeApiController;

class BestdocConciergeApiJob extends Command
{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'bestdoc-Concierge-notification:cron';



    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'send mobile notification to patient to rise request ';



    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }



    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $get_send_notification_job = (new BestdocConciergeApiController())->SendBestdocNotification();

    }



}
