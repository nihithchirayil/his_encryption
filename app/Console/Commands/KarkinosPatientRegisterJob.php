<?php



namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\KarkinosApiController;
use ExtensionsValley\Apis\KarkinosController;

class KarkinosPatientRegisterJob extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'schedule-patient-registration:cron';



    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'schedule patient registration.';



    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }



    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
       // if(env('KARKINOS_API_ENABLED',0)==1){
            $patient_register_job = (new KarkinosApiController())->RegisterPatientInKarkinos();
       // }
    }



}
