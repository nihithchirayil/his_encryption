<?php

   

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use ExtensionsValley\Apis\MailController;

class SendMailJob extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'send-mail-job:cron';

    

    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'Schedular for sending emails.';

    

    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

    }

    

    /**

     * Execute the console command.

     *

     * @return mixed

     */

    public function handle()
    {
        $mail_job = (new MailController())->sendMail();
    }

    

}