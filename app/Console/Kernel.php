<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\SendMailJob::class,
        Commands\SendSMSJob::class,
        Commands\PatientHistoryJob::class,
        Commands\KarkinosPatientRegisterJob::class, // please comment if db become slow
        Commands\KarkinosGetPatientIDJob::class,// please comment if db become slow
        commands\KarkinosSendDiaganosisResults::class, // please comment if db become slow
        commands\BestdocConciergeApiJob::class, // please comment if db become slow
        commands\PacsIntegrationAddStudy::class,
        commands\GeneratorReportExcel::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $company_code = \DB::table('company')->where('id', 1)->value('code');
        $company_sub_code = \DB::table('company')->where('id', 1)->value('sub_code');
        if($company_code == 'ROHINI'){
            $schedule->command('send-sms-job:cron')->everyMinute()->withoutOverlapping();
        } else if($company_code == 'DAYAH' && $company_sub_code == 'DAYAPKD') {
            $schedule->command('send-mail-job:cron')->everyMinute()->withoutOverlapping();
        } else {
            $schedule->command('send-mail-job:cron')->everyMinute()->withoutOverlapping();
            $schedule->command('send-sms-job:cron')->everyMinute()->withoutOverlapping();
            // $schedule->command('genarate-excel-report:cron')->everyMinute()->withoutOverlapping();
            $schedule->command('fetch-patient-history-job:cron')->everyMinute()->withoutOverlapping(); // please comment if db become slow
            $schedule->command('bestdoc-Concierge-notification:cron')->everyMinute()->withoutOverlapping(); // please comment if db become slow
            $schedule->command('alter-assessment-job:cron')->everyMinute()->withoutOverlapping();
            if(env('KARKINOS_API_ENABLED',0)==1 && env('KARKINOS_API_PREFIX','') != ''){
                $schedule->command('schedule-patient-registration:cron')->everyMinute()->withoutOverlapping(); // please comment if db become slow

                $schedule->command('schedule-get-patient-id-from-karkinos:cron')->everyMinute()->withoutOverlapping(); // please comment if db become slow
                $schedule->command('send-diagonosis-result-to-karkinos:cron')->everyMinute()->withoutOverlapping(); // please comment if db become slow
            }
            $schedule->command('pacsintegration-addstudynew:cron')->everyMinute()->withoutOverlapping(); // commented due to evertech server not responding
        }

        $schedule->command('genarate-excel-report:cron')->everyMinute()->withoutOverlapping();
        $schedule->command('bill-edit-entry-json-log:cron')->everyMinute()->withoutOverlapping();



    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
