<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Hash;
use App\Models\BlacklistedTokens;

class TokenController extends Controller
{
    /**
     * @var array $data response data
     */
    protected $data;

    /**
     * @var string  $status response code
     */
    protected $status;

     /**
     * Initialize response
     * @author Vishnu R Nair
     *
     * @return void
     */
    public function __construct()
    {
        $this->data     = [
            'success'       => true,
            'info'          => '',
            'result'        => []
        ];
        $this->status  = Response::HTTP_OK;
    }

    /**
     * Create user authentication token
     * @author  Vishnu R Nair
     * @param   integer     $user_id User id returned after authentication
     * @param   string      $username
     *
     * @return  string      jwt token
     */
    public static function createToken($userDetails ='')
    {
        return (string)((new Builder())->setIssuer(config('jwt.issuer'))
            ->setAudience(config('jwt.audience'))
            ->setId(config('jwt.jti'), true)
            ->setIssuedAt(time())
            ->setExpiration(time() + config('jwt.expiration'))

            ->set('expires_at', time() + config('jwt.expiration'))
            ->sign(new Sha256(), config('jwt.secret'))
            ->getToken());
    }

    /**
     * verify authentication Token whether is valid or not
     * @author Vishnu R Nair
     * @param  string $token
     *
     * @return bool
     */
    public function verifyToken($token)
    {
        try
        {
            return ((new Parser())->parse((string) $token))->verify(new Sha256(), config('jwt.secret'));
        }
        catch(\Exception $e)
        {
            throw new \Exception('Invalid token');
        }
    }

    /**
     * Check token is expired
     * @author Vishnu R Nair
     * @param  string $token
     * @return bool
     */

    public function isTokenExpired($token)
    {
        $token = (new Parser())->parse((string) $token);
        return $token->validate(new ValidationData());
    }
    /**
     * Check token is blacklisted or not
     * @author Vishnu R Nair
     * @param  string $token
     * @return bool
     */

    public function isTokenBlackListed($token)
    {
        return (new BlacklistedTokens())->isBlackListed((string) $token);
    }

     /**
     * get authenticate token from header
     * @author Vishnu R Nair
     *
     * @return string
     */
    public function getAuthToken()
    {
        $token      = null;
        $headers    = array_change_key_case(getallheaders(),CASE_LOWER);
        $bearer     = ( isset($headers['authorization']) && !empty(trim($headers['authorization'])) ) ? $headers['authorization'] : null;
        if( $bearer )
        {
            $parts = explode(" ", $bearer);
            $token = $parts[1];
        }
        return $token;
    }

    /**
     * get payload from auth token
     * @author Vishnu R Nair
     * @param Request $request
     *
     * @return array
     */

     public function getPayloadFromToken($request)
     {
        return (new Parser())->parse((string) $this->getAuthToken($request))->getClaims();
     }

     /**
     * Generate and send  new token
     * @author Vishnu R Nair
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */

    public function refreshToken(Request $request)
    {
        $this->data['info']         = 'authenticated';
        $this->data['result']       = [
            'token'=> (new TokenController())->createToken([
                'username'  => $request->input('token_claim_username'),
                'user_id'   => $request->input('token_claim_user_id'),
                'user_type' => $request->input('token_claim_user_type'),
                'role_id'   => $request->input('token_claim_role_id')
            ])
        ];
        // return API Response
        return (new Response($this->data, (int)$this->status));
    }

    /**
     * Validate token from external/child calls
     * @author Sajessh
     *
     * @param Illuminate\Http\Request;
     * @return  Illuminate\Http\Response;
     */
    public function validateChildToken(Request $request)
    {
        $token = $this->getAuthToken();
        if ($token == null)
        {
            return (new Response(['success' => false, 'info' => 'Token is Mandotary'], Response::HTTP_UNAUTHORIZED));
        }
        else if ($this->verifyToken($token) == true)
        {
            if(!$this->isTokenExpired($token) == true)
            {
                return (new Response(['success' => false, 'info' => 'Token Expired'], Response::HTTP_UNAUTHORIZED));
            }

            return (new Response(['success' => true, 'info' => 'Valid Token'], Response::HTTP_OK));
        }
        else
        {
            // return API Response
            return (new Response(['success' => false, 'info' => 'Invalid Token'], Response::HTTP_UNAUTHORIZED));
        }
    }

    /**
     * get authenticate token from header for third party  API
     * @author Vishnu R Nair
     *
     * @return string
     */
    public function getAuthTokenFrom()
    {
        $token      = null;
        $headers    = array_change_key_case(getallheaders(),CASE_LOWER);
        $bearer     = ( isset($headers['authorization']) && !empty(trim($headers['authorization'])) ) ? $headers['authorization'] : null;
        if( $bearer )
        {
            $parts = explode(" ", $bearer);
            $token = $parts[1];
        }
        return $token;
    }
}
