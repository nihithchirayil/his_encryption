<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserRequestLogs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_date = date('Y-m-d H:i:s');
        $user_id = (!empty(\Auth::user()->id)) ? \Auth::user()->id : 0;
        $route = $request->route();
        $name = $route->getName();
        $uri = $route->uri();
        $ip_address = $request->ip();
        if ($name == 'extensionsvalley.admin.auth') {
            $email = $request->input('email');
            $user_id = \DB::table('users')
                        ->where(function($q) use ($email){
                            $q->where('email', $email)
                            ->orWhere('username', $email);
                        })
                        ->value('id');
            $user_id = @$user_id ?  $user_id : 0;
   
        if (!empty($user_id)) {

            $insert_arr = [
                'user_id' => $user_id,
                'route_name' => $name,
                'uri' => $uri,
                'request_data' => json_encode($request->all()),
                'app_name' => 'web',
                'ip_address' => $ip_address,
                'created_by' => $user_id,
                'updated_by' => $user_id,
                'created_at' => $current_date,
                'updated_at' => $current_date,
            ];

            \DB::table('user_request_logs')->insert($insert_arr);
        }
    }

        return $next($request);
    }
}
