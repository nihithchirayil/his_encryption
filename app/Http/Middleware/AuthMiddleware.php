<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use App\Http\Controllers\TokenController;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     * @author Vishnu R Nair
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try
        {
            $token_controller   = new TokenController();
            $token              = $token_controller->getAuthToken();

            if ($token == null)
            {
                throw new \Exception('Token is mandatory');
            }
            else if ($token_controller->verifyToken($token) == true)
            {
                if(!$token_controller->isTokenExpired($token) == true)
                {
                    throw new \Exception('Token expired');
                }


                return $next($request);
            }
            else
            {
                throw new \Exception('Invalid token');
            }
        }
        catch(\Exception $e)
        {
            // return API Response
            return (new Response(['success' => false, 'info' => $e->getMessage()], Response::HTTP_UNAUTHORIZED));
        }
    }
}
