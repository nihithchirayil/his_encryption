<?php

namespace App\Http\Middleware;

use Closure;
use ExtensionsValley\Dashboard\Models\Menu;
use ExtensionsValley\Dashboard\Models\MenuPermissions;
use Illuminate\Support\Facades\Auth;

class CheckMenuAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Schema::hasTable('menu_permissions')) {
            $menu_sql = NULL;
            $menu_permission = NULL;
            try {
                $menu_sql = Menu::select('id')->where('is_desktop_menu', 0)->where('menu.route_name', '=', \Route::currentRouteName())
                ->where('status', '=', 1)
                ->get()->toArray();

            } catch (\PDOException $e) {

            }
            $menu_sql=array_map('current', $menu_sql);
            if (isset($menu_sql) && count($menu_sql)) {

                $menu_permission = MenuPermissions::join('user_group', 'user_group.group_id', '=', 'menu_permissions.group_id')
                    ->where('user_group.user_id', '=', \Auth::user()->id)
                    ->whereIn('menu_permissions.menu_id', $menu_sql)
                    ->where( function ($query) {
                        $query->orWhere('menu_permissions.has_add', '=', '1')
                        ->orWhere('menu_permissions.has_edit', '=', '1')
                        ->orWhere('menu_permissions.has_view', '=', '1');
                    })
                    ->count('menu_permissions.id');
                if (! $menu_permission) {
                    return redirect('admin/dashboard')->with('error','You are not authorised to access admin pages.');
                }
            }
        }
        return $next($request);
    }
}
