<?php 

namespace App\Mail;

use Illuminate\Mail\Mailable;

class AttachmentEmail extends Mailable
{
    /**
     * @var     string  email object
     * @author  vishnu
     * 
     */
    public $email;

    /**
     * Init general email class
     * @author  vishnu
     * @param   object  $email  email data object
     */
    public function __construct($email)
    {
        $this->email     = $email;
    }

    /**
     * Build the message.
     * @author  vishnu
     *
     * @return $this
     */
    public function build()
    {

        if($this->email['attachment_type'] == 1) {
            if($this->email['mail_type'] == 'LAB'){
                $prefix = env('LAB_REPORT_FILE_PREFIX_PUBLIC', '');
            } else {
                $prefix = '';
            }
            
            $email = $this->view($this->email['template'], $this->email['data'])->subject($this->email['subject']);
            if(!empty($this->email['attachments'])) {
                foreach($this->email['attachments'] as $filePath){
                    $email->attach($prefix.$filePath);
                }
            }

        } else if($this->email['attachment_type'] == 2) {
            
            $email = $this->view($this->email['template'], $this->email['data'])
            ->subject($this->email['subject']);

            foreach($this->email['attachments'] as $document){
                $email->attach($document->getRealPath(), [
                    'as' => $document->getClientOriginalName(),
                    'mime' => $document->getClientMimeType(),
                ]);
            }

        } else {
            // $email =  $this->subject($this->email['subject'])->view($this->email['template'], $this->email['data'])->attachData(base64_decode($this->email['data']['attachment']), $this->email['subject'].'.pdf');
            $email =  $this->subject($this->email['subject'])->view($this->email['template'], $this->email['data']);
        }
            

        return $email;
    }
}
?>