<?php

use Illuminate\Database\Seeder;

class PaymentModeChequeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // if does not exisits insert it
        if(!\DB::table('payment_mode')->where('code','cheque')->exists()){
            $paymentModeData = [
                [
                    'name' => 'Cheque',
                    'code' => 'cheque',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => 1,
                    'payment_type_id' => 1,
                    'status' => 1,
                    'returnapplicable' => true,
                    'billpayvisible' => true,
                    'account_head' => null,
                    'is_cardno_mandatory' => false,
                    'is_bank_mandatory' => false,
                    'is_expirydate_mandatory' => false,
                    'is_insurance' => true,
                    'default_insurance' => 1,
                    'show_in_dotnet' => true,
                    'is_machine_bank_mandatory' => false,
                    'payment_mode_type' => 6,
                    'sl_no' => 0,
                    'is_upi' => false,
                ],
                // Add more rows if needed
            ];

            // Insert data into the payment_mode table
            \DB::table('payment_mode')->insert($paymentModeData);
        }
    }
}
