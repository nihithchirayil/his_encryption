<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Check if the department with the given dept_name already exists
        $existingDepartment = DB::table('department_master')->where('dept_name', 'OT')->first();

        if (!$existingDepartment) {
            // Department does not exist, insert the data
            DB::table('department_master')->insert([
                'dept_code' => '182',
                'dept_name' => 'OT',
                'status' => 1,
                'created_at' => '2019-06-11 15:44:51.724883',
                'created_by' => 2465,
                'unit_id' => 1,
                'application_name' => 'mig_data',
                'is_lab_department' => 0,
                'is_ip_package' => 0,
                'is_radiology' => 0,
            ]);
        } else {
            // Department already exists, display a message or log
            echo "Department 'OT' already exists.\n";
        }
    }
}
