<?php

use Illuminate\Database\Seeder;

class DosageUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');
        \DB::table('dosage_unit')->truncate();
        $dosage_unit = ['MG', 'ML', 'DROPS', 'Tablets', 'GM'];

        $create_arr = [];
        foreach ($dosage_unit as $key => $value) {
            $create_arr[] = [
                'name' => $value,
                'status' => 1,
                'created_at' => $date_time,
                'updated_at' => $date_time,
                'created_by' => 1,
                'updated_by' => 1,
            ];
        }
        \DB::table('dosage_unit')->insert($create_arr);    
    }
}
