<?php

use Illuminate\Database\Seeder;

class StockReturnUpdation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dropSql1 = "DROP TRIGGER tg_update_stock_return_detail_update_product_stock ON public.stock_return_detail";
        \DB::statement($dropSql1);

        $dropSql2 = "DROP TRIGGER tg_insert_stock_return_detail_update_product_stock ON public.stock_return_detail";
        DB::statement($dropSql2);

        $sql3 = "ALTER TABLE stock_return_detail ALTER COLUMN return_quantity TYPE numeric(10,2) USING return_quantity::numeric(10,2)";
        \DB::statement($sql3);

        $sql4 = "ALTER TABLE stock_return_detail ALTER COLUMN return_received_qty TYPE numeric(10,2) USING return_received_qty::numeric(10,2)";
        \DB::statement($sql4);

        $createsql1 = "CREATE TRIGGER tg_update_stock_return_detail_update_product_stock
        AFTER UPDATE
        ON public.stock_return_detail
        FOR EACH ROW
        WHEN (new.deleted_at IS NULL AND new.return_received_qty IS NOT NULL AND new.return_received_qty <> COALESCE(old.return_received_qty, 0))
        EXECUTE FUNCTION public.tg_update_stock_return_detail_update_product_stock_fn();";

        \DB::statement($createsql1);

        $createsql2 = " CREATE TRIGGER tg_insert_stock_return_detail_update_product_stock
        AFTER INSERT
        ON public.stock_return_detail
        FOR EACH ROW
        WHEN (new.deleted_at IS NULL AND new.return_quantity > 0)
        EXECUTE FUNCTION public.tg_insert_stock_return_detail_update_product_stock_fn();";
        \DB::statement($createsql2);

    }
}
