<?php

use Illuminate\Database\Seeder;

class CreateGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');
        $group[] = array(
            'name' => 'LOGINUSERREPORTADMIN',
        );
        $create_arr = [];
        foreach ($group as $key => $value) {
            $group_name = @$value['name'] ? $value['name'] : NULL;
            if (!\DB::table('groups')->where('name', '=', $group_name)->where('status', 1)->exists()) {
                $create_arr[] = [
                    'name' => $group_name,
                    'status' => 1,
                    'created_at' => $date_time,
                    'updated_at' => $date_time,
                    
                ];
            }
        }
        \DB::table('groups')->insert($create_arr);     
    }
}
