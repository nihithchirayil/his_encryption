<?php

use Illuminate\Database\Seeder;

class VitalDefaultValue extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vital_id = \DB::table('vital_master')->where('name','Temperature F')->value('id');
        \DB::table('vital_master')->where('id',$vital_id)->update(['defaultvalue' => 1]);  
        $date_time = date('Y-m-d H:i:s');
        $vital = [ 
            [
                "name" => "Spirometry",
                "defaultvalue" => 1,
                "display_order" => 23,
                "vital_head" => 'Spirometry',
                "status" => 1,
            ],
           
        ];

        $create_arr = [];
        foreach ($vital as $key => $value) {
        if (!\DB::table('vital_master')->where('name', 'Spirometry')->where('status', 1)->exists()) {

            $create_arr[] = [
                'name' => $value['name'],
                'defaultvalue' => $value['defaultvalue'],
                'status' => $value['status'],
                'display_order' => $value['display_order'],
                'vital_head' => $value['vital_head'],
                'created_at' => $date_time,
                'updated_at' => $date_time,
                'created_by' => 1,
                'updated_by' => 1,
            ];
       
        \DB::table('vital_master')->insert($create_arr);    
        }
        }
    }
}
