<?php

use Illuminate\Database\Seeder;
use ExtensionsValley\Emr\CommonController;

class ConfigDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');
        $company_code = CommonController::getCompanyCode();
        $config_list[] = array(
            "head_name" => "INVENTORY",
            "detail_name" => "disable_generalstore_if_receive_pending",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "LAB",
            "detail_name" => "enable_automatic_mail",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "AccountPostingBlock30days",
            "detail_name" => "block_greater30days_selectionandupdate",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "ShowFarenheitInAllScreen",
            "detail_name" => "ShowFarenheit",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "pharmacy",
            "detail_name" => "enable_password_confirmation_before_save",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "LicenseNoFlagEmrLite",
            "detail_name" => "license_no_flag",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "Enable_load_area_master",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_Shift_wise_pricing",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "Print_doctor_prescription_after_reg",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_default_walk_in_reg",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "Enable_mobile_number_validation",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "POS_Device_Integration_Settings",
            "detail_name" => "world_line_status_check_url",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "POS_Device_Integration_Settings",
            "detail_name" => "enable_pos_device_hmis_integration",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "POS_Device_Integration_Settings",
            "detail_name" => "world_line_iv",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "POS_Device_Integration_Settings",
            "detail_name" => "world_line_enc_key",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "POS_Device_Integration_Settings",
            "detail_name" => "world_line_url",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_continues_special_token",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "remove_duplicate_charges_for_multiple_doctor_consultation",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "cash_settings",
            "detail_name" => "allow_cash_close_without_denomination",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "disable_outside_basic_registration_in_service_billing",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "pharmacy",
            "detail_name" => "enable_new_pharmacy_print_daya",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_ip_package",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "LAB",
            "detail_name" => "show_lab_name_in_result_entry",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "ShowShiftWisePatient",
            "detail_name" => "show_shiftwise_patient",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        if ($company_code == 'SPARSH') {
            $config_list[] = array(
                "head_name" => "ShowPatientPaymentNotDone",
                "detail_name" => "ShowPatientPaymentNotDoneSpursh",
                "status" => 1,
                "value" => 1,
                "value_string" => '',
            );
        } else if ($company_code == 'DAYAH') {
            $config_list[] = array(
                "head_name" => "NoOfSlotsEditableBlock",
                "detail_name" => "NoOfSlotsEditableBlockInEmr",
                "status" => 1,
                "value" => 1,
                "value_string" => '',
            );
        } else if ($company_code == 'KMC') {
            $config_list[] = array(
                "head_name" => "LicenseNoFlagEmrLite",
                "detail_name" => "license_no_flag",
                "status" => 1,
                "value" => 1,
                "value_string" => '',
            );
        }

        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "enable_new_bookmark_in_ns",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );

        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "casuality_user_investigation_indent_type",
            "status" => 1,
            "value" => 3,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "casuality_user_prescription_indent_type",
            "status" => 1,
            "value" => 3,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "enable_intend_paid_status_on_nursing",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "PrescroptionChanges",
            "detail_name" => "prescription_changes",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "shift1_active",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "web_emr_settings",
            "detail_name" => "enable_medicine_administration_in_nursing_dashboard",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "department_id_for_systsem_generated_services",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "return_gate_pass_into_advance",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_discount_percentage_limit_for_billing",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_card_number",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );

        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "enable_yellow_sheet",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "nurse_bed_changes",
            "detail_name" => "bed_transfer_icu_bed_seperate",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "enable_external_uhid_for_patient_registration_in_nursing",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_discount_request",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_payment_receipt_in_patient_registration",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "Purchase",
            "detail_name" => "expiry_date_format",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "discharge_settings",
            "detail_name" => "hide_cancel_button_from_include_exclude",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "discharge_settings",
            "detail_name" => "generate_discharge_bill_based_on_sent_for_billing",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "discharge_settings",
            "detail_name" => "check_pending_bils_count_in_dischage_generation",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "discharge_settings",
            "detail_name" => "excepting_discharge_status",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "LAB",
            "detail_name" => "enable_lab_integration_api",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "discharge_settings",
            "detail_name" => "enable_send_for_billing",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "discharge_settings",
            "detail_name" => "enable_advance_auto_refund_after_discharge_bill_payment",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );

        $config_list[] = array(
            "head_name" => "file_uploads",
            "detail_name" => "outside_lab_result_view_btn",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "file_uploads",
            "detail_name" => "file_upload_view_path",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        $config_list[] = array(
            "head_name" => "Finance",
            "detail_name" => "enable_save_continue",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "Finance",
            "detail_name" => "enable_show_all_entry",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "Finance",
            "detail_name" => "enable_multiple_naration",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "Finance",
            "detail_name" => "new_accounts_posting",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_camp",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );

        $config_list[] = array(
            "head_name" => "pharmacy",
            "detail_name" => "enable_discount_code_service",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "disable_barcode_in_service_print",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );

        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "enable_location_wise_inv_group_ns",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "enable_user_wise_locations",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "AllowAdmissionDateEdit",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "Finance",
            "detail_name" => "hide_check_no_bank_name_from_expense_master",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "EMR Changes",
            "detail_name" => "allow_outside_medicine_config",
            "status" => 1,
            "value" => 1,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "front_office_settings",
            "detail_name" => "link_expenses_to_cash_close",
            "status" => 1,
            "value" => 0,
            "value_string" => '',
        );
        $config_list[] = array(
            "head_name" => "Purchase",
            "detail_name" => "enable_payment_mode_grn",
            "status" => 1,
            "value" => 0,
            "value_string" => "",
        );
        if (count($config_list) != 0) {
            foreach ($config_list as $key => $value) {
                $head_name = @$value['head_name'] ? $value['head_name'] : NULL;
                $detail_name =  @$value['detail_name'] ? $value['detail_name'] : NULL;
                $status =  @$value['status'] ? $value['status'] : 0;
                $value =  @$value['value'] ? $value['value'] : 0;
                $value_string =  @$value['value_string'] ? $value['value_string'] : NULL;

                if ($head_name) {
                    if (\DB::table('config_head')->where('name', '=', $head_name)->where('status', 1)->exists()) {
                        $head_id = \DB::table('config_head')->where('name', '=', $head_name)->value('id');
                    } else {
                        $head_value = [
                            'name' => $head_name,
                            'status' => $status,
                            'value' => $value,
                            "created_at" => $date_time,
                            "created_by" => 1,
                            "unit_id" => 1,
                            "application_name" => 'Laravel Migration',
                        ];
                        $head_id = \DB::table('config_head')->insertGetId($head_value);
                    }
                    if (!\DB::table('config_detail')->where('name', '=', $detail_name)->where('head_id', $head_id)->exists()) {
                        $detail_value = [
                            'name'       => $detail_name,
                            'head_id'    => $head_id,
                            'status'     => $status,
                            'value'      => $value,
                            "created_at" => $date_time,
                            "created_by" => 1,
                            "unit_id"    => 1,
                            "application_name" => 'Laravel Migration',
                            'value_string' => $value_string,
                        ];
                        \DB::table('config_detail')->insert($detail_value);
                    }
                }
            }
        }
    }
}
