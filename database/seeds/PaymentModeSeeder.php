<?php

use Illuminate\Database\Seeder;

class PaymentModeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('payment_mode')
        ->whereIn('code', ['cash','credit','creditcard','debitcard','upi','cheque','demanddraft'])
        ->update(['include_grn' => 1]);
    }
}
