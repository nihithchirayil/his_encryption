<?php

use Illuminate\Database\Seeder;

class SurgeryAnesthesiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'name' => 'G.A', 'status' => 1],
            ['id' => 2, 'name' => 'G.A', 'status' => 1],
            ['id' => 3, 'name' => 'S.A', 'status' => 1],
            ['id' => 4, 'name' => 'E.A', 'status' => 1],
            ['id' => 5, 'name' => 'CSE', 'status' => 1],
            ['id' => 6, 'name' => 'I.V SEDATION', 'status' => 1],
            ['id' => 7, 'name' => 'MAC', 'status' => 1],
            ['id' => 8, 'name' => 'RA', 'status' => 1],
            ['id' => 9, 'name' => 'L.A', 'status' => 1],
            ['id' => 10, 'name' => 'BLOCK', 'status' => 1],
        ];

        \DB::table('surgery_anesthesia')->insert($data);
    }
}
