<?php

use Illuminate\Database\Seeder;

class SpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $specialityName = 'ANAESTHESIA';

        // Check if the record with the given name already exists
        $existingRecord = DB::table('speciality')->where('name', $specialityName)->first();

        // Insert data only if the record doesn't exist
        if (!$existingRecord) {
            DB::table('speciality')->insert([
                'name' => $specialityName,
                'speciality_code' => 'anae',
                'status' => 1,
                'created_at' => null,
                'updated_at' => '2019-06-24 15:54:56.573814',
                'updated_by' => 2490,
                'created_by' => null,
                'deleted_at' => null,
                'pc_name' => null,
                'ip_address' => null,
                'unit_id' => 1,
                'application_name' => 'mig_data',
                'maximum_visits' => null,
                'is_consultation' => 0,
                'department_id' => 1,
                'sub_department_id' => 56,
                'revenue_target' => 0,
            ]);
        }
    }
}
