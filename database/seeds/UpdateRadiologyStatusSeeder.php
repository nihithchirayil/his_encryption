<?php

use Illuminate\Database\Seeder;

class UpdateRadiologyStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Define the department names to search for
        $departmentNames = ['Radiology', 'RADIOLOGY', 'radiology','radio','RADIO','Radio'];

        // Update the 'is_radiology' column to 1 for matching department names
        DB::table('department_master')
            ->whereIn('dept_name', $departmentNames)
            ->update(['is_radiology' => 1]);
    }
}
