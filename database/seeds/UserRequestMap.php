<?php

use Illuminate\Database\Seeder;

class UserRequestMap extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_request_map')->truncate();
        $date = date('Y-m-d H:i:s');
        $data[] = [
            'route_name' => 'extensionsvalley.admin.auth',
            'description' => 'Login',
            'created_at' => $date,
            'updated_at' => $date
        ];
    \DB::table('user_request_map')->insert($data);
    }
}
