<?php

use Illuminate\Database\Seeder;
use ExtensionsValley\Emr\CommonController;

class DiscountApproveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');

        $approve_status[] = array(
            'name' => 'Requested',
            'value' => 1,
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
        );
        $approve_status[] = array(
            'name' => 'Approved',
            'value' => 2,
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
        );
        $approve_status[] = array(
            'name' => 'Rejected',
            'value' => 3,
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
        );

        if (count($approve_status) != 0) {

            foreach ($approve_status as $each) {
                $status_name = @$each['name'] ? trim($each['name']) : '';
                if ($status_name) {
                    if (!\DB::table('discount_approve_status')->where('name', '=', $status_name)->exists()) {
                        \DB::table('discount_approve_status')->insert($each);
                    }
                }
            }
        }
    }
}
