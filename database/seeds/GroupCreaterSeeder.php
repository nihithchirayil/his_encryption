<?php

use Illuminate\Database\Seeder;
use ExtensionsValley\Emr\CommonController;

class GroupCreaterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');

        $group_list[] = array(
            'name' => 'OT Surgery Checklist Parameters',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Surgery Checklist PreOpVitals',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Surgery Checklist SignIn',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Surgery Checklist Timeout',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Surgery Checklist NursingRecord',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Surgery Checklist SignOut',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Anaesthesia Checklist PreAnaesthetic',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Anaesthesia AnaesthesiaRecord',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Anaesthesia Checklist',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Anaesthesia PostOpAssessment',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Operation Notes',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Billing',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Histopathology Form',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Microbiology Form',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Medicine Indent',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Schedule',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );
        $group_list[] = array(
            'name' => 'OT Approval Access',
            'status' => 1,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => 'Web Seeder',
            'is_system_role' => 1,
        );


        if (count($group_list) != 0) {

            foreach ($group_list as $each) {
                $detail_name = @$each['name'] ? trim($each['name']) : '';
                if ($detail_name) {
                    if (!\DB::table('groups')->where('name', '=', $detail_name)->where('status', 1)->exists()) {
                        \DB::table('groups')->insert($each);
                    }
                }
            }
        }
    }
}
