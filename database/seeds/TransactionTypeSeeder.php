<?php

use Illuminate\Database\Seeder;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('transaction_type')->insert([
            [
                'transaction_name' => 'Opening Stock',
                'transaction_type' => 'OS',
                'table_name' => 'stock_transaction_track',
                'operation_name' => 'Opening Stock',
            ],
            // Add more rows if needed
        ]);
    }
}
