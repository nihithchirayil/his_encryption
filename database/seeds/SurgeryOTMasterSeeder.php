<?php

use Illuminate\Database\Seeder;

class SurgeryOTMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('surgery_ot_master')->insert([
            ['id' => 1, 'ot_name' => 'OT1', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 2, 'ot_name' => 'OT2', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 3, 'ot_name' => 'OT3', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 4, 'ot_name' => 'OT4', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 5, 'ot_name' => 'OT5', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 6, 'ot_name' => 'OT6', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 7, 'ot_name' => 'OT7', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 8, 'ot_name' => 'OT8', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 9, 'ot_name' => 'OT9', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 10, 'ot_name' => 'OT10', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 11, 'ot_name' => 'OT11', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
            ['id' => 12, 'ot_name' => 'OT12', 'start_time' => '08:00:00', 'end_time' => '20:00:00'],
        ]);
    }
}
