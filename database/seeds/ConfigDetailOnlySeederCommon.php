<?php

use Illuminate\Database\Seeder;
use ExtensionsValley\Emr\CommonController;

class ConfigDetailOnlySeederCommon extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');

        $config_list[] = array(
            'name' => 'Enable_new_lab_format_orchid',
            'head_id' => 21,
            'status' => 1,
            'value' => 0,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
            'application_name' => 'WebMig',
            'value_string' => '60',
        );

        $config_list[] = array(
            'name' => 'disable_indent_details_in_Service',
            'head_id' => 21,
            'status' => 1,
            'value' => 0,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
            'application_name' => 'WebMig',
            'value_string' => '60',
        );


        $config_list[] = array(
            'name' => 'Disable_panel_From_CashClose',
            'head_id' => 31,
            'status' => 1,
            'value' => 0,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
            'application_name' => 'WebMig',
            'value_string' => '#ffffff',
        );


        $config_list[] = array(
            'name' => 'add_pending_bills_to_discharge',
            'head_id' => 1,
            'status' => 1,
            'value' => 0,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
            'application_name' => 'WebMig',
            'value_string' => '#ffffff',
        );

        $config_list[] = array(
            'name' => 'edit_bill_payment_mode',
            'head_id' => 1,
            'status' => 1,
            'value' => 5,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
            'application_name' => 'Web',
            'value_string' => '#ffffff',
        );
        $config_list[] = array(
            'name' => 'disable_issue_greaterthan_approve_qty',
            'head_id' => 1015,
            'status' => 1,
            'value' => 0,
            'created_at' => $date_time,
            'updated_at' => $date_time,
            'created_by' => 1,
            'updated_by' => 1,
            'application_name' => 'Web',
            'value_string' => '#ffffff',
        );

        if (count($config_list) != 0) {

            foreach ($config_list as $each) {
                $detail_name = @$each['name'] ? trim($each['name']) : '';
                $head_id = @$each['head_id'] ? trim($each['head_id']) : '';
                if ($detail_name && $head_id) {
                    if (!\DB::table('config_detail')->where('name', '=', $detail_name)->where('head_id', $head_id)->exists()) {
                        \DB::table('config_detail')->insert($each);
                    }
                }
            }
        }
    }
}
