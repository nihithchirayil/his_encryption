<?php

use Illuminate\Database\Seeder;

class ConfigDetailSeederStock extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_time = date('Y-m-d H:i:s');
        $config_list = array();

        $config_list = array(

                "head_name" => "DisableZeroStockItems",
                "detail_name" => "disable_zero_stock_items_prescription",
                "status" => 1,
                "value" => 1,
                "value_string" => '',
                
            );

            if (count($config_list) != 0) {
            $head_name = @$config_list['head_name'] ? $config_list['head_name'] : '';
            $detail_name =  @$config_list['detail_name'] ? $config_list['detail_name'] : '';
            $status =  @$config_list['status'] ? $config_list['status'] : 0;
            $value =  @$config_list['value'] ? $config_list['value'] : 0;
            $value_string =  @$config_list['value_string'] ? $config_list['value_string'] : '';

            if (\DB::table('config_head')->where('name', '=', $head_name)->where('status', 1)->exists()) {
                $head_id = \DB::table('config_head')->where('name', '=', $head_name)->value('id');

            }else{
                $head_value = [
                    'name' => $head_name,
                    'status'     => $status,
                    'value'      => $value,
                    "created_at" => $date_time,
                    "created_by" => 1,
                    "unit_id" => 1,
                    "application_name" => 'Laravel Migration',
                   
                ];
                $head_id = \DB::table('config_head')->insertGetId($head_value);
            }
            if (!\DB::table('config_detail')->where('name', '=', $detail_name)->where('head_id', $head_id)->exists()) {
                $detail_value = [
                    'name'       => $detail_name,
                    'head_id'    => $head_id,
                    'status'     => $status,
                    'value'      => $value,
                    "created_at" => $date_time,
                    "created_by" => 1,
                    "unit_id"    => 1,
                    "application_name" => 'Laravel Migration',
                    'value_string'=> $value_string
                ];

                \DB::table('config_detail')->insert($detail_value);
            }
        }    
    }
}
