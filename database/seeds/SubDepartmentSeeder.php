<?php

use Illuminate\Database\Seeder;

class SubDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Check if the sub_department with the given sub_dept_name already exists
        $existingSubDepartment = DB::table('sub_department_master')->where('sub_dept_name', 'OT')->first();

        if (!$existingSubDepartment) {
            // Sub_department does not exist, insert the data
            DB::table('sub_department_master')->insert([
                'dept_id' => 30,
                'sub_dept_code' => '250',
                'sub_dept_name' => 'OT',
                'status' => 1,
                'created_at' => '2019-06-11 15:45:08.25687',
                'created_by' => 2465,
                'updated_at' => '2019-06-11 15:45:08.25687',
                'updated_by' => 2465,
                'unit_id' => 1,
                'application_name' => 'mig_data',
                'lab_order' => 50,
                'is_microbiology' => 0,
            ]);
        } else {
            // Sub_department already exists, display a message or log
            echo "Sub_department 'OT' already exists.\n";
        }
    }
}
