<?php

use Illuminate\Database\Seeder;

class GroupsTableOtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert data into the "groups" table
        \DB::table('groups')->insert([
            'name' => 'ot_start_end_time_access',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'is_default' => 0,
            'is_lab_admin' => 0,
            'application_name' => null,
            'is_system_role' => 0,
        ]);
    }
}
