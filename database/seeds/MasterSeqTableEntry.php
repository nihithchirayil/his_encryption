<?php

use Illuminate\Database\Seeder;

class MasterSeqTableEntry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $master_sequence = array();

        $master_sequence = array(

                "type_code_value" => "SCAT",
                "type_number" => 1,
                "prefix_value" => "S000",
                "sequence_length" => 5,
                "sequence_name" => 'sub_category_id_seq',
                
            );   
            if (count($master_sequence) != 0) {
                $type_code_value = @$master_sequence['type_code_value'] ? $master_sequence['type_code_value'] : '';
                $sequence_name =  @$master_sequence['sequence_name'] ? $master_sequence['sequence_name'] : '';
                $type_number =  @$master_sequence['type_number'] ? $master_sequence['type_number'] : 0;
                $sequence_length =  @$master_sequence['sequence_length'] ? $master_sequence['sequence_length'] : 0;
                $prefix_value =  @$master_sequence['prefix_value'] ? $master_sequence['prefix_value'] : '';
    
                if (!\DB::table('master_seq_table')->where('type_code_value', '=', $type_code_value)->exists()) {
                    $sequence_value = [
                        'type_code_value' => $type_code_value,
                        'sequence_name'   => $sequence_name,
                        'type_number'     => $type_number,
                        'sequence_length' => $sequence_length,
                        "prefix_value"    => $prefix_value,
                       
                    ];
    
                    \DB::table('master_seq_table')->insert($sequence_value);
                }
            }      
    }
}
