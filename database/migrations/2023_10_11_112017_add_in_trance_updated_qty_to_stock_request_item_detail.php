<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInTranceUpdatedQtyToStockRequestItemDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('stock_request_item_detail')) {
            if (!Schema::hasColumn('stock_request_item_detail', 'in_trance_updated_qty')) {
        Schema::table('stock_request_item_detail', function (Blueprint $table) {
            $table->decimal('in_trance_updated_qty', 10, 2)->nullable();
        });
            }
        }
        // Set the table comment
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('stock_request_item_detail')) {
            if (Schema::hasColumn('stock_request_item_detail', 'in_trance_updated_qty')) {
                Schema::table('stock_request_item_detail', function (Blueprint $table) {
                    $table->dropColumn('in_trance_updated_qty');
                });
            }
        }
    }
}
