<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFreeStockEntryDetailTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('free_stock_entry_detail')) {
            Schema::create('free_stock_entry_detail', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('head_id')->nullable();
                $table->string('item_code')->nullable();
                $table->string('item_desc')->nullable();
                $table->string('batch')->nullable();
                $table->date('expiry_date')->nullable();
                $table->string('uom')->nullable();
                $table->decimal('tot_qty', 10, 2)->default(0);
                $table->decimal('unit_rate', 10, 2)->default(0);
                $table->decimal('unit_cost', 10, 2)->default(0);
                $table->decimal('unit_mrp', 10, 2)->default(0);
                $table->decimal('sales_price', 10, 2)->default(0);
                $table->decimal('unit_tax', 10, 2)->default(0);
                $table->decimal('tot_tax_amt', 10, 2)->default(0);
                $table->decimal('net_cost', 10, 2)->default(0);
                $table->string('location_code')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->timestamp('created_at')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamp('deleted_at')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('free_stock_entry_detail');
    }
}
