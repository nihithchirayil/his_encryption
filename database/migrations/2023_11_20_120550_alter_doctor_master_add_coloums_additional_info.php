<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDoctorMasterAddColoumsAdditionalInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('doctor_master')) {
            if (!Schema::hasColumn('doctor_master', 'additional_information')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->text('additional_information')->nullable();
                });
            }
            if (!Schema::hasColumn('doctor_master', 'ip_room_no')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->string('ip_room_no')->nullable();
                });
            }
            if (!Schema::hasColumn('doctor_master', 'hospital_wise_renewal_rule_times')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->integer('hospital_wise_renewal_rule_times')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('doctor_master')) {
            if (Schema::hasColumn('doctor_master', 'additional_information')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->dropColumn('additional_information');
                });
            }
            if (Schema::hasColumn('doctor_master', 'ip_room_no')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->dropColumn('ip_room_no');
                });
            }
            if (Schema::hasColumn('doctor_master', 'hospital_wise_renewal_rule_times')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->dropColumn('hospital_wise_renewal_rule_times');
                });
            }
        }
    }
}
