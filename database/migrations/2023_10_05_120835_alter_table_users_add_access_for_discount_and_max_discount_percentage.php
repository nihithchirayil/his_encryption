<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUsersAddAccessForDiscountAndMaxDiscountPercentage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'access_for_discount')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->smallInteger('access_for_discount')->default(0);
                });
            }
            if (!Schema::hasColumn('users', 'max_discount_percentage')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->decimal('max_discount_percentage')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'access_for_discount')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('access_for_discount');
                });
            }
            if (Schema::hasColumn('users', 'max_discount_percentage')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('max_discount_percentage');
                });
            }
        }
    }
}
