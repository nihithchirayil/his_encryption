<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersAddIsOtUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'is_ot_user')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->smallInteger('is_ot_user')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'is_ot_user')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('is_ot_user');
                });
            }
        }
    }
}
