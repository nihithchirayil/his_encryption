<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGynecologyObstreticsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('gynecology_obstretics_history')) {
            Schema::create('gynecology_obstretics_history', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->Integer('doctor_id')->nullable();
                $table->smallInteger('delevery_type1')->nullable();
                $table->smallInteger('gender1')->nullable();
                $table->string('birth_weight1',250)->nullable();
                $table->Integer('age1')->nullable();
                $table->string('place_of_birth1',250)->nullable();
                $table->string('any_complications1',250)->nullable();
                $table->smallInteger('delevery_type2')->nullable();
                $table->smallInteger('gender2')->nullable();
                $table->string('birth_weight2',250)->nullable();
                $table->Integer('age2')->nullable();
                $table->string('place_of_birth2',250)->nullable();
                $table->string('any_complications2',250)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->smallInteger('gender1_old')->nullable()->default(0);
                $table->smallInteger('gender2_old')->nullable()->default(0);
                $table->text('additional_child_information')->nullable();
                $table->string('indication1',250)->nullable();
                $table->string('indication2',250)->nullable();
                $table->text('notes')->nullable();
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gynecology_obstretics_history');
    }
}
