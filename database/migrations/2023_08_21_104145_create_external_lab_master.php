<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalLabMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('external_lab_master')) {
            Schema::create('external_lab_master', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',250)->nullable();
                $table->text('address')->nullable();
                $table->string('email',25)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
                $table->smallInteger('status')->nullable();
                $table->string('phone',250)->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_lab_master');
    }
}
