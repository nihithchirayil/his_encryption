<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataPointToTemplateControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('template_control')) {
            if (!Schema::hasColumn('template_control', 'data_point')) {
                Schema::table('template_control', function (Blueprint $table) {
                    $table->string('data_point')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_control', function (Blueprint $table) {
            $table->dropColumn('data_point');
        });
    }
}
