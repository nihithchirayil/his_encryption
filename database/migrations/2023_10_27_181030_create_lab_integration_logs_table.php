<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabIntegrationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('lab_integration_logs')) {
            Schema::create('lab_integration_logs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('sample_id')->nullable();
                $table->string('equipment_id', 200)->nullable();
                $table->string('request')->nullable();
                $table->string('response')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_integration_logs');
    }
}
