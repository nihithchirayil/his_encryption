<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_master')) {
            Schema::create('surgery_master', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',1000)->nullable();
                $table->string('service_code',1000)->nullable();
                $table->string('approximate_amount',1000)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_master')) {


            if (!Schema::hasColumn('surgery_master', 'name')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->string('name',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_master', 'service_code')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->string('service_code',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_master', 'approximate_amount')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->string('approximate_amount',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_master', 'created_by')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_master', 'updated_by')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_master', 'deleted_by')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_master', 'created_at')) {
                Schema::table('surgery_master', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_master');
    }
}
