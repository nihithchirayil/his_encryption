<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAllergicHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_master')) {
            if (!Schema::hasColumn('patient_master', 'allergic_history')) {
                Schema::table('patient_master', function (Blueprint $table) {
                    $table->text('allergic_history')->nullable();
                });
            }
        }
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_master')) {
            if (Schema::hasColumn('patient_master', 'allergic_history')) {
                Schema::table('patient_master', function (Blueprint $table) {
                    $table->dropColumn('allergic_history');
                });
            }
        }    
    }
}
