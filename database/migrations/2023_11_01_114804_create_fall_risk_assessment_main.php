<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFallRiskAssessmentMain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('fall_risk_assessment_main')) {
            Schema::create('fall_risk_assessment_main', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('doctor_id')->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->integer('history_of_falling')->nullable();
                $table->integer('secondary_diagnosis')->nullable();
                $table->integer('ambulatory_aid')->nullable();
                $table->integer('iv_heparin_lock')->nullable();
                $table->integer('gait_transferring')->nullable();
                $table->integer('mental_status')->nullable();
                $table->integer('mfs_value')->nullable();
                $table->string('risk_level')->nullable();
                $table->string('risk_action')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fall_risk_assessment_main');
    }
}
