<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryBillDivision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_bill_division')) {
            Schema::create('surgery_bill_division', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('surgery_division_id')->nullable();
                $table->string('percentage',500)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_bill_division')) {
            if (!Schema::hasColumn('surgery_bill_division', 'surgery_id')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_bill_division', 'surgery_division_id')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->bigInteger('surgery_division_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_bill_division', 'percentage')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->string('percentage',500)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_bill_division', 'created_by')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_bill_division', 'updated_by')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_bill_division', 'deleted_by')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_bill_division', 'created_at')) {
                Schema::table('surgery_bill_division', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_bill_division');
    }
}
