<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterServiceDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_investigation_detail')) {
            if (!Schema::hasColumn('patient_investigation_detail', 'service_date')) {
                Schema::table('patient_investigation_detail', function (Blueprint $table) {
                    $table->date('service_date')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_investigation_detail')) {
            Schema::table('patient_investigation_detail', function (Blueprint $table) {
                $table->dropColumn('service_date');
            });
        }    
    }
}
