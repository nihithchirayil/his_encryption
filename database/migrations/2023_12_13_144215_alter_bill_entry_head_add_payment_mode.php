<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillEntryHeadAddPaymentMode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_entry_head')) {
            if (!Schema::hasColumn('bill_entry_head', 'payment_mode')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->string('payment_mode')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_entry_head', 'bank')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->string('bank')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_entry_head', 'card_no')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->string('card_no')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_entry_head', 'payment_phone_no')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->string('payment_phone_no')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_entry_head', 'exp_month')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->string('exp_month')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_entry_head', 'exp_year')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->string('exp_year')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
