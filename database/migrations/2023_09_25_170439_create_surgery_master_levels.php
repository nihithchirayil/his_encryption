<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryMasterLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_level_master')) {
            Schema::create('surgery_level_master', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('level_id')->nullable();
                $table->decimal('surgeon_amount')->nullable();
                $table->smallInteger('is_editable')->nullable()->default(0);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_level_master');
    }
}
