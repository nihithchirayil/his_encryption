<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewMenuForListingFreeStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $route = 'extensionsvalley.free_stock.listFreeStock';
        if (!\DB::table('menu')->where('route_name', '=', $route)->where('status', 1)->exists()) {

            $parent_id = \DB::table('menu')->where('name', '=', 'Purchase')->value('id');
            
            $menu = [
                'name' => 'Free Stock Entry List',
                'parent_id'   => $parent_id,
                'route_name'  => $route,
                'menu_level'  => 1,
                'is_desktop_menu' => 0,
                'status'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s'),
            ];
            \DB::table('menu')->insert($menu);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
