<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillHeadAddColoumIsVisitMerged extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'is_visit_merged')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->smallInteger('is_visit_merged')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_head', 'previous_visit_id')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->bigInteger('previous_visit_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'is_visit_merged')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('is_visit_merged');
                });
            }
            if (Schema::hasColumn('bill_head', 'previous_visit_id')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('previous_visit_id');
                });
            }
        }
    }
}
