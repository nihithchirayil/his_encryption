<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientClinicalDataHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('patient_clinical_data_history')) {
            Schema::create('patient_clinical_data_history', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->text('allergic_history')->nullable();
                $table->text('obstretic_history')->nullable();
                $table->text('medical_and_surgical_history')->nullable();
                $table->text('nurtitional_and_screening')->nullable();
                $table->text('family_history')->nullable();
                $table->text('menstrual_history')->nullable();
                $table->text('personal_history')->nullable();
                $table->text('chief_complaints')->nullable();
                $table->text('history_of_present_illness')->nullable();
                $table->text('general_examinations')->nullable();
                $table->text('systematic_examinations')->nullable();
                $table->text('diagnosis')->nullable();
                $table->text('treatment_plan_management')->nullable();
                $table->text('chronic_illness_history')->nullable();
                $table->text('investigation_history')->nullable();
                $table->text('patient_medical_and_surgical_history')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
            });
        }
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_clinical_data_history');
    }
}
