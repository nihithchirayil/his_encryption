<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePatientRefractionAddColoumGlassPrescriptionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_refraction')) {
            if (!Schema::hasColumn('patient_refraction', 'glass_prescription_id')) {
                Schema::table('patient_refraction', function (Blueprint $table) {
                    $table->bigInteger('glass_prescription_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_refraction')) {
            if (Schema::hasColumn('patient_refraction', 'glass_prescription_id')) {
                Schema::table('patient_refraction', function (Blueprint $table) {
                    $table->dropColumn('glass_prescription_id');
                });
            }
        }
    }
}
