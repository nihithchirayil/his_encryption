<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterIsemergencyPostAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_entry_head')) {
            if (!Schema::hasColumn('bill_entry_head', 'is_emergency_post_accounts')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->smallInteger('is_emergency_post_accounts')->default(0);
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_entry_head')) {
            if (!Schema::hasColumn('bill_entry_head', 'is_emergency_post_accounts')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->dropColumn('is_emergency_post_accounts');
                });
            }
        }    
    }
}
