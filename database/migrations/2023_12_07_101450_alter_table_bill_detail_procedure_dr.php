<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillDetailProcedureDr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_detail')) {
            if (!Schema::hasColumn('bill_detail', 'procedure_dr')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->Integer('procedure_dr')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_detail')) {
            if (Schema::hasColumn('bill_detail', 'procedure_dr')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('procedure_dr');
                });
            }
        }
    }
}
