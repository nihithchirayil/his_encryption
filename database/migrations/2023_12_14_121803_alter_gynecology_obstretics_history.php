<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterGynecologyObstreticsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $company_code = \DB::table('company')->where('id', 1)->value('code');
        if ($company_code != 'ROHINI') {
            if (Schema::hasTable('gynecology_obstretics_history')) {
                if (Schema::hasColumn('gynecology_obstretics_history', 'created_by')) {
                    Schema::table('gynecology_obstretics_history', function (Blueprint $table) {
                        $table->dropColumn('created_by');
                    });
                }
                if (Schema::hasColumn('gynecology_obstretics_history', 'updated_by')) {
                    Schema::table('gynecology_obstretics_history', function (Blueprint $table) {
                        $table->dropColumn('updated_by');
                    });
                }
                if (Schema::hasColumn('gynecology_obstretics_history', 'deleted_by')) {
                    Schema::table('gynecology_obstretics_history', function (Blueprint $table) {
                        $table->dropColumn('deleted_by');
                    });
                }
                Schema::table('gynecology_obstretics_history', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                    $table->bigInteger('updated_by')->nullable();
                    $table->bigInteger('deleted_by')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
