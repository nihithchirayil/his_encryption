<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryRequestAnaesthetist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_request_anaesthetist')) {
            Schema::create('surgery_request_anaesthetist', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('request_detail_id')->nullable();
                $table->bigInteger('anaesthetist_id')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_request_anaesthetist')) {


            if (!Schema::hasColumn('surgery_request_anaesthetist', 'request_detail_id')) {
                Schema::table('surgery_request_anaesthetist', function (Blueprint $table) {
                    $table->bigInteger('request_detail_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_anaesthetist', 'anaesthetist_id')) {
                Schema::table('surgery_request_anaesthetist', function (Blueprint $table) {
                    $table->bigInteger('anaesthetist_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_anaesthetist', 'created_by')) {
                Schema::table('surgery_request_anaesthetist', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_anaesthetist', 'updated_by')) {
                Schema::table('surgery_request_anaesthetist', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_anaesthetist', 'deleted_by')) {
                Schema::table('surgery_request_anaesthetist', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_anaesthetist', 'created_at')) {
                Schema::table('surgery_request_anaesthetist', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_request_anaesthetist');
    }
}
