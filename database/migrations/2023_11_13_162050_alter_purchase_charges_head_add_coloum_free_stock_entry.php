<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPurchaseChargesHeadAddColoumFreeStockEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('purchase_charges_head')) {
            if (!Schema::hasColumn('purchase_charges_head', 'free_stock_entry')) {
                Schema::table('purchase_charges_head', function (Blueprint $table) {
                    $table->smallInteger('free_stock_entry')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('purchase_charges_head')) {
            if (!Schema::hasColumn('purchase_charges_head', 'free_stock_entry')) {
                Schema::table('purchase_charges_head', function (Blueprint $table) {
                    $table->dropColumn('free_stock_entry');
                });
            }
        }
    }
}
