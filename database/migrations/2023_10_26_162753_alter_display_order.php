<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDisplayOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fav_group_items')) {
            if (!Schema::hasColumn('fav_group_items', 'display_order')) {
                Schema::table('fav_group_items', function (Blueprint $table) {
                    $table->smallInteger('display_order')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('fav_group_items')) {
            if (Schema::hasColumn('fav_group_items', 'display_order')) {
                Schema::table('fav_group_items', function (Blueprint $table) {
                    $table->dropColumn('display_order');
                });
            }
        }      
    }
}
