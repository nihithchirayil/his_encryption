<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRequestLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_request_logs')) {
            Schema::create('user_request_logs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id')->nullable();
                $table->string('route_name', 250)->nullable();
                $table->string('uri', 250)->nullable();
                $table->text('request_data')->nullable();
                $table->string('app_name', 100)->nullable();
                $table->string('ip_address', 500)->nullable();
                $table->integer('created_by')->default(0);
                $table->integer('updated_by')->default(0);
                $table->integer('deleted_by')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_request_logs');
    }
}
