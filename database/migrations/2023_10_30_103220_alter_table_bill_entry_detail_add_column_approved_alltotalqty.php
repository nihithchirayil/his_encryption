<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillEntryDetailAddColumnApprovedAlltotalqty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_entry_detail')) {
            if (!Schema::hasColumn('bill_entry_detail', 'approved_all_total_qty')) {
                Schema::table('bill_entry_detail', function (Blueprint $table) {
                    $table->decimal('approved_all_total_qty', 12, 3)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_entry_detail')) {
            if (Schema::hasColumn('bill_entry_detail', 'approved_all_total_qty')) {
                Schema::table('bill_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('approved_all_total_qty');
                });
            }
        }
    }
}
