<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeletedYellowsheetServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('deleted_yellow_sheet_services')) {
            Schema::create('deleted_yellow_sheet_services', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('service_id')->default(0);
                $table->integer('yellow_sheet_id')->default(0);
                $table->integer('visit_id')->default(0);
                $table->integer('doctor_id')->default(0);
                $table->integer('patient_id')->default(0);
                $table->integer('ip_procedure_id')->default(0);
                $table->timestamp('service_datetime')->nullable();
                $table->decimal('quantity')->default(0);
                $table->decimal('total_charge',)->default(0);
                $table->integer('created_by')->default(0);
                $table->integer('updated_by')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_yellow_sheet_services');
    }
}
