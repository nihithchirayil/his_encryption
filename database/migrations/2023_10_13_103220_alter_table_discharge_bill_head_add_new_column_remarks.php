<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDischargeBillHeadAddNewColumnRemarks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discharge_bill_head')) {
            if (!Schema::hasColumn('discharge_bill_head', 'user_remarks')) {
                Schema::table('discharge_bill_head', function (Blueprint $table) {
                    $table->text('user_remarks')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_bill_head', 'dis_req_status')) {
                Schema::table('discharge_bill_head', function (Blueprint $table) {
                    $table->smallInteger('dis_req_status')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('discharge_bill_head')) {
            if (Schema::hasColumn('discharge_bill_head', 'user_remarks')) {
                Schema::table('discharge_bill_head', function (Blueprint $table) {
                    $table->dropColumn('user_remarks');
                });
            }
            if (Schema::hasColumn('discharge_bill_head', 'dis_req_status')) {
                Schema::table('discharge_bill_head', function (Blueprint $table) {
                    $table->dropColumn('dis_req_status');
                });
            }
        }
    }
}
