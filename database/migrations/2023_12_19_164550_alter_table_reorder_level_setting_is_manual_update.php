<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableReorderLevelSettingIsManualUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reorder_level_setting')) {
            if (!Schema::hasColumn('reorder_level_setting', 'is_manual_update')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->integer('is_manual_update')->nullable();
                });
            }
            if (!Schema::hasColumn('reorder_level_setting', 'created_by')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->integer('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('reorder_level_setting', 'updated_by')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->integer('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('reorder_level_setting', 'created_at')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->timestamp('created_at')->nullable();
                });
            }
            if (!Schema::hasColumn('reorder_level_setting', 'updated_at')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->timestamp('updated_at')->nullable();
                });
            }


        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reorder_level_setting')) {
            if (Schema::hasColumn('reorder_level_setting', 'is_manual_update')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->dropColumn('is_manual_update');
                });
            }
            if (Schema::hasColumn('reorder_level_setting', 'created_by')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->dropColumn('created_by');
                });
            }
            if (Schema::hasColumn('reorder_level_setting', 'updated_by')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->dropColumn('updated_by');
                });
            }
            if (Schema::hasColumn('reorder_level_setting', 'created_at')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->dropColumn('created_at');
                });
            }
            if (Schema::hasColumn('reorder_level_setting', 'updated_at')) {
                Schema::table('reorder_level_setting', function (Blueprint $table) {
                    $table->dropColumn('updated_at');
                });
            }
        }
    }
}
