<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryChecklistDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_checklist_detail')) {
            Schema::create('surgery_checklist_detail', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('surgery_request_id')->nullable();
                $table->json('checklist_json')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_checklist_detail')) {


            if (!Schema::hasColumn('surgery_checklist_detail', 'surgery_id')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist_detail', 'surgery_request_id')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->bigInteger('surgery_request_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist_detail', 'checklist_json')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->json('checklist_json')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_checklist_detail', 'created_by')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist_detail', 'updated_by')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist_detail', 'deleted_by')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_checklist_detail', 'created_at')) {
                Schema::table('surgery_checklist_detail', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_checklist_detail');
    }
}
