<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryAnesthesia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_anesthesia')) {
            Schema::create('surgery_anesthesia', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name', 250)->nullable();
                $table->smallInteger('status')->nullable()->default(1);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_anesthesia')) {
            if (!Schema::hasColumn('surgery_anesthesia', 'name')) {
                Schema::table('surgery_anesthesia', function (Blueprint $table) {
                    $table->string('name', 250)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_anesthesia', 'status')) {
                Schema::table('surgery_anesthesia', function (Blueprint $table) {
                    $table->smallInteger('status')->nullable()->default(1);
                });
            }
            if (!Schema::hasColumn('surgery_anesthesia', 'created_by')) {
                Schema::table('surgery_anesthesia', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_anesthesia', 'updated_by')) {
                Schema::table('surgery_anesthesia', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_anesthesia', 'deleted_by')) {
                Schema::table('surgery_anesthesia', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_anesthesia', 'created_at')) {
                Schema::table('surgery_anesthesia', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_anesthesia');
    }
}
