<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataPointToTemplateControlMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('template_control_mapping')) {
            if (!Schema::hasColumn('template_control_mapping', 'data_point')) {
                Schema::table('template_control_mapping', function (Blueprint $table) {
                    $table->string('data_point')->nullable();
                });
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_control_mapping', function (Blueprint $table) {
            $table->dropColumn('data_point');
        });
    }
}
