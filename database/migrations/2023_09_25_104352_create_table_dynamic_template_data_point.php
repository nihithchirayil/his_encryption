<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDynamicTemplateDataPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('dynamic_template_data_point_master')) {
            Schema::create('dynamic_template_data_point_master', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name', 150)->nullable();
                $table->string('code', 25)->nullable();
                $table->smallInteger('priority')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });

            if (!Schema::hasColumn('dynamic_template_data_point_master', 'priority'))
            {
                \DB::statement("COMMENT ON COLUMN dynamic_template_data_point_master.priority IS0-patient wise,1-visit wise");
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_template_data_point_master');
    }
}
