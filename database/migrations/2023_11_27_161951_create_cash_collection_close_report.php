<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashCollectionCloseReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cash_collection_close_report')) {
            Schema::create('cash_collection_close_report', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('bill_date')->nullable();
                $table->decimal('amount', 15, 2)->default(0);
                $table->decimal('net_amount', 15, 2)->default(0);
                $table->bigInteger('collected_user')->nullable();
                $table->smallInteger('status')->default(1);
                $table->decimal('diff_amount', 15, 2)->default(0);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_collection_close_report');
    }
}
