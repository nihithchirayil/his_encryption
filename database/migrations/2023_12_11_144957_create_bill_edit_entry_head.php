<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillEditEntryHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bill_edit_entry_head')) {
            Schema::create('bill_edit_entry_head', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('json_log_id')->default(0);
                $table->string('bill_no', 50)->nullable();
                $table->string('bill_tag', 20)->nullable();
                $table->decimal('bill_amount', 15, 2)->default(0);
                $table->decimal('net_amount_wo_roundoff', 15, 2)->default(0);
                $table->bigInteger('admitting_doctor_id')->nullable();
                $table->bigInteger('consulting_doctor_id')->nullable();
                $table->string('payment_type')->nullable();
                $table->integer('discount_type')->nullable();
                $table->decimal('discount', 15, 2)->default(0);
                $table->text('discount_reason')->nullable();
                $table->decimal('discount_amount', 15, 2)->default(0);
                $table->decimal('pricing_discount_amount', 15, 2)->default(0);
                $table->integer('function_type')->default(0);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });

            \DB::statement("COMMENT ON COLUMN bill_edit_entry_head.function_type IS '1-Created Bill, 2-Edited Bill'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_edit_entry_head');
    }
}
