<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportDataExcel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('report_data_excel')) {
            Schema::create('report_data_excel', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('report_name',250)->nullable();
                $table->string('execution_time',500)->nullable();
                $table->text('report_filters')->nullable();
                $table->text('report_sql')->nullable();
                $table->string('generated_excel',300)->nullable();
                $table->smallInteger('run_status')->nullable()->comment('1-created,2-process,3-completed success,4-completed fail');
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_data_excel');
    }
}
