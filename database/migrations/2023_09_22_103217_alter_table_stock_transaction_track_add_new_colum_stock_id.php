<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableStockTransactionTrackAddNewColumStockId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('stock_transaction_track')) {
            if (!Schema::hasColumn('stock_transaction_track', 'product_stock_id')) {
                Schema::table('stock_transaction_track', function (Blueprint $table) {
                    $table->bigInteger('product_stock_id')->nullable();
                });
            }
        }  

        if (Schema::hasTable('free_stock_entry_detail')) {
            if (!Schema::hasColumn('free_stock_entry_detail', 'stock_transaction_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->bigInteger('stock_transaction_id')->nullable();
                });
            }

            if (!Schema::hasColumn('free_stock_entry_detail', 'product_stock_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->bigInteger('product_stock_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('stock_transaction_track')) {
            if (Schema::hasColumn('stock_transaction_track', 'product_stock_id')) {
                Schema::table('stock_transaction_track', function (Blueprint $table) {
                    $table->dropColumn('product_stock_id');
                });
            }
        } 

        if (Schema::hasTable('free_stock_entry_detail')) {
            if (Schema::hasColumn('free_stock_entry_detail', 'stock_transaction_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('stock_transaction_id');
                });
            }
            if (Schema::hasColumn('free_stock_entry_detail', 'product_stock_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('product_stock_id');
                });
            }
        } 
    }
}
