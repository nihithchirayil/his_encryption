<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOtChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('operation_checklist_details')) {
            Schema::create('operation_checklist_details', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->default(0)->notNullable();
                $table->bigInteger('surgery_request_id')->default(0)->notNullable();
                $table->json('operation_details_json')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->default(0)->notNullable();
                $table->bigInteger('updated_by')->default(0)->notNullable();
                $table->bigInteger('deleted_by')->default(0)->notNullable();
                $table->smallInteger('is_finalized')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_checklist_details');
    }
}
