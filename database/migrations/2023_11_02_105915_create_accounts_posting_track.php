<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsPostingTrack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('accounts_posting_track')) {
            Schema::create('accounts_posting_track', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('voucher_type', 25)->nullable();
                $table->date('account_date')->nullable();
                $table->string('comments', 500)->nullable();
                $table->integer('created_by')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_posting_track');
    }
}
