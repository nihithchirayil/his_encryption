<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosDeviceJsonLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pos_device_json_log')) {     
            Schema::create('pos_device_json_log', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->string('visit_id',500)->nullable();
                $table->string('bill_head_id',500)->nullable();
                $table->string('bill_tag',500)->nullable();
                $table->string('povider_api', 100)->nullable();
                $table->string('api_call_location', 100)->nullable();
                $table->text('response_string')->nullable();
                $table->string('response_code',500)->nullable();
                $table->string('device_brand',500)->nullable();
                $table->string('deviceid',500)->nullable();
                $table->integer('status')->nullable();
                $table->string('application_name',30)->nullable();
                $table->string('pc_name',100)->nullable();
                $table->integer('unit_id')->nullable();
                $table->string('ip_address',100)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }
    }   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_device_json_log');
    }
}
