<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAllvisitsCamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('all_visits')) {
            if (!Schema::hasColumn('all_visits', 'camp')) {
                Schema::table('all_visits', function (Blueprint $table) {
                    $table->smallInteger('camp')->nullable()->default(0);
                });
            }
        }     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('all_visits')) {
            if (Schema::hasColumn('all_visits', 'camp')) {
                Schema::table('all_visits', function (Blueprint $table) {
                    $table->dropColumn('camp');
                });
            }
        }    
    }
}
