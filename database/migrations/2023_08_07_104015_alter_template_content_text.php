<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTemplateContentText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'template_conent_text')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('template_conent_text')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (Schema::hasColumn('discharge_summary', 'template_conent_text')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('template_conent_text');
                });
            }
        }    
    }
}
