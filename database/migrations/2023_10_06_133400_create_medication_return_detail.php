<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicationReturnDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('medication_return_detail')) {
            Schema::create('medication_return_detail', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('medication_return_head_id');
                $table->bigInteger('item_id');
                $table->bigInteger('bill_detail_id');
                $table->bigInteger('bill_head_id');
                $table->decimal('requested_qty', 15, 2);
                $table->decimal('returned_qty', 15, 2)->nullable()->default(0);
                $table->bigInteger('patient_medication_detail_id');
                $table->smallInteger('status')->default(0);
                $table->bigInteger('created_by');
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });

            \DB::statement("COMMENT ON COLUMN medication_return_detail.status IS '0-Requested, 1-Converted, 2-Partially Converted'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medication_return_detail');
    }
}
