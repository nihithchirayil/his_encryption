<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmrComplaintsEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('emr_complaints_entry')) {

            Schema::create('emr_complaints_entry', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->integer('doctor_id')->nullable();
                $table->string('complaint',500)->nullable();
                $table->string('duration',250)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
                $table->string('mig_vsit_notlist')->nullable();
                $table->text('hpi')->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emr_complaints_entry');
    }
}
