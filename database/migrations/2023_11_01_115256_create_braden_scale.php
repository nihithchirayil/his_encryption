<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBradenScale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('braden_scale')) {
            Schema::create('braden_scale', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('doctor_id')->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->integer('sensory_perception')->nullable();
                $table->integer('moisture')->nullable();
                $table->integer('activity')->nullable();
                $table->integer('mobility')->nullable();
                $table->integer('nutrition')->nullable();
                $table->integer('friction_shear')->nullable();
                $table->integer('total_score')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('braden_scale');
    }
}
