<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicationReturnHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('medication_return_head')) {
            Schema::create('medication_return_head', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id');
                $table->bigInteger('visit_id');
                $table->string('return_requested_ns_location', 150);
                $table->integer('room_number')->nullable();
                $table->smallInteger('status')->default(0);
                $table->string('remarks', 250)->nullable();
                $table->string('cancel_remarks', 250)->nullable();
                $table->bigInteger('cancelled_by')->nullable();
                $table->timestamp('cancelled_at')->nullable();
                $table->bigInteger('created_by');
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });

            \DB::statement("COMMENT ON COLUMN medication_return_head.status IS '0-Requested, 1-Converted, 2-Partially Converted'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medication_return_head');
    }
}
