<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePatientMedicationDetailAddColoumIsReplaced extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_medication_detail')) {
            if (!Schema::hasColumn('patient_medication_detail', 'is_replaced')) {
                Schema::table('patient_medication_detail', function (Blueprint $table) {
                    $table->smallInteger('is_replaced')->nullable();
                });
            }
            if (!Schema::hasColumn('patient_medication_detail', 'replaced_medicine_code')) {
                Schema::table('patient_medication_detail', function (Blueprint $table) {
                    $table->string('replaced_medicine_code')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_medication_detail')) {
            if (Schema::hasColumn('patient_medication_detail', 'is_replaced')) {
                Schema::table('patient_medication_detail', function (Blueprint $table) {
                    $table->dropColumn('is_replaced');
                });
            }
            if (Schema::hasColumn('patient_medication_detail', 'replaced_medicine_code')) {
                Schema::table('patient_medication_detail', function (Blueprint $table) {
                    $table->dropColumn('replaced_medicine_code');
                });
            }
        }
    }
}
