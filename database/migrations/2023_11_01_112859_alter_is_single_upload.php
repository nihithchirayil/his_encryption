<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterIsSingleUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lab_sample_details')) {
            if (!Schema::hasColumn('lab_sample_details', 'is_single_upload')) {
                Schema::table('lab_sample_details', function (Blueprint $table) {
                    $table->smallInteger('is_single_upload')->nullable()->default(0);
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('lab_sample_details')) {
            if (Schema::hasColumn('lab_sample_details', 'is_single_upload')) {
                Schema::table('lab_sample_details', function (Blueprint $table) {
                    $table->dropColumn('is_single_upload');
                });
            }
        }    
    }
}
