<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillEntryHeadAddColumInsertFrom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_entry_head')) {
            if (!Schema::hasColumn('bill_entry_head', 'insert_from')) {
                Schema::table('bill_entry_head', function (Blueprint $table) {
                    $table->smallInteger('insert_from')->default(1);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_entry_head')) {
            Schema::table('bill_entry_head', function (Blueprint $table) {
                $table->dropColumn('insert_from');
            });
        }
    }
}
