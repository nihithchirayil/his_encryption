<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSomeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hourly_yellow_sheet_billing')) {     
            Schema::create('hourly_yellow_sheet_billing', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('service_id')->nullable();
                $table->integer('quantity')->nullable();
                $table->integer('total_charge')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('procedure_types')) {     
            Schema::create('procedure_types', function (Blueprint $table2) {
                $table2->bigIncrements('id');
                $table2->string('name',500)->nullable();
                $table2->smallInteger('status')->nullable();
                $table2->bigInteger('created_by')->nullable();
                $table2->bigInteger('updated_by')->nullable();
                $table2->bigInteger('deleted_by')->nullable();
                $table2->timestamps();
            });
        }

        if (Schema::hasTable('yellow_sheet_service')) {
            if (!Schema::hasColumn('yellow_sheet_service', 'is_hourly')) {
                Schema::table('yellow_sheet_service', function (Blueprint $table) {
                    $table->smallInteger('is_hourly')->default(0);
                });
            }
            if (!Schema::hasColumn('yellow_sheet_service', 'procedure_type_id')) {
                Schema::table('yellow_sheet_service', function (Blueprint $table) {
                    $table->integer('procedure_type_id')->nullable()->default(0);
                });
            }
        }

        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'default_procedure_type')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->integer('default_procedure_type')->nullable();
                });
            }
        }

        if (Schema::hasTable('billing_yellow_sheet_service')) {
            if (!Schema::hasColumn('billing_yellow_sheet_service', 'quantity')) {
                Schema::table('billing_yellow_sheet_service', function (Blueprint $table) {
                    $table->decimal('quantity',15,2)->nullable();
                });
            }
            if (!Schema::hasColumn('billing_yellow_sheet_service', 'total_charge')) {
                Schema::table('billing_yellow_sheet_service', function (Blueprint $table) {
                    $table->decimal('total_charge',15,2)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hourly_yellow_sheet_billing');
        Schema::dropIfExists('procedure_types');

        if (Schema::hasTable('yellow_sheet_service')) {
            if (Schema::hasColumn('yellow_sheet_service', 'is_hourly')) {
                Schema::table('yellow_sheet_service', function (Blueprint $table) {
                    $table->dropColumn('is_hourly');
                });
            }
            if (Schema::hasColumn('yellow_sheet_service', 'procedure_type_id')) {
                Schema::table('yellow_sheet_service', function (Blueprint $table) {
                    $table->dropColumn('procedure_type_id');
                });
            }
        }

        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'default_procedure_type')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('default_procedure_type');
                });
            }
        }

        if (Schema::hasTable('billing_yellow_sheet_service')) {
            if (Schema::hasColumn('billing_yellow_sheet_service', 'quantity')) {
                Schema::table('billing_yellow_sheet_service', function (Blueprint $table) {
                    $table->dropColumn('quantity');
                });
            }
            if (Schema::hasColumn('billing_yellow_sheet_service', 'total_charge')) {
                Schema::table('billing_yellow_sheet_service', function (Blueprint $table) {
                    $table->dropColumn('total_charge');
                });
            }
        }
    }
}
