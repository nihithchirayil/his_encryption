<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMenuAddNewColumnWorkflowAccessMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('menu')) {
            if (!Schema::hasColumn('menu', 'work_flow_access')) {
                Schema::table('menu', function (Blueprint $table) {
                    $table->smallInteger('work_flow_access')->nullable()->default(0);
                });
            }
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('menu')) {
            if (Schema::hasColumn('menu', 'work_flow_access')) {
                Schema::table('menu', function (Blueprint $table) {
                    $table->dropColumn('work_flow_access');
                });
            }
        }    
    }
}
