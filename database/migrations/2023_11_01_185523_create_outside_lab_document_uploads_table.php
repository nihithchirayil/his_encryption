<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutsideLabDocumentUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('outside_lab_document_uploads')) {
            Schema::create('outside_lab_document_uploads', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('sample_no',500)->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->string('file_name',500)->nullable();
                $table->string('original_name',500)->nullable();
                $table->string('display_name',500)->nullable();
                $table->string('extension',500)->nullable();
                $table->string('file_size',500)->nullable();
                $table->bigInteger('type')->nullable()->comment("1='LAB',2='OTHER'");;
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outside_lab_document_uploads');
    }
}
