<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryAnaesthesiaRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_anaesthesia_record')) {
            Schema::create('surgery_anaesthesia_record', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id');
                $table->bigInteger('surgery_head_id');
                $table->bigInteger('surgery_detail_id');
                $table->text('pre_anaesthetic_data')->nullable();
                $table->text('anasthesia_record_data')->nullable();
                $table->text('post_op_assessment_data')->nullable();
                $table->text('anaesthesia_cheklist_data')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_anaesthesia_record')) {
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'patient_id')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->bigInteger('patient_id');
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'surgery_head_id')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->bigInteger('surgery_head_id');
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'surgery_detail_id')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->bigInteger('surgery_detail_id');
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'pre_anaesthetic_data')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->text('pre_anaesthetic_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'anasthesia_record_data')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->text('anasthesia_record_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'post_op_assessment_data')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->text('post_op_assessment_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'anaesthesia_cheklist_data')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->text('anaesthesia_cheklist_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'created_by')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'updated_by')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_anaesthesia_record', 'deleted_by')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_anaesthesia_record', 'created_at')) {
                Schema::table('surgery_anaesthesia_record', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_anaesthesia_record');
    }
}
