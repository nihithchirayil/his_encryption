<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryInstruments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_instruments')) {
            Schema::create('surgery_instruments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable()->default(0);
                $table->string('service_code',1000)->nullable();
                $table->string('amount',1000)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_instruments')) {


            if (!Schema::hasColumn('surgery_instruments', 'surgery_id')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_instruments', 'service_code')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->string('service_code',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_instruments', 'amount')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->string('amount',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_instruments', 'created_by')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_instruments', 'updated_by')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_instruments', 'deleted_by')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_instruments', 'created_at')) {
                Schema::table('surgery_instruments', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_instruments');
    }
}
