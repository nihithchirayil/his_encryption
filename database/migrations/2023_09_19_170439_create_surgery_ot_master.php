<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryOtMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_ot_master')) {
            Schema::create('surgery_ot_master', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('ot_name',1000)->nullable();
                $table->time('start_time')->nullable();
                $table->time('end_time')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_ot_master')) {


            if (!Schema::hasColumn('surgery_ot_master', 'ot_name')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->bigInteger('ot_name')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_master', 'start_time')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->timestamp('start_time')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_master', 'end_time')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->timestamp('end_time')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_master', 'created_by')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_master', 'updated_by')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_master', 'deleted_by')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_ot_master', 'created_at')) {
                Schema::table('surgery_ot_master', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_ot_master');
    }
}
