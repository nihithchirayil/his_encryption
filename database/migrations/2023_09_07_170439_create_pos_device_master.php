<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosDeviceMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pos_device_master')) {     
            Schema::create('pos_device_master', function (Blueprint $table) {
                $table->bigIncrements('deviceid');
                $table->string('device_code',500)->nullable()->unique();
                $table->string('device_name',500)->nullable();
                $table->integer('device_brand')->nullable();
                $table->integer('device_bank')->nullable();
                $table->string('terminal_id',500)->nullable();
                $table->string('serail_number',500)->nullable();
                $table->integer('connection_type')->nullable();
                $table->string('ip_address',500)->nullable();
                $table->integer('port')->nullable();
                $table->string('device_location',500)->nullable();
                $table->string('default_counter',500)->nullable();
                $table->string('application_name',500)->nullable();
                $table->integer('company_id')->nullable();
                $table->integer('branch_id')->nullable();
                $table->integer('is_active')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_device_master');
    }
}
