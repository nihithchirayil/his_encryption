<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMachineAssayCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lab_integration_log_detail')) {
            if (!Schema::hasColumn('lab_integration_log_detail', 'machine_assay_code')) {
                Schema::table('lab_integration_log_detail', function (Blueprint $table) {
                    $table->string('machine_assay_code')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('lab_integration_log_detail')) {
            if (Schema::hasColumn('lab_integration_log_detail', 'machine_assay_code')) {
                Schema::table('lab_integration_log_detail', function (Blueprint $table) {
                    $table->dropColumn('machine_assay_code');
                });
            }
        }    
    }
}
