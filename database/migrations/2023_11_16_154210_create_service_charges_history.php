<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceChargesHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('service_charges_history')) {
            Schema::create('service_charges_history', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('service_id')->nullable();
                $table->integer('roomtype_id')->nullable();
                $table->integer('mode')->nullable()->comment("1.'PERCENTAGE',2.'AMOUNT'");
                $table->integer('type')->nullable()->comment("1.'INCREMENT',2'DECREMENT'");
                $table->integer('value')->nullable();
                $table->decimal('difference')->default(0);
                $table->decimal('new_price')->default(0);
                $table->decimal('old_price')->default(0);
                $table->string('application_name', 100)->nullable();
                $table->timestamp('effective_date')->nullable();
                $table->smallInteger('status')->default(1);
                $table->integer('created_by')->default(0);
                $table->integer('updated_by')->default(0);
                $table->integer('service_charges_id')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_charges_history');
    }
}
