<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFavGroupMasterAddNursingStation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fav_group_master')) {
            if (!Schema::hasColumn('fav_group_master', 'nursing_station')) {
                Schema::table('fav_group_master', function (Blueprint $table) {
                    $table->Integer('nursing_station')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('fav_group_master')) {
            if (Schema::hasColumn('fav_group_master', 'nursing_station')) {
                Schema::table('fav_group_master', function (Blueprint $table) {
                    $table->dropColumn('nursing_station');
                });
            }
        }
    }
}
