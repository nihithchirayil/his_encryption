<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryConsumables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_consumables')) {
            Schema::create('surgery_consumables', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->string('item_code',1000)->nullable();
                $table->decimal('quantity')->nullable();
                $table->string('amount',1000)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_consumables')) {


            if (!Schema::hasColumn('surgery_consumables', 'surgery_id')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_consumables', 'item_code')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->string('item_code',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_consumables', 'quantity')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->decimal('quantity')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_consumables', 'amount')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->string('amount',1000)->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_consumables', 'created_by')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_consumables', 'updated_by')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_consumables', 'deleted_by')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_consumables', 'created_at')) {
                Schema::table('surgery_consumables', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_consumables');
    }
}
