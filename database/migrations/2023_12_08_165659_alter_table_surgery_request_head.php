<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSurgeryRequestHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('surgery_request_head')) {
            if (!Schema::hasColumn('surgery_request_head', 'actual_start_time')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->time('actual_start_time')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'actual_end_time')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->time('actual_end_time')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
