<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_detail')) {
            if (!Schema::hasColumn('bill_detail', 'intend_location_code')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->string('intend_location_code')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_detail', 'is_ward_stock')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->smallInteger('is_ward_stock')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_detail', 'pricing_discount_percentage')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->decimal('pricing_discount_percentage',15,2)->nullable();
                });
            }
            if (!Schema::hasColumn('bill_detail', 'pricing_disc_type')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->smallInteger('pricing_disc_type')->nullable();
                });
            }
            if (!Schema::hasColumn('bill_detail', 'sales_cost')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->decimal('sales_cost')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_detail')) {
            if (Schema::hasColumn('bill_detail', 'intend_location_code')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('intend_location_code');
                });
            }
            if (Schema::hasColumn('bill_detail', 'is_ward_stock')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('is_ward_stock');
                });
            }
            if (Schema::hasColumn('bill_detail', 'pricing_discount_percentage')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('pricing_discount_percentage');
                });
            }
            if (Schema::hasColumn('bill_detail', 'pricing_disc_type')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('pricing_disc_type');
                });
            }
            if (Schema::hasColumn('bill_detail', 'sales_cost')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('sales_cost');
                });
            }
        }
    }
}
