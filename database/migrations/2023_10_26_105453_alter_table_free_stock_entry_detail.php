<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableFreeStockEntryDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('free_stock_entry_detail')) {
            if (!Schema::hasColumn('free_stock_entry_detail', 'unit_tax_amount')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('unit_tax_amount',10,2)->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('free_stock_entry_detail')) {
            if (Schema::hasColumn('free_stock_entry_detail', 'unit_tax_amount')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('unit_tax_amount');
                });
            }
        }
    }
}
