<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryMedicines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_medicines')) {
            Schema::create('surgery_medicines', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->string('item_code',1000)->nullable();
                $table->decimal('quantity')->nullable();
                $table->string('amount',1000)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_medicines')) {


            if (!Schema::hasColumn('surgery_medicines', 'surgery_id')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_medicines', 'item_code')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->string('item_code',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_medicines', 'quantity')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->decimal('quantity')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_medicines', 'amount')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->string('amount',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_medicines', 'created_by')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_medicines', 'updated_by')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_medicines', 'deleted_by')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_medicines', 'created_at')) {
                Schema::table('surgery_medicines', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_medicines');
    }
}
