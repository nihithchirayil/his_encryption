<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDoseUnitId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_medication_detail')) {
            if (!Schema::hasColumn('patient_medication_detail', 'dose_unit_id')) {
                Schema::table('patient_medication_detail', function (Blueprint $table) {
                    $table->integer('dose_unit_id')->nullable()->default(0);
                });
            }
        }     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_medication_detail')) {
            if (!Schema::hasColumn('patient_medication_detail', 'dose_unit_id')) {
                Schema::table('patient_medication_detail', function (Blueprint $table) {
                    $table->dropColumn('dose_unit_id');
                });
            }
        }   
    }
}
