<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'counter_id')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->bigInteger('counter_id')->nullable()->default(1);

                });
            }
          
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'counter_id')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('counter_id');
                });
            }
           
        }
    }
}
