<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableFreeStockEntryDetailAddColoums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('free_stock_entry_detail')) {
            if (!Schema::hasColumn('free_stock_entry_detail', 'product_stock_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->integer('product_stock_id')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'adj_detail_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->integer('adj_detail_id')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'stock_qty')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('stock_qty')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'unit_cost_with_out_tax')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('unit_cost_with_out_tax')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'uom_val')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('uom_val')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'item_rate')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('item_rate')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'total_rate')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('total_rate')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'product_stock_qty')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->decimal('product_stock_qty')->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('free_stock_entry_detail')) {
            if (!Schema::hasColumn('free_stock_entry_detail', 'product_stock_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('product_stock_id');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'adj_detail_id')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('adj_detail_id');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'stock_qty')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('stock_qty');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'unit_cost_with_out_tax')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('unit_cost_with_out_tax');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'uom_val')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('uom_val');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'item_rate')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('item_rate');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'total_rate')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('total_rate');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_detail', 'product_stock_qty')) {
                Schema::table('free_stock_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('product_stock_qty');
                });
            }
        }
    }
}
