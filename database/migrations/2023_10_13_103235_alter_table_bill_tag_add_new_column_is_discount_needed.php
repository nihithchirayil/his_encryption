<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillTagAddNewColumnIsDiscountNeeded extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_tag')) {
            if (!Schema::hasColumn('bill_tag', 'is_discount_needed')) {
                Schema::table('bill_tag', function (Blueprint $table) {
                    $table->smallInteger('is_discount_needed')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_tag')) {
            if (Schema::hasColumn('bill_tag', 'is_discount_needed')) {
                Schema::table('bill_tag', function (Blueprint $table) {
                    $table->dropColumn('is_discount_needed');
                });
            }
        }
    }
}
