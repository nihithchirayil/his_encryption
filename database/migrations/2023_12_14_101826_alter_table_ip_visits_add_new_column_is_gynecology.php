<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableIpVisitsAddNewColumnIsGynecology extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('ip_visits')) {
            if (!Schema::hasColumn('ip_visits', 'is_gynecology')) {
                Schema::table('ip_visits', function (Blueprint $table) {
                    $table->smallInteger('is_gynecology')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ip_visits')) {
            if (Schema::hasColumn('ip_visits', 'is_gynecology')) {
                Schema::table('ip_visits', function (Blueprint $table) {
                    $table->dropColumn('is_gynecology');
                });
            }
        }
    }
}
