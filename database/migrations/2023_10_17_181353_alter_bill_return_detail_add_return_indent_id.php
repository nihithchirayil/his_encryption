<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillReturnDetailAddReturnIndentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_return_detail')) {
            if (!Schema::hasColumn('bill_return_detail', 'return_indent_id')) {
                Schema::table('bill_return_detail', function (Blueprint $table) {
                    $table->bigInteger('return_indent_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_return_detail')) {
            if (Schema::hasColumn('bill_return_detail', 'return_indent_id')) {
                Schema::table('bill_return_detail', function (Blueprint $table) {
                    $table->dropColumn('return_indent_id');
                });
            }
        }
    }
}
