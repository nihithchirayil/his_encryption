<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_checklist')) {
            Schema::create('surgery_checklist', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('surgery_request_id')->nullable();
                $table->json('parameters_data')->nullable();
                $table->json('pre_op_vitals_data')->nullable();
                $table->json('sign_in_data')->nullable();
                $table->json('time_out_data')->nullable();
                $table->json('nursing_record_data')->nullable();
                $table->json('sign_out_data')->nullable();
                $table->smallInteger('checklist_completed_status')->nullable()->default(0);
                $table->smallInteger('approved_status')->nullable()->default(0);
                $table->smallInteger('parameters_approved_status')->nullable()->default(0);
                $table->smallInteger('pre_op_approved_status')->nullable()->default(0);
                $table->smallInteger('sign_in_approved_status')->nullable()->default(0);
                $table->smallInteger('timeout_approved_status')->nullable()->default(0);
                $table->smallInteger('nursing_record_approved_status')->nullable()->default(0);
                $table->smallInteger('signout_approved_status')->nullable()->default(0);
                $table->bigInteger('parameters_upated_by')->nullable();
                $table->bigInteger('pre_op_vitals_updated_by')->nullable();
                $table->bigInteger('sign_in_updated_by')->nullable();
                $table->bigInteger('time_out_updated_by')->nullable();
                $table->bigInteger('nursing_record_updated_by')->nullable();
                $table->bigInteger('sign_out_updated_by')->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('approved_by')->nullable();
                $table->bigInteger('parameters_approved_by')->nullable();
                $table->bigInteger('pre_op_approved_by')->nullable();
                $table->bigInteger('sign_in_approved_by')->nullable();
                $table->bigInteger('timeout_approved_by')->nullable();
                $table->bigInteger('nursing_record_approved_by')->nullable();
                $table->bigInteger('signout_approved_by')->nullable();
                $table->timestamp('parameters_upated_at')->nullable();
                $table->timestamp('pre_op_vitals_updated_at')->nullable();
                $table->timestamp('sign_in_updated_at')->nullable();
                $table->timestamp('time_out_updated_at')->nullable();
                $table->timestamp('nursing_record_updated_at')->nullable();
                $table->timestamp('sign_out_updated_at')->nullable();
                $table->timestamp('approved_at')->nullable();
                $table->timestamp('parameters_approved_at')->nullable();
                $table->timestamp('pre_op_approved_at')->nullable();
                $table->timestamp('sign_in_approved_at')->nullable();
                $table->timestamp('timeout_approved_at')->nullable();
                $table->timestamp('nursing_record_approved_at')->nullable();
                $table->timestamp('signout_approved_at')->nullable();
                $table->timestamp('signin_completed_at')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_checklist')) {


            if (!Schema::hasColumn('surgery_checklist', 'surgery_id')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'surgery_request_id')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('surgery_request_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'parameters_data')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->json('parameters_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'pre_op_vitals_data')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->json('pre_op_vitals_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_in_data')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->json('sign_in_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'time_out_data')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->json('time_out_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'nursing_record_data')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->json('nursing_record_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_out_data')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->json('sign_out_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'checklist_completed_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('checklist_completed_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'parameters_approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('parameters_approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'pre_op_approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('pre_op_approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_in_approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('sign_in_approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'timeout_approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('timeout_approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'nursing_record_approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('nursing_record_approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'signout_approved_status')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->smallInteger('signout_approved_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'parameters_upated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('parameters_upated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'pre_op_vitals_updated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('pre_op_vitals_updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_in_updated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('sign_in_updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'time_out_updated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('time_out_updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'nursing_record_updated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('nursing_record_updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_out_updated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('sign_out_updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'patient_id')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('patient_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'parameters_approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('parameters_approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'pre_op_approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('pre_op_approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_in_approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('sign_in_approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'timeout_approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('timeout_approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'nursing_record_approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('nursing_record_approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'signout_approved_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('signout_approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'parameters_upated_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('parameters_upated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'pre_op_vitals_updated_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('pre_op_vitals_updated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_in_updated_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('sign_in_updated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'time_out_updated_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('time_out_updated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'nursing_record_updated_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('nursing_record_updated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_out_updated_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('sign_out_updated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'parameters_approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('parameters_approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'pre_op_approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('pre_op_approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'sign_in_approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('sign_in_approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'timeout_approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('timeout_approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'nursing_record_approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('nursing_record_approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'signout_approved_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('signout_approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'signin_completed_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamp('signin_completed_at')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_checklist', 'created_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'updated_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_checklist', 'deleted_by')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_checklist', 'created_at')) {
                Schema::table('surgery_checklist', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_checklist');
    }
}
