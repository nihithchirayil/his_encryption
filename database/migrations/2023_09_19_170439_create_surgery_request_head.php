<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryRequestHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_request_head')) {
            Schema::create('surgery_request_head', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('surgery_date')->nullable();
                $table->date('previous_surgery_date')->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('anaesthesia_id')->nullable();
                $table->bigInteger('nursing_station')->nullable();
                $table->bigInteger('postponed_by')->nullable();
                $table->string('surgeon', 1000)->nullable();
                $table->string('surgery_desc', 1000)->nullable();
                $table->string('anaesthetist', 1000)->nullable();
                $table->text('diaganosis')->nullable();
                $table->text('postponed_remark')->nullable();
                $table->smallInteger('is_scheduled')->nullable()->default(0);
                $table->smallInteger('is_postponed')->nullable()->default(0);
                $table->time('surgery_time')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_request_head')) {


            if (!Schema::hasColumn('surgery_request_head', 'surgery_date')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->date('surgery_date')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'previous_surgery_date')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->date('previous_surgery_date')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'patient_id')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('patient_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'anaesthesia_id')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('anaesthesia_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'nursing_station')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('nursing_station')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'postponed_by')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('postponed_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'surgeon')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->string('surgeon', 1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'surgery_desc')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->string('surgery_desc', 1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'anaesthetist')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->string('anaesthetist', 1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'diaganosis')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->text('diaganosis')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'postponed_remark')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->text('postponed_remark')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'is_scheduled')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->smallInteger('is_scheduled')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'is_postponed')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->smallInteger('is_postponed')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'surgery_time')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->timestamp('surgery_time')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_head', 'created_by')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'updated_by')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_head', 'deleted_by')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_head', 'created_at')) {
                Schema::table('surgery_request_head', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_request_head');
    }
}
