<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableRoomTypeMasterAddIsOt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('room_type_master')) {
            if (!Schema::hasColumn('room_type_master', 'is_ot')) {
                Schema::table('room_type_master', function (Blueprint $table) {
                    $table->boolean('is_ot')->nullable()->default('f');
                });
            }
            if (!Schema::hasColumn('room_type_master', 'is_labour_room')) {
                Schema::table('room_type_master', function (Blueprint $table) {
                    $table->boolean('is_labour_room')->nullable()->default('f');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('room_type_master')) {
            if (Schema::hasColumn('room_type_master', 'is_ot')) {
                Schema::table('room_type_master', function (Blueprint $table) {
                    $table->dropColumn('is_ot');
                });
            }
            if (Schema::hasColumn('room_type_master', 'is_labour_room')) {
                Schema::table('room_type_master', function (Blueprint $table) {
                    $table->dropColumn('is_labour_room');
                });
            }
        }
    }
}
