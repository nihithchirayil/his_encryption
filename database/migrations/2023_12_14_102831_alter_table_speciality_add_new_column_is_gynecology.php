<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSpecialityAddNewColumnIsGynecology extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('speciality')) {
            if (!Schema::hasColumn('speciality', 'is_gynecology')) {
                Schema::table('speciality', function (Blueprint $table) {
                    $table->smallInteger('is_gynecology')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('speciality')) {
            if (Schema::hasColumn('speciality', 'is_gynecology')) {
                Schema::table('speciality', function (Blueprint $table) {
                    $table->dropColumn('is_gynecology');
                });
            }
        }
    }
}
