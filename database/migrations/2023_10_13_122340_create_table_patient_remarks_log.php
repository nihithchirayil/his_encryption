<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePatientRemarksLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('discount_approve_status')) {
            Schema::create('discount_approve_status', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->text('message')->nullable();
                $table->string('application_name', 200)->nullable();
                $table->string('pc_name', 200)->nullable();
                $table->string('uhid', 200)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_approve_status');
    }
}
