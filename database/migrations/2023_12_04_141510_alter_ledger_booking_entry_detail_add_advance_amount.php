<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLedgerBookingEntryDetailAddAdvanceAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('ledger_booking_entry_detail')) {

            if (!Schema::hasColumn('ledger_booking_entry_detail', 'advance_amount')) {
                Schema::table('ledger_booking_entry_detail', function (Blueprint $table) {
                    $table->decimal('advance_amount',15,2)->default(0)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ledger_booking_entry_detail')) {

            if (Schema::hasColumn('ledger_booking_entry_detail', 'advance_amount')) {
                Schema::table('ledger_booking_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('advance_amount');
                });
            }
        }
    }
}
