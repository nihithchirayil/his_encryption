<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCashCloseCollectionDetailsAddTotalExpenseNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cash_close_collection_details')) {
            if (!Schema::hasColumn('cash_close_collection_details', 'total_expense')) {
                Schema::table('cash_close_collection_details', function (Blueprint $table) {
                    $table->integer('total_expense')->nullable();
                });
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cash_close_collection_details')) {
            if (Schema::hasColumn('cash_close_collection_details', 'total_expense')) {
                Schema::table('cash_close_collection_details', function (Blueprint $table) {
                    $table->dropColumn('total_expense');
                });
            }
        }
    }
}
