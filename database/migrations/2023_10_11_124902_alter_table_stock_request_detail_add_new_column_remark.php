<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableStockRequestDetailAddNewColumnRemark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('stock_request_item_detail')) {
            if (!Schema::hasColumn('stock_request_item_detail', 'remarks')) {
                Schema::table('stock_request_item_detail', function (Blueprint $table) {
                    $table->text('remarks')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('stock_request_item_detail')) {
            if (Schema::hasColumn('stock_request_item_detail', 'remarks')) {
                Schema::table('stock_request_item_detail', function (Blueprint $table) {
                    $table->dropColumn('remarks');
                });
            }
        }
    }
}
