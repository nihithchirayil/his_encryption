<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillHeadAddColumnTotalItemDis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'total_item_dis')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->decimal('total_item_dis', 12, 2)->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'total_item_dis')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('total_item_dis');
                });
            }
        }
    }
}
