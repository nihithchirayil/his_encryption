<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLabNumericFormatMasterTypeChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lab_numeric_format_master')) {
        
            \DB::statement('ALTER TABLE "lab_numeric_format_master"
            ALTER "agefromtype" TYPE character varying,
            ALTER "agefromtype" DROP DEFAULT,
            ALTER "agefromtype" DROP NOT NULL,
            ALTER "agetotype" TYPE character varying,
            ALTER "agetotype" DROP DEFAULT,
            ALTER "agetotype" DROP NOT NULL,
            ALTER "formula_id" TYPE integer,
            ALTER "formula_id" SET DEFAULT 0,
            ALTER "formula_id" DROP NOT NULL,
            ALTER "age_type" TYPE character(1),
            ALTER "age_type" SET DEFAULT 0,
            ALTER "age_type" DROP NOT NULL;');

            DB::statement("COMMENT ON COLUMN lab_numeric_format_master.agefromtype IS 'YEAR MONTH DAY'");
            DB::statement("COMMENT ON COLUMN lab_numeric_format_master.agetotype IS 'YEAR MONTH DAY'");
            DB::statement("COMMENT ON COLUMN lab_numeric_format_master.formula_id IS '1 to,2 < and >,3 <= and >='");
            DB::statement("COMMENT ON COLUMN lab_numeric_format_master.age_type IS '0 no need,1 from and to,2 from,3 to'");

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
