<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterExtLabId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'ext_lab_id')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->smallInteger('ext_lab_id')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'ext_lab_id')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('ext_lab_id');
                });
            }
        }    
    }
}
