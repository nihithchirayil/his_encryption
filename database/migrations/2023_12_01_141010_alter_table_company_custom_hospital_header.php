<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCompanyCustomHospitalHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company')) {
            if (!Schema::hasColumn('company', 'custom_hospital_address')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->text('custom_hospital_address')->nullable();
                });
            }
            if (!Schema::hasColumn('company', 'custom_hospital_header')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->text('custom_hospital_header')->nullable();
                });
            }
            if (!Schema::hasColumn('company', 'custom_name')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->text('custom_name')->nullable();
                });
            }
            if (!Schema::hasColumn('company', 'hospital_name_for_pharmacy_bill')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->string('hospital_name_for_pharmacy_bill',250)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('company')) {
            if (Schema::hasColumn('company', 'custom_hospital_address')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->dropColumn('custom_hospital_address');
                });
            }
            if (Schema::hasColumn('company', 'custom_hospital_header')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->dropColumn('custom_hospital_header');
                });
            }
            if (Schema::hasColumn('company', 'custom_name')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->dropColumn('custom_name');
                });
            }
            if (Schema::hasColumn('company', 'hospital_name_for_pharmacy_bill')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->dropColumn('hospital_name_for_pharmacy_bill');
                });
            }

        }
    }
}
