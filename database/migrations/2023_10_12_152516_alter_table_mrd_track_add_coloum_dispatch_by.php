<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMrdTrackAddColoumDispatchBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mrd_track')) {
            if (!Schema::hasColumn('mrd_track', 'dispatch_by')) {
                Schema::table('mrd_track', function (Blueprint $table) {
                    $table->bigInteger('dispatch_by')->nullable();
                });
            }
            if (!Schema::hasColumn('mrd_track', 'dispatch_at')) {
                Schema::table('mrd_track', function (Blueprint $table) {
                    $table->timestamp('dispatch_at')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('mrd_track')) {
            if (Schema::hasColumn('mrd_track', 'dispatch_by')) {
                Schema::table('mrd_track', function (Blueprint $table) {
                    $table->dropColumn('dispatch_by');
                });
            }
            if (Schema::hasColumn('mrd_track', 'dispatch_at')) {
                Schema::table('mrd_track', function (Blueprint $table) {
                    $table->dropColumn('dispatch_at');
                });
            }
        }
    }
}
