<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillEntryDetailOldBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_entry_detail')) {
            if (!Schema::hasColumn('bill_entry_detail', 'old_batch')) {
                Schema::table('bill_entry_detail', function (Blueprint $table) {
                    $table->string('old_batch', 100)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_entry_detail')) {
            if (Schema::hasColumn('bill_entry_detail', 'old_batch')) {
                Schema::table('bill_entry_detail', function (Blueprint $table) {
                    $table->dropColumn('old_batch');
                });
            }
        }
    }
}
