<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPaymentModeAddShowGrn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_mode')) {
            if (!Schema::hasColumn('payment_mode', 'include_grn')) {
                Schema::table('payment_mode', function (Blueprint $table) {
                    $table->smallInteger('include_grn')->nullable()->default(0);
                });
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
