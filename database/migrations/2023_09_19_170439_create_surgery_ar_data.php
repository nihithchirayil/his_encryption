<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryArData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_ar_data')) {
            Schema::create('surgery_ar_data', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_head_id')->nullable();
                $table->text('ar_data')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_ar_data')) {
            if (!Schema::hasColumn('surgery_ar_data', 'surgery_head_id')) {
                Schema::table('surgery_ar_data', function (Blueprint $table) {
                    $table->bigInteger('surgery_head_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ar_data', 'ar_data')) {
                Schema::table('surgery_ar_data', function (Blueprint $table) {
                    $table->text('ar_data')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ar_data', 'created_by')) {
                Schema::table('surgery_ar_data', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_ar_data', 'updated_by')) {
                Schema::table('surgery_ar_data', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_ar_data', 'deleted_by')) {
                Schema::table('surgery_ar_data', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_ar_data', 'created_at')) {
                Schema::table('surgery_ar_data', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_ar_data');
    }
}
