<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryRequestSurgeon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_request_surgeon')) {
            Schema::create('surgery_request_surgeon', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('request_detail_id')->nullable();
                $table->bigInteger('surgeon_id')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_request_surgeon')) {

            if (!Schema::hasColumn('surgery_request_surgeon', 'request_detail_id')) {
                Schema::table('surgery_request_surgeon', function (Blueprint $table) {
                    $table->bigInteger('request_detail_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_surgeon', 'surgeon_id')) {
                Schema::table('surgery_request_surgeon', function (Blueprint $table) {
                    $table->bigInteger('surgeon_id')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_surgeon', 'created_by')) {
                Schema::table('surgery_request_surgeon', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_surgeon', 'updated_by')) {
                Schema::table('surgery_request_surgeon', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_surgeon', 'deleted_by')) {
                Schema::table('surgery_request_surgeon', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_surgeon', 'created_at')) {
                Schema::table('surgery_request_surgeon', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_request_surgeon');
    }
}
