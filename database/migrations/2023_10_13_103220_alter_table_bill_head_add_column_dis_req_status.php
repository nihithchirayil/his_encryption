<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillHeadAddColumnDisReqStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'dis_req_status')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->smallInteger('dis_req_status')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'dis_req_status')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('dis_req_status');
                });
            }
        }
    }
}
