<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMenuAddNewMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $route = "extensionsvalley.report.op_collection_report";
        if (!\DB::table('menu')->where('route_name', '=', $route)->where('status', 1)->exists()) {
            $parent_menu = 'Accounts';
            $super_parent_menu = 'Reports';
            if (!empty($super_parent_menu)) {
                $parent_id = \DB::table('menu as m')
                    ->join('menu as m1', 'm1.id', '=', 'm.parent_id')
                    ->where('m.name', '=', $parent_menu)->where('m1.name', '=', $super_parent_menu)->value('m.id');
            } else {
                $parent_id = \DB::table('menu')->where('name', '=', $parent_menu)->value('id');
            }
            $menu = [
                'name' => 'Doctor Wise OP Collection Report',
                'parent_id'   => $parent_id,
                'route_name'  => $route,
                'menu_icon'   => '',
                'menu_level'  => 2,
                'display_order'   => 0,
                'is_desktop_menu' => 0,
                'status'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s'),
            ];
            \DB::table('menu')->insert($menu);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
