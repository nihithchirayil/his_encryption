<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDischargeSummaryAddFollowupDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'followup_date')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->date('followup_date')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'followup_date')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->date('followup_date')->nullable();
                });
            }
        }
    }
}
