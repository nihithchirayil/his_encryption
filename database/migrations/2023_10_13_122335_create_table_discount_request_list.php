<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDiscountRequestList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('discount_request_list')) {
            Schema::create('discount_request_list', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('bill_no', 150)->nullable();
                $table->string('bill_tag', 150)->nullable();
                $table->bigInteger('bill_head_id')->nullable();
                $table->decimal('discount_amount', 12, 2)->default(0);
                $table->bigInteger('requested_by')->nullable();
                $table->timestamp('requested_at')->nullable();
                $table->string('requested_remark', 200)->nullable();
                $table->bigInteger('approved_by')->nullable();
                $table->timestamp('approved_at')->nullable();
                $table->string('remark', 200)->nullable();
                $table->smallInteger('discount_type')->nullable(0);
                $table->bigInteger('discount_value')->nullable();
                $table->smallInteger('status')->nullable(1);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_request_list');
    }
}
