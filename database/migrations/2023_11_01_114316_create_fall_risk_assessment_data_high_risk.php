<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFallRiskAssessmentDataHighRisk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('fall_risk_assessment_data_high_risk')) {
            Schema::create('fall_risk_assessment_data_high_risk', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('fall_risk_main_id')->nullable();
                $table->integer('patient_id')->nullable();
                $table->integer('visit_id')->nullable();
                $table->integer('doctor_id')->nullable();
                $table->integer('high_risk_sign_posted')->nullable();
                $table->integer('nurse_assist_mobilizing')->nullable();
                $table->integer('night_light')->nullable();
                $table->integer('commode_at_bedside')->nullable();
                $table->integer('high_risk_patient_family_education')->nullable();
                $table->integer('ensure_siderails')->nullable();
                $table->integer('access_type')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->date('entry_date')->nullable();
            });
            \DB::statement("COMMENT ON COLUMN fall_risk_assessment_data_high_risk.access_type IS '1-morning, 2-evening , 3-night'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fall_risk_assessment_data_high_risk');
    }
}
