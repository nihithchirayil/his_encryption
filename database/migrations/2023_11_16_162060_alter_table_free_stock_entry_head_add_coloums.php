<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableFreeStockEntryHeadAddColoums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('free_stock_entry_head')) {
            if (!Schema::hasColumn('free_stock_entry_head', 'adjustment_head_id')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->bigInteger('adjustment_head_id')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'item_type')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->integer('item_type')->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'sales_date')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->date('sales_date')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'reason')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->string('reason')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'remarks')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->string('remarks')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'free_stock_no')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->string('free_stock_no')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'total_rate')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->decimal('total_rate')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'total_tax')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->decimal('total_tax')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'round_off')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->decimal('round_off')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'net_amount')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->decimal('net_amount')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'approve_status')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->smallInteger('approve_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'approved_at')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->timestamp('approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'approved_by')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->integer('approved_by')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('free_stock_entry_head')) {
            if (!Schema::hasColumn('free_stock_entry_head', 'adjustment_head_id')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('adjustment_head_id');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'item_type')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('item_type');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'sales_date')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('sales_date');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'reason')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('reason');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'remarks')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('remarks');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'free_stock_no')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('free_stock_no');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'total_rate')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('total_rate');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'total_tax')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('total_tax');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'round_off')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('round_off');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'net_amount')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('net_amount');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'approve_status')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('approve_status');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'approved_by')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('approved_by');
                });
            }
            if (!Schema::hasColumn('free_stock_entry_head', 'approved_at')) {
                Schema::table('free_stock_entry_head', function (Blueprint $table) {
                    $table->dropColumn('approved_at');
                });
            }
        }
    }
}
