<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLabNumericFormatMasterAddNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lab_numeric_format_master')) {
            if (!Schema::hasColumn('lab_numeric_format_master', 'agefromtype')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->string('agefromtype')->nullable()->comment("YEAR MONTH DAY");
                });
            }
            if (!Schema::hasColumn('lab_numeric_format_master', 'agetotype')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->string('agetotype')->nullable()->comment("YEAR MONTH DAY");
                });
            }
            if (!Schema::hasColumn('lab_numeric_format_master', 'formula_id')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->integer('formula_id')->nullable()->default(0)->comment("1 to,2 < and >,3 <= and >=");
                });
            }
            if (!Schema::hasColumn('lab_numeric_format_master', 'age_type')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->string('age_type',1)->nullable()->default(0)->comment("0 no need,1 from and to,2 from,3 to");
                });
            }
            if (!Schema::hasColumn('lab_numeric_format_master', 'include_month_day')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->smallInteger('include_month_day')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('lab_numeric_format_master', 'age_from_in_year')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->integer('age_from_in_year')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('lab_numeric_format_master', 'age_to_in_year')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->integer('age_to_in_year')->nullable()->default(0);
                });
            }
            
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('lab_numeric_format_master')) {
            if (Schema::hasColumn('lab_numeric_format_master', 'agefromtype')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('agefromtype');
                });
            }
            if (Schema::hasColumn('lab_numeric_format_master', 'agetotype')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('agetotype');
                });
            }
            if (Schema::hasColumn('lab_numeric_format_master', 'formula_id')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('formula_id');
                });
            }
            if (Schema::hasColumn('lab_numeric_format_master', 'age_type')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('age_type');
                });
            }
            if (Schema::hasColumn('lab_numeric_format_master', 'include_month_day')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('include_month_day');
                });
            }
            if (Schema::hasColumn('lab_numeric_format_master', 'age_from_in_year')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('age_from_in_year');
                });
            }
            if (Schema::hasColumn('lab_numeric_format_master', 'age_to_in_year')) {
                Schema::table('lab_numeric_format_master', function (Blueprint $table) {
                    $table->dropColumn('age_to_in_year');
                });
            }
        }
    }
}
