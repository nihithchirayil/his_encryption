<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEmpCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'emp_code')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->string('emp_code',10)->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'emp_code')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('emp_code');
                });
            }
        }    
    }
}
