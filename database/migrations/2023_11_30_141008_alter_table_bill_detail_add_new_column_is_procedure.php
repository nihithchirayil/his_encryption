<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillDetailAddNewColumnIsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_detail')) {
            if (!Schema::hasColumn('bill_detail', 'is_procedure')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->smallInteger('is_procedure')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_detail')) {
            if (Schema::hasColumn('bill_detail', 'is_procedure')) {
                Schema::table('bill_detail', function (Blueprint $table) {
                    $table->dropColumn('is_procedure');
                });
            }
           
        }
    }
}
