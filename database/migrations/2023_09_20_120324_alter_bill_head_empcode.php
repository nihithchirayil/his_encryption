<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillHeadEmpcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'emp_code')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->string('emp_code',50)->nullable();
                });
            }
            if (!Schema::hasColumn('bill_head', 'updated_by_emp')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->string('updated_by_emp',50)->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'emp_code')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('emp_code');
                });
            }
            if (Schema::hasColumn('bill_head', 'updated_by_emp')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('updated_by_emp');
                });
            }
        }    
    }
}
