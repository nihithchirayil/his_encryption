<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRefundRequestList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('refund_request_list')) {
            Schema::create('refund_request_list', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('bill_no', 30)->nullable();
                $table->string('bill_tag', 30)->nullable();
                $table->bigInteger('requested_by')->nullable();
                $table->timestamp('requested_at')->nullable();
                $table->string('requested_remark', 100)->nullable();
                $table->bigInteger('level_1_approved_by')->nullable();
                $table->timestamp('level_1_approved_at')->nullable();
                $table->string('level_1_remark', 100)->nullable();
                $table->bigInteger('level_2_approved_by')->nullable();
                $table->timestamp('level_2_approved_at')->nullable();
                $table->string('level_2_remark', 100)->nullable();
                $table->bigInteger('level_3_approved_by')->nullable();
                $table->timestamp('level_3_approved_at')->nullable();
                $table->string('level_3_remark', 100)->nullable();
                $table->smallInteger('refund_status')->nullable()->comment('0-Requested,1-Approved,2-Rejected');
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });


            if (!Schema::hasColumn('refund_request_list', 'refund_status'))
            {
                \DB::statement("COMMENT ON COLUMN refund_request_list.refund_status IS 0-Requested,1-Approved,2-Rejected");
            }

        }

        // Artisan::call('db:seed', array('--class' => 'EmployeeRatingQuestions'));       
        // shell_exec('php artisan db:seed --class=EmployeeRatingQuestions');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refund_request_list');
    }
}
