<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDischargeSummaryNewColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'presenting_symptoms')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('presenting_symptoms')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'physical_findings')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('physical_findings')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'clinical_impression')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('clinical_impression')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'treatment_and_course')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('treatment_and_course')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'final_diagnosis')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('final_diagnosis')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'condition_at_discharge')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('condition_at_discharge')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'recommendations')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('recommendations')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'follow_up_details')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('follow_up_details')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'template_conent_text')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('template_conent_text')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'provisional_diagnosis')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('provisional_diagnosis')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'present_illness')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('present_illness')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'history_of_alcoholism')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('history_of_alcoholism')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'past_medical_and_surgical_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('past_medical_and_surgical_history')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'family_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('family_history')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'significant_of_lama_or_dama')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('significant_of_lama_or_dama')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'course_hospital')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('course_hospital')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'surgical_procedure')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('surgical_procedure')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'urgent_care')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('urgent_care')->nullable();
                });
            }

            if (!Schema::hasColumn('discharge_summary', 'app_name')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->string('app_name')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'provisional_diagnosis')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('provisional_diagnosis')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'present_illness')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('present_illness')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'history_of_alcoholism')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('history_of_alcoholism')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'past_medical_and_surgical_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('past_medical_and_surgical_history')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'family_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('family_history')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'significant_of_lama_or_dama')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('significant_of_lama_or_dama')->nullable();
                });
            }
        }
    }
}
