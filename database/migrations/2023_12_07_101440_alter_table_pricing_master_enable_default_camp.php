<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePricingMasterEnableDefaultCamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('pricing_master')) {
            if (!Schema::hasColumn('pricing_master', 'is_camp')) {
                Schema::table('pricing_master', function (Blueprint $table) {
                    $table->smallInteger('is_camp')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('pricing_master', 'default_camp')) {
                Schema::table('pricing_master', function (Blueprint $table) {
                    $table->smallInteger('default_camp')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('pricing_master')) {
            if (Schema::hasColumn('pricing_master', 'is_camp')) {
                Schema::table('pricing_master', function (Blueprint $table) {
                    $table->dropColumn('is_camp');
                });
            }
            if (Schema::hasColumn('pricing_master', 'default_camp')) {
                Schema::table('pricing_master', function (Blueprint $table) {
                    $table->dropColumn('default_camp');
                });
            }

        }
    }
}
