<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableExpenseMasterAddNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('expense_master')) {
            if (!Schema::hasColumn('expense_master', 'collection_mode')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->integer('collection_mode')->nullable();
                });
            }
            if (!Schema::hasColumn('expense_master', 'cash_close_head_id')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->bigInteger('cash_close_head_id')->nullable();
                });
            }
            if (!Schema::hasColumn('expense_master', 'approved_at')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->timestamp('approved_at')->nullable();
                });
            }
            if (!Schema::hasColumn('expense_master', 'approved_by')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->bigInteger('approved_by')->nullable();
                });
            }
            if (!Schema::hasColumn('expense_master', 'rejected_at')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->timestamp('rejected_at')->nullable();
                });
            }
            if (!Schema::hasColumn('expense_master', 'rejected_by')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->bigInteger('rejected_by')->nullable();
                });
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('expense_master')) {
            if (Schema::hasColumn('expense_master', 'collection_mode')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->dropColumn('collection_mode');
                });
            }
            if (Schema::hasColumn('expense_master', 'cash_close_head_id')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->dropColumn('cash_close_head_id');
                });
            }
            if (Schema::hasColumn('expense_master', 'approved_at')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->dropColumn('approved_at');
                });
            }
            if (Schema::hasColumn('expense_master', 'approved_by')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->dropColumn('approved_by');
                });
            }
            if (Schema::hasColumn('expense_master', 'rejected_at')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->dropColumn('rejected_at');
                });
            }
            if (Schema::hasColumn('expense_master', 'rejected_by')) {
                Schema::table('expense_master', function (Blueprint $table) {
                    $table->dropColumn('rejected_by');
                });
            }
        }
    }
}
