<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistopathologyChecklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('histopathology_checklist')) {
            Schema::create('histopathology_checklist', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('surgery_id')->nullable();
                $table->bigInteger('surgery_request_id')->nullable();
                $table->text('histopathology_json')->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histopathology_checklist');
    }
}
