<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableErrorHandling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('error_handling')) {
            Schema::create('error_handling', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('error_details', 1000)->nullable();
                $table->text('error_msg')->nullable();
                $table->text('error_data1')->nullable();
                $table->text('error_data2')->nullable();
                $table->string('application_name', 100);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('error_handling');
    }
}
