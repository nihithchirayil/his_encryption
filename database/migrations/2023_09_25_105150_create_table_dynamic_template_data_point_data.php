<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDynamicTemplateDataPointData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('dynamic_template_data_point_data')) {
            Schema::create('dynamic_template_data_point_data', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id');
                $table->bigInteger('visit_id');
                $table->string('code', 25)->nullable();
                $table->text('entry_data')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_template_data_point_data');
    }
}
