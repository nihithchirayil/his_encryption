<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDynamicTemplateDataPointDataAddDynamicDataId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('dynamic_template_data_point_data')) {
            if (!Schema::hasColumn('dynamic_template_data_point_data', 'dynamic_data_id')) {
                Schema::table('dynamic_template_data_point_data', function (Blueprint $table) {
                    $table->bigInteger('dynamic_data_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('dynamic_template_data_point_data')) {
            if (Schema::hasColumn('dynamic_template_data_point_data', 'dynamic_data_id')) {
                Schema::table('dynamic_template_data_point_data', function (Blueprint $table) {
                    $table->dropColumn('dynamic_data_id');
                });
            }
        }
    }
}
