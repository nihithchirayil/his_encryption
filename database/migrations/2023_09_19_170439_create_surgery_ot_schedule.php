<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryOtSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_ot_schedule')) {
            Schema::create('surgery_ot_schedule', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('request_id')->nullable();
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('ot_id')->nullable();
                $table->timestamp('start_time')->nullable();
                $table->timestamp('end_time')->nullable();
                $table->smallInteger('approval_status')->nullable()->default(0);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_ot_schedule')) {


            if (!Schema::hasColumn('surgery_ot_schedule', 'request_id')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->bigInteger('request_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'surgery_id')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'ot_id')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->bigInteger('ot_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'start_time')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->timestamp('start_time')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'end_time')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->timestamp('end_time')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'approval_status')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->smallInteger('approval_status')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'created_by')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'updated_by')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'deleted_by')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_ot_schedule', 'created_at')) {
                Schema::table('surgery_ot_schedule', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_ot_schedule');
    }
}
