<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDoseUnitIdMedicineFavoriteList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('medicine_favorite_list')) {
            if (!Schema::hasColumn('medicine_favorite_list', 'dose_unit_id')) {
                Schema::table('medicine_favorite_list', function (Blueprint $table) {
                    $table->integer('dose_unit_id')->nullable()->default(0);
                });
            }
        }     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('medicine_favorite_list')) {
            if (!Schema::hasColumn('medicine_favorite_list', 'dose_unit_id')) {
                Schema::table('medicine_favorite_list', function (Blueprint $table) {
                    $table->dropColumn('dose_unit_id');
                });
            }
        }    
    }
}
