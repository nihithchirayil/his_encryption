<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDoctorconsultationFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('doctor_consultation_fee')) {
            if (!Schema::hasColumn('doctor_consultation_fee', 'session_id')) {
                Schema::table('doctor_consultation_fee', function (Blueprint $table) {
                    $table->smallInteger('session_id')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('doctor_consultation_fee')) {
            if (Schema::hasColumn('doctor_consultation_fee', 'session_id')) {
                Schema::table('doctor_consultation_fee', function (Blueprint $table) {
                    $table->dropColumn('session_id');
                });
            }
        }    
    }
}
