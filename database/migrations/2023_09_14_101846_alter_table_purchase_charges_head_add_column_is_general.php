<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePurchaseChargesHeadAddColumnIsGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('purchase_charges_head')) {
            if (!Schema::hasColumn('purchase_charges_head', 'is_general')) {
                Schema::table('purchase_charges_head', function (Blueprint $table) {
                    $table->smallInteger('is_general')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('purchase_charges_head')) {
            if (Schema::hasColumn('purchase_charges_head', 'is_general')) {
                Schema::table('purchase_charges_head', function (Blueprint $table) {
                    $table->dropColumn('is_general');
                });
            }
        }
    }
}
