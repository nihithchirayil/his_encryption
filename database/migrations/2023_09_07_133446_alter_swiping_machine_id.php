<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSwipingMachineId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cash_collection')) {
            if (!Schema::hasColumn('cash_collection', 'swiping_machine_id')) {
                Schema::table('cash_collection', function (Blueprint $table) {
                    $table->integer('swiping_machine_id')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cash_collection')) {
            if (Schema::hasColumn('cash_collection', 'swiping_machine_id')) {
                Schema::table('cash_collection', function (Blueprint $table) {
                    $table->dropColumn('swiping_machine_id');
                });
            }
        }     
    }
}
