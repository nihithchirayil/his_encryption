<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBillTagEnableProcedurDr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_tag')) {
            if (!Schema::hasColumn('bill_tag', 'enable_procedur_dr')) {
                Schema::table('bill_tag', function (Blueprint $table) {
                    $table->Integer('enable_procedur_dr')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_tag')) {
            if (Schema::hasColumn('bill_tag', 'enable_procedur_dr')) {
                Schema::table('bill_tag', function (Blueprint $table) {
                    $table->dropColumn('enable_procedur_dr');
                });
            }

        }
    }
}
