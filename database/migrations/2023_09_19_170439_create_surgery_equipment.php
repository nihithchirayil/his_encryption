<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryEquipment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_equipment')) {
            Schema::create('surgery_equipment', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable()->default(0);
                $table->string('service_code',1000)->nullable();
                $table->string('amount',1000)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_equipment')) {


            if (!Schema::hasColumn('surgery_equipment', 'surgery_id')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_equipment', 'service_code')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->string('service_code',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_equipment', 'amount')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->string('amount',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_equipment', 'created_by')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_equipment', 'updated_by')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_equipment', 'deleted_by')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_equipment', 'created_at')) {
                Schema::table('surgery_equipment', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_equipment');
    }
}
