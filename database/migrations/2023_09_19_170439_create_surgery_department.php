<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_department')) {
            Schema::create('surgery_department', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('speciality_id')->nullable();
                $table->string('department_code',1000)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_department')) {


            if (!Schema::hasColumn('surgery_department', 'surgery_id')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_department', 'speciality_id')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->bigInteger('speciality_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_department', 'department_code')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->string('department_code',1000)->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_department', 'created_by')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_department', 'updated_by')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_department', 'deleted_by')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_department', 'created_at')) {
                Schema::table('surgery_department', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_department');
    }
}
