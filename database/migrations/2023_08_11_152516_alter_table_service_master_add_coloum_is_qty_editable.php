<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableServiceMasterAddColoumIsQtyEditable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('service_master')) {
            if (!Schema::hasColumn('service_master', 'is_qty_editable')) {
                Schema::table('service_master', function (Blueprint $table) {
                    $table->boolean('is_qty_editable')->default('false');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('service_master')) {
            if (Schema::hasColumn('service_master', 'is_qty_editable')) {
                Schema::table('service_master', function (Blueprint $table) {
                    $table->dropColumn('is_qty_editable');
                });
            }
        }
    }
}
