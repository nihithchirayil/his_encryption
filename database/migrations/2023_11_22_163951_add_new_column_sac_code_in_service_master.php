<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnSacCodeInServiceMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_master', function (Blueprint $table) {
            if (!Schema::hasColumn('service_master', 'sac_code')) {
                Schema::table('service_master', function (Blueprint $table) {
                    $table->string('sac_code')->nullable();
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_master', function (Blueprint $table) {
            if (Schema::hasColumn('service_master', 'sac_code')) {
                Schema::table('service_master', function (Blueprint $table) {
                    $table->dropColumn('sac_code');
                });
            }
        });
    }
}
