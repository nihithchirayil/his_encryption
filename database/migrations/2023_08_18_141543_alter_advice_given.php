<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdviceGiven extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_medication_head')) {
            if (!Schema::hasColumn('patient_medication_head', 'advice_given')) {
                Schema::table('patient_medication_head', function (Blueprint $table) {
                    $table->text('advice_given')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_medication_head')) {
            Schema::table('patient_medication_head', function (Blueprint $table) {
                $table->dropColumn('advice_given');
            });
        }        
    }
}
