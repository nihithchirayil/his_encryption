<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableManagerLevelRefundLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('manager_level_refund_log')) {
        Schema::create('manager_level_refund_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bill_id')->nullable();
            $table->string('bill_no', 100)->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('return_id')->nullable();            
            $table->bigInteger('cashier')->nullable(); 
            $table->timestamp('refund_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();            
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manager_level_refund_log');
    }
}
