<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillEditEntryDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bill_edit_entry_detail')) {
            Schema::create('bill_edit_entry_detail', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('bill_head_id')->default(0);
                $table->string('bill_tag', 20)->nullable();
                $table->bigInteger('item_id')->nullable();
                $table->string('item_desc', 200)->nullable();
                $table->string('batch', 20)->nullable();
                $table->date('expiry')->nullable();
                $table->decimal('qty', 15, 2)->default(0);
                $table->decimal('selling_price_wo_tax', 15, 2)->default(0);
                $table->decimal('selling_price_with_tax', 15, 2)->default(0);
                $table->integer('discount_type')->nullable();
                $table->decimal('discount', 15, 2)->default(0);
                $table->text('discount_reason')->nullable();
                $table->decimal('discount_amount', 15, 2)->default(0);
                $table->decimal('pricing_discount_amount', 15, 2)->default(0);
                $table->decimal('bill_discount_split', 15, 2)->default(0);
                $table->decimal('net_amount', 15, 2)->default(0);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_edit_entry_detail');
    }
}
