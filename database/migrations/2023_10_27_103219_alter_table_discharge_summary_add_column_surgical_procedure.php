<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDischargeSummaryAddColumnSurgicalProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'course_hospital')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('course_hospital')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'surgical_procedure')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('surgical_procedure')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (Schema::hasColumn('discharge_summary', 'course_hospital')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('course_hospital');
                });
            }
            if (Schema::hasColumn('discharge_summary', 'surgical_procedure')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('surgical_procedure');
                });
            }
        }
    }
}
