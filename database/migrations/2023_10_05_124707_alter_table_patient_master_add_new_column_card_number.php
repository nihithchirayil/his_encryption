<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePatientMasterAddNewColumnCardNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_master')) {
            if (!Schema::hasColumn('patient_master', 'card_number')) {
                Schema::table('patient_master', function (Blueprint $table) {
                    $table->string('card_number')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_master')) {
            if (Schema::hasColumn('patient_master', 'card_number')) {
                Schema::table('patient_master', function (Blueprint $table) {
                    $table->dropColumn('card_number');
                });
            }
        }
    }
}
