<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFallRiskAssessmentData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('fall_risk_assessment_data')) {
            Schema::create('fall_risk_assessment_data', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('fall_risk_main_id')->nullable();
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->bigInteger('doctor_id')->nullable();
                $table->smallInteger('bed_height')->nullable();
                $table->smallInteger('ensure_footwear')->nullable();
                $table->smallInteger('items_within_reach')->nullable();
                $table->smallInteger('walking_aids')->nullable();
                $table->smallInteger('access_toileting')->nullable();
                $table->smallInteger('items_on_stronger_side')->nullable();
                $table->smallInteger('patient_family_education')->nullable();
                $table->smallInteger('access_type')->nullable();
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->date('entry_date')->nullable();
            });
            \DB::statement("COMMENT ON COLUMN fall_risk_assessment_data.access_type IS '1-morning, 2-evening , 3-night'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fall_risk_assessment_data');
    }
}
