<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBillHeadAddColumnInsuranceApprovalAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bill_head')) {
            if (!Schema::hasColumn('bill_head', 'insurance_approved_amount')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->decimal('insurance_approved_amount', 12, 2)->default(0)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bill_head')) {
            if (Schema::hasColumn('bill_head', 'insurance_approved_amount')) {
                Schema::table('bill_head', function (Blueprint $table) {
                    $table->dropColumn('insurance_approved_amount');
                });
            }
        }
    }
}
