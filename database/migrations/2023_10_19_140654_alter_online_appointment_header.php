<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOnlineAppointmentHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company')) {
            if (!Schema::hasColumn('company', 'online_appointment_header')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->text('online_appointment_header')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('company')) {
            if (Schema::hasColumn('company', 'online_appointment_header')) {
                Schema::table('company', function (Blueprint $table) {
                    $table->dropColumn('online_appointment_header');
                });
            }
        }    
    }
}
