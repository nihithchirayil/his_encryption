<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableGroupsAddNewGroupExpensePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date_time = date('Y-m-d H:i:s');
        $data = \DB::table('groups')->where('name','Expense Payment')->count();
        if ($data == 0) {
            $create_arr[] = [
                'name' => 'Expense Payment',
                'status' => 1,
                'created_at' => $date_time,
                'updated_at' => $date_time,
                
            ];
            \DB::table('groups')->insert($create_arr);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
