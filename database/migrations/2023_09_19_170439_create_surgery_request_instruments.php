<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryRequestInstruments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_request_instruments')) {
            Schema::create('surgery_request_instruments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('request_id')->nullable();
                $table->bigInteger('instrument_id')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_request_instruments')) {

            if (!Schema::hasColumn('surgery_request_instruments', 'request_id')) {
                Schema::table('surgery_request_instruments', function (Blueprint $table) {
                    $table->bigInteger('request_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_instruments', 'instrument_id')) {
                Schema::table('surgery_request_instruments', function (Blueprint $table) {
                    $table->bigInteger('instrument_id')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_instruments', 'created_by')) {
                Schema::table('surgery_request_instruments', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_instruments', 'updated_by')) {
                Schema::table('surgery_request_instruments', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_instruments', 'deleted_by')) {
                Schema::table('surgery_request_instruments', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_instruments', 'created_at')) {
                Schema::table('surgery_request_instruments', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_request_instruments');
    }
}
