<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryRequestDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_request_detail')) {
            Schema::create('surgery_request_detail', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('request_id')->nullable();
                $table->bigInteger('surgery_id')->nullable();
                $table->bigInteger('is_requested_by')->nullable();
                $table->bigInteger('is_scheduled_by')->nullable();
                $table->bigInteger('is_surgery_checklist_completed_by')->nullable();
                $table->bigInteger('is_anasthesia_checklist_completed_by')->nullable();
                $table->bigInteger('is_surgery_notes_added_by')->nullable();
                $table->bigInteger('is_surgery_bill_generated_by')->nullable();
                $table->bigInteger('is_primary_surgery_by')->nullable();
                $table->bigInteger('is_medicine_indented_by')->nullable();
                $table->bigInteger('surgery_notes_added_by')->nullable();
                $table->bigInteger('histopathology_completed_by')->nullable();
                $table->bigInteger('microbiology_completed_by')->nullable();
                $table->smallInteger('is_requested')->nullable()->default(1);
                $table->smallInteger('is_scheduled')->nullable()->default(0);
                $table->smallInteger('is_surgery_checklist_completed')->nullable()->default(0);
                $table->smallInteger('is_anasthesia_checklist_completed')->nullable()->default(0);
                $table->smallInteger('is_surgery_notes_added')->nullable()->default(0);
                $table->smallInteger('is_surgery_bill_generated')->nullable()->default(0);
                $table->smallInteger('is_primary_surgery')->nullable()->default(0);
                $table->smallInteger('is_medicine_indented')->nullable()->default(0);
                $table->smallInteger('is_histopathology_completed')->nullable()->default(0);
                $table->smallInteger('is_microbiology_completed')->nullable()->default(0);
                $table->timestamp('is_requested_at')->nullable();
                $table->timestamp('is_scheduled_at')->nullable();
                $table->timestamp('is_surgery_checklist_completed_at')->nullable();
                $table->timestamp('is_anasthesia_checklist_completed_at')->nullable();
                $table->timestamp('is_surgery_notes_added_at')->nullable();
                $table->timestamp('is_surgery_bill_generated_at')->nullable();
                $table->timestamp('is_primary_surgery_at')->nullable();
                $table->timestamp('is_medicine_indented_at')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_request_detail')) {
            if (!Schema::hasColumn('surgery_request_detail', 'request_id')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('request_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'surgery_id')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('surgery_id')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_requested_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_requested_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_scheduled_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_scheduled_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_checklist_completed_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_surgery_checklist_completed_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_anasthesia_checklist_completed_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_anasthesia_checklist_completed_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_notes_added_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_surgery_notes_added_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_bill_generated_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_surgery_bill_generated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_primary_surgery_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_primary_surgery_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_medicine_indented_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('is_medicine_indented_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'surgery_notes_added_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('surgery_notes_added_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'histopathology_completed_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('histopathology_completed_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'microbiology_completed_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('microbiology_completed_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_requested')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_requested')->nullable()->default(1);
                });
            }

            if (!Schema::hasColumn('surgery_request_detail', 'is_scheduled')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_scheduled')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_checklist_completed')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_surgery_checklist_completed')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_anasthesia_checklist_completed')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_anasthesia_checklist_completed')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_notes_added')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_surgery_notes_added')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_bill_generated')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_surgery_bill_generated')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_primary_surgery')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_primary_surgery')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_medicine_indented')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_medicine_indented')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_histopathology_completed')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_histopathology_completed')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_microbiology_completed')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->smallInteger('is_microbiology_completed')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_requested_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_requested_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_scheduled_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_scheduled_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_checklist_completed_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_surgery_checklist_completed_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_anasthesia_checklist_completed_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_anasthesia_checklist_completed_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_notes_added_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_surgery_notes_added_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_surgery_bill_generated_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_surgery_bill_generated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_primary_surgery_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_primary_surgery_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'is_medicine_indented_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('is_medicine_indented_at')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_detail', 'created_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'updated_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'deleted_by')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_request_detail', 'created_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('created_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'updated_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('updated_at')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_request_detail', 'deleted_at')) {
                Schema::table('surgery_request_detail', function (Blueprint $table) {
                    $table->timestamp('deleted_at')->nullable();
                });
            }



        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_request_detail');
    }
}
