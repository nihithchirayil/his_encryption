<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableHoldBillDetailIsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('hold_bill_detail')) {
            if (!Schema::hasColumn('hold_bill_detail', 'is_procedure')) {
                Schema::table('hold_bill_detail', function (Blueprint $table) {
                    $table->smallInteger('is_procedure')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('hold_bill_detail')) {
            if (Schema::hasColumn('hold_bill_detail', 'is_procedure')) {
                Schema::table('hold_bill_detail', function (Blueprint $table) {
                    $table->dropColumn('is_procedure');
                });
            }

        }
    }
}
