<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashCloseCurrentCollectionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cash_close_current_collection_details')) {
            Schema::create('cash_close_current_collection_details', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('head_id');
                $table->string('bill_no',50);
                $table->decimal('net_amount');
                $table->integer('cash_collected_by');
                $table->integer('payment_mode_type');
                $table->decimal('collection_amount');
                $table->decimal('refund_collection_amount');
                $table->timestamps();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->string('pc_name',100);
                $table->integer('unit_id');
                $table->string('ip_address',100);
                $table->string('application_name',30);
                $table->smallInteger('type')->nullable();
                $table->integer('type_id')->nullable();
                $table->integer('closed_counter_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_close_current_collection_details');
    }
}
