<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOperationChecklistDetailsAddAdditionalOrData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('operation_checklist_details')) {
            if (!Schema::hasColumn('operation_checklist_details', 'additional_operational_data')) {
                Schema::table('operation_checklist_details', function (Blueprint $table) {
                    $table->text('additional_operational_data', 100)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('operation_checklist_details')) {
            if (Schema::hasColumn('operation_checklist_details', 'additional_operational_data')) {
                Schema::table('operation_checklist_details', function (Blueprint $table) {
                    $table->dropColumn('additional_operational_data');
                });
            }
        }
    }
}
