<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePurchaseReturnHeadRoundoff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('purchase_return_head')) {
            if (!Schema::hasColumn('purchase_return_head', 'roundoff')) {
                Schema::table('purchase_return_head', function (Blueprint $table) {
                    $table->decimal('roundoff')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('purchase_return_head')) {
            if (Schema::hasColumn('purchase_return_head', 'roundoff')) {
                Schema::table('purchase_return_head', function (Blueprint $table) {
                    $table->dropColumn('roundoff');
                });
            }
        }
    }
}
