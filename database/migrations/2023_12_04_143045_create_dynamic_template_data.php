<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicTemplateData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('dynamic_template_data')) {
            Schema::create('dynamic_template_data', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->bigInteger('encounter_id')->nullable();
                $table->integer('doctor_id')->nullable();
                $table->text('input_data')->nullable();
                $table->integer('template_id')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        }else{
            if (!Schema::hasColumn('dynamic_template_data', 'patient_id')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->bigInteger('patient_id')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'visit_id')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->bigInteger('visit_id')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'encounter_id')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->bigInteger('encounter_id')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'doctor_id')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->integer('doctor_id')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'input_data')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->text('input_data')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'template_id')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->integer('template_id')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'created_by')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'updated_by')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('dynamic_template_data', 'deleted_by')) {
                Schema::table('dynamic_template_data', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_template_data');
    }
}
