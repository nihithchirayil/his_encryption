<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterIsResultUploaded extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lab_sample_details')) {
            if (!Schema::hasColumn('lab_sample_details', 'is_result_uploaded')) {
                Schema::table('lab_sample_details', function (Blueprint $table) {
                    $table->boolean('is_result_uploaded')->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('lab_sample_details')) {
            if (Schema::hasColumn('lab_sample_details', 'is_result_uploaded')) {
                Schema::table('lab_sample_details', function (Blueprint $table) {
                    $table->dropColumn('is_result_uploaded');
                });
            }
        }    
    }
}
