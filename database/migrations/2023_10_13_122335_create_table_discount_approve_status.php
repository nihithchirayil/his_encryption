<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDiscountApproveStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('discount_approve_status')) {
            Schema::create('discount_approve_status', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',500)->nullable();
                $table->smallInteger('value')->nullable(0);
                $table->smallInteger('status')->nullable(1);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_approve_status');
    }
}
