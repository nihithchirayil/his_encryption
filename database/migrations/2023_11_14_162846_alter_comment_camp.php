<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCommentCamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('appointment')) {
            if (!Schema::hasColumn('appointment', 'comments')) {
                Schema::table('appointment', function (Blueprint $table) {
                    $table->string('comments')->nullable();
                });
            }
            if (!Schema::hasColumn('appointment', 'camp')) {
                Schema::table('appointment', function (Blueprint $table) {
                    $table->smallInteger('camp')->nullable()->default(0);
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('appointment')) {
            if (Schema::hasColumn('appointment', 'comments')) {
                Schema::table('appointment', function (Blueprint $table) {
                    $table->dropColumn('comments');
                });
            }
            if (Schema::hasColumn('appointment', 'camp')) {
                Schema::table('appointment', function (Blueprint $table) {
                    $table->dropColumn('camp');
                });
            }
        }     
    }
}
