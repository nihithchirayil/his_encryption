<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableOpVisitsAddNewColumnNextReviewDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('op_visits')) {
            if (!Schema::hasColumn('op_visits', 'next_review_date')) {
                Schema::table('op_visits', function (Blueprint $table) {
                    $table->date('next_review_date')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('op_visits')) {
            if (Schema::hasColumn('op_visits', 'next_review_date')) {
                Schema::table('op_visits', function (Blueprint $table) {
                    $table->dropColumn('next_review_date');
                });
            }
        }
    }
}
