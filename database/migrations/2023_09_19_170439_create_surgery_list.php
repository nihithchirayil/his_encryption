<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_list')) {
            Schema::create('surgery_list', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',1000)->nullable();
                $table->smallInteger('status')->nullable()->default(1);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_list')) {


            if (!Schema::hasColumn('surgery_list', 'name')) {
                Schema::table('surgery_list', function (Blueprint $table) {
                    $table->string('name',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_list', 'status')) {
                Schema::table('surgery_list', function (Blueprint $table) {
                    $table->smallInteger('status')->nullable()->default(1);
                });
            }
            if (!Schema::hasColumn('surgery_list', 'created_by')) {
                Schema::table('surgery_list', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_list', 'updated_by')) {
                Schema::table('surgery_list', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_list', 'deleted_by')) {
                Schema::table('surgery_list', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_list', 'created_at')) {
                Schema::table('surgery_list', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_list');
    }
}
