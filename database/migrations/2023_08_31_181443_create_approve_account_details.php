<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApproveAccountDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('approve_account_details')) {
            Schema::create('approve_account_details', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('head_id')->nullable();
                $table->string('vouncher_no',250)->nullable();
                $table->bigInteger('vouncher_type')->nullable();
                $table->smallInteger('status')->nullable()->comment('1-Requested,2-Approved,3-Rejected');
                $table->string('remarks',250)->nullable();
                $table->timestamp('approved_at')->nullable();
                $table->bigInteger('approved_by')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->smallInteger('updated_status')->nullable()->default(0);
                $table->timestamp('invoice_date')->nullable();
                $table->smallInteger('is_post')->nullable()->default(0);
                $table->integer('post_type')->nullable();
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approve_account_details');
    }
}
