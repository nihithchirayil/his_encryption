<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('doctor_notes')) {
            Schema::create('doctor_notes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable();
                $table->bigInteger('doctor_id')->nullable();
                $table->bigInteger('visit_id')->nullable();
                $table->text('doctor_notes')->nullable();
                $table->smallInteger('status')->nullable()->default(1);
                $table->string('application_name', 200)->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_notes');
    }
}
