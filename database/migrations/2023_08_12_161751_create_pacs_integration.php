<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacsIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pacs_integration_detail')) {
            Schema::create('pacs_integration_detail', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('patient_id')->nullable()->default(0);
                $table->string('uhid')->nullable();
                $table->bigInteger('indent_id')->nullable()->default(0);
                $table->bigInteger('bill_detail_id')->nullable()->default(0);
                $table->bigInteger('item_id')->nullable()->default(0);
                $table->string('accession_no')->nullable();
                $table->smallInteger('status')->nullable()->default(0);
                $table->bigInteger('visit_id')->nullable()->default(0);
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->text('response')->nullable();
                $table->string('check_list',550)->nullable();
                $table->timestamp('status1_created_at')->nullable();
                $table->timestamp('status1_updated_at')->nullable();
                $table->timestamp('status2_created_at')->nullable();
                $table->timestamp('status2_updated_at')->nullable();
                $table->timestamp('status3_created_at')->nullable();
                $table->timestamp('status3_updated_at')->nullable();
                $table->timestamp('status4_created_at')->nullable();
                $table->timestamp('status4_updated_at')->nullable();
                $table->timestamp('status5_created_at')->nullable();
                $table->timestamp('status5_updated_at')->nullable();
                $table->timestamp('status6_created_at')->nullable();
                $table->timestamp('status6_updated_at')->nullable();
                $table->timestamp('status7_created_at')->nullable();
                $table->timestamp('status7_updated_at')->nullable();
                $table->timestamp('status8_created_at')->nullable();
                $table->timestamp('status8_updated_at')->nullable();
                $table->timestamp('status9_created_at')->nullable();
                $table->timestamp('status9_updated_at')->nullable();
                $table->timestamp('status10_created_at')->nullable();
                $table->timestamp('status10_updated_at')->nullable();
                $table->timestamp('status11_created_at')->nullable();
                $table->timestamp('status11_updated_at')->nullable();
                $table->string('na_check_list',550)->nullable();
                $table->timestamp('status3_generated_at')->nullable();
                $table->timestamp('status4_generated_at')->nullable();
                $table->timestamp('status5_generated_at')->nullable();
                $table->timestamp('status6_generated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacs_integration_detail');
    }
}
