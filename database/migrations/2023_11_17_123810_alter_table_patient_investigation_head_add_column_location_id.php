<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePatientInvestigationHeadAddColumnLocationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_investigation_head')) {
            if (!Schema::hasColumn('patient_investigation_head', 'location_id')) {
                Schema::table('patient_investigation_head', function (Blueprint $table) {
                    $table->Integer('location_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('patient_investigation_head')) {
            if (Schema::hasColumn('patient_investigation_head', 'location_id')) {
                Schema::table('patient_investigation_head', function (Blueprint $table) {
                    $table->dropColumn('location_id');
                });
            }
        }
    }
}
