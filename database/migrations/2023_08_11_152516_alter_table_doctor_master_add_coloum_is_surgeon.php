<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDoctorMasterAddColoumIsSurgeon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('doctor_master')) {
            if (!Schema::hasColumn('doctor_master', 'is_surgeon')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->smallInteger('is_surgeon')->nullable()->default(0);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('doctor_master')) {
            if (Schema::hasColumn('doctor_master', 'is_surgeon')) {
                Schema::table('doctor_master', function (Blueprint $table) {
                    $table->dropColumn('is_surgeon');
                });
            }
        }
    }
}
