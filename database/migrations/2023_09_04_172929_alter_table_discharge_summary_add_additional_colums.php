<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDischargeSummaryAddAdditionalColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (!Schema::hasColumn('discharge_summary', 'provisional_diagnosis')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('provisional_diagnosis')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'present_illness')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('present_illness')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'history_of_alcoholism')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('history_of_alcoholism')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'past_medical_and_surgical_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('past_medical_and_surgical_history')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'family_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('family_history')->nullable();
                });
            }
            if (!Schema::hasColumn('discharge_summary', 'significant_of_lama_or_dama')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->text('significant_of_lama_or_dama')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('discharge_summary')) {
            if (Schema::hasColumn('discharge_summary', 'provisional_diagnosis')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('provisional_diagnosis');
                });
            }
            if (Schema::hasColumn('discharge_summary', 'present_illness')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('present_illness');
                });
            }
            if (Schema::hasColumn('discharge_summary', 'history_of_alcoholism')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('history_of_alcoholism');
                });
            }
            if (Schema::hasColumn('discharge_summary', 'past_medical_and_surgical_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('past_medical_and_surgical_history');
                });
            }
            if (Schema::hasColumn('discharge_summary', 'family_history')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('family_history');
                });
            }
            if (Schema::hasColumn('discharge_summary', 'significant_of_lama_or_dama')) {
                Schema::table('discharge_summary', function (Blueprint $table) {
                    $table->dropColumn('significant_of_lama_or_dama');
                });
            }
        }
    }
}
