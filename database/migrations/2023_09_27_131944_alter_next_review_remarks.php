<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNextReviewRemarks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('encounter_visits')) {
            if (!Schema::hasColumn('encounter_visits', 'next_review_remarks')) {
                Schema::table('encounter_visits', function (Blueprint $table) {
                    $table->string('next_review_remarks',100)->nullable();
                });
            }
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('encounter_visits')) {
            if (Schema::hasColumn('encounter_visits', 'next_review_remarks')) {
                Schema::table('encounter_visits', function (Blueprint $table) {
                    $table->dropColumn('next_review_remarks');
                });
            }
        }    
    }
}
