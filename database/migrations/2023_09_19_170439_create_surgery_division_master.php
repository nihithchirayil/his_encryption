<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeryDivisionMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('surgery_division_master')) {
            Schema::create('surgery_division_master', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',1000)->nullable();
                $table->string('default_percentage',1000)->nullable();
                $table->string('service_code',1000)->nullable();
                $table->smallInteger('type')->nullable()->default(0);
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->bigInteger('deleted_by')->nullable();
                $table->timestamps();
            });
        } else if (Schema::hasTable('surgery_division_master')) {


            if (!Schema::hasColumn('surgery_division_master', 'name')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->string('name',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_division_master', 'default_percentage')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->string('default_percentage',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_division_master', 'service_code')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->string('service_code',1000)->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_division_master', 'type')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->smallInteger('type')->nullable()->default(0);
                });
            }
            if (!Schema::hasColumn('surgery_division_master', 'created_by')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->bigInteger('created_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_division_master', 'updated_by')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->bigInteger('updated_by')->nullable();
                });
            }
            if (!Schema::hasColumn('surgery_division_master', 'deleted_by')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->bigInteger('deleted_by')->nullable();
                });
            }

            if (!Schema::hasColumn('surgery_division_master', 'created_at')) {
                Schema::table('surgery_division_master', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_division_master');
    }
}
