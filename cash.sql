select * from (select tg.name as bill_type,sum(cc.paid_amount) as bill_amount,
0 as refund,ur.id
FROM cash_collection cc
join patient_master pat on pat.uhid=cc.uhid
join bill_tag tg on tg.code=cc.bill_tag
join users ur on ur.id = cc.cash_collected_by
WHERE 1=1  and cc.cash_collected_accounting_date::date between 'Jul-31-2023' and 'Jul-31-2023'
group by tg.name,ur.id
union All
select tg.name as bill_type,0 as bill_amount,sum(cc.paid_amount) as refund,ur.id
FROM bill_refund cc
left join patient_master pat on pat.uhid=cc.uhid
join bill_return_head brh on brh.id = cc.return_id and brh.application_name<>'ACC_APP'
join bill_tag tg on tg.code=brh.return_type
join users ur on ur.id = cc.paid_by
WHERE
1=1  and cc.accounting_date::date between 'Jul-31-2023' and 'Jul-31-2023'
group by tg.name,ur.id)a order by id
424ms
/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php:261
nammudedb_prod
with collection  as (
select 'TOTAL BILL AMOUNT' as typ,coalesce(sum(paid_amount),0) OPCASH,u.name as user_name,u.id as id
from cash_collection cc
join patient_master pat on pat.uhid=cc.uhid
join users u on u.id=cc.cash_collected_by
join bill_tag tg on tg.code=cc.bill_tag
where 1=1  and cash_collected_accounting_date::date between 'Jul-31-2023' and 'Jul-31-2023'
group by u.name,u.id
union all
select 'OP CASH COLLECTION' as typ,coalesce(sum(paid_amount),0) OPCASH,u.name as user_name,u.id as id
from cash_collection cc
join patient_master pat on pat.uhid=cc.uhid
join users u on u.id=cc.cash_collected_by
join bill_tag tg on tg.code=cc.bill_tag
where 1=1  and cash_collected_accounting_date::date between 'Jul-31-2023' and 'Jul-31-2023'
and cc.collection_mode =1
group by u.name,u.id
union   all
select 'OP CARD/OTH COLLECTION' as typ,coalesce(sum(paid_amount),0) ,u.name as user_name,u.id as id
from cash_collection cc
join patient_master pat on pat.uhid=cc.uhid
join users u on u.id=cc.cash_collected_by
join bill_tag tg on tg.code=cc.bill_tag
where 1=1  and cash_collected_accounting_date::date between 'Jul-31-2023' and 'Jul-31-2023'
and cc.collection_mode <> 1
group by u.name,u.id
union   all
select 'ADVANCE ADJUST OP' as typ,coalesce(sum(amount),0) ,u.name as user_name,u.id as id
from advance_collection cc
join patient_master pat on pat.id=cc.patient_id
join users u on u.id=cc.created_by
join bill_tag tg on tg.code=cc.bill_tag
where 1=1  and account_date::date between 'Jul-31-2023' and 'Jul-31-2023'
and cc.transaction_type =2
group by u.name,u.id
union   all
select 'REFUND OP CASH' as typ,coalesce(sum(paid_amount),0),u.name as user_name,u.id as id
from bill_refund cc
left join patient_master pat on pat.uhid=cc.uhid
join users u on u.id=cc.created_by
join bill_return_head brh on brh.id = cc.return_id
join bill_tag tg on tg.code=brh.return_type
where 1=1 and brh.application_name<>'ACC_APP'  and cc.accounting_date::date between 'Jul-31-2023' and 'Jul-31-2023'
group by u.name,u.id
)
select user_name,id,
sum(case when typ='TOTAL BILL AMOUNT' then OPCASH else 0 end) as bill_collecction,
sum(case when typ='OP CASH COLLECTION' then OPCASH else 0 end) as cash_collection,
sum(case when typ='OP CARD/OTH COLLECTION' then OPCASH else 0 end) as card_other_collection,
sum(case when typ='ADVANCE ADJUST OP' then OPCASH else 0 end) as advance_adjust ,
sum(case when typ='REFUND OP CASH' then OPCASH else 0 end) as refund_cash,
(sum(case when typ='OP CASH COLLECTION' then OPCASH else 0 end)-
sum(case when typ='REFUND OP CASH' then OPCASH else 0 end)) as cash_in_hand
from collection
group by user_name,id
order by user_name