@php
$padding_size = 0;
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 5*$padding_number.'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
@endphp
                @if(sizeof($fixedAsset_result)>0)
                @foreach($fixedAsset_result as $k)
                @php $addition = ($k->dr_amount) - ($k->cr_amount); @endphp
                @if($k->is_ledger == 0)
                    @php $fn_name = "onclick = getFixedAssetExpand(this,'$k->id')"; @endphp
                @else
                    @php $fn_name = ""; @endphp
                @endif
                <!--  ============================================ SALES ROW ============================ -->
                <tr style="cursor:pointer" {{$fn_name}}  class="{{$show_row_group}}" {{$fn_name}}>
                    <td class="common_td_rules" style="{{$padding}}">
                    @if($k->is_ledger == 0) <b> {{$k->ledger_name}} </b> @else  {{$k->ledger_name}} @endif
                    </td>
                    <td class="td_common_numeric_rules">
                        {{$k->open_bal}}
                    </td>
                    <td class="td_common_numeric_rules">{{$addition}}</td>
                    <td class=" td_common_numeric_rules"></td>
                    <td class="td_common_numeric_rules"></td>
                    <td class=" td_common_numeric_rules" style="padding-right: 25px !important"> {{ $k->cls }} </td>
                </tr>
                @endforeach
                @endif
