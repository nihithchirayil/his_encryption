@php
$padding_size = 0;
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 5*$padding_number.'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
@endphp
               @if(isset($trade_payable_array) && count($trade_payable_array) > 0)
               @foreach($trade_payable_array as $i => $j)
                @php 
                $growth_array_ttl[] = ''; $row_total_ttl = $m = 0;$sname = explode('#',$i); 
                @endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr class="{{$show_row_group}}" style="cursor: pointer;" onclick="getReceivableSub(this,'{{$sname[1]}}')">
                    <td class="sticky-col first-col" style="width: 15%;cursor: pointer;{{$padding}}">{{$sname[0]}}</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p] ? number_format($j[$p], 2, '.', ','): 0}}
                        @php  
                            $row_total_ttl += $j[$p]; 
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $row_total_ttl += 0; 
                        @endphp
                        @endif
                    </td>
                    @endforeach
                </tr>
                @endforeach
                @endif