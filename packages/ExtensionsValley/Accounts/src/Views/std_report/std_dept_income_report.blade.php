<style>
    .view {
        margin: auto;
        width: 100%;
    }

    .wrapper {
        position: relative;
        overflow: auto;
        /*        border: 1px solid black;*/
        white-space: nowrap;
    }

    .sticky-col {
        position: -webkit-sticky;
        position: sticky;
        background-color: #efeded !important;
    }

    .first-col {
        width: 150px;
        min-width: 150px !important;
        max-width: 170px !important;
        left: 0px;
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        background-color:#e9f7f4 !important;
    }

    .second-col {
        width: 15%;
        min-width: 15%;
        max-width: 15%;
        left: 100px;
    }
    .table>tbody>tr>td{
        vertical-align: middle !important;
    }
    .table>tbody>tr{
        height: 35px !important;
        border-bottom: 1px solid #efcdcd;
    }

</style>
<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th class="sticky-col first-col" style="min-width: 275px !important;background-color: #36A693 !important">Details</th>
                    @foreach($month_array_data_head as $mnth)
                    <th style=" min-width: 75px !important;background-color: #36A693 !important">{{$mnth}}</th>
                    @endforeach
                    <th style=" min-width: 85px !important;padding-right: 20px;background-color: #36A693 !important"> Till Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sales_ttl_array as $i => $j)
                @php $growth_array_ttl[] = ''; $row_total_ttl = $m = 0; $sname = explode('#',$i)@endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr>
                    <td class="sticky-col first-col" style="width: 15%;cursor: pointer" onclick="getDptSub(this,'{{$sname[1]}}','cr')">Sales</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p]}}
                        @php  
                            $row_total_ttl += $j[$p]; 
                            $sales_total_array[] = $j[$p];
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $row_total_ttl += 0; 
                            $sales_total_array[] = 0;
                        @endphp
                        @endif
                    </td>
                    @endforeach
                    @php $sales_total_array[] = round($row_total_ttl,2); @endphp
                    <td style="width: 15%">{{round($row_total_ttl,2)}}</td>
                </tr>
                @endforeach
                @foreach($opening_data_array as $i => $j)
                @php $growth_array_ttl[] = ''; $row_total_ttl = $m = 0; @endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr>
                    <td class="sticky-col first-col" style="width: 15%;">Opening Stock</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p]}}
                        @php  
                            $growth_array_ttl[] = $j[$p];  
                            $row_total_ttl += $j[$p]; 
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $growth_array_ttl[] = 0; 
                            $row_total_ttl += 0; 
                        @endphp
                        @endif
                    </td>
                    @endforeach
                    <td style="width: 15%">{{round($row_total_ttl,2)}}</td>
                </tr>
                @foreach($pharmacy_data_array as $k => $v)
                @php $growth_array[] = ''; $row_total = $m = 0; @endphp
                <!--  ============================================ SALES ROW ============================ -->
                <tr>
                    @php $sname = explode('#',$k) @endphp
                    <td class="sticky-col first-col" style="background-color:#e9f7f4 !important;width: 15%;cursor: pointer" onclick="getDptSub(this,'{{$sname[1]}}','dr')">
                        {{$sname[0]}}
                    </td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($v[$p]))
                        @php $growth_array[] = $v[$p]; $row_total += $v[$p]; @endphp
                        {{$v[$p]}}
                        @else
                        {{0}}
                        @php $growth_array[] = 0; $row_total += 0; @endphp
                        @endif
                    </td>
                    @endforeach
                    <td style="width: 15%">{{round($row_total,2)}}</td>
                </tr>
                @endforeach
                @endforeach
                @foreach($closing_data_array as $i => $j)
                @php $growth_array_ttl[] = ''; $row_total_ttl = $m = 0; @endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr>
                    <td class="sticky-col first-col" style="width: 15%;">Closing Stock</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p]}}
                        @php  
                            $growth_array_ttl[] = $j[$p];  
                            $row_total_ttl += $j[$p]; 
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $growth_array_ttl[] = 0; 
                            $row_total_ttl += 0; 
                        @endphp
                        @endif
                    </td>
                    @endforeach
                    <td style="width: 15%">{{round($row_total_ttl,2)}}</td>
                </tr>
                @endforeach
                @foreach($cogs as $k => $v)
                @php $growth_array[] = ''; $row_total = $m = 0; @endphp
                <!--  ============================================ SALES ROW ============================ -->
                <tr style="background-color:#bae3ba !important;font-weight: bold">
                    @php $sname = explode('#',$k) @endphp
                    <td class="sticky-col first-col" style="background-color:#bae3ba !important;width: 15%;cursor: pointer">
                       COGS
                    </td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($v[$p]))
                        @php 
                            $cogs_array[] = $v[$p]; 
                            $row_total += $v[$p]; 
                        @endphp
                            {{$v[$p]}}
                        @else
                            {{0}}
                        @php 
                            $cogs_array[] = 0; 
                            $row_total += 0; 
                        @endphp
                        @endif
                    </td>
                    @endforeach
                    @php $cogs_array[] = round($row_total,2) @endphp
                    <td style="width: 15%">{{round($row_total,2)}}</td>
                </tr>
                @endforeach
                <tr style="background-color:#f5c9df !important;font-weight: bold">
                    <td class="sticky-col first-col" style="background-color:#f5c9df !important;width: 15%;cursor: pointer">
                       Percentage
                    </td>
                    @for($x=0; $x<sizeof($sales_total_array);$x++)
                    <td style="width: 15%">
                        @if(isset($sales_total_array[$x]) && isset($cogs_array[$x]) && $sales_total_array[$x] != 0)
                        @php 
                            $perc = $cogs_array[$x]/$sales_total_array[$x]*100;
                            $perc_arr[] = 100-$perc;
                        @endphp
                            {{round($perc,2)}}%
                        @else
                            {{0}}
                        @php 
                            $perc = 0;
                            $perc_arr[] = 0 ;
                        @endphp
                        @endif
                    </td>
                    @endfor
                </tr>
                <tr style="background-color:#9bffda !important;font-weight: bold">
                    <td class="sticky-col first-col" style="background-color:#9bffda !important;width: 15%;cursor: pointer">
                       Margin
                    </td>
                    @php $marg_total = 0 @endphp
                    @for($x=0; $x< sizeof($perc_arr);$x++)
                    <td style="width: 15%">
                        @if(isset($perc_arr[$x]) && $perc_arr[$x] != 0)
                            {{round($perc_arr[$x],2)}}%
                        @else
                            {{0}}
                        @endif
                    </td>
                    @endfor
                </tr>
            </tbody>
        </table>
    </div>
</div>
