<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
        @if(isset($consolidate_report_ind) && $consolidate_report_ind == '0')
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th style="min-width: 75px !important">Booking Date</th>
                    <th style="min-width: 275px !important">Name of party</th>
                    <th style="min-width: 75px !important"> Tds rate</th>
                    <th style="min-width: 75px !important;"> Total TDS applicable amount</th>
                    <th style="min-width: 75px !important;"> TDS amount</th>
                    <th style="min-width: 75px !important;"> Challan No.</th>
                    <th style="min-width: 75px !important;"> Challan Date</th>
                    <th style="min-width: 75px !important;"> BSR Code</th>
                    <th style="min-width: 95px !important;"> Pan No</th>
                </tr>
            </thead>
            <tbody>
                @php $total_amount = $total_tds = 0;@endphp
                @foreach($tds_194c_result as $k)
                <!--  ============================================ SALES ROW ============================ -->
                <tr onclick="editLedgerHeder('{{ $k->head_id }}')" style="cursor:pointer">
                    <td class="common_td_rules" >
                        {{$k->inv}}
                    </td>
                    @if($k->ledger_name != '')
                        @php $ledger_name = $k->ledger_name; @endphp
                    @else
                        @php $ledger_name = $k->ledger_name2; @endphp
                    @endif
                    <td class="common_td_rules" >
                        {{$ledger_name}}
                    </td>
                     <td class="td_common_numeric_rules" >
                        @if($k->total_amount != 0) {{round(($k->td_amount/$k->total_amount)*100,2)}} %@else - @endif
                    </td>
                    <td class=" td_common_numeric_rules">
                        {{$k->total_amount}}
                        @php $total_amount += $k->total_amount; @endphp
                    </td>
                    <td class="td_common_numeric_rules">
                        @if($k->cr_dr == 'cr')
                        {{$k->td_amount}}
                        @php $total_tds += $k->td_amount; @endphp
                        @else
                        -{{$k->td_amount}}
                        @php $total_tds = $total_tds -$k->td_amount;@endphp
                        @endif
                    </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="3"> <b>Total</b></td>
                    <td class="td_common_numeric_rules"> <b>{{ $total_amount }}</b></td>
                    <td class="td_common_numeric_rules"> <b>{{ $total_tds }}</b></td>
                </tr>
            </tbody>
        </table>
        @else
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th style="min-width: 275px !important">Name of party</th>
                    <th style="min-width: 75px !important"> Tds rate</th>
                    <th style="min-width: 75px !important;"> Total TDS applicable amount</th>
                    <th style="min-width: 75px !important;"> TDS amount</th>
                    <th style="min-width: 75px !important;"> Challan No.</th>
                    <th style="min-width: 75px !important;"> Challan Date</th>
                    <th style="min-width: 75px !important;"> BSR Code</th>
                    <th style="min-width: 95px !important;"> Pan No</th>
                </tr>
            </thead>
            <tbody>
                @php $total_amount = $total_tds = 0;@endphp
                @foreach($tds_194c_result_cons as $k)
                <!--  ============================================ SALES ROW ============================ -->
                <tr>

                    <td class="common_td_rules" >
                        {{$k->ledger_name}}
                    </td>
                     <td class="td_common_numeric_rules" >
                        @if($k->total_amount != 0) {{round(($k->td_amount/$k->total_amount)*100,2)}} %@else - @endif
                    </td>
                    <td class=" td_common_numeric_rules">
                        {{$k->total_amount}}
                        @php $total_amount += $k->total_amount; @endphp
                    </td>
                    <td class="td_common_numeric_rules">
                        {{$k->td_amount}}
                        @php $total_tds = $total_tds+$k->td_amount; @endphp

                    </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2"> <b>Total</b></td>
                    <td class="td_common_numeric_rules"> <b>{{ $total_amount }}</b></td>
                    <td class="td_common_numeric_rules"> <b>{{ $total_tds }}</b></td>
                </tr>
            </tbody>
        </table>
        @endif
    </div>
</div>
