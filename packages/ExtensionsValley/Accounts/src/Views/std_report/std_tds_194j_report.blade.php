<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 500px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th style="min-width: 75px !important">Sl No</th>
                    <th style="min-width: 275px !important">Name</th>
                    <th style="min-width: 75px !important"> PAN</th>
                    <th style="min-width: 75px !important;"> Month</th>
                    <th style="min-width: 75px !important;"> Gross Amount</th>
                    <th style="min-width: 75px !important;"> TDS Amount</th>
                    <th style="min-width: 75px !important;"> NET Amount</th>
                </tr>
            </thead>
            <tbody>
                @php $i=1; $total_gross = $total_tds = $total_net = 0; @endphp
                @foreach($tds_194j_result as $k)
                <!--  ============================================ SALES ROW ============================ -->
                <tr>
                <td class="common_td_rules" >
                        {{$i}}
                    </td>
                    <td class="common_td_rules" >
                        {{$k->doctor_name}}
                    </td>
                    <td class="common_td_rules" >
                    </td>
                     <td class="common_td_rules" >
                        {{ @$to_month ? $to_month :''}}
                    </td>
                    <td class=" td_common_numeric_rules">
                        {{ @$k->net_amount ? $k->net_amount : 0 }}
                    </td>
                    <td class="td_common_numeric_rules">
                        {{ @$k->tds_amount ? $k->tds_amount: 0 }}
                    </td>
                    <td class="td_common_numeric_rules">
                        {{$k->net_amount - $k->tds_amount}}
                    </td>
                </tr>
                @php 
                $i++; 
                $total_gross += $k->net_amount;
                $total_tds += $k->tds_amount;
                $total_net += ($k->net_amount - $k->tds_amount);
                @endphp
                @endforeach 
                <tr>
                    <td class="common_td_rules" colspan="4"><b>Total</b></td>
                    <td class="td_common_numeric_rules"><b>{{ $total_gross }}</b></td>
                    <td class="td_common_numeric_rules"><b>{{ $total_tds }}</b></td>
                    <td class="td_common_numeric_rules"><b>{{ $total_net }}</b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
