<style>
    .view {
        margin: auto;
        width: 100%;
    }

    .wrapper {
        position: relative;
        overflow: auto;
        /*        border: 1px solid black;*/
        white-space: nowrap;
    }

    .sticky-col {
        position: -webkit-sticky;
        position: sticky;
        background-color: #efeded !important;
    }

    .first-col {
        width: 150px;
        min-width: 150px !important;
        max-width: 170px !important;
        left: 0px;
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        background-color:#e9f7f4 !important;
    }

    .second-col {
        width: 15%;
        min-width: 15%;
        max-width: 15%;
        left: 100px;
    }
    .table>tbody>tr>td{
        vertical-align: middle !important;
    }
    .table>tbody>tr{
        height: 35px !important;
        border-bottom: 1px solid #efcdcd;
    }

</style>
<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th class="sticky-col first-col" style="min-width: 275px !important;background-color: #36A693 !important">Details</th>
                    @foreach($month_array_data_head as $mnth)
                    <th style=" min-width: 75px !important;background-color: #36A693 !important">{{$mnth}}</th>
                    @endforeach
                    <th style=" min-width: 75px !important;background-color: #36A693 !important"> Growth</th>
                    <th style=" min-width: 85px !important;padding-right: 20px;background-color: #36A693 !important"> Till Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_array as $k => $v)
                @php $growth_array[] = ''; $row_total = $m = 0; @endphp
                <!--  ============================================ SALES ROW ============================ -->
                <tr>
                    @php $sname = explode('#',$k) @endphp
                    <td class="sticky-col first-col" style="background-color:#e9f7f4 !important;width: 15%;cursor: pointer" onclick="getIncomeSub(this,'{{$sname[1]}}','cr')">
                        {{$sname[0]}}
                    </td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($v[$p]))
                        @php $growth_array[] = $v[$p]; $row_total += $v[$p]; @endphp
                        {{$v[$p]}}
                        @else
                        {{0}}
                        @php $growth_array[] = 0; $row_total += 0; @endphp
                        @endif
                    </td>
                    @endforeach
                    <td style="width: 15%"> 
                        @php 
                            $last_column = $growth_array[count($growth_array)-1]; 
                            $second_last_column = $growth_array[count($growth_array) -2];
                        @endphp
                        @if($second_last_column != 0 && $last_column != 0)
                            @php  
                                $growth_per = ($last_column-$second_last_column)/$second_last_column; 
                            @endphp
                        @else
                            @php $growth_per = 0;@endphp 
                        @endif
                        {{round($growth_per*100,2)}}%
                    </td>
                    <td style="width: 15%">{{$row_total}}</td>
                </tr>
                @endforeach
                @foreach($ttl_data_array as $i => $j)
                @php $growth_array_ttl[] = ''; $row_total_ttl = $m = 0; @endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr style=" background-color: #fdfdc2 !important;font-weight: bold">
                    <td class="sticky-col first-col" style="width: 15%;background-color: #fdfdc2 !important">Total Sales</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p]}}
                        @php  
                            $growth_array_ttl[] = $j[$p];  
                            $row_total_ttl += $j[$p]; 
                            $sales_total_array[] = $j[$p];
                            $sales_total_array_fixed[] = $j[$p];
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $growth_array_ttl[] = 0; 
                            $row_total_ttl += 0; 
                            $sales_total_array[] = 0;
                            $sales_total_array_fixed[] = 0;
                        @endphp
                        @endif
                    </td>
                    @endforeach
                    <td style="width: 15%"> 
                        @php 
                            $last_column = $growth_array_ttl[count($growth_array_ttl)-1]; 
                            $second_last_column = $growth_array_ttl[count($growth_array_ttl) -2]; 
                        @endphp
                        @if($second_last_column != 0 && $last_column != 0)
                            @php $growth_per_ttl = ($last_column-$second_last_column)/$second_last_column;@endphp
                        @else
                            @php $growth_per_ttl = 0; @endphp
                        @endif
                         
                        {{round($growth_per_ttl*100,2)}}%
                        @php 
                            $sales_total_array[] = round($growth_per_ttl*100,2);
                            $sales_total_array[] = round($row_total_ttl,2);
                        @endphp
                    </td>
                    <td style="width: 15%">{{round($row_total_ttl,2)}}</td>
                </tr>
                @endforeach
                @foreach($pharmacy_data_array as $a => $b)
                @php 
                $growth_array[] = ''; $row_total = $m = 0;
                $ttl_expense_name[] = $a;
                $pname = explode('#',$a)
                @endphp
                <!--  ============================================ PHARMACY/PURCHASE ROW ============================ -->
                <tr>
                    @php $divd_amount = $sales_total_array_fixed[count($sales_total_array_fixed) -1] @endphp
                    <td class="sticky-col first-col" style="background-color:#e9f7f4 !important;width: 15%;cursor: pointer" onclick="getExpSub(this,'{{$pname[1]}}','dr','{{$divd_amount}}')">
                        {{$pname[0]}}
                    </td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($b[$p]))
                            @php 
                                $growth_array[] = $b[$p]; 
                                $row_total += $b[$p]; 
                            @endphp
                        {{$b[$p]}}
                        @else
                        {{0}}
                            @php 
                                $growth_array[] = 0; 
                                $row_total += 0; 
                            @endphp
                        @endif
                    </td>
                    @endforeach
                    <td style="width: 15%"> 
                        @php 
                            $last_column = $growth_array[count($growth_array)-1]; 
                            $second_last_column = $growth_array[count($growth_array) -2];
                        @endphp
                        @if($second_last_column != 0 && $last_column != 0)
                            @php  $growth_per = ($last_column-$second_last_column)/$second_last_column; @endphp
                        @else
                            @php $growth_per = 0;@endphp 
                        @endif
                        {{-- round($growth_per*100,2) --}}
                    </td>
                    <td style="width: 15%">{{$row_total}}</td>
                </tr>
                <!--  ============================================ TOTAL EXPENSE ROW ============================ -->
                @endforeach
                @foreach($month_array_data as $p)
                @php 
                $ps = @$pharmacy_data_array[$ttl_expense_name[0]][$p] ? $pharmacy_data_array[$ttl_expense_name[0]][$p] : 0;
                $ie = @$pharmacy_data_array[$ttl_expense_name[1]][$p] ? $pharmacy_data_array[$ttl_expense_name[1]][$p] : 0; 
                $ps_ie_total[] = $ps+$ie;
                @endphp
                @endforeach
                <tr style=" background-color: #fdfdc2;font-weight: bold">
                    <td class="sticky-col first-col" style="background-color: #fdfdc2 !important;" >Total Expenses</td>
                    @php $till_total_exp = $last_column = $second_last_column = $growth_per = 0; @endphp
                    @foreach($ps_ie_total as $pi)
                    <td style="width: 15%">
                        {{$pi}}
                       @php $till_total_exp += $pi; @endphp
                    </td>
                    @endforeach
                        @php
                            $last_column = $ps_ie_total[count($ps_ie_total)-1]; 
                            $second_last_column = $ps_ie_total[count($ps_ie_total) -2];
                        @endphp
                    @if($second_last_column != 0 && $last_column != 0)
                            @php  $growth_per = ($last_column-$second_last_column)/$second_last_column; @endphp
                    @else
                            @php $growth_per = 0;@endphp 
                    @endif
                    @php
                        $ps_ie_total[] = 'na';//round($growth_per*100,2);
                        $ps_ie_total[] = round($till_total_exp,2);
                    @endphp
                    <td style="width: 15%">{{-- round($growth_per*100,2) --}}</td>
                    <td style="width: 15%">{{round($till_total_exp,2)}}</td>
                </tr>
                <!--  ============================================ LOSS PROFIT ROW ============================ -->
                <tr style=" background-color: #dfc0ae;font-weight: bold">
                    <td class="sticky-col first-col" style="background-color: #dfc0ae !important;" >Loss/Profit</td>
                    @php $r=0; @endphp
                    @foreach($ps_ie_total as $pi)
                    @if($pi =='na')
                    <td style="width: 15%"></td>
                    @else
                    @if($pi<0) @php $pi = $pi*-1 @endphp @endif
                    <td style="width: 15%">{{round(($sales_total_array[$r]) - ($pi),2)}}</td>
                    @endif
                    @php $r++; @endphp
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
</div>
