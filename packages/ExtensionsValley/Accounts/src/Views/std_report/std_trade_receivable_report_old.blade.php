<style>
    .view {
        margin: auto;
        width: 100%;
    }

    .wrapper {
        position: relative;
        overflow: auto;
        /*        border: 1px solid black;*/
        white-space: nowrap;
    }

    .sticky-col {
        position: -webkit-sticky;
        position: sticky;
        background-color: #efeded !important;
    }

    .first-col {
        width: 150px;
        min-width: 150px !important;
        max-width: 170px !important;
        left: 0px;
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        background-color:#e9f7f4 !important;
    }

    .second-col {
        width: 15%;
        min-width: 15%;
        max-width: 15%;
        left: 100px;
    }
    .table>tbody>tr>td{
        vertical-align: middle !important;
    }
    .table>tbody>tr{
        height: 35px !important;
        border-bottom: 1px solid #efcdcd;
    }

</style>
<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th class="sticky-col first-col" style="min-width: 275px !important;background-color: #36A693 !important">Details</th>
                    @foreach($month_array_data_head as $mnth)
                    <th style=" min-width: 75px !important">{{$mnth}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($trade_receivable_array as $i => $j)
                @php $growth_array_ttl[] = ''; $row_total_ttl = $m = 0; @endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr>
                    <td class="sticky-col first-col" style="width: 15%;cursor: pointer">{{$i}}</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p]}}
                        @php  
                            $row_total_ttl += $j[$p]; 
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $row_total_ttl += 0; 
                        @endphp
                        @endif
                    </td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
