<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed" id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th style="min-width: 275px !important">Name of party</th>
                    <th style="min-width: 75px !important"> Tds rate</th>
                    <th style="min-width: 75px !important;"> Total TDS applicable amount</th>
                    <th style="min-width: 75px !important;"> TDS amount</th>
                    <th style="min-width: 75px !important;"> Challan No.</th>
                    <th style="min-width: 75px !important;"> Challan Date</th>
                    <th style="min-width: 75px !important;"> BSR Code</th>
                    <th style="min-width: 95px !important;"> Pan No</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tds_194c_result as $k)
                <!--  ============================================ SALES ROW ============================ -->
                <tr>
                    <td class="common_td_rules" >
                        {{$k->ledger_name}}
                    </td>
                     <td class="td_common_numeric_rules" >
                        
                    </td>
                    <td class=" td_common_numeric_rules">
                        {{$k->total_amount}}
                    </td>
                    <td class="td_common_numeric_rules">
                        {{$k->td_amount}}
                    </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                    <td class="common_td_rules"> </td>
                </tr>
                @endforeach 
            </tbody>
        </table>
    </div>
</div>
