@php
$padding_size = 0;
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
    $padding_size = '30px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
@endphp
@foreach($data_array as $k => $v)
@php $growth_array[] = ''; $row_total = $m = 0; @endphp
<tr class="{{$show_row_group}}">
    @php $sname = explode('#',$k) @endphp
    <td class="sticky-col first-col" style="cursor: pointer;{{$padding}}">
        {{$sname[0]}}
    </td>
    @foreach($month_array_data as $p)
    <td>
        @if(isset($v[$p]))
        @php $growth_array[] = $v[$p]; $row_total += $v[$p]; @endphp
        {{$v[$p]}}
        @else
        {{0}}
        @php $growth_array[] = 0; $row_total += 0; @endphp
        @endif
    </td>
    @endforeach
    <td> 
        @php 
        $last_column = $growth_array[count($growth_array)-1]; 
        $second_last_column = $growth_array[count($growth_array) -2];
        @endphp
        @if($second_last_column != 0)
        @php  
        $growth_per = ($last_column-$second_last_column)/$second_last_column; 
        @endphp
        @else
        @php $growth_per = 0;@endphp 
        @endif
        {{round($growth_per*100,2)}}
    </td>
    <td>{{$row_total}}</td>
</tr>
@endforeach
