@php
$padding_size = 0;
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 5*$padding_number.'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
@endphp
             @foreach($data_array as $i => $j)
                @php $growth_array_ttl[] = ''; $row_total_ttl = $m = 0; $sname = explode('#',$i)@endphp
                <!--  ============================================ TOTAL SALES ROW ============================ -->
                <tr class="{{$show_row_group}}">
                    <td class="sticky-col first-col" style="cursor: pointer;{{$padding}}" onclick="getDptSub(this,'{{$sname[1]}}','cr')">{{$sname[0]}}</td>
                    @foreach($month_array_data as $p)
                    <td style="width: 15%">
                        @if(isset($j[$p]))
                        {{$j[$p]}}
                        @php  
                            $row_total_ttl += $j[$p]; 
                            $sales_total_array[] = $j[$p];
                        @endphp
                        @else
                        {{0}}
                        @php 
                            $row_total_ttl += 0; 
                            $sales_total_array[] = 0;
                        @endphp
                        @endif
                    </td>
                    @endforeach
                    @php $sales_total_array[] = round($row_total_ttl,2); @endphp
                    <td style="width: 15%">{{round($row_total_ttl,2)}}</td>
                </tr>
                @endforeach