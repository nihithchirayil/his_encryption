<div class="view">
    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed"
            id="result_data_table">
            <thead>
                <tr class="table_header_bg">
                    <th style="min-width: 75px !important">Account</th>
                    <th style="min-width: 75px !important"> As on <span style="padding-left: 10px;">{{ @$from_date ? $from_date : '' }}</span></th>
                    <th style="min-width: 75px !important;"> Addition</th>
                    <th style="min-width: 75px !important;">Revaluation </th>
                    <th style="min-width: 75px !important;"> Sales/Transfer</th>
                    <th style="min-width: 75px !important;"> As on <span style="padding-left: 10px;">{{ @$to_date ? $to_date : '' }}</span></th>
                </tr>
            </thead>
            <tbody>
                @if(sizeof($fixedAsset_result)>0)
                @php $total_addition = 0;$total_from_date = 0;$total_to_date = 0;@endphp
                @foreach($fixedAsset_result as $k)
                @php
                $addition = ($k->dr_amount) - ($k->cr_amount);
                $total_addition += $addition;
                $total_from_date += $k->open_bal;
                $total_to_date += $k->cls;
                @endphp
                @if($k->is_ledger == 0)
                    @php $fn_name = "onclick = getFixedAssetExpand(this,'$k->id')"; @endphp
                @else
                    @php $fn_name = ""; @endphp
                @endif
                <!--  ============================================ SALES ROW ============================ -->
                <tr style="cursor:pointer" {{$fn_name}}>
                    <td class="common_td_rules">
                    @if($k->is_ledger == 0) <b> {{$k->ledger_name}} </b> @else  {{$k->ledger_name}} @endif
                    </td>
                    <td class="td_common_numeric_rules">
                       <b> {{$k->open_bal}} </b>
                    </td>
                    <td class="td_common_numeric_rules"><b>{{$addition}}</b></td>
                    <td class=" td_common_numeric_rules"></td>
                    <td class="td_common_numeric_rules"></td>
                    <td class=" td_common_numeric_rules" style="padding-right: 25px !important"> <b>{{ $k->cls }} </b></td>
                </tr>
                @endforeach
                <tr>
                    <td><b>Total</b></td>
                    <td class="td_common_numeric_rules"><b>{{ $total_from_date }}</b></td>
                    <td class="td_common_numeric_rules"><b>{{ $total_addition }}</b></td>
                    <td></td>
                    <td></td>
                    <td class="td_common_numeric_rules" style="padding-right: 25px !important"><b>{{ $total_to_date }}</b></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
