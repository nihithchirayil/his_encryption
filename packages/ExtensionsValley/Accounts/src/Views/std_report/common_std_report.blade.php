@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_address" value="{{$hospital_address}}">
<input type="hidden" id="hospital_header" value="{{$hospital_header}}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<?php $company = \DB::table('company')->pluck('sub_code'); ?>
<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body">
                <div class="box no-border no-margin">
                    <div class="box-body" style="padding-bottom:15px;">
                        <div class="col-md-12 no-padding table_header_bg" style="height: 30px !important;text-align: right !important;padding-right: 40px !important;margin-bottom: 6px !important;">
                            <span class='pull-right'><h5>{{$title}}</h5></span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                                @if(isset($is_to_date))
                                <div class="col-md-2 padding_sm date_filter_div">
                                    <div class="mate-input-box">
                                        <label class="filter_label">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" autocomplete="off" name="bill_date_from"
                                               value="{{ date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="bill_date_from">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm date_filter_div">
                                    <div class="mate-input-box">
                                        <label class="filter_label">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" autocomplete="off" name="bill_date_to"
                                               value="{{ date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="bill_date_to">
                                    </div>
                                </div>
                                @elseif(isset($is_month_picker))
                                <div class="col-md-2 padding_sm date_filter_div">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Month</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" autocomplete="off" name="bill_date_to"
                                               value="{{ date('M-Y')}}" class="form-control datepicker_month filters" placeholder="YYYY-MM-DD" id="bill_date_to">
                                    </div>
                                </div>
                                @else
                                <div class="col-md-2 padding_sm date_filter_div">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Till Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" autocomplete="off" name="bill_date_to"
                                               value="{{ date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="bill_date_to">
                                    </div>
                                </div>
                                @endif
                                @if(isset($consolidate_report))
                                <div class="col-md-2 padding_sm" style="margin-top: 15px;">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" class="form-control filters" value = "1" name="consolidate_report"  id="consolidate_report">
                                        <label class="text-blue " for="">Consolidate</label>
                                    </div>
                               </div>
                                @endif
                                <div class="col-md-6 pull-right" style="text-align:right; padding: 0px; margin-top: 10px;">
                                    <a class="btn light_purple_bg" onclick="ResultData();"  name="search_results" id="search_results">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        Search
                                    </a>
                                    <a class="btn light_purple_bg disabled" name="csv_results" id="csv_results" onclick="exceller_template_styled_std('std_report',12);">
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                        Excel
                                    </a>
                                    <button onclick="datarst();" type="reset" class="btn light_purple_bg"  name="clear_results" id="clear_results">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                        Reset
                                    </button>
                                </div>
                                {!! Form::token() !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ResultDataContainer" style="max-height: 650px;display:none;font-family:poppinsregular">
                    <div style="background:#686666;">
                        <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                              width: 100%;
                              padding: 15px;" id="ResultsViewArea">

                        </page>
                </div>
            </div>
        </div>
    </div>
</div>
<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/std_reports/std_common_account_report.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/std_reports/std_pnl_report.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/std_reports/std_dpt_income_report.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/std_reports/std_receivable_report.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/std_reports/std_fixed_asset_report.js")}}"></script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
