<div class="theadscroll" style="position: relative; height: 350px;" id="div_list_supplier_data">

    <div class="row codfox_container">
        <div class="col-md-12 no-padding">
            <div class="col-md-12 padding_sm">
                <table
                    class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                    style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="width:5%">Sl No</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $k = 0; @endphp
                    @if (isset($result) && count($result) > 0)
                    @php $newNarration = ''; @endphp
                        @for ($j = 0; $j < sizeof($result['column_name']); $j++)

                        @if ($result['column_name'][$j] == 'Naration' && $result['new_value'][$j] != $newNarration)
                        @php $k = $k+1 @endphp
                                <tr style="height: 25px !important;">
                                    <td class="common_td_rules">{{ $k }}</td>
                                    <td class=" common_td_rules">
                                        @php
                                            echo $result['column_name'][$j] . ' is changed from ' . $result['old_value'][$j] . ' to ' . $result['new_value'][$j] . ' on ' . $result['date_time'][$j] . ' by ' . $result['user_name'][$j];
                                        @endphp
                                    </td>
                                </tr>
                                @php $newNarration = $result['new_value'][$j]; @endphp
                            @elseif ($result['column_name'][$j] != '' && $result['column_name'][$j] != 'Naration')
                            @php $k = $k+1 @endphp
                                @if ($result['ledger_name'][$j] != '')
                                    <tr style="height: 25px !important;">
                                        <td class="common_td_rules">{{ $k }}</td>
                                        <td class=" common_td_rules">
                                            @php
                                                echo $result['column_name'][$j] . ' of ' . $result['ledger_name'][$j] . ' is changed from ' . $result['old_value'][$j] . ' to ' . $result['new_value'][$j] . ' on ' . $result['date_time'][$j] . ' by ' . $result['user_name'][$j];
                                            @endphp
                                        </td>
                                    </tr>
                                @else
                                    <tr style="height: 25px !important;">
                                        <td class="common_td_rules">{{ $k }}</td>
                                        <td class=" common_td_rules">
                                            @php
                                                echo $result['column_name'][$j] . ' is changed from <strong>' . $result['old_value'][$j] . ' </strong> to <strong>' . $result['new_value'][$j] . ' </strong> on ' . $result['date_time'][$j] . ' by ' . $result['user_name'][$j];
                                            @endphp
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endfor
                        @elseif (isset($ledgerMasterResult) && count($ledgerMasterResult) > 0)
                        @for ($j = 0; $j < sizeof($ledgerMasterResult['column_name']); $j++)

                            @if ($ledgerMasterResult['column_name'][$j] != '')
                            @php $k = $k+1 @endphp
                                @if ($ledgerMasterResult['ledger_name'][$j] != '')
                                    <tr style="height: 25px !important;">
                                        <td class="common_td_rules">{{ $k }}</td>
                                        <td class=" common_td_rules">
                                            @php
                                                echo $ledgerMasterResult['column_name'][$j] . ' of ' . $ledgerMasterResult['ledger_name'][$j] . ' is changed from ' . $ledgerMasterResult['old_value'][$j] . ' to ' . $ledgerMasterResult['new_value'][$j] . ' on ' . $ledgerMasterResult['date_time'][$j] . ' by ' . $ledgerMasterResult['user_name'][$j];
                                            @endphp
                                        </td>
                                    </tr>
                                @else
                                    <tr style="height: 25px !important;">
                                        <td class="common_td_rules">{{ $k }}</td>
                                        <td class=" common_td_rules">
                                            @php
                                                echo $ledgerMasterResult['column_name'][$j] . ' is changed from <strong>' . $ledgerMasterResult['old_value'][$j] . ' </strong> to <strong>' . $ledgerMasterResult['new_value'][$j] . ' </strong> on ' . $ledgerMasterResult['date_time'][$j] . ' by ' . $ledgerMasterResult['user_name'][$j];
                                            @endphp
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endfor
                        @elseif (isset($ledgerMappingResult) && count($ledgerMappingResult) > 0)
                        @for ($j = 0; $j < sizeof($ledgerMappingResult['data']); $j++)

                            @if ($ledgerMappingResult['data'][$j] != '')
                            @php $k = $k+1; @endphp
                                    <tr style="height: 25px !important;">
                                        <td class="common_td_rules">{{ $k }}</td>
                                        <td class=" common_td_rules">
                                            @php
                                                echo $ledgerMappingResult['data'][$j] ;
                                            @endphp
                                        </td>
                                    </tr>

                            @endif
                        @endfor
                        @elseif (isset($Closingdata) && count($Closingdata) > 0)
                        @for ($j = 0; $j < sizeof($Closingdata['Closingdata']); $j++)

                            @if ($Closingdata['Closingdata'][$j] != '')
                            @php $k = $k+1 @endphp
                                    <tr style="height: 25px !important;">
                                        <td class="common_td_rules">{{ $k }}</td>
                                        <td class=" common_td_rules">
                                            @php
                                                echo $Closingdata['Closingdata'][$j] ;
                                            @endphp
                                        </td>
                                    </tr>

                            @endif
                        @endfor
                        @else
                        <tr style="height: 25px !important;">
                            <td colspan="2" style="text-align: center">
                                No results found..
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

