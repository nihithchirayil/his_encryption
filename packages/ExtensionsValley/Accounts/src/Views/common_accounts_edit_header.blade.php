<div class="col-md-3 padding_sm">
    <div class="mate-input-box">
        <label>Account Name <span style="color: red">*</span></label>
        <input type="text" required="" autocomplete="off" value="{{$header[0]->ledger_name??''}}"
               onkeyup="select_item_desc(this.id, event,'{{$header[0]->voucher_type??''}}', '1')" id="ledger_item_desc-0"
               class="form-control auto_focus_cls" name="party[]"
               placeholder="Search Party">
        <div class='ajaxSearchBox' id="search_ledger_item_box-0" index='1'
             style="margin-top: 14px !important;width: 470px !important;max-height: 290px;"> </div>
        <input type='hidden' name='ledger_id_head' value="{{$header[0]->ledger_id??''}}" id="ledger_id_head-0" >
        <input type='hidden' name='ledger_group_head' value="{{$header[0]->group_id??''}}" id="ledger_group_head-0">
    </div>
</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box" style="height: 45px !important">
        <label>Curr.Balance</label>
        <div class="clearfix"></div>
        @if(isset($header[0]->current_balance) && $header[0]->current_balance < 0)
        <span id="ledger_code">{{$header[0]->current_balance? abs($header[0]->current_balance):''}}. Dr </span>
        @else
        <span id="ledger_code">{{$header[0]->current_balance? $header[0]->current_balance:'0'}}. Cr </span>
        @endif
    </div>
</div>
@php  $header_amount = 0; @endphp
@foreach($header as $h)
@php  $header_amount += $h->amount; @endphp
@endforeach
<div class="col-md-2 padding_sm">
    <div class="mate-input-box">
        <label>Amount<span style="color: red">*</span></label>
        <div class="clearfix"></div>
        <input type="text" value="{{$header_amount??''}}" id="amount_header" name="amount_header" autocomplete="off" onkeyup="number_validation(this)" onblur="amount_fetch(this); calculate_table_total('amount')"   class="form-control">
    </div>
</div>                         
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Date</label>
        <div class="clearfix"></div>
        <input type="text" value="<?= $header[0]->invoice_date ? date('d-m-Y', strtotime($header[0]->invoice_date)) : '' ?>" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
    </div>
</div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label for="">Dr/Cr.</label>
        <div class="clearfix"></div>
        {!! Form::select('header_cr', array("-1"=> " Select","dr"=> " Debit", "cr"=> " Credit"),$header[0]->cr_dr??'',
        [
        'class'       => 'form-control',
        'id'          => 'header_cr', 'onchange' => 'drCrChange(this.value)'
        ]) !!}
    </div>
</div> 
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Int.Ref.No</label>
        <div class="clearfix"></div>
        <input type="text" value="{{$header[0]->ref_detail??''}}" id="refer_no" name="refer_no" autocomplete="off" class="form-control">
    </div>
</div>                     
<div class="col-md-2 padding_sm  pull-right" style=" padding-top: 10px !important">
    <label for=""style="float:right">Voucher No : <span style="color: red" id="generated_vouchr_label"><b>{{$header[0]->voucher_no??''}}</b></span></label>
    <input type="hidden" value="{{$header[0]->voucher_no??''}}" name="generated_vouchr_text" id="generated_vouchr_text">
    <div class="clearfix">
    </div>
</div>

