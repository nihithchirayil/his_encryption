@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

<style>
    /* Tabs */
    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:focus,
    .nav-tabs>li.active>a:hover {
        color: #01987a;
        cursor: default;
        background-color: #fff;
        border: 1px solid #01987a;
        border-bottom-color: #01987a;
        height: 40px !important;
        font-weight: bold;
    }

    table tr:hover td {
        text-overflow: initial;
        white-space: normal;
    }
</style>
@endsection
@section('content-area')

<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{ $title }}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Vendor Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="vendor_code" id="txt_vendor_code" value=""
                                    autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Vendor Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="txt_vendor_name" id="txt_vendor_name"
                                    value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status', ['-1' => ' Select Status', '1' => ' Active', '0' => '
                                Inactive'], -1, [
                                'class' => 'form-control',
                                'id' => 'txt_status',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block light_purple_bg" id="vendorSearchBtn"><i
                                    class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block btn-warning" id="vendorClearBtn"><i class="fa fa-times"></i>
                                Clear</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="table_body_contents" id="div_list_supplier_data" data-search-url="{{ $search_url }}">

                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <ul class="nav nav-tabs" role="tablist" id="tabs-nav">
                        <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#vendor_basic_details_tab" role="tab"
                                aria-controls="vendor_basic_details_tab" aria-selected="false" aria-expanded="true">
                                Basic Details</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" data-toggle="tab" href="#vendor_contact_details_tab" role="tab"
                                aria-controls="vendor_contact_details_tab" aria-selected="false" aria-expanded="true">
                                Contact Details</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" data-toggle="tab" href="#vendor_company_details_tab" role="tab"
                                aria-controls="vendor_company_details_tab" aria-selected="false" aria-expanded="true">
                                Business Details</a>
                        </li>
                    </ul>
                    <div id="tabs-content" class="no-padding">
                        <form id="vendorDataForm" name="vendorDataForm" method="POST">
                            <div id="vendor_basic_details_tab" class="tab-content tab-content1"
                                style="display: block; height: 460px;">
                                <input type="hidden" name="vendor_id" id="vendor_id" value="0">
                                <input type="hidden" name="ledger_id" id="ledger_id" value="0">
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Vendor Name<span class="error_red">*</span></label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" required name="vendor_name"
                                            value="{{ request()->old('vendor_name') }}" id="vendor_name"
                                            autocomplete="off">
                                        <span class="error_red">{{ $errors->first('vendor_name') }}</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-12 padding_sm vendor_code_div" style="display:none;">
                                    <div class="form-group control-required">
                                        <div class="mate-input-box">
                                            <label for="">Vendor Code<span class="error_red">*</span></label>
                                            <div class="clearfix"></div>
                                            <input onblur="checkVendorCode(2)" type="text" class="form-control"
                                                name="vendor_code" id="vendor_code" value="{{$vendor_code??''}}"
                                                readonly autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">First Address</label><span class="error_red"></span>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="address_1"
                                            value="{{ request()->old('address_1') }}" id="address_1" autocomplete="off">
                                        <span class="error_red">{{ $errors->first('address_1') }}</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Second Address</label><span class="error_red"></span>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="address_2"
                                            value="{{ request()->old('address_2') }}" id="address_2" autocomplete="off">
                                        <span class="error_red">{{ $errors->first('address_2') }}</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Third Address</label><span class="error_red"></span>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="address_3"
                                            value="{{ request()->old('address_3') }}" id="address_3" autocomplete="off">
                                        <span class="error_red">{{ $errors->first('address_3') }}</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-6 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Country</label><span class="error_red"></span>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control" name="country"
                                                value="{{ request()->old('country') }}" id="country" autocomplete="off">
                                            <span class="error_red">{{ $errors->first('country') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">State</label><span class="error_red"></span>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control" name="state"
                                                value="{{ request()->old('state') }}" id="state" autocomplete="off">
                                            <span class="error_red">{{ $errors->first('state') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-6 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">City</label><span class="error_red"></span>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control" name="city"
                                                value="{{ request()->old('city') }}" id="city" autocomplete="off">
                                            <span class="error_red">{{ $errors->first('city') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Pin Code</label><span class="error_red"></span>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control" name="pincode"
                                                value="{{ request()->old('pincode') }}" id="pincode" autocomplete="off">
                                            <span class="error_red">{{ $errors->first('pincode') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div id="vendor_contact_details_tab" class="tab-content tab-content1"
                        style="display: none; height: 460px;">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Contact Person</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{ request()->old('contact_person') }}" class="form-control"
                                    name="contact_person" id="contact_person" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Contact Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ request()->old('contact_no') }}" class="form-control"
                                        name="contact_no" id="contact_no" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Email ID</label>
                                    <div class="clearfix"></div>
                                    <input type="email" class="form-control" value="{{ request()->old('email') }}"
                                        name="email" id="email" autocomplete="off"><span class="error_red">{{
                                        $errors->first('email') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="vendor_company_details_tab" class="tab-content tab-content1"
                        style="display: none; height: 460px;">
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">TIN Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ request()->old('tin_no') }}" class="form-control"
                                        name="tin_no" id="tin_no">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">TAN Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ request()->old('tan_no') }}" class="form-control"
                                        name="tan_no" id="tan_no">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">PAN Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ request()->old('pan_no') }}" class="form-control"
                                        name="pan_no" id="pan_no">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">GST Vendor Code</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ request()->old('gst_vendor_code') }}"
                                        class="form-control" name="gst_vendor_code" id="gst_vendor_code">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Drug License Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ request()->old('drug_license_no') }}"
                                        class="form-control" name="drug_license_no" id="drug_license_no">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{ __('Active') }}</option>
                                        <option value="0">{{ __('Inactive') }}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            {{-- <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Group<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="ledger_group" id="ledger_group">
                                        <option value="67">{{ __('Sundry Creditors') }}</option>
                                        <option value="68">{{ __('Creditors for Fixed Assets') }}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('ledger_group') }}</span>
                                </div>
                            </div> --}}
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Payment Terms</label>
                                    <div class="clearfix"></div>
                                    @php $payment_terms = ['' => 'Select Payment Terms', '30' => '30', '45' => '45',
                                    '60' =>
                                    '60', '90' => '90',
                                    '120' => '120', '130' => 'Above 120']; @endphp
                                    {!! Form::select('payment_terms', $payment_terms, 0, [
                                    'class' => 'form-control',
                                    'id' => 'payment_terms',
                                    ]) !!}
                                </div>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-4 padding_sm">
                            <button id="clearVendorDataBtn" class="btn btn-block btn-warning"
                                onclick="resetFunction();"><i class="fa fa-times"></i>
                                Cancel</button>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <button id="saveVendorDataBtn" class="btn btn-block light_purple_bg"
                                onclick="save_vendor_details();"><i id="saveVendorDataSpin" class="fa fa-save"></i>
                                Save</button>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <span class="btn btn-block btn-info" onclick="generateReport()"><i class="fa fa-search"></i>
                                Report</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }

    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });

});
$('#tabs-nav li').click(function() {
    $('#tabs-nav li').removeClass('active');
    $(this).addClass('active');
    $('.tab-content1').hide();

    var activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn();
    return false;
});

window.search_url = $(".table_body_contents").attr('data-search-url');
search_vendor_data(window.search_url);

$(document).on('click', '.page-link', function(e) {
    e.preventDefault();
    if ($(e.target).parent().hasClass('active') == false) {
        var url = $(this).attr("href");
        $('.table_body_contents').attr('data-action-url', url);
        search_vendor_data(url);
    }
});
$(document).on('click', '#vendorSearchBtn', function(e) {
    search_vendor_data(window.search_url);
});

$(document).on('click', '#vendorClearBtn', function(e) {
    $('#txt_vendor_code').val('');
    $('#txt_vendor_name').val('');
    $('#txt_status').val('-1');
    search_vendor_data(window.search_url);
});


function checkVendorCode(frm_type) {
    var base_url = $("#base_url").val();
    var url = base_url + "/accounts/checkVendorCode";
    var vendor_code = $('#vendor_code').val();
    var vendor_name = $('#vendor_name').val();
    if (vendor_name) {
        var vendor_id = $('#vendor_id').val();
        var param = {
            vendor_code: vendor_code,
            vendor_id: vendor_id,
            vendor_name: vendor_name,
        };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function() {
                $('#saveVendorDataBtn').attr('disabled', true);
                $('#saveVendorDataSpin').removeClass('fa fa-save');
                $('#saveVendorDataSpin').addClass('fa fa-spinner fa-spin');
            },
            success: function(data) {
                if (data != 0 && frm_type == 2) {
                    toastr.warning("Vendor Code Already Exists");
                } else if (data != 0 && frm_type == 1) {
                    toastr.warning("Vendor Name Already Exists");
                    $('#vendor_name').val('');
                }
            },
            complete: function() {
                $('#saveVendorDataBtn').attr('disabled', false);
                $('#saveVendorDataSpin').removeClass('fa fa-spinner fa-spin');
                $('#saveVendorDataSpin').addClass('fa fa-save');
            },
            error: function() {
                toastr.error('Please check your internet connection and try again');
            }
        });
    }
}

function vendorEditLoadData(obj, id, code, name, add_1, add_2, add_3, city, state, country, pin, contact_person,
    contact_no, email, status, tin, tan, pan, gst, drug_no, ledger_id, credit_days, ledger_group_id) {
    $('#vendor_id').val(id);
    $('#vendor_name').val(name);
    $('#address_1').val(add_1);
    $('#address_2').val(add_2);
    $('#address_3').val(add_3);
    $('#city').val(city);
    $('#state').val(state);
    $('#country').val(country);
    $('#pincode').val(pin);
    $('#contact_person').val(contact_person);
    $('#contact_no').val(contact_no);
    $('#email').val(email);
    $('#status').val(status);
    $('#tin_no').val(tin);
    $('#tan_no').val(tan);
    $('#pan_no').val(pan);
    $('#gst_vendor_code').val(gst);
    $('#drug_license_no').val(drug_no);
    $('#ledger_id').val(ledger_id);
    $('#payment_terms').val(credit_days);
    $('#ledger_group').val(ledger_group_id);
}

function resetFunction() {

    $('#vendor_id').val('0');
    $('#ledger_id').val('0');
    $('#vendorDataForm')[0].reset();
}

function generateReport(id) {
    var ledger_id = $('#ledger_id').val();
    var vendor_name = $("#vendor_name").val();
    var base_url = $("#base_url").val();
    window.open(base_url + '/accounts/supplierPaymentReportData?ledger_id=' + ledger_id + '&vendor_name=' +
        vendor_name, 'blank');
}

function search_vendor_data(url) {
    var vendor_code = $('#txt_vendor_code').val();
    var vendor_name = $('#txt_vendor_name').val();
    var status = $('#txt_status').val();
    var param = {
        vendor_code: vendor_code,
        status: status,
        vendor_name: vendor_name,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $('#vendorSearchBtn').attr('disabled', true);
            $('#vendorSearchBtn').find('i').removeClass('fa fa-search');
            $('#vendorSearchBtn').find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            $('#div_list_supplier_data').html(data);
        },
        complete: function() {
            $('#vendorSearchBtn').attr('disabled', false);
            $('#vendorSearchBtn').find('i').removeClass('fa fa-spinner fa-spin');
            $('#vendorSearchBtn').find('i').addClass('fa fa-search');
        },
        error: function() {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

function save_vendor_details() {
    var data_params = $('#vendorDataForm').serialize();
    var base_url = $("#base_url").val();
    var vendor_name = $("#vendor_name").val();
    if (vendor_name.trim() == '' || vendor_name.trim() == null) {
        toastr.warning('Vendor name mandatory field');
        $("#vendor_name").focus();
        return;
    }
    var url = base_url + "/accounts/saveSupplierData";
    $.ajax({
        type: "POST",
        url: url,
        data: data_params,
        beforeSend: function() {
            $('#saveVendorDataBtn').attr('disabled', true);
            $('#saveVendorDataBtn').find('i').removeClass('fa fa-search');
            $('#saveVendorDataBtn').find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            if (parseInt(data.status) == 1) {
                toastr.success(data.message);
                resetFunction();
                $('#vendorSearchBtn').trigger('click');
            } else {
                toastr.warning(data.message);
            }
        },
        complete: function() {
            $('#saveVendorDataBtn').attr('disabled', false);
            $('#saveVendorDataBtn').find('i').removeClass('fa fa-spinner fa-spin');
            $('#saveVendorDataBtn').find('i').addClass('fa fa-search');
        },
        error: function() {
            toastr.error('Please check your internet connection and try again');
        }
    });
}

@include('Purchase::messagetemplate')
</script>

@endsection
