<div class="col-md-12">
    <div class="col-md-12"><h4 class="blue" align="center" style="margin-bottom: 5px;margin-top:-10px">Ledger Details</h4></div>
    <div class="col-md-6" style="margin-bottom:10px;border: 1px solid #a52a2a26;padding: 5px 0px 0px 17px;"><label for=""style="font-weight:700;font-size:15px;">Ledger Name</label><label for="" style="font-size:15px">:{{$VendorDetails[0]->ledger_name}}</label></div>
    <div class="col-md-3" style="margin-bottom:10px;border: 1px solid #a52a2a26;padding: 5px 0px 0px 17px;"><label for=""style="font-weight:700;font-size:15px;">Invoice Date</label><label for="" style="font-size:15px">:{{$VendorDetails[0]->invoice_date}}</label></div>
    <div class="col-md-3" style="margin-bottom:10px;border: 1px solid #a52a2a26;padding: 5px 0px 0px 17px;"><label for=""style="font-weight:700;font-size:15px;">Amount</label><label for="" style="font-size:15px">:{{@$VendorDetails[0]->amount ? $VendorDetails[0]->amount : ''}}</label></div>
    <div class="clearfix"></div>
    <div class="col-md-12"style="margin-bottom:10px;border: 1px solid #a52a2a26;padding: 5px 0px 0px 17px;"><label for=""style="font-weight:700;font-size:15px;">Invoice</label><label for="" style="font-size:15px">:{{$VendorDetails[0]->invoice}}</label></div>
   <div class="col-md-12" id="add_transfer_details">
    <div class="col-md-12"><h4 class="blue" align="center" style="margin-bottom: 5px">Add Transfer Details</h4></div>
    <div class="clearfix"></div>
    <hr style="margin-top:2px!important;margin-bottom:2px !important;">
   
     <div class="col-md-12" style="margin-top: 5px">
        <div class="col-md-4"><select name="remitter" id="remitter" class="form-control">
            <option value="" style="font-size: 14px !important;height:33px;">Select Remitter</option>  
            @foreach ($remitter as $data)
            <option value="{{ $data->bank_id }}" style="font-size: 14px !important;height:33px;">{{ $data->bank_name }}</option>  
            @endforeach  
         </select></div>
          <div class="col-md-4"><select name="beneficiary" id="beneficiary" class="form-control">
            <option value="" style="font-size: 14px !important;height:33px;">Select Beneficiary</option>  
            @foreach ($beneficiary as $data)
            <option value="{{ $data->bank_id }}" style="font-size: 14px !important;height:33px;">{{ $data->bank_name }}</option>  
            @endforeach    
         </select></div>
          <div class="col-md-4"><select name="type_of_pay" id="type_of_pay" class="form-control">
            <option value="" style="font-size: 14px !important;height:33px;">Select Type Of Payment</option>    
            <option value="1" style="font-size: 14px !important;height:33px;">NEFT</option>    
            <option value="2" style="font-size: 14px !important;height:33px;">IMPS</option>    
         </select></div>
         <div class="col-md-12" style="margin-top: 10px;">
           <textarea class="form-control" placeholder="Payment remarks" style="resize:none"  id="payment_remarks"></textarea>
         </div>
     </div>
   </div>
</div>