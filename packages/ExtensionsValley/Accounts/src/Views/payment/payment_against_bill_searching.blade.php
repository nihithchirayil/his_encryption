<div class="row">
    <div class="clearfix"></div>
    <div class="h10"></div>
    <div class="col-md-12">
        <table class="table table_round_border styled-table" id="receipt_table">
            <thead>
                <tr class="table_header_bg">
                    <th style=";width:3%"><input class="checkAll" type="checkbox" ></th>
                    <th style="text-align:center"><input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Search party.. " style="display: none;width: 90%;color:black" >
                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                        <span id="item_search_btn_text">Party</span></th>
                    <th style="text-align:center;width:10%">Voucher Type</th>
                    <th style="text-align:center;width:10%">Voucher No</th>
                    <th style="text-align:center">Invoice No</th>
                    <th style="text-align:center;width:10%">Invoice Date</th>
                    <th style="text-align:center;width:10%">Due Date</th>
                    <th style="text-align:center;width:8%">Amount</th>
                    <th style="text-align:center;width:8%;" class="partial_payment"> Part.Pay</th>
                </tr>
            </thead>
            <tbody id="append_tbdy">
                @if(isset($pendingData))
                @foreach($pendingData as $st)
                @php 
                $tr_style = ''; 
                $orginal_amnt = !empty($st->amount) ? number_format($st->amount, 2, '.', ',') :'';
                @endphp
                @if($st->settled_status == 1)
                @php 
                $orginal_amnt = !empty($st->amount) ? $st->amount :0 ;
                $partial_payment_amount = !empty($st->partial_payment_amount) ? $st->partial_payment_amount :0 ;
                $orginal_amnt = $orginal_amnt-$partial_payment_amount;
                $orginal_amnt = number_format($orginal_amnt, 2, '.', ',');
                @endphp
                @php $tr_style = "background-color:#badfd5"; @endphp
                @endif
                <tr style="{{$tr_style}}">
                    <td><input data-id="{{$st->master_id}}"  data-advanceamnt ="{{$st->advance_amount}}" data-invoicedate="{{!empty($st->invoice_date) ? $st->invoice_date :''}}" data-invoice="{{$st->invoice_no}}" onclick="checkIsSameParty(this,'{{$st->master_id}}','{{$st->amount}}')" name="detail_id[]" class="{{$st->amount}}" type="checkbox" value="{{$st->detail_id}}"  ></td>
                    <td class="common_td_rules">{{ $st->ledger_name }}<input type="hidden" value="{{ $st->ledger_name }}" name="party_name[]"></td>
                    <td class="common_td_rules">{{ !empty($st->voucher_type) ? $st->voucher_type:'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->voucher_no) ? $st->voucher_no:'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->invoice_no) ? $st->invoice_no :'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->invoice_date) ? $st->invoice_date :'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->invoice_date) ? (date('M-d-Y',strtotime($st->invoice_date. ' + '.$st->credit_days.' days'))) :'' }}</td>
                    <td class="td_common_numeric_rules">{{ !empty($st->amount) ? number_format($st->amount, 2, '.', ',') :'' }}</td>
                    <td class="partial_payment" style="text-align:center;"><span style="font-size: 10px;padding: 1px 2px;"class="btn btn-warning" onclick="ProcessPartialPayment(this,{{$st->detail_id}},{{$st->amount}})"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span></td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif                        
            </tbody>
        </table>
    </div>                                           
</div>