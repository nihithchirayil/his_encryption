<table class="table table_round_border styled-table" id="receipt_table1" style="margin: 0px 0px !important">
            <thead>
                <tr class="table_header_bg" style="background: #53aab3 !important;">
                    <th style="text-align:center;width:3%">&nbsp;</th>
                    <th style="text-align:center;width:10%">Voucher No</th>
                    <th style="text-align:center;width:10%">Credit No</th>
                    <th style="text-align:center;width:10%">Date</th>
                    <th style="text-align:center;width:8%">Amount</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($credit_note_array))
                @foreach($credit_note_array as $rec)
                <tr>
                    <td class="common_td_rules">
                        <input data-creditno="{{ !empty($rec->invoice_no) ? $rec->invoice_no:'' }}" data-id="{{$rec->amount}}" data-date="{{!empty($rec->invoice_date) ? date('d-M-Y',strtotime($rec->invoice_date)) :''}}" data-voucher="{{$rec->voucher_no}}" name="credit_note_id[]" class="credit_note_id" type="checkbox" value="{{$rec->id}}" onclick="creditNotCheck(this)"  >
                    </td>
                    <td class="common_td_rules">{{ !empty($rec->voucher_no) ? $rec->voucher_no:'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->invoice_no) ? $rec->invoice_no:'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->invoice_date) ? date('d-M-Y',strtotime($rec->invoice_date)) :'' }}</td>
                    <td class="td_common_numeric_rules">{{ !empty($rec->amount) ? number_format($rec->amount, 2, '.', ',') :'' }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif                        
            </tbody>
        </table>
 @if(isset($credit_note_array) && count($credit_note_array) > 0)
        <div style="padding-top: 15px;width: 15%" class="pull-right">
                    <button type="button" class="btn btn-block btn-warning pull-right" onclick="SavecreditNote()">
                        <i class="fa fa-thumbs-up" ></i>&nbsp;&nbsp;OK
                    </button>
        </div>
 @else 
 <span style="color:red;padding-left: 270px">No Records found</span>
 @endif
 <div style="padding-top: 16px;padding-left: 270px">Total <span id="tot_checked_credit_amount" style="color:red">0</span</div>