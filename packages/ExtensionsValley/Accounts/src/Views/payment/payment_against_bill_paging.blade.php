
                @if(isset($pendingData))
                @foreach($pendingData as $st)
                @php $tr_style1 = '';@endphp
                @if($st->settled_status == 1)
                @php $tr_style1 = "background-color:#badfd5"; @endphp
                @endif
                <tr style="{{$tr_style1}}">
                    <td><input data-id="{{$st->master_id}}" data-advanceamnt ="{{$st->advance_amount}}" data-invoicedate="{{!empty($st->invoice_date) ? $st->invoice_date :''}}" data-invoice="{{$st->invoice_no}}" onclick="checkIsSameParty(this,'{{$st->master_id}}','{{$st->amount}}')" name="detail_id[]" class="{{$st->amount}}" type="checkbox" value="{{$st->detail_id}}"  ></td>
                    <td class="common_td_rules">{{ $st->ledger_name }}<input type="hidden" value="{{ $st->ledger_name }}" name="party_name[]"></td>
                    <td class="common_td_rules">{{ !empty($st->voucher_type) ? $st->voucher_type:'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->voucher_no) ? $st->voucher_no:'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->invoice_no) ? $st->invoice_no :'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->invoice_date) ? $st->invoice_date :'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->invoice_date) ? (date('M-d-Y',strtotime($st->invoice_date. ' + '.$st->credit_days.' days'))) :'' }}</td>
                    <td class="td_common_numeric_rules">{{ !empty($st->amount) ? number_format($st->amount, 2, '.', ',') :'' }}</td>
                    <td class="partial_payment" style="text-align:center;"><span style="font-size: 10px;padding: 1px 2px;"class="btn btn-warning" onclick="ProcessPartialPayment(this,{{$st->detail_id}},{{$st->amount}})"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span></td>
                    @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif                        

