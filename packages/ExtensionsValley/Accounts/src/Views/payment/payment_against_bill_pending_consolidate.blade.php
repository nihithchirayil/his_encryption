<div class="row">
    <div class="clearfix"></div>
    <div class="h10"></div>
<!--    <div class="col-md-3 pull-left">
        <input type="checkbox" onclick="showVoucherDiv(this)">voucher wise
    </div>-->
    <div class="col-md-4 padding_sm" style="padding-bottom: 10px;">
        <div class="date custom-float-label-control">
            <div class="mate-input-box">
                <label class="custom_floatlabel">Bank</label>
                <?php
                $bank_details = \DB::table('ledger_master')->Where('is_bank', 1)
                        ->orderBy('ledger_name', 'asc')
                        ->pluck('ledger_name', 'id');
                $default_pay_bank_details = \DB::table('ledger_master')->Where('is_bank', 1)->Where('payment_bank_default', 1)
                        ->select('id')->first();
                $default_bank = isset($default_pay_bank_details->id) ? $default_pay_bank_details->id:'';
                ?>
                {!! Form::select('bank_name',$bank_details,$default_bank,['class' => 'form-control','placeholder' => 'Bank','title' => 'Bank','id' => 'bank_details','style' => 'color:#555555; padding:2px 12px;']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 padding_sm" style="display: none">
        <div class="mate-input-box">
            <label for="">Vouchr. Type</label>
            <div class="clearfix"></div>
            @php $v_type = \DB::table('ledger_voucher_type')->where('status', 1)->where('payment_receipt',1)->orderBy('type')->pluck('type', 'id'); @endphp
            {!! Form::select('v_type',$v_type->toArray(),'',
            ['class'=>"form-control v_type", 'id'=>"v_type"]) !!}
        </div></div>
    <div class="col-md-8 padding_sm">
        <div class="mate-input-box">
            <label for="">Remarks</label>
            <div class="clearfix"></div>
            <textarea style="height: 55px !important"  wrap="off" class="form-control" cols="35" rows="1" name="payment_naration" id="payment_naration" ></textarea>
        </div>
    </div>
    <div class="col-md-12" id="consolidate" style="margin-top: -15px">
        <div class="table-responsive theadscroll"  style="height: 360px;">
            <table class="table table_round_border styled-table">
                <thead>
                    <tr class="table_header_bg">
                        <th style="text-align:center">Party</th>
                        <th style="text-align:center;width: 17%">Total No.bill</th>
                        <th style="text-align:center;width: 20%">Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($consolidateData))
                    @php $total_pay_amnt = 0; @endphp
                    @foreach($consolidateData as $cd)
                    <tr>
                        <td class="common_td_rules">{{ $cd->ledger_name }}</td>
                        <td class="td_common_numeric_rules">{{ !empty($cd->bill_cnt) ? $cd->bill_cnt:'' }}</td>
                        <td class="td_common_numeric_rules">{{ !empty($cd->sum_amnt) ? $cd->sum_amnt:'' }}</td>
                        @php $total_pay_amnt += $cd->sum_amnt @endphp
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                    @endif                        
                </tbody>
            </table>
            <div class="col-md-2 pull-right"> 
        <label style="color:red">
            <b>{{$total_pay_amnt}}</b>
        </div>
        </div>
        <div class="col-md-2 pull-right"> 
        <button type="button" class="btn btn-block btn-primary" onclick="SavePayment()">
            <i class="fa fa-save" ></i>&nbsp;&nbsp;Save
        </button></div>
    </div>
    <div class="col-md-12" id="voucherWise" style="display: none">
        <div class="table-responsive theadscroll"  style="height: 400px;">
            <table class="table table_round_border styled-table">
                <thead>
                    <tr class="table_header_bg">
                        <th style="text-align:center">Voucher No</th>
                        <th style="text-align:center">Bill Count</th>
                        <th style="text-align:center">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @php  $sort_array = array();@endphp
                    @if(isset($consolidateVoucherData))
                    @foreach($consolidateVoucherData as $st)
                    @if(!in_array($st->lm_id,$sort_array))
                    @php 
                    $sort_array[] = $st->lm_id;
                    $sort_order = true;
                    @endphp
                    @endif
                    @if($sort_order)
                    <tr style="background-color: #afd6be"><td colspan='3' style="text-align: center"><b>{{$st->ledger_name}}</b></td></tr>
                    @endif
                    @php 
                    $sort_order = false; 
                    @endphp
                    <tr>
                        <td class="common_td_rules">{{ $st->voucher_no }}</td>
                        <td class="td_common_numeric_rules">{{ !empty($st->total_count) ? $st->total_count:'' }}</td>
                        <td class="td_common_numeric_rules">{{ !empty($st->total_sum) ? $st->total_sum:'' }}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                    @endif                        
                </tbody>
            </table>
        </div>
    </div>  

</div>
<div class="row">
    <div class="modal-footer">
        <input type="hidden" id="ledger_id" value="<?= $detail_array ?>">
    </div>
</div>
