@extends('Dashboard::dashboard.dashboard')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
    /*   Change Color for Active Tabs     */
    .nav-pills>li.active>a,
    .nav-pills>li.active>a:focus,
    .nav-pills>li.active>a:hover,
    .nav-pills>li>a:hover,
    .nav>li>a:hover {
        background-color: #F47321 ;
        color: #FFFFFF;
    }
    .nav>li>a {
        padding: 3px 6px !important;
    }

    /*   Change Color for Inactive Tabs     */
    .nav-pills>li>a {
        background-color: #01987a;
        color: #FFFFFF;

    }
    /* toggle button */
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" style="padding: 10px 22px !important" >
    <div class="row" style="text-align: right;  font-size: 12px; font-weight: bold;padding-right: 80px"> {{$title}}
        <div class="col-xs-3 padding_sm">
            <div class="mate-input-box">
                <label>Party</label>
                <input type="text" required="" autocomplete="off"
                       onkeyup="select_item_desc(this.id, event)" id="ledger_item_desc-0"
                       class="form-control auto_focus_cls" name="payment_party"
                       placeholder="Party">
                <input type="hidden" id="ledger_id_head-1" >
                <div class='ajaxSearchBox' id="search_ledger_item_box-0" index='1'
                     style="top: 48px !important;width: 470px !important;max-height: 290px;"> </div>
            </div>
        </div>
        <div class="col-xs-2 padding_sm">
            <div class="mate-input-box">
                <label>From Date</label>
                <input type="text" autocomplete="off"
                       onblur="change_date(this)" id="from_date_invoice-0"
                       class="form-control datepickers" name="from_date_invoice"
                       placeholder="From Date">
            </div>
        </div>
        <div class="col-xs-2 padding_sm">
            <div class="mate-input-box">
                <label>To Date</label>
                <input type="text" autocomplete="off"
                       onblur="change_date(this)" id="to_date_invoice-0"
                       class="form-control datepickers" name="to_date_invoice"
                       placeholder="To Date">
            </div>
        </div>
        <div class="col-xs-1 padding_sm" style="margin-top:16px">
        <button onclick="datarst();" type="reset" class="btn light_purple_bg"  name="clear_results" id="clear_results">
            <i class="fa fa-refresh" aria-hidden="true"></i>
            Reset
        </button>
        </div>
        <div class="col-xs-1 padding_sm" style="margin-top:16px">
        <button onclick="viewBillPrint('prt_div');" type="reset" class="btn lightgrey_bg"  name="clear_results" id="clear_results">
            <i class="fa fa-print" aria-hidden="true"></i>
            Print
        </button>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div id="product_tab_container">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs sm_nav nav-pills">
                                <li class="active"><a data-toggle="tab" href="#tab_1" id="" onclick="LabelChange()">Show pending bills<i id="" class=""></i></a></li>
                                <li><a data-toggle="tab" href="#tab_2" id="" onclick="receivedDetails()">Show paid bills <i id="" class=""></i></a></li>
                                <li style="padding:4px"><b>Total Amount&nbsp;&nbsp;-&nbsp;&nbsp;<span style="color:red" id="ttl_selected_amount_label_span"></span></b></li>
                                <li class="pull-right"><button onclick="viewConsolidate()" class="btn btn-block btn-primary" style="height: 26px;"><i class=" fa fa-thumbs-up"></i> Make Payment </button></li>
                                <li class="pull-right" style="padding:4px" id="pending_label"><b>Total Pending&nbsp;&nbsp;-&nbsp;&nbsp;<span style="color:red" id="pending_label_span">{{$total ?? '0'}}</span></b></li>
                                <li class="pull-right" style="padding:4px;display: none" id="payment_label"><b>Total Paid&nbsp;&nbsp;-&nbsp;&nbsp;<span style="color:red" id="show_total_paid_bills_span">{{$total_rec ?? '0'}}</span></b></li>
                            </ul>
                            <div class="tab-content" style="min-height: 535px;">
                                <div id="tab_1" class="tab-pane active">
                                    <div class="row">
                                        <div class="clearfix"></div>
                                        <div class="h10"></div>
                                        <div class="col-md-12">
                                            <table class="table table_round_border styled-table" id="receipt_table">
                                                <thead>
                                                    <tr class="table_header_bg">
                                                        <th style=";width:3%"><input class="checkAll" type="checkbox" ></th>
                                                        <th style="text-align:center"><input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Search party.. " style="display: none;width: 90%;color:black" >
                                                            <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                                            <span id="item_search_btn_text">Party</span></th>
                                                        <th style="text-align:center;width:10%">Voucher Type</th>
                                                        <th style="text-align:center;width:10%">Voucher No</th>
                                                        <th style="text-align:center">Invoice No</th>
                                                        <th style="text-align:center;width:10%">Invoice Date</th>
                                                        <th style="text-align:center;width:10%">Due Date</th>
                                                        <th style="text-align:center;width:8%">Amount</th>
                                                        <th style="text-align:center;width:8%;" class="partial_payment"> Part.Pay</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="append_tbdy">
                                                    @if(isset($pendingData))
                                                    @foreach($pendingData as $st)
                                                    @php
                                                        $tr_style = '';
                                                        $orginal_amnt = !empty($st->amount) ? number_format($st->amount, 2, '.', ',') :'';
                                                    @endphp
                                                    @if($st->settled_status == 1)
                                                    @php
                                                        $orginal_amnt = !empty($st->amount) ? $st->amount :0 ;
                                                        $partial_payment_amount = !empty($st->partial_payment_amount) ? $st->partial_payment_amount :0 ;
                                                        $orginal_amnt = $orginal_amnt-$partial_payment_amount;
                                                        $orginal_amnt = number_format($orginal_amnt, 2, '.', ',');
                                                    @endphp
                                                    @php $tr_style = "background-color:#badfd5"; @endphp
                                                    @endif
                                                    <tr style="{{$tr_style}}">
                                                        <td><input data-id="{{$st->master_id}}" data-advanceamnt ="{{$st->advance_amount}}" data-invoicedate="{{!empty($st->invoice_date) ? $st->invoice_date :''}}" data-invoice="{{$st->invoice_no}}" onclick="checkIsSameParty(this,'{{$st->master_id}}','{{$st->amount}}')" name="detail_id[]" class="{{$st->amount}}" type="checkbox" value="{{$st->detail_id}}"  ></td>
                                                        <td class="common_td_rules">{{ $st->ledger_name }}<input type="hidden" value="{{ $st->ledger_name }}" name="party_name[]"></td>
                                                        <td class="common_td_rules">{{ !empty($st->voucher_type) ? $st->voucher_type:'' }}</td>
                                                        <td class="common_td_rules">{{ !empty($st->voucher_no) ? $st->voucher_no:'' }}</td>
                                                        <td class="common_td_rules">{{ !empty($st->invoice_no) ? $st->invoice_no :'' }}</td>
                                                        <td class="common_td_rules">{{ !empty($st->invoice_date) ? $st->invoice_date :'' }}</td>
                                                        <td class="common_td_rules">{{ !empty($st->invoice_date) ? (date('M-d-Y',strtotime($st->invoice_date. ' + '.$st->credit_days.' days'))) :'' }}</td>
                                                        <td class="td_common_numeric_rules">{{ !empty($st->amount) ? number_format($st->amount, 2, '.', ',') :'' }}</td>
                                                        <td class="partial_payment" style="text-align:center;"><span style="font-size: 10px;padding: 1px 2px;"class="btn btn-warning" onclick="ProcessPartialPayment(this,{{$st->detail_id}},{{$st->amount}})"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span></td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="5">No Records Found</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="h10"></div>

                                    <div class="row" style="">
                                        <div style="padding:10px 30px;">
                                            <div class="col-md-12" style="text-align:center;">
                                                <span id="purchase_paging" class="btn btn-warning" data-id="2" style="padding: 4px 11px;" >Load More..</span>
                                                <!--                                                    <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
                                                                                                    </nav>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="tab_2" class="tab-pane">

                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div id="modal_pending_consolidate" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 50%;height:700px;overflow: auto">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Process Pending Bills</h4>
                </div>
                <div class="modal-body" id="append_pending" style="">
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12" id="consolidate" style="margin-top: -15px;">
                            <div class="table-responsive theadscroll">
                                <table class="table table_round_border styled-table">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th style="text-align:center">Party</th>
                                            <th style="text-align:center;width: 17%">No.bills</th>
                                            <th style="text-align:center;width: 20%">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="common_td_rules" id="consolidate_led_name"></td>
                                            <td class="td_common_numeric_rules" id="consolidate_bill_cnt"></td>
                                            <td class="td_common_numeric_rules" id="consolidate_sum_amnt"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12" >
                            <div class="theadscroll always-visible" style="margin-top: -40px;">
                                <table class="table table_round_border styled-table" style="width: 50%">
                                    <thead>
                                        <tr>
                                            <th colspan="3" style="text-align: left"> Advance</th>
                                        </tr>
                                        <tr class="table_header_bg" style=" background-color: #82a55e !important">
                                            <th style="text-align: center">Adv.Outstanding</th>
                                            <th style="text-align: center"> Adj.Amount</th>
                                            <th> &nbsp;</th></tr>
                                    </thead>
                                    <tr>
                                        <td id="party_adv" style="width:20%;text-align: center">0</td>
                                        <td><input type="text" class="form-control advance_proces_vl"  value="0" id="party_adv_amnt" onkeyup="number_validation(this);"></td>
                                        <td style="width:5%">
                                           <button type="button" class="btn btn-block btn-warning" onclick="processTotal()" id="">
                                                        <i class="fa fa-thumbs-up" ></i>&nbsp;&nbsp;Total
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    </div>

                        <div class="col-md-12" style="margin-top:-15px" >
                            <div class="box no-border no-margin">
                                <div class="theadscroll" style="position: relative; height: 100px;" id="credit_not_div">
                                </div>
                            </div>
                        </div>
                            <div class="col-md-2 pull-left" >
                                <button type="button" class="btn btn-block btn-warning" onclick="creditNoteImport()" id="creditNoteImportbtn">
                                    <i class="fa fa-save" ></i>&nbsp;&nbsp;Credit Note
                                </button>
                            </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12" style="padding-bottom: 10px;padding-top: 10px;">
                        <div class="col-md-4 padding_sm" >
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box">
                                    <label class="custom_floatlabel">Bank</label>
                                    <?php
                                    $bank_details = \DB::table('ledger_master')->WhereIn('is_bank', array(1,2))->where('is_ledger',1)
                                            ->orderBy('ledger_name', 'asc')
                                            ->pluck('ledger_name', 'id');
                                    $default_pay_bank_details = \DB::table('ledger_master')->Where('is_bank', 1)->Where('payment_bank_default', 1)
                                                    ->select('id')->first();
                                    $default_bank = isset($default_pay_bank_details->id) ? $default_pay_bank_details->id : '';
                                    ?>
                                    {!! Form::select('bank_name',$bank_details,$default_bank,['class' => 'form-control','placeholder' => 'Bank','title' => 'Bank','id' => 'bank_details','style' => 'color:#555555; padding:2px 12px;']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label>CDV No</label>
                                <input type="text" autocomplete="off" id="cdv_no" class="form-control" name="cdv_no" placeholder="CDV">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label>Cheque No</label>
                                <input type="text" autocomplete="off" id="check_no" class="form-control" name="check_no" placeholder="Cheque No">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="h10"></div>
                        <div class="col-md-4 padding_sm" style="padding-top:10px !important">
                            <div class="mate-input-box">
                                <label>Cheque Date</label>
                                <input type="text" autocomplete="off" id="check_dt" class="form-control datepicker" name="check_dt" placeholder="Cheque Date">
                            </div>
                        </div>
                        <div class="col-md-8 padding_sm" style="padding-top:10px !important">
                            <div class="mate-input-box">
                                <label for="">Remarks</label>
                                <div class="clearfix"></div>
                                <textarea style="height: 55px !important"  wrap="off" class="form-control" cols="35" rows="1" name="payment_naration" id="payment_naration" ></textarea>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="display: none">
                            <div class="mate-input-box">
                                <label for="">Vouchr. Type</label>
                                <div class="clearfix"></div>
                                @php $v_type = \DB::table('ledger_voucher_type')->where('status', 1)->where('payment_receipt',1)->orderBy('type')->pluck('type', 'id'); @endphp
                                {!! Form::select('v_type',$v_type->toArray(),'',
                                ['class'=>"form-control v_type", 'id'=>"v_type"]) !!}
                            </div>
                        </div>
                    </div>
                        <div class="clearfix"></div>
                        <div class="h10"></div>
                        <div class="col-md-4" id="amount_after_credit_note_attach_div" style="display:none;padding-top: 10px">
                            <span><b>Total Amount To Pay</b> &nbsp;&nbsp;&nbsp;</span>
                            <b>
                                <span id="amount_after_credit_note_attach" style="color:red;font-weight: 800;text-decoration:underline;text-decoration-style: double;">0</span>
                            </b>
                        </div>
                        <div class="col-md-2 pull-right" style="padding-top: 10px">
                                <button type="button" class="btn btn-block btn-primary" onclick="SavePayment()">
                                    <i class="fa fa-save" ></i>&nbsp;&nbsp;Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="ledger_id" value="">
                </div>
                </div>

            </div>
        </div>
    </div>
    <div id="part_payment" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 50%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Process Partial Payment <span style="padding-left:15px;" id="part_payment_ttl_amnt"></span></h4>
                </div>
                <div class="modal-body" id="part_payment_pending">
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="h10"></div>
                        <div class="col-md-4 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box">
                                    <label class="custom_floatlabel">Bank</label>
                                    <?php
                                    $bank_details = \DB::table('ledger_master')->Where('is_bank', 1)
                                            ->orderBy('ledger_name', 'asc')
                                            ->pluck('ledger_name', 'id');
                                    $default_pay_bank_details = \DB::table('ledger_master')->Where('is_bank', 1)->Where('payment_bank_default', 1)
                                                    ->select('id')->first();
                                    $default_bank = isset($default_pay_bank_details->id) ? $default_pay_bank_details->id : '';
                                    ?>
                                    {!! Form::select('part_bank_name',$bank_details,$default_bank,['class' => 'form-control','placeholder' => 'Bank','title' => 'Bank','id' => 'part_bank_details','style' => 'color:#555555; padding:2px 12px;']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label>CDV No</label>
                                <input type="text" autocomplete="off" id="part_cdv_no" class="form-control" name="part_cdv_no" placeholder="CDV">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label>Cheque No</label>
                                <input type="text" autocomplete="off" id="part_check_no" class="form-control" name="part_check_no" placeholder="Cheque No">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm" style="padding-top:10px !important">
                            <div class="mate-input-box">
                                <label>Cheque Date</label>
                                <input type="text" autocomplete="off" id="part_check_dt" class="form-control datepicker" name="part_check_dt" placeholder="Cheque Date">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm" style="display: none">
                            <div class="mate-input-box">
                                <label for="">Vouchr. Type</label>
                                <div class="clearfix"></div>
                                @php $v_type = \DB::table('ledger_voucher_type')->where('status', 1)->where('payment_receipt',1)->orderBy('type')->pluck('type', 'id'); @endphp
                                {!! Form::select('part_v_type',$v_type->toArray(),'',
                                ['class'=>"form-control v_type", 'id'=>"part_v_type"]) !!}
                            </div>
                        </div>
                        <div class="col-md-8 padding_sm" style="padding-top:10px !important">
                            <div class="mate-input-box">
                                <label for="">Remarks</label>
                                <div class="clearfix"></div>
                                <textarea style="height: 55px !important"  wrap="off" class="form-control" cols="35" rows="1" name="part_payment_naration" id="part_payment_naration" ></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" id="consolidate" >
                            <div class="table-responsive theadscroll" style=" width: 66%">
                                <table class="table table_round_border styled-table" style=" width: 50%">
                                    <tbody>
                                        <tr>
                                            <td class="common_td_rules" id="consolidate_led_name">
                                                <label>Amount</label>
                                                <input type="text" value="" name="pp_amount" id="pp_amount" class="form-control" placeholder="Partial Pay Amount">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-2 pull-right" style="margin-top: -50px;margin-right: 110px">
                                <button type="button" class="btn btn-block btn-primary" onclick="savePartialPayment()">
                                    <i class="fa fa-save" ></i>&nbsp;&nbsp;Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="modal-footer">
                            <input type="hidden" id="part_payment_head_id" value="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="credit_not_list" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 50%;">
            <!-- Modal content-->
            <div class="modal-content" style="height: 500px;overflow: auto">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Credit Note Attach</h4>
                </div>
                <div class="modal-body" id="credit_not_attach">

                </div>
            </div>
        </div>

    </div>
<div id="prt_div"  style="display:none"></div>
    <input type="hidden" id="tot_credit_note_amount" value="0">
    <input type="hidden" id="tot_credit_note_id" value="0">
    <input type="hidden" value="0" id="receive_ajax">
    <input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
    <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
    <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
    <script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
    @stop
    @section('javascript_extra')
    <script type="text/javascript">
            var ids = [];
            var ttl_amount_in_header = 0;
            var is_chk = 0;
            $(document).ready(function () {

            setTimeout(function () {
            // $('#menu_toggle').trigger('click');
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            }, 300);
            $('.month_picker').datetimepicker({
            format: 'MM'
            });
            $('.year_picker').datetimepicker({
            format: 'YYYY'
            });
            $('.datepickers').datetimepicker({
            format: 'MMM-DD-YYYY'
            });
            $('.date_time_picker').datetimepicker();

            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            });
            $("#purchase_paging").click(function (e) {
            var to_date_invoice = $("#to_date_invoice-0").val();
            var from_date_invoice = $("#from_date_invoice-0").val();
            var ledger_id_head = $("#ledger_id_head-1").val();
            e.preventDefault();
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            var param = {from_pagination: 1,ledger_id: ledger_id_head,to_date_invoice:to_date_invoice,from_date_invoice:from_date_invoice, page: $(this).attr('data-id')};
            $.ajax({
            url: "",
                    data: param,
                    dataType: 'html',
            }).done(function (data) {
            $('#append_tbdy').append(data);
            $.LoadingOverlay("hide");
            var load_btn = $("#purchase_paging").attr('data-id');
            var new_load_btn = parseFloat(load_btn) + 1;
            $("#purchase_paging").attr('data-id', new_load_btn);
            }).fail(function () {
            $.LoadingOverlay("hide");
            alert('Posts could not be loaded.');
            });
            });
            function viewConsolidate() {
            var detailArray = [];
            var amountArray = [];
            var checkSameArray = [];
            var invoiceArray = [];
            var party_name = '';
            var advance_amnt = 0;
            var i = 0;
            var urls = $("#req_base_url").val();
            $('input[name="detail_id[]"]:checked').each(function () {
            detailArray.push(this.value);
            amountArray.push($(this).attr('class'));
            checkSameArray.push($(this).attr('data-id'));
            invoiceArray.push($(this).attr('data-invoice'));
            advance_amnt = $(this).attr('data-advanceamnt');
            party_name = $(this).parents("tr").find("input[name='party_name[]']").val();
            i++;
            });
            if (checkSameArray.every((val, i, arr) => val === arr[0]) == false){
            toastr.warning("Please select same party");
            return false;
            }
            if (detailArray.length == 0) {
            toastr.error('Please select atleast one entry !!');
            return false;
            }
            var sum_amnt = amountArray.reduce(function(a, b){
            return parseFloat(a) + parseFloat(b);
            }, 0);
            var bills = "Payment for the bills ";
            jQuery.each(invoiceArray, function(index, item) {
               bills += item +',';
            });
            var bills_apprnd = bills.slice(0, -1);
            $("#payment_naration").val(bills_apprnd);
            $("#consolidate_led_name").html(party_name);
            $("#consolidate_bill_cnt").html(i);
            $("#consolidate_sum_amnt").html(sum_amnt);
            $("#ledger_id").val(detailArray);
            $("#modal_pending_consolidate").modal('show');
            $("#party_adv").html(advance_amnt);
//                                                            var param = {show_pending_consolidate: 1, detailArray: detailArray,sum_amnt:sum_amnt};
//                                                            $.ajax({
//                                                            type: "GET",
//                                                                    url: urls + "/accounts/viewConsolidatedPaymentList",
//                                                                    data: param,
//                                                                    beforeSend: function () {
//                                                                    // $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
//                                                                    },
//                                                                    success: function (html) {
//                                                                    $.LoadingOverlay("hide");
//                                                                    $("#append_pending").html(html);
//                                                                    },
//                                                                    complete: function () {
//                                                                    $("#modal_pending_consolidate").modal('show');
//                                                                    }
//                                                            });
            }
            function showVoucherDiv(e) {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            if ($(e).is(':checked')) {
            $("#voucherWise").show();
            $("#consolidate").hide();
            } else {
            $("#voucherWise").hide();
            $("#consolidate").show();
            }
            $.LoadingOverlay("hide");
            }
            function SavePayment() {
            var urls = $("#req_base_url").val();
            var ledger_id = $("#ledger_id").val();
            var bank_details = $("#bank_details").val();
            var payment_naration = $("#payment_naration").val();
            var cdv_no = $("#cdv_no").val();
            var check_no = $("#check_no").val();
            var check_dt = $("#check_dt").val();
            var tot_credit_note_amount = $("#tot_credit_note_amount").val();
            var tot_credit_note_id = $("#tot_credit_note_id").val();
            var party_adv_amnt = $("#party_adv_amnt").val();
            if (bank_details == '') {
            toastr.error('Please select Bank !!');
            return false;
            }
            if (check_dt == '') {
            toastr.error('Please select Check Date !!');
            return false;
            }
            var v_type = $("#v_type").val();
            var param = {tot_credit_note_amount:tot_credit_note_amount,tot_credit_note_id:tot_credit_note_id,
                ledger_id_array: ledger_id, cdv_no:cdv_no, check_no:check_no, check_dt:check_dt,
                payment_naration: payment_naration, save_receive: 1, bank_id: bank_details, v_type: v_type,party_adv_amnt:party_adv_amnt};
            $.ajax({
            type: "GET",
                    url: urls + "/accounts/savePayment",
                    data: param,
                    beforeSend: function () {
                    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                    },
                    success: function (data) {
                    $.LoadingOverlay("hide");
                    if (data == 1) {
                    toastr.success('Added successfully');
                    } else {
                    toastr.error('Error occured !!');
                    }
                    },
                    complete: function () {
                    $("#modal_pending_consolidate").modal('hide');
                    setTimeout(function () {
                     window.location.reload();
                    },300);
                    }
            });
            }
            function receivedDetails(from_search=0) {
            var ledger_id_head = $("#ledger_id_head-1").val();
            var to_date_invoice = $("#to_date_invoice-0").val();
            var from_date_invoice = $("#from_date_invoice-0").val();
            var urls = $("#req_base_url").val();
            var param = {received: 1,ledger_id:ledger_id_head,from_date_invoice:from_date_invoice,to_date_invoice:to_date_invoice};
            if ($("#receive_ajax").val() == 0 || from_search == 1) {
            $.ajax({
            type: "GET",
                    url: urls + "/accounts/viewPaidBills",
                    data: param,
                    beforeSend: function () {
                    $("#pending_label").hide();
                    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                    },
                    success: function (html) {
                    $.LoadingOverlay("hide");
                    $("#tab_2").html(html);
                    },
                    complete: function () {
                    $("#receive_ajax").val('1');
                    var ttl = $("#show_total_paid_bills").val();
                    var ttl_paid_amnt = $("#total_paid_amount_hidden").val();
                    $("#show_total_paid_bills_span").html(ttl_paid_amnt+'&nbsp;[Total bill No:&nbsp;'+ttl+']');
                    $("#payment_label").show();
                    }
            });
            } else {
            $("#pending_label").hide();
            $("#payment_label").show();
            }
            }
            function LabelChange() {
            $("#payment_label").hide();
            $("#pending_label").show();
            }
            $(".checkAll").click(function () {
            $('input[name="detail_id[]"]').not(this).prop('checked', this.checked);
            });
            $("#item_search_btn").click(function () {
            $("#issue_search_box").toggle();
            document.getElementById("issue_search_box").focus();
            $("#item_search_btn_text").toggle();
            });
            function searchProducts() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("issue_search_box");
            filter = input.value.toUpperCase();
            table = document.getElementById("receipt_table");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            label = td.getElementsByTagName("label");
            if (td) {
            if (td.textContent.toUpperCase().indexOf(filter) > - 1) {
            tr[i].style.display = "";
            } else {
            tr[i].style.display = "none";
            }
            }
            }
            }
            function select_item_desc(id, event, type, is_header = 0) {
            var amount_header = $("#amount_header").val();
            var party_header = $("#ledger_id_head-0").val();
            if (id == "ledger_item_desc-1") {
            if (amount_header == '' || party_header == '') {
            toastr.warning("Please fill Header section");
            $("#" + id).val('');
            return false;
            }
            }
            var keycheck = /[a-zA-Z0-9 ]/;
            id = id.split('-');
            id = id[1];
            var value = event.key;
            var ajax_div = 'search_ledger_item_box-' + id;
            var ledger_desc = $('#ledger_item_desc-' + id).val();
            if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {

            if (ledger_desc == "") {
            $("#" + ajax_div).html("");
            } else {
            var url = $("#req_base_url").val();
            $.ajax({
            type: "GET",
                    url: url + "/accounts/searchLedgerMaster",
                    data: 'ledger_desc=' + encodeURIComponent(ledger_desc) + '&type=1&search_ledger_desc=1&is_header=1',
                    beforeSend: function () {
                    $("#" + ajax_div).css('top', '');
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).css('margin-left', '310px');
                    if (parseFloat($("#" + ajax_div + " li").size()) > 15 && id != '0') {
                    $("#" + ajax_div).css('top', '-12vh');
                    $("#" + ajax_div).css('position', 'absolute');
                    } else {
                    $("#" + ajax_div).css('top', '');
                    }

                    $("#" + ajax_div).css('z-index', '1004');
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    }
            });
            }

            } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            }
            }
            function fillParty(e, ledger_name, id, ledger_group_id){
            $("#ledger_item_desc-0").val(ledger_name);
            $("#ledger_id_head-0").val(id);
            $("#ledger_id_head-1").val(id);
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            var param = {from_pending: 1, ledger_id: id};
            $.ajax({
            url: "",
                    data: param,
                    dataType: 'html',
            }).done(function (data) {
            $('#tab_1').html(data);
            var pending_total = $("#pending_total").val();
            $("#pending_label_span").html(pending_total);
            receivedDetails(1);
            $.LoadingOverlay("hide");
            }).fail(function () {
            $.LoadingOverlay("hide");
            alert('Posts could not be loaded.');
            });
            }
            function change_date(e){

            var ledger_id_head = $("#ledger_id_head-1").val();
            var to_date_invoice = $("#to_date_invoice-0").val();
            var from_date_invoice = $("#from_date_invoice-0").val();

            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            var param = {from_pending: 1, ledger_id: ledger_id_head,to_date_invoice:to_date_invoice,from_date_invoice:from_date_invoice};
            $.ajax({
            url: "",
                    data: param,
                    dataType: 'html',
            }).done(function (data) {
            $('#tab_1').html(data);
            var pending_total = $("#pending_total").val();
            $("#pending_label_span").html(pending_total);
            receivedDetails(1);
            $.LoadingOverlay("hide");
            }).fail(function () {
            $.LoadingOverlay("hide");
            alert('Posts could not be loaded.');
            });
            }
            function checkIsSameParty(e, cls, amnt){
            if (!ids.includes(cls) && ids.length > 0){
            toastr.warning("Please select same Party");
            $(e).prop('checked', false);
            return false;
            } else{
            if ($(e).prop('checked')) {
            ids.push(cls);
            ttl_amount_in_header = parseFloat(ttl_amount_in_header) + parseFloat(amnt);
            $("#ttl_selected_amount_label_span").html(parseFloat(ttl_amount_in_header, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            } else{
            ids.pop(cls);
            ttl_amount_in_header = parseFloat(ttl_amount_in_header) - parseFloat(amnt);
            $("#ttl_selected_amount_label_span").html(parseFloat(ttl_amount_in_header, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            }
            console.log(ids);
            }
            }
            function ProcessPartialPayment(e,head_id,amnt){
                $("#part_payment").modal('show');
                $("#part_payment_head_id").val(head_id);
            }
            function savePartialPayment(){
            var urls = $("#req_base_url").val();
            var head_id = $("#part_payment_head_id").val()
            var amount = $("#pp_amount").val();
            var part_bank_details = $("#part_bank_details").val();
            var part_payment_naration = $("#part_payment_naration").val();
            var part_cdv_no = $("#part_cdv_no").val();
            var part_check_no = $("#part_check_no").val();
            var part_check_dt = $("#part_check_dt").val();
            var v_type = $("#part_v_type").val();
              if (part_bank_details == '') {
            toastr.error('Please select Bank !!');
            return false;
            }
            var param = {pp_amount:amount,head_id:head_id,part_bank_details:part_bank_details,part_payment_naration:part_payment_naration,part_cdv_no:part_cdv_no,part_check_no:part_check_no,part_check_dt:part_check_dt,v_type:v_type};
            $.ajax({
            type: "GET",
                    url: urls + "/accounts/savePartialPayment",
                    data: param,
                    beforeSend: function () {
                    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                    },
                    success: function (html) {
                    $.LoadingOverlay("hide");
                     window.location.reload();
                    },
                    complete: function () {
                    $("#part_payment_head_id").val('');
                    }
            });
            }
            function datarst(){
                window.location.reload();
            }
            function creditNoteImport(){
            $("#tot_credit_note_amount").val(0);
            $("#tot_credit_note_id").val(0);
          $('input[name="detail_id[]"]:checked').each(function () {
            partys = $(this).attr('data-id');
            });
            var param = {ledger_id:partys};
            var urls = $("#req_base_url").val();
            $.ajax({
            type: "GET",
                    url: urls + "/accounts/processCreditNote",
                    data: param,
                    beforeSend: function () {
                    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                    },
                    success: function (html) {
                    $("#credit_not_attach").html(html);
                    $("#credit_not_list").modal('show');
                    $.LoadingOverlay("hide");
                    },
                    complete: function () {
                    }
            });
            }
            function SavecreditNote(){
            if (confirm("Are you sure ?")) {
            var creditNotArray = [];
            var creditNoteAmount = [];
            var creditNoteVoucher = [];
            var creditNoteDate = [];
            var creditNoteNo = [];
            var total_credit_note_amount = 0;
            var credit_html_tbl = '';
            var credit_not_naration = '';
            credit_not_naration += ' credit not attached with credit no:';
            credit_html_tbl += '<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"\n\
                                             style="border: 1px solid #CCC;" id="receipt_table1" style="margin: 0px 0px !important">\n\
                                        <thead>\n\
                                            <tr>\n\
                                            <th style="text-align:left;width:10%" colspan="3">Credit Note</th></tr>\n\
                                            <tr class="table_header_bg" style="background: #53aab3 !important;">\n\
                                            <th style="text-align:center;width:10%">Voucher No</th>\n\
                                            <th style="text-align:center;width:10%">Credit No</th>\n\
                                            <th style="text-align:center;width:10%">Date</th>\n\
                                            <th style="text-align:center;width:8%">Amount</th>\n\
                                            </tr>\n\
                                </thead>\n\
                                <tbody>';
            $('input[name="credit_note_id[]"]:checked').each(function () {
            creditNotArray.push(this.value);
            creditNoteAmount.push($(this).attr('data-id'));
            creditNoteDate.push($(this).attr('data-date'));
            creditNoteVoucher.push($(this).attr('data-voucher'));
            creditNoteNo.push($(this).attr('data-creditno'));
            });
            for (var i = 0; i < creditNoteAmount.length; i++) {
                total_credit_note_amount += parseFloat(creditNoteAmount[i]) ;
                credit_not_naration += creditNoteNo[i] +':'+creditNoteAmount[i]+',';
                credit_html_tbl += '<tr>\n\
                                            <td class="common_td_rules">'+ creditNoteVoucher[i] +'</td>\n\
                                            <td class="common_td_rules">'+ creditNoteNo[i] +'</td>\n\
                                            <td class="common_td_rules">'+ creditNoteDate[i] +'</td>\n\
                                            <td class="td_common_numeric_rules">'+ creditNoteAmount[i] +'</td>\n\
                                    </tr>';

            }
            credit_html_tbl += '</tbody></table>';
            credit_not_naration +=' and total credit not amount is '+total_credit_note_amount;
            var payment_naration_cr = $("#payment_naration").val();
            var full_narration = payment_naration_cr+'-'+credit_not_naration
            $("#payment_naration").val(full_narration);
            $("#tot_credit_note_amount").val(total_credit_note_amount);
            $("#tot_credit_note_id").val(creditNotArray);
            var consolidate_sum_amnt_c = $("#consolidate_sum_amnt").html();
            var amount_after_credit_note_attach = $("#amount_after_credit_note_attach").html();
            if(parseFloat(amount_after_credit_note_attach) > 0){
            consolidate_sum_amnt_c =  parseFloat(amount_after_credit_note_attach)-parseFloat(total_credit_note_amount);
            }else{
                 consolidate_sum_amnt_c =  parseFloat(consolidate_sum_amnt_c)-parseFloat(total_credit_note_amount);
            }
            $("#amount_after_credit_note_attach").html(consolidate_sum_amnt_c);
            $("#amount_after_credit_note_attach_div").show();
            $("#credit_not_div").html(credit_html_tbl);
                       var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            $("#creditNoteImportbtn").hide();
            $("#credit_not_list").modal('hide');
                }
            }
            function creditNotCheck(e){
                var tot_checked_credit_amount = $("#tot_checked_credit_amount").html();
                if($(e).prop('checked')){
                   var ttls_checked = parseFloat(tot_checked_credit_amount)+parseFloat($(e).attr('data-id'));
                   $("#tot_checked_credit_amount").html(ttls_checked);
                }else{
                   var ttls_checked = parseFloat(tot_checked_credit_amount)-parseFloat($(e).attr('data-id'));
                   $("#tot_checked_credit_amount").html(ttls_checked);
                }
            }
            function number_validation(e) {
                var adv_val =  $("#party_adv").html();
                var cur_val = $(e).val();
                var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value);
                val = e.value;
                if (!valid) {
                    $(e).val(0);
                }else if(parseFloat(adv_val) < parseFloat(cur_val)){
                       $(e).val(0);
                   }else if(cur_val.length < 1){
                       $(e).val(0);
                   }
            }
            function processTotal(e){
                var ad_val = $("#party_adv_amnt").val();
                console.log(ad_val);
                if($("#amount_after_credit_note_attach").html() !='' && $("#amount_after_credit_note_attach").html() != 0  && ($(".credit_note_id").length > 0)){
                    var consolidate_sum_amnt_c = $("#consolidate_sum_amnt").html();
                    consolidate_sum_amnt_c = parseFloat(consolidate_sum_amnt_c)-parseFloat($("#tot_credit_note_amount").val())-parseFloat(ad_val);
                }else{
                    var consolidate_sum_amnt_c = $("#consolidate_sum_amnt").html();
                    consolidate_sum_amnt_c = parseFloat(consolidate_sum_amnt_c)-parseFloat(ad_val);
                  }
                  console.log("dd"+consolidate_sum_amnt_c);
                  $("#amount_after_credit_note_attach").html(consolidate_sum_amnt_c);
                  $("#amount_after_credit_note_attach_div").show();
            }
            @include('Purchase::messagetemplate')
    </script>

    @endsection
