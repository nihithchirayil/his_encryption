@if($offset == 0)
<div class="row">
    <div class="col-md-12 theadscroll" id="load_data" style="position: relative; height: 350px;">
        <table class="table table_round_border theadfix_wrapper styled-table" id="receipt_table1">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align:center;width:10%">Date</th>
                    <th style="text-align:center;width:10%">Column</th>
                    <th style="text-align:center;width:20%">Old Value</th>
                    <th style="text-align:center;width:20%">New Value</th>
                    <th style="text-align:center;width:20%">User</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($receivedData))
                @foreach($receivedData as $rec)
                <tr style="cursor: pointer"  data-narations="1">
                    <?php 
                    if($rec->column_name == 'group_id' || $rec->column_name == 'ledger_id'){
                       $ledgers_old = \DB::table('ledger_master')->where('id',$rec->old_value)->pluck('ledger_name');
                       $ledger_old = $ledgers_old[0]?$ledgers_old[0]:'';
                       $ledgers_new = \DB::table('ledger_master')->where('id',$rec->new_value)->pluck('ledger_name');
                       $ledger_new = $ledgers_new[0]?$ledgers_new[0]:'';
                    }else{
                        $ledger_new = $rec->new_value ? $rec->new_value :'';
                        $ledger_old = $rec->old_value ? $rec->old_value :'';
                    }
                    if($rec->column_name == 'group_id'){
                        $column = 'Group';
                    }
                    else if($rec->column_name == 'ledger_id'){
                        $column = 'Ledger';
                    }
                    else if($rec->column_name == 'cr_dr'){
                        $column = 'Credit/Debit';
                    }else{
                        $column = $rec->column_name;
                    }
                    ?>
                    <td class="common_td_rules">{{ !empty($rec->date_time) ?$rec->date_time :'' }}</td>
                    <td class="common_td_rules">{{ $column }}</td>
                    <td class="common_td_rules">{{ $ledger_old }}</td>
                    <td class="common_td_rules">{{ $ledger_new }}</td>
                    <td class="common_td_rules">{{ !empty($rec->user) ? $rec->user:'' }}</td>
                    
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<script>

    $('#load_data').on('scroll', function() {
        var scrollHeight = $('#load_data').height();
        //var scrollPosition = $('#load_data').height() + $('#load_data').scrollTop();
        var scrollPosition = $('#load_data').scrollTop() + $('#load_data').innerHeight();
        if(scrollPosition+3 >= $('#load_data')[0].scrollHeight){
        //if (scrollPosition > scroll_length_initial) {
            //alert('ddd');
            offset = offset+limit;
            //limit = limit+15;
            console.log(limit+'##'+offset+'##'+total_rec);
            if(offset < total_rec){
                setTimeout(function(){
                    searchList(limit,offset);
                },500);
                //searchList(limit,offset);
            }
        }
    })
</script>
@else
    @if(isset($receivedData))
    @foreach($receivedData as $rec)
    <tr style="cursor: pointer">
              <?php 
                    if($rec->column_name == 'group_id' || $rec->column_name == 'ledger_id'){
                       $ledgers_old = \DB::table('ledger_master')->where('id',$rec->old_value)->pluck('ledger_name');
                       $ledger_old = $ledgers_old[0]?$ledgers_old[0]:'';
                       $ledgers_new = \DB::table('ledger_master')->where('id',$rec->new_value)->pluck('ledger_name');
                       $ledger_new = $ledgers_new[0]?$ledgers_new[0]:'';
                    }else{
                        $ledger_new = $rec->new_value ? $rec->new_value :'';
                        $ledger_old = $rec->old_value ? $rec->old_value :'';
                    }
                    if($rec->column_name == 'group_id'){
                        $column = 'Group';
                    }
                    else if($rec->column_name == 'ledger_id'){
                        $column = 'Ledger';
                    }
                    else if($rec->column_name == 'cr_dr'){
                        $column = 'Credit/Debit';
                    }else{
                        $column = $rec->column_name;
                    }
                    ?>
                    <td class="common_td_rules">{{ !empty($rec->date_time) ?$rec->date_time :'' }}</td>
                    <td class="common_td_rules">{{ $column }}</td>
                    <td class="common_td_rules">{{ $ledger_old }}</td>
                    <td class="common_td_rules">{{ $ledger_new }}</td>
                    <td class="common_td_rules">{{ !empty($rec->user) ? $rec->user:'' }}</td>
    </tr>
    @endforeach
    @else
    <tr>
        <td colspan="5">No Records Found</td>
    </tr>
    @endif
@endif