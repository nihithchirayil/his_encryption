<style>
.excludeItemButton{
    display:none;
}
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">

            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
            <?php
            $collect1 = collect($pl_res);
            $collect2 = collect($bs_res);
            $total_records = count($collect1)+count($collect2);
            ?>
<!--            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>-->
            <h4 style="text-align: center;margin-top: -10px;" id="heading"><b> </b></h4>
            <div class="print_data" style="margin-top: 10px" id="print_data" >
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>
                    @if(sizeof($bs_res)>0 || sizeof($pl_res)>0)
                    @php
                        $sort_array = array();
                        $i = 1;
                        $group_id   = 0;
                        $credit_total = $debit_total = $current_credit = 0;
                    @endphp

                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="30%" colspan="7">Account</th>
                        <th width="8%">Debit</th>
                        <th width="8%">Credit</th>
                    </tr>

                    @foreach ($bs_res as $bs_data)
                        <?php $fn_name = "showSingleAmountDetailTrail(this," .$bs_data->parent_id . ",'bs');"; ?>
                    {{-- @if($bs_data->dr_amount > 0 || $bs_data->cr_amount > 0) --}}
                    <tr style="cursor:pointer;border: thin solid #d7d7d7;" id="{{$bs_data->parent_id}}"class="expand_all_trail" >
                        <td onclick="<?= $fn_name ?>" class="common_td_rules" style="width:22%;padding-right: 15%" colspan="7">
                            {{$bs_data->parent_name}}</td>
                        <?php if(strtolower($bs_data->parent_name) == 'current assets'){
                            $current_debit  =  $trail_opening_stock + $bs_data->dr_amount;
                        }else{
                            $current_debit  =  $bs_data->dr_amount;
                        } ?>
                        <td onclick="<?= $fn_name ?>" style='font-size: 14px;' class="td_common_numeric_rules"><b class="debit_amount">{{number_format($current_debit, 2, '.', ',')}}</b></td>
                        <td onclick="<?= $fn_name ?>" style='font-size: 14px;' class="td_common_numeric_rules"><b class="credit_amount">{{number_format($bs_data->cr_amount, 2, '.', ',')}}</b></td>
                        <td class="common_td_rules excludeItemButton" style=" width:1%;"><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this,'{{$bs_data->parent_id}}');" ><i class="fa fa-minus"></i></button></td>
                    </tr>
                    @php $i++;
                    $credit_total += $bs_data->cr_amount;
                    $debit_total += $current_debit;
                    @endphp
                    {{-- @endif --}}
                @endforeach
                @foreach ($pl_res as $pl_data)
                        <?php $fn_name = "showSingleAmountDetailTrail(this," .$pl_data->parent_id . ",'pl');"; ?>
                    @if($pl_data->dr_amount > 0 || $pl_data->cr_amount > 0)
                    <tr style="cursor:pointer;border: thin solid #d7d7d7;" id="{{$pl_data->parent_id}}" class="expand_all_trail" >
                        <td onclick="<?= $fn_name ?>" class="common_td_rules" style="width:22%;padding-right: 15%" colspan="7">
                            {{$pl_data->parent_name}}</td>
                        <td onclick="<?= $fn_name ?>" style='font-size: 14px;' class="td_common_numeric_rules"><b class="debit_amount">{{number_format($pl_data->dr_amount, 2, '.', ',')}}</b></td>
                        <td onclick="<?= $fn_name ?>" style='font-size: 14px;' class="td_common_numeric_rules"><b class="credit_amount">{{number_format($pl_data->cr_amount, 2, '.', ',')}}</b></td>
                        <td class="common_td_rules excludeItemButton" style=" width:1%;"><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this,'{{$pl_data->parent_id}}');" ><i class="fa fa-minus"></i></button></td>
                    </tr>
                    @php $i++;
                    $credit_total += $pl_data->cr_amount;
                    $debit_total += $pl_data->dr_amount;
                    @endphp
                    @endif
                @endforeach
                <tr style="cursor:pointer;border: thin solid #d7d7d7;" >
                        <td class="common_td_rules" style="width:22%;padding-right: 15%" colspan="7">
                           P&L Account</td>
                        <?php
                        if($proft_loss==1){
                            $credit_total = $credit_total + abs($net_profit); ?>
                        <td style='font-size: 14px;' class="td_common_numeric_rules debit_amount"></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules"><b class="credit_amount">{{number_format(abs($net_profit), 2, '.', ',')}}<b></td>
                       <?php }else{
                            $debit_total = $debit_total + abs($net_profit); ?>
                        <td style='font-size: 14px;' class="td_common_numeric_rules" ><b class="debit_amount"><b>{{number_format(abs($net_profit), 2, '.', ',')}}</b></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules credit_amount"></td>
                       <?php } ?>
                    </tr>
                    <tr style="cursor:pointer;border: thin solid #d7d7d7;" >
                        <td class="common_td_rules" style="width:22%;padding-right: 15%" colspan="7">
                            <b> Total </b> </td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules"><b class="total_debit_amount">{{number_format($debit_total, 2, '.', ',')}}</b></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules"><b class="total_credit_amount">{{number_format($credit_total, 2, '.', ',')}}</b></td>
                    </tr>
                @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                @endif
            </table>
        </div>
    </div>
</div>

