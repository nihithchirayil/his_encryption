<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        font-size: 12px;
        font-family:robotoregular !important;
    }
    td{
        padding:5px;
        width:10%; 
    }
    .td_head_cls {
        background-color: #c6d4e5;
        font-weight:bold;
    }
/*tr:hover {
    color: blue !important;
}*/
hr{
margin-top: 0px;
margin-bottom: 1px;
border: 0;
border-top: 1px solid black;
}
</style>
<!-- 
GROSS PROFIT = (SALES A/C + CLOSING STOCK) - (OPENING STOCK + PURCHASE A/C)
NET PROFIT = (GROSS PROFIT + INDIRECT INCOME - INDIRECT EXPENSE)
TOTAL =  (GROSS PROFIT + INDIRECT INCOME)
-->
<?php
$sort_liab_array = $sort_asset_array = array();
$asset_total = $liab_total = $gross_profit = $opening_group = $purchase_group = $closing_group = $sales_group = $indirect_expense = $indirect_income = $show_net_profit = 0;
?>
<div class="row">
    <div class="col-md-12" id="result_container_div" border='2px !important' >
        @if($company_code=='DAYAPKD')
        <table id="result_data_table" class="" >
            <tr >
                <td colspan="6"><b>DAYA SPECIALITY SURGICAL & LAPAROSCOPIC CENTER- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</b></td>


            </tr>
            <tr >
                <td colspan="6">(A Unit of Daya General Hospital Ltd. Thrissur)</td>


            </tr>
            <tr>
                <td colspan="6">Opp. GLP School, Kunnathurmedu, Palakkad</td>

            </tr>
            <tr>
                <td colspan="6">Palakkad</td>


            </tr>
            <tr >
                <td colspan="6"><b>Profit & Loss A/c</b></td>

            </tr>
            <tr >
                <td colspan="6" >{{$from_date}} to {{$to_date}}</td>

            </tr>
        </table>
        @else
        <table id="result_data_table" class="" >
            <tr >
                <td colspan="6"><b>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</b></td>


            </tr>
            <tr >
                <td colspan="6">(A Unit of Medical Centre Trichur Ltd)</td>


            </tr>
            <tr>
                <td colspan="6">Near Viyyur Bridge,Shoranur Road</td>

            </tr>
            <tr >
                <td colspan="6">Thrissur</td>


            </tr>
            <tr >
                <td colspan="6"><b>Profit & Loss A/c</b></td>

            </tr>
            <tr >
                <td colspan="6" >{{$from_date}} to {{$to_date}}</td>

            </tr>
        </table>
        @endif
        <?php
        $j = $showDirectIncome = $show_net_prof = 0;


// =============================================== ASSET ARRAY REARRANGE ==============================
        $openIndex = array_search('Opening Stock', array_column($liability_results, 'ledger_name'));
        //$open_stock1 = $liability_results[$openIndex]->amount ?$liability_results[$openIndex]->amount:0 ;
        $open_stock = $open_stock_results[0]->closing_stock ? $open_stock_results[0]->closing_stock : 0;

        $purchaseIndex = array_search('Purchase Accounts', array_column($liability_results, 'ledger_name'));
        $purchasestock = $liability_results[$purchaseIndex]->amount ? $liability_results[$purchaseIndex]->amount : 0;

        $indExpIndex = array_search('Indirect Expenses', array_column($liability_results, 'ledger_name'));
        $indexpstock = $liability_results[$indExpIndex]->amount ? $liability_results[$indExpIndex]->amount : 0;

        $salesIndex = array_search('Sales Accounts', array_column($asset_results, 'ledger_name'));
        $salesstock = $asset_results[$salesIndex]->amount ? $asset_results[$salesIndex]->amount : 0;

        $afterIndex = array_search('Closing Stock', array_column($asset_results, 'ledger_name'));
       // $closingstock = $asset_results[$afterIndex]->amount ? $asset_results[$afterIndex]->amount : 0;
        //$closingstock = $open_stock_results[0]->closing_stock ? $open_stock_results[0]->closing_stock:0;
        $closingstock = $closing_stock_results[0]->closing_stock ? $closing_stock_results[0]->closing_stock : 0;
        $before_indirect_income = $salesstock + $closingstock;
        $gross_profit = $before_indirect_income - ($purchasestock + $open_stock);
        $gross_profit_ledger ="Gross Profit";
        $is_profit =1;
        if($gross_profit<0){
            $gross_profit_ledger ="Gross Loss";
            $gross_profit = ($purchasestock + $open_stock)-$before_indirect_income ;
            $is_profit =0;
            $before_indirect_income = ($purchasestock + $open_stock);

        }

        $newVal = array((object) array('typ' => 2, 'id' => '-1', 'ledger_name' => 'Direct Income', 'display_order' => 3, 'amount' => '', 'is_ledger' => 1));
       // $asset_results = array_merge(array_slice($asset_results, 0, $afterIndex), $newVal, array_slice($asset_results, $afterIndex));

        $afterIndex1 = array_search('Indirect Incomes', array_column($asset_results, 'ledger_name'));
        $indirectstock = $asset_results[$afterIndex1]->amount ? $asset_results[$afterIndex1]->amount : 0;
        if($is_profit==1){
        $newVal1 = array((object) array('typ' => 2, 'id' => '-2', 'ledger_name' => '', 'display_order' => 3, 'amount' => $before_indirect_income, 'is_ledger' => 1),
            (object) array('typ' => 2, 'id' => '-3', 'ledger_name' => $gross_profit_ledger.' b/f', 'display_order' => 3, 'amount' => $gross_profit, 'is_ledger' => 1));
        }else{
          $newVal1 =  array((object) array('typ' => 2, 'id' => '-3', 'ledger_name' => $gross_profit_ledger.' c/o', 'display_order' => 3, 'amount' => $gross_profit, 'is_ledger' => 1),
              (object) array('typ' => 2, 'id' => '-2', 'ledger_name' => '', 'display_order' => 3, 'amount' => $before_indirect_income, 'is_ledger' => 1)
            );
        }
        $asset_results = array_merge(array_slice($asset_results, 0, $afterIndex1), $newVal1, array_slice($asset_results, $afterIndex1));
// ====================================================================================================================================================
        //=============================================== LIABILITY ARRAY REARANGE ================================
        $afterIndex_liab = array_search('Indirect Expenses', array_column($liability_results, 'ledger_name'));
        if($is_profit==1){
        $newVal_liab = array((object) array('typ' => 1, 'id' => '-6', 'ledger_name' => $gross_profit_ledger.' c/o', 'display_order' => 3, 'amount' => $gross_profit, 'is_ledger' => 1),
            (object) array('typ' => 1, 'id' => '-3', 'ledger_name' => '', 'display_order' => 3, 'amount' => $before_indirect_income, 'is_ledger' => 1));
        }else{
            $newVal_liab = array((object) array('typ' => 1, 'id' => '-3', 'ledger_name' => '', 'display_order' => 3, 'amount' => $before_indirect_income, 'is_ledger' => 1),
                (object) array('typ' => 1, 'id' => '-6', 'ledger_name' => $gross_profit_ledger.' b/f', 'display_order' => 3, 'amount' => $gross_profit, 'is_ledger' => 1));
        }
        $liability_results = array_merge(array_slice($liability_results, 0, $afterIndex_liab), $newVal_liab, array_slice($liability_results, $afterIndex_liab));
        //=====================================================================================================  
        
        //=================================================NET PROFIT AND NET LOSS ============================
        
            $afterIndex_liab_net = array_search('Indirect Expenses', array_column($liability_results, 'ledger_name'));
            $dd_exp = $liability_results[$afterIndex_liab_net]->amount;
            $afterIndex_asset_net = array_search('Indirect Incomes', array_column($asset_results, 'ledger_name'));
            $dd_inc = $asset_results[$afterIndex_asset_net]->amount;
            if($is_profit==0){
               $net_loss = ($dd_exp+$gross_profit)-$dd_inc;
            }else{
               $net_loss = ($dd_inc+$gross_profit)-$dd_exp;
            }
             $net_loss1 = ($dd_exp+$gross_profit)-$dd_inc;
             $net_loss2 = ($dd_inc+$gross_profit)-$dd_exp;
            if($net_loss2>$net_loss1){
                $is_profit=0;
            }else{
                $is_profit=1;
            }
            if(($dd_inc+$gross_profit)>$dd_exp){
                $prof_side=1;
            }else{
                $prof_side=0;
            }
            
//            if($net_loss1<$net_loss2){
//                $prof_side=1;
//            }else{
//                $prof_side=0;
//            }
            
        //========================================================================================================
        ?>

<table width="100%" border="1" style="border-collapse: collapse;">
	<tbody>
		<tr>
        <td style="vertical-align: top;padding: 0;">
        <table border="0" style="border-collapse: collapse;">
                <tr>
                    <td  style="border-bottom: thin solid black;" colspan="4"><b>Expense</b>
                        <b style="float: right">{{$from_date}} to {{$to_date}}</b></td>
                </tr>

                <?php
                for ($i = 0; $i < sizeof($liability_results); $i++) {
                    if($liability_results[$i]->ledger_name == 'Opening Stock'){
                       $show_sub = "onclick=showOpeningDetail()";
                    }else if ($liability_results[$i]->is_ledger == 0) {
                        $show_sub = "onclick=showSingleAmountDetail(this," . $liability_results[$i]->id . ",'balance_amnt')";
                    }else {
                        $show_sub = "";
                    }

                    if (isset($liability_results[$i])) {
                        if (!in_array($liability_results[$i]->id, $sort_liab_array)) {
                            $sort_liab_array[] = $liability_results[$i]->id;
                            $sort_liab_order = true;
                        }
                    }
                    ?>

                    <tr   id="liab_{{$i}}">
                        <?php if ($sort_liab_order) { ?>
                        <td <?= $show_sub ?> style="cursor: pointer;width:80%" >{{$liability_results[$i]->ledger_name??''}}
                                </td>
                            <?php if ($liability_results[$i]->ledger_name == '') { ?>
                                <td  style=" "></td>
                                <?php 
                                        if($is_profit == 1){ ?>
                                <td style="cursor: pointer;text-align:right"><span style="text-decoration:underline;text-decoration-style: double;">
                                        <b>
                                       <?php echo number_format($gross_profit+$purchasestock + $open_stock, 2, '.', ','); ?>
                                        </b></span></td>
                                        <?php }else{ ?>
                                        <td style="cursor: pointer;text-align:right;"><span style="text-decoration:underline;text-decoration-style: double;">
                                                <?php echo number_format($liability_results[$i]->amount, 2, '.', ','); ?>
                                       <?php } ?></span></td>
                            <?php } else { ?>
                                <td style=""></td>
                                <td  style="cursor: pointer;text-align:right">
                                        <?php
                                        if ($liability_results[$i]->ledger_name != '' && $liability_results[$i]->ledger_name == 'Opening Stock') {
                                            echo number_format($open_stock, 2, '.', ',');
                                        } else {
                                            echo number_format( $liability_results[$i]->amount, 2, '.', ',');
                                        }
                                        ?>
                                    </td> 
                                <?php
                            }
                        }
                        ?> 


                        <?php
                        $sort_liab_order = false;
                    }
                    ?>
                </tr>
                <?php if($prof_side==1){ ?>
                <tr>
                    <td><b>Net Profit</b></td>
                    <td></td>
                    <td style="text-align:right"><b>{{abs($net_loss)}}</b></td>
                    <?php $grand_total = $net_loss+$dd_exp; ?>
                </tr>
                <?php                                if(($salesstock + $closingstock)<($purchasestock + $open_stock)){
                                $gr_bf = abs(($salesstock + $closingstock)-($purchasestock + $open_stock));
                            }else{
                                $gr_bf = 0;
                            }
              if($gr_bf > 0){
                     $grand_total = abs($net_loss)+abs($dd_exp)+abs($gr_bf);
                }
                
                } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
               
        </table>
        </td>
		<td style="padding: 0;vertical-align: top;">
        <table border="0" style="margin-left: -1px;border-collapse: collapse;">
                <tr>
                    <td   style="border-bottom: thin solid black;" colspan="4"><b>Income</b>
                        <b style="float: right">{{$from_date}} to {{$to_date}}</b></td>
                </tr>

                <?php
                for ($i = 0; $i < sizeof($asset_results); $i++) {
                    if ($asset_results[$i]->is_ledger == 0) {
                        $show_sub = "onclick=showSingleAmountDetail(this," . $asset_results[$i]->id . ",'balance_amnt')";
                    } else {
                        $show_sub = "";
                    }
                    if (isset($asset_results[$i])) {
                        if (!in_array($asset_results[$i]->id, $sort_asset_array)) {
                            $sort_asset_array[] = $asset_results[$i]->id;
                            $sort_asset_order = true;
                        }
                    }
                    ?>

                    <tr id="asset_{{$i}}">
                        <?php if ($sort_asset_order) { ?>
                            <td  <?= $show_sub ?> style="cursor: pointer;width: 80%" >{{$asset_results[$i]->ledger_name??''}}</td>
                            <?php if ($asset_results[$i]->ledger_name == '') { ?>
                                <td  style="cursor: pointer;text-align:right"><b></b></td>
                                <td  style="cursor: pointer;text-align:right;" >
                                    <span style="text-decoration:underline;text-decoration-style: double;">
                                        <b><?php echo number_format($asset_results[$i]->amount, 2, '.', ','); ?></b></span></td>
                            <?php } else { ?>
                                <td  style="cursor: pointer;text-align:right" ></td>
                                <td  style="cursor: pointer;text-align:right" >
                                        <?php
                                        if ($asset_results[$i]->ledger_name != '' && $asset_results[$i]->ledger_name == 'Closing Stock') {
                                            echo number_format($closingstock, 2, '.', ',');
                                        } else {
                                            echo number_format($asset_results[$i]->amount, 2, '.', ',');
                                        }
                                        ?>
                                    </td> 
                                <?php
                            }
                        }  ?>
                    </tr>

                    <?php
                    $sort_asset_order = false;
                }
                if($prof_side==0){ ?>
                <tr>
                    <td><b>Net Loss</b></td>
                    <td></td>
                    <td style="text-align:right"><b>{{number_format(abs($net_loss), 2, '.', ',')}}</b></td>
                </tr>
                <?php 
                if($is_profit==1 && $net_loss > 0){
                     $grand_total = abs($net_loss)+abs($dd_inc);
                }else{
                     $grand_total = abs($net_loss)+abs($dd_inc)+abs($gross_profit);
                }
               
                
                } ?>

            </table>
			</td>
		</tr>
		<tr>
        <td style="padding: 0;vertical-align: top;margin-top: -1px;">
                <table border="0" style="border-collapse: collapse;margin-top: -1px;" >
                    <tr>
                    <td colspan="3" style="cursor: pointer;padding: 0px;padding-top: 5px;width:80%" ><b style="padding-left:5px">Total</b></td>
                    <td style="cursor: pointer;text-align:right;padding: 0px;padding-top: 5px"><b><?= number_format(abs($grand_total), 2, '.', ',') ?></b></td>
                </tr>
                </table>
			</td>
        <td style="padding: 0;">
                <table style="margin-left: -1px;margin-top: -1px;" >
                    <tr>
                    <td colspan="3" style="cursor: pointer;border-top:thin solid black;padding: 0px;padding-top: 5px;width:80%"><b style="padding-left:5px">Total</b></td>
                    <td style="cursor: pointer;text-align:right;padding: 0px;padding-top: 5px;"><b><?=  number_format(abs($grand_total), 2, '.', ',') ?></b></td>
                </tr>
                </table>
			</td>
		</tr>
	</tbody>
</table>
</div>
