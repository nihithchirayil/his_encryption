<?php
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 10 * $padding_number . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";

for ($i = 0; $i < sizeof($ledger_result); $i++) {

    if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $show_sub1 = "style= 'cursor:pointer;font-weight: 600;'";
        $onclick = "onclick=showSingleAmountDetail(this," . $ledger_result[$i]->id . ",'$from_type')";
    } else {
        $show_sub = "style= 'cursor:pointer;border: thin solid #d7d7d7!important' ";
        $show_sub1 = "style= 'cursor:pointer;'";
        if($ledger_result[$i]->ledger_name == 'Advance from Patients'){
         $onclick = "onclick=getAdvanceFromPatients();";   
        }else{
        $onclick = "onclick=getSingleLedgerDetailsDashBoard(this," . $ledger_result[$i]->id . ",'$from_type');";
        }
    }
    
        if($ledger_result[$i]->pl_type == 2){
            $bal_amount = $ledger_result[$i]->cr_amount-$ledger_result[$i]->dr_amount;
        }else{
            $bal_amount = $ledger_result[$i]->dr_amount-$ledger_result[$i]->cr_amount;
        }
        
        $bal_amount = ($bal_amount);
        $border_style = '';
        if($bal_amount != 0){
        ?>
        <tr <?= $show_sub1 ?> class="{{$show_row_group}}" data-parent-ids="{{$show_row_group}}" id="{{$ledger_result[$i]->id}}" >
           <td <?= $onclick ?>  rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" class="{{$show_row_group}}" >{{$ledger_result[$i]->ledger_name}}</td> 
            <td <?= $onclick ?>  rowspan="" style="width:10%;{{$border_style}};border-left: 0px !important" class="td_common_numeric_rules credit_amount"  id="dr_amount_{{$ledger_result[$i]->id}}">{{number_format(abs($bal_amount), 2, '.', ',')}}</td>
            <td <?= $onclick ?>  rowspan="" style="width:10%;{{$border_style}};border-left: 0px !important" class="td_common_numeric_rules credit_amount" id="cr_amount_{{$ledger_result[$i]->id}}"></td>
        </tr>
        <?php }

   
}
?>