<div class="row">
    <div class="col-md-12" id="result_container_div">
    <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
    <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <div class="print_data" style="margin-top: 10px" id = "print_data">

            <table id="result_data_table" class='table table-condensed table_sm'
                   style="font-size: 12px;">
                <thead>

                    @if(sizeof($res)>0)
                    @php
                        $sort_array = array();
                        $i = 1;
                        $sort_change   = $exceeded = $exceeded_amount  = $nearly_exceeded = $nearly_exceeded_amount = 0;
                        $sub_amount_total = $grand_amount_total = $sub_exceeded_amnt = $sub_nearly_exceeded_amnt = 0;
                    @endphp
                    <tr class="headerclass" style="background-color:rgb(125 208 134);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th  style="padding: 2pxtext-align: center;border-right: 1px solid black !important;">No</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;">Vendor</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" colspan="2">Within 30</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" colspan="2">31-45</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" colspan="2">46-60</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" colspan="2">61-90</th>
                      <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;"   colspan="2">Greater than 90</th>
                    </tr>
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th  style="padding: 2px;width:2%;text-align: center;border-right:0px !important; "></th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important; "></th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Not paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Not paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Not paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Not paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Paid</th>
                        <th  style="padding: 2px;text-align: center;border-right:0px !important;" >Not paid</th>

                    </tr>

                    @foreach ($res as $data)
                    <tr style="cursor:pointer;border: thin solid black;" >
                        <td style="width:2%;text-align: center;">
                            {{$i}}
                        </td>
                        <td class="common_td_rules" >{{$data->ledger_name}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->t1, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->ut1, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->t2, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->ut2, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->t3, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->ut3, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->t4, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->ut4, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->t5, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->ut5, 2, '.', ',')}}</td>
                    </tr>
                    @php $i++; @endphp
                @endforeach
                @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                @endif
            </table>
        </div>
    </div>

