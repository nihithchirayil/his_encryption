<?php
$credit_tot = $debits_total = 0.0;
$debit_tot = $debits_total = 0.0;
$head_id = '';
if (count($res) != 0) {
    ?>
    <tr class="{{$show_row_group}}" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        <th width="2%" ></th>
        <th width="27%">Account</th>
        <th width="13%">Voucher Type</th>
        <th width="11%">Voucher No.</th>
        <th width="8%">Invoice No</th>
        <th width="8%">Invoice Date</th>
        <th width="5%">Cheq.No</th>
        <th width="8%">Debit</th>
        <th width="8%">Credit</th>
    </tr>
    <?php
    foreach ($res as $data) {
        if ($data->voucher_type == 'Journal') {
            $v_no = 3;
            if ($head_id != $data->head_id) {
            $head_id = $data->head_id;
            $journalQry = \DB::select("select *,case when cr_dr = 'dr' then amount end as debit_amount"
                    . " ,case when cr_dr = 'cr' then amount end as credit_amount "
                    . "from ledger_booking_entry_detail where head_id = '$head_id' and deleted_at is null and ledger_id = $ledger_id");
            $ilen = 0;
            $len = count($journalQry);
            foreach($journalQry as $jq){
                //if ($ilen != $len - 1) {
            ?>
            <tr class="{{$show_row_group}}">
                <td style="border-left: thin solid #d7d7d7 !important;">
                    <button type="button" onclick="editLedgerHeder('<?= $jq->head_id; ?>', '<?= $v_no ?>')" style="padding: 1px 1px" class="btn btn-prmary">
                        <i class="fa fa-info"></i></button>
                </td>
                <td style="text-align: left">{{$jq->naration}}</td>
                <td style="text-align: left">Journal</td>
                <td style="text-align: left">{{$jq->voucher_no}}</td>
                <td style="text-align: left">{{$jq->invoice_no}}</td> 
                @if($jq->invoice_date == '01-Jan-1970')
                <td style="text-align: left">{{$jq->created_date}}</td>
                @else
                <td style="text-align: left">{{$jq->invoice_date}}</td>
                @endif
                <td style="text-align: left">{{$jq->cheq_no}}</td>
                <td style="text-align: right">{{number_format($jq->debit_amount, 2, '.', ',')}}</td>
                <td style="text-align: right">{{number_format($jq->credit_amount, 2, '.', ',')}}</td>
            </tr>
            <?php
           // }
            $ilen++;
            }
        }
        } else {
            $v_no = 0;
        
        if ($head_id != $data->head_id) {
            $head_id = $data->head_id;
            ?>
            <tr class="{{$show_row_group}}">
                <td style="border-left: thin solid #d7d7d7 !important;"><button type="button" onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')" style="padding: 1px 1px" class="btn btn-prmary">
                        <i class="fa fa-info"></i></button></td>
                @if($data->voucher_type == 'Journal')
                <td style="text-align: left" onclick="narationRow(this, '<?= $head_id ?>')">{{$data->naration}}</td>
                @else
                <td style="text-align: left" onclick="narationRow(this, '<?= $head_id ?>')">{{$data->ledger_name}}</td>
                @endif
                <td style="text-align: left">{{$data->voucher_type}}</td>
                <td style="text-align: left">{{$data->voucher_no}}</td>
                <td style="text-align: left">{{$data->invoice_no}}</td> 
                @if($data->invoice_date == '01-Jan-1970')
                <td style="text-align: left">{{$data->created_date}}</td>
                @else
                <td style="text-align: left">{{$data->invoice_date}}</td>
                @endif
                <td style="text-align: left">{{$data->cheq_no}}</td>
                <td style="text-align: right">{{number_format($data->debit_amount, 2, '.', ',')}}</td>
                <td style="text-align: right">{{number_format($data->credit_amount, 2, '.', ',')}}</td>
            </tr>
            <?php
        }
        }
    }
} else {
    ?>
    <tr>
        <td style="font-family:robotoregular" style="text-align: center;"></td>
    </tr>
<?php } ?>
