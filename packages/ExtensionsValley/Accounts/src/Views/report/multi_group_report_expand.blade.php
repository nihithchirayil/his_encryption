<?php
$expand_all = '';
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 10 * $padding_number . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
$closing_stock_text_qry = \DB::table('ledger_master')->where('ledger_type',5)->select('ledger_name','id')->get();
$closing_stock_text = @$closing_stock_text_qry[0]->ledger_name ? $closing_stock_text_qry[0]->ledger_name:'Closing Stock';
$closing_stock_id = @$closing_stock_text_qry[0]->id ? $closing_stock_text_qry[0]->id:'1';
if((isset($first_td_text) && ($first_td_text =='Current Assets') ) && (isset($from_type) && $from_type == 'balance_amnt')){ ?>

<?php }
if((isset($first_td_text) && ($first_td_text =='Closing Stock' || $first_td_text =='Inventory') )){ ?>
<tr style="padding-left:40px !important" class="{{$show_row_group}}">
    <td style="width:22%;padding-left: 30px !important;">Stock of medicine</td>
    <td  style="padding-left:8px !important;text-align:right">{{@$closing_stock_val[0]->stock_of_medicine??''}}</td>
    <td style="text-align:right"></td>
</tr>
<tr style="padding-left:40px !important" class="{{$show_row_group}}">
    <td style="width:22%;padding-left: 30px !important;">Stock of x-ray</td>
    <td  style="padding-left:8px !important;text-align:right">{{@$closing_stock_val[0]->stock_of_xray??''}}</td>
    <td style="text-align:right"></td>
</tr>
<?php }
if((isset($first_td_text) && ($first_td_text =='Opening Stock') )){ ?>
<tr style="padding-left:40px !important" class="{{$show_row_group}}">
    <td style="width:22%;padding-left: 30px !important;">Stock of medicine</td>
    <td  style="padding-left:8px !important;text-align:right">{{@$open_stock[0]->stock_of_medicine??''}}</td>
    <td style="text-align:right"></td>
</tr>
<tr style="padding-left:40px !important" class="{{$show_row_group}}">
    <td style="width:22%;padding-left: 30px !important;">Stock of x-ray</td>
    <td  style="padding-left:8px !important;text-align:right">{{@$open_stock[0]->stock_of_xray??''}}</td>
    <td style="text-align:right"></td>
</tr>
<?php }
for ($i = 0; $i < sizeof($ledger_result); $i++) {
if($i+1 == sizeof($ledger_result)){
    $final = 1;
}else{
    $final = 0;
}
    if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $show_sub1 = "style= 'cursor:pointer;font-weight: 600;'";
        $onclick = "onclick=showGroupAmountDetailMulti(this," . $ledger_result[$i]->id . ")";
        
    } else {
        $show_sub = "style= 'cursor:pointer;border: thin solid #d7d7d7!important' ";
        $show_sub1 = "style= 'cursor:pointer;'";
        if($ledger_result[$i]->ledger_name == 'Advance from Patients'){
         $onclick = "onclick=getAdvanceFromPatients();";   
        }else{
        $onclick = "onclick=showLedgerAmountDetailMulti(this," . $ledger_result[$i]->id . ");";
        }
    } if($ledger_result[$i]->dr_amount > 0  || $ledger_result[$i]->cr_amount > 0){ ?>
            <tr <?= $show_sub ?>  class="{{$show_row_group}}" data-parent-ids="{{$show_row_group}}" id="{{$ledger_result[$i]->id}}">
                <td <?= $onclick ?> rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" colspan="7" class="common_td_rules" >
                {{$ledger_result[$i]->ledger_name}}
            </td>  
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules "  id="dr_amount_{{$ledger_result[$i]->id}}">
                {{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}
            </td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules "> 
                {{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}} 
            </td>
            </td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules ">
                <?php 
                if(isset($pl_item) && $pl_item == '1'){
                   echo '';
                }else{
                echo number_format($ledger_result[$i]->cls, 2, '.', ',');
                } ?>
            </td>
            </tr>
        <?php
    }
    
}
?>