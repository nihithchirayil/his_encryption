<style>
.excludeItemButton {
    display: none;
}
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <h4 style="text-align: center;margin-top: -10px;" id="heading"><b> </b></h4>
        <div class="print_data" style="margin-top: 10px" id="print_data">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    @if(sizeof($results)>0)
                    @php
                    $credit_total = $debit_total = $cls_total = 0;
                    @endphp

                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="30%" colspan="7">Account</th>
                        <th width="8%">Debit</th>
                        <th width="8%">Credit</th>
                        <th width="8%">Closing Balance</th>
                    </tr>
                    @foreach ($results as $bs_data)
                    @if($bs_data->is_ledger == 0)
                    <?php $fn_name = "showGroupAmountDetailMulti(this," .$bs_data->id . ");"; $font_style = "font-weight:600"; ?>
                    @else
                    <?php $fn_name = "showLedgerAmountDetailMulti(this," .$bs_data->id . ");"; $font_style = "";
                       ?>
                    @endif
                    @if($bs_data->dr_amount != 0 || $bs_data->cr_amount != 0)
                    <tr style="cursor:pointer;border: thin solid #d7d7d7;" id="{{$bs_data->id}}"
                        onclick="<?= $fn_name ?>">
                        <td class="common_td_rules" style="width:22%;padding-right: 15%;{{$font_style}}" colspan="7">
                            {{$bs_data->ledger_name}}</td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules">
                            <b>{{number_format($bs_data->dr_amount, 2, '.', ',')}}</b></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules">
                            <b>{{number_format($bs_data->cr_amount, 2, '.', ',')}}</b></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules">
                        <?php 
                                if(isset($pl_item) && $pl_item == '1'){
                                echo '';
                                }else{ ?>
                            <b> <?php echo number_format($bs_data->cls, 2, '.', ','); ?> </b>
                            <?php  } ?>
                        </td>
                    </tr>
                    @php
                    $credit_total += $bs_data->cr_amount;
                    $debit_total += $bs_data->dr_amount;
                    $cls_total += $bs_data->cls;
                    @endphp
                    @endif
                    @endforeach
                    <?php if((isset($ledger_name) && ($ledger_name == 'Current Assets'))){ 
                        $closing_stock_val = $closing_stock;?>
                    <tr style="cursor:pointer;border: thin solid #d7d7d7!important">
                        <td style="width:22%;text-align:left;" colspan="7"
                            class="common_td_rules"><strong>Opening Stock</strong></td>
                        <td style="padding-left:8px !important;text-align:right"
                            class="td_common_numeric_rules debit_amount">
                            <b>{{number_format($closing_stock, 2, '.', ',')??''}}</b></td>
                        <td style="padding-left:8px !important;text-align:right"
                            class="td_common_numeric_rules credit_amount" ><b>0.00</b></td>
                            <td style="padding-left:8px !important;text-align:right"
                            class="td_common_numeric_rules credit_amount"><b>0.00</b></td>
                    </tr>
                    <?php }else{
                        $closing_stock_val = 0;
                    } ?>
                    <tr style="cursor:pointer;border: thin solid #d7d7d7;">
                        <td class="common_td_rules" style="width:22%;padding-right: 15%" colspan="7"><b> Total </b>
                        </td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules">
                            <b>{{number_format(($debit_total+$closing_stock_val), 2, '.', ',')}}</b></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules">
                            <b>{{number_format($credit_total, 2, '.', ',')}}</b></td>
                        <td style='font-size: 14px;' class="td_common_numeric_rules">
                        <?php 
                if(isset($pl_item) && $pl_item == '1'){
                   echo '';
                }else{ ?>
               <b> <?php echo number_format($cls_total, 2, '.', ','); ?> </b>
              <?php  } ?>
                        </td>
                    </tr>
                    @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                    @endif
            </table>
        </div>
    </div>
</div>