<div class="row">
    <div class="col-md-12" style="padding-left: 100px;
    padding-right: 100px;" id="result_container_div">
                
        <p style="font-size: 12px;" id="total_data">Report Print Date:
            <b>{{ date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i A'))) }} </b>
        </p>
        <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Chart Of Accounts </b></h4>
        <div id="print_data" style="margin-top: 10px">
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;border: 1px solid">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="5%;">Account No</th>
                        <th width="40%;">Account</th>
                        <th width="25%;">Group</th>
                        <th width="20%;">Parent Group</th>
                        <th width="10%;">Level3</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(sizeof($res)>0){
                    foreach ($res as $data){
                    ?>

                    <tr style="font-size: 12px;border: 1px solid;cursor: pointer" id="{{$data->ledger_id}}">
                        <td class="common_td_rules" onclick="editAccount('{{$data->ledger_id}}','{{$data->account_name}}')" >{{ $data->ledger_code }}</td>
                        <td class="common_td_rules" onclick="editAccount('{{$data->ledger_id}}','{{$data->account_name}}')" >{{ $data->account_name }}</td>
                        <td class="common_td_rules" onclick="editAccount('{{$data->ledger_id}}','{{$data->account_name}}')" >{{ $data->ledger_group }}</td>
                        <td class="common_td_rules" onclick="editAccount('{{$data->ledger_id}}','{{$data->account_name}}')" >{{ $data->level2 }}</td>
                        <td class="td_common_numeric_rules" onclick="editAccount('{{$data->ledger_id}}','{{$data->account_name}}')" >{{ $data->level3 }}</td>
                        
                    </tr>
                    <?php
        }

        ?>
                    <?php } else{
        ?>
                    <tr>
                        <td colspan="4" style="text-align: center;">No Results Found!</td>
                    </tr>
                    <?php
    }
    ?></tbody>
        </tbody>

            </table>
        </font>
    </div>
    </div>
</div>
