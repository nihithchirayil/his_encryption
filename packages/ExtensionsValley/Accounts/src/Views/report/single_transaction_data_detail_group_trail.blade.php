<?php
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 10 * $padding_number . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";

if((isset($first_td_text) && (strtolower($first_td_text) =='current assets'))){ ?>
<tr style= "cursor:pointer;border: thin solid #d7d7d7!important" class="{{$show_row_group}}" data-parent-ids="{{$show_row_group}}" id="12333s">
    <td style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" colspan="7" class="common_td_rules">Opening Stock</td>
    <td  style="padding-left:8px !important;text-align:right" class="td_common_numeric_rules debit_amount" id="dr_amount_12333s">{{number_format($closing_stock, 2, '.', ',')??''}}</td>
    <td  style="padding-left:8px !important;text-align:right" class="td_common_numeric_rules credit_amount" id="cr_amount_12333s">0.00</td>
    <td class="common_td_rules excludeItemButton" style="<?= $border_style; ?>; width:1%; "><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this);" ><i class="fa fa-minus"></i></button></td>
</tr>
<?php }
for ($i = 0; $i < sizeof($ledger_result); $i++) {

    if ($ledger_result[$i]->custom_flag == 11) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $show_sub1 = "style= 'cursor:pointer;font-weight: 600;'";
        $ledg_code = $ledger_result[$i]->ledger_code;
        $onclick = "onclick=getSingleTransactionReportDataDetailTrailBySpeciality(this," . $ledger_result[$i]->id . ",'$from_type','$ledg_code')";
        $expand_class = 'expand_all_trail';

    }else if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $onclick = "onclick=showSingleAmountDetailTrail(this," . $ledger_result[$i]->id . ",'$from_type')";
        $expand_class = 'expand_all_trail';
    } else {
        $show_sub = "style= 'cursor:pointer;border: thin solid #d7d7d7!important' ";
        if($ledger_result[$i]->custom_flag == 12){
         $onclick = "onclick=getAdvanceFromPatients();";
        }else{
        $onclick = "onclick=getSingleLedgerDetailsTrail(this," . $ledger_result[$i]->id . ",'$from_type');";
        }
        $expand_class = '';
    }

        if ($ledger_result[$i]->dr_amount > 0 || $ledger_result[$i]->cr_amount > 0) {
            if(isset($ledger_result[$i]->speciality)  && $ledger_result[$i]->speciality != ''){
                $onclick = "onclick=showSpecialityTrailAmount(this," . $ledger_result[$i]->speciality_id . ",'$from_type')"; ?>
             <tr style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'  class="{{$show_row_group}} {{$expand_class}}" data-parent-ids="{{$show_row_group}} {{ $ledger_result[$i]->speciality_id }}" id="{{$ledger_result[$i]->id}}">
                <td <?= $onclick ?> rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" colspan="7" class="common_td_rules" >{{$ledger_result[$i]->speciality}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules debit_amount"  id="dr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules credit_amount" id="cr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}}</td>
                <td class="common_td_rules excludeItemButton" style="<?= $border_style; ?>; width:1%; "><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this,'{{$ledger_result[$i]->id}}');" ><i class="fa fa-minus"></i></button></td>
            </tr>
           <?php }else{
            ?>
            <tr <?= $show_sub ?>  class="{{$show_row_group}} {{$expand_class}}" data-parent-ids="{{$show_row_group}}" id="{{$ledger_result[$i]->id}}">
                <td <?= $onclick ?> rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" colspan="7" class="common_td_rules" >{{$ledger_result[$i]->ledger_name}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules debit_amount"  id="dr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules credit_amount" id="cr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}}</td>
                <td class="common_td_rules excludeItemButton" style="<?= $border_style; ?>; width:1%; "><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this,'{{$ledger_result[$i]->id}}');" ><i class="fa fa-minus"></i></button></td>
            </tr>
        <?php
            }
    }
}
?>
