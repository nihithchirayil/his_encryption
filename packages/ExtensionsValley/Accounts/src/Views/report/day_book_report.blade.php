@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 5px;
    max-width: 200px;
}
</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_header" value="{{$hospital_header}}">
<input type="hidden" id="hospital_address" value="{{$hospital_address}}">
<?php  $company = \DB::table('company')->pluck('sub_code'); ?>
<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
<div class="right_col" >
    <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                            {!! Form::token() !!}
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Account</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" name="ledger_name" id="ledger_name" value="" autocomplete="false">
                                    <input type="hidden" class="form-control" name="ledger_name_hidden" id="ledger_name_hidden" value="">
                                    <div id="AjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important"></div>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control datepicker" name="from_date" id="from_date" value="<?= date('Mon-dd-Y')?>">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control datepicker" name="to_date" id="to_date" value="<?= date('Mon-dd-Y') ?>">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Narration</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="narration_search" id="narration_search" value="">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Amount</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="amount_search" id="amount_search" value="">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">amnt.Crit.</label>
                                    <div class="clearfix"></div>
                                    <select name="amount_crt" id="amount_crt" class="form-control">
                                        <option value="1">Equal</option>
                                        <option value="2">Greater or equal to</option>
                                        <option value="3">Less or equal to</option>
                                        <option value="4">Greater</option>
                                        <option value="5">Less</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: 15px;">
                                        <div class="checkbox checkbox-success inline no-margin">
                                            <input type="checkbox" class="form-control filters" name="show_all_narration"  id="show_all_narration">
                                            <label class="text-blue " for="show_all_narration">All Narration</label>
                                        </div>
                             </div>
<!--                             <div class="col-md-2 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchList(20,0)"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-2 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div> -->


                            <div class="col-md-12"
                                         style="text-align:right; padding: 0px; margin-top: 10px;">
                                         <a href="{{Request::url()}}" style="float:right !important" class="btn btn-light btn-warning"><i class="fa fa-times"></i> Clear</a>

                                        <a class="btn light_purple_bg" onclick="exceller_template_day_book('day_book',7);"
                                           name="csv_results" id="csv_results" style="float:right !important">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            Excel
                                        </a>

                                        <a class="btn light_purple_bg" onclick="printReportData();"
                                           name="print_results" id="print_results" style="float:right !important">
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                            Print
                                        </a>

                                        <a id="searchdatabtn" type="button" style="float:right !important" class="btn btn-light light_purple_bg" onclick="searchList(20,0)">
                                            <i id="searchdataspin" class="fa fa-search" ></i>
                                            Search
                                        </a>

                                    </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="common_list_div">

            </div>
        </div>

    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate_day_book('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>
<script type="text/javascript">
    var limit = 100;
    var offset = 0;
    var total_rec = 0;
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    searchList(15,0);
            var table_company_code = $("#table_company_code").val();
    if(table_company_code == 'DAYAH'){
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate : '04-01-2022',
    });
}else{
       $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    }); 
}



    });


    function searchList(limit = 100,offset =0){

        var url='<?=route('extensionsvalley.accounts.dayBookReportData')?>';
        var ledger_id_head = $("#ledger_name_hidden").val();
        var from_date_invoice = $("#from_date").val();
        var  to_date_invoice = $("#to_date").val();
        var narration_search = $("#narration_search").val();
        var amount_search = $("#amount_search").val();
        var amount_crt = $("#amount_crt").val();
        var param1 = {narration_search:narration_search,amount_search:amount_search,amount_crt:amount_crt,
            ledger_id:ledger_id_head,bill_date_from:from_date_invoice,
            bill_date_to:to_date_invoice,limit:limit,offset:offset};
        $.ajax({
        type: "GET",
            url: url,
            data:param1,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled',true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
                $.LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            },
            success: function (msg) {
                var obj = JSON.parse(msg);
                 msg = obj.viewData;
                total_rec = obj.total_rec;
                if(offset == 0){
                    $('#common_list_div').html(msg);
                }else{
                    $('#receipt_table1').append(msg);

                }
                if($("#show_all_narration").prop("checked")){
                    $('.checkAllNarration').attr('data-narations',1);
                    $(".append_narration").css('display','');
                } else {
                    $('.checkAllNarration').attr('data-narations',2);
                    $(".append_narration").css('display','none');
                }
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled',false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $.LoadingOverlay("hide");


                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');


            },error: function(){
                toastr.error("Please Check Internet Connection");
            }
        });
   }

//    $("#load_data").scroll(function(){
//         alert('ddd');
//         if($(window).scrollTop() + $(window).height() > $("#load_data").height())
//         {
//             action = 'active';
//             start = start + limit;
//             setTimeout(function(){
//                 load_country_data(limit, start);
//             }, 1000);
//         }
//     });





   $('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#ledger_name_hidden').val() != "") {
            $('#ledger_name_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var datastring = '';



        if (search_key == "") {
            $("#AjaxDiv").html("");
        } else {
            var url='<?=route('extensionsvalley.accounts.searchLedgerMaster')?>';
            $.ajax({
                type: "GET",
                url: url,
                data: 'ledger_desc=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#AjaxDiv").html("No results found!").show();

                        $("#AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#AjaxDiv").html(html).show();
                        $("#AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down('AjaxDiv', event);
    }
});

function fillLedgerValues(e,name, id) {
    $('#ledger_name_hidden').val(id);
    $('#ledger_name').val(name);
     $("#AjaxDiv").hide();
}
function editLedgerHeder(booking_id,v_type){
    if(v_type=='Journal' || v_type == 3 ){
        var v_no = 3;
    }else{
        var v_no = 0;
    }
    var url = $("#base_url").val();
    var urls = url + "/accounts/editLedgerBooking/"+booking_id+"/"+v_no
    window.open(urls, '_blank');
}
function narationRow(e,head_id){
    var url = $("#base_url").val();
    var is_naration =  $(e).closest('tr').attr('data-narations');
    if(is_naration==1){
         if($(e).closest('tr').next().attr('data-narration')==head_id){
             $(e).closest('tr').next('tr').remove();
             return false;
         }
    }
     $.ajax({
                type: "GET",
                url: url + "/accounts/show_naration_row/",
                data: {head_id:head_id,from_report:'day_book'},
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $(e).closest('tr').after('');
                    $(e).closest('tr').attr('data-narations',1);
                  $(e).closest('tr').after(data);
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $.LoadingOverlay("hide");
                }
            });
    }
    $(document).on("click", "#show_all_narration", function(){
    if($(this).prop("checked")){
        $('.checkAllNarration').attr('data-narations',1);
        $(".append_narration").css('display','');
    } else {
        $('.checkAllNarration').attr('data-narations',2);
        $(".append_narration").css('display','none');
    }
    
});
function excelleExp(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table');

    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}

</script>

@endsection
