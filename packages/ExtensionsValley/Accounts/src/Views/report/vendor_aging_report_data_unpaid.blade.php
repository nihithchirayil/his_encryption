<div class="row">
    <div class="col-md-12" id="result_container_div">
    <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
    <?php
            $collect = collect($res);
            $total_records = count($collect);
            $count = @$count ? $count : 0;
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $count }}</b></p>
        <div class="print_data" style="margin-top: 10px" id = "print_data">
            <div class="wrapper theadscroll" style="position: relative; height: 400px;">
                <table class="table table-condensed table_sm table-col-bordered theadfix_wrapper" id="result_data_table">
                <thead>

                    @if(sizeof($res)>0)
                    @php
                        $sort_array = array();
                        $i = 1;
                        $sort_change   = $exceeded = $exceeded_amount  = $nearly_exceeded = $nearly_exceeded_amount = 0;
                        $sub_amount_total = $grand_amount_total = $sub_exceeded_amnt = $sub_nearly_exceeded_amnt = 0;
                    @endphp
                    <tr class="table_header_bg" style="background-color:rgb(125 208 134);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th  style="padding: 2pxtext-align: center;border-right: 1px solid black !important;">No</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;">Vendor</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" >Less than 90</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" >90 days to 1 year</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" >1 to 3 year</th>
                        <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;" >More than 3 years</th>
                      <th  style="padding: 2px;text-align: center;border-right: 1px solid black !important;"   >Total</th>
                    </tr>
                </thead>

                    @foreach ($res as $data)
                    @php
                        $total_for_supplier = 0;
                        $total_for_supplier = $total_for_supplier+$data->less_than_90_days+$data->between_90_days_and_1_year+$data->between_1_year_and_3_years+$data->greater_than_3_years
                    @endphp
                    @if($total_for_supplier != 0)
                    <tr style="cursor:pointer;border: thin solid black;" >
                        <td style="width:2%;text-align: center;">
                            {{$i}}
                        </td>
                        <td class="common_td_rules" >{{$data->ledger_name}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->less_than_90_days, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->between_90_days_and_1_year, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->between_1_year_and_3_years, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($data->greater_than_3_years, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" >{{number_format($total_for_supplier, 2, '.', ',')}}</td>
                    </tr>
                    @php $i++; @endphp
                    @endif
                @endforeach
                @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                @endif
            </table>
            </div>
        </div>
    </div>

