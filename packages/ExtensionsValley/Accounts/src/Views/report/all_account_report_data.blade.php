<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <?php
            $collect = collect($ledger_result);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -10px;" id="heading"><b> </b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="11">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="30%" colspan="7">Account</th>
                        <th width="10%">Debit</th>
                        <th width="10%">Credit</th>
                    </tr>

<?php
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 10 * $padding_number . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
        
for ($i = 0; $i < sizeof($ledger_result); $i++) {

    if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $onclick = "onclick=showSingleAmountDetailTrail(this," . $ledger_result[$i]->id . ",'$from_type')";
    } else {
        $show_sub = "style= 'cursor:pointer;border: thin solid #d7d7d7!important' ";
        if($ledger_result[$i]->ledger_name == 'Advance from Patients'){
         $onclick = "onclick=getAdvanceFromPatients();";   
        }else{
        $onclick = "onclick=getSingleLedgerDetailsTrail(this," . $ledger_result[$i]->id . ",'$from_type');";
        }
    }
    
        if ($ledger_result[$i]->dr_amount > 0 || $ledger_result[$i]->cr_amount > 0) {
            ?>
            <tr <?= $show_sub ?>  class="{{$show_row_group}}" data-parent-ids="{{$show_row_group}}" id="{{$ledger_result[$i]->id}}">
                <td <?= $onclick ?> rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" colspan="7" class="common_td_rules" >{{$ledger_result[$i]->ledger_name}}</td>  
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules debit_amount"  id="dr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules credit_amount" id="cr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}}</td>
                <td class="common_td_rules excludeItemButton" style="<?= $border_style; ?>; width:1%; "><button type="button" class="btn btn-sm padding_sm" onclick="excludeItem(this);" ><i class="fa fa-minus"></i></button></td>
            </tr>
        <?php
        
    }
}
?>
            </table>
        </div>
    </div>

