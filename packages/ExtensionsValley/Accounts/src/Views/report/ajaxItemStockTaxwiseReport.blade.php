<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Item Stock</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="10">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="3%">SL.NO.</th>
                        <th width="30%">Item Description</th>
                        <th width="8%">Quantity</th>
                        <th width="10%">Stock Value</th>
                    </tr>
                </thead>
                <?php
                $i = 0;
                $unit_tax_rate = '';
                $slno = 1;
                $nettot_stock = 0.0;
                $net_unit_purchase_cost = 0.0;
                $nettot_cal_stock = 0.0;
                $nettot_purchase_rate = 0.0;
                $nettot_unit_selling_price = 0.0;
                $nettot_total_selling_price = 0.0;
                $nettot_unit_cost_without_tax = 0.0;
                $nettot_total_cost_without_tax = 0.0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
         foreach ($res as $data){ ?>

                        <tr>
                            <td class="common_td_rules">{{ $slno }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->stock ? $data->stock : 0 }}</td>
                            <td class="td_common_numeric_rules">
                                <?= $data->unit_purchase_cost * $data->stock ?></td>
                        </tr>
                        <?php
                            $nettot_stock+=floatval($data->stock);
                            $nettot_cal_stock+=floatval($data->unit_purchase_cost * $data->stock);

                            $slno++;

                 } ?>
                        <tr class="headerclass"
                            style="background-color:rgb(135,206,250);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="2" class="common_td_rules" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $nettot_stock ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_cal_stock ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
            </font>
        </div>
    </div>
</div>
