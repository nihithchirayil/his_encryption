<style>
.excludeItemButton {
    display: none;
}
</style>
<?php $company_code = \DB::table('company')->get(); ?>

<p style="font-size: 12px;" id="total_data"><b><?= $ledger_name ?></b></p>
<p style="font-size: 12px;" id="total_data">Report Print Date:
    <b>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </b></p>
<p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>

<div class="row" id="print_data">
    <div class="col-md-12" id="result_data_table">
        <div class="wrapper theadscroll" style="position: relative; height: 400px;">
            <table
                class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="{{$show_row_group}} headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="2%"></th>
                        <th width="28%" class="clr_class">Account</th>
                        <th width="12%" class="clr_class">Voucher Type</th>
                        <th width="11%" class="clr_class">Voucher No.</th>
                        <th width="8%" class="clr_class">Invoice No</th>
                        <th width="8%" class="clr_class">Date</th>
                        <th width="5%" class="clr_class">Cheq.No</th>
                        <th width="8%" class="clr_class">Debit</th>
                        <th width="8%" class="clr_class">Credit</th>
                </thead>
                <?php
        $credit_tot = $debits_total = 0.0;
        $debit_tot = $debits_total = 0.0;
        $head_id = '';
        $old_openings = isset($current_opening) ? $current_opening : 0;
                if ($old_openings < 0) {
                    $is_debit_old = 1;
                    $is_debit_current = 1;
                } else {
                    $is_debit_old = 0;
                    $is_debit_current = 0;
                } 
        if (count($res) != 0) {
            ?>
                <tbody>
                    <?php
                foreach ($res as $data) {
                    $v_no = 0;
                    if ($data->voucher_type == 'Journal') {
                        $v_no = 3;
                    }
                    if ($head_id != $data->head_id) {
                        $head_id = $data->head_id;
                        if ($data->debit_amount > 0 || $data->credit_amount > 0) {
                            ?>
                    <tr style="border-left: 1px solid #CCC !important;cursor: pointer"
                        class="{{$show_row_group}} checkAllNarration">
                        <td style="border-left: thin solid black !important;">
                            <input type="checkbox" onclick="color_row(this)" class="hid_cls">
                        </td>
                        <td style="text-align: left" onclick="narationRow(this, '<?= $head_id ?>')">
                            {{$data->ledger_name}}</td>
                        <td style="text-align: left"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')">{{$data->voucher_type}}
                        </td>
                        <td style="text-align: left"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')">{{$data->voucher_no}}
                        </td>
                        <td style="text-align: left"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')">{{$data->invoice_no}}
                        </td>
                        <td style="text-align: left"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')">{{$data->invoice_date}}
                        </td>
                        <td style="text-align: left"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')">{{$data->cheq_no}}</td>
                        <td style="text-align: right"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')"><span
                                class="debit_amount">{{number_format($data->debit_amount, 2, '.', ',')}}</span></td>
                        <td style="text-align: right"
                            onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $v_no ?>')"><span
                                class="credit_amount">{{number_format($data->credit_amount, 2, '.', ',')}}</span></td>
                        <td class="common_td_rules excludeItemButton" style=" width:1%;"><button type="button"
                                class="btn btn-sm padding_sm hid_cls"
                                onclick="excludeItem(this, '<?= $data->voucher_no ?>');"><i
                                    class="fa fa-minus"></i></button></td>
                    </tr>
                    <tr style='background-color:#cde4d6;display: none' data-narration="<?= $data->head_id; ?>"
                        class="append_narration">
                        <td></td>
                        <td style='text-align:left' title="<?= $data->naration ?>">
                            <?php  $break = 35; ?>
                            <b><?= implode(PHP_EOL, str_split($data->naration, $break)); ?></b>
                        </td>
                        <td colspan=7></td>
                    </tr>
                    <?php
                        }
                    }
                }
                $credit_tot = floatval($res[0]->grp_credit);
                $debit_tot = floatval($res[0]->grp_debit);
                if ($debit_tot > $credit_tot) {
                    $debits_total = $debit_tot - $credit_tot;
                    $is_debit_current = 1;
                } else {
                    $debits_total = $credit_tot - $debit_tot;
                    $is_debit_current = 0;
                }
                
                
                } else {
                ?>
                    <tr>
                        <td style="font-family:robotoregular" style="text-align: center;"></td>
                    </tr>
                    <?php } ?>
                    <tr class=""
                        style="height: 30px;border-top:1px solid black;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Opening Balance</td>
                        @if($old_openings > 0)
                        <td style="text-align: right"></td>
                        <td><b><?= abs($old_openings) ?></b></td>
                        @else
                        <td><b><?= abs($old_openings) ?></b></td>
                        <td style="text-align: right"></td>
                        @endif
                    </tr>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Current Balance</td>
                        <td style="text-align: right"><b
                                class="total_debit_amount"><?= number_format((float) abs($debit_tot), 2, '.', ','); ?></b>
                        </td>
                        <td style="text-align: right"><b
                                class="total_credit_amount"><?= number_format((float) abs($credit_tot), 2, '.', ','); ?></b>
                        </td>
                    </tr>
                    <?php if ($is_debit_current == 1 && $is_debit_old == 1) { ?>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Closing Balance</td>
                        <td style="text-align: right"><b
                                class="total_debit_amount"><?= number_format((float) abs(abs($old_openings) + $debits_total), 2, '.', ','); ?></b>
                        </td>
                        <td style="text-align: right"></td>
                    </tr>
                    <?php } else if ($is_debit_current == 0 && $is_debit_old == 0) { ?>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Closing Balance</td>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"><b
                                class="total_credit_amount"><?= number_format((float) abs(abs($old_openings) + $debits_total), 2, '.', ','); ?></b>
                        </td>
                    </tr>
                    <?php
                }else if ($is_debit_current == 0 && $is_debit_old == 1) { ?>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Closing Balance</td>
                        <?php if(abs($old_openings) < abs($debits_total)){ ?>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"><b
                                class="total_credit_amount"><?= number_format((float) abs(abs($old_openings) - abs($debits_total)), 2, '.', ','); ?></b>
                        </td>
                        <?php }else{ ?>
                        <td style="text-align: right"><b
                                class="total_debit_amount"><?= number_format((float) abs(abs($old_openings) - abs($debits_total)), 2, '.', ','); ?></b>
                        </td>
                        </td>
                        <td style="text-align: right"></td>
                        <?php } ?>
                    </tr>
                    <?php
                }else if ($is_debit_current == 1 && $is_debit_old == 0) { ?>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Closing Balance</td>
                        <?php if(abs($old_openings) < abs($debits_total)){ ?>
                        <td style="text-align: right"><b
                                class="total_debit_amount"><?= number_format((float) abs(abs($old_openings) - abs($debits_total)), 2, '.', ','); ?></b>
                        </td>
                        </td>
                        <td style="text-align: right"></td>
                        <?php }else{ ?>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"><b
                                class="total_credit_amount"><?= number_format((float) abs(abs($old_openings) - abs($debits_total)), 2, '.', ','); ?></b>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php
                } else {
                    if ($debits_total > $old_openings ) {
                        ?>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Closing Balance</td>
                        <td style="text-align: right"><b class="total_debit_amount"><?php
                                    if ($old_openings > 0) {
                                        echo number_format((float) abs($old_openings - $debits_total), 2, '.', ',');
                                    } else {
                                        echo number_format((float) abs($old_openings + $debits_total), 2, '.', ',');
                                    }
                                    ?></b></td>
                        <td style="text-align: right"></td>
                    </tr>
                    <?php } else { ?>
                    <tr class=""
                        style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                        <td width="10%" colspan="7">Closing Balance</td>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"><b class="total_credit_amount"><?php
                                    if ($old_openings > 0) {
                                        echo number_format((float) abs($old_openings - $debits_total), 2, '.', ',');
                                    } else {
                                        echo number_format((float) abs($old_openings + $debits_total), 2, '.', ',');
                                    }
                                    ?></b></td>
                    </tr>
                    <?php
                    }
                } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="exp_hide_div" style="display:none"></div>