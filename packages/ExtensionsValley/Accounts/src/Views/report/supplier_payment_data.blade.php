<div class="row">
    @if(isset($report_type) && $report_type == 1)
    <div class="col-md-12" id="result_container_div">

            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Supplier Payment Detail Report</b></h4>
            <div class="print_data" style="margin-top: 10px" id="print_data">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                   <?php $company_code = \DB::table('company')->get(); ?>
                <thead>


                    @if(sizeof($res)>0)
                    @php
                    $sort_array = array();
                    $i = 1;
                    $sort_change   = $exceeded = $exceeded_amount  = $nearly_exceeded = $nearly_exceeded_amount = 0;
                    $sub_amount_total = $grand_amount_total = $sub_exceeded_amnt = $sub_nearly_exceeded_amnt = 0;
                    @endphp
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th  style="padding: 2px;width:2%;text-align: center;border:2px; "></th>
                        <th  style="padding: 2px;width:15%;text-align: center;border:2px; ">Invoice No</th>
                        <th  style="padding: 2px;width:15%;text-align: center;">Invoice Date</th>
                        <th  style="padding: 2px;width:15%;text-align: center;">Purchase Category</th>
                        <th  style="padding: 2px;width:5%;text-align: center;">Amount</th>
                    </tr>

                    @foreach ($res as $data)
                    <?php
                    $is_exceed = 0;
                    $is_nearly_exceed = 0;
                    $date1 = date_create($credit_date);
                    $date2 = date_create(date('Y-m-d', strtotime($data->invoice_date)));
                    $diff = date_diff($date1, $date2);
                    $credit_diff_days = $diff->format("%a");
                    $color = '';
                    if ($credit_diff_days > $data->credit_days && $data->credit_days > 0) {
                        $color = "red";
                        $exceeded++;
                        $exceeded_amount += $data->amount;
                        $is_exceed = 1;
                    } else if ($data->credit_days - $credit_diff_days <= 7 && $data->credit_days > 0) {
                        $is_nearly_exceed = 1;
                        $color = "darkviolet";
                        $nearly_exceeded++;
                        $nearly_exceeded_amount += $data->amount;
                    }
                    ?>
                    @if(!in_array($data->master_id,$sort_array))
                    @php
                    $sort_array[] = $data->master_id;
                    $sort_order = true;
                    $sort_change = 1;
                    @endphp
                    @endif

                    @if($sort_order && $sort_order==1 )
                    @if($sort_change == 1)
                    @php $sort_change = 0;@endphp
                    @if($i != 1)
                    <tr style="background-color: #d7d7d7">
                        <td colspan='3'>
                            @if(isset($paid_status) && $paid_status ==1 )
                            <span style="float: right"><strong>Exceeded: <span style="color:red;padding-right: 10px">{{$sub_exceeded_amnt}}</span>
                                Nearly Exceed: <span style="color:darkviolet">{{$sub_nearly_exceeded_amnt}}</span></strong></span>
                            @endif
                        </td>
                        <td style='font-size: 14px;'><strong>Sub Total</strong></td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{$sub_amount_total}} </td>
                    </tr>
                    @endif
                    @php $sub_amount_total = $sub_exceeded_amnt = $sub_exceeded_amnt=$sub_nearly_exceeded_amnt=0; @endphp
                    @endif
                    <tr>
                        <td colspan="5" style="font-size: 12px;background-color: #ddf5e6;text-align:left">
                            <strong>
                                {{strtoupper($data->ledger_name)}} &nbsp;( Max.Credit Days: &nbsp;{{($data->credit_days)}})
                            </strong>
                        </td>
                    </tr>
                    @php
                    $sort_order = false;
                    @endphp
                    @endif

                    <tr style="color:{{$color}}">
                        <td style="border-left: 1px solid #CCC !important;">
                            <button type="button" onclick="editLedgerHeder('<?= $data->head_id; ?>', '<?= $data->voucher_type ?>')" style="padding: 1px 1px" class="btn btn-prmary">
                                    <i class="fa fa-info"></i>
                            </button>
                        </td>
                        <td style="padding:5px;width:10%;" class="common_td_rules">{{$data->invoice_no}}</td>
                        <td style="padding:5px;width:10%;" class="common_td_rules">{{$data->invoice_date}}</td>
                        <td style="padding:5px;width:10%;" class="common_td_rules">{{$data->purchase_category}}</td>
                        <td style="padding:5px;width:10%;" class="td_common_numeric_rules" >
                            {{number_format($data->amount, 2, '.', ',')}}
                            @if($data->settled_status==1) (Partialy Paid) @endif
                        </td>
                    </tr>
                    <?php
                    if ($is_exceed == 1) {
                        $sub_exceeded_amnt += $data->amount;
                    }
                    if ($is_nearly_exceed == 1) {
                        $sub_nearly_exceeded_amnt += $data->amount;
                    }
                    $grand_amount_total += $data->amount;
                    $sub_amount_total += $data->amount;
                    $i++;
                    ?>
                    @endforeach
                    <tr style="background-color: #d7d7d7">
                        <td colspan='3'>
                            @if(isset($paid_status) && $paid_status ==1 )
                            <span style="float: right"><strong>Exceeded: <span style="color:red;padding-right: 10px">{{number_format($sub_exceeded_amnt, 2, '.', ',')}}</span>
                                Nearly Exceed: <span style="color:darkviolet">{{number_format($sub_nearly_exceeded_amnt, 2, '.', ',')}}</span></strong></span>
                            @endif
                        </td>
                                <td style='font-size: 14px;'><strong>Sub Total</strong></td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{number_format($sub_amount_total, 2, '.', ',')}} </td>
                    </tr>
                    <tr style="background-color: #dedc91">
                        <td colspan='4' style='font-size: 14px;'><strong>Grand Total</strong></td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{number_format($grand_amount_total, 2, '.', ',')}} </td>
                    </tr>
                    @if(isset($paid_status) && $paid_status ==1 )
                    <tr style="font-family:robotoregular;color: red">
                        <td class="common_td_rules"><b>Exceeded Bill Count</b></td>
                        <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$exceeded}}</b></td>
                        <td class="common_td_rules"><b>Exceeded Bill Amount</b></td>
                        <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{number_format($exceeded_amount, 2, '.', ',')}}</b></td>
                    </tr>
                    <tr style="font-family:robotoregular;color: darkviolet">
                        <td class="common_td_rules"><b>Nearly Exceeded Bill Count</b></td>
                        <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$nearly_exceeded}}</b></td>
                        <td class="common_td_rules"><b>Nearly Exceeded Bill Amount</b></td>
                        <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{number_format($nearly_exceeded_amount, 2, '.', ',')}}</b></td>
                    </tr>
                    @endif
                    @else
                <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                @endif
            </table>
        </div>
    </div>
    @else
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px" id="print_data">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Supplier Payment Summary Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>

                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th onclick="sortTable('result_data_table', 4, 1);" style="padding: 2px;width:15%;text-align: center;border:2px; ">Supplier</th>
                        <th onclick="sortTable('result_data_table', 3, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Amount</th>
                    </tr>


                </thead>
                <tbody>
                    @php $total_summary_amount = 0;@endphp
                    <?php
                    if (count($res) != 0) {
                        $i = 1;
                        foreach ($res as $list) {
                            ?>
                            <tr>
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->ledger_name }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{number_format($list->amount, 2, '.', ',')}}</td>
                                @php $total_summary_amount += $list->amount;@endphp
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                        <tr style="font-family:robotoregular">
                            <td colspan="1"><b>Total</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{number_format($total_summary_amount, 2, '.', ',')}}</b></td>
                        </tr>

                        <?php
                    } else {
                        ?>
                        <tr>
                            <td colspan="5" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    @endif
