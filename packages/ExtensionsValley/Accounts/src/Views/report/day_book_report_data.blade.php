@if($offset == 0)
<div class="row">
    <div id="result_container_div">

        <div class="col-md-12 ">
            <?php $company_code = \DB::table('company')->get(); ?>
            <div id="print_data" style="margin-top: 10px">
                <table class="table table_round_border theadfix_wrapper" id="result_data_table">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="text-align:center;width:10%" id="date_header">Date</th>
                            <th style="text-align:center;width:30%"class="print_cl">Particulars</th>
                            <th style="text-align:center;width:10%" class="print_cl1">Voucher Type</th>
                            <th style="text-align:center;width:10%" class="print_cl1">Voucher No</th>
                            <th style="text-align:center;width:15%" class="print_cl2">Debit</th>
                            <th style="text-align:center;width:15%" class="print_cl2">Credit</th>
                            <th style="text-align:center;width:10%" class="hid_cls">Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($receivedData))
                        @foreach($receivedData as $rec)
                        <?php 
                            $ledge_data_details = \DB::table('ledger_booking_entry_detail as pl')
                        ->select('pl.*',
                        \DB::raw("lm.ledger_name||' -- '||lm1.ledger_name as ledger_name"),
                        \DB::raw("(select sum(case when cr_dr = 'cr' then amount else 0 end)-sum(case when cr_dr = 'dr' then amount else 0 end)+lm.opening_balence
                    from ledger_booking_entry_detail where ledger_id=lm.id and deleted_at is null) as current_balance "))
                        ->join('ledger_master as lm', 'lm.id', '=', 'pl.ledger_id')
                        ->join('ledger_master as lm1', 'lm1.id', '=', 'lm.ledger_group_id')
                        ->where('head_id', $rec->head_id)->where('pl.is_header_part', 0)->whereNull('pl.deleted_at')->orderBy('pl.id', 'asc')->get();
                            $ledge_data = '';
                            $ledge_data_strg = '';
                            $ledge_arr = [];
                            if (!empty($ledge_data_details)) {
                                foreach ($ledge_data_details as $key1 => $value1) {
                                    $ledge_data = isset($value1->ledger_name) ? $value1->ledger_name:'';
                                    array_push($ledge_arr, $ledge_data);
                                }
                                $ledge_data_strg = implode(",", $ledge_arr);
                            }
                        ?>
                        <tr onclick="narationRow(this,'{{$rec->head_id}}')" style="cursor: pointer" data-narations="1">
                            <td class="common_td_rules" style="max-width:200px; word-wrap:break-word;">{{ !empty($rec->invoice_date) ?$rec->invoice_date :'' }}</td>
                            <td class="common_td_rules" style="max-width:500px; word-wrap:break-word;" data-toggle = 'popover' data-trigger="hover" data-container = 'body'  data-content="<p>{{$ledge_data_strg}}</p>">{{ $rec->ledger_name }}</td>
                            
                            <td class="common_td_rules" style="max-width:200px; word-wrap:break-word;">{{ !empty($rec->voucher_type) ? $rec->voucher_type:'' }}</td>
                            <td class="common_td_rules" style="max-width:200px; word-wrap:break-word;">{{ !empty($rec->voucher_no) ? $rec->voucher_no:'' }}</td>
                            <td class="td_common_numeric_rules" style="max-width:200px; word-wrap:break-word;">
                                {{ !empty($rec->dr) ? number_format($rec->dr, 2, '.', ',') :'' }}</td>
                            <td class="td_common_numeric_rules" style="max-width:200px; word-wrap:break-word;">
                                {{ !empty($rec->cr) ? number_format($rec->cr, 2, '.', ',') :'' }}</td>
                            <td class="text-center  common_td_rules hid_cls" style="max-width:200px; word-wrap:break-word;text-align: center !important;">
                                <button class='btn btn-sm light_purple_bg hid_cls' type="button"
                                    onclick="editLedgerHeder('{{$rec->head_id}}','{{$rec->voucher_type_id}}')"><i
                                        class="fa fa-edit"></i></button>
                            </td>
                        </tr>
                        <tr style='background-color:#cde4d6;display: none;' data-narration="<?= $rec->head_id; ?>"
                            class="append_narration">
                            <td></td>
                            <td style='text-align:left'>
                            <span  style="max-width:200px; word-wrap:break-word;"><?= !empty($rec->naration) ? $rec->naration : 'Narration is empty'; ?></span></td>
                            <td colspan=5></td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5">No Records Found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endif
<div id ="exp_hide_div" style= "display:none"></div>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({ html: true, sanitize: false,trigger: "hover",placement : 'top'});
    });
</script>