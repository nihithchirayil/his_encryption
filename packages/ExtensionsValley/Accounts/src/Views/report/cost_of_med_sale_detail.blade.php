@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_header" value="{{$hospital_header}}">
<div class="right_col">
    <div class="container-fluid" style="width: 80%">
        <!-------print modal---------------->
        <div class="col-md-12 padding_sm">
            <div id="ResultDataContainer" style="max-height: 650px; padding: 10px;font-family:poppinsregular">
                <div style="background:#686666;">
                    <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                          width: 100%;
                          padding: 30px;" id="ResultsViewArea">
                        <div class="row">
                            <div class="col-md-12" style="padding-left: 100px;
                                 padding-right: 100px;" id="result_container_div">
                                <p style="font-size: 12px;" id="total_data">Report Print Date:
                                    <b>{{ date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i A'))) }} </b>
                                </p>
                                <?php
                                $collect = collect($res);
                                $total_records = count($collect);
                                ?>
                                <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
                                <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Item Wise Cost </b></h4>
                                <font size="16px" face="verdana">
                                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                                       style="font-size: 12px;border: 1px solid">
                                    <thead>
                                        <tr class="headerclass"
                                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                            <th width="30%;">Product</th>
                                            <th width="20%;">Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $total_net_sales = 0;
                                        if (sizeof($res) > 0) {
                                            foreach ($res as $data) {
                                                ?>

                                                <tr style="font-size: 12px;border: 1px solid;cursor: pointer">
                                                    <td class="common_td_rules" >{{ $data->item_desc }}</td>
                                                    <td class="td_common_numeric_rules" >{{ $data->net_sales  }}</td>

                                                </tr>
                                                <?php
                                                $total_net_sales += $data->net_sales;
                                            }
                                            ?>
                                                <tr><td><b>Total</b></td> <td class="td_common_numeric_rules"><b>{{$total_net_sales}}</b></td></tr>
                                        <?php } else {
                                            ?>
                                            <tr>
                                                <td colspan="4" style="text-align: center;">No Results Found!</td>
                                            </tr>
                                            <?php
                                        }
                                        ?></tbody>
                                    </tbody>

                                </table>
                                </font>
                            </div>
                        </div>

                    </page>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
@endsection
