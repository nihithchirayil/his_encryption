<?php
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 10 * $padding_number . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";

if(isset($expand_speciality) && $expand_speciality == 1){
    for ($i = 0; $i < sizeof($ledger_result); $i++) {

if ($ledger_result[$i]->dr_amount > 0 || $ledger_result[$i]->cr_amount > 0) {
    $expand_class = '';
    $onclick = "onclick=getSingleLedgerDetails(this," . $ledger_result[$i]->id . ",'$from_type')";
    ?>
    <tr style= 'cursor:pointer;border: thin solid #d7d7d7!important' class="{{$show_row_group}} {{$ledger_result[$i]->id}} " data-parent-ids="{{$show_row_group}}" id="{{$ledger_result[$i]->id}}">
        <td <?= $onclick ?> rowspan="" style="width:22%;padding-left:30px !important;text-align:left;" colspan="7" class="common_td_rules" >{{$ledger_result[$i]->ledger_name}}</td>
        <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules debit_amount"  id="dr_amount_{{$ledger_result[$i]->speciality_id}}">{{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}</td>
        <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules credit_amount" id="cr_amount_{{$ledger_result[$i]->speciality_id}}">{{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}}</td>
        <td class="common_td_rules excludeItemButton" style="<?= $border_style; ?>; width:1%; "><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this,'{{$ledger_result[$i]->id}}');" ><i class="fa fa-minus"></i></button></td>
    </tr>
<?php

}
}
}else{
for ($i = 0; $i < sizeof($ledger_result); $i++) {

        if ($ledger_result[$i]->dr_amount > 0 || $ledger_result[$i]->cr_amount > 0) {
            $expand_class = 'expand_all_trail';
            $onclick = "onclick=showSpecialityTrailAmount(this," . $ledger_result[$i]->speciality_id . ",'$from_type','$ledger_code')";
            ?>
            <tr style= 'cursor:pointer;font-weight: 600;border: thin solid #d7d7d7!important' class="{{$show_row_group}} {{$expand_class}}" data-parent-ids="{{$show_row_group}}" id="{{$ledger_result[$i]->speciality_id}}">
                <td <?= $onclick ?> rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left;{{$padding}}" colspan="7" class="common_td_rules" >{{$ledger_result[$i]->speciality}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules debit_amount"  id="dr_amount_{{$ledger_result[$i]->speciality_id}}">{{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}</td>
                <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules credit_amount" id="cr_amount_{{$ledger_result[$i]->speciality_id}}">{{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}}</td>
                <td class="common_td_rules excludeItemButton" style="<?= $border_style; ?>; width:1%; "><button type="button" class="btn btn-sm padding_sm" onclick="excludeItemTrail(this,'{{$ledger_result[$i]->speciality_id}}');" ><i class="fa fa-minus"></i></button></td>
            </tr>
        <?php

    }
}
}
?>
