<input type="hidden" id="ttl_adv_pat_amnt" value="{{$advtotaldata}}">
<div class="row">
    <div class="col-md-12">
        <div class="theadscroll" style="position: relative; height: 350px;">
            <table id="result_data_table" class='table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table' style="font-size: 12px;">
                <thead>
                    <tr class="table_header_bg" style="background: #53aab3 !important;">
                        <th>UHID</th>
                        <th>Patient Name</th>
                        <th>Amount</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $balance = 0;
                    if (count($res) != 0) {
                        foreach ($res as $data) {
                            ?>
                            <tr>
                                <td style="text-align: left">{{$data->uhid}}</td>
                                <td style="text-align: left">{{$data->patient_name}}</td>
                                <td style="text-align: right">{{$data->balance}}</td> 
                            </tr>
                            <?php $balance += $data->balance;
                        } ?>

<?php } else { ?>
                        <tr>
                            <td style="font-family:robotoregular" style="text-align: center;"></td>
                        </tr>
<?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 text-right"  style="padding-top:10px">
        <ul class="advance_from_pages pagination purple_pagination" style="text-align:right !important;">
            {!! $paginator->render() !!}
        </ul>
    </div>
</div>