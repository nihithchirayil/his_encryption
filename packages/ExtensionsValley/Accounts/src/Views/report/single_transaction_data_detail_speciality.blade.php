<?php
$expand_all = '';
$padding_size = 10 * 5 . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";

if(isset($expand_speciality) && $expand_speciality == 1){
for ($i = 0; $i < sizeof($ledger_result); $i++) {

    $show_sub = "style= 'cursor:pointer;font-weight: 500;;border: thin solid #d7d7d7!important'";
        $show_sub1 = "style= 'cursor:pointer;font-weight: 500;'";
        $onclick = "onclick=getSingleLedgerDetails(this," . $ledger_result[$i]->id . ",'$from_type')";
        $expand_all = 'expand_all';

            $bal_amount = $ledger_result[$i]->cr_amount-$ledger_result[$i]->dr_amount;

        if ($ledger_result[$i]->is_ledger == 0 && $bal_amount != 0) {
            $expand_all = 'expand_all';
        }else{
            $expand_all = '';
        }
        $border_style = 'padding-left:60px !important';
        if($bal_amount != 0 ){
            ?>
<tr <?= $show_sub1 ?> <?= $onclick ?> class=" {{ $expand_all }} {{ $speciality_id }} {{ $show_row_group }}"
    data-parent-ids="" id="{{ $ledger_result[$i]->id }}">
    <td rowspan="" style="width:22%;<?= $border_style ?>;text-align:left;" class="">
        {{ $ledger_result[$i]->ledger_name }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="dr_amount_{{ $ledger_result[$i]->id }}">
        {{ number_format($bal_amount, 2, '.', ',') }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="cr_amount_{{ $ledger_result[$i]->id }}"></td>
</tr>
<?php }


}
}else{
    for ($i = 0; $i < sizeof($ledger_result); $i++) {

$show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
$show_sub1 = "style= 'cursor:pointer;font-weight: 600;'";
$onclick = "onclick=showSpecialityAmount(this," . $ledger_result[$i]->speciality_id . ",'$from_type','$ledger_code')";
$expand_all = 'expand_all';
$bal_amount = $ledger_result[$i]->cr_amount-$ledger_result[$i]->dr_amount;

$border_style = '';
if($bal_amount != 0){
    ?>
<tr <?= $show_sub1 ?> <?= $onclick ?> class="{{ $show_row_group }} {{ $expand_all }} "
    data-parent-ids="{{ $show_row_group }}" id="{{ $ledger_result[$i]->speciality_id }}">
    <td rowspan="" style="width:22%;<?= $border_style ?>;text-align:left;{{ $padding }}"
        class="{{ $show_row_group }}">{{ $ledger_result[$i]->speciality }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="dr_amount_{{ $ledger_result[$i]->speciality_id }}">
        {{ number_format($bal_amount, 2, '.', ',') }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="cr_amount_{{ $ledger_result[$i]->speciality_id }}"></td>
</tr>
<?php
    }

}
}



?>
