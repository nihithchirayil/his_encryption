<?php
$expand_all = '';
$padding_number_arr = explode(" ", $show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 10 * $padding_number . 'px';
$padding = "padding-left:$padding_size !important";
$border_style = "$padding";
$closing_stock_text_qry = \DB::table('ledger_master')->where('ledger_type',5)->select('ledger_name','id')->get();
$closing_stock_text = @$closing_stock_text_qry[0]->ledger_name ? $closing_stock_text_qry[0]->ledger_name:'Closing Stock';
$closing_stock_id = @$closing_stock_text_qry[0]->id ? $closing_stock_text_qry[0]->id:'1';
if((isset($first_td_text) && ($first_td_text =='Current Assets') ) && (isset($from_type) && $from_type == 'balance_amnt')){ ?>

<?php }
if((isset($first_td_text) && ($first_td_text =='Closing Stock' || $first_td_text =='Inventory') )){ ?>
<tr style="padding-left:40px !important" class="{{ $show_row_group }}">
    <td style="width:22%;padding-left: 30px !important;">Stock of medicine</td>
    <td style="padding-left:8px !important;text-align:right">{{ @$closing_stock_val[0]->stock_of_medicine ?? '' }}</td>
    <td style="text-align:right"></td>
</tr>
<tr style="padding-left:40px !important" class="{{ $show_row_group }}">
    <td style="width:22%;padding-left: 30px !important;">Stock of x-ray</td>
    <td style="padding-left:8px !important;text-align:right">{{ @$closing_stock_val[0]->stock_of_xray ?? '' }}</td>
    <td style="text-align:right"></td>
</tr>
<?php }
if((isset($first_td_text) && ($first_td_text =='Opening Stock') )){ ?>
<tr style="padding-left:40px !important" class="{{ $show_row_group }}">
    <td style="width:22%;padding-left: 30px !important;">Stock of medicine</td>
    <td style="padding-left:8px !important;text-align:right">{{ @$open_stock[0]->stock_of_medicine ?? '' }}</td>
    <td style="text-align:right"></td>
</tr>
<tr style="padding-left:40px !important" class="{{ $show_row_group }}">
    <td style="width:22%;padding-left: 30px !important;">Stock of x-ray</td>
    <td style="padding-left:8px !important;text-align:right">{{ @$open_stock[0]->stock_of_xray ?? '' }}</td>
    <td style="text-align:right"></td>
</tr>
<?php }
for ($i = 0; $i < sizeof($ledger_result); $i++) {
if($i+1 == sizeof($ledger_result)){
    $final = 1;
}else{
    $final = 0;
}
$data_spec_attr = '';
    if (isset($ledger_result[$i]->custom_flag) && $ledger_result[$i]->custom_flag == 11) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $show_sub1 = "style= 'cursor:pointer;font-weight: 600;'";
        $ledg_code = $ledger_result[$i]->ledger_code;
        $onclick = "onclick=getSingleTransactionReportDataDetailBySpeciality(this," . $ledger_result[$i]->id . ",'$from_type','$ledg_code')";
        $expand_all = 'expand_all';
        $data_spec_attr = 'data-speciality-attr=1';

    }else if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;font-weight: 600;;border: thin solid #d7d7d7!important'";
        $show_sub1 = "style= 'cursor:pointer;font-weight: 600;'";
        $onclick = "onclick=showSingleAmountDetail(this," . $ledger_result[$i]->id . ",'$from_type')";
        $expand_all = 'expand_all';

    } else {
        $show_sub = "style= 'cursor:pointer;border: thin solid #d7d7d7!important' ";
        $show_sub1 = "style= 'cursor:pointer;'";
        if($ledger_result[$i]->custom_flag == 12){
         $onclick = "onclick=getAdvanceFromPatients();";
        }else{
        $onclick = "onclick=getSingleLedgerDetails(this," . $ledger_result[$i]->id . ",'$from_type');";
        }
        $expand_all = '';
    }
    if (isset($from_type) && $from_type == 'balance_amnt') {
        if($ledger_result[$i]->pl_type == 2){
            $bal_amount = $ledger_result[$i]->cr_amount-$ledger_result[$i]->dr_amount;
        }else{
            $bal_amount = $ledger_result[$i]->dr_amount-$ledger_result[$i]->cr_amount;
        }

        $bal_amount = ($bal_amount);
        if ($ledger_result[$i]->is_ledger == 0 && $bal_amount != 0) {
            $expand_all = 'expand_all';
        }else{
            $expand_all = '';
        }
        $border_style = '';
        if(isset($ledger_result[$i]->speciality)  && $ledger_result[$i]->speciality != '' && $bal_amount != 0){
            $onclick = "onclick=showSpecialityAmount(this," . $ledger_result[$i]->speciality_id . ",'$from_type')"; ?>
<tr style='cursor:pointer;font-weight: 600;' <?= $onclick ?> class="{{ $show_row_group }} {{ $expand_all }} "
    data-parent-ids="{{ $show_row_group }}" id="{{ $ledger_result[$i]->speciality_id }}">
    <td rowspan="" style="width:22%;<?= $border_style ?>;text-align:left;{{ $padding }}"
        class="{{ $show_row_group }}">{{ $ledger_result[$i]->speciality }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="dr_amount_{{ $ledger_result[$i]->speciality_id }}">
        {{ number_format($bal_amount, 2, '.', ',') }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="cr_amount_{{ $ledger_result[$i]->speciality_id }}"></td>
</tr>
<?php }else if($ledger_result[$i]->ledger_name=='Inventory' || $ledger_result[$i]->ledger_name=='Closing Stock'){?>
<tr <?= $show_sub1 ?> class="{{ $show_row_group }}"
    style="cursor: pointer"onclick="showSingleAmountDetail(this,'{{ $closing_stock_id }}','balance_amnt')">
    <td style="padding-left:20px !important;<?= $border_style ?>">{{ $closing_stock_text }}</td>
    <td style="padding-left:8px !important;text-align:right;<?= $border_style ?>">{{ $closing_stock ?? '' }}</td>
    <td style="text-align:right"></td>
</tr>
<?php }else if($bal_amount != 0 ){ //else if($bal_amount != 0 || $ledger_result[$i]->is_ledger == 0)
            ?>
<tr <?= $show_sub1 ?> <?= $onclick ?> class="{{ $show_row_group }} {{ $expand_all }}"
    data-parent-ids="{{ $show_row_group }}" id="{{ $ledger_result[$i]->id }}" {{ $data_spec_attr }}>
    <td rowspan="" style="width:22%;<?= $border_style ?>;text-align:left;{{ $padding }}"
        class="{{ $show_row_group }}">{{ $ledger_result[$i]->ledger_name }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="dr_amount_{{ $ledger_result[$i]->id }}">
        {{ number_format($bal_amount, 2, '.', ',') }}</td>
    <td rowspan="" style="width:10%;{{ $border_style }};border-left: 0px !important"
        class="td_common_numeric_rules credit_amount" id="cr_amount_{{ $ledger_result[$i]->id }}"></td>
</tr>
<?php }

    } else {
        if ($ledger_result[$i]->dr_amount > 0 || $ledger_result[$i]->cr_amount > 0) {
            ?>
<tr <?= $show_sub ?> class="{{ $show_row_group }}" data-parent-ids="{{ $show_row_group }}"
    id="{{ $ledger_result[$i]->id }}">
    <td <?= $onclick ?> rowspan="" style="width:22%;<?= $border_style ?>;text-align:left;{{ $padding }}"
        colspan="7" class="common_td_rules">{{ $ledger_result[$i]->ledger_name }}</td>
    <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules debit_amount"
        id="dr_amount_{{ $ledger_result[$i]->id }}">{{ number_format($ledger_result[$i]->dr_amount, 2, '.', ',') }}
    </td>
    <td <?= $onclick ?> rowspan="" style="width:10%;" class="td_common_numeric_rules credit_amount"
        id="cr_amount_{{ $ledger_result[$i]->id }}">{{ number_format($ledger_result[$i]->cr_amount, 2, '.', ',') }}
    </td>
    <td class="common_td_rules excludeItemButton" style="<?= $border_style ?>; width:1%; "><button type="button"
            class="btn btn-sm padding_sm" onclick="excludeItem(this);"><i class="fa fa-minus"></i></button></td>
</tr>
<?php
        }
    }
}
?>
