<style>
   td{
        padding:5px;
        width:10%;
    }
    .td_head_cls {
        background-color: #c6d4e5;
    }

    hr{
        margin-top: 0px;
        margin-bottom: 1px;
        border: 0;
        border-top: 1px solid black;
    }
</style>
<?php
$sort_liab_array = $sort_asset_array = array();
$asset_total = $liab_total = $asset_amnts = 0;
?>
<div class="row" id="result_container_div">
    <div class="col-md-12">
        <table id="result_data_table" class="" >
            <tr >
                <td colspan="6"><b>{{@$company_code[0]->custom_name}}- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</b></td>


            </tr>
            <tr>
                <td colspan="6">{{@$company_code[0]->address}}</td>

            </tr>
            <tr>
                <td colspan="6">{{@$company_code[0]->city}}</td>


            </tr>
            <tr >
                <td colspan="6"><b>Balance Sheet</b></td>

            </tr>
            <tr >
                <td colspan="6" >{{$from_date}} to {{$to_date}}</td>

            </tr>
        </table>
        <table width="100%" border="1" style="border-collapse: collapse;">
            <tbody>
                <tr>
                    <td style="vertical-align: top;padding: 0;">
                        <table border="0" style="border-collapse: collapse;">
                            <tr>
                                <td  style="border-bottom: thin solid black;" colspan="3"><b>Liabilities</b>
                                    <b style="float: right">{{$from_date}} to {{$to_date}}</b></td>
                            </tr>
                            <?php
                            for ($i = 0; $i < sizeof($liability_results); $i++) {
                                if (isset($liability_results[$i])) {
                                    if (!in_array($liability_results[$i]->id, $sort_liab_array)) {
                                        $sort_liab_array[] = $liability_results[$i]->id;
                                        $sort_liab_order = true;
                                    }
                                }
                                if ($sort_liab_order) {
                                    //if(abs($liability_results[$i]->amount) > 0){
                                           $cur_liab_amnt = $liability_results[$i]->amount;
                                    ?>
                                    <tr onclick="showSingleAmountDetail(this,'{{$liability_results[$i]->id}}','balance_amnt')" class="expand_all">
                                        <?php if ($sort_liab_order) { ?>
                                            <td style="cursor: pointer;width:80%">{{$liability_results[$i]->ledger_name??''}}</td>
                                            <td style="cursor: pointer" ></td>
                                            <td style="cursor: pointer;text-align:right">
                                                <b>{{ $cur_liab_amnt ? number_format($cur_liab_amnt, 2, '.', ',') : ''}}</b>
                                            </td>
                                            <?php }  ?>
                                    </tr>
                                    <?php
                                    $liab_total = $liab_total + $cur_liab_amnt;
                                    $sort_liab_order = false;
                               // }
                                }
                                ?>
                                <?php
                            }
                            if (isset($proft_loss) && $proft_loss == 2) {
                                $liab_total = $liab_total+$pl_amount;
                                $pl_ac_amount = ($pl_amount);

                            }else{
                                $pl_ac_amount = $pl_amount;
                                $liab_total = ($liab_total)+$pl_ac_amount;
                            }
                                ?>
                                <tr onclick="getPlOpeningBalance(this,'<?= $pl_ac_amount ?>')" data-opb_filter="2">
                                    <td style="cursor: pointer;width:80%" >Profit & Loss A/c</td>
                                    <td style="cursor: pointer" ></td>
                                    <td style="cursor: pointer;text-align:right"><b><?= number_format($pl_ac_amount, 2, '.', ',') ?></b></td>
                                </tr>
                                <?php

                            ?>

                        </table>
                    </td>
                    <td style="padding: 0;vertical-align: top;">
                        <table border="0" style="margin-left: -1px;border-collapse: collapse;">
                            <tr>
                                <td   style="border-bottom: thin solid black;" colspan="3"><b>Assets</b>
                                    <b style="float: right">{{$from_date}} to {{$to_date}}</b></td>
                            </tr>
                            <?php
                            for ($i = 0; $i < sizeof($asset_results); $i++) {
                                if (isset($asset_results[$i])) {
                                    if (!in_array($asset_results[$i]->id, $sort_asset_array)) {
                                        $sort_asset_array[] = $asset_results[$i]->id;
                                        $sort_asset_order = true;
                                    }
                                }
                                if ($sort_asset_order) {
                                    //if(abs($asset_results[$i]->amount) > 0){
                                    ?>
                                    <tr onclick="showSingleAmountDetail(this,'{{$asset_results[$i]->id}}','balance_amnt')" class="expand_all">
                                        <?php if ($sort_asset_order) { ?>
                                            <td style="cursor: pointer;width:80%">{{$asset_results[$i]->ledger_name??''}}</td>
                                            <td style="cursor: pointer" ></td>
                                            <?php if(strtolower($asset_results[$i]->ledger_name)=='current assets'){
                                                $asset_amnt = ($asset_results[$i]->amount)+($closing_stock);
                                            }else{
                                                $asset_amnt = ($asset_results[$i]->amount);
                                            } ?>
                                            <td style="cursor: pointer;text-align:right">

                                                <b>{{@$asset_amnt ? number_format($asset_amnt, 2, '.', ',') : ''}}</b>
                                            </td>
                                            <?php
                                            $asset_total += ($asset_amnt);
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    $sort_asset_order = false;
                                //}
                                }
                                ?>
                            <?php }
                            ?>

                        </table>
                </tr>
                <tr>
                    <td style="padding: 0;vertical-align: top;margin-top: -1px;">
                        <table border="0" style="border-collapse: collapse;margin-top: -1px;" >
                            <tr>
                                <td colspan="2" style="cursor: pointer;padding: 0px;padding-top: 5px;width:80%" ><b style="padding-left:5px">Total</b></td>
                                <td style="cursor: pointer;text-align:right;padding: 0px;padding-top: 5px"><b>{{number_format(abs($liab_total), 2, '.', ',')}}</b></td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding: 0;">
                        <table style="margin-left: -1px;margin-top: -1px;" >
                            <tr>
                                <td colspan="2" style="cursor: pointer;padding: 0px;padding-top: 5px;width:80%" ><b style="padding-left:5px">Total</b></td>
                                <td style="cursor: pointer;text-align:right;padding: 0px;padding-top: 5px"><b>{{number_format(abs($asset_total), 2, '.', ',')}}</b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
