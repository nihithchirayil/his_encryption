<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> </b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="4">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>

                    @if(sizeof($res)>0)
                    @php
                        $sort_array = array();
                        $i = 1;
                        $sort_change   = $exceeded = $exceeded_amount  = $nearly_exceeded = $nearly_exceeded_amount = 0;
                        $sub_amount_total = $grand_amount_total = $sub_exceeded_amnt = $sub_nearly_exceeded_amnt = 0;
                    @endphp
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th  style="padding: 2px;width:2%;text-align: center;border-right:0px !important; ">No</th>
                        <th  style="padding: 2px;width:22%;text-align: center;border-right:0px !important; ">Account</th>
                        <th  style="padding: 2px;width:10%;text-align: center;border-right:0px !important;">Debit</th>
                        <th  style="padding: 2px;width:10%;text-align: center;border-right:0px !important;">Credit</th>
                    </tr>

                    @foreach ($res as $data)
                    <?php  if ($data->is_ledger == 0) {
                        $fn_name = "onclick = expandRow(this," . $data->parent_id . ")";
                    } else {
                        $fn_name = "onclick = getSingleLeaderDetails(this," .$data->parent_id . ");";
                    } ?>
                    <tr id="{{$data->parent_id}}" style="cursor:pointer;border: thin solid black;" onclick="<?= $fn_name ?>" >
                        <td style="width:2%;text-align: center;border:thin solid black;width:2%">
                            {{$i}}
                        </td>
                        <td class="common_td_rules" style="width:22%">{{$data->parent_name}}</td>
                        <td class="td_common_numeric_rules" style="font-weight: bold;width:10%" id="dr_amount_{{$data->parent_id}}">{{number_format($data->dr_amount, 2, '.', ',')}}</td>
                        <td class="td_common_numeric_rules" style="font-weight: bold;width:10%" id="cr_amount_{{$data->parent_id}}">{{number_format($data->cr_amount, 2, '.', ',')}}</td>
                    </tr>
                    @php $i++; @endphp
                @endforeach
                @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                @endif
            </table>
        </div>
    </div>

