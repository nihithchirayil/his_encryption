@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Name</th>
                                </tr>
                                <tbody class="row_position">
                                    <?php
                                    foreach ($ledgerGroups as $gp) {
                                        ?>
                                        <tr  id="<?php echo $gp->id ?>">
                                            <td><?php echo $gp->ledger_name; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js")}}"></script>
<!--<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>-->
    <script type="text/javascript">
        $(".row_position").sortable({
            delay: 150,
            stop: function() {
                var selectedData = new Array();
                $('.row_position>tr').each(function() {
                    selectedData.push($(this).attr("id"));
                });
                updateOrder(selectedData);
            }
        });
        function updateOrder(data) {
            $.ajax({
                url:" ",
                type:'post',
                data:{position:data},
                success:function(data){
                    toastr.success('Your Change Successfully Saved.');
                }
            })
        }
    </script>

@endsection
