<input type="hidden" name="voucher_type_id" id="voucher_type_id" value="{{$voucher_type_id}}">
<input type="hidden" name="detail_cr_top" id="detail_cr_top" value="{{$detail_cr}}" class="detail_cr_top">
<input type="hidden" name="exp_inc_type" id="exp_inc_type" value="<?= $exp_inc_type ?>">
<div class="col-md-3 padding_sm">
    <div class="mate-input-box">
        <label>Party <span style="color: red">*</span></label>
        <input type="text" required="" autocomplete="off"
               onkeyup="select_item_desc(this.id, event,'{{$voucher_type_id}}','1')" id="ledger_item_desc-0"
               class="form-control auto_focus_cls" name="party[]"
               placeholder="Search Party">
        <div class='ajaxSearchBox' id="search_ledger_item_box-0" index='1'
             style="margin-top: 14px !important;width: 470px !important;max-height: 290px;"> </div>
        <input type='hidden' name='ledger_id_head' value="" id="ledger_id_head-0">
        <input type='hidden' name='ledger_group_head' value="" id="ledger_group_head-0">
    </div>
</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box" style="height: 45px !important">
        <label>Curr.Balance</label>
        <div class="clearfix"></div>
        <span id="ledger_code"> </span>
    </div>
</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box">
        <label>Amount<span style="color: red">*</span></label>
        <div class="clearfix"></div>
        <input type="text" value="" id="amount_header" name="amount_header" autocomplete="off" onkeyup="number_validation(this)" onblur="amount_fetch(this); calculate_table_total('amount')"   class="form-control">
    </div>
</div>
    <div class="col-md-1 padding_sm" style="padding-bottom: 10px;">
    <div class="mate-input-box">
        <label>Invoice No</label>
        <input type="text" required="" autocomplete="off"
               id="purchase_invoice_no"
               class="form-control auto_focus_cls" name="purchase_invoice_no"
               placeholder="Invoice">
        <div class='ajaxSearchBox' id="search_invoice_box-0" index='1'
             style="top: 48px !important;width: 470px !important;max-height: 290px;"> </div>
    </div>
    </div>
    <div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Invoice Date</label>
        <div class="clearfix"></div>
        <input type="text" value="" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
    </div>
</div>

<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label for="">Dr/Cr.</label>
            <div class="clearfix"></div>
                 {!! Form::select('header_cr', array( "cr"=> " Credit","dr"=> " Debit"),'',
                     [
                         'class'       => 'form-control',
                         'id'          => 'header_cr', 'onchange' => 'drCrChange(this.value)'
                    ]) !!}
            </div>
</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box">
        <label>Int.Ref.No</label>
        <div class="clearfix"></div>
        <input type="text" value="" id="refer_no" name="refer_no" autocomplete="off" class="form-control">
    </div>
</div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-md-2 padding_sm  pull-right" style=" padding-top: 10px !important">
    <label for=""style="float:right">Voucher No : <span style="color: red" id="generated_vouchr_label"><b><?= $voucher_no ?></b></span></label>
    <input type="hidden" value="<?= $voucher_no ?>" name="generated_vouchr_text" id="generated_vouchr_text">
    <div class="clearfix">
    </div>


