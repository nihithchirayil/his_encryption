<style>
    .blinking-red{
animation:blinkingText 0.8s infinite;
}
@keyframes  blinkingText{
0%{     color: #FF0000;    }
49%{    color: #FF0000; }
60%{    color: transparent; }
99%{    color:transparent;  }
100%{   color: #FF0000;    }
}
.overlay{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: transparent;
    z-index: 1;
    pointer-events: all;
    opacity: 0;
}
</style>

@php $readonly = $span_text ='' @endphp
@if($header[0]->voucher_type == 12 && $header[0]->is_advance == 1)
   @php
   $readonly = 'readonly';
   $span_text = "<span class='blinking-red'>cannot change Ledger with advance amount</span>";
   @endphp
@endif
<div class="col-md-3 padding_sm">
    <div class="mate-input-box">
        <label>Account Name <span style="color: red">*</span></label>
        <input type="text" required="" autocomplete="off" value="{{$header[0]->ledger_name??''}}"
               onkeyup="select_item_desc(this.id, event,'{{$header[0]->voucher_type??''}}', '1')" id="ledger_item_desc-0"
               class="form-control auto_focus_cls" name="party[]"
               placeholder="Search Party" {{ $readonly }}>

        <div class='ajaxSearchBox' id="search_ledger_item_box-0" index='1'
             style="top: 48px !important;width: 470px !important;max-height: 290px;"> </div>
        <input type='hidden' name='ledger_id_head' value="{{$header[0]->ledger_id??''}}" id="ledger_id_head-0" >
        <input type='hidden' name='ledger_group_head' value="{{$header[0]->group_id??''}}" id="ledger_group_head-0">
    <?= $span_text ?>
    </div>


</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box" style="height: 45px !important">
        <label>Curr.Balance</label>
        <div class="clearfix"></div>
        @if(isset($header[0]->current_balance) && $header[0]->current_balance < 0)
        <span id="ledger_code">{{$header[0]->current_balance? abs($header[0]->current_balance):''}}. Dr </span>
        @else
        <span id="ledger_code">{{$header[0]->current_balance? abs($header[0]->current_balance):'0'}}. Cr </span>
        @endif
    </div>
</div>


<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Amount<span style="color: red">*</span></label>
        <div class="clearfix"></div>
        <input type="text" value="{{$header_amount??''}}" id="amount_header" name="amount_header" autocomplete="off" onkeyup="number_validation(this)" onblur="amount_fetch(this); calculate_table_total('amount')"   class="form-control">
    </div>
</div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Date</label>
        <div class="clearfix"></div>
        <input type="text" value="<?= $header[0]->invoice_date ? date('d-m-Y', strtotime($header[0]->invoice_date)) : '' ?>" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
    </div>
</div>
<div class="col-md-1 padding_sm">
            <div class="mate-input-box">
                <label>CDV No</label>
                <input type="text" autocomplete="off" value="{{$header[0]->cdv ??''}}" id="cdv_no" class="form-control" name="cdv_no" placeholder="CDV">
            </div>
    </div>
    <div class="col-md-1 padding_sm">
            <div class="mate-input-box">
                <label>Cheque No</label>
                <input type="text" autocomplete="off" value="{{$header[0]->cheq_no ??''}}" id="check_no" class="form-control" name="check_no" placeholder="Cheque No">
            </div>
    </div>
    <div class="col-md-1 padding_sm" >
            <div class="mate-input-box">
                <label>Cheque Date</label>
                <input type="text" value="<?= $header[0]->cheq_dt ? date('d-m-Y', strtotime($header[0]->cheq_dt)) : '' ?>" autocomplete="off" id="check_dt" class="form-control datepicker" name="check_dt" placeholder="Cheque Date">
            </div>
    </div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label for="">Dr/Cr.</label>
        <div class="clearfix"></div>
        {!! Form::select('header_cr', array("-1"=> " Select","dr"=> " Debit", "cr"=> " Credit"),$header[0]->cr_dr??'',
        [
        'class'       => 'form-control',
        'id'          => 'header_cr', 'onchange' => 'drCrChange(this.value)'
        ]) !!}
    </div>
</div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Int.Ref.No</label>
        <div class="clearfix"></div>
        <input type="text" value="{{$header[0]->ref_detail??''}}" id="refer_no" name="refer_no" autocomplete="off" class="form-control">
    </div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-md-2 padding_sm  pull-right" style=" padding-top: 10px !important">
    <label for=""style="float:right">Voucher No : <span style="color: red" id="generated_vouchr_label"><b>{{$header[0]->voucher_no??''}}</b></span></label>
    <input type="hidden" value="{{$header[0]->voucher_no??''}}" name="generated_vouchr_text" id="generated_vouchr_text">
    <div class="clearfix">
    </div>
</div>

@if($header[0]->voucher_type == 12)
<div class="col-md-1 padding_sm" style="margin-top: 15px;">
    <div class="checkbox checkbox-success inline no-margin">
        <input type="checkbox" class="form-control filters" name="advance_from_party"  id="advance_from_party" value="1" <?php if($header[0]->is_advance == 1){ ?> checked <?php } ?> >
        <label class="text-blue " for="advance_from_party">Advance</label>
        <?php if($header[0]->is_advance == 1){ ?> <div class="overlay"></div> <?php } ?>
    </div>
</div>
@endif
