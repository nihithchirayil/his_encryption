@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')

    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-8 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                          <form action="{{ route('extensionsvalley.accounts.listStockClosingBalance') }}" id="categorySearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepicker" name="clsing_dt"
                                            value="{{ $searchFields['clsing_dt'] ?? '' }}">
                                    </div>
                                </div>

                               <!-- <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Vendor Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="vendor_name"
                                            value="{{ $searchFields['vendor_name'] ?? '' }}">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Status</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('status', ['-1' => ' Select Status', '1' => ' Active', '0' => ' Inactive'], $searchFields['status'] ?? '', [
    'class' => 'form-control',
]) !!}
                                    </div>
                                </div>-->
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i> Clear</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th>Closing Month</th>
                                        <th>Stock Medicine</th>
                                        <th>Stock X-Ray</th>
                                        <th>Total</th>
                                        <th>Edit</th>
                                        <th ><i class="fa fa-history"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($stock_list) > 0)
                                        @foreach ($stock_list as $stk)
                                            <tr >
                                                <td class=" common_td_rules">{{ $stk->cl_mnth }}</td>
                                                <td class=" td_common_numeric_rules">{{ $stk->stock_of_medicine }}</td>
                                                <td class=" td_common_numeric_rules ">{{ $stk->stock_of_xray }}</td>
                                                <td class=" td_common_numeric_rules ">{{ $stk->stock_value }}</td>
                                                <td style="cursor: pointer;" onclick="closingEditLoadData(this,
                                                '{{ $stk->id }}',
                                                '{{ $stk->cl_mnth }}',
                                                '{{ $stk->stock_of_medicine }}','{{ $stk->stock_of_xray }}');">
                                                 <i class="fa fa-edit"></i>
                                                </td>
                                                <td>
                                                    <i  style="cursor:pointer;color: #f0ad4e"  id="history_closing_{{$stk->id}}" onclick="closingStockAuditLog({{$stk->id}})"
                                                        class="fa fa-history"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="vendor_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-right">
                            <ul class="pagination purple_pagination" style="text-align:right !important;">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <form action="{{ route('extensionsvalley.accounts.saveClosingStock') }}" method="POST"
                            id="stockForm">
                            {!! Form::token() !!}
                            <input type="hidden" name="stock_id" id="stock_id" value="0">
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Closing Month</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="stock_date"
                                        value=" " id="stock_date">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Stock Of Medicine</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="stock_medicine"
                                        value="" id="stock_medicine">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Stock Of X-RAY</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="stock_xray"
                                        value="" id="stock_xray">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-4 padding_sm">
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i> Cancel</a>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <button id="saveVendorDataBtn" class="btn btn-block light_purple_bg"><i
                                        id="saveVendorDataSpin" class="fa fa-save"></i> Save</button>
                            </div>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="getAuditLog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1300px;width: 80%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <span class="modal-title" id="audit_log_header"></span>
                </div>
                <div class="modal-body" style="min-height:400px;height:500px;overflow: auto" id="audit_log_body">

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/audit_log.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        });



        function closingEditLoadData(obj, id, closing_month, stock_of_medicine, stock_of_xray) {
            $('#stock_id').val(id);
            $('#stock_date').val(closing_month);
            $('#stock_medicine').val(stock_of_medicine);
            $('#stock_xray').val(stock_of_xray);
            $('#stockForm').attr('action', '{{ $updateUrl }}');
        }
        @include('Purchase::messagetemplate')
    </script>

@endsection
