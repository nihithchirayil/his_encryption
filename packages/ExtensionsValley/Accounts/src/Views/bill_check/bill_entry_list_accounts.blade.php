@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

@stop

@section('css_extra')
    <style>
        .close_btn_vendor_search {

            position: absolute;
            z-index: 99;
            color: #FFF !important;
            background: #000;
            right: -11px;
            top: -1px;
            border-radius: 100%;
            text-align: center;
            width: 20px;
            height: 20px;
            line-height: 20px;
            cursor: pointer;
        }

        .vendor-list-div {
            position: absolute;
            z-index: 99;
            background: #FFF;
            box-shadow: 0 0 6px #ccc;
            padding: 10px;
            /* width: 95%; */
            width: 300px;
        }

        #VendorTable>tbody>tr:hover {
            background: #87d7ca52;
        }

        #VendorTable>tbody>tr>td {
            padding: 8px;
            line-height: 2;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .ajaxSearchBox {
            display: block;
            z-index: 7000;
            margin-top: 15px !important;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->

    <div class="modal fade" id="grnlist_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Grn View</h4>
                </div>
                <div class="modal-body" style="min-height: 400px" id="grnlist_div">

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" type="button" class="btn light_purple_bg" onclick="exceller()" ;>Excel
                        <i class="fa fa-file-excel-o"></i></button>
                    <button style="padding: 3px 3px" type="button" class="btn btn-primary"
                        onclick="printReportData();">Print <i class="fa fa-print"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col">

        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.purchase.grnList') }}" id="requestSearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepicker" name="from_date" id="from_date">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepicker" name="to_date" id="to_date">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">GRN No</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="grn_no_search">
                                        <div id="ajaxGrnNoSearchBox" class="ajaxSearchBox"></div>
                                        <input type="hidden" id="grn_id_hidden">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill No</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="bill_no_search">
                                        <div id="ajaxBillNoSearchBox" class="ajaxSearchBox"></div>
                                        <input type="hidden" id="bill_id_hidden">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Vendor Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control"
                                            id="vendor_name_search">
                                        <div id="ajaxVendorSearchBox" class="ajaxSearchBox"></div>
                                        <input type="hidden" id="vendoritem_id_hidden">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Product Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="item_desc">
                                        <div id="item_desc_AjaxDiv" class="ajaxSearchBox"></div>
                                        <input type="hidden" name="item_desc_hidden" id="item_desc_hidden">
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill Location</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('location', ['-1' => ' Bill Location'] + $location->toArray(), $default_location, [
                                            'class' => 'form-control to_location',
                                            'onchange' => 'searchGrnList()',
                                            'id' => 'to_location',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Select Status</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('status', [ '4' => ' Closed', '1' => ' Not Approved', '2' => ' Approved', '3' => ' Cancelled'], $grnListStatus, [
                                        'class' => 'form-control status',
                                        'onchange' => 'searchGrnList()',
                                        'id' => 'status',
                                    ]) !!}
                                 </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                        <input type="checkbox"  id="grn_receicved_checkbox" value="">
                                        <label for="grn_receicved_checkbox" style="font-weight: 700">Received</label>
                                </div>


                                
                                <div class="col-md-1 padding_sm pull-right">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" onclick="searchGrnList()" id="searchgrnlistBtn"
                                        class="btn btn-block light_purple_bg"><i id="searchgrnlistSpin"
                                            class="fa fa-search"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-1 padding_sm pull-right">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block light_purple_bg"><i
                                            class="fa fa-times"></i> Clear</a>
                                </div>
        
                                <div class="col-md-1 padding_sm pull-right">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <div onclick="updateAccountsGrn();" class="btn btn-block light_purple_bg" id="updateAccountsGrn"><i
                                            class="fa fa-save" id="updateAccountsGrn_spin"></i> Save</div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" id="searchGrnListData" style="min-height: 400px;">


                    </div>
                </div>
                <input type="hidden" id="base_url" value="{{ url('/') }}" />
                <input type="hidden" id="token" value="{{ csrf_token() }}" />
            </div>
        </div>
    </div>

    {{-- include('core::dashboard.partials.bootstrap_datetimepicker_js') --}}
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
      <script 
        src="{{asset("packages/extensionsvalley/accounts/default/javascript/accounts_grn_list.js")}}">
    </script>
    <script 
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}">
    </script>

@stop
