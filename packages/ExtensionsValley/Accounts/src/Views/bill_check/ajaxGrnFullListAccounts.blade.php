<div class="col-md-12 wrapper" id="result_container_div">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th width="3%">SI.No.</th>
                <th width="9%">GRN No</th>
                <th width="9%">Bill No.</th>
                <th width="9%">Location</th>
                <th width="9%">Created Date</th>
                <th width="9%">Created By</th>
                <th width="9%">Approved Date</th>
                <th width="9%">Approved By</th>
                <th width="9%">PO No.</th>
                <th width="11%">Vendor Name</th>
                <th width="9%">Net Amount</th>
                <th width="5%">Status</th>
            </tr>
        </thead>
        <tbody>
            @if (count($item) > 0)
                <?php $i = 1; ?>
                @foreach ($item as $each)
                    <?php

                    $status = '';
                    if ($each->approve_status == 1) {
                        $status = 'Not Approved';
                    } elseif ($each->approve_status == 2) {
                        $status = 'Approved';
                    } elseif ($each->approve_status == 3) {
                        $status = 'Cancelled';
                    } elseif ($each->approve_status == 4) {
                        $status = 'Closed';
                    }
                    ?>
                    <tr>
                        <td style="text-align: left;">{{ $i }}</td>
                        <td style="text-align: left;">{{ $each->grn_no }}</td>
                        <td style="text-align: left;">{{ $each->bill_no }}</td>
                        <td style="text-align: left;">{{ $each->location }}</td>
                        <td style="text-align: left;" title="{{ $each->created_at }}">
                            {{ $each->created_at }}
                        </td>
                        <td style="text-align: left;" title="{{ $each->created_by }}">
                            {{ $each->created_by }}
                        </td>
                        <td style="text-align: left;" title="{{ $each->approved_at }}">
                            {{ $each->approved_at }}
                        </td>
                        <td style="text-align: left;" title="{{ $each->approved_by }}">
                            {{ $each->approved_by }}
                        </td>
                        <td style="text-align: left;" title="{{ $each->po_no }}">
                            @if (!empty($each->po_no))
                                {{ $each->po_no }}
                            @else
                                --
                            @endif
                        </td>
                        <td style="text-align: left;" title="{{ $each->vendor_name }}">
                            {{ $each->vendor_name }}
                        </td>
                        <td style="text-align: left;" title="{{ $each->net_amount }}">
                            {{ $each->net_amount }}
                        </td>
                        <td style="text-align: left;" title="{{ $status }}">
                            {{ $status }}
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="12" class="location_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
