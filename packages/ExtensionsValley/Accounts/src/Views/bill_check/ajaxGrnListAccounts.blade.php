<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var grn_id = $('#grn_id_hidden').val();
            var bill_no = $('#bill_no_search').val();
            var vendor_id = $('#vendoritem_id_hidden').val();
            var item_code = $('#item_desc_hidden').val();
            var location = $('#to_location').val();
            var status = $('#status').val();
            var param = {
                _token: token,
                grn_id: grn_id,
                from_date: from_date,
                to_date: to_date,
                vendor_id: vendor_id,
                item_code: item_code,
                bill_no: bill_no,
                location: location,
                status: status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#searchGrnListData").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    $('#searchgrnlistBtn').attr('disabled', true);
                    $('#searchgrnlistSpin').removeClass('fa fa-search');
                    $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchGrnListData').html(data);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                        if(empArr.length > 0){
                        var id_arr = new Array();
                        for (var k in empArr) {
                            if (empArr.hasOwnProperty(k)) {
                                id_arr.push(empArr[k]['bil_id']);
                            }
                        }
                       for(var k=0;k<id_arr.length;k++){
                        $("#"+id_arr[k]).prop('checked',true);
                       }
                    }
                    }, 400);
                },
                complete: function() {
                    $("#searchGrnListData").LoadingOverlay("hide");
                    $('#searchgrnlistBtn').attr('disabled', false);
                    $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchgrnlistSpin').addClass('fa fa-search');
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
            return false;
        });

    });
</script>
<table class="table no-margin table-striped table-col-bordered table-condensed" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th>GRN No</th>
            <th>Bill No.</th>
            <th>Location</th>
            <th>Created Date</th>
            <th>Created By</th>
            <th>Approved Date</th>
            <th>Approved By</th>
            <th>PO No.</th>
            <th>Vendor Name</th>
            <th>Net Amount</th>
            <th>Status</th>
            <th><i class="fa fa-info"></i></th>
            <th><i class="fa fa-edit"></i></th>
        </tr>
    </thead>
    <tbody>
        @if (count($item) > 0)
            @foreach ($item as $each)
                <?php

                $status = '';
                $checked='';
                if ($each->approve_status == 1) {
                    $status = 'Not Approved';
                } elseif ($each->approve_status == 2) {
                    $status = 'Approved';
                } elseif ($each->approve_status == 3) {
                    $status = 'Cancelled';
                } elseif ($each->approve_status == 4) {
                    $status = 'Closed';
                }
                if($each->accounts_receive_status==1){
                    $checked = 'checked';
                }
                ?>

                <tr>
                    <td style="text-align: left;">{{ $each->grn_no }}</td>
                    <td style="text-align: left;">{{ $each->bill_no }}</td>
                    <td style="text-align: left;">{{ $each->location }}</td>
                    <td style="text-align: left;" title="{{ $each->created_at }}">
                        {{ $each->created_at }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->created_by }}">
                        {{ $each->created_by }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->approved_at }}">
                        {{ $each->approved_at }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->approved_by }}">
                        {{ $each->approved_by }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->po_no }}">
                        @if (!empty($each->po_no))
                            {{ $each->po_no }}
                        @else
                            --
                        @endif
                    </td>
                    <td style="text-align: left;" title="{{ $each->vendor_name }}">
                        {{ $each->vendor_name }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->net_amount }}">
                        {{ $each->net_amount }}
                    </td>
                    <td style="text-align: left;" title="{{ $status }}">
                        {{ $status }}
                    </td>
                    <td style="text-align: center;">
                        <button title="GRN View" onclick="grn_listview(1,'{{ $each->bill_id }}')"
                            id="grn_listviewbtn{{ $each->bill_id }}" type="button" class="btn btn-info"><i
                                id="grn_listviewspin{{ $each->bill_id }}" class="fa fa-info"></i></button>
                    </td>
                    <td style="text-align: center;">
                        @if ($status != 'Cancelled')
                            <input type="checkbox" {{ $checked }} class="grn_receicved" id="{{ $each->bill_id   }}"value="{{ $each->bill_id }}">
                        @endif
                    </td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="12" class="location_code">No Records found</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination">
        {!! $page_links !!}
    </ul>
</div>
