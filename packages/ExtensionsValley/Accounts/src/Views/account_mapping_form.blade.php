@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.accounts.listAccountMapping')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Ledger Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="ledger"
                                           value="{{ $searchFields['ledger'] ?? '' }}">
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Speciality</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ $searchFields['speciality'] ?? '' }}"  name="speciality" autocomplete="off" id="speciality"
                                           class="form-control value"
                                           placeholder="Mapping Value" onkeyup="searchMapValue(this.id, event)">
                                    <div class="ajaxSearchBox" id="value_div" style=""></div>
                                    <input type="hidden" value="{{ $searchFields['value_id'] ?? '' }}" name="value_id" id="value_id">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Ledger Name</th>
                                    <th>Speciality</th>
                                    <th>Department</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                @foreach ($list as $list)
                                <tr style="cursor: pointer;" onclick="editLoadData(this,
                                    '{{$list->id}}',
                                    '{{$list->ledger_id}}',
                                    '{{$list->ledger_name}}',
                                    '{{$list->dept_name}}',
                                    '{{$list->dept_id}}',
                                    '{{$list->speciality}}',
                                    '{{$list->speciality_id}}');" >
                                    <td class="ledger_id common_td_rules">{{ $list->ledger_name }}</td>
                                    <td class="mapping_type common_td_rules">{{ $list->speciality }}</td>
                                    <td class="mapping_value common_td_rules">{{ $list->dept_name }}</td>

                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5" class="tax_code">No Records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.accounts.saveAccountMapping')}}" method="POST" id="mapForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="ac_mapping_id" id="ac_mapping_id" value="0">
                        
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Speciality Name<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="text" name="spec_name" autocomplete="off" id="spec_name"
                                       class="form-control fav_grp"
                                       placeholder="Speciality Name" required onkeyup="searchSpeciality(this.id, event)">
                                <div class="ajaxSearchBox" id="spec_id_div" style="z-index:999"></div>
                                <input type="hidden" name="spec_id" id="spec_id">
                                <span class="error_red">{{ $errors->first('spec_name') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Department Name<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="text" name="dept_name" autocomplete="off" id="dept_name"
                                       class="form-control fav_grp"
                                       placeholder="Department Name" required onkeyup="searchDepartment(this.id, event)">
                                <div class="ajaxSearchBox" id="dept_id_div" style="z-index:999"></div>
                                <input type="hidden" name="dept_id" id="dept_id">
                                <span class="error_red">{{ $errors->first('dept_name') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Ledger Name<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="text" name="ledger_name" autocomplete="off" id="ledger_name"
                                       class="form-control fav_grp"
                                       placeholder="Ledger Name" required onkeyup="searchLedger(this.id, event)">
                                <div class="ajaxSearchBox" id="ledger_div" style="z-index:999"></div>
                                <input type="hidden" name="ledger_id" id="ledger_id">
                                <span class="error_red">{{ $errors->first('ledger_name') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
                                           $(document).ready(function () {
                                               var $table = $('table.theadfix_wrapper');

                                               $table.floatThead({
                                                   scrollContainer: function ($table) {
                                                       return $table.closest('.theadscroll');
                                                   }

                                               });

                                               $('.theadscroll').perfectScrollbar({
                                                   wheelPropagation: true,
                                                   minScrollbarLength: 30
                                               });


                                               $('.fixed_header').floatThead({
                                                   position: 'absolute',
                                                   scrollContainer: true
                                               });

                                           });

                                           function fillLedgerDetails(e, id, name) {
                                               $('#ledger_name').val(name);
                                               $('#ledger_id').val(id);
                                               $('#ledger_div').hide();
                                           }
                                           function searchLedger(id, event) {
                                               var keycheck = /[a-zA-Z0-9 ]/;
                                               var value = event.key;
                                               var ajax_div = 'ledger_div';

                                               if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                                                   var group = $('#' + id).val();
                                                   if (group == "") {
                                                       $("#" + ajax_div).html("");
                                                   } else {
                                                       $.ajax({
                                                           type: "GET",
                                                           url: "",
                                                           data: 'ledger_name=' + group,
                                                           beforeSend: function () {
                                                               $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                                                           },
                                                           success: function (html) {
                                                               $("#" + ajax_div).html(html).show();
                                                               $("#" + ajax_div).find('li').first().addClass('liHover');
                                                           },
                                                           complete: function () {
                                                               $('.theadscroll').perfectScrollbar("update");
                                                               $(".theadfix_wrapper").floatThead('reflow');
                                                           }
                                                       });
                                                   }

                                               } else {
                                                   ajaxProgressiveKeyUpDown(ajax_div, event);
                                               }
                                           }

                                           function searchSpeciality(id, event) {
                                               var keycheck = /[a-zA-Z0-9 ]/;
                                               var value = event.key;
                                               var ajax_div = 'spec_id_div';

                                               if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                                                   var group = $('#' + id).val();
                                                   if (group == "") {
                                                       $("#" + ajax_div).html("");
                                                   } else {
                                                       $.ajax({
                                                           type: "GET",
                                                           url: "",
                                                           data: 'spec_name=' + group,
                                                           beforeSend: function () {
                                                               $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                                                           },
                                                           success: function (html) {
                                                               $("#" + ajax_div).html(html).show();
                                                               $("#" + ajax_div).find('li').first().addClass('liHover');
                                                           },
                                                           complete: function () {
                                                               $('.theadscroll').perfectScrollbar("update");
                                                               $(".theadfix_wrapper").floatThead('reflow');
                                                           }
                                                       });
                                                   }

                                               } else {
                                                   ajaxProgressiveKeyUpDown(ajax_div, event);
                                               }
                                           }

                                           function fillSpecialityValues(e, id, name) {
                                               $('#spec_name').val(name);
                                               $('#spec_id').val(id);
                                               $('#spec_id_div').hide();
                                           }
                                           function searchDepartment(id, event) {
                                               var keycheck = /[a-zA-Z0-9 ]/;
                                               var value = event.key;
                                               var ajax_div = 'dept_id_div';

                                               if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                                                   var group = $('#' + id).val();
                                                   if (group == "") {
                                                       $("#" + ajax_div).html("");
                                                   } else {
                                                       $.ajax({
                                                           type: "GET",
                                                           url: "",
                                                           data: 'dept_name=' + group,
                                                           beforeSend: function () {
                                                               $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                                                           },
                                                           success: function (html) {
                                                               $("#" + ajax_div).html(html).show();
                                                               $("#" + ajax_div).find('li').first().addClass('liHover');
                                                           },
                                                           complete: function () {
                                                               $('.theadscroll').perfectScrollbar("update");
                                                               $(".theadfix_wrapper").floatThead('reflow');
                                                           }
                                                       });
                                                   }

                                               } else {
                                                   ajaxProgressiveKeyUpDown(ajax_div, event);
                                               }
                                           }

                                           function fillDepartmentValues(e, id, name) {
                                               $('#dept_name').val(name);
                                               $('#dept_id').val(id);
                                               $('#dept_id_div').hide();
                                           }

                                           function editLoadData(obj, id, ledger_id, ledger_name, dept_name, dept_id, speciality, speciality_id) {
                                               $('#ac_mapping_id').val(id);
                                               $('#ledger_id').val(ledger_id);
                                               $('#ledger_name').val(ledger_name);
                                               $('#spec_name').val(speciality);
                                               $('#spec_id').val(speciality_id);
                                               $('#dept_name').val(dept_name);
                                               $('#dept_id').val(dept_id);
                                               $('#mapForm').attr('action', '{{$updateUrl}}');
                                           }
                                           function resetFunction(){
                                           $('#mapForm')[0].reset();
                                           }
                                           @include('Purchase::messagetemplate')
</script>

@endsection
