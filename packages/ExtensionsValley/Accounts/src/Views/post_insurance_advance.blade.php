@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-2 padding_sm pull-right">
                <?= $title ?>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token" value="{{ csrf_token() }}">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">                    
                    <div class="col-md-2 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label>Date</label>
                            <div class="clearfix"></div>
                            <input type="text" value="<?= $current_date ?>" autocomplete="off" id="servieDate"
                                class="form-control datepicker" placeholder="Service Date">
                        </div>
                    </div>

                    <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="processInsuranceData()" id="processdatabtn"
                            class="btn btn-primary btn-block">Process <i id="processdataspin"
                                class="fa fa-list"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm pull-right" style="margin-top: 25px;">
                        <button type="button" onclick="reloadData()" class="btn btn-warning btn-block">Cancel <i
                            class="fa fa-times"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm pull-right" style="margin-top: 25px;">
                        <button type="button" onclick="revertData()" class="btn btn-danger btn-block">Revert <i
                                id="revertbtn" class="fa fa-reply"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="row padding_sm" style="min-height: 450px;">
                <div class="col-md-12 padding_sm" id="postdatadiv" style="">

                </div>
            </div>
        </div>

    </div>
@stop

@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/accounts/default/javascript/postInsurance.js') }}">
    </script>
@endsection
