
@if (count($VendorDetails) > 0)
<div class="col-md-12">
    <div class="col-md-12"><h4 class="blue" align="center" style="margin-bottom: 5px;margin-top:-10px;margin-left: 20px;">Payment Details</h4></div>
    <div class="col-md-6" style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 20px;"><label for=""style="font-weight:700;font-size:15px;">Ledger Name</label><label for="" style="font-size:15px">:{{$VendorDetails[0]->ledger_name}}</label></div>
    <div class="col-md-5" style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 28px;"><label for=""style="font-weight:700;font-size:15px;">Invoice Date</label><label for="" style="font-size:15px">:{{$VendorDetails[0]->invoice_date}}</label></div>
    <div class="col-md-6" style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 20px;"><label for=""style="font-weight:700;font-size:15px;">Amount</label><label for="" style="font-size:15px">:{{@$VendorDetails[0]->amount ? $VendorDetails[0]->amount : ''}}</label></div>
    <div class="col-md-5" style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 28px;"><label for=""style="font-weight:700;font-size:15px;">Remitter Bank</label><label for="" style="font-size:15px">:{{@$VendorDetails[0]->sender_bank_name ? $VendorDetails[0]->sender_bank_name : ''}}</label></div>
    <div class="col-md-6" style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 20px;"><label for=""style="font-weight:700;font-size:15px;">Beneficiary Bank</label><label for="" style="font-size:15px">:{{@$VendorDetails[0]->beneficiary_bank_name ? $VendorDetails[0]->beneficiary_bank_name : ''}}</label></div>
     @if($VendorDetails[0]->type_of_payment == 1)
         @php
             $payment_type='NEFT';
         @endphp
     @elseif($VendorDetails[0]->type_of_payment == 2)
     @php
     $payment_type='IMPS';
     @endphp
     @else
     @php
     $payment_type='';
     @endphp
     @endif
     
    <div class="col-md-5" style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 28px"><label for=""style="font-weight:700;font-size:15px;">Payment Type</label><label for="" style="font-size:15px">:{{$payment_type}}</label></div>
    <div class="clearfix"></div>
    <div class="col-md-10"style="margin-bottom:10px;border: 2px dotted #a52a2a26;padding: 5px 0px 0px 17px;margin-left: 20px;"><label for=""style="font-weight:700;font-size:15px;">Invoice</label><label for="" style="font-size:15px">:{{$VendorDetails[0]->invoice}}</label></div>
   
</div>
<img src="{{URL::to("/")}}./packages/uploads/Purchase/paid_image.png" style="margin-top:-125px;float: right;" alt="Girl in a jacket" width="200" height="200">

@endif