<input type="hidden" name="voucher_type_id" id="voucher_type_id" value="{{$voucher_type_id}}">
<input type="hidden" name="detail_cr_top" id="detail_cr_top" value="{{$detail_cr}}" class="detail_cr_top">
<input type="hidden" name="exp_inc_type" id="exp_inc_type" value="<?= $exp_inc_type ?>">
<?php
$bank_voucher_generated = \DB::table('config_detail')->where('name','bank_voucher_generated')->value('value');
if($bank_voucher_generated==1){
    $fn_name = 'select_item_desc_voucher';
}else{
    $fn_name = 'select_item_desc';
}
?>
<input type="hidden" name="bank_voucher_generated" id="bank_voucher_generated" value="<?= $bank_voucher_generated ?>">
<div class="col-md-3 padding_sm">
    <div class="mate-input-box">
        <label>Account Name <span style="color: red">*</span></label>
        <input type="text" required="" autocomplete="off"
               onkeyup="<?= $fn_name ?>(this.id, event,'{{$voucher_type_id}}','1')" id="ledger_item_desc-0"
               class="form-control auto_focus_cls" name="party[]"
               placeholder="Search Particular">
        <div class='ajaxSearchBox' id="search_ledger_item_box-0" index='1'
             style="margin-top: 14px !important;width: 470px !important;max-height: 290px;"> </div>
        <input type='hidden' name='ledger_id_head' value="" id="ledger_id_head-0">
        <input type='hidden' name='ledger_group_head' value="" id="ledger_group_head-0">
    </div>
</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box" style="height: 45px !important">
        <label>Curr.Balance</label>
        <div class="clearfix"></div>
        <span id="ledger_code"> </span>
    </div>
</div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Amount<span style="color: red">*</span></label>
        <div class="clearfix"></div>
        <input type="text" value="" id="amount_header" name="amount_header" autocomplete="off" onkeyup="number_validation(this)" onblur="amount_fetch(this); calculate_table_total('amount')"   class="form-control">
    </div>
</div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Date</label>
        <div class="clearfix"></div>
        <input type="text" value="<?= date('M-d-Y'); ?>" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
    </div>
</div>
 <div class="col-md-1 padding_sm">
            <div class="mate-input-box">
                <label>CDV No</label>
                <input type="text" autocomplete="off" id="cdv_no" class="form-control" name="cdv_no" placeholder="CDV">
            </div>
    </div>
    <div class="col-md-1 padding_sm">
            <div class="mate-input-box">
                <label>Cheque No</label>
                <input type="text" autocomplete="off" id="check_no" class="form-control" name="check_no" placeholder="Cheque No">
            </div>
    </div>
    <div class="col-md-1 padding_sm" >
            <div class="mate-input-box">
                <label>Cheque Date</label>
                <input type="text" autocomplete="off" id="check_dt" class="form-control datepicker" name="check_dt" placeholder="Cheque Date">
            </div>
    </div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label for="">Dr/Cr.</label>
            <div class="clearfix"></div>
                @if($voucher_type_id == 18)
                {!! Form::select('header_cr', array( "dr"=> " Debit"),'',
                     [
                         'class'       => 'form-control',
                         'id'          => 'header_cr', 'onchange' => 'drCrChange(this.value)'
                    ]) !!}
                @else
                 {!! Form::select('header_cr', array( "cr"=> " Credit","dr"=> " Debit"),'',
                     [
                         'class'       => 'form-control',
                         'id'          => 'header_cr', 'onchange' => 'drCrChange(this.value)'
                    ]) !!}
                @endif
            </div>
</div>
<div class="col-md-1 padding_sm">
    <div class="mate-input-box">
        <label>Int.Ref.No</label>
        <div class="clearfix"></div>
        <input type="text" value="" id="refer_no" name="refer_no" autocomplete="off" class="form-control">
    </div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-md-2 padding_sm  pull-right" style=" padding-top: 10px !important">
    <label for=""style="float:right">Voucher No : <span style="color: red" id="generated_vouchr_label"><b><?= @$voucher_no ? $voucher_no:'' ?></b></span></label>
    <input type="hidden" value="<?= @$voucher_no ? $voucher_no:'' ?>" name="generated_vouchr_text" id="generated_vouchr_text">
    <div class="clearfix">
    </div>
</div>

@if($voucher_type_id == 12)
<div class="col-md-1 padding_sm" style="margin-top: 15px;">
    <div class="checkbox checkbox-success inline no-margin">
        <input type="checkbox" class="form-control filters" name="advance_from_party"  id="advance_from_party" value="1">
        <label class="text-blue " for="advance_from_party">Advance</label>
    </div>
</div>
@endif
