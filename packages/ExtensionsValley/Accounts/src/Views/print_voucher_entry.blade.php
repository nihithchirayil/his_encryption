<table class="table no-margin theadfix_wrapper table-striped ">
    <thead>
        <tr>
            <td colspan="2">{{@$data['ledger_name'] ? $data['ledger_name']:''}}</d>
        </tr>
        <tr>
            <td style="text-align: left;width:20%">Amount</td>
            <td style="text-align: left;width:30%">{{@$data['amount'] ? $data['amount']:''}}</td>

            <td style="text-align: left;width:20%">Dr/Cr</td>
            <td style="text-align: left;width:30%">{{@$data['cr_dr'] ? $data['cr_dr']:''}}</td>
            
        </tr>
         @if($data['invoice_no'] != '')
        <tr>
            <td style="text-align: left;width:20%">Invoice No</td>
            <td style="text-align: left;width:30%">{{@$data['invoice_no'] ? $data['invoice_no']:''}}</td>
            
            <td style="text-align: left;width:20%">Invoice Date</td>
            <td style="text-align: left;width:30%">{{@$data['invoice_date'] ? $data['invoice_date']:''}}</td>

        </tr>
        @endif
        @if($data['cheq_no'] != '')
        <tr>
            <td style="text-align: left;width:20%">Cheque No</td>
            <td style="text-align: left;width:30%">{{@$data['cheq_no'] ? $data['cheq_no']:''}}</td>

            <td style="text-align: left;width:20%">Cheque date</td>
            <td style="text-align: left;width:30%">{{@$data['cheq_dt'] ? $data['cheq_dt']:''}}</td>
        </tr>
        <tr>
            <td style="text-align: left;width:20%">CDV</td>
            <td style="text-align: left;width:30%">{{@$data['cdv'] ? $data['cdv']:''}}</td>
        </tr>
        @endif
        <tr>
            <td style="text-align: left">Voucher No</td>
            <td>{{@$data['voucher_no'] ? $data['voucher_no']:''}}</td>
        </tr>
    </thead>
</table> 
<div class="clearfix"></div>
<div class="col-md-12">
    <table class="table" style="border: 1px solid #cccccc;">
        <thead>
            <tr >
                <th style="border-bottom: 1px solid #cccccc;width:50%">Account Name</th>
                <th style="border-bottom: 1px solid #cccccc;width:25%">Amount</th>
                <th style="border-bottom: 1px solid #cccccc;width:25%">Cr/Dr</th>
            </tr>
        </thead>
        <tbody id="">
            @php $ttl_amnt = 0; @endphp
            @foreach($datas as $dt)
            <tr id="">
                <td class="common_td_rules" style="border-right: 1px solid #cccccc;">{{@$dt['ledger_name'] ? $dt['ledger_name'] : ''}}</td>
                <td class="td_common_numeric_rules" style="border-right: 1px solid #cccccc;padding-left: 20px;text-align: right">{{@$dt['amount'] ? $dt['amount'] : ''}}</td>
                <td style="text-align:center;">{{@$dt['cr_dr'] ? $dt['cr_dr'] : ''}}</td>
            </tr>
            @if($dt['amount'])
            @php $amounts = $dt['amount']; @endphp
            @else
            @php $amounts = 0; @endphp
            @endif
            @php $ttl_amnt += $amounts; @endphp
            @endforeach
            <tr>
                <td class="common_td_rules" style="border-top: 1px solid #cccccc;border-right: 1px solid #cccccc;"><b>TOTAL</b></td>
                <td style="border-top: 1px solid #cccccc;border-right: 1px solid #cccccc;padding-left: 20px;text-align: right"><b>{{$ttl_amnt}}</b></td>
                <td style="border-top: 1px solid #cccccc;"></td>
            </tr>
        </tbody>
    </table>
    <div class="clearfix"></div>
    <div class="ht10"></div>
    <div class="col-md-8 padding_sm" style="padding-top: 15%">
        <div class="mate-input-box">
            <label>Narration: {{@$dt['naration']??''}}</label>
            <div class="clearfix"></div>
        </div>
    </div>
</div>