@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')

    <style>
        .table_header_bg{
            background: #2b6ea3 !important;
        }
        .mate-input-box{
            border-bottom: 2px solid #2b6ea3;
        }
        .particular_item{
            cursor: pointer;
        }
        .particular_item:hover{
            background: #ebebeb !important;
        }
        .activeItem{
            background: #75ff79 !important;
        }
        .activeItem:hover{
            background: #75ff79 !important;
        }
        .common_td_rules{
            text-align: left !important;
            overflow: hidden !important;
            border-right: solid 1px #bbd2bd !important;
            border-left: solid 1px #bbd2bd !important;
            max-width: 100px;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size:11px !important;
        }
        .match_found_div{
            display:none;
            width: 50%;
            border: 1px solid green;
            padding: 10px;
            position: absolute;
            background: #f5f5dcad;
            text-align: center;
            font-size: 25px;
            font-weight: 600;
            top: 50%;
            left: 25%;
        }
        .no_stmt_found_div{
            display:none;
            width: 50%;
            border: 1px solid red;
            padding: 10px;
            position: absolute;
            background: #f5f5dcad;
            text-align: center;
            font-size: 25px;
            font-weight: 600;
            top: 50%;
            left: 25%;
        }
        .blink_me {
            animation: blinker 0.1s linear infinite;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }
        .reconceiled_class{
            display:none;
            color: #006cad;
            font-weight: 600;

        }
        .custom_modal_header{
            background-color: #456aad;
        }
        .mark_as_missmatch_btn{
            padding: 3px 10px;
            border-radius: 27px;
            font-size: 20px;
        }
        .mark_as_match_btn{
            padding: 3px 10px;
            border-radius: 27px;
            font-size: 20px;
        }
    </style>
@endsection
@section('content-area')

    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="head_id" value="0">
    <!-- page content -->
    <div class="right_col">
        <div class="row codfox_container">
            <h5 style="margin: 0px !important" class="blue pull-right"><strong> <?= $title ?> </strong></h4>
                <div class="clearfix"></div>
                <div class="col-md-6 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #2378a0; border: 2px solid #2378a0;min-height: 10vh;">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-11 no-padding">
                                    <div class="col-md-3 padding_sm ml">
                                        <div class="mate-input-box">
                                            <label for="">Bank</label>
                                            <div class="clearfix"></div>
                                            {!! Form::select('ledger_bank_id', ['' => 'Select Bank'] + $bank_list->toArray(), null, [
                                                'class' => 'form-control',
                                                'id' => 'ledger_bank_id',
                                                'style' => ' color:#555555; padding:4px 12px;',
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm ml">
                                        <div class="mate-input-box">
                                            <label for="">From Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" id="from_date" class="form-control datepicker" name="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm ml">
                                        <div class="mate-input-box">
                                            <label for="">To Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" id="to_date" class="form-control datepicker" name="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm ml">
                                        <div class="mate-input-box">
                                            <label for="">Voucher Type</label>
                                            <div class="clearfix"></div>
                                            {!! Form::select('voucher_type', ['' => 'Voucher Type'] + $voucher_types->toArray(), null, [
                                                'class' => 'form-control',
                                                'id' => 'voucher_type',
                                                'style' => ' color:#555555; padding:4px 12px;',
                                            ]) !!}
                                        </div>
                                    </div>

                                    
                                </div>
                                <div class="col-md-1 no-padding">
                                    <div class="col-md-12 padding_sm">
                                        <button type="button" id="show_hide_recon_entries_btn" class="btn btn-primary btn-block"><i class="fa fa-eye"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 padding_sm">
                                        <button type="button" id="searchBtn" class="btn btn-primary btn-block"><i id="searchSpin" class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box no-border no-margin" style="margin-top: 10px !important">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #2378a0; border: 2px solid #2378a0;min-height: 60vh;">
                            <div id="searchDataDiv">
                                <div class="theadscroll" style="position: relative;  height:60vh">
                                    <table id="leger_details_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                                        style="border: 1px solid #CCC;">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th width="15%" >Voucher No</th>
                                                <th width="15%" >Voucher Type</th>
                                                <th width="40%" >Particular</th>
                                                <th width="15%" >Debit/Credit</th>
                                                <th width="15%" >Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" style="text-align: center">No Records found</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding_sm" id="patientappoinment_div">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #2378a0; border: 2px solid #2378a0;min-height: 10vh;">
                            <div class="col-md-12 padding_sm">
                                <form id="uploadBankStatement" >
                                    <div class="col-md-4 padding_sm ml">
                                        <div class="mate-input-box">
                                            <label for=""></label>
                                            <div class="clearfix"></div>
                                            <input type="file" autocomplete="off" id="uploadExcelID" class="form-control" name="uploadExcelID" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-2  padding_sm">
                                        <label for="">&nbsp;</label>
                                        <button type="submit" id="uploadExcelButton" class="btn btn-primary btn-block"><i id="uploadExcelButtonSpin" class="fa fa-upload"></i> Upload</button>
                                    </div>

                                    
                                    <div class="col-md-3 pull-right  padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Filter Items <i class="fa fa-filter"></i> </label>
                                            <div class="clearfix"></div>
                                            <select class="form-control filter_stmt_items">
                                                <option value="">Credit and Debit</option>
                                                <option value="cr">Credit</option>
                                                <option value="dr">Debit</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 pull-right  padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Show All </label>
                                            <div class="clearfix"></div>
                                            <input type="checkbox" class="show_all_statement_items"  />
                                        </div>
                                    </div>
                                </form>
                               
                            </div>
                        </div>
                    </div>
                    <div class="box no-border no-margin" style="margin-top: 10px !important">
                        <div class="box-footer" style="padding: 5px; box-shadow: 0px 1px #2378a0; border: 2px solid #2378a0;min-height: 60vh;">
                            <div id="excelUploadData">
                                <div class="theadscroll" style="position: relative; height:60vh">
                                    <table id="bank_stmt_detais_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                                        style="border: 1px solid #CCC;">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th width="50%" >Particular</th>
                                                <th width="20%" >Date</th>
                                                <th width="15%" >Debit/Credit</th>
                                                <th width="15%" >Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4" style="text-align: center">No Records found</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-md-6 padding_sm" style="margin-top:10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #2378a0; border: 2px solid #2378a0;min-height: 10vh;">
                            <div class="col-md-12 padding_sm">

                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Total Ledger Amount</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control datepicker total_ledger_amount" readonly disabled name="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Total Statement Amount</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control datepicker total_statement_amount" readonly disabled name="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Balance Amount</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control datepicker balance_amount" readonly disabled name="" placeholder="">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 padding_sm" style="margin-top:10px;">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-2 padding_sm">
                            <button type="button" class="btn btn-sm btn-warning auto_reconcile_btn" title="Auto Reconcile" style="padding: 5px;font-size: 16px;"><i style="font-size: 20px;" class="fa fa-cogs"></i> Auto Reconcile</button>
                        </div>
                        <div class="col-md-1 col-md-offset-2 padding_sm">
                            <button type="button" class="btn btn-sm btn-primary mark_as_match_btn" title="Mark as Match"><i class="fa fa-thumbs-up"></i></button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <button type="button" class="btn btn-sm btn-danger mark_as_missmatch_btn" title="Mark as Missmatch"><i class="fa fa-thumbs-down"></i></button>
                        </div>
                        <div class="col-md-3 pull-right padding_sm">
                            <button type="button" class="btn btn-sm btn-block btn-primary show_reconciliation_summary_btn"><i class="fa fa-list"></i> Show Summary</button>
                        </div>
                    </div>
                </div>
                

                
        </div>
    </div>

    <div class="match_found_div">
        <span style="color:green; " class="blink_me">Matched!</span>
    </div>
    <div class="no_stmt_found_div">
        <span style="color:red; " class="blink_me">No bank statement available!</span>
    </div>

     <div class="modal" tabindex="-1" role="dialog" id="show_summary_modal">
        <div class="modal-dialog" role="document" style="width: 91%; left: 2%;">
            <div class="modal-content">
                <div class="modal-header custom_modal_header">
                    <h4 class="modal-title" style="color: white;">Reconciliation Summary</h4>
                    <button type="button" class="close print_config_close_btn" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body show_summary_modal_body" style="height:535px;">
                    
                </div>
            </div>
        </div>
    </div>
     
    <div class="modal" tabindex="-1" role="dialog" id="select_ledger_head_entry">
        <div class="modal-dialog" role="document" style="width: 45%; left: 2%;">
            <div class="modal-content">
                <div class="modal-header custom_modal_header">
                    <h4 class="modal-title" style="color: white;">Reconciliation List</h4>
                </div>
                <div class="modal-body select_ledger_head_entry_body" style="height:290px;">
                <table class="table no-margin table_sm table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th width="35%" >From Date</th>
                            <th width="35%" >To Date</th>
                            <th width="30%" >Create By</th>
                        </tr>
                    </thead>
                    <tbody class="select_ledger_head_entry_table_body">
                        <tr>
                            <td colspan="3" style="text-align: center">No Records found</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>



    <!-- /page content -->

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/js/reconciliation.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
@endsection
