<div class="theadscroll" style="position: relative;  height:60vh">
    <table id="leger_details_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="15%" >Voucher No</th>
                <th width="10%" >Voucher Type</th>
                <th width="10%" >Date</th>
                <th width="35%" >Particular</th>
                <th width="15%" >Debit/Credit</th>
                <th width="15%" >Amount</th>
            </tr>
        </thead>
        <tbody>
            @if(count($ledger_list) > 0)
            @foreach($ledger_list as $ledger)
            <tr class="ledger_items particular_item @if($ledger->reconciliation_status == 1) reconceiled_class @endif " data-ledger-id="{{$ledger->id}}" data-voucher-type="{{$ledger->type}}" data-voucher-no="{{$ledger->voucher_no}}" data-amount="{{$ledger->amount}}" data-cr-dr="{{$ledger->cr_dr}}">
                <td class="common_td_rules">{{$ledger->voucher_no}}</td>
                <td class="common_td_rules">{{$ledger->type}}</td>
                <td class="common_td_rules">{{date('M-d-Y', strtotime($ledger->invoice_date))}}</td>
                <td class="common_td_rules ledger_name" title="{{$ledger->naration}}">{{$ledger->ledger_name}}</td>
                <td class="common_td_rules">@if($ledger->cr_dr == "cr") Credit @else Debit @endif</td>
                <td class="common_td_rules"><strong>{{$ledger->amount}}</strong></td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="5" style="text-align: center">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>