@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<style>

.search_header{
    background: #2b6ea3 !important;
    color: #FFF !important;
}
.ajaxSearchBox{
    z-index: 9999999;
}
.mate-input-box{
    border-bottom: 2px solid #2b6ea3;
}
.table_header_bg{
    background: #2b6ea3 !important;
}

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_header" value="{{$hospital_header}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">
                                <span class="padding_sm">{{$title}}</span>
                            </div>
                            <div class="box-body clearfix">
                                <div class="col-md-2 padding_sm ml">
                                    <div class="mate-input-box">
                                        <label for="">Bank</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('ledger_bank_id', ['' => 'Select Bank'] + $bank_list->toArray(), null, [
                                            'class' => 'form-control',
                                            'id' => 'ledger_bank_id',
                                            'style' => ' color:#555555; padding:4px 12px;',
                                        ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" id="from_date" class="form-control datepicker" name="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" id="to_date" class="form-control datepicker" name="" placeholder="">
                                    </div>
                                </div>

                                <div class="col-md-1  padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary getReport"><i class="fa fa-search"></i> Get Report</button>
                                </div>
                                <div class="col-md-1  padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary printReport"><i class="fa fa-print"></i> Print Report</button>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ht10"></div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box no-border no-margin " id="reconciliationReportDataDiv">


                    </div>


                </div>
            </div>

        </div>
    </div>
</div>


<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate('reconciliationReportDataDiv')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/reconciliation_report.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
