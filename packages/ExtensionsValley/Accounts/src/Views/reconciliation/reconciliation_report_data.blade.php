<div class="" style="position: relative;  height:60vh">
    <table id="leger_details_table" class="table no-margin table_sm  table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%"> Sl.No.</th>
                <th width="15%" >Invoice Date</th>
                <th width="15%" >Voucher No</th>
                <th width="50%" >Particulars</th>
                <th width="15%" >Amount</th>
            </tr>
        </thead>
        <tbody>
            @if(count($receipts_not_reconceiled) > 0 && count($payments_not_reconceiled) > 0)
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">BALANCE AS PER BANK STATEMENT</td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">{{$bank_balance_data}}</td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            @endif
            @if(count($payments_not_reconceiled) > 0)
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">Cheque Issued But not Cleared - Annexure 1</td>
                <td class="common_td_rules"></td>
            </tr>
            @foreach($payments_not_reconceiled as $ledger)
            <tr class="ledger_items particular_item">
                <td class="common_td_rules">{{$loop->iteration}}</td>
                <td class="common_td_rules">{{date('M-d-Y', strtotime($ledger->invoice_date))}}</td>
                <td class="common_td_rules">{{$ledger->voucher_no}}</td>
                <td class="common_td_rules" title="{{$ledger->naration}}">{{$ledger->ledger_name}}</td>
                <td class="common_td_rules"><strong>{{$ledger->amount}}</strong></td>
            </tr>
            @endforeach
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">Total Amount</td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600; color:red;"><strong>{{$payments_not_reconceiled[0]->total}}</strong></td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            @endif

            @if(count($receipts_not_reconceiled) > 0)
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">Cheque received but not credited - Annexure 2</td>
                <td class="common_td_rules"></td>
            </tr>
            @foreach($receipts_not_reconceiled as $ledger)
            <tr class="ledger_items particular_item">
                <td class="common_td_rules">{{$loop->iteration}}</td>
                <td class="common_td_rules">{{date('M-d-Y', strtotime($ledger->invoice_date))}}</td>
                <td class="common_td_rules">{{$ledger->voucher_no}}</td>
                <td class="common_td_rules" title="{{$ledger->naration}}">{{$ledger->ledger_name}}</td>
                <td class="common_td_rules"><strong>{{$ledger->amount}}</strong></td>
            </tr>
            @endforeach
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">Total Amount</td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600; color:red;" ><strong>{{$receipts_not_reconceiled[0]->total}}</strong></td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            @endif
            @if(count($receipts_not_reconceiled) > 0 && count($payments_not_reconceiled) > 0)
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">BALANCE AS PER LEDGER</td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">{{($bank_balance_data) + ($receipts_not_reconceiled[0]->total) - ($payments_not_reconceiled[0]->total)}}</td>
            </tr>
            @endif
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">
                    @if(count($credit_stmt_bal_data) > 0 )
                        {{$credit_stmt_bal_data[0]->total}}
                    @else
                    0.00
                    @endif
                </td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules" colspan="5">&nbsp;</td>
            </tr>
            <tr class="ledger_items particular_item">
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules"></td>
                <td class="common_td_rules" style="font-size: 14px !important; font-weight: 600;">
                    @if(count($credit_stmt_bal_data) > 0 )
                    {{(($bank_balance_data) + ($receipts_not_reconceiled[0]->total) - ($payments_not_reconceiled[0]->total))-$credit_stmt_bal_data[0]->total}}
                    @else
                    {{($bank_balance_data) + ($receipts_not_reconceiled[0]->total) - ($payments_not_reconceiled[0]->total)}}
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</div>