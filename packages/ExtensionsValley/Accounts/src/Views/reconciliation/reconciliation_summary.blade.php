@php
$total_receipt_amount = 0;
$total_payment_amount = 0;
$total_bank_payment_amount = 0;
$total_bank_receipt_amount = 0;
$total_contra_amount = 0;
$total_receipt_amount_bank = 0;
$total_payment_amount_bank = 0;
$total_bank_payment_amount_bank = 0;
$total_bank_receipt_amount_bank = 0;
$total_contra_amount_bank = 0;
@endphp
<div class="col-md-6 padding_sm">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
        <tbody>
            <tr>
                <td style="text-align: left">From Date</td>
                <td style="text-align: left">:</td>
                <td style="text-align: left">{{$from_date}}</td>

            </tr>
            <tr>
                <td style="text-align: left">To Date</td>
                <td style="text-align: left">:</td>
                <td style="text-align: left">{{$to_date}}</td>
            </tr>
            <tr>
                <td style="text-align: left">Ledger Name</td>
                <td style="text-align: left">:</td>
                <td style="text-align: left">{{$ledger_name}}</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-md-6 padding_sm">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
        <tbody>
            <tr>
                <td style="text-align: left">Created By</td>
                <td style="text-align: left">:</td>
                <td style="text-align: left">@if(isset($recon_ledger_details[0])) {{$recon_ledger_details[0]->created_user }} @endif</td>

            </tr>
            <tr>
                <td style="text-align: left">Created On</td>
                <td style="text-align: left">:</td>
                <td style="text-align: left">@if(isset($recon_ledger_details[0])) {{date('M-d-Y', strtotime($recon_ledger_details[0]->created_at)) }} @endif</td>
            </tr>
            <tr>
                <td style="text-align: left">Status</td>
                <td style="text-align: left">:</td>
                <td style="text-align: left">Inprogress</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-6 padding_sm" style="margin-top: 10px;">
    <div class="theadscroll" style="position: relative;  height: 300px; border: 1px solid #524fb0;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="15%" >Voucher No</th>
                    <th width="15%" >Voucher Type</th>
                    <th width="40%" >Particular</th>
                    <th width="15%" >Debit/Credit</th>
                    <th width="15%" >Amount</th>
                </tr>
            </thead>
            <tbody>
                @if(count($recon_ledger_details) > 0)
                @foreach($recon_ledger_details as $ledger)
                @php
                if($ledger->type == "Receipt"){
                    if($ledger->cr_dr == "cr"){
                        $total_receipt_amount = $total_receipt_amount + $ledger->amount;
                    } else {
                        $total_receipt_amount = $total_receipt_amount - $ledger->amount;
                    }
                } else if($ledger->type == "Payment"){
                    if($ledger->cr_dr == "cr"){
                        $total_payment_amount = $total_payment_amount - $ledger->amount;
                    } else {
                        $total_payment_amount = $total_payment_amount + $ledger->amount;
                    }
                } else if($ledger->type == "Journal Bank Payment"){
                    $total_bank_payment_amount = $total_bank_payment_amount + $ledger->amount;
                } else if($ledger->type == "Journal Bank Receipt"){
                    $total_bank_receipt_amount = $total_bank_receipt_amount + $ledger->amount;
                } else if($ledger->type == "Contra"){
                    $total_contra_amount = $total_contra_amount + $ledger->amount;
                }
                @endphp
                <tr class="particular_item">
                    <td class="common_td_rules">{{$ledger->voucher_no}}</td>
                    <td class="common_td_rules">{{$ledger->type}}</td>
                    <td class="common_td_rules ledger_name" title="{{$ledger->naration}}">{{$ledger->ledger_name}}</td>
                    <td class="common_td_rules">@if($ledger->cr_dr == "cr") Credit @else Debit @endif</td>
                    <td class="common_td_rules"><strong>{{$ledger->amount}}</strong></td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" style="text-align: center">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-6 padding_sm" style="margin-top: 10px;">
    <div class="theadscroll" style="position: relative; height: 300px; border: 1px solid #524fb0;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="50%" >Particular</th>
                    <th width="20%" >Date</th>
                    <th width="15%" >Debit/Credit</th>
                    <th width="15%" >Amount</th>
                </tr>
            </thead>
            <tbody>
                @if(count($recon_bank_stmt_details) > 0)
                @foreach($recon_bank_stmt_details as $stmt)
                @php
                if($stmt->type == "Receipt"){
                    $total_receipt_amount_bank = $total_receipt_amount_bank + $stmt->amount;
                } else if($stmt->type == "Payment"){
                    $total_payment_amount_bank = $total_payment_amount_bank + $stmt->amount;
                } else if($stmt->type == "Journal Bank Payment"){
                    $total_bank_payment_amount_bank = $total_bank_payment_amount_bank + $stmt->amount;
                } else if($stmt->type == "Journal Bank Receipt"){
                    $total_bank_receipt_amount_bank = $total_bank_receipt_amount_bank + $stmt->amount;
                } else if($stmt->type == "Contra"){
                    $total_contra_amount_bank = $total_contra_amount_bank + $stmt->amount;
                }
                @endphp
                <tr class="particular_item">
                    <td class="common_td_rules particular_name" title="{{$stmt->particular}}">{{$stmt->particular}}</td>
                    <td class="common_td_rules">{{date('M-d-Y', strtotime($stmt->transaction_date))}}</td>
                    <td class="common_td_rules">@if($stmt->cr_dr == "cr") Credit @else Debit @endif</td>
                    <td class="common_td_rules"><strong>{{$stmt->amount}}</strong></td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="4" style="text-align: center">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>

</div>
<div class="col-md-6 padding_sm" style="margin-top: 10px; ">
    <table class="table no-margin table_sm table-striped table-condensed styled-table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style="text-align: left" width="25%" >Voucher Type</th>
                <th style="text-align: left" width="25%" >Amount</th>
                <th style="text-align: left" width="25%" >Bank</th>
                <th style="text-align: left" width="25%" >Difference</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: left">Receipt</td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_receipt_amount, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_receipt_amount_bank, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)($total_receipt_amount-$total_receipt_amount_bank), 2, '.', '')}}</strong></td>

            </tr>
            <tr>
                <td style="text-align: left">Payment</td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_payment_amount, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_payment_amount_bank, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)($total_payment_amount-$total_payment_amount_bank), 2, '.', '')}}</strong></td>
            </tr>
            <tr>
                <td style="text-align: left">Journal Bank Payment</td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_bank_payment_amount, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_bank_payment_amount_bank, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)($total_bank_payment_amount-$total_bank_payment_amount_bank), 2, '.', '')}}</strong></td>
            </tr>
            <tr>
                <td style="text-align: left">Journal Bank Receipt</td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_bank_receipt_amount, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_bank_receipt_amount_bank, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)($total_bank_receipt_amount-$total_bank_receipt_amount_bank), 2, '.', '')}}</strong></td>
            </tr>
            <tr>
                <td style="text-align: left">Contra</td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_contra_amount, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)$total_contra_amount_bank, 2, '.', '')}}</strong></td>
                <td style="text-align: left"><strong>{{ number_format((float)($total_contra_amount-$total_contra_amount_bank), 2, '.', '')}}</strong></td>
            </tr>
        </tbody>
    </table>
</div>