<div class="theadscroll" style="position: relative; height:60vh">
    <table id="bank_stmt_detais_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="50%" >Particular</th>
                <th width="20%" >Date</th>
                <th width="15%" >Debit/Credit</th>
                <th width="15%" >Amount</th>
            </tr>
        </thead>
        <tbody>
            @if(count($stmt_data) > 0)
            @foreach($stmt_data as $stmt)
            <tr class="stmt_items particular_item @if($stmt->cr_dr == 'cr') cr_class @else dr_class @endif @if($stmt->reconciliation_status == 1) reconceiled_class @endif" data-stmt-id="{{$stmt->id}}" data-amount="{{$stmt->amount}}">
                <td class="common_td_rules particular_name" title="{{$stmt->particular}}">{{$stmt->particular}}</td>
                <td class="common_td_rules">{{date('M-d-Y', strtotime($stmt->transaction_date))}}</td>
                <td class="common_td_rules">@if($stmt->cr_dr == "cr") Credit @else Debit @endif</td>
                <td class="common_td_rules"><strong>{{$stmt->amount}}</strong></td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="4" style="text-align: center">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>