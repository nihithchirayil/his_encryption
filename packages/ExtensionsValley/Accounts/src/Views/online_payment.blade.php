@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<div id="paymentDetailsModal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:60%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title" id="setTheHeader"></h4>
            </div>
            <div class="modal-body" id="paymentDetailsBody" style="height:232px">
               


            </div>
            <div class="modal-footer" id="readyToSave">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="current_date" value="{{ date('M-d-Y')}}">
<?php $company = \DB::table('company')->pluck('sub_code'); ?>
<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
<div class="right_col" >
    <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Account</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search reset" name="ledger_name" id="ledger_name" value="" autocomplete="false">
                                <input type="hidden" class="form-control reset" name="ledger_name_hidden" id="ledger_name_hidden" value="">
                                <div id="AjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important"></div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control datepicker reset" name="from_date" id="from_date" value="{{ date('M-d-Y')}}">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control datepicker reset" name="to_date" id="to_date" value="{{ date('M-d-Y')}}">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchList(20,4)"><i id="searchdataspin" class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a onclick="resetPage()" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
                        <div id="msform">
                            <ul id="progressbar" style="cursor: pointer" class="nav nav-tabs" role="tablist">
                                    <li class="nav-item" id="not_paid">
                                        <a class="nav-link" id="not_paid-tab"
                                            href="#not_paid_tab" role="tab"
                                            aria-controls="not_paid"
                                            aria-selected="true"><strong>Not Paid</strong></a>
                                    </li>
                                    <li class="nav-item" id="requested">
                                        <a class="nav-link" id="requested-tab"
                                            href="#requested_tab" role="tab"
                                            aria-controls="requested"
                                            aria-selected="true"><strong>Proccesing</strong></a>
                                    </li>
                                    <li class="nav-item" id="paid">
                                        <a class="nav-link" id="paid-tab"
                                            href="#paid_tab" role="tab"
                                            aria-controls="paid"
                                            aria-selected="true"><strong>Paid</strong></a>
                                    </li>
                                
                            </ul>
                            <div class="tab-content" id="loadSpin">
                                <div class="tab-pane fade active" id="not_paid_tab" role="tabpanel"
                                aria-labelledby="nav-not_paid-tab">
                                <div class="box no-border no-margin" id="common_list_div">

                                </div>
                                </div>
                                <div class="tab-pane fade active" id="requested_tab" role="tabpanel"
                                aria-labelledby="nav-requested-tab">
                                <div class="box no-border no-margin" id="common_list_div2">

                                </div>
                                </div>
                                <div class="tab-pane fade active" id="paid_tab" role="tabpanel"
                                aria-labelledby="nav-paid-tab">
                                <div class="box no-border no-margin" id="common_list_div1">

                                </div>
                                </div>
                               
                            </div>
                        </div>
           
        </div>

    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script
src="{{ asset('packages/extensionsvalley/emr/js/payment_online.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

@endsection
