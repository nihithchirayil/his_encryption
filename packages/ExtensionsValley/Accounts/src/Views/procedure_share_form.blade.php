@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <input type="hidden" name="base_url" id="base_url" value="{{URL::To('/')}}">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.accounts.listProcedureShare')}}" id="listingSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Doctor</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" style='color:#555555;' name="doctor_name_search" id="doctor_name_search" onchange="selectFormFields()">
                                <option value="0">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                                </div>

                            </div> 
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Service</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" style='color:#555555;' name="service_name_search" id="service_name_search" onchange="selectFormFields()">
                                <option value="0">Select Service</option>
                                   <?php foreach($service_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                                </div>

                            </div> 
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12" style="margin-top:-15px" >
                <div class="box no-border no-margin" id="generilistdiv" style="height: 400px;">
                </div>
            </div>
        </div>
<!--        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix" id="procedure_share_form_whole">
                    <form action="{{route('extensionsvalley.accounts.saveProcedureShare')}}" method="POST" id="procedure_share_form">
                        {!! Form::token() !!}
                        <input type="hidden" name="procedure_share_id" id="procedure_share_id" value="">
                        <div class="clearfix"></div>
                        <div class="ht10"></div>    
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Doctor</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style='color:#555555;' name="doctor_id" id="doctor_id">
                                <option value="">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm" id="subcategory_select">
                            <div class="mate-input-box">
                                <label for="">Service</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style='color:#555555;' name="service_id" id="service_id">
                                <option value="">Select Service</option>
                                   <?php foreach($service_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Percentage</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="percentage" id="percentage" onkeyup="number_validation(this)">
                                <span style="color: #d14;"> {{ $errors->first('percentage') }}</span>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm" id="generic_cancel">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <span class="btn btn-block light_purple_bg" onclick="saveProcedureShare()"><i class="fa fa-save"></i> Save</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>-->
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    });
    function selectFormFields(){
        searchForm();
    }
    function billEditLoadData(id,doctor_id,department_name,percentage){
        $('#procedure_share_id').val(id);
        $('#percentage').val(percentage);
        $('#doctor_id').val(doctor_id).select2();
        $('#service_id').val(department_name).select2();
        $('#procedure_share_form').attr('action', '{{$updateUrl}}');
    }
    
    function searchForm(){
        var url='<?=route('extensionsvalley.accounts.getProcedureShare')?>';
        var doctor_name_search = $('#doctor_name_search').val();
        var service_name_search = $('#service_name_search').val();
        $.ajax({
        type: "GET",
            url: url,
            data: "doctor_name_search="+doctor_name_search+"& service_name_search="+service_name_search,
            beforeSend: function () {
               $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) { 
                $('#generilistdiv').html(msg);
                                    
                $('#generilistdiv').LoadingOverlay("hide");
                $("#doctor_id").val(doctor_name_search).select2();
                $("#service_id").val(service_name_search).select2();
                if(service_name_search !='0' && doctor_name_search !='0'){
                    $("#new_row").hide();
                }
                    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
            },
            complete: function () {
                $('#generilistdiv').LoadingOverlay("hide");
            },error: function(){
                toastr.error("Please Check Internet Connection");
            }
        });
   }
    
function saveProcedureShare(){
   
   var url = '<?=route('extensionsvalley.accounts.saveProcedureShare')?>';
   if($("#doctor_id").val()==''){
    toastr.warning("Please select doctor");
    return false;
   }
   if($("#percentage").val()==''){
    toastr.warning("Please select percentage");
    return false;
   }
   if($("#service_id").val()=='') {
    toastr.warning("Please select Department");
    return false;
   }
   var doctor_id = $("#doctor_id").val();
   var service_id = $("#service_id").val();
   var percentage = $("#percentage").val();
    var params = {doctor_id: doctor_id, service_id: service_id,percentage:percentage};
  $.ajax({
        type: "GET",
        url: url ,
        data: params,
        beforeSend: function () {
            $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (datas) {
            var data = JSON.parse(datas);
           if(data.success == '1'){
               toastr.success(data.message);
               }else if(data.success == '3'){
                toastr.warning(data.message);
               }else{
                toastr.error("Error Occured");
               }
        },
        complete: function () {
            $('#generilistdiv').LoadingOverlay("hide");
            searchForm();
            
        }
    });
}
function updateProcedureShare(procedure_share_id){
   var url = '<?=route('extensionsvalley.accounts.updateDoctorProcedureShare')?>'
   if($("#doctor_id_"+procedure_share_id).val()==''){
    toastr.warning("Please select Doctor");
    return false;
   }
   if($("#service_id_"+procedure_share_id).val()==''){
    toastr.warning("Please select Department");
    return false;
   }
   if($("#percentage_"+procedure_share_id).val()=='') {
    toastr.warning("Please select Percentage");
    return false;
   }
   var doctor_id = $("#doctor_"+procedure_share_id).val();
   var service_id = $("#service_"+procedure_share_id).val();
   var percentage = $("#percentage_"+procedure_share_id).val();
    $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
    var params = {procedure_share_id:procedure_share_id,doctor_id: doctor_id, service_id: service_id,percentage:percentage};
  $.ajax({
        type: "GET",
        url: url ,
        data: params,
        beforeSend: function () {
            $('#savebtn_'+procedure_share_id).attr('disabled',true);
        },
        success: function (datas) {
            var data = JSON.parse(datas);
           if(data.success == '1'){
               toastr.success(data.message);
               }else if(data.success == '3'){
                toastr.warning(data.message);
               }else{
                toastr.error("Error Occured");
               }
        },
        complete: function () {
            $('#savebtn_'+procedure_share_id).attr('disabled',false);
            searchForm();
        }
    });
}
 function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
 
</script>

@endsection
