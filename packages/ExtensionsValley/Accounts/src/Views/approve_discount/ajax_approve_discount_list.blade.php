<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if(url && url!= 'undefined') {
                var from_date = $('#request_fromdate').val();
                var to_date = $('#request_todate').val();
                var bill_id = $('#bill_id').val();
                var bill_no = $('#search_bill_no').val();
                var bill_tag = $('#search_bill_tag').val();
                var status = $('#search_status').val();
                var patient_name = $('#search_patient_name').val();
                var uhid = $('#patient_uhid').val();
                var patient_id = $('#patient_id').val();
                var param = {
                    patient_id: patient_id,
                    bill_id: bill_id,
                    bill_no: bill_no,
                    bill_tag: bill_tag,
                    patient_name: patient_name,
                    uhid: uhid,
                    from_date: from_date,
                    to_date: to_date,
                    status: status
                }
                resetApproveBill();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#searchDiscountRequestedBillsBtn').attr('disabled', true);
                        $('#searchDiscountRequestedBillsSpin').removeClass('fa fa-search');
                        $('#searchDiscountRequestedBillsSpin').addClass('fa fa-spinner fa-spin');
                        $("#approveDiscountData").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function (data) {
                        $('#approveDiscountData').html(data);
                    },
                    complete: function () {
                        $('#searchDiscountRequestedBillsBtn').attr('disabled', false);
                        $('#searchDiscountRequestedBillsSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchDiscountRequestedBillsSpin').addClass('fa fa-search');
                        $("#approveDiscountData").LoadingOverlay("hide");
                    }
                });
        }
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 500px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="common_td_rules" width="12%">Bill No.</th>
                <th class="common_td_rules" width="12%">Patient Name</th>
                <th class="common_td_rules" width="10%">Bill Tag</th>
                <th class="common_td_rules" width="10%">Discount Type</th>
                <th class="common_td_rules" width="5%">Discount Value</th>
                <th class="common_td_rules" width="10%">Approve Status</th>
                <th class="common_td_rules" width="10%">Requested At</th>
                <th class="common_td_rules" width="10%">Requested By</th>
                <th class="common_td_rules" width="10%">Net Amount</th>
                <th style="text-align: center" width="3%"><i class="fa fa-list"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($discount_request) != 0) {
                foreach ($discount_request as $list) {
                    $td_class="blue";
                    if(intval($list->status)==2){
                        $td_class="green";
                    } else if(intval($list->status)==3){
                        $td_class="red";
                    }
                    ?>
            <tr>
                <td title="{{  $list->bill_no }}" class="common_td_rules">{{ $list->bill_no }}</td>
                <td title="{{ $list->patient_name }}" class="common_td_rules">{{ $list->patient_name }}</td>
                <td title="{{ $list->bill_tag }}" class="common_td_rules">{{ $list->bill_tag }}</td>
                <td title="{{ $list->discount_type }}" class="common_td_rules">{{ $list->discount_type }}</td>
                <td title="{{ $list->discount_value  }}" class="common_td_rules">{{ $list->discount_value }}</td>
                <td title="{{ $list->approve_status }}" class="common_td_rules text-bold {{ $td_class }}">{{
                    $list->approve_status
                    }}</td>
                <td title="{{ $list->requested_at }}" class="common_td_rules">{{ $list->requested_at }}</td>
                <td title="{{ $list->requested_by }}" class="common_td_rules">{{ $list->requested_by }}</td>
                <td title="{{ $list->net_amount_wo_roundoff }}" class="td_common_numeric_rules">
                    {{ number_format($list->net_amount_wo_roundoff,2) }}
                </td>
                <td style="text-align: center"><button id="approveDiscountRequestBtn{{ $list->request_id }}"
                        onclick="approveDiscountRequest({{ $list->request_id }},{{ $list->status }},{{ $list->bill_type }})"
                        type="button" class="btn btn-primary"><i id="approveDiscountRequestSPin{{ $list->request_id }}"
                            class="fa fa-list"></i></button>
                </td>
            </tr>
            <?php
                }
            }else{
                ?>
            <tr>
                <td colspan="11" style="text-align: center">No Records Found</td>
            </tr>
            <?php
            }
            ?>

        </tbody>
    </table>
</div>
