<div class="col-md-8 padding_sm">
    <div class="mate-input-box">
        <label for="">Bill No.</label>
        <div class="clearfix"></div>
        <label style="margin-top:23px">{{ @$result[0]->bill_no ? $result[0]->bill_no : '' }}</label>
    </div>
</div>
<div class="col-md-4 padding_sm">
    <div class="mate-input-box">
        <label for="">Status</label>
        <div class="clearfix"></div>
        <label style="margin-top:23px">{{ @$result[0]->discount_status ? $result[0]->discount_status : '' }}</label>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-6 padding_sm">
    <div class="mate-input-box">
        <label for="">Patient Name</label>
        <div class="clearfix"></div>
        <label style="margin-top:23px">{{ @$result[0]->patient_name ? $result[0]->patient_name : '' }}</label>
    </div>
</div>
<div class="col-md-6 padding_sm">
    <div class="mate-input-box">
        <label for="">UHID</label>
        <div class="clearfix"></div>
        <label style="margin-top:23px">{{ @$result[0]->uhid ? $result[0]->uhid : '' }}</label>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm">
    <div class="mate-input-box">
        <label for="">Remarks</label>
        <div class="clearfix"></div>
        <label style="margin-top:23px">{{ @$result[0]->requested_remark ? $result[0]->requested_remark : '' }}</label>
    </div>
</div>
<div class="clearfix"></div>
@php
$col_class="col-md-12";
$col_width="45vh;";
@endphp
@if(intval($status)==1)
@php
$col_class="col-md-4";
$col_width="30vh;";
@endphp
@endif
<div class="col-md-12 padding_sm">
    <div class="theadscroll" style="position: relative; height: {{ $col_width }}">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;min-width:250px !important">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="80%">Item Description</th>
                    <th class="common_td_rules" width="20%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $net_total=0;
                if (count($result) != 0) {
                    foreach ($result as $list) {
                        $net_total += $list->net_amount;
                        ?>
                <tr>
                    <td title="{{  $list->item_desc }}" class="common_td_rules">{{ $list->item_desc }}</td>
                    <td title="{{ $list->net_amount }}" class="td_common_numeric_rules">{{
                        number_format($list->net_amount,2) }}</td>
                </tr>
                <?php
                    }
                    ?>
                <tr class="table_header_bg">
                    <td class="common_td_rules">Total</td>
                    <td class="td_common_numeric_rules">{{ number_format($net_total,2) }}
                        <input type="hidden" id="total_netamount" value="{{ $net_total }}">
                    </td>
                </tr>
                <?php
                }else{
                    ?>
                <tr>
                    <td colspan="2" style="text-align: center">No Records Found</td>
                </tr>
                <?php
                }
                ?>

            </tbody>
        </table>
    </div>
</div>
<div class="clearfix"></div>
@if(intval($status)==1)
<div class="col-md-6 padding_sm">
    <div class="mate-input-box">
        <label style="margin-top: -3px" for="">Discount Type</label>
        <div class="clearfix"></div>
        @php
        $discount_type=@$result[0]->discount_type ? $result[0]->discount_type : '';
        @endphp
        <select onchange="calculateDiscountValue()" class="form-control" id="approve_discount_type"
            name="approve_discount_type">
            <option value="">All</option>
            @foreach ($discount_array as $key=>$val)
            @php
            $selected="";
            if($discount_type==$key){
            $selected="selected=''";
            }
            @endphp
            <option {{ $selected }} value="{{ $key }}">{{ $val }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="col-md-6 padding_sm">
    <div class="mate-input-box">
        <label for="">Discount Value</label>
        <div class="clearfix"></div>
        <input onblur="calculateDiscountValue()"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value="{{ $result[0]->discount_value ? $result[0]->discount_value : 0 }}" autocomplete="off"
            class="form-control td_common_numeric_rules" id="approve_discount_value">
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-6 padding_sm">
    <div class="mate-input-box">
        <label for="">Discount Amount</label>
        <div class="clearfix"></div>
        <input readonly type="text" autocomplete="off" class="form-control td_common_numeric_rules"
            id="approve_calculated_bill_amt">
    </div>
</div>
<div class="col-md-6 padding_sm">
    <div class="mate-input-box">
        <label for="">Bill Amount</label>
        <div class="clearfix"></div>
        <input readonly type="text" autocomplete="off" class="form-control td_common_numeric_rules"
            id="approve_calculated_value">
    </div>
</div>
<div class="clearfix"></div>
@endif
<div class="col-md-12 padding_sm">
    <div class="{{ $col_class }} padding_sm">
        <button type="button" id="searchDiscountRequestedBillsBtn" onclick="resetApproveBill()"
            class="btn btn-warning btn-block">
            Clear <i class="fa fa-recycle"></i>
        </button>
    </div>
    @if(intval($status)==1)
    <div class="col-md-4 padding_sm">
        <button type="button" id="saveDiscountRequestBtn3" onclick="saveDiscountRequest(3)"
            class="btn btn-danger btn-block">
            Reject <i id="saveDiscountRequestSpin3" class="fa fa-save"></i>
        </button>
    </div>
    <div class="col-md-4 padding_sm">
        <button type="button" id="saveDiscountRequestBtn2" onclick="saveDiscountRequest(2)"
            class="btn btn-success btn-block">
            Approve <i id="saveDiscountRequestSpin2" class="fa fa-save"></i>
        </button>
    </div>
    @endif
</div>
