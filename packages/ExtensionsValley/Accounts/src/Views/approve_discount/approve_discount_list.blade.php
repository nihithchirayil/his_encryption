@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
<input type="hidden" id="request_id" value="0">
<input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
<input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">

<div class="right_col" role="main">
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm"><span class="pull-right" style="color: #01987a">{{ $title }}</span></div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 8vh;">
                    <div class="col-md-11 padding_sm">
                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{ date('M-d-Y') }}" autocomplete="off"
                                    class="form-control datepicker" id="request_fromdate">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{ date('M-d-Y') }}" autocomplete="off"
                                    class="form-control datepicker" id="request_todate">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name/UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="search_patient_name">
                                <div style="margin-top: -17px" id="patient_name_AjaxDiv" class="ajaxSearchBox"></div>
                                <input type="hidden" name="patient_uhid" id="patient_uhid">
                                <input type="hidden" name="patient_id" id="patient_id">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Bill No</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="search_bill_no">
                                <div style="margin-top: -17px" id="bill_no_AjaxDiv" class="ajaxSearchBox"></div>
                                <input type="hidden" name="bill_id" id="bill_id">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -3px" for="">Bill Tag</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="search_bill_tag" name="search_bill_tag">
                                    <option value="">All</option>
                                    @foreach ($bill_tag as $each)
                                    <option value="{{ $each->code }}">{{ $each->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -3px" for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="search_status" name="search_status">
                                    <option value="">All</option>
                                    <option value="1">Requested</option>
                                    <option value="2">Approved</option>
                                    <option value="3">Rejected</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <div class="col-md-12 padding_sm pull-right">
                            <button type="button" id="searchDiscountRequestedBillsBtn"
                                onclick="searchDiscountRequestedBills()" class="btn btn-primary btn-block">
                                Search <i id="searchDiscountRequestedBillsSpin" class="fa fa-search"></i>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm pull-right">
                            <button type="button" onclick="resetSearchFilters()" class="btn btn-warning btn-block">
                                Reset <i class="fa fa-recycle"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-9 padding_sm" style="margin-top: 5px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 75vh;">
                    <div class="col-md-12 padding_sm" id="approveDiscountData">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 5px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 75vh;">
                    <div class="col-md-12 padding_sm" id="approveDiscountList">
                        <div class="green" style="text-align: center;margin-top: 35vh">
                            <h4>Please Select any Bill</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/approve_discount.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
