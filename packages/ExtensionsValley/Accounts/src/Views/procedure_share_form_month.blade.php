@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <input type="hidden" name="base_url" id="base_url" value="{{URL::To('/')}}">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.accounts.listBillAnalysisShare')}}" id="listingSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Doctor</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" style='color:#555555;' name="doctor_name_search" id="doctor_name_search" onchange="selectFormFields()">
                                <option value="0">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                                </div>

                            </div> 
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Service</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" style='color:#555555;' name="departmet_name_search" id="departmet_name_search" onchange="selectFormFields()">
                                   <option value="0">Select Service</option>
                                   <?php foreach($department_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                                </div>

                            </div>
                            
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <form id="bill_analysis_form_month">
            <div class="box no-border no-margin" id="generilistdiv" style="overflow: auto !important">
                
            </div>
            </form>
            
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }
    
    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
        $('.datepicker').datetimepicker({
        format: 'MMM-YYYY',
    });
    });
    function selectFormFields(){
        searchForm();
    }
    
    function searchForm(){
        var url='<?=route('extensionsvalley.accounts.getProcedureShareMonth')?>';
        var doctor_name_search = $('#doctor_name_search').val();
        var departmet_name_search = $('#departmet_name_search').val();
        $.ajax({
        type: "GET",
            url: url,
            data: "doctor_name_search="+doctor_name_search+"& departmet_name_search="+departmet_name_search,
            beforeSend: function () {
                $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) { 
                $('#generilistdiv').html(msg);
                if(departmet_name_search !='0' && doctor_name_search !='0'){
                    $("#new_row").hide();
                }
                $('#doctor_id_0').val(doctor_name_search).select2();
                $('#department_id_0').val(departmet_name_search).select2();
                $('#generilistdiv').LoadingOverlay("hide");
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
                }
                });
                $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                        minScrollbarLength: 30
                });
                $('.fixed_header').floatThead({
                position: 'absolute',
                        scrollContainer: true
                });
            },
            complete: function () {
                $('#generilistdiv').LoadingOverlay("hide");
            },error: function(){
                toastr.error("Please Check Internet Connection");
   }
        });
   }
    
function saveBillAnalysis(){
   
   var url = '<?=route('extensionsvalley.accounts.saveProcedureShareMonth')?>';
   var doctor_id = $('input[name="doctor_id[]"]').filter((i, el) => el.value.trim() != '').length;
   var department_id = $('input[name="department_id[]"]').filter((i, el) => el.value.trim() != '').length;
   var percentage = $('input[name="percentage[]"]').filter((i, el) => el.value.trim() != '').length;

   if($("#doctor_id").val() == '0' && $("#department_id").val() =='0'){
   if($("#percentage").val()==''){
    toastr.warning("Please select percentage");
    return false;
   }
   }
  $.ajax({
        type: "GET",
        url: url ,
        data: $("#bill_analysis_form_month").serialize(),
        beforeSend: function () {
            $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (datas) {
            var data = JSON.parse(datas)
           if(data.success == '1'){
               toastr.success(data.message);
               }else if(data.success == '3'){
                toastr.warning(data.message);
               }else{
                toastr.error("Error Occured");
               }
        },
        complete: function () {
            $('#generilistdiv').LoadingOverlay("hide");
            searchForm();
        }
    });
}

 function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function nextrow(e){ 
    var doctor_name_search = $('#doctor_name_search').val();
    var departmet_name_search = $('#departmet_name_search').val();
    dept_show = doct_show = '';
    if(departmet_name_search != '0' && doctor_name_search =='0'){
    var dept_show ="display:none";
    var doct_show ="";
}
if(departmet_name_search == 0 && doctor_name_search !=0){
    var dept_show ="";
    var doct_show ="display:none";   
}
    var department_master = JSON.parse('<?php echo $department_master; ?>');
    department_master_str = "<option  value='0'>Select Service</option>";
    $.each(department_master, function (key, val) {
        department_master_str += "<option  value='" + key + "'>" + val + "</option>";
    });
    var doctor_master = JSON.parse('<?php echo $doctor_master; ?>');
    doctor_master_str = "<option  value='0'>Select Doctor</option>";
    $.each(doctor_master, function (key, val) {
        doctor_master_str += "<option  value='" + key + "'>" + val + "</option>";
    });
    var k = 110;
    var doctor_name_search = $('#doctor_name_search').val();
    var departmet_name_search = $('#departmet_name_search').val();
    $("#doctor_id_0").attr("id","doctor_"+k);
    $("#department_id_0").attr("id","department_"+k);
    var new_r = "<tr style='background-color: #f1dcb7'>\n\
<td class='common_td_rules' style='"+doct_show+"'>\n\
<select class='form-control select2' style='color:#555555;' name='doctor_id[]' id='doctor_id_0'>\n\
"+doctor_master_str+"</select></td>\n\
<td class='common_td_rules' style='"+dept_show+"'>\n\
<select class='form-control select2' style='color:#555555;' name='department_id[]' id='department_id_0'>\n\
"+department_master_str+"</select></td>\n\
<td class='common_td_rules'>\n\
<input type='text' class='form-control' name='percentage[]' id'percentage' onkeyup='number_validation(this);nextrow(this)'>\n\
</td>\n\
</tr>";
            k++;
         var row_ck = 0;
        $('#payment_receipt_tbody tr').each(function () {
        if ($(this).find('input[name="percentage[]"]').val() ==''){
            row_ck = 1;
        }
    });
    if(row_ck == 0){
    $('#payment_receipt_tbody').append(new_r);
    $(".theadfix_wrapper").floatThead('reflow');
    }
                $('#doctor_id_0').val(doctor_name_search).select2();
                $('#department_id_0').val(departmet_name_search).select2();
}
</script>

@endsection
