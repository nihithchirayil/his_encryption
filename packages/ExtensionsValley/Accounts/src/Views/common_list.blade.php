@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 100px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                            {!! Form::token() !!}
                            <div class= "col-md-2">
                                <div class="mate-input-box">
                                    <label class="filter_label">Account</label>
                                    <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="ledger_name" name="ledger_name" onkeyup="select_item_desc(this.id, event,'{{$voucher_type}}','All')"  />
                                    <div id="AjaxDiv" class="ajaxSearchBox" style="z-index: 1004;width:500px !important;max-height: 500px !important;margin-top: 14px !important;margin-left: -4px !important"></div>
                                    <input class="filters" value=""  type="hidden" name="ledger_id_hidden" value="" id="ledger_id_hidden">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="from_date" id="from_date" value="">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="to_date" id="to_date" value="">
                                </div>
                            </div>
                            <?php if($voucher_type == 1){ ?>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <input type="checkbox" class="" name="approve_dt" id="approve_dt" value="1" >
                                    &nbsp; &nbsp;<span style="font-size: 12px;font-weight: 700;">Appr.Dt</span>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Voucher No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="voucher_no_search" id="voucher_no_search" value="">
                                    <input type="hidden" class="form-control" name="voucher_type" id="voucher_type" value="<?= $voucher_type ?>">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Narration</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="narration_search" id="narration_search" value="">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Amount</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="amount_search" id="amount_search" value="">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Amount Criteria</label>
                                    <div class="clearfix"></div>
                                    <select name="amount_crt" id="amount_crt" class="form-control">
                                        <option value="1">Equal</option>
                                        <option value="2">Greater or equal to</option>
                                        <option value="3">Less or equal to</option>
                                        <option value="4">Greater</option>
                                        <option value="5">Less</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchList()"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="common_list_div">

            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="getAuditLog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title" id="audit_log_header"></span>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow: auto" id="audit_log_body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getrequstapproveModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1000px;width: 30%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title">Send Request</span>
            </div>
            <div class="modal-body" style="overflow: auto;height: 155px;">
                <input type="hidden" id="head_id" value="">
                <input type="hidden" id="voucher_type" value="">
                <input type="hidden" id="voucher_no" value="">
                <input type="hidden" id="inv_date_approval" value="">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm'>
                        <table id="getrequstapproveData">
                            <div class="col-md-12 padding_sm"  id="">
                                <div>
                                    <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                   <textarea class="form-control" name="remarks" id="remarks" style="resize: none;height: 100px !important;"></textarea>
                                </div>
                            </div>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="readyToSave">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="saverequesttoedit();" data-dismiss="modal">Request</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<?php  $company = \DB::table('company')->pluck('sub_code'); ?>
<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/audit_log.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    searchList();
    var table_company_code = $("#table_company_code").val();
    if(table_company_code == 'DAYAH'){
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
        minDate : '04-01-2022',
    });
}else{
       $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
}
    });


    function searchList(){
        var url='<?=route('extensionsvalley.accounts.commonList')?>';
        var voucher_no_search = $('#voucher_no_search').val();
        var voucher_type = $('#voucher_type').val();
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var ledger_id_hidden = $("#ledger_id_hidden").val();
        var narration_search = $("#narration_search").val();
        var amount_search = $("#amount_search").val();
        var amount_crt = $("#amount_crt").val();
        if($('#approve_dt').prop('checked')){
            var approve_dt = 1;
        }else{
            var approve_dt = 2;
        }
        var parm = { voucher_no_search: voucher_no_search,voucher_type:voucher_type,
                    from_date:from_date,to_date:to_date,approve_dt:approve_dt,ledger_id_hidden:ledger_id_hidden,
                    narration_search:narration_search,amount_search:amount_search,amount_crt:amount_crt };
        $.ajax({
        type: "GET",
            url: url,
            data: parm,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled',true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
                $('#common_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) {
                $('#common_list_div').html(msg);
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled',false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#common_list_div').LoadingOverlay("hide");
            },error: function(){
                toastr.error("Please Check Internet Connection");
            }
        });
   }
   function select_item_desc(id, event,type,is_header=0) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        var ajax_div = 'AjaxDiv';
        var ledger_desc = $('#ledger_name').val();

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {

        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
             var url = $("#ins_base_url").val();
            $.ajax({
                type: "GET",
                url: '<?= route('extensionsvalley.accounts.searchLedgerMaster') ?>',
                data: 'ledger_desc=' + ledger_desc + '&type='+type+'&search_ledger_desc=1&is_header='+is_header,
                beforeSend: function () {
                    $("#" + ajax_div).css('top', '');
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();

                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillLedgerValues(e, ledger_name, id,ledger_group_id,ledger_code) {
$('#ledger_name').val(ledger_name);
$('#ledger_id_hidden').val(id);
$("#AjaxDiv").hide();
}
</script>

@endsection
