@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">

@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
         {!!Form::open(array('url' => '', 'method' => 'get', 'id'=>'form'))!!}
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="accounts_common_header">
                            @if($voucher_type_id==1 || $voucher_type_id==11)
                            @include('Accounts::common_accounts_entry_header_purchase')
                            @elseif($voucher_type_id==15 || $voucher_type_id== 9 || $voucher_type_id== 12 || $voucher_type_id== 18)
                             @include('Accounts::common_accounts_entry_header_bank_payment')
                            @else
                           @include('Accounts::common_accounts_entry_header')
                           @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php
        $enable_multiple_naration = \DB::table('config_detail')->where('name','enable_multiple_naration')->value('value');
        ?>
        <div class="clearfix"></div>
        <div class="box no-border no-margin" style="height: 450px;">
            <div class="box-body clearfix" style="width:70%;margin-left: 180px">

                <div>
                    @include('Accounts::add_entry_template')

                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>
    <div class="modal fade" id="getrequstapproveModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1000px;width: 30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <span class="modal-title">Send Request</span>
                </div>
                <div class="modal-body" style="overflow: auto;height: 155px;">
                    <input type="hidden" id="head_id" value="">
                    <input type="hidden" id="voucher_type" value="">
                    <input type="hidden" id="voucher_no" value="">
                    <div class="row padding_sm">
                        <div class='col-md-12 padding_sm'>
                            <table id="getrequstapproveData">
                                <div class="col-md-12 padding_sm"  id="">
                                    <div>
                                        <label for="">Remarks</label>
                                        <div class="clearfix"></div>
                                       <textarea class="form-control" name="remarks" id="remarks" style="resize: none;height: 100px !important;"></textarea>
                                    </div>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="readyToSave">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" onclick="saverequesttoedit();" data-dismiss="modal">Request</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="add_entry_approved" name="add_entry_approved" value="0">
     {!! Form::token() !!} {!! Form::close() !!}
</div>
@php
$block_greater30days_selectionandupdate = \DB::table('config_detail')->where('name','block_greater30days_selectionandupdate')->value('value');
$enable_save_continue = \DB::table('config_detail')->where('name','enable_save_continue')->value('value');
 @endphp
<input type="hidden" id="block_thirty_days" value="{{ $block_greater30days_selectionandupdate }}">
<input type="hidden" id="enable_save_continue" value="{{ $enable_save_continue }}">
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" id="add_entry_value" value="1">
<input type="hidden" id="bank_voucher_generated" value="0">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
<input type="hidden" value="{{$voucher_type_id}}" id="voucher_type_id" name="voucher_type_id">
<?php  $company = \DB::table('company')->pluck('sub_code'); ?>

<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
<input type="hidden" id="enable_multiple_naration" value="<?= $enable_multiple_naration ?>">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<!--<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>-->
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/accounts.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">
                            function autoheight(element) {
                                var el = document.getElementById(element);
                                el.style.height = "5px";
                                if (el.scrollHeight < 300)
                                    el.style.height = (el.scrollHeight) + "px";
                            }
</script>
@endsection
