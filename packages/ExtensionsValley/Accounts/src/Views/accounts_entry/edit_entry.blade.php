@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
@php
$user_name1 = '';
@endphp
<div class="right_col"  role="main">
    <div class="row codfox_container">
         {!!Form::open(array('url' => '', 'method' => 'get', 'id'=>'form'))!!}
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <input type="hidden" name="voucher_type_id" id="voucher_type_id" value="{{$header[0]->voucher_type??''}}">
                        <input type="hidden" name="detail_cr" id="detail_cr" value="{{$detail[0]->cr_dr??''}}">
                        <input type="hidden" name="exp_inc_type" id="exp_inc_type" value="{{$type??''}}">
                        <input type="hidden" name="head_id" id="head_id" value="{{$header[0]->head_id??''}}">
                        <input type="hidden" name="detail_id_in_header" id="head_id" value="{{$header[0]->id??''}}">
                        <input type="hidden" id="enable_multiple_naration" value="<?= $enable_multiple_naration ?>">
                        <div class="accounts_common_header">
                        @php  $header_amount = 0; @endphp
                        @foreach($header as $h)
                          @php  $header_amount += $h->amount; @endphp
                        @endforeach
                            @if($header[0]->voucher_type == 1  || $header[0]->voucher_type == 11)
                            @include('Accounts::common_accounts_edit_header_purchase')
                            @elseif($header[0]->voucher_type == 15 || $header[0]->voucher_type == 9 || $header[0]->voucher_type == 18)
                            @include('Accounts::common_accounts_edit_header_bank_payment')
                            @elseif($header[0]->voucher_type == 12)
                            @include('Accounts::common_accounts_edit_header_payment')
                            @else
                           @include('Accounts::common_accounts_edit_header')
                           @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box no-border no-margin" style="height: 450px;">
            <div class="box-body clearfix" style="width:70%;margin-left: 180px">
                <div>
                    @include('Accounts::edit_entry_template')


                </div>

            </div>
            <div class="clearfix"></div>

        </div>
        <div class="col-xs-12 padding_sm pull-left" style="padding-top: 10px !important;">
            <?php
            $style ='';
            $cr_at = @$detail[0]->created_at ? $detail[0]->created_at : 0;
            $cr_by = @$detail[0]->created_by ? $detail[0]->created_by : 0;
            if($cr_at != 0){
                $created_at = date('M-d-Y h:i:s',strtotime($cr_at));
            }else{
                $created_at = '';
            }
            if($cr_by !=0){
            $u_name = \DB::table('users')->where('id',$cr_by)->pluck('name');
            }
            $u_name = @$u_name[0] ? $u_name[0] : '';

            ?>
            <span style="margin-left: 170px !important;"><label style="color:red">Created At :&nbsp;</label>{{$created_at}}</span>
            <span style="margin-left: 174px !important;"><label style="color:red">Created By :&nbsp;</label>{{$u_name}}</span><br>
             <?php
                $up_at = @$detail[0]->updated_at ? $detail[0]->updated_at : 0;
                $up_by = @$detail[0]->updated_by ? $detail[0]->updated_by : 0;
             if($up_at != 0){
                 $updated_at = date('M-d-Y h:i:s',strtotime($up_at));
             }else{
                 $updated_at = '';
             }
             if($up_by != ''){
               $user_name = \DB::table('users')->where('id',$up_by)->value('name');
               $user_name1 = @$user_name ? $user_name : '';
            }else{
                $style = "width: 199px;";
            }
             ?>
            <span style="margin-left: 170px !important;"><label style="color:red;{{ $style }}">Updated At :&nbsp;</label>{{$updated_at}}</span>
            <span style="margin-left: 170px !important;"><label style="color:red">Updated By :&nbsp;</label>{{$user_name1}}</span>
        </div>
    </div>
     {!! Form::token() !!} {!! Form::close() !!}
</div>

<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
<?php  $company = \DB::table('company')->pluck('sub_code'); ?>
<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/accounts.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">
                            function autoheight(element) {
                                var el = document.getElementById(element);
                                el.style.height = "5px";
                                if (el.scrollHeight < 300)
                                    el.style.height = (el.scrollHeight) + "px";
                            }
</script>
@endsection
