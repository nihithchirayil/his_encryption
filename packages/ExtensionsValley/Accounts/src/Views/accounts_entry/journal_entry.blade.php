@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">

@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        {!!Form::open(array('url' => '', 'method' => 'get', 'id'=>'form'))!!}
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="accounts_common_header">
                            <input type="hidden" name="voucher_type_id" id="voucher_type_id" value="{{$voucher_type_id}}">
                            <input type="hidden" name="detail_cr" id="detail_cr" value="">
                            <input type="hidden" name="exp_inc_type" id="exp_inc_type" value="<?= $exp_inc_type ?>">

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label>Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="<?= date('dd-mm-YYYY')?>" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
                                </div>
                            </div>                      
                            <div class="col-md-2 padding_sm  pull-right" style=" padding-top: 10px !important">
                                <label for=""style="float:right">Voucher No : <span style="color: red" id="generated_vouchr_label"><b><?= $voucher_no ?></b></span></label>
                                <input type="hidden" value="<?= $voucher_no ?>" name="generated_vouchr_text" id="generated_vouchr_text">
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="box no-border no-margin" style="height: 450px;">
            <div class="box-body clearfix" style="width:70%;margin-left: 180px">                        

                <div>
                    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
                        <thead>
                            <tr class="table_header_bg">
                                <th width="20%">Description</th>
                                <th width="20%">Acc.Name</th>
                                <th width="15%">Acc.No</th>
                                <th width="15%">Debit</th>
                                <th width="15%">Credit</th>
                            </tr>
                        </thead>
                        <tbody id="payment_receipt_tbody">
                            <tr>
                                <td>
                                    <input type="text" value="" name="description[]" class="form-control" >
                                </td>
                                <td>
                                    <input type="text" value=""  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,'journal', 'All')" id="ledger_item_desc-1">
                                    <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-1">
                                    <input type="hidden" value="" id="group_id_1" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                                    <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-1" 
                                         style="text-align: left; list-style: none;  cursor: pointer; 
                                         margin: -2px 0px 0px 0px;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="" name="acc_no[]" style="border:none">
                                </td>
                                <td>
                                    <input type="text" value="" name="dr_amnt[]" class="form-control amount dra_amnt" id="dr_amount_1" onkeyup="calculate_dr_total(this);" onblur="calculate_dr_total(this);"> 
                                </td>
                                <td>
                                    <input type="text" value="" name="cr_amnt[]" class="form-control amount cra_amnt" id="cr_amount_1" onkeyup="calculate_cr_total(this);" onblur="calculate_cr_total(this);"> 
                                </td>

                            </tr>

                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                   <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                        <div class="date custom-float-label-control">
                            <label class="custom_floatlabel">Total Credit Amount</label>
                            <input type="text" id="total_cr_amount_bottom" class="form-control" readonly="" value="" >                          
                        </div>
                    </div>
                      <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                        <div class="date custom-float-label-control">
                            <label class="custom_floatlabel">Total Debit Amount</label>
                            <input type="text" id="total_dr_amount_bottom" class="form-control" readonly="" value="" >                          
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm pull-right" style="padding-bottom: 10px;">
                        <div class="date custom-float-label-control">
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <label class="custom_floatlabel" >Total Difference </label>
                             <input type="hidden" id="ttl_dr_cr_dif" class="form-control" readonly="" value="" >     
                            <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dr_cr_dif_label"></h4></label>
                        </div>
                    </div>
<!--                    <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                        <div class="date custom-float-label-control">
                            <label class="custom_floatlabel">Total Amount</label>
                            <input type="text" id="total_amount_bottom" class="form-control" readonly="" value="" >                          
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm pull-right" style="padding-bottom: 10px;">
                        <div class="date custom-float-label-control">
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <label class="custom_floatlabel" >Total Difference </label>
                            <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label"></h4></label>
                            <input type="hidden" id="ttl_diff" class="form-control" readonly="" value="" >                          
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-xs-12 padding_sm pull-right" style="padding-bottom: 10px;">
<!--                        <span type="button" class="btn btn-success pull-right" onclick="saveJournalForm(1)">
                            <i class="fa fa-save" id="add_share_holder_spin"></i> Save</span>-->
                        <span type="button" class="btn btn-info pull-right" onclick="saveJournalForm(0)">
                            <i class="fa fa-thumbs-up" id="add_share_holder_spin_t"></i> Save & continue</span>
                        <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
                            <i class="fa fa-times" id="add_share_holder_spin" ></i>Cancel</span>
                        <span type="button" class="btn btn-default pull-right" onclick="refreshAllData()" >
                            <i class="fa fa-refresh" id="add_share_holder_spin" ></i>Refresh</span>
                    </div>


                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>
    {!! Form::token() !!} {!! Form::close() !!}
</div>

<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
<input type="hidden" value="{{$voucher_type_id}}" id="voucher_type_id" name="voucher_type_id">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<!--<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>-->
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/accounts.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">
        function autoheight(element) {
        var el = document.getElementById(element);
        el.style.height = "5px";
        if (el.scrollHeight < 300)
                el.style.height = (el.scrollHeight) + "px";
        }
</script>
@endsection
