<div class="theadscroll" style="position: relative; height: 350px;" id="div_list_supplier_data">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>Vendor Code</th>
                <th>Vendor Name</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Country</th>
                <th>Contact Person</th>
                <th>Contact Number</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @if (count($vendor_list) > 0)
            @foreach ($vendor_list as $vendor)
            <tr style="cursor: pointer;" onclick="vendorEditLoadData(this,
                                                                        '{{ $vendor->id }}',
                                                                        '{{ $vendor->vendor_code }}',
                                                                        '{{ $vendor->vendor_name }}',
                                                                        '{{ $vendor->address_1 }}',
                                                                        '{{ $vendor->address_2 }}',
                                                                        '{{ $vendor->address_3 }}',
                                                                        '{{ $vendor->city }}',
                                                                        '{{ $vendor->state }}',
                                                                        '{{ $vendor->country }}',
                                                                        '{{ $vendor->pincode }}',
                                                                        '{{ $vendor->contact_person }}',
                                                                        '{{ $vendor->contact_no }}',
                                                                        '{{ $vendor->email }}',
                                                                        '{{ $vendor->status }}',
                                                                        '{{ $vendor->tin_no }}',
                                                                        '{{ $vendor->tan_no }}',
                                                                        '{{ $vendor->pan_no }}',
                                                                        '{{ $vendor->gst_vendor_code }}',
                                                                        '{{ $vendor->drug_license_no }}',
                                                                        '{{ $vendor->ledger_id }}',
                                                                        '{{ $vendor->credit_days }}',
                                                                        '{{ $vendor->ledger_group_id }}');">
                <td class="vendor_code common_td_rules">{{ $vendor->vendor_code }}</td>
                <td class="vendor_name common_td_rules">{{ $vendor->vendor_name }}</td>
                <td class="address common_td_rules">{{ $vendor->address_1 }}</td>
                <td class="city common_td_rules">{{ $vendor->city }}</td>
                <td class="state common_td_rules">{{ $vendor->state }}</td>
                <td class="country common_td_rules">{{ $vendor->country }}</td>
                <td class="contact_person common_td_rules">{{ $vendor->contact_person }}
                </td>
                <td class="contact_no common_td_rules">{{ $vendor->contact_no }}</td>
                <td class="status common_td_rules">{{ $vendor->status_name }}</td>

            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="5" class="vendor_code">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>