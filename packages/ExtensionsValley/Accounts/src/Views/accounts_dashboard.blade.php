@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/profit_and_loss_dashboard.css') }}" rel="stylesheet">


@endsection
@section('content-area')
<div class="right_col" role="main">
    <div class="col-md-12 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 34px;">
                <div class="col-md-10 padding_sm">
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates('fy')" title="Current Year" class="btn btn-primary btn-block">Current Year <i
                                class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates('p_fy')" title="Previous Year"
                            class="btn btn-success btn-block">Previous Year <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <select class="form-control" id="select_month" onchange="select_month(this.value)">
                            <option value="0">Select</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                        </select>
                    </div>
<!--                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates('three_fy')" title="Last 3 Year"
                            class="btn btn-warning btn-block">Last 3 Year <i class="fa fa-calendar"></i></button>
                    </div>-->
<!--                    <div class="col-md-2">
                        <input class="btn bg-teal-active btn-block datepicker" type="text" id="datepicker" name="datepicker" value="Custom">
                    </div>-->
                    <div class="col-md-6">
                        <span style="padding-right: 20px; padding-left: 30px">Last Income Posted: <span style="color:red"><?= $inc_dt?$inc_dt:'' ?></span></span>
                        <span>Last Expense Posted: <span style="color:red"><?= $expns_dt?$expns_dt:'' ?></span></span>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="col-md-12 padding_sm" style="margin-top: 25px;height:700px">
        <div class="col-md-6 padding_sm" >
            <div class="col-md-12 no-padding" style="background:aliceblue;">
                <figure class="highcharts-figure">
                    <div id="container" style="height:175px;"></div>
                    <div style="width: 100%;overflow: auto">
                    <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC; background:white;">
                        <tbody id="exp_inc_tbody">
                            
                        </tbody>
                    </table>
                </div>
                </figure>
            </div>
            <div class="col-md-12 no-padding theadscroll" style="margin-top: 10px;height: 250px;position: relative">
                <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
                    <tbody>
                        <thead>
                        <tr class="table_header_bg">
                            <td style="width:35%">Bank</td>
                            <td style="width:20%">Opening Balance</td>
                            <td style="width:15%">Receipts</td>
                            <td style="width:15%">Payments</td>
                            <td style="width:15%">Closing Balance</td>
                        </tr>
                    </thead>
                    <tbody id="bank_tbdy">

                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-md-6 no-padding">

            <div class="col-md-12 no-padding">
                <div class="col-md-3 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div style=" height: 210px; background: #cbd6f5; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Net Profit </b> </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #ecf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span><span id="this_month_label"> Current Month</span> <br><b><span id="current_month_net"><i id="current_month_net_i" class=""></i></span></b></span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #ecf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span><span id="prev_month_label"> Last Month</span> <br><b><span id="last_month_net"><i id="last_month_net_i" class=""></i></span></b> </span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #ecf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span><span id="fy_label" style="padding-right: 3px"></span>
                                    <br><b><span id="fy_month_net"><i id="fy_month_net_i" class=""></i></span></b> 
                                </span>   
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="col-md-9 no-padding">
                    
                    
                    <div class="col-md-6 padding_sm">
                        <div style="height: 210px;  background: #f5e4cb; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Income </b> </div>
                            <div class="block direct_income_block" onclick="showDetailData(this,'Direct Income','direct_income_span')" style="cursor:pointer;margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%;"> Direct Income</div><div style="width:50%; text-align:right;"><b><span id="direct_income_span">-</span></b></div>
                            </div>
                            <div class="block indirect_income_block" onclick="showDetailData(this,'Indirect Income','indirect_income_span')" style="cursor:pointer;margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%;">Indirect Income</div><div style="width:50%; text-align:right;"><b><span id="indirect_income_span">-</span></b></div>
                            </div>
                            <div class="block pharmacy_block" onclick="showDetailData(this,'Pharmacy Sales','pharmacy_sale_span')" style="cursor:pointer;margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:55%;">Pharmacy Sales</div><div style="width:47%; text-align:right;"><b><span id="pharmacy_sale_span">-</span></b></div>
                            </div>
                            <div class="block other_income_block" onclick="showDetailData(this,'Other Income','other_income_span')" style="cursor:pointer;margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%;">Other Income </div><div style="width:50%; text-align:right;"><b><span id="other_income_span">-</span></b></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div style=" height: 158px;  background: #9adeb8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Expense </b> </div>
                            <div class="block direct_expense_block" onclick="showDetailData(this,'Direct Expense','direct_exp_span')" style="cursor:pointer;margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%">Direct Expense </div><div style="width:50%; text-align:right;"><b><span id="direct_exp_span">-</span></b></div>
                            </div>
                            <div class="block indirect_expense_block" onclick="showDetailData(this,'Indirect Expense','indirect_exp_span')" style="cursor:pointer;margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:55%">Indirect Expense </div><div style="width:45%; text-align:right;"><b><span id="indirect_exp_span">-</span></b></div>
                            </div>
                            <div class="block purchase_accounts_block" onclick="showDetailData(this,'Purchase Accounts','purchase_span')" style="cursor:pointer;margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:62%">Purchase Accounts </div><div style="width:38%; text-align:right;"><b><span id="purchase_span">-</span></b></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div style=" height: 52px;  background: #a4cf6b; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                        <div class="block fixed_asset_block" onclick="showDetailData(this,'Fixed Assets','fixed_asset_span')" style="cursor:pointer;margin-bottom: 5px;  display:flex; background: #ff9d9d; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%">Fixed Assets </div><div style="width:50%; text-align:right;"><b><span id="fixed_asset_span">-</span></b></div>
                        </div>
                        </div>
                    </div>
                </div>


            </div>


            <div class="col-md-12 no-padding" style="margin-top:10px;">
                <div class="col-md-3 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div style=" height: 330px; background: #d5cbf5; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Collection </b> </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>Lab <b id="lab_span">-</b></span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span> Pharmacy <b id="ph_collection_span">-</b> </span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>Radiology <b id="rad_span">-</b> </span>   
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>ICU <b id="icu_span">-</b> </span>   
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>OT <b id="ot_span">-</b> </span>   
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>Other <b id="other_coll_span">-</b> </span>   
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="col-md-9 no-padding">
                    
                    
                    <div class="col-md-6 padding_sm">
                        <div style="background: #dcf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Total OP</div>
                                <div style="width: 30%; text-align:right;">
                                    <b><span id="total_op_data"><i id="total_op_data_i" class=""></i></span></b> 
                                </div> 
                            </div>
                            <div class="block" style="margin-bottom: 5px; margin-top:5px; background: #faffb2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <table style="width:100%;">
                                    <tr>
                                        <td style="white-space: nowrap;">New </td>
                                        <td style="text-align:center;"> Renewal</td>
                                        <td style="white-space: nowrap; text-align:right;">Followup</td>
                                    </tr>
                                </table>
                                <table style="width:100%;">
                                    <tr>
                                        <td style="white-space: nowrap;"><b><span id="total_new_data"><i id="total_new_data_i" class=""></i></span></b></td>
                                        <td><b><span id="total_renew_data"><i id="total_renew_data_i" class=""></i></span></b></td>
                                        <td style="white-space: nowrap; text-align:center;"><b><span id="total_follow_data"><i id="total_follow_data_i" class=""></i></span></b></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="background: #dcf1a8;  height: 75px; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Total Unpaid Amount </div>
                                <div style="width: 40%; text-align:right;">  <b> 
                                        <span id="unpaid_data">
                                        </span> </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Unpaid OT Amount </div>
                                <div style="width: 40%; text-align:right;">  <b> <span id="unpaid_ot_data">
                                        </span> </b> </div>
                            </div>
                        </div>
                        <div style="background: #dcf1a8; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Company Credits </div>
                                <div style="width: 40%; text-align:right;">  <b> 
                                        <span id="company_credit_data">
                                            <i id="company_credit_data_i" class=""></i>
                                        </span> </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Current Outstanding </div>
                                <div style="width: 40%; text-align:right;">  <b> 
                                        <span id="company_credit_outstanding">
                                        </span> </b> </div>
                            </div>
<!--                            <div style="display:flex; ">
                                <div style="width: 70%;"> Settled This Month  </div>
                                <div style="width: 30%; text-align:right;">  <b> - </b> </div>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-md-6 padding_sm">
                        <div style="background: #ffbf7d;height: 102px; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 60%;"> No of IP Patients </div>
                                <div style="width: 40%; text-align:right;">  
                                    <b> 
                                        <span id="ip_data">
                                            <i id="ip_data_i" class=""></i>
                                        </span>
                                    </b> 
                                </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 30%;"> Avg.Los </div>
                                <div style="width: 70%; text-align:right;">  
                                    <b> <span id="los_data">
                                        </span> </b> 
                                </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> IP Income </div>
                                <div style="width: 40%; text-align:right;">  <b> <span id="ip_income_data">
                                        </span> </b> </div>
                            </div>
                        </div>
                        <div style="background: #ffbf7d;  color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Pharmacy Purchase </div>
                                <div style="width: 40%; text-align:right;">  
                                    <b> <span id="pharmacy_purchase_data">
                                            <i id="pharmacy_purchase_data_i" class=""></i>
                                        </span></b>
                                </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Pharmacy Sales </div>
                                <div style="width: 40%; text-align:right;">  <b> 
                                        <span id="pharmacy_sales_data">
                                            <i id="pharmacy_sales_data_i" class=""></i>
                                        </span> </b> 
                                </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Margin %  </div>
                                <div style="width: 40%; text-align:right;">  <b>
                                        <span id="pharmacy_margin_data">
                                            <i id="pharmacy_margin_data_i" class=""></i>
                                        </span> </b> </div>
                            </div>
                        </div>
                        <div style="background: #ffbf7d; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Total Discount </div>
                                <div style="width: 40%; text-align:right;">  
                                    <b> 
                                    <span id="total_discount_data">
                                            <i id="total_discount_data_i" class=""></i>
                                        </span> 
                                    </b>
                                     </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Shareholder Disc. </div>
                                <div style="width: 40%; text-align:right;">  <b> 
                                        <span id="share_discount_data">
                                        </span> </b> 
                                </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 60%;"> Discount - Others </div>
                                <div style="width: 40%; text-align:right;">  <b>  
                                        <span id="other_discount_data">
                                        </span> </b> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" >
                        <div class="col-md-12 padding_sm" style="background: #ffcce8; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;" >
                            <div class="col-md-4 padding_sm">
                                <div style="display:flex; ">
                                    <div style="width: 100%;"> <b> Closing Stock </b> </div>
                                </div>
<!--                                <div style="display:flex; ">
                                    <div style="width: 60%;"> Lab Cons. </div>
                                    <div style="width: 40%; text-align:right;">  <b> <span id="lab_clos_data">
                                        </span> </b> </div>
                                </div>-->
                            </div>
                            <div class="col-md-6 col-md-offset-2 padding_sm">
                                <div style="display:flex; ">
                                    <div style="width: 60%;"> Medicine </div>
                                    <div style="width: 40%; text-align:right;">  <b> <span id="closing_med_data">
                                        </span> </b> </div> 
                                </div>
                                <div style="display:flex; ">
                                    <div style="width: 60%;"> X-Ray Films </div>
                                    <div style="width: 40%; text-align:right;">  <b> <span id="closing_xray_data">
                                        </span> </b> </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>


            </div>

        </div>
    </div>
<input type="hidden" value="" id="bill_date_from">
<input type="hidden" value="" id="bill_date_to">
<input type="hidden" value="fy" id="current_year">
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
</div>
<div class="modal fade" id="getLeaderDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 50%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title" id="leder_name_header"></span>:&nbsp;&nbsp;<span class="modal-title" id="leder_amount_header"></span>

            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow: auto">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm'>
                        <table id="getLeaderDetailData">
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getLeaderDetailModeltr" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow:auto">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getLeaderDetailDatatr">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/accounts/default/javascript/profit_and_loss_dashboard.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
