@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>
    .view {
        margin: auto;
        width: 100%;
    }

    .wrapper {
        position: relative;
        overflow: auto;
        /*        border: 1px solid black;*/
        white-space: nowrap;
    }

    .sticky-col {
        position: -webkit-sticky;
        position: sticky;
        background-color: #efeded !important;
    }

    .first-col {
        width: 15%;
        min-width: 15%;
        max-width: 15%;
        left: 0px;
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
    }

    .second-col {
        width: 15%;
        min-width: 15%;
        max-width: 15%;
        left: 100px;
    }


</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin" >
                {!!Form::open(array('url' => $searchUrl, 'method' => 'post', 'id'=>'form'))!!}
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label>From Date</label>
                        <div class="clearfix"></div>
                        <input type="text" value="{{$searchFields['from_date']}}" id="from_date" name="from_date" autocomplete="off"    class="form-control datepicker">
                    </div>
                </div>
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label>To Date</label>
                        <div class="clearfix"></div>
                        <input type="text" value="{{$searchFields['to_date']}}" id="to_date" name="to_date" autocomplete="off"    class="form-control datepicker">
                    </div>
                </div>
                <div class="col-md-1 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <button class="btn btn-block light_purple_bg"  ><i class="fa fa-search"></i> Search</button>
                </div>
                <div class="col-md-1 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <span class="btn btn-block btn-warning" onclick="clear_search();" ><i class="fa fa-times"></i> Clear</span>
                </div>
                <div class="col-md-2 padding_sm pull-right">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <a class="btn light_purple_bg" onclick="exceller();"
                       name="csv_results" id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Excel
                    </a>
                </div>

                {!! Form::token() !!} {!! Form::close() !!}
                <div class="view">
                    <div class="wrapper theadscroll" style="position: relative; height: 400px;">
                        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" id="result_data_table">
                            <thead>
                                <tr class="table_header_bg">
                                    <th class="sticky-col first-col" style="background-color:#01987a !important;width: 15%">Details</th>
                                    <th style=" min-width: 75px !important">Qty</th>
                                    <th style=" min-width: 75px !important">Original Cost</th>
                                    <th style=" min-width: 75px !important">Date of Purchase</th>
                                    <th style=" min-width: 75px !important">Date of First use</th>
                                    <th style=" min-width: 75px !important">Life Yrs</th>
                                    <th style=" min-width: 75px !important">Orig Cost of Item Sold/Disposed</th>
                                    <th style=" min-width: 75px !important">Date of Sale/Disposal</th>
                                    <th style=" min-width: 75px !important">Date of Last Use</th> 
                                    <th style=" min-width: 75px !important">Till Used</th> 
                                    <th style=" min-width: 75px !important">Residue Value 5%</th>
                                    <th style=" min-width: 75px !important">Bal useful Days as on</th>
                                    <th style=" min-width: 75px !important">Opening WDV</th>
                                    <th style=" min-width: 75px !important">New Addition</th>
                                    <th style=" min-width: 75px !important">Disposed Assets Op WDV/Cost </th>
                                    <th style=" min-width: 75px !important">Days of Depreciation</th>
                                    <th style=" min-width: 75px !important">Depreciation for the year</th>
                                    <th style=" min-width: 75px !important">Closing WDV</th>
                                    <th style=" min-width: 75px !important">Sale Proceeds</th>
                                    <th style=" min-width: 75px !important"> Profit/(Loss) on Disposal</th>
                                    <th style=" min-width: 75px !important">Acc. Dep on Sale</th>
                                    <th style=" min-width: 75px !important">Adjustment in Depreciation</th>
                                    <th style=" min-width: 75px !important">WDV As per Software</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1; $group_name = ''; @endphp
                                @foreach($asset_data as $dt)
                                
                                    @if($dt->group_name != $group_name)
                                <tr style="background-color: #afd9ac !important;color: #a14848;">
                                    <td style="text-align:left;background-color: #afd9ac !important" class="sticky-col first-col"><b>{{$dt->group_name}}</b></td>
                                    <td colspan="23"></td>
                                </tr>
                                @endif
                                <tr onclick="editFixedRegister('{{$dt->id}}')" style="cursor:pointer">
                            <td class="sticky-col first-col">{{$dt->ledger}}</td>
                            <td>{{$dt->qty}}</td>
                            <td>{{$dt->unit_purchase_cost}}</td>
                            <td>{{$dt->date_purchase ?  date('d-M-Y',strtotime($dt->date_purchase)) :''}}</td>
                            <td>{{$dt->date_first_use ?  date('d-M-Y',strtotime($dt->date_first_use)) :''}}</td>
                            <td>{{$dt->life}}</td>
                            <td>{{$dt->original_cost_item}}</td>
                            <td>{{$dt->date_sale ?  date('d-M-Y',strtotime($dt->date_sale)) :''}}</td>
                            <td>{{$dt->date_last_use ?  date('d-M-Y',strtotime($dt->date_last_use)) :''}}</td>
                            <td>{{$dt->till_used_date ? date('d-M-Y',strtotime($dt->till_used_date)) :''}}</td>
                            <td>{{$dt->reside_value}}</td>
                            <td>{{$dt->bal_useful_day}}</td>
                            <td>{{$dt->opening_wdv}}</td>
                            <td>{{$dt->new_addition}}</td>
                            <td>{{$dt->disposed_asset}}</td>
                            <td>{{$dt->days_of_depre}}</td>
                            <td>{{$dt->deprec_for_the_year}}</td>
                            <td>{{$dt->closing_wdv}}</td>
                            <td>{{$dt->sale_proceeds}}</td>
                            <td>{{$dt->profit_los}}</td>
                            <td>{{$dt->acc_dep_sale}}</td>
                            <td>{{$dt->adj_deprec}}</td>
                            <td>{{$dt->wdv_as_soft}}</td>
                            </tr>
                            @php $i++; $group_name = $dt->group_name; @endphp
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    });
    function exceller(type, fn, dl) {
    $('#result_data_table td input[type="text"]').each(function() {
    var val = $(this).val();
    $(this).parent('td').html(val);
    });
    window.location.reload();
    var type = 'xlsx';
    var elt = document.getElementById('result_data_table');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
            XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
            XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
    }
   
function clear_search(){
    $("#from_date").val('');
    $("#to_date").val('');
    window.location.reload();
}
function editFixedRegister(id){
    window.location = "{!! route('extensionsvalley.accounts.editFixedAssetRegister') !!}/" + id;
}

</script>

@endsection
