@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
    <form >
    {!! Form::token() !!}
    @php $i=1; $group_name = ''; @endphp
      
        <div class="col-md-4 padding_sm">
        <div class="box no-border">
                <div class="box-body clearfix">
                    
                <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <input type="hidden" name="asset_register_id" id="asset_register_id" value="{{$asset_data[0]->id ?? 1 }}">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Qty</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->qty?? ' ' }}" name="qty[]" class="form-control qty" id="qty_{{$i}}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Original Cost</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->unit_purchase_cost ?? ' ' }}" name="original_cost[]" class="form-control original_cost" onblur="costChange(this.value)" id="original_cost_{{$i}}">
                        </div>
                    </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Date of Purchase</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{ $asset_data[0]->date_purchase ? date('d-m-Y',strtotime($asset_data[0]->date_purchase)): ' ' }}" name="date_purchase[]" class="form-control date_purchase datepicker" id="date_purchase_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Date of First use</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->date_first_use ? date('d-m-Y',strtotime($asset_data[0]->date_first_use)): ' ' }}" name="date_first_use[]" onblur="calculateLifeWise()" class="form-control date_first_use datepicker" id="date_first_use_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Life Yrs</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->life ?? ' ' }}" name="life[]" class="form-control life" id="life_{{$i}}" onblur="calculateLifeWise()">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Orig Cost of Item Sold/Disposed</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->original_cost_item ?? ' ' }}" name="original_cost_item[]" class="form-control original_cost_item" id="original_cost_item_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Date of Sale/Disposal</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->date_sale ?? ' ' }}" name="date_sale[]" class="form-control date_sale" id="date_sale_{{$i}}">
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                    <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Date of Last Use</label>
                            <div class="clearfix"></div>
                            <?php 
                            if($asset_data[0]->life !=''){
                               $date_last_use =  date("d-m-Y", strtotime(date("Y-m-d", strtotime($asset_data[0]->date_first_use)) . " + ".$asset_data[0]->life." year"));
                            }else{
                                $date_last_use = $asset_data[0]->date_last_use ?? ' ';
                            }
                            ?>
                            <input type="text" value="{{$date_last_use}}" name="date_last_use[]" class="form-control date_last_use datepicker" id="date_last_use_{{$i}}">
                            <div class="" id="category_code_status">
                            </div>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">           
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Till Used</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->till_used_date ? date('d-m-Y',strtotime($asset_data[0]->till_used_date)): ' ' }}" name="till_used_date[]" class="form-control till_used_date datepicker" id="till_used_date_{{$i}}">                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Residue Value 5%</label>
                            <div class="clearfix"></div>
                            <?php
                            $residevaue = 0;
                            if($asset_data[0]->reside_value == ''){
                            if($asset_data[0]->unit_purchase_cost > 0){
                                $residevaue = ($asset_data[0]->unit_purchase_cost*5)/100;
                            }else{
                                $residevaue =  $asset_data[0]->reside_value;
                            }
                        }
                            ?>
                            <input type="text" value="{{$residevaue}}" name="reside_value[]" class="form-control reside_value" id="reside_value_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Bal useful Days as on</label>
                            <div class="clearfix"></div>
                            <?php 
                            $last_use = strtotime( $date_last_use);
                            $till_used = $asset_data[0]->till_used_date?? 0;
                            $till_used = strtotime($till_used);
                            $bal_datediff = $last_use-$till_used;
                            $bal_useful_day = round($bal_datediff / (60 * 60 * 24));
                            ?>
                            <input type="text" value="{{$bal_useful_day ?? ' ' }}" name="bal_useful_day[]" class="form-control bal_useful_day" id="bal_useful_day_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Opening WDV</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->opening_wdv ?? ' ' }}" name="opening_wdv[]" class="form-control opening_wdv" id="opening_wdv_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">New Addition</label>
                            <div class="clearfix"></div>
                            <?php if($asset_data[0]->opening_wdv != '' || $asset_data[0]->opening_wdv !=0){
                                $new_addition=0;
                            }else{
                                $new_addition=$asset_data[0]->original_cost_item;
                            } ?>
                            <input type="text" value="{{$new_addition ?? ' ' }}" name="new_addition[]" class="form-control new_addition" id="new_addition_{{$i}}">
                        </div>
                    </div>
             
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Disposed Assets Op WDV/Cost</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->disposed_asset ?? ' ' }}" name="disposed_asset[]" class="form-control disposed_asset" id="disposed_asset_{{$i}}">
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                    <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Days of Depreciation</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->days_of_depre ?? ' ' }}" name="days_of_depre[]" class="form-control days_of_depre" id="days_of_depre_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Depreciation for the year</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->deprec_for_the_year ?? ' ' }}" name="deprec_for_the_year[]" class="form-control deprec_for_the_year" id="deprec_for_the_year_{{$i}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Closing WDV</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->closing_wdv ?? ' ' }}" name="closing_wdv[]" class="form-control closing_wdv" id="closing_wdv_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Sale Proceeds</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->sale_proceeds ?? ' ' }}" name="sale_proceeds[]" class="form-control sale_proceeds" id="sale_proceeds_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Profit/(Loss) on Disposal</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->profit_los ?? ' ' }}" name="profit_los[]" class="form-control profit_los" id="profit_los_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Acc. Dep on Sale</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->acc_dep_sale ?? ' ' }}" name="acc_dep_sale[]" class="form-control acc_dep_sale" id="acc_dep_sale_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Adjustment in Depreciation</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->adj_deprec ?? ' ' }}" name="adj_deprec[]" class="form-control adj_deprec" id="adj_deprec_{{$i}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">WDV As per Software</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{$asset_data[0]->wdv_as_soft ?? ' ' }}" name="wdv_as_soft[]" class="form-control wdv_as_soft" id="wdv_as_soft_{{$i}}">
                        </div>
                    </div>                       
                </div>
            </div>
        </div>
        <div class="col-md-12 padding_sm">
            <div class="box no-border">
                <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-3 padding_sm" style="margin-left: 210px;">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <span class="btn btn-block light_purple_bg save_fixed" onclick="updateFixedRegister(1)"><i class="fa fa-save"></i> Save</span>
                        </div>
                </div>
        </div>
    </form>
</div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    
    
    function updateFixedRegister(i) {
    var url = '<?= route('extensionsvalley.accounts.saveFixedAssetRegister') ?>';
    var qty = $('#qty_' + i).val();
    var original_cost = $('#original_cost_' + i).val();
    var date_purchase = $('#date_purchase_' + i).val();
    var date_first_use = $('#date_first_use_' + i).val();
    var life = $('#life_' + i).val();
    var original_cost_item = $('#original_cost_item_' + i).val();
    var date_sale = $('#date_sale_' + i).val();
    var date_last_use = $('#date_last_use_' + i).val();
    var reside_value = $('#reside_value_' + i).val();
    var bal_useful_day = $('#bal_useful_day_' + i).val();
    var opening_wdv = $('#opening_wdv_' + i).val();
    var new_addition = $('#new_addition_' + i).val();
    var disposed_asset = $('#disposed_asset_' + i).val();
    var days_of_depre = $('#days_of_depre_' + i).val();
    var deprec_for_the_year = $('#deprec_for_the_year_' + i).val();
    var closing_wdv = $('#closing_wdv_' + i).val();
    var sale_proceeds = $('#sale_proceeds_' + i).val();
    var profit_los = $('#profit_los_' + i).val();
    var acc_dep_sale = $('#acc_dep_sale_' + i).val();
    var adj_deprec = $('#adj_deprec_' + i).val();
    var wdv_as_soft = $('#wdv_as_soft_' + i).val();
    var till_used_date = $('#till_used_date_' + i).val();
    var asset_register_id = $("#asset_register_id").val();
    var params = {
            qty: qty,
            original_cost: original_cost,
            date_purchase:date_purchase,
            date_first_use:date_first_use,
            life:life,
            original_cost_item:original_cost_item,
            date_sale:date_sale,
            date_last_use:date_last_use,
            reside_value:reside_value,
            bal_useful_day:bal_useful_day,
            opening_wdv:opening_wdv,
            new_addition:new_addition,
            disposed_asset:disposed_asset,
            days_of_depre:days_of_depre,
            deprec_for_the_year:deprec_for_the_year,
            closing_wdv:closing_wdv,
            sale_proceeds:sale_proceeds,
            profit_los:profit_los,
            acc_dep_sale:acc_dep_sale,
            adj_deprec:adj_deprec,
            wdv_as_soft:wdv_as_soft,
            till_used_date:till_used_date,
            asset_register_id:asset_register_id
    };
    $.ajax({
    type: "POST",
            url: url,
            data: params,
            beforeSend: function () {
            $('.save_fixed').attr('disabled', true);
            $('#save_fixed_' + i).removeClass('fa fa-save');
            $('#save_fixed_' + i).addClass('fa fa-spinner');
            $('.codfox_container').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) {
            if (msg == 1){
            toastr.success("Successfully Updated");
            window.location = "{!! route('extensionsvalley.accounts.fixedAssetRegister') !!}";
            }
            },
            complete: function () {
            $('.save_fixed').attr('disabled', false);
            $('#save_fixed_' + i).removeClass('fa fa-spinner');
            $('#save_fixed_' + i).addClass('fa fa-save');
            $('.codfox_container').LoadingOverlay("hide");
            }, error: function () {
    toastr.error("Please Check Internet Connection");
    }
    });
    }
function clear_search(){
    $("#from_date").val('');
    $("#to_date").val('');
    window.location.reload();
}
function calculateLifeWise(){
var years_life = $("#life_1").val();
var date_first_use = $("#date_first_use_1").val();
var date_till_use = $("#till_used_date_1").val();
date_first_use  = date_first_use.split('-');
years = parseInt(date_first_use[date_first_use.length-1])+parseInt(years_life);
months = date_first_use[date_first_use.length-2];
dates = date_first_use[date_first_use.length-3];
var a = moment(dates+'-'+months+'-'+years,'M/D/YYYY');
var b = moment(date_till_use,'M/D/YYYY');
var diffDays = a.diff(b, 'days');
$("#bal_useful_day_1").val(diffDays);
$("#date_last_use_1").val(dates+'-'+months+'-'+years);
}
function costChange(e){
    var pur_cost = e;
    var dep_value= parseFloat(e*5)/100;
    $("#reside_value_1").val(dep_value);
}
function firstUsedChange(e){

}
@include('Purchase::messagetemplate')
</script>

@endsection
