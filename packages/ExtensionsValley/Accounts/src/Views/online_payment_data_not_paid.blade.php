@if($offset == 0)
<div class="row">
    <div class="col-md-12 theadscroll" id="load_data" style="position: relative; height: 501px;">
        <table class="table table_round_border theadfix_wrapper" id="receipt_table">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align:center;width:10%">Date</th>
                    <th style="text-align:center">Vendor</th>
                    <th style="text-align:center;width:35%">Bill No Details</th>
                    <th style="text-align:center;width:15%">Amount</th>
                    <th style="text-align:center;width:8%">Action</th>
                </tr>
            </thead>
            <tbody>
                @if(count($receivedData) > 0)
                @foreach($receivedData as $rec)
                <tr>
                    <td class="common_td_rules">{{ !empty($rec->invoice_date) ? $rec->invoice_date :'' }}</td>
                    <td class="common_td_rules">{{ $rec->ledger_name }}</td>
                    <td class="common_td_rules" title="{{ $rec->invoice }}">{{ $rec->invoice }}</td>
                    <td class="td_common_numeric_rules">{{ !empty($rec->amount) ? number_format($rec->amount, 2, '.', ',') :'' }}</td>
                    <td class="text-center common_td_rules">
                    <button class='btn btn-block light_purple_bg onlinePayment' type="button"  onclick="payDetails(this,'{{$rec->amount}}',{{$rec->vendor_id}},'{{ $rec->detail_id }}')">
                       <i class="fa fa-money"></i> pay
                    </button>
                </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<script>

    $('#load_data').on('scroll', function() {
        var scrollHeight = $('#load_data').height();
        //var scrollPosition = $('#load_data').height() + $('#load_data').scrollTop();
        var scrollPosition = $('#load_data').scrollTop() + $('#load_data').innerHeight();
        if(scrollPosition+3 >= $('#load_data')[0].scrollHeight){
        //if (scrollPosition > scroll_length_initial) {
            //alert('ddd');
            offset = offset+limit;
            //limit = limit+15;
            console.log(limit+'##'+offset+'##'+total_rec);
            if(offset < total_rec){
                setTimeout(function(){
                    searchList(limit,1);
                },500);
                //searchList(limit,offset);
            }
        }
    })
</script>

@else
    @if(isset($receivedData))
    @foreach($receivedData as $rec)
        <tr>
            <td class="common_td_rules">{{ !empty($rec->invoice_date) ? $rec->invoice_date :'' }}</td>
            <td class="common_td_rules">{{ $rec->ledger_name }}</td>
            <td class="common_td_rules" title="{{ $rec->invoice }}">{{ $rec->invoice }}</td>
            <td class="td_common_numeric_rules">{{ !empty($rec->amount) ? number_format($rec->amount, 2, '.', ',') :'' }}</td>
            <td class="text-center common_td_rules">
            <button class='btn btn-block light_purple_bg' type="button" onclick="payDetails(this,'{{$rec->amount}}',{{$rec->vendor_id}},'{{ $rec->detail_id }}')">
               <i class="fa fa-money"></i> pay
            </button>
        </td>
        </tr>
    @endforeach
    @else
    <tr>
        <td colspan="5">No Records Found</td>
    </tr>
    @endif
@endif
