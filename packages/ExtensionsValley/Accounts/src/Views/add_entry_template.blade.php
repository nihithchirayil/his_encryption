<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
    <thead>
        <tr class="table_header_bg">
            <th width="30%">Account Name</th>
            <th width="13%">Curr.Balance</th>
            <th width="15%">Amount</th>
            <th width="10%">&nbsp;</th>
            @if($enable_multiple_naration==1)
            <th width="20%">Naration</th>
            @endif
            <th width="3%">
                <i class="fa fa-plus" onclick="appendRawNew()" style="cursor:pointer"></i>
            </th>
        </tr>
    </thead>
    <tbody id="payment_receipt_tbody" class="accounts_tbody">
        <tr>
            <td>
                <input type="text" value=""  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event,'{{$voucher_type_id}}','All')" id="ledger_item_desc-1">
                <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-1">
                <input type="hidden" value="" id="group_id_1" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-1"
                     style="text-align: left; list-style: none;  cursor: pointer;
                     margin-top: 14px !important;margin-left: 370px !important;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
            </td>
            <td>
                <input type="text" readonly="" value="" name="acc_no[]" style="border:none">
            </td>
            <td>
                <input type="text" value="" name="amnt[]" class="form-control amount" id="amount_1" onkeyup="calculate_table_total('amount');number_validation(this)" onblur="calculate_table_total('amount');">
            </td>
            <td>
                @if($voucher_type_id == 18 || $voucher_type_id == 30)
                <select name="detail_cr[]" class="form-control detail_cr" id="detail_cr_1" onchange="current_dr_cr(this);calculate_table_total('amount')">
                    <option value="cr">Credit</option>
                    <option value="dr">Debit</option>
                </select>
                @else
                <select name="detail_cr[]" class="form-control detail_cr" id="detail_cr_1" onchange="current_dr_cr(this);calculate_table_total('amount')">
                    <option value="dr">Debit</option>
                    <option value="cr">Credit</option>
                </select>
                @endif
            </td>
            @if($enable_multiple_naration==1)
            <td>
                <input type="text" value="" name="multiple_naration[]" class="form-control multiple_naration" id="multiple_naration_1">
            </td>
            @endif
            <td>
                <i class="fa fa-trash-o delete_font_aws delete_return_entry_added" style="cursor:pointer"></i>
            </td>

        </tr>

    </tbody>
</table>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
    <div class="date custom-float-label-control">
        <label class="custom_floatlabel">Total Amount</label>
        <input type="text" id="total_amount_bottom" class="form-control" readonly="" value="" >
    </div>
</div>
<div class="col-xs-4 padding_sm pull-right" style="padding-bottom: 10px;">
    <div class="date custom-float-label-control">
        <div class="clearfix"></div>
        <div class="ht10"></div>
        <label class="custom_floatlabel" >Total Difference </label>
        <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label"></h4></label>
        <input type="hidden" id="ttl_diff" class="form-control" readonly="" value="" >
    </div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-md-8 padding_sm">
    <div class="mate-input-box">
        <label>Narration</label>
        <div class="clearfix"></div>
        <input type="text" value="" id="naration" name="naration" autocomplete="off" class="form-control">
    </div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-xs-12 padding_sm pull-right" style="padding-bottom: 10px;">
    <span type="button" class="btn btn-success pull-right saving_div_class" onclick="saveForm(1)">
        <i class="fa fa-save" id="add_share_holder_spin"></i> Save</span>
    <span type="button" class="btn btn-info pull-right saving_div_class" onclick="saveForm(0)">
        <i class="fa fa-thumbs-up" id="add_share_holder_spin_t"></i> Save & continue</span>
    <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
        <i class="fa fa-times" id="add_share_holder_spin" ></i>Cancel</span>
    <span type="button" class="btn btn-default pull-right" onclick="refreshAllData()" >
        <i class="fa fa-refresh" id="add_share_holder_spin" ></i>Refresh</span>
        <span type="button" class="btn btn-default pull-left" onclick="printAllData()" >
        <i class="fa fa-print" id="add_print_spin" ></i>Print</span>

        <span type="button" style="display: none" class="btn btn-success pull-right send_for_approvel" onclick="sendApproveForAdd()">
            <i class="fa fa-paper-plane"></i></span>
        <span type="button" style="display: none" class="btn btn-warning pull-right send_for_approvel_requested" >
                <i class="fa fa-thumbs-up"></i> Requested</span>
</div>




