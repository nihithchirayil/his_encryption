@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }
    .disabled { color: #999; }
    .enabled { color: #000000; }
    .mate-input-box input.form-control{
        margin-top:8px;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Request List</strong></h4>
            <div class="col-md-12 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label>Voucher No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="" id="voucher_no" class="form-control voucher_no">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label>Status</label>
                                <select class="form-control" id="select_status">
                                    <option value="0">Select</option>
                                    <option value="1">Request</option>
                                    <option value="2">Approve</option>
                                    <option value="3">Reject</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label>Voucher Type</label>
                                <select class="form-control" id="select_voucher">
                                    <option value="0">Select</option>
                                    <option value="1">Purchase</option>
                                    <option value="2">Sales</option>
                                    <option value="3">Journal</option>
                                    <option value="5">Receipt</option>
                                    <option value="6">Credit Note Import</option>
                                    <option value="7">Sales Import</option>
                                    <option value="8">Receipt Import</option>
                                    <option value="9">Contra</option>
                                    <option value="10">Credit Note</option>
                                    <option value="11">Debit Note</option>
                                    <option value="12">Payment</option>
                                    <option value="13">Insurance Journal</option>
                                    <option value="14">test</option>
                                    <option value="15">Journal Bank Payment</option>
                                    <option value="16">Cash Book</option>
                                    <option value="17">Journal Cash Payment</option>
                                    <option value="18">Journal Bank Receipt</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label>Post Type</label>
                                <select class="form-control" id="select_post_type">
                                    <option value="0">Select</option>
                                    <option value="1">Department Wise Collection</option>
                                    <option value="2">Registration</option>
                                    <option value="3">Discharge</option>
                                    <option value="4">OT</option>
                                    <option value="5">Pharmacy Sales</option>
                                    <option value="6">General Store Purchase</option>
                                    <option value="7">Pharmacy Store Purchase</option>
                                    <option value="8">Sales Return</option>
                                    <option value="9">IP Store Purchase</option>
                                    <option value="11">Pharmacy Sales (IP)</option>
                                    <option value="12">Pharmacy Sales Return (IP)</option>
                                    <option value="100">Debit Note</option>
                                    <option value="20">Advance Refund</option>
                                    <option value="102">Receivables Dept</option>
                                    <option value="101">Receivable collection</option>
                                    <option value="103">Receivables OT</option>
                                    <option value="104">Receivables Discharge</option>
                                    <option value="105">Receivables Pharmacy(IP)</option>
                                </select>
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" >

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script
src="{{ asset('packages/extensionsvalley/master/default/javascript/request_list.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
