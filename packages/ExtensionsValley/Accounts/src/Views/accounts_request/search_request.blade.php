<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
    .wrapper {
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
}

.circle {
  position: relative;
  background: #00B01D;
  border-radius: 100%;
  height: 120px;
  width: 120px;
}

.checkMark {
  position: absolute;
  transform: rotate(50deg) translate(-50%, -50%);
  left: 27%;
  top: 43%;
  height: 60px;
  width: 25px;
  border-bottom: 5px solid #fff;
  border-right: 5px solid #fff;
}
</style>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="20%">Voucher No</th>
                <th width="17%">Voucher Type / Post Type</th>
                <th width="18%">Remarks</th>
                <th width="18%">Invoice Date</th>
                <th style = "width:5%"><i class="fa fa-check"></i></th>
                <th style = "width:5%"><i class="fa fa-times"></i></th>
            </tr>
        </thead>
        <tbody>
            @if(count($data) > 0)
                @foreach ($data as $each)
               
                    <tr style="cursor: pointer;" >
                        <td class = "td_common_numeric_rules">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $each->voucher_no}}</td>
                        <td class ="common_td_rules">@if($each->is_post == 1) {{ $each->post_type}} @else {{ $each->type}} @endif</td>
                        <td class ="common_td_rules">{{ $each->remarks}}</td>
                        <td class ="common_td_rules">@if($each->invoice_date !=''){{ date('M-d-Y', strtotime($each->invoice_date)) }} @endif</td>
                        @php
                        $class = '' ; $apphead ='';
                        $color = '';  $rejhead ='';
                        $color1 = '';
                        if($each->status_name == 2) {
                            $class ="fa-check";
                            $color = "style = background:green";
                            $apphead = "Approved";
                        }else {
                            $class ="fa-edit";
                            $apphead = "Approve";
                        }
                        if($each->status_name == 3){
                            $color1 = "style=background:red";
                            $rejhead = "Rejected";
                        }else{
                            $rejhead = "Reject";
                        }
                      
                        @endphp
                        <td>         
                            @if($each->status_name == 2)                    
                            <button type="button" id="button" {{ $color }} class="btn btn-sm btn-primary" onclick="ApproveReq(this,'{{$each->id}}',1);" ><i class = "fa {{ $class }}"></i> {{ $apphead }}</button>
                            @elseif($each->status_name != 2 && $each->status_name != 3)
                            <button type="button" id="button" {{ $color }} class="btn btn-sm btn-primary" onclick="ApproveReq(this,'{{$each->id}}',1);" ><i class = "fa {{ $class }}"></i> {{ $apphead }}</button>
                            @endif
                        </td>
                        <td>
                            @if($each->status_name == 3)
                            <button type="button" id="button" {{ $color1 }} class="btn btn-sm btn-warning " onclick="RejectReq(this,'{{$each->id}}',2);" ><i class="fa fa-times"></i> {{ $rejhead }}</button>
                            @elseif($each->status_name != 2 && $each->status_name != 3)
                            <button type="button" id="button" {{ $color1 }} class="btn btn-sm btn-warning " onclick="RejectReq(this,'{{$each->id}}',2);" ><i class="fa fa-times"></i> {{ $rejhead }}</button>
                            @endif
                        </td>
                           
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
