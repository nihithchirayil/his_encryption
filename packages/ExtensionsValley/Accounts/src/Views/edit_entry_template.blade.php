<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
    <thead>
        <tr class="table_header_bg">
            <th width="40%">Account Name</th>
            <th width="3%">Cur.Balance</th>
            <th width="15%">Amount</th>
            <th width="10%">&nbsp;</th>
            @if($enable_multiple_naration==1)
            <th width="20%">Naration</th>
            @endif
            <th width="3%">
                <i class="fa fa-plus" onclick="appendRawNew()" style="cursor:pointer"></i>
            </th>
        </tr>
    </thead>
    <tbody id="payment_receipt_tbody" class="accounts_tbody">
        @php $i=1; $total_amount = 0;@endphp
        @if(isset($header[0]->voucher_type) && $header[0]->voucher_type == 9)
          @php $header_voucher_type = '9'; @endphp
        @else
        @php $header_voucher_type = 'all'; @endphp
        @endif

        @if(isset($detail)&& sizeof($detail) > 0)
        @foreach($detail as $dt)
        @if($dt->cr_dr == 'cr')
            @php $tr_class = 'cr'; @endphp
        @else
            @php $tr_class = 'dr'; @endphp
        @endif
        <tr id="edit_ledger_booking_{{$i}}" class="{{$tr_class??''}}">
            <td>
                <input type="text" value="{{$dt->ledger_name ?? ''}}"  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, '{{$header_voucher_type}}')" id="ledger_item_desc-{{$i}}">
                <input type="hidden" value="{{$dt->ledger_id ?? ''}}" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-{{$i}}">
                <input type="hidden" value="{{$dt->group_id ?? ''}}" id="group_id_{{$i}}" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                <input type="hidden" value="{{$dt->id}}" id="detail_id_{{$i}}" name=detail_id[]"  autocomplete="off"  class="form-control">
                <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-{{$i}}"
                     style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;
                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
            </td>
            <td>
                @if(isset($dt->current_balance) && $dt->current_balance < 0)
                <input type="text" readonly="" value="{{abs($dt->current_balance)}} .Dr" name="acc_no[]" style="border:none">
               @else
                <input type="text" readonly="" value="{{abs($dt->current_balance)}} .Cr" name="acc_no[]" style="border:none">
                @endif
            </td>
            <td>
                <input type="text" value="{{$dt->amount ?? ''}}" name="amnt[]" class="form-control amount" id="amount_{{$i}}" onkeyup="calculate_table_total('amount');" onblur="calculate_table_total('amount');">
            </td>
            <td>
              <select name="detail_cr[]" class="form-control detail_cr" id="detail_cr_{{$i}}" onchange="current_dr_cr(this);calculate_table_total('amount')">
                    <?php if($dt->cr_dr == 'dr'){ ?>
                        <option value="dr" selected>Debit</option>
                        <option value="cr">Credit</option>
                    <?php }else{ ?>
                        <option value="dr" >Debit</option>
                        <option value="cr" selected>Credit</option>
                    <?php  } ?>
                </select>
            </td>
            @if($enable_multiple_naration==1)
            <td>
                <input type="text" value="{{$dt->naration ?? ''}}" name="multiple_naration[]" class="form-control multiple_naration" id="multiple_naration_{{$i}}">
            </td>
            @endif
            <td>
                <i class="fa fa-trash-o delete_font_aws " style="cursor:pointer" onclick="deleteBookingEntry('{{$dt->id}}',this)"></i>
            </td>

        </tr>
        <?php
        $i++;
        if($header[0]->cr_dr == 'cr'){
            if($dt->cr_dr == 'dr') {
                $total_amount += $dt->amount;
            }else{
                $header_amount += $dt->amount;
            }
        }else{
            if($dt->cr_dr == 'cr') {
                $total_amount += $dt->amount;
            }else{
                $header_amount += $dt->amount;
            }
        }
        ?>
        @endforeach
        @endif
        <tr>
            <td>
                <input type="text" value=""  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, '{{$header_voucher_type}}')" id="ledger_item_desc-{{$i}}">
                <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-{{$i}}">
                <input type="hidden" value="" id="group_id_{{$i}}" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-{{$i}}"
                     style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px;
                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 28% !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
            </td>
            <td>
                <input type="text" readonly="" value="" name="acc_no[]" style="border:none">
            </td>
            <td>
                <input type="text" value="" name="amnt[]" class="form-control amount" id="amount_{{$i}}" onkeyup="calculate_table_total('amount');" onblur="calculate_table_total('amount');">
            </td>
            <td>
                <select name="detail_cr[]" class="form-control detail_cr" id="detail_cr_{{$i}}" onchange="current_dr_cr(this);calculate_table_total('amount')">
                    <option value="dr">Debit</option>
                    <option value="cr">Credit</option>
                </select>
            </td>
            @if($enable_multiple_naration==1)
            <td>
                <input type="text" value="" name="multiple_naration[]" class="form-control multiple_naration" id="multiple_naration_{{$i}}">
            </td>
            @endif
            <td>
                <i class="fa fa-trash-o delete_font_aws delete_return_entry_added" style="cursor:pointer"></i>
            </td>

        </tr>
    </tbody>
</table>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
    <div class="date custom-float-label-control">
        <label class="custom_floatlabel">Total Amount</label>
        <input type="text" id="total_amount_bottom" class="form-control" readonly="" value="{{$total_amount}}" >
    </div>
</div>
<div class="col-xs-4 padding_sm pull-right" style="padding-bottom: 10px;">
    <div class="date custom-float-label-control">
        <div class="clearfix"></div>
        <div class="ht10"></div>
        <label class="custom_floatlabel" >Total Difference </label>
        <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label">{{round(abs($header_amount-$total_amount),2)}}</h4></label>
        <input type="hidden" id="ttl_diff" class="form-control" readonly="" value="{{round(abs($header_amount-$total_amount),2)}}" >
    </div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-md-8 padding_sm">
    <div class="mate-input-box">
        <label>Narration</label>
        <div class="clearfix"></div>
        <input type="text" value="{{$header[0]->naration??''}}" id="naration" name="naration" autocomplete="off" class="form-control">
    </div>
</div>
<div class="clearfix"></div>
<div class="ht10"></div>
<div class="col-xs-12 padding_sm pull-right" style="padding-bottom: 10px;">
    @php
        $block_greater30days = @$block_greater30days ? $block_greater30days : 0;
        $status = @$approved_status[0]->status ? $approved_status[0]->status : 0;
        $updated_status = @$approved_status[0]->updated_status ? $approved_status[0]->updated_status : 0;
    @endphp
    @if($block_greater30days == 1)
    @if(($days < 30 || $status == 2) && $updated_status == 0)
    <span type="button" class="btn btn-success pull-right" onclick="updateForm(1)">
        <i class="fa fa-save" id="add_share_holder_spin"></i> Update</span>
    <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
        <i class="fa fa-times" id="add_share_holder_spin" ></i>Cancel</span>
    <span type="button" class="btn btn-default pull-right" onclick="refreshAllData()" >
        <i class="fa fa-refresh" id="add_share_holder_spin" ></i>Refresh</span>
    @endif
    @else
    <span type="button" class="btn btn-success pull-right" onclick="updateForm(1)">
        <i class="fa fa-save" id="add_share_holder_spin"></i> Update</span>
    <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
        <i class="fa fa-times" id="add_share_holder_spin" ></i>Cancel</span>
    <span type="button" class="btn btn-default pull-right" onclick="refreshAllData()" >
        <i class="fa fa-refresh" id="add_share_holder_spin" ></i>Refresh</span>
    @endif
    <span type="button" class="btn btn-default pull-left" onclick="printAllData()" >
        <i class="fa fa-print" id="add_print_spin" ></i>Print</span>
</div>



