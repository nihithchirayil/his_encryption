<div class="theadscroll" style="position: relative; height: 430px;">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg" style="cursor: pointer;">
                <th width="20%" style="text-align: left;">Company Name</th>
                <th width="20%" style="text-align: left;">Ledger Name</th>
                <th width="20%" style="text-align: left;">Patient Name</th>
                <th width="20%" style="text-align: left;">Admitted date</th>
                <th width="20%" style="text-align: left;">Discharge date</th>
                <th width="10%" style="text-align: left;">Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $mapped_status = '';
            if (count($post_data) != 0) {
                $total = 0.0;
                foreach ($post_data as $each) {
                    $mapped_class = '';
                    if (strpos($each->ledger_name, 'Ledger not mapped') !== false) {
                        $mapped_class = 'text-red';
                        $mapped_status = 1;
                    }

                    $total += floatval($each->advance_collected);
                    ?>
                    <tr class="<?= $mapped_class ?>">
                        <td class="common_td_rules" title="<?= $each->company_name ?>"><?= $each->company_name ?></td>
                        <td class="common_td_rules" title="<?= $each->ledger_name ?>"><?= $each->ledger_name ?></td>
                        <td class="common_td_rules" title="<?= $each->patient_name ?>"><?= $each->patient_name. '('.$each->uhid. ')' ?></td>
                        <td class="common_td_rules" title="<?= $each->admission_date ?>"><?= $each->admission_date ?></td>
                        <td class="common_td_rules" title="<?= $each->discharge_date ?>"><?= $each->discharge_date ?></td>
                        <td class="td_common_numeric_rules"><?= number_format($each->advance_collected, 2) ?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <th class="common_td_rules" >Total</th>
                    <th class="td_common_numeric_rules" colspan="5"><?= number_format($total, 2) ?></th>

                </tr>
                <?php
            } else {
                $mapped_status = 1;
                ?>
                <tr class="common_td_rules">
                    <td colspan="3" style="text-align: center;"> No Result Found</td>
                </tr>
                <?php
            }
            ?>

        </tbody>
    </table>
</div>
<?php
if (!$mapped_status) {
    ?>
    <div class="col-md-1 padding_sm pull-right">
        <button type="button" onclick="insertInsuranceData()" id="nextsequencedatabtn"
                class="btn btn-success btn-block">Post SW
            <i id="nextsequencedataspin" class="fa fa-forward"></i></button>
    </div>
    <?php
}
?>
