<script type="text/javascript">
    $(document).ready(function () {
        $(".page-link").click(function () {
            var url = $(this).attr("href");
            var doctor_name_search = $('#doctor_name_search').val();
            var departmet_name_search = $('#departmet_name_search').val();
            $.ajax({
            type: "GET",
                url: url,
                data: "doctor_name_search="+doctor_name_search+"& departmet_name_search="+departmet_name_search,
                beforeSend: function () {
                    $('#searchdatabtn').attr('disabled',true);
                    $('#searchdataspin').removeClass('fa fa-search');
                    $('#searchdataspin').addClass('fa fa-spinner');
                    $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (msg) { 
                    $('#generilistdiv').html(msg);
                },
                complete: function () {
                    $('#searchdatabtn').attr('disabled',false);
                    $('#searchdataspin').removeClass('fa fa-spinner');
                    $('#searchdataspin').addClass('fa fa-search');
                    $('#generilistdiv').LoadingOverlay("hide");
                },error: function(){
                    toastr.error("Please Check Internet Connection");
                }
            });
            return false;
        });

    });

</script>
<div class="box-body clearfix">
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="30%">Doctor Name</th>
                <th width="30%">Department Name</th>
                <th width="30%">Percentage</th>
                <th width="5%">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr style="background-color: #f1dcb7">
                <td class="common_td_rules" >
                    <select class="form-control select2" style='color:#555555;' name="doctor_id" id="doctor_id">
                                <option value="">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                </td>
                <td class="common_td_rules">
                    <select class="form-control select2" style='color:#555555;' name="department_id" id="department_id">
                                <option value="">Select Department</option>
                                   <?php foreach($department_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                    </select>
                </td>
                <td class="common_td_rules">
                    <input type="text" class="form-control" name="percentage" id="percentage" onkeyup="number_validation(this)">
                </td>
                <td class="text-center">
                    <button class='btn btn-block btn-info savebtn_0' type="button" onclick="saveBillAnalysis()" id="savebtn1"><i class="fa fa-save"></i></button>
                </td>
            </tr>
            <?php
            if (count($bill_analysis_list) != 0) {
                foreach ($bill_analysis_list as $list) {
                    ?>
            <tr class='{{$list->bill_analysis_id}}'>
                <td class="common_td_rules" >
                    <select class="form-control select2" style='color:#555555;' id="doctor_{{$list->bill_analysis_id}}">
                                   <?php foreach($doctor_master as $key=>$val){
                                       if($key==$list->doctor_id){
                                       ?>
                                        <option value="<?=$key?>" selected=""><?=$val?></option>
                                       <?php
                                       }else{ ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php }
                                   }
                                   ?>
                                </select>
                </td>
                <td class="common_td_rules">
                                <select class="form-control select2" style='color:#555555;' id="department_{{$list->bill_analysis_id}}">
                                   <?php foreach($department_master as $key=>$val){
                                       if($key == $list->department_name){
                                       ?>
                                        <option value="<?=$key?>" selected=""><?=$val?></option>
                                       <?php
                                       }else{ ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php }
                                   }
                                   ?>
                                </select>
                </td>
                <td id="procedure_{{$list->bill_analysis_id}}" class="td_common_nummeric_rules">
                    <input type="text" value="{{ $list->percentage }}" class="form-control" id="percentage_{{$list->bill_analysis_id}}" onkeyup="number_validation(this)">
                    
                </td>
                <td id="{{$list->bill_analysis_id}}" class="text-center">
                    <button class='btn btn-block light_purple_bg savebtn_{{$list->bill_analysis_id}}' type="button" onclick="updateBillAnalysis('{{$list->bill_analysis_id}}')"><i class="fa fa-thumbs-up"></i></button>
                </td>
            </tr>
            <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="5" class="re-records-found">No Records found</td>
                </tr>
                <?php
            }
            ?>
            
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(".select2").select2({
         placeholder: "",
         width: '100%'
     });
    </script>