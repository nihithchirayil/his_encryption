<?php  $dept_show = $doct_show ="";
if($department_name != '0' && $doctor_name == '0'){
    $dept_show ="display:none";
    $doct_show ="display:''";
}
if($department_name == '0' && $doctor_name != '0'){
    $dept_show ="display:''";
    $doct_show ="display:none";   
}else if($department_name == '0' && $doctor_name != '0'){
    $doct_show ="display:''";
    $dept_show ="display:''";
}
?>
<div class="box-body clearfix">
    <div class="append_html">
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="30%" style="{{$doct_show}}">Doctor Name</th>
                <th width="30%" style="{{$dept_show}}">Service Name</th>
                <th width="30%" >Percentage</th>
            </tr>
        </thead>
        <tbody id="payment_receipt_tbody">
  
            <?php
            if (count($bill_analysis_list) != 0) {
                foreach ($bill_analysis_list as $list) {
                    ?>
            <tr class='{{$list->procedure_share_id}}'>
                <td class="common_td_rules" style="{{$doct_show}}">
                    <select class="form-control select2" style='color:#555555;' id="doctor_{{$list->procedure_share_id}}" name="doctor_id[]">
                        <option value="0">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       if($key==$list->doctor_id){
                                       ?>
                                        <option value="<?=$key?>" selected=""><?=$val?></option>
                                       <?php
                                       }else{ ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php }
                                   }
                                   ?>
                                </select>
                </td>
                <td class="common_td_rules" style="{{$dept_show}}">
                                <select class="form-control select2" style='color:#555555;' id="department_{{$list->procedure_share_id}}" name="department_id[]">
                                   <option value="0">Select Service</option>
                                    <?php foreach($department_master as $key=>$val){
                                       if($key == $list->service_name){
                                       ?>
                                        <option value="<?=$key?>" selected=""><?=$val?></option>
                                       <?php
                                       }else{ ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php }
                                   }
                                   ?>
                                </select>
                </td>
                <td id="procedure_{{$list->procedure_share_id}}" class="td_common_nummeric_rules">
                    <input type="text" value="{{ $list->percentage }}" class="form-control" id="percentage_{{$list->procedure_share_id}}" name="percentage[]" onkeyup="number_validation(this)">
                    
                </td>
            </tr>
            <?php
                }
            }
            ?>
            <tr style="background-color: #f1dcb7" id="new_row">
                <td class="common_td_rules" style="{{$doct_show}}">
                    <select class="form-control select2" style='color:#555555;' name="doctor_id[]" id="doctor_id_0">
                                <option value="0">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                </td>
                <td class="common_td_rules" style="{{$dept_show}}">
                    <select class="form-control select2" style='color:#555555;' name="department_id[]" id="department_id_0">
                                <option value="0">Select Service</option>
                                   <?php foreach($department_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                    </select>
                </td>
                <td class="common_td_rules">
                    <input type="text" class="form-control" name="percentage[]" id="percentage" onkeyup="number_validation(this);nextrow(this)">
                </td>
            </tr>
        </tbody>
    </table>
 
</div>
</div>
    <div class="clearfix"></div>
<div class="row" style="background-color: #e3ebdf;padding-top: 10px">
   <div class="col-md-2 padding_sm pull-right">
<label for="">&nbsp;</label>
 <div class="clearfix"></div>
<button class='btn btn-block btn-info savebtn_0 pull-right' type="button" onclick="saveBillAnalysis()" id="savebtn1"><i class="fa fa-save"></i> Save</button>
</div>
    </div>
<script type="text/javascript">
    $(".select2").select2({
         placeholder: "",
         width: '100%'
     });
     $('.datepicker').datetimepicker({
        format: 'MMM-YYYY',
    });
    </script>