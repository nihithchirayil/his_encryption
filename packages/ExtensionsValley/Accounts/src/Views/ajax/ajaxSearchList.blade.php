<?php
if (isset($ledger_data)) {
    if (isset($ledger_type) && $is_header == '1') {
        $fn_name = 'fillParty';
    } else if($ledger_type=='journal'){
        $fn_name = 'fillJournalValues';
    }else {
        $fn_name = 'fillLedgerValues';
    }
    if (!empty($ledger_data)) {
        foreach ($ledger_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='<?= $fn_name ?>(this,"{{htmlentities($desc->ledger_name)}}","{{htmlentities($desc->id)}}",
                "{{htmlentities($desc->ledger_group_id)}}","{{htmlentities($desc->ledger_code)}}","{{htmlentities($desc->current_balance)}}","{{htmlentities($desc->allow_multiple_entry)}}")' class="list_hover">
               <?= $desc->ledger_name ?>
            </li>
            <?php
        }
    } else {
        if (isset($from_ledger_booking)) {
            ?>
            <span type="button" class="btn btn-block btn-primary" onclick="saveNewLedger(this)" style="margin-bottom: 0px;">
                <i class="fa fa-plus" id="add_new_ledger_spin"></i> Add new ledger</span>
        <?php
        } else {
            echo 'No Result Found';
        }
    }
}
if (isset($invoice_data)) {

    if (!empty($invoice_data)) {
        foreach ($invoice_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillInvoice(this,"{{htmlentities($desc->invoice_no)}}","{{htmlentities($desc->ledger_name)}}","{{htmlentities($desc->id)}}",
                "{{htmlentities($desc->ledger_group_id)}}","{{htmlentities($desc->amount)}}")' class="list_hover">
                {!! $desc->invoice_no !!}
            </li>
            <?php
        }
    }
}
if (isset($ledgerDetails)) {
    if (!empty($ledgerDetails)) {

        foreach ($ledgerDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLedgerDetails(this,"{{htmlentities($data->id)}}","{{htmlentities($data->ledger_name)}}")' class="list_hover">
                {!! $data->ledger_name !!}<span style="color:red;padding-left: 5px">[{{$data->ledger_code}}]</span>
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($ledgerDetailsMap)) {
    if (!empty($ledgerDetailsMap)) {

        foreach ($ledgerDetailsMap as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLedgerDetails(this,"{{htmlentities($data->id)}}","{{htmlentities($data->ledger_name)}}")' class="list_hover">
                {!! $data->ledger_name !!}<span style="color:red;padding-left: 5px">[{{$data->ledger_group}}]</span>
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($deptDetails)) {
    if (!empty($deptDetails)) {

        foreach ($deptDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDepartmentValues(this,"{{htmlentities($data->id)}}","{{htmlentities($data->name)}}")' class="list_hover">
                {!! $data->name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($specDetails)) {
    if (!empty($specDetails)) {

        foreach ($specDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSpecialityValues(this,"{{htmlentities($data->id)}}","{{htmlentities($data->name)}}")' class="list_hover">
                {!! $data->name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($ledger_data_for_voucher)) {
        $fn_name = 'fillBankForVoucher';

    if (!empty($ledger_data_for_voucher)) {
        foreach ($ledger_data_for_voucher as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='<?= $fn_name ?>(this,"{{htmlentities($desc->ledger_name)}}","{{htmlentities($desc->id)}}",
                "{{htmlentities($desc->ledger_group_id)}}","{{htmlentities($desc->ledger_code)}}","{{htmlentities($desc->current_balance)}}","{{htmlentities($desc->allow_multiple_entry)}}")' class="list_hover">
               <?= $desc->ledger_name ?>
            </li>
            <?php
        }
    } else {
        if (isset($from_ledger_booking)) {
            ?>
            <span type="button" class="btn btn-block btn-primary" onclick="saveNewLedger(this)" style="margin-bottom: 0px;">
                <i class="fa fa-plus" id="add_new_ledger_spin"></i> Add new ledger</span>
        <?php
        } else {
            echo 'No Result Found';
        }
    }
}
?>
