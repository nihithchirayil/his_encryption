<script type="text/javascript">
    $(document).ready(function () {
        $(".page-link").click(function () {
            var url = $(this).attr("href");
            var voucher_no_search = $('#voucher_no_search').val();
            var voucher_type = $('#voucher_type').val();
            $.ajax({
            type: "GET",
                url: url,
                 data: "voucher_no_search="+voucher_no_search+"&voucher_type="+voucher_type,
                beforeSend: function () {
                    $('#searchdatabtn').attr('disabled',true);
                    $('#searchdataspin').removeClass('fa fa-search');
                    $('#searchdataspin').addClass('fa fa-spinner');
                    $('#common_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (msg) {
                    $('#common_list_div').html(msg);
                },
                complete: function () {
                    $('#searchdatabtn').attr('disabled',false);
                    $('#searchdataspin').removeClass('fa fa-spinner');
                    $('#searchdataspin').addClass('fa fa-search');
                    $('#common_list_div').LoadingOverlay("hide");
                },error: function(){
                    toastr.error("Please Check Internet Connection");
                }
            });
            return false;
        });

    });
function requesttoedit(head_id,voucher_type,voucher_no,inv_date){
    var head_id = $('#head_id').val(head_id);
    var voucher_type = $('#voucher_type').val(voucher_type);
    var voucher_no = $('#voucher_no').val(voucher_no);
     $('#inv_date_approval').val(inv_date);
    $('#remarks').val('');

    $("#getrequstapproveModel").modal({
                backdrop: 'static',
                keyboard: false
            });
}
function saverequesttoedit(){
    var head_id = $('#head_id').val();
    var voucher_type = $('#voucher_type').val();
    var voucher_no = $('#voucher_no').val();
    var remarks = $('#remarks').val();
    var inv_date = $('#inv_date_approval').val();
    var params = {head_id: head_id,voucher_type : voucher_type,voucher_no : voucher_no,remarks : remarks,vaoucher_date:inv_date};
         var url = $("#ins_base_url").val();
         if($("#remarks").val() == ''){
            Command: toastr["warning"]("Please enter Remarks");
            return;
        }
        $.ajax({
            type: "POST",
            url: url + "/accounts/requesttoedit",
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                toastr.success('Requested Succcessfuly !!');
                searchList();
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
}
function BillEntryEdit(head_id,voucher_type){
    window.location = "{!! route('extensionsvalley.accounts.editLedgerBooking') !!}/" + head_id+'/'+voucher_type
}
function BillEntryDelete(head_id,voucher_type){
    if (confirm("Are you sure you want to Delete ?")) {
        var params = {head_id: head_id};
         var url = $("#ins_base_url").val();
        $.ajax({
            type: "GET",
            url: url + "/accounts/fullEntryDelete",
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                if (data == 1) {
                    $.LoadingOverlay("hide");
                    $('#' + head_id).remove();
                    toastr.success('Deleted Succcessfuly !!');
                }else{
                    $.LoadingOverlay("hide");
                    toastr.error(data);
                }
            },
            complete: function () {

            }
        });
    }
}
</script>
<div class="box-body clearfix">
<div class="theadscroll" style="position: relative; height: 400px;">
    @php
    $block_greater30days = $block_greater30days ? $block_greater30days : 0;
    @endphp
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="20%">Voucher No</th>
                @if($voucher_type !=1 )
                <th width="20%">Date</th>
                @endif
                @if(($voucher_type ==15 || $voucher_type ==18) && $bank_voucher_generated == 1 )
                <th width="20%">Account Name</th>
                @endif
                @if($voucher_type==1 )
                <th width="20%">Name</th>
                <th width="20%">Invoice No</th>
                <th width="20%">Invoice Date</th>
                <th width="20%">Approve Date</th>
                @endif
                <th width="15%">Amount</th>
                <th width="5%">Edit</th>
                <th width="5%"><i class="fa fa-history"></i></th>
                @if($block_greater30days == 1)
                <th width="5%">Request</th>
                @endif
                <th width="5%">Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($common_list) != 0) {
                foreach ($common_list as $list) {
                    $current_date = date('M-d-Y');
                    $current_date = new \DateTime($current_date);
                    $invoice_date = $list->invoice_date;
                    $invoice_date = new \DateTime($invoice_date);
                    $interval = $invoice_date->diff($current_date);
                    $days = $interval->days + 1;
                    $style = '';
                    $reqname ='';
                    $status_name = \DB::table('approve_account_details')->where('head_id',$list->head_id)->orderby('id','desc')->limit(1)->get();
                    $status = @$status_name[0]->status ? $status_name[0]->status : 0;
                    $updated_status = @$status_name[0]->updated_status ? $status_name[0]->updated_status : 0;
                    if($status == 1 && $updated_status == 0){
                        $style = 'disabled';
                        $reqname = 'Req Pending';
                    }else if($status == 2 && $updated_status == 0){
                        $style = 'disabled';
                        $reqname = 'Approved';
                    }
                    ?>
            <tr id="{{$list->head_id}}">
                <td class="category_name common_td_rules">{{ $list->voucher_no }}</td>
                @if($voucher_type ==3 )
                <td class="status common_td_rules"><?= date('d-M-Y',strtotime($list->invoice_date)) ?></td>
                @elseif($voucher_type !=1 )
                <td class="status common_td_rules"><?= date('d-M-Y',strtotime($list->bill_entry_date)) ?></td>
                @endif
                @if(($voucher_type ==15 || $voucher_type ==18) && $bank_voucher_generated == 1 )
                <td class="status common_td_rules">{{$list->opposit_name}}</td>
                @endif
                @if($voucher_type==1)
                <td class="status common_td_rules">{{$list->ledger_name}}</td>
                <td class="status common_td_rules">{{$list->invoice_no}}</td>
                <td class="status common_td_rules">{{date('d-M-Y',strtotime($list->invoice_date))}}</td>
                 <td class="status common_td_rules">{{isset($list->purchase_bill_date)?date('d-M-Y',strtotime($list->purchase_bill_date)):''}}</td>
                @endif
                <td class="status td_common_numeric_rules "><?= number_format($list->amount, 2, '.', ',') ?></td>
                <td class="text-center">
                    <button class='btn btn-block light_purple_bg' type="button" onclick="BillEntryEdit('{{$list->head_id}}','{{$voucher_type}}')"><i class="fa fa-edit"></i></button>
                </td>
                <td class="text-center">
                    @if($list->audit_status == 1)
                        <button class='btn btn-block btn-warning' type="button" onclick="viewAuditLog('{{$list->head_id}}','ledger_booking_entry_detail','1')"><i class="fa fa-history " id="historyspin-{{$list->head_id}}" ></i></button>
                    @endif
                </td>
                @if( $block_greater30days == 1)

                <td class="text-center">
                    @if($days > 30)
                    <button class='btn btn-block light_purple_bg' title='Request' type="button" onclick="requesttoedit('{{$list->head_id}}','{{$voucher_type}}','{{ $list->voucher_no }}','{{ $list->invoice_date }}')" {{ $style }}><i class="fa fa-paper-plane requstbtn">{{ $reqname }}</i></button>
                    @endif
                </td>
                @endif
                @if(($days < 30 || $status==2 || $block_greater30days != 1) && $updated_status == 0)
                <td class="text-center">
                    <button class='btn btn-block' type="button" onclick="BillEntryDelete('{{$list->head_id}}','{{$voucher_type}}')"><i class="fa fa-trash" style="color:red"></i></button>
                </td>
                @endif
            </tr>
            <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="5" class="re-records-found">No Records found</td>
                </tr>
                <?php
            }
            ?>

        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
</div>
