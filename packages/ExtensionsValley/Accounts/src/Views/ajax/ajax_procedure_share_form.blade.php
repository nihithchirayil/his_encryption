<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function () {
        $(".page-link").click(function () {
            var url = $(this).attr("href");
            var doctor_name_search = $('#doctor_name_search').val();
            var service_name_search = $('#service_name_search').val();
            $.ajax({
            type: "GET",
                url: url,
                data: "doctor_name_search="+doctor_name_search+"& service_name_search="+service_name_search,
                beforeSend: function () {
                    $('#searchdatabtn').attr('disabled',true);
                    $('#searchdataspin').removeClass('fa fa-search');
                    $('#searchdataspin').addClass('fa fa-spinner');
                    $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (msg) { 
                    $('#generilistdiv').html(msg);
                },
                complete: function () {
                    $('#searchdatabtn').attr('disabled',false);
                    $('#searchdataspin').removeClass('fa fa-spinner');
                    $('#searchdataspin').addClass('fa fa-search');
                    $('#generilistdiv').LoadingOverlay("hide");
                },error: function(){
                    toastr.error("Please Check Internet Connection");
                }
            });
            return false;
        });
    });

</script>
<?php  $dept_show = $doct_show ="";
if($service_name != '0' && $doctor_name == '0'){
    $dept_show ="display:none";
    $doct_show ="display:''";
}
if($service_name == '0' && $doctor_name != '0'){
    $dept_show ="display:''";
    $doct_show ="display:none";   
}else if($service_name == '0' && $doctor_name != '0'){
    $doct_show ="display:''";
    $dept_show ="display:''";
}
?>
<div class="theadscroll" style="position: relative; height: 400px;overflow: auto; z-index: 999">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="30%" style="{{$doct_show}}">Doctor Name</th>
                <th width="30%" style="{{$dept_show}}">Service Name</th>
                <th width="30%">Percentage</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr style="background-color: #f1dcb7" id="new_row">
                <td class="common_td_rules" style="{{$doct_show}}">
                    <select class="form-control select2" style='color:#555555;' name="doctor_id" id="doctor_id">
                                <option value="0">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                                </select>
                </td>
                <td class="common_td_rules" style="{{$dept_show}}">
                    <select class="form-control select2" style='color:#555555;' name="service_id" id="service_id">
                                <option value="0">Select Service</option>
                                   <?php foreach($service_master as $key=>$val){
                                       ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php
                                   }
                                   ?>
                    </select>
                </td>
                <td class="common_td_rules">
                    <input type="text" class="form-control" name="percentage" id="percentage" onkeyup="number_validation(this)">
                </td>
                <td class="common_td_rules">
                    <button class='btn btn-info savebtn_0' style="margin-left: 30px" type="button" onclick="saveProcedureShare()" id="savebtn1"><i class="fa fa-save"></i></button>
                </td>
            </tr>
            <?php
            if (count($procedure_share_list) != 0) {
                foreach ($procedure_share_list as $list) {
                    ?>
            <tr class='{{$list->procedure_share_id}}'>
                <td class="common_td_rules" style="{{$doct_show}}">
                    <select class="form-control select2" style='color:#555555;' id="doctor_{{$list->procedure_share_id}}">
                        <option value="0">Select Doctor</option>
                                   <?php foreach($doctor_master as $key=>$val){
                                       if($key==$list->doctor_id){
                                       ?>
                                        <option value="<?=$key?>" selected=""><?=$val?></option>
                                       <?php
                                       }else{ ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php }
                                   }
                                   ?>
                                </select>
                </td>
                <td class="common_td_rules" style="{{$dept_show}}">
                                <select class="form-control select2" style='color:#555555;' id="service_{{$list->procedure_share_id}}">
                                    <option value="0">Select Service</option>
                                   <?php foreach($service_master as $key=>$val){
                                       if($key == $list->service_name){
                                       ?>
                                        <option value="<?=$key?>" selected=""><?=$val?></option>
                                       <?php
                                       }else{ ?>
                                        <option value="<?=$key?>"><?=$val?></option>
                                       <?php }
                                   }
                                   ?>
                                </select>
                </td>
                <td id="procedure_{{$list->procedure_share_id}}" class="td_common_nummeric_rules">
                    <input type="text" value="{{ $list->percentage }}" class="form-control" id="percentage_{{$list->procedure_share_id}}" onkeyup="number_validation(this)">
                    
                </td>
                <td id="{{$list->procedure_share_id}}" class="common_td_rules">
                    <button class='btn light_purple_bg savebtn_{{$list->procedure_share_id}}' style="margin-left: 30px" type="button" onclick="updateProcedureShare('{{$list->procedure_share_id}}')"><i class="fa fa-thumbs-up"></i></button>
                </td>
            </tr>
            <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="5" class="re-records-found">No Records found</td>
                </tr>
                <?php
            }
            ?>
            
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(".select2").select2({
         placeholder: "",
         width: '100%'
     });
    </script>