@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<style>
.select2-container--default .select2-selection--single, .select2-container--default .select2-selection--multiple{
    min-height: 23px !important;
}
.select2-container {
width: 100% !important;
padding: 0;
}
.select2-container .select2-selection--single {
    height: 23px !important;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b{
    margin: -4px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 12px !important;
}
table tr td span {
    line-height: 24px !important;
}
</style>
@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">

    	        <div class="col-md-12 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <div class="col-md-12 no-padding headerInputCheck">
                                @if($type > 0)
                                <div class="col-xs-2 padding_xs">
                                    <label>Request No.</label>
                                    <input type="text" class="form-control" autofocus="" id="" name="" placeholder="Request No." autocomplete="off">
                                </div>
                                <div class="col-md-2 padding_xs">
                                    <label>Date And Time</label>
                                    <input class="form-control" placeholder="Date And Time" readonly="" value="{{date('d-M-Y h:i:s')}}" name="" id="" type="text">
                                </div>
                                @else
                                <div class="col-xs-2 padding_xs"></div>
                                <div class="col-xs-2 padding_xs"></div>
                                @endif
                                <div class="col-md-2 padding_sm">
                                    <label>Department</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="department">
                                        <option value="11">Department 1</option>
                                        <option value="12">Department 2</option>
                                    </select>
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <label>Store</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="store">
                                        <option value="11">Store 1</option>
                                        <option value="12">Store 2</option>
                                    </select>
                                </div>
                                <div class="col-md-1 padding_sm text-center" style="margin-top: 20px;">
                                    <div class="custom-float-label-control">
	                                    <span class="label label-default">
                                        @if($type == 0)
                                        New
                                        @elseif($type == 1)
                                        Drafted
                                        @elseif($type == 2)
                                        Post For Verification
                                        @elseif($type == 3)
                                        Verifyed
                                        @elseif($type == 4)
                                        Approved
                                        @endif
                                        </span>
	                                </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 20px;">
                                    <button type="button"  class="btn btn-success pull-right mul-item-list" name="" value="">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 20px;">
                                    <button type="button"  class="btn btn-warning pull-right pending-list" name="" value="">
                                        <i class="fa fa-info"></i>
                                        Pending List
                                    </button>
                            	</div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>

                <div class="col-md-12 padding_sm">
                    <table class="table no-margin table-striped table-col-bordered table_sm table-condensed" id="adding-table" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg">
                             <th width="1%"><i style="padding: 5px 8px;cursor: pointer;" class="fa fa-trash text-red deleteSelectedRows"></i></th>
                             <th width="2%">#</th>
                             <th width="25%">Item Name
                             	<div style="float: right;cursor: pointer;" id="search-item-name-selected"><i class="fa fa-search"></i></div>
                             	<input type="text" class="form-control" autofocus="" id="list_item_search" name="list_item_search" placeholder="Search Item." autocomplete="off" style="display: none;">
                             </th>
                             <th width="8%">Item<br> Unit</th>
                             <th width="7%">Qty</th>
                             @if($type >= 2)
                             <th width="7%">Verified Qty</th>
                             @endif
                             @if($type >= 3)
                             <th width="7%">Approved Qty</th>
                             @endif
                             <th width="9%">Remarks</th>
                             <th width="3%" class="text-center"><i class="fa fa-exclamation-triangle text-red"></i></th>
                             <th width="7%">S. Stock</th>
                             <th width="7%">G. Stock</th>
                             <th width="7%">Dept. Stock</th>
                             <th width="7%">Last Approved Qty</th>
                             <th width="3%">&nbsp;</th>
                         </tr>

                        </thead>
                        <tbody>
                            <tr style="background: #FFF;">

                                <td>
                                    &nbsp;
                                    {{-- <input type="hidden" name="item_edit_id[]" disabled="" value="0"><i style="padding: 5px 8px;" class="fa fa-trash text-red deleteRow"></i> --}}
                                </td>
                                <td>
                                     &nbsp; <input type='hidden' name='item_edit_id' value='0'>
                                    {{-- <input type="checkbox" class="show_item_popup_chk_box" value="" name="selected_item_code[]"> --}}
                                </td>
                                <td>
                                    <select name="search_item_desc">
                                    	<option selected="selected">Select Item</option>
                                    </select>
                                    <input type="hidden" name="search_item_code_hidden" value="V001">
                                    <input type="hidden" name="search_item_name_hidden" value="Dolo 650">
                                </td>
                                <td>
                                    <input type="text" class="form-control" required="" autocomplete="off" readonly=""  placeholder="Item Unit"  readonly="" name="search_item_unit" value="mg">
                                </td>
                                <td>
                                	<input type="text" class="form-control" @if($type >= 2) readonly="" @endif required="" autocomplete="off" placeholder="Req. Qty"  value="0.00" maxlength="8" name="search_req_quantity">
                                </td>
                                @if($type >= 2)
                                <td>
                                    <input type="text" class="form-control" @if($type >= 3) readonly="" @endif required="" autocomplete="off" placeholder="Verified Qty"  value="0.00" maxlength="8" name="search_ver_quantity">
                                </td>
                                @endif
                                @if($type >= 3)
                                <td>
                                    <input type="text" class="form-control" @if($type >= 4) readonly="" @endif required="" autocomplete="off" placeholder="Approved Qty"  value="0.00" maxlength="8" name="search_app_quantity">
                                </td>
                                @endif
                                 <td>
                                    <input type="text" class="form-control" required="" autocomplete="off" placeholder="Remarks"   name="search_remarks">
                                </td>
                                <td style="vertical-align: middle;">
                                	<input type="checkbox" class="no-margin" name="sw" id="sw" value="1">
                                </td>
                                <td>
                                	<span>0.00</span>
                                	<input type="hidden" name="search_s_stock" value="0.00">
                                </td>
                                <td>
                                    <span>0.00</span>
                                    <input type="hidden" name="search_g_stock" value="0.00">
                                </td>
                                <td>
                                	<span>0.00</span>
                                	<input type="hidden" name="search_d_stock" value="0.00">
                                </td>
                                <td>
                                    <span>0.00</span>
                                    <input type="hidden" name="search_app_qty" value="0.00">
                                </td>
                                <td align="center" style="vertical-align: middle;">
                                    <button type="button" tabindex="-1" id="add" onclick="getNewRowInserted();" class="btn btn-success  no-margin add_row_btn">
                                    	Add
                                    </button>
                                    <button type="button" tabindex="-1" id="update" style="display: none;" onclick="updateRowInserted();" class="btn btn-success  no-margin update_row_btn">
                                        Update
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="theadscroll" style="position: relative; height: 260px;">
                        <table class="table no-margin table-striped table-col-bordered table_sm table-condensed" id="listing-table" style="border: 1px solid #CCC;">
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>

            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="col-md-12 no-padding" id="total_bill_values">

                    	<div class="col-md-4 padding_xs">
                        	<table class="table no-margin table_sm table-bordered">
                            <tbody>
                                    <tr>
                                        <td colspan="5">
                                            <label>Remarks</label>
                                            <textarea class="form-control" name="" id=""  style="min-height:47px;"></textarea>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    	<div class="col-md-4 padding_xs">

                    	</div>

                      	<div class="col-md-4 padding_xs">
                            @if($type != 4)
                            <div class="col-md-3 padding_sm" style="margin-top: 25px;">
                            	<button type="button" id="grn_save_btn" onclick="grn_savedata(this);" name="save" value="save" class="btn btn-success btn-block saveForm disable_on_save">Add Item</button>
                        	</div>
                            @endif
                            @if($type > 0 && $type != 4)
                            <div class="col-md-2 padding_sm" style="margin-top: 25px;">
                            	<button type="button" id="grn_approve_btn"  name="approve"  value="approve" class="btn btn-danger btn-block disable_on_save" >Delete</button>
                            </div>
                            @endif
                            @if($type != 4)
                            <div class="col-md-5 padding_sm" style="margin-top: 25px;">
                            	<div class="btn-group btn-block">
								    <button type="button" class="btn btn-info" style="width: 80%">Action</button>
								    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
								      <span class="caret"></span>
								    </button>

								    <ul class="dropdown-menu" role="menu" >
                                    @if($type <= 0)
								      <li><a href="#">Save As Draft</a></li>
                                    @endif
                                    @if($type <= 1)
								      <li><a href="#">Post For Verification</a></li>
                                    @endif
                                    @if($type <= 2)
								      <li><a href="#">Verify Request</a></li>
                                    @endif
                                    @if($type <= 3)
								      <li><a href="#">Approve Request</a></li>
                                    @endif
								    </ul>
								 </div>
                            </div>
                            @endif
                            @if($type != 4)
                            <div class="col-md-2 padding_sm" style="margin-top: 25px;">
                            	<button type="button" id="grn_save_print_btn" onclick="grn_savedata(this);" name="save" value="save" class="btn btn-success btn-block disable_on_save">Submit</button>
                        	</div>
                            @endif

                        </div>

                                {{-- <div class="clearfix"></div>
                                <div class="h10"></div> --}}

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- /page content -->

    <!-- Modal -->
    <div id="pending_list" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pending List</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="theadscroll">
                                <div class="col-md-12 padding_sm">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i
                            class="fa fa-times"></i> Cancel</button>
                    <button class="btn light_purple_bg"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div id="mul_items_list" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Items List</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="theadscroll">
                                <div class="col-md-12 padding_sm">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i
                            class="fa fa-times"></i> Cancel</button>
                    <button class="btn light_purple_bg"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('javascript_extra')
     <script type="text/javascript">
       $(document).ready(function() {

           $(document).on('click', '.grn_drop_btn', function(e) {
            e.stopPropagation();
            $(".grn_btn_dropdown").hide();
            $(this).next().slideDown('');
        });
           $(document).on('click', '.btn_group_box', function(e) {
            e.stopPropagation();
         });

		$(document).on('click', function() {
		            $(".grn_btn_dropdown").hide();
		});

        $(".select_button li").click(function() {
            $(this).toggleClass('active');
        });


        $(document).on('click', '.notes_sec_list ul li', function() {
            var disset = $(this).attr("id");
            $('.notes_sec_list ul li').removeClass("active");
            $(this).addClass("active");
            $(this).closest('.notes_box').find(".note_content").css("display", "none");
            $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
        });



        // $('.month_picker').datetimepicker({
        //     format: 'MM'
        // });
        // $('.year_picker').datetimepicker({
        //     format: 'YYYY'
        // });


        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }

        });

        // $('.datepicker').datetimepicker({
        //     format: 'DD-MM-YYYY'
        // });
        //$('.date_time_picker').datetimepicker();


        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });




        $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        });

        $('#search-item-name-selected').on('click',function(argument) {
        	var x = document.getElementById("list_item_search");
			  if (x.style.display === "none") {
			    x.style.display = "block";
			  } else {
			    x.style.display = "none";
			  }
        });

        $('select[name="search_item_desc"]').select2({
		  minimumInputLength: 2,
          ajax: {
                url: "{{route('extensionsvalley.stock.searchItem')}}",
                dataType: 'json',
                data: function (params) {
                      var query = {
                        term: params.term,
                      }
                      return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
          }
		});

		$('select[name="department"]').select2({
          minimumInputLength: 2,
          ajax: {
                url: "{{route('extensionsvalley.stock.searchDepartment')}}",
                dataType: 'json',
                data: function (params) {
                      var query = {
                        term: params.term,
                      }
                      return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
          }
		});

		$('select[name="store"]').select2({
		  minimumInputLength: 2,
          ajax: {
                url: "{{route('extensionsvalley.stock.searchStore')}}",
                dataType: 'json',
                data: function (params) {
                      var query = {
                        term: params.term,
                      }
                      return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
          }
		});


    });


	function getNewRowInserted(){
	  var table = document.getElementById("listing-table");
	  let row_count = table.rows.length;
	  let row_id = row_count;
	  if(parseInt(row_count) > 0){
	  	 let last_id = $('#listing-table tr:last').attr('row-id');
        if(last_id != undefined){
	  	    row_id = parseInt(last_id)+1;
        }
	  }

	  var row = table.insertRow(row_count);

	  row.setAttribute("row-id", row_id, 0);

	  var cell1 = row.insertCell(0);
	  cell1.width = '1%';

	  var cell2 = row.insertCell(1);
	  cell2.width = '2%';

	  var cell3 = row.insertCell(2);
	  cell3.width = '25%';

	  var cell4 = row.insertCell(3);
	  cell4.width = '8%';

	  var cell5 = row.insertCell(4);
	  cell5.width = '7%';

	  var cell6 = row.insertCell(5);
	  cell6.width = '7%';

	  var cell7 = row.insertCell(6);
	  cell7.width = '7%';

	  var cell8 = row.insertCell(7);
	  cell8.width = '9%';

	  var cell9 = row.insertCell(8);
	  cell9.width = '3%';

	  var cell10 = row.insertCell(9);
	  cell10.width = '7%';

	  var cell11 = row.insertCell(10);
	  cell11.width = '7%';

	  var cell12 = row.insertCell(11);
	  cell12.width = '7%';

	  var cell13 = row.insertCell(12);
	  cell13.width = '7%';

	  var cell14 = row.insertCell(13);
	  cell14.width = '3%';

	  let item_code = $('input[name="search_item_code_hidden"]').val();
	  let item_name = $('input[name="search_item_name_hidden"]').val();
	  let unit = $('input[name="search_item_unit"]').val();
	  let req_qty = $('input[name="search_req_quantity"]').val();
	  let ver_qty = $('input[name="search_ver_quantity"]').val();
	  let app_qty = $('input[name="search_app_quantity"]').val();
	  let remarks = $('input[name="search_remarks"]').val();
	  var sw = document.getElementById("sw").checked;
	  let s_stock = $('input[name="search_s_stock"]').val();
	  let g_stock = $('input[name="search_g_stock"]').val();
	  let d_stock = $('input[name="search_d_stock"]').val();
	  let last_app_qty = $('input[name="search_app_qty"]').val();

	  cell1.innerHTML = "<i style='padding: 5px 8px;cursor:pointer' class='fa fa-trash text-red deleteRow'></i>";
	  cell2.innerHTML = "<input type='checkbox' class='show_item_popup_chk_box' value='1' name='selected_item_code[]'>";
	  cell3.innerHTML = item_name+"<input type='hidden' name='listing_item_name_hidden[]' value="+item_name+"><input type='hidden' name='listing_item_code_hidden[]' value="+item_code+">";
	  cell4.innerHTML = "<span>"+unit+"</span><input type='hidden' name='listing_unit_hidden[]' value="+unit+">";
	  cell5.innerHTML = "<span>"+req_qty+"</span><input type='hidden' name='listing_req_qty_hidden[]' value="+req_qty+">";
	  cell6.innerHTML = "<span>"+ver_qty+"</span><input type='hidden' name='listing_ver_qty_hidden[]' value="+ver_qty+">";
	  cell7.innerHTML = "<span>"+app_qty+"</span><input type='hidden' name='listing_app_qty_hidden[]' value="+app_qty+">";
	  cell8.innerHTML = "<span>"+remarks+"</span><input type='hidden' name='listing_remarks_hidden[]' value="+remarks+">";
	  cell9.innerHTML = (sw == 1) ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>";
	  cell10.innerHTML = s_stock;
	  cell11.innerHTML = g_stock;
	  cell12.innerHTML = d_stock;
	  cell13.innerHTML = last_app_qty;
	  cell14.innerHTML = "<i style='font-size: 15px;color: #714FA6;cursor:pointer' class='fa fa-edit editRow'></i>";

	}

	$(document).on('click','.deleteRow',function(obj) { $(this).closest('tr').remove(); });
    $(document).on('click','.deleteSelectedRows',function(obj) {

        $('input[name="selected_item_code[]"]').each(function() {
            if($(this).prop('checked')){
                $(this).closest('tr').remove();
            }
        });

    });
    $(document).on('click','.editRow',function() {
        let tr = $(this).closest('tr');
        let item_name = tr.find('input[name="listing_item_name_hidden[]"]').val();
        let item_code = tr.find('input[name="listing_item_code_hidden[]"]').val();
        let unit = tr.find('input[name="listing_unit_hidden[]"]').val();
        let req_qty = tr.find('input[name="listing_req_qty_hidden[]"]').val();
        let ver_qty = tr.find('input[name="listing_ver_qty_hidden[]"]').val();
        let app_qty = tr.find('input[name="listing_app_qty_hidden[]"]').val();
        let remarks = tr.find('input[name="listing_remarks_hidden[]"]').val();
        let row_id = tr.attr('row-id');

        $('input[name="search_item_code_hidden"]').val(item_code);
        $('input[name="search_item_name_hidden"]').val(item_name);
        $('input[name="search_item_unit"]').val(unit);
        $('input[name="search_req_quantity"]').val(req_qty);
        $('input[name="search_ver_quantity"]').val(ver_qty);
        $('input[name="search_app_quantity"]').val(app_qty);
        $('input[name="search_remarks"]').val(remarks);
        $('input[name="item_edit_id"]').val(row_id);

        $('.add_row_btn').css({display: 'none'});
        $('.update_row_btn').css({display: 'block'});

    });

    function updateRowInserted() {

        let item_code = $('input[name="search_item_code_hidden"]').val();
        let item_name = $('input[name="search_item_name_hidden"]').val();
        let unit = $('input[name="search_item_unit"]').val();
        let req_qty = $('input[name="search_req_quantity"]').val();
        let ver_qty = $('input[name="search_ver_quantity"]').val();
        let app_qty = $('input[name="search_app_quantity"]').val();
        let remarks = $('input[name="search_remarks"]').val();
        var sw = document.getElementById("sw").checked;
        let s_stock = $('input[name="search_s_stock"]').val();
        let g_stock = $('input[name="search_g_stock"]').val();
        let d_stock = $('input[name="search_d_stock"]').val();
        let last_app_qty = $('input[name="search_app_qty"]').val();
        let item_edit_id = $('input[name="item_edit_id"]').val();

        if(item_edit_id != undefined){
            if($('#listing-table tr[row-id="'+item_edit_id+'"]').length > 0){
               let tr = $('#listing-table tr[row-id="'+item_edit_id+'"]');
               tr.find('input[name="listing_item_name_hidden[]"]').val(item_name);
               tr.find('input[name="listing_item_code_hidden[]"]').val(item_code);

               tr.find('input[name="listing_unit_hidden[]"]').val(unit);
               tr.find('input[name="listing_unit_hidden[]"]').prev('span').text(unit);

               tr.find('input[name="listing_req_qty_hidden[]"]').val(req_qty);
               tr.find('input[name="listing_req_qty_hidden[]"]').prev('span').text(req_qty);

               tr.find('input[name="listing_ver_qty_hidden[]"]').val(ver_qty);
               tr.find('input[name="listing_ver_qty_hidden[]"]').prev('span').text(ver_qty);

               tr.find('input[name="listing_app_qty_hidden[]"]').val(app_qty);
               tr.find('input[name="listing_app_qty_hidden[]"]').prev('span').text(app_qty);

               tr.find('input[name="listing_remarks_hidden[]"]').val(remarks);
               tr.find('input[name="listing_remarks_hidden[]"]').prev('span').text(remarks);

               let sw_i = (sw == 1) ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>";

               tr.find('td:nth-child(9)').html(sw_i);
               tr.find('td:nth-child(10)').val(s_stock);
               tr.find('td:nth-child(11)').val(g_stock);
               tr.find('td:nth-child(12)').val(d_stock);
               tr.find('td:nth-child(13)').val(last_app_qty);
            }
        }

        $('input[name="item_edit_id"]').val('');
        $('input[name="search_item_code_hidden"]').val('');
        $('input[name="search_item_name_hidden"]').val('');
        $('input[name="search_item_unit"]').val('');
        $('input[name="search_req_quantity"]').val('0.00');
        $('input[name="search_ver_quantity"]').val('0.00');
        $('input[name="search_app_quantity"]').val('0.00');

        $('input[name="search_remarks"]').val('');


        $('.add_row_btn').css({display: 'block'});
        $('.update_row_btn').css({display: 'none'});
    }

    $(document).on('click', '.pending-list', function(event) {
        $('#pending_list').modal('show');
    });

    $(document).on('click', '.mul-item-list', function(event) {
        $('#mul_items_list').modal('show');
    });

    </script>
@endsection
