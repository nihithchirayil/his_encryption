<style>
    .td_class {
        border: 1px solid !important;
    }
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('Y-m-d h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to_date ?></p>

        <div class="col-md-12" id="print_data">
            <div class="col-md-6">
                <h5 style="color: blue">Exempted Sales</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    if (count($ledger_sales_gst_res_30) != 0){
                        $total_dr_30 = $total_cr_30 =0;
                        foreach ($ledger_sales_gst_res_30 as $data){
                            if($data->cr_dr='cr'){
                                $cr_amount = $data->amount;
                                $dr_amount = 0;
                            }else{
                                $dr_amount = $data->amount;
                                $cr_amount = 0;
                            }
                            $total_cr_30 += $cr_amount;
                            $total_dr_30 += $dr_amount
                            ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_30 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_30 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_30 - $total_dr_30 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                            <td colspan="5"  class="common_td_rules">No results found</td>
                         </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">
                <h5 style="color: blue">Sales GST 5 %</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                if (count($ledger_sales_gst_res_31) != 0){
                    $total_dr_31 = $total_cr_31 =0;
                    foreach ($ledger_sales_gst_res_31 as $data){
                        if($data->cr_dr='cr'){
                            $cr_amount = $data->amount;
                            $dr_amount = 0;
                        }else{
                            $dr_amount = $data->amount;
                            $cr_amount = 0;
                        }
                        $total_cr_31 += $cr_amount;
                        $total_dr_31 += $dr_amount
                        ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_31 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_31 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_31 - $total_dr_31 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                            <td colspan="5"  class="common_td_rules">No results found</td>
                         </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">
                <h5 style="color: blue">Selling CGST/SGST 2.5%</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
            if (count($ledger_sales_gst_res_32) != 0){
                $total_dr_32 = $total_cr_32 =0;
                foreach ($ledger_sales_gst_res_32 as $data){
                    if($data->cr_dr='cr'){
                        $cr_amount = $data->amount;
                        $dr_amount = 0;
                    }else{
                        $dr_amount = $data->amount;
                        $cr_amount = 0;
                    }
                    $total_cr_32 += $cr_amount;
                    $total_dr_32 += $dr_amount
                    ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_32 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_32 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_32 - $total_dr_32 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                            <td colspan="5"  class="common_td_rules">No results found</td>
                         </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">
                <h5 style="color: blue">Sales GST 12 %</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
        if (count($ledger_sales_gst_res_33) != 0){
            $total_dr_33 = $total_cr_33 =0;
            foreach ($ledger_sales_gst_res_33 as $data){
                if($data->cr_dr='cr'){
                    $cr_amount = $data->amount;
                    $dr_amount = 0;
                }else{
                    $dr_amount = $data->amount;
                    $cr_amount = 0;
                }
                $total_cr_33 += $cr_amount;
                $total_dr_33 += $dr_amount
                ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_33 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_33 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_33 - $total_dr_33 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                            <td colspan="5"  class="common_td_rules">No results found</td>
                         </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">
                <h5 style="color: blue">Selling GST 6%</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
        if (count($ledger_sales_gst_res_34) != 0){
            $total_dr_34 = $total_cr_34 =0;
            foreach ($ledger_sales_gst_res_34 as $data){
                if($data->cr_dr='cr'){
                    $cr_amount = $data->amount;
                    $dr_amount = 0;
                }else{
                    $dr_amount = $data->amount;
                    $cr_amount = 0;
                }
                $total_cr_34 += $cr_amount;
                $total_dr_34 += $dr_amount
                ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_34 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_34 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_34 - $total_dr_34 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                            <td colspan="5"  class="common_td_rules">No results found</td>
                         </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">
                <h5 style="color: blue">Sales GST 18 %</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
        if (count($ledger_sales_gst_res_35) != 0){
            $total_dr_35 = $total_cr_35 =0;
            foreach ($ledger_sales_gst_res_35 as $data){
                if($data->cr_dr='cr'){
                    $cr_amount = $data->amount;
                    $dr_amount = 0;
                }else{
                    $dr_amount = $data->amount;
                    $cr_amount = 0;
                }
                $total_cr_35 += $cr_amount;
                $total_dr_35 += $dr_amount
                ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_35 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_35 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_35 - $total_dr_35 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                            <td colspan="5"  class="common_td_rules">No results found</td>
                         </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">
                <h5 style="color: blue">Selling CGST/SGST 9%</h5>
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;border-bottom: 1px solid #CCC !important;min-height:200px">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th>TRANSDATE</th>
                            <th>VOUCHER</th>
                            <th>NARRATION</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
        if (count($ledger_sales_gst_res_36) != 0){
            $total_dr_36 = $total_cr_36 =0;
            foreach ($ledger_sales_gst_res_36 as $data){
                if($data->cr_dr='cr'){
                    $cr_amount = $data->amount;
                    $dr_amount = 0;
                }else{
                    $dr_amount = $data->amount;
                    $cr_amount = 0;
                }
                $total_cr_36 += $cr_amount;
                $total_dr_36 += $dr_amount
                ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->invoice_date }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->naration }}</td>
                            <td class="td_common_numeric_rules">{{ $cr_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $dr_amount }}</td>
                        </tr>

                        <?php } ?>
                        <tr style="border-top: 1px solid">
                            <td colspan="3" style="border-left: 1px solid #CCC !important"><b>TOTAL</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_cr_36 }}</b></td>
                            <td class="td_common_numeric_rules"><b>{{ $total_dr_36 }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-left: 1px solid #CCC !important;"></td>
                            <td class="td_common_numeric_rules" style="border-right: 1px solid #CCC !important;">
                                {{ $total_cr_36 - $total_dr_36 }}</td>
                            <td class="common_td_rules"></td>
                        </tr>
                        <?php }else{ ?>
                           <td colspan="5"  class="common_td_rules">No results found</td>
                        </tr>
                       <?php } ?>
                    </tbody>

                </table>

            </div>
        </div>
    </div>
</div>
