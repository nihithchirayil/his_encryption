<style>
    .td_class {
        border: 1px solid !important;
    }
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('Y-m-d h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to_date ?></p>

        <div class="col-md-12" id="print_data">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;border-bottom: 1px solid #CCC !important;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th class=" common_td_rules" style="width: 20%"></th>
                        <th>0%</th>
                        <th colspan="4">5%</th>
                        <th colspan="4">12%</th>
                        <th colspan="4">18%</th>
                        <th colspan="4">28%</th>
                </tr>
                    <tr>
                        <th class=" common_td_rules"></th>
                        <th >Taxable Value</th>
                        <th >Taxable Value</th>
                        <th >CGST</th>
                        <th >SGST</th>
                        <th >IGST</th>
                        <th >Taxable Value</th>
                        <th >CGST</th>
                        <th >SGST</th>
                        <th >IGST</th>
                        <th >Taxable Value</th>
                        <th >CGST</th>
                        <th >SGST</th>
                        <th >IGST</th>
                        <th >Taxable Value</th>
                        <th >CGST</th>
                        <th >SGST</th>
                        <th >IGST</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                if (count($purchase_res) != 0){
                    $i=1;
                    $netamttotal=0;
                        $exempted_amount=0.0;
                        $three_amount=0.0;
                        $five_amount=0.0;
                        $twelve_amount=0.0;
                        $eighteen_amount=0.0;
                        $twentyeight_amount=0.0;
                        $three_CGST=0.0;
                        $three_SGST=0.0;
                        $three_IGST=0.0;
                        $five_CGST=0.0;
                        $five_SGST=0.0;
                        $five_IGST=0.0;
                        $twelve_CGST=0.0;
                        $twelve_SGST=0.0;
                        $twelve_IGST=0.0;
                        $eighteen_CGST=0.0;
                        $eighteen_SGST=0.0;
                        $eighteen_IGST=0.0;
                        $twentyeight_CGST=0.0;
                        $twentyeight_SGST=0.0;
                        $twentyeight_IGST=0.0;
                    foreach ($purchase_res as $data){
                        $tax_rate=intval($data->grn_tax_total_rate);


                        If($tax_rate==0){
                            $exempted_amount=$data->exemptedsales;
                        }else If($tax_rate==3){
                            $three_amount += $data->salesthree;
                            $three_CGST = floatval($data->salesthreetax) / 2;
                            $three_SGST = floatval($data->salesthreetax) / 2;
                            $three_IGST =floatval($data->salesthreeigst);
                        }else If($tax_rate==5){
                            $five_amount =$data->salesfive;
                            $five_CGST = floatval($data->salesfivetax) / 2;
                            $five_SGST = floatval($data->salesfivetax) / 2;
                            $five_IGST =floatval($data->salesfivetaxigst);
                        }else If($tax_rate==12){
                            $twelve_amount =$data->salestwelve;
                            $twelve_CGST = floatval($data->salestwelvetax) / 2;
                            $twelve_SGST = floatval($data->salestwelvetax) / 2;
                            $twelve_IGST =floatval($data->salestwelvetaxigst);
                        }else If($tax_rate==18){
                            $eighteen_amount +=$data->saleseighteen;
                            $eighteen_CGST = floatval($data->saleseighteentax) / 2;
                            $eighteen_SGST = floatval($data->saleseighteentax) / 2;
                            $eighteen_IGST =floatval($data->saleseighteentaxigst);
                        }else If($tax_rate==28){
                            $twentyeight_amount = $data->salestwentyeight;
                            $twentyeight_CGST = floatval($data->salestwentyeighttax) / 2;
                            $twentyeight_SGST = floatval($data->salestwentyeighttax) / 2;
                            $twentyeight_IGST = floatval($data->salestwentyeighttaxigst);
                        }
                    }
                    ?>
                    <tr>
                       <td class=" common_td_rules"><b> Total Purchase </b></td>
                        <td class="td_common_numeric_rules">{{ number_format(($exempted_amount-$PURCHASE0), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($five_amount-($PURCHASE5)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_CGST-$purchase_return_five_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_SGST-$purchase_return_five_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_IGST-$purchase_return_five_IGST), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($twelve_amount-($PURCHASE12)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_CGST-$purchase_return_twelve_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_SGST-$purchase_return_twelve_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_IGST-$purchase_return_twelve_IGST), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_amount-($PURCHASE18)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_CGST-$purchase_return_eighteen_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_SGST-$purchase_return_eighteen_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_IGST-$purchase_return_eighteen_IGST), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_amount-($PURCHASE28)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_CGST-$purchase_return_twentyeight_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_SGST-$purchase_return_twentyeight_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_IGST-$purchase_return_twentyeight_IGST), 2, '.', '') }}</td>
                    </tr>
                    <?php
                }else{
                    ?>
                    <tr>
                        <td class=" common_td_rules"><b> Total Purchase </b></td>
                        <td colspan="20" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
<!-- ==================================================== NON MED ================================= -->
                <tbody>
                    <?php
                        $i=1;
                        $netamttotal=0;
                        $exempted_amount=0.0;
                        $three_amount=0.0;
                        $five_amount=0.0;
                        $twelve_amount=0.0;
                        $eighteen_amount=0.0;
                        $twentyeight_amount=0.0;
                        $three_CGST=0.0;
                        $three_SGST=0.0;
                        $three_IGST=0.0;
                        $five_CGST=0.0;
                        $five_SGST=0.0;
                        $five_IGST=0.0;
                        $twelve_CGST=0.0;
                        $twelve_SGST=0.0;
                        $twelve_IGST=0.0;
                        $eighteen_CGST=0.0;
                        $eighteen_SGST=0.0;
                        $eighteen_IGST=0.0;
                        $twentyeight_CGST=0.0;
                        $twentyeight_SGST=0.0;
                        $twentyeight_IGST=0.0;
                if (count($purchase_non_med_res) != 0){
                    foreach ($purchase_non_med_res as $data){
                        $tax_rate=intval($data->grn_tax_total_rate);

                        If($tax_rate==0){
                            $exempted_amount=$data->exemptedsales;
                        }else If($tax_rate==3){
                            $three_amount += $data->salesthree;
                            $three_CGST = floatval($data->salesthreetax) / 2;
                            $three_SGST = floatval($data->salesthreetax) / 2;
                            $three_IGST =floatval($data->salesthreeigst);
                        }else If($tax_rate==5){
                            $five_amount =$data->salesfive;
                            $five_CGST = floatval($data->salesfivetax) / 2;
                            $five_SGST = floatval($data->salesfivetax) / 2;
                            $five_IGST =floatval($data->salesfivetaxigst);
                        }else If($tax_rate==12){
                            $twelve_amount =$data->salestwelve;
                            $twelve_CGST = floatval($data->salestwelvetax) / 2;
                            $twelve_SGST = floatval($data->salestwelvetax) / 2;
                            $twelve_IGST =floatval($data->salestwelvetaxigst);
                        }else If($tax_rate==18){
                            $eighteen_amount +=$data->saleseighteen;
                            $eighteen_CGST = floatval($data->saleseighteentax) / 2;
                            $eighteen_SGST = floatval($data->saleseighteentax) / 2;
                            $eighteen_IGST =floatval($data->saleseighteentaxigst);
                        }else If($tax_rate==28){
                            $twentyeight_amount = $data->salestwentyeight;
                            $twentyeight_CGST = floatval($data->salestwentyeighttax) / 2;
                            $twentyeight_SGST = floatval($data->salestwentyeighttax) / 2;
                            $twentyeight_IGST = floatval($data->salestwentyeighttaxigst);
                        }
                    }
                    ?>
                    <tr>
                    <td class=" common_td_rules" style="white-space:normal !important"><b> Less Ineligible ( Other than Medicines ) </b></td>
                        <td class="td_common_numeric_rules">{{ number_format(($exempted_amount-$PURCHASE0_non_med), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($five_amount-($PURCHASE5_non_med)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_CGST-$purchase_return_five_non_med_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_SGST-$purchase_return_five_non_med_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_IGST-$purchase_return_five_non_med_IGST), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($twelve_amount-($PURCHASE12_non_med)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_CGST-$purchase_return_twelve_non_med_IGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_SGST-$purchase_return_twelve_non_med_IGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_IGST-$purchase_return_twelve_non_med_IGST), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_amount-($PURCHASE18_non_med)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_CGST-$purchase_return_eighteen_non_med_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_SGST-$purchase_return_eighteen_non_med_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_IGST-$purchase_return_eighteen_non_med_IGST), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_amount-($PURCHASE28_non_med)), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_CGST-$purchase_return_twentyeight_non_med_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_SGST-$purchase_return_twentyeight_non_med_CGST), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_IGST-$purchase_return_twentyeight_non_med_IGST), 2, '.', '') }}</td>
                    </tr>
                    <?php
                }else{
                    ?>
                    <tr>
                        <td class=" common_td_rules" style="white-space:normal !important"><b> Less Ineligible ( Other than Medicines ) </b></td>
                        <td colspan="20" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
                <!-- ============================================== IP RETURN =============================================== -->
                <tbody>
                    <?php
                        $i=1;
                        $netamttotal_non=0;
                        $exempted_amount_non=0.0;
                        $three_amount_non=0.0;
                        $five_amount_non=0.0;
                        $twelve_amount_non=0.0;
                        $eighteen_amount_non=0.0;
                        $twentyeight_amount_non=0.0;
                        $three_CGST_non=0.0;
                        $three_SGST_non=0.0;
                        $three_IGST_non=0.0;
                        $five_CGST_non=0.0;
                        $five_SGST_non=0.0;
                        $five_IGST_non=0.0;
                        $twelve_CGST_non=0.0;
                        $twelve_SGST_non=0.0;
                        $twelve_IGST_non=0.0;
                        $eighteen_CGST_non=0.0;
                        $eighteen_SGST_non=0.0;
                        $eighteen_IGST_non=0.0;
                        $twentyeight_CGST_non=0.0;
                        $twentyeight_SGST_non=0.0;
                        $twentyeight_IGST_non=0.0;
                if (count($purchase_ip_res) != 0){

                    foreach ($purchase_ip_res as $data){
                        $tax_rate=intval($data->grn_tax_total_rate);


                        If($tax_rate==0){
                            $exempted_amount_non=$data->exemptedsales;
                        }else If($tax_rate==3){
                            $three_amount_non += $data->salesthree;
                            $three_CGST_non = floatval($data->salesthreetax) / 2;
                            $three_SGST_non = floatval($data->salesthreetax) / 2;
                            $three_IGST_non =floatval($data->salesthreeigst);
                        }else If($tax_rate==5){
                            $five_amount_non =$data->salesfive;
                            $five_CGST_non = floatval($data->salesfivetax) / 2;
                            $five_SGST_non = floatval($data->salesfivetax) / 2;
                            $five_IGST_non =floatval($data->salesfivetaxigst);
                        }else If($tax_rate==12){
                            $twelve_amount_non =$data->salestwelve;
                            $twelve_CGST_non = floatval($data->salestwelvetax) / 2;
                            $twelve_SGST_non = floatval($data->salestwelvetax) / 2;
                            $twelve_IGST_non =floatval($data->salestwelvetaxigst);
                        }else If($tax_rate==18){
                            $eighteen_amount_non = $data->saleseighteen;
                            $eighteen_CGST_non = floatval($data->saleseighteentax) / 2;
                            $eighteen_SGST_non = floatval($data->saleseighteentax) / 2;
                            $eighteen_IGST_non =floatval($data->saleseighteentaxigst);
                        }else If($tax_rate==28){
                            $twentyeight_amount_non = $data->salestwentyeight;
                            $twentyeight_CGST_non = floatval($data->salestwentyeighttax) / 2;
                            $twentyeight_SGST_non = floatval($data->salestwentyeighttax) / 2;
                            $twentyeight_IGST_non = floatval($data->salestwentyeighttaxigst);
                        }
                    }
                    ?>
                    <tr>
                    <td class=" common_td_rules" style="white-space:normal !important"><b>Less Ineligible ( Purchase Tax of IP Pharmacy Sold Medicines) </b></td>
                        <td class="td_common_numeric_rules">{{ number_format(($exempted_amount_non), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($five_amount_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_CGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_SGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($five_IGST_non), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($twelve_amount_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_CGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_SGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twelve_IGST_non), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_amount_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_CGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_SGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($eighteen_IGST_non), 2, '.', '') }}</td>

                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_amount_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_CGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_SGST_non), 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format(($twentyeight_IGST_non), 2, '.', '') }}</td>
                    </tr>
                    <?php
                }else{
                    ?>
                    <tr>
                        <td class=" common_td_rules" style="white-space:normal !important"><b>Less Ineligible ( Purchase Tax of IP Pharmacy Sold Medicines) </b></td>
                        <td colspan="20" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>

            <!-- ====================================================  MED ================================= -->
            <tbody>
                <?php
            if (count($purchase_med_res) != 0){
                $i=1;
                    $netamttotal=0;
                    $exempted_amount=0.0;
                    $three_amount=0.0;
                    $five_amount=0.0;
                    $twelve_amount=0.0;
                    $eighteen_amount=0.0;
                    $twentyeight_amount=0.0;
                    $three_CGST=0.0;
                    $three_SGST=0.0;
                    $three_IGST=0.0;
                    $five_CGST=0.0;
                    $five_SGST=0.0;
                    $five_IGST=0.0;
                    $twelve_CGST=0.0;
                    $twelve_SGST=0.0;
                    $twelve_IGST=0.0;
                    $eighteen_CGST=0.0;
                    $eighteen_SGST=0.0;
                    $eighteen_IGST=0.0;
                    $twentyeight_CGST=0.0;
                    $twentyeight_SGST=0.0;
                    $twentyeight_IGST=0.0;
                foreach ($purchase_med_res as $data){
                    $tax_rate=intval($data->grn_tax_total_rate);


                    If($tax_rate==0){
                        $exempted_amount=($data->exemptedsales)-$exempted_amount_non;
                    }else If($tax_rate==3){
                        $three_amount = ($data->salesthree)-$three_amount_non;
                        $three_CGST = (floatval($data->salesthreetax) / 2)-$three_CGST_non;
                        $three_SGST = (floatval($data->salesthreetax) / 2)-$three_SGST_non;
                        $three_IGST = (floatval($data->salesthreeigst))-$three_IGST_non;
                    }else If($tax_rate==5){
                        $five_amount = ($data->salesfive)-$five_amount_non;
                        $five_CGST = (floatval($data->salesfivetax) / 2)-$five_CGST_non;
                        $five_SGST = (floatval($data->salesfivetax) / 2)-$five_SGST_non;
                        $five_IGST = (floatval($data->salesfivetaxigst))-$five_IGST_non;
                    }else If($tax_rate==12){
                        $twelve_amount = ($data->salestwelve)-$twelve_amount_non;
                        $twelve_CGST = (floatval($data->salestwelvetax) / 2)-$twelve_CGST_non;
                        $twelve_SGST = (floatval($data->salestwelvetax) / 2)-$twelve_SGST_non;
                        $twelve_IGST = (floatval($data->salestwelvetaxigst))-$twelve_IGST_non;
                    }else If($tax_rate==18){
                        $eighteen_amount = ($data->saleseighteen)-$eighteen_amount_non;
                        $eighteen_CGST = (floatval($data->saleseighteentax) / 2)-$eighteen_CGST_non;
                        $eighteen_SGST = (floatval($data->saleseighteentax) / 2)-$eighteen_SGST_non;
                        $eighteen_IGST = (floatval($data->saleseighteentaxigst))-$eighteen_IGST_non;
                    }else If($tax_rate==28){
                        $twentyeight_amount = ($data->salestwentyeight)-$twentyeight_amount_non;
                        $twentyeight_CGST = (floatval($data->salestwentyeighttax) / 2)-$twentyeight_CGST_non;
                        $twentyeight_SGST = (floatval($data->salestwentyeighttax) / 2)-$twentyeight_SGST_non;
                        $twentyeight_IGST = (floatval($data->salestwentyeighttaxigst))-$twentyeight_IGST_non;
                    }
                }
                ?>
                <tr>
                <td class=" common_td_rules" style="white-space:normal !important"><b> Total Eligible Input ( Medicine Purchases) </b></td>
                    <td class="td_common_numeric_rules">{{ number_format(($exempted_amount-$PURCHASE0_med), 2, '.', '') }}</td>

                    <td class="td_common_numeric_rules">{{ number_format(($five_amount-($PURCHASE5_med)), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($five_CGST-$purchase_return_five_med_CGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($five_SGST-$purchase_return_five_med_CGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($five_IGST-$purchase_return_five_med_IGST), 2, '.', '') }}</td>

                    <td class="td_common_numeric_rules">{{ number_format(($twelve_amount-($PURCHASE12_med)), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($twelve_CGST-$purchase_return_twelve_med_IGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($twelve_SGST-$purchase_return_twelve_med_IGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($twelve_IGST-$purchase_return_twelve_med_IGST), 2, '.', '') }}</td>

                    <td class="td_common_numeric_rules">{{ number_format(($eighteen_amount-($PURCHASE18_med)), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($eighteen_CGST-$purchase_return_eighteen_med_CGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($eighteen_SGST-$purchase_return_eighteen_med_CGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($eighteen_IGST-$purchase_return_eighteen_med_IGST), 2, '.', '') }}</td>

                    <td class="td_common_numeric_rules">{{ number_format(($twentyeight_amount-($PURCHASE28_med)), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($twentyeight_CGST-$purchase_return_twentyeight_med_CGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($twentyeight_SGST-$purchase_return_twentyeight_med_CGST), 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format(($twentyeight_IGST-$purchase_return_twentyeight_med_IGST), 2, '.', '') }}</td>
                </tr>
                <?php
            }else{
                ?>
                <tr>
                    <td class=" common_td_rules" style="white-space:normal !important"><b> Total Eligible Input ( Medicine Purchases) </b></td>
                    <td colspan="20" style="text-align: center;white-space:normal !important">
                        No Result Found
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>


        </div>

    </div>
