<div class="row">

    <div class="col-md-12 padding_sm" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <h4 style="text-align: center" id="heading"> <b>Stock List</b></h4>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;margin-left: -41px;">
            <tr class="headerclass"
                style="background-color: rgb(227 227 227);color: #0c0c0c;border-spacing: 0 1em;font-family:sans-serif">
                <th style="width: 3%">SI.No.</th>
                <th style="width: 7%;">Category</th>
                <th style="width: 7%;">Sub Category</th>
                <th style="width:15%;">Item Description</th>
                <th style="width: 7%;">HSN Code</th>
                <th style="width: 9%">Vendor Name</th>
                <th style="width: 8%;">Doctor</th>
                <th style="width: 5%;">Rack</th>
                <th style="width: 8%;">Manufacturer</th>
                @if ($batch_wise == 1)
                    <th style="width: 10%;">Batch No</th>
                    <th style="width: 10%;">Expiry Date</th>
                @endif
                <th style="width: 3%;">Intransit Qty</th>
                <th style="width: 3%;">Stock</th>
                <th style="width: 10%;">Unit Cost</th>
                <th style="width: 7%;">MRP</th>
                <th style="width: 7%;">Price</th>
                @php
                    $span = 15;
                @endphp
            </tr>

            @php
                $sort_array = [];
                $i = 0;
                $sort_change = 0;
                $user_name = '';
                $intransit_qty = 0;
                $stock = 0;
                $unit_purchase_cost = 0;
                $unit_mrp = 0;
                $unit_selling_price = 0;
                $cols_span = 9;
            @endphp
            @if (count($res) != 0)
                @foreach ($res as $data)
                    @php
                        $intransit_qty += floatval($data->intransit_qty);
                        $stock += floatval($data->stock);
                        $unit_purchase_cost += floatval($data->unit_purchase_cost);
                        $unit_mrp += floatval($data->unit_mrp);
                        $unit_selling_price += floatval($data->unit_selling_price);
                    @endphp
                    @if (!in_array($data->group, $sort_array))
                        @php
                            $sort_array[] = $data->group;
                            $sort_order = true;
                        @endphp
                    @endif

                    @if ($user_name != $data->location_name)
                        @php $user_name = $data->location_name; @endphp
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        </tr>
                    @endif
                    <tr>
                        <td class="td_common_numeric_rules">{{ ++$i }}</td>
                        <td class="common_td_rules">{{ $data->category }}</td>
                        <td class="common_td_rules">{{ $data->subcategory_name }}</td>
                        <td class="common_td_rules"><b>{{ $data->item_desc }}</b></td>
                        <td class="common_td_rules">{{ $data->hsn_code }}</td>
                        <td class="common_td_rules">{{ $data->vendor_name }}</td>
                        <td class="common_td_rules">{{ $data->doctor_name ? $data->doctor_name : '-' }}</td>
                        <td class="td_common_numeric_rules">{{ $data->rack ? $data->rack : '-' }}</td>
                        <td class="common_td_rules">{{ $data->manufacturer ? $data->manufacturer : '-' }}</td>
                        @if ($batch_wise == 1)
                            @php $cols_span=11; @endphp
                            <td class="common_td_rules">{{ $data->batch_no }}</td>
                            <td class="common_td_rules">{{ $data->expiry_date }}</td>
                        @endif
                        <td class="td_common_numeric_rules">{{ $data->intransit_qty }}</td>
                        <td class="td_common_numeric_rules">{{ $data->stock }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->unit_purchase_cost, 2, '.', '') }}
                        <td class="td_common_numeric_rules">{{ number_format($data->unit_mrp, 2, '.', '') }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->unit_selling_price, 2, '.', '') }}</td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="{{ $cols_span }}">Total</th>
                    <td class="td_common_numeric_rules">{{ number_format($intransit_qty, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($stock, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($unit_purchase_cost, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($unit_mrp, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($unit_selling_price, 2, '.', '') }}</td>
                </tr>
            @else
                <tr>
                    <td colspan="14" style="text-align: center;">No Data Found</td>
                </tr>
            @endif
        </table>
    </div>
</div>
