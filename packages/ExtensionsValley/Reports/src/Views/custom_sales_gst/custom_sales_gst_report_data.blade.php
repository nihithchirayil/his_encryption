<style>
    .td_class {
        border: 1px solid !important;
    }
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('Y-m-d h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to_date ?></p>

        <div class="col-md-12" id="print_data">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;border-bottom: 1px solid #CCC !important;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th >Rate</th>
                        <th >Taxable Value</th>
                        <th >CGST Amount</th>
                        <th >SGST Amount</th>
                </tr>

                </thead>
                <tbody>
                    <?php
                    $sales_05 =0;
                    $sales_12=0;
                    $sales_18=0;
                    $sales_cgst_18=0;
                    $sales_cgst_12=0;
                    $sales_cgst_05=0;
                    $sales_ret_05=0;
                    $sales_ret_12=0;
                    $sales_ret_18=0;
                    $sales_ret_cgst_18 =0;
                    $sales_ret_cgst_12=0;
                    $sales_ret_cgst_05=0;
                if (count($sales_res) != 0){
                     foreach($sales_res as $data){
                        $sales_0 = $data->exemptedsales;
                        $sales_05 = $data->salesfive_taxable;
                        $sales_12 = $data->salestwelve_taxable;
                        $sales_18 = $data->saleseighteen_taxable;
                        $sales_28 = $data->salestwentyeight_taxable;

                        $sales_cgst_28 = $data->twentyeight_tax_cgst;
                        $sales_cgst_18 = $data->eighteen_tax_cgst;
                        $sales_cgst_12 = $data->twelve_tax_cgst;
                        $sales_cgst_05 = $data->five_tax_cgst;
                     }
                    if(count($sales_ret_res) != 0){
                        foreach($sales_ret_res as $data){
                        $sales_ret_0 = $data->exemptedsales;
                        $sales_ret_05 = $data->salesfive_taxable;
                        $sales_ret_12 = $data->salestwelve_taxable;
                        $sales_ret_18 = $data->saleseighteen_taxable;
                        $sales_ret_28 = $data->salestwentyeight_taxable;

                        $sales_ret_cgst_28 = $data->twentyeighteentax_cgst;
                        $sales_ret_cgst_18 = $data->eighteen_tax_cgst;
                        $sales_ret_cgst_12 = $data->twelve_tax_cgst;
                        $sales_ret_cgst_05 = $data->five_tax_cgst;
                     }
                    }
                    $sales_0 = $sales_0-$sales_ret_0;
                    $sales_05 =$sales_05-$sales_ret_05;
                    $sales_12 =$sales_12-$sales_ret_12;
                    $sales_18 =$sales_18-$sales_ret_18;
                    $sales_28 =$sales_28-$sales_ret_28;

                    $sales_cgst_28 = $sales_cgst_28 - $sales_ret_cgst_28;
                    $sales_cgst_18 =$sales_cgst_18 - $sales_ret_cgst_18;
                    $sales_cgst_12 =$sales_cgst_12 - $sales_ret_cgst_12;
                    $sales_cgst_05 =$sales_cgst_05 - $sales_ret_cgst_05;

                    $taxable = $sales_0+$sales_05+$sales_12+$sales_18+$sales_28;

                    $cgst = $sales_cgst_05+$sales_cgst_12+$sales_cgst_18+$sales_cgst_28;
                    ?>
                    <tr>
                        <td class="td_common_numeric_rules">0</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_0, 2,) }}</td>
                        <td class="td_common_numeric_rules">-</td>
                        <td class="td_common_numeric_rules">-</td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">5</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_05, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_05, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_05, 2,) }}</td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">12</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_12, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_12, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_12, 2,) }}</td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">18</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_18, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_18, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_18, 2,) }}</td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">28</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_28, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_28, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($sales_cgst_28, 2,) }}</td>
                    </tr>
                    <tr style="border-top: 1px solid #000">
                        <td class="common_td_rules" style="border-right:1px solid #fff !important"><b>Total</b></td>
                        <td class="td_common_numeric_rules">{{ number_format($taxable, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($cgst, 2,) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($cgst, 2,) }}</td>
                    </tr>
                    <?php
                }else{
                    ?>
                    <tr>
                        <td class=" common_td_rules"><b> Total Purchase </b></td>
                        <td colspan="20" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>


        </div>

    </div>
