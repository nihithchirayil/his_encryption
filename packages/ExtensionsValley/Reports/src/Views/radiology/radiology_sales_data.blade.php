<div class="row" id="print_data">
    <div class="col-md-12 " id="result_container_div">
        <div class="" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>
                    <?= date('M-d-Y h:i A') ?>
                </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From:
                <?= $from_date ?> To
                <?= $to_date ?>
            </p>
            
            <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>Radiology Sales Report</b></h4>
            <div class="col-md-12 no-padding" style=" ">
        <div class="" id="ResultDataContainer"
            style=" padding: 10px; display:none;font-family:poppinsregular;width: 1626px;">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadscroll'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif;max-height: 200px;overflow-x: auto;overflow-y: auto;">
                        <th width='1%'>Sn. No.</th>
                        <th width='15%'>Bill Number</th>
                        <th width='10%'>Bill Date</th>
                        <th width='10%'>Bill Time</th>
                        <th width='11%'>UHID</th>
                        <th width='8%'>Patient Name</th>
                        <th width='9%'>Doctor Name</th>
                        <th width='9%'>Procedure Doctor</th>
                        <th width='9%'>Speciality</th>
                        <th width='3%'>Visit Type</th>
                        <th width='12%'>Sub Department</th>
                        <th width='7%'>Item</th>
                        <th width='17%'>Location</th>
                        <th width='17%'>Billed Qty</th>
                        <th width='17%'>Return</th>
                        <th width='17%'>Gross</th>
                        <th width='17%'>Discount</th>
                        <th width='17%'>Net Amount</th>
                        <th width='17%'>Payment Status</th>
                        <th width='17%'>Billed Staff</th>
                        <th width='17%'>Payment Type</th>
                        <th width='17%'>Company Type</th>
                       
                    </tr>


                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr style="cursor: pointer;">
                                <td class="td_common_numeric_rules">{{ $i }}.</td>
                                <td class="common_td_rules">{{ $data->bill_no }}</td>
                                <td class="common_td_rules">{{ $data->bill_date }}</td>
                                <td class="common_td_rules">{{ $data->bill_time }}</td>
                                <td class="common_td_rules">{{ $data->uhid }}</td>
                                <td class="common_td_rules">{{ $data->patient_name }}</td>
                                <td class="common_td_rules">{{ $data->doctor_name }}</td>
                                <td class="common_td_rules">{{ $data->speciality }}</td>
                                <td class="common_td_rules">{{ $data->visit_type }}</td>
                                <td class="common_td_rules">{{ $data->sub_dept_name }}</td>
                                <td class="common_td_rules">{{ $data->item_desc }}</td>
                                <td class="common_td_rules">{{ $data->location_name }}</td>
                               <td class="common_td_rules">{{ $data->qty }}</td>
                               <td class="common_td_rules">{{ $data->return_qty }}</td>
                               <td class="common_td_rules">{{ $data->gross_amount }}</td>
                               <td class="common_td_rules">{{ $data->discount }}</td>
                               <td class="common_td_rules">{{ $data->net_amount }}</td>
                               <td class="common_td_rules">{{ $data->paid_status }}</td>
                               <td class="common_td_rules">{{ $data->billed_staff }}</td>
                               <td class="common_td_rules">{{ $data->payment_type }}</td>
                               <td class="common_td_rules">{{ $data->company_name }}</td>


                            </tr>

                            <?php $i++; ?>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
</div></div>
        </div>
    </div>
</div>
