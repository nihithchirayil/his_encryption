@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
<style>
  .liHover{
     background: #48705f;
  }
  .box_border {
      padding-top: 25px;
      padding-bottom: 10px;
      border: 1px solid #ccc;
      background: #f3f3f3;
  }
  .tinymce-content p {
      padding: 0;
      margin: 2px 0;
  }
  .content_add:hover{
   color: red;
   background-color: yellow;
  }
  .content_remove:hover{
   color: red;
   background-color: yellow;
  }
  .modal-header{
      background-color:orange;
  }

    #MTable > tbody > tr > td{
        word-wrap:break-word
      }
      table#posts {
        word-wrap: break-word;
    }
    .page{
        padding: 65px;
        min-height: 1134px;
        width: 812px;
    }



    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-xs-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        overflow-y: auto;
        width: 250px;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .ajaxSearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }

    .liHover{
        background: #4c6456 !important;
        color:white;
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400;
        font-family: "sans-serif";
        border-bottom: 1px solid white;
    }
    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }
    .liHover {
        background: #8e948f;
        color: white;
    }
    .filter_label {
       margin-bottom: 0px;
    }
    .btn-group {
        width:100% !important;
    }
    .col-xs-12{
        margin-bottom:8px;
    }
    .col-xs-6{
        margin-bottom:8px;
    }
    .filter_label {
        color: #294236 !important;
        font-weight: 600 !important;
    }
    tr:hover{
        font-size: 12.5px;
        color:blue !important;
        background-color: #f2f3cc !important;
    }
    @media print{
        .headerclass{
            background:#999;
        }
        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 1px #aaa;
        }
    }

</style>
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" value='<?= $route_data ?>' id="route_value">
    <input type="hidden" id="hospital_address" value="{{$hospital_address}}">
    <input type="hidden" id="current_date" value="{{date('M-d-Y')}}">

    <div class="right_col">
        <div class="container-fluid">

            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th colspan="11"><?= $title ?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                    <div class="col-md-12">
                        {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                        <div>
                            <?php
                            $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    $hidden_filed_name = $fieldTypeArray[$i][1] . 'title';
                                    ?>

                                    <div class= "col-xs-2">
                                        <label class="filter_label">
                                        {{$fieldTypeArray[$i][2]}}</label>
                                        <div class="mate-input-box">

                                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                        <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                        <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                    </div>
                                    </div>

                                    <?php
                                }else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>
                                    <div class="col-xs-2 date_filter_div">
                                        <label class="filter_label">{{$fieldTypeArray[$i][2]}} From</label>
                                        <div class="mate-input-box">
                                        <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                            if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            }
                            ?>  value="{{date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_from">
</div>
                                    </div>
                                    <div class="col-xs-2 date_filter_div">
                                        <label class="filter_label">{{$fieldTypeArray[$i][2]}} To</label>
                                        <div class="mate-input-box">
                                        <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                            if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            }
                                    ?>  value="{{date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_to">

                                    </div>
                                </div>
                                    <?php

                            } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>
                                        <div class="col-md-2 padding_sm date_filter_div">
                                            <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                            <div class="mate-input-box">
                                            <div class="clearfix"></div>
                                            {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2 filters datapicker','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1],'style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                                            </div>
                                        </div>
                                    <?php
                                }
                                $i++;

                            }
                            ?>

                            <div class="col-xs-12" style="text-align:right; padding: 0px;">
                                <div class="clearfix"></div>

                                <button type="reset" class="btn light_purple_bg" onclick="search_clear();"  name="clear_results" id="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </button>
                                <a class="btn light_purple_bg disabled" onclick="exceller_template_styled('Radiology Sales Report',18);"  name="csv_results" id="csv_results">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    Excel
                                </a>
                                <a class="btn light_purple_bg disabled" onclick="printReportData();"  name="print_results" id="print_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                                <a class="btn light_purple_bg" onclick="getRadioloagySales();"  name="search_results" id="search_results">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </a>

                            </div>
                            {!! Form::token() !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                        </div>
                    </div>
                </div>

            </div>


            <!-- <div class="col-md-12 padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="theadscroll always-visible" id="ResultDataContainer"
                        style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular;width: 1379px;
                        margin-left: -13px;">
                        <div style="background:#686666;">
                            <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                                                                width: 100%;
                                                                padding: 30px;" id="ResultsViewArea">

                            </page>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- <div class="col-md-12 no-padding" style="    overflow-y: hidden !important;">
        <div class="theadscroll always-visible" id="ResultDataContainer"
            style=" padding: 10px; display:none;font-family:poppinsregular;width: 1626px;"> -->
            <div class ="theadscroll always-visible" style="float: left; width: 100% !important;position: relative; height: 400px;" id="ResultsViewArea">


            </div>
        <!-- </div>
    </div> -->
        </div>
    </div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/radiology_sale.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{asset('packages/extensionsvalley/emr/js/report.js')}}"></script>

<script @endsection
