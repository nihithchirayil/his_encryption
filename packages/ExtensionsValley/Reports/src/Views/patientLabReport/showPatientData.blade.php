<div class="row padding_sm">
    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box">
                <label for="">Name</label>
                <?= @$res[0]->patient_name ? $res[0]->patient_name : '-' ?>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box">
                <label for="">UHID</label>
                <?= @$res[0]->uhid ? $res[0]->uhid : '-' ?>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box">
                <label for="">Gender</label>
                <?= @$res[0]->gender ? $res[0]->gender : '-' ?>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box">
                <label for="">DOB</label>
                <?= @$res[0]->dob ? $res[0]->dob : '-' ?>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box">
                <label for="">Phone</label>
                <?= @$res[0]->phone ? $res[0]->phone : '-' ?>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box">
                <label for="">Email</label>
                <?= @$res[0]->email ? $res[0]->email : '-' ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
        <div class="blue"><strong> Showing Report Date From: <?= date('M-d-Y', strtotime($from_date)) ?> To
                <?= date('M-d-Y', strtotime($to_date)) ?></strong></div>
    </div>
    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
        <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;width: 25% !important">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='10%'>SI.No.</th>
                    <th width='75%'>Finalized At</th>
                    <th width='15%'>Download</th>
                </tr>


            </thead>
            <tbody>
                @if (count($result) != 0)
                    @foreach ($result as $data)
                        <tr>
                            <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                            <td class="common_td_rules">
                                {{ $data->created_at ? date('M-d-Y H:i:s', strtotime($data->created_at)) : '-' }}
                            </td>
                            <td style="text-align: center"><a class="btn btn-warning btn-block" target="_blank"
                                    href="<?= env('LAB_REPORT_FILE_PREFIX_PUBLIC','') . $data->file_name ?>" download><i
                                        class="fa fa-download"></i></a></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
