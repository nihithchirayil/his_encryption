@extends('Dashboard::dashboard.notUserDashboard')
@section('content-header')
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">

@endsection
@section('content-area')

    <div class="right_col" style="margin-left: 0px !important" role="main">
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <h5 style="text-align: center" class="blue pull-right"><?= $title ?></h5>
            </div>
            <div class="col-md-12 padding_sm">
                <?= base64_decode($hospital_header) ?>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
            <input type="hidden" id="smstrack_id" value="">
        </div>

        <div class="x_panel">
            <div class="row padding_sm">
                <div class="col-md-12 no-padding no-margin">
                    <div class="col-md-4 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 56px;">
                                <div class="col-md-9 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="red">Enter UHID *</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="searchpatient_uhid"
                                            name="searchpatient_uhid" value="">
                                        <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                        <input type="hidden" name="patient_name_hidden" value="" id="patient_name_hidden">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 13px">
                                    <button type="button" onclick="searchPatient()" class="btn btn-primary btn-block"
                                        id="search_patientBtn">Search <i class="fa fa-search"
                                            id="search_patientSpin"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" id="patientOTPDiv" style="display: none">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 56px;">
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label id="patient_phone_hidden">Enter OTP</label>
                                        <button type="button" onclick="sendOTP()" class="btn btn-primary btn-block"
                                            id="sendPatientOtpBtn"><span id="sendPatientOtpBtnSpan">Send OTP </span> <i
                                                class="fa fa-comment" id="sendPatientOtpSpin"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" id="patientOTPEnterDiv" style="display: none">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 56px;">
                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Time Ends in...</label>
                                        <div class="clearfix"></div>
                                        <div style="margin-top: 5px">
                                            <span class="red" id="timer">
                                                <span id="time"></span> Seconds Remaining
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Enter OTP</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="enterPatientOtp"
                                            name="enterPatientOtp" value="">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm pull-right" style="margin-top: 13px">
                                    <button type="button" onclick="submitOTP()" class="btn btn-success btn-block"
                                        id="submitatientOtpBtn">Submit <i class="fa fa-save"
                                            id="submitPatientOtpSpin"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" id="patientOTPEnterTimeOutDiv" style="display: none">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 56px;">
                                <div class="col-md-12 padding_sm tex-center red" style="margin-top: 15px">
                                    OTP gets time out please resend and try again
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding no-margin" style="margin-top: 10px" id="patientDataDiv"></div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/patientLabReports.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
@endsection
