<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>
                    <?= date('M-d-Y h:i A') ?>
                </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From:
                <?= $from ?> To
                <?= $to ?>
            </p>
            <?php
            $collect = collect($post_data);
            $total_records = count($collect);
            $net_amount_tot=0.0;
            $net_amountret_tot=0.0;
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Sales GST Report - Bill wise</b></h4>
            <div id="result_data_table">
                <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                    <thead>
                        <tr class="table_header_bg">
                            <th colspan="19">Sales</th>
                        </tr>
                        <tr class="table_header_bg">
                            <th width="10%">DATE</th>
                            <th width="20%">Bill No</th>
                            <th width="3%">sales 0 %</th>
                            <th width="3%">sales 3%</th>
                            <th width="3%">CGST 1.5%</th>
                            <th width="3%">SGST 1.5%</th>
                            <th width="3%">sales 5%</th>
                            <th width="3%">CGST 2.5 %</th>
                            <th width="3%">SGST 2.5%</th>
                            <th width="3%">sales 12%</th>
                            <th width="3%">CGST 6%</th>
                            <th width="3%">SGST 6%</th>
                            <th width="3%">sales 18%</th>
                            <th width="3%">CGST 9%</th>
                            <th width="3%">SGST 9%</th>
                            <th width="3%">sales 28%</th>
                            <th width="3%">CGST 14%</th>
                            <th width="3%">SGST 14%</th>
                            <th width="3%">BILL AMT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                if (count($post_data) != 0) {
                    $sales_zero=0.0;
                    $sales_three=0.0;
                    $sales_sthree=0.0;
                    $sales_cthree=0.0;
                    $sales_ithree=0.0;
                    $sales_five=0.0;
                    $sales_sfive=0.0;
                    $sales_cfive=0.0;
                    $sales_ifive=0.0;
                    $sales_twelve=0.0;
                    $sales_stwelve=0.0;
                    $sales_ctwelve=0.0;
                    $sales_itwelve=0.0;
                    $sales_eigtheen=0.0;
                    $sales_seigtheen=0.0;
                    $sales_ceigtheen=0.0;
                    $sales_ieigtheen=0.0;
                    $sales_twntyeight=0.0;
                    $sales_ctwntyeight=0.0;
                    $sales_stwntyeight=0.0;
                    $sales_itwntyeight=0.0;
                    foreach ($post_data as $each) {
                        $sales_zero+=floatval($each['SALES0']);
                        $sales_three+=floatval($each['SALES3']);
                        $sales_cthree+=floatval($each['CGST15']);
                        $sales_sthree+=floatval($each['SGST15']);

                        $sales_five+=floatval($each['SALES5']);
                        $sales_cfive+=floatval($each['CGST25']);
                        $sales_sfive+=floatval($each['SGST25']);

                        $sales_twelve+=floatval($each['SALES12']);
                        $sales_ctwelve+=floatval($each['CGST6']);
                        $sales_stwelve+=floatval($each['SGST6']);

                        $sales_eigtheen+=floatval($each['SALES18']);
                        $sales_ceigtheen+=floatval($each['CGST9']);
                        $sales_seigtheen+=floatval($each['SGST9']);


                        $sales_twntyeight+=floatval($each['SALES28']);
                        $sales_ctwntyeight+=floatval($each['CGST14']);
                        $sales_stwntyeight+=floatval($each['SGST14']);
                        $total_tax= $each['SALES0']+$each['SALES3']+$each['CGST15']+$each['SGST15']+$each['IGST3']+$each['SALES5']+$each['CGST25']+$each['SGST25']+$each['IGST5']+$each['SALES12']+$each['CGST6']+$each['SGST6']+$each['IGST12']+$each['SALES18']+$each['CGST9']+$each['SGST9']+$each['IGST18']+$each['SALES28']+$each['CGST14']+$each['SGST14']+$each['IGST28'];
                        $net_amount_tot += $total_tax;
                    ?>
                        <tr>
                            <td class="common_td_rules">
                                <?= $each['bill_date'] ?>
                            </td>
                            <td>
                                <?= $each['bill_no'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES0'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES3'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST15'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST15'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES5'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST25'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST25'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES12'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST6'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST6'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES18'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST9'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST9'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES28'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST14'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST14'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $total_tax ?>
                            </td>
                        </tr>
                        <?php
                }
                ?>
                        <tr>
                            <th class="common_td_rules" colspan="2">
                                Total
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_zero }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_three }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_cthree }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_sthree }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_five }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_cfive }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_sfive }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_twelve }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_ctwelve }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_stwelve }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_eigtheen }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_ceigtheen }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_seigtheen }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_twntyeight }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_ctwntyeight }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_stwntyeight }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $net_amount_tot }}
                            </th>
                        </tr>
                        <?php
            } else {
                ?>
                        <tr class="common_td_rules">
                            <td colspan="33" style="text-align: center;"> No Result Found</td>
                        </tr>
                        <?php
            }
            ?>

                    </tbody>
                </table>

                <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                    <thead>
                        <tr class="table_header_bg">
                            <th colspan="19">Sales Return</th>
                        </tr>
                        <tr class="table_header_bg">
                            <th width="10%">DATE</th>
                            <th width="20%">Bill No</th>
                            <th width="3%">sales 0 %</th>
                            <th width="3%">sales 3%</th>
                            <th width="3%">CGST 1.5%</th>
                            <th width="3%">SGST 1.5%</th>
                            <th width="3%">sales 5%</th>
                            <th width="3%">CGST 2.5 %</th>
                            <th width="3%">SGST 2.5%</th>
                            <th width="3%">sales 12%</th>
                            <th width="3%">CGST 6%</th>
                            <th width="3%">SGST 6%</th>
                            <th width="3%">sales 18%</th>
                            <th width="3%">CGST 9%</th>
                            <th width="3%">SGST 9%</th>
                            <th width="3%">sales 28%</th>
                            <th width="3%">CGST 14%</th>
                            <th width="3%">SGST 14%</th>
                            <th width="3%">BILL AMT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                if (count($sales_array_return) != 0) {
                    $sales_zero=0.0;
                    $sales_three=0.0;
                    $sales_sthree=0.0;
                    $sales_cthree=0.0;
                    $sales_ithree=0.0;
                    $sales_five=0.0;
                    $sales_sfive=0.0;
                    $sales_cfive=0.0;
                    $sales_ifive=0.0;
                    $sales_twelve=0.0;
                    $sales_stwelve=0.0;
                    $sales_ctwelve=0.0;
                    $sales_itwelve=0.0;
                    $sales_eigtheen=0.0;
                    $sales_seigtheen=0.0;
                    $sales_ceigtheen=0.0;
                    $sales_ieigtheen=0.0;
                    $sales_twntyeight=0.0;
                    $sales_ctwntyeight=0.0;
                    $sales_stwntyeight=0.0;
                    $sales_itwntyeight=0.0;
                    foreach ($sales_array_return as $each) {
                        $sales_zero+=floatval($each['SALES0']);
                        $sales_three+=floatval($each['SALES3']);
                        $sales_cthree+=floatval($each['CGST15']);
                        $sales_sthree+=floatval($each['SGST15']);

                        $sales_five+=floatval($each['SALES5']);
                        $sales_cfive+=floatval($each['CGST25']);
                        $sales_sfive+=floatval($each['SGST25']);

                        $sales_twelve+=floatval($each['SALES12']);
                        $sales_ctwelve+=floatval($each['CGST6']);
                        $sales_stwelve+=floatval($each['SGST6']);

                        $sales_eigtheen+=floatval($each['SALES18']);
                        $sales_ceigtheen+=floatval($each['CGST9']);
                        $sales_seigtheen+=floatval($each['SGST9']);


                        $sales_twntyeight+=floatval($each['SALES28']);
                        $sales_ctwntyeight+=floatval($each['CGST14']);
                        $sales_stwntyeight+=floatval($each['SGST14']);
                        $total_tax= $each['SALES0']+$each['SALES3']+$each['CGST15']+$each['SGST15']+$each['IGST3']+$each['SALES5']+$each['CGST25']+$each['SGST25']+$each['IGST5']+$each['SALES12']+$each['CGST6']+$each['SGST6']+$each['IGST12']+$each['SALES18']+$each['CGST9']+$each['SGST9']+$each['IGST18']+$each['SALES28']+$each['CGST14']+$each['SGST14']+$each['IGST28'];
                        $net_amountret_tot += $total_tax;
                    ?>
                        <tr>
                            <td class="common_td_rules">
                                <?= $each['bill_date'] ?>
                            </td>
                            <td>
                                <?= $each['bill_no'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES0'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES3'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST15'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST15'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES5'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST25'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST25'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES12'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST6'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST6'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES18'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST9'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST9'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SALES28'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['CGST14'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $each['SGST14'] ?>
                            </td>
                            <td class="td_common_numeric_rules">
                                <?= $total_tax ?>
                            </td>
                        </tr>
                        <?php
                }
                ?>
                        <tr>
                            <th class="common_td_rules" colspan="2">
                                Total
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_zero }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_three }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_cthree }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_sthree }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_five }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_cfive }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_sfive }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_twelve }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_ctwelve }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_stwelve }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_eigtheen }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_ceigtheen }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_seigtheen }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_twntyeight }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_ctwntyeight }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $sales_stwntyeight }}
                            </th>
                            <th class="td_common_numeric_rules">
                                {{ $net_amountret_tot }}
                            </th>
                        </tr>
                        <?php
            } else {
                ?>
                        <tr class="common_td_rules">
                            <td colspan="20" style="text-align: center;"> No Result Found</td>
                        </tr>
                        <?php
            }
            ?>
                        <tr>
                            <th class="common_td_rules" colspan="18">
                                Net Sales(Sales-Sales Return)
                            </th>
                            <th class="td_common_numeric_rules">
                                <?= round(floatval($net_amount_tot)-floatval($net_amountret_tot))?>
                            </th>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
