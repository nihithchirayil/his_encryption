@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->

    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="quotationListModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="quotationListHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px" id="quotationListDiv">

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="right_col">
        <div class="row pull-right" style="font-size: 12px; font-weight: bold;">
            {{ $title }}
            <div class="clearfix"></div>
            <div class="h10"></div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="hospital_header" value="{{ $hospital_headder }}">
            <input type="hidden" id="current_date" value="<?= $current_date ?>">

        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="fromdate" name=""
                                        value="<?= $current_date ?>">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="todate" name=""
                                        value="<?= $current_date ?>">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">RFQ Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="rfq_number"
                                        name="rfq_number" value="">
                                    <div id="rfq_idAjaxDiv" class="ajaxSearchBox"></div>
                                    <input type="hidden" name="rfq_id_hidden" value="" id="rfq_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Location</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="location">
                                        <option value="">Location</option>
                                        <?php
                                            foreach($location as $each){
                                                ?>
                                        <option value='<?= $each->location_code ?>'><?= $each->location_name ?>
                                        </option>
                                        <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="clearfix"></div>

                            <div class="col-md-12 padding_sm" style="text-align:right;">
                                <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                    <button class="btn light_purple_bg btn-block" type="button" onclick="getReportData();"
                                        name="search_results" id="search_results">
                                        <i id="search_results_spin" class="fa fa-search" aria-hidden="true"></i>
                                        Search
                                    </button>
                                </div>
                                <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                    <button class="btn light_purple_bg disabled btn-block" type="button"
                                        onclick="exceller();" name="csv_results" id="csv_results">
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                        Excel
                                    </button>
                                </div>
                                <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                    <button class="btn light_purple_bg disabled btn-block" type="button"
                                        onclick="printReportData();" name="print_results" id="print_results">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                        Print
                                    </button>
                                </div>
                                <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                    <button class="btn light_purple_bg btn-block" type="button" onclick="search_clear();"
                                        name="clear_results" id="clear_results">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm">
                    <div id="ResultDataContainer"
                        style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                        <div style="background:#686666;">
                            <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 1cm; box-shadow: 0 0 1cm rgb(113 113 113 / 50%);
                        width: 100%; padding: 50px;" id="ResultsViewArea">
                            </page>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/rfq_report.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>



@endsection
