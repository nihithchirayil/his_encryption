<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(8, 98, 47);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th colspan="11" style="text-align: center">
                            Item Stock - Tax Wise Report
                        </th>
                    </tr>
                    <tr class="headerclass"
                    style="background-color:rgb(8, 98, 47);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="6%">SL.NO.</th>
                        <th width="15%">Item Description</th>
                        <th width="8%">Quantity</th>
                        <th width="8%">Unit Purchase Cost</th>
                        <th width="8%">Total Purchase Cost</th>
                        <th width="8%">Total Purchase Rate</th>
                        <th width="8%">Unit Selling Price</th>
                        <th width="8%">Total Selling Price</th>
                        <th width="8%">Unit Cost without Tax</th>
                        <th width="8%">Total Cost without Tax</th>
                        @if ($show_location == 1)
                            <th width="15%">Location Name</th>
                        @endif
                    </tr>
                </thead>
                <?php
                $i = 0;
                $unit_tax_rate = '';
                $slno = 1;
                $nettot_stock = 0.0;
                $net_unit_purchase_cost = 0.0;
                $nettot_cal_stock = 0.0;
                $nettot_purchase_rate = 0.0;
                $nettot_unit_selling_price = 0.0;
                $nettot_total_selling_price = 0.0;
                $nettot_unit_cost_without_tax = 0.0;
                $nettot_total_cost_without_tax = 0.0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
         foreach ($res as $data){
             if($unit_tax_rate != $data->unit_tax_rate){
                 $slno=1;
                 $i=0;
                 $unit_tax_rate = $data->unit_tax_rate;
                 $tot_unit_purchase_cost=0.0;
                 $tot_stock=0.0;
                 $tot_cal_stock=0.0;
                 $tot_purchase_rate=0.0;
                 $tot_unit_selling_price=0.0;
                 $tot_total_selling_price=0.0;
                 $tot_unit_cost_without_tax=0.0;
                 $tot_total_cost_without_tax=0.0;
               ?>

                        <tr class="headerclass"
                            style="background-color:rgb(32,178,170);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            @if ($show_location == 1)
                                <th colspan="11" class="common_td_rules">Unit Tax Rate : {{ $unit_tax_rate }}</th>
                            @else
                                <th colspan="10" class="common_td_rules">Unit Tax Rate : {{ $unit_tax_rate }}</th>
                            @endif
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $slno }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->stock ? $data->stock : 0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->unit_purchase_cost ? $data->unit_purchase_cost : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_unit_purchase_cost ? $data->total_unit_purchase_cost : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_purchase_rate ? $data->total_purchase_rate : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->unit_selling_price ? $data->unit_selling_price : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_selling_price ? $data->total_selling_price : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->unit_cost_without_tax ? $data->unit_cost_without_tax : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_cost_without_tax ? $data->total_cost_without_tax : 0.0 }}</td>
                            @if ($show_location == 1)
                                <td class="common_td_rules">{{ $data->location_name }}</td>
                            @endif
                        </tr>
                        <?php
             }else{
                 ?>
                        <tr>
                            <td class="common_td_rules">{{ $slno }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->stock ? $data->stock : 0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->unit_purchase_cost ? $data->unit_purchase_cost : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_unit_purchase_cost ? $data->total_unit_purchase_cost : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_purchase_rate ? $data->total_purchase_rate : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->unit_selling_price ? $data->unit_selling_price : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_selling_price ? $data->total_selling_price : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->unit_cost_without_tax ? $data->unit_cost_without_tax : 0.0 }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->total_cost_without_tax ? $data->total_cost_without_tax : 0.0 }}</td>
                            @if ($show_location == 1)
                                <td class="common_td_rules">{{ $data->location_name }}</td>
                            @endif
                        </tr>
                        <?php

             }
                 $tot_stock+=floatval($data->stock);
                 $tot_unit_purchase_cost+=floatval($data->unit_purchase_cost);
                 $tot_cal_stock+=floatval($data->total_unit_purchase_cost);
                 $tot_purchase_rate+=floatval($data->total_purchase_rate);
                 $tot_unit_selling_price+=floatval($data->unit_selling_price);
                 $tot_total_selling_price+=floatval($data->total_selling_price);
                 $tot_unit_cost_without_tax+=floatval($data->unit_cost_without_tax);
                 $tot_total_cost_without_tax+=floatval($data->total_cost_without_tax);
                 if($i==$unit_tax_rateccnt[$unit_tax_rate]){
             ?>
                        <tr class="headerclass"
                            style="background-color:rgb(102,205,170);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="2" class="common_td_rules" style="text-align: left;">Sub Total</th>
                            <th class="td_common_numeric_rules"><?= $tot_stock ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_unit_purchase_cost ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_cal_stock ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_purchase_rate ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_unit_selling_price ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_total_selling_price ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_unit_cost_without_tax ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_total_cost_without_tax ?></th>
                            @if ($show_location == 1)
                                <th class="td_common_numeric_rules"></th>
                            @endif
                        </tr>
                        <?php
                    $nettot_stock+=$tot_stock;
                    $net_unit_purchase_cost+=$tot_unit_purchase_cost;
                    $nettot_cal_stock+=$tot_cal_stock;
                    $nettot_purchase_rate+=$tot_purchase_rate;
                    $nettot_unit_selling_price+=$tot_unit_selling_price;
                    $nettot_total_selling_price+=$tot_total_selling_price;
                    $nettot_unit_cost_without_tax+=$tot_unit_cost_without_tax;
                    $nettot_total_cost_without_tax+=$tot_total_cost_without_tax;
             }
             $i++;
             $slno++;

         }
         ?>
                        <tr class="headerclass"
                            style="background-color:rgb(135,206,250);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="2" class="common_td_rules" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $nettot_stock ?></th>
                            <th class="td_common_numeric_rules"><?= $net_unit_purchase_cost ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_cal_stock ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_purchase_rate ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_unit_selling_price ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_total_selling_price ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_unit_cost_without_tax ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_total_cost_without_tax ?></th>
                            @if ($show_location == 1)
                                <th class="td_common_numeric_rules"></th>
                            @endif
                        </tr>
                    @else
                        <tr>
                            <td colspan="10" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
            </font>
        </div>
    </div>
</div>
