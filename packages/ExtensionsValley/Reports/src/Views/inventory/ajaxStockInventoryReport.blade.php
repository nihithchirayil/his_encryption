<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Stock Ledger Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="12">Date</th>
                        <th width="10%">Transc. No</th>
                        <th width="11%">Patient Name</th>
                        <th width="11%">UHID</th>
                        <th width="10%">Transc. Type</th>
                        <th width="11%">Location From</th>
                        <th width="11%">Location To</th>
                        <th width="7%">Receipt Qty</th>
                        <th width="7%">Issue Qty</th>
                        <th width="7%">Net Balance Qty</th>
                    </tr>
                </thead>
                <?php
                $i = 0;
                $item_desc = '';
                $tot_receipt_qty = 0;
                $nettot_receipt_qty = 0;
                $tot_issue_qty = 0;
                $nettot_issue_qty = 0;
                $tot_net_balance = 0;
                $nettot_net_balance = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
         foreach ($res as $data){
             if($item_desc != $data->item_desc){
                 $i=0;
                 $item_desc = $data->item_desc;
                 $tot_receipt_qty=0;
                 $tot_issue_qty=0;
                 $tot_net_balance=0;
               ?>

                        <tr class="headerclass"
                            style="background-color:rgb(32,178,170);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="10" class="common_td_rules">{{ $item_desc }}</th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:rgb(0,139,139);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="10" class="common_td_rules">Opening Balance : <?=$data->openingstock?> </th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ date('M-d-Y H:i:s', strtotime($data->date)) }}</td>
                            <td class="common_td_rules">{{ $data->transaction_no }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->opno }}</td>
                            <td class="common_td_rules">{{ $data->transaction_name }}</td>
                            <td class="common_td_rules">{{ $data->from_loc }}</td>
                            <td class="common_td_rules">{{ $data->to_loc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->receipt_qty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->issueqty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->balance_qty }}</td>
                        </tr>
                        <?php
             }else{
                 ?>
                        <tr>
                            <td class="common_td_rules">{{ date('M-d-Y h:i A', strtotime($data->date)) }}</td>
                            <td class="common_td_rules">{{ $data->transaction_no }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->opno }}</td>
                            <td class="common_td_rules">{{ $data->transaction_name }}</td>
                            <td class="common_td_rules">{{ $data->from_loc }}</td>
                            <td class="common_td_rules">{{ $data->to_loc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->receipt_qty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->issueqty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->balance_qty }}</td>
                        </tr>
                        <?php

             }
                 $tot_receipt_qty+=floatval($data->receipt_qty);
                 $tot_issue_qty+=floatval($data->issueqty);
                 $tot_net_balance+=floatval($data->balance_qty);
                 if($i==$item_desccnt[$item_desc]){
             ?>
                        <tr class="headerclass"
                            style="background-color:rgb(102,205,170);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th class="common_td_rules" colspan="7" style="text-align: left;">Sub Total</th>
                            <th class="td_common_numeric_rules"><?= $tot_receipt_qty ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_issue_qty ?></th>
                            <th class="td_common_numeric_rules">-</th>

                        </tr>
                        <?php
                        $nettot_receipt_qty+=$tot_receipt_qty;
                        $nettot_issue_qty+=$tot_issue_qty;
                        $nettot_net_balance+=$tot_net_balance;
             }
             $i++;

         }
         ?>
                        <tr class="headerclass"
                            style="background-color:rgb(135,206,250);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th class="common_td_rules" colspan="7" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $nettot_receipt_qty ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_issue_qty ?></th>
                            <th class="td_common_numeric_rules">-</th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="10" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
            </font>
        </div>
    </div>
</div>
