<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> RFQ Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="2">Si.No.</th>
                        <th width="15%">RFQ No</th>
                        <th width="15%">Created At</th>
                        <th width="43%">Supply Location</th>
                        <th width="15%">Status</th>
                        <th width="10%">RFQ Items</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                         if (count($res) != 0){
                    foreach ($res as $data){
                    $status="RFQ Drafted";
                    if($data->status=='1'){
                        $status="RFQ Generated";
                    }
               ?>
                    <tr>
                        <td class="common_td_rules">{{ $i }}</td>
                        <td class="common_td_rules">{{ $data->rfq_no }}</td>
                        <td class="common_td_rules">{{ trim($data->rfq_date) }}</td>
                        <td class="common_td_rules">{{ $data->location_name }}</td>
                        <td class="common_td_rules">{{ $status }}</td>
                        <td>
                            <button title="RFQ View" id="listQuotationItemsBtn<?= $data->head_id ?>" type="button"
                                class="btn bg-purple" onclick="listQuotationItems(<?= $data->head_id ?>)"><i
                                    id="listQuotationItemsSpin<?= $data->head_id ?>" class="fa fa-list"></i>
                            </button>
                        </td>
                    </tr>
                    <?php
                        $i++;
                }
             }else{
                    ?>
                    <tr>
                        <td colspan="7" style="text-align: center;">No Results Found!</td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
