<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Stock Ledger Report New</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="10%">Item</th>
                        <th width="8%">Date</th>
                        <th width="8%">Transaction No</th>
                        <th width="10%">Opening Stock</th>
                        <th width="6%">Pur. Qty</th>
                        <th width="8%">Transfer In</th>
                        <th width="8%">Transfer Out</th>
                        <th width="5%">Sales</th>
                        <th width="8%">Sales Return</th>
                        <th width="8%">Stock Adj. in</th>
                        <th width="11%">Stock Adj. Out</th>
                        <th width="10%">Closing stock</th>

                    </tr>
                </thead>

                @if (sizeof($res) > 0)
                    <tbody>
                        @foreach ($res as $data)

                            <tr>
                                <td class="common_td_rules"><strong>{{ $data->item_desc }}</strong></td>
                                <td class="common_td_rules"><strong>{{ date('M-d-Y', strtotime($data->created_at)) }}</strong></td>
                                <td>{{ $data->transaction_no ? $data->transaction_no : 0 }}</td>
                                <td>{{ $data->opening_stock ? $data->opening_stock : 0 }}</td>
                                <td>{{ $data->purchase ? $data->purchase : 0  }}</td>
                                <td>{{ @$data->stock_transfer_in ? $data->stock_transfer_in : 0 }}</td>
                                <td>{{ @$data->stock_transfer_out ? $data->stock_transfer_out : 0 }}</td>
                                <td>{{ @$data->sales ? $data->sales : 0 }}</td>
                                <td>{{ @$data->sales_return ? $data->sales_return : 0 }}</td>
                                <td>{{ @$data->stock_adjustment_in ? $data->stock_adjustment_in : 0 }}</td>
                                <td>{{ @$data->stock_adjustment_out ? $data->stock_adjustment_out : 0 }}</td>
                                <td>{{ @$data->closingstock ? $data->closingstock : 0 }}</td>


                            </tr>
                        @endforeach

                    @else
                        <tr>
                            <td colspan="10" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
</div>
