<div class="nav-tabs-custom">
    <ul class="nav nav-tabs sm_nav nav-pills">
        <li class="active"><a data-toggle="tab" href="#tab_1" id="">Stock Position <i id="" class=""></i></a></li>
        <li><a data-toggle="tab" href="#tab_2" id="" onclick="purchaseDetails(1)">Purchase History <i id="" class=""></i></a></li>
        <li><a data-toggle="tab" href="#tab_3" id="" onclick="productDetails()">Product Details <i id="" class=""></i></a></li>
        <li><a data-toggle="tab" href="#tab_4" id="" onclick="summaryDetails()">Summary <i id="" class=""></i></a></li>
        <li><a data-toggle="tab" href="#tab_5" id="" >Purchase/Sale <i id="" class=""></i></a></li>
    </ul>
    <div class="tab-content" style="min-height: 535px;">
        <div id="tab_1" class="tab-pane active">
            <div class="row">
                <div class="col-md-12" style="padding-top: 10px;">
                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="col-md-8 bg-primary sm-padding">
                        <b>Location-wise Stock Position : {{ $item_desc }}</b>
                    </div>
                    <div class="col-md-4 bg-primary sm-padding">
                        <b>Total Stock : {{ $total_stock }}</b>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="h10"></div>
                <div class="col-md-12">
                    <div class="table-responsive theadscroll"  style="height: 165px;">
                        <table class="table table_round_border styled-table">
                            <thead>
                                <tr class="table_header_bg">
                                    <th style="text-align:center">Location</th>
                                    <th style="text-align:center">Total Stock</th>
                                    <th style="text-align:center">Expiry Total Stock</th>
                                    <th style="text-align:center">RO - Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($stockDetails))
                                @foreach($stockDetails as $st)
                                <tr>
                                    <td class="common_td_rules">{{ $st->location_name }}</td>
                                    <td class="td_common_numeric_rules">{{ !empty($st->stock) ? $st->stock:'0.00' }}</td>
                                    <td class="td_common_numeric_rules">{{ !empty($st->expired_qty) ? $st->expired_qty:'0.00' }}</td>
                                    <td class="td_common_numeric_rules">{{ !empty($st->reorder_level) ?$st->reorder_level :'0.00' }}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5">No Records Found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 sm-col-padding">
                    <h4><b>Batch Details</b></h4>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive theadscroll"  style="height: 225px;">
                        <table class="table table_round_border styled-table">
                            <thead>
                                <tr class="table_header_bg">
                                    <th style="text-align:center">Location</th>
                                    <th style="text-align:center">Batch</th>
                                    <th style="text-align:center">Expiry</th>
                                    <th style="text-align:center">Stock</th>
                                    <th style="text-align:center">MRP</th>
                                    <th style="text-align:center">Pur.Cost</th>
                                    <th style="text-align:center">Selling Rate</th>
                                    <th style="text-align:center">Tax</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($batchDetails))
                                @foreach($batchDetails as $btch)
                                <tr>
                                    <td class="common_td_rules">{{ $btch->location_name }}</td>
                                    <td class="common_td_rules">{{ $btch->batch_no }}</td>
                                    <td class="common_td_rules">{{ $btch->exp_dt }}</td>
                                    <td class="td_common_numeric_rules">{{ $btch->batch_stock }}</td>
                                    <td class="td_common_numeric_rules">{{ $btch->unit_mrp }}</td>
                                    <td class="td_common_numeric_rules">{{ $btch->unit_purchase_cost }}</td>
                                    <td class="td_common_numeric_rules">{{ $btch->unit_selling_price }}</td>
                                    <td class="td_common_numeric_rules">{{ $btch->unit_tax_amount }}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="9">No Records Found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab_2" class="tab-pane">
            <div class="col-md-12 padding_sm">
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box" style="height:40px">
                        <label style="margin-top:-4px">Bill From Date</label>
                        <div class="clearfix"></div>
                        <input class="form-control datepicker" type="text" autocomplete="off" id="purchaseFromDate"
                            name="purchaseFromDate">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box" style="height:40px">
                        <label style="margin-top:-4px">Bill To Date</label>
                        <div class="clearfix"></div>
                        <input class="form-control datepicker" type="text" autocomplete="off" id="purchaseToDate"
                            name="purchaseToDate">
                    </div>
                </div>
                <div class="col-md-1 padding_sm" style="margin-top: 20px;">
                    <button type="button" class="btn btn-primary btn-block" onclick="purchaseDetails(2)">Search </button>
                </div>
                <div class="col-md-1 padding_sm" style="margin-top: 20px;">
                    <button type="button" class="btn btn-warning btn-block" onclick="resetPurchaseDetails()">Reset </button>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm" id="purchaseDetailData"></div>
        </div>

        <div id="tab_3" class="tab-pane">

        </div>
        <div id="tab_4" class="tab-pane">

        </div>
        <div id="tab_5" class="tab-pane">
            <div class="row">
                <div class="col-md-12" style="padding-top: 10px;">
                    <div class="col-md-12 bg-primary sm-padding">
                        <b>Purchase/Sale : {{ $item_desc }}</b>
                    </div>
                </div>

                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label for="">From Date</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control datepicker" id="from_date" name=""
                        value="">
                    </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                        <label for="">To Date</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control datepicker" id="to_date" name=""
                        value="">
                    </div>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <span class="btn btn-block btn-success" onclick="pursaleDetails()">
                            <i class="fa fa-search"></i>Search</span>
                    </div>
            <!-- page content -->


                <div class="clearfix"></div>
                <div class="h10"></div>
                <div id="tab_5_html">
                </div>
        </div>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
