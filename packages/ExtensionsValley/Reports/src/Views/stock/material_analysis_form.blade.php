@extends('Dashboard::dashboard.dashboard')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
    /*   Change Color for Active Tabs     */
    .nav-pills>li.active>a,
    .nav-pills>li.active>a:focus,
    .nav-pills>li.active>a:hover,
    .nav-pills>li>a:hover,
    .nav-pills>li>a:hover {
      background-color: #F47321 ;
      color: #FFFFFF;
    }
    .nav-pills>li>a {
    padding: 3px 6px !important;
    }

    /*   Change Color for Inactive Tabs     */
    .nav-pills>li>a {
      background-color: #01987a;
      color: #FFFFFF;

    }
    .ajaxSearchBox{
        top: 50px !important;
        z-index: 99;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" >
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 66px;"> {{$title}}
        <div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">

            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Category</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="category" id="category">
                                <input type="hidden" id="category_hidden" name="category_hidden">
                                <div id="category_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Sub category</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="sub_category" id="sub_category">
                                <input type="hidden" id="sub_category_hidden" name="sub_category_hidden">
                                <div id="sub_category_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Vendor</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="vendor" id="vendor">
                                <input type="hidden" id="vendor_hidden" name="vendor_hidden">
                                <div id="vendor_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Generic Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="generic_name" id="generic_name">
                                <input type="hidden" id="generic_name_hidden" name="generic_name_hidden">
                                <div id="generic_name_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Product</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="product" id="product">
                                <input type="hidden" id="product_hidden" name="product_hidden">
                                <div id="product_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>


                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning">
                                <i class="fa fa-times"></i>  Clear</a>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-success" onclick="fill_table_values()">
                                <i class="fa fa-search"></i>  Search</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-2 sm-padding" id="product_tbody" style="min-height: 470px;background-color: #d9edf7">

            </div>

            <div class="col-md-10 sm-padding">
                 <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div id="product_tab_container">

                        </div>


                    </div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
            <input type="hidden" name="stock_ajax" id="stock_ajax" value="0"/>
            <input type="hidden" name="purchase_ajax" id="purchase_ajax" value="0"/>
            <input type="hidden" name="product_ajax" id="product_ajax" value="0"/>
            <input type="hidden" name="summary_ajax" id="summary_ajax" value="0"/>
            <input type="hidden" name="sale_ajax" id="sale_ajax" value="0"/>
        </div>
    </div>

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
@stop
@section('javascript_extra')
<script type="text/javascript">
$(document).ready(function () {
setTimeout(function(){
    // $('#menu_toggle').trigger('click');
    $(".theadfix_wrapper").floatThead('reflow');
}, 1000);
    setTimeout(function () {
        // $('#menu_toggle').trigger('click');
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    }, 300);


    $('.month_picker').datetimepicker({
        format: 'MM'
    });
    $('.year_picker').datetimepicker({
        format: 'YYYY'
    });

    $('.datepicker').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    $('.date_time_picker').datetimepicker();
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
});

//------------------------------------------------------------------------------------
$('#vendor').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var vendor = $(this).val();
        vendor = vendor.trim();
        if (vendor == "") {
            $("#vendor_searchCodeAjaxDiv").html("");
            $("#vendor_hidden").val("");
        } else {
            try {
                var param = {vendor: vendor};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#vendor_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                        }
                        $("#vendor_searchCodeAjaxDiv").html(html).show();
                        $("#vendor_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('vendor_searchCodeAjaxDiv', event);
    }
});
$('#vendor').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('vendor_searchCodeAjaxDiv');
        return false;
    }
});
function fill_vendor_details(e, vendor_name, vendor_id) {
    $('#vendor').val(vendor_name);
    $('#vendor_hidden').val(vendor_id);
    $('#vendor_searchCodeAjaxDiv').hide();
    fill_table_values();
}
//-----------------------------------------------------------------------------------------------------------------------
$('#sub_category').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var sub_category = $(this).val();
        sub_category = sub_category.trim();
        if (sub_category == "") {
            $("#sub_category_searchCodeAjaxDiv").html("");
            $("#sub_category_hidden").val("");
        } else {
            try {
                var category_hidden = '';
                if($("#category").val() != ''){
                    category_hidden = $('#category_hidden').val();
                }
                var param = {sub_category_search: sub_category, category_hidden:category_hidden};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#sub_category_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                        }
                        $("#sub_category_searchCodeAjaxDiv").html(html).show();
                        $("#sub_category_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('sub_category_searchCodeAjaxDiv', event);
    }
});
$('#sub_category').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('sub_category_searchCodeAjaxDiv');
        return false;
    }
});
function fill_sub_category_details(e, subcategory_name, sub_id) {
    $('#sub_category').val(subcategory_name);
    $('#sub_category_hidden').val(sub_id);
    $('#sub_category_searchCodeAjaxDiv').hide();
    fill_table_values();
}
$('#category').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var category = $(this).val();
        category = category.trim();
        if (category == "") {
            $("#category_searchCodeAjaxDiv").html("");
            $("#category_hidden").val("");
        } else {
            try {
                var param = {category_search: category};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#category_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                        }
                        $("#category_searchCodeAjaxDiv").html(html).show();
                        $("#category_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('category_searchCodeAjaxDiv', event);
    }
});
$('#category').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('category_searchCodeAjaxDiv');
        return false;
    }
});
function fill_category_details(e, cat_name, cat_id) {
    $('#category').val(cat_name);
    $('#category_hidden').val(cat_id);
    $('#category_searchCodeAjaxDiv').hide();
    $('#sub_category').val('');
    $('#sub_category_hidden').val('');
    fill_table_values();
}
//-----------------------------------------------------------------------------------------------------------------------
$('#product').keyup(function (event) {
    var sub_category = '';
    var vendor_search = '';
    var generic_search = '';

    if($("#sub_category").val() != ''){
    sub_category = $('#sub_category_hidden').val();
    }
    if($("#vendor").val() != ''){
    vendor_search = $('#vendor_hidden').val();
    }
    if($("#generic_name").val() != ''){
    generic_search = $("#generic_name").val();
    }
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var product = $(this).val();
        product = product.trim();
        if (product == "") {
            $("#product_searchCodeAjaxDiv").html("");
            $("#product_hidden").val("");
        } else {
            try {
                var param = {product_search: product,sub_category:sub_category,vendor_search:vendor_search,generic_search:generic_search};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#product_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                        }
                        $("#product_searchCodeAjaxDiv").html(html).show();
                        $("#product_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('product_searchCodeAjaxDiv', event);
    }
});
$('#product').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('product_searchCodeAjaxDiv');
        return false;
    }
});
function fill_desc_details(e, item_desc, item_code) {
    $('#product').val(item_desc);
    $('#product_hidden').val(item_code);
    $('#product_searchCodeAjaxDiv').hide();
    $("#tab_1").html('');
    $("#purchaseDetailData").html('');
    $("#tab_3").html('');
    $("#tab_4").html('');
    $("#tab_5").html('');
    $("#tab_5_html").html('');
    $("#stock_ajax").val('0');
    $("#purchase_ajax").val('0');
    $("#summary_ajax").val('0');
    $("#sale_ajax").val('0');
    $("#product_ajax").val('0');
get_product_details(item_code,item_desc)
}
function get_product_details(item_code,item_desc){
        var param = {item_code: item_code, get_material: 1, item_dec: item_desc};
    $.ajax({
        type: "GET",
        url: '',
        data: param,
        beforeSend: function () {
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (html) {
            $.LoadingOverlay("hide");
            $("#product_tab_container").html(html);
        },
        complete: function () {
        }
    });
}

function resetPurchaseDetails(){
     $('#purchaseFromDate').val('');
     $('#purchaseToDate').val('');
     purchaseDetails(2);
}

function purchaseDetails(search_val) {
    var item_desc = $('#product').val();
    var item_code = $('#product_hidden').val();
    var from_date = $('#purchaseFromDate').val();
    var to_date = $('#purchaseToDate').val();
    var param = {item_code: item_code, get_purchase: 1, item_dec: item_desc,from_date:from_date,to_date:to_date};
    var search_flag=false;
    if (parseInt(search_val) == 2) {
        search_flag=true;
    }else if ($("#product_ajax").val() == 0) {
        search_flag=true;
    }
    if (search_flag) {
        $.ajax({
            type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (html) {
                $.LoadingOverlay("hide");
                $("#purchaseDetailData").html(html);
            },
            complete: function () {
                $("#purchase_ajax").val('1');
            }
        });
    }
}
function productDetails() {
    var item_desc = $('#product').val();
    var item_code = $('#product_hidden').val();
    var param = {item_code: item_code, get_product: 1, item_dec: item_desc};
    if ($("#product_ajax").val() == 0) {
        $.ajax({
            type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
                 $.LoadingOverlay("show",{ background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                $.LoadingOverlay("hide");
                $("#tab_3").html(html);
            },
            complete: function () {
                $("#product_ajax").val('1');
            }
        });
    }
}
function summaryDetails() {
    var item_desc = $('#product').val();
    var item_code = $('#product_hidden').val();
    var param = {item_code: item_code, get_summary: 1, item_dec: item_desc};
    if ($("#summary_ajax").val() == 0) {
        $.ajax({
            type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
                 $.LoadingOverlay("show",{ background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                $.LoadingOverlay("hide");
                $("#tab_4").html(html);
            },
            complete: function () {
                $("#summary_ajax").val('1');
            }
        });
    }
}
function pursaleDetails() {
    var item_desc = $('#product').val();
    // var item_code= $('#item_code').val();
    var item_code = $('#product_hidden').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var param = {from_date:from_date,to_date:to_date, get_sale: 1, item_dec: item_desc, item_code: item_code};
    if ($("#sale_ajax").val() == 0) {
        $.ajax({
            type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
                 $.LoadingOverlay("show",{ background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                $.LoadingOverlay("hide");
                $("#tab_5_html").html(html);
            },
            complete: function () {
                // $("#sale_ajax").val('1');
            }
        });
    }
}

function fill_table_values(){
    var sub_category_hidden = '';
    var category_hidden = '';
    var vendor_hidden = '';
    var generic_hidden = '';

    if($("#category").val() != ''){
    category_hidden = $('#category_hidden').val();
    }
    if($("#sub_category").val() != ''){
    sub_category_hidden = $('#sub_category_hidden').val();
    }
    if($("#vendor").val() != ''){
    vendor_hidden = $('#vendor_hidden').val();
    }
    if($("#generic_name").val() != ''){
    generic_hidden = $('#generic_name').val();
    }
    var param = {category_hidden:category_hidden,sub_category_hidden: sub_category_hidden, vendor_hidden: vendor_hidden, generic_hidden: generic_hidden};
        $.ajax({
            type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
                 $.LoadingOverlay("show",{ background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (html) {
                $.LoadingOverlay("hide");
                $("#product_tbody").html(html);
            },
            complete: function () {
            }
        });
    }
@include('Purchase::messagetemplate')
</script>

@endsection
