<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        border: 1px solid #00000054;
        font-size: 10px;

    }
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div" border='2px !important'; >
        <table id="result_data_table" class="" style="font-size:xx-small !important;font-family: 'robotoregular';">

            @if(sizeof($res)>0)
            @php 
            $sort_array = array();
            $i = 1;
            $sort_change   = 0;
            $sub_requested_total = $sub_issued_total = $sub_received_total =  0; 
            $grand_requested_total = $grand_issued_total = $grand_received_total =0;
            $grand_total = 0;
            @endphp
            <p style="font-size: smaller;padding-left: 10px">Report Date: <span>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}</span> </p><br>
            <h2 style="text-align: center;margin-top: -29px;"> Stock Transfer Report  </h2>

            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th  style="padding: 2px;width:15%;text-align: center;border:2px; ">Product</th>
                <th  style="padding: 2px;width:15%;text-align: center;border:2px; ">Issued From</th>
                <th  style="padding: 2px;width:15%;text-align: center;">Issued To</th>
                <th  style="padding: 2px;width:5%;text-align: center;">Batch No</th>
                <th  style="padding: 2px;width:5%;text-align: center;">Requested Qty.</th>
                <th  style="padding: 2px;width:5%;text-align: center;">Issued Qty.</th>
                <th  style="padding: 2px;width:5%;text-align: center;">Received Qty</th>
                <th  style="padding: 2px;width:5%;text-align: center;">Issue Dt.</th>
                <th  style="padding: 2px;width:15%;text-align: center;">Received Dt.</th>
                <th  style="padding: 2px;width:15%;text-align: center;">Status</th>
            </tr>

            @foreach ($res as $data)
            @if($current_sorting == 'item')
            @if(!in_array($data->item_code,$sort_array)) 
            @php 
            $sort_array[] = $data->item_code;
            $sort_order = true;
            $sort_change = 1;
            @endphp
            @endif
            @endif
            @if($current_sorting == 'from_location')
            @if(!in_array($data->from_code,$sort_array)) 
            @php 
            $sort_array[] = $data->from_code;
            $sort_order = true;
            $sort_change = 1;
            @endphp
            @endif
            @endif
            @if($current_sorting == 'to_location')
            @if(!in_array($data->to_code,$sort_array)) 
            @php 
            $sort_array[] = $data->to_code;
            $sort_order = true;
            $sort_change = 1;
            @endphp
            @endif
            @endif
           
            @if($sort_order && $sort_order==1 )
            @if($sort_change == 1)
            @php $sort_change = 0;@endphp
            @if($i != 1)
            <tr style="background-color: #c6d4e5"> 
                <td colspan='4' style='font-size: 14px;'><strong>Sub Total</strong></td> 
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$sub_requested_total}} </td>  
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$sub_issued_total }}  </td>  
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$sub_received_total}} </td> 
                <td colspan='3'></td>
            </tr>
            @endif
            @php $sub_requested_total = $sub_issued_total = $sub_received_total =  0; @endphp
            @endif
            <tr> 
                <td colspan="10" style="font-size: 12px;background-color: #afd6be">
                    <strong>
                        @if($current_sorting == 'to_location')
                        {{$data->issued_to}}
                        @endif
                        @if($current_sorting == 'from_location')
                        {{$data->issued_from}}
                        @endif
                        @if($current_sorting == 'item')
                        {{$data->item_desc}}
                        @endif
                    </strong>
                </td>    
            </tr>
            @php 
            $sort_order = false; 
            @endphp
            @endif
            <tr>
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules" title="{{$data->item_desc}}">{{$data->item_desc}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules" >{{$data->issued_from}}</td>  
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules" >{{($data->issued_to)}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules" >{{($data->batch_no)}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="td_common_numeric_rules">{{($data->requested_qty)}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="td_common_numeric_rules">{{($data->issued_qty)}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="td_common_numeric_rules" >{{($data->received_qty)}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">{{($data->issue_dt)}}</td>
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules"><?php
                    if ($data->received_qty > 0) {
                        echo $data->rec_dt;
                    }
                    ?></td>
                <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">{{($data->status)}}</td>
            </tr>
            <?php
            $sub_requested_total += $data->requested_qty;
            $sub_issued_total += $data->issued_qty;
            $sub_received_total += $data->received_qty;
            
            $grand_requested_total += $data->requested_qty;
            $grand_issued_total += $data->issued_qty;
            $grand_received_total += $data->received_qty;   
            $i++;
            ?>
            @endforeach
            <tr style="background-color: #c6d4e5"> 
                <td colspan='4' style='font-size: 14px;'><strong>Sub Total</strong></td> 
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$sub_requested_total}} </td>  
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$sub_issued_total }}  </td>  
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$sub_received_total}} </td> 
                <td colspan='3'></td>
            </tr>
            <tr style="background-color: #dedc91"> 
                <td colspan='4' style='font-size: 14px;'><strong>Grand Total</strong></td> 
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$grand_requested_total}} </td>  
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$grand_issued_total }}  </td>  
                <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                    {{$grand_received_total}} </td> 
                <td colspan='3'></td>
            </tr>
            @else 
            <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
            @endif   
        </table>
    </div>
</div>
