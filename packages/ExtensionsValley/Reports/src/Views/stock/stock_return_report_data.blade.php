<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div id="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Stock Return Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">

                @if (sizeof($res) > 0)
                    @php
                        $sort_array = [];
                        $i = 1;
                        $sort_change = 0;
                        $sub_return_total = $sub_received_total = 0;
                        $grand_return_total = $grand_return_received_total = 0;
                    @endphp
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th  style="padding: 2px;width:15%;text-align: center;border:2px; ">Product</th>
                        <th  style="padding: 2px;width:15%;text-align: center;border:2px; ">Return From</th>
                        <th  style="padding: 2px;width:15%;text-align: center;">Return To</th>
                        <th  style="padding: 2px;width:5%;text-align: center;">Batch No</th>
                        <th  style="padding: 2px;width:5%;text-align: center;">Return Qty.</th>
                        <th  style="padding: 2px;width:5%;text-align: center;">Ret. rec. Qty.</th>
                        <th  style="padding: 2px;width:5%;text-align: center;">Return Dt.</th>
                        <th  style="padding: 2px;width:5%;text-align: center;">Reason</th>
                    </tr>

                    @foreach ($res as $data)
                        @if ($current_sorting == 'item')
                            @if (!in_array($data->item_code, $sort_array))
                                @php
                                    $sort_array[] = $data->item_code;
                                    $sort_order = true;
                                    $sort_change = 1;
                                @endphp
                            @endif
                        @endif
                        @if ($current_sorting == 'from_location')
                            @if (!in_array($data->from_code, $sort_array))
                                @php
                                    $sort_array[] = $data->from_code;
                                    $sort_order = true;
                                    $sort_change = 1;
                                @endphp
                            @endif
                        @endif
                        @if ($current_sorting == 'to_location')
                            @if (!in_array($data->to_code, $sort_array))
                                @php
                                    $sort_array[] = $data->to_code;
                                    $sort_order = true;
                                    $sort_change = 1;
                                @endphp
                            @endif
                        @endif
                        @if ($sort_order && $sort_order == 1)
                            @if ($sort_change == 1)
                                @php $sort_change = 0;@endphp
                                @if ($i != 1)
                                    <tr style="background-color: #c6d4e5">
                                        <td colspan='4' style='font-size: 14px;'><strong>Sub Total</strong></td>
                                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                                            {{ $sub_return_total }} </td>
                                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                                            {{ $sub_received_total }} </td>
                                        <td colspan='2'></td>
                                    </tr>
                                @endif
                                @php $sub_received_total = $sub_return_total =  0; @endphp
                            @endif
                            <tr>
                                <td colspan="8" style="font-size: 12px;background-color: #afd6be">
                                    <strong>
                                        @if ($current_sorting == 'to_location')
                                            {{ $data->return_to }}
                                        @endif
                                        @if ($current_sorting == 'from_location')
                                            {{ $data->return_from }}
                                        @endif
                                        @if ($current_sorting == 'item')
                                            {{ $data->item_desc }}
                                        @endif
                                    </strong>
                                </td>
                            </tr>
                            @php
                                $sort_order = false;
                            @endphp
                        @endif
                        <tr>
                            <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">
                                {{ $data->item_desc }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">
                                {{ $data->return_from }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">
                                {{ $data->return_to }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">
                                {{ $data->batch_no }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="td_common_numeric_rules">
                                {{ $data->return_quantity }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="td_common_numeric_rules">
                                {{ $data->return_received_qty }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="common_td_rules">
                                {{ $data->ret_dt }}</td>
                            <td rowspan="" style="padding:5px;width:10%;" class="td_common_numeric_rules">
                                {{ $data->reason }}</td>
                        </tr>
                        <?php
                        $sub_return_total += $data->return_quantity;
                        $sub_received_total += $data->return_received_qty;

                        $grand_return_total += $data->return_quantity;
                        $grand_return_received_total += $data->return_received_qty;
                        $i++;
                        ?>
                    @endforeach
                    <tr style="background-color: #c6d4e5">
                        <td colspan='4' style='font-size: 14px;'><strong>Sub Total</strong></td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{ $sub_return_total }} </td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{ $sub_received_total }} </td>
                        <td colspan='2'></td>
                    </tr>
                    <tr style="background-color: #dedc91">
                        <td colspan='4' style='font-size: 14px;'><strong>Grand Total</strong></td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{ $grand_return_total }} </td>
                        <td style='font-size: 14px;font-weight:bold' class="td_common_numeric_rules">
                            {{ $grand_return_received_total }} </td>
                        <td colspan='2'></td>
                    </tr>
                @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                @endif
            </table>
        </div>
    </div>
