<div class="row">
        <div class="col-md-12" style="padding-top: 10px;">
            <div class="col-md-12 bg-primary sm-padding">
                <b>Purchase/Sale : {{ $item_desc }}</b>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="h10"></div>
        

        <div class="col-md-12">            
            <div class="theadscroll" style="height:400px;">
                <div style="">
                    <table class="table table_round_border styled-table">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="text-align:center">Total Purchase Cost</th>
                            <th style="text-align:center">Total Purchase Qty</th>
                            <th style="text-align:center">Total Selling Price</th>
                            <th style="text-align:center">Total Selling Qty</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        @if((isset($purchase_summary_details)) && (isset($sales_summary_details)))
                            <tr>
                                <td class="td_common_numeric_rules">
                                    <?php if(isset($purchase_summary_details[0]->purchase_cost) && !empty($purchase_summary_details[0]->purchase_cost)) { 
                                        echo round($purchase_summary_details[0]->purchase_cost,2); 
                                    } else { 
                                        echo '-'; } 
                                        ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?php if(isset($purchase_summary_details[0]->purchase_qty)) { 
                                        echo round($purchase_summary_details[0]->purchase_qty,2); 
                                    } else {  
                                        echo '-'; } ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?php if(isset($sales_summary_details[0]->sale_price)) { 
                                        echo round($sales_summary_details[0]->sale_price,2); 
                                    } else {  echo '-'; } ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?php if(isset($sales_summary_details[0]->sale_qty)) { 
                                        echo round($sales_summary_details[0]->sale_qty,2);  
                                        
                                    } else {  echo '-'; }?>
                                </td>                            
                            </tr>
                        @else
                        <tr>
                            <td colspan="15">No Data Found</td>
                        </tr>
                        @endif

                    </tbody>
                </table>
                </div>
            </div>
        </div>
</div>  