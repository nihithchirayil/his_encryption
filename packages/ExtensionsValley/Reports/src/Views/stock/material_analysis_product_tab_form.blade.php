<div class="row">
    <div class="col-md-12" style="padding-top: 10px;">
        <div class="col-md-12 bg-primary sm-padding">
            <b>{{ $item_desc }}</b>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="h10"></div>


    <div class="col-md-6">            
        <div class="theadscroll" style="height:400px;">
            <div style="">
                <table class="table table_round_border styled-table">
                    <thead>
                        <tr >
                            <th style="text-align:left" class="table_header_bg">Category</th>
                            <td class=""> <b>{{ $productData[0]->category_name??'' }}</b></td>
                        </tr>
                        <tr >
                            <th style="text-align:left" class="table_header_bg">Sub Category</th>
                            <td class=""><b>{{ $productData[0]->sub_category??'' }}</b></td>
                        </tr>
                        <tr >
                            <th style="text-align:left" class="table_header_bg">Chemical Name</th>
                            <td class=""><b>{{ $productData[0]->generic_name??'' }}</b></td>
                        </tr>
                        <tr>
                            <th style="text-align:left" class="table_header_bg">Manufacturer</th> 
                            <td class=""><b>{{ $productData[0]->manufacturer??'' }}</b></td>
                        </tr>
                        <tr>
                            <th style="text-align:left" class="table_header_bg">Doctor</th>
                            <td class=""><b>{{ $productData[0]->doctor??'' }}</b></td>
                        </tr>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>