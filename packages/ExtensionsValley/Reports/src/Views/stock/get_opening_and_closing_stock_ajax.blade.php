<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>  {{date('d-M-Y h:i A')}}  </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date:  {{ date('d-M-Y', strtotime($from_date)) }}  To  {{ date('d-M-Y', strtotime($to_date)) }}</p>
            @php
                $collect = collect($res);
                $total_records = count($collect);
            @endphp
                    <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
                    <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Opening and Closing Stock Report</b></h4>
                    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                        style="font-size: 12px;">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="2%">SI</th>
                                {{-- <th width="10%">Date</th> --}}
                                <th width="68%">Item Name</th>
                                <th width="10%">Opening Stock	</th>
                                <th width="10%">Closing Stock </th>
                            </tr>
                        </thead>
                        @if (sizeof($res) > 0)
                            <tbody>
                        @foreach ($res as $data)
                            <tr>
                                <td class="common_td_rules">{{ $loop->iteration }}</td>
                                {{-- <td class="common_td_rules">{{ date('d-M-Y', strtotime($data->stock_date)) }}</td>  --}}
                                <td class="common_td_rules">{{ $data->item_desc }}</td>
                                <td class="common_td_rules">{{ $data->opening_stock }}</td>
                                <td class="td_common_numeric_rules">{{ $data->closing_stock }}</td>
                            </tr>
                        @endforeach

            @else
                <tr>
                    <td colspan="10" style="text-align: center;">No Results Found!</td>
                </tr>
            @endif

                </tbody>
            </table>
            </font>
        </div>
    </div>
</div>
