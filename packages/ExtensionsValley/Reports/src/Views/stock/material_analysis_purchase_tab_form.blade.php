
<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="row">
    <div class="col-md-12" style="padding-top: 10px;">
        <div class="col-md-12 bg-primary sm-padding">
            <b>Purchase History Product : {{ $item_desc }}</b>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="h10"></div>
    <div class="col-md-12">            
        <div class="theadscroll" style="height:400px;position: relative; max-height: 450px;cursor:pointer;">
            <div style="">
                <table class="table table_round_border styled-table">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="text-align:center">Voucher Date</th>
                            <th style="text-align:center">Account Date</th>
                            <th style="text-align:center">Supplier</th>
                            <th style="text-align:center">Voucher</th>                          
                            <th style="text-align:center" width="5%";>App.Status</th>                          
                            <th style="text-align:center" width="12%";>Reference</th>
                            <th style="text-align:center">Batch</th>
                            <th style="text-align:center">Location</th>
                            <th style="text-align:center">GRN Qty</th>
                            <th style="text-align:center">Total Qty</th>
                            <th style="text-align:center">Free</th>
                            <th style="text-align:center">Tax</th>
                            <th style="text-align:center">Unit Cost</th>
                            <th style="text-align:center">Unit Rate</th>
                            <th style="text-align:center">Sales Rate</th>
                            <th style="text-align:center">MRP</th>
                            <th style="text-align:center">Disc(%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($purchaseData))
                        @foreach($purchaseData as $pur)

                        <tr>
                            <td class="common_td_rules">{{ isset($pur->voucher_date)? $pur->voucher_date:''}}</td>
                            <td class="common_td_rules">{{ isset($pur->account_date)? $pur->account_date:''}}</td>
                            <td class="common_td_rules">{{ isset($pur->vendor)? $pur->vendor:'' }}</td>
                            <td class="common_td_rules">{{ isset($pur->voucher)? $pur->voucher:'' }}</td>
                            @if($pur->status=='Not Approved')
                            <td class="common_td_rules" style="background-color:yellow">{{ isset($pur->status)? $pur->status:'' }}</td>
                            @else
                            <td class="common_td_rules">{{ isset($pur->status)? $pur->status:'' }}</td>
                            @endif
                            <td class="common_td_rules">{{ isset($pur->ref_no)? $pur->ref_no:'' }}</td>
                            <td class="common_td_rules">{{ isset($pur->batch)? $pur->batch:'' }}</td>
                            <td class="common_td_rules">{{ isset($pur->location_name)? $pur->location_name:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->quantity)? $pur->quantity:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->total_quantity)? $pur->total_quantity:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->free)? $pur->free:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->tax)? $pur->tax:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->unit_cost)? $pur->unit_cost:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->unit_rate)? $pur->unit_rate:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->sales_rate)? $pur->sales_rate:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->unit_mrp)? $pur->unit_mrp:'' }}</td>
                            <td class="td_common_numeric_rules">{{ isset($pur->total_discount_perc)? $pur->total_discount_perc:'' }}</td>
                        </tr>
                        @endforeach    
                        @else
                        <tr>
                            <td colspan="15">No Purchase History Found</td>
                        </tr>
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <div class="h10"></div>

    <div class="row" style="">
        <div style="padding:10px 30px;">
            <div class="col-md-12" style="text-align:right;">
                <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
                </nav>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">  
    $("#tab_2 #purchase_paging .pagination a").click(function(e){
        e.preventDefault();
        var item_code = $("#product_hidden").val();
        var item_desc = $("#product").val();
        var param = {item_code: item_code,get_purchase:1,item_dec:item_desc,page:$(this).attr('href').split('page=')[1]};
        $.ajax({
            url: "",
            data: param,
            dataType: 'html',
        }).done(function (data) {
            $('#tab_2').html(data);
        }).fail(function () {
            alert('Posts could not be loaded.');
        });
    }); 
</script>