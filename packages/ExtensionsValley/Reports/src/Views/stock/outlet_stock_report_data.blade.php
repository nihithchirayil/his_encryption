@if(isset($batch_wise) && ($batch_wise == 0))
<div class="row">
   <div class="col-md-12" id="result_container_div">
    <table id="result_data_table" class="" style="font-size: 12px;">
    @if(sizeof($res)>0)
    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;width: 180px;">Location</th>
        <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;width: 180px;">Category</th>
        <th onclick="sortTable('result_data_table',2,1);" style="padding: 2px;width: 75px;">Subcategory</th>
        <th onclick="sortTable('result_data_table',3,1);" style="padding: 2px;width: 180px;">Type</th>
        <th onclick="sortTable('result_data_table',3,1);" style="padding: 2px;width: 180px;">Item</th>
        <th onclick="sortTable('result_data_table',4,1);" style="padding: 2px;width: 75px;">HSN Code</th>
        <th onclick="sortTable('result_data_table',5,1);" style="padding: 2px;width: 75px;">Generic Name</th>
        <th onclick="sortTable('result_data_table',6,1);" style="padding: 2px;width: 75px;">Manufacturer</th>
        <th onclick="sortTable('result_data_table',7,1);" style="padding: 2px;width: 75px;">MRP</th>
        <th onclick="sortTable('result_data_table',8,1);" style="padding: 2px;width: 75px;">Stock</th>
    </tr>
    @foreach ($res as $list)
        <tr>
                    <td class="common_td_rules">{{ $list->location }}</td>
                    <td class="common_td_rules">{{ $list->category_name }}</td>
                    <td class="common_td_rules">{{ $list->group }}</td>
                    <td class="common_td_rules">{{ $list->item_type_name }}</td>
                    <td class="common_td_rules">{{ $list->item_description }}</td>
                    <td class="common_td_rules">{{ $list->hsn_code }}</td>
                    <td class="common_td_rules">{{ $list->generic_name }}</td>
                    <td class="common_td_rules">{{ $list->manufacturer_name }}</td>
                    <td class="common_td_rules">{{ $list->mrp }}</td>
                    <td class="common_td_rules">{{ $list->stock }}</td>

        </tr>
        
    @endforeach
        <!-- <tr>
            <td colspan="9">
                <hr style="height:1px;border-width:0;color:gray;background-color:gray">
            </td>
        </tr>
        <tr style="font-family:robotoregular">
            <td colspan="5"><b></b></td>
            <td><b></b></td>
            <td><b></b></td>
            <td><b></b></td>
        </tr> -->
    @else
    <tr>
        <td>No Results Found!</td>
    </tr>
    @endif

</table>
   </div>
</div>
@endif

@if(isset($batch_wise) && ($batch_wise == 1))
<div class="row">
   <div class="col-md-12" id="result_container_div">
    <table id="result_data_table" class="" style="font-size: 12px;">
    @if(sizeof($res)>0)
    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;width: 180px;">Location</th>
        <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;width: 180px;">Category</th>
        <th onclick="sortTable('result_data_table',2,1);" style="padding: 2px;width: 75px;">Subcategory</th>
        <th onclick="sortTable('result_data_table',3,1);" style="padding: 2px;width: 180px;">Item</th>
        <th onclick="sortTable('result_data_table',4,1);" style="padding: 2px;width: 75px;">HSN Code</th>
        <th onclick="sortTable('result_data_table',5,1);" style="padding: 2px;width: 75px;">Batch</th>
        <th onclick="sortTable('result_data_table',6,1);" style="padding: 2px;width: 75px;">Expiry Date</th>
        <th onclick="sortTable('result_data_table',6,1);" style="padding: 2px;width: 75px;">Selling Price</th>
        <th onclick="sortTable('result_data_table',7,1);" style="padding: 2px;width: 75px;">MRP</th>
        <th onclick="sortTable('result_data_table',8,1);" style="padding: 2px;width: 75px;">Stock</th>
    </tr>
    @foreach ($res as $list)
        <tr>
            <td class="common_td_rules">{{ $list->location }}</td>
            <td class="common_td_rules">{{ $list->category_name }}</td>
            <td class="common_td_rules">{{ $list->group }}</td>
            <td class="common_td_rules">{{ $list->item_description }}</td>
            <td class="common_td_rules">{{ $list->hsn_code }}</td>
            <td class="common_td_rules">{{ $list->batch }}</td>
            <td class="common_td_rules">{{ $list->expiry_date }}</td>
            <td class="common_td_rules">{{ $list->selling_price }}</td>
            <td class="common_td_rules">{{ $list->mrp }}</td>
            <td class="common_td_rules">{{ $list->stock }}</td>

        </tr>
        
    @endforeach
    @else
    <tr>
        <td>No Results Found!</td>
    </tr>
    @endif

</table>
   </div>
</div>
@endif