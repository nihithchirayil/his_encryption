<div class="panel panel-info">
    <div class="panel-heading top_panel_row">
        <div class="row">
            <div class="col-md-12">
            </div>
            <div class="clearfix"></div>
            <div class="h10"></div>
            <div class="col-md-12">
                <div class="theadscroll" style="min-height: 470px; margin-bottom: 25px;">
                    <table class="table table_round_border styled-table" style="width: 100%;min-width: 160px !important;margin-left: 0px;">
                        <thead>
                            <tr class="table_header_bg">
                                <th>Product</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(isset($productData) && count($productData) > 0)

                            @foreach($productData as $data)

                            <tr onclick='get_product_details("{{ $data->item_code }}","{{ $data->item_desc }}")' style="cursor:pointer">
                                <td>{{ $data->item_desc }} </td> 
                            </tr>
                            @endforeach
                            @else
                            <tr >
                                <td>No Items Found</td> 
                            </tr>
                            @endif   
                        </tbody>
                    </table>
                </div>
            </div>
                <div>
                    <div class="col-md-12" style="text-align:right;">
                        <nav id="product_paging"> <?php echo $paginator->render(); ?>
                        </nav>
                    </div>
                </div>
        </div>
    </div>
</div>
<script type="text/javascript">
     $("#product_paging .pagination a").click(function(e){
        e.preventDefault();
        var sub_category_hidden = $('#sub_category_hidden').val();
        var vendor_hidden = $('#vendor_hidden').val();
        var generic_hidden = $('#generic_name').val();
        var category_hidden = $('#category_hidden').val();
        $.LoadingOverlay("show",{ background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});   
        var param = {category_hidden:category_hidden, sub_category_hidden: sub_category_hidden, vendor_hidden: vendor_hidden, generic_hidden: generic_hidden,page:$(this).attr('href').split('page=')[1]};
        $.ajax({
            url: "",
            data: param,
            dataType: 'html',
            }).done(function (data) {
            $('#product_tbody').html(data);
            $.LoadingOverlay("hide");
            }).fail(function () {
            $.LoadingOverlay("hide");
            alert('Posts could not be loaded.');
        });
    });
   
</script>