 


    <div class="clearfix"></div>
    <div class="h10"></div>
    <div id="tab_5_html">
    </div>

    <div class="col-md-6">            
        <div class="theadscroll" style="height:400px;">
            <div style="">
                <table class="table table_round_border styled-table">
                    <thead>
                        <tr >
                            <th style="text-align:left" class="table_header_bg">Total Purchase Cost</th>
                            <td class=""> <b>{{$purchase_sale_details[0]->purchase_cost??'' }}</b></td>
                        </tr>
                        <tr >
                            <th style="text-align:left" class="table_header_bg">Total Sales</th>
                            <td class=""><b>{{ $sale_details[0]->amount??'' }}</b></td>
                        </tr>
                        <tr >
                            <th style="text-align:left" class="table_header_bg">Quantity</th>
                            <td class=""><b>{{$sale_details[0]->quantity??'' }}</b></td>
                        </tr>
                      
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>

