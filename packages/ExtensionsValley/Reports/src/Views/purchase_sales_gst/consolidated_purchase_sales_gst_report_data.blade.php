<style>
    .td_class {
        border: 1px solid !important;
    }
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('Y-m-d h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to_date ?></p>

        <!-- <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Patientwise GST report</b></h4> -->
        <div class="col-md-12" id="print_data">

        <div class="col-md-6" style="margin-left: 10%">
            <table id="result_data_table" class='table-condensed table_sm'
                style="font-size: 12px;">

                <thead class="headerclass"
                    style="font-family:robotoregular">
                    <tr>
                        <td colspan="4" style="background-color: rgb(54 68 157);color:#ffff" class=" td_class">
                            <b>Sales</b>
                        </td>
                    </tr>
                    <tr>
                        <th width='12%' class="td_common_numeric_rules td_class">Total Sales Taxable</th>
                        <th width='8%' class="td_class">CGST</th>
                        <th width='8%' class="td_class">SGST</th>
                        <th width='10%' class="td_class">Total Output Tax Liability</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
            $ret = $taxable_return =0;
            if(isset($sales_ret_res[0]->tax_ret) && $sales_ret_res[0]->tax_ret !=''){
                $ret = ($sales_ret_res[0]->tax_ret)/2;
                $taxable_return = $sales_ret_res[0]->return_taxable;
            }
            if($sales_res[0]->taxable_value !=''){
              foreach($sales_res as $each){ ?>
                    <tr>
                        <td class="td_common_numeric_rules td_class"><?= $each->taxable_value-$taxable_return ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->cgst-$ret ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->sgst-$ret ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->total-$ret ?></td>
                    </tr>

                    <?php
            }
        }else{
              ?>
                    <tr>
                        <td colspan='4'  class="td_class" style='text-align:center'>No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

            </table>
        </div>
        <div class="col-md-6" style="margin-left: 10%;margin-top:3%">
            <table id="result_data_table1" class=' table-condensed table_sm ' style="font-size: 12px;">
                <thead class="headerclass"
                    style="font-family:robotoregular">
                    <tr>
                        <td colspan="4" style="background-color: rgb(54 68 157);color:#ffff" class=" td_class">
                            <b>Purchase</b>
                        </td>
                    </tr>
                    <tr>
                        <th width='12%' class="td_class">Total Purchase Taxable</th>
                        <th width='8%' class="td_class">CGST</th>
                        <th width='8%' class="td_class">SGST</th>
                        <th width='8%' class="td_class">Total Input Tax</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $ret_tax = 0;
                    if(isset($purchase_ret_res[0]->tax) && $purchase_ret_res[0]->tax !=''){
                        $ret_tax = $purchase_ret_res[0]->tax;
                    }
            if($purchase_res[0]->taxable_sale !=''){
              foreach($purchase_res as $each){ ?>
                    <tr>
                        <td class="td_common_numeric_rules td_class"><?= $each->taxable_sale ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->tax-$purchase_ret_cgst ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->tax-$purchase_ret_sgst ?></td>
                        <td class="td_common_numeric_rules td_class"><?= ($each->tax-($purchase_ret_cgst+$purchase_ret_sgst))*2 ?></td>
                    </tr>

                    <?php
            }
        }else{
              ?>
                    <tr>
                        <td colspan='4' class="td_class" style='text-align:center'>No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

            </table>
        </div>
        <div class="col-md-6" style="margin-left: 10%;margin-top:3%">
            <table id="result_data_table" class='table-condensed table_sm'
                style="font-size: 12px;">

                <thead class="headerclass"
                    style="font-family:robotoregular">
                    <tr>
                        <td colspan="4" style="background-color: rgb(54 68 157);color:#ffff" class=" td_class">
                            <b>Exempt, nil and non GST inward supplies</b>
                        </td>
                    </tr>
                    <tr>
                        <th width='12%' class="td_common_numeric_rules td_class">IGST</th>
                        <th width='8%' class="td_class">CGST</th>
                        <th width='8%' class="td_class">SGST</th>
                        <th width='10%' class="td_class">Total</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
            if($purchase_exempted_res[0]->taxable_value !=''){
              foreach($purchase_exempted_res as $each){ ?>
                    <tr>
                        <td class="td_common_numeric_rules td_class"><?= $each->igst_tax-$purchase_return_IGST_excepmted ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->tax-$purchase_return_CGST_excepmted ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->tax-$purchase_return_SGST_excepmted ?></td>
                        <td class="td_common_numeric_rules td_class"><?= $each->igst_tax+$each->tax+$each->tax ?></td>
                    </tr>

                    <?php
            }
        }else{
              ?>
                    <tr>
                        <td colspan='4'  class="td_class" style='text-align:center'>No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

            </table>
        </div>
        </div>

    </div>
