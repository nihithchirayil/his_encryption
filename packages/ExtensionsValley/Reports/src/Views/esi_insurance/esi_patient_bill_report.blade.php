@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->

    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right_col">
        <input type="hidden" value='<?= $route_data ?>' id="route_value">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <input type="hidden" id="current_date" value="{{ $current_date }}">
        <div class="row pull-right" style="font-size: 12px; font-weight: bold;">
            {{ $title }}
            <div class="clearfix"></div>
            <div class="h10"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Form Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="bill_fromdate" name=""
                                        value="<?= $current_date ?>">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="bill_todate" name=""
                                        value="<?= $current_date ?>">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="bill_no" name="bill_no"
                                        value="">
                                    <div id="bill_no_AjaxDiv" class="ajaxSearchBox"
                                    style="width: 200px !important;margin-top: -15px;margin-left: -5px;"></div>
                                    <input type="hidden" name="bill_no_hidden" value="" id="bill_no_hidden">
                                    <input type="hidden" name="bill_id_hidden" value="" id="bill_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">UHID</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                        name="patient_name" value="">
                                    <div id="patient_idAjaxDiv" class="ajaxSearchBox"
                                    style="width: 200px !important;margin-top: -15px;margin-left: -5px;"></div>
                                    <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                    <input type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">IP Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="ip_number"
                                        name="ip_number" value="">
                                    <div id="ip_numberAjaxDiv" class="ajaxSearchBox"
                                    style="width: 200px !important;margin-top: -15px;margin-left: -5px;"></div>
                                    <input type="hidden" name="ip_number_hidden" value="" id="ip_number_hidden">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Visit Status</label>
                                    <div class="clearfix"></div>
                                    <select id="visit_status" class="form-control select2">
                                        <option value="">All</option>
                                        <option value="IP">IP</option>
                                        <option value="OP">OP</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill Tag</label>
                                    <div class="clearfix"></div>
                                    <select id="bill_tag" class="form-control select2">
                                        <option value="">All</option>
                                        <?php
                                            foreach ($bill_tag as $key=>$val) {
                                                ?>
                                        <option value="<?= $key ?>"><?= $val ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Payment Type</label>
                                    <div class="clearfix"></div>
                                    <select id="payment_type" class="form-control select2">
                                        <option value="">All</option>
                                        <?php
                                            foreach ($payment_type as $key=>$val) {
                                                ?>
                                        <option value="<?= $key ?>"><?= $val ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: 20px;">
                                <div class="checkbox checkbox-success inline no-margin">
                                    <input id="cash_bills" type="checkbox" name="checkbox">
                                    <label class="text-blue" for="cash_bills">
                                        Cash Bills
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <button class="btn light_purple_bg btn-block" onclick="getReportData();"
                                    name="search_results" id="search_results">
                                    <i id="serachResultSpin" class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <a class="btn light_purple_bg disabled btn-block" onclick="exceller();"
                                    name="csv_results" id="csv_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Excel
                                </a>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <a class="btn light_purple_bg disabled btn-block" onclick="printReportData();"
                                    name="print_results" id="print_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <a class="btn light_purple_bg btn-block" onclick="search_clear();" name="clear_results"
                                    id="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </a>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm">
                    <div id="ResultDataContainer"
                        style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                        <div style="background:#686666;">
                            <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                    width: 100%;
                    padding: 30px;" id="ResultsViewArea">

                            </page>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/esiPatientBillReport.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection
