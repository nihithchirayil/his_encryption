<div class="row">

    <div class="col-md-12" id="result_container_div">
        <input type="hidden" id="base_url" value="{{URL::to('/')}}">
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">

            <thead>
                  <tr>
                    <th colspan="4">
                        <?= base64_decode($hospital_headder) ?>
                    </th>
                </tr>
                <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b>Lab Report-ESI</b></h4>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th  class="common_td_rules">Name</th>
                    <th class="common_td_rules">
                        <?php $name = isset($res[0]->patient_name) ? $res[0]->patient_name : '';
                        echo $name; ?></th>
                    <th  class="common_td_rules">OP Number</th>
                    <th class="common_td_rules">
                        <?php $uhid = isset($res[0]->uhid) ? $res[0]->uhid : '';
                        echo $uhid; ?>
                    </th>
                </tr>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th  class="common_td_rules">Address</th>
                    <th  class="common_td_rules">
                        <?php $address = isset($res[0]->address) ? $res[0]->address : '';
                        echo $address; ?></th>
                    <th  class="common_td_rules">IP Number</th>
                    <th  class="common_td_rules"><?php $ip_no = isset($res[0]->fin) ? $res[0]->fin : '';
                    echo $ip_no; ?>
                    </th>
                </tr>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th  class="common_td_rules">Sex</th>
                    <th  class="common_td_rules"><?php if (@$res[0]->gender == '1') {
                        echo 'Male';
                    } elseif (@$res[0]->gender == '2') {
                        echo 'Female';
                    } ?></th>
                    <th  class="common_td_rules">Age</th>
                    <th class="common_td_rules""><?php $dob = isset($res[0]->dob) ? $res[0]->dob : '';
                    echo $dob; ?>
                  </th>
            </tr>
    </thead>
    <thbody>
            <?php
            $bill_no='';
            $bill_no = '';
            $service_desc = '';
            ?>

            @if(count($res)!=0)
                @foreach ($res as $each)
                    @if($bill_no != $each->bill_no)
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th class="common_td_rules" width='35%'>Test</th>
                        <th class="common_td_rules" width='15%'>Result Value</th>
                        <th class="common_td_rules" width='15%'>Normal Value</th>
                        <th class="common_td_rules" width='15%'>UOM</th>
                    </tr>
                    <tr>
                        <td colspan="4" class="common_td_rules"><b>Bill No : <?=$each->bill_no?>, Bill Date :<?=date('M-d-Y',strtotime($each->bill_date))?> </b></td>
                    </tr>
                    @endif
                    @if($each->sub_test_id=='0')
                        @if($service_desc != $each->service_desc)
                            <tr>
                                <td  class="common_td_rules" colspan="4"><b>{{ $each->service_desc }}</b></td>
                            </tr>
                        @endif
                    @endif

                    <tr>
                        <td  class="common_td_rules">{{ $each->detail_description }}</td>
                        @if($each->report_type=='T')
                                <td class="common_td_rules"><button id="servicedatatestbtn<?=$each->head_id?>" onclick="getResultDetails(<?=$each->head_id?>,'<?=$each->service_desc?>')" class="btn btn-primary"><i id="servicedatatestspin<?=$each->head_id?>" class="fa fa-list"></i></button></td>
                        @endif
                        @if($each->report_type=='N')
                               <td  class="common_td_rules">{{ $each->actual_result }}</td>
                        @endif
                        <td  class="common_td_rules">{{ $each->refrange }}</td>
                        <td  class="common_td_rules">{{ $each->name }}</td>
                    </tr>
                    @php
                        $bill_no = $each->bill_no;
                        $service_desc = $each->service_desc;
                    @endphp
                @endforeach
            @endif
    </thbody>
</table>
<table class="table"width="100%">
    <tr> <td style="text-align: left; ">Software generated system signature not required.</td>
     <td style="text-align: right; "><b>Authorized Signatory</b></td></tr>
</table>
