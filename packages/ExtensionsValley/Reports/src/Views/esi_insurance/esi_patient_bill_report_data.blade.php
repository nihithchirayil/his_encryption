<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $bill_from ?> To <?= $bill_to ?></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Patient Bill Details
                Report(Credit)</b></h4>

        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;width: 100%">

            <thead>
                <tr class='table_header'>
                    <td class="common_td_rules"><b>Name</b></td>
                    <td colspan="3" class="common_td_rules">
                        <b><?= @$res[0]->patient_name ? $res[0]->patient_name : '-' ?></b>
                    </td>
                    <td class="common_td_rules"><b>UHID</b>
                    </td>
                    <td colspan="5" class="common_td_rules"><b><?= @$res[0]->uhid ? $res[0]->uhid : '' ?></b></td>
                </tr>
                <tr class='table_header'>
                    <td class="common_td_rules"><b>Address</b></td>
                    <td colspan="3" class="common_td_rules">
                        <b><?= @$res[0]->address ? $res[0]->address : '-' ?></b>
                    </td>
                    <td class="common_td_rules"><b>IP Number</b>
                    </td>
                    <td colspan="5" class="common_td_rules">
                        <b><?= @$res[0]->admission_no ? $res[0]->admission_no : '' ?></b>
                    </td>
                </tr>
                <tr class='table_header'>
                    <td class="common_td_rules"><b>Gender</b></td>
                    <th colspan="3" class="common_td_rules">
                        <b><?= @$res[0]->gender ? @$res[0]->gender  : '-' ?></b>
                    </th>
                    <td class="common_td_rules"><b>Age</b>
                    </td>
                    <td colspan="5" class="common_td_rules"><b><?= @$res[0]->dob ? $res[0]->dob : '' ?></b></td>
                </tr>
                <?php $row_cnt = 0;
                $bill_no = '';
                $pharmacy_total = 0;
                $lab_bill_total = 0;
                $xray_bill_total = 0;
                $ultra_sound_total = 0;
                $cevees_total = 0;
                $eachUniqueTotal = 0;
                $previousBillType = '';
                $i = 0;
                $result2 = array_count_values(
                    array_map(function ($d) {
                        return $d->bill_no;
                    }, $res),
                );
                ?>
            </thead>
            </tbody>
            @if (isset($res) && !empty($res))
            @foreach ($res as $data)
            <?php

            if ($data->ord == '1' || $data->ord == '2' || $data->ord == '3') {
                if ($bill_no != $data->bill_no) {
                    $billTypeCount = 0;
                    $bill_no_total = $data->grand_total;
                    $bill_total = '';
                    $each_bill_type_total = 0;
                    $gst12 = 0;
                    $gst18 = 0;
                    $gst28 = 0;

                    if ($data->bill_type != $previousBillType) {
                        $i = 0;
                        if ($row_cnt != 0) {
            ?>

                            <tr class='table_header'>
                                <td class="common_td_rules" colspan="9" style="text-align: left;"><b><?= $previousBillType ?>
                                        Sub Total:</b></td>
                                <td class="td_common_numeric_rules" style="text-align: left;">
                                    <b><?= number_format($eachUniqueTotal, 2, '.', '') ?></b>
                                </td>
                            </tr>

                        <?php  }
                        $eachUniqueTotal = 0; ?>
                        <tr class='table_header'>
                            <td colspan="10" style="text-align: center;"><b><?= $data->bill_type ?></b></td>
                        </tr>
                    <?php $previousBillType = $data->bill_type;
                    } ?>
                    <tr class='table_header'>
                        <td colspan="5" class="common_td_rules"><b>Bill No : <?= $data->bill_no ?></b></td>
                        <td colspan="5" class="common_td_rules"><b>Bill Date & Time: <?= $data->voucher_date ?></b></td>
                    </tr>
                    @if ($i == 0)
                    <tr class='table_header'>
                        <td colspan="19" class="common_td_rules">Doctor : <b><?= $data->doctor ?></b></td>
                    </tr>
                    @endif
                    <?php
                    $theadspan_string = '';
                    if ($data->ord == '2') {
                    ?>
                        <tr class='table_header'>
                            <td class="common_td_rules" width="15%"><b>Item Name</b></td>
                            <td class="common_td_rules" width="15%"><b>Chemical</b></td>
                            <td class="common_td_rules" width="10%"><b>Batch</b></td>
                            <td class="common_td_rules" width="10%"><b>Expiry</b></td>
                            <td class="common_td_rules" width="5%"><b>PaymentType</b></td>
                            <td class="common_td_rules" width="5%"><b>Qty</b></td>
                            <td class="common_td_rules" width="5%"><b>MRP</b></td>
                            <td class="common_td_rules" width="5%"><b>GST</b></td>
                            <td class="common_td_rules" width="5%"><b>Rate</b></td>
                            <td class="common_td_rules" width="5%"><b>Amount</b></td>
                        </tr>
                    <?php } else {
                        $theadspan_string = "colspan='4'"; ?>
                        <tr class='table_header'>
                            <td <?= $theadspan_string ?> class="common_td_rules" width="20%"><b>Item Name</b></td>
                            <td class="common_td_rules" width="5%"><b>PaymentType</b></td>
                            <td class="common_td_rules" width="5%"><b>Qty</b></td>
                            <td class="common_td_rules" width="5%"><b>MRP</b></td>
                            <td class="common_td_rules" width="5%"><b>GST</b></td>
                            <td class="common_td_rules" width="5%"><b>Rate</b></td>
                            <td class="common_td_rules" width="5%"><b>Amount</b></td>
                        </tr>
                <?php
                    }
                } else {
                    $billTypeCount++;
                }
                $row_cnt++;
                $each_bill_type_total += $data->net_amount;
                if ($data->tax_rate = '12.00') {
                    $gst12 += $data->taxpaid * $data->qty;
                }
                if ($data->tax_rate = '18.00') {
                    $gst18 += $data->taxpaid * $data->qty;
                }
                if ($data->tax_rate = '28.00') {
                    $gst18 += $data->taxpaid * $data->qty;
                }



                $bill_no = $data->bill_no;
                $chemiName = explode("+", $data->generic_name);



                if ($data->ord == '3') {
                    $xray_bill_total +=  $data->net_amount;
                    $eachUniqueTotal += $data->net_amount;
                } elseif ($data->ord == '1') {
                    $lab_bill_total +=  $data->net_amount;
                    $eachUniqueTotal += $data->net_amount;
                } elseif ($data->ord == '2') {
                    $pharmacy_total +=  $data->net_amount;
                    $eachUniqueTotal += $data->net_amount;
                } ?>
                <tr>
                    <td <?= $theadspan_string ?> class="common_td_rules">{{ $data->description }}</td>
                    <?php
                    if ($data->ord == '2') {
                    ?>
                        <td class="common_td_rules">{{ $chemiName[0] }}</td>
                        <td class="common_td_rules">{{ $data->batch_code }}</td>
                        <td class="common_td_rules">{{ $data->exp_date }}</td>
                    <?php
                    }
                    ?>
                    <td class="common_td_rules">{{ $data->payment_type }}</td>
                    <td class="td_common_numeric_rules">{{ $data->qty }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($data->unit_mrp, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($data->taxpaid, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($data->eachprice, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($data->net_amount, 2, '.', '') }}
                    </td>
                </tr>
                <?php if ($billTypeCount == $depatment_namecnt[$data->bill_type][$data->bill_no]) {
                    $each_bill_type_total = number_format((float)$each_bill_type_total, 2, '.', '');
                    $billTypeCount = 0;
                ?>
                    <tr class='table_header'>
                        <td colspan="9" style="text-align: left; border-top: 1px solid #333 !important;"><b>Total Bill
                                Amount</b></td>
                        <td style="text-align: right; border-top: 1px solid #333 !important;   ">
                            <b><?= number_format($each_bill_type_total, 2, '.', '') ?></b>
                        </td>
                    </tr>
                    <?php
                    if ($i == $depatment_namecnt[$previousBillType]) {
                        $full_net_total = 0.0;
                        if ($data->ord == '3') {
                            $full_net_total = $xray_bill_total;
                        } elseif ($data->ord == '1') {
                            $full_net_total = $lab_bill_total;
                        } elseif ($data->ord == '2') {
                            $full_net_total = $pharmacy_total;
                        } ?>
                        <tr class='table_header'>
                            <td class="common_td_rules" colspan="9" style="text-align: left;"><b>Net Total</b></td>
                            <td class="td_common_numeric_rules" style="text-align: left;">
                                <b><?= number_format($full_net_total, 2, '.', '') ?></b>
                            </td>
                        </tr>
                    <?php
                    } ?>
            <?php
                }
                $i++;
            }
            ?>
            @endforeach
            <tr class='table_header'>
                <td class="common_td_rules" colspan="9" style="text-align: left;"><b><?= $previousBillType ?> Sub
                        Total:</b></td>
                <td class="td_common_numeric_rules" style="text-align: left;">
                    <b><?= number_format($eachUniqueTotal, 2, '.', '') ?></b>
                </td>
            </tr>
            <tr>
                <td class="common_td_rules" colspan="9"><B>Pharmacy Bill</B></td>
                <td class="td_common_numeric_rules"><B><?= number_format($pharmacy_total, 2, '.', '') ?></B></td>
            </tr>
            <tr>
                <td class="common_td_rules" colspan="9"><B>Lab Bill</B></td>
                <td class="td_common_numeric_rules"><B><?= number_format($lab_bill_total, 2, '.', '') ?></B></td>
            </tr>
            <tr>
                <td class="common_td_rules" colspan="9"><B>Radilogy Bill</B></td>
                <td class="td_common_numeric_rules"><B><?= number_format($xray_bill_total, 2, '.', '') ?></B></td>
            </tr>
            <tr>
                <td class="common_td_rules" colspan="5">Software generated system signature not required.</td>
                <td class="td_common_numeric_rules" colspan="4"><b>Autdorized Signatory</b></td>
            </tr>

            </tbody>
        </table>

        @endif
    </div>
</div>