<div class="row">
    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $bill_from ?> To <?= $bill_to ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b>MIS Report</b></h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='5%'>Sn. No.</th>
                    <th width='12%'>UHID</th>
                    <th width='18%'>Patient Name</th>
                    <th width='12%'>Bill NO.</th>
                    <th width='10%'>Bill Date</th>
                    <th width='10%'>Insurance Type</th>
                    <th width='16%'>Doctor Name</th>
                    <th width='10%'>Speciality</th>
                    <th width='7%'>Net Amount</th>
                </tr>
                </thead>
                <?php
                $nettotal_bill_net_amount = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
        $i=0;
        $j=1;
        $bill_tag = '';
        $total_bill_net_amount = 0;

        foreach ($res as $data){
            if($bill_tag != $data->bill_tag){
                $i=0;
                $j=1;
                $bill_tag = $data->bill_tag;
                $total_bill_net_amount = 0.0;
              ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="9" class="common_td_rules">{{ $data->bill_tag_desc }}</th>
                        </tr>
                        <tr>
                            <td class="td_common_numeric_rules">{{ $j }}.</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                            <td class="common_td_rules">{{ $data->company_name }}</td>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="common_td_rules">{{ $data->speciality }}</td>
                            <td class="td_common_numeric_rules">
                                {{ number_format($data->net_amount_wo_roundoff, 2, '.', '') }}</td>
                        </tr>
                        <?php
            }else{
                ?>
                        <tr>
                            <td class="td_common_numeric_rules">{{ $j }}.</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                            <td class="common_td_rules">{{ $data->company_name }}</td>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="common_td_rules">{{ $data->speciality }}</td>
                            <td class="td_common_numeric_rules">
                                {{ number_format($data->net_amount_wo_roundoff, 2, '.', '') }}</td>
                        </tr>
                        <?php

            }
                             $total_bill_net_amount+= floatval($data->net_amount_wo_roundoff);
            if($i==$bill_tag_cnt[$bill_tag]){
            ?>
                        <tr class="" style="height: 30px;">
                            <th class="common_td_rules" colspan="8" style="text-align: left;">Sub Total</th>
                            <th class="td_common_numeric_rules"><?= number_format($total_bill_net_amount, 2, '.', '') ?>
                            </th>
                        </tr>
                        <?php
                 $nettotal_bill_net_amount+=$total_bill_net_amount;
            }
            $i++;
            $j++;

        }
        ?>
                        <tr class="bg-info" style="height: 30px;">
                            <th class="common_td_rules" colspan="8" style="text-align: left;">Total</th>
                            <th class="td_common_numeric_rules">
                                <?= number_format($nettotal_bill_net_amount, 2, '.', '') ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="10" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </font>
    </div>
</div>
