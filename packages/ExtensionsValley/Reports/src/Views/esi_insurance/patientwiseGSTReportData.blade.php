<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('Y-m-d h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to_date ?></p>

        <!-- <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Patientwise GST report</b></h4> -->
        <div class="col-md-12" id="result_container_div">

            @php
                extract($headData);
                $result1 = $top;
                $result2 = $table1;
                $report_decimal_separator;
                $result3 = $table2;
            @endphp


            @if (sizeof($result1) > 0)

                @foreach ($result1 as $data)
                    <h6 style="font-size: xx-small;color: black;margin-top: 0px;">{{ $data->name }}
                        &nbsp;&nbsp;&nbsp;&nbspStart From:{{ $data->minbillno }} To:{{ $data->maxbillno }}</h6>

                @endforeach
            @endif

            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">

                <thead class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
                    <tr>
                        <th width='12%'>Payment Date</th>
                        <th width='8%'>Divisions</th>
                        <th width='8%'>sales (5%)</th>
                        <th width='8%'>sales (5%)tax</th>
                        <th width='8%'>sales (12%)</th>
                        <th width='8%'>sales (12%)tax</th>
                        <th width='8%'>sales (18%)</th>
                        <th width='8%'>sales (18%)tax</th>
                        <th width='8%'>sales (28%)</th>
                        <th width='8%'>sales (28%)tax</th>
                        <th width='8%'>Exe. sales</th>
                        <th width='8%'>Total</th>
                    </tr>
                </thead>
                <?php
                $sales_five = 0.0;
                $sales_five_tax = 0.0;
                $sales_twelve = 0.0;
                $sales_twelve_tax = 0.0;
                $sales_eighteen = 0.0;
                $sales_eighteen_tax = 0.0;
                $sales_twentyeight = 0.0;
                $sales_twentyeight_tax = 0.0;
                $exemptedsales = 0.0;
                $fullnetsales_five = 0.0;
                $fullnetsales_five_tax = 0.0;
                $fullnetsales_twelve = 0.0;
                $fullnetsales_twelve_tax = 0.0;
                $fullnetsales_eighteen = 0.0;
                $fullnetsales_eighteen_tax = 0.0;
                $fullnetsales_twentyeight = 0.0;
                $fullnetsales_twentyeight_tax = 0.0;
                $fullnetexemptedsales = 0.0;
                ?>
                <tbody>
                    <?php
            if(count($result2)!=0){
              $netsales_five=0.0;
              $netsales_five_tax=0.0;
              $netsales_twelve=0.0;
              $netsales_twelve_tax=0.0;
              $netsales_eighteen=0.0;
              $netsales_eighteen_tax=0.0;
              $netsales_twentyeight=0.0;
              $netsales_twentyeight_tax=0.0;
              $netexemptedsales=0.0;
              $net_total=0.0;
              $full_total=0.0;
              foreach($result2 as $each){
                $netsales_five=0.0;
              $netsales_five_tax=0.0;
              $netsales_twelve=0.0;
              $netsales_twelve_tax=0.0;
              $netsales_eighteen=0.0;
              $netsales_eighteen_tax=0.0;
              $netsales_twentyeight=0.0;
              $netsales_twentyeight_tax=0.0;
              $netexemptedsales=0.0;
                $sales_five= floatval($each->salesfive)/2;
                $netsales_five+=floatval($each->salesfive);
                $sales_five_tax= floatval($each->salesfivetax)/2;
                $netsales_five_tax+=floatval($each->salesfivetax);
                $sales_twelve= floatval($each->salestwelve)/2;
                $netsales_twelve+=floatval($each->salestwelve);
                $sales_twelve_tax= floatval($each->salestwelvetax)/2;
                $netsales_twelve_tax+=floatval($each->salestwelvetax);
                $sales_eighteen= floatval($each->saleseighteen)/2;
                $netsales_eighteen+=floatval($each->saleseighteen);
                $sales_eighteen_tax= floatval($each->saleseighteentax)/2;
                $netsales_eighteen_tax+=floatval($each->saleseighteentax);
                $sales_twentyeight= floatval($each->salestwentyeight)/2;
                $netsales_twentyeight+=floatval($each->salestwentyeight);
                $sales_twentyeight_tax= floatval($each->salestwentyeighttax)/2;
                $netsales_twentyeight_tax+=floatval($each->salestwentyeighttax);
                $exemptedsales= floatval($each->exemptedsales)/2;
                $netexemptedsales=+floatval($each->exemptedsales);
                $total_tax=$sales_five+$sales_five_tax+$sales_twelve+$sales_twelve_tax+$sales_eighteen+$sales_eighteen_tax+
                $sales_twentyeight+$sales_twentyeight_tax+$exemptedsales;
                $net_total+=$total_tax*2;
                ?>
                    <tr>
                        <td class="common_td_rules"><?= date('M-d-Y', strtotime($each->payment_date)) ?></td>
                        <td class="common_td_rules">CGST</td>
                        <td class="td_common_numeric_rules"><?= $sales_five ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_five_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $exemptedsales ?></td>
                        <td class="td_common_numeric_rules"><?= $total_tax ?></td>
                    </tr>
                    <tr>
                        <td class="common_td_rules">-</td>
                        <td class="common_td_rules">SGST</td>
                        <td class="td_common_numeric_rules"><?= $sales_five ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_five_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $exemptedsales ?></td>
                        <td class="td_common_numeric_rules"><?= $total_tax ?></td>

                    </tr>
                    <tr>
                        <td class='common_td_rules' style='text-align:left'><strong> Sub Total </strong></td>
                        <td class='td_common_numeric_rules'></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_five ?> </strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_five_tax ?> </strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twelve ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twelve_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_eighteen ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_eighteen_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twentyeight ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twentyeight_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netexemptedsales ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $total_tax * 2 ?></strong></td>
                    </tr>
                    <?php
                $fullnetsales_five+=$netsales_five;
                $fullnetsales_five_tax+=$netsales_five_tax;
                $fullnetsales_twelve+=$netsales_twelve;
                $fullnetsales_twelve_tax+=$netsales_twelve_tax;
                $fullnetsales_eighteen+=$netsales_eighteen;
                $fullnetsales_eighteen_tax+=$netsales_eighteen_tax;
                $fullnetsales_twentyeight+=$netsales_twentyeight;
                $fullnetsales_twentyeight_tax+=$netsales_twentyeight_tax;
                $fullnetexemptedsales+=$netexemptedsales;
              }
              $full_total=$net_total;
              ?>
                    <tr>
                        <td class='common_td_rules' style='text-align:left'>Net Total</td>
                        <td class='td_common_numeric_rules'> </td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_five ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_five_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twelve ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twelve_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_eighteen ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_eighteen_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twentyeight ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twentyeight_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetexemptedsales ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $net_total ?></strong></td>
                    </tr>
                    <?php
            }else{
              ?>
                    <tr>
                        <td colspan='14' style='text-align:center'>No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

            </table>



            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">

                <thead class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
                    <tr>
                        <th width='12%'>Payment Date</th>
                        <th width='8%'>Divisions</th>
                        <th width='8%'>sales (5%)</th>
                        <th width='8%'>sales (5%)tax</th>
                        <th width='8%'>sales (12%)</th>
                        <th width='8%'>sales (12%)tax</th>
                        <th width='8%'>sales (18%)</th>
                        <th width='8%'>sales (18%)tax</th>
                        <th width='8%'>sales (28%)</th>
                        <th width='8%'>sales (28%)tax</th>
                        <th width='8%'>Exe. sales</th>
                        <th width='8%'>Total</th>
                    </tr>
                </thead>
                <?php
                $sales_five = 0.0;
                $sales_five_tax = 0.0;
                $sales_twelve = 0.0;
                $sales_twelve_tax = 0.0;
                $sales_eighteen = 0.0;
                $sales_eighteen_tax = 0.0;
                $sales_twentyeight = 0.0;
                $sales_twentyeight_tax = 0.0;
                $exemptedsales = 0.0;
                $fullnetsales_five = 0.0;
                $fullnetsales_five_tax = 0.0;
                $fullnetsales_twelve = 0.0;
                $fullnetsales_twelve_tax = 0.0;
                $fullnetsales_eighteen = 0.0;
                $fullnetsales_eighteen_tax = 0.0;
                $fullnetsales_twentyeight = 0.0;
                $fullnetsales_twentyeight_tax = 0.0;
                $fullnetexemptedsales = 0.0;
                ?>
                <tbody>
                    <?php
            if(count($result3)!=0){
              $netsales_five=0.0;
              $netsales_five_tax=0.0;
              $netsales_twelve=0.0;
              $netsales_twelve_tax=0.0;
              $netsales_eighteen=0.0;
              $netsales_eighteen_tax=0.0;
              $netsales_twentyeight=0.0;
              $netsales_twentyeight_tax=0.0;
              $netexemptedsales=0.0;
              foreach($result3 as $each){
                $sales_five= floatval($each->salesfive)/2;
                $netsales_five+=floatval($each->salesfive);
                $sales_five_tax= floatval($each->salesfivetax)/2;
                $netsales_five_tax+=floatval($each->salesfivetax);
                $sales_twelve= floatval($each->salestwelve)/2;
                $netsales_twelve+=floatval($each->salestwelve);
                $sales_twelve_tax= floatval($each->salestwelvetax)/2;
                $netsales_twelve_tax+=floatval($each->salestwelvetax);
                $sales_eighteen= floatval($each->saleseighteen)/2;
                $netsales_eighteen+=floatval($each->saleseighteen);
                $sales_eighteen_tax= floatval($each->saleseighteentax)/2;
                $netsales_eighteen_tax+=floatval($each->saleseighteentax);
                $sales_twentyeight= floatval($each->salestwentyeight)/2;
                $netsales_twentyeight+=floatval($each->salestwentyeight);
                $sales_twentyeight_tax= floatval($each->salestwentyeighttax)/2;
                $netsales_twentyeight_tax+=floatval($each->salestwentyeighttax);
                $exemptedsales= floatval($each->exemptedsales)/2;
                $netexemptedsales=+floatval($each->exemptedsales);
                $total_tax=$sales_five+$sales_five_tax+$sales_twelve+$sales_twelve_tax+$sales_eighteen+$sales_eighteen_tax+
                $sales_twentyeight+$sales_twentyeight_tax+$exemptedsales;
                $net_total+=$total_tax*2;
                ?>
                    <tr>
                        <td class="common_td_rules"><?= date('M-d-Y', strtotime($each->payment_date)) ?></td>
                        <td class="common_td_rules">CGST</td>
                        <td class="td_common_numeric_rules"><?= $sales_five ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_five_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $exemptedsales ?></td>
                        <td class="td_common_numeric_rules"><?= $total_tax ?></td>
                    </tr>
                    <tr>
                        <td class="common_td_rules">-</td>
                        <td class="common_td_rules">SGST</td>
                        <td class="td_common_numeric_rules"><?= $sales_five ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_five_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twelve_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_eighteen_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_twentyeight_tax ?></td>
                        <td class="td_common_numeric_rules"><?= $exemptedsales ?></td>
                        <td class="td_common_numeric_rules"><?= $total_tax ?></td>

                    </tr>
                    <tr>
                        <td class='common_td_rules' colspan='2' style='text-align:left'><strong> Sub Total </strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_five ?> </strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_five_tax ?> </strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twelve ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twelve_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_eighteen ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_eighteen_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twentyeight ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netsales_twentyeight_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $netexemptedsales ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $total_tax * 2 ?></strong></td>
                    </tr>
                    <?php
                    $fullnetsales_five+=$netsales_five;
                    $fullnetsales_five_tax+=$netsales_five_tax;
                    $fullnetsales_twelve+=$netsales_twelve;
                    $fullnetsales_twelve_tax+=$netsales_twelve_tax;
                    $fullnetsales_eighteen+=$netsales_eighteen;
                    $fullnetsales_eighteen_tax+=$netsales_eighteen_tax;
                    $fullnetsales_twentyeight+=$netsales_twentyeight;
                    $fullnetsales_twentyeight_tax+=$netsales_twentyeight_tax;
                    $fullnetexemptedsales+=$netexemptedsales;
              }
              ?>
                    <tr>
                        <td class='common_td_rules' colspan='2' style='text-align:left'>Net Total</td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_five ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_five_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twelve ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twelve_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_eighteen ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_eighteen_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twentyeight ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetsales_twentyeight_tax ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $fullnetexemptedsales ?></strong></td>
                        <td class='td_common_numeric_rules'><strong><?= $net_total ?></strong></td>
                    </tr>
                    <tr class='headerclass'>
                        <th class='common_td_rules bg-info' colspan='11' style='text-align:left'>Net Sales(Sales-Sales
                            Return)</th>
                        <th class='td_common_numeric_rules bg-info'><?= $full_total - $net_total ?></th>
                    </tr>
                    <?php
            }else{
              ?>
                    <tr>
                        <td colspan='14' style='text-align:center'>No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

            </table>

        </div>

    </div>
