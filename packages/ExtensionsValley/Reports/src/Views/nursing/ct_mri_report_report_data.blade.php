<div class="row">
  <div class="col-md-12" id="result_container_div">

            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
            <?php
                $collect = collect($res); $total_records=count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Ceeves Scan Report </b></h4>
            <font size="16px" face="verdana" >
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
      <thead>
    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width="10%;">Bill Date</th>
                <th width="15%;">UHID</th>
                <th width="25%;">Patient Name</th>
                <th width="15%;">Bill No</th>
                <th width="15%;">Test Name</th>
                <th width="7%;">Bill Amount</th>
                <th width="8%;">Payment Date</th>
                <th width="5%;">Paid Status</th>


            </tr>
    </thead>
    @if(sizeof($res)>0)
    <tbody>
        <?php
        $total_amount = 0.0;
        foreach ($res as $data){
                $total_amount+= floatval($data->bill_amount);
              ?>

            <tr>

                <td class="common_td_rules">{{$data->bill_date}}</td>
                <td class="common_td_rules">{{$data->uhid}}</td>
                <td class="common_td_rules">{{$data->patient_name}}</td>
                <td class="common_td_rules">{{$data->bill_no}}</td>
                 <td class="common_td_rules">{{$data->test_name}}</td>
                <td class="td_common_numeric_rules">{{$data->bill_amount}}</td>
                <td class="common_td_rules">{{$data->payment_date}}</td>
                <td class="common_td_rules">{{$data->paid_status}}</td>
                <!-- <td class="common_td_rules">{{$data->payment_type}}</td> -->

            </tr>
            <?php
        }

        ?>
        <tr style="height: 30px;background-color:rgb(230 218 173);color:black;border-spacing: 0 1em;font-family:sans-serif">
            <th colspan="4" class="common_td_rules">Total</th>
            <th class="td_common_numeric_rules"><?=number_format((float)$total_amount, 2, '.', '')?></th>
             <th colspan="3"></th>
        </tr>
    </tbody>
    @else
    <tr>
        <td colspan="4" style="text-align: center;">No Results Found!</td>
    </tr>
    @endif

</table>
            </font>
</div>
</div>
