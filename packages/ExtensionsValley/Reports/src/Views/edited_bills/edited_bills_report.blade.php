@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">

<style>
    .container-fluid {
        padding: 0 10px !important;
    }

    .mate-input-box input.form-control {
        margin-top: 5px;
    }

    .ajaxSearchBox>li {
        font-size: 12px !important;
    }

    #bill_item_details .common_td_rules {
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 0px #bbd2bd !important;
        border-left: solid 0px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
</style>

@endsection

@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_headder" value="{{$hospital_header}}">
<input type="hidden" id="hospital_address" value="">
<input type="hidden" id="current_date" value="{{date('M-d-Y')}}">
<input type="hidden" id="exceller_data" value="<?= base64_encode('<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>') ?>">

<div class="right_col">
    <div class="container-fluid">
        <div class="col-md-12 padding_sm">
            <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                <div class="box no-border no-margin">
                    <div class="box-body" style="padding-bottom:15px;">
                        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th colspan="11"> {{$title}} </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                                {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                                <div class="col-md-12">
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label>Date From</label>
                                            <div class="clearfix"></div>
                                            <input type="text" data-attr="date" autocomplete="off" name="date_from" autofocus="" value="{{date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="date_from">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label>Date To</label>
                                            <div class="clearfix"></div>
                                            <input type="text" data-attr="date" autocomplete="off" name="date_to" autofocus="" value="{{date('M-d-Y')}}" class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="date_to">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label>UHID</label>
                                            <div class="clearfix"></div>
                                            <input class="form-control hidden_search" value="" autocomplete="off" type="text" id="op_no" name="op_no">
                                            <div id="op_noAjaxDiv" class="ajaxSearchBox" style="width: 175% !important; display: none; margin-top: -15px;"></div>
                                            <input class="filters" value="" type="hidden" name="op_no_hidden" id="op_no_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label>Bill No</label>
                                            <div class="clearfix"></div>
                                            <input class="form-control hidden_search" value="" autocomplete="off" type="text" id="bill_number" name="bill_number">
                                            <div id="bill_numberAjaxDiv" class="ajaxSearchBox" style="width: 175% !important; display: none; margin-top: -15px;"></div>
                                            <input class="filters" value="" type="hidden" name="bill_number_hidden" id="bill_number_hidden">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                                    <div class="col-md-1 padding_sm pull-right">
                                        <button class="btn light_purple_bg btn-block" type="button" onclick="loadReportData();" name="search_results" id="search_results">
                                            <i id="search_results_spin" class="fa fa-search" aria-hidden="true"></i>
                                            Search
                                        </button>
                                    </div>
                                    <div class="col-md-1 padding_sm pull-right">
                                        <button class="btn light_purple_bg btn-block" type="button" onclick="exceller();" name="csv_results" id="csv_results" disabled>
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            Excel
                                        </button>
                                    </div>
                                    <!-- <div class="col-md-1 padding_sm pull-right">
                                        <button class="btn light_purple_bg btn-block" type="button" onclick="printReportData();" name="print_results" id="print_results" disabled>
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                            Print
                                        </button>
                                    </div> -->
                                    <div class="col-md-1 padding_sm pull-right">
                                        <button class="btn light_purple_bg btn-block" type="button" onclick="search_clear();" name="clear_results" id="clear_results">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            Reset
                                        </button>
                                    </div>
                                </div>
                                {!! Form::token() !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="always-visible" id="ResultDataContainer" style="max-height: 650px; padding: 10px 10px 10px 0px; display:none;font-family:poppinsregular">
                    <div style="background:#686666;">
                        <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%); width: 100%; padding: 30px; min-height: 650px;" id="ResultsViewArea">

                        </page>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------bill detail modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="edited_bill_detail_modal">
    <div class="modal-dialog" role="document" style="width: 80vw; height: 80vh;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" id="edited_bill_detail_modal_header" style="color: white;">Bill No</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 72vh;">
                <div class="col-md-12 no-padding theadscroll" id="edited_bill_detail_modal_data" style="height: 65vh;">
                </div>
            </div>
        </div>
    </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/reports/default/javascript/edited_bills_report.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<!-- <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"> -->
</script>
@endsection