<div class="row">
    <div class="col-md-12" id="result_container_div">
        <input type="hidden" id="enable_print" value="{{ $enable_print }}">
        <!-- <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p> -->
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <!-- <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Bill Edit Report </b></h4> -->
        <font size="16px" face="verdana">
            <table border='0' ; width="100%;" class=" no-padding" style="border-collapse: collapse;table-layout:fixed;border-color:white; font-size: 13px;">
                <tr style="height: 25px;">
                    <td style="text-align:left;" colspan="6">
                        Report Print Date: {{date('M-d-Y h:i A')}}
                    </td>
                </tr>
                <tr style="height: 25px;">
                    <td style="text-align:left;" colspan="6">
                        Report Date: {{$from_date}} To {{$to_date}}
                    </td>
                </tr>
                <tr style="height: 25px;">
                    <td style="text-align:left;" colspan="6">
                        Number Of Records: {{$total_records}}
                    </td>
                </tr>
                @php
                $company_details = \DB::table('company')->where('id',1)->first();
                @endphp
                <tr class="hospital_header_excel" style="display:none;">
                    <td style="text-align: center !important;" colspan="6">
                        <span><strong>{{$company_details->name}}</strong></span><br>
                        <span>{{$company_details->address}}</span><br>
                        <span>Phone :{{$company_details->phone}}</span>
                        <span>Booking No :{{$company_details->booking_no}}</span>
                    </td>
                </tr>
                <tr class="no-padding" style="height: 30px;">
                    <td style="font-size: 16px !important;text-align:center;" colspan="6">
                        Bill Edit Report
                    </td>
                </tr>
            </table>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                <thead>
                    <tr class="" style="background-color:#36a693; color:white; border-spacing: 0 1em;font-family:sans-serif">
                        <th width="14%;">Bill No</th>
                        <th width="10%;">Bill Tag</th>
                        <th width="13%;">Bill Date</th>
                        <th width="12%;">UHID</th>
                        <th width="20%;">Patient Name</th>
                        <th width="20%;">Doctor Name</th>
                    </tr>
                </thead>
                <tbody>
                    @if (sizeof($res) > 0)
                    @foreach($res as $data)
                    <tr onclick="detailView('{{ $data->bill_no }}', '{{ $data->bill_tag }}')" style="height: 25px; cursor: pointer;">
                        <td class="common_td_rules">{{ $data->bill_no }}</td>
                        <td class="common_td_rules">{{ $data->bill_tag_name }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y h:i A', strtotime($data->bill_datetime)) }}</td>
                        <td class="common_td_rules">{{ $data->uhid }}</td>
                        <td class="common_td_rules">{{ $data->patient_name }}</td>
                        <td class="common_td_rules">{{ $data->doctor_name }}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="6" style="text-align: center;">No Results Found!</td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </font>
    </div>
</div>