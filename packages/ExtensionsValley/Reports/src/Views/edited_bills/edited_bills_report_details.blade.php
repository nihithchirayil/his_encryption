<div class="col-md-12 padding_sm">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered1 no-border' style="font-size: 12px; width: 100%;">
        <tbody>
            @foreach($res as $key => $data)
            <tr style="border-bottom: 1px solid #ccc; ">
                <td>
                    @php
                    $item_arr = @$data['item_arr'] ?? [];
                    $new_items = @$data['new_items'] ?? [];
                    $removed_items = @$data['removed_items'] ?? [];
                    $head_arr = @$data['head_arr'] ?? [];
                    @endphp
                    <table class="table table_sm no-border" id="bill_item_details" style="width: 100%;margin-bottom: 0px;">
                        <!-- <thead>
                            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="50%;">Item</th>
                                <th width="10%;">Qty</th>
                                <th width="10%;">Discount</th>
                                <th width="30%;">Reason</th>
                            </tr>
                        </thead> -->
                        <tbody>
                            @php
                            $type = 'Edited';
                            $bg_color = 'background-color:rgb(63 180 237);';
                            $changes_applied = 0;
                            if ($key == 0) {
                            $type = 'Created';
                            $bg_color = 'background-color:rgb(3 147 140);';
                            }
                            @endphp
                            <tr style="height: 25px;">
                                <td class="common_td_rules"><b> {{$type}} Date & Time: {{ $head_arr['json_date'] }} </b></td>
                                <td class="common_td_rules"><b> {{$type}} By: {{ ucwords(strtolower($head_arr['created_by'])) }} </b></td>
                            </tr>
                            @if(!empty($head_arr['reason']))
                            @php $changes_applied = 1; @endphp
                            <tr>
                                <td colspan="2" style="text-align: left;">{!! $head_arr['reason'] !!}</td>
                            </tr>
                            @endif
                            @if(count($item_arr) > 0 || count($new_items) > 0 || count($removed_items) > 0)
                            @php $changes_applied = 1; @endphp
                            <tr class="headerclass" style="{{$bg_color}};color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="40%;">Item</th>
                                <!-- <th width="10%;">Qty</th>
                                <th width="10%;">Discount</th> -->
                                <th width="60%;">Reason</th>
                            </tr>
                            @foreach($item_arr as $item)
                            <tr class="existing_items" style="height: 20px;">
                                <td class="common_td_rules">{{ $item->item_desc }}</td>
                                <!-- <td class="common_td_rules">{{ $item->qty }}</td>
                                <td class="common_td_rules">{{ $item->discount_amount }}</td> -->
                                <td class="common_td_rules">{{ @$item->reason ?? '' }}</td>
                            </tr>
                            @endforeach
                            @foreach($new_items as $item)
                            <tr class="new_items" style="height: 20px; color: green;">
                                <td class="common_td_rules">{{ $item->item_desc }}</td>
                                <!-- <td class="common_td_rules">{{ $item->qty }}</td>
                                <td class="common_td_rules">{{ $item->discount_amount }}</td> -->
                                <td class="common_td_rules">{{ @$item->reason ?? 'Item added to the list' }}</td>
                            </tr>
                            @endforeach
                            @foreach($removed_items as $item)
                            <tr class="removed_items" style="height: 20px; color: red;">
                                <td class="common_td_rules">{{ $item->item_desc }}</td>
                                <!-- <td class="common_td_rules">{{ $item->qty }}</td>
                                <td class="common_td_rules">{{ $item->discount_amount }}</td> -->
                                <td class="common_td_rules">{{ @$item->reason ?? 'Item removed from the list' }}</td>
                            </tr>
                            @endforeach
                            @endif
                            @if($changes_applied == 0)
                            <tr style="height: 20px;">
                                <td colspan="2" style="text-align: left;">No changes found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px;">
                <td>&nbsp;</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>