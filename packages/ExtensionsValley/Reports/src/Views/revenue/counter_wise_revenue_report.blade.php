@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>

  .box_border {
      padding-top: 25px;
      padding-bottom: 10px;
      border: 1px solid #ccc;
      background: #f3f3f3;
  }
  .tinymce-content p {
      padding: 0;
      margin: 2px 0;
  }
  .modal-header{
      background-color:orange;
  }

    #MTable > tbody > tr > td{
        word-wrap:break-word
      }
      table#posts {
        word-wrap: break-word;
    }
    .page{
        padding: 65px;
        min-height: 1134px;
        width: 812px;
    }



    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-md-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajamdearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        overflow-y: auto;
        width: 250px;
        z-index: 599;
        position:relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .ajamdearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }


    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }

    .filter_label {
       margin-bottom: 0px;
    }
    .btn-group {
        width:100% !important;
    }
    .col-md-12{
        margin-bottom:8px;
    }
    .col-md-6{
        margin-bottom:8px;
    }
    .filter_label {
        color: #294236 !important;
        font-weight: 600 !important;
    }
    @media print{
        .headerclass{
            background:#999;
        }
        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 1px #aaa;
        }
    }
    .mate_select_data {
        width: 100%;
        position: relative;
        padding: 2px 4px 14px 4px !important;
        border-bottom: 2px solid #01A881;
        box-shadow: 0 0 3px #ccc;
        border-radius: 6px 6px 0 0;
    }

</style>
@endsection
@section('content-area')
     <!-- page content -->

     <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title pull-left" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" >
                <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="service_datamodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1100px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="service_dataheader">NA</h4>
            </div>
            <div class="modal-body"  style="min-height: 400px">
            <div class="box no-border no-margin">
                <div class="box-footer" style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 510px;">
                    <div class="col-md-12 padding_sm" id="service_datadiv">

                    </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="right_col" >
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">

<div class="row pull-right" style="font-size: 12px; font-weight: bold;">
{{$title}}
    <div class="clearfix"></div>
    <div class="h10"></div>
</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <input type="hidden" value="<?=$current_date?>" id="current_date">
                    <div class="box-body clearfix">

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" id="bill_fromdate" name=""
                                    value="<?=$current_date?>">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" id="bill_todate" name=""
                                    value="<?=$current_date?>">
                                </div>
                            </div>


                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Counter</label>
                                    <div class="clearfix"></div>
                                    <select name="counter" id="counter" class="form-control select2">
                                        <option value="">Select Counter</option>
                                        @foreach ($counter as $data)
                                           <option value="{{ $data->id }}">{{ $data->counter_name }}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                    <label for="">Doctor</label>
                                    <div class="clearfix"></div>
                                    <select name="doctor" id="doctor" class="form-control select2">
                                        <option value="">Select Doctor</option>
                                        @foreach ($doctor as $data)
                                           <option value="{{ $data->id }}">{{ $data->doctor_name }}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </div> -->
                            <!-- <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Payment Mode</label>
                                    <div class="clearfix"></div>
                                    <select name="payment_mode" id="payment_mode" class="form-control select2">
                                        <option value="">Select Mode</option>
                                        @foreach ($payment_code as $data)
                                           <option value="{{ $data->code }}">{{ $data->name }}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </div> -->
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill Tag</label>
                                    <div class="clearfix"></div>
                                    <select name="bill_tag" id="bill_tag" class="form-control select2">
                                        <option value="">Select Bill Tag</option>
                                        @foreach ($bill_tag as $data)
                                           <option value="{{ $data->code }}">{{ $data->name }}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>

                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <button class="btn light_purple_bg btn-block" onclick="getReportData();" name="search_results" id="search_results">
                                    <i id="serachResultSpin" class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                            <a class="btn light_purple_bg disabled btn-block" onclick="exceller();"
                                                name="csv_results" id="csv_results">
                                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                Excel
                                            </a>
                                        </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <a class="btn light_purple_bg disabled btn-block" onclick="printReportData();"  name="print_results" id="print_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                <a class="btn light_purple_bg btn-block" onclick="search_clear();"  name="clear_results" id="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm">
                <div id="ResultDataContainer" style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                    <div style="background:#686666;">
                    <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 1cm; box-shadow: 0 0 1cm rgb(113 113 113 / 50%);
                    width: 100%; padding: 50px;" id="ResultsViewArea">
                    </page>
                    </div>
                </div>
             </div>

        </div>
    </div>
</div>

{!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
<script src="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{asset('packages/extensionsvalley/emr/js/counterWiseRevenueReport.js?version=' . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
     <script
     src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
 </script>

@endsection
