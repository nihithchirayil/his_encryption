<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Counter Wise Revenue report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='5%'>Sn. No.</th>
                        <th width='23%'>Counter</th>
                        <th width='8%'>Cash</th>
                        <th width='8%'>Chq/DD</th>
                        <th width='8%'>Cr.Card</th>
                        <th width='8%'>UPI</th>
                        <th width='8%'>Total</th>
                        <th width='8%'>Corporate</th>
                        <th width='8%'>Op Credit</th>
                        <th width='8%'>Ip Cedit</th>
                        <th width='8%'>Compl.</th>
                       
                    </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                    @php
                             $grand_total_cash=0;
                             $grand_total_chq_dd=0;
                             $grand_total_card=0;
                             $grand_total_upi=0;
                             $grand_total_total=0;
                             $grand_total_corp_company=0;
                             $grand_total_op_credit=0;
                             $grand_total_ip_credit=0;
                             $grand_total_compl=0;
                              @endphp
                        @foreach ($res as $key => $row)
                          <tr class="headerclass" 
                            style="font-weight:500;background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif"><td colspan="11" style="text-align:left;" >{{$key}}</td></tr>
                             @php
                             $count=1;
                             $total_cash=0;
                             $total_chq_dd=0;
                             $total_card=0;
                             $total_upi=0;
                             $total_total=0;
                             $total_corp_company=0;
                             $total_op_credit=0;
                             $total_ip_credit=0;
                             $total_compl=0;
                              @endphp
                            @foreach($row as  $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $count}}.</td>
                                <!-- <td class="common_td_rules">{{ @$data['counter_name'] ? $data['counter_name'] : '--' }}</td> -->
                                <td class="common_td_rules">{{ $data['counter_name'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['cash'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['chq_dd'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['card'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['upi'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['total'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['corp_company'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['op_credit'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['ip_credit'] }}</td>
                                <td class="td_common_numeric_rules">{{ $data['compl'] }}</td>
                            


                            </tr>
                            @php
                            $count+=1;
                             $total_cash+= $data['cash'];
                             $total_chq_dd+= $data['chq_dd'];
                             $total_card+=$data['card'];
                             $total_upi+=$data['upi'];
                             $total_total+=$data['total'];
                             $total_corp_company+=$data['corp_company'];
                             $total_op_credit== $data['op_credit'];
                             $total_ip_credit+=$data['ip_credit'];
                             $total_compl+=$data['compl'];
                             $grand_total_cash+=$data['cash'];
                             $grand_total_chq_dd+=$data['chq_dd'];
                             $grand_total_card+=$data['card'];
                             $grand_total_upi+=$data['upi'];
                             $grand_total_total+=$data['total'];
                             $grand_total_corp_company+=$data['corp_company'];
                             $grand_total_op_credit+=$data['op_credit'];
                             $grand_total_ip_credit+=$data['ip_credit'];
                             $grand_total_compl+=$data['compl'];
                            @endphp
                          @endforeach
                      <tr><td></td><td><strong>{{$key}} Total</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_cash,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_chq_dd,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_card,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_upi,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_total,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_corp_company,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_op_credit,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_ip_credit,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($total_compl,2)}}</strong></td>
                    </tr>
                        @endforeach

                        <tr style="border-top:2px solid black"><td></td><td><strong>Grand Total</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_cash,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_chq_dd,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_card,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_upi,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_total,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_corp_company,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_op_credit,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_ip_credit,2)}}</strong></td>
                      <td class="td_common_numeric_rules"><strong>{{ number_format($grand_total_compl,2)}}</strong></td>
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
