<?php
$padding_number_arr = explode(" ",$show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 8*$padding_number.'px';
$padding =  "padding-left:$padding_size";
$border_style = "";
?>
<div class="theadscroll always-visible" style="max-height: 400px">
<table id="result_data_table" class="theadfix_wrapper">
<thead>
<tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;">
        <th ></th>
        <th style="text-align:center">Leader Name</th>
        <th style="text-align:center">Amount</th>
    </tr>
</thead>
<?php
    for ($i = 0; $i < sizeof($ledger_result); $i++) {
       
        ?>
    <tbody>
    <tr id="liab_{{$i}}"  class="{{$show_row_group}}">
        @if($ledger_result[$i]->is_ledger == 0)
        <td style="width:2%"><button type="button" onclick="showDetailModal(this,'<?= $ledger_result[$i]->id; ?>','<?=$ledger_result[$i]->ledger_name?>')" style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info" id="infodatapopup<?=$i?>"></i></button></td>
        @else
        <td></td>
        @endif
        <td style="cursor:pointer;width:80%" onclick="getLeaderDetails('<?= $ledger_result[$i]->ledger_name ?>',<?= $ledger_result[$i]->id ?>)" >
            @if($ledger_result[$i]->is_ledger == 0)
            <b >{{$ledger_result[$i]->ledger_name??''}}</b>
            @else
            {{$ledger_result[$i]->ledger_name??''}}
            @endif
        </td>
        <td  style=" <?= $border_style; ?> text-align:right;width:15%;padding-right:20px" >
            @if($ledger_result[$i]->is_ledger == 1)
            {{$ledger_result[$i]->amount}}
            @else
            {{$ledger_result[$i]->grp_sum}}
            @endif
        </td>
    </tr>
    </tbody>
    </div>

    <?php }
    
    
     ?>