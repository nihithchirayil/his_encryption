<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        font-size: 12px;
        font-family:robotoregular !important;
    }
    td{
        padding:5px;
        width:10%; 
    }
    .td_head_cls {
        background-color: #c6d4e5;
        font-weight:bold;
    }
tr:hover {
    color: blue !important;
}
hr{
margin-top: 0px;
margin-bottom: 1px;
border: 0;
border-top: 1px solid black;
}
</style>
<?php
$sort_liab_array = $sort_asset_array = array();
$asset_total = $liab_total = $asset_amnts = 0;
?>
<div class="row" id="result_container_div">
    <div class="col-md-12">
        <table id="result_data_table" class="" style="">
            <tr>
                <td colspan="6"><b>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</b></td>
            </tr>
            <tr>
                <td colspan="6"><b>(A Unit of Medical Centre Trichur Ltd)</td>

            </tr>
            <tr>
                <td colspan="6" >Near Viyyur Bridge,Shoranur Road</td>
            </tr>
            <tr>
                <td colspan="6" >Thrissur</td>
            </tr>
            <tr>
                <td colspan="6" ><b>Balance Sheet</b></td>
            </tr>
            <tr>
                <td colspan="6" >{{$from_date}} to {{$to_date}}</td>
            </tr>
        </table>
        <div class="col-md-12" style="border:thin solid black;border-top: none;padding: 0" >
        <table style="float: left;width: 50%;border:thin solid black;border-left: none;border-bottom: none" id="liab_table">
           <tr>
                    <td  style="border-bottom: thin solid black;" colspan="4"><b>Liabilities</b>
                        <b style="float: right">{{$from_date}} to {{$to_date}}</b></td>
                </tr>
               <?php
                for ($i = 0; $i < sizeof($liability_results); $i++) {
                    $show_sub = "onclick=expandRow(this," . $liability_results[$i]->parent_id . ")";
                    if (isset($liability_results[$i])) {
                        if (!in_array($liability_results[$i]->parent_id, $sort_liab_array)) {
                            $sort_liab_array[] = $liability_results[$i]->parent_id;
                            $sort_liab_order = true;
                        }
                    }
                    if ($sort_liab_order) {
                        ?>
                        <tr >
                            <?php if ($sort_liab_order) { ?>
                            <td style="width:2%">
                                <button type="button" onclick="showDetailModal(this,'<?= $liability_results[$i]->parent_id; ?>','<?= $liability_results[$i]->parent_name?>')" style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info"></i></button>
                            </td>
                                <td style="cursor: pointer" {{$show_sub}}  ><strong>{{$liability_results[$i]->parent_name??''}}</strong></td>
                                <td style="cursor: pointer" ></td>
                                <td style="cursor: pointer;text-align:right"><b>{{$liability_results[$i]->amount??''}}</b></td>
                                <?php
                                $liab_total += $liability_results[$i]->amount;
                            }
                            ?>
                        </tr>
                        <?php
                        $sort_liab_order = false;
                    }
                    ?>
                <?php }
                if(isset($gross_profit)){ ?>
                        <tr>    
                                <td style="width:2%"></td>
                                <td style="cursor: pointer;width:80%" ><b>Profit & Loss A/c</b></td>
                                <td style="cursor: pointer" ></td>
                                <td style="cursor: pointer;text-align:right"><b>{{$gross_profit??''}}</b></td>
                        </tr>
               <?php $liab_total += $gross_profit; }
                ?>
                     
            </table>
       
  
        <table style="width: 50%;border:thin solid black;float: left;margin-left: -1px;border-right: none;border-bottom: none" id="asset_table">
                <tr>
                    <td   style="border-bottom: thin solid black;" colspan="4"><b>Assets</b>
                        <b style="float: right">{{$from_date}} to {{$to_date}}</b></td>
                </tr>
                <?php
                for ($i = 0; $i < sizeof($asset_results); $i++) {
                        $show_sub = "onclick=expandRow(this," . $asset_results[$i]->parent_id . ")";
                    
                    if (isset($asset_results[$i])) {
                        if (!in_array($asset_results[$i]->parent_id, $sort_asset_array)) {
                            $sort_asset_array[] = $asset_results[$i]->parent_id;
                            $sort_asset_order = true;
                        }
                    }
                    if ($sort_asset_order) {
                        ?>
                        <tr >
                            <?php if ($sort_asset_order) { ?>
                            <td style="width:2%" >
                                <button type="button" onclick="showDetailModal(this,'<?= $asset_results[$i]->parent_id; ?>','<?= $asset_results[$i]->parent_name?>')" style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info" ></i></button>
                            </td>
                                <td style="cursor: pointer;width:80%" {{$show_sub}}><b>{{$asset_results[$i]->parent_name??''}}</b></td>
                                <td style="cursor: pointer" ></td>
                                <?php if($asset_results[$i]->parent_name=='Current Asset') {
                                    $asset_amnts = $asset_results[$i]->amount + $open_stock_results[0]->opening_stock;
                                }else{
                                    $asset_amnts = $asset_results[$i]->amount;
                                }?>
                                <td style="cursor: pointer;text-align:right">
                                    <b>{{$asset_amnts??''}}</b>
                                </td>
                                <?php
                                $asset_total += $asset_amnts;
                            }
                            ?>
                        </tr>
                        <?php
                        $sort_asset_order = false;
                    }
                    ?>
                <?php }if(isset($gross_loss)){ ?>
                        <tr>
                            <td style="width:2%"></td>
                                <td style="cursor: pointer;width:80%" ><b>Profit & Loss As/c</b></td>
                                <td style="cursor: pointer" ></td>
                                <td style="cursor: pointer;text-align:right"><b>{{$gross_loss??''}}</b></td>
                        </tr>
               <?php $asset_total += $gross_loss; }
                ?>
                
            </table>
            <div class="col-md-12" style="padding: 0">
                <table style="width: 50%;border:thin solid black;float: left;border-left: none;border-bottom: none" >
                    <tr>
                    <td colspan="2" style="cursor: pointer;border-top:thin solid black;padding: 0px;padding-top: 5px" ><b style="padding-left:5px">Total</b><hr><hr></td>
                     <td style="cursor: pointer;text-align:right;border-top:thin solid black;padding: 0px;padding-top: 5px"><b>{{$liab_total}}</b><hr><hr></td>
                     
                </tr>
                </table>
                <table style="width: 50%;border:thin solid black;float: left;margin-left: -1px;border-right: none;border-bottom: none" >
                    <tr>
                    <td colspan="2" style="cursor: pointer;border-top:thin solid black;padding: 0px;padding-top: 5px" ><b style="padding-left:5px">Total</b><hr><hr></td>
                     <td style="cursor: pointer;text-align:right;border-top:thin solid black;padding: 0px;padding-top: 5px"><b>{{$asset_total}}</b><hr><hr></td>
                     
                </tr>
                </table>
            </div>
        </div>
        </div>
    </div>
