<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        border: 1px solid #8e8282;;
        font-size: 10px;
    }
    td{
        padding:5px;
        width:10%; 
    }
    .td_head_cls {
        background-color: #c6d4e5;
        font-weight:bold;
    }
</style>
<?php $sort_liab_array = $sort_asset_array = array(); $asset_total = $liab_total = 0; ?>
<div class="row">
    <div class="col-md-12" id="result_container_div" border='2px !important' >
        <table id="result_data_table" class="" style="font-size:xx-small !important;font-family: 'robotoregular';">
            <tr>
                <td colspan="6"  style="border:thin solid black;"><b>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</b></td>


            </tr>
            <tr>
                <td colspan="6"  style="border:thin solid black;">(A Unit of Medical Centre Trichur Ltd)</td>


            </tr>
            <tr>
                <td colspan="6" style="border:thin solid black;">Near Viyyur Bridge,Shoranur Road</td>
 
            </tr>
            <tr>
                <td colspan="6" style="border:thin solid black;">Thrissur</td>


            </tr>
            <tr>
                <td colspan="6" style="border:thin solid black;"><b>Balance Sheet</b></td>

            </tr>
            <tr>
                <td colspan="6">{{$from_date}} to {{$to_date}}</td>

            </tr>
            <tr>
                <td  style="border:thin solid black;"></td>
                <td style="width:120px !important;border:thin solid black;"><strong>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</strong></td>
                <td style="border:thin solid black;width:120px !important;"></td>.
                <td style="border:thin solid black;" ></td>
                <td style="width:120px !important;border:thin solid black;"><strong>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</strong></td>
                <td style="border:thin solid black;width:120px !important;"></td>
            </tr>
            <tr>
                <td style="width:30%;border:thin solid black;"><b>Particulars</b></td>
                <td style="width:10%;border:thin solid black;">{{$from_date}} to {{$to_date}}</td>
                <td style="width:10%;border:thin solid black;" ></td>
                <td style="width:30%;border:thin solid black;"><b>Particulars</b></td>
                <td style="width:10%;border:thin solid black;">{{$from_date}} to {{$to_date}}</td>
                <td style="width:10%;border:thin solid black;"></td>
            </tr>
            <?php 
                for($i=0;$i<$total;$i++){
                    if(isset($liability_results[$i])){
                    if(!in_array($liability_results[$i]->parent_id,$sort_liab_array)){
                        $sort_liab_array[] = $liability_results[$i]->parent_id;
                        $sort_liab_order = true;
                    }
                    }
                    if(isset($asset_results[$i])){
                    if(!in_array($asset_results[$i]->parent_id,$sort_asset_array)){
                        $sort_asset_array[] = $asset_results[$i]->parent_id;
                        $sort_asset_order = true;
                    }
                    }
            if($sort_liab_order || $sort_asset_order){ ?>
            <tr <?php if($asset_results[$i]->parent_name=='Profit & Loss A/c') {?> onclick="showPlReport()" style="cursor:pointer" <?php } ?>>
                <?php if($sort_liab_order && !$sort_asset_order){ ?>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $liability_results[$i]->parent_id ?>)"><b>{{$liability_results[$i]->parent_name??''}}</b></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $liability_results[$i]->parent_id ?>)"></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $liability_results[$i]->parent_id ?>)" class="td_common_numeric_rules"><b>{{$liability_results[$i]->group_amount??''}}</b></td>
                <td style="border:thin solid black;">{{$asset_results[$i]->child_name}}</td>
                <td style="border:thin solid black;" class="td_common_numeric_rules">{{$asset_results[$i]->amount}}</td>
                <td style="border:thin solid black;"></td>
                <?php $liab_total += $liability_results[$i]->group_amount; }else if( !$sort_liab_order && $sort_asset_order){ ?>
                <td style="border:thin solid black;" >{{$liability_results[$i]->child_name??''}}</td>
                <td style="border:thin solid black;" ></td>
                <td style="border:thin solid black;" class="td_common_numeric_rules">{{$liability_results[$i]->amount??''}}</td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $asset_results[$i]->parent_id ?>)"><b>{{$asset_results[$i]->parent_name??''}}</b></td>
                <?php if($asset_results[$i]->parent_name=='Profit & Loss A/c') { ?>
                <td style="border:thin solid black;" class="td_common_numeric_rules"><b></b></td>
                <td style="border:thin solid black;" class="td_common_numeric_rules"><b>{{$prof_los??''}}</b></td>
                <?php $asset_total += $prof_los;}else{ ?>
                <td style="border:thin solid black;"></td>
                <td style="border:thin solid black;cursor: pointer" conclick="showGroupReport(<?= $asset_results[$i]->parent_id ?>)" lass="td_common_numeric_rules"><b>{{$asset_results[$i]->group_amount??''}}</b></td>
                <?php } ?>
                
               <?php $asset_total += $asset_results[$i]->group_amount; }else{ ?>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $liability_results[$i]->parent_id ?>)"><b>{{$liability_results[$i]->parent_name??''}}</b></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $liability_results[$i]->parent_id ?>)"></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $liability_results[$i]->parent_id ?>)" class="td_common_numeric_rules"><b>{{$liability_results[$i]->group_amount??''}}</b></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $asset_results[$i]->parent_id ?>)"><b>{{$asset_results[$i]->parent_name}}</b></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $asset_results[$i]->parent_id ?>)"></td>
                <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $asset_results[$i]->parent_id ?>)" class="td_common_numeric_rules"><b>{{$asset_results[$i]->group_amount}}</b></td>
                    <?php $asset_total += $asset_results[$i]->group_amount;
                          $liab_total += $liability_results[$i]->group_amount; } ?>
            </tr>
            <?php $sort_liab_order = $sort_asset_order = false;
            
            } ?>
            <tr>
                <td style="border:thin solid black;">{{$liability_results[$i]->child_name??''}}</td>
                <td style="border:thin solid black;" class="td_common_numeric_rules">{{$liability_results[$i]->amount??''}}</td>
                <td style="border:thin solid black;" ></td>
                <td style="border:thin solid black;">{{$asset_results[$i]->child_name}}</td>
                <td style="border:thin solid black;" class="td_common_numeric_rules">{{$asset_results[$i]->amount}}</td>
                <td style="border:thin solid black;"></td>
            </tr>
            <?php }
                 ?>
            <tr>
                <td style="border:thin solid black;"><b>Total</b></td><td style="border:thin solid black;"></td>
                <td style="border:thin solid black;"></td><td style="border:thin solid black;"></td>
                <td style="border:thin solid black;"></td><td style="border:thin solid black;"></td>
            </tr>
            <tr><td style="border:thin solid black;"></td><td style="border:thin solid black;"></td><td style="border:thin solid black;" class="td_common_numeric_rules"><b><?= $asset_total - $liab_total ?></b></td>
                <td style="border:thin solid black;"></td><td style="border:thin solid black;"></td><td style="border:thin solid black;"></td></tr>
            <tr><td style="border:thin solid black;"></td>
                <td style="border:thin solid black;" class="td_common_numeric_rules"><?= $asset_total - $liab_total ?></td>
                <td style="border:thin solid black;"></td><td style="border:thin solid black;"></td>
                <td style="border:thin solid black;"></td><td style="border:thin solid black;"></td></tr>
            <tr><td style="border:thin solid black;"></td><td style="border:thin solid black;"></td>
                <td style="border:thin solid black;" class="td_common_numeric_rules"><b><?= $asset_total ?></b></td>
                <td style="border:thin solid black;"><b>Total</b></td>
                <td style="border:thin solid black;"></td>
                <td style="border:thin solid black;" class="td_common_numeric_rules"><b><?= $asset_total ?></b></td></tr>
        </table>
    </div>
</div>