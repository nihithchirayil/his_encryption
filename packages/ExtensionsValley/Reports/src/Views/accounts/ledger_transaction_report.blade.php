@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>

  .box_border {
      padding-top: 25px;
      padding-bottom: 10px;
      border: 1px solid #ccc;
      background: #f3f3f3;
  }
  .tinymce-content p {
      padding: 0;
      margin: 2px 0;
  }

  .modal-header{
      background-color:orange;
  }

    #MTable > tbody > tr > td{
        word-wrap:break-word
      }
      table#posts {
        word-wrap: break-word;
    }
    .page{
        padding: 65px;
        min-height: 1134px;
        width: 812px;
    }



    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-md-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajamdearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        overflow-y: auto;
        width: 250px;
        z-index: 599;
        position:relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .ajamdearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }


    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }

    .filter_label {
       margin-bottom: 0px;
    }
    .btn-group {
        width:100% !important;
    }
    .col-md-12{
        margin-bottom:8px;
    }
    .col-md-6{
        margin-bottom:8px;
    }
    .filter_label {
        color: #294236 !important;
        font-weight: 600 !important;
    }

    @media print{
        .headerclass{
            background:#999;
        }
        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 1px #aaa;
        }
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="current_date" value="{{date('M-d-Y')}}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<?php
function calculateFiscalYearForDate($month)
{
if($month > 4)
{
$y = date('Y');
$pt = date('Y', strtotime('+1 year'));
$fy = "Apr-01-".$y.":"."Mar-31-".$pt;
}
else
{
$y = date('Y', strtotime('-1 year'));
$pt = date('Y');
$fy = "Apr-01-".$y.":"."Mar-31-".$pt;
}
return $fy;
}
$curr_date_month = date('m');
$calculate_fiscal_year_for_date = calculateFiscalYearForDate($curr_date_month);
$finanaceyear = explode(':',$calculate_fiscal_year_for_date);
$from = $finanaceyear[0];
$to =  $finanaceyear[1];
?>
<div class="right_col">
    <div class="container-fluid">


        <div class="col-md-3 no-padding">
            <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                <div class="box no-border no-margin">
                    <div class="box-body" style="padding-bottom:15px;">
                        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                            <thead><tr class="table_header_bg">
                                    <th colspan="11">Trail Balance

                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                                {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                                <div>

                                    <?php
                                    $i = 1;

                                    foreach ($showSearchFields as $fieldType => $fieldName) {
                                        $fieldTypeArray[$i] = explode('-', $fieldType);

                                   if ($fieldTypeArray[$i][0] == 'checkbox') {
                                       if($fieldTypeArray[$i][1]=='show_naration'){
                                           $chekd ='';
                                       }else{
                                            $chekd ='checked="';
                                       }
                                    ?>
                                    <div class="col-md-5 padding_sm" style="margin-top: 9px;padding-left:10px !important;">
                                        <div class="checkbox checkbox-success inline no-margin">
                                            <input type="checkbox" name="{{$fieldTypeArray[$i][1]}}"  id="{{$fieldTypeArray[$i][1]}}" <?= $chekd; ?> value="">
                                             <label class="text-blue" for="{{$fieldTypeArray[$i][1]}}">{{$fieldTypeArray[$i][2]}}</label>
                                        </div></div>
                                    <?php
                                }else if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                            $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                            ?>

                                            <div class= "col-xs-12">
                                                <div class="mate-input-box">

                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                                <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                                <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox" style="max-height:475px !important;width:100%;"></div>
                                                <input class="hidden" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                            </div></div>

                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'text') {
                                            ?>
                                            <div class="col-xs-12">
                                                <div class="mate-input-box">

                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                                <input  type="text" name="{{$fieldTypeArray[$i][1]}}" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> >
                                                </div> </div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'date') {
                                            ?>
                                            <div class="col-xs-6 date_filter_div">
                                                <div class="mate-input-box">

                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}} From</label>
                                                <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>  value="{{$from}}" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_from">

                                            </div></div>
                                            <div class="col-xs-6 date_filter_div">
                                                <div class="mate-input-box">

                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}} To</label>
                                                <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>  value="{{$to}}" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_to">

                                            </div></div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'combo') {
                                            ?>
                                            <div class="col-xs-12 date_filter_div">
                                                <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters ','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            </div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                            ?>
                                            <div class="col-xs-12 date_filter_div">
                                                <label style="width: 100%;"class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters multiple_selectbox','multiple'=>'multiple','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            </div>
                                            <?php
                                        }
                                        $i++;
                                    }
                                    ?>

                                    <div class="col-xs-12" style="text-align:right; padding: 10px;">
                                        <div class="clearfix"></div>

                                        <button type="reset" class="btn light_purple_bg" onclick="search_clear();"  name="clear_results" id="clear_results">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            Reset
                                        </button>
                                        <a class="btn light_purple_bg disabled"   name="csv_results" id="csv_results">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            Excel
                                        </a>
                                        <a class="btn light_purple_bg disabled" onclick="printReportData();"  name="print_results" id="print_results">
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                            Print
                                        </a>
                                        <a class="btn light_purple_bg" onclick="getTransferDataF();"  name="search_results" id="search_results">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            Search
                                        </a>

                                    </div>
                                    {!! Form::token() !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <div class="col-md-9 no-padding">
            <div class="col-md-12 padding_sm">
                <div class="theadscroll always-visible" id="ResultDataContainer" style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                    <div style="background:#686666;padding-top:30px;padding-bottom:30px;">
                        <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);max-width: 90%;
                              width: 100%;
                              padding: 30px;" id="ResultsViewArea">

                        </page>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!----------- DRILL DOWN ---------------->
<div class="modal fade" id="getledgerdataModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 700px">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            <h4 class="modal-title" id="leder_headerdata">Ledger Detail Data</h4>
            </div>

            <div class="modal-body" style="min-height:400px;height:500px">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getledgerdataModalDiv">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getledgerdataModal_1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 700px">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            <h4 class="modal-title" id="leder_headerdata_1">Ledger Detail Data</h4>
            </div>

            <div class="modal-body" style="min-height:400px;height:500px">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getledgerdataModalDiv_1">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getledgerdataModal_2" role="dialog" aria-labelledby="myModalLabel_2" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 700px">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            <h4 class="modal-title" id="leder_headerdata_2">Ledger Detail Data</h4>
            </div>

            <div class="modal-body" style="min-height:400px;height:500px">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getledgerdataModalDiv_2">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getLeaderDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="leder_name_header">Ledger Detail Data</h4>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getLeaderDetailData">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editLeaderDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="editleder_name_header">Ledger Detail Data</h4>
            </div>
            <div class="modal-body" style="min-height:500px">
                <div class="row padding_sm">
                <div class="theadscroll always-visible" id="ResultDataContainer" style="min-height: 400px;">
                    <div class='col-md-12 padding_sm' id="editLeaderDetailData">

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate_exclude_last_column('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/reports/default/javascript/custom_report.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/table2excel.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/jspdf.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/autojspdf.min.js")}}"></script>
<script type="text/javascript">
var arr = new Array();
function getTransferDataF(){
arr.pop();
getTransferData();
}
$("#csv_results").click(function(){
    $("#result_data_table").remove(".noExl").table2excel({
      exclude:".noExl",
      name:"Worksheet Name",
      filename:"Balance_sheet",//do not include extension
      fileext:".xls" // file extension
});
});
function removeRow(e,dr,cr,typ,grp_id,class_name){
    if(typ==1){
var classList = class_name.split(/\s+/);
for (var i = 0; i < (classList.length)-1; i++) {
    var total_debit = $("#dr_amount_"+classList[i]).html() ? $("#dr_amount_"+classList[i]).html():0 ;
    var total_credit = $("#cr_amount_"+classList[i]).html() ?$("#cr_amount_"+classList[i]).html():0;
    var upd_total_debit = total_debit-dr;
    var upd_total_credit = total_credit-cr;
    upd_total_debit = upd_total_debit.toFixed(2);
    upd_total_credit = upd_total_credit.toFixed(2);
    $("#dr_amount_"+classList[i]).html(upd_total_debit);
     $("#cr_amount_"+classList[i]).html(upd_total_credit);
    }
 $(e).closest('tr').remove();
    var total_debit1 = $("#debit_total").html();
    var total_credit1 = $("#credit_total").html();
    var drr =parseFloat(total_debit1)-parseFloat(dr);
    var crr =parseFloat(total_credit1)-parseFloat(cr);
    $("#debit_total").html(drr);
    $("#credit_total").html(crr);
    }else{
    var total_debit = $("#debit_total").html();
    var total_credit = $("#credit_total").html();
    var upd_total_debit = total_debit-dr;
    var upd_total_credit = total_credit-cr;
    upd_total_debit = upd_total_debit.toFixed(2);
    upd_total_credit = upd_total_credit.toFixed(2);
    $("#debit_total").html(upd_total_debit);
    $("#credit_total").html(upd_total_credit);
    $(e).closest('tr').remove();
    }
}
function expandRow(e,id){
           var added=false;
$.map(arr, function(elementOfArray, indexInArray) {
 if (elementOfArray.id == id) {
   added = true;
   $("."+id).remove();
     arr = arr.filter(function( obj ) {
    return obj.id !== id;
});
   //arr.pop({id: id});
 }
});
if (!added) {
  arr.push({id: id});
    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        if (filters_id == 'bill_no_hidden') {
            filters_id = 'bill_no';
        }
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }
        if (filters_id == 'no_date') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_date';
            }else{
                filters_value = 0;
                filters_id = 'no_date';
            }
        }
        if (filters_id == 'no_opening') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_opening';
            }else{
                filters_value = 0;
                filters_id = 'no_opening';
            }
        }
        if (filters_id == 'show_naration') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'show_naration';
            }else{
                filters_value = 0;
                filters_id = 'show_naration';
            }
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
    if (id != '0') {
                filters_value_det = id;
                filters_id_det = 'group_id';
            }
    filters_list[filters_id_det] = filters_value_det;
    filters_value_inc = 'include_cr_dr';
    filters_id_inc = 'include_cr_dr';
    filters_list[filters_id_inc] = filters_value_inc;
    $("#remove_group_id_"+id).show();
    $("#expand_group_id_"+id).hide();

    var url = $("#ins_base_url").val();
     $.ajax({
                type: "GET",
                url: url + "/reports/common_row_expand",
                data: filters_list,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                  $(e).closest('tr').after(data);
                $.LoadingOverlay("hide");
                $(".theadfix_wrapper").floatThead('reflow');
                },
                complete: function () {
                }
            });
}
}
function hideRow(e,id){
    $("#expand_group_id_"+id).show();
    $("#remove_group_id_"+id).hide();
    $(".child_"+id).remove();
}
function showPlReport(){
    var url = $("#ins_base_url").val();
    var to_date = ($("input[name=bill_date_to]").val());
    var from_date = ($("input[name=bill_date_from]").val());
    window.open(url+"/reports/expense_income_report?from_date ="+from_date+"&to_date ="+to_date+"&from_balance_sheet_open=1", '_blank');
}
$(document).on("click", function(event){
    var $trigger = $(".ajaxSearchBox");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".ajaxSearchBox").hide();
    }
});
function search_clear() {

  // var current_date = $('#current_date').val();

   $('#bill_date_from').val('');
   $('#bill_date_to').val('');
   $('.hidden_search').val('');
   $('.hidden').val('');
   $('input:text').val('');
   $('input:checkbox').removeAttr('checked');

}


//====================================DRILL DOWN==============================================
function showDetailModal(e,id,ledger_name){
    var first_td_text = $(e).closest('tr').find("td:eq(1)").text();

    var row_id = e.id;
    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        if (filters_id == 'bill_no_hidden') {
            filters_id = 'bill_no';
        }
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }
        if (filters_id == 'no_date') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_date';
            }else{
                filters_value = 0;
                filters_id = 'no_date';
            }
        }
        if (filters_id == 'no_opening') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_opening';
            }else{
                filters_value = 0;
                filters_id = 'no_opening';
            }
        }
        if (filters_id == 'show_naration') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'show_naration';
            }else{
                filters_value = 0;
                filters_id = 'show_naration';
            }
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
    if (id != '0') {
                filters_value_det = id;
                filters_id_det = 'group_id';
            }
    filters_list[filters_id_det] = filters_value_det;
    filters_value_inc = 'include_cr_dr';
    filters_id_inc = 'include_cr_dr';
    filters_list[filters_id_inc] = filters_value_inc;
    filters_list['show_detail_modal'] = 1;
    filters_list['first_td_text'] = first_td_text;
    $("#remove_group_id_"+id).show();
    $("#expand_group_id_"+id).hide();
    var url = $("#ins_base_url").val();
     $.ajax({
                type: "GET",
                url: url + "/reports/common_row_expand",
                data: filters_list,
                beforeSend: function () {
                    //$('#leder_headerdata').html(ledger_name);
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    if($('#getledgerdataModal').hasClass('in')){
                        if($('#getledgerdataModal_1').hasClass('in')){
                        $("#getledgerdataModal_2").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#leder_headerdata_2').html(ledger_name);
                        $('#getledgerdataModalDiv_2').html(data);
                    }else{
                        $("#getledgerdataModal_1").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#leder_headerdata_1').html(ledger_name);
                        $('#getledgerdataModalDiv_1').html(data);
                        }
                   }else{
                    $("#getledgerdataModal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#getledgerdataModalDiv').html(data);
                    $('#leder_headerdata').html(ledger_name);
                }

                    $.LoadingOverlay("hide");
                 setTimeout(function() {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
             }, 400);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                    });
                },
                complete: function () {
                }
            });
        }

function getLeaderDetails(ledger_name,ledger_id){

   var url = $("#ins_base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name}
     $.ajax({
                type: "GET",
                url: url + "/report/getLedgerdataReport",
                data: param,
                beforeSend: function () {
                    $('#leder_name_header').html(ledger_name);
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $("#getLeaderDetailModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#getLeaderDetailData').html(data);
                    $.LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                    });
                },
                complete: function () {
                }
            });
}
function editLedgerHeder(booking_id){
    var ledger_name= $('#ins_base_url').text();
    var url = $("#req_base_url").val();
    var urls =  url + "/master/edit_ledger_booking_entry/"+booking_id;
    window.open(urls, '_blank');
}
</script>
@endsection
