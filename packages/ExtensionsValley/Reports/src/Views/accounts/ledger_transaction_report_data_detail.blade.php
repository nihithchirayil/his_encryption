<?php
$padding_number_arr = explode(" ",$show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 8*$padding_number.'px';
$padding =  "padding-left:$padding_size";
$border_style = "";

if(isset($first_td_text) && ($first_td_text =='Opening Stock' || $first_td_text =='Closing Stock')){ ?>
<tr class="{{$show_row_group}}">
    <td></td>
    <td  style="padding-left:8px !important">Stock Of Medicine</td>
    <td style="text-align:right">{{$ledger_result[0]->stock_of_medicine}}</td>
</tr>
<tr class="{{$show_row_group}}">
    <td></td>
    <td style="padding-left:8px !important">Stock Of X-Ray</td>
    <td style="text-align:right">{{$ledger_result[0]->stock_of_xray}}</td>
</tr>
<?php }else{
    for ($i = 0; $i < sizeof($ledger_result); $i++) {

    if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;color:#ec2a2a' onclick=expandRow(this," . $ledger_result[$i]->id . ")";
    } else {
        $show_sub = "";
    }
    
        ?>

    <tr  <?= $show_sub ?> id="liab_{{$i}}" class="{{$show_row_group}}">
        <td></td>
        <td  style=" <?= $padding ?> " >
            @if($ledger_result[$i]->is_ledger == 0)
            <b >{{$ledger_result[$i]->ledger_name??''}}</b>
            @else
            {{$ledger_result[$i]->ledger_name??''}}
            @endif
        </td>
        <td  style=" <?= $border_style; ?> text-align:right" >
            {{$ledger_result[$i]->dr_amount}}
        </td>
        <td  style=" <?= $border_style; ?> text-align:right" >
            {{$ledger_result[$i]->cr_amount}}
        </td>
        
    </tr>

    <?php }
    }
    
     ?>