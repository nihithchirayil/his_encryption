<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        border: 1px solid #8e8282;;
        font-size: 10px;
    }
    td{
        padding:5px;
        width:10%; 
    }
    .td_head_cls {
        background-color: #c6d4e5;
        font-weight:bold;
    }
</style>
<?php $sort_liab_array = $sort_asset_array = array();
$total_credit = $total_debit = 0;
?>
<div class="row">
    <div class="col-md-12" id="result_container_div" border='2px !important' >
        <table id="result_data_table" class="" style="font-size:xx-small !important;font-family: 'robotoregular';">
            <tr>
                <td></td>
                <td style="border:thin solid black;" colspan="2">
                    <b>
                        Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)
                        <br><?= $from_date ?> - <?= $to_date ?>
                    </b>
                </td>


            </tr>

            <tr>
                <td></td>
                <td style="border:thin solid black;text-align: center" colspan="2" ><b>Closing Balance</b></td>

            </tr>
            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th  style="border:thin solid black;padding: 2px;width:25%;text-align: center;">Particulars</th>
                <th  style="border:thin solid black;padding: 2px;width:10%;text-align: center;">Debit</th>
                <th  style="border:thin solid black;padding: 2px;width:10%;text-align: center;">Credit</th>
            </tr>


            <?php
            foreach ($trail_results as $list) {
                if (($list->ledger_name != 'Profit & Loss A/c') && $list->ledger_name != 'Closing Stock' && $list->ledger_name != 'Opening Stock') {
                    ?>
                    <tr>
                        <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $list->parent_id ?>,<?= $list->is_asset ?>)">{{$list->ledger_name??''}}</td>
                        <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $list->parent_id ?>,<?= $list->is_asset ?>))" class="td_common_numeric_rules">
                            {{$list->dr_sum??''}} <?php $total_debit +=  $list->dr_sum ?></td>
                        <td style="border:thin solid black;cursor: pointer" onclick="showGroupReport(<?= $list->parent_id ?>,<?= $list->is_asset ?>))" class="td_common_numeric_rules">
                            {{$list->cr_sum??''}}<?php $total_credit +=  $list->cr_sum ?></td>
                    </tr>
                <?php }
            }
            ?>
                    <tr onclick="showPlReport()" style="cursor:pointer">
                        <td style="border:thin solid black;cursor: pointer">Profit & Loss A/c</td>
                        <td style="border:thin solid black;cursor: pointer" class="td_common_numeric_rules"><b></b></td>
                        <td style="border:thin solid black;cursor: pointer" class="td_common_numeric_rules"><b>{{$prof_los}}</b></td>
                    </tr>
                    <tr>
                        <td style="border:thin solid black;cursor: pointer"><b>Total</b></td>
                        <td style="border:thin solid black;cursor: pointer" class="td_common_numeric_rules"><b>{{$total_debit}}</b></td>
                        <td style="border:thin solid black;cursor: pointer" class="td_common_numeric_rules"><b>{{$total_credit}}</b></td>
                    </tr>      
        </table>
    </div>
</div>