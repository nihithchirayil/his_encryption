<?php
$padding_number_arr = explode(" ",$show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 8*$padding_number.'px';
$padding =  "padding-left:$padding_size !important";
$border_style = "border:thin solid black;$padding";

if($show_naration ==1)
    $naration_display="";
else
   $naration_display="display:none" ;
for ($i = 0; $i < sizeof($ledger_result); $i++) {

    if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;color:#ec2a2a' onclick=expandRow(this," . $ledger_result[$i]->id . ")";
    } else {
        $show_sub = "style= 'cursor:pointer;' onclick=getSingleLeaderDetails(this," .$ledger_result[$i]->id . ");";
    }
    if($ledger_result[$i]->dr_amount > 0 ||  $ledger_result[$i]->cr_amount > 0){
        ?>
        <tr <?= $show_sub ?> class="{{$show_row_group}}">
            <td style="<?= $border_style; ?>">&nbsp;</td>
            <td rowspan="" style="width:22%;<?= $border_style; ?>;text-align:left" class="" >{{$ledger_result[$i]->ledger_name}}</td>  
            <td rowspan="" style="width:10%;border:thin solid black" class="td_common_numeric_rules"  id="dr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->dr_amount, 2, '.', ',')}}</td>
            <td rowspan="" style="width:10%;border:thin solid black" class="td_common_numeric_rules" id="cr_amount_{{$ledger_result[$i]->id}}">{{number_format($ledger_result[$i]->cr_amount, 2, '.', ',')}}</td>
<!--            <td rowspan="" style="width:3%;cursor: pointer;border:thin solid black" class="noExl"  >
                <i class="fa fa-2x fa-minus-circle noExl" style="color:#f97d79;margin-left: 2px"  onclick="removeRow(this,{{$ledger_result[$i]->dr_amount??0}},{{$ledger_result[$i]->cr_amount??0}}, 1,{{$ledger_result[$i]->id}},'{{$show_row_group}}')"></i>
            </td>-->
        </tr>
    <?php }
    }
    
 ?>