<?php
$padding_number_arr = explode(" ",$show_row_group);
$padding_number = sizeof($padding_number_arr);
$padding_size = 8*$padding_number.'px';
$padding =  "padding-left:$padding_size";
$border_style = "$padding";?>
<style>
.thead_clas {
  display: table; /* to take the same width as tr */
  width: calc(100% - 17px); /* - 17px because of the scrollbar width */
}

.tbody_clas {
  display: block; /* to enable vertical scrolling */
  max-height: 400px; /* e.g. */
  overflow-y: scroll; /* keeps the scrollbar even if it doesn't need it; display purpose */
}


</style>
<table style="width:100% !important">
    <tbody class="tbody_clas">
     <?php
    if(isset($first_td_text) && ($first_td_text =='Opening Stock' || $first_td_text =='Closing Stock')){ ?>
<tr class="{{$show_row_group}}">
    <td></td>
    <td  style="padding-left:8px !important">Stock Of Medicine</td> <td style="width:10%"></td>
    <td style="text-align:right">{{$ledger_result[0]->stock_of_medicine}}</td>
</tr>
<tr class="{{$show_row_group}}">
    <td></td>
    <td style="padding-left:8px !important">Stock Of X-Ray</td><td style="width:10%"></td>
    <td style="text-align:right">{{$ledger_result[0]->stock_of_xray}}</td>
</tr>
</tbody>
<?php }else{ ?>
<thead class="thead_clas">
    <tr  style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;">
    <th></th><th style="text-align:center">Ledger Name</th><th style="text-align:center">Debit</th><th style="text-align:center">Credit</th>
</tr>
    </thead>
    <tbody class="tbody_clas">
   <?php
for ($i = 0; $i < sizeof($ledger_result); $i++) {

    if ($ledger_result[$i]->is_ledger == 0) {
        $show_sub = "style= 'cursor:pointer;color:#ec2a2a' onclick=expandRow(this," . $ledger_result[$i]->id . ")";
    } else {
        $show_sub = "";
    }
    
        ?>
        <tr class="{{$show_row_group}}" >
            @if($ledger_result[$i]->is_ledger == 0) 
            <td style="width:2%;padding-left: 5px"><button type="button" onclick="showDetailModal(this,'<?= $ledger_result[$i]->id; ?>','<?=$ledger_result[$i]->ledger_name?>')" style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info" id="infodatapopup<?=$i?>"></i></button></td>
            <td rowspan="" style="width:22%;cursor:pointer;color: red;<?= $border_style; ?>"  >{{$ledger_result[$i]->ledger_name}}</td>  
            @else
            @if($ledger_result[$i]->ledger_name=='Opening Stock')
            <td style="width:2%;padding-left: 5px"></td>
             <td rowspan="" style="width:22%;cursor:pointer;<?= $border_style; ?>" >{{$ledger_result[$i]->ledger_name}}</td>  
             @else
            <td style="width:2%;padding-left: 5px"><button type="button"  onclick="getLeaderDetails('<?= $ledger_result[$i]->ledger_name ?>',<?= $ledger_result[$i]->id ?>)"  style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info" id="infodatapopup<?=$i?>"></i></button></td>
             <td rowspan="" style="width:22%;cursor:pointer;<?= $border_style; ?>" >{{$ledger_result[$i]->ledger_name}}</td>  
             @endif
            @endif
             @if($ledger_result[$i]->ledger_name=='Opening Stock')
             @php $dr_am = $ledger_result[$i]->dr_amount + $open_stock_results[0]->opening_stock; @endphp
             @else
             @php $dr_am = $ledger_result[$i]->dr_amount; @endphp
             @endif
            <td rowspan="" style="width:10%;text-align: right;<?= $border_style; ?>"  id="dr_amount_{{$ledger_result[$i]->id}}">{{$dr_am}}</td>
            <td rowspan="" style="width:10%;text-align: right;<?= $border_style; ?>" id="cr_amount_{{$ledger_result[$i]->id}}">{{$ledger_result[$i]->cr_amount}}</td>
            
        </tr>
    <?php } 
} ?></tbody>
</table>

