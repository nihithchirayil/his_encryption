<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        border: 2px  #8e8282;
        font-size: 10px;
    }
    td{
        padding:5px;
        width:10%; 
    }
    .td_head_cls {
        background-color: #c6d4e5;
        font-weight:bold;
    }
   
</style>
<!-- 
GROSS PROFIT = (SALES A/C + CLOSING STOCK) - (OPENING STOCK + PURCHASE A/C)
NET PROFIT = (GROSS PROFIT + INDIRECT INCOME - INDIRECT EXPENSE)
TOTAL =  (GROSS PROFIT + INDIRECT INCOME)
-->
<?php $sort_liab_array = $sort_asset_array = array(); 
$asset_total = $liab_total = $gross_profit = $opening_group = $purchase_group =$closing_group= $sales_group =$indirect_expense= $indirect_income = $show_net_profit=0; ?>
<div class="row">
    <div class="col-md-12" id="result_container_div" border='2px !important' >
        <table id="result_data_table" class="" style="font-size:xx-small !important;font-family: 'robotoregular';">
            <tr style="border:thin solid black;">
                <td colspan="6" style="border:thin solid black;"><b>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</b></td>


            </tr>
            <tr style="border:thin solid black;">
                <td colspan="6" style="border:thin solid black;">(A Unit of Medical Centre Trichur Ltd)</td>


            </tr>
            <tr style="border:thin solid black;">
                <td colspan="6" style="border:thin solid black;">Near Viyyur Bridge,Shoranur Road</td>

            </tr>
            <tr style="border:thin solid black;">
                <td colspan="6" style="border:thin solid black;">Thrissur</td>


            </tr>
            <tr style="border:thin solid black;">
                <td colspan="6" style="border:thin solid black;"><b>Profit & Loss A/c</b></td>

            </tr>
            <tr style="border:thin solid black;">
                <td colspan="6" style="border:thin solid black;">{{$from_date}} to {{$to_date}}</td>

            </tr>
            <tr>
                <td style="border:thin solid black;"></td>
                <td style="width:120px !important;border:thin solid black;"><strong>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</strong></td>
                <td style="border:thin solid black;width:120px !important;"></td>.
                <td style="border:thin solid black;"></td>
                <td style="width:120px !important;border:thin solid black;"><strong>Daya General Hospital Limited- (<?php echo date('Y', strtotime($from_date)) . '-' . date('Y', strtotime($to_date)) ?>)</strong></td>
                <td style="border:thin solid black;width:120px !important;"></td>
            </tr>
            <tr>
                <td style="width:30%;border:thin solid black;"><b>Particulars</b></td>
                <td style="width:10%;border:thin solid black;">{{$from_date}} to {{$to_date}}</td>
                <td style="width:10%;border:thin solid black;"></td>
                <td style="width:30%;border:thin solid black;"><b>Particulars</b></td>
                <td style="width:10%;border:thin solid black;">{{$from_date}} to {{$to_date}}</td>
                <td style="width:10%;border:thin solid black;"></td>
            </tr>
            <?php $j= $showDirectIncome = $show_net_prof=0;

                
// =============================================== ASSET ARRAY REARRANGE ==============================
                $openIndex = array_search('Opening Stock', array_column($liability_results, 'ledger_name'));
                //$open_stock1 = $liability_results[$openIndex]->amount ?$liability_results[$openIndex]->amount:0 ;
                $open_stock = $open_stock_results[0]->closing_stock ? $open_stock_results[0]->closing_stock:0;
                
                $purchaseIndex = array_search('Purchase Accounts', array_column($liability_results, 'ledger_name'));
                $purchasestock = $liability_results[$purchaseIndex]->amount ? $liability_results[$purchaseIndex]->amount:0 ;
                
                $indExpIndex = array_search('Indirect Expenses', array_column($liability_results, 'ledger_name'));
                $indexpstock = $liability_results[$indExpIndex]->amount ? $liability_results[$indExpIndex]->amount:0 ;
                
                $salesIndex = array_search('Sales Accounts', array_column($asset_results, 'ledger_name'));
                $salesstock = $asset_results[$salesIndex]->amount ? $asset_results[$salesIndex]->amount:0;
                
                $afterIndex = array_search('Closing Stock', array_column($asset_results, 'ledger_name'));
                $closingstock = $asset_results[$afterIndex]->amount? $asset_results[$afterIndex]->amount:0;
                //$closingstock = $open_stock_results[0]->closing_stock ? $open_stock_results[0]->closing_stock:0;
                $before_indirect_income = $salesstock+$closingstock;
                $gross_profit = $before_indirect_income - ($purchasestock+$open_stock);
                
                $newVal= array((object)array('typ' => 2,'id' => '-1','ledger_name' => 'Direct Income','display_order' => 3,'amount' =>'' ));
                $asset_results = array_merge(array_slice($asset_results,0,$afterIndex), $newVal,array_slice($asset_results,$afterIndex));
                
                $afterIndex1 = array_search('Indirect Incomes', array_column($asset_results, 'ledger_name'));
                $indirectstock = $asset_results[$afterIndex1]->amount ? $asset_results[$afterIndex1]->amount:0;
                
                $newVal1= array((object)array('typ' => 2,'id' => '-2','ledger_name' => '','display_order' => 3,'amount' => $before_indirect_income ),
                               (object)array('typ' => 2,'id' => '-3','ledger_name' => 'Gross Profit b/f','display_order' => 3,'amount' => $gross_profit ));
                $asset_results = array_merge(array_slice($asset_results,0,$afterIndex1), $newVal1,array_slice($asset_results,$afterIndex1));
// ====================================================================================================================================================
 //=============================================== LIABILITY ARRAY REARANGE ================================
                $afterIndex_liab = array_search('Indirect Expenses', array_column($liability_results, 'ledger_name'));
                $newVal_liab= array((object)array('typ' => 1,'id' => '-6','ledger_name' => 'Gross Profit c/o','display_order' => 3,'amount' => $gross_profit ),
                               (object)array('typ' => 1,'id' => '-3','ledger_name' => '','display_order' => 3,'amount' => $before_indirect_income ));
                $liability_results = array_merge(array_slice($liability_results,0,$afterIndex_liab), $newVal_liab,array_slice($liability_results,$afterIndex_liab));
      //=====================================================================================================          
                $newVal= array();
                for($i=0;$i<sizeof($asset_results);$i++){
                    
                    if(isset($liability_results[$i])){
                    if(!in_array($liability_results[$i]->id,$sort_liab_array)){
                        $sort_liab_array[] = $liability_results[$i]->id;
                        $sort_liab_order = true;
                    }
                    }
                    if(isset($asset_results[$i])){
                    if(!in_array($asset_results[$i]->id,$sort_asset_array)){
                        $sort_asset_array[] = $asset_results[$i]->id;
                        $sort_asset_order = true;
                    }
                    } ?>

            <tr>
                <?php if($sort_liab_order){ ?>
                <td  style=" border:thin solid black;cursor: pointer" onclick="showGroup(<?= $liability_results[$i]->id; ?>)"><b>{{$liability_results[$i]->ledger_name??''}}</b></td>
                <?php if($liability_results[$i]->ledger_name == '') { ?>
                <td  style=" border:thin solid black;cursor: pointer" class="td_common_numeric_rules" onclick="showGroup(<?= $liability_results[$i]->id; ?>)"><b><?php echo $liability_results[$i]->amount; ?></b></td>
                <td></td>
                <?php }else{ ?>
                <td></td>
                <td  style=" border:thin solid black;cursor: pointer" class="td_common_numeric_rules" onclick="showGroup(<?= $liability_results[$i]->id; ?>)"><b>
                    <?php if($liability_results[$i]->ledger_name != '' && $liability_results[$i]->ledger_name=='Opening Stock' )
                        { 
                        echo $open_stock; 
                        }else{
                            echo $liability_results[$i]->amount;
                        }?>
                    </b></td> 
                <?php }}else{
                     if(isset($liability_results[$i]->id)){ ?>
                <td  style=" border:thin solid black;">{{$liability_results[$i]->ledger_name??''}}</td>
                <td  style=" border:thin solid black;" class="td_common_numeric_rules">{{$liability_results[$i]->amount??''}}</td>
                <td  style=" border:thin solid black;"></td>
                     <?php }else if($show_net_prof == 0){ ?>
                <td  style=" border:thin solid black;"><b>Net Profit</b></td>
                <td  style=" border:thin solid black;"></td>
                <td  style=" border:thin solid black;" class="td_common_numeric_rules"><b><?= $gross_profit + $indirectstock - $indexpstock;?></b></td>
                    <?php $show_net_prof =1;}else{ ?>
                <td  style=" border:thin solid black;"></td>
                <td  style=" border:thin solid black;"></td>
                <td  style=" border:thin solid black;"></td>                   
                <?php } 
                 } 
                if($sort_asset_order){ ?>
                <td  style=" border:thin solid black;cursor: pointer" onclick="showGroup(<?= $asset_results[$i]->id; ?>)"><b>{{$asset_results[$i]->ledger_name??''}}</b></td>
                <?php if($asset_results[$i]->ledger_name == '') { ?>
                <td  style=" border:thin solid black;cursor: pointer" class="td_common_numeric_rules" onclick="showGroup(<?= $asset_results[$i]->id; ?>)"><b><?php  echo $asset_results[$i]->amount; ?></b></td>
                <td  style=" border:thin solid black;cursor: pointer" class="td_common_numeric_rules" onclick="showGroup(<?= $asset_results[$i]->id; ?>)"></td>
                <?php }else{ ?>
                <td  style=" border:thin solid black;cursor: pointer" class="td_common_numeric_rules" onclick="showGroup(<?= $asset_results[$i]->id; ?>)"></td>
                <td  style=" border:thin solid black;cursor: pointer" class="td_common_numeric_rules" onclick="showGroup(<?= $asset_results[$i]->id; ?>)"><b>
                    <?php if($asset_results[$i]->ledger_name != '' && $asset_results[$i]->ledger_name =='Closing Stock' ) 
                        {  echo $closingstock; }else{ 
                            echo $asset_results[$i]->amount;
                        } ?>
                    </b></td> 
                <?php }}else{ ?>
                <td  style=" border:thin solid black;">{{$asset_results[$i]->ledger_name??''}}</td>
                <td  style=" border:thin solid black;" class="td_common_numeric_rules">{{$asset_results[$i]->amount??''}}</td>
                <td  style=" border:thin solid black;"></td>
                <?php } ?>
            </tr>
            
            <?php  $sort_liab_order = $sort_asset_order = false; 
            
                }
                 ?>
            <tr>
                <td style=" border:thin solid black;"><b>Total</b></td><td  style=" border:thin solid black;"></td><td style=" border:thin solid black;" class="td_common_numeric_rules"><b><?= $gross_profit + ($indirectstock) ?></b></td>
                <td style=" border:thin solid black;"><b>Total</b></td><td  style=" border:thin solid black;"></td><td style=" border:thin solid black;" class="td_common_numeric_rules"><b><?= $gross_profit + ($indirectstock) ?></b></td>
            </tr>
        </table>
    </div>
</div>