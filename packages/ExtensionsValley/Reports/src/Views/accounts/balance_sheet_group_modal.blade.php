<div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center">{{$group_results[0]->parent_name??''}}-{{$group_results[0]->group_amount??''}}</h4>
                </div>
<div class="modal-body" >
<div class="row">
    <div class="clearfix"></div>
    <div class="h10"></div>
    <div class="col-md-12" id="consolidate" style="margin-top: -15px">
        <div class="table-responsive theadscroll"  style="height: 360px;">
            <table class="table table_round_border styled-table">
                <thead>
                    <tr class="table_header_bg">
                        <th style="text-align:center;width: 20%">Particulars</th>
                        <th style="text-align:center;">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($group_results))
                    <?php for($i=0;$i< sizeof($group_results);$i++){ ?>
                    <tr>
                        <td class="common_td_rules">{{ $group_results[$i]->child_name }}</td>
                        <td class="td_common_numeric_rules">{{ !empty($group_results[$i]->amount) ? $group_results[$i]->amount:'' }}</td>
                    </tr>
                    <?php } ?>
                    @else
                    <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                    @endif                        
                </tbody>
            </table>
        </div>
        
    </div>
    

</div>
 </div>