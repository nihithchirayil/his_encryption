<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        /*        border: thin solid black;*/
        font-size: 12px;
        font-family:robotoregular !important;

    }
</style>
<?php
if ($show_naration == 1)
    $naration_display = "";
else
    $naration_display = "display:none";
?>
<div class="row">
    <div class="col-md-12" id="result_container_div">
        @if(sizeof($ledger_group_result)>0)
        @php $grand_debit_total = $grand_credit_total = 0; @endphp
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Trail Balance Report </b></h4>
        <font size="16px" face="verdana" >
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
            <thead>
                <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th  style="padding: 2px;width:2%;text-align: center;border-right:0px !important; "></th>
                    <th  style="padding: 2px;width:22%;text-align: center;border-right:0px !important; ">Particulars</th>
                    <th  style="padding: 2px;width:10%;text-align: center;border-right:0px !important;">Debit</th>
                    <th  style="padding: 2px;width:10%;text-align: center;border-right:0px !important;">Credit</th>
                    <th  style="padding: 2px;width:3%;text-align: center;border-right:0px !important;" class="noExl"></th>

                </tr>
            </thead>
            @foreach ($ledger_group_result as $data)


            <tr id="{{$data->parent_id}}" style="cursor:pointer;border: thin solid black;" >
                <td style="width:2%;text-align: center;border:thin solid black;width:2%">
                    <button type="button" onclick="showDetailModal(this, '<?= $data->parent_id; ?>', '<?= $data->parent_name ?>')" style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info"></i></button>
                </td>
                <td class="common_td_rules" onclick="expandRow(this,{{$data->parent_id}})" style="width:22%">{{$data->parent_name}}</td>
                @if($data->parent_name =='Current Asset')
                @php $dr_amnt = $data->dr_amount + $open_stock_results[0]->opening_stock; @endphp
                @else
                 @php $dr_amnt = $data->dr_amount @endphp
                 @endif
                <td class="td_common_numeric_rules" style="font-weight: bold;width:10%" id="dr_amount_{{$data->parent_id}}">{{$dr_amnt}}</td>
                
                <td class="td_common_numeric_rules" style="font-weight: bold;width:10%" id="cr_amount_{{$data->parent_id}}">{{$data->cr_amount}}</td>
                <td class="noExl" style="width:3%">
                    <i class="fa fa-2x fa-minus-circle noExl" style="color:#f97d79;margin-left: 2px"  onclick="removeRow(this,{{$data->dr_amount??0}},{{$data->cr_amount??0}}, 0,{{$data->parent_id}}, 0)"></i>
                </td>
            </tr>
            <?php $grand_credit_total += $data->cr_amount;
            $grand_debit_total += $data->dr_amount ?>
            @endforeach
            <tr style="cursor:pointer;border: thin solid black;" onclick="showPlReport()">
                <td></td>
                <td class="common_td_rules" >Profit & Loss A/c</td>  
                <td class="td_common_numeric_rules" style="font-weight: bold" id="dr_amount_{{$data->parent_id}}">{{$prof_los??''}}</td>
                <td class="td_common_numeric_rules" style="font-weight: bold" id="cr_amount_{{$data->parent_id}}"></td>
                <td class="noExl"  >
                </td>
            </tr>
            <tr style="background-color: #dedc91;border: thin solid black;"> 
                <td>&nbsp;</td>
                <td colspan='1' class="common_td_rules" style="font-weight: bold" ><strong> Total</strong></td> 
                <td class="td_common_numeric_rules" id="debit_total" style="font-weight: bold" >
                    <?php $opn_sum = $open_stock_results[0]->opening_stock ? $open_stock_results[0]->opening_stock: 0 ; ?>
                    {{$grand_debit_total+$opn_sum}} </td>  
                <td class="td_common_numeric_rules" id="credit_total" style="font-weight: bold" >
                    {{$grand_credit_total }}  </td>
                <td class="common_td_rules">&nbsp;</td>
            </tr>
            @else 
            <h1 >No Data Found</h1>
            @endif  
        </table>
        </font>
    </div>
</div>

