<div class="row">

           <table id="result_data_table" class='table table-condensed table_sm table-col-bordered for_search_ledger' style="font-size: 12px;">
                <thead>
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;">
                        <th style="width:2%"></th>
                        <th style="width:10%">Date</th>
                        <th style="width:38%">Particular</th>
                        <th style="width:10%">Voucher Type</th>
                        <th style="width:10%"> Voucher No.</th>
                        <th style="width:10%">Debit</th>
                        <th style="width:10%">Credit</th>

                    </tr>
                </thead>
                <?php
                $credit_tot = $debits_total = 0.0;
                $debit_tot = $debits_total = 0.0;
                $head_id='';
                if (count($res) != 0) {
                    ?>
                    <tbody>
                        <?php
                        foreach ($res as $data) {
                            
                            if ($head_id != $data->head_id) {
                                $head_id = $data->head_id; ?>
                        <tr>
                            <td style="border-left: 1px solid #CCC !important;width:2%"><button type="button" onclick="editLedgerHeder('<?= $data->head_id; ?>')" style="padding: 1px 1px" class="btn btn-prmary">
                        <i class="fa fa-info"></i></button></td>
                            <td style="text-align: left;width:10%">{{$data->created_date}}</td>
                            <td style="text-align: left;width:38%" onclick="narationRow(this,'<?= $head_id ?>')">{{$data->ledger_name}}</td>
                            <td style="text-align: left;width:10%">{{$data->voucher_type}}</td>
                            <td style="text-align: left;width:10%">{{$data->voucher_no}}</td>
                            <td style="text-align: right;width:10%">{{$data->debit_amount}}</td>
                            <td style="text-align: right;width:10%">{{$data->credit_amount}}</td>
                        </tr>
                            <?php

                            }
                            
                        }
                        $credit_tot = floatval($res[0]->grp_credit);
                        $debit_tot = floatval($res[0]->grp_debit);
                        if($debit_tot > $credit_tot){
                            $debits_total = $debit_tot-$credit_tot;
                            $is_debit_current = 1;
                        }else{
                            $debits_total = $credit_tot - $debit_tot;
                            $is_debit_current = 0;
                        }
                        $old_openings = isset($current_opening) ? $current_opening:0;
                        if($old_openings < 0){
                            $is_debit_old = 1;
                        }else{
                            $is_debit_old = 0;
                        }

                        ?>
                        <tr class="" style="height: 30px;border-top:1px solid black;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                            <td colspan="5">Opening Balance</td>
                            @if($old_openings > 0)
                            <td style="text-align: right"></td>
                            <td><b><?= abs($old_openings) ?></b></td>
                            @else
                            <td><?= abs($old_openings) ?></td>
                            <td style="text-align: right"></td>
                            @endif
                        </tr>
                         <tr class="" style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                             <td colspan="5">Current Balance</td>
                             <td style="text-align: right"><b><?= abs($debit_tot) ?></b></td>
                             <td style="text-align: right"><b><?= abs($credit_tot) ?></b></td>
                         </tr>
                        <?php 
                  
                        if($is_debit_current == 1 && $is_debit_old == 1){ ?>
                        <tr class="" style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                            <td colspan="5">Closing Balance</td>
                            <td style="text-align: right"><b><?= abs($old_openings + $debits_total); ?></b></td>
                            <td style="text-align: right"></td>
                        </tr>
                        <?php }else if($is_debit_current == 0 && $is_debit_old == 0){ ?>
                        <tr class="" style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                            <td colspan="5">Closing Balance</td>
                            <td style="text-align: right"></td>
                            <td style="text-align: right"><b><?= abs($old_openings + $debits_total); ?></b></td>
                        </tr>
                        <?php }else{ 
                              if($debits_total > $old_openings){  ?>
                        <tr class="" style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                            <td colspan="5">Closing Balance</td>
                            <td style="text-align: right"><b><?php
                            if($old_openings > 0){
                            echo abs($old_openings -$debits_total);
                            }else{
                             echo abs($old_openings + $debits_total);
                            }
                            ?></b></td>
                            <td style="text-align: right"></td>
                        </tr>
                              <?php }else{ ?>
                        <tr class="" style="height: 30px;border-left: 1px solid #CCC !important;border-bottom: 1px solid #CCC !important;">
                            <td colspan="5">Closing Balance</td>
                            <td style="text-align: right"></td>
                            <td style="text-align: right"><b><?php
                            if($old_openings > 0){
                            echo abs($old_openings -$debits_total);
                            }else{
                             abs($old_openings + $debits_total);
                            }
                            ?></b></td>
                        </tr>                                  
                             <?php } 
                        }
                         } else { ?>
                        <tr>
                            <td style="font-family:robotoregular" style="text-align: center;"></td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

</div>
