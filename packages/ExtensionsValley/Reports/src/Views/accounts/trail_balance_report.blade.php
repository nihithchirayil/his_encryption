@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/reports/default/css/reports-custom.css")}}" rel="stylesheet">
<style>

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<?php 
function calculateFiscalYearForDate($month)
{
if($month > 4)
{
$y = date('Y');
$pt = date('Y', strtotime('+1 year'));
$fy = "Apr-01-".$y.":"."Mar-31-".$pt;
}
else
{
$y = date('Y', strtotime('-1 year'));
$pt = date('Y');
$fy = "Apr-01-".$y.":"."Mar-31-".$pt;
}
return $fy;
}
$curr_date_month = date('m');
$calculate_fiscal_year_for_date = calculateFiscalYearForDate($curr_date_month);
$finanaceyear = explode(':',$calculate_fiscal_year_for_date);
$from = $finanaceyear[0];
$to =  $finanaceyear[1];
?>
<div class="right_col">
    <div class="container-fluid">


        <div class="col-md-3 no-padding">
            <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                <div class="box no-border no-margin">
                    <div class="box-body" style="padding-bottom:15px;">
                        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                            <thead><tr class="table_header_bg">
                                    <th colspan="11">Trail Balance

                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                                {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                                <div>
                                    
                                    <?php
                                    $i = 1;

                                    foreach ($showSearchFields as $fieldType => $fieldName) {
                                        $fieldTypeArray[$i] = explode('-', $fieldType);

                                        if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                            $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                            ?>

                                            <div class= "col-xs-12">
                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                                <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                                <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                                <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                            </div>

                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'text') {
                                            ?>
                                            <div class="col-xs-12">
                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                                <input type="text" name="{{$fieldTypeArray[$i][1]}}" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>  value="{{date('M-d-Y')}}" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> >
                                            </div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'date') {
                                            ?>
                                            <div class="col-xs-6 date_filter_div">
                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}} From</label>
                                                <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>  value="{{$from}}" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_from">

                                            </div>
                                            <div class="col-xs-6 date_filter_div">
                                                <label class="filter_label">{{$fieldTypeArray[$i][2]}} To</label>
                                                <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>  value="{{$to}}" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_to">

                                            </div>
                                            <?php
                                        }else if ($fieldTypeArray[$i][0] == 'checkbox') {
                                    ?>
                                    <div class="col-xs-12 date_filter_div">
                                        <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                        <input type="checkbox" name="{{$fieldTypeArray[$i][1]}}" class="filters" id="{{$fieldTypeArray[$i][1]}}" value="">
                                    </div>
                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                            ?>
                                            <div class="col-xs-12 date_filter_div">
                                                <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters ','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            </div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                            ?>
                                            <div class="col-xs-12 date_filter_div">
                                                <label style="width: 100%;"class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters multiple_selectbox','multiple'=>'multiple','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            </div>
                                            <?php
                                        }
                                        $i++;
                                    }
                                    ?>                                    
                                    <div class="col-xs-12" style="text-align:right; padding: 0px;">
                                        <div class="clearfix"></div>

                                        <button type="reset" class="btn light_purple_bg" onclick="search_clear();"  name="clear_results" id="clear_results">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            Reset
                                        </button>
                                        <a class="btn light_purple_bg disabled"   name="csv_results" id="csv_results">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            Excel
                                        </a>
                                        <a class="btn light_purple_bg disabled" onclick="printReportData();"  name="print_results" id="print_results">
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                            Print
                                        </a>
                                        <a class="btn light_purple_bg" onclick="getStockTransferData();"  name="search_results" id="search_results">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            Search
                                        </a>

                                    </div>
                                    {!! Form::token() !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <div class="col-md-9 no-padding">
            <div class="col-md-12 padding_sm">
                <div class="theadscroll always-visible" id="ResultDataContainer" style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                    <div style="background:#686666;padding-top:30px;padding-bottom:30px;">
                        <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);max-width: 90%;
                              width: 100%;
                              padding: 30px;" id="ResultsViewArea">

                        </page>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
<div id="modal_group_pl" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 40%;">

            <!-- Modal content-->
            <div class="modal-content" id="append_group_pl">
                

            </div>
        </div>
    </div>
@stop
@section('javascript_extra')controller

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/reports/default/javascript/custom_report.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/table2excel.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/jspdf.min.js")}}"></script>  
<script src="{{asset("packages/extensionsvalley/default/js/autojspdf.min.js")}}"></script>
<script type="text/javascript">
function getStockTransferData() {
    $('#ResultDataContainer').show();
    var url = route_json.trail_balance_report_data;

    //-------filters---------------------------------------------

    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        if (filters_id == 'bill_no_hidden') {
            filters_id = 'bill_no';
        }
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });

    $.ajax({
        type: "GET",
        url: url,
        data: filters_list,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}
$("#csv_results").click(function(){
    $("#result_data_table").table2excel({
      exclude:".noExl",
      name:"Worksheet Name",
      filename:"Trail_Balance_sheet",//do not include extension
      fileext:".xls" // file extension
    });
});
function showPlReport(){
    var url = $("#req_base_url").val();
    var to_date = ($("input[name=bill_date_to]").val());
    var from_date = ($("input[name=bill_date_from]").val());
    window.open(url+"/reports/expense_income_report?from_date ="+from_date+"&to_date ="+to_date+"&from_balance_sheet_open=1", '_blank');
}
function showGroupReport(id,is_asset){
        let urls = $('#req_base_url').val();
    var to_date = ($("input[name=bill_date_to]").val());
    var from_date = ($("input[name=bill_date_from]").val());
    if(is_asset==1){
       var type_url ="reports/show_balance_sheet_group_wise";
    }else{
        var type_url ="reports/show_profit_loss_group_wise";
    }
    var param = {from_date: from_date,to_date:to_date,searchGroupId:id}
    $.ajax({
        type: "GET",
        url: urls + "/"+type_url,
        data: param,
        beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
        },
        success: function (data) {
            $("#append_group_pl").html(data);
            $("#modal_group_pl").modal('show');
        },
        complete: function () {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        },
        error: function () {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

    });
}
</script>
@endsection
