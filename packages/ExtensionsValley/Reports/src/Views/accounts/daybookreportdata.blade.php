<div class="row">
 
   <div class="col-md-12" id="result_container_div">

   <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
<table id="result_data_table" class="theadfix_wrapper">
<thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th style="padding: 2px;width: 10%;">Date</th>
            <th style="padding: 2px;width: 10%;">Voucher No.</th>
            <th style="padding: 2px; width: 9%;">Voucher Type</th>
            <th style="padding: 2px; width: 9%;">Group Name</th>
            <th style="padding: 2px; width: 15%;">Ledger Name</th>
            <th style="padding: 2px; width: 35%;">Narration</th>
            <th style="padding: 2px; width: 8%;">Credit</th>
            <th style="padding: 2px; width: 8%;">Debit</th>
        </tr>
    </thead>
    <?php
    $credit_tot = 0.0;
    $debit_tot = 0.0;
    if(count($res)!=0){
    ?>
    <tbody>
        <?php foreach($res as $data){
    ?>
        <tr>
            <td class="common_td_rules">{{date('M-d-Y',strtotime($data->date))}}</td>
            <td class="common_td_rules">{{$data->voucher_no}}</td>
            <td class="common_td_rules">{{$data->voucher_type}}</td>
            <td class="common_td_rules">{{$data->group_name}}</td>
            <td class="common_td_rules">{{$data->ledger_name}}</td>
            <td class="common_td_rules">{{$data->narration}}</td>
            <td class="td_common_numeric_rules">{{$data->credit}}</td>
            <td class="td_common_numeric_rules">{{$data->debit}}</td>
                 
        </tr>
    <?php
            $credit_tot += floatval($data->credit);
            $debit_tot+= floatval($data->debit);
        }
        ?>
        <tr>
            <td class="common_td_rules" colspan="6" style="text-align: left;"> <strong>Total</strong></td>
            <td class="td_common_numeric_rules"><strong><?=$credit_tot?></strong></td>
            <td class="td_common_numeric_rules"><strong><?=$debit_tot?></strong></td>
        </tr>
        <?php
     } ?>
    </tbody>
  
</table>

</div>
</div>
</div>
