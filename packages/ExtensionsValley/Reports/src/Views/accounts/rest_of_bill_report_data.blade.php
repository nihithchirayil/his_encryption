<div class="row">
   <div class="col-md-12" id="result_container_div">
    <table id="result_data_table" class="" style="font-size: 12px;">
    @if(sizeof($res)>0)
    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;width: 180px;">Department</th>
        <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;width: 180px;">Item Name</th>
        <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;width: 180px;">Item Code</th>
        <th onclick="sortTable('result_data_table',6,1);" style="padding: 2px;width: 75px;">Total Bill Amount</th>
        <th onclick="sortTable('result_data_table',7,1);" style="padding: 2px;width: 75px;">Total Return Amount</th>
    </tr>
    @foreach ($res as $list)
        <tr>
            <td class="common_td_rules">{{ $list->department }}</td>
            <td class="common_td_rules">{{ $list->item_description }}</td>
            <td class="common_td_rules">{{ $list->item_code }}</td>
            <td class="common_td_rules">{{ $list->net_amount }}</td>
            <td class="common_td_rules">{{ $list->total_amount }}</td>

        </tr>
        
    @endforeach
    @else
    <tr>
        <td>No Results Found!</td>
    </tr>
    @endif

</table>
   </div>
</div>