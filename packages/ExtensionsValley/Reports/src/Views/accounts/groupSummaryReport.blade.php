@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

<style>
  .liHover{
     background: #48705f;
  }
  .box_border {
      padding-top: 25px;
      padding-bottom: 10px;
      border: 1px solid #ccc;
      background: #f3f3f3;
  }
  .tinymce-content p {
      padding: 0;
      margin: 2px 0;
  }
  .content_add:hover{
   color: red;
   background-color: yellow;
  }
  .content_remove:hover{
   color: red;
   background-color: yellow;
  }
  .modal-header{
      background-color:orange;
  }

  .mate_select_data {
        width: 100%;
        position: relative;
        padding: 2px 4px 14px 4px !important;
        border-bottom: 2px solid #01A881;
        box-shadow: 0 0 3px #ccc;
        border-radius: 6px 6px 0 0;
    }

    #MTable > tbody > tr > td{
        word-wrap:break-word
      }
      table#posts {
        word-wrap: break-word;
    }
    .page{
        padding: 65px;
        min-height: 1134px;
        width: 812px;
    }



    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-xs-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        overflow-y: auto;
        width: 250px;
        z-index: 599;
        position:relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .ajaxSearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }

    .liHover{
        background: #4c6456 !important;
        color:white;
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400;
        font-family: "sans-serif";
        border-bottom: 1px solid white;
    }
    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }
    .liHover {
        background: #8e948f;
        color: white;
    }
    .filter_label {
       margin-bottom: 0px;
    }
    .btn-group {
        width:100% !important;
    }
    .col-xs-12{
        margin-bottom:8px;
    }
    .col-xs-6{
        margin-bottom:8px;
    }
    .filter_label {
        color: #294236 !important;
        font-weight: 600 !important;
    }

    @media print{
        .headerclass{
            background:#999;
        }
        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 1px #aaa;
        }
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_headder" value="{{$hospital_headder}}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<div class="right_col">
<div class="container-fluid">


 <div class="col-md-12 padding_sm">
    <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
        <div class="box no-border no-margin">
            <div class="box-body" style="padding-bottom:15px;">
                <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                    <thead><tr class="" >
                        <th class="filter_header" colspan="11" style="font-size: 15px !important;padding-top: 5px !important;color: #4c0c0c85;">{{$filterheader}}

                        </th>
                    </tr>
                </thead>
                    <thead><tr class="table_header_bg">
                            <th colspan="11">Filters

                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                        <div>
                            <?php
                            $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    ?>

                                    <div class= "col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label >{{$fieldTypeArray[$i][2]}}</label>
                                        <div class="clearfix"></div>
                                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                        <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"style="width: 100%";></div>
                                        <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden"/>
                                    </div></div>
                                    <div class="col-md-2 padding_sm" style="margin-top: 20px;">
                                        <div class="checkbox checkbox-success inline no-margin">
                                            <input id="checkbox" type="checkbox" name="checkbox">
                                                <label class="text-blue" for="checkbox">
                                                Insured
                                                </label>
                                        </div>
                                        </div>
                                        <div class="col-md-2 padding_sm" style="margin-top: 20px;">
                                            <div class="checkbox checkbox-success inline no-margin">
                                                <input id="occu" type="checkbox" name="occu">
                                                    <label class="text-blue" for="occu">
                                                    Current Occupancy
                                                    </label>
                                            </div>
                                        </div>
                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'text') {
                                    ?>
                                    <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label >{{$fieldTypeArray[$i][2]}}</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                                        echo $fieldTypeArray[$i][3];
                                } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> >
                                    </div></div>
                                    <?php
                                }else if ($fieldTypeArray[$i][0] == 'number') {
                                    ?>
                                    <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label >{{$fieldTypeArray[$i][2]}}</label>
                                        <div class="clearfix"></div>
                                        <input type="number" name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                                        echo $fieldTypeArray[$i][3];
                                } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> >
                                    </div></div>
                                    <div class='clearfix'></div>
                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>
                                    <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label >{{$fieldTypeArray[$i][2]}} From</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                            if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            }
                            ?> value="{{date('M-d-Y')}}" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_from">
                                    </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label>{{$fieldTypeArray[$i][2]}} To</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                            if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            }
                                    ?>  value="{{ date('M-d-Y')}}" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_to">

                                    </div>
                                    </div>
                                    <?php

                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>

                                    <div class="col-md-3 padding_sm date_filter_div">
                                    <div class="mate_select_data">
                                    <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2 filters','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1],'style' => 'width:50%;color:#555555; padding:2px 12px;']) !!}
                                    </div></div>
                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                    ?>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate_select_data">
                                            <label class="filter_label" style="width: 100%;">{{$fieldTypeArray[$i][2]}}</label>
                                            <div class="clearfix"></div>
                                            {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2 filters','multiple'=>'multiple-select','placeholder' => " ",'id' => $fieldTypeArray[$i][1],'style' => 'width:50%;color:#555555; padding:2px 12px;']) !!}
                                        </div>

                                    </div>
                                    <?php
                                }
                                $i++;

                            }
                            ?>

                            <div class="col-md-12 padding_sm" style="text-align:right; margin-top: 10px;">

                                <a class="btn light_purple_bg" onclick="search_clear();"  name="clear_results" id="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </a>
                                <a class="btn light_purple_bg disabled" onclick="generate_csv();"  name="csv_results" id="csv_results">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    Excel
                                </a>
                                <a class="btn light_purple_bg disabled" onclick="printReportData();"  name="print_results" id="print_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                                <a class="btn light_purple_bg" onclick="getTransferData();"  name="search_results" id="search_results">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </a><br>



                            </div>
                            {!! Form::token() !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
 </div>

 <div class="col-md-12 padding_sm">
        <div class="col-md-12 padding_sm">
            <div class="theadscroll always-visible" id="ResultDataContainer" style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                <div style="background:#686666;">
                <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                width: 100%;
                padding: 30px;" id="ResultsViewArea">

                </page>
            </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!----------- DRILL DOWN ---------------->
<div class="modal fade" id="getledgerdataModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 40%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            <h4 class="modal-title" id="leder_headerdata">Ledger Detail Data</h4>
            </div>

            <div class="modal-body" style="min-height:400px">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getledgerdataModalDiv">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getLeaderDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 50%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="leder_name_header">Ledger Detail Data</h4>
            </div>
            <div class="modal-body" style="min-height:400px">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="getLeaderDetailData">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editLeaderDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="editleder_name_header">Ledger Detail Data</h4>
            </div>
            <div class="modal-body" style="min-height:500px">
                <div class="row padding_sm">
                <div class="theadscroll always-visible" id="ResultDataContainer" style="min-height: 400px;">
                    <div class='col-md-12 padding_sm' id="editLeaderDetailData">

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/reports/default/javascript/custom_report.js")}}"></script>

<script type="text/javascript">
//====================================DRILL DOWN==============================================
function showDetailModal(e,id,ledger_name){
    var first_td_text = $(e).closest('tr').children('td:first').text();

    var row_id = e.id;
    var filters_list = new Object();
    var filters_value = '';
    var filters_id = '';

    $('.filters').each(function () {
        filters_id = this.id;
        if (filters_id == 'bill_no_hidden') {
            filters_id = 'bill_no';
        }
        filters_value = $('#' + filters_id).val();
        filters_id = filters_id.replace('_hidden', '');

        if (filters_id == 'bill_date_from') {
            filters_id = 'from_date';
        }
        if (filters_id == 'bill_date_to') {
            filters_id = 'to_date';
        }
        if (filters_id == 'no_date') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_date';
            }else{
                filters_value = 0;
                filters_id = 'no_date';
            }
        }
        if (filters_id == 'no_opening') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'no_opening';
            }else{
                filters_value = 0;
                filters_id = 'no_opening';
            }
        }
        if (filters_id == 'show_naration') {
            if($('#' + filters_id).prop("checked") == true){
                filters_value = 1;
                filters_id = 'show_naration';
            }else{
                filters_value = 0;
                filters_id = 'show_naration';
            }
        }

        if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
            filters_list[filters_id] = filters_value;
        }
    });
    if (id != '0') {
                filters_value_det = id;
                filters_id_det = 'group_id';
            }
    filters_list[filters_id_det] = filters_value_det;
    filters_value_inc = 'include_cr_dr';
    filters_id_inc = 'include_cr_dr';
    filters_list[filters_id_inc] = filters_value_inc;
    filters_list['show_detail_modal'] = 1;
    $("#remove_group_id_"+id).show();
    $("#expand_group_id_"+id).hide();
    var url = $("#ins_base_url").val();
     $.ajax({
                type: "GET",
                url: url + "/reports/common_row_expand",
                data: filters_list,
                beforeSend: function () {
                    $('#leder_headerdata').html(ledger_name);
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $("#getledgerdataModal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#getledgerdataModalDiv').html(data);
                    $.LoadingOverlay("hide");
                 setTimeout(function() {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
             }, 400);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                    });
                },
                complete: function () {
                }
            });
        }

function getLeaderDetails(ledger_name,ledger_id){

   var url = $("#ins_base_url").val();
    var bill_date_from = $("#bill_date_from").val();
    var bill_date_to = $("#bill_date_to").val();
    var param={ledger_id:ledger_id,bill_date_from:bill_date_from,bill_date_to:bill_date_to,from_type:ledger_name}
     $.ajax({
                type: "GET",
                url: url + "/report/getLedgerdataReport",
                data: param,
                beforeSend: function () {
                    $('#leder_name_header').html(ledger_name);
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                 },
                success: function (data) {
                    $("#getLeaderDetailModel").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#getLeaderDetailData').html(data);
                    $.LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                    });
                },
                complete: function () {
                }
            });
}

function loadLedgerEditData(){
var typ_vl = localStorage.getItem("type_val");
var exp_pay_type =localStorage.getItem("exp_pay_type");
if(typ_vl != null){
$("#v_type_1").val(typ_vl);
$("#type").val(exp_pay_type);
$('#v_type_1').val(typ_vl).change();
}
localStorage.removeItem("type_val");
localStorage.removeItem("exp_pay_type");

calculate_table_total('amount','total_amount_by_bill');
calculate_table_total('amnt_cd','total_amount_cd');
   if (window.location.href.indexOf("add_ledger_booking_entry") > -1)
    {
         insertDefaultBankDetails(1);
    }
    $("#amount_1").focus();
    setTimeout(function(){
    $('#ledger_item_desc-1').focus();
            },300);

      setTimeout(function(){
        var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('?');
  if(sURLVariables[1] != 'undefined'){
      var roww_id = (/id=(\d+)/.exec(sPageURL)[1]);
      $("#edit_ledger_booking_"+roww_id).css("background-color","#edb2b2");
      $('table tr#edit_ledger_booking_'+roww_id).find('input:first').focus();
  }
            },300);
$(document).bind('keydown', function(e) {
  if(e.ctrlKey && (e.which == 83)) { // SAVE AND CONTINUE +s
    e.preventDefault();
    saveForm(0);
    return false;
  }else if(e.ctrlKey && (e.which == 80) && !e.altKey){ //purchase +p
      e.preventDefault();
      $('#v_type_1').val(1).change();
      return false;
  }else if(e.ctrlKey && e.altKey && (e.which == 80)){//payment +alt+p
      e.preventDefault();
      $('#v_type_1').val(12).change();
      return false;
  }else if(e.ctrlKey &&(e.which == 76)){//sales +l
      e.preventDefault();
      $('#v_type_1').val(2).change();
      return false;
  }else if(e.ctrlKey && (e.which == 74)){//journel +j
      e.preventDefault();
      $('#v_type_1').val(3).change();
      return false;
  }else if(e.ctrlKey && (e.which == 82) && !e.shiftKey){ //Receipt +r
      e.preventDefault();
      $('#v_type_1').val(5).change();
      return false;
  }else if(e.ctrlKey && (e.which == 67)){//Credit Note Import  +c
      e.preventDefault();
      $('#v_type_1').val(6).change();
      return false;
  }else if(e.ctrlKey && (e.which == 73)){// Sales import +i
      e.preventDefault();
      $('#v_type_1').val(7).change();
      return false;
  }else if(e.ctrlKey && (e.which == 77)&& !e.shiftKey){// Receipt Import +m
      e.preventDefault();
      $('#v_type_1').val(8).change();
      return false;
  }else if(e.ctrlKey && (e.which == 79)){//Contra +o
      e.preventDefault();
      $('#v_type_1').val(9).change();
      return false;
  }else if(e.ctrlKey && (e.which == 69)){//credit note +e
      e.preventDefault();
      $('#v_type_1').val(10).change();
      return false;
  }else if(e.ctrlKey && (e.which == 68)){//debit note +d
      e.preventDefault();
      $('#v_type_1').val(11).change();
      return false;
  }else if(e.ctrlKey && (e.which == 65)){//advance search popup +a
      e.preventDefault();
     editAdvanceSearch();
      setTimeout(function(){
        $("#ledger_item_desc-11").focus();
    },300);
      return false;
  }else if(e.altKey && (e.which == 65)){//advance search fn alt+a
      e.preventDefault();
      AdvanceEditSearch();
      return false;
  }else if(e.altKey && (e.which == 65)){//save exit alt+s
      e.preventDefault();
      saveForm(1);
      return false;
  }else if(e.ctrlKey && (e.which == 8)){//cancel alt+backspace
      e.preventDefault();
      backToList();
      return false;
  }
});
$('.theadscroll').perfectScrollbar("update");
 $(".theadfix_wrapper").floatThead('reflow');
}
function editLedgerHeder(booking_id){

    var url = $("#ins_base_url").val();
    var urls =  url + "/master/edit_ledger_booking_entry/"+booking_id;
    window.open(urls, '_blank');
//     $.ajax({
//                type: "GET",
//                url: url + "/master/edit_ledger_booking_entry/"+booking_id+'/'+1,
//                data: {},
//                beforeSend: function () {
//                    $('#editleder_name_header').html(ledger_name);
//                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
//                 },
//                success: function (data) {
//                    $("#editLeaderDetailModel").modal({
//                        backdrop: 'static',
//                        keyboard: false
//                    });
//                    $('#editLeaderDetailData').html(data);
//                },
//                complete: function () {
//                    $('.theadscroll').perfectScrollbar("update");
//                    $(".theadfix_wrapper").floatThead('reflow');
//                    loadLedgerEditData();
//                    $.LoadingOverlay("hide");
//                }
//            });
}
</script>
@endsection
