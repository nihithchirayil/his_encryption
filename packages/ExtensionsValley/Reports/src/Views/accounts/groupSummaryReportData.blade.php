<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    table, th, td {
        border: 1px solid #00000054;
        font-size: 10px;

    }
</style>

<div class="row">
 
   <div class="col-md-12" id="result_container_div">

    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                        @if(sizeof($res)>0)
                        @php $grand_debit_total = $grand_credit_total = 0; @endphp
            
                         <thead>
                         <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th  style="padding: 2px;width:2%;text-align: center;border:thin solid black; "></th>
                            <th  style="padding: 2px;width:22%;text-align: center;border:thin solid black; ">Particulars</th>
                            <th  style="padding: 2px;width:10%;text-align: center;border:thin solid black;">Debit</th>
                            <th  style="padding: 2px;width:10%;text-align: center;border:thin solid black;">Credit</th>
                            <th  style="padding: 2px;width:3%;text-align: center;" class="noExl"></th>
                        </tr>
                         </thead>

                        @foreach ($res as $data)


                        <tr id="{{$data->id}}" style="cursor:pointer" >
                             <td style="width:2%;text-align: center">
                                 <button type="button" onclick="showDetailModal(this,'<?= $data->id; ?>','<?= $data->ledger_name?>')" style="padding: 1px 1px" class="btn btn-prmary"><i class="fa fa-info"></i></button>
                             </td>
                            <td rowspan="" style="width:22%;border:thin solid black;text-align: left" class="" onclick="expandRow(this,{{$data->id}})">{{$data->ledger_name}}</td>  
                            <td rowspan="" style="width:10%;border:thin solid black;font-weight: bold;text-align: right" class="td_common_numeric_rules"  id="dr_amount_{{$data->id}}">{{$data->dr_amount}}</td>
                            <td rowspan="" style="width:10%;border:thin solid black;font-weight: bold;text-align: right" class="td_common_numeric_rules" id="cr_amount_{{$data->id}}">{{$data->cr_amount}}</td>
                            <td rowspan="" style="width:3%;cursor: pointer" class="noExl"  >
                                <i class="fa fa-2x fa-minus-circle noExl" style="color:#f97d79;margin-left: 2px"  onclick="removeRow(this,{{$data->dr_amount??0}},{{$data->cr_amount??0}},0,{{$data->id}},0)"></i>
                            </td>
                        </tr>
          <?php $grand_credit_total += $data->cr_amount ; $grand_debit_total += $data->dr_amount?>
                        @endforeach
                        <tr style="cursor:pointer" onclick="showPlReport()">
                            <td></td>
                            <td rowspan="" style="width:22%;border:thin solid black;text-align: left" class="" >Profit & Loss A/c</td>  
                            <td rowspan="" style="width:10%;border:thin solid black;font-weight: bold;" class="td_common_numeric_rules"  id="dr_amount_{{$data->id}}">{{$prof_los??''}}</td>
                            <td rowspan="" style="width:10%;border:thin solid black;font-weight: bold;" class="td_common_numeric_rules" id="cr_amount_{{$data->id}}"></td>
                            <td rowspan="" style="width:3%;cursor: pointer" class="noExl"  >
                            </td>
                        </tr>
            <tr style="background-color: #dedc91"> 
                <td></td>
                <td colspan='1' style='font-size: 14px;border:thin solid black;'><strong> Total</strong></td> 
                <td style='font-size: 14px;font-weight:bold;border:thin solid black;' class="td_common_numeric_rules" id="debit_total">
                    {{$grand_debit_total}} </td>  
                <td style='font-size: 14px;font-weight:bold;border:thin solid black;' class="td_common_numeric_rules" id="credit_total">
                    {{$grand_credit_total }}  </td>
                <td style="border:thin solid black;">&nbsp;</td>
                        </tr>
            @else 
            <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
                        @endif  
                    </table>
    </div>
</div>
