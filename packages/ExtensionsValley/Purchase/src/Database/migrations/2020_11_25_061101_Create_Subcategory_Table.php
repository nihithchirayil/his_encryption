<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubcategoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subcategory')) {
            Schema::create('subcategory', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100);
                $table->string('subcategory_code')->unique();
                $table->string('category_code', 50);
                $table->smallInteger('status')->default(1);
                $table->integer('created_by')->default(0);
                $table->integer('updated_by')->default(0);
                $table->integer('deleted_by')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (Schema::hasTable('subcategory')) {
            Schema::drop('subcategory');
        }
    }
}
