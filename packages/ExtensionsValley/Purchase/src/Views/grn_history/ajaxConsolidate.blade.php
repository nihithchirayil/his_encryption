<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <h4 style="text-align: center;margin-top: 30px;" id="heading"><b> Consolidated Data </b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="11%">GRN No</th>
                        <th width="6%">Created Date</th>
                        <th width="6%">Invoice Date</th>
                        <th width="6%">Approved Date</th>
                        <th width="6%">Invoice No.</th>
                        <th width="6%">Vendor</th>
                        <th width="6%">Purchase Category</th>
                        <th width="6%">GST Code</th>
                        <th width="6%">Location</th>
                        <th width="6%">Total Cost Without Tax</th>
                        <th width="5%">Total Tax</th>
                        <th width="5%">Adjustment</th>
                        <th width="6%">Total Amount</th>
                        <th width="6%">Created By</th>
                        <th width="6%">Approved By</th>
                        <th width="5%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                $total_net_tax_amount=0.0;
                $total_round_amout=0.0;
                $total_net_amout=0.0;
            if (count($get_consolidated) != 0) {
                foreach ($get_consolidated as $list) {
                    $grn_status="NA";
                    if($list->approve_status=='1'){
                        $grn_status="Created";
                    }else if($list->approve_status=='2'){
                        $grn_status="Approved";
                    }else if($list->approve_status=='3'){
                        $grn_status="Rejected";
                    }else if($list->approve_status=='4'){
                        $grn_status="Closed";
                    }
                    ?>
                    <tr>
                        <td class="common_td_rules">{{ $list->grn_no }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->created_at)) }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->bill_date)) }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->approved_at)) }}</td>
                        <td class="common_td_rules">{{ $list->bill_no }}</td>
                        <td class="common_td_rules">{{ $list->vendor_name }}</td>
                        <td class="status common_td_rules">{{ $list->purchase_category }}</td>
                        <td class="common_td_rules">{{ $list->gst_vendor_code }}</td>
                        <td class="common_td_rules">{{ $list->location_name }}</td>
                        <td class="common_td_rules">{{ $list->total_net_cost_without_tax }}</td>
                        <td class="td_common_numeric_rules">{{ $list->net_tax_amount }}</td>
                        <td class="td_common_numeric_rules">{{ $list->round_amount }}</td>
                        <td class="td_common_numeric_rules">{{ $list->net_amount }}</td>
                        <td class="common_td_rules">{{ $list->created_by }}</td>
                        <td class="common_td_rules">{{ $list->approved_by }}</td>
                        <td class="common_td_rules">{{ $grn_status }}</td>
                    </tr>
                    <?php
                 $total_net_tax_amount+= floatval($list->net_tax_amount);
                 $total_round_amout+= floatval($list->round_amount);
                 $total_net_amout+= floatval($list->net_amount);
                }
                ?>
                    <tr>
                        <th class="common_td_rules" colspan="10" style="text-align: center">Total</th>
                        <th class="td_common_numeric_rules"><?= $total_net_tax_amount ?></th>
                        <th class="td_common_numeric_rules"><?= $total_round_amout ?></th>
                        <th class="td_common_numeric_rules"><?= $total_net_amout ?></th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>

                    </tr>
                    <?php
            }else{
                ?>
                    <tr>
                        <td colspan="15" class="re-records-found">No Records found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
