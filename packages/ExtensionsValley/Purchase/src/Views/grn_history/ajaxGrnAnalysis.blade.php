<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <h4 style="text-align: center;margin-top: 30px;" id="heading"><b> GRN Analysis Report </b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="5%">#</th>
                        <th width="16%">Supplier Name</th>
                        <th width="16%">Grn Bills Count</th>
                        <th width="16%">Purchase Amount</th>
                        <th width="16%">Purchase Ret. Count</th>
                        <th width="16%">Purchase Return Amount</th>
                        <th width="15%">Net Purchase</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                $total_grn_bills_count=0.0;
                $total_purchase_net_amt=0.0;
                $total_purchase_return_count=0.0;
                $total_return_net_amt=0.0;
                $total_grn_net_amount=0.0;
            if (count($res) != 0) {
                $i=1;
                foreach ($res as $list) {
                    ?>
                    <tr>
                        <td class="common_td_rules">{{ $i }}</td>
                        <td class="common_td_rules">{{ $list->vendor_name }}</td>
                        <td class="td_common_numeric_rules">{{ $list->grn_bills_count }}</td>
                        <td class="td_common_numeric_rules">{{ $list->purchase_net_amt }}</td>
                        <td class="td_common_numeric_rules">{{ $list->purchase_return_count }}</td>
                        <td class="td_common_numeric_rules">{{ $list->return_net_amt }}</td>
                        <td class="td_common_numeric_rules">{{ $list->grn_net_amount }}</td>
                    </tr>
                    <?php
                 $total_grn_bills_count+= floatval($list->grn_bills_count);
                 $total_purchase_net_amt+= floatval($list->purchase_net_amt);
                 $total_purchase_return_count+= floatval($list->purchase_return_count);
                 $total_return_net_amt+= floatval($list->return_net_amt);
                 $total_grn_net_amount+= floatval($list->grn_net_amount);
                 $i++;
                }
                ?>
                    <tr>
                        <th class="common_td_rules" colspan="2" style="text-align: center">Total</th>
                        <th class="td_common_numeric_rules"><?= $total_grn_bills_count ?></th>
                        <th class="td_common_numeric_rules"><?= $total_purchase_net_amt ?></th>
                        <th class="td_common_numeric_rules"><?= $total_purchase_return_count ?></th>
                        <th class="td_common_numeric_rules"><?= $total_return_net_amt ?></th>
                        <th class="td_common_numeric_rules"><?= $total_grn_net_amount ?></th>
                    </tr>
                    <?php
            }else{
                ?>
                    <tr>
                        <td colspan="6" class="re-records-found">No Records found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
