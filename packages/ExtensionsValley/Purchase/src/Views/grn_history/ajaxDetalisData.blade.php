<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('Y-m-d h:i A') ?> </p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> Purchase History Report</h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <?php
                $i = 0;
                $grn_number = '';
                $tot_qty = 0;
                $nettot_qty = 0;
                $tot_free_qty = 0;
                $nettot_free_qty = 0;
                $tot_tax = 0;
                $nettot_tax = 0;
                $tot_rate = 0;
                $nettot_rate = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
         foreach ($res as $data){
             if($grn_number != $data->grn_no){
                 $i=0;
                 $grn_number = $data->grn_no;
                 $tot_qty=0;
                 $tot_free_qty=0;
                 $tot_tax=0;
                 $tot_rate=0;
               ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width="25%">Ref No :<b><?= $data->grn_no ?> </b></th>
                            <th width="25%">Voucher No: <b><?= $data->bill_no ?></b></th>
                            <th width="25%">Vendor: <b><?= $data->vendor_name ?></b></th>
                            <th colspan="3" width="25%">
                                Invoice Date: <b><?= $data->bill_date ?></b><br>
                                Approved Date: <b><?= $data->approved_date ?></b><br>
                                Created Date: <b><?= $data->created_at ?></b>
                            </th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:rgb(0 128 128);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width="25%">Created By: <b><?= $data->created_by ?></b></th>
                            <th width="25%">Approved By: <b><?= $data->approved_by ?></b></th>
                            <th></th>
                            <th colspan="3"></th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:rgb(0 206 209);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th><b>Item Name</b></th>
                            <th><b>Batch No.</b></th>
                            <th><b>Qty</b></th>
                            <th><b>Free Qty</b></th>
                            <th style="text-align:right"><b>Total Tax</b></th>
                            <th style="text-align:right"><b>Total Rate</b></th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="common_td_rules">{{ $data->batch_no }}</td>
                            <td class="td_common_numeric_rules">{{ $data->grn_qty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_free_qty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->grn_tax_total_amt }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_rate }}</td>
                        </tr>
                        <?php
             }else{
                 ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="common_td_rules">{{ $data->batch_no }}</td>
                            <td class="td_common_numeric_rules">{{ $data->grn_qty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_free_qty }}</td>
                            <td class="td_common_numeric_rules">{{ $data->grn_tax_total_amt }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_rate }}</td>
                        </tr>
                        <?php

             }

                 $tot_qty+= floatval($data->grn_qty);
                 $tot_free_qty+= floatval($data->total_free_qty);
                 $tot_tax+= floatval($data->grn_tax_total_amt);
                 $tot_rate+= floatval($data->total_rate);

             if($i==$grn_data_cnt[$grn_number]){
                $whole = floor($tot_rate);
                $fraction = $tot_rate - $whole;
             ?>
                        <tr class="headerclass"
                            style="background-color:rgb(32 178 170);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th class="common_td_rules" colspan="2" style="text-align: left;">Sub Net Total</th>
                            <th class="td_common_numeric_rules"><?= $tot_qty ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_free_qty ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_tax ?></th>
                            <th class="td_common_numeric_rules"><?= $tot_rate ?></th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:rgb(32 178 170);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th class="common_td_rules" colspan="2" style="text-align: left;">Bill Net Total</th>
                            <th class="td_common_numeric_rules">Adjustment : <?= number_format((float)$fraction, 2, '.', ''); ?></th>
                            <th class="td_common_numeric_rules" colspan="3"><?= $whole ?></th>
                        </tr>
                        <?php
                    $nettot_qty +=$tot_qty;
                    $nettot_free_qty +=$tot_free_qty;
                    $nettot_tax +=$tot_tax;
                    $nettot_rate +=$tot_rate;
             }
             $i++;

         }
         ?>
                        <tr class="headerclass"
                            style="background-color:rgb(95 158 160);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th class="common_td_rules" colspan="2" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $nettot_qty ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_free_qty ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_tax ?></th>
                            <th class="td_common_numeric_rules"><?= $nettot_rate ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="6" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </font>
    </div>
</div>
