<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var param = getFiltersData(0);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#search_results').attr('disabled', true);
                        $('#search_results_Spin').removeClass('fa fa-search');
                        $('#search_results_Spin').addClass('fa fa-spinner fa-spin');
                        $('#pagination_data').LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#009869'
                        });
                    },
                    success: function(html) {
                        $('#pagination_data').html(html);
                    },
                    complete: function() {
                        $('#pagination_data').LoadingOverlay("hide");
                        $('#search_results').attr('disabled', false);
                        $('#search_results_Spin').removeClass('fa fa-spinner fa-spin');
                        $('#search_results_Spin').addClass('fa fa-search');
                    },
                    error: function() {
                        Command: toastr["error"]("Network Error!");
                    }

                });
            }
            return false;
        });

    });
</script>
<div>
    <p style="font-size:12px;"><b>Total Amount : {{ $total_amount }}</b></p>
</div>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="11%">GRN No</th>
                <th width="6%">Created Date</th>
                <th width="6%">Invoice Date</th>
                <th width="6%">Approved Date</th>
                <th width="6%">Invoice No.</th>
                <th width="6%">Vendor</th>
                <th width="6%">Purchase Category</th>
                <th width="6%">GST Code</th>
                <th width="6%">Location</th>
                <th width="6%">Total Cost Without Tax</th>
                <th width="5%">Total Tax</th>
                <th width="5%">Adjustment</th>
                <th width="6%">Total Amount</th>
                <th width="6%">Created By</th>
                <th width="6%">Approved By</th>
                <th width="5%">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($grn_history) != 0) {
                foreach ($grn_history as $list) {
                    $grn_status="NA";
                    if($list->approve_status=='1'){
                        $grn_status="Created";
                    }else if($list->approve_status=='2'){
                        $grn_status="Approved";
                    }else if($list->approve_status=='3'){
                        $grn_status="Rejected";
                    }else if($list->approve_status=='4'){
                        $grn_status="Closed";
                    }
                    ?>
            <tr>
                <td class="category_name common_td_rules">{{ $list->grn_no }}</td>
                <td class="status common_td_rules">{{ date('M-d-Y', strtotime($list->created_at)) }}</td>
                <td class="status common_td_rules">{{ date('M-d-Y', strtotime($list->bill_date)) }}</td>
                <td class="status common_td_rules">{{ date('M-d-Y', strtotime($list->approved_at)) }}</td>
                <td class="status common_td_rules">{{ $list->bill_no }}</td>
                <td class="status common_td_rules">{{ $list->vendor_name }}</td>
                <td class="status common_td_rules">{{ $list->purchase_category }}</td>
                <td class="status common_td_rules">{{ $list->gst_vendor_code }}</td>
                <td class="status common_td_rules">{{ $list->location_name }}</td>
                <td class="status common_td_rules">{{ $list->total_net_cost_without_tax }}</td>
                <td class="status common_td_rules">{{ $list->net_tax_amount }}</td>
                <td class="status common_td_rules">{{ $list->round_amount }}</td>
                <td class="status common_td_rules">{{ $list->net_amount }}</td>
                <td class="status common_td_rules">{{ $list->created_by }}</td>
                <td class="status common_td_rules">{{ $list->approved_by }}</td>
                <td class="status common_td_rules">{{ $grn_status }}</td>
            </tr>
            <?php
                }
            }else{
                ?>
            <tr>
                <td colspan="15" class="re-records-found">No Records found</td>
            </tr>
            <?php
            }
            ?>

        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
