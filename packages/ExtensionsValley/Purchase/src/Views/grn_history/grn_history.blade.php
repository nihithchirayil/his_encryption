@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <div class="modal fade" id="getGrnDetalisModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1400px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="getGrnDetalisHeader">NA</h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12 padding_sm" id="printstyle_disign">
                        <div class="col-md-12 padding_sm">
                            <div id="ResultDataContainer" style="padding: 10px;font-family:poppinsregular">
                                <div style="background:#686666;">
                                    <page size="A4"
                                        style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                                                                                                                                            width: 100%;  padding: 30px;"
                                        id="ResultsViewArea">
                                    </page>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="exceller();" type="button" class="btn btn-warning">Excel <i
                            class="fa fa-file-excel-o"></i></button>
                    <button style="padding: 3px 3px" onclick="printReportData();" type="button"
                        class="btn btn-primary">Print <i class="fa fa-print"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="right_col">
        <div class="container-fluid">


            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                                <thead>
                                    <tr class="">
                                        <th class="filter_header" colspan="11"
                                            style="font-size: 15px !important;padding-top: 5px !important;color: #4c0c0c85;">
                                            {{ $title }}

                                        </th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr class="table_header_bg">
                                        <th colspan="11">Filters

                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                                <div class="col-md-12 padding_sm">
                                    {!! Form::open(['name' => 'search_from', 'id' => 'search_from']) !!}
                                    <div>
                                        <?php
                            $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    if($fieldTypeArray[$i][1]=='approved_by'){
                                        echo "<div class='clearfix'></div>";
                                    }
                                    ?>

                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search" value="" autocomplete="off"
                                                    type="text" id="{{ $fieldTypeArray[$i][1] }}"
                                                    name="{{ $fieldTypeArray[$i][1] }}" />
                                                <div id="{{ $fieldTypeArray[$i][1] }}AjaxDiv" class="ajaxSearchBox"
                                                    style="width: 100%;position:absolute;margin-top: 24px;" ;></div>
                                                <input class="filters"
                                                    value="{{ $hiddenFields[$hidden_filed_id] }}" type="hidden"
                                                    name="{{ $fieldTypeArray[$i][1] }}_hidden" value=""
                                                    id="{{ $fieldTypeArray[$i][1] }}_hidden" />
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'text') {
                                    ?>
                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="{{ $fieldTypeArray[$i][1] }}"
                                                    <?php if (isset($fieldTypeArray[$i][3])) {
                                                        echo $fieldTypeArray[$i][3];
                                                    } ?> value="" class="form-control filters"
                                                    id="{{ $fieldTypeArray[$i][1] }}"
                                                    data="{{ isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : '' }}"
                                                    <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?>>
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>
                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }} From</label>
                                                <div class="clearfix"></div>
                                                <input type="text" data-attr="date" autocomplete="off"
                                                    name="{{ $fieldTypeArray[$i][1] }}_from" <?php
if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
}
?>
                                                    class="form-control datepicker filters" placeholder="Mon-DD-YYYY"
                                                    id="{{ $fieldTypeArray[$i][1] }}_from">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }} To</label>
                                                <div class="clearfix"></div>
                                                <input type="text" data-attr="date" autocomplete="off"
                                                    name="{{ $fieldTypeArray[$i][1] }}_to" <?php
if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
}
?>
                                                    class="form-control datepicker filters" placeholder="Mon-DD-YYYY"
                                                    id="{{ $fieldTypeArray[$i][1] }}_to">

                                            </div>
                                        </div>
                                        <?php

                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>

                                        <div class="col-md-2 padding_sm date_filter_div"
                                            style="padding-bottom:9px !important; margin-top: 10px">

                                            <div class="mate-input-box">
                                                <label class="filter_label ">{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <select class="form-control select2 filters"
                                                    id="<?= $fieldTypeArray[$i][1] ?>">
                                                    <option value="">Select <?= $fieldTypeArray[$i][2] ?></option>
                                                    <?php
                                                         foreach ($fieldName as $each) {
                                                                   ?>
                                                    <option value="<?= $each->code ?>"><?= $each->name ?></option>
                                                    <?php
                                                               }
                                                           ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                    $colmd= "col-md-6 padding_sm";
                                    if($fieldTypeArray[$i][1]=='status'){
                                        $colmd= "col-md-4 padding_sm";
                                    }
                                    ?>

                                        <div class="<?= $colmd ?>" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label class="filter_label"
                                                    style="width: 100%;">{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <select class="form-control select2 filters" multiple="multiple-select"
                                                    id="<?= $fieldTypeArray[$i][1] ?>">
                                                    <option value="">Select <?= $fieldTypeArray[$i][2] ?></option>
                                                    <?php
                                                         foreach ($fieldName as $each) {
                                                                   ?>
                                                    <option value="<?= $each->code ?>"><?= $each->name ?></option>
                                                    <?php
                                                               }
                                                           ?>
                                                </select>
                                            </div>

                                        </div>
                                        <?php
                                }
                                $i++;

                            }
                            ?>

                                        <div class="col-md-12 padding_sm" style="text-align:right; margin-top: 10px;">

                                            <button type="button" class="btn btn-warning" onclick="search_clear();"
                                                name="clear_results" id="clear_results">
                                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                                Reset
                                            </button>
                                            <button type="button" class="btn bg-purple" onclick="getGrnDetalis();"
                                                id="grnDetailsBtn"> <i class="fa fa-list" id="grnDetailsSpin"></i>
                                                Details</button>
                                            <button type="button" class="btn bg-teal-active" onclick="getGrnConsolidated();"
                                                id="getConsolidatedBtn"> <i class="fa fa-list"
                                                    id="getConsolidatedSpin"></i>
                                                Consolidated</button>
                                            <button type="button" class="btn btn-primary" onclick="getGrnAnalysis();"
                                                id="getGrnAnalysisBtn">
                                                <i class="fa fa-list" id="getGrnAnalysisSpin"></i>
                                                GRN Analysis
                                            </button>
                                            <button type="button" class="btn btn-info" onclick="getSearchPreview();"
                                                id="getlistpreviewBtn">
                                                <i class="fa fa-eye" id="getlistpreviewSpin"></i>
                                                Preview
                                            </button>
                                            <button type="button" class="btn btn-success" onclick="getReportData();"
                                                name="search_results" id="search_results">
                                                <i class="fa fa-search" id="search_results_Spin" aria-hidden="true"></i>
                                                Search
                                            </button>

                                        </div>
                                        {!! Form::token() !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            <div class="col-md-12 padding_sm" id="pagination_data" style="margin-top: 15px;">

            </div>
        </div>
    </div>

    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/grn_history.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>
@endsection
