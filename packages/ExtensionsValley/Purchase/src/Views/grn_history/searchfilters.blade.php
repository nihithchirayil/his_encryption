<?php
if(sizeof($resultData)>0){
    if($input_id == 'grn_number'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->grn_no)) }}","{{ $input_id }}")'>
    {{ $item->grn_no }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'return_number'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->purchase_return_no)) }}","{{ $input_id }}")'>
    {{ $item->purchase_return_no }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'stock_return_receive'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->return_no)) }}","{{ $input_id }}")'>
    {{ $item->return_no }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'free_stock_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->free_stock_no)) }}","{{ $input_id }}")'>
    {{ $item->free_stock_no }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'created_by' || $input_id == 'approved_by'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}")'>
    {{ $item->name }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'invoice_number' || $input_id == 'bill_receive_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillInvoice_no("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->bill_no)) }}","{{ $input_id }}")'>
    {{ $item->bill_no }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'grn_receive_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillInvoice_no("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->grn_no)) }}","{{ $input_id }}")'>
    {{ $item->grn_no }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }

        } else {
            echo 'No Results Found';
        }
        ?>
