<?php
 if(isset($request_no_data)){
    if(!empty($request_no_data)){
        $search_type='0';
        if(isset($type))
        $search_type=$type;
        foreach ($request_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillitemdescValues2(this,"{{htmlentities($desc->request_no)}}")' class="list_hover">
                {!! $desc->request_no !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($op_no_data)){
    if(!empty($op_no_data)){
        foreach ($op_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_id(this,"{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}")' class="list_hover">
                {!! $desc->uhid !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($bill_no_search)){
    if(!empty($bill_no_search)){
        foreach ($bill_no_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_id(this,"{{htmlentities($desc->bill_no)}}")' class="list_hover">
                {!! $desc->bill_no !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($itemDescDetails)){
    if(!empty($itemDescDetails)){
        foreach ($itemDescDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_desc_details(this,"{{htmlentities($desc->item_desc)}}","{{htmlentities($desc->item_code)}}",)' class="list_hover">
                {!! $desc->item_desc !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($subCatDetails)){
    if(!empty($subCatDetails)){
        foreach ($subCatDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_sub_category_details(this,"{{htmlentities($desc->subcategory_name)}}","{{htmlentities($desc->sub_id)}}")' class="list_hover">
                {!! $desc->subcategory_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($vendorDetails)){
    if(!empty($vendorDetails)){
        foreach ($vendorDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_vendor_details(this,"{{htmlentities($desc->vendor_name)}}","{{htmlentities($desc->vendor_code)}}")' class="list_hover">
                {!! $desc->vendor_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($transaction_no_data)){
    if(!empty($transaction_no_data)){
        foreach ($transaction_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_transaction_details(this,"{{htmlentities($desc->transaction_num)}}")' class="list_hover">
                {!! $desc->transaction_num !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($user_name_data)){
    if(!empty($user_name_data)){
        foreach ($user_name_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_user_details(this,"{{htmlentities($desc->name)}}","{{htmlentities($desc->id)}}")' class="list_hover">
                {!! $desc->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($patienDetails)){
    if(!empty($patienDetails)){
        foreach ($patienDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_details(this,"{{htmlentities($desc->patient_name)}}","{{htmlentities($desc->id)}}")' class="list_hover">
                {!! $desc->patient_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($item_code_data)){
    if(!empty($item_code_data)){
        foreach ($item_code_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillRequestItemDetails(this,"{{htmlentities($desc->item_code)}}","{{htmlentities($desc->item_desc)}}","{{htmlentities($desc->expiry_date)}}","{{htmlentities($desc->unit_tax_rate)}}","{{htmlentities($desc->unit_selling_price)}}","{{htmlentities($desc->unit_mrp)}}")' class="list_hover">
                {!! $desc->item_desc !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($item_code)){
    if(!empty($item_code)){
        foreach ($item_code as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_desc_details(this,"{{htmlentities($desc->item_desc)}}","{{htmlentities($desc->item_code)}}")' class="list_hover">
                {!! $desc->item_desc !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($catgoryDetails)){
    if(!empty($catgoryDetails)){
        foreach ($catgoryDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_category_details(this,"{{htmlentities($desc->name)}}","{{htmlentities($desc->cat_id)}}")' class="list_hover">
                {!! $desc->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}
if(isset($access_location_details)){
    if (!empty($access_location_details)) {

        foreach ($access_location_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillAccessLocationData("{{htmlentities($data->location_name)}}","{{htmlentities($data->id)}}","{{htmlentities($data->location_code)}}","{{$group_id}}","{{$group_name}}")' class="list_hover">
                {!! $data->location_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}