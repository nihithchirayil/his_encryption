<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var contract_no = $('#contract_no').val();
                var to_date = $('#to_date').val();
                var from_date = $('#from_date').val();
                var vendor_id = $('#vendoritem_id_hidden').val();
                var status = $('#status').val();
                var token = $('#token_hiddendata').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        contract_no: contract_no,
                        vendor_id: vendor_id,
                        to_date: to_date,
                        from_date: from_date,
                        status: status
                    },
                    beforeSend: function() {
                        $('#searchContractBtn').attr('disabled', true);
                        $('#searchContractSpin').removeClass('fa fa-search');
                        $('#searchContractSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#searchDataDiv').html(data);
                    },
                    complete: function() {
                        $('#searchContractBtn').attr('disabled', false);
                        $('#searchContractSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchContractSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
                return false;
            }
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="15%">Contract No.</th>
                    <th class="common_td_rules" width="10%">Created At</th>
                    <th class="common_td_rules" width="30%">Vendor Name</th>
                    <th class="common_td_rules" width="10%">Closing Date</th>
                    <th class="common_td_rules" width="10%">Status</th>
                    <th class="common_td_rules" width="10%">Mail Status</th>
                    <th class="common_td_rules" width="5%">View Contract</th>
                    <th class="common_td_rules" width="5%">Edit</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($contract_list) != 0) {
                foreach ($contract_list as $list) {
                    $contract_string='Created';
                    if(intval($list->approved_status)==2){
                        $contract_string='Approved';
                    }else if(intval($list->approved_status)==3){
                        $contract_string='Cancelled';
                    }
                    $mail_status='Mail Not Send';
                    if(intval($list->mail_id)!=0){
                        $mail_status='Mail Send';
                    }
                    ?>
                <tr>
                    <td id="contract_no_data<?= $list->head_id ?>" class="common_td_rules">{{ $list->contract_no }}
                    </td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->contract_date)) }}</td>
                    <td class="common_td_rules">{{ $list->vendor_name }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->closing_date)) }}</td>
                    <td class="common_td_rules">{{ $contract_string }}</td>
                    <td class="common_td_rules">{{ $mail_status }}</td>
                    <td style="text-align: center">
                        <button title="RFQ View" id="listContractItemsBtn<?= $list->head_id ?>" type="button"
                            class="btn bg-purple" onclick="listContractItems(<?= $list->head_id ?>)"><i
                                id="listContractItemsSpin<?= $list->head_id ?>" class="fa fa-list"></i>
                        </button>
                    </td>
                    <td style="text-align: center">
                        <button title="Edit RFQ List" type="button" class="btn btn-warning"
                            onclick="addWindowLoad('addpriceContract',<?= $list->head_id ?>)"><i class="fa fa-edit"></i>
                        </button>
                    </td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="8" style="text-align: center">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
