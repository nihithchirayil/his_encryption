<?php
$row_count=1;
if(count($item_array)!=0){
    foreach ($item_array as $each) {
        ?>
<tr style="background: #FFF;" class="row_class" id="row_data_{{ $row_count }}">
    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
    </td>
    <td style="text-align: center">
        @php $checked_status='';@endphp
        @if (intval($each['is_free']) == 1)
            @php $checked_status='checked';@endphp
        @endif
        <div class="checkbox checkbox-success inline no-margin" style="margin-top:4px !important">
            <input {{ $checked_status }} type="checkbox" name="is_free" id="is_free{{ $row_count }}" value="1">
            <label style="padding-left: 2px;" for="is_free{{ $row_count }}">
            </label>
        </div>
    </td>
    <td style="position: relative;">
        <input readonly style="border-radius: 4px;" type="text" required="" autocomplete="off"
            id="item_desc_{{ $row_count }}" onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
            value="<?= $each['item_desc'] ?>" class="form-control popinput" name="item_desc[]"
            placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
               position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td title="Unit converion">
        <select name="uom_select[]" id="uom_select_id_{{ $row_count }}" class="form-control">
            @php
                $uom_data = @$item_detail_array[$each['item_desc']] ? $item_detail_array[$each['item_desc']] : [];
            @endphp
            @php
                $selected = '';
            @endphp
            @if ($each['uom_name'] == @$uom_data['uom_name'] ? $uom_data['uom_name'] : '')
                @php
                    $selected = 'selected';
                @endphp
            @endif

            <option value="{{ @$uom_data['uom_id'] ? $uom_data['uom_id'] : 0 }}"
                data-uom_value="{{ @$uom_data['conv_factor'] ? $uom_data['conv_factor'] : '' }}" {$selected}>
                {{ @$uom_data['uom_name'] ? $uom_data['uom_name'] : '' }}</option>
        </select>
    </td>
    <td>
        <input class="form-control number_class" value="<?= $each['qty'] ?>" id="request_qty{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='request_qty[]'
            type="text">
        <input type='hidden' name='row_id_hidden[]' value="{{ $row_count }}"
            id="row_id_hidden{{ $row_count }}">
        <input type='hidden'
            value="<?= @$item_detail_array[$each['item_desc']]['item_id'] ? $item_detail_array[$each['item_desc']]['item_id'] : 0 ?>"
            name='item_id_hidden[]' id="item_id_hidden{{ $row_count }}">
    </td>

    <td>
        <input class="form-control number_class" autocomplete="off" id="request_unitrate{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            name='request_unitrate[]' value="<?= $each['rate'] ?>">
    </td>
    <td>
        <input class="form-control number_class" autocomplete="off" id="request_freeqty{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            name='request_freeqty[]' value="<?= $each['free_qty'] ?>">
    </td>
    <td><input class="form-control" value="<?= $each['comments'] ?>" id="comments{{ $row_count }}" name="comments[]"
            type="text"></td>
    <td style="text-align: center">
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
<?php
 $row_count++;
    }
}
?>
<input type='hidden' value="<?= $row_count ?>" id="editdatalistmaxrow">
