<html>

<head>
    <style>
        /*  @page { margin: 5px;  }
    body { margin: 5px;  }*/
        .total_table {
            width: 100%;
            border: none !important;
            font-size: 12px;
        }

        .total_table td {
            border: none;
        }

        .total_table td:last-child {
            width: 10%;
        }

        table tr,
        table td {
            page-break-inside: avoid !important;
            page-break-before: always !important;
            page-break-after: always !important;
        }

        .classtable tr {
            page-break-before: always !important;
            page-break-inside: avoid !important;
        }

        /* @page {
            size: landscape;
            position: absolute;
            margin-top: 80px;
            margin-bottom: 80px;
            margin-right: 20px;
            margin-left: 20px;
            overflow: auto;
        } */

        .table.no-border,
        .table.no-border td,
        .table.no-border th {
            border: 0;
        }

        .table_sm th,
        .table_sm td {
            font-size: 12px !important;
            padding: 1px 5px !important;
        }

        .table_sm td span {
            line-height: 18px !important;
        }

        .table_lg th,
        .table_lg td {
            padding: 10px 5px !important;
            font-size: 12px;
        }

        .table_head.tth {
            font-size: 12px !important;
            padding: 1px !important;
        }

        .table-bordered>thead>tr>th,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>tbody>tr>td,
        .table-bordered>tfoot>tr>td {
            border: 1px solid #CCC !important;
        }

        .table-col-bordered>thead>tr>th,
        .table-col-bordered>tbody>tr>th,
        .table-col-bordered>tfoot>tr>th,
        .table-col-bordered>thead>tr>td,
        .table-col-bordered>tbody>tr>td,
        .table-col-bordered>tfoot>tr>td {
            border-right: 1px solid #CCC !important;
            border-top: none;
        }

        .headerclass {
            text-align: left !important;
            background-color: #000 !important;
            color: black !important;
            font-size: 12.5px !important
        }

        th {
            background-color: #d9dfe0 !important;
        }

        .common_td_rules {
            text-align: left !important;
            overflow: hidden !important;
        }

        .td_common_numeric_rules {
            border-left: solid 1px #bbd2bd !important;
            text-align: right !important;
        }
    </style>

</head>

<body>
    <div class="row">
        <div class="col-md-12 padding_sm">
            <div class="print_data">
                <table id="result_data_table" width="100%"
                    class="table no-margin table_sm table-striped no-border styled-table" style="border: 1px solid #CCC;">
                    <thead>
                        <tr>
                            <th style="text-align: center" colspan="8">
                                <?= @$hospital_header[0]->hospital_header ? $hospital_header[0]->hospital_header : '' ?>
                            </th>
                        </tr>
                        <tr>
                            <th style="text-align: center" colspan="8">
                                <?= @$contract_status ? $contract_status : '' ?>
                            </th>
                        </tr>
                        <tr>
                            <th class="common_td_rules" colspan="4" width="50%">
                                <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?></th>
                            <th class="common_td_rules" colspan="4" width="50%">Contract No. :
                                <?= @$contract_no ? $contract_no : '' ?>
                            </th>
                        </tr>
                        <tr>
                            <th class="common_td_rules" colspan="4" width="50%">Contract From :
                                <?= @$contract_date ? $contract_date : '' ?></th>
                            <th class="common_td_rules" colspan="4" width="50%">Contract To :
                                <?= @$closing_date ? $closing_date : '' ?></th>
                        </tr>
                        <tr>
                            <th class="common_td_rules" colspan="8" width="100%">Address :
                                <?= @$vendor_data['vendor_address'] ? $vendor_data['vendor_address'] : '' ?>
                            </th>
                        </tr>
                        <tr>
                            <th class="common_td_rules" colspan="4" width="50%">Email :
                                <?= @$vendor_data['vendor_email'] ? $vendor_data['vendor_email'] : '' ?></th>
                            <th class="common_td_rules" colspan="4" width="50%">Telephone No. :
                                <?= @$vendor_data['contact_no'] ? $vendor_data['contact_no'] : '' ?>
                            </th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width="5%">SL.No.</th>
                            <th width="25%"><b>Item Description</b></th>
                            <th width="10%"><b>Is Free</b></th>
                            <th width="10%"><b>Unit</b></th>
                            <th width="10%"><b>Qty.</b></th>
                            <th width="10%"><b>Rate</b></th>
                            <th width="10%"><b>Free Qty.</b></th>
                            <th width="20%"><b>Comments</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i=1;
                            if(count($fetchitem)!=0){
                                foreach ($fetchitem as $each) {
                                    ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td class="common_td_rules"> <?= @$each['item_desc'] ? $each['item_desc'] : '' ?></td>
                            <td class="common_td_rules"> <?= @$each['is_free'] ? 'Free' : '-' ?></td>
                            <td class="common_td_rules"> <?= @$each['uom_text'] ? $each['uom_text'] : '' ?></td>
                            <td class="common_td_rules"> <?= @$each['request_qty'] ? $each['request_qty'] : '' ?>
                            </td>
                            <td class="common_td_rules"> <?= @$each['unit_rate'] ? $each['unit_rate'] : '' ?></td>
                            <td class="common_td_rules"> <?= @$each['free_qty'] ? $each['free_qty'] : '' ?></td>
                            <td class="common_td_rules"> <?= @$each['comments'] ? $each['comments'] : '' ?></td>

                        </tr>
                        <?php
                            $i++;
                                }
                                        }else {
                                            ?>
                        <tr>
                            <td colspan="8" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                        <?php
                                        }
                                        ?>

                        <tr>
                            <th colspan="8" class="common_td_rules">
                                GST NO. <?= @$hospital_header[0]->gst_no ? $hospital_header[0]->gst_no : '' ?>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="8" class="common_td_rules">
                                <b>Buyer : </b>
                                <?= @$hospital_header[0]->address ? $hospital_header[0]->address : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <b>PO Terms And Conditions: </b> <?= @$full_remarks ? $full_remarks : '' ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
