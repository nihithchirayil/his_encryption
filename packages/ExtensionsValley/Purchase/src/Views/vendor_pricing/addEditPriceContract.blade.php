@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')


    <div class="modal fade" id="getPoTermsConditionsModel" data-keyboard="false" data-backdrop="static" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Contract Terms And Conditions</h4>
                </div>
                <div class="modal-body" style="min-height: 500px">
                    <div class="col-md-7 padding_sm">
                        <label class="custom_floatlabel"><strong> Payment Terms </strong></label>
                        <textarea cols="10" rows="10" name="payment_terms" id="payment_terms" class="form-control"></textarea>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <div id="getPoTermsConditionsDiv"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <hr>
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="contract_print_model" data-keyboard="false" data-backdrop="static" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button onclick="reloadToList()" type="button" class="close close_white" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Contract Preview</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div id="contract_print_model_div"></div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" onclick="reloadToList()" type="button" data-dismiss="modal"
                        aria-label="Close" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button onclick="DownloadVendorContract(1)" style="padding: 3px 3px;" type="button"
                        id="DownloadVendorContractBtn" class="btn btn-success">PDF <i id="DownloadVendorContractSpin"
                            class="fa fa-file"></i>
                    </button>
                    <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                        class="btn btn-primary">Print <i class="fa fa-print"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" role="main">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" name="row_count" id="row_count_id" value="0">
        <input type="hidden" name="contract_id" id="contract_id" value="<?= $contract_id ?>">
        <input type="hidden" id="location_defaultvalue" value="">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <a id="downloadPoPFDlinka" style="display: none" href="" download="">
            <img id="downloadPoPFDlinkimg">
        </a>
        <input type="hidden" id="loadPrintData" value="0">
        <div class="row padding_sm">
            <div class="col-md-6 padding_sm">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 136px;">
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label style="margin-top:-3px">Supplier <span style="color: red">*</span></label>
                            <input style="position: relative;" type="text" autocomplete="off" required=""
                                id="item_supplier" class="form-control popinput auto_focus_cls"
                                placeholder="Search Supplier">
                            <div class="ajaxSearchBox" id="ajaxSearchBox_supplier" index="1"
                                style="top: 10px !important;max-height: 290px;margin-top:33px;margin-left:-4px"> </div>
                            <input type="hidden" value="" id="vender_id">
                            <input type="hidden" value="" id="vender_code">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label style="margin-top:-3px">Contract Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="contract_date"
                                value="<?= date('M-d-Y') ?>" class="form-control filters datepicker"
                                placeholder="MM-DD-YYYY">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label style="margin-top:-3px">Closing Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="closing_date"
                                value="<?= $closing_date ?>" class="form-control filters datepicker"
                                placeholder="MM-DD-YYYY">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label style="margin-top:-3px">Contract No.</label>
                            <input type="text" readonly style="background: #fff" id="contract_number"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label style="margin-top:-3px">Contract Status</label>
                            <input type="text" readonly style="background: #fff" id="contract_status"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">Remarks</label>
                            <input name="contract_remarks" id="contract_remarks" class="form-control" autocomplete="off"
                                type="text" style="border-color: rgb(239, 239, 239);">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 60px;">
                        <form id="uploadVendorContractForm">
                            <div class="col-md-12 padding_sm">
                                <div class="clearfix"></div>
                                <input style="height:25px !important" type="file" class="form-control btn bg-green"
                                    name="vendor_document" id="vendor_document">
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="clearfix"></div>
                                <button onclick="downloadVendorExcelContract()" id="downloadDocumentBtn" type="button"
                                    class="btn btn-success btn-block" title="Upload">Download <i
                                        id="downloadDocumentSpin" class="fa fa-download"></i></button>
                                <a style="display: none" id="bulk_download_link" href="" download></a>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="clearfix"></div>
                                <button id="uploadVendorDocumentBtn" class="btn btn-success btn-block" type="submit"
                                    title="Upload">Upload <i id="uploadVendorDocumentSpin"
                                        class="fa fa-upload"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12 padding_sm" style="margin-top:6px">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 60px;">
                        <div class="col-md-4 padding_sm savegrnDiv" id="sendMailDiv" style="margin-top: 13px">
                            <div class="clearfix"></div>
                            <button id="contractSendMailBtn" onclick="DownloadVendorContract(2)"
                                class="btn btn-success btn-block" type="button" title="Upload">Send Mail <i
                                    id="contractSendMailSpin" class="fa fa-envelope"></i></button>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top: 13px">
                            <div class="clearfix"></div>
                            <button id="contractPreviewBtn" onclick="getPrintPreview(1)"
                                class="btn btn-success btn-block" type="button" title="">Preview <i
                                    id="contractPreviewSpin" class="fa fa-eye"></i></button>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top: 13px">
                            <div class="clearfix"></div>
                            <button id="vendor_contractterms_btn" onclick="getPoTermsConditions();"
                                class="btn btn-success btn-block" type="button" title="">Contract Terms <i
                                    id="vendor_contractterms_spin" class="fa fa-gavel"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 136px;">
                    <div style="margin-top: 23px">
                        <div class="col-md-6 padding_sm">
                            <a type="reset" href="{{ route('extensionsvalley.quotation.addpriceContract') }}">
                                <button class="btn btn-warning btn-block disable_on_save">Reload <i
                                        class="fa fa-refresh"></i></button>
                            </a>
                        </div>
                        <div class="col-md-6 padding_sm savegrnDiv" id="savegrnDiv5">
                            <button type="button" onclick="saveVendorPriceContract(3,1,5);" name="cancel"
                                value="cancel" id="savegrnBtn5" class="btn btn-danger btn-block disable_on_save">Cancel
                                <i id="savegrnSpin5" class="fa fa-save"></i></button>
                        </div>
                        <div class="col-md-6 padding_sm savegrnDiv" id="savegrnDiv1">
                            <button type="button" id="grn_save_btn" onclick="saveVendorPriceContract(1,0,1);"
                                name="save" value="save" id="savegrnBtn1"
                                class="btn btn-success btn-block saveForm disable_on_save">Save <i id="savegrnSpin1"
                                    class="fa fa-save"></i></button>
                        </div>
                        <div class="col-md-6 padding_sm savegrnDiv" id="savegrnDiv2">
                            <button type="button" id="grn_save_print_btn" onclick="saveVendorPriceContract(1,1,2);"
                                id="savegrnBtn2" value="save" class="btn btn-success btn-block disable_on_save">Save
                                &amp; Print <i id="savegrnSpin2" class="fa fa-save"></i></button>
                        </div>
                        @if (!empty($accessPerm['approve']) && $accessPerm['approve'] == 1)
                            <div class="col-md-6 padding_sm savegrnDiv" id="savegrnDiv3">
                                <button type="button" onclick="saveVendorPriceContract(2,0,3);" name="approve"
                                    value="approve" id="savegrnBtn3"
                                    class="btn btn-success btn-block disable_on_save">Approve <i id="savegrnSpin3"
                                        class="fa fa-save"></i></button>
                            </div>
                            <div class="col-md-6 padding_sm savegrnDiv" id="savegrnDiv4">
                                <button type="button" onclick="saveVendorPriceContract(2,1,4);" name="approve"
                                    value="approve" id="savegrnBtn4"
                                    class="btn btn-success btn-block disable_on_save">Approve &amp;
                                    Print <i id="savegrnSpin4" class="fa fa-save"></i></button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 500px;">
                    <div class="theadscroll" style="position: relative; height: 500px;">
                        <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
                            style="border: 1px solid #CCC;" id="main_row_tbl">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th style="text-align: center" width="3%">Sl.No.</th>
                                    <th style="text-align: center" width="3%" title="Free item"><i
                                            class="fa fa-list"></i>
                                    </th>
                                    <th style="text-align: center" width="30%">Item </th>
                                    <th style="text-align: center" width="10%">Unit</th>
                                    <th style="text-align: center" width="8%">Qty</th>
                                    <th style="text-align: center" width="8%">Rate</th>
                                    <th style="text-align: center" width="8%">Free Qty</th>
                                    <th style="text-align: center" width="26%">Comments</th>
                                    <th style="text-align: center" width="3%"><button type="button"
                                            style="margin-top: 5px;" id="addQuotationRowbtn"
                                            onclick="getNewRowInserted();" class="btn btn-primary add_row_btn"><i
                                                id="addQuotationRowSpin" class="fa fa-plus"></i></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="row_body_data">

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/addVendorPricing.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
