<div class="theadscroll" style="position: relative; height: 510px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">SL.No.</th>
                <th width="25%"><b>Item Description</b></th>
                <th width="10%"><b>Is Free</b></th>
                <th width="10%"><b>Unit</b></th>
                <th width="10%"><b>Qty.</b></th>
                <th width="10%"><b>Rate</b></th>
                <th width="10%"><b>Free Qty.</b></th>
                <th width="20%"><b>Comments</b></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $row_count=1;
            if(count($res)!=0){
                foreach ($res as $each) {
                    ?>
            <tr style="background: #FFF;" class="row_class">
                <td class='common_td_rules'>{{ $row_count }}</td>
                <td class="common_td_rules">
                    <?= $each->item_desc ?>
                </td>
                <td class="common_td_rules">
                    <?= intval($each->free_item == 1) ? 'Free Item' : '-' ?>
                </td>
                <td class="common_td_rules">
                    <?= $each->uom_name ?>
                </td>
                <td class="td_common_numeric_rules">
                    <?= $each->quantity ?>
                </td>
                <td class="td_common_numeric_rules">
                    <?= $each->unit_rate ?>
                </td>
                <td class="td_common_numeric_rules">
                    <?= $each->free_qty ?>
                </td>
                <td class="common_td_rules">
                    <?= $each->comments ?>
                </td>
            </tr>
            <?php
                $row_count++;
                }
            }else{
                ?>
            <tr>
                <td colspan="8" style="text-align: center">No Result Found</td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
