@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="modal fade" id="quotationListModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="quotationListHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="col-md-12 padding_sm" id="quotationListDiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col" role="main">
        <div class="row padding_sm">
            <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                <thead>
                    <tr class="table_header_bg">
                        <th colspan="11"><?= strtoupper($title) ?>
                        </th>
                    </tr>
                </thead>
            </table>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">

                    <div class="row codfox_container">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 no-padding">
                                <div class="box no-border">
                                    <div class="box-body clearfix">
                                        <form action="{{ route('extensionsvalley.purchase.grnList') }}"
                                            id="requestSearchForm" method="POST">
                                            {!! Form::token() !!}
                                            <div class="col-md-1 padding_sm">
                                                <div class="mate-input-box">
                                                    <label for="">From Date</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control datepicker" name="from_date"
                                                        id="from_date" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <div class="mate-input-box">
                                                    <label for="">To Date</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control datepicker" name="to_date"
                                                        id="to_date" value="">
                                                </div>
                                            </div>

                                            <div class="col-md-2 padding_sm ">
                                                <div class="mate-input-box">
                                                    <label style="margin-top: -2px;">Contract No.</label>
                                                    <div class="clearfix"></div>
                                                    <input style="border-radius: 4px;" type="text" required=""
                                                        autocomplete="off" id="searchContract" onkeyup="searchContractNo()"
                                                        class="form-control popinput" placeholder="Contract No.">
                                                    <input type="hidden" id="contract_id_hidden" value="0">
                                                    <div class="ajaxSearchBox" id="ajaxContractSearchBox"
                                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                                                   position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label style="margin-top:-3px">Supplier </label>
                                                    <input style="position: relative;" type="text" autocomplete="off"
                                                        required="" id="item_supplier"
                                                        class="form-control popinput auto_focus_cls"
                                                        placeholder="Search Supplier">
                                                    <div class="ajaxSearchBox" id="ajaxSearchBox_supplier" index="1"
                                                        style="top: 10px !important;max-height: 290px;margin-top:33px;margin-left:-4px">
                                                    </div>
                                                    <input type="hidden" value="" id="vender_id">
                                                    <input type="hidden" value="" id="vender_code">
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label style="margin-top: -4px;">Status</label>
                                                    <div class="clearfix"></div>
                                                    {!! Form::select(
                                                        'status',
                                                        ['' => ' Select Status', '1' => 'Created', '2' => 'Approved', '3' => 'Cancelled'],
                                                        $searchFields['status'] ?? null,
                                                        [
                                                            'class' => 'form-control status select2',
                                                            'id' => 'status',
                                                            'onchange' => 'searchQuotation()',
                                                        ],
                                                    ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning">Clear <i
                                                        class="fa fa-recycle"></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                <button type="button" id="searchQuotationBtn"
                                                    onclick="searchQuotation()" class="btn btn-block btn-primary"><i
                                                        id="searchQuotationSpin" class="fa fa-search"></i>
                                                    Search</button>
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                <div onclick="addWindowLoad('addpriceContract','','')"
                                                    class="btn btn-block btn-success"><i class="fa fa-plus"></i>
                                                    Create Vendor Pricing
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="searchDataDiv">

                </div>
            </div>
        </div>

    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/vendorContractList.js') }}"></script>
@endsection
