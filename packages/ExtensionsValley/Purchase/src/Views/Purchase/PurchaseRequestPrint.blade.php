
    <head>
        <style>
            table tbody tr td{
                border: 1px solid #b9b8b8;
            }
            .table>thead>tr>th{
                border: 1px solid #b9b8b8;
            }
            .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
             border: 1px solid #b9b8b8;
            }
            .text_font-size {
                font-size: 14px
            }
     /* div.footer {
                position:fixed;
                bottom:0px;
                left:0px;
                width:100%;
                color:#CCC;
                background:#333;
                padding:9px;
            }*/



            .total_table{
                width: 100%;
    border: none !important;
    font-size: 12px;
            }
.total_table td{
    border: none;
}
.total_table td:last-child{
    width: 10%;
}

@page
{
    size: auto;
    margin: 25mm 0mm;
}


        </style>
    </head>
    <body>
        <?php
        $bill_disc = 0;
        $hospital_details = \DB::table('company')->first();

        $single_tax_status = 0;

        ?>
        <a><button type="button" data-dismiss="modal" class="btn btn-success" style="float:right;margin-left: 7px;">Close</button></a>&nbsp;
        <input type="button" value="Print" name="preview_print" id="preview_print" class="btn btn-primary" onclick="PrintPO();" style="margin-bottom:4px;float:right;"> &nbsp;&nbsp;
        <div class="getPoContent">

        <table border="1" width="100%" style="font-size: 12px; border-collapse: collapse; max-width: 100%; margin-bottom: 2px;" class="table table-striped  table-condensed  sorted_table">
            <thead>
                <tr class="header">
                    <th colspan="18" style="padding: 0px;display: none"  id="print_header">
                        <table style="width: 100%; font-size: 12px;">
                        </table>
                    </th>
                </tr>
                <tr class="header">
                    <th colspan="18">
                        <h4 align="center" style="font-size:23px;margin:0"><b>Purchase Order</b></h4>
                    </th>
                </tr>
                <tr class="header">
                    <th colspan="7" align="left">
                        <b>M/s.{{$resultData0[0]->vendor_name}}</b>
                    </th>
                    <th colspan="11" align="left">
                        <b>PO No : {{$resultData0[0]->po_no}}
                            <!-- /Fin Year :
                            @if($resultData4[0]->financial_year)
                                {{$resultData4[0]->financial_year}}
                            @endif -->
                        </b>
                    </th>
                </tr>
                <tr class="header">
                    <th colspan="7" align="left">Address:  @if($resultData0[0]->address_1){{$resultData0[0]->address_1}},@endif
            @if($resultData0[0]->address_2){{$resultData0[0]->address_2}},@endif
            @if($resultData0[0]->address_3){{$resultData0[0]->address_3}}@endif</th>
                    <th colspan="11" align="left">
                       <b> P.O.DATE : {{$resultData0[0]->po_date}}</b>
                    </th>
                </tr>
                <tr class="header">
                    <th colspan="7" align="left">
                      <!-- <b>  Telephone No.:{{$resultData0[0]->contact_no}}</b> -->
                        <b>Email :{{$resultData0[0]->email}}</b>
                    </th>
                    <th colspan="11" align="left">
                      <b>  Department : {{$resultData0[0]->dep_name}}</b>
                    </th>
                </tr>
                <tr class="header">
                    <th colspan="7" align="left">
                        <!-- <b>GSTIN :{{$resultData0[0]->gst_vendor_code}}</b> -->
                        <b> Telephone No.:{{$resultData0[0]->contact_no}}</b>
                    </th>
                    <th colspan="11" align="left">
                      <b>  Location : {{$resultData0[0]->location_name}}</b>
                    </th>
                </tr>
                <tr class="header">
                    <th colspan="7" align="left">
                        <!--<b>GSTIN :{{$resultData0[0]->gst_vendor_code}}</b>-->
                    </th>
                    <th colspan="11" align="left">

                    </th>
                </tr>
            </thead>


        </table>
<table border="1" width="100%" style="font-size: 12px; border-collapse: collapse;max-width: 100%; margin-bottom: 5px;" class="table table-striped  table-condensed  sorted_table">
             <thead>
                                 <tr class="headergroupbg">

                        <th width="1%" align="left">SL.<br>No.</th>
                        <th width="4%" align="left"><b>HSN</b></th>

                        <th width="50%" align="left" ><b>Item Name</b></th>
                        <th width="7%" align="right"><b>Qty</b></th>
                        <th width="4%" align="right"><b>Free <br> Qty</b></th>
                        <th width="4%" align="right"><b>Unit</b></th>
                         @if($last_pur_mode == 'true')
                          <th width="4%" align="right"><b>Mrp</b></th>
                         @endif
                        <th width="4%" align="right"><b>Rate</b></th>
                        <th width="3%" align="right"><b>Bill<br>Disc.<br>(%)</b></th>
                        <th width="3%" align="right"><b>Disc.<br> (%)</b></th>
                        <!-- <th width="3%" align="right"><b>VAT<br> (%)</b></th> -->
                        <?php   if ($single_tax_status == '1') { ?>
                        <th width="9%" align="right"><b>TAX<br> (%)</b></th>
                        <?php   }else{ ?>
                        <th width="3%" align="right"><b>CGST<br> (%)</b></th>
                        <th width="3%" align="right"><b>SGST <br> (%)</b></th>
                        <th width="3%" align="right"><b>IGST<br> (%)</b></th>
                           <?php } ?>

                        @if($last_pur_mode == 'true')
                            <th width="3%" align="right"><b>Mrp <br>(Last Pur)</b></th>
                            <th width="3%" align="right"><b>Cost <br>(Last Pur)</b></th>
                            <th width="3%" align="right"><b>Rate <br>(Last Pur)</b></th>
                        @endif
                        <th width="3%"><b>Cost<br>(Per Qty)</b></th>
                        <th width="3%" align="right"><b>Item Value</b></th>

                </tr>
            </thead>
              <tbody>
                <?php $i = 1; $total_val = $total_disc =  $total_cgst =  $total_sgst = $total_igst = 0;$tot_other=0;$total_tcs = 0; ?>
                @foreach($resultData1 as $key => $val)
                <?php
                    $cost_per=0;



                    $cgst_per =!empty($val->cgst_perc)?$val->cgst_perc:0;
                    $sgst_perc =!empty($val->sgst_perc)?$val->sgst_perc:0;
                    $igst_perc =!empty($val->igst_perc)?$val->igst_perc:0;

                    $cost_per = $val->item_total/($val->pur_qnty+$val->free_qty);
                    if($val->pur_qnty< 1){
                        $cost_per = $val->item_total;
                    }
                    //$cost_per = $purchase_cost_with_disc * ($cgst_per+$sgst_perc+$igst_perc)/100+$val->pur_rate;
                ?>
                    <tr>
                        <td align="left">{{$i}}</td>
                        <td align="left">{{$val->hsn_code}}</td>
                        <td align="left">{{htmlspecialchars($val->item_desc)}}</td>

                        <td align="right">{{intval($val->pur_qnty)}}</td>
                        <td align="right">{{intval($val->free_qty)}}</td>
                        <td align="left">{{$val->unit_name}}</td>

                          @if($last_pur_mode == 'true')
                              <td align="right">{{number_format((float)$val->mrp, 2, '.', '')}}</td>
                           @endif
                            <td align="right">{{number_format((float)$val->pur_rate, 2, '.', '')}}</td>

                            <td align="right">
                                @if($val->discount_cal_mode == 2)
                                    {{number_format((float)$val->bill_discount_value, 2, '.', '')}}
                                @else
                                    {{number_format((float)$val->item_dis_prc, 2, '.', '')}}
                                @endif
                            </td>
                            <td align="right">{{number_format((float)$val->discount_perc, 2, '.', '')}}</td>
                            <!-- <td align="right">{{number_format((float)$val->vat_perc, 2, '.', '')}}</td> -->
                             <?php   if ($single_tax_status == '1') { ?>
                            <td align="right">{{number_format((float)$val->cgst_perc, 2, '.', '')}}</td>
                                <?php   }else{ ?>
                            <td align="right">{{number_format((float)$val->cgst_perc, 2, '.', '')}}</td>
                            <td align="right">{{number_format((float)$val->sgst_perc, 2, '.', '')}}</td>
                            <td align="right">{{number_format((float)$val->igst_perc, 2, '.', '')}}</td>
                                  <?php } ?>
                            @if($last_pur_mode == 'true')
                                <td align="right">{{number_format((float)$val->grn_mrp, 2, '.', '')}}</td>
                                <td align="right">{{number_format((float)$val->grn_cost, 2, '.', '')}}</td>
                                <td align="right">{{number_format((float)$val->grn_rate, 2, '.', '')}}</td>
                            @endif
                            <td align="right">{{number_format((float)$cost_per, 2, '.', '')}}</td>
                            <td align="right">{{$val->pur_qnty*$val->pur_rate}}</td>
                    </tr>
                    <?php
                        $i++;
                        if($val->discount_cal_mode == 2){
                            $bill_disc = $val->bill_discount_amount;
                        }else{
                            $bill_disc += $val->item_dis_amt;
                        }
                        $total_val += $val->pur_qnty*$val->pur_rate;
                        $total_disc += $val->discount_val;
                        $tot_other += $val->frt_val+ $val->oth_val;
                        $total_cgst += $val->cgst_val;
                        $total_sgst += $val->sgst_val;
                        $total_igst += $val->igst_val;



                    ?>
                @endforeach
                  </tbody>
</table>
<table border="1" width="100%" style="font-size: 12px; border-collapse: collapse;max-width: 100%;" class="table table-striped  table-condensed  sorted_table">
             <tbody>
                <tr>
                    <td colspan="18" align="right">
                    <table class="total_table" width="100%" style="font-size: 12px; border-collapse: collapse;">
                        <tbody>
                            <tr>
                                <td style="text-align: right;" colspan="2"> Item Discount(-) : </td>
                                <td style="text-align: right;"><b>{{number_format((float)$total_disc,2, '.', '')}}</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" colspan="2">Bill Discount(-) : </td>
                            <td style="text-align: right;"><b>{{number_format((float)$bill_disc,2, '.', '')}}</b></td>
                        </tr>

                        <?php   if ($single_tax_status == '1') { ?>
                            <tr>
                                <td style="text-align: right;" colspan="2">TAX :</td>
                                <td style="text-align: right;"><b>{{$total_cgst}}</b></td>

                            </tr>
                          <?php   }else{ ?>
                            <tr>
                         <td style="text-align: right;" colspan="2">Other Charges : </td>
                         <td style="text-align: right;"><b>{{number_format((float)$tot_other,2, '.', '')}}</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" colspan="2">CGST :</td>
                            <td style="text-align: right;"><b>{{$total_cgst}}</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" colspan="2">SGST :</td>
                                <td style="text-align: right;"><b>{{$total_sgst}}</b></td>

                        </tr>
                        <tr>
                            <td style="text-align: right;" colspan="2">IGST : </td>

                            <td style="text-align: right;"><b>{{$total_igst}}</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" colspan="2">Total Value :</td>
                            <td style="text-align: right;"><b>{{$total_val}} </b></td>
                            </tr>

                            <?php } ?>


                        </tbody>
                    </table>

                    </td>
                </tr>
                <!-- <tr>
                    <td colspan="16" align="left"><b>Total Amount In Words:</b></td>
                </tr>
                <tr>
                   <td  colspan="16" align="left">
                   <?php
                        //echo  $number_to_word =  \ExtensionsValley\Master\CommonController::number_to_word($resultData0[0]->net_total);
                    ?>
                    </td>
                </tr> -->

                    {{-- <tr>
                        <td colspan="18" align="left"><b>Payment :</b> {{isset($resultData0[0]->payment_terms)?
                        $resultData0[0]->payment_terms:''}}</td>
                    </tr>
                    <tr>
                        <td colspan="18" align="left" height="20">
                        <b>Warranty :</b>
                        @if(isset($resultData0[0]->warranty_terms))
                            {{$resultData0[0]->warranty_terms}}
                        @endif
                        </td>
                    </tr> --}}

                    <tr>
                        <td align="left"><b>Remarks </b></td>
                        <td colspan="17" align="left">
                            <p style=" word-break: break-all;margin-bottom: 0px;margin-top: 0px;">{{$resultData0[0]->remarks}}</p>
                        </td>
                    </tr>
                    <tr>
                        {{-- <td align="left"><b>Terms & Conditions </b></td> --}}
                        <td colspan="17" align="left">
                            <ul style="list-style: none;padding-left: 10px;margin-bottom: 0px;margin-top: 0px;">
                                <?php
                                if(count($resultData2) > 0) {
                                    foreach($resultData2 as $k => $v) { ?>
                                        <li>*{{$v->value}}</li>
                                    <?php }
                                } ?>
                            </ul>
                        </td>
                    </tr>



                    <tr>
                       <td colspan="18" align="left">
                        <b>GST No :</b>{{$hospital_details->gst_no}}   ,<b>Drug License No : </b>{{$hospital_details->drug_licence_no}},
                        <b>PAN No : </b> {{$hospital_details->pan_no}}
                       </td>
                    </tr>

            </tbody>
            <tfoot>

                <tr class="footer">
                    <th colspan="5" align="left" style="white-space: normal;">
                        <p><b>Seller Details</b></p>
                        <p>
                            <?php
                                if(count($resultData3) > 0) {
                                    foreach($resultData3 as $k => $v) {
                                        if($v->type == 1)
                                        echo html_entity_decode($v->value);
                                    }
                                } ?>
                        </p>
                        <p><b>Vendor: {{$resultData0[0]->vendor_name}}</b></p>
                    </th>

                    <th colspan="13" align="left" style="text-align: left; font-size: 12px;word-break: break-all;white-space: normal;">
                        <p><b>Buyer</b></p>
                            <p><b>{{$hospital_details->city}},{{$hospital_details->address}},{{$hospital_details->name}}</b></p>

                            <?php
                                if(count($resultData3) > 0) {
                                    foreach($resultData3 as $k => $v) {
                                        if($v->type == 2)
                                         echo html_entity_decode($v->value);
                                     }
                                } ?>
                    </th>
                </tr>
                <tr class="footer">

                        <?php
                            $user_id = !empty($resultData0[0]->approved_by_id) ? $resultData0[0]->approved_by_id : 0;

                        ?>
                        <th colspan="5" align="left">
                            Approved By : {{ ucfirst($resultData0[0]->approved_by) }}

                                @if(isset($resultData0[0]->approved_at) && !empty($resultData0[0]->approved_at))({{ $resultData0[0]->approved_at }}) @endif
                                        @if(!empty($resultData0[0]->signature_name))
                                        <br>
                                            <img id="sign" alt="" width="100" height="50" style="margin:0 100px"
                                            src="{{URL('packages/visits/uploads/signature/').'/'.$resultData0[0]->signature_name}}" />
                                        @endif
                        </th>
                        <th colspan="13" align="left">
                            Verified By : {{ ucfirst($resultData0[0]->verified_by) }}
                                @if(isset($resultData0[0]->verified_at) && !empty($resultData0[0]->verified_at))({{ $resultData0[0]->verified_at }}) @endif
                        </th>
                </tr>
                <tr class="footer">
                    <th colspan="4" align="left">Prepared By : {{ ucfirst($resultData0[0]->created_by) }}

                        @if(isset($resultData0[0]->created_at) && !empty($resultData0[0]->created_at))({{ $resultData0[0]->created_at }}) @endif
                    </th>
                    <th colspan="5" align="left">Printed By: {{ ucfirst(\Auth::user()->name) }}</th>
                    <th colspan="9" align="left">Print Date Time : {{date('d-m-Y')}}</th>
                </tr>
            </tfoot>

</table>
    </div>
    <a><button type="button" data-dismiss="modal" class="btn btn-success" style="margin-bottom:4px;float:right;margin-left: 7px;">Close</button></a>&nbsp;
    <input type="button" value="Print" name="preview_print" id="preview_print" class="btn btn-primary" onclick="PrintPO();" style="margin-bottom:4px;float:right">
</body>
