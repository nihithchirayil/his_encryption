@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_old.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <?php
    if (isset($default_state_id[0]->value) && !empty($default_state_id[0]->value)) {
        $default_state_id = $default_state_id[0]->value;
    } else {
        $default_state_id = '';
    }

    ?>
    <div class="right_col" style="min-height: 714px !important" role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <div class="nav nav-tabs sm_nav text-center list_tab">
                    <input type="hidden" value='{{ $config_data }}' id="config_details_hidden">
                    <input type="hidden" value='0' id="add_edit_status">
                    <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                    <input type="hidden" value='{{ $unit_master }}' id="hidden_unit_master">
                    <input type="hidden" value='' id="hidden_status">
                    <input type="hidden" value='0' id="hiddensearch_code_expression">
                    <ul class="nav nav-tabs primary_nav">
                        <li style="display: none;width:100%" id="genarate_poli"><span
                                style="font-weight: 700;color: #ebad29;font-size: 16px;"><b>PO Status: </b></span> <b
                                style="color:#21760c;font-size: 16px;" id="po_status"></b></a>
                            {{-- <label class="form-control custom_floatinput" style="background-color: #eee;font-size: 13px !important;"></label> --}}

                        </li>
                    </ul>

                    <div class="tab1-content2" style="min-height: 600px;">


                        <div id="desc_amount_modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Product Extra Info</h4>

                                    </div>
                                    <div class="modal-body">
                                        <div id='model_charges_div'>

                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn light_purple_bg"><i
                                                    class="fa fa-save"></i> OK</button>
                                            {{-- <button class="btn light_purple_bg"><i class="fa fa-save"></i> Add</button> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="genarate_po" class="tab-pane1 active">
                            <div class="panel-body" style="padding: 0px;">
                                <div class="panel panel-default">
                                    <div class="row">
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <div class="tab_pop_container" id="get_otherchargespopup" style="display: none">

                                        </div>

                                        <div id="poplast_item_box_div" class="poplast_item_box" style="display: none;">

                                        </div>

                                        <div id="pop_up_rate_sync" class="pop_up_rate_sync" style="display: none;">

                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>

                                        <div class="col-md-12" style="min-height: 138px;">
                                            <div class="col-md-12">


                                                <div class="col-md-2 pull-right" id="po_mail_div1" style="display:none">
                                                    <div class="custom-float-label-control">
                                                        <label class="form-control custom_floatinput"
                                                            style="background-color: #eee;font-size: 13px !important;">
                                                            <b id="po_mail"></b>
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-md-3 padding_sm">
                                                <div class="box no-border no-margin">
                                                    <div class="box-body clearfix">

                                                        <div class="col-md-12 no-padding" style="height: 110px">

                                                            <div class="col-md-12 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">Supplier <span
                                                                            style="color: red">*</span></label></label>
                                                                    <input type="text" name="vendor_list_po2" value=""
                                                                        class="form-control" autocomplete="off"
                                                                        id="vendor_list_po2" placeholder="">
                                                                    <span
                                                                        style="color: #d14;">{{ $errors->first('vendor_list_po2') }}</span>
                                                                    <div id="vendorListAjaxDiv2" class="ajaxSearchBox"
                                                                        style="margin: 0; width: 100%; z-index: 9999;">
                                                                    </div>
                                                                    <input type="hidden" name="vendor_list_po_hidden2"
                                                                        value="" id="vendor_list_po_hidden2">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">Manufacturer</label>
                                                                    <?php

                                                                    $vendor_list = \DB::table('manufacturer')
                                                                        ->where('status', 1)
                                                                        ->orderBy('manufacturer_name', 'asc')
                                                                        ->pluck('manufacturer_name', 'manufacturer_code');
                                                                    ?>
                                                                    {!! Form::select('manufacturer_list', $vendor_list, '', ['class' => 'form-control custom_floatinput selectbox_initializr', 'placeholder' => 'Select Manufacturer', 'title' => 'Select Manufacturer', 'id' => 'manufacturer_list', 'style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">Location</label>
                                                                    <?php
                                                                    $user_id = \Auth::user()->id;
                                                                    $default_location = \DB::table('users')
                                                                        ->select('default_location')
                                                                        ->where('id', $user_id)
                                                                        ->first();
                                                                    $user_id = \Auth::user()->id;
                                                                    $roleData = [];

                                                                    $roleData = $roleData = \DB::table('user_group')
                                                                        ->where('user_id', $user_id)
                                                                        ->where('status', 1)
                                                                        ->pluck('group_id');
                                                                    $listdept = $to_location = \DB::table('location as l')
                                                                        ->leftJoin('location_group as r', 'l.id', '=', 'r.location_id')
                                                                        ->whereIn('r.group_id', $roleData)
                                                                        ->whereNotIn('l.location_type', ['-1', '10'])
                                                                        ->where('is_store', 1)
                                                                        ->orderBy('location_name')
                                                                        ->pluck('l.location_name', 'l.location_code');
                                                                    ?>
                                                                    {!! Form::select('deprt_list', $listdept, $default_location->default_location, ['class' => 'form-control custom_floatinput ', 'id' => 'deprt_list', 'style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div style="display: none;" class="col-md-12 no-padding">
                                                            <div class="col-md-12 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel"></label>
                                                                    <div class="custom-float-label-control">
                                                                        <div class="clearfix"></div>
                                                                        <input class="form-control custom_floatinput"
                                                                            id="requisition_no"
                                                                            placeholder="Requisition No."
                                                                            name="requisition_no" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3 padding_sm">
                                                <div class="box no-border no-margin">
                                                    <div class="box-body clearfix">
                                                        <div class="col-md-12 no-padding">
                                                            <div class="col-md-6 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">PO</label>
                                                                    <input class="form-control custom_floatinput"
                                                                        id="po_added" placeholder="PO" name="po_added"
                                                                        type="text" readonly="">
                                                                    <input type="hidden" id="is_amended" value='0' />
                                                                    <input type="hidden" id="is_po_status" value='0' />
                                                                    <input type="hidden" id="parent_po_status" value='0' />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">PO Date</label>
                                                                    <div class="clearfix"></div>
                                                                    <input class="form-control custom_floatinput"
                                                                        value="{{ $current_date }}" readonly id="po_date"
                                                                        placeholder="PO Date" name="po_date" type="text">
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="col-md-6 no-padding">
                                                            <div class="col-md-12 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">Payment Terms</label>
                                                                    <input name="payment_terms" value=" " id="payment_terms"
                                                                        class="form-control" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 no-padding">
                                                            <div class="col-md-12 padding_sm">
                                                                <div class="custom-float-label-control mate-input-box">
                                                                    <label class="custom_floatlabel">Remarks</label>
                                                                    <input name="payment_remarks" id="payment_remarks"
                                                                        class="form-control" autocomplete="off"
                                                                        type="text">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box no-border no-margin">
                                                    <div class="box-body clearfix">
                                                        <div class="col-md-6 padding_sm">
                                                            <div class="custom-float-label-control mate-input-box">
                                                                <label class="custom_floatlabel">Disc.Mode</label>
                                                                <select class="form-control" id="disc_mode"
                                                                    name="disc_mode"
                                                                    style="color:#555555; padding:4px 12px;">
                                                                    {{-- <option value="1">Item Wise</option> --}}
                                                                    <option value="2">On Net Amt</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 padding_sm">
                                                            <div class="custom-float-label-control mate-input-box">
                                                                <label class="custom_floatlabel">Disc.Type</label>
                                                                {!! Form::select('bill_discount_type', ['1' => 'Percentage', '2' => 'Amount'], '', ['class' => 'form-control', 'id' => 'bill_discount_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4 padding_sm">
                                                            <div class="custom-float-label-control mate-input-box">
                                                                <label class="custom_floatlabel">Disc. Value</label>
                                                                <input class="form-control" value="0.00" type="text"
                                                                    maxlength="8" id="bill_discount_value"
                                                                    name="bill_discount_value">
                                                            </div>
                                                        </div>
                                                        {{-- <div class="col-md-3  pull-left" >Dis Amount
                                            <input type="checkbox" style="display: block;margin: 0 auto;height: 30px;" name="from_discounted" id="from_discounted">
                                        </div> --}}
                                                        <div class="col-md-3 padding_sm"
                                                            style="padding-top:21px !important">
                                                            <input class="btn btn-success" type="button"
                                                                onclick="BillDiscountcalculate();" value="Apply Discount"
                                                                data-original-title="Generate Bill Discount"
                                                                id="bill_discount_generate">
                                                        </div>
                                                        <div class="col-md-3 padding_sm"
                                                            style="    padding-top: 15px !important;">
                                                            <label class="pull-left"
                                                                style="font-size: 12px;font-weight: 700;">
                                                                Disc. Amnt
                                                            </label>
                                                        </div>
                                                        <div class="col-md-2 padding_sm"
                                                            style="    padding-top: 15px !important;">
                                                            <label class="pull-left" style="font-size: 18px;">
                                                                <span class="label  total_discount"
                                                                    style="color: rgb(179, 33, 33);font-weight:700;font-size: 14px;">0.00
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-2 padding_sm">
                                                <div class="box no-border no-margin">
                                                    <table class="table no-margin table_sm no-border grn_total_table">
                                                        <tbody>
                                                            <tr>
                                                                <td width="50%"><label class="text-bold">Gross
                                                                        Amount</label></td>
                                                                <td><input readonly="" type="text" id="item_value"
                                                                        Value="0.00" name="item_value"
                                                                        class="form-control text-right"></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="text-bold">-Disc</label></td>
                                                                <td><input readonly="" type="text" id="payment_other"
                                                                        Value="0.00" name="payment_other"
                                                                        class="form-control text-right"></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="text-bold">+Tax</label></td>
                                                                <td><input readonly="" id="payment_othercharges" type="text"
                                                                        Value="0.00" name="payment_othercharges"
                                                                        class="form-control text-right"></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="text-bold">RoundOff</label></td>
                                                                <td><input autocomplete="off" name="round_off"
                                                                        id="round_off" type="text" Value="0.00"
                                                                        class="form-control text-right"
                                                                        onblur="purchaseAdjustAmount(); roundOff_validation();">
                                                                </td>
                                                            </tr>
                                                            <tr style="background: #F1E9FF;">
                                                                <td><label class="text-bold;"
                                                                        style="font-size: 16px;font-weight: 700;">Tot.
                                                                        Amnt</label></td>
                                                                <td><input readonly="" name="po_amount"
                                                                        style="font-size: 16px !important;font-weight: 700;"
                                                                        id="po_amount" type="text" Value="0.00"
                                                                        class="form-control text-right"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>

                                        </div>

                                        {{-- btn section --}}
                                        <div class="col-md-12 padding_sm">
                                            <input type='hidden' name='po_approve_amt' id='po_approve_amt_id' value=''>
                                            <div class="col-md-3 pull-left" style="display:none"
                                                id="purchase_order_status">
                                                <div class="custom-float-label-control">
                                                    {{-- <label class="form-control custom_floatinput" style="background-color: #eee;font-size: 13px !important;"><b id="po_status"></b></label> --}}
                                                </div>
                                            </div>
                                            <!-- Print Priview Button-->
                                            <div class="col-md-2  pull-right" id="print_preview" style="display:none">
                                                <input type="button" value="Print Preview" name="po_preview" id="po_preview"
                                                    class="btn btn-success" onclick="PoPrintPreview();"
                                                    style="margin-bottom:4px">&nbsp;
                                                <span>Margin</span>
                                                <input type="checkbox" name="is_empty" id="is_empty">
                                            </div>
                                            <!-- Print Priview Button-->
                                            <?php if (isset($accessPerm['approval']) && $accessPerm['approval'] == '1') { ?>
                                            <div id="po_approve_btn_id" style="display: none;"
                                                class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="purchase_mail(0)"
                                                    class="btn btn-success btn-block po_list_bttn btn-block"><i
                                                        id="po_approve_spin"></i> Approve</button>
                                            </div>
                                            <div id="po_amend_btn_id" style="display: none;"
                                                class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="po_confirm(4)"
                                                    class="btn btn-success btn-block po_list_bttn"><i
                                                        id="po_amend_spin"></i> Amend </button>
                                            </div>
                                            {{-- <div id="aprove_amend_btn_id" style="display: none;" class="col-md-1 pull-right  padding_sm">
                                    <button type="button" onclick="po_confirm(17)" class="btn bg-navy-active btn-block po_list_bttn"><i id="approve_amend_spin" ></i> Approve Amend</button>
                                </div> --}}
                                            <!-- <div id="vendor_pdf_print_amned" style="display: none;" class="col-md-1 pull-right  padding_sm"><button type="button" onclick="po_print(4)" class="btn bg-aqua-active btn-block po_list_bttn"><i id="po_vender_amend_print" class="fa fa-print"></i> Print</button></div> -->
                                            <?php }
                                if (isset($accessPerm['verification']) && $accessPerm['verification'] == '1') {
                                ?>
                                            <div id="po_verify_btn_id" style="display: none;"
                                                class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="savePurchaseOrder(6)"
                                                    class="btn bg-green-active btn-block btn-success po_list_bttn"><i
                                                        id="po_verify_spin"></i> Verify </button>
                                            </div>
                                            {{-- <div id="request_for_amend_btn_id"  class="col-md-1 pull-right no-padding" style="display: none; width: 150px;">
                                    <button type="button" onclick="po_confirm(15)" class="btn bg-navy-active btn-block po_list_bttn">
                                        <i id="request_for_amend_spin" ></i> Amend Request</button>
                                </div> --}}
                                            <?php }
                                if (isset($accessPerm['add']) && $accessPerm['add'] == '1') {
                                ?>
                                            <div id="po_post_btn_id" class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="savePurchaseOrder(1)"
                                                    class="btn btn-warning btn-block po_list_bttn po_save_bttn"><i
                                                        id="po_post_spin"></i> Save </button>
                                            </div>
                                            <?php
                            }
                            if ((isset($accessPerm['add']) && ($accessPerm['add'] == '1')) || (isset($accessPerm['approval']) && $accessPerm['approval'] == '1' )) {
                                ?>

                                            <div id="download_pdf_js_id" style="display: none;"
                                                class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="DownloadPdf()"
                                                    class="btn bg-red-active btn-danger btn-block po_list_bttn"><i
                                                        id="download_pdf_js" class="glyphicon glyphicon-download-alt"></i>
                                                    PDF</button>
                                            </div>

                                            @if (isset($accessPerm['approval']) && $accessPerm['approval'] == '1')
                                                <div id="vendor_mail_btn_id" style="display: none;"
                                                    class="col-md-1 pull-right  padding_sm">
                                                    <button type="button" onclick="purchase_mail(1)"
                                                        class="btn btn-warning po_list_bttn btn-block"><i
                                                            id="po_vender_mail" class="fa fa-mail-forward"></i> Send
                                                        Mail</button>
                                                </div>
                                            @endif
                                            {{-- <div id="qutation_btn_id" style="display: none;" class="col-md-1 pull-right  padding_sm">
                                    <button type="button" onclick="quotation_upload(1)" class="btn bg-aqua-active btn-block po_list_bttn"><i id="po_quotation" class="glyphicon glyphicon-upload"></i> Quotation</button>
                                </div> --}}
                                            <!-- <div id="vendor_pdf_print" style="display: none;" class="col-md-1 pull-right  padding_sm"><button type="button" onclick="po_print(3)" class="btn bg-aqua-active btn-block po_list_bttn"><i id="po_vender_print" class="fa fa-print"></i> Print</button></div> -->
                                            <div id="po_close_btn_id" style="display: none;"
                                                class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="savePurchaseOrder(2)"
                                                    class="btn bg-orange-active btn-success btn-block po_list_bttn"><i
                                                        id="po_close_spin"></i> Close</button>
                                            </div>
                                            <div id="po_cancel_btn_id" style="display: none;"
                                                class="col-md-1 pull-right  padding_sm">
                                                <button type="button" onclick="savePurchaseOrder(0)"
                                                    class="btn bg-red-active btn-block btn-danger po_list_bttn"><i
                                                        id="po_cancel_spin"></i> Cancel</button>
                                            </div>
                                            {{-- <div id="po_approve_revert_btn_id" style="display: none;" class="col-md-1 pull-right  padding_sm">
                                    <button type="button" onclick="savePurchaseOrder(16)" class="btn bg-red-active btn-block po_list_bttn"><i id="po_approve_revert_spin" ></i> Revert to Verify</button>
                                </div> --}}
                                            <?php } ?>
                                            <div class="col-md-1 pull-right padding_sm po_save_common_bttn">
                                                <!-- <button type="button" onclick="add_itemto_master()" class="btn bg-blue-active btn-block po_list_bttn"><i id="po_master_spin" class="fa fa-plus-square"></i> Item Master</button> -->
                                            </div>
                                            <!--    <div class="col-md-1 pull-right  padding_sm po_save_common_bttn">
                                        <button type="button" onclick="getannexuredata();" class="btn bg-maroon-active btn-block po_list_bttn"><i id="annexure_pospin" class="fa fa-anchor"></i> Annexure/PO Reference</button>
                                    </div> -->
                                            {{-- <div class="col-md-1 pull-right  padding_sm po_save_common_bttn" style="width: 150px;">
                                <button type="button" onclick="termsAndConditions()" class="btn bg-aqua-active btn-block po_list_bttn"><i id="termsAndConditionspin" class="fa fa-copyright"></i> Terms & Conditions</button>
                            </div> --}}
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="h10"></div>
                                        <div class="col-md-12">
                                            <div class="box box-widget no-margin">
                                                <div class="box-footer">

                                                    <form id="save_itemlist">
                                                        <div class="theadscroll always-visible"
                                                            style="padding-right: 15px; height:400px;">
                                                            <table id="main_row_new"
                                                                class="table table-striped table-bordered table_sm table_condensed theadfix_wrapper">
                                                                <thead>
                                                                    <tr class="headergroupbg">
                                                                        <th width="2%">#</th>
                                                                        <th width="26%">
                                                                            <input id="issue_search_box"
                                                                                onkeyup="searchProducts();" type="text"
                                                                                placeholder="Item Search.. "
                                                                                style="color: #000;display: none;width: 90%;">
                                                                            <span id="item_search_btn" style=" float: right"
                                                                                class="btn btn-warning"><i
                                                                                    class="fa fa-search"></i></span>
                                                                            <span id="item_search_btn_text">Product
                                                                                Name</span>
                                                                        </th>
                                                                        <th width="5%">Qty</th>
                                                                        <th width="5%">Unit</th>
                                                                        <th width="5%">Rate</th>
                                                                        <th width="5%">Free Qty</th>
                                                                        <th width="5%">MRP </th>
                                                                        {{-- <th width="5%">Exclusive</th> --}}
                                                                        <th width="3%" class="text-center"></th>
                                                                        <th width="7%">Net Amt</th>
                                                                        <th width="5%">Free Item</th>
                                                                        <th width="15%">Remarks</th>
                                                                        <th width="5%">
                                                                            <button type="button"
                                                                                onclick="AddNewProduct(0, 1,0,'');"
                                                                                class="btn btn-success">
                                                                                <i id="addProductSpin"
                                                                                    class="fa fa-plus"></i>
                                                                            </button>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="added_new_list_table_product">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        {{ csrf_field() }}
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="h10"></div>
                                        <input type="hidden" value="1" id="row_count">



                                        <div class="clearfix"></div>
                                        <div class="h10"></div>
                                        <div class="col-md-12">


                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="h10"></div>
                                        <?php if (isset($accessPerm['approval']) && $accessPerm['approval'] == '1') { ?>
                                        <div class="col-md-12 padding_sm" style="display:none" id="enable_mail_option">
                                            <div class="col-md-2 pull-right">
                                                <!--<input type="checkbox" name="enable_mail" id="enable_mail"  style="margin-top:2px" checked><label style="margin: 0 0 0 5px;">Enable Mail</label>-->
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="purchaselistModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 100%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green-active">
                    <h4 class="modal-title">Added Items</h4>
                </div>
                <div class="modal-body" id="purchaselistDiv">

                </div>

            </div>
        </div>
    </div>
    <div id="itemcode_searchModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 100%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-purple-active" style="background-color: #26b99a;color: #fff;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Search Information</h4>
                </div>
                <div class="modal-body" id="itemts_getdiv">

                </div>

            </div>
        </div>
    </div>
    <div id="annexture_poref_model" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 100%;">

            <div class="modal-content">
                <div class="modal-header bg-blue-active">
                    <h4 class="modal-title">Annexure /PO Reference</h4>
                </div>
                <div class="modal-body" id="annexture_poref_div" style="min-height: 300px;">

                </div>
            </div>
        </div>
    </div>
    <div id="terms_conndition_model" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 100%;">

            <div class="modal-content">
                <div class="modal-header bg-red-active">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Terms And Conditions </h4>
                </div>
                <div class="modal-body" id="terms_conndition_div" style="min-height: 300px;">

                </div>
            </div>
        </div>
    </div>


    <!-- /***************mail Pop up Body**************/-->
    <div id="mailBodyPopUpModel" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1000px; width: 100%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Purchase Order<span id="modal_itemcodehr"> Mail</span></h4>
                </div>
                <div class="modal-body" style="padding: 7px 15px;" id="purchaseEmail">
                    <div class="mail_input_form">
                        <table class="table no-margin no-border table_sm purchaseMailClass">
                            <tbody>
                                <tr class="mailid_box">
                                    <td colspan="2" style="padding: 5px 0 !important;" id="mailid_div_td_id">
                                        <span class="mailid_div"></span>
                                    </td>
                                </tr>
                                <tr class="enter_mailid_box" style="display: none;">
                                    <td style="padding: 5px 0 0 0 !important;" width="5%">To</td>
                                    <td width="100%">
                                        <input type="text" height="60px" id="mailinput"
                                            title="Multiple Email address separate with comma" class="form-control">
                                        <div id="mailinputListAjaxDiv1" class="ajaxSearchBox"
                                            style="margin: 0; width: 50%; z-index: 9999;"></div>
                                        <input type="hidden" name="mailinput_list_po_hidden1" value=""
                                            id="mailinput_list_po_hidden1">
                                    </td>
                                    <!--  <td width="4%"><div class="cc_button">Cc</div></td>
                                <tr class="cc_mailid_box">
                                    <td colspan="3" style="padding: 5px 0 !important;">
                                        <span class="cc_mailid_div"><span class="cc_mailid_close">X</span></span>
                                    </td>
                                </tr>  -->
                                </tr>

                                <tr class="enter_cc_box">
                                    <td style="padding: 5px 0 0 0 !important;" width="5%">Cc</td>
                                    <td colspan="2">
                                        <input type="text" height="60px" id="ccinput"
                                            title="Multiple Email address separate with comma" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input class="form-control" style="font-weight: 700;" id="subject_id"
                                            value="" type="text" placeholder="Subject"></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <textarea
                        style="overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:100%; height: 200px"
                        cols="50" id="mail_template_text" name="mail_template_text"></textarea>
                    {{-- <form action="{{route('ExtensionsValley.purchase.uploadMailAttachment')}}"
                              class="dropzone" id='mydropzoneidpofromreq' enctype='multipart/form-data' method="post">
                {{ csrf_field() }}
<div class="dz-message">
                <div class="col-xs-8">
                    <div class="message">
                        <p>Drop files here or Click to Upload</p>
                    </div>
                </div>
            </div>
            <div class="fallback">
                <input type="file" name="file"   multiple>
            </div>

                            <input name="po_no_id" type="hidden" id='po_no_id' value="" />
                            <input name="po_no_no" type="hidden" id='po_no' value="" />
            </form>
            <div id="mydropzoneidpofromreq"  class="dropzone"></div> --}}
                </div>
                <div class="modal-footer" style="padding: 10px 7px;">
                    <input type="hidden" value="{{ url('') }}" id="url_id">
                    <input type="hidden" value="" id="attachment_file_id_string" class='attachment_file_id_string'>
                    <a id="pdf_download_popup" href="" class="pull-right" style="margin-left: 10px"
                        target="_blank"><button class="btn btn-success" type="button" style="padding: 5px 2px;"><i
                                class="fa fa-download"> Download PDF</i></button></a>
                    <button class="btn btn-success" id="setbatchdatabtn1" type="button" onclick="PO_mail_save();"
                        style="padding: 5px 2px;"><i> PO Mail</i></button>
                    <button class="btn btn-danger" data-dismiss="modal" style="padding: 5px 2px;"><i
                            class="fa fa-trash-o"> Cancel</i></button>
                </div>
            </div>
        </div>
    </div>
    <!-- /***************mail Pop up Body End**************/-->
    <input type="hidden" name="po_req_url" id="po_req_url" value="{{ route('ExtensionsValley.purchase.poList') }}">
    <!-- PO Print Preview -->
    <div class="modal fade" id="po_print_preview" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width: 1100px !important; width: 100% !important;">
            <div class="modal-content">
                <div class="modal-header bg-orange-active">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Purchase Order</h4>
                </div>
                <div class="modal-body" style="padding: 7px 15px;">
                    <div class="theadscroll" style="height: 500px">
                        <div id="PoItemDetails">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PO Print Preview -->
    <!-- PO Print Preview -->
    <div class="modal fade" id="po_qutation" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width: 1100px !important; width: 100% !important;">
            <div class="modal-content">
                <div class="modal-header bg-orange-active">
                    <button type="button" class="close" onclick='modelClose();'>&times;</button>
                    <h4 class="modal-title">Upload Quotation</h4>
                </div>
                <div class="modal-body" id='quotation_model_body' style="padding: 7px 15px;">
                    <div class="theadscroll" style="height: 500px">
                        <div id="po_qutation_body">
                            <div class="col-md-12" id='drop-zone-container1'>
                                <form action="{{ route('ExtensionsValley.purchase.uploadQuotation') }}"
                                    class="dropzone" enctype='multipart/form-data' method="post">

                                    <div class="fallback" id='mydropzoneId'>
                                        <input name="file[]" type="file" accept=".jpg,.jpeg,.png,.doc,.pdf" id='file_upload'
                                            multiple />
                                    </div>
                                    <input name="po_no_id" type="hidden" id='po_no_id' value="" />
                                    <input name="po_no_no" type="hidden" id='po_no' value="" />
                                </form>
                            </div>

                            <div class="cold-md-12">
                                <div class="selected_div_list">
                                    <ul class="list-unstyled" id="quotation_file_list_id">

                                        <!-- <li>
                                            <div class="select_img_close">x</div>
                                                <div class="select_img_box">
                                                    <img src=""/>
                                                </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="purchase_charge_modal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 400px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Purchase Charge</h4>

                </div>
                <div class="modal-body">
                    <div id='purchase_charges_div'>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- lab result Modal -->
    <div id="global_stock_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 73%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Global Stock</h4>
                </div>
                <div class="modal-body" style="height:520px;">

                    <div class="col-md-12" id="global_stock_data">

                    </div>

                </div>

            </div>

        </div>
    </div>
    <!-- lab result Modal -->
    <!-- PO Print Preview -->



    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    {!! Html::script('packages/extensionsvalley/master/canvas/js/jsonh.js') !!}
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>

    <!-- Tiny Mce Editor JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>

    {!! Html::script('packages/Purchase/default/javascript/purchase_order.js?v=2') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript">
        function save_attachment_id() {
            var attachment_file_id_string = new Array();
            $('.dz-remove').each(function() {
                attachment_file_id = $(this).attr('id');
                attachment_file_id_string.push({
                    attachment_file_id: attachment_file_id
                });


            });

            document.getElementById("attachment_file_id_string").value = JSON.stringify(attachment_file_id_string);
        }


        var total_photos_counter = 0;
        Dropzone.options.mydropzoneidpofromreq = {

            maxFilesize: 4,
            addRemoveLinks: true,
            url: "{{ route('ExtensionsValley.purchase.uploadMailAttachment') }}",
            dictRemoveFile: 'Remove file',
            dictFileTooBig: 'Image is larger than 4MB',
            //acceptedFiles: this is changed or modified in the library
            acceptedFiles: ".jpg,.jpeg,.png,.doc,.pdf,.xlsx,.odt,.csv,.xls,.docx,.txt",
            timeout: 1000000,
            removedfile: function(file) {

                var btndelete = file.previewElement.querySelector("[data-dz-remove]");
                if (btndelete.hasAttribute("id")) {
                    var iddelete = btndelete.getAttribute("id");
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{!! route('ExtensionsValley.purchase.removeMailAttachment') !!}",
                    data: {
                        id: iddelete
                    },
                    success: function(data) {
                        console.log("File has been successfully removed!!");
                        save_attachment_id();
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            success: function(file, response) {
                var fileuploded = file.previewElement.querySelector("[data-dz-name]");
                fileuploded.innerHTML = response.newfilename;
                var btndelete = file.previewElement.querySelector("[data-dz-remove]");
                btndelete.setAttribute("id", response.id);
                save_attachment_id();
            },
            error: function(file, response) {

                if (!file.accepted) {
                    alert(response);
                    var fileRef;
                    return (fileRef = file.previewElement) != null ?
                        fileRef.parentNode.removeChild(file.previewElement) : void 0
                }
                return false;
            },
        };
        $(document).ready(function() {
            setTimeout(function() {
                // $('#menu_toggle').trigger('click');
                // $(body).addClass('sidebar-collapse');
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
            }, 300);
            $("#item_search_btn").click(function() {
                $("#issue_search_box").toggle();
                $("#item_search_btn_text").toggle();
            });
        })
    </script>
@stop
@section('javascript_extra')
    <script type="text/javascript">
        function quotation_upload() {
            var po_no = $('#po_added').val();
            var is_amended = $('#is_amended').val();
            $.ajax({
                url: '',
                type: 'get',
                data: {
                    po_no: po_no,
                    is_amended: is_amended,
                    quotation_popup: 1
                },
                success: function(data) {
                    var img_body = '';
                    var data = JSON.parse(data);
                    po_res = data.po_res;
                    quo_res = data.quo_res;
                    // popup_html =data.popup_html;
                    // $('#drop-zone-container').html(popup_html);
                    $('#po_no').val(po_res.po_no);
                    $('#po_no_id').val(po_res.po_id);
                    var i;
                    for (i = 0; i < quo_res.length; i++) {
                        var ext = quo_res[i].file_name.split('.').pop().toLowerCase();
                        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == 1) {
                            img_body += '<li id="image_' + quo_res[i].id + '">' +
                                '<div class="select_img_close" onclick="removeQuotationFile(' + quo_res[i].id +
                                ')">x</div>' +
                                '<div class="select_img_box">' +
                                '<a href="' + quo_res[i].file_name_path + '/' + quo_res[i].file_name +
                                '" target="_blanck">' +
                                '<img src="' + quo_res[i].file_name_path + '/' + quo_res[i].file_name +
                                '"/></a>' +
                                '</div>' +
                                '</li>';
                        } else if (ext == 'pdf') {
                            img_body += '<li id="image_' + quo_res[i].id + '">' +
                                '<div class="select_img_close" onclick="removeQuotationFile(' + quo_res[i].id +
                                ')">x</div>' +
                                '<div class="select_img_box">' +
                                '<a href="' + quo_res[i].file_name_path + '/' + quo_res[i].file_name +
                                '" target="_blanck">' +
                                '<img src="{{ url('packages / Purchase / PurchaseImage / pdf.png') }}"/></a>' +
                                '</div>' +
                                '</li>';
                        } else {
                            img_body += '<li id="image_' + quo_res[i].id + '">' +
                                '<div class="select_img_close" onclick="removeQuotationFile(' + quo_res[i].id +
                                ')">x</div>' +
                                '<div class="select_img_box">' +
                                '<a href="' + quo_res[i].file_name_path + '/' + quo_res[i].file_name +
                                '" target="_blanck">' +
                                '<img src="{{ url('packages / Purchase / PurchaseImage / doc.png') }}"/></a>' +
                                '</div>' +
                                '</li>';
                        }
                    }

                    $('#quotation_file_list_id').html(img_body);
                    $('#po_qutation').modal('show');
                },
                complete: function() {

                }
            });
        }

        /********************************************************************* */
        function removeQuotationFile(id) {
            var url = "{!! route('ExtensionsValley.purchase.removeQuotation') !!}";
            $.ajax({
                url: '',
                type: "get",
                data: {
                    id: id,
                    removeQuotationFile: '1'
                },
                success: function(data) {
                    if (data == 1) {
                        $('#image_' + id).remove();
                        toastr.success('File Remove Successfuly !!');
                    } else {
                        toastr.error('File Not Removed !!');
                    }
                }
            });
        }

        /*************************Dropzone*****************************/


        /*************************PDF Downloader*****************************/
        function DownloadPdf() {
            var po_type_id = $('#add_edit_status').val();
            var po_status = $('#hidden_status').val();
            $.ajax({
                url: "",
                data: {
                    po_id_type: po_type_id,
                    po_status: po_status,
                    pdf_download: 'pdf_download'
                },
                type: 'GET',
                beforeSend: function() {
                    $('#adddataspin').removeClass('fa fa-plus-square-o');
                    $('#adddataspin').addClass('fa fa-spinner fa-spin');
                },
                success: function(result) {
                    if (result) {
                        var url_path = $('#url_id').val();
                        var path = url_path + '/packages/uploads/Purchase/' + result;
                        window.open(path);
                    } else {
                        toastr.info("Purchase Order PDF Not Created!!");
                    }
                },
                complite: function() {
                    $('#adddataspin').removeClass('fa fa-spinner fa-spin');
                    $('#adddataspin').addClass('fa fa-plus-square-o');
                }
            });
        }
        var is_igst = 0;
        var billDiscount = {{ $billDiscount }};
        var CGST_ID = {{ $CGST_ID }};
        var IGST_ID = {{ $IGST_ID }};
        var SGST_ID = {{ $SGST_ID }};
        var purchase_rund_val = 0;

        function po_pagereload() {
            window.location = "{!! route('ExtensionsValley.purchase.purchaseOrder') !!}";
        }

        // function add_itemto_master() {

        // }

        function PoPrintPreview() {
            var id = $("#add_edit_status").val();
            var is_amended = $("#is_amended").val();
            var checked = $("#is_empty").prop('checked');
            var url = "{{ route('ExtensionsValley.purchase.purchaseOrder') }}";
            $.ajax({
                type: "GET",
                data: "PrintPoItemDetails=" + 1 + "&id=" + id + "&checked=" + checked + "&is_amended=" + is_amended,
                url: url,
                beforeSend: function() {},
                success: function(html) {
                    $('#PoItemDetails').html(html);
                    $('#po_print_preview').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                },
                complete: function() {}
            });
        }

        function PrintPO() {
            var showw = "";
            var header = "";
            var mywindow = window.open('', 'my div', 'height=3508,width=2480');
            if ($('#header_include').is(':checked') == true) {
                header = $("#print_header").html();
            }
            var msglist = $(".getPoContent").html();
            showw = header + msglist;
            mywindow.document.write('<style></style>');
            mywindow.document.write(showw);
            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print()
            setTimeout(function() {
                mywindow.close();
            }, 300);

            return true;
        }

        function fillVendorDetialsNew(code, vendor_list_po, contact_no = '') {
            $('#vendor_list_po').val(vendor_list_po);
            $('#vendor_list_po_hidden').val(code);
            $('#vendorListAjaxDiv').hide();
            $('#vendor_list_po').attr('title', contact_no);
            item_list_final = [];
            serach_po();
        }

        function fillVendorDetialsNew1(code1, vendor_list_po1, contact_no = '') {
            $('#vendor_list_po1').val(vendor_list_po1);
            $('#vendor_list_po_hidden1').val(code1);
            $('#vendor_list_po1').attr('title', contact_no);
            $('#vendorListAjaxDiv1').hide();
        }

        function fillVendorDetialsNew2(code2, vendor_list_po2, vendor_state_id, contact_no = '') {
            $('#vendor_list_po2').val(vendor_list_po2);
            $('#vendor_list_po_hidden2').val(code2);
            $('#vendor_list_po2').attr('title', contact_no);
            $('#vendorListAjaxDiv2').hide();
            $('#vendor_state_id').val(vendor_state_id);
            is_igst = 0;

        }

        function clearpovendor() {
            $("#vendor_list_po").val('');
            $("#vendor_list_po_hidden").val('');
        }

        function clearpovendor1() {

            $("#vendor_list_po1").val('');
            $("#vendor_list_po_hidden1").val('');
        }

        function fillEmailDetialsTo(email) {
            $('#mailinput').val(email);
            $('#mailinput_list_po_hidden1').val(email);
            $('#mailinputListAjaxDiv1').hide();
        }
    </script>


    <script type="text/javascript">
        @if (!empty($is_save) && $is_save == 1)
            $('.nav-tabs a[href="#po_list"]').tab('show');
            serachPurchaseOrder(1, 0);
        @endif
        @if (isset($_GET) && !empty($_GET))
            $('.nav-tabs a[href="#po_list"]').tab('show');
            serachPurchaseOrder(0, 1);
        @endif


        $(document).on('keyup change', 'input[name="bill_discount_value"]', function(event) {
            $(this).val($(this).val().replace(/[^0-9.]/g, ""));
        });
        $('#vendor_list_po').keyup(delay(function(event) {

            var keycheck =
                /[a-zA-Z0-9 & ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            var current;
            if (value.match(keycheck) || event.keyCode == '8') {
                if ($('#vendor_list_po_hidden').val() != "") {
                    $('#vendor_list_po_hidden').val('');
                }
                var vendor_list_po = $(this).val();
                vendor_list_po = vendor_list_po.trim();
                if (vendor_list_po == "") {
                    $("#vendorListAjaxDiv").html("");
                } else {
                    var url = '';
                    var param = {
                        vendor_list_po: vendor_list_po
                    };
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: param,
                        beforeSend: function() {
                            // aler('ggg');
                            $("#vendorListAjaxDiv").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#vendorListAjaxDiv").html(html).show();
                            $("#vendorListAjaxDiv").find('li').first().addClass('liHover');
                        },
                        complete: function() {

                        }
                    });
                }

            } else {
                ajax_list_key_down('vendorListAjaxDiv', event);
            }
        }, 1000));
        $('#vendor_list_po').on('keydown', function(event) {
            if (event.keyCode === 13) {
                ajaxlistenter('vendorListAjaxDiv');
                return false;
            }
        });
        /***********************Email Search List*******************************/
        $('#mailinput').keyup(delay(function(event) {

            var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            var current;
            if (value.match(keycheck) || event.keyCode == '8') {
                if ($('#mailinput_list_po_hidden1').val() != "") {
                    $('#mailinput_list_po_hidden1').val('');
                }
                var mailinput = $(this).val();
                //                ccinput = ccinput.split(',');
                //                ccinput = ccinput[mail_arr.length-1];
                mailinput = mailinput.trim();
                if (mailinput == "") {
                    $("#ccinputListAjaxDiv1").html("");
                } else {
                    var url = '';
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: "mailinput=" + mailinput,
                        beforeSend: function() {
                            // aler('ggg');
                            $("#ccinputAjaxDiv1").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#mailinputListAjaxDiv1").html(html).show();
                            $("#mailinputListAjaxDiv1").find('li').first().addClass('liHover');
                        },
                        complete: function() {

                        }
                    });
                }

            } else {
                ajax_list_key_down('mailinputListAjaxDiv1', event);
            }
        }, 1000));
        $('#mailinputListAjaxDiv1').on('keydown', function(event) {
            if (event.keyCode === 13) {
                ajaxlistenter('mailinputListAjaxDiv1');
                return false;
            }
        });

        $('#vendor_list_po1').keyup(delay(function(event) {

            var keycheck = /[a-zA-Z0-9 & ]/;
            var value = event.key;
            var current;
            if (value.match(keycheck) || event.keyCode == '8') {
                if ($('#vendor_list_po_hidden1').val() != "") {
                    $('#vendor_list_po_hidden1').val('');
                }
                var vendor_list_po1 = $(this).val();
                vendor_list_po1 = vendor_list_po1.trim();
                if (vendor_list_po1 == "") {
                    $("#vendorListAjaxDiv1").html("");
                } else {
                    var url = '';
                    var param = {
                        vendor_list_po1: vendor_list_po1
                    };
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: param,
                        beforeSend: function() {
                            // aler('ggg');
                            $("#vendorListAjaxDiv1").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#vendorListAjaxDiv1").html(html).show();
                            $("#vendorListAjaxDiv1").find('li').first().addClass('liHover');
                        },
                        complete: function() {

                        }
                    });
                }

            } else {
                ajax_list_key_down('vendorListAjaxDiv1', event);
            }
        }, 1000));
        $('#vendor_list_po1').on('keydown', function(event) {
            if (event.keyCode === 13) {
                ajaxlistenter('vendorListAjaxDiv1');
                return false;
            }
        });
        $('#vendor_list_po2').keyup(delay(function(event) {

            var keycheck = /[a-zA-Z0-9 & ]/;
            var value = event.key;
            var current;
            if (value.match(keycheck) || event.keyCode == '8') {
                if ($('#vendor_list_po_hidden2').val() != "") {
                    $('#vendor_list_po_hidden2').val('');
                }
                var vendor_list_po2 = $(this).val();
                vendor_list_po2 = vendor_list_po2.trim();
                if (vendor_list_po2 == "") {
                    $("#vendorListAjaxDiv2").html("");
                } else {
                    var url = '';
                    var param = {
                        vendor_list_po2: vendor_list_po2
                    };
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: param,
                        beforeSend: function() {
                            // aler('ggg');
                            $("#vendorListAjaxDiv1").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#vendorListAjaxDiv2").html(html).show();
                            $("#vendorListAjaxDiv2").find('li').first().addClass('liHover');
                        },
                        complete: function() {

                        }
                    });
                }

            } else {
                ajax_list_key_down('vendorListAjaxDiv2', event);
            }
        }, 1000));
        $('#vendor_list_po2').on('keydown', function(event) {
            if (event.keyCode === 13) {
                ajaxlistenter('vendorListAjaxDiv2');
                return false;
            }
        });
        /**Po fuction call for notfication link to redirect to edit**/
        <?php
    if (isset($req_id) && $req_id != '') {
        $a = explode("-",$req_id);
        ?>

        purchaseEditList({{ $a[0] }}, {{ $a[1] }});
        <?php
    }
    ?>
        // $("#fixTable").tableHeadFixer();
        $(document).on('click', '.pagination a', function(e) {
            getPosts($(this).attr('href'));
            e.preventDefault();
        });



        $('.date_format').datetimepicker({
            format: "DD-MM-YYYY"

        });

        $('#quotation_date').datetimepicker({
            format: "DD-MM-YYYY"
        });

        $('#po_date').datetimepicker({
            format: "DD-MM-YYYY"
        });

        $('.ajaxSearchBox').bind('DOMNodeInserted DOMNodeRemoved', function() {
            $('html').one('click', function() {
                $(".ajaxSearchBox").hide();
            });
        });
        $('input[data="date"]').datetimepicker({
            format: "DD-MM-YYYY"
        });


        function searchProducts() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("issue_search_box");
            filter = input.value.toUpperCase();
            table = document.getElementById("main_row_new");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
@endsection
