@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<style>
.bg-green-active{
        background-color: #005580 !important;
    }
    .tab_pop_container {
        position: relative;
    }
    .tab_pop {
        background: #b0c7dc none repeat scroll 0 0;
        border-radius: 6px;
        box-shadow: 1px 1px 3px #d0d0d0;
        padding: 22px 10px 10px 10px;
        position: absolute;
        left: 1.5%;
        top: 0px;
        width: 750px;
        z-index: 850;
    }
    .tab_pop_closebtn {
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        left: 0;
        top: 2px;
        width: 20px;
    }
    .tab_pop_drag {
        color: #fff;
        cursor: pointer;
        font-size: 15px;
        font-weight: bold;
        height: 16px;
        left: 10px;
        padding: 3px 2px;
        position: absolute;
        top: 4px;
        width: 21px;
    }
    .tab_pop table thead th {
        background: #19a0d8 none repeat scroll 0 0 !important;
        font-weight: normal !important;
        color: #FFF !important;
        text-align: center;
    }
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 25px 0px 0px 0px;
        overflow-y: auto;
        width: auto;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }

    .liHover{
        background: #cde3eb;
    }
    .box_border {
        padding-top: 25px;
        padding-bottom: 10px;
        border: 1px solid #ccc;
        background: #f3f3f3;
    }
    /* bootstrap-select css starts here*/
    .bootstrap-select .btn-default {
        background: transparent !important;
        border-bottom: 1px solid #ccc!important;
        color: #444 !important;
        height: 23px !important;
        padding: 0 0 0 5px!important;
        border-left: none !important;
        border-right: none !important;
        border-top: none !important;
        border-radius: 0 !important;
    }
    .bootstrap-select .btn-default:hover, .bootstrap-select .btn-default:active, .bootstrap-select .btn-default.hover {
        background: transparent !important;
        border-bottom: 1px solid #ccc !important;
        border-left: none !important;
        border-right: none !important;
        border-top: none !important;
        box-shadow: none !important;
    }
    .bootstrap-select .dropdown-toggle:focus {
        outline: medium none !important;
        outline-offset: -2px !important;
    }
    .bootstrap-select.btn-group .dropdown-menu li a {
        -moz-user-select: none !important;
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
        color: #000 !important;
        cursor: pointer !important;
        font-size: 13px !important;
        font-weight: 400 !important;
        padding: 2px 0 2px 10px !important;
        width: 100% !important;
    }
    .bootstrap-select.btn-group .dropdown-menu li a:hover , .bootstrap-select.btn-group .dropdown-menu li a:focus, .bootstrap-select.btn-group .dropdown-menu li a:active {
        background: #ccc !important;
    }
    .bootstrap-select.btn-group .dropdown-menu.inner{
        overflow-x: hidden !important;
        max-height: 200px !important;
    }
    .bootstrap-select.btn-group .dropdown-menu {
        box-shadow: 0 0 3px #909090 !important;
        box-sizing: border-box !important;
        min-width: auto !important;
        /*width: 100% !important;*/
        width: auto !important;
    }
    .poplast_item_box {
        position: relative;
    }
    .popup_lastitem {
        background: #fff3e0 none repeat scroll 0 0;
        border-radius: 6px;
        box-shadow: 0 0 5px #909090;
        left: 1.5%;
        padding: 25px 5px 5px;
        position: absolute;
        top: 30px;
        width: 960px;
        z-index: 999;
    }
    .popup_lastitem_close {
        background: red none repeat scroll 0 0;
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        right: 5px;
        top: 2px;
        width: 20px;
    }

    .poplast_item_box {
        position: relative;
    }
    .popup_sync {
        background: #fff3e0 none repeat scroll 0 0;
        border-radius: 6px;
        box-shadow: 0 0 5px #909090;
        left: 1.5%;
        padding: 25px 5px 5px;
        position: absolute;
        top: 125px;
        width: 960px;
        z-index: 999;
    }
    .popup_sync_close {
        background: red none repeat scroll 0 0;
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        right: 5px;
        top: 2px;
        width: 20px;
    }
    .pop_up_rate_sync {
        position: relative;
    }
    .text_boxalert{
        background: #ffc1c1;
    }
    .close_btnspan{
        margin-right: 2px;
        border-radius: 3px;
        color: white;
        float: left;
        font-size: 12px;
        margin-top: 3px;
        overflow: auto;
        padding: 2px 20px 2px 2px;
        position: relative;
    }
    .item_code_close {
        position: absolute;
        right: 5px;
        top: 2px;
        cursor: pointer;
    }

    .batch_pop_container {
        position: relative;
    }
    .batch_pop{
        background: #deecf9 none repeat scroll 0 0;
        border-radius: 6px;
        box-shadow: 1px 1px 3px #d0d0d0;
        padding: 10px;
        position: absolute;
        top: 26px;
        width: 1000px;
        z-index: 850;
        right: 0;
    }
    .batch_pop::before {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #deecf9;
        border-image: none;
        border-style: solid;
        border-width: 16px;
        content: " ";
        height: 0;
        right: 8px;
        pointer-events: none;
        position: absolute;
        top: -30px;
        width: 0;
    }
    .batch_pop_closebtn {
        background: #333 none repeat scroll 0 0;
        border-radius: 50%;
        box-shadow: 0 0 5px #a2a2a2;
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        left: -10px;
        top: -4px;
        width: 20px;
    }
    .batch_pop table thead th {
        background: #19a0d8 none repeat scroll 0 0 !important;
        font-weight: normal !important;
        color: #FFF !important;
        text-align: center;
    }
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }
    input[type='number'] {
        -moz-appearance:textfield;
    }
    input[type=text]::-ms-clear {
        display: none;
    }

    .foo {
        float: left;
        width: 15px;
        height: 15px;
        margin: 5px;
        border: 1px solid rgba(0, 0, 0, .2);
    }

    .blue {
        background: lightblue;
    }
    .lightbrown {
        background: #d5e6a2;
    }
    .lightorange {
        background: #c1ec3c;
    }
    .purple {
        background: #ab3fdd;
    }
    .no_tax {
        background: #F0E68C;
    }
    .wine {
        background: #ffb399;
    }
 .lightyellow {
       background: #ccffcc;
   }
    .grn_partially_generate{
        background: #80ccff;
    }
    .request_for_amend{
        background: #c1ec3c;
    }
    .grn_generate{
        background: #abff80;
    }
    .partially_received{
        background: #F0E68C;
    }
    .cancelled{
        background: #b3ffff;
    }
    .created{
        background: #ebebe0;
    }
    .approved{
        background: #c6d9ec;
    }
    .closed{
        background: #ffcc99;
    }
    .verified{
        background: #b3cccc;
    }
    .dropzone {
        border-width: 2px;
        border-style: dashed;
        border-color: rgb(0, 135, 247);
        border-image: initial;
        border-radius: 5px;
        background: white;
    }

    .selected_div_list {
        float: left;
        width: 100%;
        padding: 0 15px;
    }

    .selected_div_list ul li {
        position: relative;
        background: #FFF;
        float: left;
        box-shadow: 0 0 6px #333;
        margin: 0 15px 15px 0;
    }
    .selected_div_list .select_img_close {
        background: #000;
        width: 20px;
        height: 20px;
        text-align: center;
        position: absolute;
        right: -10px;
        top: -10px;
        border-radius: 100%;
        color: #FFF;
        cursor: pointer;
    }
    .select_img_box {
        position: relative;
        width: 110px;
        height: 110px;
    }
    .select_img_box img {
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
        max-width: 100%;
        max-height: 100%;
        margin: auto;
    }
    .item_category_div .multiselect-container li.active a:hover{
        background: #337AB7 !important;
        color: #fff !important;
    }
    .item_category_div .multiselect-container li.active a{
        background: #337AB7 !important;
        color: #fff !important;
    }
    .item_category_div .multiselect-container li a{
        background: #fff !important;
        color: #777 !important;
    }

</style>
<?php
if (isset($default_state_id[0]->value) && !empty($default_state_id[0]->value)) {
    $default_state_id = $default_state_id[0]->value;
} else {
    $default_state_id = '';
}

?>
@endsection
@section('content-area')
    <div class="right_col"  role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <div class="nav nav-tabs sm_nav text-center list_tab">
        <input type="hidden" value='{{$config_data}}' id="config_details_hidden">
        <input type="hidden" value='0' id="add_edit_status">
        <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
        <input type="hidden" value='{{ $unit_master }}' id="hidden_unit_master">
        <input type="hidden" value='' id="hidden_status">
        <input type="hidden" value='' id="hiddensearch_code_expression">

    <li id="po_listli"> <a data-toggle="tab" href="#po_list">  Purchase List</a></li>
    <div class="tab-content" style="min-height: 600px;">



        <div id="po_list" class="tab-pane active">

            <div class="panel-body" style="padding: 0px;">
                <div class="panel panel-default">
                    <div class="row" style="">
                        <div class="col-md-12">

                            <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;width: 100%">

                                <div class="clearfix panel-body" style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">
                                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                                        <div class="date custom-float-label-control mate-input-box">
                                            <label class="custom_floatlabel">PO NO. </label>
                                            <div class="clearfix"></div>
                                            <div class="ht5"></div>
                                            <input type="text" autocomplete="off" class="form-control custom_floatinput" id="po_number_search"  placeholder="PO Number" value="@if(isset($_GET['po_number_search']) && !empty($_GET['po_number_search']) ){{$_GET['po_number_search']}}@endif">
                                            <div id="po_number_searchCodeAjaxDiv" style="margin: 0; z-index: 999; font-size: 12px; width: 100%" class="ajaxSearchBox"></div>
                                            <input type="hidden" id="po_number_search_hidden" value="@if(isset($_GET['po_number_search']) && !empty($_GET['po_number_search']) ){{$_GET['po_number_search']}}@endif"/>
                                        </div>
                                    </div>


                                    <div class="col-md-3 padding_sm">
                                        <div style="padding-top: 3px;" class="custom-float-label-control mate-input-box">
                                            <label class="custom_floatlabel">Department</label>
                                            <div class="clearfix"></div>
                                            <div class="ht10"></div>
                                            <?php
                                                         $user_id = \Auth::user()->id;
                                                            $default_location = \DB::table('users')
                                                                        ->select('default_location')
                                                                        ->where('id',$user_id)
                                                                        ->first();
                                                             $user_id = \Auth::user()->id;
                                                             $roleData = [];

                                                             $roleData = $roleData = \DB::table('user_group')
                                                                ->where('user_id', $user_id)->where('status', 1)->pluck('group_id');
                                                $listdept = $to_location = \DB::table('location as l')
                                                            ->leftJoin('location_group as r', 'l.id', '=', 'r.location_id')
                                                            ->whereIn('r.group_id', $roleData)
                                                            ->whereNotIn('l.location_type', ['-1','10'])
                                                            ->where('is_store', 1)
                                                            ->orderBy('location_name')
                                                            ->pluck('l.location_name', 'l.location_code');

                                            if (isset($_GET['posearch_from_department_search']) && !empty($_GET['posearch_from_department_search'])) {
                                                $posearch_from_department_search = $_GET["posearch_from_department_search"];
                                            } else {
                                                $posearch_from_department_search = $default_location->default_location;
                                            }
                                            ?>
                                            {!! Form::select('posearch_from_department',$listdept,$posearch_from_department_search,['class' => 'form-control custom_floatinput selectbox_initializr','id' => 'posearch_from_department','placeholder' => 'Select','style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                                        </div>
                                    </div>

                                    <div class="col-md-3 padding_sm">
                                        <div style="padding-top: 3px;" class="custom-float-label-control mate-input-box">
                                            <label class="custom_floatlabel">Filter by Date</label>
                                            <div class="clearfix"></div>
                                            <div class="h10"></div>
                                            <?php
                                            if (isset($_GET['po_date_filter']) && !empty($_GET['po_date_filter'])) {
                                                $po_date_filter = $_GET["po_date_filter"];
                                            } else {
                                                $po_date_filter = '';
                                            }
                                            ?>
                                            {!! Form::select('date_filter', [
                                            'po_date' => 'Created Date','verified_at' => 'Verified Date','approved_at' => 'Approved Date',] ,
                                            $po_date_filter,['class' => 'form-control','id' => 'date_filter','style' => 'color:#555555; padding:4px 12px;']) !!}
                                        </div>
                                    </div>


                                    <div class="col-md-2 padding_sm" style="padding-bottom: 10px;">
                                        <div class="input-group date custom-float-label-control mate-input-box" id="datetimepicker5">
                                            <label class="custom_floatlabel">Date From</label>
                                            <div class="clearfix"></div>
                                            <div class="ht5"></div>
                                            <input type="text" autocomplete="off" id="po_created_date" placeholder="Date From"
                                                   class="form-control date_format custom_floatinput"
                                                   value="@if(isset($_GET['po_created_date_search']) && !empty($_GET['po_created_date_search']) ) {{$_GET["po_created_date_search"]}} @endif ">
                                        </div>
                                    </div>

                                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                                        <div class="input-group date custom-float-label-control mate-input-box">
                                            <label class="custom_floatlabel">Date To</label>
                                            <div class="clearfix"></div>
                                            <div class="ht5"></div>
                                            <input type="text" autocomplete="off" id="po_created_date_to" placeholder="Date To"
                                                   class="form-control date_format custom_floatinput"
                                                   value="@if(isset($_GET['po_created_date_to_search']) && !empty($_GET['po_created_date_to_search']) )
                                                   {{$_GET["po_created_date_to_search"]}} @endif ">
                                        </div>
                                    </div>

                                    <div class="col-md-4 padding_sm" >
                                        <div class="custom-float-label-control mate-input-box">
                                            <label class="custom_floatlabel">Supplier</label>
                                            <div class="clearfix"></div>
                                            <div class="ht5"></div>
                                            <input type="text" name="vendor_list_po1" value="@if(isset($_GET['vendor_list_po1_search']) && !empty($_GET['vendor_list_po1_search']) ){{$_GET["vendor_list_po1_search"]}}@endif"
                                                    class="form-control" autocomplete="off" id="vendor_list_po1" placeholder="">
                                                    <span style="color: #d14;">{{ $errors->first('vendor_list_po1') }}</span>
                                            <div id="vendorListAjaxDiv1" class="ajaxSearchBox" style="margin: 0; width: 100%; z-index: 9999;"></div>
                                            <input type="hidden" name="vendor_list_po_hidden1"
                                                    value="" id="vendor_list_po_hidden1">
                                        </div>
                                    </div>
                                    <div class="col-md-1 padding_sm text-left">
                                        <label style="margin: 5px 0 0 0;">&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <button data-toggle="tooltip" title="Clear" onClick="clearpovendor1()" data-placement="top" class="btn btn-success"><i class="fa fa-times" style="font-size: 11px;"></i></button>
                                    </div>

                                    <div class="col-md-3 padding_sm">
                                        <div style="padding-top: 3px;" class="custom-float-label-control mate-input-box">
                                            <label class="custom_floatlabel">Status</label>
                                            <div class="clearfix"></div>
                                            <div class="ht10"></div>
                                            <?php
                                            if (isset($_GET['posearch_status_search']) && !empty($_GET['posearch_status_search'])) {
                                                $posearch_status = $_GET["posearch_status_search"];
                                            } else {
                                                $posearch_status = '';
                                            }
                                            ?>
                                            {!! Form::select('posearch_status',[
                                            '' => 'Select',
                                            '1' => 'Created',
                                            '6' => 'Verified',
                                            '3' => 'Approved',
                                            '4' =>'Amended',
                                            '5' =>'Bill Partially Generate',
                                            '10' =>'Bill Completed',
                                            '0' => 'Canceled',
                                            '2' => 'Closed'],$posearch_status,['class' => 'form-control custom_floatinput ','id' => 'posearch_status','style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                                        </div>
                                    </div>



                                    <div class="col-xs-2 padding_sm " >
                                        <div style="padding-top: 10px;">
                                            <label class="custom_floatlabel"></label>
                                            <div class="clearfix"></div>
                                            <div class="ht5"></div>
                                            <div class=""></div>
                                            <button style="" type="button" onclick="serachPurchaseOrder(0, 0);" class="btn btn-block light_purple_bg"> <i id="posearchdataspin" class="fa fa-search"></i>
                                                Search </button>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 padding_sm " >
                                        <div style="padding-top: 10px;">
                                            <label class="custom_floatlabel"></label>
                                            <div class="clearfix"></div>
                                            <div class="ht5"></div>
                                            <div class=""></div>
                                            <a href="{{route('ExtensionsValley.purchase.purchaseOrder')}}"><button type="button" style="color: #000;font-weight: 700" class="btn btn-warning btn btn-block ">New PO</button></a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <input type="hidden" id="po_status" value=0 />

                        </div>
                        <div class="clearfix"></div>
                        <!-- <div class="h10"></div> -->
                        <div class="row1">
                            <div class="col-md-12">
                                <div class="theadscroll always-visible1" id="scrolldown_polist" style="height: auto; max-height: 475px;padding-right: 15px;">

                                    <table class="table no-margin theadfix_wrapper table-striped table-col-bordered table-condensed ">
                                        <thead>
                                            <tr class="headergroupbg">
                                                <th style="width: 10%;text-align: left;color:#fff;background: #26B99A;">Purchase No</th>
                                                <th style="width: 7%;text-align: left;color:#fff;background: #26B99A;">Created Date</th>
                                                <th style="width: 15%;text-align: left;color:#fff;background: #26B99A;">Location</th>
                                                <th style="width: 24%;text-align: left;color:#fff;background: #26B99A;">Supplier</th>
                                                <th style="width: 10%;text-align: left;color:#fff;background: #26B99A;">Created By</th>
                                                <th style="width: 10%;text-align: left;color:#fff;background: #26B99A;">Approved By</th>
                                                <th style="width: 8%;text-align: left;color:#fff;background: #26B99A;">Status</th>
                                                <th style="width: 10%;text-align: left;color:#fff;background: #26B99A;">Net Total</th>
                                                <th style="width: 3%;text-align: center;color:#fff;background: #26B99A;"><i class="fa fa-envelope"></i></th>
                                                <th style="width: 3%;text-align: center;color:#fff;background: #26B99A;"><i class="fa fa-pencil-square-o"></i></th>
                                            </tr>
                                        </thead>

                                        <tbody id="purchase_order_list_body">
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="h10"></div>

                        <div class="clearfix"></div>
                        <div class="h10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</div>
</div>
</div>







<!-- /***************mail Pop up Body End**************/-->
<input type="hidden" name="po_req_url" id="po_req_url" value="{{route('ExtensionsValley.purchase.purchaseOrder')}}">



<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
{!! Html::script('packages/extensionsvalley/master/canvas/js/jsonh.js') !!}
{!! Html::script('packages/Purchase/default/javascript/purchase_order.js?v=2') !!}
<script type="text/javascript">

  $('.date_format').datetimepicker({
    format: "DD-MM-YYYY"

            });
</script>
@stop
@section('javascript_extra')
<script type="text/javascript">




    var rund_val = 0;
    function po_pagereload() {
    window.location = "{!! route('ExtensionsValley.purchase.purchaseOrder') !!}";
    }

    // function add_itemto_master() {

    // }
    $(document).ready(function(){
        serachPurchaseOrder(0, 0);
        setTimeout(function() {
        // $('#menu_toggle').trigger('click');
        // $(body).addClass('sidebar-collapse');
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    }, 300);

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });

    })



    function fillVendorDetialsNew(code, vendor_list_po, contact_no = '') {
    $('#vendor_list_po').val(vendor_list_po);
    $('#vendor_list_po_hidden').val(code);
    $('#vendorListAjaxDiv').hide();
    $('#vendor_list_po').attr('title', contact_no);
    item_list_final =[];
    serach_po();
    }
    function fillVendorDetialsNew1(code1, vendor_list_po1, contact_no = '') {
    $('#vendor_list_po1').val(vendor_list_po1);
    $('#vendor_list_po_hidden1').val(code1);
    $('#vendor_list_po1').attr('title', contact_no);
    $('#vendorListAjaxDiv1').hide();
    }

    function fillVendorDetialsNew2(code2, vendor_list_po2, vendor_state_id, contact_no = '') {
    $('#vendor_list_po2').val(vendor_list_po2);
    $('#vendor_list_po_hidden2').val(code2);
    $('#vendor_list_po2').attr('title', contact_no);
    $('#vendorListAjaxDiv2').hide();
    $('#vendor_state_id').val(vendor_state_id);
    if (vendor_state_id == ''){
    alert("Please update vendor state before creating GRN Bill !!");
    } else{
    var single_tax_status = 1;
    if (single_tax_status != '1'){
    if (default_state_id != vendor_state_id){
    alert("Vendor state is different. PO Bill tax set to IGST.");
    is_igst = 1;
    } else{
    is_igst = 0;
    }
    } else{

    is_igst = 0;
    }
    }
    }

    function clearpovendor(){
    $("#vendor_list_po").val('');
    $("#vendor_list_po_hidden").val('');
    }
    function clearpovendor1(){

    $("#vendor_list_po1").val('');
    $("#vendor_list_po_hidden1").val('');
    }


</script>
<input type='hidden' id='purchase_order_serch_route' value="{{route('ExtensionsValley.purchase.purchaseOrder')}}">

<script type="text/javascript">

    @if (!empty($is_save) && ($is_save == 1))
        $('.nav-tabs a[href="#po_list"]').tab('show');
        serachPurchaseOrder(1, 0);
    @endif
        @if (isset($_GET) && !empty($_GET))
        $('.nav-tabs a[href="#po_list"]').tab('show');
        serachPurchaseOrder(0, 1);
    @endif


$(document).on('keyup change', 'input[name="bill_discount_value"]', function(event){
    console.log('fg');
    $(this).val($(this).val().replace(/[^0-9.]/g, ""));
    });
    $('#vendor_list_po').keyup(delay(function (event) {

    var keycheck = /[a-zA-Z0-9 & ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
    if ($('#vendor_list_po_hidden').val() != "") {
    $('#vendor_list_po_hidden').val('');
    }
    var vendor_list_po = $(this).val();
     vendor_list_po = vendor_list_po.trim();
    if (vendor_list_po == "") {
    $("#vendorListAjaxDiv").html("");
    } else {
    var url = '';
    var param = {vendor_list_po:vendor_list_po};
    $.ajax({
    type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
            // aler('ggg');
            $("#vendorListAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#vendorListAjaxDiv").html(html).show();
            $("#vendorListAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {

            }
    });
    }

    } else {
    ajax_list_key_down('vendorListAjaxDiv', event);
    }
    }, 1000));
    $('#vendor_list_po').on('keydown', function(event){
    if (event.keyCode === 13){
    ajaxlistenter('vendorListAjaxDiv');
    return false;
    }
    });



    $('#vendor_list_po1').keyup(delay(function (event) {

    var keycheck = /[a-zA-Z0-9 & ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
    if ($('#vendor_list_po_hidden1').val() != "") {
        $('#vendor_list_po_hidden1').val('');
    }
    var vendor_list_po1 = $(this).val();
     vendor_list_po1 = vendor_list_po1.trim();
    if (vendor_list_po1 == "") {
    $("#vendorListAjaxDiv1").html("");
    } else {
    var url = '';
    var param = {vendor_list_po1:vendor_list_po1};
    $.ajax({
    type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
            // aler('ggg');
            $("#vendorListAjaxDiv1").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#vendorListAjaxDiv1").html(html).show();
            $("#vendorListAjaxDiv1").find('li').first().addClass('liHover');
            },
            complete: function () {

            }
    });
    }

    } else {
    ajax_list_key_down('vendorListAjaxDiv1', event);
    }
    }, 1000));
    $('#vendor_list_po1').on('keydown', function(event){
    if (event.keyCode === 13){
    ajaxlistenter('vendorListAjaxDiv1');
    return false;
    }
    });
    $('#vendor_list_po2').keyup(delay(function (event) {

    var keycheck = /[a-zA-Z0-9 & ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
    if ($('#vendor_list_po_hidden2').val() != "") {
    $('#vendor_list_po_hidden2').val('');
    }
    var vendor_list_po2 = $(this).val();
     vendor_list_po2 = vendor_list_po2.trim();
    if (vendor_list_po2 == "") {
    $("#vendorListAjaxDiv2").html("");
    } else {
    var url = '';
    var param = {vendor_list_po2:vendor_list_po2};
    $.ajax({
    type: "GET",
            url: url,
            data: param,
            beforeSend: function () {
            // aler('ggg');
            $("#vendorListAjaxDiv1").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#vendorListAjaxDiv2").html(html).show();
            $("#vendorListAjaxDiv2").find('li').first().addClass('liHover');
            },
            complete: function () {

            }
    });
    }

    } else {
    ajax_list_key_down('vendorListAjaxDiv2', event);
    }
    }, 1000));
    $('#vendor_list_po2').on('keydown', function(event){
        if (event.keyCode === 13){
        ajaxlistenter('vendorListAjaxDiv2');
        return false;
    }
    });
    /**Po fuction call for notfication link to redirect to edit**/
    <?php
    if (isset($req_id) && $req_id != '') {
        ?>
            purchaseEditList({{$req_id}}, 0);
        <?php
    }
    ?>
    // $("#fixTable").tableHeadFixer();
    $(document).on('click', '.pagination a', function (e) {
        getPosts($(this).attr('href'));
        e.preventDefault();
    });
    $(document).ready(function () {
        // $('#from_category1').multiselect({
        //     enableClickableOptGroups: true,
        //     enableCollapsibleOptGroups: true,
        //     enableFiltering: true,
        //     includeSelectAllOption: true,
        //     minWidth: 180,
        //     buttonWidth: '300px',
        //     maxHeight: 400,
        //     enableCaseInsensitiveFiltering: true,
        // });
        // setTimeout(function () {
        // $(body).addClass('sidebar-collapse');
        // }, 300);
    });


    $('.date_format').datetimepicker({
    format: "DD-MM-YYYY"

            });
    //$(".date_format").mask("99-99-9999", {placeholder: "dd-mm-yyyy"});

    $('#quotation_date').datetimepicker({
    format: "DD-MM-YYYY"
            });
    //$("#quotation_date").mask("99-99-9999 99:99:99", {placeholder: "dd-mm-yyyy HH:mm:ss"});

    $('#po_date').datetimepicker({
    format: "DD-MM-YYYY"
            });
    //$("#po_date").mask("99-99-9999 99:99:99", {placeholder: "dd-mm-yyyy HH:mm:ss"});

    $('.ajaxSearchBox').bind('DOMNodeInserted DOMNodeRemoved', function () {
    $('html').one('click', function () {
    $(".ajaxSearchBox").hide();
    });
    });
    $('input[data="date"]').datetimepicker({format: "DD-MM-YYYY"});
    //Date Picker Initiate

</script>
@endsection
