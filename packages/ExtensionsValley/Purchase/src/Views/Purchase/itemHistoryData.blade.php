<div class="theadscroll always-visible" style="position: relative; height: 400px;">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;" id="main_row_tbl">
        <thead>
            <tr class="table_header_bg ">
                <th style="text-align: center;">GRN No</th>
                <th style="text-align: center;">GRN Date</th>
                <th style="text-align: center;">Vendor Name </th>
                <th style="text-align: center;">GRN Qty </th>
                <th style="text-align: center;">GRN Unit</th>
                <th style="text-align: center;">GRN Rate</th>
                <th style="text-align: center;">Unit Cost</th>
                <th style="text-align: center;">Free Qty</th>
                <th style="text-align: center;">MRP</th>
            </tr>
        </thead>
        <tbody>
            <?php
             if(isset($resultData) && count($resultData)){
                foreach ($resultData as $pre_detail){
                    ?>
            <tr>
                <td class="common_td_rules">{{ $pre_detail->r_grn_no }}</td>
                <td class="common_td_rules">{{ date('M-d-Y',strtotime($pre_detail->r_grn_date)) }}</td>
                <td class="common_td_rules">{{ $pre_detail->r_vendor_name }}</td>
                <td class="td_common_numeric_rules">{{ number_format($pre_detail->r_grn_qty, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($pre_detail->r_grn_unit, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($pre_detail->r_grn_rate, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($pre_detail->r_unit_cost, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($pre_detail->r_free_qty, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($pre_detail->r_mrp, 2, '.', '') }}</td>
            </tr>
            <?php  }
            }else {
                        ?>
            <tr>
                <td colspan="9" class="text-center">No Record Found</td>
            </tr>
            <?php
                    } ?>
        </tbody>
    </table>

</div>
