<?php
$total_cgst = 0.0;
$total_sgst = 0.0;
$total_igst = 0.0;
$total_cess = 0.0;
$total_discount = 0.0;
$total_othercharges = 0.0;
$total_freightcharges = 0.0;
$total_amt = 0.0;
$net_total = 0.0;
$tot_tax = 0.0;
$oth_charges = 0.0;
$cgst = 0.0;
$sgst = 0.0;
$igst = 0.0;
?>
<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" class="table no-margin table_sm table-striped no-border styled-table"
                        style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td style="text-align: center" colspan="12">
                                    <strong>Purchase Order</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="common_td_rules" colspan="6" width="60%"><strong>Supplier :</strong>
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </td>
                                <td class="common_td_rules" colspan="6" width="40%">
                                    <strong>
                                        <?= @$po_number ? $po_number : '' ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="common_td_rules" colspan="6" width="60%"><strong>Address :</strong>
                                    <?= @$vendor_data['vendor_address'] ? $vendor_data['vendor_address'] : '' ?>
                                </td>
                                <td class="common_td_rules" colspan="6" width="40%"><strong>P.O.DATE :</strong>
                                    <?= @$po_date ? $po_date : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="common_td_rules" colspan="6" width="60%"><strong>Email :</strong>
                                    <?= @$vendor_data['vendor_email'] ? $vendor_data['vendor_email'] : '' ?>
                                </td>
                                <td class="common_td_rules" colspan="6" width="40%"><strong>Manufacturer :</strong>
                                    <?= @$manufacturer_name ? $manufacturer_name : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="common_td_rules" colspan="6" width="60%"><strong>Telephone No. :</strong>
                                    <?= @$vendor_data['contact_no'] ? $vendor_data['contact_no'] : '' ?>
                                </td>
                                <td class="common_td_rules" colspan="6" width="40%"><strong>Location :</strong>
                                    <?= @$location_name ? $location_name : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" width="4%"><strong>#</strong></td>
                                <td style="text-align: center" width="36%"><strong>Item Name</strong></td>
                                <td style="text-align: center" width="6%"><strong>Qty</strong></td>
                                <td style="text-align: center" width="6%"><strong>Free <br> Qty</strong></td>
                                <td style="text-align: center" width="10%"><strong>Unit</strong></td>
                                <td style="text-align: center" width="6%"><strong>Rate</strong></td>
                                <td style="text-align: center" width="6%"><strong>Disc.<br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>CGST<br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>SGST <br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>IGST<br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>Total Rate</strong></td>
                                <td style="text-align: center" width="6%"><strong>Net Rate</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($item_string)!=0){
                                foreach ($item_string as $each) {
                                    $string=$each['item_desc'];
                                    $total_cgst += floatval($each['cgst_amt']);
                                    $total_sgst += floatval($each['sgst_amt']);
                                    $total_igst += floatval($each['igst_amt']);
                                    $total_discount += floatval($each['discount_amt']);
                                    $total_othercharges += floatval($each['othercharge_amt']);
                                    $total_freightcharges += floatval($each['flightcharge_amt']);
                                    $total_amt += floatval($each['totalRate']);
                                    $net_total += floatval($each['netRate']);
                                    $cgst += floatval($each['cgst_amt']);
                                    $sgst += floatval($each['sgst_amt']);
                                    $igst += floatval($each['igst_amt']);
                                    ?>
                            <tr>
                                <td>
                                    <?= $i ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= htmlspecialchars($string)  ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['pur_qty'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['free_qty'], 2, '.', '') ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$uom_array[$each['item_id']] ? $uom_array[$each['item_id']] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['item_rate'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['discount_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['cgst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['sgst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['igst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['totalRate'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['netRate'], 2, '.', '') ?>
                                </td>
                            </tr>
                            <?php
                                       $i++;
                                        }
                                        $tot_tax = floatval($total_cgst) + floatval($total_sgst) + floatval($total_igst) + floatval($total_cess);
                                        $oth_charges = floatval($total_othercharges) + floatval($total_freightcharges);
                                        $net_total = floatval($net_total) - floatval($bill_discount)+floatval($round_off);
                                        }else {
                                            ?>
                            <tr>
                                <td colspan="12" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                            <?php
                                        }
                                        ?>

                            <tr>
                                <td colspan="11" class="td_common_numeric_rules"> Gross Amount :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($total_amt, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11" class="td_common_numeric_rules">Item Discount(-) : </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($total_discount, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11" class="td_common_numeric_rules">Bill Discount(-) : </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($bill_discount, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11" class="td_common_numeric_rules">Freight/Other :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($oth_charges, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11" class="td_common_numeric_rules">CGST :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($cgst, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11" class="td_common_numeric_rules">SGST :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($sgst, 2, '.', '') ?>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="11" class="td_common_numeric_rules">IGST : </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($igst, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr class="bg-blue">
                                <td colspan="11" class="td_common_numeric_rules">Total Tax :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($tot_tax, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr class="bg-blue">
                                <td colspan="11" class="td_common_numeric_rules">RoundOff :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($round_off, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr class="bg-blue">
                                <td colspan="11" class="td_common_numeric_rules">Total Amount :</td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($net_total, 2, '.', '') ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="common_td_rules" width="40%">
                                    <strong> GST NO.</strong>
                                    <?= @$hospital_header[0]->gst_no ? $hospital_header[0]->gst_no : '' ?>
                                </td>
                                <td colspan="8" class="common_td_rules" width="60%">
                                    <strong>Drug License No.</strong>
                                    <?= @$drug_licence_no ? $drug_licence_no : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" class="common_td_rules" width="60%" colspan="2">
                                    <strong>Buyer : </strong>
                                    <?= @$hospital_header[0]->address ? $hospital_header[0]->address : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="common_td_rules" colspan="12">
                                    <strong>PO Terms And Conditions: </strong>
                                    <?= @$full_remarks ? $full_remarks : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="common_td_rules" width="30%">
                                    <strong>PO Status : </strong>
                                    <?= $po_status ?>
                                </td>
                                <td colspan="4" class="common_td_rules" width="40%">
                                    <strong>Approved By : </strong>
                                    <?= @$users[0]->approved_by ? $users[0]->approved_by : '' ?>
                                </td>
                                <td colspan="4" class="common_td_rules" width="30%">
                                    <strong>Verified By : </strong>
                                    <?= @$users[0]->verified_by ? $users[0]->verified_by : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="common_td_rules" width="30%">
                                    <strong>Prepared By : </strong>
                                    <?= @$users[0]->created_by ? $users[0]->created_by : '' ?>
                                </td>
                                <td colspan="4" class="common_td_rules" width="30%">
                                    <strong>Printed By : </strong>
                                    <?= @$users[0]->printed_by ? $users[0]->printed_by : '' ?>
                                </td>
                                <td colspan="4" class="common_td_rules" width="40%">
                                    <strong>Print Date Time : </strong>
                                    <?= date('M-d-Y H:i:s') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
