<div class="theadscroll" style="position: relative; height: 400px;">
    <?php
        if($batch_status=='false'){
    ?>
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="80%">Location Name</th>
                <th class="10%">Batch No</th>
                <th class="10%">Stock</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($item_details) > 0){
                $net_total=0.0;
                foreach ($item_details as $data){
                        $net_total += floatval( $data->stock);
                        ?>
            <tr>
                <td class="common_td_rules">{{ $data->location_name }}</td>
                <td class="common_td_rules">{{ $data->batch_no }}</td>
                <td class="td_common_numeric_rules">{{ $data->stock }}</td>
            </tr>
            <?php }
            ?>
            <tr class="btn-info">
                <th colspan="2" class="common_td_rules">Total</th>
                <th class="td_common_numeric_rules">{{ number_format($net_total, 2, '.', '') }}</th>
            </tr>
            <?php
            }else {
                ?>
            <tr style="text-align: center;">
                <td colspan="3" style="text-align: center">No Data Found.!</td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
    <?php } else {
        ?>
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="90%">Location Name</th>
                <th class="10%">Stock</th>
            </tr>
        </thead>
        <tbody>
            <?php
        if (sizeof($item_details) > 0){
            $net_total=0.0;
            foreach ($item_details as $data){
                    $net_total += floatval( $data->stock);
                    ?>
            <tr>
                <td class="common_td_rules">{{ $data->location_name }}</td>
                <td class="td_common_numeric_rules">{{ $data->stock }}</td>
            </tr>
            <?php }
        ?>
            <tr class="btn-info">
                <th class="common_td_rules">Total</th>
                <th class="td_common_numeric_rules">{{ number_format($net_total, 2, '.', '') }}</th>
            </tr>
            <?php
        }else {
            ?>
            <tr style="text-align: center;">
                <td colspan="2" style="text-align: center">No Data Found.!</td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
    }?>

</div>
