<?php
$row_count=1;
if(count($res)!=0){
    foreach ($res as $each) {
        $string=$each->item_desc;
        $narration = $each->narration;
        $readonly_status='';
        if($each->is_free=='1'){
            $readonly_status='readonly';
        }
        ?>
<tr onclick="lastClicKItemRow(<?= $row_count ?>)" style="background: #FFF;" class="row_class"
    id="row_data_{{ $row_count }}">
    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
    </td>
    <td style="position: relative;">
        <input readonly style="border-radius: 4px;" type="text" required="" autocomplete="off"
            id="item_desc_{{ $row_count }}" value="<?= htmlspecialchars($string) ?>" class="form-control popinput" name="item_desc[]"
            placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
               position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td style="text-align: center">
        <button type="button" id="chargesbtnbtn{{ $row_count }}"
            onclick="getProductCharges(<?= $row_count ?>, <?= $each->item_id ?>);" style="padding:3px 3px"
            class="btn btn-success"><i id="chargesbtnspin{{ $row_count }}" class="fa fa-calculator"></i></button>
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            value="<?= $each->pur_qnty ?>" autocomplete="off" id="request_qty{{ $row_count }}"
            oninput='validateNumber(this)' type="text">
        <input type='hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' value="<?= $each->item_code ?>" id="item_code_hidden{{ $row_count }}"
            name="item_code_hidden">
        <input type='hidden' value="<?= $each->item_id ?>" id="itemid_hidden{{ $row_count }}">
        <input type='hidden' id="vat_onselect{{ $row_count }}" value="2">
        <input type='hidden' id="free_vat_onselect{{ $row_count }}" value="2">
    </td>


    <td title="Unit converion">
        <select name="uom_select[]" onchange='changeUOMVal({{ $row_count }},2)'
            id="uom_select_id_{{ $row_count }}" class="form-control">
            <?= @$item_array[$each->item_id] ? $item_array[$each->item_id] : '' ?>
        </select>
    </td>
    <td>
        <input class="form-control number_class" <?= $readonly_status ?> autocomplete="off"
            id="request_rate{{ $row_count }}" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' type="text" value="<?= $each->pur_rate ?>">
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' autocomplete="off" id="request_freeqty{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value="<?= $each->free_qty ?>">
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' autocomplete="off" id="request_mrp{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value="<?= $each->mrp ?>">
        <input type="hidden" id="pack_size<?= $row_count ?>" name="pack_size[]" value="1">
    </td>
    <td>
        <input class="form-control number_class" readonly id="request_tax{{ $row_count }}" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control number_class" readonly id="request_discount{{ $row_count }}" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control number_class" readonly autocomplete="off" id="request_netamt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value="<?= $each->ponet_amount ?>">
    </td>
    <td><input class="form-control" autocomplete="off" onblur="listDataCalculation(<?= $row_count ?>)"
            id="request_comments{{ $row_count }}" name="comments[]" type="text" value="<?= $narration ?>">
    </td>
    <td style="text-align: center">
        <button type="button" title='Global Item' id="getGlobalItemsbtn{{ $row_count }}"
            onclick="getGlobalItems({{ $row_count }})" style="padding:3px 3px" class="btn btn-primary"><i
                id="getGlobalItemsspin{{ $row_count }}" class="fa fa-globe"></i></button>
    </td>
    <td style="text-align: center">
        <button type="button" title='Item History' id="ItemHistoryBtn{{ $row_count }}"
            onclick="getIemsHistory({{ $row_count }})" style="padding:3px 3px" class="btn btn-warning"><i
                id="ItemHistoryBtnspin{{ $row_count }}" class="fa fa-history"></i></button>
    </td>
    <td style="text-align: center">
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
<?php
 $row_count++;
    }
}
?>
