
<div class="col-md-4 no-padding" style="margin-bottom: 15px;">
              <div class="custom-float-label-control mate-input-box">
                                                    <label class="custom_floatlabel">Location</label>
                                                    @php
                                                    $sql="select location_name,id from location order by location_name";
                                                    $location_list = DB::select($sql);
                                                    @endphp

                                                   <select class="form-control" onchange="location_list('{{$item_code}}');" title="Select location" id="location_list"  name="location_list"><option  value="0">Select location</option> @foreach($location_list as $items)<option value="{{$items->id}}">{{ $items->location_name}}</option>@endforeach</select>
                                                </div>



        </div>
        <div class="clearfix"></div>
        <div class="theadscroll always-visible" style="position: relative; height: 410px;">

            <table class="table table_sm table-bordered ">
                <thead>
                    <tr class="table_header_bg">

                        <th>Item Name</th>
                        <th>Batch No</th>
                        <th>Stock</th>
                        <th>Location Name</th>



                    </tr>
                </thead>
                <tbody>
                      @if(sizeof($item_details)>0)

                    @foreach($item_details as $data)


                        <tr >


                             <td >{{htmlspecialchars($data->item_desc)}}</td>
                             <td >{{$data->batch_no}}</td>
                             <td >{{$data->stock}}</td>
                             <td >{{$data->location_name}}</td>

                        </tr>

                    @endforeach
                     @else
          <tr style="text-align: center;">
    <td colspan="8" >No Data Found.!</td>
</tr>
            @endif
                </tbody>
            </table>

        </div>

