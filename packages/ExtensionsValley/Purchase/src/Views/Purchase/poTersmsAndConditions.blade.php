<div class="row padding_sm">
    <div class="col-md-12 padding_sm theadscroll" style="position: relative; height: 500px;">
        <?php
if(count($data)!=0){
    foreach ($data as $each) {
        $checked="";
        if (in_array($each->id, $po_terms))
        {
            $checked="checked";
        }

        ?>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="checkbox checkbox-success inline no-margin">
                <input <?= $checked ?> onclick="addPotermsConditions(<?= $each->id ?>)" class="po_terms_conditions"
                    type="checkbox" data-detail-id="291" value="1683" id="po_terms_conditions<?= $each->id ?>">
                <label for="po_terms_conditions<?= $each->id ?>"><?= strtoupper($each->terms) ?>
                </label>
            </div>

        </div>
        <?php
    }
}else {
    ?>
        <div class="row padding_sm">
            <div style="text-align: center" class="col-md-12 padding_sm">
                No Result Found
            </div>
        </div>
        <?php
}
?>
    </div>
</div>
