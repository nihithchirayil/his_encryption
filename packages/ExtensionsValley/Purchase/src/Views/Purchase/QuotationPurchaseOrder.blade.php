@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')


<div class="modal fade" id="sendEmailCCModel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 800px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Email CC Users</h4>
            </div>
            <div class="modal-body" style="height: 440px" id="">
                <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 410px;">
                            <table class="table no-margin table_sm table-striped no-border styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th width="40%">Name</th>
                                        <th width="55%">Email</th>
                                        <th width="5%" style="text-align: center">
                                            <button type="button" onclick="manageEmailCCUsers('','')"
                                                class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="addUsersToEmail">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger"> Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" onclick="addUserEmailCC()" type="button" class="btn btn-primary"> Add
                    to
                    Email <i class="fa fa-save"></i></button>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="purchase_charge_modal" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="width: 900px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <h4 class="modal-title">Purchase Charge</h4>
            </div>
            <div class="modal-body">
                <div id='purchase_charges_div' style="min-height: 400px">

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-4 padding_sm red">
                    <strong> Press Esc Key to Close the Popup </strong>
                </div>
                <div class="col-md-8 padding_sm">
                    <button id="addTaxDetailsBtn" style="padding: 3px 3px" type="button"
                        onclick="addItemVendorDetails()" class="btn btn-success">OK
                        <i id="addTaxDetailsSpin" class="fa fa-save"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getQuotationItemListModel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 600px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getQuotationItemListModelHeader"></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-3 padding_sm pull-right" style="margin-top: -5px;">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input onclick="getGlobalItems('',2)" type="checkbox" id="without_batch_no"
                            name="without_batch_no">
                        <label style="padding-left:2px;" for="without_batch_no">
                            Without Batch No</label><br>
                    </div>
                </div>
                <div id="getQuotationItemListModelDiv"></div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" onclick="resetheadScrollData()" type="button" data-dismiss="modal"
                    aria-label="Close" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getVendorQuotationModel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getVendorQuotationModelHeader"></h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height: 75px;">
                        <div class="col-md-3 padding_sm rfqnumber_sequencediv" style="display: none">
                            <div class="mate-input-box">
                                <label for="">Quotation Number</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="quotation_number"
                                    name="quotation_number" value="" placeholder="Quotation Number">
                                <input type="hidden" name="quotation_no_hidden" value="" id="quotation_no_hidden">
                                <div class="ajaxSearchBox" id="rfq_idAjaxDiv"
                                    style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm vendorcontract_sequencediv" style="display: none">
                            <div class="mate-input-box">
                                <label style="margin-top: -2px;">Contract Number</label>
                                <div class="clearfix"></div>
                                <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
                                    id="searchContract" onkeyup="searchContractNo()" class="form-control popinput"
                                    placeholder="Contract Number">
                                <input type="hidden" id="contract_id_hidden" value="0">
                                <div class="ajaxSearchBox" id="ajaxContractSearchBox"
                                    style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                                                                                                                                                                                                                                                                                                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="poListfromtype" value="0">

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="from_date" id="from_date"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="to_date" id="to_date" value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm rfqnumber_sequencediv" style="display: none">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" id="searchQuotationBtn" onclick="searchQuotation()"
                                class="btn btn-block light_purple_bg"><i id="searchQuotationSpin"
                                    class="fa fa-search"></i>
                                Search Quotations</button>
                        </div>
                        <div class="col-md-2 padding_sm vendorcontract_sequencediv" style="display: none">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" id="searchContractBtn" onclick="searchVendorContract()"
                                class="btn btn-block light_purple_bg"><i id="searchContractSpin"
                                    class="fa fa-search"></i>
                                Search Contract</button>
                        </div>
                    </div>
                </div>
                <div id="searchDataDiv"></div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="getAllProductModel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getAllProductModelHeader"></h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix" style="height: 75px;">
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Product Category</label>
                                    <div id="product_category_search_div">
                                        <select class="form-control select2"
                                            onchange="search_product_category();searchListItems();"
                                            id="product_category_search">

                                            <option value="">Product Category</option>
                                            <?php
                                            if(count($category)!=0){
                                                foreach ($category as $each) {
                                                    ?>
                                            <option value="<?= $each->id ?>">
                                                <?= $each->name ?>
                                            </option>
                                            <?php
                                                }
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Product Sub Category</label>
                                    <div id="product_subcategory_div">
                                        <select class="form-control select2" id="product_subcategory_search"
                                            onchange="searchListItems();">
                                            <option value="">Product Sub Category</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlbel">Search Item</label>
                                    <div class="clearfix"></div>
                                    <input onkeyup="searchProducts('searchpolistitem','purchaseCatageoryDataList')"
                                        class="form-control custom_floatinput" id="searchpolistitem" type="text">
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Supplier</label>
                                    <input onkeyup="searchSubVendor(this.id,event)" type="text" value=""
                                        class="form-control" autocomplete="off" id="searchSubVendor" placeholder="">
                                    <div id="ajaxSubVendorSearchBox" class="ajaxSearchBox"
                                        style="margin-top: -16px; width: 100%; z-index: 9999;">
                                    </div>
                                    <input type="hidden" value="0" id="vendorSubitem_id_hidden">
                                    <input type="hidden" value="" id="vendorSubitem_code_hidden">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm" style="margin-top: 27px">
                                <button id="printOrderListBtn" type="button" onclick="printOrderList()"
                                    class="btn btn-warning btn-block">
                                    <i id="printOrderListSpin" class="fa fa-print"></i> Print</button>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: 27px">
                                <button id="searchListItemBtn" type="button" onclick="searchListItems()"
                                    class="btn btn-primary btn-block">
                                    <i id="searchListItemSpin" class="fa fa-search"></i> Search</button>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="searchListDataDiv"></div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getPoTermsConditionsModel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Po Terms And Conditions</h4>
            </div>
            <div class="modal-body" style="min-height: 500px">
                <div class="col-md-7 padding_sm">
                    <label class="custom_floatlabel"><strong> Payment Terms </strong></label>
                    <textarea cols="10" rows="10" name="payment_terms" id="payment_terms"
                        class="form-control"></textarea>
                </div>
                <div class="col-md-5 padding_sm">
                    <div id="getPoTermsConditionsDiv"></div>
                </div>
            </div>
            <div class="modal-footer">
                <hr>
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="print_po_model" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Print Purchase Order</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div id="print_po_modelDiv"></div>

            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button onclick="exceller();" style="padding: 3px 3px" type="button" class="btn btn-warning">Excel
                    <i class="fa fa-file-excel-o"></i></button>
                <button onclick="DownloadPdfSendMail(0)" style="padding: 3px 3px" type="button" id="file_pdf_btn"
                    class="btn btn-success">PDF <i id="file_pdf_spin" class="fa fa-file"></i>
                </button>
                <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                    class="btn btn-primary">Print <i class="fa fa-print"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                        value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                        value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="lineTablePrint('result_container_div')" class="btn bg-primary pull-right"
                        style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="right_col" role="main">
    <div class="row padding_sm">
        <div class="col-md-2 padding_sm">
            <strong>
                <?= $title ?>
            </strong>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
        <input type="hidden" id="row_count_id" name="row_count" value="1">
        <input type="hidden" id="po_id" value="<?= $po_id ?>">
        <input type="hidden" id="copy_status" value="<?= $copy_status ?>">
        <input type="hidden" id="calculationSelectRowID" value="0">
        <input type="hidden" id="decimal_configuration" value="<?= $decimal_configuration ?>">
        <input type="hidden" id="location_defaultvalue" value="">
        <input type="hidden" value='{{ $unit_master }}' id="hidden_unit_master">
        <input type="hidden" value='{{ $config_data }}' id="config_details_hidden">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <input type="hidden" id="lastClickedItemCode" value="">
        <input type="hidden" id="lastClickedRowID" value="0">
        <input type="hidden" id="pono_hidden" value="{{ $po_no }}">
        <input type="hidden" id="purchase_type_hidden" value="<?= $purchase_type ?>">
        <input type="hidden" id="defaultCategory" value="<?= $defaultCategory ?>">
    </div>
    <div class="row padding_sm">
        <div id="poplast_item_box_div" class="poplast_item_box col-md-12 padding_sm"
            style="z-index: 999999; display: block;">
        </div>
        <div class="col-md-12 padding_sm">
            <div class="col-md-4 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                        <div class="col-md-12 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Supplier <span style="color: red">*</span></label>
                                <input onkeyup="searchNewVendor(this.id,event)" type="text" name="searchnewvendor"
                                    value="" class="form-control" autocomplete="off" id="searchnewvendor"
                                    placeholder="">
                                <div id="ajaxVendorSearchBox" class="ajaxSearchBox"
                                    style="margin-top: -16px; width: 100%; z-index: 9999;">
                                </div>
                                <input type="hidden" name="vendoritem_id_hidden" value="0" id="vendoritem_id_hidden">
                                <input type="hidden" name="vendoritem_code_hidden" value="" id="vendoritem_code_hidden">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Manufacturer</label>
                                <?php

                                    $vendor_list = \DB::table('manufacturer')
                                        ->where('status', 1)
                                        ->orderBy('manufacturer_name', 'asc')
                                        ->pluck('manufacturer_name', 'manufacturer_code');
                                    ?>
                                {!! Form::select('manufacturer_list', $vendor_list, '', [
                                'class' => 'form-control custom_floatinput select2',
                                'placeholder' => 'Select Manufacturer',
                                'title' => 'Select Manufacturer',
                                'id' => 'manufacturer_list',
                                'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Location</label>
                                <?php
                                    $user_id = \Auth::user()->id;
                                    $default_location = \DB::table('users')
                                        ->select('default_location')
                                        ->where('id', $user_id)
                                        ->first();
                                    $user_id = \Auth::user()->id;
                                    $roleData = [];

                                    $roleData = $roleData = \DB::table('user_group')
                                        ->where('user_id', $user_id)
                                        ->where('status', 1)
                                        ->pluck('group_id');
                                    $listdept = $to_location = \DB::table('location as l')
                                        ->leftJoin('location_group as r', 'l.id', '=', 'r.location_id')
                                        ->whereIn('r.group_id', $roleData)
                                        ->whereNotIn('l.location_type', ['-1', '10'])
                                        ->where('is_store', 1)
                                        ->orderBy('location_name')
                                        ->pluck('l.location_name', 'l.location_code');
                                    ?>
                                {!! Form::select('deprt_list', $listdept, $default_location->default_location, [
                                'class' => 'form-control custom_floatinput select2',
                                'onchange' => 'changePurchaseLocation()',
                                'id' => 'deprt_list',
                                'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm text-center" style="margin-top: 5px">
                            <h5 class="red"><span id="po_number_show">
                                    <?= 'PO Number : ' . $po_no ?>
                                </span></h5>
                        </div>
                        <div class="col-md-6 padding_sm text-center" style="margin-top: 5px">
                            <h5 class="red">PO Status : <span id="po_status_show"></span></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">PO</label>
                                <input class="form-control custom_floatinput" id="po_added" placeholder="PO"
                                    name="po_added" type="text" readonly="">
                                <input type="hidden" id="is_amended" value='0' />
                                <input type="hidden" id="is_po_status" value='0' />
                                <input type="hidden" id="parent_po_status" value='0' />
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">PO Date</label>
                                <div class="clearfix"></div>
                                <input class="form-control custom_floatinput" value="<?= date('M-d-Y') ?>" readonly
                                    id="po_date" placeholder="PO Date" name="po_date" type="text">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Remarks</label>
                                <input name="payment_remarks" id="payment_remarks" class="form-control"
                                    autocomplete="off" type="text">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm" style="margin-top: 21px">
                            <button class="btn btn-success btn-block" style="padding: 4px 0px;" type="button"
                                onclick="getPoTermsConditions();" id="po_terms_condtion_btn"><i
                                    id="po_terms_condtion_spin" class="fa fa-tumblr"></i> Po Terms</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Disc.Type</label>
                                {!! Form::select('bill_discount_type', ['1' => 'Percentage', '2' => 'Amount'], '', [
                                'class' => 'form-control',
                                'id' => 'bill_discount_type',
                                'style' => 'color:#555555; padding:4px 12px;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Discount Value</label>
                                <input class="form-control" value="0.00" type="text" maxlength="8"
                                    id="bill_discount_value" name="bill_discount_value">
                            </div>
                        </div>
                        <div class="col-md-8 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Discount Amount</label>
                                <input readonly class="form-control" value="0.00" type="text" maxlength="8"
                                    id="bill_discount_amount" name="bill_discount_amount">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top: 21px">
                            <button class="btn btn-warning btn-block" style="padding: 4px 0px;" type="button"
                                onclick="BillDiscountcalculate();" id="bill_discount_generate">Apply Discount <i
                                    class="fa fa-dollar"></i></button>
                        </div>
                        <div id="po_printpreview_btn_id" class="col-md-6 pull-right padding_sm"
                            style="margin-top: 10px;display: none">
                            <button type="button" onclick="getPrintPreview()" id="print_preview_btn"
                                class="btn btn-success btn-block"><i class="fa fa-eye" id="print_preview_spin"></i>
                                Preview
                            </button>
                            <a id="downloadPoPFDlinka" style="display: none" href="" download="">
                                <img id="downloadPoPFDlinkimg">
                            </a>
                        </div>
                        <div id="po_sendmail_btn_id" class="col-md-6 pull-right padding_sm"
                            style="margin-top: 10px;display: none">
                            <button type="button" onclick="DownloadPdfSendMail(1)" id="po_sendmailbtn"
                                class="btn btn-success btn-block"><i id="po_sendmailbtn_spin"
                                    class="fa fa-envelope"></i>
                                Send Mail</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                        <table class="table no-margin table_sm no-border grn_total_table">
                            <tbody>
                                <tr>
                                    <td width="50%"><label class="text-bold">+ Gross Amount</label></td>
                                    <td><input readonly="" type="text" id="item_value" Value="0.00" name="item_value"
                                            class="form-control text-right"></td>
                                </tr>
                                <tr>
                                    <td><label class="text-bold">- Total Disc.</label></td>
                                    <td><input readonly="" type="text" id="payment_other" Value="0.00"
                                            name="payment_other" class="form-control text-right"></td>
                                </tr>
                                <tr>
                                    <td><label class="text-bold">+ Total Tax</label></td>
                                    <td><input readonly="" id="payment_othercharges" type="text" Value="0.00"
                                            name="payment_othercharges" class="form-control text-right"></td>
                                </tr>
                                <tr>
                                    <td><label class="text-bold">+ Freight/Other</label></td>
                                    <td><input readonly="" autocomplete="off" name="freight_orther_charges"
                                            id="freight_orther_charges" type="text" Value="0.00"
                                            class="form-control text-right">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label class="text-bold">RoundOff</label></td>
                                    <td><input value="0.00" autocomplete="off" onblur="updateRoundAmt('round_off','')"
                                            onkeyup="getRoundOffAmt()" name="round_off" id="round_off" type="text"
                                            class="form-control text-right">

                                    </td>
                                </tr>
                                <tr>
                                    <td><label class="text-bold">Total Amount</label></td>
                                    <td><input readonly="" name="po_amount"
                                            style="font-size: 16px !important;font-weight: 700;" id="po_amount"
                                            type="text" Value="0.00" class="form-control text-right">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding_sm" style="margin-top: 10px;">
        <div class="col-md-12 padding_sm">
            <div class="col-md-5 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 36px;">
                        <div id="po_quotatinlist_btn_id" class="col-md-3 padding_sm">
                            <button type="button" onclick="getVendorSavedData(1)" id="po_fromrfq_btn"
                                class="btn bg-info btn-block"><i class="fa fa-quora" id="po_fromrfq_spin"></i>
                                Quotations
                                List
                            </button>
                        </div>
                        <div id="po_pricecontract_list_btn_id" class="col-md-3 padding_sm">
                            <button type="button" onclick="getVendorSavedData(2)" id="po_fromvendorpricing_btn"
                                class="btn bg-info btn-block"><i class="fa fa-vine" id="po_fromvendorpricing_spin"></i>
                                Price Contract
                            </button>
                        </div>
                        <div id="po_reorderlist_btn_id" class="col-md-3 padding_sm">
                            <button type="button" onclick="getVendorSavedData(3)" id="po_reorderlist_btn"
                                class="btn bg-info btn-block"><i class="fa fa-opera" id="po_reorderlist_spin"></i>
                                Re-order Level Items
                            </button>
                        </div>
                        <div id="po_productslist_btn_id" class="col-md-3 padding_sm">
                            <button type="button" onclick="getVendorSavedData(4)" id="po_allproduct_btn"
                                class="btn bg-info btn-block"><i class="fa fa-product-hunt" id="po_allproduct_spin"></i>
                                All
                                Products
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 36px;">
                        <?php if (isset($accessPerm['approval']) && $accessPerm['approval'] == '1') { ?>
                        <div id="po_approve_btn_id" class="col-md-2 pull-right padding_sm" style="display: none">
                            <button type="button" onclick="savePurchaseOrder(3)" id="po_approve_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-buysellads"
                                    id="po_approve_spin"></i>
                                Approve</button>
                        </div>
                        <div id="po_amend_btn_id" class="col-md-2 pull-right padding_sm" style="display: none">
                            <button type="button" onclick="savePurchaseOrder(4)" id="po_amend_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-edit" id="po_amend_spin"></i>
                                Amend</button>
                        </div>
                        <?php }if (isset($accessPerm['verification']) && $accessPerm['verification'] == '1') { ?>
                        <div id="po_verify_btn_id" class="col-md-2 pull-right padding_sm" style="display: none">
                            <button type="button" onclick="savePurchaseOrder(5)" id="po_verify_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-user-secret"
                                    id="po_verify_spin"></i>
                                Verify </button>
                        </div>
                        <?php }if (isset($accessPerm['add']) && $accessPerm['add'] == '1') {  ?>
                        <div id="po_post_btn_id" class="col-md-2 pull-right padding_sm" style="display: none">
                            <button type="button" onclick="savePurchaseOrder(1)" id="po_post_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-paper-plane-o"
                                    id="po_post_spin"></i>
                                Save
                            </button>
                        </div>
                        <?php }if ((isset($accessPerm['add']) && ($accessPerm['add'] == '1')) || (isset($accessPerm['approval']) && $accessPerm['approval'] == '1' )) { ?>
                        <div id="po_close_btn_id" class="col-md-2 pull-right padding_sm" style="display: none">
                            <button type="button" onclick="savePurchaseOrder(2)" id="po_close_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-times-circle-o"
                                    id="po_close_spin"></i>
                                Close</button>
                        </div>

                        <div id="po_cancel_btn_id" class="col-md-2 pull-right padding_sm" style="display: none">
                            <button type="button" onclick="savePurchaseOrder(0)" id="po_cancel_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-fast-backward"
                                    id="po_cancel_spin"></i>
                                Cancel</button>
                        </div>

                        <?php } ?>
                        <div id="po_email_cc_btn_id" class="col-md-2 pull-left padding_sm" style="display: none">
                            <button type="button" onclick="createPoSendEmailUsers()" id="addEmail_cc_btn"
                                class="btn bg-green btn-block savepobtn"><i class="fa fa-cc" id="addEmail_cc_Spin"></i>
                                Email CC</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding_sm" style="margin-top: 10px">
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 400px;">

                    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
                        style="border: 1px solid #CCC;" id="main_row_tbl">
                        <thead>
                            <tr class="table_header_bg ">
                                <th width="2%">SL.No.</th>
                                <th width="20%">
                                    <input id="issue_search_box"
                                        onkeyup="searchProducts('issue_search_box','added_new_list_table_product');"
                                        type="text" placeholder="Item Search.. "
                                        style="color: #000;display: none;width: 90%;">
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                            class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Product
                                        Name</span>
                                </th>
                                <th width="3%" class="text-center"><i class="fa fa-calculator"></i></th>
                                <th width="5%">Qty</th>
                                <th width="5%">Unit</th>
                                <th width="5%">Rate</th>
                                <th width="5%">Free Qty</th>
                                <th width="5%">MRP </th>
                                <th width="5%">Tax </th>
                                <th width="5%">Discount </th>
                                <th width="7%">Net Amt</th>
                                <th width="10%">Remarks</th>
                                <th width="2%"><i class="fa fa-globe"></i></th>
                                <th width="2%"><i class="fa fa-history"></i></th>
                                <th width="2%"><button type="button" style="margin-top: 5px;" id="addQuotationRowbtn"
                                        onclick="getNewRowInserted();" class="btn btn-primary add_row_btn"><i
                                            id="addQuotationRowSpin" class="fa fa-plus"></i></button></th>
                            </tr>
                        </thead>
                        <tbody id="added_new_list_table_product">

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/QuotationPurchaseOrder.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
@endsection
