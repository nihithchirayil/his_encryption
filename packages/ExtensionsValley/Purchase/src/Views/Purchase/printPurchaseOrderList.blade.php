<?php
$total_cgst = 0.0;
$total_sgst = 0.0;
$total_igst = 0.0;
$total_cess = 0.0;
$total_discount = 0.0;
$total_othercharges = 0.0;
$total_freightcharges = 0.0;
$total_amt = 0.0;
$net_total = 0.0;
$tot_tax = 0.0;
$oth_charges = 0.0;
$cgst = 0.0;
$sgst = 0.0;
$igst = 0.0;
?>

<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" class="table no-margin table_sm table-striped no-border styled-table"
                        style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td style="text-align: center" colspan="12">
                                    <strong>Purchase Order</strong>
                                </td>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="6" width="60%"><strong>Supplier :</strong>
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="6" width="40%">PO No :
                                    <?= @$poHeadDetails[0]->po_no ? $poHeadDetails[0]->po_no : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="6" width="60%">Address :
                                    <?= @$poHeadDetails[0]->vendor_address ? $poHeadDetails[0]->vendor_address : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="6" width="40%">P.O.DATE :
                                    <?= @$poHeadDetails[0]->po_date ? $poHeadDetails[0]->po_date : '' ?>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="6" width="100%">Email :
                                    <?= @$poHeadDetails[0]->email ? $poHeadDetails[0]->email : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="6" width="60%">Telephone No. :
                                    <?= @$poHeadDetails[0]->contact_no ? $poHeadDetails[0]->contact_no : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="6" width="40%">Location :
                                    <?= @$poHeadDetails[0]->location_name ? $poHeadDetails[0]->location_name : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td style="text-align: center;" width="4%"><strong>#</strong></td>
                                <td style="text-align: center" width="36%"><strong>Item Name</strong></td>
                                <td style="text-align: center" width="6%"><strong>Qty</strong></td>
                                <td style="text-align: center" width="6%"><strong>Free <br> Qty</strong></td>
                                <td style="text-align: center" width="10%"><strong>Unit</strong></td>
                                <td style="text-align: center" width="6%"><strong>Rate</strong></td>
                                <td style="text-align: center" width="6%"><strong>Disc.<br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>CGST<br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>SGST <br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>IGST<br> (%)</strong></td>
                                <td style="text-align: center" width="6%"><strong>Total Rate</strong></td>
                                <td style="text-align: center" width="6%"><strong>Net Rate</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($item_string)!=0){
                                foreach ($item_string as $each) {
                                    $string=$each['item_desc'];
                                    $total_cgst += floatval($each['cgst_amt']);
                                    $total_sgst += floatval($each['sgst_amt']);
                                    $total_igst += floatval($each['igst_amt']);
                                    $total_discount += floatval($each['discount_amt']);
                                    $total_othercharges += floatval($each['othercharge_amt']);
                                    $total_freightcharges += floatval($each['flightcharge_amt']);
                                    $total_amt += floatval($each['totalRate']);
                                    $net_total += floatval($each['netRate']);
                                    $cgst += floatval($each['cgst_amt']);
                                    $sgst += floatval($each['sgst_amt']);
                                    $igst += floatval($each['igst_amt']);
                                    ?>
                            <tr>
                                <td>
                                    <?= $i ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= htmlspecialchars($string)  ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['pur_qty'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['free_qty'], 2, '.', '') ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$uom_array[$each['item_id']] ? $uom_array[$each['item_id']] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['item_rate'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['discount_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['cgst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['sgst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['igst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['totalRate'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['netRate'], 2, '.', '') ?>
                                </td>
                            </tr>
                            <?php
                                       $i++;
                                        }
                                        $tot_tax = floatval($total_cgst) + floatval($total_sgst) + floatval($total_igst) + floatval($total_cess);
                                        $bill_discount_amount= @$poHeadDetails[0]->bill_discount_amount ? $poHeadDetails[0]->bill_discount_amount : 0;
                                        $round_amount= @$poHeadDetails[0]->round_amount ? $poHeadDetails[0]->round_amount : 0;
                                        $oth_charges = floatval($total_othercharges) + floatval($total_freightcharges);
                                        $net_total  = floatval($net_total) - floatval($bill_discount_amount)+floatval($round_amount);
                                        }else {
                                            ?>
                            <tr>
                                <td colspan="12" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                            <?php
                                        }
                                        ?>

                            <tr>
                                <th colspan="11" class="td_common_numeric_rules"> Gross Amount :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($total_amt, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="11" class="td_common_numeric_rules">Item Discount(-) : </th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($total_discount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="11" class="td_common_numeric_rules">Bill Discount(-) : </th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($bill_discount_amount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="11" class="td_common_numeric_rules">Freight/Other :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($oth_charges, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="11" class="td_common_numeric_rules">CGST :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($cgst, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="11" class="td_common_numeric_rules">SGST :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($sgst, 2, '.', '') ?>
                                </th>

                            </tr>
                            <tr>
                                <th colspan="11" class="td_common_numeric_rules">IGST : </th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($igst, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="">
                                <th colspan="11" class="td_common_numeric_rules">Total Tax :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($tot_tax, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="">
                                <th colspan="11" class="td_common_numeric_rules">RoundOff :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($round_amount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="">
                                <th colspan="11" class="td_common_numeric_rules">Total Amount :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($net_total, 2, '.', '') ?>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
