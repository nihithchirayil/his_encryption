 <style type="text/css">
   .inner_div{
      background-color: #fff;
      max-height: 300px;
      overflow: auto;
      list-style-type: none;
      margin: 0;
      padding: 0px 10px;
      height: 300px;
   }
   .outer_div{
      height: 310px;
   }
   table td{
     word-break: break-all;
     overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
   }
 /*  td {
  white-space: nowrap;
}*/

.clusterize-headers {
  overflow: hidden;
}
.grn_class {
  background-color: #7CFC00 !important;
}
 </style>
 <div class="modal fade" id="requisition_popup" role="dialog">
        <div class="modal-dialog " style="width: 100%;">
          <div class="modal-content">
            <div class="modal-header" style="min-height: 20px;padding:10px 15px; background:#19a0d8; color:#ffffff;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Purchase Requisition</h4>
            </div>
            <div class="modal-body" style="font-size: 11px; background: #FFF;
">
           
            <div class="row" style="background-color: #ECF0F1;margin-right: 0px;margin-left: 0px;">
              <div class="col-md-12 panel panel-primary no-margin" style="padding-bottom: 10px;background-color: #dbe0bd;" >
               <div class="" >
                 <div class="row" >
                   <div class="col-md-3">
                     <label>From : </label>
                     <input type="text" name="fdate" id="fdate" autocomplete="off" value="{{ date(\Site::getConfig('date_format_web'), strtotime(date(\Site::getConfig('date_format_web')).'-2 month')) }}" class="form-control">
                   </div>
                   <div class="col-md-3">
                     <label>To : </label>
                     <input type="text" name="tdate" id="tdate" value="{{ date(\Site::getConfig('date_format_web')) }}" autocomplete="off" class="form-control datepicker">
                   </div>
                   <div class="col-md-3">
                     <label>Location : </label>
                      <?php
                      $main_location=\ExtensionsValley\core\models\Location::where('purchase_allowed', '=', '1')->select(['location_code'])->get()->toArray();
                       $user_id = \Auth::user()->id;
                        $usr_res = Users::where('id', $user_id)->first();
                        $query = "select group_id from user_group where user_id=$user_id and status = 1";
                        $result = \DB::select($query);
                        $result = json_decode(json_encode($result), true);
                        $resultRoles = array_column($result, 'group_id');
                        // $resultRoles=array('1', '218');  
                                $listdept = \ExtensionsValley\core\models\Location::select(DB::raw("location.location_name,location.location_code"))
                                                ->join('location_group', 'location_group.location_code', '=', 'location.location_code')
                                        ->whereIn('location.location_code',$main_location)
                                        ->where('location.status',1)
                                        ->whereIn('location_group.group_id', $resultRoles)
                                        ->where('location_group.status',1)
                                        ->lists('location_name', 'location_code');
                                ?>
                                {!! Form::select('from_department',$listdept,'',['class' => 'form-control custom_floatinput selectbox_initializr','placeholder' => 'From Department','title' => 'From Department','id' => 'from_department','style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                   </div>
                   <div class="col-md-3" id="getreport">
                   <label>  </label>
                     <button  class="btn btn-primary form-control" onclick="getReport();" style="margin-top: 10px;">Get Report</button>
                     
                   </div>
                    <div class="col-md-3" id="result" style="display: none;">
                     <button  class="btn btn-primary form-control" onclick="search_again();" style="margin-top: 25px;width: 45%;">Search Again</button>
                     <button  class="btn btn-primary form-control" onclick="post();" style="margin-top: 25px;width: 45%;">Post</button>
                     
                   </div>
                 </div>
               </div>
              </div>
              
              <div class="col-md-12"  id="search_filter"><hr style="border-top: 1px solid #0c0c0c;">
              <div class="row" >
                <div class="col-md-2">
                  <input type="radio" name="itm" id="itm_all"  onclick="selectAll(this,'inner_div');" value="All"><label for="itm_all">All</label>
                  <label class="pull-right" for="itm_sel">Selective</label><input type="radio" name="itm" id="itm_sel" class="pull-right " onclick="selective('inner_div');" value="Selective" checked>
                </div>
                <div class="col-md-4">
                  <input type="radio" name="sub_itm" id="sub_itm_all" onclick="selectAll(this,'cat_div');" value="all"><label for="sub_itm_all">All</label>
                  <label class="pull-right" for="sub_itm_sel">Selective</label><input type="radio" name="sub_itm" id="sub_itm_sel" class="pull-right " onclick="selective('cat_div');" checked value="selective">
                </div>
                <div class="col-md-6">
                  <input type="radio" name="sub_cat_all" id="sub_cat_all" onclick="selectAll(this,'contentArea');" checked><label for="sub_cat_all">All</label>
                  <label style="padding-left: 83px;">Search On Subcategory &nbsp;</label><input type="text" name="search" id="myInput" onkeyup="myFunction()" placeholder="Search for sub items.." autocomplete="off">
                  <label class="pull-right" for="sub_cat" style="padding-right: 20px;">Selective</label><input type="radio" name="sub_cat_all" id="sub_cat" class="pull-right " onclick="selective('contentArea');">
                  <label class="pull-right" for="sub_cat_de" style="padding-right: 20px;">Deselection</label><input type="radio" name="sub_cat_all" id="sub_cat_de" class="pull-right " onclick="deSelective('contentArea');">
                </div>
              </div>

              <div class="row">
                <div class="col-md-2 outer_div" >
                <?php
                $main_items=\ExtensionsValley\core\models\Groups::where('is_requisition', 't')->where('status','1')->get()->pluck('name','group_code')->toArray(); 
                ?>
                   <ul class="inner_div" style="list-style-type: none;margin: 0;padding: 0px 10px;" id="inner_div">
                    <?php foreach ($main_items as $key => $main_item) {?>
                      <li><input type='checkbox' onclick="requisitionChk('<?php echo $key;?>');" name='<?php echo $key;?>'/><lable><?php echo $main_item;?></lable></li>
                      <?php } ?>
                   </ul>
                </div>
                <div class="col-md-4" id="scrollArea1" class="clusterize-scroll outer_div" >
                
                    <ul id="cat_div" class="clusterize-content inner_div"  >
                   <li class="clusterize-no-data">No data Found…</li>
                    </ul>
                    
                </div>
                <div class="col-md-6 outer_div" >
                  <div id="scrollArea" class="clusterize-scroll" style="background-color: #fff;height: 300px;max-height: 300px;">
                    <div id="contentArea" class="clusterize-content">
                      <div class="clusterize-no-data">No data Found…</div>
                    </div>
                  </div>
                </div>
              </div>                
              </div>


              <div class="col-md-12"  id="purchase_indent" style="display: none;"><hr style="border-top: 1px solid #0c0c0c;">
              <div class="clusterize">
               <!-- <div class="clusterize-headers"> -->
               <div class="clusterize">
               <div class="col-md-12">
                  <div style="width:15px;height: 15px;background: #89ef3b;border: solid 1px;display: block;float: left;padding-top: 8px; "></div> 
                  <div style="float: left;display: block;padding-left: 8px;font-size: 12px;padding-top: 3px;">Purchase Requisition Pending</div>
               </div>
               <div id="scrollArea5" class="clusterize-scroll" style="height: 64vh;position: relative;max-height: 64vh;overflow-x: hidden;">
               <table width="100%" style="font-size: 12px; border-collapse: collapse;background: khaki;" id="headersArea" class="table theadfix_wrapper_purch table-striped table-bordered table_sm table_condensed">
               <thead class="headergroupbg">
               <tr>
                <th width='2%'><input type="checkbox" name="selectAll" id="selectAllRow" onclick="selectAllRow();"></th>
                <th width='5%'>Item Code</th>
                <th width='21%'><input class='form-control' id='myInput1' onkeyup='mysearchFunction();' type='text' placeholder='Item Search.. '>Item Desc</th>
                <th width='7%'>Category</th>
                <th width='5%'>Sub Category</th>
                <th width='5%'>Generic Name</th>
                <th width='7%'>Vendor Name</th>
                
                <!-- <th>Selling Unit</th> -->
                <!-- <th width='3%'>Unit Name</th> -->
                
                <th width='6%'>Purchase Cost</th>
                <!-- <th>Purchase Unit</th> -->
                <th width='4%' title='Purchase Unit Name'>Purchase Unit Name</th>
                <th width='5%'>Stock</th>
                <th width='5%'>Global Stock</th>
                <th width='5%'>Free Qty</th>
                <th width='5%'>ROL</th>
                <th width='5%'>Actual Con</th>
                <th width='5%' title='Previous Quantity'>Prev.Req.Qty</th>
                <th width='5%' title='Consumption Quantity'>Con.Qty</th>
                <th width='8%' title='Purchase Quantity'>Pur.. Qty</th>
               </tr>
              </thead>             
                     <tbody id="contentArea5" class="clusterize-content">
                         <tr class="clusterize-no-data"><td colspan="15"> Loading data…</td></tr>
                     </tbody>
               </table>
            </div>
            </div>
                         
              </div>


            </div>
            </div>
          </div>
        </div>
      </div>
 </div>
       <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

{!! Html::script('packages/core/canvas/js/jsonh.js') !!}
{!! Html::style('packages/core/default/css/clusterize.css') !!}
{!! Html::script('packages/core/default/javascript/clusterize.js') !!}

    <!--   <link href="https://cdnjs.cloudflare.com/ajax/libs/clusterize.js/0.18.0/clusterize.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/clusterize.js/0.18.0/clusterize.js"></script> -->
<script type="text/javascript">
var req_item_arr=[],req_category=[],req_sub_cat=[],tot_qty=0;
   var dataParam=[]; 

function myFunction() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('myInput');
  filter = input.value.toUpperCase();
  ul = document.getElementById("contentArea");
  li = ul.getElementsByTagName('div');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName("lable")[0];
    if(a!=undefined){
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
  }
}

function requisitionPopup(){
  req_item_arr=[],req_category=[],req_sub_cat=[];
   $("#cat_div").html("<div class='clusterize-no-data'>No data Found…</div>");
   $("#contentArea").html("<div class='clusterize-no-data'>No data Found…</div>");
   $('#fdate').datetimepicker({
    format: "{{\Site::getConfig('day_format_JS')}}",
    });
   $('#tdate').datetimepicker({
    format: "{{\Site::getConfig('day_format_JS')}}",
    });
   $("#inner_div").find("input").each(function (){
      $(this).prop('checked', false); 
   });
   $("#purchase_indent").hide();
   $("#search_filter").show();
   $("#result").hide();
   $("#getreport").show();
   $("#sub_itm_all").prop('checked', false);
   var tot_qty=0;
   var dataParam=[];
}

function mysearchFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput1");
      filter = input.value.toUpperCase();
      table = document.getElementById("headersArea");
      tr = table.getElementsByTagName("tr");
      for (i = 1; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[2];

          // label =td.getElementsByTagName("label");
          // console.log(td.textContent.toUpperCase());
          
          
          if (td) {
              if (td.textContent.toUpperCase().indexOf(filter) > -1) {
                  tr[i].style.display = "";
              } else {
                  tr[i].style.display = "none";
              }
          }
      }
}

function selectAllRow(){
  // $("#contentArea5").find("input[type='checkbox']").each(function(){
  
    if($("#selectAllRow").prop('checked')==false){
      if($(this).is(':disabled') == false){
        $('.checkAll').attr('checked', false);
        $("input[name='con[]']").val(0.00);
        $('.checkAll').attr('disabled',true);
      }
    }else{
      if($(this).is(':disabled') == false){
        $('.checkAll').attr('checked', true);
      }
    }
  // });
}


function post(){
  tot_qty=0;
  dataParam=[];
  $("#contentArea5").find("input[type='checkbox']").each(function(){
    var elm=$(this).parent().parent();
    var chk_box=this;
    var data_arr=[];
      $(elm).find("input[type='text']").each(function (){
        if($(this).val()>0){
        data_arr["item_qty"]=$(this).val();  
        $(chk_box).prop('checked', true);
        tot_qty+=+$(this).val();
      }else{
          $(chk_box).prop('checked', false);
      }       

      });
    if($(this).prop('checked')==true){     
      data_arr["last_unitrate"]=$(this).data().purchase_cost;
      data_arr["global_stock"]=$(this).data().global_stock;
      data_arr["item_unit"]=$(this).data().purchase_unit;
      data_arr["item_code"]=$(this).data().item_code;
      data_arr["con"]=$(this).data().con_qty;
      data_arr["last_free_qty"]=$(this).data().last_free_qty;
      dataParam.push(data_arr);
    }
  });//+"&from_department="+$("#from_department").val()
  // var json = JSON.stringify(dataParam); 
  _token = $('#_token').val(); 
  var po_data = JSONH.pack(dataParam);
  po_data = JSON.stringify(po_data);
  if(dataParam.length > 0){
        $.ajax({

              type: "POST",
               url: "{{route('ExtensionsValley.renPurchase.postRequest')}}",
               // contentType: "application/json; charset=utf-8",
               data: {"po_data" : po_data,'requested_qty' : tot_qty, 'from_location_code' : $("#from_department").val(),_token:_token},
               // dataType: "json",
               beforeSend: function () {
                   $("#contentArea").html('<div style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></div>').show();
                  //  $.LoadingOverlay("show");
                  },
               success: function (res) {
                //  $.LoadingOverlay("hide");
                $('#requisition_popup').modal('toggle');
                // po_reqlist();
               },
               complete: function () {
                toastr.info("Purchase Requisition saved as draft !!");
                setTimeout(function(){
                  location.reload();
                }, 1000);
                
               }
        });
  }else{
    toastr.warning("Please select a Item !!");
    return;
  }
}
function roundUp(num, precision) {
  precision = Math.pow(10, precision)
  return Math.ceil(num * precision) / precision
}
function getReport(){
  $('#selectAllRow').attr('checked',true);
  var location=$("#from_department").val();
  if(location==""){
    $("#from_department").focus();
    toastr.error("Please select a location !!");
    return;
  }
  var tr=[];
  var $scroll = $('#scrollArea5');
var $content = $('#contentArea5');
var $headers = $("#headersArea");
// var fitHeaderColumns = (function() {
//   var prevWidth = [];
//   return function() {
//     var $firstRow = $content.find('tr:not(.clusterize-extra-row):first');
//     var columnsWidth = [];
//     $firstRow.children().each(function() {
//       columnsWidth.push($(this).width());
//     });
//     if (columnsWidth.toString() == prevWidth.toString()) return;
//     $headers.find('tr').children().each(function(i) {
//       $(this).width(columnsWidth[i]);
//     });
//     prevWidth = columnsWidth;
//   }
// })();
// var setHeaderWidth = function() {
//   $headers.width($content.width());
// }
// var setHeaderLeftMargin = function(scrollLeft) {
//   $headers.css('margin-left', -scrollLeft);
// }
var fdate=$("#fdate").val();
var tdate=$("#tdate").val();
// var report=$("#getReport").val();
var sub_cat=[];
$("#contentArea").find("input").each(function(){
  if($(this).prop('checked')==true){
sub_cat.push($(this).attr("name"));
  }
});
if(sub_cat.length < 1){
  toastr.error("Please select atleast on sub item !!");
    return;
}
if(req_item_arr.length < 1){
  toastr.error("Please select atleast on Item type !!");
    return;
}

   $.ajax({
               type: "GET",
               url: "{{route('ExtensionsValley.renPurchase.getReport')}}",
               data: {'fdate': fdate, 'tdate' : tdate, 'from_department' :location, 'sub_cat' : sub_cat,'gp_code':req_item_arr},
               datatype : 'json',
               beforeSend: function () {
                   $("#contentArea5").html(' <tr class="clusterize-no-data"><td colspan="15"><div style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></div></td></tr>').show();
                  //  $.LoadingOverlay("show");
                  },
               success: function (html) {
                //  $.LoadingOverlay("hide");
                 var $table = $('table.theadfix_wrapper_purch');
                 $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.clusterize-scroll');
                    }
                 });
                var ad=JSON.parse(html);
                $.each(ad, function (i) {
                  var td="";
                      // $.each(ad[i], function (key, val) {
                      //     // console.log(key + val);
                      //     td+="<td>"+val+"</td>";
                      // });
                      // tr.push("<tr><td>"+i+"</td>"+td+"</tr>"); 
                      // <td>"+ad[i]['selling_unit']+"</td>
                      var reoder=0;
                      var ac_reoder=0;
                      var checked = '';
                      var disable = '';
                      var con_cal = 0;
                      var act_con = 0;
                      var prev_qty = 0;
                      var grn_status = 0;
                      var grn_status_class = '';
                      if(+ad[i]['con'] > 0){
                        act_con =  +ad[i]['con'];
                        con_cal = +ad[i]['stock'] + +ad[i]['con'] + +ad[i]['lead_qty'];
                        if(con_cal < +ad[i]['reorder_cons']){
                          if(ad[i]['reorder_cons'] > ad[i]['stock']){
                            reoder=+ad[i]['reorder_cons']- (+ad[i]['stock'] + +ad[i]['con']);
                            
                          }
                        }
                      }
                      ac_reoder = +ad[i]['reorder_cons'];
                      // if(ad[i]['lead_qty']==0){
                      //   ad[i]['lead_qty']=1;
                      // }
                      var ress=((+act_con+ +(reoder))+ (+ad[i]['lead_qty']))/+ad[i]['pack_val']; 
                      var con=(+act_con+ +(reoder)+(+ad[i]['lead_qty']));
                      var prev_qty = ad[i]['requested_qty'];
                      var grn_status = ad[i]['grn_status'];
                      if(grn_status==0){
                        var grn_status_class = 'grn_class';
                      }
                      if(isNaN(ress)){
                        ress=0;
                      }
                      if(isNaN(con)){
                        con=0;
                        checked = '';
                        disable = 'disabled';
                        act_con = 0;
                      }else if(con > 0){
                        checked = 'checked';
                        disable = '';
                        act_con = +ad[i]['con'];
                      }else{
                        checked = '';
                        disable = 'disabled';
                        act_con = +ad[i]['con'];
                        con = 0;
                      }
                      td="<td>"+ad[i]['item_code']+"</td><td title='"+ad[i]['item_desc']+"'>"+ad[i]['item_desc']+"</td><td>"+ad[i]['cat_name']+"</td><td>"+ad[i]['sub_category']+"</td><td title='"+ad[i]['generic_name']+"'>"+ad[i]['generic_name']+"</td><td title='"+ad[i]['vendor_name']+"'>"+ad[i]['vendor_name']+"</td><td>"+ad[i]['purchase_cost']+"</td><td title='Unit Name : "+ad[i]['unit_name']+"'>"+ad[i]['purchase_unit_name']+"</td><td  title='"+ad[i]['stock']+"'>"+ad[i]['stock']+"</td><td>"+ad[i]['global_stock']+"</td><td>"+roundUp(ad[i]['last_free_qty'],0).toFixed(2)+"</td><td>"+ac_reoder+"</td><td>"+act_con+"</td><td>"+roundUp(prev_qty,0).toFixed(2)+"</td><td>"+roundUp(con,0).toFixed(2)+"</td><td><input type='text' title='"+roundUp(ress,0).toFixed(2)+"' class='form-control' value='"+roundUp(ress,0).toFixed(2)+"' id='con_pur_"+ad[i]['item_code']+"' name='con[]' onblur='con(this);'></td>";
//
                    //  tr+="<tr><td><input onclick='consumptionZero(\""+ad[i]['item_code']+"\");' type='checkbox' "+disable+" "+checked+" id='checkbox_"+ad[i]['item_code']+"' data-item_code='"+ad[i]['item_code']+"' data-purchase_cost='"+ad[i]['purchase_cost']+"' data-purchase_unit='"+ad[i]['purchase_unit']+"' data-global_stock='"+ad[i]['global_stock']+"' data-con_qty='"+ad[i]['con']+"' data-last_free_qty='"+ad[i]['last_free_qty']+"'></td>"+td+"</tr>"; 
                     tr.push("<tr class='"+grn_status_class+"'><td><input class='checkAll' onclick='consumptionZero(\""+ad[i]['item_code']+"\");' type='checkbox' "+disable+" "+checked+" id='checkbox_"+ad[i]['item_code']+"' data-item_code='"+ad[i]['item_code']+"' data-purchase_cost='"+ad[i]['purchase_cost']+"' data-purchase_unit='"+ad[i]['purchase_unit']+"' data-global_stock='"+ad[i]['global_stock']+"' data-con_qty='"+act_con+"' data-last_free_qty='"+ad[i]['last_free_qty']+"'></td>"+td+"</tr>"); 
                  });
                              
               },
               complete: function () {

              //  $("#contentArea5").html(tr);
              //  $('.theadfix_wrapper').floatThead('reflow');
                    var clusterize1 = new Clusterize({
                       rows: tr,
                       scrollId: 'scrollArea5',
                       contentId: 'contentArea5',
                       rows_in_block: 300,
                       blocks_in_cluster: 24,
                       verify_change: true
                       // callbacks: {
                       //   clusterChanged: function() {
                       //     // fitHeaderColumns();
                       //     // setHeaderWidth();
                       //   }
                       // }
                     }); 
              //      document.getElementById('scrollArea1').scrollTop = 500;
              //     // $(window).resize(_.debounce(fitHeaderColumns, 300));
              //       // alert(clusterize.getRowsAmount()); 

              //   $scroll.on('scroll', (function() {alert($(this).scrollTop());
              //   var prevScrollLeft = 0;
              //   return function() {
              //     var scrollLeft = $(this).scrollTop();
              //     if (scrollLeft == prevScrollLeft) return;
              //     prevScrollLeft = scrollLeft;

              //     setHeaderLeftMargin(scrollLeft);
              //   }
              // }()));
                   
               }
           });
   $("#getreport").hide();
   $("#result").show();
   $("#search_filter").hide();
  $("#purchase_indent").show();
}

function search_again(){
  $("#getreport").show();
  $("#search_filter").show();
  $("#result").hide();
  $("#purchase_indent").hide();

}
function consumptionZero(item_code) {
  $('#con_pur_'+item_code).val(0);
  $('#checkbox_'+item_code).prop('disabled',true);
}
function con(el){
  if($(el).val()>0){
    var chk=$(el).parent().parent();
    $(chk).find("input[type='checkbox']").prop('checked',true);
    $(chk).find("input[type='checkbox']").prop('disabled',false);
  }else{
    var chk=$(el).parent().parent();
    $(chk).find("input[type='checkbox']").prop('checked',false);
    $(chk).find("input[type='checkbox']").prop('disabled',true);
  }
}

function close_modal(modal_id){
  $("#"+modal_id).hide();
}

function selectAll(clk,div_id){
      // alert($("input[name='"+clk.name+"']:checked").val());
    $("#"+div_id).find("input").each(function (){
     if($(this).prop('checked')==false){
          $(this).trigger("click");
        }
          $(this).attr("disabled","disabled"); 
    });
}

function selective(div_id){
   $("#"+div_id).find("input").each(function (){
         $(this).removeAttr('disabled');
   });
}
function deSelective(div_id){
   $("#"+div_id).find("input").each(function (){
        $(this).prop('checked', false);
        $(this).removeAttr('disabled');
   });
}

function requisitionChk(req_id){
  if($.inArray(req_id, req_item_arr) !== -1){
    req_item_arr.splice( $.inArray(req_id, req_item_arr), 1 );
  }else{
    req_item_arr.push(req_id);
  }
          if(req_item_arr.length !== 0){
            var cat=[];
            var select_type=$('input[name="sub_itm"]').val();
            
          $.ajax({
               type: "GET",
               url: "{{route('ExtensionsValley.renPurchase.getItemname')}}",
               data: {'gp': req_item_arr},
               datatype : 'json',
               beforeSend: function () {
                   $("#cat_div").html('<li style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
               },
               success: function (html) {
                var ad=JSON.parse(html);
                for (var key in ad) {
                    if (ad.hasOwnProperty(key)) {
                      var key_val='"'+key+'"';
                        cat.push("<li><input type='checkbox' name='"+key+"' id='cat_"+key+"' onclick='requisition_cat("+key_val+");'/><lable for='cat_"+key+"' >"+ad[key]+"</lable></li>");
                    }
                  }                             
               },
               complete: function () {
                 req_category=[];
                 requisition_cat("NaN");
                   var clusterize = new Clusterize({
                      rows: cat,
                      scrollId: 'scrollArea1',
                      contentId: 'cat_div'
                    });
               }
           });
        }else{
          $("#cat_div").html("<div class='clusterize-no-data'>No data Found…</div>");
        }
  }

  function requisition_cat(cat_id){
    if(cat_id=="NaN"){
      $("#contentArea").html("<div class='clusterize-no-data'>No data Found…</div>");
      return;
    }
     if($.inArray(cat_id, req_category) !== -1){
    req_category.splice( $.inArray(cat_id, req_category), 1 );
  }else{
    req_category.push(cat_id);
  }

 if(req_category.length !== 0){
            var sub_cat=[];
          $.ajax({
               type: "GET",
               url: "{{route('ExtensionsValley.renPurchase.getSubCategory')}}",
               data: {'cat': req_category},
               datatype : 'json',
               beforeSend: function () {
                   $("#contentArea").html('<div style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></div>').show();
               },
               success: function (html) {
                var ad=JSON.parse(html);
                for (var key in ad) {
                    if (ad.hasOwnProperty(key)) {
                      var key_val='"'+key+'"';
                        sub_cat.push("<div><input checked type='checkbox' name='"+key+"' id='cat_"+key+"' disabled='disabled'/><lable for='cat_"+key+"' >"+ad[key]+"</lable></div>");
                    }
                  }                             
               },
               complete: function () {
                   var clusterize = new Clusterize({
                      rows: sub_cat,
                      scrollId: 'scrollArea',
                      contentId: 'contentArea'
                    });
               }
           });
        }else{
          $("#contentArea").html("<div class='clusterize-no-data'>No data Found…</div>");
        }
  } 
// var data = [];
// for(var i=0;i<1000000;i++){
//   if(i % 2 == 0){
//   data.push("<div><input type='checkbox' name='"+i+"'/><lable>"+i+"</lable></div>");
// }
// }
// var clusterize = new Clusterize({
//   rows: data,
//   scrollId: 'scrollArea2',
//   contentId: 'contentArea2'
// });

</script>