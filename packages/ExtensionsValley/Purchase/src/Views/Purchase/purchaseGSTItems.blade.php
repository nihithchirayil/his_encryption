<div class="col-md-12 padding_sm" style="margin-top:5px;">
    <div class="col-md-7 padding_sm">
        <h5 class="blue"><?= @$_POST['item_desc'] ? htmlspecialchars($_POST['item_desc']) : '' ?></h5>
    </div>
    <div class="col-md-5 padding_sm">
        <h5 class="green pull-right">Last Buyer : {{ $vendor_name ? $vendor_name : '-' }}</h5>
    </div>

</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm">
    <div class="col-md-6 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #C3C3C3;min-height: 220px;">
                <table class="table no-border table-striped table_sm">
                    <tbody>
                        <tr>
                            <?php
                            $checked_status = '';
                            $rate_status = '';
                            $free_tem = @$vendor_array[$row_id]['is_free'] ? $vendor_array[$row_id]['is_free'] : 0;
                            if ($free_tem == '1') {
                                $checked_status = 'checked';
                                $rate_status = 'readonly';
                            }
                            ?>
                            <td><label for=""><b>Free Item</b></label></td>
                            <td><input <?= $checked_status ?> type="checkbox" onclick="freeItem(<?= $row_id ?>)"
                                    id="free_item_id" name="free_item"></td>
                        </tr>
                        <tr>
                            <td><label for=""><b>Purchase Qty</b></label></td>
                            <td><input type="text" autocomplete="off"
                                    onblur="calculateAmount(<?= $row_id ?>,<?= $item_id ?>,100,1)"
                                    value="<?= @$_POST['qty'] ? $_POST['qty'] : 0 ?>" id="purchase_qty"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    class="form-control number_class">
                            </td>
                        </tr>
                        <tr>
                            <td><label for=""><b>Free Qty</b></label></td>
                            <td><input type="text" autocomplete="off"
                                    onblur="calculateAmount(<?= $row_id ?>,<?= $item_id ?>,100,1)"
                                    value="<?= @$_POST['free_qty'] ? $_POST['free_qty'] : 0 ?>"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    id="purchase_freeqty" class="form-control number_class">
                            </td>
                        </tr>
                        <tr>
                            <td><label for=""><b>UOM</b></label></td>
                            <td>
                                <select name="uom_select_pop[]" onchange="changeUOMVal({{ $row_id }},1)"
                                    id="uom_select_pop_id" class="form-control">
                                    <?php
                                       foreach ($uom_list as $pur_each){
                                        $selected="";
                                        $item_uom = @$vendor_array[$row_id]['item_unit'] ? $vendor_array[$row_id]['item_unit'] : 0;
                                        if($item_uom == $pur_each->id){
                                            $selected="selected";
                                        }
                                        ?>
                                    <option {{ $selected }} value='{{ $pur_each->id }}'
                                        data-uom_value='{{ $pur_each->conv_factor }}'>{{ $pur_each->uom_name }}
                                    </option>
                                    <?php
                                       }
                                        ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for=""><b>UOM Value</b></label></td>
                            <td><input type="text" id="uom_value_pop_id" readonly
                                    onblur="calculateAmount(<?= $row_id ?>,<?= $item_id ?>,100,1)"
                                    class="form-control number_class">
                            </td>
                        </tr>
                        <tr>
                            <td><label for=""><b>Rate/UOM</b></label></td>
                            <td>
                                <input <?= $rate_status ?> type="text" autocomplete="off"
                                    onblur="calculateAmount(<?= $row_id ?>,<?= $item_id ?>,100,1)"
                                    value="<?= @$_POST['rate'] ? $_POST['rate'] : 0 ?>"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    id="rate_per_uom" class="form-control validation number_class">
                                <input type="hidden" id="previous_rate_hidden"
                                    value="<?= @$_POST['rate'] ? $_POST['rate'] : 0 ?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label for=""><b>MRP/UOM</b></label></td>
                            <td><input type="text" autocomplete="off"
                                    onblur="calculateAmount(<?= $row_id ?>,<?= $item_id ?>,100,1)"
                                    value="<?= @$_POST['mrp'] ? $_POST['mrp'] : 0 ?>"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    id="mrp_per_uom" class="form-control validation number_class">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #C3C3C3;min-height: 220px;">
                <table class="table no-margin table_sm no-border" style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th class="common_td_rules" width="50%">Description</th>
                            <th class="common_td_rules" width="25%">Percentage</th>
                            <th class="common_td_rules" width="25%">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($charges_head as $charge) {
                            ?>
                        <tr>
                            <td class="common_td_rules">{{ $charge->name }}</td>
                            <?php
                            $val_per = 0;
                            $class = '';
                            $val_amt = 0;
                            $read_only = '';
                            $onblur_data = '';
                            if ($charge->calc_code == 'GA') {
                                $class = 'active_class';
                                $onblur_data = "onblur='calculateAmount($row_id,$item_id,$charge->id,1)'";
                            } elseif ($charge->calc_code == 'OTHCH') {
                                $read_only = 'readonly';
                                $class = '';
                            } else {
                                $onblur_data = "onblur='calculateAmount($row_id,$item_id,$charge->id,1)'";
                                $class = 'active_class';
                            }
                            if ($charge->status_code == 'CGST') {
                                $val_per = @$vendor_array[$row_id]['cgst_per'] ? $vendor_array[$row_id]['cgst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['cgst_amt'] ? $vendor_array[$row_id]['cgst_amt'] : 0;
                            } elseif ($charge->status_code == 'SGST') {
                                $val_per = @$vendor_array[$row_id]['sgst_per'] ? $vendor_array[$row_id]['sgst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['sgst_amt'] ? $vendor_array[$row_id]['sgst_amt'] : 0;
                            } elseif ($charge->status_code == 'IGST') {
                                $val_per = @$vendor_array[$row_id]['igst_per'] ? $vendor_array[$row_id]['igst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['igst_amt'] ? $vendor_array[$row_id]['igst_amt'] : 0;
                            } elseif ($charge->status_code == 'CESS') {
                                $val_per = @$vendor_array[$row_id]['cess_per'] ? $vendor_array[$row_id]['cess_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['cess_amt'] ? $vendor_array[$row_id]['cess_amt'] : 0;
                            } elseif ($charge->status_code == 'ITEMDISC') {
                                $val_per = @$vendor_array[$row_id]['discount_per'] ? $vendor_array[$row_id]['discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['discount_amt'] ? $vendor_array[$row_id]['discount_amt'] : 0;
                            } elseif ($charge->status_code == 'OTHCHG') {
                                $val_per = @$vendor_array[$row_id]['othercharge_per'] ? $vendor_array[$row_id]['othercharge_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['othercharge_amt'] ? $vendor_array[$row_id]['othercharge_amt'] : 0;
                            } elseif ($charge->status_code == 'FRCHG') {
                                $val_per = @$vendor_array[$row_id]['flightcharge_per'] ? $vendor_array[$row_id]['flightcharge_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['flightcharge_amt'] ? $vendor_array[$row_id]['flightcharge_amt'] : 0;
                            } elseif ($charge->status_code == 'SCHEMADISC') {
                                $val_per = @$vendor_array[$row_id]['special_discount_per'] ? $vendor_array[$row_id]['special_discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                $val_amt = @$vendor_array[$row_id]['special_discount_amt'] ? $vendor_array[$row_id]['special_discount_amt'] : 0;
                            }
                            ?>
                            <td>
                                <input <?= $read_only ?> <?= $onblur_data ?> data-code="{{ $charge->calc_code }}"
                                    data-id="{{ $charge->status_code }}"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    type="text" autocomplete="off"
                                    class="<?= $class ?> percentage_class form-control td_common_numeric_rules check_selling_price tax_class"
                                    id="charge_percentage{{ $charge->id }}" name="taxpercentage"
                                    value="<?= $val_per ?>" placeholder="{{ $charge->name }}">
                            </td>
                            <?php
                            $read_only = '';
                            $onblur_data = '';
                            if ($charge->calc_code == 'GA') {
                                $class = 'active_class';
                                $onblur_data = "onblur='calculateAmount($row_id,$item_id,$charge->id,2)'";
                            } elseif ($charge->calc_code != 'OTHCH') {
                                $read_only = 'readonly';
                                $class = '';
                            } else {
                                $onblur_data = "onblur='calculateAmount($row_id,$item_id,$charge->id,2)'";
                                $class = 'active_class';
                            }
                            ?>
                            <td>
                                <input <?= $read_only ?> <?= $onblur_data ?> data-id="{{ $charge->status_code }}"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    type="text" autocomplete="off" data-code="{{ $charge->calc_code }}"
                                    class="<?= $class ?> amount_class form-control td_common_numeric_rules check_selling_price nummeric_class tax_class"
                                    id="charge_amount{{ $charge->id }}" name="taxamount" value="<?= $val_amt ?>"
                                    placeholder="{{ $charge->name }}">
                            </td>
                        </tr>
                        <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 padding_sm" style="margin-top: 20px;">
        <div class="box no-border no-margin">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #C3C3C3;min-height: 137px;">
                <div class="col-md-12 padding_sm">
                    <div class="mate-input-box">
                        <label>Remarks</label>
                        <input value="<?= @$_POST['comments'] ? $_POST['comments'] : '' ?>" type="text"
                            autocomplete="off" id="purchase_remarks" class="form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="h10"></div>
                <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Rate</label>
                            <input type="text" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                style="color: red;font-weight:700;font-size:14px !important;"
                                class="active_class form-control number_class" readonly id='tot_rate_cal'>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label>Net Rate</label>
                            <input type="text" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                style="color: red;font-weight:700;font-size:14px !important;"
                                class="active_class form-control number_class" readonly id='charges_tot_pop'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
