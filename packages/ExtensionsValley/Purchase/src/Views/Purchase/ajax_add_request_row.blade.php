<tr onclick="lastClicKItemRow(<?= $row_count ?>)" style="background: #FFF;" class="row_class"
    id="row_data_{{ $row_count }}">
    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
    </td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
            id="item_desc_{{ $row_count }}" onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
            class="form-control popinput" name="item_desc[]" placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td style="text-align: center">
        <button type="button" disabled id="chargesbtnbtn{{ $row_count }}" style="padding:3px 3px"
            class="btn btn-success"><i id="chargesbtnspin{{ $row_count }}" class="fa fa-calculator"></i></button>
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)" autocomplete="off"
            id="request_qty{{ $row_count }}" oninput='validateNumber(this)' type="text" value=''>
        <input type='hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' id="item_code_hidden{{ $row_count }}" name="item_code_hidden" value="">
        <input type='hidden' id="vat_onselect{{ $row_count }}" value="2">
        <input type='hidden' id="free_vat_onselect{{ $row_count }}" value="2">
        <input type='hidden' id="itemid_hidden{{ $row_count }}" value="0">
    </td>
    <td title="Unit converion">
        <select name="uom_select[]" onchange='changeUOMVal({{ $row_count }},2)'
            id="uom_select_id_{{ $row_count }}" class="form-control">

        </select>
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' autocomplete="off" id="request_rate{{ $row_count }}" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' autocomplete="off" id="request_freeqty{{ $row_count }}" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)" autocomplete="off"
            id="request_mrp{{ $row_count }}" oninput='validateNumber(this)' type="text" value=''>
        <input type="hidden" id="pack_size<?= $row_count ?>" name="pack_size[]" value="1">
    </td>
    <td>
        <input class="form-control number_class" readonly id="request_tax{{ $row_count }}" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control number_class" readonly id="request_discount{{ $row_count }}" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control number_class" readonly autocomplete="off" id="request_netamt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value=''>
    </td>
    <td>
        <input class="form-control" autocomplete="off" onblur="listDataCalculation(<?= $row_count ?>)"
            id="request_comments{{ $row_count }}" name="comments[]" type="text" value="">
    </td>
    <td style="text-align: center">
        <button type="button" title='Global Item' disabled id="getGlobalItemsbtn{{ $row_count }}"
            onclick="getGlobalItems({{ $row_count }})" style="padding:3px 3px" class="btn btn-primary"><i
                id="getGlobalItemsspin{{ $row_count }}" class="fa fa-globe"></i></button>
    </td>
    <td style="text-align: center">
        <button type="button" title='Item History' id="ItemHistoryBtn{{ $row_count }}"
            onclick="getIemsHistory({{ $row_count }})" style="padding:3px 3px" class="btn btn-warning"><i
                id="ItemHistoryBtnspin{{ $row_count }}" class="fa fa-history"></i></button>
    </td>
    <td style="text-align: center">
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
