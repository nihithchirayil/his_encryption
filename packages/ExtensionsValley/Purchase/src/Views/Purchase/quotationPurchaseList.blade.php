@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')


<div class="modal fade" id="getPrintPreviewModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPrintPreviewModelHeader">Print Preview</h4>
            </div>
            <div class="modal-body" style="min-height: 500px">
                <div id="getPrintPreviewModelDiv"></div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button onclick="exceller_template_without_header('Purchase Order');" style="padding: 3px 3px"
                    type="button" class="btn btn-warning">Excel
                    <i class="fa fa-file-excel-o"></i></button>
                <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                    class="btn btn-primary">Print <i class="fa fa-print"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                        value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                        value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="lineTablePrint('result_container_div')" class="btn bg-primary pull-right"
                        style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="right_col" role="main">
    <div class="row padding_sm">
        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
            <thead>
                <tr class="table_header_bg">
                    <th colspan="11">
                        <?= strtoupper($title) ?>
                    </th>
                </tr>
            </thead>
        </table>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    </div>
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm">
            <div class="x_panel">
                <div class="row codfox_container">
                    <div class="col-md-7 padding_sm">
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">PO Number</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="po_number"
                                    name="po_number" value="">
                                <input type="hidden" name="po_id_hidden" value="" id="po_id_hidden">
                                <div class="ajaxSearchBox" id="po_idAjaxDiv"
                                    style="margin-top: -13px;width: 100%;z-index: 9999;display: none;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Location</label>
                                <?php
                                    $user_id = \Auth::user()->id;
                                    $default_location = \DB::table('users')
                                        ->select('default_location')
                                        ->where('id', $user_id)
                                        ->first();
                                    $user_id = \Auth::user()->id;
                                    $roleData = [];

                                    $roleData = \DB::table('user_group')
                                        ->where('user_id', $user_id)
                                        ->where('status', 1)
                                        ->pluck('group_id');
                                    $listdept = $to_location = \DB::table('location as l')
                                        ->leftJoin('location_group as r', 'l.id', '=', 'r.location_id')
                                        ->whereIn('r.group_id', $roleData)
                                        ->whereNotIn('l.location_type', ['-1', '10'])
                                        ->where('is_store', 1)
                                        ->orderBy('location_name')
                                        ->pluck('l.location_name', 'l.location_code');
                                    ?>
                                {!! Form::select('deprt_list', $listdept, $default_location->default_location, [
                                'class' => 'form-control custom_floatinput select2',
                                'onchange' => 'searchQuotation()',
                                'id' => 'deprt_list',
                                'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -4px;">Dates</label>
                                <div class="clearfix"></div>
                                {!! Form::select(
                                'po_dated',
                                ['1' => 'Created Date', '2' => 'Verified Date', '3' => 'Approved Date'],
                                $searchFields['status'] ?? null,
                                [
                                'class' => 'form-control status select2',
                                'id' => 'po_dated',
                                'onchange' => 'searchQuotation()',
                                ],
                                ) !!}
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="from_date" id="from_date"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="to_date" id="to_date" value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -4px;">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select(
                                'po_status',
                                [
                                '' => ' Select Status',
                                '0' => 'Cancelled',
                                '1' => 'Created',
                                '2' => 'Closed',
                                '3' => 'Approved',
                                '4' => 'Amend',
                                '5' => 'Verified',
                                ],
                                $searchFields['status'] ?? null,
                                [
                                'class' => 'form-control status select2',
                                'id' => 'po_status',
                                'onchange' => 'searchQuotation()',
                                ],
                                ) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Product Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="item_desc">
                            <div id="item_desc_AjaxDiv" class="ajaxSearchBox" style="display: none;margin-top: -16px;">
                            </div>
                            <input type="hidden" name="item_desc_hidden" id="item_desc_hidden">
                        </div>
                    </div>

                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Supplier</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="supplier_number"
                                name="po_number" value="">
                            <input type="hidden" value="" id="supplier_id_hidden">
                            <div class="ajaxSearchBox" id="supplier_idAjaxDiv"
                                style="margin-top: -13px;width: 100%;z-index: 9999;display: none;">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-1 padding_sm">
                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <button type="button" id="searchQuotationBtn" onclick="searchQuotation()" title="Search Po"
                                class="btn-block btn btn-primary"><i id="searchQuotationSpin"
                                    class="fa fa-search"></i>
                            </button>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <button type="button" onclick="addWindowLoad('quotationPurchaseOrder',0,0)"
                                title="Add New PO" class="btn-block btn btn-success"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <a style="display: none" id="downloadfile" href="" download> </a>
                            <button type="button" id="downloadDetailPoListExcelBtn"
                                onclick="downloadDetailPoListExcel()" title="Download PO in Detail"
                                class="btn btn-block bg-purple"><i id="downloadDetailPoListExcelSpin"
                                    class="fa fa-table"></i>
                            </button>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <a style="display: none" id="downloadfile" href="" download> </a>
                            <button type="button" id="downloadPoListExcelBtn" onclick="downloadPoListExcel()"
                                title="Download PO List" class="btn-block btn btn-warning"><i
                                    id="downloadPoListExcelSpin" class="fa fa-file-excel-o"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="searchDataDiv">

            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/quotationPurchaseList.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
