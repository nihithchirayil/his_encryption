@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<style>
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
  top: 100%;
  left: 50%;
  margin-left: -60px;
}

.tool:hover .tooltip {
  visibility: visible;
}
</style>
@endsection
@section('content-area')
     <!-- page content -->
<div class="right_col" >
<div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div id="po_requisition" class="tab-pane active">
                <div class="panel-body" style="padding: 0px;">
                    <div class="panel panel-default">
                        <div class="row" style="">

                            <div class="col-md-12">
                                <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;">
                                    <div class="clearfix panel-body" style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">
                                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                                            <div class="date custom-float-label-control">
                                                <label class="custom_floatlabel">Requisition No.</label>

                                        <?php
                                        $one_month_before_date = date(config('ext.date_format_db'), strtotime('-150 day', strtotime($current_date)));
                                        $listrequest = \ExtensionsValley\Purchase\models\PurchaseRequisitionHead::select(DB::raw("purchase_requisition_head.request_no,purchase_requisition_head.request_no"))
                                                    ->join('purchase_requisition_detail', 'purchase_requisition_head.id', '=', 'purchase_requisition_detail.request_head_id')->
                                                    where('purchase_requisition_detail.status', '=', '1')->where('purchase_requisition_head.status', '=', '4')->where('purchase_requisition_head.approved_date', '>', $one_month_before_date)->pluck('request_no', 'request_no');
                                        ?>
                                                {!! Form::select('item_requistion_no',$listrequest,'',['class' => 'form-control custom_floatinput selectbox_initializr','id' => 'item_requistion_no','style' => 'width:50%;color:#555555; padding:2px 12px;','placeholder' => 'Select Request No','title' => 'Select Request No']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-1 padding_sm">
                                            <div class="custom-float-label-control">
                                                <label class="custom_floatlabel">Contains
                                                </label>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
                                                <input type="checkbox" id="all_search" name="all_search" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="custom-float-label-control">
                                                <label class="custom_floatlabel">Item Description</label>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
                                                <input type="text" name="item_desc_search" autocomplete="off" class="form-control custom_floatinput" id="item_desc_search"  placeholder="Item Description">
                                                <div id="item_desc_searchCodeAjaxDiv" style="margin: 0; z-index: 999; font-size: 12px; width: 100%" class="ajaxSearchBox"></div>
                                                <input type="hidden" name="item_desc_search_hidden" id="item_desc_search_hidden" />

                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="custom-float-label-control">
                                                <label class="custom_floatlabel">Requested By</label>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
                                                <input type="text" name="requested_by_search" autocomplete="off" class="form-control custom_floatinput" id="requested_by_search"  placeholder="Requested By">
                                                <div id="requested_byCodeAjaxDiv" style="margin: 0; z-index: 999; font-size: 12px; width: 100%" class="ajaxSearchBox"></div>
                                                <input type="hidden" name="requested_by_hidden" id="requested_by_hidden" />

                                            </div>
                                        </div>

                                        <div class="col-md-2 padding_sm">
                                            <div style="padding-top: 3px;" class="custom-float-label-control">
                                                <label class="custom_floatlabel">Department</label>
                                        <?php
                                        $listdept = \extensionsvalley\Master\models\Location::select(DB::raw("location.location_name,location.location_code"))
                                            ->join('location_role', 'location_role.location_code', '=', 'location.location_code')
                                            ->whereIn('location_role.role_id', $resultRoles)
                                            ->whereIn('location.location_code', $main_location)
                                            ->where('location_role.status', 1)
                                            ->where('location.status', 1)
                                            ->pluck('location_name', 'location_code');
                                        ?>
                                                {!! Form::select('search_from_department',$listdept,'',['class' => 'form-control custom_floatinput selectbox_initializr','onchange'=>'department_change()','id' => 'search_from_department','style' => 'width:50%;color:#555555; padding:2px 12px;']) !!}
                                            </div>
                                        </div>


                                        <div class="col-xs-1 padding_sm" style="padding-bottom: 10px;">
                                            <div class="input-group date custom-float-label-control" id="datetimepicker5">
                                                <label class="custom_floatlabel">From Date</label>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
                                        <?php
                                        $one_month_before_date1 = date('d-M-Y', strtotime('-1 months', strtotime($current_date)));
                                        ?>
                                                <input type="text" name="from_date" autocomplete="off"
                                                       value="{{$one_month_before_date1}}"
                                                       class="form-control date_format" id="from_date"
                                                       placeholder="From Date">
                                                <span class="field_error">{{$errors->first('from_date')}}</span>
                                            </div>
                                        </div>

                                        <div class="col-xs-1 padding_sm" style="padding-bottom: 10px;">
                                            <div class="input-group date custom-float-label-control" id="datetimepicker5">
                                                <label class="custom_floatlabel">To Date</label>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
                                                <input type="text" name="to_date" autocomplete="off"
                                                       value="{{$current_date}}"
                                                       class="form-control date_format" id="to_date"
                                                       placeholder="To Date">
                                                <span class="field_error">{{$errors->first('to_date')}}</span>
                                            </div>
                                        </div>


                                        <div class="col-md-1 padding_sm">
                                            <div class="col-xs-6 padding_sm" >
                                                <div style="padding-top: 10px;" class="custom-float-label-control">
                                                    <label class="custom_floatlabel"></label>
                                                    <div class="clearfix"></div>
                                                    <button title="Search" style="padding: 0px 2px;" type="button" onclick="serach_po();" class="padding_sm btn btn-block btn-primary"> <i id="searchdataspin" class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>


                                            <div class="col-xs-6 padding_sm" >
                                                <div style="padding-top: 10px;" class="custom-float-label-control">
                                                    <label class="custom_floatlabel">&nbsp;</label>
                                                    <div class="clearfix"></div>
                                                    <button title="Next" style="padding: 0px 2px;" type="button" onclick="purchaseOrderList();" class="padding_sm btn btn-block btn-success"> <i id="generate_polistspin" class="fa fa-forward"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" >
                                            <div class="col-md-12">
                                                <div class="col-xs-3 no-padding" >
                                                    <div class="custom-float-label-control">
                                                        <label class="custom_floatlabel">Supplier</label>

                                                        <input type="text" name="vendor_list_po" value=""
                                                               class="form-control" autocomplete="off" id="vendor_list_po" placeholder="">
                                                        <span style="color: #d14;">{{ $errors->first('vendor_list_po') }}</span>
                                                        <div id="vendorListAjaxDiv" class="ajaxSearchBox" style="margin: 0; width: 100%; z-index: 9999;"></div>
                                                        <input type="hidden" name="vendor_list_po_hidden"
                                                               value="" id="vendor_list_po_hidden">
                                                    </div>
                                                </div>
                                                <div class="col-md-1 padding_sm text-left">
                                                    <label style="margin: 5px 0 0 0;">&nbsp;</label>
                                                    <div class="clearfix"></div>
                                                    <button data-toggle="tooltip" title="Clear" onClick="clearpovendor()" data-placement="top" class="btn btn-primary"><i class="fa fa-times" style="font-size: 11px;"></i></button>
                                                </div>
                                                <div class="col-md-2 padding_sm">
                                                    <div class="custom-float-label-control">
                                                        <label class="custom_floatlabel">Manufacture Name</label>
                                                        <div class="clearfix"></div>
                                                        <div class="ht5"></div>
                                                        <input type="text" name="manufacture_search" autocomplete="off" class="form-control custom_floatinput" id="manufacture_search"  placeholder="Manufacture Name Search">
                                                        <div id="manufacture_searchCodeAjaxDiv" style="margin: 0; z-index: 999; font-size: 12px; width: 100%" class="ajaxSearchBox"></div>
                                                        <input type="hidden" name="manufacture_search_hidden" id="manufacture_search_hidden" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 padding_sm item_category_div">
                                                    <div class="custom-float-label-control">
                                                        <label class="custom_floatlabel">Search On Item Category</label>
                                                        <div class="clearfix"></div>
                                                        <div class="ht5"></div>
                                                <?php
                                                $listdept = [];/* \DB::table('product as item')->distinct('category.name')
                                                        ->select('category.name', 'item.category_code')
                                                        ->join('category', 'category.category_code', '=', 'item.category_code')
                                                        ->join('item_stock', 'item_stock.item_code', '=', 'item.item_code')
                                                        ->where('category.status', 1)
                                                        ->pluck ('category.name', 'item.category_code');*/
                                                ?>
                                                        {!! Form::select('from_category1',$listdept,'',['class' => 'form-control custom_floatinput','onchange'=>'get_subcategory1(this)','title' => 'Category','id' => 'from_category1','style' => 'width:100%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 padding_sm item_category_div">
                                                    <div class="custom-float-label-control" id='getsubcategory_code_div_main1' style="display:none">
                                                        <label class="custom_floatlabel">Search On Item Group</label>
                                                        <div class="clearfix"></div>
                                                        <div id="getsubcategory_code_div1" class="ht5" style="padding-top: 5px;"></div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <!-- <div class="h10"></div> -->
                                <div id="gridContent">
                                    <div class="row" >
                                        <div class="col-md-12">
                                            <div id="poplast_item_box_div_req" class="poplast_item_box" style="display: none;">

                            </div>
                                        <div class="theadscroll always-visible" id="scrollDownLoad" style="height: auto; max-height: 500px; position: relative;padding-right: 15px;">

                                                <table class="table table-striped table-bordered table_sm table_condensed theadfix_wrapper">
                                                    <thead>
                                                        <tr class="headergroupbg">

                                                            <th width="5%" class="text-center"><input type="checkbox" name="check_all" id="check_all" onclick="CheckAllCheckbox();" title="Select All">&nbsp;<i class="fa fa-check-circle text-green"></i></th>
                                                            <th style="width: 8%">Request No</th>
                                                            <th style="width: 10%">Approved Date</th>
                                                            <th style="width: 10%">Approved By</th>
                                                            <th style="width: 5%">Item Unit</th>
                                                            <th style="width: 5%">Item Code</th>
                                                            <th style="width: 20%">Item Desc</th>
                                                            <th style="width: 5%">Quantity</th>
                                                            <th style="width: 8%">Comments</th>
                                                            <th style="width: 6%">Stock</th>
                                                            <th style="width: 15%">Supplier(Last.Purch)</th>
                                                            <th style="width: 3%">Is <br>Urgent</th>
                                                            <th style="width: 7%"><button style="padding: 0 4px; visibility: hidden;" class="btn btn-danger" <i style="font-size: 13px;" class="fa fa-trash"></i></button></th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="tbody_poitemdata">

                                                    </tbody>

                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
        </div>
    </div>
</div>

             {{-- include('core::dashboard.partials.bootstrap_datetimepicker_js') --}}
             {!! Html::script('packages/extensionsvalley/purchase/default/javascript/grn_calculation.js') !!}
            {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
            {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

            {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
            {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
             <script type="text/javascript">
                $(document).ready(function() {

                    setTimeout(function() {
                        // $('#menu_toggle').trigger('click');
                        // $(body).addClass('sidebar-collapse');
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 300);

                   $(document).on('click', '.grn_drop_btn', function(e) {
                    e.stopPropagation();
                    $(".grn_btn_dropdown").hide();
                    $(this).next().slideDown('');
                });
                   $(document).on('click', '.btn_group_box', function(e) {
                    e.stopPropagation();
                });

                   $(document).on('click', function() {
                    $(".grn_btn_dropdown").hide();
                });

                   $(".select_button li").click(function() {
                    $(this).toggleClass('active');
                });


                   $(document).on('click', '.notes_sec_list ul li', function() {
                    var disset = $(this).attr("id");
                    $('.notes_sec_list ul li').removeClass("active");
                    $(this).addClass("active");
                    $(this).closest('.notes_box').find(".note_content").css("display", "none");
                    $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
                });



                   $('.month_picker').datetimepicker({
                    format: 'MM'
                });
                   $('.year_picker').datetimepicker({
                    format: 'YYYY'
                });


                   var $table = $('table.theadfix_wrapper');

                   $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }

                });

                   $('.datepicker').datetimepicker({
                    format: 'DD-MMM-YYYY'
                });
                   $('.date_time_picker').datetimepicker();


                   $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });


    //       $('.fixed_header').floatThead({
    //     position: 'absolute',
    //     scrollContainer: true
    // });






    $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
        var $table = $('table.theadfix_wrapper');
        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }
        });
    })



});
</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function() {
    });

    function purchaseOrderList() {
    if ($('#po_added').val() != '') {
        $('#po_added').val('');
    }
    if (item_list_final.length)
    {
        for (var key in  item_list_final)
        {
            //  alert(item_list_final[key]);
            item_manage(item_list_final[key], null, 'po_req');

        }

    }
    purchaseOrderNewList();
    item_list_final = [];
}
function purchaseOrderNewList() {
    var file_token = $('#hidden_filetoken').val();
    var param = {_token: file_token, terms_condition_load: 1};
    $.ajax({
        type: "POST",
        url: '',
        data: param,
        beforeSend: function () {
            $('#generate_polistspin').removeClass('fa fa-forward');
            $('#generate_polistspin').addClass('fa fa-spinner fa-spin');
        },
        success: function (data) {
            if (data) {
                if (data.json_return != '') {
                    var jsonArray = JSON.parse(data.json_return);
                    $.each(jsonArray, function (index, jsonObject) {
                        var terms_id = parseInt(jsonObject.id);
                        var position = purchase_order_condition_array.indexOf(terms_id);
                        if (position) {
                            purchase_order_condition_array.push(terms_id);
                        }
                    });
                }
                /******** Access Permissions start ********/
                if (data.accessPerm.add != 1)
                    $("#genarate_po #po_post_btn_id").hide();
                /******** Access Permissions end ********/
            }
            po_add_status = 1;
            $('.req_listcheck').attr('onclick', 'return false');
            $('#genarate_poli').show();

            $('.nav-tabs a[href="#genarate_po"]').tab('show');
            $("#deprt_list").val($("#search_from_department").val());
            $("#vendor_list_po2").attr('title',$('#vendor_list_po').attr('title'));
            if($('#vendor_list_po_hidden').val()!='' && $('#vendor_list_po_hidden').val() != 'undefined' && $('#vendor_list_po_hidden').val() != null ){
                $("#vendor_list_po2").val($('#vendor_list_po').val());
            }
            $("#vendor_list_po_hidden2").val($('#vendor_list_po_hidden').val());
            //$("button[data-id='vendor_list']").find('span:first').html($("#vendor_list option:selected").text());


//$("#vendor_list").val('472');$("#vendor_list").multiselect("refresh");
        },
        complete: function () {
            $('#generate_polistspin').removeClass('fa fa-spinner fa-spin');
            $('#generate_polistspin').addClass('fa fa-forward');
        }
    });
}
</script>

@endsection
