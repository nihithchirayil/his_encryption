<?php
$total_cgst = 0.0;
$total_sgst = 0.0;
$total_igst = 0.0;
$total_cess = 0.0;
$total_discount = 0.0;
$total_othercharges = 0.0;
$total_freightcharges = 0.0;
$total_amt = 0.0;
$net_total = 0.0;
$tot_tax = 0.0;
$oth_charges = 0.0;
$cgst = 0.0;
$sgst = 0.0;
$igst = 0.0;
?>
<html>

<head>
    <style>
        /*  @page { margin: 5px;  }
    body { margin: 5px;  }*/
        .total_table {
            width: 100%;
            border: none !important;
            font-size: 12px;
        }

        .total_table td {
            border: none;
        }

        .total_table td:last-child {
            width: 10%;
        }

        table tr,
        table td {
            page-break-inside: avoid !important;
            page-break-before: always !important;
            page-break-after: always !important;
        }

        .classtable tr {
            page-break-before: always !important;
            page-break-inside: avoid !important;
        }

        /* @page {
            size: landscape;
            position: absolute;
            margin-top: 80px;
            margin-bottom: 80px;
            margin-right: 20px;
            margin-left: 20px;
            overflow: auto;
        } */

        .table.no-border,
        .table.no-border td,
        .table.no-border th {
            border: 0;
        }

        .table_sm th,
        .table_sm td {
            font-size: 12px !important;
            padding: 1px 5px !important;
        }

        .table_sm td span {
            line-height: 18px !important;
        }

        .table_lg th,
        .table_lg td {
            padding: 10px 5px !important;
            font-size: 12px;
        }

        .table_head.tth {
            font-size: 12px !important;
            padding: 1px !important;
        }

        .table-bordered>thead>tr>th,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>tbody>tr>td,
        .table-bordered>tfoot>tr>td {
            border: 1px solid #CCC !important;
        }

        .table-col-bordered>thead>tr>th,
        .table-col-bordered>tbody>tr>th,
        .table-col-bordered>tfoot>tr>th,
        .table-col-bordered>thead>tr>td,
        .table-col-bordered>tbody>tr>td,
        .table-col-bordered>tfoot>tr>td {
            border-right: 1px solid #CCC !important;
            border-top: none;
        }

        .headerclass {
            text-align: left !important;
            background-color: #000 !important;
            color: black !important;
            font-size: 12.5px !important
        }

        th {
            background-color: #b7b9b8 !important;
        }

        .common_td_rules {
            text-align: left !important;
            overflow: hidden !important;
        }

        .td_common_numeric_rules {
            border-left: solid 1px #bbd2bd !important;
            text-align: right !important;
        }

        .no-margin {
            margin: 0;
        }

        .no-padding {
            padding: 0;
        }

        .small-space {
            font-size: 1px;
        }

        .bordered {
            border: 1px solid black;
        }

        .no-space {
            line-height: 2;
        }

        .no-page-break {
            page-break-inside: avoid;
        }
    </style>

</head>

<body>
    <div class="row no-page-break bordered">
        <div class="col-md-12 padding_sm">
            <table width="100%" class="table no-margin table_sm table-striped no-border styled-table"
                style="border: 1px solid #CCC;">
                <thead>

                    <tr>
                        <th style="text-align: center" colspan="12">
                            <?= @$hospital_header[0]->hospital_header ? $hospital_header[0]->hospital_header : '' ?>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="text-align: center" colspan="12">
                            Purchase Order
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules" colspan="6" width="60%">
                            <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                        </th>
                        <th class="common_td_rules" colspan="6" width="40%">PO No :
                            <?= @$po_number ? $po_number : '' ?>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules" colspan="6" width="60%">Address :
                            <?= @$vendor_data['vendor_address'] ? $vendor_data['vendor_address'] : '' ?>
                        </th>
                        <th class="common_td_rules" colspan="6" width="40%">P.O.DATE :
                            <?= @$po_date ? $po_date : '' ?>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules" colspan="6" width="60%">Email :
                            <?= @$vendor_data['vendor_email'] ? $vendor_data['vendor_email'] : '' ?>
                        </th>
                        <th class="common_td_rules" colspan="6" width="40%">PO Status :
                            <?= @$po_status ? $po_status : '' ?>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules" colspan="6" width="60%">Telephone No. :
                            <?= @$vendor_data['contact_no'] ? $vendor_data['contact_no'] : '' ?>
                        </th>
                        <th class="common_td_rules" colspan="6" width="40%">Location :
                            <?= @$location_name ? $location_name : '' ?>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="6%">SL.No.</th>
                        <th width="30%"><b>Item Name</b></th>
                        <th width="6%"><b>Qty</b></th>
                        <th width="6%"><b>Free <br> Qty</b></th>
                        <th width="10%"><b>Unit</b></th>
                        <th width="6%"><b>Rate</b></th>
                        <th width="6%"><b>Disc.<br> (%)</b></th>
                        <th width="6%"><b>CGST<br> (%)</b></th>
                        <th width="6%"><b>SGST <br> (%)</b></th>
                        <th width="6%"><b>IGST<br> (%)</b></th>
                        <th width="6%"><b>Total Rate</b></th>
                        <th width="6%"><b>Net Rate</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                                        $i=1;
                                        if(count($item_string)!=0){
                                            foreach ($item_string as $each) {
                                                $string=$each['item_desc'];
                                                $total_cgst += floatval($each['cgst_amt']);
                                                $total_sgst += floatval($each['sgst_amt']);
                                                $total_igst += floatval($each['igst_amt']);
                                                $total_discount += floatval($each['discount_amt']);
                                                $total_othercharges += floatval($each['othercharge_amt']);
                                                $total_freightcharges += floatval($each['flightcharge_amt']);
                                                $total_amt += floatval($each['totalRate']);
                                                $net_total += floatval($each['netRate']);
                                                $cgst += floatval($each['cgst_amt']);
                                                $sgst += floatval($each['sgst_amt']);
                                                $igst += floatval($each['igst_amt']);
                                                ?>
                    <tr>
                        <td>
                            <?= $i ?>
                        </td>
                        <td class="common_td_rules">
                            <?= htmlspecialchars($string)  ?>

                        </td>
                        <td class="td_common_numeric_rules">
                            <?=$each['pur_qty'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?=$each['free_qty'] ?>
                        </td>
                        <td class="common_td_rules">
                            <?= @$uom_array[$each['item_id']] ? $uom_array[$each['item_id']] : '' ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?= $each['item_rate'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?= $each['discount_per'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?= $each['cgst_per'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?=$each['sgst_per'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?=$each['igst_per'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?=$each['totalRate'] ?>
                        </td>
                        <td class="td_common_numeric_rules">
                            <?=$each['netRate'] ?>
                        </td>
                    </tr>
                    <?php
                                       $i++;
                                        }
                                        $tot_tax = floatval($total_cgst) + floatval($total_sgst) + floatval($total_igst) + floatval($total_cess);
                                        $oth_charges = floatval($total_othercharges) + floatval($total_freightcharges);
                                        $net_total = floatval($net_total) - floatval($bill_discount)+floatval($round_off);
                                        }else {
                                            ?>
                    <tr>
                        <td colspan="12" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    <?php
                                        }
                                        ?>

                    <tr>
                        <th colspan="11" class="td_common_numeric_rules"> Gross Amount :</th>
                        <th class="td_common_numeric_rules">
                            <?=$total_amt ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">Item Discount(-) : </th>
                        <th class="td_common_numeric_rules">
                            <?=$total_discount ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">Bill Discount(-) : </th>
                        <th class="td_common_numeric_rules">
                            <?=$bill_discount ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">Freight/Other :</th>
                        <th class="td_common_numeric_rules">
                            <?=$oth_charges ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">CGST :</th>
                        <th class="td_common_numeric_rules">
                            <?=$cgst ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">SGST :</th>
                        <th class="td_common_numeric_rules">
                            <?= $sgst ?>
                        </th>

                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">IGST : </th>
                        <th class="td_common_numeric_rules">
                            <?= $igst ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">Total Tax :</th>
                        <th class="td_common_numeric_rules">
                            <?= $tot_tax ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">RoundOff :</th>
                        <th class="td_common_numeric_rules">
                            <?= $round_off ?>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="11" class="td_common_numeric_rules">Total Amount :</th>
                        <th class="td_common_numeric_rules">
                            <?= $net_total ?>
                        </th>
                    </tr>

                    <tr>
                        <th colspan="4" class="common_td_rules" width="40%">
                            GST NO.
                            <?= @$hospital_header[0]->gst_no ? $hospital_header[0]->gst_no : '' ?>
                        </th>
                        <th colspan="8" class="common_td_rules" width="60%">
                            Drug License No.
                            <?= @$drug_licence_no ? $drug_licence_no : '' ?>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4" class="common_td_rules" width="40%">
                            <b>Seller : </b>
                            <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                        </td>
                        <td colspan="8" class="common_td_rules" width="60%" colspan="2">
                            <b>Buyer : </b>
                            <?= @$hospital_header[0]->address ? $hospital_header[0]->address : '' ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            @if($full_remarks)
            <b>PO Terms And Conditions: </b>
            <?= @$full_remarks ? $full_remarks : '' ?>
            @endif
        </div>
    </div>
</body>

</html>



