<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>RTF Converter</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <div class="row padding_sm">
        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
            <thead>
                <tr class="table_header_bg">
                    <th colspan="11"><?= strtoupper($title) ?>
                    </th>
                </tr>
            </thead>
        </table>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
    </div>
</header>
<div class="right_col" role="main">
       
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm">
            <div class="x_panel">

                <div class="row codfox_container">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12 no-padding">
                            <div class="box no-border">
                                <div class="box-body clearfix">
                                    <form action="{{ route('extensionsvalley.purchase.grnList') }}"
                                        id="requestSearchForm" method="POST">
                                        {!! Form::token() !!}

                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </div>

</div>
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/rtfconverter.js') }}"></script>
</body>
</html>

