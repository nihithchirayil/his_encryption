@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <!-- page content -->


    <div class="modal fade" id="detailBillsModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="detailBillsModelheader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="row padding_sm">
                        <div class="col-md-12 padding_sm" id="detailBillsModelDiv"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col">
        <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        <input type="hidden" id="last_month_date" value="<?= date('M-d-Y', strtotime('-30 days')) ?>">
        <input type="hidden" id="current_date" value="<?= date('M-d-Y') ?>">
        <div class="row">
            <div class="col-md-2 padding_sm pull-right">{{ $title }}</div>
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-9 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 60px;">
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="from_date"
                                        value="<?= date('M-d-Y', strtotime('-30 days')) ?>">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="to_date"
                                        value="<?= date('M-d-Y') ?>">

                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="bill_no" name="bill_no"
                                        value="">
                                    <div id="bill_no_AjaxDiv" class="ajaxSearchBox"  style="margin-top:12px;z-index: 99999;"></div>
                                    <input type="hidden" name="bill_no_hidden" value="" id="bill_no_hidden">
                                    <input type="hidden" name="bill_id_hidden" value="" id="bill_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Patient Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                        name="patient_name" value="">
                                    <div id="patient_idAjaxDiv" class="ajaxSearchBox" style="margin-top:12px;z-index: 99999;"></div>
                                    <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                    <input type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-warning btn-block" onclick="resetfilter()"><i
                                        class="fa fa-recycle"></i>
                                    Reset</button>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-primary btn-block"
                                    onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 padding_sm" id="select_cashierdatadiv" style="display: none">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 60px;">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Cashier</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="cashier_name"
                                        name="cashier_name" value="">
                                    <div id="cashier_nameAjaxDiv" class="ajaxSearchBox" style="margin-top:12px;z-index: 99999;"></div>
                                    <input type="hidden" value="0" id="cashier_id_hidden">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm" style="margin-top: 20px;" id='pending_bil_list'>

                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/js/managerLevelRefund.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
    {!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
@endsection
