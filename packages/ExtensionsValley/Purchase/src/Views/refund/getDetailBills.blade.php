<div class="row padding_sm">
    <input type='hidden' name='payment_accounting_date' id='payment_accounting_date' value='{{ $payment_date }}'>
    <div class="theadscroll" style="position: relative; height: 450px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="3%">SL.No.</th>
                    <th style="text-align: center" width="5%">
                        <div class="checkbox checkbox-warning inline">
                            <input onclick="checkallBills()" class="checkallBills" id="checkallBills" type="checkbox">
                            <label class="text-blue" for="checkallBills">
                            </label>
                        </div>
                    </th>
                    <th class="common_td_rules" width="27%">Item Name</th>
                    <th class="common_td_rules" width="10%">Qty.</th>
                    <th class="common_td_rules" width="10%">Return Qty.</th>
                    <th class="common_td_rules" width="10%">Balance Qty.</th>
                    <th class="common_td_rules" width="10%">Net Amt.</th>
                    <th class="common_td_rules" width="10%">Return Amt.</th>
                    <th class="common_td_rules" width="10%">Balance Amt.</th>
                    <th style="text-align: center" width="5%">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                 $k=0;
        if (count($bill_detail_data) != 0) {
            $i=1;
            foreach ($bill_detail_data as $list) {
                $balance_qty=(float)$list->qty - (float)$list->return_qty;
                $balance_amt=(float)$list->net_amount - (float)$list->return_amount;
                if($balance_amt < 1){
                    $balance_amt=0;
                }
                $quantity = (float)$list->qty - (float)$list->return_qty;
                $status="<i class='red fa fa-times'></i>";
                $flag=0;
                $balance_class="";
                if ($quantity != 0 && $item_array[$list->id] == 1) {
                    $status="<i class='green fa fa-check'></i>";
                    $flag=1;
                    $balance_class="balance_amt";
                    $k++;
                }
                ?>
                <tr>
                    <td class="common_td_rules"><?= $i ?></td>
                    <td style="text-align: center">
                        <?php
                    if($flag==1){
                        ?>
                        <div class="checkbox checkbox-success inline">
                            <input onclick="checkDeatilBills(<?= $list->id ?>)" value="<?= $list->id ?>"
                                id="checkBillsDetails<?= $list->id ?>" class="billDetailId" type="checkbox">
                            <label class="text-blue" for="checkBillsDetails<?= $list->id ?>">
                            </label>
                        </div>
                        <?php
                    }else {
                        ?>
                        -
                        <?php
                    }
                    ?>

                    </td>
                    <td class="common_td_rules"><?= $list->item_desc ?></td>
                    <td class="common_td_rules"><?= $list->qty ?></td>
                    <td class="common_td_rules"><?= $list->return_qty ?></td>
                    <td class="common_td_rules"><?= $balance_qty ?></td>
                    <td class="common_td_rules"><?= number_format($list->net_amount, 2, '.', '') ?></td>
                    <td class="common_td_rules"><?= number_format($list->return_amount, 2, '.', '') ?></td>
                    <td class="td_common_numeric_rules <?= $balance_class ?>" id="balance_amt<?= $list->id ?>">
                        <?= number_format($balance_amt, 2, '.', '') ?>
                    </td>
                    <td style="text-align: center"><?= $status ?></td>

                </tr>
                <?php
            $i++;
            }
        }else{
            ?>
                <tr>
                    <td colspan="11" style="text-align: center">No Records Found</td>
                </tr>
                <?php
        }

        ?>

            </tbody>
        </table>
    </div>

        <div class="col-md-12 padding_sm" >
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 2px; box-shadow: 0px 0px #ccc;border: 1px solid #efebeb;min-height: 60px;">
                    <input type="hidden" value="<?= $k ?>" id="checkboxdata">
                    <div class="col-md-2 padding_sm pull-right" style="margin-top: 10px;">
                        <button id="refundBillDataBtn" class="btn btn-primary btn-block" type="button">Refund <i
                        id="refundBillDataSpin" class="fa fa-undo"></i></button>
                    </div>

                    <div class="col-md-2 padding_sm pull-right">
                        <div class="mate-input-box">
                            <label for="">Refund Time</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control bottom-border-text" name="custom_bill_refund_date_time" id="custom_bill_refund_date_time" value="" autocomplete="off" style="">                           
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm pull-right">
                        <div class="mate-input-box">
                            <label for="">Refund Date</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control bottom-border-text" name="custom_bill_refund_date" id="custom_bill_refund_date" value="" autocomplete="off" style="">                           
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm pull-right">
                        <div class="mate-input-box">
                            <label for="">Cashier</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="cashierSub_name"
                                name="cashierSub_name" value="">
                            <div id="cashierSub_nameAjaxDiv" class="ajaxSearchBox" style="margin-top:-230px;z-index: 99999;"></div>
                            <input type="hidden" value="0" id="cashierSub_id_hidden">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm pull-right blue" style="margin-top: 10px;">
                        <h4>Refund Amount : <span id="refund_amtspan">0</span></h4>
                    </div>
                </div>
            </div>
        </div>


    <div class="col-md-12 padding_sm">
        

    </div>
</div>
