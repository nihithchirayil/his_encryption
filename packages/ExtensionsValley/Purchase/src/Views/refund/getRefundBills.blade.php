<div class="theadscroll" style="position: relative; height: 550px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="common_td_rules" width="3%">SL.No.</th>
                <th class="common_td_rules" width="14%">Patient Name</th>
                <th class="common_td_rules" width="13%">UHID</th>
                <th class="common_td_rules" width="13%">Bill No.</th>
                <th class="common_td_rules" width="13%">Bill Tag</th>
                <th class="common_td_rules" width="13%">Return No.</th>
                <th class="common_td_rules" width="13%">Return Date</th>
                <th class="common_td_rules" width="6%">Payment Date</th>
                <th class="common_td_rules" width="7%">Net Amt.</th>
                <th class="common_td_rules" width="7%">Return Amt.</th>
                <th class="common_td_rules" width="7%">Balance Amt.</th>
                <th class="common_td_rules" width="13%">Cash Collected By</th>
                <th style="text-align: center" width="4%"><i class="fa fa-list"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php
        if (count($resultData) != 0) {
            $i=1;
            foreach ($resultData as $list) {
                $balance_amt=(float)$list->net_amount_wo_roundoff - (float)$list->return_amount;
                if($balance_amt < 1){
                    $balance_amt=0;
                }
                ?>
            <tr id="refundlist<?= $list->bill_id ?>">
                <td class="common_td_rules"><?= $i ?></td>
                <td class="common_td_rules"><?= $list->patient_name ?></td>
                <td class="common_td_rules"><?= $list->uhid ?></td>
                <td id="bill_nolist<?= $list->bill_id ?>" class="common_td_rules"><?= $list->bill_no ?></td>
                <td class="common_td_rules"><?= $list->bill_tag ?></td>
                <td class="common_td_rules"><?= @$list->return_no ? $list->return_no: '' ?></td>
                <td class="common_td_rules"><?= @$list->return_date ? $list->return_date: '' ?></td>
                <td class="common_td_rules"><?= $list->payment_accounting_date ?></td>
                <td class="td_common_numeric_rules"><?= number_format($list->net_amount_wo_roundoff, 2, '.', '') ?></td>
                <td class="td_common_numeric_rules"><?= number_format($list->return_amount, 2, '.', '') ?></td>
                <td class="td_common_numeric_rules"><?= number_format($balance_amt, 2, '.', '') ?></td>
                <td class="common_td_rules"><?= $list->cash_collected_by ?></td>
                <td style="text-align: center"><button onclick="getDetailBills(<?= $list->bill_id ?>, this)"
                        id="refundBillBtn<?= $list->bill_id ?>" class="btn btn-success" type="button"><i
                            id="refundBillSpin<?= $list->bill_id ?>" class="fa fa-list"></i></button>
                </td>
            </tr>
            <?php
            $i++;
            }
        }else{
            ?>
            <tr>
                <td colspan="11" style="text-align: center">No Records Found</td>
            </tr>
            <?php
        }

        ?>

        </tbody>
    </table>
</div>
