@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->

<div class="modal fade" id="item_charges_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <h4 class="modal-title" id="popup_itemdesc_header">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div id="item_chargesmodel_div"></div>
            </div>
            <div class="modal-footer">
                <label style="float: left;"><b style="color: #a71313;">Press Esc Key to Close the
                        Popop</b></label>
                <button class="btn btn-success" id='charges_pop_btn_id' onclick="calculatePopupCharges(1)"><i
                        id='charges_pop_spin_id' class="fa fa-save"></i> OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="purchase_historymodel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="purchase_historymodel_header"></h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;width: 100%">

                    <div class="clearfix panel-body"
                        style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">

                        <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                            <table
                                class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align "
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_light_purple_bg ">
                                        <th style="text-align: center;">Grn No</th>
                                        <th style="text-align: center;">Grn Date</th>
                                        <th style="text-align: center;">Vendor Name </th>
                                        <th style="text-align: center;">Batch </th>
                                        <th style="text-align: center;">Expiry </th>
                                        <th style="text-align: center;">Grn Qty </th>
                                        <th style="text-align: center;">Grn Unit</th>
                                        <th style="text-align: center;">Grn Rate</th>
                                        <th style="text-align: center;">Unit Cost</th>
                                        <th style="text-align: center;">Free Qty</th>
                                        <th style="text-align: center;">MRP</th>
                                    </tr>
                                </thead>
                                <tbody id="history_dl_list">

                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                            class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="print_po_model" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button onclick="reloadToList()" type="button" class="close close_white" data-dismiss="modal"
                    aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">GRN Preview</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div id="print_po_modelDiv"></div>

            </div>
            <div class="modal-footer">
                <button style="display: none" onclick="DownloadPdfSendMail(1)" style="padding: 3px 3px" type="button"
                    id="file_pdf_btn1" class="btn bg-purple pull-left">Send Email <i id="file_pdf_spin1"
                        class="fa fa-file"></i>
                </button>
                <a id="downloadPoPFDlinka" style="display: none" href="" download="">
                    <img id="downloadPoPFDlinkimg">
                </a>
                <button style="padding: 3px 3px" onclick="reloadToList()" type="button" data-dismiss="modal"
                    aria-label="Close" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button onclick="exceller_template_without_header('GRN');" style="padding: 3px 3px;display: none"
                    type="button" class="btn btn-warning">Excel
                    <i class="fa fa-file-excel-o"></i></button>
                <button onclick="DownloadPdfSendMail(0)" style="padding: 3px 3px;display: none" type="button"
                    id="file_pdf_btn0" class="btn btn-success">PDF <i id="file_pdf_spin0" class="fa fa-file"></i>
                </button>
                <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                    class="btn btn-primary">Print <i class="fa fa-print"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                        value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                        value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                        style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="po_list_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <h4 class="modal-title">Purchase Order List</h4>
            </div>
            <div class="modal-body" style="min-height: 500px;">
                <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-3 padding_sm">
                        <div class="date custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">PO NO. </label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control custom_floatinput"
                                id="po_number_search" placeholder="PO Number">
                        </div>
                    </div>

                    <div class="col-md-3 padding_sm">
                        <div style="padding-top: 3px;" class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">Location</label>
                            <div class="clearfix"></div>
                            {!! Form::select('po_department', $location, $default_location, [
                            'class' => 'form-control select2',
                            'id' => 'po_department',
                            'style' => 'color:#555555; padding:2px 12px;',
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-3 padding_sm">
                        <div style="padding-top: 3px;" class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">Vendor</label>
                            <div class="clearfix"></div>
                            <select onchange="changePoVendor()" name="po_vendor" id="po_vendor"
                                class="form-control select2">
                                <option value="">Select</option>
                                @foreach ($getPOVendors as $each)
                                <option attt-id="{{ $each->vendor_id }}" value="{{ $each->vendor_code }}">{{
                                    $each->vendor_name }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="col-md-1 padding_sm">
                        <div class="input-group date custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">From Date</label>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <input type="text" autocomplete="off" id="po_created_date_from" placeholder=""
                                class="form-control datepicker">
                        </div>
                    </div>

                    <div class="col-md-1 padding_sm">
                        <div class="input-group date custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">To Date</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" id="po_created_date_to" placeholder=""
                                class="form-control datepicker">
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top: 16px;">

                        <button style="padding: 0px 2px;" type="button" id="searchpobtn" onclick="serach_po();"
                            class="padding_sm btn btn-block btn-success"> <i id="posearchdataspin"
                                class="fa fa-search"></i>
                            Search </button>
                    </div>
                </div>
                <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                    <div class="theadscroll always-visible" style="position: relative; height: 330px;">
                        <table
                            class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align "
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_light_purple_bg " style="    background: #539a7a;color: white;">
                                    <th>#</th>
                                    <th>PO No </th>
                                    <th>PO Date</th>
                                    <th>Vendor</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="row_po_list">


                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" onclick="closePoPopup();"><i class="fa fa-times"></i>
                        Close</button>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="right_col" role="main">
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 40px;">
                    <div class="col-md-2 padding_sm">
                        <label id="" style="font-weight: 700;color: #ebad29;">GRN Status : <span id="grn_status_head"
                                style="font-weight: 700;color: #238823;">New GRN</span>
                        </label>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <label class="switch">
                            <input type="checkbox" name="poCheckBox" id='poCheckBox' value="1">
                            <span class="slider round"></span>
                        </label>
                        <label for="">GRN With PO</label>
                    </div>
                    <div title="Item Master" class="col-md-1 padding_sm"><a
                            href="{{ route('extensionsvalley.item.listItem') }}" target="_blank" id="item_master_id"
                            class="btn btn-block bg-blue"><i class="fa fa-list-alt"></i></a>
                    </div>
                    <div title="Supplier" class="col-md-1 padding_sm"><a
                            href="{{ route('extensionsvalley.item.listVendor') }}" target="_blank" id="item_vendor_id"
                            class="btn btn-block bg-green"><i class="fa fa-industry"></i></a>
                    </div>
                    <div title="Purchase History" class="col-md-1 padding_sm"><button class="btn btn-block bg-orange"
                            type="button" onclick="purchase_history()" id="grn_purchase_history"><i
                                class="fa fa-shopping-cart"></i> F2</button>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="pull-right"> <strong> {{ $title }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm" style="margin-top: 10px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                    <div class="col-md-12 padding_sm headerInputCheck">
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label>GRN No</label>
                                <label class="red" id="grn_no"
                                    style="font-weight:700;font-size: 12px;margin-top: 15px;margin-top: 20px;">-</label>
                            </div>
                        </div>
                        <div class="col-md-8 padding_sm">
                            <div class="mate-input-box">
                                <label>Supplier <span style="color: red">*</span></label>
                                <input style=position: relative;" type="text" autocomplete="off" required=""
                                    id="item_supplier" class="form-control popinput auto_focus_cls"
                                    placeholder="Search Supplier">
                                <div class='ajaxSearchBox' id="ajaxSearchBox_supplier" index='1'
                                    style="top: 10px !important;max-height: 290px;"> </div>
                                <input type='hidden' value="" id="vender_id">
                                <input type='hidden' value="" id="vender_code">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                                <label>Bill No <span style="color: red">*</span></label>
                                <input class="form-control" type="text" autocomplete="off" index='2' name="bill_no"
                                    id="bill_no">

                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label>Bill Date <span style="color: red">*</span></label>
                                <input class="form-control block_future_date" type="text" autocomplete="off" index='3'
                                    name="bill_date" id="bill_date">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 15px">
                            <div style="margin-top: 3px !important" class="checkbox checkbox-info inline no-margin">
                                <input onclick="checkIgstStatus()" title="Is IGST" type="checkbox" id="is_igstcheck"
                                    value="1">
                                <label for="is_igstcheck">IGST</label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm headerInputCheck">
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                                <label for=""><b>Remarks</b></label>
                                <div class="clearfix"></div>
                                <textarea name="" class="form-control" id="remark_txt" name="remark_txt"
                                    style="resize: none; height: 22px !important;"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm ">
                            <div class="mate-input-box">
                                <label style="margin-top: -5px;">Supply Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('location', $location, $default_location, [
                                'class' => 'form-control select2',
                                'id' => 'location',
                                ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                    <div class="col-md-12 padding_sm">
                        <div class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel ">Freight Charges</label>
                            <input class="form-control number_class" value="0.00" type="text"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onchange="getAllItemsTotals()" autocomplete="off" maxlength="8" id="freight_charges"
                                name="freight_charges">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 3px">
                        <div class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel ">Other Charges</label>
                            <input class="form-control number_class" value="0.00" type="text"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onchange="getAllItemsTotals()" autocomplete="off" maxlength="8" id="other_charges"
                                name="other_charges">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="padding-top: 2px !important;text-align: right">
                        <div class="custom-float-label-control " style="padding-left: 4px;">
                            <label class="red" id="tototherfreight_amt"
                                style="font-weight:700;font-size: 12px;margin-top: 15px">
                                0.0
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                    <div class="col-md-12 padding_sm">
                        <div class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel">Discount Type</label>
                            {!! Form::select('bill_discount_type', ['1' => 'Percentage', '2' => 'Amount'],
                            '', [
                            'class' => 'form-control',
                            'onchange' => 'getAllItemsTotals()',
                            'id' => 'bill_discount_type',
                            'style' => 'color:#555555; padding:4px 12px;',
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="custom-float-label-control mate-input-box">
                            <label class="custom_floatlabel ">Discount Val</label>
                            <input class="form-control number_class" value="0.00" type="text"
                                onchange="getAllItemsTotals()" autocomplete="off" maxlength="8" id="bill_discount_value"
                                name="bill_discount_value">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="padding-top: 2px !important;text-align: right">
                        <div class="custom-float-label-control " style="padding-left: 4px;">
                            <label class="red" id="tot_bill_discount_amt"
                                style="font-weight:700;font-size: 12px;margin-top: 15px">
                                0.0
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                    <table class="table no-margin table_sm no-border grn_total_table">
                        <tbody>
                            <tr>
                                <td width="35%"><label class="text-bold">Amount</label></td>
                                <td><input type="text" autocomplete="off" class="form-control text-right" readonly
                                        value="0.00" style="font-weight: 700;" readonly="" id="gross_amount_hd"
                                        name="gross_amount_hh"></td>
                            </tr>
                            <tr>
                                <td width="35%"><label class="text-bold">-Disc</label></td>
                                <td><input type="text" autocomplete="off" class="form-control text-right" readonly
                                        value="0.00" style="font-weight: 700;" id="discount_hd" name="discount_hd">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%"><label class="text-bold">+Tax</label></td>
                                <td><input type="text" autocomplete="off" class="form-control text-right" readonly
                                        value="0.00" style="font-weight: 700;" id="tot_tax_amt_hd"
                                        name="tot_tax_amt_hd"></td>
                            </tr>
                            <tr>
                                <td width="35%"><label class="text-bold">RoundOff</label></td>
                                <td><input type="text" autocomplete="off" value="0.00" class="form-control text-right"
                                        style="font-weight: 700;" onchange="getAllItemsTotals()" id="round_off"
                                        name="round_off">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%"><label class="text-bold"
                                        style=" margin: 3px 0 0 0; font-weight: 600 !important;">Total
                                        Amt.
                                    </label>
                                </td>
                                <td><input style="font-weight: 600 !important;" type="text" autocomplete="off"
                                        readonly="" class="form-control text-right " value="0.00" id="net_amount_hd"
                                        name="net_amount_hd">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                    <div class="col-md-12 padding_sm">
                        <a type="reset" href="{{ route('extensionsvalley.grn.addEditGeneralStoreGrn') }}">
                            <button id="cancel_btn"
                                class="btn light_purple_bg btn-block disable_on_save">Reload</button>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm savegrnDiv" id="savegrnDiv1">
                        <button type="button" id="grn_save_btn" onclick="grnSaveData(1,0,1);" name="save" value="save"
                            id="savegrnBtn1" class="btn btn-success btn-block saveForm disable_on_save">Save <i
                                id="savegrnSpin1" class="fa fa-save"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm savegrnDiv" id="savegrnDiv2">
                        <button type="button" id="grn_save_print_btn" onclick="grnSaveData(1,1,2);" id="savegrnBtn2"
                            value="save" class="btn btn-success btn-block disable_on_save">Save
                            &amp; Print <i id="savegrnSpin2" class="fa fa-save"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    @if (!empty($accessPerm['approve']) && $accessPerm['approve'] == 1)
                    <div class="col-md-12 padding_sm savegrnDiv" id="savegrnDiv3">
                        <button type="button" onclick="grnSaveData(2,0,3);" name="approve" value="approve"
                            id="savegrnBtn3" class="btn btn-success btn-block disable_on_save">Approve <i
                                id="savegrnSpin3" class="fa fa-save"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm savegrnDiv" id="savegrnDiv4">
                        <button type="button" onclick="grnSaveData(2,1,4);" name="approve" value="approve"
                            id="savegrnBtn4" class="btn btn-success btn-block disable_on_save">Approve &amp;
                            Print <i id="savegrnSpin4" class="fa fa-save"></i></button>
                    </div>
                    @endif
                    @if (!empty($accessPerm['submit']) && $accessPerm['submit'] == 1)
                    <div class="col-md-12 padding_sm savegrnDiv" id="savegrnDiv5">
                        <button type="button" onclick="grnSaveData(3,0,5);" id="savegrnBtn5" name="reject"
                            value="reject" class="btn btn-danger btn-block disable_on_save">
                            Cancel Bill <i id="savegrnSpin5" class="fa fa-save"></i></button>
                    </div>
                    @endif
                    @if (!empty($accessPerm['approve']) && $accessPerm['approve'] == 1)
                    <div class="col-md-12 padding_sm savegrnDiv" id="savegrnDiv6">
                        <button value="close_bill" type="button" id="savegrnBtn6" onclick="grnSaveData(4,0,6);"
                            class="btn btn-warning btn-block disable_on_save">
                            Close Bill <i id="savegrnSpin6" class="fa fa-save"></i></button>
                    </div>
                    @endif

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px;">
            <div class="theadscroll always-visible" style="position: relative; height: 400px;">
                <div class="medicine-list-div" style="display: none;">
                    <a style="float: left;" class="close_btn_med_search">X</a>
                    <div class=" presc_theadscroll" style="position: relative;">
                        <table id="MedicationTable"
                            class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                            <thead>
                                <tr class="light_purple_bg">
                                    <th>Medicine</th>
                                </tr>
                            </thead>
                            <tbody id="ListMedicineSearchData">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="grn_list_data_div">
                    <table
                        class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed"
                        id="main_row_tbl" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg ">
                                <th width="2%">#</th>
                                <th width="32%">
                                    <input id="issue_search_box" onkeyup="searchProducts();" type="text"
                                        autocomplete="off" placeholder="Item Search.. "
                                        style="color: #000;display: none;width: 90%;">
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                            class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Item Name</span>
                                </th>
                                <th width="8%">GRN Qty</th>
                                <th width="8%">Unit Rate</th>
                                <th width="8%">Sell. Price</th>
                                <th width="8%">Dis. Type</th>
                                <th width="8%">Dis. Amt</th>
                                <th width="8%">Tax Per.</th>
                                <th width="8%">Tot. Tax</th>
                                <th width="8%">Net Rate</th>
                                <th width="2%"><button type="button" id="addNewGrnRow" onclick="getNewRowInserted();"
                                        class="btn btn-success add_row_btn"><i id="addNewGrnRowSpin"
                                            class="fa fa-plus"></i></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="row_body_data">

                        </tbody>

                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
            <table class="table no-margin table_sm no-border table-condensed">
                <thead>
                    <tr class="table_header_bg ">
                        <th width="25%" style="border: 1px solid #CCC !important;">
                            Gross Total
                        </th>
                        <th width="25%" style="border: 1px solid #CCC !important;">
                            Tax Total
                        </th>
                        <th width="25%" style="border: 1px solid #CCC !important;">
                            Discount Total
                        </th>
                        <th width="25%" style="border: 1px solid #CCC !important;">
                            Total Net Amount
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="itemParentClass input_readonly" id="footer_tr">
                        <td style="border: 1px solid #CCC !important;" id="tot_gross_footer">
                            0.00
                        </td>
                        <td style="border: 1px solid #CCC !important;" id="tot_tax_footer">
                            0.00
                        </td>
                        <td style="border: 1px solid #CCC !important;" id="tot_dicount_footer">
                            0.00
                        </td>
                        <td style="border: 1px solid #CCC !important;" id="tot_net_amt_footer">
                            0.00
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="modal fade" id="po_list_detail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <h4 class="modal-title" id="po_list_detail_header">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 450px">
                <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                    <table
                        class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed"
                        style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_light_purple_bg ">
                                <th><input type="checkbox" id='po_select_all' onclick="checkAllPo();"></th>
                                <th>Item Code</th>
                                <th>Item Desc</th>
                                <th>Qty</th>
                                <th>Free Qty</th>
                                <th>Rate</th>
                                <th>Net Amount</th>
                            </tr>
                        </thead>
                        <tbody id="row_po_dl_list">

                        </tbody>
                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="addPolisttoGRNBtn" type="button" onclick="addPolisttoGRN()"
                    class="btn btn-primary">Add to GRN <i id="addPolisttoGRNSpin" class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
<input type="hidden" id="approve_status" value="{{ $approve_status }}">
<input type="hidden" id="partially_receive_status" value="{{ $partially_receive_status }}">
<input type="hidden" id="row_count_id" value="0">
<input type="hidden" id="grn_listrow_id" value="0">
<input type="hidden" id='hdn_item_history' value="" name="hdn_item_history">
<input type="hidden" id='grnBillId' value="{{ $grn_id }}">
<input type="hidden" id='po_id' value="{{ $grn_id }}">
<input type="hidden" id='po_id_viewd' value="0">
<input type="hidden" id='grnSavePosttype' value="0">
<input type="hidden" id="grn_delete_check" value="">
<input type="hidden" id="item_type_hidden" value="{{ $item_type }}">
<input type="hidden" id='current_date' value="{{ date('M-d-Y') }}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">
<input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/eyeFrameGRN.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/eyeFrameGRNCalculation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
