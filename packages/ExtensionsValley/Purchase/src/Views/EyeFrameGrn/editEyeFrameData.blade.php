@if(count($po_result)!=0)
@php
$row_count=1;
@endphp
@foreach ($po_result as $each)
<tr style="background: #FFF;" onclick="item_history({{ $row_count }})" class="row_class" id="row_data_{{ $row_count }}">
    <td class='row_count_class'>{{ $row_count }}</td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc{{ $row_count }}"
            onkeyup='searchItemCode(this.id,event,{{ $row_count }})' class="form-control popinput" name="item_desc"
            placeholder="Search Item" value="{{ $each->item_desc }}">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
        <input type='hidden' name='row_id_hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' name='item_code_hidden' value="{{ $each->item_code }}"
            id="item_code_hidden{{ $row_count }}">
        <input type='hidden' name='item_id_hidden' value="{{ $each->item_id }}" id="item_id_hidden{{ $row_count }}">
        <input type='hidden' name='po_head_id_hidden' value="{{ $each->po_head_id }}"
            id="po_head_id_hidden{{ $row_count }}">
        <input type='hidden' name='po_detail_id_hidden' value="{{ $each->po_detail_id }}"
            id="po_detail_id_hidden{{ $row_count }}">
        <input type='hidden' name='bill_detail_id_hidden' value="{{ $each->bill_detail_id }}"
            id="bill_detail_id_hidden{{ $row_count }}">
    </td>

    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='grn_qty'
            id="grn_qty{{ $row_count }}" type="text" value="{{ $each->grn_qty }}">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='unit_rate'
            id="unit_rate{{ $row_count }}" type="text" value="{{ $each->grn_rate }}">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='selling_rate'
            id="selling_rate{{ $row_count }}" type="text" value="{{ $each->sales_rate }}">
    </td>
    <td title="Discount Type">
        <select onchange="calculateListAmount({{ $row_count }},1)" name="discount_type"
            id="discount_type{{ $row_count }}" class="form-control">
            @foreach ($discount_type_array as $dis=>$type)
            @php
            $selected='';
            @endphp
            @if($dis==$each->discount_type)
            @php
            $selected='selected';
            @endphp
            @endif
            <option {{ $selected }} value="{{ $dis }}">{{ $type }}</option>
            @endforeach

        </select>
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_dis_amt{{ $row_count }}" name="tot_dis_amt" type="text" value="{{ $each->discount_value }}">
    </td>
    <td title="Tax Type">
        <select onchange="calculateListAmount({{ $row_count }},1)" name="tax_type" id="tax_type{{ $row_count }}"
            class="form-control">
            <option value="">Select</option>
            @foreach ($tax_head as $tax)
            @php
            $selected='';
            @endphp
            @if($tax->tax_name==$each->grn_tax_total_rate)
            @php
            $selected='selected';
            @endphp
            @endif
            <option {{ $selected }} attt-id="{{ $tax->tax_division }}" value="{{ $tax->tax_name }}">
                {{ $tax->tax_val }}</option>
            @endforeach
        </select>
    </td>
    <td><input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_tax_amt{{ $row_count }}" name="tot_tax_amt" type="text" value="0.0">
    </td>
    <td>
        <input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="net_rate{{ $row_count }}" name="net_cost" type="text" value="0.0">
    </td>
    <td>
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
@php
$row_count++;
@endphp
@endforeach
@endif
