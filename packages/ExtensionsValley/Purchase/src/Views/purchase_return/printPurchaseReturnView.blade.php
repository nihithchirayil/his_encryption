<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" style="width: 100%"
                        class="table no-margin table-sm table-striped no-border table-condensed styled-table">
                        <thead>
                            <tr class="table_header_bg ">
                                <th class="common_td_rules" width="8%">Si.No.</th>
                                <th class="common_td_rules" width="20%">Return No</th>
                                <th class="common_td_rules" width="10%">Created Date </th>
                                <th class="common_td_rules" width="24%">Location</th>
                                <th class="common_td_rules" width="28%">Vendor</th>
                                <th class="common_td_rules" width="10%">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($return_list) != 0)
                                @php $i=1;  @endphp
                                @foreach ($return_list as $list)
                                    @php $approve_status='-';  @endphp
                                    @if (intval($list->approve_status) == 0)
                                        @php $approve_status='Not Approved';  @endphp
                                    @elseif (intval($list->approve_status) == 1)
                                        @php $approve_status='Approved';  @endphp
                                    @elseif (intval($list->approve_status) == 2)
                                        @php $approve_status='Credit Approved';  @endphp
                                    @endif

                                    <tr>
                                        <td class="common_td_rules">{{ $i }}.</td>
                                        <td class="common_td_rules">{{ $list->purchase_return_no }}</td>
                                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->created_at)) }}
                                        </td>
                                        <td class="common_td_rules">{{ $list->location_name }}</td>
                                        <td class="common_td_rules">{{ $list->vendor_name }}</td>
                                        <td class="common_td_rules">{{ $approve_status }}</td>
                                    </tr>
                                    @php $i++;  @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td style="text-align: center" colspan="5">
                                        No Result Found
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
