<?php $i=1; ?>
@if(isset($vendorItemsList) && !empty($vendorItemsList))
<div class="theadscroll">
<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
    <thead>
    <tr class="table_header_bg ">
       <th width="3%">#</th>
       <th width="15%">Item Name</th>
       <th width="5%">Item Code</th>
       <th width="5%">Stock </th>
       <th width="10%">Vendor Name</th>
    </tr>
    </thead>
    <tbody >
@foreach($vendorItemsList as $data)
<tr style="cursor: pointer;">
    <td width="3%" style="text-align: left;">
        <input type="checkbox" class="chk_reorder_items" onclick="list_vendor_return_items(this,'{{$data->item_code}}','{{$data->item_desc}}');">
    </td>
    <td width="15%" style="text-align: left;" class="common_td_rules"> {{$data->item_desc}} </td>
    <td width="5%" style="text-align: left;" class="common_td_rules"> {{$data->item_code}} </td>
    <td width="5%" style="text-align: left;" class="common_td_rules"> {{$data->r_depstock}} </td>
    <td width="10%" style="text-align: left;" class="common_td_rules"> {{$data->vendor_name}} </td>
</tr> 
<?php $i++; ?>
@endforeach
</tbody>
</table>
    <div class="clearfix"></div>
    <div class="col-md-12 text-right"  style="padding-top:10px">
    <ul class="return_items_pages pagination purple_pagination" style="text-align:right !important;">
                {!! $paginator->render() !!}
    </ul>
</div>
</div>

@endif