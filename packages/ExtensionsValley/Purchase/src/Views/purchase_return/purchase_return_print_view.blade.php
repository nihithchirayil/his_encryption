<table cellspacing="0" cellpadding="0" width="100%" style=" font-size: 12px; ">
    <tbody>
      <tr style="border-bottom: none">
          <td style="padding: 0px; vertical-align: middle; border-bottom: none" width="15%" align="center" colspan="2">
          <h2>Purchase Return</h2>
          </td>
      </tr>

      <tr>

        <td width="75%" valign="top" style="padding: 20px 5px 5px 5px;">

            <br><br>
            <table  width="100%" style=" font-size: 13px;">

                <tr>
                  <td><strong>SL# </strong></td>
                  <td><strong>Return No </strong></td>
                  <td><strong>Date </strong></td>
                  <td><strong>Location </strong></td>
                  <td><strong>Vendor </strong></td>
                  <td><strong>Status </strong></td>

                </tr>

              <?php $i = 1;?>
              @if(sizeof($data)>0)
              @foreach($data as $res )

              <tr>
                <td> {{$i}} </td>
                <td> {{ $res->purchase_return_no}}</td>
                <td> {{ $res->created_date}}</td>
                <td>{{ $res->location_name}} </td>
                <td>{{ $res->vendor}} </td>
                <td>{{ $res->approve_status}}</td>
              </tr>

              <?php $i++;?>
              @endforeach
              @else
            <tr><td colspan="6">No Data found  </td></tr>
            @endif
            </table>
            <br>


            <br><br>
          </td>
      </tr>

    </tbody>
  </table>
