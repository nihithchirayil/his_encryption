@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" role="main">
    <div class="row codfox_container">

        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">

                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Return No</label>
                                <div class="clearfix"></div>
                                <input type="text" name="refference_no" autocomplete="off" value="" class="form-control"
                                    id="refference_no" placeholder="Return No">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Accounting Date</label>
                                <div class="clearfix"></div>
                                <input type="text" name="accounting_date" autocomplete="off"
                                    value="<?= date('d-M-Y') ?>" class="form-control" id="accounting_date">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <?php
                                $vendor = \DB::table('vendor')
                                    ->where('status', 1)
                                    ->orderBy('vendor_name')
                                    ->pluck('vendor_name', 'vendor_code');
                                ?>
                            <div class="mate-input-box">
                                <label for="">Supplier Name</label>
                                {!! Form::select('vendor', $vendor, '', [
                                'class' => 'form-control select2',
                                'placeholder' => 'Select',
                                'title' => 'Vendor',
                                'id' => 'vendor',
                                'style' => 'color:#555555;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <?php
                                $listdept = \ExtensionsValley\Purchase\CommonInvController::getLocationByroleToReturn();
                                $dflt_ret_store = \WebConf::getConfig('default_return_store');
                                $return_store_exist = \DB::table('config_detail')
                                    ->where('name', '=', 'no_return_store')
                                    ->pluck('value');
                                $is_return_store = @$return_store_exist[0] ? $return_store_exist[0] : 0;
                                if ($is_return_store == 1) {
                                    $listdept = \ExtensionsValley\Purchase\CommonInvController::getStoreByRole(0);
                                    $dflt_ret_store = \ExtensionsValley\Purchase\CommonInvController::defaultLocation();
                                }

                                ?>
                            <div class="mate-input-box">
                                <label for="">Return Store</label>
                                {!! Form::select('department', $listdept, $dflt_ret_store, [
                                'class' => 'form-control select2',
                                'placeholder' => 'Select Return Store',
                                'title' => 'Return Store',
                                'id' => 'department',
                                'style' => 'color:#555555;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Gate Pass No</label>
                                <div class="clearfix"></div>
                                <input type="text" name="gate_pass_no" autocomplete="off" value="" class="form-control"
                                    id="gate_pass_no" placeholder="Gate pass No">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Purchase Category</label>
                                <div class="clearfix"></div>
                                <?php $purchase_category = \DB::table('purchase_category')
                                        ->where('status', 1)
                                        ->orderby('name')
                                        ->pluck('name', 'id'); ?>
                                {!! Form::select('purchase_category', $purchase_category, '', [
                                'class' => 'form-control select2',
                                'placeholder' => 'Select',
                                'title' => 'Purchase Category',
                                'id' => 'purchase_category',
                                'style' => 'color:#555555;',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 10px">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio22" value="1" name="tax_category">
                                <label for="inlineRadio22"> IGST </label>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 10px">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio33" value="0" name="tax_category" checked="">
                                <label for="inlineRadio33"> CGST/SGST </label>
                            </div>
                        </div>

                        <div class="col-md-1 padding_sm pull-right" style="margin-top: -10px">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-primary" onclick="add_vendor_return_items();"><i
                                    class="fa fa-plus"></i> Return Items</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table
                            class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="5%">Item Name </th>
                                    <th width="2%">Item Code</th>
                                    <th width="1%">Batch</th>
                                    <th width="2%">Quantity</th>
                                    <th width="4%">Remarks</th>
                                    <th width="4%">Net Tax</th>
                                    <th width="4%">Net Amount</th>
                                    <th width="1%"><i class="fa fa-plus" onclick="add_purchase_return_row();"
                                            style="cursor: pointer"></i></th>
                                </tr>
                            </thead>
                            <tbody id="purchase_return_tbl_body">

                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
            <div class="box no-border" style="height: 100px;margin-top: 5px">
                <div class="box-body clearfix">
                    <div class="col-md-12 padding_sm" style="margin-top: 5px">
                        <?php
                if (isset($access_control[0]->approve) && $access_control[0]->approve == 0) {
                    $approve_style = 'disabled';
                } else {
                    $approve_style = '';
                }
                if (isset($access_control[0]->submit) && $access_control[0]->submit == 0) {
                    $submit_style = 'disabled';
                } else {
                    $submit_style = '';
                }
                ?>
                        <div class="col-md-2 padding_sm">
                            Total Items:<input type="text" name="net_total_items" id="net_total_items" value="0"
                                tabindex="-1" readonly="" class=""
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 50%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-2 padding_sm">
                            Total Tax:<input type="text" name="net_total_tax" id="net_total_tax" value="0" tabindex="-1"
                                readonly="" class=""
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 63%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-2 padding_sm">
                            Round-Off:
                            <input type="text" name="round_off" id="round_off" value="0" tabindex="-1"
                                onchange="updateRoundOff()"
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 63%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-2 padding_sm">
                            Net Amount:
                            <input type="text" name="net_amount" id="net_amount" value="0.00" tabindex="-1" readonly=""
                                class=""
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 64%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="save_all_btn_print" {{ $submit_style }} onclick="save_all_data(0, 0, 1)"
                                type="button" class="btn btn-primary btn-block save_all_btn_cls"><i class="fa fa-save"
                                    id="save_all_btn_savespin_p"></i>
                                Save & Print </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="save_all_btn" {{ $submit_style }} onclick="save_all_data(0, 0, 0)" type="button"
                                class="btn btn-success btn-block save_all_btn_cls"><i class="fa fa-save"
                                    id="save_all_btn_savespin"></i>
                                Save
                            </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="approve_all_btn_print" {{ $approve_style }} onclick="save_all_data(1, 0, 1)"
                                type="button" class="btn btn-primary btn-block approve_all_btn_p"><i class="fa fa-save"
                                    id="approve_all_btn_approvespin_p"></i> Approve & Print </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="approve_all_btn" {{ $approve_style }} onclick="save_all_data(1, 0, 0)"
                                type="button" class="btn btn-success btn-block approve_all_btn"><i
                                    class="fa fa-thumbs-up" id="approve_all_btn_approvespin"></i> Approve </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="item_return_batch_model" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 95%;margin-left: 72px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> <span id="modal_item_code"> </span></h4>
                </div>
                <div class="modal-body" style="padding: 7px 15px;" id="itemts_batchwise_div">

                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="vendor_return_items_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">List of Return Items</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Item Name or Code</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="vendor_item_search"
                                        id="vendor_item_search">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <?php
                                    $vendor_list = \DB::table('vendor')
                                        ->where('status', 1)
                                        ->where('deleted_at', null)
                                        ->orderBy('vendor_name')
                                        ->pluck('vendor_name', 'vendor_code');
                                    ?>
                                <div class="mate-input-box">
                                    <label for="">Supplier Name</label>
                                    {!! Form::select('vendor_popup', $vendor_list, '', [
                                    'class' => 'form-control select2',
                                    'placeholder' => 'Select',
                                    'title' => 'Vendor',
                                    'onchange' => 'changeVendorPopup(this)',
                                    'id' => 'vendor_popup',
                                    'style' => 'color:#555555;',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Expiry Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="to_expiry_date" autocomplete="off" class="form-control"
                                        id="to_expiry_date">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <label for="" style="font-size: 12px; font-weight: 700;">Expired</label>
                                <input type="checkbox" name="chk_exp_items" id="chk_exp_items">
                            </div>
                            <div class="col-md-2 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="vendor_return_items(0);" class="btn btn-block light_purple_bg"><i
                                        class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="clear_vendor_returnSearch();" style="margin: 0 5px 0 0;"
                                    class="btn btn-block btn-warning">Clear</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix" style="position: relative; height: 350px;"
                                id="fill_vendor_return_items_list">


                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal -->
</div>
<input type="hidden" id="ret_base_url" value="{{ URL::to('/') }}">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
<input type="hidden" value="" id="pur_return_no">
<input type="hidden" value="{{ $approve_status }}" id="approve_status_check" name="approve_status_check">
<input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">

@stop
@section('javascript_extra')
<script
    src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_return.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

@endsection
