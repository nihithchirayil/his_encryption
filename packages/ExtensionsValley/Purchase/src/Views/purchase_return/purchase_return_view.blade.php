@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <div class="modal fade" id="print_po_modelList" data-keyboard="false" data-backdrop="static" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Print Purchase Return List</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div id="print_po_modelListDiv"></div>

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button onclick="exceller_template_without_header('GoodsDebitNoteList');" style="padding: 3px 3px"
                        type="button" class="btn btn-warning">Excel
                        <i class="fa fa-file-excel-o"></i></button>
                    <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                        class="btn btn-primary">Print <i class="fa fa-print"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="print_po_model" data-keyboard="false" data-backdrop="static" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Print Purchase Return Order</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div id="print_po_modelDiv"></div>

                </div>
                <div class="modal-footer">
                    <a id="downloadPoPFDlinka" style="display: none" href="" download="">
                        <img id="downloadPoPFDlinkimg">
                    </a>
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button onclick="DownloadPdfSendMail('envelope');" id="sendEmailBtnenvelope" style="padding: 3px 3px"
                        type="button" class="btn bg-purple">Send Email
                        <i id="sendEmailSpinenvelope" class="fa fa-envelope"></i></button>
                    <button onclick="exceller_template_without_header('GoodsDebitNote');" style="padding: 3px 3px"
                        type="button" class="btn btn-warning">Excel
                        <i class="fa fa-file-excel-o"></i></button>
                    <button onclick="DownloadPdfSendMail('file')" style="padding: 3px 3px" type="button"
                        id="sendEmailBtnfile" class="btn btn-success">PDF <i id="sendEmailSpinfile" class="fa fa-file"></i>
                    </button>
                    <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                        class="btn btn-primary">Print <i class="fa fa-print"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" role="main">
        <div class="row" style="text-align: right; padding-right: 66px; font-size: 12px; font-weight: bold;"> Purchase
            return List
            <div class="clearfix"></div>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-11 padding_sm">
                            <div class="col-md-2 padding_sm" style="padding-bottom: 10px;">
                                <div class="mate-input-box">
                                    <div class=" custom-float-label-control">
                                        <label class="custom_floatlabel">Return No.</label>
                                        <input type="text" name="purchase_return_no" autocomplete="off" autofocus
                                            value="" class="form-control" id="purchase_return_no"
                                            placeholder="Return No">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="from_date" autocomplete="off" value=""
                                        class="form-control datepicker" id="from_date" placeholder="From Date">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="to_date" autocomplete="off" value=""
                                        class="form-control datepicker" id="to_date" placeholder="To Date">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <?php
                                $listdept = \ExtensionsValley\Purchase\CommonInvController::getLocationByroleToReturn();
                                $dflt_ret_store = \WebConf::getConfig('default_return_store');
                                $return_store_exist = \DB::table('config_detail')
                                    ->where('name', '=', 'no_return_store')
                                    ->pluck('value');
                                $is_return_store = @$return_store_exist[0] ? $return_store_exist[0] : 0;
                                if ($is_return_store == 1) {
                                    $listdept = \ExtensionsValley\Purchase\CommonInvController::getStoreByRole(0);
                                    $dflt_ret_store = \ExtensionsValley\Purchase\CommonInvController::defaultLocation();
                                }
                                ?>
                                <div class="mate-input-box">
                                    <label for="">Return Store</label>
                                    {!! Form::select('return_store', $listdept, $dflt_ret_store, [
                                        'class' => 'form-control select2',
                                        'placeholder' => 'Return Store',
                                        'title' => 'Return Store',
                                        'id' => 'return_store',
                                        'style' => 'color:#555555;',
                                    ]) !!}
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <?php
                                $vendor = \DB::table('vendor')
                                    ->where('status', 1)
                                    ->pluck('vendor_name', 'vendor_code');
                                ?>
                                <div class="mate-input-box">
                                    <label for="">Vendor</label>
                                    {!! Form::select('vendor', $vendor, 0, [
                                        'class' => 'form-control  select2',
                                        'placeholder' => 'Vendor',
                                        'title' => 'Vendor',
                                        'id' => 'vendor',
                                        'style' => 'color:#555555;',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Product Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="item_desc">
                                    <div id="item_desc_AjaxDiv" class="ajaxSearchBox"></div>
                                    <input type="hidden" name="item_desc_hidden" id="item_desc_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <?php
                                    $datalist = [
                                        '0' => 'Not Approved',
                                        '1' => 'Approved',
                                        '2' => 'Credit Approved',
                                    ];
                                    ?>
                                    {!! Form::select('statusdata', $datalist, 0, [
                                        'class' => 'form-control  select2',
                                        'placeholder' => 'Select',
                                        'title' => 'Select',
                                        'id' => 'statusdata',
                                        'style' => 'color:#555555;',
                                    ]) !!}
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm pull-right" style="margin-top:-10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block light_purple_bg" id="reqSearchBtn"
                                    onclick="reqSearch();"><i id="reqSearchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top:-10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-warning" onclick="clear_search();"><i
                                        class="fa fa-times"></i>
                                    Clear</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top:-10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block btn-primary" id="printlistBtn" onclick="printList(0);"><i
                                      id="printlistspin"  class="fa fa-print"></i> Print List</button>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm pull-right" style="margin-top:-10px">
                            <div class="col-md-12 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block btn-success" onclick="editReturnData(0);"><i
                                        class="fa fa-plus"></i> Add items</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 550px;"
                        id="purchaseRetunDataList">
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_return_list.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
