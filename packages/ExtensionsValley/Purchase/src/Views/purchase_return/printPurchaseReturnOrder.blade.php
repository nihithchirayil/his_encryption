<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <input type="hidden" id="return_id_printpreview" value="{{ $return_id }}">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" width="100%" border="0" cellspacing="0" cellpadding="0"
                        style="font-size: 12px; border-collapse: collapse;">
                        <tr>
                            <td align="left">
                                <table style="font-size: 12px; border-collapse: collapse;" width="100%" border="1"
                                    cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td colspan="6" align="center"><strong style="font-size: 16px;">Goods Debit
                                                Note</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong style="font-size: 14px;">Vendor Details</strong></td>
                                        <td colspan="5"><strong style="font-size: 14px;">
                                                <?php
                                        if (isset($vendor_dtls->approve_status) && $vendor_dtls->approve_status == 1) {
                                            echo 'Approved';
                                        } elseif (isset($vendor_dtls->approve_status) && $vendor_dtls->approve_status == 2) {
                                            echo 'Credit Approved';
                                        } else {
                                            echo 'Not Approved';
                                        }
                                        ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name : </strong></td>
                                        <td colspan="5">
                                            {{ $vendor_dtls->vendor_name ? $vendor_dtls->vendor_name : '-' }} </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address : </strong></td>
                                        <td colspan="5">{{ $vendor_dtls->address_1 ? $vendor_dtls->address_1 : '' }}
                                            {{ $vendor_dtls->address_2 ? ',' . $vendor_dtls->address_2 : '' }}
                                            {{ $vendor_dtls->address_3 ? ',' . $vendor_dtls->address_3 : '' }}
                                            {{ $vendor_dtls->city ? ',' . $vendor_dtls->city : '' }}
                                            {{ $vendor_dtls->state ? ',' . $vendor_dtls->state : '' }}
                                            {{ $vendor_dtls->country ? ',' . $vendor_dtls->country : '' }}
                                            {{ $vendor_dtls->pincode ? ',' . $vendor_dtls->pincode : '' }}
                                            {{ $vendor_dtls->contact_no ? ',' . $vendor_dtls->contact_no : '' }}
                                            {{ $vendor_dtls->email ? ',' . $vendor_dtls->email : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Debit Note # : </strong></td>
                                        <td><strong>{{ $vendor_dtls->purchase_return_no ?
                                                $vendor_dtls->purchase_return_no : '' }}</strong>
                                        </td>
                                        <td colspan="2"><strong>Approved Date</strong></td>
                                        <td colspan="2"><strong>Gate Pass No.:</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Vendor GSTIN :</strong></td>
                                        <td>{{ $vendor_dtls->gst_vendor_code }}</td>
                                        <td colspan="2">
                                            {{ $vendor_dtls->approved_date ? $vendor_dtls->approved_date : '' }}</td>
                                        <td colspan="2">
                                            {{ $vendor_dtls->gate_pass_number ? $vendor_dtls->gate_pass_number : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"><strong style="font-size: 14px;">Product-wise Details :
                                            </strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <?php $total_discount_amnt = $total_net_amt = $total_sales_amt = $total_sgst_amt = $total_cgst_amt = $total_igst_amt = $total_taxable_value = 0; ?>
                            <td align="left">
                                <table style="font-size: 12px; border-collapse: collapse;" width="100%" border="1"
                                    cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td rowspan="2"><strong>Sr. No.</strong></td>
                                        <td rowspan="2"><strong>Product Desciption</strong></td>
                                        <td rowspan="2" align="center"><strong>Batch/ Exp. Date</strong></td>
                                        <td rowspan="2" align="center"><strong>Qty</strong></td>
                                        <td rowspan="2" align="center"><strong>Unit</strong></td>
                                        <td rowspan="2" align="center"><strong> Unit Rate</strong></td>
                                        <td rowspan="2" align="center"><strong>Unit Calc. Cost </strong></td>
                                        <td rowspan="2" align="center"><strong>Total Disc Rs</strong></td>
                                        <td rowspan="2" align="center"><strong>Taxable Value</strong></td>
                                        <td rowspan="2" align="center"><strong>Against Invoice</strong></td>
                                        <td rowspan="2" align="center"><strong>Invoice Date</strong></td>
                                        <td rowspan="2" align="center"><strong>MRP</strong></td>
                                        <td colspan="2" align="center"><strong>CGST</strong></td>
                                        <td colspan="2" align="center"><strong>SGST</strong></td>
                                        <td colspan="2" align="center"><strong>IGST</strong></td>

                                        <td rowspan="2" align="center"><strong>Net Amount</strong></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><strong>Rate%</strong></td>
                                        <td align="center"><strong>Amt</strong></td>
                                        <td align="center"><strong>Rate%</strong></td>
                                        <td align="center"><strong>Amt</strong></td>
                                        <td align="center"><strong>Rate%</strong></td>
                                        <td align="center"><strong>Amt</strong></td>
                                    </tr>
                                    <?php
                                     $i = 1;
                                     $exp_date='';
                                            if (isset($print_dtls) && sizeof($print_dtls) > 0) {

                                                foreach ($print_dtls as $pd) {
                                                    if(isset($pd->pur_exp_date) && ($pd->pur_exp_date != '')){
                                                        $exp_date = $pd->pur_exp_date;
                                                    }else{
                                                        $exp_date = $pd->expiry_date;
                                                    }
                                                    $total_rate= ($pd->quantity)*($pd->actual_purchase_rate);
                                                    $taxable_value = $pd->total_cost - $pd->net_tax;
                                                    $tax_amount = ($pd->tax_rate * $taxable_value) / 100;
                                                    $net_rate= ($pd->quantity)*($pd->purchase_cost);
                                                    ?>
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $pd->item_desc }}</td>
                                        <td align="center">
                                            @if ($pd->print_batch != '')
                                            {{ $pd->print_batch }}
                                            @else
                                            {{ $pd->batch_no }}
                                            @endif
                                            / {{ $exp_date }}
                                        </td>
                                        <td align="center">{{ $pd->quantity }}</td>
                                        <td align="center">{{ $pd->unit_name }}</td>
                                        <td align="center">{{ $pd->actual_purchase_rate }}</td>
                                        <td align="center">{{ $pd->purchase_cost }}</td>
                                        <td align="center"> {{ $pd->discount_amt }}
                                            <?php $total_discount_amnt += $pd->discount_amt; ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                            echo $taxable_value;
                                            $total_taxable_value = $total_taxable_value + $taxable_value;
                                            ?>
                                        </td>
                                        <td align="center">{{ $pd->invoice_no }}</td>
                                        <td align="center">{{ date('j-M-Y', strtotime($pd->invoice_date)) }}</td>
                                        <td>{{ $pd->mrp }}</td>
                                        <td align="center">
                                            <?php
                                        if (isset($print_dtls[0]->is_igst) && $print_dtls[0]->is_igst == 0) {
                                            $cgst_rate = $pd->tax_rate / 2;
                                            echo $cgst_rate;
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                        if (isset($print_dtls[0]->is_igst) && $print_dtls[0]->is_igst == 0) {
                                            echo $tax_amount/2;
                                             $total_sgst_amt+=$tax_amount / 2;
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                        if (isset($print_dtls[0]->is_igst) && $print_dtls[0]->is_igst == 0) {
                                            $sgst_rate = $pd->tax_rate / 2;
                                            echo $sgst_rate;
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                        if (isset($print_dtls[0]->is_igst) && $print_dtls[0]->is_igst == 0) {
                                            echo $tax_amount / 2;
                                            $total_cgst_amt+=$tax_amount / 2;
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                        if (isset($print_dtls[0]->is_igst) && $print_dtls[0]->is_igst == 1) {
                                            echo $pd->tax_rate;
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                        if (isset($print_dtls[0]->is_igst) && $print_dtls[0]->is_igst == 1) {
                                            echo $pd->net_tax;
                                            $total_igst_amt += $pd->net_tax;
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                        echo  $net_rate;
                                        $total_net_amt+= $net_rate;
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                $i++;
                            }
                        }
                        ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center"><strong>Total</strong></td>
                                        <td align="center"><strong>{{ round($total_discount_amnt, 2) }}</strong></td>
                                        <td align="center"><strong>{{ round($total_taxable_value, 2) }}</strong></td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center"><strong>{{ round($total_cgst_amt, 2) }}</strong></td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center"><strong> {{ round($total_sgst_amt, 2) }}</strong></td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center"><strong> {{ round($total_igst_amt, 2) }}</strong></td>
                                        <td align="center"><strong>{{ round($total_net_amt, 2) }}</strong></td>
                                    </tr>
                                    <tr>
                                        @php
                                        $total_net_amt=floatval($total_net_amt)+floatval($pd->roundoff);
                                        @endphp
                                        <td colspan="15" rowspan="2" align="left" valign="middle">
                                            <strong>Amount in Words
                                                :
                                                <?php echo \ExtensionsValley\Purchase\CommonInvController::numberToWord($total_net_amt); ?>
                                            </strong>
                                        </td>
                                        <td colspan="3" align="center"><strong style="font-size: 14px;">Summary</strong>
                                        </td>
                                        <td colspan="2" align="center"><strong style="font-size: 14px;">Amount</strong>
                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>
                                    <tr>
                                        <td colspan="9" align="center"><strong>Remarks</strong></td>
                                        <td colspan="6" rowspan="3" align="center">&nbsp;</td>
                                        <td colspan="3" align="right"><strong>Total Discounts</strong></td>
                                        <td colspan="2" align="right">
                                            <?= $total_discount_amnt ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="3">&nbsp;</td>
                                        <td colspan="3" align="right">Total Taxable Value</td>
                                        <td colspan="2" align="right">{{ $total_taxable_value }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right">Total CGST</td>
                                        <td colspan="2" align="right">{{ $total_cgst_amt }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center"><strong>Authorized Signatory </strong></td>
                                        <td colspan="3" align="right">Total SGST</td>
                                        <td colspan="2" align="right">{{ $total_sgst_amt }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="15" rowspan="3" align="center" valign="middle">Note : DN
                                            would be adjusted against
                                            dues to your account </td>
                                        <td colspan="3" align="right">Total IGST</td>
                                        <td colspan="2" align="right">{{ $total_igst_amt }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><strong>Round-Off</strong></td>
                                        <td colspan="2" align="right">
                                            <strong>{{ round($pd->roundoff, 2) }}</strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" align="right"><strong>Grand Total</strong></td>
                                        <td colspan="2" align="right">
                                            <strong>{{ round($total_net_amt, 2) }}</strong>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table style="font-size: 12px; border-collapse: collapse;" width="100%" border="1"
                                    cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td width="15%"><strong>Approved By </strong></td>
                                        <td width="6%">:</td>
                                        <td width="28%">{{ ucfirst($print_dtls[0]->approved_by) }}</td>
                                        <td width="25%" align="right"><strong>Prepared By</strong></td>
                                        <td width="9%">:</td>
                                        <td width="17%">{{ ucfirst(\Auth::user()->name) }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Printed By</strong></td>
                                        <td>:</td>
                                        <td>{{ ucfirst(\Auth::user()->name) }}</td>
                                        <td align="right"><strong>Print Date Time</strong></td>
                                        <td>:</td>
                                        <td>
                                            <?= date('d-M-Y H:i:s') ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
