<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var purchase_return_no = $('#purchase_return_no').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var return_store = $('#return_store').val();
            var vendor_id = $('#vendor').val();
            var status = $('#statusdata').val();
            var param = {
                _token: token,
                purchase_return_no: purchase_return_no,
                from_date: from_date,
                to_date: to_date,
                vendor_id: vendor_id,
                return_store: return_store,
                from_type: 1,
                status: status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#purchaseRetunDataList").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    $('#reqSearchBtn').attr('disabled', true);
                    $('#reqSearchSpin').removeClass('fa fa-search');
                    $('#reqSearchSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#purchaseRetunDataList').html(data);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function() {
                    $("#purchaseRetunDataList").LoadingOverlay("hide");
                    $('#reqSearchBtn').attr('disabled', false);
                    $('#reqSearchSpin').removeClass('fa fa-spinner fa-spin');
                    $('#reqSearchSpin').addClass('fa fa-search');
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 450px;">
        <table class="table no-margin table-sm theadfix_wrapper table-striped no-border table-condensed styled-table">
            <thead>
                <tr class="table_header_bg ">
                    <th class="common_td_rules" width="20%">Return No</th>
                    <th class="common_td_rules" width="10%">Created Date </th>
                    <th class="common_td_rules" width="20%">Location</th>
                    <th class="common_td_rules" width="25%">Vendor</th>
                    <th class="common_td_rules" width="10%">Net Amount</th>
                    <th class="common_td_rules" width="10%">Status</th>
                    <th style="text-align: center" width="3%"><i class="fa fa-eye"></i></th>
                    <th style="text-align: center" width="3%"><i class="fa fa-edit"></i></th>
                </tr>
            </thead>
            <tbody>
                @if (count($return_list) != 0)
                @foreach ($return_list as $list)
                @php $approve_status='-'; @endphp
                @if (intval($list->approve_status) == 0)
                @php $approve_status='Not Approved'; @endphp
                @elseif (intval($list->approve_status) == 1)
                @php $approve_status='Approved'; @endphp
                @elseif (intval($list->approve_status) == 2)
                @php $approve_status='Credit Approved'; @endphp
                @endif

                <tr>
                    <td class="common_td_rules">{{ $list->purchase_return_no }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->created_at)) }}</td>
                    <td class="common_td_rules">{{ $list->location_name }}</td>
                    <td class="common_td_rules">{{ $list->vendor_name }}</td>
                    <td class="td_common_numeric_rules">{{ $list->net_amount }}</td>
                    <td class="common_td_rules">{{ $approve_status }}</td>
                    @if (intval($list->approve_status) == 0)
                    <td style="text-align: center">
                        --
                    </td>
                    @else
                    <td style="text-align: center">
                        <button class="btn btn-primary" style="padding: 0px 4px"
                            onclick="getPrintPreview({{ $list->return_id }})" id="sendEmailBtn{{ $list->return_id }}"
                            type="button"><i id="sendEmailSpin{{ $list->return_id }}" class="fa fa-eye"></i>
                        </button>
                    </td>
                    @endif
                    <td style="text-align: center"><button class="btn btn-warning" style="padding: 0px 4px"
                            onclick="editReturnData({{ $list->return_id }})" id="editreturnBtn{{ $list->return_id }}"
                            type="button"><i id="editreturnSpin{{ $list->return_id }}" class="fa fa-edit"></i></button>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td style="text-align: center" colspan="7">
                        No Result Found
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
