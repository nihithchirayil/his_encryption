@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" role="main">
    <div class="row codfox_container">

        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Return No</label>
                                <div class="clearfix"></div>
                                <input type="text" readonly="" name="refference_no" autocomplete="off"
                                    value="{{$purchase_head_result[0]->purchase_return_no ? $purchase_head_result[0]->purchase_return_no:''}}"
                                    class="form-control" id="refference_no" placeholder="Refference No">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Accnt. Dt.</label>
                                <div class="clearfix"></div>
                                <input type="text" readonly="" name="accounting_date" autocomplete="off"
                                    value="{{$purchase_head_result[0]->account_date?$purchase_head_result[0]->account_date:date('d-M-Y')}}"
                                    class="form-control" id="accounting_date">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <?php
                            $vendor = \DB::table('vendor')->where('status', 1)->orderBy('vendor_name')->pluck('vendor_name', 'vendor_code');
                            $vendor_code = $purchase_head_result[0]->vendor_code ? $purchase_head_result[0]->vendor_code : '';
                            ?>
                            <div class="mate-input-box">
                                <label for="">Supplier Name</label>
                                {!! Form::select('vendor',$vendor,$vendor_code,
                                ['class' => 'form-control select2','placeholder' => 'Vendor','title' => 'Vendor',
                                'id' => 'vendor','style' => 'color:#555555;']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <?php
                            $listdept = \ExtensionsValley\Purchase\CommonInvController::getLocationByroleToReturn();
                            $loc_code = $purchase_head_result[0]->location_code ? $purchase_head_result[0]->location_code : '';
                            $return_store_exist = \DB::table('config_detail')->where('name','=','no_return_store')->pluck('value');
                            $is_return_store = @$return_store_exist[0] ? $return_store_exist[0]:0;
                            if($is_return_store==1){
                            $listdept = \ExtensionsValley\Purchase\CommonInvController::getStoreByRole(0);
                            }
                            ?>
                            <div class="mate-input-box">
                                <label for="">Return Store</label>
                                {!! Form::select('department',$listdept,$loc_code,['class' => 'form-control
                                select2','placeholder' => 'Select Return Store','title' => 'Return Store','id' =>
                                'department','style' => 'color:#555555;']) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Gate Pass No</label>
                                <div class="clearfix"></div>
                                <input type="text" name="gate_pass_no" autocomplete="off"
                                    value="{{$purchase_head_result[0]->gate_pass_number}}" class="form-control"
                                    id="gate_pass_no" placeholder="Gate pass No">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Purchase Category</label>
                                <div class="clearfix"></div>
                                <?php $purchase_category = \DB::table('purchase_category')->where('status', 1)->orderby('name')->pluck('name', 'id'); ?>
                                {!!
                                Form::select('purchase_category',$purchase_category,$purchase_head_result[0]->purchase_category,['class'
                                => 'form-control select2','placeholder' => 'Select','title' => 'Purchase Category','id'
                                => 'purchase_category','style' => 'color:#555555;']) !!}
                            </div>
                        </div>
                        <?php
                        if ($purchase_head_result[0]->is_igst == 0) {
                            $cgst = "checked";
                            $igst = "";
                        } else {
                            $cgst = "";
                            $igst = "checked";
                        }
                        ?>
                        <div class="col-md-1 padding_sm">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio22" value="1" name="tax_category[]" <?php echo $igst
                                    ?> onchange="changeTaxCategory(this)">
                                <label for="inlineRadio22"> IGST </label>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio33" value="0" name="tax_category[]" <?=$cgst ?>
                                onchange="changeTaxCategory(this)">
                                <label for="inlineRadio33"> CGST/SGST </label>
                            </div>
                        </div>
                        @if($approve_status == 1 || $approve_status == 2)
                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Credit Bill Dt.</label>
                                <div class="clearfix"></div>
                                <?php
                                if(isset($purchase_head_result[0]->credit_bill_date) && $purchase_head_result[0]->credit_bill_date !=''){
                                    $date_clas= '';
                                }else{
                                    $date_clas= 'datepicker';
                                } ?>
                                <input type="text" name="credit_bill_date" autocomplete="off"
                                    value="{{$purchase_head_result[0]->credit_bill_date??''}}"
                                    class="form-control {{$date_clas}}" id="credit_bill_date">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Credit Bill No</label>
                                <div class="clearfix"></div>
                                <input type="text" name="credit_bill_no" autocomplete="off"
                                    value="{{$purchase_head_result[0]->credit_bill_no??''}}" class="form-control "
                                    id="credit_bill_no" placeholder="Credit Bill No">
                            </div>
                        </div>
                        @endif
                        <div class="col-md-1 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-primary" onclick="add_vendor_return_items();"><i
                                    class="fa fa-plus"></i> Return Items</button>
                        </div>
                        <div class="col-md-1 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <button class="btn btn-block light_purple_bg"
                                onclick="print_out('{{$purchase_return_head_id}}');"><i class="fa fa-print"></i>
                                Print</button>
                        </div>




                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table id="p_return_edit_tbl"
                            class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="5%">Item Name </th>
                                    <th width="2%">Item Code</th>
                                    <th width="1%">Batch</th>
                                    <th width="2%">Quantity</th>
                                    <th width="4%">Remarks</th>
                                    <th width="4%">Net Tax</th>
                                    <th width="4%">Net Amount</th>
                                    <th width="1%"><i class="fa fa-plus" style="cursor:pointer"
                                            onclick="add_purchase_return_row_edit();"></i></th>
                                </tr>
                            </thead>
                            <tbody id="purchase_return_tbl_body_edit">
                                <?php
                                $all_net_amt = 0;
                                $all_tax_amount = 0;
                                $all_qty = 0;
                                $approve_status = $approve_status;
                                if ($approve_status == 1 || $approve_status == 2) {
                                    $table_row_class = "class='avd_clas'";
                                    $disabled_btn_class = "disabled";
                                } else {
                                    $table_row_class = "";
                                    $disabled_btn_class = "";
                                }
                                if ($approve_status == 2) {
                                    $table_row_class = "class='avd_clas'";
                                    $credit_disabled_btn_class = "disabled";
                                } else {
                                    $table_row_class = "";
                                    $credit_disabled_btn_class = "";
                                }
                                if (sizeof($purchase_detail_result) > 0) {
                                    foreach ($purchase_detail_result as $dtls) {
                                        ?>
                                <tr item-code="{{$dtls->item_code}}" <?=$table_row_class ?>
                                    id="tr_{{$dtls->item_code}}">
                                    <td class="common_td_rules">
                                        <input type="text" name="item_desc[]" value="{{$dtls->item_desc}}"
                                            autocomplete="off" readonly="" class="form-control return_item_desc">
                                        <div class="ajaxSearchBox search_return_item_box" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                                     border: 1px solid rgba(0, 0, 0, 0.3);"></div>
                                    </td>
                                    <td class="common_td_rules">
                                        <input type="text" value="{{$dtls->item_code}}" name="item_code[]" readonly=""
                                            class="form-control">
                                    </td>
                                    <td>
                                        <i class="fa fa-info batch_wise_info_class" style="cursor:pointer;"
                                            onclick="select_batch_wise_item_edit(this)"></i>
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        <input type="text" style="text-align: right !important"
                                            value="{{$dtls->total_qty}}" name="quantity[]" autocomplete="off"
                                            readonly="" class="form-control" id="item_code_qty_{{$dtls->item_code}}">
                                    </td>
                                    <td class="common_td_rules">
                                        <input type="text" value="{{$dtls->remarks}}" name="remarks[]" readonly=""
                                            class="form-control">
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        <input type="text" style="text-align: right !important"
                                            value="{{round($dtls->tax_amount,2)}}" name="net_tax[]" readonly=""
                                            readonly="" id="item_code_net_tax_{{$dtls->item_code}}"
                                            class="form-control item_code_net_tax">
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        <input type="text" style="text-align: right !important"
                                            value="{{round($dtls->net_amount,2)}}" name="net_amount[]" readonly=""
                                            readonly="" id="item_code_net_amount_{{$dtls->item_code}}"
                                            class="form-control item_code_net_amount">
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"
                                            onclick="delete_return_entry('{{$purchase_return_head_id}}','{{$dtls->item_code}}','{{$dtls->tax_amount}}','{{$dtls->net_amount}}')"></i>
                                    </td>
                                </tr>
                                <?php
                                        $all_net_amt += $dtls->net_amount;
                                        $all_tax_amount += $dtls->tax_amount;
                                        $all_qty += $dtls->total_qty;
                                    }
                                }
                                ?>
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
            <div class="box no-border" style="height: 100px;margin-top: 5px">
                <div class="box-body clearfix">
                    <div class="col-md-12 padding_sm" style="margin-top: 5px">
                        <?php
                if (isset($access_control[0]->approve) && $access_control[0]->approve == 0) {
                    $approve_style = 'disabled';
                } else {
                    $approve_style = '';
                }
                if (isset($access_control[0]->submit) && $access_control[0]->submit == 0) {
                    $submit_style = 'disabled';
                } else {
                    $submit_style = '';
                }
                ?>
                        <div class="col-md-2 padding_sm">
                            Total Items:<input type="text" name="net_total_items" id="net_total_items" value="0"
                                tabindex="-1" readonly="" class=""
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 50%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-2 padding_sm">
                            Total Tax:<input type="text" name="net_total_tax" id="net_total_tax"
                                value="{{$all_tax_amount}}" tabindex="-1" readonly="" class=""
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 63%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-2 padding_sm">
                            Round-Off:
                            <input type="text" value="{{ $purchase_head_result[0]->roundoff ? $purchase_head_result[0]->roundoff:0 }}" name="round_off" id="round_off" value="0" tabindex="-1"
                                onchange="updateRoundOff()"
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 63%; font-size: 15px !important;">
                        </div>
                        <div class="col-md-2 padding_sm">
                            Net Amount:
                            <input type="text" name="net_amount" id="net_amount" value="{{$all_net_amt}}" tabindex="-1"
                                readonly="" class=""
                                style="border: 0px;color: red;background-color: #fff;text-align: left;width: 64%; font-size: 15px !important;">
                        </div>
                        @if($approve_status == 1 || $approve_status == 2)
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="credit_approve_all_btn_print" {{$approve_style}}
                                onclick="save_all_data(2, 1, 1)" type="button"
                                class="btn btn-primary credit_approve_all_btn" <?=$credit_disabled_btn_class ?> ><i
                                    class="fa fa-save" id="credit_approve_all_btn_approvespin_p"></i> Credit App. &
                                Print </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="credit_approve_all_btn" {{$approve_style}} onclick="save_all_data(2, 1, 0)"
                                type="button" class="btn btn-success credit_approve_all_btn "
                                <?=$credit_disabled_btn_class ?> ><i class="fa fa-thumbs-up"
                                    id="credit_approve_all_btn_approvespin"></i> Credit Approve
                            </button>
                        </div>
                        @else
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="save_all_btn_print" {{ $submit_style }} onclick="save_all_data(0, 1, 1)"
                                type="button" class="btn btn-primary btn-block save_all_btn_cls"><i class="fa fa-save"
                                    id="save_all_btn_savespin_p"></i>
                                Save & Print </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="save_all_btn" {{ $submit_style }} onclick="save_all_data(0, 1, 0)" type="button"
                                class="btn btn-success btn-block save_all_btn_cls"><i class="fa fa-save"
                                    id="save_all_btn_savespin"></i>
                                Save
                            </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="approve_all_btn_print" {{ $approve_style }} onclick="save_all_data(1, 1, 1)"
                                type="button" class="btn btn-primary btn-block approve_all_btn_p"><i class="fa fa-save"
                                    id="approve_all_btn_approvespin_p"></i> Approve & Print </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button id="approve_all_btn" {{ $approve_style }} onclick="save_all_data(1, 1, 0)"
                                type="button" class="btn btn-success btn-block approve_all_btn"><i
                                    class="fa fa-thumbs-up" id="approve_all_btn_approvespin"></i> Approve </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="item_return_batch_model_edit" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 95%;margin-left: 72px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span id="modal_item_code"> </span></h4>
                </div>
                <div class="modal-body" style="padding: 7px 15px;" id="itemts_batchwise_div_edit">

                </div>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="vendor_return_items_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">List of Return Items</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Item Name or Code</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="vendor_item_search"
                                        id="vendor_item_search">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <?php
                            $vendor_list = \DB::table('vendor')->where('status', 1)->where('deleted_at',NULL)->orderBy('vendor_name')->pluck('vendor_name', 'vendor_code');
                            ?>
                                <div class="mate-input-box">
                                    <label for="">Supplier Name</label>
                                    {!! Form::select('vendor_popup',$vendor_list,'',
                                    ['class' => 'form-control select2','placeholder' => 'Select','title' =>
                                    'Vendor','onchange' => 'changeVendorPopup(this)',
                                    'id' => 'vendor_popup','style' => 'color:#555555;']) !!}
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Expiry Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="to_expiry_date" autocomplete="off" class="form-control"
                                        id="to_expiry_date">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <label for="" style="font-size: 12px; font-weight: 700;">Expired</label>
                                <input type="checkbox" name="chk_exp_items" id="chk_exp_items">
                            </div>
                            <div class="col-md-2 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="vendor_return_items(0);" class="btn btn-block light_purple_bg"><i
                                        class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="clear_vendor_returnSearch();" style="margin: 0 5px 0 0;"
                                    class="btn btn-block btn-warning">Clear</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix" style="position: relative; height: 350px;"
                                id="fill_vendor_return_items_list">


                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal -->
</div>
<input type="hidden" id="ret_base_url" value="{{URL::to('/')}}">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
<input type="hidden" value="{{$purchase_return_head_id}}" id="purchase_return_head_id" name="purchase_return_head_id">
<input type="hidden" value="" id="pur_return_no">
<input type="hidden" value="{{$approve_status}}" id="approve_status_check" name="approve_status_check">
<input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">


@stop
@section('javascript_extra')
<script
    src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_return.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

@endsection
