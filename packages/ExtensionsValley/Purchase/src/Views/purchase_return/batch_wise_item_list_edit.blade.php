<div class="row">
    <div class="col-md-12 item_label">
        <div class="col-md-12 pull-right padding_sm">
            <div class="checkbox checkbox-primary checkbox-inline">
                <input id="remove_all_tax" class="styled" type="checkbox">
                <label for="remove_all_tax">Remove Tax</label>
            </div>
        </div>
        <div class="col-md-12 padding_sm">
            <div class="theadscroll">
                <table class="table table_round_border styled-table">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="width: 6%;">Vendor</th>
                            <th>Batch</th>
                            <th title="Expiry Date" style="width: 9%;">Exp.Dt</th>
                            <th style="width: 6%;">Bill No.</th>
                            <th title="Invoice Date" style="width: 6%;">Inv.Dt</th>
                            <th title="Print Batch" style="width: 6%;">Prt.Batch</th>
                            <th style="width: 5%;">Stock</th>
                            <th style="width: 5%;">Unit</th>
                            <th title="Return Qty" style="width: 6%;">Ret.Qty</th>
                            @if ($approve_status > 0)
                            <th title="Rejected Qty" style="width: 6%;">Rej.Qty</th>
                            @endif
                            <th style="width: 5%; display: none;">Fr.Qty</th>
                            <th title="Discount Percentage" style="width: 6%;">Dis.(%)</th>
                            <th title="Discount Amount" style="width: 6%;">Dis. Amt.</th>
                            <th title="Actual Purchase Rate" style="width: 5%;">Act.Pur.Rt</th>
                            <th style="width: 6%;">Pur Cost</th>
                            <th style="width: 5%;">Tax(%)</th>
                            <th style="width: 5%;">MRP</th>
                            <th style="width: 6%;">Net Tax</th>
                            <th style="width: 6%;">Net Amt</th>
                        </tr>
                    </thead>
                    <tbody id="item_return_tbody_edit">
                        <?php
                        $i = 0;


                        $selected_batch_items = json_decode($selected_batch_items,TRUE);
                        $item_units = array();
                        if (count($item_return_detail) > 0) {
                            foreach ($item_return_detail as $each) {
                                    $batch_no = '';
                                    $print_batch = '';
                                    $bill_no = '';
                                    $return_qty = '';
                                    $rejected_qty = '';
                                    $return_expirydate = '';
                                    $return_invoice_date = '';
                                    $return_stock = '';
                                    $return_item_unit_batch = '';
                                    $return_purchase_cost = '';
                                    $return_discount_perc = '';
                                    $return_discount_amt = '';
                                    $return_tax_perc = '';
                                    $return_mrp = '';
                                    $return_net_amount = '';
                                    $return_net_tax = '';
                                    $actual_purchase_rate = '';
                                    $return_purchase_rate = '';

                                $bill_no = trim($each->bill_no);
                                $print_batch = isset($each->print_batch)?$each->print_batch:'';
                                $invoice_date = !empty($each->grn_date) ? $each->grn_date : '';
                                $purchase_cost = isset($each->purchase_cost) ? $each->purchase_cost : 0;
                                $discount_perc = isset($each->discount_perc) ? $each->discount_perc : 0;
                                $discount_amt = isset($each->discount_amt) ? $each->discount_amt : 0;
                                $pur_rate = isset($each->purchase_rate)?$each->purchase_rate:0;
                                $return_expirydate = isset($each->expiry_date)?$each->expiry_date:'';
                                $return_item_unit_batch = isset($each->item_issue_unit)?$each->item_issue_unit:'';
                                $return_qty = isset($each->quantity)?$each->quantity:'';
                                $rejected_qty = isset($each->rejected_qty)?$each->rejected_qty:'';
                                $actual_purchase_rate = isset($each->actual_pur_rate)?$each->actual_pur_rate:'';
                                $return_purchase_cost = isset($each->unit_cost)?$each->unit_cost:'';
                                $return_net_tax = isset($each->net_tax)?$each->net_tax:'';
                                $return_net_amount= isset($each->total_cost)?$each->total_cost:'';
                                $total_free_qty = isset($each->total_free_qty)?$each->total_free_qty:'';

                                if(isset($selected_batch_items[$item_code][$i]['return_qty']) && ($selected_batch_items[$item_code][$i]['return_qty'] !='' || $selected_batch_items[$item_code][$i]['return_qty'] !=0 )){

                                    $batch_no = $selected_batch_items[$item_code][$i]['return_batch'] ? $selected_batch_items[$item_code][$i]['return_batch'] :'';
                                    $print_batch = $selected_batch_items[$item_code][$i]['print_batch'] ? $selected_batch_items[$item_code][$i]['print_batch'] :$batch_no;
                                    $bill_no = $selected_batch_items[$item_code][$i]['return_bill_no'] ? $selected_batch_items[$item_code][$i]['return_bill_no'] :'';
                                    $return_qty = $selected_batch_items[$item_code][$i]['return_qty'] ? $selected_batch_items[$item_code][$i]['return_qty'] :'';
                                    $rejected_qty = $selected_batch_items[$item_code][$i]['rejected_qty'] ? $selected_batch_items[$item_code][$i]['rejected_qty'] :'';
                                    $return_expirydate = $selected_batch_items[$item_code][$i]['return_expirydate'] ? $selected_batch_items[$item_code][$i]['return_expirydate'] :'';
                                    $invoice_date = $selected_batch_items[$item_code][$i]['return_invoice_date'] ? $selected_batch_items[$item_code][$i]['return_invoice_date'] :'';
                                    $stock = $selected_batch_items[$item_code][$i]['return_stock'] ? $selected_batch_items[$item_code][$i]['return_stock'] :'';
                                    $return_item_unit_batch = $selected_batch_items[$item_code][$i]['return_item_unit_batch'] ? $selected_batch_items[$item_code][$i]['return_item_unit_batch'] :'';
                                    $return_purchase_cost = $selected_batch_items[$item_code][$i]['return_purchase_cost'] ? $selected_batch_items[$item_code][$i]['return_purchase_cost'] :'';
                                    $discount_perc = $selected_batch_items[$item_code][$i]['return_discount_perc'] ? $selected_batch_items[$item_code][$i]['return_discount_perc'] :'';
                                    $discount_amt = $selected_batch_items[$item_code][$i]['return_discount_amt'] ? $selected_batch_items[$item_code][$i]['return_discount_amt'] :'';
                                    $return_tax_perc = $selected_batch_items[$item_code][$i]['return_tax_perc'] ? $selected_batch_items[$item_code][$i]['return_tax_perc'] :'';
                                    $return_mrp = $selected_batch_items[$item_code][$i]['return_mrp'] ? $selected_batch_items[$item_code][$i]['return_mrp'] :'';
                                    $return_net_amount = $selected_batch_items[$item_code][$i]['return_net_amount'] ? $selected_batch_items[$item_code][$i]['return_net_amount'] :'';
                                    $return_net_tax = $selected_batch_items[$item_code][$i]['return_net_tax'] ? $selected_batch_items[$item_code][$i]['return_net_tax'] :'';
                                    $actual_purchase_rate = $selected_batch_items[$item_code][$i]['actual_purchase_rate'] ? $selected_batch_items[$item_code][$i]['actual_purchase_rate'] :'';
                                    $pur_rate = $selected_batch_items[$item_code][$i]['return_purchase_rate'] ? $selected_batch_items[$item_code][$i]['return_purchase_rate'] :'';


                                }
                                ?>

                        <tr>
                            <td class="return_vname" title=" {{ trim($each->vendor_name) }}"
                                style="text-align: left;max-width: 50px;width: 40px;text-overflow: ellipsis;white-space: nowrap;border-right: solid 1px #bbd2bd !important;overflow: hidden !important; ">
                                <input type="hidden" name="edit_value[]"
                                    value='{{ isset($each->details_id) ? $each->details_id : '' }}'>
                                {{ trim($each->vendor_name) }}
                            </td>
                            <td class="return_batch" title=" {{ trim($each->batch_no) }}"
                                style="text-align: left;max-width: 50px;width: 40px;text-overflow: ellipsis;white-space: nowrap;border-right: solid 1px #bbd2bd !important;overflow: hidden !important; ">
                                {{ trim($each->batch_no) }}
                            </td>
                            <td class="return_expirydate"
                                title="{{ $each->expiry_date ? $each->expiry_date : date(\WebConf::getConfig('date_format_web'), strtotime($each->expiry_date)) }}"
                                style="border-right: solid 1px #bbd2bd !important; ">

                                <input type="text" autocomplete="off" name="return_expiry_date[]"
                                    class="return_expiry_date form-control"
                                    value="{{ $each->expiry_date ? $each->expiry_date : date(\WebConf::getConfig('date_format_web'), strtotime($each->expiry_date)) }}">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" autocomplete="off" name="return_bill_no[]"
                                    class="form-control return_bill_no" value="{{ $bill_no }}"
                                    id="batch_bill_pop_{{ $i }}">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" autocomplete="off" name="return_invoice_date[]"
                                    class="return_invoice_date form-control" value="{{ $invoice_date }}">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" autocomplete="off" name="print_batch[]"
                                    class="form-control print_batch" value="{{ $print_batch }}">
                            </td>
                            <td class="return_stock" id="return_stock{{ $i }}"
                                style="border-right: solid 1px #bbd2bd !important;text-align: right ">
                                {{ $each->stock }}
                            </td>
                            <td>
                                <select class="item_unit_batch" class="form-control item_unit_batch"
                                    name="item_unit_batch[]" id="item_unit_batch{{ $i }}"
                                    onchange="return_qty_calculate({{ $i }},'{{ $each->item_code }}')">
                                    <?php
                                    if (isset($item_unit) && sizeof($item_unit) > 0) {
                                        $item_units = $item_unit;
                                        foreach ($item_units as $units) {
                                            if ($return_item_unit_batch == $units->uom_id) {
                                                $selected = 'selected';
                                            } else {
                                                $selected = '';
                                            }
                                            echo "<option $selected value='$units->uom_id' >$units->uom_name</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type='number' autocomplete="off" name="ret_item_qty[]" id="ret_item_qty{{ $i }}"
                                    class="form-control" value="{{ $return_qty }}" placeholder="Quantity" min='0'
                                    onkeyup="number_validation(this)"
                                    onblur="return_qty_calculate({{ $i }},'{{ $each->item_code }}')"
                                    style="text-align: right">
                            </td>
                            @if ($approve_status > 0)
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type='number' autocomplete="off" name="rejected_qty[]" id="rejected_qty{{ $i }}"
                                    class="form-control" value="{{ $rejected_qty }}" placeholder="Quantity" min='0'
                                    onkeyup="number_validation(this)"
                                    onchange="changeReturnQty({{ $i }},'{{ $each->item_code }}')"
                                    onblur="return_qty_calculate({{ $i }},'{{ $each->item_code }}')"
                                    style="text-align: right">
                            </td>
                            @endif
                            <td style="border-right: solid 1px #bbd2bd !important; display: none; ">
                                <input type="text" autocomplete="off" name="free_qty[]" id="free_qty{{ $i }}"
                                    class="form-control " value='{{ $total_free_qty }}' readonly=""
                                    style="text-align: right">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type='number' autocomplete="off" name="discount_perc[]"
                                    id="discount_perc{{ $i }}" class="form-control" value="{{ $discount_perc }}"
                                    placeholder="Disc.(%)" min='0'
                                    onblur="return_qty_calculate({{ $i }},'{{ $each->item_code }}')"
                                    style="text-align: right">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" autocomplete="off" name="discount_amt[]" id="discount_amt{{ $i }}"
                                    class="form-control " value="{{ $discount_amt }}" placeholder="Disc Amt."
                                    readonly="" style="text-align: right">
                            </td>

                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" autocomplete="off" name="actual_purchase_rate[]"
                                    id="actual_purchase_rate{{ $i }}" class="form-control "
                                    value="{{ $actual_purchase_rate }}" readonly="" style="text-align: right">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important;">
                                <input type="text" autocomplete="off" name="purchase_cost[]" id="purchase_cost{{ $i }}"
                                    class="form-control " value='{{ $return_purchase_cost }}'
                                    onblur="return_qty_calculate({{ $i }},'{{ $each->item_code }}')"
                                    style="text-align: right">
                            </td>
                            <td id="return_tax_perc{{ $i }}" class="return_tax_perc"
                                style="text-align: right;border-right: solid 1px #bbd2bd !important;">
                                <input type="text" name="return_tax_perce[]"
                                    onblur="return_qty_calculate({{ $i }},'{{ $each->item_code }}')"
                                    id="return_tax_perce{{ $i }}" value="{{ $each->tax_rate }}"
                                    class="form-control return_tax_perce_all" readonly>
                                <input type="hidden" name="return_tax_perce_hid[]" id="return_tax_perce_hid{{ $i }}"
                                    value="{{ $each->tax_rate }}" class="form-control return_tax_hid_all">
                            </td>
                            <td id="return_mrp" class="return_mrp"
                                style="text-align: right;border-right: solid 1px #bbd2bd !important;">
                                {{ $each->mrp }}</td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" autocomplete="off" name="return_net_tax[]"
                                    id="return_net_tax{{ $i }}" class="form-control" value="{{ $return_net_tax }}"
                                    placeholder="Net Tax" readonly="" style="text-align: right">
                            </td>
                            <td style="border-right: solid 1px #bbd2bd !important; ">
                                <input type="text" name="return_net_amount[]" id="return_net_amount{{ $i }}"
                                    class="form-control " value="{{ $return_net_amount }}"
                                    placeholder="Net Amount" readonly="" style="text-align: right">
                                <input type="hidden" name="return_purchase_rate_data"
                                    onblur="return_qty_calculate({{ $i }},'{{ $each->item_code }}')"
                                    id="return_purchase_rate_data{{ $i }}" value="{{ $pur_rate }}" class="form-control "
                                    readonly>
                            </td>

                        </tr>
                        <?php
                                $i++;
                            }
                        } else {
                            echo " <tr><td colspan='20' style='text-align: center;'>No Result Found</td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" name="item_code_hidden" id="item_code_hidden" value="">
    <input type="hidden" id="item_unit_hidden" value='<?= $item_unit_array ?>'>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class=" btn btn-warning"><i class="fa fa-times"></i> Close</button>
    <span id="calculate_okbtn" onclick="save_return_list(<?= count($item_return_detail) ?>);"
        class=" btn btn-success"><i class="fa fa-thumbs-up"></i> OK</span>
</div>
