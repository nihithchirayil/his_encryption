<?php
$row_count=1;
if(count($res)!=0){
    foreach ($res as $each) {
        ?>
<tr style="background: #FFF;" class="row_class" id="row_data_{{ $row_count }}">
    <td class='row_count_class'>{{ $row_count }}
    </td>
    <td style="position: relative;">
        <input readonly style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc_{{ $row_count }}"
            onkeyup='searchItemCode(this.id,event,{{ $row_count }})' value="<?= $each->item_desc ?>"
            class="form-control popinput" name="item_desc[]" placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
               position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td title="Unit converion">
        <select name="uom_select[]" id="uom_select_id_{{ $row_count }}" class="form-control">
            <?= @$item_array[$each->item_id] ? $item_array[$each->item_id] : '' ?>
        </select>
    </td>
    <td>
        <input class="form-control number_class" value="<?= $each->quantity ?>"  id="request_qty{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='request_qty[]'
            type="text">
        <input type='hidden' name='row_id_hidden[]' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' value="<?= $each->item_id ?>" name='item_id_hidden[]'
            id="item_id_hidden{{ $row_count }}">
    </td>
    <td><input class="form-control" value="<?= $each->comments ?>" id="comments{{ $row_count }}" name="comments[]"
            type="text"></td>
    <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i></td>
</tr>
<?php
 $row_count++;
    }
}
?>
<input type='hidden' value="<?= $row_count ?>" id="editdatalistmaxrow">
