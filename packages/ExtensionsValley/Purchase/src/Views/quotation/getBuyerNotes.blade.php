<div class="theadscroll" style="position: relative; height: 400px;">
    <?php
            if (count($res) != 0){
                $user_name='';
                foreach ($res as $each){
                    ?>
    <div id="buyernotes_list<?= $each->id ?>">
        <div class='clearfix'></div>
        <div class="col-md-12 no-padding no-margin bg-green">
            <div class=" col-md-5 no-padding no-margin" style="margin-top: 3px !important">
                <label><strong> User Name :<?= strtoupper($each->name) ?></strong></label>
            </div>
            <div class="col-md-5 no-padding no-margin" style="margin-top: 3px !important">
                <label><strong> Negotiate Price :<?= $each->negotiate_price ?></strong></label>
            </div>
            <?php if ($user_id == $each->created_by && $finalize_negotiation=='0') {
            ?>
            <div class="col-md-2 no-padding no-margin" style="margin-top: 3px !important">
                <button id="editBuyerNotesBtn<?= $each->id ?>" class="btn btn-warning"
                    onclick="editBuyerNotes(<?= $each->id ?>)" type="button"><i id="editBuyerNotesSpin<?= $each->id ?>"
                        class="fa fa-edit"></i></button>
                <button id="deleteBuyerNotesBtn<?= $each->id ?>" class="btn btn-danger"
                    onclick="deleteBuyerNotes(<?= $each->id ?>,<?= $vendor_id ?>)" type="button"><i
                        id="deleteBuyerNotesSpin<?= $each->id ?>" class="fa fa-trash"></i></button>
            </div>
            <?php
        }
        ?>
        </div>
        <div class='clearfix'></div>
        <div class="col-md-12 padding_sm" style="min-height: 100px;">
            <?= $each->buyer_notes ?>
        </div>

        <?php
                }
            }
            ?>
    </div>
</div>
