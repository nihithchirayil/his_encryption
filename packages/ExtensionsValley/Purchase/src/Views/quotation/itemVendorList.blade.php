<div class="row padding_sm">
    <div class="col-md-12 padding_sm">
        <div class="col-md-7 padding_sm">
            <div class="theadscroll" style="position: relative; height: 510px;">
                <table
                    class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                    style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th class="common_td_rules" width="5%">SI.No.</th>
                            <th class="common_td_rules" width="30%">RFQ</th>
                            <th class="common_td_rules" width="15%">Unit</th>
                            <th class="common_td_rules" width="15%">Qty</th>
                            <th class="common_td_rules" width="35%">Comments</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $row_count=1;
                        if(count($res)!=0){
                            foreach ($res as $each) {
                                ?>
                        <tr style="background: #FFF;" class="row_class">
                            <td class='common_td_rules'>{{ $row_count }}</td>
                            <td>
                                <input readonly style="border-radius: 4px;" type="text" required="" autocomplete="off"
                                    value="<?= $each->item_desc ?>" class="form-control" placeholder="Search Item">
                            </td>
                            <td title="Unit converion">
                                <select name="uom_select[]" class="form-control">
                                    <?= @$item_array[$each->item_id] ? $item_array[$each->item_id] : '' ?>
                                </select>
                            </td>
                            <td>
                                <input class="form-control" readonly type="text" value="<?= $each->quantity ?>">
                            </td>
                            <td>
                                <input readonly class="form-control" value="<?= $each->comments ?>" type="text">
                            </td>
                        </tr>
                        <?php
                         $row_count++;
                            }
                        }else{
                            ?>
                        <tr>
                            <td colspan="6" style="text-align: center">No Result Found</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-5 padding_sm">
            <div class="theadscroll" style="position: relative; height: 400px;">
                <table
                    class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                    style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th class="common_td_rules" width="5%">SI.No.</th>
                            <th class="common_td_rules" width="45%">Vendor Name</th>
                            <th class="common_td_rules" width="25%">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i=1;
                        if(count($vendors)!=0){
                            foreach ($vendors as $each) {
                                ?>
                        <tr style="background: #FFF;" class="row_class">
                            <td class='common_td_rules'>{{ $i }}</td>
                            <td class='common_td_rules'>{{ $each->vendor_name }}</td>
                            <td class='common_td_rules'>{{ $each->email }}</td>
                        </tr>
                        <?php
                         $i++;
                            }
                        }else{
                            ?>
                        <tr>
                            <td colspan="4" style="text-align: center">No Result Found</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
