<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var quotation_id = $('#quotation_no_hidden').val();
            var to_date = $('#to_date').val();
            var from_date = $('#from_date').val();
            var token = $('#hidden_filetoken').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    quotation_id: quotation_id,
                    to_date: to_date,
                    from_date: from_date
                },
                beforeSend: function() {
                    $('#searchQuotationBtn').attr('disabled', true);
                    $('#searchQuotationSpin').removeClass('fa fa-search');
                    $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function() {
                    $('#searchQuotationBtn').attr('disabled', false);
                    $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchQuotationSpin').addClass('fa fa-search');
                },
                error: function() {
                    toastr.error('Please check your internet connection and try again');
                }
            });
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="common_td_rules" width="25%">Quotation No.</th>
                <th class="common_td_rules" width="15%">Created At</th>
                <th class="common_td_rules" width="65%">Vendor Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($quotation_list) != 0) {
                foreach ($quotation_list as $list) {
                    $count=0;
                    if(isset($count_array[$list->head_id])){
                        $count=$count_array[$list->head_id];
                    }
                    ?>
            <tr style="cursor: pointer" onclick="getQuotationData(<?= $list->head_id ?>,1,'<?= $list->quotation_no ?>')">
                <td id="quotation_no_data<?= $list->head_id ?>" class="common_td_rules">{{ $list->quotation_no }}</td>
                <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->quotation_date)) }}</td>
                <td class="common_td_rules">{{ $list->vendor_name }}</td>
            </tr>
            <?php
                }
            }else{
                ?>
            <tr>
                <td style="text-align: center" colspan="4" class="re-records-found">No Records Found</td>
            </tr>
            <?php
            }
            ?>

        </tbody>
    </table>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
