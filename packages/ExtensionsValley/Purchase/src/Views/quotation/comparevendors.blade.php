<div class="col-md-12 padding_sm">
    <?php
    $col_width='col-md-12';
    if($array_cnt=='2'){
        $col_width='col-md-6';
    }else if($array_cnt=='3'){
        $col_width='col-md-4';
    }else if($array_cnt=='4'){
        $col_width='col-md-3';
    }
    if(count($notes_array['vendor_note'])!=0){
        $vender_id='';
        foreach ($notes_array['vendor_note'] as $key=>$val) {
            ?>
    <div class="<?= $col_width ?> padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 680px;">
                <?php
            if($vender_id!=$key){
                ?>
                <div class="col-md-12 padding_sm bg-green">
                    <div class=" col-md-9 padding_sm pull-right" style="margin-top: 3px !important">
                        <label><strong>
                                <?= @$notes_array['vendor_name'][$key][0]->vendor_name ? $notes_array['vendor_name'][$key][0]->vendor_name : '' ?>
                            </strong></label>
                    </div>
                    <div class=" col-md-3 padding_sm pull-right" style="margin-top: 3px !important">
                        <label><strong> Vendor Notes</strong></label>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="theadscroll" style="position: relative; height: 250px">
                        <?= @$val['item_remarks'] ? $val['item_remarks'] : '' ?>
                    </div>
                </div>
                <?php
            }
            if(isset($notes_array['buyer_note'][$key])){
                foreach ($notes_array['buyer_note'][$key] as $each) {
            ?>
                <div class="col-md-12 no-padding no-margin bg-info">

                    <div class=" col-md-7 no-padding no-margin pull-right" style="margin-top: 3px !important">
                        <label><strong> User Name :<?= strtoupper($each->name) ?></strong></label>
                    </div>
                    <div class="col-md-5 no-padding no-margin pull-right" style="margin-top: 3px !important">
                        <label><strong> Negotiate Price :<?= $each->negotiate_price ?></strong></label>
                    </div>
                    <div class='clearfix'></div>
                </div>
                <div class="col-md-12 padding_sm" style="min-height: 250px">
                    <div class="theadscroll" style="position: relative; height: 250px">
                        <?= $each->buyer_notes ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>

            </div>
        </div>
    </div>
    <?php
        }
            }
    ?>
</div>
