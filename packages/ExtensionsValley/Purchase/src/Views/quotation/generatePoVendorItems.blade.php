<div class="theadscroll" style="position: relative; height: 510px;">
    <input type="hidden" id="po_check_status_hidden" value="<?= $check_status ?>">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th  width="5%">SI.No.</th>
                <th  width="30%">Item</th>
                <th  width="15%">Unit</th>
                <th  width="10%">quantity</th>
                <th title="Negotiate Unit Price" width="15%">Negotiate Unit Price</th>
                <th  width="15%">Net Amount</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $row_count=1;
            if(count($res)!=0){
                foreach ($res as $each) {
                    $net_amount=floatval($each->quantity * $each->negotiate_price);
                    ?>
            <tr style="background: #FFF;" class="row_class">
                <td class='common_td_rules'>{{ $row_count }}</td>
                <td>
                    <input readonly style="border-radius: 4px;" type="text" required="" autocomplete="off"
                        value="<?= $each->item_desc ?>" class="form-control" placeholder="Search Item">
                </td>
                <td title="Unit converion">
                    <select readonly name="uom_select[]" class="form-control">
                        <?= @$item_array[$each->item_id] ? $item_array[$each->item_id] : '' ?>
                    </select>
                </td>
                <td>
                    <input readonly class="form-control" readonly type="text" value="<?= $each->quantity ?>">
                </td>
                <td>
                    <input readonly class="form-control" readonly type="text" value="<?= $each->negotiate_price ?>">
                </td>
                <td>
                    <input readonly class="form-control" value="<?= $net_amount ?>" type="text">
                </td>
            </tr>
            <?php
             $row_count++;
                }
            }else{
                ?>
            <tr>
                <td colspan="6" style="text-align: center">No Result Found</td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
