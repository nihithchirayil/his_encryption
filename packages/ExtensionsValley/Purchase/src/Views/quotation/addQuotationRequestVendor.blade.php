<div class="theadscroll" style="position: relative; height: 450px;">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg ">
                <th colspan="5" style="text-align: center">Mapped Vendors</th>
            </tr>
            <tr class="table_header_bg ">
                <th width="5%">SL.No.</th>
                <th width="35%">Vendor Name</th>
                <th width="15%">Code</th>
                <th width="35%">Email</th>
                <th width="10%">
                    <div class="checkbox checkbox-warning inline no-margin">
                        <input type="checkbox" onclick="checkallvendor()" class="form-control filters"
                            id="checkAllVendor">
                        <label for="checkAllVendor"></label>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(count($res)!=0){
                $i=1;
                foreach ($res as $each) {
                    $checked="";
                    if (in_array($each->vendor_id, $vendor_array)){
                        $checked="checked";
                    }
                        ?>
            <tr>
                <td id="VendorRowDatatd<?= $each->vendor_id ?>" class="common_td_rules"><?= $i ?></td>
                <td class="common_td_rules"><?= $each->vendor_name ?></td>
                <td id="VendorCodetd<?= $each->vendor_id ?>" class="common_td_rules"><?= $each->vendor_code ?></td>
                <td class="common_td_rules">
                    <input class="form-control" onblur="updateVendorEmail(<?= $each->vendor_id ?>,1)"
                        autocomplete="off" id="VendorEmailtd<?= $each->vendor_id ?>" placeholder="Email"
                        name="vendor_email[]" type="text" value="<?= $each->email ?>">
                </td>
                <td style="text-align: center">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input <?= $checked ?> type="checkbox" class="check_vendor" name="summary"
                            value="<?= $each->vendor_id ?>" id="selectVendor<?= $each->vendor_id ?>">
                        <label for="selectVendor<?= $each->vendor_id ?>"></label>
                    </div>
                </td>
            </tr>
            <?php
            $i++;
                }
            }else {
                ?>
            <tr>
                <td colspan="5" style="text-align: center">No Result Found</td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
