@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="modal fade" id="addVendorModelData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Create RFQ</h4>
                </div>
                <div class="modal-body" style="min-height: 500px">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 padding_sm" id="addvendorDiv">
                        </div>
                        <div class="col-md-6 padding_sm">
                            <input type="hidden" name="vendorrow_count" id="vendorrow_count_id" value="0">
                            <div class="theadscroll" style="position: relative; height: 450px;">
                                <table
                                    class="table theadfix_wrapper table_sm table-condensed table-col-bordered center_align"
                                    style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_bg ">
                                            <th colspan="5" style="text-align: center"><button onclick="addNewVendor()"
                                                    id="addNewVendorRowbtn" class="btn btn-warning" type="button">More
                                                    Vendors <i class="fa fa-list" id="addNewVendorRowSpin"></i></button>
                                            </th>
                                        </tr>
                                        <tr class="table_header_bg" id="add_new_vendor_thead">
                                            <th width="5%">SL.No.</th>
                                            <th width="40%">Vendor Name</th>
                                            <th width="10%">Code</th>
                                            <th width="40%">Email</th>
                                            <th width="5%">-</th>
                                        </tr>
                                    </thead>
                                    <tbody id="add_new_vendor">
                                        <?php
                                    $row_count=0;
                                    if(count($rfq_vendor)!=0){
                                        foreach ($rfq_vendor as $each) {
                                            $row_count = $each->row_id;
                                            ?>
                                        <?php $row_count = ++$row_count; ?>
                                        <tr style="background: #FFF;" class="vendorrow_class"
                                            id="vendorrow_data_{{ $row_count }}">
                                            <td class='vendorrow_count_class'>{{ $row_count }}</td>
                                            <td style="position: relative;">
                                                <input readonly style="border-radius: 4px;" type="text" required=""
                                                    autocomplete="off" id="newvendor_{{ $row_count }}"
                                                    value="<?= $each->vendor_name ?>"
                                                    onkeyup='searchNewVendor(this.id,event,{{ $row_count }})'
                                                    class="form-control popinput" name="NewvendorData[]"
                                                    placeholder="Vendor Search">
                                                <div class='ajaxSearchBox' id="ajaxVendorSearchBox_{{ $row_count }}"
                                                    style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
                                                                                                                                                                       position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
                                                </div>
                                            </td>
                                            <td>
                                                <input class="form-control" name='vendorCode[]'
                                                    id="vendorcode{{ $row_count }}" autocomplete="off" placeholder="Code"
                                                    type="text" value="<?= $each->vendor_code ?>">
                                            </td>
                                            <td>
                                                <input class="form-control" name='vendorEmail[]'
                                                    onblur="updateVendorEmail(<?= $row_count ?>,2)"
                                                    id="vendoremail{{ $row_count }}" autocomplete="off"
                                                    placeholder="Email" type="text" value="<?= $each->email ?>">
                                                <input type='hidden' name='vendorrow_id_hidden[]'
                                                    value="{{ $row_count }}"
                                                    id="vendorrow_id_hidden{{ $row_count }}">
                                                <input type='hidden' name='vendoritem_id_hidden[]'
                                                    value="<?= $each->vendor_id ?>"
                                                    id="vendoritem_id_hidden{{ $row_count }}">
                                            </td>
                                            <td>
                                                <i style="padding: 5px 8px; font-size: 15px;"
                                                    onclick="removevenderRow({{ $row_count }})"
                                                    class="fa fa-trash text-red deleteRow"></i>
                                            </td>
                                        </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                        <input type='hidden' value="<?= $row_count ?>" id="editvendoristmaxrow">
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
                    <?php
                        if($rfq_status==0 || $rfq_from=='2'){
                        ?>
                    <button style="padding: 3px 3px" type="button" id="completeRequestQuotationbtn"
                        onclick="completeRequestQuotation(<?= $rfq_from ?>)" class="btn btn-primary"><i
                            id="completeRequestQuotationSpin" class="fa fa-save"></i> Complete</button>
                    <button style="padding: 3px 3px" type="button" id="saveRequestQuotationbtn"
                        onclick="saveRequestQuotation(<?= $rfq_from ?>)" class="btn btn-warning"><i
                            id="saveRequestQuotationspin" class="fa fa-save"></i> Save As Draft</button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="quotationListModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" onclick="closeRFQView()" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="quotationListHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px" id="quotationListDiv">

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" onclick="closeRFQView()" data-dismiss="modal"
                        aria-label="Close" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-2 padding_sm">
                <strong><?= $title ?></strong>
            </div>
            <div class="col-md-2 padding_sm pull-right">
                <strong><?= $quotation_status ?></strong>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" name="row_count" id="row_count_id" value="0">
            <input type="hidden" name="rfq_id" id="rfq_id" value="<?= $rfq_id ?>">
            <input type="hidden" name="rfq_no" id="rfq_no" value="<?= $rfq_no ?>">
            <input type="hidden" id="location_defaultvalue" value="">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-3 padding_sm ">
                        <div class="mate-input-box">
                            <label style="margin-top: -2px;">Supply Location</label>
                            <div class="clearfix"></div>
                            <?php

                            ?>
                            {!! Form::select('location', $location, $default_location, ['onchange' => 'changeLocation()', 'class' => 'form-control select2', 'id' => 'location']) !!}
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Closing Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="closing_date" autofocus=""
                                value="<?= $closing_date ?>" class="form-control filters datepicker"
                                placeholder="MM-DD-YYYY">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="theadscroll" style="position: relative; height: 400px;">
                        <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
                            style="border: 1px solid #CCC;" id="main_row_tbl">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="5%">Sl.No.</th>
                                    <th width="38%">Item </th>
                                    <th width="10%">Unit</th>
                                    <th width="10%">Qty</th>
                                    <th width="35%">Comments</th>
                                    <th width="2%"><button type="button" style="margin-top: 5px;" id="addQuotationRowbtn"
                                            onclick="getNewRowInserted();" class="btn btn-primary add_row_btn"><i
                                                id="addQuotationRowSpin" class="fa fa-plus"></i></button></th>
                                </tr>
                            </thead>
                            <tbody id="row_body_data">

                            </tbody>

                        </table>
                    </div>
                    <div class="col-md-2 padding_sm pull-right" style="margin-top: 25px;">
                        <button type="button" id="addVendorBtn" onclick="addVendor()" id="generate_csvbtn"
                            class="btn btn-success btn-block"><i id="addVendorSpin" class="fa fa-forward"></i>
                            Next</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/requestforquotation.js') }}"></script>
@endsection
