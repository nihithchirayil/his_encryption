<div class="row padding_sm">
    <div class="col-md-12 padding_sm">
        <ul class="nav nav-tabs sm_nav">
            <li class="active"><a data-toggle="tab" href="#priceindetailLi" style="border-radius: 0px;"
                    aria-expanded="false"> Price in Detail</a></li>
            <li><a data-toggle="tab" href="#taxDataListli" style="border-radius: 0px;" aria-expanded="true">Tax
                    Detalis</a>
            </li>
            <li><a data-toggle="tab" href="#item_description_li" style="border-radius: 0px;" aria-expanded="true">
                    Item Description</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="priceindetailLi" class="tab-pane active in">
                <div class="col-md-12 padding_sm" style="margin-top:15px;min-height: 300px">
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Quotation Date</label>
                            <input class="form-control" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->quotation_date ? $res[0]->quotation_date : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Amount</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->total_amount_withouttax ? $res[0]->total_amount_withouttax : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Item Discount</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->total_itemdiscount ? $res[0]->total_itemdiscount : 0 ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Tax</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->total_tax ? $res[0]->total_tax : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Other Charges</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->total_other_charges ? $res[0]->total_other_charges : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Bill Discount</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->total_bill_discount ? $res[0]->total_bill_discount : 0 ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Bill Amout</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->net_total_bill ? $res[0]->net_total_bill : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Free Item</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->free_qty ? $res[0]->free_qty : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>MRP(Per Uom)</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->item_mrp ? $res[0]->item_mrp : 0 ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Total Rate</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->item_total_rate ? $res[0]->item_total_rate : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Net Rate</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->item_net_rate ? $res[0]->item_net_rate : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Rate(Per Uom)</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->item_rate ? $res[0]->item_rate : 0 ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label>Unit Cost</label>
                            <input class="form-control td_common_numeric_rules" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->unit_cost ? $res[0]->unit_cost : 0 ?>">
                        </div>
                    </div>
                    <div class="col-md-8 padding_sm">
                        <div class="mate-input-box">
                            <label>Bill Remarks</label>
                            <input class="form-control" type="text" autocomplete="off" readonly
                                value="<?= @$res[0]->narration ? base64_decode($res[0]->narration) : '' ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div id="taxDataListli" class="tab-pane">
                <div class="col-md-12 padding_sm" style="margin-top:15px;min-height: 300px">
                    <div class="theadscroll" style="position: relative; height: 300px;">
                        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th class="common_td_rules" width="5%">SI.No.</th>
                                    <th class="common_td_rules" width="45%">Charge Name</th>
                                    <th class="common_td_rules" width="25%">Percentage</th>
                                    <th class="common_td_rules" width="55%">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if (count($res) != 0) {
                                        $i=1;
                                        foreach ($res as $list) {
                                            ?>
                                <tr>
                                    <td class="common_td_rules">{{ $i }}</td>
                                    <td class="common_td_rules">{{ $list->name }}</td>
                                    <td class="td_common_numeric_rules">{{ $list->perc }}</td>
                                    <td class="td_common_numeric_rules">{{ $list->value }}</td>
                                </tr>
                                <?php
                                        $i++;
                                        }
                                    }else{
                                        ?>
                                <tr>
                                    <td colspan="4" class="re-records-found">No Records found</td>
                                </tr>
                                <?php
                                    }
                                    ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="item_description_li" class="tab-pane">
                <div class="col-md-12 padding_sm" style="margin-top:15px;min-height: 300px">
                    <div class="theadscroll" style="position: relative; height: 300px;">
                        <?= @$res[0]->item_remarks ? $res[0]->item_remarks : '' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
