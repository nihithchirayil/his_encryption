<div class="row padding_sm" style="min-height: 350px">
    <div class="col-md-12 padding_sm">
        <div class="col-md-3 padding_sm">
            <div class="theadscroll" style="position: relative; height: 450px;">
                <table class="table theadfix_wrapper table_sm table-condensed" style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg ">
                            <th width="5%">Sl.No.</th>
                            <th width="35%">Vendor Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($res)!=0){
                            $i=1;
                            foreach ($res as $each) {
                                        ?>
                        <tr class="VendorRowDatatr" id="VendorRowDatatr<?= $each->vendor_id ?>" style="cursor: pointer"
                            onclick="getQuotationPoVendorItems(<?= $each->vendor_id ?>)">
                            <td class="common_td_rules"><?= $i ?></td>
                            <td id="VendorRowVndorName<?= $each->vendor_id ?>" class="common_td_rules">
                                <?= $each->vendor_name ?></td>
                        </tr>
                        <?php
                        $i++;
                            }
                        }else {
                            ?>
                        <tr>
                            <td colspan="2" style="text-align: center">No Result Found</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-9 padding_sm">
            <h4 class="blue" id="poselected_itemvendornameheader" style="text-align: center"></h4>
            <input type="hidden" id="vendorIDPolist" value="0">
            <div id="vendorQuotationItemsDiv"> </div>
        </div>
    </div>
</div>
