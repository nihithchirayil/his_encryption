<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var quotation_no = $('#quotation_no').val();
            var to_date = $('#to_date').val();
            var from_date = $('#from_date').val();
            var to_location = $('#to_location').val();
            var status = $('#status').val();
            var token = $('#token_hiddendata').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    quotation_no: quotation_no,
                    to_location: to_location,
                    to_date: to_date,
                    from_date: from_date,
                    status: status
                },
                beforeSend: function() {
                    $('#searchQuotationBtn').attr('disabled', true);
                    $('#searchQuotationSpin').removeClass('fa fa-search');
                    $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function() {
                    $('#searchQuotationBtn').attr('disabled', false);
                    $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchQuotationSpin').addClass('fa fa-search');
                }
            });
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="15%">RFQ No.</th>
                    <th class="common_td_rules" width="10%">Created At</th>
                    <th class="common_td_rules" width="20%">Supply Location</th>
                    <th class="common_td_rules" width="10%">Closing Date</th>
                    <th class="common_td_rules" width="20%">Status</th>
                    <th class="common_td_rules" width="5%">Quotations</th>
                    <th class="common_td_rules" width="5%">Copy</th>
                    <th class="common_td_rules" width="5%">View RFQ</th>
                    <th class="common_td_rules" width="5%">Edit</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($quotation_list) != 0) {
                foreach ($quotation_list as $list) {
                    $count=0;
                    if(isset($count_array[$list->head_id])){
                        $count=$count_array[$list->head_id];
                    }
                    ?>
                <tr>
                    <td id="quotation_no_data<?= $list->head_id ?>" class="common_td_rules">{{ $list->rfq_no }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->rfq_date)) }}</td>
                    <td class="common_td_rules">{{ $list->location_name }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->closing_date)) }}</td>

                    <?php if($list->status=='1'){
                        $rfq_string='RFQ Generated';
                        if(intval($count)!=0){
                            $rfq_string='Quotation Listed';
                        }
                        if(intval($list->close_rfq)==1){
                            $rfq_string='RFQ Closed';
                        }
                        ?>
                    <td class="common_td_rules"><?= $rfq_string ?></td>
                    <td style="text-align: center">
                        <button title="Number of Vendors" style="padding: 2px 9px;" type="button"
                            class="btn btn-primary" onclick="addWindowLoad('getItemVendors',<?= $list->head_id ?>,'')">
                            <?= $count ?>
                        </button>
                    </td>
                    <td style="text-align: center">
                        <button title="Copy RFQ" type="button" class="btn bg-green"
                            onclick="addWindowLoad('editRequestForQuotation',<?= $list->head_id ?>,'2')"><i
                                class="fa fa-copy"></i>
                        </button>
                    </td>
                    <?php
                    }else{
                        ?>
                    <td class="common_td_rules">RFQ Drafted</td>
                    <td>
                        -
                    </td>
                    <td>
                        -
                    </td>
                    <?php
                    }
                    ?>
                    <td style="text-align: center">
                        <button title="RFQ View" id="listQuotationItemsBtn<?= $list->head_id ?>" type="button"
                            class="btn bg-purple" onclick="listQuotationItems(<?= $list->head_id ?>)"><i
                                id="listQuotationItemsSpin<?= $list->head_id ?>" class="fa fa-list"></i>
                        </button>
                    </td>
                    <td style="text-align: center">
                        <button title="Edit RFQ List" type="button" class="btn btn-warning"
                            onclick="addWindowLoad('editRequestForQuotation',<?= $list->head_id ?>,'1')"><i
                                class="fa fa-edit"></i>
                        </button>
                    </td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="8" class="re-records-found">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
