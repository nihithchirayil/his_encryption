@extends('Dashboard::dashboard.notUserDashboard')
@section('content-header')
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">

@endsection
@section('content-area')

    <div class="right_col" style="margin-left: 0px !important" role="main">
        <div class="row padding_sm">
            <div class="col-md-4 padding_sm">
                <h5 style="text-align: center" id="quotation_vendor_title" class="green"><?= $title ?></h5>
            </div>
            <div class="col-md-4 padding_sm">
                <h5 style="text-align: center" class="blue">Vendor Name: <?= $quotation_status ?></h5>
            </div>
            <div class="col-md-4 padding_sm">
                <h5 style="text-align: center" class="red closing_date">Closing Date: <?= $closing_date ?></h5>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
            <input type="hidden" id="head_id_hidden" value="0">
            <input type="hidden" id="vendor_status_hidden" value='<?= base64_encode(json_encode($vendor_status)) ?>'>
        </div>

        <div class="x_panel">
            <div class="row padding_sm">
                <div class="col-md-9 no-padding no-margin">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            <div class="theadscroll" style="position: relative; height: 450px;">
                                <table
                                    class="table theadfix_wrapper table_sm table-condensed table-col-bordered center_align"
                                    id="main_row_tbl" style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_bg ">
                                            <th width="3%">SL.No.</th>
                                            <th width="36%">Item </th>
                                            <th width="13%">Unit</th>
                                            <th width="8%">Qty</th>
                                            <th width="8%">Rate</th>
                                            <th width="8%">Unit Cost</th>
                                            <th width="8%">Tax Amt.</th>
                                            <th width="8%">Discount Amt.</th>
                                            <th width="8%">Net Rate.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                        $row_count=1;
                        if(count($res)!=0){
                            foreach ($res as $each) {
                                $free_qty=@$vendor_status['detail'][$each->detail_id]['free_qty'] ? $vendor_status['detail'][$each->detail_id]['free_qty'] :0;
                                $item_rate=@$vendor_status['detail'][$each->detail_id]['item_rate'] ? $vendor_status['detail'][$each->detail_id]['item_rate'] :0;
                                $unit_cost=@$vendor_status['detail'][$each->detail_id]['unit_cost'] ? $vendor_status['detail'][$each->detail_id]['unit_cost'] :0;
                                $item_net_rate=@$vendor_status['detail'][$each->detail_id]['item_net_rate'] ? $vendor_status['detail'][$each->detail_id]['item_net_rate'] :0;
                                $quantity=intval($each->quantity)+$free_qty;
                                ?>
                                        <tr style="background: #FFF; cursor: pointer;"
                                            class="row_class additemPurchaseDetails"
                                            onclick="addItemDetails(<?= $row_count ?>,<?= $each->item_id ?>,<?= $each->detail_id ?>)"
                                            id="row_data_{{ $row_count }}">
                                            <td class='row_count_class addItemData item_selection_{{ $row_count }}'>
                                                {{ $row_count }}</td>
                                            <td>
                                                <input readonly type="text" autocomplete="off" required=""
                                                    autocomplete="off" id="item_desc_{{ $row_count }}"
                                                    value="<?= trim($each->item_desc) ?>"
                                                    class="form-control addItemData item_selection_{{ $row_count }}"
                                                    name="item_desc[]" placeholder="Search Item">
                                            </td>
                                            <td>
                                                <select readonly name="uom_select[]" id="uom_select_id_{{ $row_count }}"
                                                    class="form-control addItemData item_selection_{{ $row_count }}">
                                                    <?= @$item_array[$each->item_id] ? $item_array[$each->item_id] : '' ?>
                                                </select>
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class td_common_numeric_rules addItemData item_selection_{{ $row_count }}"
                                                    id="request_qty{{ $row_count }}" value="<?= $quantity ?>"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='request_qty[]' type="text" autocomplete="off">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class td_common_numeric_rules addItemData item_selection_{{ $row_count }}"
                                                    id="item_rate{{ $row_count }}" value="<?= $item_rate ?>"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='item_rate[]' type="text" autocomplete="off" value="0">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class td_common_numeric_rules addItemData item_selection_{{ $row_count }}"
                                                    id="item_unit_cost{{ $row_count }}" value="<?= $unit_cost ?>"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='unit_cost[]' type="text" autocomplete="off" value="0">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class td_common_numeric_rules addItemData item_selection_{{ $row_count }}"
                                                    id="item_tax_amt{{ $row_count }}"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='tax_amt[]' type="text" autocomplete="off" value="0">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class td_common_numeric_rules addItemData item_selection_{{ $row_count }}"
                                                    id="item_dicount_amt{{ $row_count }}"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='dicount_amt[]' type="text" autocomplete="off" value="0">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class td_common_numeric_rules addItemData item_selection_{{ $row_count }}"
                                                    id="item_net_amt{{ $row_count }}"
                                                    value="<?= number_format($item_net_rate, 2, '.', '') ?>"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='net_amt[]' type="text" autocomplete="off" value="0">
                                            </td>
                                        </tr>
                                        <?php
                                $row_count++;
                                    }
                                }
                                ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top:15px">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <div class="col-md-3 padding_sm">
                                    <table class="table no-margin table_sm theadfix_wrapper"
                                        style="border: 1px solid #CCC;">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th>Total </th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($charges_head as $charge) {
                                                ?>
                                            <tr>
                                                <td class="common_td_rules">{{ $charge->name }}</td>
                                                <td class="td_common_numeric_rules"
                                                    id="<?= $charge->status_code ?>total_amt">
                                                    0
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <div class="box no-border no-margin">
                                        <div class="box-body clearfix">
                                            <div class="col-md-12 padding_sm">
                                                <div class="custom-float-label-control mate-input-box">
                                                    <label class="custom_floatlabel ">Total Discount Percentage</label>
                                                    <input
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        onblur="addTotalDiscount(this,1)"
                                                        class="form-control td_common_numeric_rules percentage_class"
                                                        value="0" type="text" autocomplete="off" maxlength="8"
                                                        id="totalbill_discountPercentage">
                                                </div>
                                            </div>
                                            <div class="col-md-12 padding_sm">
                                                <div class="custom-float-label-control mate-input-box">
                                                    <label class="custom_floatlabel ">Total Discount Amount</label>
                                                    <input
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        onblur="addTotalDiscount(this,2)"
                                                        class="form-control td_common_numeric_rules" value="0" type="text"
                                                        autocomplete="off" maxlength="8" id="totalbill_discountAmount">
                                                </div>
                                            </div>
                                            <div class="col-md-12 padding_sm">
                                                <div class="custom-float-label-control mate-input-box">
                                                    <label class="custom_floatlabel ">Remarks</label>
                                                    <textarea style="height: 45px !important" class="form-control"
                                                        value="" type="text" autocomplete="off"
                                                        id="fullbill_remarks"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="box no-border no-margin">
                                        <div class="box-body clearfix">
                                            <table class="table no-margin table_sm no-border grn_total_table">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%"><label class="text-bold">(+) Total
                                                                Amount</label>
                                                        </td>
                                                        <td><input type="text" autocomplete="off"
                                                                class="form-control text-right td_common_numeric_rules"
                                                                readonly="" value="0.00" style="font-weight: 700;"
                                                                id="gross_amount_hd" name="gross_amount_hh"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"><label class="text-bold">(-) Item
                                                                Discount</label>
                                                        </td>
                                                        <td><input type="text" autocomplete="off"
                                                                class="form-control text-right td_common_numeric_rules"
                                                                readonly="" value="0.00" style="font-weight: 700;"
                                                                autocomplete="off" id="discount_hd" name="discount_hd"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"><label class="text-bold">(+) Total Tax</label>
                                                        </td>
                                                        <td><input type="text" autocomplete="off"
                                                                class="form-control text-right td_common_numeric_rules"
                                                                readonly="" value="0.00" style="font-weight: 700;"
                                                                autocomplete="off" id="tot_tax_amt_hd"
                                                                name="tot_tax_amt_hd"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"><label class="text-bold">(+) Other
                                                                Charges</label>
                                                        </td>
                                                        <td><input type="text" autocomplete="off"
                                                                class="form-control text-right td_common_numeric_rules"
                                                                readonly="" value="0.00" style="font-weight: 700;"
                                                                autocomplete="off" id="tot_other_charges_hd"
                                                                name="tot_tax_amt_hd"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"><label class="text-bold">(-) Total Bill
                                                                Discount</label></td>
                                                        <td><input type="text" autocomplete="off"
                                                                class="form-control text-right td_common_numeric_rules"
                                                                readonly="" value="0.00" style="font-weight: 700;"
                                                                autocomplete="off" id="bill_total_discount"
                                                                name="tot_tax_amt_hd"></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="box no-border no-margin">
                                        <div class="box-body clearfix">
                                            <div class="col-md-12 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Total Amt</label>
                                                    <input type="hidden" id="bill_nettotal_hidden" value="0">
                                                    <input
                                                        style="height: 36px !important; font-weight: 700; font-size: 16px !important;"
                                                        type="text" autocomplete="off" readonly=""
                                                        class="form-control text-right td_common_numeric_rules" value="0.00"
                                                        id="net_amount_hd" name="net_amount_hd">
                                                </div>
                                            </div>
                                            <div class="col-md-12 padding_sm">
                                                <button id="saveVendorQuotationbtn"
                                                    onclick="saveVendorQuotation(<?= $rfq_id ?>,<?= $vendor_id ?>,'<?= $rfq_vendor ?>','<?= $rfqvendor_id ?>')"
                                                    class="btn btn-success btn-block" type="button"><i
                                                        class="fa fa-save" id="saveVendorQuotationSpin"></i>
                                                    Save</button>
                                            </div>
                                            <div class="col-md-12 padding_sm">
                                                <button onclick="reloadSubmit('<?= $rfq_vendor ?>')"
                                                    class="btn btn-warning btn-block" type="button"><i
                                                        class="fa fa-recycle"></i> Reload</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 no-padding no-margin">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            <h4 id="item_descheader" class="blue" style="text-align: center"></h4>
                            <div style="margin-top:10px">
                                <div class="row no-padding no-margin">
                                    <div id="model_charges_div"></div>
                                    <div id="model_remarks_div" class="col-md-12 padding_sm"
                                        style="margin-top:10px;display: none">
                                        <div class="col-md-12 no-padding no-margin">
                                            <ul class="nav nav-tabs sm_nav">
                                                <li class="active"><a data-toggle="tab" href="#setvendorpriceli"
                                                        style="border-radius: 0px;" aria-expanded="false"> Rate</a></li>
                                                <li><a data-toggle="tab" href="#add_vendorremarksli"
                                                        style="border-radius: 0px;" aria-expanded="true">Remarks</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="setvendorpriceli" class="tab-pane active in"
                                                    style="margin-top: 10px;min-height: 250px">
                                                    <div class="col-md-12 padding_sm">
                                                        <div class="mate-input-box">
                                                            <label>Unit Cost</label>
                                                            <input readonly
                                                                class="form-control td_common_numeric_rules check_selling_price"
                                                                type="text" autocomplete="off" id="itemUnitCost"
                                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                                value="0">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 padding_sm">
                                                        <div class="mate-input-box">
                                                            <label>Total Rate</label>
                                                            <input readonly
                                                                class="form-control td_common_numeric_rules check_selling_price"
                                                                type="text" autocomplete="off" id="itemTotalRate"
                                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                                value="0">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 padding_sm">
                                                        <div class="mate-input-box">
                                                            <label>Net Rate</label>
                                                            <input readonly
                                                                class="form-control td_common_numeric_rules check_selling_price"
                                                                type="text" autocomplete="off" id="itemNetRate"
                                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                                value="0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="add_vendorremarksli" class="tab-pane"
                                                    style="margin-top: 10px; min-height: 250px">
                                                    <div class="col-md-12 padding_sm">
                                                        <div class="mate-input-box">
                                                            <label>Item Notes</label>
                                                            <div class="clearfix"></div>
                                                            <textarea
                                                                class="tiny_editor form-control"
                                                                id="vendor_notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/submitVendorQuotation.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
@endsection
