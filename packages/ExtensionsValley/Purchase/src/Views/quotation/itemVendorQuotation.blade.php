@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="modal fade" id="vendorQuotationmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 800px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="vendorQuotationmodelHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 180px">
                    <div class="row padding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel ">Remarks</label>
                                <textarea class="form-control" value="" type="text" autocomplete="off"
                                    id="orthervendor_remarks"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Negotiate Price</label>
                                <div class="clearfix"></div>
                                <input type="text"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    autocomplete="off" autofocus="" class="form-control" id="finalize_negotiate_price">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm pull-right" style="margin-top: 10px">
                            <div class="pull-right" style="margin-top: 10px;">
                                <input type="hidden" id="selected_vendor_id" value="0">
                                <button id="FinalizeNotesBtn" onclick="FinalizeVendorItem()"
                                    style="padding: 3px 3px;margin-top: 10px;" type="button"
                                    class="btn btn-success">Finalize <i id="FinalizeNotesSpin"
                                        class="fa fa-flag-checkered"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="compareVendorQuotationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1250px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="compareVendorQuotationHeader">NA</h4>
                </div>
                <div class="modal-body">

                    <div class="row padding_sm" id="compareVendorQuotationDiv">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="VendorQuotedDetalisModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <input type="hidden" id="buyernotesID" value="0">
        <div class="modal-dialog" style="max-width: 1250px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="VendorQuotedDetalisHeader"></h4>
                </div>
                <div class="modal-body">

                    <div class="row padding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="col-md-12 padding_sm">
                                    <div class="theadscroll" style="position: relative; height: 200px;">
                                        <div class="col-md-2 no-padding no-margin bg-green">
                                            <label><strong> Vendor Notes</label>
                                        </div>
                                        <div class="col-md-10 no-padding no-margin bg-green">
                                            <label><strong> Item Name : <span
                                                        id="notesdata_itemname"></span></strong></label>

                                        </div>
                                        <div class='clearfix'></div>
                                        <div class="col-md-12 padding_sm" style="min-height: 100px;"
                                            id="notesdata_vendornotes">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 padding_sm" id="getBuyerQuotedNotesDiv" style="margin-top: 10px;">

                                </div>

                            </div>

                            <div class="col-md-6 no-padding no-margin">
                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-12 no-padding no-margin bg-green">
                                        <label><strong> <span id="buyer_notes_label"></span></label>
                                    </div>
                                    <div class="col-md-12 no-padding no-margin">
                                        <textarea class="tiny_editor form-control" id="buyer_notes"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12 padding_sm" style="margin-top: 10px">
                                    <div class="box no-border no-margin">
                                        <div class="box-body clearfix">
                                            <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                                <div class="mate-input-box">
                                                    <label>Negotiate Price</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text"
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        autocomplete="off" autofocus="" class="form-control"
                                                        id="buyer_negotiate_price">
                                                </div>
                                            </div>
                                            <div id="notes_save_btn" style="display: none">
                                                <div class="col-md-2 padding_sm pull-right" id="finalizebtndiv"
                                                    style="margin-top: 15px;display: none;">
                                                    <button style="padding: 3px 3px" type="button"
                                                        id="finalizebuyerNotesBtn"
                                                        class="btn btn-success btn-block">Finalize <i
                                                            id="finalizebuyerNotesSpin"
                                                            class="fa fa-flag-checkered"></i></button>
                                                </div>
                                                <div class="col-md-2 padding_sm pull-right" style="margin-top: 15px">
                                                    <button style="padding: 3px 3px" type="button" id="addbuyerNotesBtn"
                                                        class="btn btn-primary btn-block">Save <i id="addbuyerNotesSpin"
                                                            class="fa fa-plus"></i></button>
                                                </div>
                                                <div class="col-md-2 padding_sm pull-right" style="margin-top: 15px">
                                                    <button style="padding: 3px 3px" type="button"
                                                        onclick="clearBuyerNewNotes(0)"
                                                        class="btn btn-warning btn-block">Clear
                                                        <i class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="submitVendorPoModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Finalize Quotation</h4>
                </div>
                <div class="modal-body">
                    <div id="submitVendorPoDiv"></div>
                </div>

                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" type="button" class="btn btn-success">Finalize <i
                            class="fa fa-bars"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" role="main">
        <div class="x_panel">
            <div class="row padding_sm">
                <div class="col-md-4 padding_sm">
                    <h5 style="text-align: center" id="quotation_vendor_title" class="green">RFQ
                        No.: <?= @$head_array[0]->rfq_no ? $head_array[0]->rfq_no : '' ?></h5>
                </div>
                <div class="col-md-4 padding_sm">
                    <h5 style="text-align: center" class="blue">Click on items for enter your quotes </h5>
                </div>
                <?php
                    $close_status= @$head_array[0]->close_rfq ? $head_array[0]->close_rfq : 0;
                if($close_status==0){
                    ?>
                <div class="col-md-4 padding_sm">
                    <h5 style="text-align: center" class="red closing_date">Closing Date:
                        <?= @$head_array[0]->closing_date ? $head_array[0]->closing_date : '' ?></h5>
                </div>
                <?php }else{
                    ?>
                <div class="col-md-4 padding_sm">
                    <h5 style="text-align: center" class="red closing_date">RFQ Closed</h5>
                </div>
                <?php
                } ?>
                <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                <input type="hidden" id="rfq_id" value="<?= $rfq_id ?>">
                <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
                <input type="hidden" id="quoted_head_details" value="<?= base64_encode(json_encode($vendor_quotes)) ?>">
            </div>
            <div class="row padding_sm">
                <div class="col-md-8 no-padding no-margin">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            <div class="theadscroll" style="position: relative; height: 550px;">
                                <table id="main_list_table"
                                    class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
                                    style="border: 1px solid #CCC;">

                                    <thead>
                                        <tr class="table_header_bg ">
                                            <th width="3%">SL.No.</th>
                                            <th width="35%">Item </th>
                                            <th width="10%">Unit</th>
                                            <th width="10%">Qty</th>
                                            <th width="10%">Vendor Price</th>
                                            <th width="10%">Net Amount</th>
                                            <th width="20%">Vendor Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                        if(count($res)!=0){
                            $i=1;
                            foreach ($res as $each) {
                                $row_count= $each->detail_id;
                                $negotiate_price=$each->negotiate_price ? $each->negotiate_price : 0;
                                if($negotiate_price!='0'){
                                    $item_price=$negotiate_price;
                                }else {
                                    $item_price=@$vendor_quotes[$row_count]['vendor_price'] ? $vendor_quotes[$row_count]['vendor_price'] : 0;
                                }
                                $total_amt=floatval($item_price)*floatval($each->quantity);

                                ?>
                                        <tr class="row_class" style="cursor: pointer"
                                            onclick="getItemsMappedVendors(<?= $row_count ?>,<?= $each->item_id ?>,<?= $each->detail_id ?>)"
                                            id="row_data_{{ $row_count }}">
                                            <td class='row_count_class addItemData item_selection_{{ $row_count }}'>
                                                {{ $i }}</td>
                                            <td>
                                                <input readonly type="text" autocomplete="off" required=""
                                                    autocomplete="off" id="item_desc_{{ $row_count }}"
                                                    value="<?= trim($each->item_desc) ?>"
                                                    class="form-control addItemData item_selection_{{ $row_count }}"
                                                    name="item_desc[]" placeholder="Search Item">
                                            </td>

                                            <td>
                                                <select readonly name="uom_select[]" id="uom_select_id_{{ $row_count }}"
                                                    class="form-control addItemData item_selection_{{ $row_count }}">
                                                    <?= @$item_array[$each->item_id] ? $item_array[$each->item_id] : '' ?>
                                                </select>
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class addItemData item_selection_{{ $row_count }}"
                                                    id="request_qty{{ $row_count }}" value="<?= $each->quantity ?>"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='request_qty[]' type="text" autocomplete="off">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control item_unitprice_amount number_class addItemData item_selection_{{ $row_count }}"
                                                    id="item_unitprice_amount{{ $row_count }}"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    type="text" autocomplete="off" value="<?= $item_price ?>">
                                            </td>
                                            <td>
                                                <input readonly
                                                    class="form-control number_class addItemData item_selection_{{ $row_count }}"
                                                    id="item_net_amount{{ $row_count }}"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    name='item_net_amount[]' type="text" autocomplete="off"
                                                    value="<?= $total_amt ?>">
                                            </td>
                                            <td>
                                                <input id="vendor_idhidden{{ $row_count }}" name='vendor_idhidden[]'
                                                    type="hidden" value="0">
                                                <input readonly
                                                    class="form-control addItemData item_selection_{{ $row_count }}"
                                                    id="vendor_name{{ $row_count }}" name='vendor_name[]' type="text"
                                                    autocomplete="off"
                                                    value="<?= @$vendor_quotes[$row_count]['vendor_name'] ? trim($vendor_quotes[$row_count]['vendor_name']) : '' ?>">
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                        <?php
                            $close_status= @$head_array[0]->close_rfq ? $head_array[0]->close_rfq : 0;
                        if($close_status==0){
                            ?>
                        <div class="col-md-3 padding_sm pull-right" id="rfq_closedatadiv">
                            <button onclick="closeQuotation(<?= $rfq_id ?>)" disabled id="closeQuotationBtn"
                                class="btn bg-orange btn-block loadbtn" type="button"><i id="closeQuotationSpin"
                                    class="fa fa-check"></i>
                                Close RFQ</button>
                        </div>
                        <?php }else{
                            ?>
                        <div class="col-md-3 padding_sm pull-right">
                            <button onclick="finialzeQuotation(<?= $rfq_id ?>)" disabled id="finialzeQuotationBtn"
                                class="btn bg-green btn-block loadbtn" type="button"><i id="finialzeQuotationSpin"
                                    class="fa fa-flag-checkered"></i>
                                Finalize RFQ
                            </button>
                        </div>
                        <?php
                        } ?>
                        <div class="col-md-3 padding_sm pull-right">
                            <button onclick="addWindowLoad('getItemVendors',<?= $rfq_id ?>,1)" disabled
                                class="btn btn-danger btn-block loadbtn" type="button"><i class="fa fa-recycle"></i>
                                Reload</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no-padding no-margin">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            <div class="col-md-12 padding_sm">
                                <div id="model_charges_div">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" id="model_data_detail_div" style="margin-top: 10px; display: none">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <strong>
                                    <h5 class="blue" id="model_charges_detail_header" style="text-align: center">
                                    </h5>
                                </strong>
                                <div id="model_charges_detail_div">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/itemVendorQuotation.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
@endsection
