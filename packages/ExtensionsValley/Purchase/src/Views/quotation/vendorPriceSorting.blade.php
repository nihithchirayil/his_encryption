<input type="hidden" id="selected_item" value="<?= $item_id ?>">
<input type="hidden" id="selected_detail_id" value="<?= $detail_id ?>">
<input type="hidden" id="buyernotesID" value="0">
<div class="col-md-12 padding_sm">
    <div class="theadscroll" style="position: relative; height: 220px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="bg-info">
                    <th colspan="6" style="text-align: center"><?= base64_decode($item_desc) ?></th>
                </tr>
                <tr class="table_header_bg">
                    <th style="text-align: center" width="5%"><i class="fa fa-anchor"></i></th>
                    <th class="common_td_rules" width="50%">Vendor name</th>
                    <th class="common_td_rules" width="15%">Unit Price</th>
                    <th style="text-align: center" width="10%"><i class="fa fa-th-list"></i></th>
                    <th style="text-align: center" width="10%"><i class="fa fa-sticky-note-o"></i></th>
                    <th style="text-align: center" width="10%"><i class="fa fa-flag-checkered"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php
        if (count($res) != 0) {
            $i=1;
            foreach ($res as $list) {
                $btn_class="btn-primary";
                if($vendor_id==$list->vendor_id){
                    $btn_class="btn-success";
                }
                ?>
                <tr>
                    <td class="common_td_rules vendorItemData vendor_selection_{{ $list->vendor_id }}">
                        <div class="checkbox checkbox-info inline no-margin">
                            <input class="compare_vendor" type="checkbox" data-detail-id="<?= $list->detail_id ?>"
                                value="<?= $list->vendor_id ?>" id="compare_vendorlist<?= $list->detail_id ?>">
                            <label for="compare_vendorlist<?= $list->detail_id ?>">
                            </label>
                        </div>
                        <input type="hidden" id="selected_vendor_head_id<?= $list->detail_id ?>"
                            value="<?= $list->head_id ?>">
                    </td>
                    <td id="selectvendor_name<?= $list->vendor_id ?>"
                        class="common_td_rules vendorItemData vendor_selection_{{ $list->vendor_id }}">
                        {{ $list->vendor_name }}

                    </td>
                    <td id="selectvendor_price<?= $list->detail_id ?>"
                        class="td_common_numeric_rules vendorItemData vendor_selection_{{ $list->vendor_id }}">
                        {{ $list->unit_cost }}</td>
                    <td style="text-align: center"><button title="Get Vendor Price in Detail"
                            onclick="getVendorQuotedDetalis(<?= $list->detail_id ?>,<?= $detail_id ?>,'<?= $list->vendor_id ?>')"
                            type="button" id="getVendorQuotedDetalisBtn<?= $list->detail_id ?>"
                            class="btn btn-warning btn-block"><i id="getVendorQuotedDetalisSpin<?= $list->detail_id ?>"
                                class="fa fa-th-list"></i></button>
                    </td>
                    <td style="text-align: center"><button title="Add/Edit Vendors Note"
                            onclick="getVendorQuotedNotes(<?= $list->detail_id ?>,'<?= $list->vendor_id ?>',<?= $detail_id ?>)"
                            type="button" id="getVendorNotesBtn<?= $list->detail_id ?>"
                            class="btn btn-info btn-block"><i id="getVendorNotesSpin<?= $list->detail_id ?>"
                                class="fa fa-sticky-note-o"></i></button>
                    </td>
                    <td style="text-align: center">
                        <button title="Finalize Item for Ventor" onclick="editBuyerNotes('<?= $list->vendor_id ?>',0,1)"
                            type="button" id="vendordata_finalizebtn<?= $list->vendor_id ?>"
                            class="btn <?= $btn_class ?> btn-block"><i
                                id="vendordata_finalizespin<?= $list->vendor_id ?>" class="fa fa-flag-checkered"></i>
                        </button>
                    </td>
                </tr>
                <?php
            $i++;
            }
        }else{
            ?>
                <tr>
                    <td colspan="5" style="text-align: center">No Records found</td>
                </tr>
                <?php
        }
        ?>

            </tbody>
        </table>
    </div>
</div>
<?php
        if (count($res) != 0) {
            ?>
<div class="col-md-12 padding_sm text-center" style="margin-top: 10px;">
    <button id="compareVendorBtn" onclick="compare_vendors()" title="Compare Vendors"
        class="btn bg-teal-active btn-block" style="0px 6px" type="button">Compare Vendors <i id="compareVendorSpin"
            class="fa fa-anchor"></i></button>
</div>
<?php } ?>
