<input type="hidden" id="rowid_hidden" value="<?= $row_id ?>">
<input type="hidden" id="itemid_hidden" value="<?= $item_id ?>">
<input type="hidden" id="detail_id_hidden" value="<?= $detail_id ?>">

<div class="col-md-12 padding_sm">
    <input class="form-control td_common_numeric_rules" type="hidden" autocomplete="off" id="item_quantity<?= $row_id ?>"
        readonly="" value="<?= $qty ?>">
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm">
        <div class="mate-input-box">
            <label>Comments</label>
            <input class="form-control" type="text" autocomplete="off" id="item_comments<?= $row_id ?>" readonly=""
                value="<?= $comments ?>">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top:10px">
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label>Free Item</label>
            <input onblur="calculateAmount(<?= $row_id ?>,1)"
                class="form-control td_common_numeric_rules check_selling_price" type="text" autocomplete="off"
                id="free_item<?= $row_id ?>"
                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                value="<?= @$vendor_array[$row_id]['free_item'] ? $vendor_array[$row_id]['free_item'] : 0 ?>">
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label>MRP(Per Uom)</label>
            <input onblur="addItemVendorDetails(<?= $row_id ?>,<?= $item_id ?>,<?= $detail_id ?>)"
                class="form-control td_common_numeric_rules check_selling_price" type="text" autocomplete="off"
                id="itemmrp_peruom<?= $row_id ?>"
                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                value="<?= @$vendor_array[$row_id]['mrp'] ? $vendor_array[$row_id]['mrp'] : 0 ?>">
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label>Rate(Per Uom)</label>
            <input onblur="calculateAmount(<?= $row_id ?>,2)"
                class="form-control td_common_numeric_rules selling_price " type="text" autocomplete="off"
                id="itemsellingprice<?= $row_id ?>"
                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                value="<?= @$vendor_array[$row_id]['item_rate'] ? $vendor_array[$row_id]['item_rate'] : 0 ?>">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top:10px">
    <table class="table no-margin table_sm theadfix_wrapper no-border" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="common_td_rules" width="80%">Description</th>
                <th class="common_td_rules" width="10%">Percentage</th>
                <th class="common_td_rules" width="10%">Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php
                        foreach ($charges_head as $charge) {
                            ?>
            <tr>
                <td class="common_td_rules">{{ $charge->name }}</td>
                <?php
                $val_per = 0;
                $val_amt = 0;
                $read_only = '';
                $onblur_data = '';
                if ($charge->calc_code == 'OTHCH') {
                    $read_only = 'readonly';
                } else {
                    $onblur_data = "onblur='calculateAmount($charge->id,3)'";
                }

                if ($charge->status_code == 'CGST') {
                    $val_per = @$vendor_array[$row_id]['cgst_per'] ? $vendor_array[$row_id]['cgst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['cgst_amt'] ? $vendor_array[$row_id]['cgst_amt'] : 0;
                } elseif ($charge->status_code == 'SGST') {
                    $val_per = @$vendor_array[$row_id]['sgst_per'] ? $vendor_array[$row_id]['sgst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['sgst_amt'] ? $vendor_array[$row_id]['sgst_amt'] : 0;
                } elseif ($charge->status_code == 'IGST') {
                    $val_per = @$vendor_array[$row_id]['igst_per'] ? $vendor_array[$row_id]['igst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['igst_amt'] ? $vendor_array[$row_id]['igst_amt'] : 0;
                } elseif ($charge->status_code == 'CESS') {
                    $val_per = @$vendor_array[$row_id]['cess_per'] ? $vendor_array[$row_id]['cess_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['cess_amt'] ? $vendor_array[$row_id]['cess_amt'] : 0;
                } elseif ($charge->status_code == 'ITEMDISC') {
                    $val_per = @$vendor_array[$row_id]['discount_per'] ? $vendor_array[$row_id]['discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['discount_amt'] ? $vendor_array[$row_id]['discount_amt'] : 0;
                } elseif ($charge->status_code == 'OTHCHG') {
                    $val_per = @$vendor_array[$row_id]['othercharge_per'] ? $vendor_array[$row_id]['othercharge_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['othercharge_amt'] ? $vendor_array[$row_id]['othercharge_amt'] : 0;
                } elseif ($charge->status_code == 'FRCHG') {
                    $val_per = @$vendor_array[$row_id]['flightcharge_per'] ? $vendor_array[$row_id]['flightcharge_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                    $val_amt = @$vendor_array[$row_id]['flightcharge_amt'] ? $vendor_array[$row_id]['flightcharge_amt'] : 0;
                }
                ?>
                <td>
                    <input <?= $read_only ?> <?= $onblur_data ?> data-code="{{ $charge->calc_code }}"
                        data-id="{{ $charge->status_code }}"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        type="text" autocomplete="off"
                        class="percentage_class form-control td_common_numeric_rules check_selling_price tax_class"
                        id="charge_percentage{{ $charge->id }}" name="taxpercentage" value="<?= $val_per ?>"
                        placeholder="{{ $charge->name }}">
                </td>
                <?php
                $read_only = '';
                $onblur_data = '';
                if ($charge->calc_code == 'DA') {
                    $read_only = 'readonly';
                } else {
                    $onblur_data = "onblur='calculateAmount($charge->id,4)'";
                }
                ?>
                <td>
                    <input <?= $read_only ?> <?= $onblur_data ?> data-id="{{ $charge->status_code }}"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        type="text" autocomplete="off" data-code="{{ $charge->calc_code }}"
                        class="amount_class form-control td_common_numeric_rules check_selling_price nummeric_class tax_class"
                        id="charge_amount{{ $charge->id }}" name="taxamount" value="<?= $val_amt ?>"
                        placeholder="{{ $charge->name }}">
                </td>
            </tr>
            <?php
                        }
                    ?>
        </tbody>
    </table>
</div>
