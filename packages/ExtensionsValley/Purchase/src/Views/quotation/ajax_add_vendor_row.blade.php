<?php $row_count = ++$row_count; ?>
<tr style="background: #FFF;" class="vendorrow_class" id="vendorrow_data_{{ $row_count }}">
    <td class='vendorrow_count_class'>{{ $row_count }}</td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="newvendor_{{ $row_count }}"
            onkeyup='searchNewVendor(this.id,event,{{ $row_count }})' class="form-control popinput"
            name="NewvendorData[]" placeholder="Vendor Search">
        <div class='ajaxSearchBox' id="ajaxVendorSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
    </td>
    <td>
        <input class="form-control" name='vendorCode[]' id="vendorcode{{ $row_count }}" autocomplete="off"
            placeholder="Code" type="text" value="">
    </td>
    <td>
        <input class="form-control" name='vendorEmail[]' onblur="updateVendorEmail(<?= $row_count ?>,2)"
            id="vendoremail{{ $row_count }}" autocomplete="off" placeholder="Email" type="text" value="">
        <input type='hidden' name='vendorrow_id_hidden[]' value="{{ $row_count }}"
            id="vendorrow_id_hidden{{ $row_count }}">
        <input type='hidden' name='vendoritem_id_hidden[]' value="" id="vendoritem_id_hidden{{ $row_count }}">
    </td>
    <td>
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removevenderRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
