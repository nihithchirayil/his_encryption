@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="modal fade" id="quotationListModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="quotationListHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px" id="quotationListDiv">

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col" role="main">
        <div class="row padding_sm">
            <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                <thead>
                    <tr class="table_header_bg">
                        <th colspan="11"><?= strtoupper($title) ?>
                        </th>
                    </tr>
                </thead>
            </table>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">

                    <div class="row codfox_container">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 no-padding">
                                <div class="box no-border">
                                    <div class="box-body clearfix">
                                        <form action="{{ route('extensionsvalley.purchase.grnList') }}"
                                            id="requestSearchForm" method="POST">
                                            {!! Form::token() !!}
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label for="">RFQ Number</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" autocomplete="off" class="form-control"
                                                        id="rfq_number" name="rfq_number" value="">
                                                    <input type="hidden" name="rfq_id_hidden" value="" id="rfq_id_hidden">
                                                    <div class="ajaxSearchBox" id="rfq_idAjaxDiv"
                                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-1 padding_sm">
                                                <div class="mate-input-box">
                                                    <label for="">From Date</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control datepicker" name="from_date"
                                                        id="from_date" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <div class="mate-input-box">
                                                    <label for="">To Date</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control datepicker" name="to_date"
                                                        id="to_date" value="">
                                                </div>
                                            </div>

                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label style="margin-top: -4px;">Supply Location</label>
                                                    <div class="clearfix"></div>
                                                    <?php

                                                    ?>
                                                    {!! Form::select('location', $location, $default_location, ['class' => 'form-control select2', 'id' => 'to_location', 'onchange' => 'searchQuotation()']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label style="margin-top: -4px;">Status</label>
                                                    <div class="clearfix"></div>
                                                    {!! Form::select('status', ['' => ' Select Status', '0' => 'RFQ Drafted', '1' => 'RFQ Generated', '3' => 'RFQ Closed'], $searchFields['status'] ?? null, [
    'class' => 'form-control status select2',
    'id' => 'status',
    'onchange' => 'searchQuotation()',
]) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                <a href="{{ Request::url() }}"
                                                    class="btn btn-block light_purple_bg">Clear</a>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                <button type="button" id="searchQuotationBtn" onclick="searchQuotation()"
                                                    class="btn btn-block light_purple_bg"><i id="searchQuotationSpin"
                                                        class="fa fa-search"></i>
                                                    Search</button>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                <div onclick="addWindowLoad('RequestForQuotation','','')"
                                                    class="btn btn-block light_purple_bg"><i class="fa fa-plus"></i>
                                                    Create RFQ
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="searchDataDiv">

                </div>
            </div>
        </div>

    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/requestforquotationlist.js') }}"></script>
@endsection
