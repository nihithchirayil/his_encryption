<div class="box-body clearfix">
<table class='theadfix_wrapper table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th colspan="2">Patient Name:</th>
                <th colspan="3"><?=@$res[0]->patient_name ? $res[0]->patient_name :'-'?></th>
                <th colspan='2'>UHID</th>
                <th colspan="3"><?=@$res[0]->uhid ? $res[0]->uhid :'-'?></th>
            </tr>
        </thead>
    </table>
<div class="theadscroll" style="position: relative; height: 380px;">
<table id="result_data_table" class='theadfix_wrapper table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th width="30%">Bill Tag</th>
                <th width="20%">Bill No</th>
                <th width="15%">Bill Date</th>
                <th width="10%">Bill Amount</th>
                <th width="10%">Net Amount</th>
                <th width="10%">Paid Status</th>
                <th width="5%">
                <div class="checkbox checkbox-warning inline no-margin">
                        <input onclick="checkallvisit()" id="checkallvisit" type="checkbox">
                        <label class="text-blue" for="checkallvisit"></label>
                </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(count($res)!=0){
            foreach ($res as $item){
                $paid_status='Unpaid';
                if($item->paid_status=='1'){
                    $paid_status='Paid';
                }
            ?>    
                <tr>
                        <td class="common_td_rules">{{$item->bill_type}}</td>
                        <td class="common_td_rules">{{$item->bill_no}}</td>
                        <td class="common_td_rules">{{$item->bill_date}}</td>
                        <td class="td_common_numeric_rules">{{$item->bill_amount}}</td>
                        <td class="td_common_numeric_rules">{{$item->net_amount_wo_roundoff}}</td>
                        <td class="common_td_rules">{{$paid_status}}</td>
                        <?php
                             if ($item->paid_status=='0') {
                                 ?>
                                 <td style="text-align:center">
                                        <div class="checkbox checkbox-success inline no-margin">
                                                <input id="changevisit_id<?=$item->head_id?>" type="checkbox" class='visit_class' value='<?=$item->head_id.'::::'.$visit_id?>'>
                                                    <label class="text-blue" for="changevisit_id<?=$item->head_id?>">
                                                    </label>
                                        </div>
                                </td>
                                 <?php
                             }else{
                                 ?>
                                   <td style="text-align:center">-</td>
                                 <?php
                             }
                        ?>
                       
                    </tr>
            <?php
                }
            }else{
                ?>
                <tr>
                    <td style="text-align: center;" colspan="7">No Records found</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

</div>
<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <button onclick="changePatientVisit(<?=$visit_id?>)" id="changePatientVisitBtn" class="btn btn-block btn-success">Save <i id="changePatientVisitSpin" class="fa fa-save"></i></button>
    </div>
</div>
 </div>