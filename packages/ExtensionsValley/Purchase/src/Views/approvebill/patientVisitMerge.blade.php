@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
.ajaxSearchBox {
    display: none;
    text-align: left;
    list-style: none;
    cursor: pointer;
    max-height: 200px;
    margin: 0px 0px 0px 0px;
    overflow-y: auto;
    width: 100%;
    z-index: 9999px;
    position: absolute;
    background: #ffffff;
    border-radius: 3px;
    border: 1px solid rgba(0, 0, 0, 0.3);

}

.search_box_bottom {
    bottom: -30px;
}

.ajax_search_box_bottomcls .ajaxSearchBox {
    bottom: 3px;
    height: 200px !important;
}

.ajax_search_box_bottomcls_reduce_box {
    height: 200px !important;
}

table td {
    position: relative !important;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col">
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm"
            style="text-align: center; font-weight: 600; color: #26b99a!important; margin-top: 1px;">{{ $title }}
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm">
        <div class="col-md-5 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 150px;">
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="">From Patient Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="from_patient_id" name="from_patient_name" value="">
                            <div id="from_patient_idAjaxDiv" class="ajaxSearchBox" style="margin-top: 15px;"></div>
                            <input type="hidden" name="from_patient_id_hidden" id="from_patient_id_hidden" value="">
                            <input type="hidden" name="from_patient_id_uhid_hidden" id="from_patient_id_uhid_hidden"
                                value="">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-8 padding_sm">
                            <label id="from_patient_id_label_name" style="font-weight: 700;color: #238823;">Patient Name
                                :</label><br>
                            <label id="from_patient_id_label_uhid" style="font-weight: 700;color: #238823;">Patient UHID
                                :</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 150px;">
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="">To Patient Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control hidden_search" id="to_patient_id"
                                name="to_patient_name" value="">
                            <div id="to_patient_idAjaxDiv" class="ajaxSearchBox" style="margin-top: 15px;"></div>
                            <input type="hidden" name="to_patient_id_hidden" value="" id="to_patient_id_hidden">
                            <input type="hidden" name="to_patient_id_uhid_hidden" value=""
                                id="to_patient_id_uhid_hidden">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-8 padding_sm">
                            <label id="to_patient_id_label_name" style="font-weight: 700;color: #238823;">Patient Name
                                :</label><br>
                            <label id="to_patient_id_label_uhid" style="font-weight: 700;color: #238823;">Patient UHID
                                :</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-2 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 150px;">
                    <div class="col-md-12 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <button id="mergedata_btn" type="button" class="btn btn-block light_purple_bg"
                            onclick="mergePatientVisit()"><i id="mergedata_spin" class="fa fa-exchange"></i>
                            Merge</button>
                        <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                            Clear</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top:1%;">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                    id="search_patient_txt" name="search_patient_txt" value="">
                                <div id="search_patient_txtAjaxDiv" class="ajaxSearchBox" style="margin-top: 15px;">
                                </div>
                                <input type="hidden" name="search_patient_txt_hidden" value=""
                                    id="search_patient_txt_hidden">
                                <input type="hidden" name="search_patient_txt_uhid_hidden" value=""
                                    id="search_patient_txt_uhid_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">From Date</label>
                                <input type="text" data-attr="date" autocomplete="off" autofocus=""
                                    value="<?= $current_date ?>" class="form-control datepicker filters"
                                    placeholder="YYYY-MM-DD" id="search_from_date">
                            </div>
                        </div>
                        <div class="col-md-2 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">To Date</label>
                                <input type="text" data-attr="date" autocomplete="off" autofocus=""
                                    value="<?= $current_date ?>" class="form-control datepicker filters"
                                    placeholder="YYYY-MM-DD" id="search_to_date">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                    class="fa fa-times"></i>Clear</a>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg"
                                onclick="search_patient_merge_details()"><i id="searchdataspin"
                                    class="fa fa-search"></i>
                                Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row box no-border no-margin">
                <div class="col-md-12 padding_sm theadscroll" id='merge_patientsDiv'
                    style="position: relative; height: 65vh;">
                   
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    search_patient_merge_details();
    $('.hidden_search').keyup(function(event) {
        var input_id = '';
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        input_id = $(this).attr('id');
        var current;

        if (event.keyCode == 13) {
            ajaxlistenter(input_id + "AjaxDiv");
            return false;
        } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            if (search_key == "") {
                $("#" + input_id + "AjaxDiv").html("").hide();
                $('#' + input_id + '_hidden').val('');
                $('#' + input_id + '_uhid_hidden').val('');
                $('#' + input_id + '_label_name').html('Patient Name :');
                $('#' + input_id + '_label_uhid').html('Patient UHID :');
            } else {
                var url = '';
                var param = {
                    patient_name_search: search_key,
                    search_key_id: input_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#" + input_id + "AjaxDiv").html(
                            '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        if (html == 0) {
                            $("#" + input_id + "AjaxDiv").html("No results found!").show();
                            $("#" + input_id + "AjaxDiv").find('li').first().addClass(
                                'liHover');
                        } else {
                            $("#" + input_id + "AjaxDiv").html(html).show();
                            $("#" + input_id + "AjaxDiv").find('li').first().addClass(
                                'liHover');
                        }
                    },
                    complete: function() {},
                    error: function() {
                        Command: toastr["error"]("Network Error!");
                        return;
                    }

                });
            }
        } else {
            ajax_list_key_down(input_id + 'AjaxDiv', event);
        }
    });

    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY',
    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
});
</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
function fillPatientValues(uhid, patient_name, patient_id, input_id) {
    $("#" + input_id + "AjaxDiv").html("").hide();
    $('#' + input_id + '_hidden').val(patient_id);
    $('#' + input_id + '_uhid_hidden').val(uhid);
    $('#' + input_id + '_label_name').html('Patient Name :' + patient_name);
    $('#' + input_id + '_label_uhid').html('Patient UHID :' + uhid);
    $('#' + input_id).val(patient_name);
}

function mergePatientVisit() {
    var from_patient_id = $('#from_patient_id_hidden').val();
    var from_patient_name = $('#from_patient_id').val();
    var from_patient_uhid = $('#from_patient_id_uhid_hidden').val();
    var to_patient_id = $('#to_patient_id_hidden').val();
    var to_patient_name = $('#to_patient_id').val();
    var to_patient_uhid = $('#to_patient_id_uhid_hidden').val();

    if (from_patient_id == '' || from_patient_uhid == '') {
        Command: toastr["warning"]("Please select From patient");
        return;
    }
    if (to_patient_id == '' || to_patient_uhid == '') {
        Command: toastr["warning"]("Please select To patient");
        return;
    }
    if (from_patient_id == to_patient_id) {
        Command: toastr["warning"]("Please select different patient");
        return;
    }

    bootbox.confirm({
        message: "Please confirm if you want to merge <strong>" + from_patient_name + " </strong> (" +
            from_patient_uhid + ") details with <strong>" + to_patient_name + " </strong> (" + to_patient_uhid +
            ") 's information. ",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result) {
                var url = '<?= route('extensionsvalley.purchase.mergePatientVisit') ?>';
                var param = {
                    from_patient_id: from_patient_id,
                    from_patient_uhid: from_patient_uhid,
                    to_patient_uhid: to_patient_uhid,
                    to_patient_id: to_patient_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#mergedata_btn').find('i').removeClass('fa-exchange').addClass(
                            'fa-spinner fa-spin');
                    },
                    success: function(html) {
                        if (html == 1) {
                            Command: toastr["success"]("Merged Successfully");
                            search_patient_merge_details();
                        }
                    },
                    complete: function() {
                        $('#mergedata_btn').find('i').removeClass('fa-spinner fa-spin')
                            .addClass('fa-exchange');
                        $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                        });
                    },
                    error: function() {
                        Command: toastr["error"]("Network Error!");
                        return;
                    }

                });
            }
        }
    });
}

function search_patient_merge_details() {
    var search_patient_txt_uhid = $('#search_patient_txt_uhid_hidden').val();
    var search_from_date = $('#search_from_date').val();
    var search_to_date = $('#search_to_date').val();
    var url = '<?= route('extensionsvalley.purchase.mergePatientDetailsList') ?>';
    var param = {
        search_patient_txt_uhid: search_patient_txt_uhid,
        search_from_date: search_from_date,
        search_to_date: search_to_date,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $('#searchdatabtn').find('i').removeClass('fa-search').addClass(
                'fa-spinner fa-spin');
        },
        success: function(html) {
            $('#merge_patientsDiv').html(html);           
        },
        complete: function() {
            $('#searchdatabtn').find('i').removeClass('fa-spinner fa-spin')
                            .addClass('fa-search');  
            setTimeout(function () {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);          
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            return;
        }

    });
}
</script>
@endsection