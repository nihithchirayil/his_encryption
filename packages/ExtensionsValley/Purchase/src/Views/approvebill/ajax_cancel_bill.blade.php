<?php
/* For Patient details search */
if (isset($patientNameSearchDetailsOt)) {

if(!empty($patientNameSearchDetailsOt)){
    foreach ($patientNameSearchDetailsOt as $patient) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px;color:black; "
    onclick='fillOtPatientValues("{{ htmlentities($patient->id) }}","{{ htmlentities($patient->patient_name) }}","{{ htmlentities($patient->current_visit_id) }}")'>
    {{ htmlentities($patient->patient_name) }} [ID:{{ htmlentities($patient->uhid) }}]
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($patientNameSearch)) {

if(!empty($patientNameSearch)){
    foreach ($patientNameSearch as $patient) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillPatientValues("{{ htmlentities($patient->uhid) }}","{{ htmlentities($patient->patient_name) }}")'>
    {{ htmlentities($patient->patient_name) }} [ID:{{ htmlentities($patient->uhid) }}]
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($patientNameDetails)) {
    $from_type=@$from_type ? $from_type : 0;
    if(!empty($patientNameDetails)){
        foreach ($patientNameDetails as $patient) {
            ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillPatientValues("{{ htmlentities($patient->uhid) }}","{{ htmlentities($patient->patient_name) }}","{{ $patient->patient_id }}","{{ $from_type }}")'>
    {{ htmlentities($patient->patient_name) }} [ID:{{ htmlentities($patient->uhid) }}]
</li>

<?php  }
    } else {
        echo 'No Results Found';
    }
}else if (isset($billNoDetails)) {

    if(!empty($billNoDetails)){
        foreach ($billNoDetails as $bill) {
            ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillBillNo("{{ htmlentities($bill->pat_id) }}","{{ htmlentities($bill->bill_id) }}","{{ htmlentities($bill->bill_no) }}","{{ htmlentities($bill->uhid) }}","{{ htmlentities($bill->patient_name) }}")'>
    {{ htmlentities($bill->bill_no) . '||' . htmlentities($bill->name) . '||' . htmlentities($bill->patient_name) }}
</li>

<?php  }
    } else {
        echo 'No Results Found';
    }
}else if (isset($insBillNO)) {

if(!empty($insBillNO)){
    foreach ($insBillNO as $bill) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillBillNo("{{ htmlentities($bill->bill_id) }}","{{ htmlentities($bill->bill_no) }}","{{ htmlentities($bill->bill_tag) }}")'>
    {{ htmlentities($bill->bill_no) . '||' . htmlentities($bill->name) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($bill_head_discharge)) {

if(!empty($bill_head_discharge)){
    foreach ($bill_head_discharge as $bill) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillFullBillNo("{{ htmlentities($bill->bill_id) }}","{{ htmlentities($bill->bill_no) }}","{{ htmlentities($bill->bill_type) }}")'>
    {{ htmlentities($bill->bill_no) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($item_description)) {

if(!empty($item_description)){
    foreach ($item_description as $item) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillitem_desc("{{ htmlentities($item->item_code) }}","{{ htmlentities($item->item_desc) }}")'>
    {{ htmlentities($item->item_desc) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($RequestNo_search)) {

if(!empty($RequestNo_search)){
    foreach ($RequestNo_search as $item) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillitem_desc("{{ htmlentities($item->id) }}","{{ htmlentities($item->request_no) }}")'>
    {{ htmlentities($item->request_no) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
} else if (isset($transaction_no)) {

if(!empty($transaction_no)){
    foreach ($transaction_no as $item) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='filltransction_no("{{ htmlentities($item->id) }}","{{ htmlentities($item->transaction_no) }}")'>
    {{ htmlentities($item->transaction_no) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($cashier_name)) {

if(!empty($cashier_name)){
    foreach ($cashier_name as $users) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='filterCashierName("{{ htmlentities($users->id) }}","{{ htmlentities(strtoupper($users->name)) }}")'>
    {{ htmlentities(strtoupper($users->name)) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($rfq_no)) {

if(!empty($rfq_no)){
    foreach ($rfq_no as $item) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillrfq_no("{{ htmlentities($item->id) }}","{{ htmlentities($item->rfq_no) }}")'>
    {{ htmlentities($item->rfq_no) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($contract_no)) {

if(!empty($contract_no)){
    foreach ($contract_no as $item) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillcontract_no("{{ htmlentities($item->id) }}","{{ htmlentities($item->contract_no) }}")'>
    {{ htmlentities($item->contract_no) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($quotation_no)) {

if(!empty($quotation_no)){
    foreach ($quotation_no as $each) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillquotation_no("{{ htmlentities($each->id) }}","{{ htmlentities($each->quotation_no) }}")'>
    {{ htmlentities($each->quotation_no) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($patientLabResult)) {

if(!empty($patientLabResult)){
    foreach ($patientLabResult as $each) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillLabPatientDetails("{{ htmlentities($each->id) }}","{{ htmlentities($each->uhid) }}","{{ htmlentities($each->patient_name) }}","{{ htmlentities($each->phone) }}")'>
    {{ htmlentities($each->patient_name) . '(' . htmlentities($each->uhid) . ')' }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($doctor_search_list)) {

if(count($doctor_search_list)!=0){
    foreach ($doctor_search_list as $each) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetalis("{{ htmlentities($each->id) }}","{{ htmlentities(trim($each->list_name)) }}","{{ htmlentities($list_type) }}")'>
    {{ htmlentities($each->list_name) }}
</li>

<?php  }
} else {

    if($list_type=='speciality_name_add'){
    echo "<button id='addNewDoctorSpecialitybtn' class='btn btn-success btn-block' onclick='addNewSpeciality();'>  <i id='addNewDoctorSpecialitySpin' class='fa fa-plus'></i> Add new speciality  </button>";
}else{
    echo 'No Results Found';
}

}
}else if (isset($service_search_list)) {

if(!empty($service_search_list)){
    foreach ($service_search_list as $each) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillServiceDetails("{{ htmlentities($each->id) }}","{{ htmlentities($each->service_desc) }}");'>
    {{ htmlentities($each->service_desc) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($cross_service_search_list)) {

if(!empty($cross_service_search_list)){
    foreach ($cross_service_search_list as $each) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillCrossServiceDetails("{{ htmlentities($each->id) }}","{{ htmlentities($each->service_desc) }}");'>
    {{ htmlentities($each->service_desc) }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
} else if (isset($patientAllergyMedicine)) {

    if(!empty($patientAllergyMedicine)){
        foreach ($patientAllergyMedicine as $each) {
            ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillMedicineDetails("{{ htmlentities($each->item_id) }}","{{ htmlentities($each->name) }}", {{$row_id}});'>
    {{ htmlentities($each->name) }}
</li>

<?php  }
    } else {
        echo 'No Results Found';
    }
} else if (isset($patientOtherAllergy)) {

    if(!empty($patientOtherAllergy)){
        foreach ($patientOtherAllergy as $each) {
            ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillOtherAllergy("{{ htmlentities($each->id) }}","{{ htmlentities($each->allergy_name) }}", {{$row_id}});'>
    {{ htmlentities($each->allergy_name) }}
</li>

<?php  }
    } else {
        echo 'No Results Found';
    }
}else if (isset($patientIpNumber)) {

if(!empty($patientIpNumber)){
    foreach ($patientIpNumber as $each) {
        ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillLabPatientDetails("{{ htmlentities($each->id) }}","{{ htmlentities($each->uhid) }}","{{ htmlentities($each->patient_name) }}","{{ htmlentities($each->phone) }}")'>
    {{ htmlentities($each->patient_name) . '(' . htmlentities($each->id) . ')' }}
</li>

<?php  }
} else {
    echo 'No Results Found';
}
}else if (isset($patientNameMergeDetails)) {

    if(!empty($patientNameMergeDetails)){
        foreach ($patientNameMergeDetails as $patient) {
            ?>

<li class="list_hover" style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillPatientValues("{{ htmlentities($patient->uhid) }}","{{ htmlentities($patient->patient_name) }}", "{{ htmlentities($patient->patient_id) }}", "{{ $search_key_id }}")'>
    {{ htmlentities($patient->patient_name) }} [ID:{{ htmlentities($patient->uhid) }}]
</li>

<?php  }
    } else {
        echo 'No Results Found';
    }
}

?>
