<div class="theadscroll" style="max-height: 500px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="20%">Patient Name</th>
                <th width="15%">UHID</th>
                <th width="20%">Bill Tag</th>
                <th width="10%">Bill No</th>
                <th width="10%">Bill Date</th>
                <th width="10%">Bill Amount</th>
                <th width="10%"><i class="fa fa-save"></i></th>
                <th width="5%"><i class="fa fa-eye"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php
                     if(count($patientBills)!=0){
                         foreach($patientBills as $each){
                             ?>
            <tr>
                <td class="common_td_rules ">
                    <?= $each->patient_name ?></td>
                <td class="common_td_rules "><?= $each->uhid ?></td>
                <td class="common_td_rules "><?= $each->name ?></td>
                <td class="common_td_rules "><?= $each->bill_no ?></td>
                <td class="common_td_rules "><?= date('M-d-Y', strtotime($each->bill_date)) ?></td>
                <td class="td_common_numeric_rules ">
                    <?= $each->net_amount ?></td>
                @if ($lab_status[$each->id] != '0')
                    <td class="common_td_rules red">Generated</td>
                @elseif ($each->cancelled_status != '3')
                    <td> <button class="btn btn-primary" type="button" id="approve_reject_btn<?= $each->id ?>"
                            onclick="cancel_approve_revertbill(<?= $each->id ?>,1,2,'{{ $each->bill_type }}')"> Approve
                            Bill <i id="approve_reject_spin<?= $each->id ?>" class="fa fa-save"></i></button></td>
                @elseif ($each->cancelled_status == '3' &&
                    $each->bill_tag != 'LAB' &&
                    $each->bill_tag != 'PH' &&
                    $each->bill_tag != 'CONS' &&
                    $each->bill_tag != 'WDB' &&
                    $each->bill_tag != 'AD')
                    <td> <button class="btn btn-warning" type="button" id="approve_reject_btn<?= $each->id ?>"
                            onclick="cancel_approve_revertbill(<?= $each->id ?>,2,2,'{{ $each->bill_type }}')"> Revert
                            Bill <i id="approve_reject_spin<?= $each->id ?>" class="fa fa-save"></i></button></td>
                @else
                    <td class="common_td_rules red">--</td>
                @endif
                <td><button class="btn btn-info" id="fetchBillBtn<?= $each->id ?>" type="button"
                        onclick="fetchBill(<?= $each->id ?>,'<?= $each->bill_tag ?>')"> View <i
                            id="fetchBillSpin<?= $each->id ?>" class="fa fa-eye"></i></button></td>
            </tr>
            <?php
                         }
                     }else{
                         ?>
            <tr>
                <th colspan='9' style='text-align: center'>No Result Found</th>
            </tr>
            <?php
                     }
                    ?>
        </tbody>
    </table>
</div>
