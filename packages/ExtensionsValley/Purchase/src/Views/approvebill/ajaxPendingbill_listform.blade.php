<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table id="result_data_table"
            class="table theadfix_wrapper table_sm table-condensed table-col-bordered center_align"
            style="font-size: 12px;">
            <thead>
                <tr class="table_header_bg">
                    <th>Bill No</th>
                    <th>Bill Tag</th>
                    <th>Patient Name</th>
                    <th>Patient ID</th>
                    <th>Doctor Name</th>
                    <th>Net Amount</th>
                    <th>Bill Date</th>
                    <th>Paid Status</th>
                    <th>Status</th>
                    <th>Reason</th>
                    <th>Approve</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($res)!=0){
                    $bill_id=0;
            foreach ($res as $item){
                $class="";
                $title="";
                if(($item->lab_detailid) && ($item->lab_detailid!='0')){
                    $class="red";
                    $title="Sample Number Already Generated";
                }
                if($bill_id!=$item->id){
                    $bill_id=$item->id;

            ?>

                <tr title="<?= $title ?>" class="<?= $class ?>" id="approveBillRow<?= $item->id ?>">
                    <td class="common_td_rules">{{ $item->bill_no }}</td>
                    <td class="common_td_rules">{{ $item->bill_type }}</td>
                    <td class="common_td_rules">{{ $item->patient_name }}</td>
                    <td class="common_td_rules">{{ $item->uhid }}</td>
                    <td class="common_td_rules">{{ $item->doctor_name }}</td>
                    <td class="td_common_numeric_rules">{{ $item->net_amount_wo_roundoff }}</td>
                    <td class="common_td_rules">{{ $item->bill_date }}</td>
                    <td class="common_td_rules">
                        <?php if ($item->paid_status == 1) {
                            $paid_status_name = 'Paid';
                        } else {
                            $paid_status_name = 'Not Paid';
                        } ?>
                        {{ $paid_status_name }}</td>
                    <td class="common_td_rules">
                        <?php if ($item->cancelled_status == 1) {
                            $cancelled_status_name = 'Cancelled';
                        } else {
                            $cancelled_status_name = 'Not Cancelled';
                        } ?>
                        {{ $cancelled_status_name }}</td>
                    <td class="common_td_rules"> {{ $item->pending_reason }}</td>
                    <?php
                        if(!$class){
                            ?>
                    <td><button type="button" id="approveBillBtn<?= $item->id ?>" class="btn btn-success"
                            onclick="approveBill(<?= $item->id ?>);"><i id="approveBillSpin<?= $item->id ?>"
                                class="fa fa-save"></i></button>
                    </td>
                    <?php
                        }else{
                            ?>
                    <td><button type="button" id="labGeneratedBillBtn<?= $item->id ?>" class="btn btn-warning"
                            onclick="labGeneratedBill(<?= $item->id ?>);"><i id="labGeneratedBillSpin<?= $item->id ?>"
                                class="fa fa-list"></i></button>
                    </td>
                    <?php
                        }
                    ?>

                </tr>
                <?php }
            }

            }else{
                ?>
                <tr>
                    <td colspan="11" class="location_code">No Records found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>

    </div>
</div>
