@extends('Dashboard::dashboard.dashboard')
@section('content-header')

@include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css')}}" rel="stylesheet">
<link href="{{asset('packages/extensionsvalley/purchase/default/css/report.css')}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">


@endsection
@section('content-area')


<div class="modal" tabindex="-1" role="dialog" id="modal_cashReciveEdit">
    <div class="modal-dialog" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header box_header">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h3 class="modal-title">Receive Bill Edit</h3>
            </div>
            <div class="modal-body" style="min-height:470px;" id="modal_cashReciveEditBody">

            </div>
        </div>
    </div>
</div>


<div class="right_col" role="main">
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
    <div class="col-md-12 padding_sm">
        <div id="filter_area" class="col-md-12" style="padding-left:0px !important;width: 101.75%;margin-left: -10px;">
            <div class="box no-border no-margin">
                <div class="box-body" style="padding-bottom:15px;">
                    <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                        <thead>
                            <tr class="table_header_bg">
                                <th colspan="11"> {{$title}}
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="row">

                        <form action="{{route('extensionsvalley.purchase.listPendingBills')}}" id="requestSearchForm"
                            method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 no_padding">
                                <div class="mate-input-box">
                                    <label for="">Patient Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" id="patient_name"
                                        name="patient_name" value="">
                                    <div id="patient_nameAjaxDiv" style="margin-top: -15px;" class="ajaxSearchBox"></div>
                                    <input type="hidden" name="patient_name_hidden" value="" id="patient_name_hidden">                                   
                                </div>
                            </div>
                            <div class="col-md-3 no_padding">
                                <div class="mate-input-box">
                                    <label for="">Bill Number</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" id="bill_number"
                                        name="bill_number" value="">
                                    <div id="bill_numberAjaxDiv" style="margin-top: -15px;" class="ajaxSearchBox"></div>
                                    <input type="hidden" name="bill_number_hidden" value="" id="bill_number_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 no_padding">
                                <div class="mate-input-box">
                                    <label>From Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" name="from_date"
                                        value="{{ date('M-d-y') }}" class="form-control datepicker filters reset"
                                        placeholder="YYYY-MM-DD" id="from_date">
                                </div>
                            </div>
                            <div class="col-md-2 no_padding">
                                <div class="mate-input-box">
                                    <label>To Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" name="to_date"
                                        value="{{ date('M-d-y') }}" class="form-control datepicker filters reset"
                                        placeholder="YYYY-MM-DD" id="to_date">
                                </div>
                            </div>

                            <div class="col-md-1 no_padding">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg"
                                    onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 no_padding">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                    class="fa fa-times"></i>Clear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin" id='patient_paidBillList'>

            </div>
        </div>
    </div>

</div>

@stop
@section('javascript_extra')
<script src="{{asset('packages/extensionsvalley/emr/js/bill_paymentmode_edit.js')}}"></script>
<script src="{{asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js')}}"></script>
{!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
@endsection