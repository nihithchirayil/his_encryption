<div class="box-body clearfix">
<table class='theadfix_wrapper table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th colspan="2">Patient Name:</th>
                <th colspan="3"><?=@$res[0]->patient_name ? $res[0]->patient_name :'-'?></th>
                <th colspan='2'>UHID</th>
                <th colspan="3"><?=@$res[0]->uhid ? $res[0]->uhid :'-'?></th>
            </tr>
        </thead>
    </table>
<div class="theadscroll" style="position: relative; height: 380px;">
<table id="result_data_table" class='theadfix_wrapper table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th width="30%">Bill Tag</th>
                <th width="20%">Bill No</th>
                <th width="15%">Bill Date</th>
                <th width="10%">Bill Amount</th>
                <th width="10%">Net Amount</th>
                <th width="10%">Paid Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(count($res)!=0){
            foreach ($res as $item){
                $paid_status='Unpaid';
                if($item->paid_status=='1'){
                    $paid_status='Paid';
                }
              ?>    
                <tr style="cursor: pointer;" onclick="getPatientPendingBills(<?=$item->visit_id?>)">
                        <td class="common_td_rules">{{$item->bill_type}}</td>
                        <td class="common_td_rules">{{$item->bill_no}}</td>
                        <td class="common_td_rules">{{$item->bill_date}}</td>
                        <td class="td_common_numeric_rules">{{$item->bill_amount}}</td>
                        <td class="td_common_numeric_rules">{{$item->net_amount}}</td>
                        <td class="common_td_rules">{{$paid_status}}</td>
                    </tr>
            <?php
                }
            }else{
                ?>
                <tr>
                    <td style="text-align: center;" colspan="6">No Records found</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

</div>

 </div>