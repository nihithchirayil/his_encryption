<div class="col-md-2 padding_sm">
    <div class="mate-input-box">
       <label class="filter_label" style="width: 100%;">TPA</label>
        <div class="clearfix"></div>
        <select id="companypricing" class="form-control select2">
            <option value="">All</option>
            <?php
                foreach ($company_pricing as $each){
                    ?>
                    <option value="<?=$each->pricing_id?>"><?=$each->pricing_name?></option>
                    <?php
                }
            ?>
        </select>
    </div>
</div>
<div class="col-md-2 padding_sm">
    <div class="mate-input-box">
     <label class="filter_label" style="width: 100%;">Mode Of Payment</label>
        <div class="clearfix"></div>
        <select id="companytype" class="form-control select2">
            <option value="">All</option>
            <?php
                foreach ($data_array as $each){
                    ?>
                    <option value="<?=$each?>"><?=$each?></option>
                    <?php
                }
            ?>
        </select>
    </div>
</div>