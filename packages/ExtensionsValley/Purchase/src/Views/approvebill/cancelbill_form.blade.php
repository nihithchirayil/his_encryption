@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')



<!-- Cancel request Modal -->
<div id="cancel_bill_req_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width:80%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cancel Requested Bill</h4>
            </div>
            <div class="modal-body" style="height: 80vh; min-height:80vh;">
                <div class="col-md-12">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #FFF; border: 2px solid #01987a;border-radius: 15px;min-height: 80px;">
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Bill No.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control hidden_search"
                                            id="req_bill_no" name="req_bill_no" value="">
                                        <div id="req_bill_no_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top:14px;z-index: 99999;max-height: 300px !important"></div>
                                        <input type="hidden" name="req_bill_no_hidden" value="" id="req_bill_no_hidden">
                                        <input type="hidden" name="req_bill_id_hidden" value="" id="req_bill_id_hidden">
                                    </div>
                                </div>

                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Patient Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control hidden_search"
                                            id="req_patient_name" name="req_patient_name" value="">
                                        <div id="req_patient_name_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top:14px;z-index: 99999;max-height: 300px !important"></div>
                                        <input type="hidden" name="req_patient_name_hidden" value=""
                                            id="req_patient_name_hidden">
                                        <input type="hidden" name="req_patient_uhid_hidden" value=""
                                            id="req_patient_uhid_hidden">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm"
                                    style="margin-top: 20px; padding-left: 9px !important; display:none;">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" id="req_cancelled_bills_only" name="req_cancelled_bills_only"
                                            value="1">
                                        <label style="padding-left: 2px;" for="req_cancelled_bills_only">
                                            Cancelled Bills Only</label><br>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm"
                                    style="margin-top: 20px; padding-left: 9px !important;">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" id="ReqCancelCreditBills">
                                        <label style="padding-left: 2px;" for="ReqCancelCreditBills">
                                            Cancel Credit Bills</label><br>
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a class="btn btn-block btn-warning" onclick="resetReqcancell()"><i class="fa fa-times"></i>Clear</a>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button id="search_cancelReqbillbtn" type="button" class="btn btn-block btn-primary"
                                        onclick="searchCancelReqBill()"><i id="search_cancelReqbillspin"
                                            class="fa fa-search"></i>
                                        Search</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="box no-border no-margin" style="margin-top: 15px !important;"
                        id="cancel_bill_req_modal_data">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Cancel request Modal  -->
<div id="cancel_bill_view_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View</h4>
            </div>
            <div class="modal-body" style="height:520px;">
                <div class="col-md-12" id="cancel_bill_view_data">

                </div>
            </div>
        </div>
    </div>
</div>

<!-- page content -->

<!-- page content -->
<div class="right_col">
    <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
    <input type="hidden" id="current_date" value="<?= date('M-d-Y') ?>">
    <div class="row">
        <div class="col-md-1 padding_sm pull-right">
            {{ $title }}
        </div>
    </div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #FFF; border: 2px solid #01987a;border-radius: 15px;min-height: 80px;">
                        <div class="col-md-3 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Bill No.</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="bill_no" name="bill_no"
                                    value="">
                                <div id="bill_no_AjaxDiv" class="ajaxSearchBox"
                                    style="margin-top:14px;z-index: 99999;max-height: 300px !important"></div>
                                <input type="hidden" name="bill_no_hidden" value="" id="bill_no_hidden">
                                <input type="hidden" name="bill_id_hidden" value="" id="bill_id_hidden">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                    name="patient_name" value="">
                                <div id="patient_idAjaxDiv" class="ajaxSearchBox"
                                    style="margin-top:14px;z-index: 99999;max-height: 300px !important"></div>
                                <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                <input type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 20px; padding-left: 9px !important;">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" id="cancelled_bills_only" name="cancelled_bills_only" value="1">
                                <label style="padding-left: 2px;" for="cancelled_bills_only">
                                    Cancelled Bills Only</label><br>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 20px; padding-left: 9px !important;">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" id="CancelCreditBills">
                                <label style="padding-left: 2px;" for="CancelCreditBills">
                                    Cancel Credit Bills</label><br>
                            </div>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                    class="fa fa-times"></i>Clear</a>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block btn-primary"
                                onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="search_reqcancelbtn" type="button" class="btn btn-block btn-success"
                                onclick="searchCancelReqBill()" title='Cancel Request Bill'><i class="fa fa-list"></i>
                                View</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="margin-top: 15px !important;">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #FFF; border: 2px solid #01987a ;border-radius: 15px; min-height: 500px;">
                    <div class="col-md-12 padding_sm" style="margin-top: 10px">
                        <div id="cancelPatientDetails">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 20px">
                        <div id="cancelBillDetails">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/js/cancelManagerBills.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
{!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
{!! Html::script(
'packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js',
) !!}
{!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
@endsection