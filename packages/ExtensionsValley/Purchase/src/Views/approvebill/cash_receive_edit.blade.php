<style>
.ajaxSearchBox {
    display: none;
    width: 250px !important;
    height: 400px !important;
    overflow: hidden;
    padding-top: 0px !important;
    margin-top: 15px !important;
}

.liHover {
    background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
}

.ajaxSearchBox {
    z-index: 9999 !important;
}

.ajaxSearchBox>li {
    font-size: 13px !important;
    font-family: Arial !important;
}

.payment_mode {
    width: 175px !important;
}

.amount_table {
    border-spacing: 10px !important;
    border: 1px solid white !important;
    border-collapse: collapse !important;
}

.amount_table>tbody>tr {
    height: 30px !important;
}

.payment_mode :hover {
    background-color: #02cf99;
    color: white;
}

.blink_me {
    animation: blinker 1s linear infinite;
}

@keyframes blinker {
    50% {
        opacity: 0;
    }
}

@media print {
    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }


    .theadscroll {
        max-height: none;
        overflow: visible;
    }

    .header_bg {
        background-color: #08898d;
        color: white;
    }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f2fef5;
    }

    #resultDataTable {
        border: 1px solid #cedfe9 !important;
        border-collapse: collapse !important;
    }

    td {
        padding-left: 5px !important;
    }

}
</style>

<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}" />
<input type="hidden" id="c_token" value="{{csrf_token()}}" />
<input type="hidden" id="is_discharge" value="{!!$is_discharge!!}" />
<input type="hidden" id="payment_mode_detail" value="{!!$payment_mode_detail_array!!}" />
<input type="hidden" id="cash_collection_data" value="{!!$cash_collection_data!!}" />
<input type="hidden" id="bill_type" value="{!!$bill_tag!!}" />
<input type="hidden" id="bill_no" value="{!!$bill_no!!}" />
<input type="hidden" id="bill_head_id" value="{!!$bill_id!!}" />
<input type="hidden" id="advance_collected" value="{!!$advance_collected!!}" />

<div class="">

    <div class="col-md-12 box-body" style="padding-top:8px;">
        <div class="col-md-12">

            <div class="col-md-2">
                UHID :
            </div>
            <div class="col-md-4">
                <input class="form-control" disabled="disabled" value="{{$bill_data['uhid']}}" autocomplete="off"
                    type="text" autocomplete="off" id="uhid" name="uhid" />
            </div>
            <div class="col-md-2">
                Total Bill Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    disabled="disabled" value="{{$bill_data['bill_amount']}}" autocomplete="off" type="text"
                    value="{{$bill_data['bill_amount']}}" autocomplete="off" id="total_bill_amount"
                    name="total_bill_amount" />
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">

            <div class="col-md-2">
                Patient Name :
            </div>
            <div class="col-md-4">
                <input class="form-control" disabled="disabled" value="{{$bill_data['patient_name']}}"
                    autocomplete="off" type="text" autocomplete="off" id="patient_name" name="patient_name" />
            </div>

            <div class="col-md-2">
                Total Net Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    disabled="disabled" value="{{$bill_data['net_amount']}}" autocomplete="off" type="text"
                    autocomplete="off" id="total_net_amount" name="total_net_amount" />
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">
            <div class="col-md-2">
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                Already Paid Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    disabled="disabled" value="{{$bill_data['already_paid_amount']}}" autocomplete="off" type="text"
                    autocomplete="off" id="already_paid_amount" name="already_paid_amount" />
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">
            <div class="col-md-6">

            </div>
            <div class="col-md-6 no-padding">
                <div class="col-md-4">
                    Amount To Be Paid :
                    @php
                    $amount_to_be_paid = $bill_data['patient_to_pay']-$advance_collected;
                    @endphp
                    <input type="hidden" id="amount_to_be_pay_hidden" value="{{$amount_to_be_paid}}" />
                </div>
                <div class="col-md-8">
                    <input class="form-control"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        disabled="disabled" value="{{$amount_to_be_paid}}" autocomplete="off" type="text"
                        autocomplete="off" id="amount_to_be_paid" name="amount_to_be_paid" />
                </div>
                <div class="col-md-4" style="margin-top:8px;">

                </div>
            </div>

        </div>
    </div>

    <div class="col-md-12 box-body" style="padding-top:8px;">
        <div class="col-md-2">
            <button type="button" onclick="changePaymentMode('1');" class="btn bg-blue payment_mode">
                CASH
            </button>
            <button type="button" onclick="changePaymentMode('4');" class="btn bg-blue payment_mode">
                CHEQUE
            </button>
            <button type="button" onclick="changePaymentMode('2');" class="btn bg-blue payment_mode">
                CREDIT CARD
            </button>

            <button type="button" onclick="changePaymentMode('3');" class="btn bg-blue payment_mode">
                DEBIT CARD
            </button>
            <button type="button" onclick="changePaymentMode('17');" class="btn bg-blue payment_mode">
                PAYMENT GATEWAY
            </button>
            <button type="button" onclick="changePaymentMode('14');" class="btn bg-blue payment_mode">
                UPI
            </button>
        </div>
        @php
        $sum_already_paid = 0;
        $remaining_amount =0;
        @endphp
        @if(count($cash_collection_data)>0)
        @foreach($cash_collection_data as $data)
        @php
        $sum_already_paid +=$data->paid_amount;
        @endphp
        @endforeach
        @php
        $remaining_amount = $bill_data['patient_to_pay']-$sum_already_paid;
        $remaining_amount = $remaining_amount-$advance_collected;
        $remaining_amount = (int)$remaining_amount;
        @endphp
        @endif
        <input type="hidden" id="remaining_amount" value={{$remaining_amount}} />
        <div class="col-md-10 box-body">
            <div class="col-md-12 no-padding">
                <div class="col-md-1 no">
                    <label>Amount</label>
                    <input class="form-control" value=""
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        autocomplete="off" type="text" autocomplete="off" id="amount" name="amount" />
                </div>
                <div class="col-md-1" style="width:120px;">
                    <label>Payment Mode</label>
                    {!! Form::select('payment_mode', $payment_mode, 1, ['class' => 'form-control select2 ', 'id' =>
                    'payment_mode','placeholder'=>'select', 'style' => '']) !!}
                </div>

                <div class="col-md-2">
                    <label>Machine Bank</label>
                    {!! Form::select('machine_bank', $mechine_bank_list, 0, ['class' => 'form-control select2 ', 'id' =>
                    'machine_bank','placeholder'=>'select', 'style' => '']) !!}
                </div>
                <div class="col-md-2">
                    <label>Bank</label>
                    {!! Form::select('bank', $bank_list, 0, ['class' => 'form-control select2 ', 'id' =>
                    'bank','placeholder'=>'select', 'style' => '']) !!}
                </div>
                <div class="col-md-1">
                    <label>Card No</label>
                    <input class="form-control" value="" autocomplete="off" type="text" autocomplete="off" id="card_no"
                        name="card_no" />
                </div>
                <div class="col-md-2">
                    <label>Phone No</label>
                    <input class="form-control" value="" autocomplete="off" type="text" autocomplete="off" id="phone_no"
                        name="phone_no" />
                </div>
                <div class="col-md-1">
                    <label>Exp Month</label>
                    <input class="form-control"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        onkeyup="checkContentLength('exp_month',2);" value="" autocomplete="off" type="text"
                        autocomplete="off" id="exp_month" name="exp_month" />
                </div>
                <div class="col-md-1">
                    <label>Exp Year</label>
                    <input class="form-control"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        onkeyup="checkContentLength('exp_year',4);" value="" autocomplete="off" type="text"
                        autocomplete="off" id="exp_year" name="exp_year" />
                </div>
                <div class="col-md-1" style="width:55px;padding-top:24px;">
                    <button type="button" class="btn btn-primary" value="" id="add_new_mode" name="add_new_mode"
                        onclick="addNewPaymentMode();">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="col-md-12">
                <table
                    class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding floatThead-table"
                    id="payment_mode_table">
                    <thead style="background-color: cadetblue;color:white;">
                        <tr>
                            <td>Payment Mode</td>
                            <td>Amount Paid</td>
                            <td>Machine Bank</td>
                            <td>Bank</td>
                            <td>Card No</td>
                            <td>Exp Date</td>
                            <td>Phone No</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($cash_collection_data)>0)
                        @php
                        $i=1;
                        $reciving_bank = '';
                        $bank = '';
                        @endphp
                        @foreach($cash_collection_data as $data)
                        <tr>
                        <tr class="payment_mode_row" id="payment_mode_row_{{$i}}">
                            @php
                            $reciving_bank = '';
                            $bank = '';
                            $payment_mode_name = \DB::table('payment_mode')
                            ->where('id',$data->collection_mode)->value('name');
                            if($data->reciving_bank_id !=''){
                            $reciving_bank = \DB::table('bank_detail')
                            ->where('id',$data->reciving_bank_id)->value('bank_name');
                            }
                            if($data->bank_id !=''){
                            $bank = \DB::table('bank_detail')
                            ->where('id',$data->bank_id)->value('bank_name');
                            }

                            @endphp
                            <td class="payment_mode">{{$payment_mode_name}}</td>
                            <td class="amount">{{$data->paid_amount}}</td>
                            <td class="machine_bank_name">{{$reciving_bank}}</td>
                            <td class="bank_name">{{$bank}}</td>
                            <td class="card_no">{{$data->remarks}}</td>
                            <td class="exp_date">
                                @if($data->card_expiry_month !=0 && $data->card_expiry_year!=0)
                                {{$data->card_expiry_month}}/{{$data->card_expiry_year}}
                                @endif
                            </td>
                            <td class="phone_no">{{$data->phone_number}}</td>
                            <td><button type="button" onclick="deleteCurrentRow(this);"
                                    class="btn btn-danger deleteCurrentRow"><i class="fa fa-trash"></i></button></td>
                        </tr>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <table style="width:100%;" class="amount_table">
                    <tr>
                        <td width="23%"><label>Amount To Pay</label></td>
                        <td width="2%">:</td>
                        <td width="23%"><input class="form-control"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                type="text" name="amount_to_pay" value="{{$amount_to_be_paid}}" id="amount_to_pay"
                                disabled="disabled" /></td>
                        <td width="4%"></td>

                        <td width="23%">
                            <label>Bill date :</label>
                        </td>
                        <td width="23%">
                            @php
                            $bill_date = date('M-d-Y',strtotime($bill_date));
                            @endphp
                            
                            <input type="text" id="new_bill_date" name="new_bill_date" value="{{$bill_date}}"
                                class="form-control datepicker" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td width="23%"><label>Collected Amount</label></td>
                        <td width="2%">:</td>
                        <td width="23%"><input class="form-control "
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                type="hidden" name="collected_amount" value="{{$sum_already_paid}}"
                                id="collected_amount" disabled />
                            <input type="text" class="form-control collection_amt"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                type="text" value="{{$sum_already_paid}}" id="collected_amount_hidden" />
                        </td>
                        <td width="4%"></td>
                        <td width="23%">
                            <label>Payback to patient: </label>
                        </td>
                        <td width="23%">
                        <input class="form-control patient_payback"
                                type="text" name="patient_payback" id="patient_payback" value="0" disabled />
                        </td>
                        <td width="2%"></td>
                    </tr>
                    <tr>
                        <td width="23%"><label>Balance Amount: </label></td>
                        <td width="2%">:</td>
                        <td width="23%"><input class="form-control"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                type="text" name="balance_amount" id="balance_amount" disabled="disabled"
                                value="{{$remaining_amount}}" /></td>
                        <td width="4%"></td>
                        <td width="23%">

                        </td>

                        <td width="23%" style="text-align:right;">
                            <button style="width:113px;" type="button" class="btn btn-primary" name="save_bill_payment"
                                onclick="save_bill_payment();" id="save_bill_payment"><i class="fa fa-save"></i>
                                Save</button>

                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>