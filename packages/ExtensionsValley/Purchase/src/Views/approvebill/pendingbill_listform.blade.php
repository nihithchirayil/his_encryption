@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
        .ajaxSearchBox {
            display: none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 200px;
            margin: 0px 0px 0px 0px;
            overflow-y: auto;
            width: 100%;
            z-index: 9999;
            position: absolute;
            background: #ffffff;
            margin-top: 13px;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);

        }

        .search_box_bottom {
            bottom: -30px;
        }

        .ajax_search_box_bottomcls .ajaxSearchBox {
            bottom: 3px;
            height: 200px !important;
        }

        .ajax_search_box_bottomcls_reduce_box {
            height: 200px !important;
        }

        table td {
            position: relative !important;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row">
            <div class="col-md-2 padding_sm pull-right">{{ $title }}</div>
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.purchase.listPendingBills') }}"
                                id="requestSearchForm" method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepicker" id="bill_date" name="bill_date"
                                            value="<?= $current_date ?>">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill Tag</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('bill_tag', ['-1' => ' Select Bill Type'] + $billTag->toArray(), $searchFields['bill_tag'] ?? null, [
    'class' => 'form-control bill_tag',
    'id' => 'bill_tag',
]) !!}
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill No.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="bill_no"
                                            name="bill_no" value="">
                                        <div id="bill_no_AjaxDiv" class="ajaxSearchBox"></div>
                                        <input type="hidden" name="bill_no_hidden" value="" id="bill_no_hidden">
                                        <input type="hidden" name="bill_id_hidden" value="" id="bill_id_hidden">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Patient Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                            name="patient_name" value="">
                                        <div id="patient_idAjaxDiv" class="ajaxSearchBox"></div>
                                        <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                        <input type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 20px; padding-left: 9px !important;">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" id="all_bills" name="all_bills" value="1">
                                        <label style="padding-left: 2px;" for="all_bills">
                                            All Bills</label><br>
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i>Clear</a>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg"
                                        onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                        Search</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin" id='pending_bil_list'>


                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>

    {{-- include('core::dashboard.partials.bootstrap_datetimepicker_js') --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#patient_name').keyup(function(event) {
                var keycheck = /[a-zA-Z0-9 ]/;
                var value = event.key;
                var current;
                if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                    if ($('#patient_id_hidden').val() != "") {
                        $('#patient_id_hidden').val('');
                    }
                    var patient_name = $(this).val();
                    patient_name = patient_name.trim();
                    if (patient_name == "") {
                        $("#patient_idAjaxDiv").html("");
                    } else {
                        var url = '';
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: "patient_name_search=" + patient_name,
                            beforeSend: function() {
                                $("#patient_idAjaxDiv").html(
                                    '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                                ).show();
                            },
                            success: function(html) {
                                //  alert(html); return;
                                $("#patient_idAjaxDiv").html(html).show();
                                $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                            }
                        });
                    }

                }
            });

            //-----------Bill No search------------
            $('#bill_no').keyup(function(event) {
                var keycheck =
                /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
                var value = event.key; //get the charcode and convert to char
                if (event.keyCode == 13) {
                    ajaxlistenter('bill_no_AjaxDiv');
                    return false;
                 } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
                    if ($('#bill_no_hidden').val() != "") {
                        $('#bill_no_hidden').val('');
                    }
                    var bill_no = $(this).val();
                    if (bill_no == "") {
                        $("#bill_no_AjaxDiv").html("");
                    } else {
                        var url = '';
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: "bill_no=" + bill_no,
                            beforeSend: function() {
                                $("#bill_no_AjaxDiv").html(
                                    '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                                ).show();
                            },
                            success: function(html) {
                                // alert(html); return;
                                $("#bill_no_AjaxDiv").html(html).show();
                                $("#bill_no_AjaxDiv").find('li').first().addClass('liHover');
                            }
                        });
                    }

                } else {
                    ajax_list_key_down('bill_no_AjaxDiv', event);
                }
            });



            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });


            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });

            $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }
                });
            })



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        $(document).ready(function() {});

        function saveItem(id, no, type) {
            if (confirm("Are you sure to approve the bill?") == true) {
                $(".saveitembtn").prop("disabled", true);
                $("#bill_head_id").val(id);
                $("#bill_no").val(no);
                $("#bill_tag").val(type);
                $("#billapproveForm").submit();
            } else {
                return false;
            }
        }

        function fillPatientValues(uhid, patient_name, from_type = 0) {
            $('#patient_uhid_hidden').val(uhid);
            $('#patient_name').val(patient_name);
            $('#patient_idAjaxDiv').hide();
            searchBill();
        }

        /* For BillNo Search, filling values */
        function fillBillNo(pat_id, bill_id, bill_no, uhid, patient_name, from_type = 0) {
            $('#bill_no').val(bill_no);
            $('#patient_uhid_hidden').val(uhid);
            $('#bill_id_hidden').val(bill_id);
            $('#bill_no_AjaxDiv').hide();
            if (from_type != 1) {
                fillPatientValues(uhid, patient_name, 1);
            }
            searchBill();
        }

        function searchBill() {
            var bill_tag = $('#bill_tag').val();
            var bill_no = $('#bill_no').val();
            var all_bills = $('#all_bills').is(":checked")
            var uhid = '';
            var bill_id = '';
            if (bill_no) {
                bill_id = $('#bill_id_hidden').val();
            }
            var patient_name = $('#patient_name').val();
            if (patient_name) {
                uhid = $('#patient_uhid_hidden').val();
            }

            var bill_date = $('#bill_date').val();
            var url = '<?= route('extensionsvalley.purchase.getPendingBillList') ?>';

            $.ajax({
                type: "GET",
                url: url,
                data: "bill_date=" + bill_date + "&bill_tag=" + bill_tag + "&bill_id=" + bill_id + "&uhid=" + uhid +
                    "&all_bills=" + all_bills,
                beforeSend: function() {
                    $('#searchdatabtn').attr('disabled', true);
                    $('#searchdataspin').removeClass('fa fa-search');
                    $('#searchdataspin').addClass('fa fa-spinner fa-spin');

                },
                success: function(msg) {
                    $('#pending_bil_list').html(msg);

                },
                complete: function() {
                    $('#searchdatabtn').attr('disabled', false);
                    $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
                    $('#searchdataspin').addClass('fa fa-search');
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                error: function() {
                    toastr.error("Please Check Internet Connection");
                }
            });
        }



        function approveBill(bill_id) {
            bootbox.confirm({
                message: "Are you sure you want to make this Bill Pending ?",
                buttons: {
                    'confirm': {
                        label: "Pending",
                        className: 'btn-warning',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-danger'
                    }
                },
                callback: function(result) {
                    if (result) {
                        var param = {
                            bill_id: bill_id
                        };
                        $.ajax({
                            type: "GET",
                            async: true,
                            url: '<?= route('extensionsvalley.purchase.approvePendingBill') ?>',
                            data: param,
                            cache: false,
                            beforeSend: function() {
                                $('#approveBillBtn' + bill_id).attr('disabled', true);
                                $('#approveBillSpin' + bill_id).removeClass('fa fa-save');
                                $('#approveBillSpin' + bill_id).addClass('fa fa-spinner fa-spin');
                            },
                            success: function(data) {
                                if (data == 1) {
                                    toastr.success("Bill Changed To Pending");
                                    $('#approveBillRow' + bill_id).remove();
                                }

                            },
                            complete: function() {
                                $('#approveBillBtn' + bill_id).attr('disabled', false);
                                $('#approveBillSpin' + bill_id).removeClass(
                                    'fa fa-spinner fa-spin');
                                $('#approveBillSpin' + bill_id).addClass('fa fa-save');
                            }
                        });
                    }
                }
            });
        }



        function labGeneratedBill(bill_id) {
            bootbox.confirm({
                message: "Sample Number Already Generated.Are You Sure You Want Make This As Pending ?",
                buttons: {
                    'confirm': {
                        label: "Pending",
                        className: 'btn-warning',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-danger'
                    }
                },
                callback: function(result) {
                    if (result) {
                        var param = {
                            bill_id: bill_id
                        };
                        $.ajax({
                            type: "GET",
                            async: true,
                            url: '<?= route('extensionsvalley.purchase.approveLabGeneratedBills') ?>',
                            data: param,
                            cache: false,
                            beforeSend: function() {
                                $('#labGeneratedBillBtn' + bill_id).attr('disabled', true);
                                $('#labGeneratedBillSpin' + bill_id).removeClass('fa fa-list');
                                $('#labGeneratedBillSpin' + bill_id).addClass(
                                    'fa fa-spinner fa-spin');
                            },
                            success: function(data) {
                                if (data == 1) {
                                    toastr.success("Bill Changed To Pending");
                                    $('#approveBillRow' + bill_id).remove();
                                } else {
                                    toastr.warning(
                                        "Bill Cannot Change To Pending <br> Result Already Generated"
                                    );
                                }

                            },
                            complete: function() {
                                $('#labGeneratedBillBtn' + bill_id).attr('disabled', false);
                                $('#labGeneratedBillSpin' + bill_id).removeClass(
                                    'fa fa-spinner fa-spin');
                                $('#labGeneratedBillSpin' + bill_id).addClass('fa fa-list');
                            }
                        });
                    }
                }
            });
        }
    </script>

@endsection
