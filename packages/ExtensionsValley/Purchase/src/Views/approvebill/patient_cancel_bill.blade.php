@if (isset($HeadDetails) && !empty($HeadDetails))
    <table cellpadding="5" cellspacing="5"
        class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="margin-bottom: 10px;">

        <tbody class="maintd">
            <tr class="contentgroupbg" style="background-color: #ddd;">
                <td class="common_td_rules">
                    <b>Patient Name</b>
                </td>

                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->patient_name ? $HeadDetails[0]->patient_name : '' }}
                </td>
                <td class="common_td_rules">
                    <b>UHID</b>
                </td>

                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->uhid ? $HeadDetails[0]->uhid : '' }}
                </td>
                <td class="common_td_rules">
                    <b> Gender</b>
                </td>

                <td class="common_td_rules">
                    <?php
                    $gender = @$HeadDetails[0]->gender ? $HeadDetails[0]->gender : '';
                    $bill_type = @$HeadDetails[0]->bill_type ? $HeadDetails[0]->bill_type : '';
                    if ($gender == 1) {
                        $gender = 'Male';
                    } elseif (@$gender == 2) {
                        $gender = 'Female';
                    }
                    ?>
                    {{ $gender }}
                </td>
            </tr>
            <tr class="contentgroupbg" style="background-color: #ddd;">
                <td class="common_td_rules">
                    <b>DOB</b>
                </td>

                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->dob ? $HeadDetails[0]->dob : '' }}
                </td>
                <td class="common_td_rules">
                    <b>Bill NO.</b>
                    <input type='hidden' name='bill_head_id'
                        value='{{ @$HeadDetails[0]->id ? $HeadDetails[0]->id : 0 }}'>
                </td>

                <td class="common_td_rules">
                    <input type="text" class="no-border" readonly
                        value="{{ @$HeadDetails[0]->bill_no ? $HeadDetails[0]->bill_no : '' }}" name="bill_no">
                </td>
                <td class="common_td_rules">
                    <b>Location</b>
                </td>

                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->location_name ? $HeadDetails[0]->location_name : '' }}
                </td>

            </tr>
            <tr class="contentgroupbg" style="background-color: #ddd;">
                <td class="common_td_rules">
                    <b>Bill Date</b>
                </td>

                <td class="common_td_rules">
                    <input type="text" class="no-border" readonly
                        value="{{ @$HeadDetails[0]->bill_date ? $HeadDetails[0]->bill_date : '' }}" name="bill_date">
                </td>
                <td class="common_td_rules">
                    <b>Doctor Name</b>
                </td>

                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->doctor_name ? $HeadDetails[0]->doctor_name : '' }}
                </td>

                <td class="common_td_rules">
                    <b>Bill Tag</b>
                </td>

                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->bill_tag ? $HeadDetails[0]->bill_tag : '' }}
                </td>
            </tr>
            @if (intval($scree_type) == 1) 
            <tr class="contentgroupbg" style="background-color: #ddd;">
                <td class="common_td_rules">
                    <b>Cancel Requested By</b>
                </td>
                <td class="common_td_rules">
                    {{ @$HeadDetails[0]->cancel_requested_by ? $HeadDetails[0]->cancel_requested_by : '' }}
                </td>
                <td class="common_td_rules">
                    <b>Cancel Reason</b>
                </td>
                <td class="common_td_rules" colspan='3'>
                    {{ @$HeadDetails[0]->cancel_reason ? $HeadDetails[0]->cancel_reason : '' }}
                </td>
            </tr>
            @endif

        </tbody>
        <thead>
            <tr class="tableheadline">
            </tr>
        </thead>
    </table>
    @if (isset($billDetails) && !empty($billDetails))
        <style>
            .th1 {
                text-align: center;
            }
        </style>
        <script>
            $('.theadscroll').perfectScrollbar();
        </script>
        <div class="theadscroll" style="max-height: 350px;">
            <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="margin-bottom: 10px;">
                <thead>
                    <tr class="table_header_bg">
                        <th width='5%'>SI.NO.</th>
                        <th width='65%'>Item Description</th>
                        <th width='15%'>Quantity</th>
                        <th width='15%'>Amount</th>
                    </tr>
                </thead>
                <tbody class="maintd">
                    <?php $i = 0;
                    $total_amount = 0.0;
                    $total_qty = 0;
                    ?>
                    @foreach ($billDetails as $data)
                        <tr class="contentgroupbg">
                            <td class="common_td_rules">{{ ++$i }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->qty }}</td>
                            <td class="td_common_numeric_rules">{{ number_format(intval($data->net_amount), 2) }}
                            </td>
                            <?php
                            $total_qty += $data->qty;
                            $total_amount += $data->net_amount;
                            ?>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="2" class="common_td_rules"> Total</th>
                        <th class="td_common_numeric_rules"><?= $total_qty ?></th>
                        <th class="td_common_numeric_rules">{{ number_format(intval($total_amount), 2) }}</th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3 padding_sm pull-right" style="margin-top: 10px">

            <?php
               if (intval($lab_cnt) == 0) {
                $btn_id_string = 'approve_reject';
                 if (intval($scree_type) == 1) {
                    $btn_id_string = 'req_approve_reject';
                 }
                 if ($billDetails[0]->cancelled_status != '3'){
                ?>
                <button type="button" onclick="cancel_approve_revertbill(<?= $bill_id ?>,1,1,'{{ $bill_type }}')"
                    id="{{$btn_id_string}}_btn" class="btn btn-primary btn-block">Approve Bill <i class="fa fa-save"
                        id="{{$btn_id_string}}_spin"></i></button>
                <?php } else if($bill_tag!='LAB' && $bill_tag!='WDB' && $bill_tag!='PH' && $bill_tag!='CONS' && $bill_tag!='AD'){
                    ?>
                <button type="button" onclick="cancel_approve_revertbill(<?= $bill_id ?>,2,1,'{{ $bill_type }}')"
                    id="{{$btn_id_string}}_btn" class="btn btn-warning btn-block">Revert Bill <i class="fa fa-save"
                        id="{{$btn_id_string}}_spin"></i></button>
            
            <?php
                }
            }else{
                ?>
            <span class="red">Sorry!! You Can't Cancel this bill, <br> Sample No. Already Generated.</span>
            <?php
            }
        ?>
        </div>
    @else
        <div class="table-responsive billsettle_theadscroll"> No Records Found! </div>
    @endif
@else
    <div class="inpatientbill_box" style="margin-top: 15px;margin-bottom: 15px">
        No Records Found!
    </div>
@endif
