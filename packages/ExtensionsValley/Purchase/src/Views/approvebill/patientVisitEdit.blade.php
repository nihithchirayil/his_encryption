@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display: none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 100%;
        z-index: 9999px;
        position: absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }

    .search_box_bottom {
        bottom: -30px;
    }

    .ajax_search_box_bottomcls .ajaxSearchBox {
        bottom: 3px;
        height: 200px !important;
    }

    .ajax_search_box_bottomcls_reduce_box {
        height: 200px !important;
    }

    table td {
        position: relative !important;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col">
    <div class="row">
        <div class="col-md-2 pull-right padding_sm">
            {{ $title }}
        </div>
    </div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <form action="{{ route('extensionsvalley.purchase.listPendingBills') }}" id="requestSearchForm"
                            method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Patient Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                        name="patient_name" value="">
                                    <div id="patient_idAjaxDiv" class="ajaxSearchBox" style="margin-top: 15px;"></div>
                                    <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                    <input type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 date_filter_div">
                                <div class="mate-input-box">
                                    <label class="filter_label">Bill From Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" autofocus=""
                                        value="<?= $current_date ?>" class="form-control datepicker filters"
                                        placeholder="YYYY-MM-DD" id="bill_date_from">
                                </div>
                            </div>
                            <div class="col-md-2 date_filter_div">
                                <div class="mate-input-box">
                                    <label class="filter_label">Bill To Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" autofocus=""
                                        value="<?= $current_date ?>" class="form-control datepicker filters"
                                        placeholder="YYYY-MM-DD" id="bill_date_to">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 20px; padding-left: 9px !important;">
                                <div class="checkbox checkbox-success inline no-margin">
                                    <input checked="" type="checkbox" id="unpaidbillsonly" name="unpaidbillsonly"
                                        value="1">
                                    <label style="padding-left: 2px;" for="unpaidbillsonly">
                                        Unpaid Bills Only</label><br>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i>Clear</a>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg"
                                    onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row box no-border no-margin">
                <div class="col-md-6 padding_sm" id='dischargebill_bil_list'>

                </div>
                <div class="col-md-6 padding_sm" id='pending_bil_list'>

                </div>

            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
            $('#patient_name').keyup(function(event) {
                var keycheck = /[a-zA-Z0-9 ]/;
                var value = event.key;
                var current;
                if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                    if ($('#patient_id_hidden').val() != "") {
                        $('#patient_id_hidden').val('');
                    }
                    var patient_name = $(this).val();
                    patient_name = patient_name.trim();
                    if (patient_name == "") {
                        $("#patient_idAjaxDiv").html("");
                    } else {
                        var url = '';
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: "patient_name_search=" + patient_name,
                            beforeSend: function() {
                                $("#patient_idAjaxDiv").html(
                                    '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                                    ).show();
                            },
                            success: function(html) {
                                //  alert(html); return;
                                $("#patient_idAjaxDiv").html(html).show();
                                $("#patient_idAjaxDiv").find('li').first().addClass('liHover');
                            }
                        });
                    }

                }
            });
            $('.datepicker').datetimepicker({
                format: 'MMM-DD-YYYY',
            });
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
        });
</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
    function fillPatientValues(uhid, patient_name, from_type = 0) {
            $('#patient_uhid_hidden').val(uhid);
            $('#patient_name').val(patient_name);
            $('#patient_idAjaxDiv').hide();
            searchBill();
            $('#pending_bil_list').html('');
        }

        function checkallvisit() {
            var status = $('#checkallvisit').is(":checked");
            if (status) {
                $('.visit_class').prop('checked', true);
            } else {
                $('.visit_class').prop('checked', false);
            }
        }

        function searchBill() {
            var patient_name = $('#patient_name').val();
            if (patient_name) {
                var date_from = $('#bill_date_from').val();
                var unpaidbills = $("#unpaidbillsonly").is(":checked");
                var date_to = $('#bill_date_to').val();
                var uhid = '';
                if (patient_name) {
                    uhid = $('#patient_uhid_hidden').val();
                }
                var url = '<?= route('extensionsvalley.purchase.getDischargeBillDetails') ?>';
                var param = {
                    uhid: uhid,
                    date_from: date_from,
                    date_to: date_to,
                    unpaidbills: unpaidbills
                };

                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#searchdatabtn').attr('disabled', true);
                        $('#searchdataspin').removeClass('fa fa-search');
                        $('#searchdataspin').addClass('fa fa-spinner fa-spin');

                    },
                    success: function(msg) {
                        $('#dischargebill_bil_list').html(msg);

                    },
                    complete: function() {
                        $('#searchdatabtn').attr('disabled', false);
                        $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
                        $('#searchdataspin').addClass('fa fa-search');
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                        setTimeout(function() {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    error: function() {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            } else {
                toastr.warning("Please Select Any Patient");
            }
        }

        function getPatientPendingBills(visit_id) {
            var patient_name = $('#patient_name').val();
            if (patient_name) {
                var date_from = $('#bill_date_from').val();
                var unpaidbills = $("#unpaidbillsonly").is(":checked");
                var date_to = $('#bill_date_to').val();
                var uhid = '';
                if (patient_name) {
                    uhid = $('#patient_uhid_hidden').val();
                }
                var url = '<?= route('extensionsvalley.purchase.getPreviousVisitPatient') ?>';
                var param = {
                    uhid: uhid,
                    date_from: date_from,
                    date_to: date_to,
                    unpaidbills: unpaidbills,
                    visit_id: visit_id
                };

                $.ajax({
                    type: "GET",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#searchdatabtn').attr('disabled', true);
                        $('#searchdataspin').removeClass('fa fa-search');
                        $('#searchdataspin').addClass('fa fa-spinner fa-spin');

                    },
                    success: function(msg) {
                        $('#pending_bil_list').html(msg);

                    },
                    complete: function() {
                        $('#searchdatabtn').attr('disabled', false);
                        $('#searchdataspin').removeClass('fa fa-spinner fa-spin');
                        $('#searchdataspin').addClass('fa fa-search');
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                        setTimeout(function() {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    error: function() {
                        toastr.error("Please Check Internet Connection");
                    }
                });
            } else {
                toastr.warning("Please Select Any Patient");
            }
        }

        function changePatientVisit(visit_id) {

            var visit_array = [];
            $('.visit_class').each(function(i, obj) {
                var status = $(this).is(":checked");
                if (status) {
                    visit_array.push($(this).val());
                }
            });

            var visit_arraystring = JSON.stringify(visit_array);

            var param = {
                visit_arraystring: visit_arraystring
            };
            $.ajax({
                type: "GET",
                async: true,
                url: '<?= route('extensionsvalley.purchase.changePatientVisit') ?>',
                data: param,
                cache: false,
                beforeSend: function() {
                    $('#changePatientVisitBtn').attr('disabled', true);
                    $('#changePatientVisitSpin').removeClass('fa fa-save');
                    $('#changePatientVisitSpin').addClass('fa fa-spinner fa-spin');

                },
                success: function(data) {
                    if (parseInt(data) == 1) {
                        toastr.success('Successfully Updated');
                        getPatientPendingBills(visit_id);
                    } else {
                        toastr.warning('No Data Updated');
                    }
                },
                complete: function() {
                    $('#changePatientVisitBtn').attr('disabled', false);
                    $('#changePatientVisitSpin').removeClass('fa fa-spinner fa-spin');
                    $('#changePatientVisitSpin').addClass('fa fa-save');
                },
                error: function() {
                    toastr.error("Please Check Internet Connection");
                }
            });
        }
</script>

@endsection
