@extends('Dashboard::dashboard.dashboard')
@section('content-header')

@include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">


@endsection
@section('content-area')


<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 padding_sm">
            <div class="colmd-2 padding_sm pull-right">
                {{$title}}
            </div>
        </div>
    </div>
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.purchase.listPendingBills')}}" id="requestSearchForm" method="POST">
                        {!! Form::token() !!}
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="patient_name" name="patient_name" value="">
                                <div id="patient_idAjaxDiv" style="margin-top: -15px;" class="ajaxSearchBox"></div>
                                <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                <input type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Bill NO</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="bill_number" name="bill_number" value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 20px;">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input id="all_bills" type="checkbox" name="all_bills">
                                <label class="text-blue" for="all_bills">
                                    All Bills
                                </label>
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm" style="margin-top:20px ;">
                            <label id="patient_name_div"></label>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:20px ;">
                            <label id="patient_uhid_div"></label>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>Clear</a>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block btn-primary" onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                Search</button>
                        </div>

                    </form>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id='pending_bil_list'>


            </div>
            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="display: none;margin-top: 10px;" id='companydetailsdiv'>
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label class="filter_label" style="width: 100%;">Parent Company</label>
                        <div class="clearfix"></div>
                        <select onchange="getCompanyType()" id="companyid" class="form-control select2" name="company">
                            <option value="">All</option>
                            <?php
                            foreach ($company_list as $key => $val) {
                            ?>
                                <option value="<?= $key ?>"><?= $val ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                </div>
                <div id="getcompanytypediv">

                </div>
                <div class="col-md-2 padding_sm pull-right" style="margin-top:20px ;">
                    <button id="paymentTypeBtn" type="button" onclick="changepaymentType()" class="btn btn-success btn-block">Save <i id="paymentTypeSpin" class="fa fa-save"></i></button>
                </div>

            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}" />
        </div>
    </div>
</div>

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/paymentmode_edit.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js")}}"></script>
{!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
@endsection
