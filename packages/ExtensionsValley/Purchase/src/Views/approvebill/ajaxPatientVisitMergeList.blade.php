<table class="table table_round_border styled-table theadfix_wrapper">
    <thead>
        <tr class="table_header_bg">
            <th style="width:10%">From Patient</th>
            <th style="width:10%">To Patient</th>
            <th style="width:10%">Created Date</th>
        </tr>
    </thead>
    <tbody id="merge_patientsBody">
        @if (!empty($result_merge_details))
        @foreach($result_merge_details as $val)
        <tr>
            <td> {{ @$val->from_patient_name ? $val->from_patient_name: '' }} </td>
            <td>{{ @$val->to_patient_name ? $val->to_patient_name: '' }}</td>
            <td>{{ @$val->created_date ? $val->created_date: '' }}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan='3'>No data found..</td>
        </tr>
        @endif
        <tr>
        </tr>
    </tbody>
</table>