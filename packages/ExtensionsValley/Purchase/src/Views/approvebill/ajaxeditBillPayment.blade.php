<div class="box-body clearfix">
<div class="theadscroll" style="position: relative; height: 400px;">
<table id="result_data_table" class='theadfix_wrapper table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">
                    <div class="checkbox checkbox-warning inline no-margin">
                    <input id="checkallvisit" onclick="checkallvisit()" type="checkbox" class="filters" name="checkallvisit" style=" margin-left: 10px;">
                    <label for="checkallvisit" style="padding-right: 4px;padding-left: 2px;padding-top: -1px;"></label>
                    </div>
                </th>
                <th width="15%">Bill No</th>
                <th width="10%">Bill Tag</th>
                <th width="10%">Bill Date</th>
                <th width="15%">Doctor Name</th>
                <th width="14%">Company Name</th>
                <th width="8%">Net Amount</th>
                <th width="7%">Paid Status</th>
                <th width="8%">Payment Type</th>
                <th width="7%">Pati.Pay</th>
                <th width="7%">Comp.Pay</th>
                <th width="7%">net.Pay</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $tot_patient_to_pay =0.0;
            $tot_company_to_pay=0.0;
            $tot_netamout =0.0;
        if(count($res)!=0){
            foreach ($res as $item){
            $tot_patient_to_pay += floatval($item->patient_to_pay);
             $tot_company_to_pay+= floatval($item->company_to_pay);
             $tot_netamout+= floatval($item->net_amount_wo_roundoff);
             $text_color="";
             if($item->is_includeindischarge=='1'){
                $text_color="text-red";
             }
            ?>    
                <tr class="<?=$text_color?>" id="paymentBillBillRow<?=$item->id?>">
                        <?php
                         if($item->is_includeindischarge=='1'){
                            ?>
                                <td style="text-align: center">-</td>
                            <?php
                         }else{
                            ?>
                            <td style="text-align: center">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input value="<?=$item->id?>" id="checkbillshead<?=$item->id?>" type="checkbox" class="visit_class" class="filters" style=" margin-left: 10px;">
                                <label for="checkbillshead<?=$item->id?>"></label>
                            </div>
                        </td>
                            <?php
                         }
                        ?>
                        <td class="common_td_rules">{{$item->bill_no}}</td>
                        <td class="common_td_rules">{{$item->bill_type}}</td>
                        <td class="common_td_rules">{{date('M-d-Y',strtotime($item->bill_datetime))}}</td>
                        <td class="common_td_rules">{{$item->doctor_name}}</td>
                        <td class="common_td_rules">{{$item->company_name}}</td>
                        <td class="td_common_numeric_rules">{{$item->net_amount_wo_roundoff}}</td>
                        <td class="common_td_rules">
                        <?php if($item->paid_status == 1){$paid_status_name = "Paid"; }else{
                            $paid_status_name = "Not Paid";
                        } ?>
                        {{ $paid_status_name }}</td>
                        <td class="common_td_rules"> {{ $item->payment_type }}</td>
                        <td class="td_common_numeric_rules"> {{ $item->patient_to_pay }}</td>
                        <td class="td_common_numeric_rules"> {{ $item->company_to_pay }}</td>
                        <td class="td_common_numeric_rules"> {{ $item->net_amount_wo_roundoff }}</td>
                    </tr>
            <?php
                }
                ?>
                    <tr id="paymentBillBillRow<?=$item->id?>">
                        <th colspan="9" class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules">{{$tot_patient_to_pay}}</th>
                        <th class="td_common_numeric_rules">{{$tot_company_to_pay}}</th>
                        <th class="td_common_numeric_rules">{{$tot_netamout}}</th>
                    </tr>
                <?php
            }else{
                ?>
                <tr>
                    <td style="text-align: center;" colspan="12">No Records found</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

</div>
  
 </div>