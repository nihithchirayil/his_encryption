<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 62vh;">
        <table id="result_data_table" class='theadfix_wrapper table table-condensed table_sm table-col-bordered'
            style="font-size: 12px; padding-right:10px;" table_page_count='0'>
            <thead>
                <tr class="table_header_bg">
                    <th width="2%"><i class="fa fa-list"> </i></th>
                    <th width="15%">Patient Name</th>
                    <th width="12%">UHID</th>
                    <th width="10%">Bill No</th>
                    <th width="10%">Bill Tag</th>
                    <th width="10%">Bill Date</th>
                    <th width="8%">Net Amount</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($res) && count($res) != 0)
                @foreach ($res as $key=> $item)
                        @php 
                            $diffInDays = 0;
                            $today = \Carbon::now();
                            if ($item->bill_date) {
                                $bill_date = date('Y-m-d', strtotime($item->bill_date));
                                $billDate = \Carbon::createFromFormat('Y-m-d', $bill_date);
                                $diffInDays = $today->diffInDays($billDate);                    
                            }

                        @endphp
                <tr id="paymentBillBillRow<?=$item->id?>">
                    <td class="common_td_rules">
                        
                        <button type="button" class="pull-right btn btn-block light_purple_bg"                           
                            style="width: 25px; height: 20px; border: none;" @if($diffInDays <= $edit_bill_payment_mode)  onclick="getBillDetails(this, {{ $item->id }}, '{{ $item->bill_tag }}')" @else disabled @endif >
                            <i class="fa fa-list"> </i>
                        </button> 
                    </td>
                    <td class="common_td_rules">{{$item->patient_name}}</td>
                    <td class="common_td_rules">{{$item->uhid}}</td>
                    <td class="common_td_rules">{{$item->bill_no}}</td>
                    <td class="common_td_rules">{{$item->bill_type}}</td>
                    <td class="common_td_rules">{{$item->bill_date}}  </td>
                    <td class="td_common_numeric_rules">{{$item->net_amount}}</td>                    
                </tr>
                @endforeach
                @endif
      
            </tbody>
        </table>
    </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right" style="max-height: 75px; height: 100%; margin-top: 0px;">
    <ul class="reorder_level_pages pagination purple_pagination" style="text-align:right !important;">
        {!! $paginator->render() !!}
    </ul>
</div>
