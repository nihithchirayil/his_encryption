<?php
$row_count=1;
if(count($res)!=0){
    foreach ($res as $each) {
        $string=$each->item_description;
        $readonly_status='';
        ?>


<tr onclick="lastClicKItemRow(<?= $row_count ?>)" style="background: #FFF;" class="row_class"
    id="row_data_{{ $row_count }}">
    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
    </td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc_{{ $row_count }}"
            onkeyup='searchItemCode(this.id,event,{{ $row_count }})' class="form-control popinput" name="item_desc[]"
            placeholder="Search Item" value="{{ $each->item_description }}">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>

    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            id="request_qty{{ $row_count }}" type="text" value='{{ $each->quantity }}'>
        <input type='hidden' name='row_id_hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' name='item_code_hidden' value="{{ $each->item_code }}"
            id="item_code_hidden{{ $row_count }}">
        <input type='hidden' name='item_id_hidden' value="{{ $each->item_id }}" id="item_id_hidden{{ $row_count }}">
        <input type='hidden' name='bill_detail_id_hidden' value="{{ $each->detail_id }}"
            id="bill_detail_id_hidden{{ $row_count }}">
    </td>

    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' autocomplete="off" id="request_rate{{ $row_count }}" type="text"
            value='{{ $each->unit_rate }}'>
    </td>
    <td>
        <input class="form-control number_class" id="request_mrp{{ $row_count }}" type="text" value='{{ $each->mrp }}'>
    </td>

    <td title="Discount Type">
        <select onchange="listDataCalculation({{ $row_count }},1)" name="discount_type"
            id="discount_type{{ $row_count }}" class="form-control">
            @foreach ($discount_type_array as $dis=>$type)
            @php
            $selected='';
            @endphp
            @if($dis==$each->discount_type)
            @php
            $selected='selected';
            @endphp
            @endif
            <option {{ $selected }} value="{{ $dis }}">{{ $type }}</option>
            @endforeach

        </select>
    </td>
    <td><input class="form-control number_class" onchange="listDataCalculation({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_dis_amt{{ $row_count }}" name="tot_dis_amt" type="text" value="{{ $each->discount_value }}">
    </td>

    <td>
        <select autocomplete="off" onchange="listDataCalculation({{ $row_count }},1)" id="request_tax{{ $row_count }}"
            name="request_tax" class="form-control">
            <option value="">Select</option>
            @foreach ($tax_head as $each1)
            @php
            $selected="";
            @endphp
            @if($each->tax_percentage == $each1->tax_val)
            @php
            $selected="selected";
            @endphp
            @endif
            <option {{ $selected }} attt-id="{{ $each1->tax_division }}" value="{{ $each1->tax_name }}">
                {{ $each1->tax_val }}</option>
            @endforeach
        </select>
    </td>

    <td>
        <input class="form-control number_class" style="background: #fff" readonly autocomplete="off"
            id="tot_tax_amt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->tax_amount }}'>
    </td>

    <td>
        <input class="form-control number_class" autocomplete="off" id="request_netamt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{$each->net_amount}}'>
    </td>

    <td style="text-align: center">
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>


<?php
 $row_count++;
    }

}
?>
<input type="hidden" id="edit_row_count" value="{{ $row_count }}">
