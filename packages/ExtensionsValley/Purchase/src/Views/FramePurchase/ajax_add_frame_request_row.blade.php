<tr onclick="lastClicKItemRow(<?= $row_count ?>)" style="background: #FFF;" class="row_class"
    id="row_data_{{ $row_count }}">
    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
    </td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc_{{ $row_count }}"
            onkeyup='searchItemCode(this.id,event,{{ $row_count }})' class="form-control popinput" name="item_desc"
            placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)" id="request_qty{{ $row_count }}" type="text" value='1'>
        <input type='hidden' name='row_id_hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' name='item_code_hidden' value="" id="item_code_hidden{{ $row_count }}">
        <input type='hidden' name='item_id_hidden' value="0" id="item_id_hidden{{ $row_count }}">
        <input type='hidden' name='bill_detail_id_hidden' value="0" id="bill_detail_id_hidden{{ $row_count }}">
    </td>

    <td>
        <input class="form-control number_class" onblur="listDataCalculation(<?= $row_count ?>)"
            oninput='validateNumber(this)' autocomplete="off" id="request_rate{{ $row_count }}" type="text" value=''>
    </td>
    <td>
        <input class="form-control number_class" id="request_mrp{{ $row_count }}" type="text" value=''>
    </td>
    <td title="Discount Type">
        <select onchange="listDataCalculation({{ $row_count }},1)" name="discount_type"
            id="discount_type{{ $row_count }}" class="form-control">
            @foreach ($discount_type_array as $dis=>$type)
            <option value="{{ $dis }}">{{ $type }}</option>
            @endforeach

        </select>
    </td>
    <td><input class="form-control number_class" onchange="listDataCalculation({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_dis_amt{{ $row_count }}" name="tot_dis_amt" type="text" value="0.00">
    </td>
    <td>
        <select onchange="listDataCalculation({{ $row_count }},1)" name="request_tax" id="request_tax{{ $row_count }}"
            class="form-control">
            <option value="">Select</option>
            @foreach ($tax_head as $each)
            <option attt-id="{{ $each->tax_division }}" value="{{ $each->tax_name }}">
                {{ $each->tax_val }}</option>
            @endforeach
        </select>
    </td>
    <td>
        <input class="form-control number_class" style="background: #fff" readonly autocomplete="off"
            id="tot_tax_amt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" value=''>
    </td>
    <td>
        <input class="form-control number_class" style="background: #fff" readonly autocomplete="off"
            id="request_netamt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" value=''>
    </td>
    <td style="text-align: center">
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
