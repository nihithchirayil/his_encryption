<?php
$total_cgst = 0.0;
$total_sgst = 0.0;
$total_igst = 0.0;
$total_cess = 0.0;
$total_discount = 0.0;
$total_othercharges = 0.0;
$total_freightcharges = 0.0;
$total_amt = 0.0;
$net_total = 0.0;
$tot_tax = 0.0;
$oth_charges = 0.0;
$cgst = 0.0;
$sgst = 0.0;
$igst = 0.0;
$gross_amt = 0;
$total_tax = 0;
$total_net_amount = 0;
?>
<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" class="table no-margin table_sm table-striped no-border styled-table"
                        style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th style="text-align: center" colspan="9">
                                    Purchase Order
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="5" width="60%">
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="4" width="40%">PO No :
                                    <?= @$po_number ? $po_number : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="5" width="60%">Address :
                                    <?= @$vendor_data['vendor_address'] ? $vendor_data['vendor_address'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="4" width="40%">P.O.DATE :
                                    <?= @$po_date ? $po_date : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="5" width="60%">Email :
                                    <?= @$vendor_data['vendor_email'] ? $vendor_data['vendor_email'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="4" width="40%">Manufacturer :
                                    <?= @$manufacturer_name ? $manufacturer_name : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="5" width="60%">Telephone No. :
                                    <?= @$vendor_data['contact_no'] ? $vendor_data['contact_no'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="4" width="40%">Location :
                                    <?= @$location_name ? $location_name : '' ?>
                                </th>
                            </tr>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <tr class="table_header_bg ">
                                <th width="2%">SL.No.</th>
                                <th width="30%">Product Name</th>
                                <th width="9%">Quantity</th>
                                <th width="9%">Rate</th>
                                <th width="9%">MRP</th>
                                <th width="9%">Dis.Amt</th>
                                <th width="9%">Tax Per</th>
                                <th width="9%">Tax Amt.</th>
                                <th width="9%">Net Amt.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($item_string)!=0){
                                foreach ($item_string['items'] as $each) {

                                    $string=$each->item_desc;

                                    $gross_amt += floatval($each->gross_amount);
                                    $total_tax += floatval($each->tax_amount);
                                    $total_net_amount += floatval($each->net_amount);

                                    ?>
                            <tr>
                                <td>
                                    <?= $i ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= $string ?>
                                </td>

                                <td class="td_common_numeric_rules">
                                    <?= $each->quantity ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each->unit_rate, 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each->mrp, 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each->discount_amount, 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each->tax_percentage, 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each->tax_amount, 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each->net_amount, 2, '.', '') ?>
                                </td>
                            </tr>
                            <?php
                                       $i++;
                                        }
                                        }else {
                                            ?>
                            <tr>
                                <td colspan="9" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                            <?php
                                        }
                                        ?>

                            <tr class="bg-blue">
                                <th class="td_common_numeric_rules" colspan="5"> Gross Amount :</th>
                                <th class="td_common_numeric_rules" colspan="4">
                                    <?= number_format($gross_amt, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th class="td_common_numeric_rules" colspan="5">Total Tax :</th>
                                <th class="td_common_numeric_rules" colspan="4">
                                    <?= number_format($total_tax, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th class="td_common_numeric_rules" colspan="5">RoundOff :</th>
                                <th class="td_common_numeric_rules" colspan="4">
                                    <?= number_format($round_off, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th class="td_common_numeric_rules" colspan="5">Total Amount :</th>
                                <th class="td_common_numeric_rules" colspan="4">
                                    <?= number_format($total_net_amount+$round_off, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="4" class="common_td_rules" width="40%">
                                    GST NO.
                                    <?= @$hospital_header[0]->gst_no ? $hospital_header[0]->gst_no : '' ?>
                                </th>
                                <th colspan="5" class="common_td_rules" width="60%">
                                    Drug License No.
                                    <?= @$drug_licence_no ? $drug_licence_no : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="4" class="common_td_rules" width="40%">
                                    <b>Seller : </b>
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </td>
                                <td colspan="5" class="common_td_rules" width="60%" colspan="2">
                                    <b>Buyer : </b>
                                    <?= @$hospital_header[0]->address ? $hospital_header[0]->address : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="common_td_rules" colspan="9">
                                    <b>PO Remarks: </b>
                                    <?= @$full_remarks ? $full_remarks : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="3" class="common_td_rules" width="30%">
                                    <b>PO Status : </b>
                                    <?= $po_status ?>
                                </th>
                                <th colspan="3" class="common_td_rules" width="40%">
                                    <b>Approved By : </b>
                                    <?= @$users[0]->approved_by ? $users[0]->approved_by : '' ?>
                                </th>
                                <th colspan="3" class="common_td_rules" width="30%">
                                    <b>Verified By : </b>
                                    <?= @$users[0]->verified_by ? $users[0]->verified_by : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="3" class="common_td_rules" width="30%">
                                    <b>Prepared By : </b>
                                    <?= @$users[0]->created_by ? $users[0]->created_by : '' ?>
                                </td>
                                <td colspan="3" class="common_td_rules" width="30%">
                                    <b>Printed By : </b>
                                    <?= @$users[0]->printed_by ? $users[0]->printed_by : '' ?>
                                </td>
                                <td colspan="3" class="common_td_rules" width="40%">
                                    <b>Print Date Time : </b>
                                    <?= date('M-d-Y H:i:s') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
