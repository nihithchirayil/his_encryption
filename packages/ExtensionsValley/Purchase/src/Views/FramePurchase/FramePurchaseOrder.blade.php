@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')


<div class="modal fade" id="sendEmailCCModel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 800px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Email CC Users</h4>
            </div>
            <div class="modal-body" style="height: 440px" id="">
                <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 410px;">
                            <table class="table no-margin table_sm table-striped no-border styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th width="40%">Name</th>
                                        <th width="55%">Email</th>
                                        <th width="5%" style="text-align: center">
                                            <button type="button" onclick="manageEmailCCUsers('','')"
                                                class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="addUsersToEmail">
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger"> Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" onclick="addUserEmailCC()" type="button" class="btn btn-primary"> Add
                    to
                    Email <i class="fa fa-save"></i></button>
            </div>

        </div>

    </div>
</div>
<div class="modal fade" id="print_po_model" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Print Purchase Order</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div id="print_po_modelDiv"></div>

            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button onclick="exceller();" style="padding: 3px 3px" type="button" class="btn btn-warning">Excel
                    <i class="fa fa-file-excel-o"></i></button>
                {{-- <button onclick="DownloadPdfSendMail(0)" style="padding: 3px 3px" type="button" id="file_pdf_btn"
                    class="btn btn-success">PDF <i id="file_pdf_spin" class="fa fa-file"></i>
                </button> --}}
                <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                    class="btn btn-primary">Print <i class="fa fa-print"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                        value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                        value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                        style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="right_col" id="mainDataDiv" role="main">
    <div class="row padding_sm">
        <div class="col-md-2 padding_sm">
            <strong>
                <?= $title ?>
            </strong>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
        <input type="hidden" id="row_count_id" name="row_count" value="1">
        <input type="hidden" id="po_id" value="<?= $po_id ?>">
        <input type="hidden" id="copy_status" value="<?= $copy_status ?>">
        <input type="hidden" id="calculationSelectRowID" value="0">
        <input type="hidden" id="decimal_configuration" value="<?= $decimal_configuration ?>">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <input type="hidden" id="lastClickedRowID" value="0">
    </div>
    <div class="row padding_sm">
        <div id="poplast_item_box_div" class="poplast_item_box col-md-12 padding_sm"
            style="z-index: 999999; display: block;">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                            <div class="col-md-12 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Supplier <span style="color: red">*</span></label>
                                    <input onkeyup="searchNewVendor(this.id,event)" type="text" name="searchnewvendor"
                                        value="" class="form-control" autocomplete="off" id="searchnewvendor"
                                        placeholder="">
                                    <div id="ajaxVendorSearchBox" class="ajaxSearchBox"
                                        style="margin-top: -16px; width: 100%; z-index: 9999;">
                                    </div>
                                    <input type="hidden" name="vendoritem_id_hidden" value="0"
                                        id="vendoritem_id_hidden">
                                    <input type="hidden" name="vendoritem_code_hidden" value=""
                                        id="vendoritem_code_hidden">
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Manufacturer</label>
                                    {!! Form::select('manufacturer_list', $manufacture_list, '', [
                                    'class' => 'form-control custom_floatinput select2',
                                    'placeholder' => 'Select Manufacturer',
                                    'title' => 'Select Manufacturer',
                                    'id' => 'manufacturer_list',
                                    'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-9 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Location</label>
                                    {!! Form::select('deprt_list', $listdept, $default_optical_store, [
                                    'class' => 'form-control custom_floatinput select2',
                                    'placeholder' => 'Select Location',
                                    'title' => 'Select Location',
                                    'id' => 'deprt_list',
                                    'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="checkbox checkbox-info inline no-margin">
                                    <input onclick="checkIgstStatus()" title="Is IGST" type="checkbox" id="is_igstcheck"
                                        value="1">
                                    <label for="is_igstcheck">IGST</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                            <div class="col-md-12 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">PO Number</label>
                                    <input name="po_number_show" id="po_number_show" readonly style="background: #fff"
                                        class="form-control" autocomplete="off" type="text">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">PO Date</label>
                                    <div class="clearfix"></div>
                                    <input class="form-control custom_floatinput" style="background: #fff"
                                        value="<?= date('M-d-Y') ?>" readonly id="po_date" placeholder="PO Date"
                                        name="po_date" type="text">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Remarks</label>
                                    <input name="payment_remarks" id="payment_remarks" class="form-control"
                                        autocomplete="off" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                            <table class="table no-margin table_sm no-border grn_total_table">
                                <tbody>
                                    <tr>
                                        <td width="50%"><label class="text-bold">+ Gross Amount</label></td>
                                        <td><input readonly="" type="text" id="gross_amount_hd" Value="0.00"
                                                name="gross_amount_hd" class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td width="50%"><label class="text-bold">- Discount Amount</label></td>
                                        <td><input readonly="" type="text" id="discount_hd" Value="0.00"
                                                name="discount_hd" class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td><label class="text-bold">+ Total Tax</label></td>
                                        <td><input readonly="" id="tot_tax_amt_hd" type="text" Value="0.00"
                                                name="tot_tax_amt_hd" class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td><label class="text-bold">RoundOff Amount</label></td>
                                        <td><input value="0.00" autocomplete="off"
                                                onblur="updateRoundAmt('round_off','')" onkeyup="getRoundOffAmt()"
                                                name="round_off" id="round_off" type="text"
                                                class="form-control text-right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label class="text-bold">Net Total Amount</label></td>
                                        <td><input readonly="" name="po_amount"
                                                style="font-size: 16px !important;font-weight: 700;" id="net_amount_hd"
                                                type="text" Value="0.00" class="form-control text-right">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 190px;">
                            <?php if (isset($accessPerm['approval']) && $accessPerm['approval'] == '1') { ?>
                            <div id="po_approve_btn_id" class="col-md-12 pull-right padding_sm" style="display: none">
                                <button type="button" onclick="savePurchaseOrder(3)" id="po_approve_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-buysellads"
                                        id="po_approve_spin"></i>
                                    Approve</button>
                            </div>
                            <div id="po_amend_btn_id" class="col-md-12 pull-right padding_sm" style="display: none">
                                <button type="button" onclick="savePurchaseOrder(4)" id="po_amend_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-edit"
                                        id="po_amend_spin"></i>
                                    Amend</button>
                            </div>
                            <?php }if (isset($accessPerm['verification']) && $accessPerm['verification'] == '1') { ?>
                            <div id="po_verify_btn_id" class="col-md-12 pull-right padding_sm" style="display: none">
                                <button type="button" onclick="savePurchaseOrder(5)" id="po_verify_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-user-secret"
                                        id="po_verify_spin"></i>
                                    Verify </button>
                            </div>
                            <?php }if (isset($accessPerm['add']) && $accessPerm['add'] == '1') {  ?>
                            <div id="po_post_btn_id" class="col-md-12 pull-right padding_sm" style="display: none">
                                <button type="button" onclick="savePurchaseOrder(1)" id="po_post_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-paper-plane-o"
                                        id="po_post_spin"></i>
                                    Save
                                </button>
                            </div>
                            <?php }if ((isset($accessPerm['add']) && ($accessPerm['add'] == '1')) || (isset($accessPerm['approval']) && $accessPerm['approval'] == '1' )) { ?>
                            <div id="po_close_btn_id" class="col-md-12 pull-right padding_sm" style="display: none">
                                <button type="button" onclick="savePurchaseOrder(2)" id="po_close_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-times-circle-o"
                                        id="po_close_spin"></i>
                                    Close</button>
                            </div>

                            <div id="po_cancel_btn_id" class="col-md-12 pull-right padding_sm" style="display: none">
                                <button type="button" onclick="savePurchaseOrder(0)" id="po_cancel_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-fast-backward"
                                        id="po_cancel_spin"></i>
                                    Cancel</button>
                            </div>

                            <?php } ?>
                            <div id="po_email_cc_btn_id" class="col-md-12 pull-left padding_sm" style="display: none">
                                <button type="button" onclick="createPoSendEmailUsers()" id="addEmail_cc_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-cc"
                                        id="addEmail_cc_Spin"></i>
                                    Email CC</button>
                            </div>
                            <div id="po_approve_btn_id" class="col-md-12 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="savePurchaseOrder(3)" id="po_approve_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-buysellads"
                                        id="po_approve_spin"></i>
                                    Approve</button>
                            </div>
                            <div id="po_post_btn_id" class="col-md-12 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="savePurchaseOrder(1)" id="po_post_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-paper-plane-o"
                                        id="po_post_spin"></i>
                                    Save
                                </button>
                            </div>

                            {{-- <div id="po_close_btn_id" class="col-md-12 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="savePurchaseOrder(2)" id="po_close_btn"
                                    class="btn btn-warning btn-block savepobtn"><i class="fa fa-times-circle-o"
                                        id="po_close_spin"></i>
                                    Close</button>
                            </div>

                            <div id="po_cancel_btn_id" class="col-md-12 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="savePurchaseOrder(0)" id="po_cancel_btn"
                                    class="btn btn-warning btn-block savepobtn"><i class="fa fa-fast-backward"
                                        id="po_cancel_spin"></i>
                                    Cancel</button> --}}
                            </div>
                            @if($po_id && $po_id > 0)
                            <div id="po_printpreview_btn_id" class="col-md-12 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="getPrintPreviewNew()" id="print_preview_btn"
                                    class="btn btn-success btn-block"><i class="fa fa-eye" id="print_preview_spin"></i>
                                    Preview
                                </button>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 400px;">

                        <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
                            style="border: 1px solid #CCC;" id="main_row_tbl">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="2%">SL.No.</th>
                                    <th width="23%">
                                        <input id="issue_search_box"
                                            onkeyup="searchProducts('issue_search_box','added_new_list_table_product');"
                                            type="text" placeholder="Item Search.. "
                                            style="color: #000;display: none;width: 90%;">
                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                                class="fa fa-search"></i></span>
                                        <span id="item_search_btn_text">Product
                                            Name</span>
                                    </th>
                                    <th width="9%">Quantity</th>
                                    <th width="9%">Rate</th>
                                    <th width="9%">MRP</th>
                                    <th width="9%">Dis.Type</th>
                                    <th width="9%">Dis.Amt</th>
                                    <th width="9%">Tax Per</th>
                                    <th width="9%">Tax Amt.</th>
                                    <th width="9%">Net Amt.</th>
                                    <th width="3%" style="text-align: center;"><button type="button"
                                            style="margin-top: 5px;" id="addQuotationRowbtn"
                                            onclick="getNewRowInserted();" class="btn btn-primary add_row_btn"><i
                                                id="addQuotationRowSpin" class="fa fa-plus"></i></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="added_new_list_table_product">

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/frame/FramePurchaseOrder.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
@endsection
