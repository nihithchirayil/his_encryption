<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var free_stock_id = $('#grn_id_hidden').val();
                var location = $('#to_location').val();
                var stock_status = $('#stock_status').val();
                var free_stock_no = $('#grn_no_search').val();

                var param = {
                    _token: token,
                    from_date: from_date,
                    to_date: to_date,
                    free_stock_id: free_stock_id,
                    location: location,
                    stock_status: stock_status,
                    free_stock_no: free_stock_no
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#searchGrnListData").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        $('#searchgrnlistBtn').attr('disabled', true);
                        $('#searchgrnlistSpin').removeClass('fa fa-search');
                        $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        $('#searchGrnListData').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function () {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    complete: function () {
                        $("#searchGrnListData").LoadingOverlay("hide");
                        $('#searchgrnlistBtn').attr('disabled', false);
                        $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchgrnlistSpin').addClass('fa fa-search');
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>
<table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
    style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th width="9%">Free Stock No.</th>
            <th width="7%">Adj No.</th>
            <th width="8%">Location</th>
            <th width="10%">Created Date</th>
            <th width="10%">Created By</th>
            <th width="10%">Approved Date</th>
            <th width="10%">Approved By</th>
            <th width="7%">Net Amount</th>
            <th width="6%">Status</th>
            <th width="3%"><i class="fa fa-edit"></i></th>
        </tr>
    </thead>
    <tbody>
        @if (count($item) > 0)
        @foreach ($item as $each)
        <?php

                $status = '';
                if ($each->approve_status == 1) {
                    $status = 'Not Approved';
                } elseif ($each->approve_status == 2) {
                    $status = 'Approved';
                }
                ?>
        <tr>
            <td class="common_td_rules">{{ $each->free_stock_no }}</td>
            <td class="common_td_rules">{{ $each->stock_adjustment_no }}</td>
            <td class="common_td_rules">{{ $each->location_name }}</td>
            <td class="common_td_rules">{{ $each->created_at }}</td>
            <td class="common_td_rules">{{ $each->created_by }}</td>
            <td class="common_td_rules">{{ $each->approved_at }}</td>
            <td class="common_td_rules">{{ $each->approved_by }}</td>
            <td class="td_common_numeric_rules">{{ $each->net_amount }}</td>
            <td class="common_td_rules">{{ $status }}</td>
            <td style="text-align: center;">
                <button title="Free Stock Edit" type="button" onclick="freeStockEditData({{ $each->free_stock_id}})"
                    class="btn btn-warning"><i class="fa fa-edit"></i></button>
            </td>

        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="10" class="location_code">No Records found</td>
        </tr>
        @endif
    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination">
        {!! $page_links !!}
    </ul>
</div>
