<?php $row_count = ++$row_count; ?>
<tr style="background: #FFF;" onclick="item_history({{ $row_count }})" class="row_class" id="row_data_{{ $row_count }}">
    <td class='row_count_class'></td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" attr-detailid="0" autocomplete="off"
            id="item_desc_{{ $row_count }}" onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
            class="form-control popinput" name="item_desc[]" placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
    </td>
    <td>
        <input name='batch[]' onchange="calculateListAmount({{$row_count}});" autocomplete="off"
            id="batch_row{{ $row_count }}" class="form-control" type="text" value="">
    </td>
    <td>
        <input name='expiry_date[]' onchange="calculateListAmount({{$row_count}});" autocomplete="off"
            id="expiry_date{{ $row_count }}" class="form-control expiry_date" type="text" value="">
    </td>

    <td title="Unit converion">
        <select style="text-align: center" onchange="calculateListAmount({{$row_count}});" name="uom_select"
            id="uom_select_id_{{ $row_count }}" class="form-control">
            <option value="0">Select</option>
        </select>
    </td>
    <td>
        <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            style="background-color: #FFF" autocomplete="off" id="convention_factor{{ $row_count }}" readonly
            class="form-control" type="text" value="">
    </td>
    <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control" id="tot_qty_{{ $row_count }}" name="tot_qty[]" type="text" value="0"
            onblur="calculateListAmount({{ $row_count }});"></td>
    <td>
        <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            style="background-color: #FFF" readonly autocomplete="off" id="all_total_qty{{ $row_count }}"
            class="form-control" type="text" value="">
    </td>
    <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control number_class" id="unit_rate{{ $row_count }}" name="unit-rate[]" type="text" value="0"
            onblur="calculateListAmount({{ $row_count }});"></td>
    <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control number_class" id="unit_cost{{ $row_count }}" name="unit-cost[]" type="text" value="0">
    </td>
    <td>
        <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control number_class" id="unit-mrp_{{ $row_count }}" name="unit-mrp[]" type="text" value="0"
            onblur="calculateMrpAmount({{ $row_count }});">
    </td>
    <td>
        <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control number_class" id="sales_price_{{ $row_count }}" name="sales_price[]" type="text"
            value="0" onblur="calculateListAmount({{ $row_count }});">
    </td>
    <td>
        <select style="text-align: center" onchange="calculateListAmount({{ $row_count }});" name="tax_type[]"
            id="tax_type_{{ $row_count }}" class="form-control number_class">
            <option value="0">Select</option>
            @foreach ($tax_head as $type)
            <option attt-id="{{ $type->tax_division }}" value="{{ $type->tax_val }}">
                {{ $type->tax_name }}</option>
            @endforeach
        </select>
    </td>
    <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control number_class" id="tot_tax_amt_new_{{ $row_count }}" name="tot_tax_amt_new[]"
            style="background-color: #FFF" type="text" value="0.00" readonly></td>

    <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off"
            class="form-control number_class" style="background-color: #FFF" id="net_cost_{{ $row_count }}" readonly
            name="net_cost[]" type="text" value="0.00"></td>
    <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i></td>
</tr>
