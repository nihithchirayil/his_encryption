@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->

<div class="modal fade" id="purchase_historymodel" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="purchase_historymodel_header"></h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;width: 100%">

                    <div class="clearfix panel-body"
                        style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">

                        <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                            <table
                                class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align "
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_light_purple_bg ">
                                        <th style="text-align: center;">Grn No</th>
                                        <th style="text-align: center;">Grn Date</th>
                                        <th style="text-align: center;">Vendor Name </th>
                                        <th style="text-align: center;">Batch No </th>
                                        <th style="text-align: center;">Expiry </th>
                                        <th style="text-align: center;">Grn Qty </th>
                                        <th style="text-align: center;">Grn Unit</th>
                                        <th style="text-align: center;">Grn Rate</th>
                                        <th style="text-align: center;">Unit Cost</th>
                                        <th style="text-align: center;">Free Qty</th>
                                        <th style="text-align: center;">MRP</th>
                                    </tr>
                                </thead>
                                <tbody id="history_dl_list">


                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                            class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                        value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                        value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                        style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="po_list_detail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <h4 class="modal-title" id="po_list_detail_header">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 450px">
                <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                    <table
                        class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed"
                        style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_light_purple_bg ">
                                <th><input type="checkbox" id='po_select_all' onclick="checkAllPo();"></th>
                                <th>Item Code</th>
                                <th>Item Desc</th>
                                <th>Qty</th>
                                <th>Free Qty</th>
                                <th>Rate</th>
                                <th>Net Amount</th>
                            </tr>
                        </thead>
                        <tbody id="row_po_dl_list">

                        </tbody>
                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="addPolisttoGRNBtn" type="button" onclick="addPolisttoGRN()"
                    class="btn btn-primary">Add to GRN <i id="addPolisttoGRNSpin" class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="right_col" role="main">
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm"
            style="text-align: right; font-weight: 600; color: #26b99a!important; margin-top: 1px;">{{
            $title }}
        </div>
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height:130px;">
                    <div class="col-md-2 padding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Item Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('item_type', array(""=> " Select Type") +
                                $item_type->toArray(),isset($item[0]->item_type) ? $item[0]->item_type :null, [
                                'class' => 'form-control item_type',
                                'id' => 'item_type'
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -5px">Supply Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('location', $location, $default_location, [
                                'class' => 'form-control',
                                'id' => 'location',
                                ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Free Stock Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{ date('M-d-Y') }}" style="background: #FFF"
                                    autocomplete="off" readonly id="free_stock_date" class="form-control"
                                    name="free_stock_date">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Free Stock No.</label>
                                <div class="clearfix"></div>
                                <input type="text" style="background: #FFF" autocomplete="off" id="free_stock_no"
                                    readonly value="{{ $free_stock_no }}" class="form-control" name="free_stock_no">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Reason</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" id="freestock_reason" class="form-control"
                                    name="reason">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Remarks</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" id="freestock_remarks" class="form-control"
                                    name="remarks">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="col-md-12 padding_sm">
                            <?php
                                    $charges = \DB::table('purchase_charges_head')
                                        ->where('status', 1)
                                        ->where('is_grn', 1)
                                        ->where('free_stock_entry', 1)
                                        ->orderBy('display_order', 'asc')
                                        ->get();
                                    ?>
                            <table class="table table-bordered table-striped table_sm">
                                <thead>
                                    <tr class="table_header_light_purple_bg ">
                                        <th>Total </th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($charges as $charge)
                                    <tr id="{{ $charge->status_code }}statusrow">
                                        <td>{{ $charge->name }}</td>
                                        <td class="charges_class" style="text-align: right"
                                            id="{{ $charge->status_code }}_amot"></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-top:-8px;text-align: center">
                            <h5 class="blue">{{ $approve_string }}</h5>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="box no-border no-margin box-widget">
                            <div class="box-body  clearfix" style="background: #FFF;">
                                <div class="col-md-12 no-padding">
                                    <table class="table no-margin table_sm no-border grn_total_table">
                                        <tbody>
                                            <tr>
                                                <td width="35%"><label class="text-bold">Amount</label></td>
                                                <td><input type="text" autocomplete="off"
                                                        class="form-control text-right" readonly value="0.00"
                                                        style="font-weight: 700;" readonly="" id="gross_amount_hd"
                                                        name="gross_amount_hh"></td>
                                            </tr>
                                            <tr>
                                                <td width="35%"><label class="text-bold">+Tax</label></td>
                                                <td><input type="text" autocomplete="off"
                                                        class="form-control text-right" readonly value="0.00"
                                                        style="font-weight: 700;" id="tot_tax_amt_hd"
                                                        name="tot_tax_amt_hd"></td>
                                            </tr>
                                            <tr>
                                                <td width="35%"><label class="text-bold">RoundOff</label></td>
                                                <td><input type="text" autocomplete="off" value="0.00"
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        class="form-control text-right" style="font-weight: 700;"
                                                        onchange="getAllItemsTotals()" id="round_off" name="round_off">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%"><label class="text-bold"
                                                        style=" margin: 3px 0 0 0; font-weight: 600 !important;">Total
                                                        Amt.
                                                    </label>
                                                </td>
                                                <td><input style="font-weight: 600 !important;" type="text"
                                                        autocomplete="off" readonly="" class="form-control text-right "
                                                        value="0.00" id="net_amount_hd" name="net_amount_hd">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top: 14px">
                        @if(intval($approve_status)!=2)
                        <button id="saveFreeStockEntryBtn2" class="btn btn-primary btn-block disable_on_save"
                            onclick="saveFreeStockEntry(1,3)" type="button">Save
                            <i id="saveFreeStockEntrySpin2" class="fa fa-save"></i></button>
                        <button id="saveFreeStockEntryBtn3" class="btn btn-success btn-block disable_on_save"
                            onclick="saveFreeStockEntry(2,3)" type="button">Approve
                            <i id="saveFreeStockEntrySpin3" class="fa fa-save"></i></button>
                        @endif
                        <button class="btn btn-warning btn-block" onclick="reloadFreeStock()" type="button">Reload
                            <i class="fa fa-recycle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="col-md-12 padding_sm" style="margin-top: 10px;">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height:500px;">
                    <div class="theadscroll always-visible" style="position: relative; height: 480px;">
                        <table
                            class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed"
                            id="main_row_tbl" style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="2%">#</th>
                                    <th width="20%">
                                        <input id="issue_search_box" onkeyup="searchProducts();" type="text"
                                            autocomplete="off" placeholder="Item Search.. "
                                            style="color: #000;display: none;width: 85%;">
                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                                class="fa fa-search"></i></span>
                                        <span id="item_search_btn_text">Item Name</span>
                                    </th>
                                    <th width="7%">Batch</th>
                                    <th width="7%">Expiry</th>
                                    <th width="5%">UOM</th>
                                    <th width="5%">Conv.</th>
                                    <th width="5%">Qty</th>
                                    <th width="5%">Tot. Qty</th>
                                    <th width="5%">Uom Rate</th>
                                    <th width="5%">Unit Cost</th>
                                    <th width="5%">Unit MRP</th>
                                    <th width="8%">Unit Price</th>
                                    <th width="5%">Tax Per</th>
                                    <th width="7%">Tot.Tax Amt</th>
                                    <th width="8%">Net Rate</th>
                                    <th width="2%"><button type="button" id="addNewGrnRow"
                                            onclick="getNewRowInserted();" class="btn btn-success add_row_btn"><i
                                                id="addNewGrnRowSpin" class="fa fa-plus"></i></button></th>
                                </tr>
                            </thead>
                            <tbody id="row_body_data">

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
<input type="hidden" id="approve_status" value="{{ $approve_status }}">
<input type="hidden" id="row_count_id" value="0">
<input type="hidden" id='hdn_item_history' value="" name="hdn_item_history">
<input type="hidden" id="grn_listrow_id" value="0">
<input type="hidden" id='freestockid' value="{{ $freestock_id }}">
<input type="hidden" id='adjustment_id' value="{{ $freestock_id }}">
<input type="hidden" id='grnSavePosttype' value="0">
<input type="hidden" id="grn_delete_check" value="">
<input type="hidden" id='current_date' value="{{ date('M-d-Y') }}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/free_quantity.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
