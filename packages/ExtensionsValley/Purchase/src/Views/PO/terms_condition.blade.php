<style type="text/css">
    .rm_close{
            position: absolute;
    z-index: 10;
    width: 18px;
    height: 18px;
    border-radius: 100%;
    line-height: 17px;
    font-size: 10px;
    color: #FFF;
    background: #000;
    text-align: center;
    right: 1px;
    top: -11px;
    font-weight: 700;
    cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-md-12 padding_sm">
        <div class="nav nav-tabs sm_nav text-center list_tab">
            <ul class="nav nav-tabs primary_nav" id="termsAndConditions_tab">
                <li id="defaulttemsli" class="active"><a data-toggle="tab" href="#defaulttems_data">Default Terms And Conditions</a></li>
                <li id="additionaltermsli"><a data-toggle="tab" href="#additionaltems_data">Additional Terms And Conditions</a></li>
                <li id="selleracceptanceli" onclick="getSellerFooterContent(1)"><a data-toggle="tab" href="#selleracceptance_data">Seller Acceptance</a></li>
                <li id="footerli" onclick="getSellerFooterContent(2)"><a data-toggle="tab" href="#footer_data">Footer</a></li>
                
            </ul>
            <div class = "tab-content padding_sm">
                <div id = "defaulttems_data" class = "tab-pane active">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                <div class="theadscroll always-visible" style="height: auto; max-height: 300px;">
                                    <table class="table table-striped table-bordered table_sm table_condensed theadfix_wrapper">
                                        <thead>
                                            <tr class="headergroupbg">
                                                <th width="5%" class="text-center"><i class="fa fa-check-circle text-green"></i></th>
                                                <th style="width: 95%">Notes</th>
                                            </tr>
                                        </thead>

                                        <tbody id="default_terms_condition">
                                            @if(count($termsAndConditions_result))
                                            @foreach($termsAndConditions_result as $data)
                                            <?php
                                            $itemflag = '';
                                            if (count($termsAndConditions) != 0) {
                                                if (in_array($data->id, $termsAndConditions)) {
                                                    $itemflag = 'checked';
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td style="text-align: center;">
                                                    <div class="checkbox checkbox-info inline"> 
                                                        <input <?= $itemflag ?> id="purchase_order_terms_remove_add{{ $data->id}}" onclick="purchaseOrderAddRemoveTerms({{$data->id}});" name="purchase_order_terms_remove_add"  class="styled" type="checkbox">
                                                        <label for='purchase_order_terms_remove_add{{ $data->id}}'></label>
                                                    </div>
                                                </td>
                                                <td style="text-align: left">
                                                    {{ $data->value}}
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <!-- <tr><td style="text-align: center;" colspan="2">{{trans('master.records.not_found')}}</td></tr> -->
                                            @endif
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                         <div class="col-md-3">
                         </div>
                         <div class="col-md-3">
                         </div>
                          <div class="col-md-3">
                         </div>
                         <div class="col-md-3" style="padding-right:0%">
                        <input align="right" type="button" name="add_new_terms" id="add_new_terms" value="Add New Terms & Condition" class="btn btn-primary"  onclick="addNewTermsConditionPopup();">
                        </div>
                        <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                    </div>
                </div>
                <div id = "additionaltems_data" class = "tab-pane">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <label class="custom_floatlabel"> <font color="green"> Additional Terms And Conditions</font> </label>
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                <textarea id="additionaltems_text" class = "form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div id = "selleracceptance_data" class = "tab-pane">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <label class="custom_floatlabel"> <font color="green"> Seller Acceptance</font> </label>
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                <textarea style="overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:100%; height: 300px" cols="50" id="seller_acceptance" name="seller_acceptance">
                                <?php foreach($seller_acceptance_footer as $val) { 
                                    if($val->type == 1)
                                    echo $val->value;
                                }?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div id = "footer_data" class = "tab-pane">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <label class="custom_floatlabel"> <font color="green"> Footer</font> </label>
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                 <textarea style="overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:100%; height: 300px" cols="50" id="footer" name="footer">
                                    <?php foreach($seller_acceptance_footer as $val) { 
                                        if($val->type == 2)
                                        echo $val->value;
                                    }?>
                                 </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_new_item_model" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width: 900px !important; width: 100% !important;">
            <div class="modal-content">
                <div class="modal-header bg-orange-active">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title">New Terms & Condition</h4>
                </div>
                <div class="modal-body" style="padding: 7px 15px;">
                    <div id="addNewItemContent">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4"><label>Terms and Condition</label></div>
                                <div class="col-md-4">
                                    <textarea name="new_terms_condition" id="new_terms_condition" class="form-control" ></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button class="btn btn-primary" onclick="saveNewTermsCondition()" style="padding: 3px 4px;margin-top:5%" type="button"><i class="fa fa-podcast"></i> Save</button>
                                     <button class="btn btn-default clearForm" onclick="close_terms_condition_popup()" style="padding: 3px 4px;margin-top:5%" type="button">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal-footer no-padding">
    <button class="btn btn-primary" onclick="termsAndConditionsetbtn()" style="padding: 3px 4px" type="button"><i class="fa fa-podcast"></i> OK</button>
</div>