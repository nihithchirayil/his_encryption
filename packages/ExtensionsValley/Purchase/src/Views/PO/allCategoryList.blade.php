<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;" id="main_row_tbl">
        <thead>
            <tr class="table_header_bg ">
                <th width="5%" style="text-align: center;">
                    <div class="checkbox checkbox-info inline no-margin">
                        <input onclick="selectAllListItem()" id="checkAllListItems" type="checkbox">
                        <label title="Check All" class="text-blue" for="checkAllListItems">
                        </label>
                    </div>
                </th>
                <th width="3%"><i class="fa fa-list"></i></th>
                <th width="9%">Item Code</th>
                <th width="25%">Item Name</th>
                <th width="9%">Ordering Qty</th>
                <th width="9%">Reorder Qty</th>
                <th width="8%">Stock</th>
                <th width="9%">Other Stock</th>
                <th width="8%">Unit Name</th>
                <th width="15%">Sub Category</th>
            </tr>
        </thead>
        <tbody id="purchaseCatageoryDataList">
            <?php
                $k=1000;
                $i = 1;
                if(count($res)!=0){
                    $item_code='';
                    foreach ($res as $each) {
                        $checked = '';
                        if ($item_code!=$each->item_code) {
                            $item_code=$each->item_code;
                            if (in_array($item_code, $code_array))
                            {
                                $checked = 'checked';
                            }
                ?>
            <tr id="addNewRow<?= $from_type . $each->id ?>">
                <td>
                    <div class="checkbox checkbox-info inline no-margin">
                        <input class="checkListItems" <?= $checked ?>
                            onclick="addCategoryListItem(<?= $each->id ?>,<?= $from_type ?>,0)"
                            id="checkListItems<?= $each->id ?>" type="checkbox">
                        <label class="text-blue" for="checkListItems<?= $each->id ?>">
                            <?= $i ?>.
                        </label>
                    </div>
                    <input type="hidden" id="CategoryListItem_unithiiden<?= $each->id ?>" value="<?= $each->units ?>">
                    <input type="hidden" id="list_added_check<?= $from_type . $each->id ?>" value="0">
                </td>
                <td>
                    <button title="Item History" type="button" id="ItemHistoryBtn{{ $k }}"
                        onclick="getIemsHistory({{ $k }},'{{ $each->item_code }}')" style="padding:3px 3px"
                        class="btn btn-warning"><i id="ItemHistoryBtnspin{{ $k }}"
                            class="fa fa-history"></i></button>
                </td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item" id="CategoryListItemitemcode<?= $each->id ?>"><?= $each->item_code ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item" id="listdataitem_description<?= $from_type . $each->id ?>">
                    <?= $each->item_desc ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item"><?= $each->auto_orderable_qty ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item"><?= $each->reorder_level ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item"><?= $each->stock ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item"><?= $each->other_stock ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item"><?= $each->unit_name ?></td>
                <td onclick="addProductListItems(<?= $each->id ?>,<?= $from_type ?>,0)" style="cursor: pointer"
                    class="selectlist_item"><?= $each->sub_category ?></td>
            </tr>
            <?php
                        $i++;
                        $k++;
                     }
                  }
                } else {
                    echo " <tr><td colspan='12' style='text-align: center;'>No Result Found</td></tr>";
                }
                ?>
        </tbody>
    </table>
    <input type="hidden" id="fulllist_itemcount" value='<?= $list_string ?>'>
</div>
