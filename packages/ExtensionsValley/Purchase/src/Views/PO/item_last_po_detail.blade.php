<div class="popup_lastitem" style="width:95%">
    <div onclick="close_last_itemlist();" class="popup_lastitem_close">X</div>
    <p align="left">
        <b>{{$item_name}}</b>
    </p>
    <div class="theadscroll always-visible" style="height: auto; max-height: 300px;">
        <table class="table table-striped no-margin table-bordered table_sm table_condensed theadfix_wrapper">
            <thead>
                <tr class="headergroupbg">
                    <!-- <th>Item Code</th> -->
                    <th>GRN No</th>
                    <th>GRN Date</th>
                    <th>PO  NO</th>
                    <th>PO  Date</th>
                    <th>Invoice  NO</th>
                    <th>Invoice  Date</th>
                    <th>Vendor Name</th>
                    <!-- <th>GRN Unit</th> -->
                    <th>GRN Qty</th>
                    <th>Free Qty</th>
                    <th>Tax %</th>
<!--                    <th>CGST %</th>
                    <th>SGST %</th>
                    <th>IGST %</th>-->
                    <th>Item Dis(-) </th>
                    <th>Bill Dis(-)</th>
                    <!-- <th>Tax Amount</th> -->
                    <th>Rate</th>
                    <th>Unit Cost</th>
                    <th>MRP</th>
                </tr>
            </thead>
            <tbody >
                <?php
                $i = 1;
                if (count($last_po_detail) != 0) {
                    foreach ($last_po_detail as $each) {
                        ?>
                <tr style="text-align: left">
                            <!-- <td>{{$each->r_item_code}}</td> -->
                            <td>{{$each->r_grn_no}}</td>
                            <td>{{$each->r_grn_date}}</td>
                            <td>{{$each->r_po_no}}</td>
                            <td>{{$each->r_po_date}}</td>
                            <td>{{isset($each->r_bill_no)?$each->r_bill_no:''}}</td>
                            <td>{{isset($each->r_bill_date)?$each->r_bill_date:''}}</td>
                            <td style="word-break: break-all;">{{$each->r_vendor_name}}</td>
                            <!-- <td>{{$each->r_grn_unit}}</td> -->
                            <td>{{$each->r_grn_qty}}</td>
                            <td>{{$each->r_free_qty}}</td>
                            <td>{{$each->r_grn_tax_rate}}</td>
<!--                            <td>{{$each->r_grn_cgst_per}}</td>
                            <td>{{$each->r_grn_sgst_per}}</td>
                            <td>{{$each->r_grn_igst_per}}</td>-->
                            <td>{{$each->r_grn_item_discount}}</td>
                            <td>{{$each->r_grn_bill_discount}}</td>
                            <td>{{$each->r_grn_rate}}</td>
                            <td>{{$each->r_unit_cost}}</td>
                            <td>{{$each->r_mrp}}</td>
                        </tr>
                        <?php
                        $i++;
                    }
                } else {
                    echo " <tr><td colspan='15' style='text-align: center;'>No Result Found</td></tr>";
                }
                ?>
            </tbody>

        </table>
    </div>
</div>
