<div id="result_container_div">
    <table id="printReoderListDataList" class="table no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;"width="100%">
        <thead>
            <tr class="table_header_bg ">
                <th width="10%">Item Code</th>
                <th width="25%">Item Name</th>
                <th width="10%">Ordering Qty</th>
                <th width="10%">Reorder Qty</th>
                <th width="10%">Stock</th>
                <th width="10%">Other Stock</th>
                <th width="10%">Unit Name</th>
                <th width="15%">Sub Category</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(count($res)!=0){
                    $item_code='';
                    foreach ($res as $each) {
                        if ($item_code!=$each->item_code) {
                            $item_code=$each->item_code;
                ?>
            <tr>
                <td>
                    <?= $each->item_code ?>
                </td>
                <td>
                    <?= $each->item_desc ?>
                </td>
                <td>
                    <?= $each->auto_orderable_qty ?>
                </td>
                <td>
                    <?= $each->reorder_level ?>
                </td>
                <td>
                    <?= $each->stock ?>
                </td>
                <td>
                    <?= $each->other_stock ?>
                </td>
                <td>
                    <?= $each->unit_name ?>
                </td>
                <td>
                    <?= $each->sub_category ?>
                </td>
            </tr>
            <?php
                     }
                  }
                } else {
                    echo " <tr><td colspan='12' style='text-align: center;'>No Result Found</td></tr>";
                }
                ?>
        </tbody>
    </table>
</div>
