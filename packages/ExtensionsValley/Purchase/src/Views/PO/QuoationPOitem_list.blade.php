<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;" id="main_row_tbl">
        <thead>
            <tr class="table_header_bg ">
                <th width="5%" style="text-align: center;">SI.No.</th>
                <th width="15%">Item Code</th>
                <th width="30%">Item Name</th>
                <th width="10%">Qty</th>
                <th width="10%">Rate</th>
                <th width="10%">Free Qty</th>
                <th width="30%">Comments</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $i = 1;
                if(count($res)!=0){
                    foreach ($res as $each) {
                ?>
            <tr style="cursor: pointer;"
                onclick="addListItem(<?= $each->details_id ?>,<?= $each->item_id ?>,'{{ $each->item_code }}',2);"
                id="addNewRow2<?= $each->item_id ?>">
                <td><?= $i ?><input type="hidden" id="list_added_check2<?= $each->item_id ?>" value="0"> </td>
                <td><?= $each->item_code ?></td>
                <td id="listdataitem_description2<?= $each->item_id ?>"><?= $each->item_desc ?></td>
                <td><?= $each->quantity ?></td>
                <td><?= $each->unit_rate ?></td>
                <td><?= $each->free_qty ?></td>
                <td><?= $each->comments ?></td>
            </tr>
            <?php
                        $i++;
                    }
                } else {
                    echo " <tr><td colspan='7' style='text-align: center;'>No Result Found</td></tr>";
                }
                ?>
        </tbody>
    </table>
</div>
