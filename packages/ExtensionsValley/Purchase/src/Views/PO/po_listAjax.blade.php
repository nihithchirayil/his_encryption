@if (count($po_data) != 0)
    @foreach ($po_data as $data)
        {{ $selected_class = '' }}
        @if ($data->po_id == $selected_id)
            {{ $selected_class = 'danger' }}
        @endif
        <tr class='po_listadata ' id='polistrow_id{{ $data->po_id }}'>
            <td style="text-align: left;">
                {{ $data->po_no ? $data->po_no : '-' }}
            </td>
            <td style="text-align: left;">
                {{ $data->created_at ? $data->created_at : '-' }}
            </td>
            <td style="text-align: left;">
                {{ $data->location_name ? $data->location_name : '-' }}
            </td>
            <td style="text-align: left;">
                {{ $data->vendor_name ? $data->vendor_name : '-' }}
            </td>
            <td style="text-align: left;">
                {{ $data->name ? $data->name : '-' }}
            </td>
            <td style="text-align: left;">
                {{ $data->app_by ? $data->app_by : '-' }}
            </td>
            <td style="text-align: left;">
                {{ !empty($data->status) ? $data->status : '--' }}
            </td>
            <td style="text-align: right;">
                {{ $data->net_total ? $data->net_total : '0' }}
            </td>
            <td style="text-align: center;">
                <?php
                if ($data->mail_status == '0') {
                    $mail_status = "<i title='Mail Queue' class='fa fa-battery-quarter text-red'></i>";
                } elseif ($data->mail_status == '1') {
                    $mail_status = "<i title='Mail Send' class='fa fa-battery-full text-green'></i>";
                } elseif ($data->mail_status == '2') {
                    $mail_status = "<i title='Mail Send Error' class='fa fa-battery-three-quarters text-red'></i>";
                } else {
                    $mail_status = "<i title='Mail Not Send' class='fa fa-battery-empty text-red'></i>";
                }
                echo $mail_status;
                ?>
            </td>
            <td style="text-align: center;">
                <?php $amend='';
        if(isset($data->amend))
            $amend=$data->amend;
            if($data->inserted_from=='1'){
                ?>
                <a href="{{ route('ExtensionsValley.purchase.purchaseOrder', $data->po_id . '-' . $amend) }}"><button
                        class="btn btn-success" style="padding: 0px 2px" type="button"><i
                            id="editpolistdata{{ $data->po_id }}" class="fa fa-pencil-square-o"></i> </button></a>
                <?php
            }else if($data->inserted_from=='2'){
                ?>
                <a href="{{ route('ExtensionsValley.purchase.purchaseQuotationOrder', $data->po_id) }}"><button
                    class="btn btn-success" style="padding: 0px 2px" type="button"><i
                        id="editpolistdata{{ $data->po_id }}" class="fa fa-pencil-square-o"></i> </button></a>
                <?php
            }
            ?>
            </td>
        </tr>
    @endforeach
@endif
