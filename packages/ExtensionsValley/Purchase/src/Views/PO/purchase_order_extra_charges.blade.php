<!-- <div class="tab_pop" style="width: 400px"> -->
    <div>
        <!-- <div class="tab_pop_drag bg-primary" title="Drag"><i class="fa fa-arrows" style="font-size: 11px;"></i></div> -->
        <div class="nav-tabs-custom no-margin">

            <div class="tab-content clearfix" style="padding: 5px;">
                <p align="left" style="margin: 0;font-size: 12px;">
                    <b>{{ $item_name }}</b>
                </p>
                <div class="tab-pane active" id="charge_info">
                    <div class="col-md-12 padding_sm no-padding">
                        <div class="custom-float-label-control" style="margin: 0;">
                            <select class="form-control" onchange="cal_typechange();" id="calcuation_type"
                                name="calcuation_type">
                                <option value="2">Percentage</option>
                                <option value="1">Amount</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="theadscroll always-visible" style="height: auto; max-height: 300px;">
                        <table id="chargecap_tableid"
                            class="table no-margin table-striped table-bordered table_sm table_condensed theadfix_wrapper">
                            <thead class="headergroupbg">
                                <tr>
                                    <th style="width: 35%">Charge Name</th>
                                    <th style="width: 15%">%</th>
                                    <th style="width: 15%">Amount</th>
                                    <!-- <th style="width: 35%">Calculated On</th> -->
                                </tr>
                            </thead>

                            <tbody>
                                @if (count($purchase_order_extra_charges))
                                    @foreach ($purchase_order_extra_charges as $data)
                                        <?php
                                        $read_only = '';
                                        $over_ride = '';
                                        $data_class = '';
                                        if (isset($data->entry_status) && $data->entry_status == 'DAH') {
                                            $read_only = 'readonly=readonly';
                                            $over_ride = 'data-entry-type=0';
                                            $data_class = 'data-entry-type';
                                        }
                                        $dis_per = 0.0;
                                        $dis_amt = 0.0;
                                        if (isset($item_price[$data->name])) {
                                            $per_val = explode('::::', $item_price[$data->name]);
                                            $dis_per = @$per_val[0] ? $per_val[0] : 0.0;
                                            $dis_amt = @$per_val[1] ? $per_val[1] : 0.0;
                                        }

                                        ?>
                                        <tr class="chargecap_table">
                                            <td style="text-align: left;">
                                                {{ $data->name }}
                                            </td>
                                            <td style="text-align: left;">
                                                <input {{ $over_ride }} id="charge_percentage{{ $data->id }}"
                                                    myAttri="myVal"
                                                    onblur="calculationExtraCharges(this,'percentage',{{ $item_id }},'{{ $item_code }}','{{ $data->calc_code }}','{{ $data->charge_code }}',{{ $data->id }},{{ $data->is_tax }},{{ $data->is_exclusive }})"
                                                    class="form-control number_class calcu_percent {{ $data_class }}"
                                                    onkeypress='restrictMinusSign(event);' min='0' value="<?= $dis_per ?>"
                                                    name="charge_percentage[]" placeholder="Percentage">
                                                <input id="charge_type{{ $data->id }}" value="{{ $data->calc_code }}"
                                                    type="hidden">

                                            </td>
                                            <td style="text-align: left;background-color: #eee;">
                                                <input {{ $over_ride }} id="charge_amount{{ $data->id }}"
                                                    onblur="calculationExtraCharges(this,'amount',{{ $item_id }},'{{ $item_code }}','{{ $data->calc_code }}','{{ $data->charge_code }}',{{ $data->id }},{{ $data->is_tax }},{{ $data->is_exclusive }})"
                                                    class="form-control number_class calcu_amount {{ $data_class }}"
                                                    onkeypress='restrictMinusSign(event);' min='0' value="<?=$dis_amt?>"
                                                    name="charge_amount[]" placeholder="Amount" style="border-color: #eee;">
                                            </td>


                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td style="text-align: center;" colspan="2">{{ trans('master.records.not_found') }}
                                        </td>
                                    </tr>
                                @endif
                            </tbody>

                        </table>
                        <!--  <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="col-md-12 no-padding text-left">
                            <button class="btn btn-success" type="button" onclick="item_codecharge('{{ $item_code }}','{{ $detail_id }}','{{ $item_id }}');" style="padding: 2px"><i id="annexture_postspin"></i> OK</button>
                        </div> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht5"></div>


                    <div class="modal-footer">

                        <button data-dismiss="modal" class="btn btn-success" type="button"
                            onclick="item_codecharge('{{ $item_code }}','{{ $detail_id }}','{{ $item_id }}');"
                            style="padding: 2px"><i class="fa fa-save"></i> OK</button>
                    </div>

                    <div class="col-md-12 no-padding text-left">

                    </div>
                    <!-- <div class = "custom-float-label-control" style="margin: 5px 0 0 0;">
                        <input id="charge_name_text" onkeyup="validate_number(this, 1)" readonly="readonly" class="form-control" type="text" value="" name="charge_name_text" placeholder="Charge Text">
                    </div> -->
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="other_charge_info">
                    <div class="panel-body" style="padding: 10px;">
                        <div class="panel panel-default">
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <input id="orther_charges_info_text" onkeyup="validate_number(this, 1)" class="form-control"
                                type="text" value="" name="orther_charges_info_text" placeholder="Other Charge Info">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="delivery_note">
                    <div class="panel-body" style="padding: 10px;">
                        <div class="panel panel-default">
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <input id="delivery_noteinfotext" onkeyup="validate_number(this, 1)" class="form-control"
                                type="text" value="" name="delivery_noteinfotext" placeholder="Delivery  Notes Info.">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="stagger">
                    <div class="panel-body" style="padding: 10px;">
                        <div class="panel panel-default">
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <input id="stagger_textnotes" onkeyup="validate_number(this, 1)" class="form-control"
                                type="text" value="" name="stagger_textnotes" placeholder="Stagger Deliver">
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- <div class="modal-footer  no-padding">
           <button class="btn btn-danger" onclick="close_charge_model()" style="padding: 3px 4px" type="button">Close</button>

        </div> -->
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            //product_each_charge['discount'][item_code]
            var free = $('#free_qtyid' + {{ $item_id }}).val();
            if (free <= 0)
                $("input.FREE").attr("readonly", true);
            else
                $("input.FREE").removeAttr("readonly");
            setTimeout(function() {
                var item_code = '<?= $item_code ?>';
                var item_id = '<?= $item_id ?>';
                if (product_each_charge['discount'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['discount'][item_id][item_code]['percentage'], function(i, val) {
                        var charge_id = parseFloat(i);
                        var percentage = parseFloat(val);
                        $('#charge_percentage' + charge_id).val(percentage);
                        $('#charge_percentage' + charge_id).blur();
                    });
                }

                if (product_each_charge['tax'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['tax'][item_id][item_code]['percentage'], function(i, val) {
                        $('#charge_percentage' + parseFloat(i)).val(parseFloat(val));
                        $('#charge_percentage' + parseFloat(i)).blur();
                    });
                }

                if (product_each_charge['on_tax_amount'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['on_tax_amount'][item_id][item_code]['percentage'], function(i, val) {
                        $('#charge_percentage' + parseFloat(i)).val(parseFloat(val));
                        $('#charge_percentage' + parseFloat(i)).blur();
                    });
                }
                if (product_each_charge['tax_free'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['tax_free'][item_id][item_code]['percentage'], function(i, val) {
                        $('#charge_percentage' + parseFloat(i)).val(parseFloat(val));
                        $('#charge_percentage' + parseFloat(i)).blur();
                    });
                }
                var detil = JSON.stringify(product_each_charge);
                if (product_each_charge['othch'].hasOwnProperty(item_id)) {
                    $.each(product_each_charge['othch'][item_id][item_code]['percentage'], function(i, val) {
                        $('#charge_amount' + parseFloat(i)).val(parseFloat(val));
                        $('#charge_percentage' + parseFloat(i)).val(parseFloat(val));
                    });
                }
            }, 1500);
        });
        /*restict negative values*/
        function restrictMinusSign(e) {
            var inputKeyCode = e.keyCode ? e.keyCode : e.which;
            if (inputKeyCode != null) {
                if (inputKeyCode == 45) e.preventDefault();
                if (inputKeyCode == 43) e.preventDefault();
            }
        }
    </script>
