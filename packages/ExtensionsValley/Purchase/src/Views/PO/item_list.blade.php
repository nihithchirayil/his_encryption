
<?php
$product_array = json_decode($request_form['item_array'], TRUE);
$check_array = array();
foreach ($product_array as $key => $val) {
    array_push($check_array, rawurldecode($key));
}
if ($request_form['item_flag'] == '1') {
    ?>
<style type="text/css">
    .multiselect-clear-filter .glyphicon-remove-circle{
            font-size: 20px !important;
    }
    .input-group .multiselect-search{
            height: 30px !important;
            padding: 10px !important;
    }

</style>
    <div class="row">
        <div class="col-md-12">
             <div class="col-md-3 padding_sm">
                <div class="custom-float-label-control">
                    <label class="custom_floatlabel">Product Category</label>
                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <?php
                    $listdept = \DB::table('category')->distinct('category.name')
                            ->where('status','=',1)
                            ->pluck('category.name', 'category.id');
                    ?>
                    {!! Form::select('from_category',$listdept,'',['class' => 'form-control custom_floatinput selectbox_initializr multi_cat','onchange'=>'get_subcategory(this)','placeholder' => 'Category','title' => 'Category','id' => 'from_category','style' => 'width:100%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="custom-float-label-control" id='getsubcategory_code_div_main' style="">
                    <label class="custom_floatlabel">Product Sub Category</label>
                    <div class="clearfix"></div>
                    <div id="getsubcategory_code_div" class="ht5" style="padding-top: 5px;">
                        {!! Form::select('from_category',[],'',['class' => 'form-control custom_floatinput selectbox_initializr multi_cat','style' => 'width:100%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}

                    </div>

                </div>
            </div>
            <div class="col-md-1 padding_sm hide">
            <div class="custom-float-label-control">
            <label class="custom_floatlabel">Contains
            </label>
                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <input type="checkbox" id="all_search" name="all_search" value="1">
            </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="custom-float-label-control">
                    <label class="custom_floatlabel">Product Description</label>

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <input type="text" name="item_desc_search_new" autocomplete="off" class="form-control custom_floatinput" id="item_desc_search_new"  placeholder="Item Description">
                    <div id="item_desc_newsearchCodeAjaxDiv" style="margin: 0; z-index: 1005; font-size: 12px; width: 100%" class="ajaxSearchBox"></div>
                    <input type="hidden" name="item_newdesc_listsearch" id="item_newdesc_listsearch" />

                </div>
            </div>

            <div class="col-md-3 pull-right padding_sm">
                <div class="checkbox checkbox-primary checkbox-inline">
                     <input  id="reorderlevelcheck" class="styled" type="checkbox">
                    <label for="reorderlevelcheck">Re-order Level Items</label>
                </div>
            </div>
            <div class="col-md-3 pull-right padding_sm" >
                 <div class="checkbox checkbox-primary checkbox-inline">
                    <input  id="search_all_item" class="styled" type="checkbox">
                    <label for="search_all_item">All Product (Note:This filter option will not shows stock & re-order level)</label>
                </div>
                <button type="button" onclick="AddNewProduct(0, 2,0,'');" class="btn btn-success"><i id="search_itemdataspin" class="fa fa-search"></i> Search</button>
                <button type="button" style='padding: 2px 15px;' class="btn btn-success" onclick="add_dataitemlist();"><i class="fa fa-paper-plane-o"> OK</i></button>
            </div>
            <div class="col-md-12 padding_sm">
                <div class="checkbox checkbox-primary checkbox-inline">
                     <input  name="all_chk_item_list_cls" id="all_chk_item_list_cls" class="styled all_chk_item_list_cls" type="checkbox">
                    <label for="reorderlevelcheck">Select All Items</label>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="h10"></div>
            <div id="search_data_getdiv" class="col-md-12 padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="theadscroll always-visible" id="newitem_scroll" style="height: auto; max-height: 355px;min-height: 355px;">
                        <table class="table table-striped table-bordered table_sm table_condensed theadfix_wrapper">
                            <thead>
                                <tr class="headergroupbg" style="background: #26b99a;color: #fff;">
                                    <th width="5%" style="text-align: center;"></th>
                                    <th width="10%">Product Code</th>
                                    <th width="25%">Product Name</th>
                                    <th width="30%">Category</th>
                                    @if($request_form['all_product'] == '0')
                                    <th width="10%">Stock</th>
                                    <th width="10%">Reorder Level</th>
                                    @endif
                                    <th width="10%">Purchase Unit</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_itemdata">
                                <?php
                                $i = 1;
                                if (count($request_form['res']) != 0) {
                                    foreach ($request_form['res'] as $each) {
                                        $itemflag = '';
                                        if ($product_array) {
                                            if (in_array($each->item_code, $check_array)) {
                                               // $itemflag = 'checked';
                                            }
                                        }
                                        ?>
                                        <tr class='{{$each->item_code}}'>


                                            <?php if ($each->purchase_unit == '') { ?>
                                            <td style="text-align: center;">--</td>
                                            <?php } else { ?>
                                            <td style="text-align: center;">
                                                <div class="checkbox checkbox-success">
                                                    <input <?= $itemflag ?> onclick="newitem_manage('{{ $each->item_code}}');" id="itemdetailselect{{$each->item_code}}" class="styled chk_item_list_cls" type="checkbox">
                                                    <label for="itemdetailselect{{$each->item_code}}"></label>
                                                </div>
                                            </td>
                                        <?php } ?>

                                            <td>{{$each->item_code}}</td>
                                            <td id="item_descdata{{$each->item_code}}">{{$each->item_desc}}<input type="hidden" value="{{$each->item_code}}" id="item_desc_short{{$each->item_code}}"></td>
                                            <td>{{$each->cat_name}}</td>
                                            @if($request_form['all_product'] == '0')
                                            <td id="item_stockdata{{$each->item_code}}" class="number_class" >{{$each->stock}}</td><td id="item_reorder{{$each->item_code}}" class="number_class" >{{$each->reorder_level}}</td>
                                            @endif
                                            <td id="item_unitdata{{$each->item_code}}" >
                                                <input type="hidden" id="item_unit_id{{$each->item_code}}" value="{{$each->purchase_unit}}">
                                                <input type="hidden" id="item_purchase_unit_id{{$each->item_code}}" value="{{$each->purchase_unit}}">
                                                <input type="hidden" id="item_purchase_unit_name{{$each->item_code}}" value="{{$each->purchase_unit_name}}">
                                                {{$each->purchase_unit_name}}</td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    echo " <tr><td colspan='7' style='text-align: center;'>No Result Found</td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
<style>
.foo {
  float: left;
  width: 20px;
  height: 20px;
  margin: 5px;
  border: 1px solid rgba(0, 0, 0, .2);
}

.blue {
  background: lightblue;
}
.lightyellow{
   /* background: lightyellow;*/
    background: #ccffcc;
}
.purple {
  background: #ab3fdd;
}

.wine {
  background: #ffb399;
}
    </style>

            </div>
        </div>
    </div>
    <script>
        //         $('.selectbox_initializr').selectpicker({
        // liveSearch: true,
        //         maxOptions: 1
        // });
                $('.form-control').keyup(function (e) {
        $(this).val($(this).val().toUpperCase());
        });
                $('.ajaxSearchBox').bind('DOMNodeInserted DOMNodeRemoved', function() {
        $('html').one('click', function() {
        $(".ajaxSearchBox").hide();
        });
        });
                $('#item_desc_search_new').keyup(delay(function(event){
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
                var value = event.key;
                var current;
                var list_all = $('#search_all_item:checked').val();
                var all_search = $('#all_search:checked').val();
                if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var item_desc_search = $(this).val();
         var loc = $('#deprt_list').val();
                item_desc_search = item_desc_search.trim();
                if (item_desc_search == "") {
                $("#item_desc_newsearchCodeAjaxDiv").html("");
                        $("#item_newdesc_listsearch").val("");
                } else {
        try {
        var url = '';
                var param = {item_desc_newsearch:item_desc_search,loc:loc,list_all:list_all,all_search:all_search};
                $.ajax({
                type: "GET",
                        url: '',
                        data: param,
                        beforeSend: function () {
                        $("#item_desc_newsearchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                        },
                        success: function (html) {
                        if (html == 'No Result Found'){
                        toLocationPub = '';
                        }
                        $("#item_desc_newsearchCodeAjaxDiv").html(html).show();
                                $("#item_desc_newsearchCodeAjaxDiv").find('li').first().addClass('liHover');
                        },
                        complete: function () {
                        //  $('#loading_image').hide();
                        }
                });
        }
        catch (err) {
        document.getElementById("demo").innerHTML = err.message;
        }
        }
        } else {
        ajax_list_key_down('item_desc_newsearchCodeAjaxDiv', event);
        }
        },1000));
                $('#item_desc_search').on('keydown', function(event){
        if (event.keyCode === 13){
        ajaxlistenter('item_desc_newsearchCodeAjaxDiv');
                return false;
        }
        });
                var col = $('#newitem_scroll');
                window.offset = 0;
                col.scroll(function () {
                if (col.outerHeight() >= (col.get(0).scrollHeight - (col.scrollTop()+50))){
                window.offset += 15;
               // alert('in oneee')
                        AddNewProduct(window.offset, 3,0,'');
                }
                });
    <?php
    foreach ($product_array as $key => $val) {
        ?>
            var item_code_get = '<?= $key ?>';
                    var item_desc_get = '<?= $val['itemdesc'] ?>';
                    $("#appentitem_codediv").append("<span value='" + rawurldecode(item_code_get) + "' onclick='stockRemove(this);' id='ietmcodeitemdesc" + rawurldecode(item_code_get) + "' class='close_btnspan tag label-primary'>" + rawurldecode(item_code_get) + ':' + rawurldecode(item_desc_get) + "<span class='item_code_close'><i class='fa fa-times'></i></span></span><div class='clearfix'></div>");
                    setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                            scrollContainer: true
                    });
                    }, 400);
                    $('.theadscroll').perfectScrollbar({
            minScrollbarLength: 30,
                    suppressScrollX: true
            });
        <?php
    }
    ?>

</script>
<?php
    } else if ($request_form['item_flag'] == '2') {
?>
    <div class="col-md-12 padding_sm">
        <div class="theadscroll always-visible" id="newitem_scroll" style="height: auto; max-height: 355px;">
            <table class="table table-striped table-bordered table_sm table_condensed theadfix_wrapper">
                <thead>
                    <tr class="headergroupbg" style="background: #26b99a;color: #fff;">
                        <th width="5%" style="text-align: center;"><i class="fa fa-plus text-green"></i></th>
                        <th width="10%">Item Code</th>
                        <th width="25%">Item Name</th>
                        <th width="30%">Category</th>
                        @if($request_form['all_product'] == '0')
                        <th width="10%">Stock</th>
                        <th width="10%">Reorder Level</th>
                        @endif
                        <th width="10%">Issue Unit</th>
                    </tr>
                </thead>
                <tbody id="tbody_itemdata">

                    <?php
                    $i = 1;
                    if (count($request_form['res']) != 0) {
                        foreach ($request_form['res'] as $each) {
                            $itemflag = '';
                            if ($product_array) {
                                if (in_array($each->item_code, $check_array)) {
                                  //  $itemflag = 'checked';
                                }
                            }
                            ?>
                            <tr class='{{$each->item_code}}'>

                            <?php if ($each->purchase_unit == '') { ?>
                                <td style="text-align: center;">--</td>
                            <?php } else { ?>
                                <td style="text-align: center;">
                                    <div class="checkbox checkbox-success">
                                        <input <?= $itemflag ?> onclick="newitem_manage('{{ $each->item_code}}');" id="itemdetailselect{{$each->item_code}}" class="styled chk_item_list_cls" type="checkbox" data="{{$each->item_desc}}" >
                                        <label for="itemdetailselect{{$each->item_code}}"></label>
                                    </div>
                                </td>
                            <?php } ?>
                                <td>{{$each->item_code}}</td>
                                <td id="item_descdata{{$each->item_code}}">{{$each->item_desc}}<input type="hidden" value="{{$each->item_code}}" id="item_desc_short{{$each->item_code}}"></td>
                                <td>{{$each->cat_name}}</td>
                                @if($request_form['all_product'] == '0')
                                <td id="item_stockdata{{$each->item_code}}" class="number_class">{{$each->stock}}</td>
                                <td id="item_reorder{{$each->item_code}}" class="number_class" >{{$each->reorder_level}}</td>
                                @endif
                                <td id="item_unitdata{{$each->item_code}}" >
                                    <input type="hidden" id="item_unit_id{{$each->item_code}}" value="{{$each->purchase_unit}}">
                                    <input type="hidden" id="item_purchase_unit_id{{$each->item_code}}" value="{{$each->purchase_unit}}">
                                    <input type="hidden" id="item_purchase_unit_name{{$each->item_code}}" value="{{$each->purchase_unit_name}}">
                                    {{$each->purchase_unit_name}}</td>
                            </tr>
                            <?php
                            $i++;
                        }
                    } else {
                        echo " <tr><td colspan='7' style='text-align: center;'>No Result Found</td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>


<style>
.foo {
  float: left;
  width: 20px;
  height: 20px;
  margin: 5px;
  border: 1px solid rgba(0, 0, 0, .2);
}

.blue {
  background: lightblue;
}
.lightyellow{
    /*background: lightyellow;*/
    background: #ccffcc;
}
.purple {
  background: #ab3fdd;
}

.wine {
  background: #ffb399;
}
</style>
<script>
    var col = $('#newitem_scroll');
    window.offset = 0;
    col.scroll(function () {
        if (col.outerHeight() >= (col.get(0).scrollHeight - (col.scrollTop()+50))){
        // alert('twoooo');
            window.offset += 15;
            AddNewProduct(window.offset, 3,0,'');
        }
    });
    <?php
    foreach ($product_array as $key => $val) {
        ?>
            var item_code_get = '<?= $key ?>';
                    var item_desc_get = '<?= $val['itemdesc'] ?>';
                    $("#appentitem_codediv").append("<span value='" + rawurldecode(item_code_get) + "' onclick='stockRemove(this);' id='ietmcodeitemdesc" + rawurldecode(item_code_get) + "' class='close_btnspan tag label-primary'>" + rawurldecode(item_code_get) + ':' + rawurldecode(item_desc_get) + "<span class='item_code_close'><i class='fa fa-times'></i></span></span><div class='clearfix'></div>");
                    setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                            scrollContainer: true
                    });
                    }, 400);
                    $('.theadscroll').perfectScrollbar({
            minScrollbarLength: 30,
                    suppressScrollX: true
            });
        <?php
    }
    ?>
    </script>
    <?php
} else if ($request_form['item_flag'] == '3') {
    ?>
    <?php
    $i = 1;
    foreach ($request_form['res'] as $each) {
        $itemflag = '';
        if ($product_array) {
            if (in_array($each->item_code, $check_array)) {
             //   $itemflag = 'checked';
            }
        }
        ?>
        <tr class='{{$each->item_code}}'>


        <?php if ($each->purchase_unit == '') { ?>
                    <td style="text-align: center;">--</td>
        <?php } else { ?>
                    <td style="text-align: center;">
                        <div class="checkbox checkbox-success">

                            <input <?= $itemflag ?> onclick="newitem_manage('{{ $each->item_code}}');" id="itemdetailselect{{$each->item_code}}" class="styled chk_item_list_cls" type="checkbox">
                            <label for="itemdetailselect{{$each->item_code}}"></label>
                        </div>
                    </td>

        <?php }
        ?>
            <td>{{$each->item_code}}</td>
            <td id="item_descdata{{$each->item_code}}">{{$each->item_desc}}<input type="hidden" value="{{$each->item_code}}" id="item_desc_short{{$each->item_code}}"></td>
            <td>{{$each->cat_name}}</td>
            @if($request_form['all_product'] == '0')
            <td id="item_stockdata{{$each->item_code}}" class="number_class">{{$each->stock}}</td>
            <td id="item_reorder{{$each->item_code}}" class="number_class" >{{$each->reorder_level}}</td>
            @endif
            <td id="item_unitdata{{$each->item_code}}" >
                <input type="hidden" id="item_unit_id{{$each->item_code}}" value="{{$each->purchase_unit}}">
                <input type="hidden" id="item_purchase_unit_id{{$each->item_code}}" value="{{$each->purchase_unit}}">
                <input type="hidden" id="item_purchase_unit_name{{$each->item_code}}" value="{{$each->purchase_unit_name}}">
                {{$each->purchase_unit_name}}</td>
        </tr>
        <?php
        $i++;
    }
    ?>
    <?php
}
?>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script> -->
<script>

    //  $('#from_category').multiselect({
    //     enableClickableOptGroups: true,
    //     enableCollapsibleOptGroups: true,
    //     enableFiltering: true,
    //     includeSelectAllOption: true,
    //     minWidth: 180,
    //     buttonWidth: '252px',
    //     maxHeight: 400,
    //     enableCaseInsensitiveFiltering: true,
    // });
    // $('#consume_type').multiselect({
    //     enableClickableOptGroups: true,
    //     enableCollapsibleOptGroups: true,
    //     enableFiltering: true,
    //     includeSelectAllOption: true,
    //     minWidth: 300,
    //     buttonWidth: '250px',
    //     maxHeight: 400,
    // });
</script>
