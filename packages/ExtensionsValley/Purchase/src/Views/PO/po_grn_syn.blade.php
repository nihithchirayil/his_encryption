<div class="popup_sync" style="height:auto">
    <div onclick="popup_sync_close();" class="popup_sync_close">X</div>
        <table class="table table-striped no-margin table-bordered table_sm table_condensed t">
            <thead>
                <tr class="headergroupbg">
                    <th>PO NO</th>
                    <th>GRN No</th>
                    <th>Item Desc</th>
                    <th>PO Entry Amount</th>
                    <th>GRN Entry Amount</th>
                   <!--  <th></th> -->
                   
                </tr>
            </thead>
            <tbody >
                <?php
                $i = 1;
                if (count($grn_po_detail) != 0) {
                    foreach ($grn_po_detail as $each) {
                        ?>
                <tr style="text-align: left">
                            <th>{{$each->po_no}}</th>
                            <th>{{$each->grn_no}}</th>
                            <th>{{$each->item_tooltip}}</th>
                            <th>{{$each->po_total}}</th>
                            <th>{{$each->grn_total}}</th>
                           <!--  <td><button type="button" onclick='po_grn_sync(<?=$grn_charges_detail?>,<?=$item_id?>,<?=$each->item_code?>)' class="btn bg-aqua-active btn-block"><i id="po_grn_sync" class="fa fa-sync"></i> Sync GRN Details</button><td> -->
                           
                        </tr>
                        <?php
                        $i++;
                    }
                } else {
                    echo " <tr><td colspan='13' style='text-align: center;'>No Result Found</td></tr>";
                }
                ?>
            </tbody>

        </table>

        <div class="clearfix"></div>
        <div class="h10"></div>

        <!-- GRN AND PO DETAILS -->
         <table class="table table-striped no-margin table-bordered table_sm table_condensed t">
           
            <thead>
                <tr>
                    <th colspan="2" style="text-align: center; background: #C5CAE9;">Rate</th>
                    <th colspan="2" style="text-align: center; background: #E1BEE7;">MRP</th>
                    <th colspan="2" style="text-align: center; background: #FFCDD2;">Qty</th>
                    <th colspan="2" style="text-align: center; background: #BBDEFB;">Free Qty</th>
                </tr>
                <tr>
                    <th style="text-align: center; background: #C5CAE9;">PO</th>
                    <th style="text-align: center; background: #C5CAE9;">GRN</th>
                    <th style="text-align: center; background: #E1BEE7;">PO</th>
                    <th style="text-align: center; background: #E1BEE7;">GRN</th>
                    <th style="text-align: center; background: #FFCDD2;">PO</th>
                    <th style="text-align: center; background: #FFCDD2;">GRN</th>
                    <th style="text-align: center; background: #BBDEFB;">PO</th>
                    <th style="text-align: center; background: #BBDEFB;">GRN</th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($grn_po_detail) != 0) {
                    foreach ($grn_po_detail as $each) { ?>
                <tr>
                    <th style="text-align: center;">@if(isset($each->po_per_rate)) {{$each->po_per_rate}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->grn_per_rate)) {{$each->grn_per_rate}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->po_mrp)) {{$each->po_mrp}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->grn_mrp)) {{$each->grn_mrp}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->po_per_qty)) {{$each->po_per_qty}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->grn_per_qty)) {{$each->grn_per_qty}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->po_free_qty)) {{$each->po_free_qty}} @endif</th>
                    <th style="text-align: center;">@if(isset($each->grn_free_qty)) {{$each->grn_free_qty}} @endif</th>
                </tr>
            <?php }
                } ?>
            </tbody>
        </table>
       

        <div class="clearfix"></div>
        <div class="h10"></div>


        <table class="table table-striped no-margin table-bordered table_sm table_condensed t">
            <thead>
                 <tr class="headergroupbg">
                  <th align="center"></th>
                    <th colspan="2" style="text-align: center;">PO</th>
                    <th colspan="2" style="text-align: center;">GRN</th>
                </tr>
                <tr class="headergroupbg">
                    <th style="text-align: center;">Charge Name</th>
                    <th style="text-align: center;">%</th>
                    <th style="text-align: center;">Amount</th>
                    <th style="text-align: center;">%</th>
                    <th style="text-align: center;">Amount</th>
                </tr>
                <?php if(count($po_grn_charge_array) > 0) { foreach($po_grn_charge_array as $k => $v) { ?>
                <tr style="text-align: left">
                            <th>{{$k}}</th>
                            <th>@if(isset($v['po']['perc'])) {{$v['po']['perc']}} @endif</th>
                            <th>@if(isset($v['po']['amnt'])) {{$v['po']['amnt']}} @endif</th>
                            <th>@if(isset($v['grn']['perc'])) {{$v['grn']['perc']}} @endif</th>
                            <th>@if(isset($v['grn']['amnt'])) {{$v['grn']['amnt']}} @endif</th>   
                        </tr>
                <?php } } else {  ?>
                <tr style="text-align: left">
                    <th colspan="5" style='text-align: center;'>No Result Found</th>
                </tr>
                <?php } ?>
            </thead>
            <tbody >
                
            </tbody>
        </table>
       

        <div class="clearfix"></div>
        <div class="h10"></div>


     <div class="col-md-2 no-padding pull-right">
         <button type="button" onclick='po_grn_sync(<?=$grn_charges_detail?>,<?=$item_id?>,"<?=$each->item_code?>")' class="btn bg-aqua-active btn-block"><i id="po_grn_sync" class="fa fa-sync"></i> Sync GRN Details</button>
     </div>   
</div>
