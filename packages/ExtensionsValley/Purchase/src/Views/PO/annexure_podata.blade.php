<div class="row">
    <div class="col-md-12 padding_sm">
        <div class="nav nav-tabs sm_nav text-center list_tab">
            <ul class="nav nav-tabs primary_nav">
                <li id="annexture_ref1li" class="active"><a data-toggle="tab" href="#annexture_ref1">Reference 1</a></li>
                <li id="annexture_ref12i"><a data-toggle="tab" href="#annexture_ref2">Reference 2</a></li>
                <li id="annexture_li"><a data-toggle="tab" href="#annexture_data">Annexure</a></li>
            </ul>
            <div class = "tab-content padding_sm">
                <div id = "annexture_ref1" class = "tab-pane active">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <label class="custom_floatlabel"> <font color="purple">Reference 1</font> </label>
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                <textarea id="annexture_ref1_text" rows="4" cols="70" class = "form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div id = "annexture_ref2" class = "tab-pane">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <label class="custom_floatlabel"> <font color="blue">Reference 2</font> </label>
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                <textarea id = "annexture_ref2_text" rows="4" cols="70" class = "form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div id = "annexture_data" class = "tab-pane">
                    <div class = "panel-body" style = "padding: 10px;">
                        <div class = "panel panel-default">
                            <div class = "custom-float-label-control">
                                <label class="custom_floatlabel"> <font color="green">Annexture</font> </label>
                                <div class = "clearfix"></div>
                                <div class = "ht5"></div>
                                <textarea id = "annexture_data_text" rows="4" cols="70" class = "form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal-footer  no-padding">
    <button class="btn btn-danger" data-dismiss="modal" style="padding: 3px 4px" type="button"><i class="fa fa-times-circle-o"></i> Close</button>
    <button class="btn btn-success" type="button" onclick="set_annexuretext()" style="padding: 3px 4px"><i id="annexture_postspin" class="fa fa-podcast"></i> OK</button>
</div>