<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Purchase Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:#36a693;color:rgb(0, 0, 0);border-spacing: 0 1em;font-family:sans-serif">
                        <th width='4%'>Sn. No.</th>
                        <!-- <th width='8%'>PO NO</th> -->
                        <th width='8%'>Product Name</th>
                        <th width='8%'>Supplier Name</th>
                        <th width='8%'>Store</th>
                        <th width='8%'>Unit Name</th>
                        <!-- <th width='8%'>PO Date</th> -->
                        <th width='8%'>Status</th>
                        <th width='8%'>Qty</th>
                        <th width='8%'>Free Qty</th>
                        <th width='8%'>Pending </th>
                        <th width='8%'>Purchase Rate</th>
                        <th width='8%'>Disc. Amount </th>
                        <th width='8%'>Net Amt</th>
                        <!-- <th width='8%'>PO Amount</th> -->
                    </tr>
                </thead>
                <tbody>
                    @if (count($res) != 0)
                    @php
                    $total_qty = 0;
                    $total_fqty = 0;
                    $total_pen = 0;
                    $total_pr = 0.0;
                    $total_po_amount = 0.0;
                    $total_disc_amount = 0.0;
                    $po_idArray = [];
                    $i=0;
                    @endphp
                    @foreach ($res as $data)
                    <?php
                     $total_qty += $data->pur_qnty;
                     $total_fqty += $data->free_qty;
                     $total_pen += $data->po_balance_qty;
                     $total_pr += $data->pur_rate;
                     $total_disc_amount += $data->total_disc_amount;                     
                     $po_head_id = @$data->po_id ? $data->po_id : 0;

                            if (!in_array($data->po_id, $po_idArray)) {      
                                $i=0;                     
                                $po_idArray[]= $data->po_id;
                            ?>
                    <tr
                        style="background-color:rgb(182 225 218);color: black;border-spacing: 0 1em;font-family: sans-serif;">
                        <th colspan="2" class="common_td_rules">
                            PO: {{ $data->po_no }}
                        </th>
                        <th colspan="2" class="common_td_rules">
                            PO Date: {{ date('d-M-Y', strtotime($data->po_date)) }}
                        </th>
                        <th colspan="2" class="common_td_rules">
                            Status: {{ $data->status }}
                        </th>
                        <th colspan="6" class="common_td_rules">
                            Discount Amount: {{ $data->bill_discount_amount }}
                        </th>
                    </tr>
                    <tr>
                        <td class="common_td_rules">{{ $loop->iteration }}.</td>
                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                        <td class="common_td_rules">{{ $data->vendor_name }}</td>
                        <td class="common_td_rules">{{ $data->store }}</td>
                        <td class="common_td_rules">{{ $data->name }}</td>
                        <td class="common_td_rules">{{ $data->status }}</td>
                        <td class="td_common_numeric_rules">{{ $data->pur_qnty }}</td>
                        <td class="td_common_numeric_rules">{{ $data->free_qty }}</td>
                        <td class="td_common_numeric_rules">{{ $data->po_balance_qty }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->pur_rate, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->total_disc_amount, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->total, 2) }}</td>
                    </tr>
                    <?php 
                        } else {
                            ?>
                    <tr>
                        <td class="common_td_rules">{{ $loop->iteration }}.</td>
                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                        <td class="common_td_rules">{{ $data->vendor_name }}</td>
                        <td class="common_td_rules">{{ $data->store }}</td>
                        <td class="common_td_rules">{{ $data->name }}</td>
                        <td class="common_td_rules">{{ $data->status }}</td>
                        <td class="td_common_numeric_rules">{{ $data->pur_qnty }}</td>
                        <td class="td_common_numeric_rules">{{ $data->free_qty }}</td>
                        <td class="td_common_numeric_rules">{{ $data->po_balance_qty }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->pur_rate, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->total_disc_amount, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->total, 2) }}</td>

                    </tr>
                    <?php }
                     if ($i == $po_cnt[$po_head_id]) {
                    ?>
                    <tr>
                        <th class="td_common_numeric_rules" style="text-align: left !important" colspan="11">Net Total
                        </th>
                        <th class="td_common_numeric_rules">{{ number_format($data->net_total, 2) }}</th>
                    </tr>
                    <?php
                     $total_po_amount += $data->net_total;
                    } $i++;?>
                    @endforeach
                    <tr style="color:#000; background: #36a693;">
                        <th class="common_td_rules" colspan="6">Total</th>
                        <th class="th_common_numeric_rules">{{ number_format($total_qty, 2) }}</th>
                        <th class="th_common_numeric_rules">{{ number_format($total_fqty, 2) }}</th>
                        <th class="th_common_numeric_rules">{{ number_format($total_pen, 2) }}</th>
                        <th class="th_common_numeric_rules">{{ number_format($total_pr, 2) }}</th>
                        <th class="th_common_numeric_rules">{{ number_format($total_disc_amount, 2) }}</th>
                        <th class="th_common_numeric_rules">{{ number_format($total_po_amount, 2) }}</th>
                    </tr>
                    @else
                    <tr>
                        <td colspan="14" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>