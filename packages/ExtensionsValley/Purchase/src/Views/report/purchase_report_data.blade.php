
<div class="row">
   <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;">
    @if(sizeof($purchaseData)>0)
    <p style="font-size: 12px;" id="total_data">Report Date: {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </p><br>
    <p style="font-size: 12px;">Total Records<b> : {{$total}}</b></p>
   <h2 style="text-align: center;margin-top: -29px;" id="heading">  Purchase Report  </h2> @endif
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">
        @if(sizeof($purchaseData)>0)
            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='5%'>Sn.No.</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='10%'>PO NO</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='15%'>Product Name</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='15%'>Supplier Name</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='5%'>Qty</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='5%'>Free Qty</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='5%'>Pending Qty</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='10%'>Store</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='5%'>Unit Name</th>
                <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;" width='5%'>Purchase Rate</th>
                <th onclick="sortTable('result_data_table',2,1);" style="padding: 2px;" width='5%'>PO Amount</th>
                <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;" width='10%'>PO Date</th>
                <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;" width='10%'>Status</th>
            </tr>

            <?php $i =1;
            $total_fqty=$total_po_amount=$total_qty=$total_pen=$total_pr=0 ?>
            @foreach ($purchaseData as $data)


                <tr>
                    <td class="td_common_nummeric_rules">{{++$offset}}</td>
                    <td class="td_common_nummeric_rules">{{$data->po_no}}</td>
                    <td class="td_common_rules">{{$data->item_desc}}</td>
                    <td class="td_common_rules">{{$data->vendor_name}}</td>
                    <td class="td_common_nummeric_rules">{{$data->pur_qnty}}</td>
                    <td class="td_common_nummeric_rules">{{$data->free_qty}}</td>
                    <td class="td_common_nummeric_rules">{{$data->po_balance_qty}}</td>
                    <td class="td_common_nummeric_rules">{{$data->store}}</td>
                    <td class="td_common_nummeric_rules">{{$data->name}}</td>
                    <td class="td_common_nummeric_rules">{{$data->pur_rate}}</td>
                    <td class="td_common_nummeric_rules">{{number_format($data->net_total,$report_decimal_separator)}}   </td>
                    <td class="td_common_nummeric_rules">{{ date('d-M-Y',strtotime($data->po_date))}}</td>
                    <td class="td_common_nummeric_rules">{{$data->status}}</td>

                </tr>
                
               @php
                //    $i++;
                   $total_qty+=$data->pur_qnty;
                   $total_fqty+=$data->free_qty;
                   $total_pen+=$data->po_balance_qty;
                   $total_pr+=$data->pur_rate;
                   $total_po_amount+=$data->net_total;
               @endphp
            @endforeach
              
              <tr style="border-top:1px solid black;"><td colspan="4"><b>Total</b></td><td><b>{{$total_qty}}</b></td><td><b>{{$total_fqty}}</b></td><td><b>{{$total_pen}}</b></td><td><td></td></td><td><b>{{number_format($total_pr,$report_decimal_separator)}}</b></td><td><b>{{number_format($total_po_amount,$report_decimal_separator)}}</b></td></tr>
            @else
            <tr>
                <td>No Results Found!</td>
            </tr>
            @endif

        </table>
   </div>
</div>
<div class="clearfix"></div>
<div class="h10"></div>

<div class="row" style="">
    <div style="padding:10px 30px;">
        <div class="col-md-12" style="text-align:right;">
            <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
            </nav>
        </div>
    </div>
</div>
</div>





<script type="text/javascript">  
     function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,1])[1];
   }

    $("#ResultsViewArea #purchase_paging .pagination a").click(function(e){
        e.preventDefault();
        var purchase_date_from =$("#purchase_date_from").val();
        var purchase_date_to = $("#purchase_date_to").val();
        var po_no = $("#po_no").val();
        var location = $("#location").val();
        var status = $("#status").val();
        var url = $(this).attr('href');
        var page_no = getURLParameter(url, 'page');
        var url = route_json.purchaseReportResults;
        var param = {purchase_date_from: purchase_date_from,purchase_date_to: purchase_date_to,po_no: po_no,page_no:page_no,location: location,status: status};
        $.ajax({
            url:url,
            data: param,
            dataType: 'html',
            beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            }).done(function (data) {
                $('#ResultsViewArea').LoadingOverlay("hide");
                $('#ResultsViewArea').html(data);
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
    }); 
</script>