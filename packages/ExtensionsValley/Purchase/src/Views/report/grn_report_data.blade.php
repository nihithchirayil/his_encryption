<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from_date ?> To <?= $to_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> GRN Register</b></h4>

            <table id="result_data_table" class='table table-condensed table_sm' style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="15%;">Location</th>
                        <th width="15%;">GRN No</th>
                        <th width="15%;">GRN Date</th>
                        <th width="15%;">Bill No/Date</th>
                        <th width="15%;">Item Name</th>
                        <th width="15%;">Generic Name</th>
                        <th width="15%;">Supplier</th>
                        <th width="15%;">Batch No</th>
                        <th width="15%;">Expiry Date</th>
                        <th width="15%;">Unit Name</th>
                        <th width="15%;">Rec. Qty (Calc.)</th>
                        <th width="15%;">Free Qty (Calc.)</th>
                        <th width="15%;">Rate</th>
                        <th width="15%;">Disc %</th>
                        <th width="15%;">CGST(%)</th>
                        <th width="15%;">SGST(%)</th>
                        <th width="15%;">IGST(%)</th>
                        <th width="15%;">Rate with Tax</th>
                        <th width="15%;">Other Charges</th>
                        <th width="15%;">P.Cost(unit)</th>
                        <th width="15%;">Selling Price</th>
                        <th width="15%;">Selling Price without Tax</th>
                        <th width="15%;">Unit MRP</th>
                        <th width="15%;">Margin/Profit (Rs.)</th>
                        <th width="15%;">Margin/Profit (%)</th>
                        <th width="15%;">Item Value</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
        $net_grn=0.0;
        $net_grn_free =0.0;
        $net_item_value =0.0;
        $net_margin_profit_amt =0.0;
        if(count($res)!=0){
        foreach ($res as $data){
            $net_grn += floatval($data['recevied_qty']);
            $net_grn_free += floatval($data['recevied_free_qty']);
            $net_item_value += floatval($data['item_value']);
            $net_margin_profit_amt += floatval($data['margin_profit_amt']);
              ?>

                    <tr>
                        <td class="common_td_rules"><?= $data['location_name'] ?></td>
                        <td class="common_td_rules"><?= $data['grn_no'] ?></td>
                        <td class="common_td_rules"><?= $data['grn_date'] ?></td>
                        <td class="common_td_rules"><?= $data['bill_no_date'] ?></td>
                        <td class="common_td_rules"><?= $data['item_desc'] ?></td>
                        <td class="common_td_rules"><?= $data['generic_name'] ?></td>
                        <td class="common_td_rules"><?= $data['vendor_name'] ?></td>
                        <td class="common_td_rules"><?= $data['batch'] ?></td>
                        <td class="common_td_rules"><?= $data['expiry'] ?></td>
                        <td class="common_td_rules"><?= $data['uom_name'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['recevied_qty'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['recevied_free_qty'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['rate'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['disc_perc'] ?></td>
                        <td class="td_common_numeric_rules"><?= @$data['cgst'] ? $data['cgst'] : '0.00' ?></td>
                        <td class="td_common_numeric_rules"><?= @$data['sgst'] ? $data['sgst'] : '0.00' ?></td>
                        <td class="td_common_numeric_rules"><?= @$data['igst'] ? $data['igst'] : '0.00' ?></td>
                        <td class="td_common_numeric_rules"><?= $data['rate_with_tax'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['other_charges'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['purchase_cost'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['selling_price'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['sales_price_without_tax'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['unit_mrp'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['margin_profit_amt'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['margin_profit_per'] ?></td>
                        <td class="td_common_numeric_rules"><?= $data['item_value'] ?></td>
                    </tr>

                    <?php
        }
        ?>
                    <tr>
                        <th class="common_td_rules" colspan="10">Total</th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $net_grn, 2, '.', '') ?></th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $net_grn_free, 2, '.', '') ?></th>
                        <th class="common_td_rules" colspan="11">Profit Amount Total</th>
                        <th class="td_common_numeric_rules">
                            <?= number_format((float) $net_margin_profit_amt, 2, '.', '') ?></th>
                        <th class="common_td_rules">Item Value Total</th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $net_item_value, 2, '.', '') ?>
                        </th>
                    </tr>
                    <?php
    }else{
        ?>
                    <tr>
                        <td colspan="26" style="text-align: center;">No Results Found!</td>
                    </tr>
                    <?php
    }
    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
