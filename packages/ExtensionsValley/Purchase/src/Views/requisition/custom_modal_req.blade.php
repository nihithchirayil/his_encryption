

<div id="product_batchModel" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1200px; width: 40%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >Item Code : <span id="modal_itemcodehr"> </span></h4>
            </div>
            <div class="modal-body" style="padding: 7px 15px;" id="itemts_batchwisediv">

            </div>

        </div>
    </div>
</div>

<div id="modal_global_stock" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1200px; width: 50%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Stock Details</h4>
            </div>
            <div class="modal-body" id="global_stock_append">

            </div>
        </div>
    </div>
</div>

<div id="close_request_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Closing Remarks</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">

                <div class="col-md-offset-1 col-md-10">
                    <form method="POST">

                        <div class="form-group has-feedback">
                            <textarea placeholder="Comments" class="form-control "  name="closed_cmnts" required></textarea>
                           
                        </div>

                        <div class="row">
                            <div class="col-xs-12 text-center">

                                <button type="button"    data-dismiss="modal"  class="btn btn-warning "> <i class="fa fa-times"></i>Cancel</button>
                                <button type="button"  onclick="save_product_list(5)"  class="btn btn-success current_issue_items_request">
                                    <i class="fa  fa-close"></i>Close Request</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>