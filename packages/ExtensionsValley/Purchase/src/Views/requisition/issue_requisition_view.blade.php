@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .select2-container--default .select2-selection--single {
        border-radius: 0px !important; line-height: 0px !important;

    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 22px !important;
}
ul.pagination {
    margin: 0;
}
.select2-container--default .select2-results>.select2-results__options {
    max-height: 370px !important;
    overflow-y: auto;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row" style="text-align: right; font-size: 12px;font-weight: bold;padding-right: 100px"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">

<!--        <div class="col-md-12 padding_sm">
             <div class="col-xs-3" style="margin-top: 15px;">
         <div class="mat_type_input anim">
            <label class="mat_label">Name <span>*</span></label>
            <input type="text" name="" placeholder="Enter Name" class="form-control mat-form-control" value="">
        </div>
    </div>

    <div class="col-md-3" style="margin-top: 15px;">
     <div class="select_mat_type_input">
        <label class="mat_label">Select <span>*</span></label>
        <select class="form-control selectdropdown mat-form-control"  name="" id="">
           <option value=""></option>
           <option value="2">Alaska</option>
           <option value="3">Thrissur</option>
       </select>
   </div>
</div>
<div class="col-md-3" style="margin-top: 15px;">
     <div class="select_mat_type_input">
        <label class="mat_label">Select <span>*</span></label>
        <select class="form-control mat-form-control"  name="" id="">
           <option value=""></option>
           <option value="2">Alaska</option>
           <option value="3">Thrissur</option>
       </select>
   </div>
</div>
<div class="col-md-3" style="margin-top: 15px;">
 <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1 hidden" data-multiple-caption="{count} files selected" multiple />
 <label class="no-margin btn-block file_upload_btn" for="file-1"><i class="fa fa-folder"></i> <span>Upload File</span></label>

</div>
<div class="col-md-3" style="margin-top: 15px;">
 <div class="mat_type_input">
    <label class="mat_label">Comment <span>*</span></label>
    <textarea class="form-control mat-form-control" name=""></textarea>
</div>
</div>
        </div>-->


        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        {!!Form::open(array('url' => $searchUrl, 'method' => 'get', 'id'=>'form'))!!}
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box">
                                <label class="custom_floatlabel header_label">Request No.</label>
                                <input type="text" name="issue_request_no" autocomplete="off"  autofocus value="@if(old('issue_request_no')){{old('issue_request_no')}}@else{{$searchFields['issue_request_no']}}@endif" class="form-control" id="issue_request_no" placeholder="Requisition Reference No">
                                <div id="product_request_no_searchCodeAjaxDiv" class="ajaxSearchBox" style="margin-top:14px"></div>
                            </div>
                            </div>
                        </div>
                        @if($page_id=='2')
                        <div class="col-md-1 padding_sm" >
                            @if(old('search_by_receive_date'))
                                @php $search_by_receive_date = old('search_by_receive_date') @endphp
                            @else
                                @php $search_by_receive_date =  $searchFields['search_by_receive_date'] @endphp
                            @endif
                            @if($search_by_receive_date=='1')
                                @php  $checked= 'checked';@endphp
                            @else
                                @php $checked= ''; @endphp
                            @endif

                            <div class="mate-input-box">
                                <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" class="form-control filters" name="search_by_receive_date"  {{ $checked }} id="search_by_receive_date" value="1">
                                <label class="text-blue ">By Rec.Dt</label>
                                </div>
                            </div>
                       </div>
                       @endif
                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">From Date</label>
                            <div class="clearfix"></div>
                            <input type="text" name="from_date" autocomplete="off"
                                   value="@if(old('from_date')){{old('from_date')}}@else{{$searchFields['from_date']}}@endif"
                                   class="form-control datepicker" id="from_date"
                                   placeholder="From Date">
                               </div>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">To Date</label>
                            <div class="clearfix"></div>
                            <input type="text" name="to_date" autocomplete="off"
                                   value="@if(old('to_date')){{old('to_date')}}@else{{$searchFields['to_date']}}@endif"
                                   class="form-control datepicker" id="to_date"
                                   placeholder="To Date">
                               </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <?php
                            $listdept = \ExtensionsValley\Purchase\CommonInvController::getLocationByrole();
                            $default_location =  \ExtensionsValley\Purchase\CommonInvController::defaultLocation();
                            ?>
                            <label for="" class="header_label">Department</label>
                            {!! Form::select('to_location',$listdept,$searchFields['to_location']?$searchFields['to_location']:$default_location,['class' => 'form-control select2 abc','placeholder' => 'Department','title' => 'Department','id' => 'from_department','style' => 'color:#555555; padding:2px 12px;']) !!}
                        </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <?php
                            $listdepts = \ExtensionsValley\Purchase\CommonInvController::getStoreByRole();
                            $dfltstore = \ExtensionsValley\Purchase\CommonInvController::defaultStore();
                            ?>
                            <label for="" class="header_label">Store</label>
                            {!! Form::select('from_store_location',$listdepts,$searchFields['from_store_location']?$searchFields['from_store_location']:$dfltstore,
                            ['class' => 'form-control  select2','placeholder' => 'Store','title' => 'Store',
                            'id' => 'from_store_location','style' => 'color:#555555;']) !!}
                        </div>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Status</label>
                            {!! Form::select('issue_status_select', [
                            '10'=> 'Issued',
                            '7' => 'Received',
                            '12'=> 'Rejected',
                            '4' => 'Approved',
                            '5' => 'Partially Issued',
                            '6' => 'Partially received',
                            '33'=>'Closed'
                            ], $searchFields['issue_status_select'],
                            ['class' => 'form-control','placeholder' => 'Select Status','id' => 'issue_status_select','style' => 'color:#555555;']) !!}
                        </div>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block light_purple_bg" onclick="PrreqSearch();" ><i class="fa fa-search"></i> Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-warning" onclick="clear_search();" ><i class="fa fa-times"></i> Clear</span>
                        </div>
                        {!! Form::token() !!} {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix" style="border-radius:6px;background-color: #fff !important">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="2%" class="common_td_rules">Req. No</th>
                                    <th width="5%" class="common_td_rules">Department </th>
                                    <th width="2%" class="common_td_rules">Store</th>
                                    <th width="4%" class="common_td_rules">Requested Date</th>
                                    <th width="4%" class="common_td_rules">Requested By</th>
                                    <th width="3%" class="common_td_rules">Aprvd. Date</th>
                                    <th width="3%" class="common_td_rules">Aprvd. By</th>
                                    <th width="3%" class="common_td_rules">Issued By</th>
                                    <th width="3%" class="common_td_rules">Received By</th>
                                    <th width="3%" class="common_td_rules">Recvd. Date</th>
                                    <th width="3%" class="common_td_rules">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($issue_requisition_list) && sizeof($issue_requisition_list))
                                @foreach($issue_requisition_list as $list)
                                @if($list->issue_status=='Closed')
                                @php $background_color = "background-color: #f7e5e5"; @endphp
                                @else
                                @php $background_color = "";@endphp
                                @endif
                                <tr onclick="selectRequisition({{$list->stock_id}},{{$page_id}})" style="cursor:pointer;{{$background_color}}">
                                    <td class="common_td_rules" style="overflow: hidden;white-space: nowrap;" title="">
                                        {{ $list->request_no }}
                                    </td>
                                    <td class="common_td_rules" title="{{ $list->department }}">{{ $list->department }}</td>
                                    <td class="common_td_rules" title="{{ $list->store }}">{{ $list->store }}</td>
                                    <td class="common_td_rules" title="{{ $list->requested_date }}">{{ $list->requested_date }}</td>
                                    <td class="common_td_rules" title="{{ $list->requested_by }}">{{ $list->requested_by }}</td>
                                    <td class="common_td_rules" title="{{ $list->approved_date }}">{{ $list->approved_date }}</td>
                                    <td class="common_td_rules" title="{{ $list->approved_by }}">{{ $list->approved_by }}</td>
                                    <td class="common_td_rules" title="{{ $list->issued_by }}">{{ $list->issued_by }}</td>
                                    <td class="common_td_rules" title="{{ $list->received_by }}">{{ $list->received_by }}</td>
                                    <td class="common_td_rules" title="{{ $list->received_date }}">{{ $list->received_date }}</td>
                                    <td class="common_td_rules" title="{{ $list->status }}">{{ $list->status }}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 text-center">
                        <div class="col-md-2" style="padding-top: 40px;">
                            <div style='width:20px;height:20px;display:block;float:left;background-color:#f7e5e5;'></div>
                            <label style="margin: 2px 0 0 10px;float: left;">Closed</label>
                        </div>
                        <nav class="col-md-10" style="text-align:right;padding-top: 40px;"> <?php echo $paginator->render(); ?></nav>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@stop
@section('javascript_extra')
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">

    function PrreqSearch() {
    var makeSearchUrl = '';
    $('#form').submit();
    }
    function selectRequisition(product_id, page_id){
    if (page_id == 1){
    window.location = "{!! route('extensionsvalley.purchase.issue_stock_items') !!}/" + product_id;
    } else{
    window.location = "{!! route('extensionsvalley.purchase.receive_stock_items') !!}/" + product_id;
    }
    }
    $('#issue_request_no').keyup(function(event){
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
   // if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var product_request_no_search = $(this).val();
    item_desc_search = product_request_no_search.trim();
    if (item_desc_search == "") {
    $("#product_request_no_searchCodeAjaxDiv").html("");
    $("#product_request_no_search_hidden").val("");
    } else {
    var url = '';
    var param = {product_request_no:btoa(product_request_no_search)};
    $.ajax({
    type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
            $("#product_request_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            if (html == 'No Result Found'){
            toLocationPub = '';
            }
            $("#product_request_no_searchCodeAjaxDiv").html(html).show();
            $("#product_request_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });

    }
    // } else {
    // ajaxProgressiveKeyUpDown('product_request_no_searchCodeAjaxDiv', event);
    // }
    });
    $('#issue_request_no').on('keydown', function(event){
    if (event.keyCode === 13){
    ajaxProgressiveEnterKey('product_request_no_searchCodeAjaxDiv');
    return false;
    }
    });
    function fillitemdescValues2(list, request_no) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#issue_request_no').val(request_no);
    $('#' + itemCodeListDivId).hide();
    }

</script>
@endsection
