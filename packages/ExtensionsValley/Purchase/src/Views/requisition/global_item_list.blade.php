<div class="row">
        <div class="col-md-12">
                <div class="box-body clearfix">
                    
                    <div class="col-md-3">
                        <label>Total Stock : <span>{{$global_result['total_stock']}}</span></label>
                    </div>
                    
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="theadscroll always-visible"  style="height: auto; max-height: 400px;">
                        <table class="table table_round_border styled-table product_issue_table">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="10%">Item Code</th>
                                    <th width="20%">Item Name</th>
                                    <th width="10%">Batch No</th>
                                    <th width="10%">Stock</th>
                                    <th width="20%">Location Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                if (count($global_result['res']) != 0) {
                                    foreach ($global_result['res'] as $each) {
                                        ?>
                                        <tr>
                                            <td>{{$each->item_code}}</td>
                                            <td>{{$each->item_desc}}</td>
                                            <td>{{$each->batch_no}}</td>
                                            <td>{{$each->stock}}</td>
                                            <td>{{$each->location_name}}</td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    echo " <tr><td colspan='5' style='text-align: center;'>No Result Found</td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>