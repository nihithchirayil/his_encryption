<div class="row" style="height: 145px;overflow: auto">
    <div class="col-md-12 item_label">
        <div>
            <h4 class="title" style=" text-align: center;padding-top: 8px;margin-top: 0px;"><span id="modal_desc" style="line-height: 28px;"> </span></h4>
        </div>

        <div class="clearfix"></div>
        <div class="h10"></div>
        <div class="col-md-12 padding_sm" >
            <div class="theadscroll" >
                <table class="table  table_round_border no-margin theadfix_wrapper table_sm">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="text-align:center">Batch No</th>
                            <th style= "text-align:center">Exp. Date</th>
                            <th style= "text-align:center">Mrp.</th>
                            <th style= "text-align:center">Stock</th>
                            <th style= "text-align:center">Issued Qty</th>
                            <th style= "text-align:center">Issue Qty</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_item_batch_data">

                        <?php
                        $selected_batch_items = json_decode($batch_tmplate_data['batch_data'],TRUE);
                        $item_codess = $batch_tmplate_data['item_code'];
                        $total_iss_qty = 0;
                        if (count($batch_tmplate_data['res']) != 0) {
                            $i = 1;
                            $k=0;
                            $total_batch_qnty = 0.00;
                            foreach ($batch_tmplate_data['res'] as $each) { ?>
                                <tr>
                                    <td class="common_td_rules">
                                        <input type="hidden" class="inc_class" value="{{$i}}">
                                        <input type="hidden" id="batch_no{{$i}}" value="{{$each->r_batch_no}}">
                                        <input type="hidden" class="decimal_qty_ind" id="decimal_qty_ind{{$i}}" value="{{$each->decimal_qty_ind}}">
                                        {{$each->r_batch_no}}
                                    </td>
                                    <td class="common_td_rules">
                                        {{$each->r_expiry_date}}
                                    </td>
                                    <td width='5%'>{{$each->r_mrp}}</td>
                                    <td id="batch_stock{{$i}}" class="td_common_numeric_rules">
                                        {{$each->r_stock}}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                       <?php
                                       $prev_iss_qty ='';
                                       $edit_array_in_item_detail = $batch_tmplate_data['edit_array_in_item_detail'];
                                       if(sizeof($edit_array_in_item_detail)>0){
                                       foreach ($edit_array_in_item_detail as $edit_in){
                                       if(isset($edit_in->batch_no) && $edit_in->batch_no == $each->r_batch_no){
                                        $prev_iss_qty = $edit_in->iss_item_qty;
                                        }
                                       }
                                       } ?>
                                        {{$prev_iss_qty}}
                                    </td>
                                    <td style="width:17%" class="common_td_rules">
                                       <?php
                                       $iss_qty = 0;
                                       if (isset($selected_batch_items[$item_codess][base64_encode($each->r_batch_no)])) {
                                        $iss_qty = $selected_batch_items[$item_codess][base64_encode($each->r_batch_no)];
                                    }
                                    $total_iss_qty += $iss_qty;
                                       ?>
                                        <input type='text' autocomplete="off" autofocus name='batch_wise_issued_qty_name[]' id='batch_wise_issued_qty{{$i}}' value='{{$iss_qty}}'  onkeyup="batch_qty_validation(this,{{$i}});number_validation(this)" class= "form-control btch_iss_qty_chng">

                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            <tr>

                                <td style="text-align: right" colspan="4">
                                    Total Stock
                                </td>
                                <td id="batch_wise_issued_qty_total">{{$total_iss_qty}}</td>
                            </tr>
                            <?php
                        } else {
                            echo '<tr><td colspan="9" style="text-align: center">No Result Found</td> </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" id="item_code_for_batch" value="{{$batch_tmplate_data['item_code']}}">
        <input type="hidden" id="disable_issue_greaterthan_approve_qty" value="{{$batch_tmplate_data['disable_issue_greaterthan_approve_qty']}}">
    </div>
</div>
<div class="modal-footer" style="padding: 2px 7px;">
<span class="btn btn-warning"  onclick="hide_batch_div('{{$batch_tmplate_data['item_id']}}')" style="padding: 0px 2px;"><i class="fa fa-times"> Cancel</i></span>
    <span class="btn btn-success" id="add_batch_button" onclick="create_batch_array({{$batch_tmplate_data['item_id']}});" style="padding: 0px 2px;"><i class="fa fa-thumbs-up"> OK</i></span>

</div>
