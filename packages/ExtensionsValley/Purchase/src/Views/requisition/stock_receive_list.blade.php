@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .label_blue{
    color: #2d2df5;
    font-size: 85%;
}
</style>
@endsection
@section('content-area')
<?php
if ($stock_head_result_view != '') {


    $Request_number = $stock_head_result_view[0]->request_no;
    $created_date = $stock_head_result_view[0]->created_date;
    $from_department = $stock_head_result_view[0]->from_loc;
    $to_department = $stock_head_result_view[0]->to_loc;
    $from_locationid = $stock_head_result_view[0]->from_location_code;
    $to_locationid = $stock_head_result_view[0]->to_location_code;
    //}
}
?>
    <div class="right_col"  role="main">
        <div class="row codfox_container">
            <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="col-md-2 padding_sm">
                                <div class="custom-float-label-control">
                                    <label class="custom_floatlabel">Request No.</label>
                                    <div class="clearfix"></div>
                                    <span class="label label_blue" id="rrequest_number">{{$Request_number}}</span>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="custom-float-label-control">
                                    <label class="custom_floatlabel">Date And Time</label>
                                    <div class="clearfix"></div>
                                    <span class="label label_blue" id="created_date">{{$created_date}}</span>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="custom-float-label-control">
                                    <label class="custom_floatlabel">Requested Department</label>
                                    <div class="clearfix"></div>
                                    <span class="label label_blue" id="from_department">{{$from_department}}</span>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="custom-float-label-control">
                                    <label class="custom_floatlabel">Issued Store</label>
                                    <div class="clearfix"></div>
                                    <span class="label label_blue" id="to_department">{{$to_department}}</span>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="custom-float-label-control">
                                    <label class="custom_floatlabel">Status</label>
                                    <div class="clearfix"></div>
                                    <span class="label label_blue">{{$status_text}}</span>

                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="custom-float-label-control">
                                    <span><i id="spin_searchbatch"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ht5"></div>
                <div class="col-md-12">
                    <?php if (($status == 5 || $status == 6 || $status == 10)) { ?>
                        <div class="checkbox checkbox-primary chekbox_table_lab">
                            <input id="fully_receive_chk" class="styled fully_receive_reject" type="checkbox">
                            <label for="fully_receive_chk" style="padding-right: 30px;">Fully receive</label>
                            @if (($status == 5 || $status == 10))
                            <input id="fully_reject_chk" class="styled fully_receive_reject" type="checkbox">
                            <label for="fully_reject_chk" style="padding-right: 30px;">Fully reject</label>
                            @endif
                        </div>
                    <?php } ?>


                    <form id="receive_product_list">
                        <div class="theadscroll" style="position: relative; height: 440px;">
                        <table id="receive_table" class="table  table_round_border styled-table theadfix_wrapper">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="22%" style=" text-align: center">
                                        <input id="receive_search_box" class="jquery_search_item_text" onkeyup="searchProducts();" type="text" placeholder="Item Search.. " style="display: none;width: 85%" >
                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                        <span id="item_search_btn_text">Item Name</span>
                                    </th>
                                    <th width="8%" style=" text-align: center">Batch</th>
                                    <th width="8%" style=" text-align: center">App. Qty</th>
                                    <th width="6%" style=" text-align: center">Iss. Qty</th>
                                    <th width="6%" style=" text-align: center">Total.Rcvd</th>
                                    <th width="6%" style=" text-align: center">Rej. Qty</th>
                                    <th width="6%" style=" text-align: center">Rej.Remarks</th>
                                    <th width="6%" style=" text-align: center">Bal.Qty.</th>
                                    <th width="6%" style=" text-align: center">Receive</th>
                                    <th width="6%" style=" text-align: center">Reject</th>
                                    <th width="10%" style=" text-align: center">Rjct. remark</th>
                                    <th width="10%" style=" text-align: center">Issue. remark</th>

                                    <th width="8%" class="text-center" style="display:none"><i class="fa fa-exclamation-triangle text-red"></i></th>
                                </tr>
                            </thead>
                            <tbody id='receive_product_list'>
                                <?php
                                if ($stock_receive_details != '') {
                                    $k = 0;
                                    foreach ($stock_receive_details as $val) {
                                        $readonly = '';
                                        $readonly_intra = '';
                                        $row_bg = '';
                                        $issue_status_indicator = $val->issued_qty - ($val->rcevd + $val->rejected_qnty);
                                        if(($val->rcevd + $val->rejected_qnty) > 0 &&  $issue_status_indicator > 0){
                                            $row_bg = "background-color:#a7bfd4";//partialy received
                                        }else if(($val->issued_qty < $val->approved_qty) && ($val->issued_qty > 0)){
                                            $row_bg = "background-color:#b2f1b2";//partialy issued
                                        }
                                        $urgent_post = '';
                                        if ($val->is_urgent != '0') {
                                            $urgent_post = $val->is_urgent;
                                        }
                                        $rejected_quantity = $val->rejected_qnty;
                                        $stock_req_item_id = $val->stock_req_item_id;
                                        $issued_quantity = $val->issued_qty;
                                        $received_quantity = $val->rcevd;

                                        if ($rejected_quantity == '' || $rejected_quantity == null) {
                                            $rejected_quantity = 0;
                                        }
                                        $balance_quantity = ($issued_quantity) - (($received_quantity) + ($rejected_quantity));
                                        // if($issued_quantity !=0 && $issued_quantity > $balance_quantity){
                                        //     $row_bg = "";//"background-color:f59028b0";
                                        // }else{
                                        //     $row_bg = "";
                                        // }
                                        //check if intransit updated or not
                                        $iss_qty = $val->issued_qty ? $val->issued_qty : 0;
                                        if(isset($val->in_transit_updated) && $val->in_transit_updated == 1){
                                             if(($iss_qty-$val->in_trance_updated_qty) <= 0){
                                                $readonly_intra = "readonly";
                                             }

                                        }
                                        ?>

                                <tr id="receive_item_row" style="<?= $row_bg ?>">

                                            <td class="common_td_rules"><label style="flex:1">{{$val->item_tooltip}}</label>
                                                @if($issue_status_indicator > 0)
                                                <?php $readonly = ""; $input_type= "text" ; ?>
                                                <span id='table_color_indication' class="led-red"></span>
                                                @else
                                                <?php $readonly = "readonly"; $input_type= "hidden" ; ?>
                                                @endif
                                                <input type="hidden" value="{{$issue_status_indicator}}" class="issue_status_indicator" name="issue_status_indicator[]" id="issue_status_indicator_{{$stock_req_item_id}}" >
                                            </td>
                                    <td  class="common_td_rules"><label>{{$val->batch_no}}</label></td>
                                    <td class="td_common_numeric_rules"><label >{{$val->approved_qty }}</label></td>
                                    <td class="td_common_numeric_rules"><label id="issued_qty_id{{$k}}" >{{$val->issued_qty ? $val->issued_qty : 0}}</label></td>
                                    <td class="td_common_numeric_rules"><label >{{$val->rcevd }}</label></td>
                                    <td class="td_common_numeric_rules"><label id="rejected_qty_id{{$k }}" >{{$rejected_quantity }}</label></td>
                                    <td  class="common_td_rules"><label>{{ $val->rejected_remark }}</label></td>
                                    <td class="td_common_numeric_rules"><label>{{ $balance_quantity }}</label>
                                   <input type="hidden" value="{{$val->issued_qty}}" class="item_full_rej_rec" name="item_full_rej_rec[]" id="item_full_rej_rec_{{$stock_req_item_id}}" >
                                    </td>
                                    <td><input type="hidden" value="{{$val->issued_qty}}" name="hidden_item_quanty[]" id="item_qnt_{{$stock_req_item_id}}" >
                                        <input type="hidden" value="{{$balance_quantity}}" name="item_balance_qnt[]" class="item_balance_qnt" id="item_balance_qnt_{{$stock_req_item_id}}" >
                                        <input  <?= $readonly; ?> <?= $readonly_intra ?> type="{{$input_type}}" min="0" name="item_receive_qty[]" id="item_issue_qty_id_<?= $stock_req_item_id; ?>" value="0"  class="form-control issue_qty_val" onkeyup="checkReceivedQuantity(this,'<?= $stock_req_item_id; ?>')" ></td>
                                    <td><input  <?= $readonly; ?> <?= $readonly_intra ?> type="{{$input_type}}" min="0" name="rejected_quantity_number[]" id="product_reje_qty_id_<?= $stock_req_item_id ?>" value="0" class="form-control rejected_qty_val" onkeyup="checkReceivedQuantity(this,'<?= $stock_req_item_id; ?>')" ></td>
                                    <td><input <?= $readonly; ?> <?= $readonly_intra ?> type="{{$input_type}}" name="item_reject_remark[]" <?= $readonly; ?> id="reject_remark_item_id_<?= $stock_req_item_id; ?>"  class="form-control reject_remark"></td>
                                    <td><input type="text" name="issue_remark"  id="issue_remark"  class="form-control" readonly value="<?= $val->description ?>"></td>

                                    </tr>

                                    <?php
                                    $k++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="clearfix"></div>
                <div class="h10"></div>
                <div class="col-md-12">
                    <div class="col-md-4 padding_sm">
                        <div class="custom-float-label-control">
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                        </div>
                    </div>
                    <div class="col-md-8 padding_sm text-right">
                        <?php
                        $submit_style = "disabled";
                        if(isset($access_control) && count($access_control) > 0){
                        for($ac = 0; $ac <= sizeof($access_control);$ac++){
                            if(isset($access_control[$ac]->submit) && $access_control[$ac]->submit ==1){
                                    $submit_style = "";
                                    break;
                                }else{
                                    $submit_style = "disabled";
                                }
                        }
                        }

                        if ($status == 5 || $status == 6 || $status == 10) { ?>

                            <button type="button" onclick="load_page()"class="btn btn-warning "><i class="fa fa-times"></i>Cancel </button>
                                 <button type="button" {{$submit_style}} onclick="save_all()" class="btn btn-success save_recieve_items"><i id="save_recieve_items_btn_spin" class="fa fa-save"></i> Save</button>
                        <?php } ?>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12">
                <div style="padding-top: 20px;">
                    <div class="col-md-2">
                        <div class="led-red"></div>
                        <label style="margin-left:10px;">Not Received</label>
                    </div>
                    <div class="col-md-2">
                        <div style="background-color: #b2f1b2;width: 20px;height: 20px;display: inline-block"></div>
                        <label style="margin-left:5px;">Partialy Issued</label>
                    </div>
                    <div class="col-md-2">
                        <div style="background-color: #a7bfd4;width: 20px;height: 20px;display: inline-block"></div>
                        <label style="margin-left:5px;">Partialy Received</label>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <input type="hidden" id="stock_id_for_receive_list" value="{{$stock_id}}">
    <input type="hidden" id="from_locationid" value="{{$from_locationid}}">
    <input type="hidden" id="to_locationid" value="{{$to_locationid}}">
    <input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
    @endsection
    @section('javascript_extra')
    <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/stock_receive.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
    <script>
    </script>
    @stop


