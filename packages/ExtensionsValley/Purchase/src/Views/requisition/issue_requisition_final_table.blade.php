<table id="product_issue_table" class="table  table_round_border styled-table">
    <thead>
        <tr class="table_header_bg">
            <th width="5%" style=" padding-left: 25px;">
                <input type='checkbox' value='' onclick='revert_check_box(this)' style="float:right">
            </th>
            <th width="6%" style="text-align:center">Item Code</th>
            <th width="19%" style="text-align:center">
                <input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Item Search.. "
                    style="display: none;width: 90%;color:black">
                <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                        class="fa fa-search"></i></span>
                <span id="item_search_btn_text">Item Name</span>
            </th>
            <th width="5%" style="text-align:center">Item Unit</th>
            <th width="5%" style="text-align:center">App. Qty</th>
            <th width="5%" style="text-align:center">Issue Qty</th>
            <th width="6%" style="text-align:center">Issued Qty</th>
            <th width="5%" style="text-align:center">Bal. Qty</th>
            <th width="7%" style="text-align:center">S.Stock</th>
            <th width="7%" style="text-align:center">D.Stock</th>
            <th width="5%" style="text-align:center">Last Issued Dt</th>
            <th width="7%" style="text-align:center">Remark</th>

        </tr>
    </thead>
    <tbody id='issue_products_tbody'>
        <?php
        if ($stock_detail_result_view != '') {
            $item_id = 1;
            foreach ($stock_detail_result_view as $val) {

                $approved_issue_quantity = $val->approved_qty;
                $quantity_issued = $val->issued_qty;
                $balance_qty = $approved_issue_quantity - $quantity_issued;
                if ($balance_qty > 0)
                    $balance_qty = $balance_qty;
                else
                    $balance_qty = 0;

                $check_urgent = '';
                $checked_bgcolor = '';
                if ($val->is_urgent != '0') {
                    $check_urgent = 'checked';
                    $checked_bgcolor = 'background-color:#efc9c9';
                }

                $bg_style = '';

                if ($approved_issue_quantity == $balance_qty) {
                    $bg_style = "led-red";
                } else if ($approved_issue_quantity > $quantity_issued) {
                    $bg_style = "led-yellow";
                }

                $total_received_quantity = 0;

                $receive_qty = $val->received_qty;
                $reject_qty = $val->rejected_qnty;
                $disabled = '';
                $disply = '';

                $total_received_quantity = $receive_qty + $reject_qty;
                if ($balance_qty != 0 && $total_received_quantity != 0) {
                    $disabled = 'disabled';
                    $disply = 'display:none';
                } else if ($val->issued_qty - $total_received_quantity == 0) {
                    $disabled = 'disabled';
                    $disply = 'display:none';
                }
                if (($total_received_quantity) == $val->approved_qty) {
                    $disabled = 'disabled';
                    $bg_style = "led-green";
                    $disply = 'display:none';
                }
                $approved_qty = 0;
                if ($val->detail_id != 0) {
                    $approved_qty = $val->approved_qty;
                } else {
                    $approved_qty = 0;
                }
                if (!$val->stock) {
                    $stock = 0;
                } else {
                    $stock = $val->stock;
                }
                $allStockData=@$allStockCnt[$val->item_code] ? $allStockCnt[$val->item_code] : '';
                if (!$val->stock) {
                    $sstock = 0;
                } else {
                    $sstock= $allStockData;
                }
                $remarks_data = \DB::table('stock_request_item_detail')->where('item_code',$val->item_code)->where('request_detail_id',$val->detail_id)->value('remarks');
                $input_remark = "";
                if (empty($remarks_data)) {
                   $remarks_data = '';
                }
                $input_remark = "readonly";
                if ($status == 4 || $status == 5 || $status == 6 ) {
                    if ($issue_status == 0) {
                        $input_remark = "";
                    }
                }
                ?>
        <tr id="item_issue_row<?= $val->item_code ?>" style="<?= $checked_bgcolor; ?>">

            <?php if ($quantity_issued > 0) { ?>
            <td>
                <input type="checkbox" name="revert_check_box[]" value="<?= $val->item_code ?>" <?=$disabled ?> class=
                "revert_chk_box_class" style="
                <?= $disply ?>;float:right;margin-right: 5px;">
            </td>
            <?php } else { ?>
            <td>
                <input type="checkbox" name="revert_check_box[]" value="<?= $val->item_code; ?>" style="<?= $disply ?>"
                    <?=$disabled ?> >
                <button class="btn btn-danger" type="button"
                    onclick="delete_issue_items('<?= $val->item_code; ?>','<?= $item_id ?>')"
                    style="padding:0px 3px;float:right">
                    <i class="fa fa-times-circle" aria-hidden="true" id="fa_delete_spin_<?= $item_id ?>"></i></button>
            </td>
            <?php } ?>

            <td class="common_td_rules">
                <label>
                    <?= $val->item_code ?>
                </label>
            </td>
            <td class="common_td_rules">
                <label style="flex:1" id="item_description_id<?= $item_id ?>">
                    <?= $val->item_desc ?>
                </label>
                <span id="table_color_indication" class="<?= $bg_style ?>"></span>
            </td>
            <td class="common_td_rules">
                <label>
                    <?= $val->unit_name; ?>
                </label>
            </td>
            <td class="td_common_numeric_rules">
                <label id="approved_item_qty<?= $item_id ?>">
                    <?= $val->approved_qty ?>
                </label>
            </td>
            <td id="td_<?= $item_id; ?>" class="pos_rel td_common_numeric_rules">
                @if($balance_qty <= 0) <input type="hidden" readonly="" style=" text-align: right"
                    name="product_quantity['<?= $val->item_code; ?>']" value='0' id="final_issue_qty<?= $item_id ?>"
                    class="form-control vchk">
                    @else
                    <input type="text" readonly=""
                        onclick="get_product_batch('<?= $item_id ?>', '<?= $approved_qty ?>', '<?= $stock ?>','<?= $balance_qty ?>');"
                        onchange="clear_batch_data('<?= $val->item_code; ?>');" style=" text-align: right"
                        name="product_quantity['<?= $val->item_code; ?>']" value='0' id="final_issue_qty<?= $item_id ?>"
                        class="form-control vchk">
                    @endif
                    <div id="td_spin_<?= $item_id ?>" style="display:none">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                    <div class="issue_qty_pop" style="display:none" id="issue_qty_batch_pop_<?= $item_id ?>"></div>
            </td>

            <td class="td_common_numeric_rules">
                <label>
                    <?= $val->issued_qty ?>
                </label>
            </td>
            <td class="td_common_numeric_rules">
                <label id="product_balance_qty<?= $item_id ?>">
                    <?= $balance_qty ?>
                </label>
            </td>
            <?php
            if($stock==0){
                $blink_cls = "blinking";
                $stock = "No Stock";
            }else{
                $blink_cls = "td_common_numeric_rules";
                $stock = $stock;
            }

            if($sstock==0){
                $blinksstock_cls = "blinking";
                $sstock = "No Stock";
            }else{
                $blinksstock_cls = "td_common_numeric_rules";
                $sstock = $sstock;
            }
            ?>
            <td class="<?= $blink_cls ?>">
                <?= $stock; ?>
            </td>
            <td class="<?= $blinksstock_cls ?>">
                <?= $sstock; ?>
            </td>
            <td class="common_td_rules">{{ @$val->last_issued_date ? $val->last_issued_date : '' }}
            </td>
            <td class="common_td_rules">
                <input type="text" name="stock_remark" id="stock_remark_<?= $item_id ?>" class="form-control stock_remark_clas" value="<?= $remarks_data ?>" <?= $input_remark ?>>
            </td>
            <input type="hidden" id="product_code_id<?= $item_id ?>" value="<?= $val->item_code ?>">
            <input type="hidden" id="store_stock_id<?= $item_id ?>" value=" <?= $stock ?> ">
        </tr>
        <?php
                $item_id++;
            }
        }
        ?>
    </tbody>
</table>
