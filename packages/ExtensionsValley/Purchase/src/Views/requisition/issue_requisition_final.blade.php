@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
.blinking{
    animation:blinkingText 0.8s infinite;
    text-align: left !important;
    border-left: solid 1px #bbd2bd !important;
}
.label_blue{
    color: #2d2df5;
    font-size: 85%;
}
@keyframes blinkingText{
    0%{     color: #FF0000;    }
    49%{    color: #FF0000; }
    60%{    color: transparent; }
    99%{    color:transparent;  }
    100%{   color: #FF0000;    }
}
</style>
@endsection
@section('content-area')
<?php
if ($stock_head_result_view != '') {
    $Request_number = $stock_head_result_view[0]->request_no;
    $created_date = $stock_head_result_view[0]->created_date;
    $from_location_code = $stock_head_result_view[0]->from_location_code;
    $from_department = $stock_head_result_view[0]->from_loc;
    $to_location_code = $stock_head_result_view[0]->to_location_code;
    $to_department = $stock_head_result_view[0]->to_loc;
    $from_locationid = $stock_head_result_view[0]->from_location_code;
    $to_locationid = $stock_head_result_view[0]->to_location_code;
    $purchase_type = $stock_head_result_view[0]->purchase_type;
}
?>
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control">
                                <label class="custom_floatlabel">Request No.</label>
                                <div class="clearfix"></div>
                                <span class="label label_blue" id="rrequest_number">{{$Request_number}}</span>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control">
                                <label class="custom_floatlabel">Date And Time</label>
                                <div class="clearfix"></div>
                                <span class="label label_blue" id="created_date">{{$created_date}}</span>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control">
                                <label class="custom_floatlabel">Request From Department</label>
                                <div class="clearfix"></div>
                                <span class="label label_blue" id="from_department">{{$from_department}}</span>
                                <input type="hidden" id="from_location_code" name="from_location_code" value="{{$from_location_code}}">
                                <input type="hidden" name="from_location_code" value="2">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control">
                                <label class="custom_floatlabel"> Store</label>
                                <div class="clearfix"></div>
                                <span class="label label_blue" id="to_department">{{$to_department}}</span>
                                <input type="hidden" id="to_location_code" name="to_location_code" value="{{$to_location_code}}">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control">
                                <label class="custom_floatlabel">Status</label>
                                <div class="clearfix"></div>
                                <span class="label label_blue">{{$issue_status_text}}</span>

                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-block-start: 13px;">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="sort_all_print" name="sort_all_print" id="sort_all_print">
                            <label for="">Sort by Rack</label>
                        </div>

                    </div>
                </div>
            </div>
            <input type="hidden" id="purchase_type" value="{{ $purchase_type }}">
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="col-md-12">
                <form id="save_product_list">
                    @include('Purchase::requisition.issue_requisition_final_table')
                    {{ csrf_field() }}
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12">

                <div class="col-md-12 padding_sm text-right">
                    @if(isset($stock_id)&& $stock_id!=0)
                    @if($status !=4 && $status !=9 )
                    <button type="button" class="btn btn-secondary" onclick="print_product_list(2)"><i class="glyphicon glyphicon-print" ></i>
                        Print Last Issued Items </button>
                    <button type="button" class="btn btn-secondary" onclick="print_product_list(0)"><i class="glyphicon glyphicon-print" ></i>
                        Print Issued Items  </button>
                    @endif
                    <button type="button" class="btn btn-secondary" onclick="print_product_list(1)"><i class="glyphicon glyphicon-print"></i>
                        Print All Items </button>
                    @endif

                    <?php
                     if(isset($access_control[0]->submit) && $access_control[0]->submit ==0){
                                    $submit_style = "disabled";
                                }else{
                                    $submit_style = "";
                                }
                    if ($status == 4 || $status == 5 || $status == 6 ) {
                        if ($issue_status == 0) {
                            ?>

                            <button type="button"  {{$submit_style}}  data-toggle="modal" data-target="#close_request_modal"  class="btn btn-danger current_issue_items_request">
                                <i id="close_product_spin" class="fa  fa-close"></i>Close the Request</button>
                            <button type="button" {{$submit_style}} onclick="save_product_list(1)" class="btn light_purple_bg current_issue_items_request">

                                <i id="issue_product_spin" {{$submit_style}} class="fa fa-save"></i> Issue Stock</button>

                            <?php
                        }
                    }
                    ?>

                    @if (($status != 7 || $status == 10) && $status != 4 && $status != 6)
                    @if($issue_status==0)
                    <button class="btn btn-warning" {{$submit_style}} style="color: white;" onclick="revert_stock_issue();"> Revert Stock</button>
                    @endif
                    @endif
                </div>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12">

                    <div style="padding-top: 20px;">
                        <div class="col-md-3">
                            <div class="led-green"></div>
                            <label style="margin-left:10px;">Fully Received OR Rejected</label>
                        </div>
                        <div class="col-md-2">
                            <div class="led-yellow"></div>
                            <label style="margin-left:10px;">Partially Issued</label>
                        </div>
                        <div class="col-md-2">
                            <div class="led-red"></div>
                            <label style="margin-left:10px;">Not Issued</label>
                        </div>
                        <div class="col-md-2">
                            <div style='width:20px;height:20px;display:block;float:left;background-color:#efc9c9;'></div>
                            <label style="margin-left:10px;">Urgent</label>
                        </div>
                    </div>
                    <div class="pull-right">

                    </div>
                </div>
            </div>


        </div>
    </div>



    @include('Purchase::requisition.custom_modal_req')
    <input type="hidden" value="<?= $stock_id ?>" id="issue_list_stock_id">
    <input type="hidden" value="<?= $from_locationid ?>" id="from_locationid">
    <input type="hidden" value="<?= $to_locationid ?>" id="to_locationid">
    <input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
</div>
@endsection
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/issue_requesition.js")}}"></script>
@stop
