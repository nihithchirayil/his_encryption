<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            @page { margin: 0; }
            body { margin: 15px; }
            thead {display: table-header-group;}
            @media print{
                table thead {display: table-header-group;}
            }
        </style>
    </head>

    <body>
        <section class="content" style="margin-top:15px;">
            <div >
                <div style="width: 100%; overflow-x: auto;">

                    @if(isset($result) && count($result) > 0)
                    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%; font-size: 12px; border-collapse: collapse; border: 1px solid #000; margin: 15px auto auto auto ;">
                        <thead>
                            <tr>
                                <td colspan="10" align="center"><b style="font-size:15px">{{$title}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="3">Location :</td>
                                <td colspan="2">{{$result[0]->issue_location}}</td>
                                <td colspan="3">Doc.No :  </td>
                                <td colspan="2">{{$result[0]->request_no}}</td>
                            </tr>
                            <tr >
                                <td colspan="3">Issued To:</td>
                                <td colspan="2">{{$result[0]->requested_location}}</td>
                                <td colspan="3">Status :</td>
                                <td colspan="2">{{$result[0]->status}}</td>
                            </tr>
                            <tr >
                                <td colspan="3">Manual Doc No :</td>
                                <td colspan="2"></td>
                                @if($is_issued == 1)
                                <td colspan="3">Issued Date</td>
                                <td colspan="2">{{$result[0]->issued_date}}</td>
                                @elseif($is_issued == 0)
                                <td colspan="3">Approved Date</td>
                                <td colspan="2">{{$result[0]->approved_date}}</td>
                                @endif
                            </tr>
                            <tr >
                                <td colspan="3">Printed Date  </td>
                                <td colspan="2">{{date("d-M-Y H:i:s")}}</td>
                                @if(isset($result[0]->grn_no))
                                <td colspan="3">GRN No </td>
                                <td colspan="2">{{$result[0]->grn_no}}</td>
                                @else
                                <td colspan="5"></td>
                                @endif
                            </tr>
                            @if($is_issued == 0)
                            <tr >
                                <td colspan="3">Approved By  </td>
                                <td colspan="2"><?php ($result[0]->approved_by) ? $result[0]->approved_by : ''; ?></td>
                                <td colspan="5"></td>
                            </tr>
                            @endif
                            @if($is_issued == '1')
                            <tr>
                                <th>SI.No</th>
                                <th>Group</th>
                                <th>Product</th>
                                <th>Batch No</th>
                                <th>Exp Date</th>
                                <th>MRP</th>
                                <th>Issued Qty</th>
                                <th >Rack</th>
                                <th colspan="1">Amount</th>

                            </tr>
                        </thead>
                        <tbody >
                            <?php
                            $i = 1;
                            $print_total = 0;
                            ?>
                            @foreach($result as $p_val)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$p_val->groups}}</td>
                                <td style="max-width:100px ">{{$p_val->item_desc}}</td>
                                <td>{{$p_val->batch_no}}</td>
                                <td>{{date(\WebConf::getConfig('datetime_format_web'),strtotime($p_val->expiry_date))}}</td>
                                <td>{{$p_val->mrp}}</td>
                                <td>{{$p_val->issued_qty}}</td>
                                <td >{{$p_val->rack}}</td>
                                <td colspan="1">{{$p_val->amount}}</td>

                            </tr>
                            <?php
                            $i++;
                            $print_total += $p_val->amount;
                            $received_user = $p_val->received_by;
                            $created_by = $p_val->created_by;
                            $issue_user = $p_val->issue_by;
                            $verified_user = $p_val->verified_by;
                            ?>

                            @endforeach
                            <tr>
                                <td colspan="7" align="right"><b>Grand Total : </b></td>
                                <td colspan="7"><b>{{$print_total}}</b></td>
                            </tr>
                        </tbody>

                        @elseif($is_issued == '0')

                        <tr colspan="10">
                            <th>SI.No</th>
                            <th colspan="3">Product</th>
                            <th>Qty</th>
                            <th>Unit</th>
                            <th>Rack</th>
                            <th colspan="2">Remarks</th>
                        </tr>
                        </thead>
                        <tbody >
                            <?php $i = 1;
                            $print_total = 0; ?>
                            @foreach($result as $p_val)
                            <tr>
                                <td>{{$i}}</td>
                                <td colspan="3">{{$p_val->item_desc}}</td>
                                <td>{{$p_val->approved_qty}}</td>
                                <td>{{$p_val->unit_name}}</td>
                                <td>{{$p_val->rack}}</td>
                                <td colspan="2">{{$p_val->comments}}</td>
                            </tr>
                            <?php
                            $i++;
                            ?>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <!-- FOOTER SECTION-->
                    @if($is_issued == '1')
                    <div style="margin-top: 20px">
                        <table width="100%" style="border-collapse: collapse;" border="0">
                            <tbody>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr style="padding-top:100px;padding-bottom: 100px;">
                                    <td valign="top" colsapn="1">
                                        <i><div>Issued By :</div>
                                            <div> @if(isset($issue_user)){{$issue_user}}@endif </div>
                                        </i></td>
                                    <td valign="top" colspan="1">
                                        <i style="float: left">
                                            <div>Received By : </div><div>
                                                @if(isset($received_user)){{$received_user}}@endif
                                            </div></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td  valign="top" colspan="1"><i><div height="1000%"></div></i></td></tr>
                                <tr style="padding-top: 100px;padding-bottom: 100px;">
                                    <td  valign="top" colspan="1"><i style="float: left"><div>Checked By : </div><div>
                                                @if(isset($verified_user)){{$verified_user}}@endif</div></i></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div style="margin-top: 20px">
                        <table width="100%" style="border-collapse: collapse;" border="0">
                            <tbody>
                                <tr>
                                    <td>Remarks :</td>
                                </tr>
                                <tr>
                                    <td>Created By :
                                        @if(isset($created_by))
                                        {{$created_by}}
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @endif
                    @endif
                </div>
            </div>

        </section>

        <script type="text/javascript">
            window.onload = window.print();
            window.onload = window.close();
        </script>
    </body>
</html>
