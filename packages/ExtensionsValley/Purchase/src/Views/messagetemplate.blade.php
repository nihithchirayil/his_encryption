@if(Session::has('mes'))
    var type = "{{ Session::get('type') }}";
    var id = "{{ Session::get('status_head') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('mes') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('mes') }}");
            break;

        case 'success':
            toastr.success("Added Successfully");
            break;

        case 'error':
            toastr.error("{{ Session::get('mes') }}");
            break;
    }
    window.location.reload();
  @endif