<?php
use ExtensionsValley\core\CommonController;

if (isset($LocationDetails)){
   if(!empty($LocationDetails)){
       foreach ($LocationDetails as $loc) {
          
          ?>
           <li style="display: block; padding:5px 10px 5px 5px; "
               onclick='fill_location(this,"{{htmlentities($loc->location_name)}}","{{htmlentities($loc->id)}}")'>
               {!!$loc->location_name!!} [ {!!$loc->id!!} ]
               <?php // echo htmlentities($loc->location_name) . '     [   ' . htmlentities($loc->id) . ' ]'; ?>
           </li>
           <?php
       }
   }else {
       echo 'No Result Found';
   }
}

/* For Item details search */
if(isset($item_search_data)){
    if(!empty($item_search_data)){
        foreach ($item_search_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillitemsearchValues(this,atob("<?= base64_encode($desc->item_desc)?>"),"{{htmlentities($desc->item_code)}}")'>
                {!! $desc->item_desc !!} [{!!$desc->item_code!!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($manufacture_name_data)){
    if(!empty($manufacture_name_data)){
        foreach ($manufacture_name_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillmanufacture(this,atob("<?= base64_encode($desc->name)?>"),"{{htmlentities($desc->code)}}")'>
                {!! $desc->name !!} [{!!$desc->code!!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if(isset($ward_data)){
    if(!empty($ward_data)){
        foreach ($ward_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillWardValues(this,atob("<?= base64_encode($desc->name)?>"),"{{htmlentities($desc->id)}}")'>
                {!! $desc->name !!} [{!!$desc->id!!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if (isset($ip_no1) ) {
   
    if(!empty($ip_no1)){
        foreach($ip_no1 as $no) { ?> 
            <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fill_ip_no1(this,"{{htmlentities($no->ip_no1)}}","{{htmlentities($no->patient_id)}}","{{htmlentities($no->name)}}")'>
            <?php echo  $no->ip_no1; ?>
            </li>
        <?php }
    } else {
        echo 'No Results Found';
    }
}else if (isset($ip_no2) ) {
  
    if(!empty($ip_no2)){
        foreach($ip_no2 as $no) { ?> 
            <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fill_ip_no_doc(this,"{{htmlentities($no->ip_no1)}}","{{htmlentities($no->patient_id)}}","{{htmlentities($no->name)}}","{{htmlentities($no->id)}}")'>
            <?php echo  $no->ip_no1; ?>
            </li>
        <?php }
    } else {
        echo 'No Results Found';
    }
}else if (isset($ip_no_ins) ) {
   
    if(!empty($ip_no_ins)){
        foreach($ip_no_ins as $no) { ?> 
            <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fill_ip_no_ins(this,"{{htmlentities($no->ip_no1)}}","{{htmlentities($no->patient_id)}}","{{htmlentities($no->name)}}","{{htmlentities($no->company_name)}}","{{htmlentities($no->company_id)}}","{{htmlentities($no->visit_id)}}")'>
            <?php echo  $no->ip_no1 .' '.'['.$no->name.'('.$no->patient_id.')'.']'; ?>
            </li>
        <?php }
    } else {
        echo 'No Results Found';
    }
}else if(isset($vendor_data)){
    if(!empty($vendor_data)){
        foreach ($vendor_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillVendorSearchValues(this,"{{htmlentities($desc->vendor_name)}}","{{htmlentities($desc->vendor_code)}}")'>
                {!! $desc->vendor_name !!} [{!!$desc->vendor_code!!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($grn_no_data)){
    if(!empty($grn_no_data)){
        foreach ($grn_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillGrnNoValues2(this,"{{htmlentities($desc->grn_no)}}")'>
                {!! $desc->grn_no !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($admission_no_data)){
    if(!empty($admission_no_data)){
        foreach ($admission_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='filladmissionNoValues(this,"{{htmlentities($desc->admission_number)}}")'>
                {!! $desc->admission_number !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($doctor_name_data)){
    if(!empty($doctor_name_data)){
        $html_id = isset($html_id)?$html_id:NULL;
        foreach ($doctor_name_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorValues(this,"{{htmlentities($desc->doctor_name)}}","{{htmlentities($desc->id)}}","{{$html_id}}")'>
                {!! $desc->doctor_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($doctor_name_list)){
    if(!empty($doctor_name_list)){
        foreach ($doctor_name_list as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorNameValues(this,"{{htmlentities($desc->doctor_name)}}","{{htmlentities($desc->id)}}","{{$div_id}}")'>
                {!! $desc->doctor_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($hospital_name_data)){
    if(!empty($hospital_name_data)){
        $html_id = isset($html_id)?$html_id:NULL;
        foreach ($hospital_name_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillHospitalValues(this,"{{htmlentities($desc->hospital_name)}}","{{htmlentities($desc->id)}}","{{$html_id}}")'>
                {!! $desc->hospital_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($hospital_name_list)){
    if(!empty($hospital_name_list)){
        foreach ($hospital_name_list as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillHospitalNameValues(this,"{{htmlentities($desc->hospital_name)}}","{{htmlentities($desc->id)}}","{{$div_id}}")'>
                {!! $desc->hospital_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($mother_name_list)){
    if(!empty($mother_name_list)){
        foreach ($mother_name_list as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillMotherNameValues(this,"{{htmlentities($desc->mother_name)}}","{{htmlentities($desc->id)}}","{{htmlentities($desc->patient_id)}}")'>
                {!! $desc->mother_name !!}[{{$desc->patient_id}}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($baby_names)){
    if(!empty($baby_names)){
        foreach ($baby_names as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorValues(this,"{{htmlentities($desc->baby_name)}}","{{htmlentities($desc->id)}}")'>
                {!! $desc->baby_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($staff_name_data)){
    if(!empty($staff_name_data)){
        foreach ($staff_name_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorValues(this,"{{htmlentities($desc->name)}}","{{htmlentities($desc->id)}}")'>
                {!! $desc->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($patient_desc)){
    if(!empty($patient_desc)){
        foreach ($patient_desc as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillPatientValues(this,"{{htmlentities($desc->patient_name)}}","{{htmlentities($desc->id)}}")'>
                {!! $desc->patient_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($registration_filter_data)){
    if(!empty($registration_filter_data)){
        foreach ($registration_filter_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillFilterValues(this,"{{htmlentities($desc->name)}}","{{htmlentities($desc->id)}}")'>
                {!! $desc->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($sponsor_name_data)){
    if(!empty($sponsor_name_data)){
        foreach ($sponsor_name_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSponsorValues(this,"{{htmlentities($desc->name)}}","{{htmlentities($desc->id)}}")'>
                {!! $desc->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($req_no_data)){
    if(!empty($req_no_data)){
        foreach ($req_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillRequestNoValues2(this,"{{htmlentities($desc->request_no)}}")'>
                {!! $desc->request_no !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($request_no_data)){
    if(!empty($request_no_data)){
        $search_type='0';
        if(isset($type))
        $search_type=$type;
        foreach ($request_no_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillitemdescValues2(this,"{{htmlentities($desc->request_no)}}","{{htmlentities($type)}}")'>
                {!! $desc->request_no !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($item_desc_data)){
    if(!empty($item_desc_data)){
        $search_type='0';
        if(isset($type))
            $search_type=$type;
        foreach ($item_desc_data as $desc) {
        ?>
    <li style="display: block; padding:5px 10px 5px 5px; " data="{{htmlentities($desc->item_desc)}}"
        onclick='fillitemdescValues(this,atob("<?= base64_encode($desc->item_desc) ?>"),"{{htmlentities($desc->item_code)}}","{{htmlentities($type)}}")'>
        {!! $desc->item_desc !!} [{!!$desc->item_code!!}]
        @if(isset($pur_req) && ($pur_req == '1') && isset($desc->unit_name)) [{!!$desc->unit_name!!}] @endif
    </li>
    <?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if(isset($item_desc_with_missing_data)){
    if(!empty($item_desc_with_missing_data)){
        $search_type='0';
        if(isset($type))
        $search_type=$type;
        foreach ($item_desc_with_missing_data as $desc) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " data="{{htmlentities($desc->item_desc)}}"
    onclick='fillitemdescValues(this,atob("<?= base64_encode($desc->item_desc) ?>"),"{{htmlentities($desc->item_code)}}")'>
    {!! $desc->item_desc !!} [{!!$desc->item_code!!}]
</li>
<?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if(isset($item_desc_with_missing_data1)){ 
    if(!empty($item_desc_with_missing_data1)){
        $search_type='0';
        if(isset($type))
        $search_type=$type;
        foreach ($item_desc_with_missing_data1 as $desc) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " data="{{htmlentities($desc->item_desc)}}"
    onclick='fillitemdescValues1(this,atob("<?= base64_encode($desc->item_desc) ?>"),"{{htmlentities($desc->item_code)}}","{{$desc->unit_name}}")'>
    {!! $desc->item_desc !!} [{!!$desc->item_code!!}] 
</li>
<?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if(isset($item_req_by)){
    if(!empty($item_req_by)){
        foreach ($item_req_by as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillitemreq_by(this,"{{htmlentities($desc->user_name)}}","{{htmlentities($desc->user_id)}}")'>
                {!! $desc->user_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if(isset($user_data)){
    if(!empty($user_data)){
        foreach ($user_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillUserData(this,"{{htmlentities($desc->user_name)}}","{{htmlentities($desc->user_id)}}")'>
                {!! $desc->user_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if (isset($item_req_no)) {
    if (!empty($item_req_no)) {
        foreach ($item_req_no as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillitemreq_no(this,"{{htmlentities($desc->request_no)}}")'>
                {!! $desc->request_no !!}
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}
else if (isset($item_subcategory_code)) {
    $name='get_subcatgeoryselect';
    if(isset($type)&&$type=='1')
    {
        $name='get_subcatgeoryselect_req';
    }
    if (!empty($item_subcategory_code)) {
    ?>
        {!! Form::select($name,$item_subcategory_code,'',['class' => 'form-control custom_floatinput selectbox_initializr','placeholder' => 'Sub Category','title' => 'Sub Category','id' => $name,'style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
        <?php
    } else {
        ?>
        <select id="<?=$name?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" style="width:50%;color:#555555; padding:2px 12px;"><option value="">No Sub Category Found </option></select>
    <?php
    }
}else if (isset($item_subcategory_code1)) {
    $name='get_subcatgeoryselect1';
    if(isset($type)&&$type=='1')
    {
        $name='get_subcatgeoryselect_req1';
    }
    if (!empty($item_subcategory_code1)) {
    ?>
        {!! Form::select($name,$item_subcategory_code1,'',['class' => 'form-control custom_floatinput selectbox_initializr','title' => 'Sub Category','id' => $name,'style' => 'width:100%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
        <?php
    } else {
        ?>
        <select id="<?=$name?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" style="width:100%;color:#555555; padding:2px 12px;"><option value="">No Sub Category Found </option></select>
    <?php
    }
}
else if (isset($item_subdepartment_code)) {
    $name='sub_department[]';
    $id='sub_department';
    if (!empty($item_subdepartment_code)) {
        ?>
        {!! Form::select($name,$item_subdepartment_code,'',
        ['class' => 'form-control custom_floatinput selectbox_initializr',
        'title' => 'Sub Department','id' => $id,
        'style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple','onchange'=>'showitembox(this);']) !!}
        <?php
    } else {
        ?>
        <select id="<?=$id?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" 
        style="width:50%;color:#555555; padding:2px 12px;"><option value="">No Sub Category Found </option></select>
        <?php
    }
}
else if (isset($item_subdepartment_code1)) {
    $name='sub_department[]';
    $id='sub_department';
    if (!empty($item_subdepartment_code1)) {
        ?>
        {!! Form::select($name,$item_subdepartment_code1,'',
        ['class' => 'form-control custom_floatinput selectbox_initializr',
        'title' => 'Sub Department','id' => $id,
        'style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
        <?php
    } else {
        ?>
        <select id="<?=$id?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" 
        style="width:50%;color:#555555; padding:2px 12px;"><option value="">No Sub Category Found </option></select>
        <?php
    }
}
else if (isset($item_category_code)) {
    $name='from_category';
    if(isset($type)&&$type=='1')
    {
        $name='from_category';
    }
               if (!empty($item_category_code)) {
                   ?>
                       {!! Form::select($name,$item_category_code,'',['class' => 'form-control custom_floatinput selectbox_initializr','onchange'=>'get_subcategory(this)','placeholder' => 'Category','title' => 'Category','id' => $name,'style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
                       <?php
                   } else {
                       ?>
                       <select id="<?=$name?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" onchange="get_subcategory(this)" style="width:50%;color:#555555; padding:2px 12px;"><option value="">No Category Found </option></select>
                       <?php
                   }
}else if (isset($item_subcategory_code_rp)) {
        $name_id='sub_department_id';
        $name='sub_department_id[]';
           if (!empty($item_subcategory_code_rp)) {
               ?>
                   {!! Form::select($name,$item_subcategory_code_rp,'',['class' => 'form-control custom_floatinput selectbox_initializr','title' => 'Sub Category','id' => $name_id,'style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
                   <?php
               } else {
                   ?>
                   <select id="<?=$name_id?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" style="width:50%;color:#555555; padding:2px 12px;"></select>
                   <?php 
               }
}else if (isset($doctor_data)) {
        $name_id='doctor_id';
        $name='doctor_name[]';
           if (!empty($doctor_data)) {
               ?>
                   {!! Form::select($name,$doctor_data,'',['class' => 'form-control custom_floatinput selectbox_initializr','title' => 'Doctor','id' => $name_id,'style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
                   <?php
               } else {
                   ?>
                   <select id="<?=$name_id?>" name="<?=$name?>" class="form-control custom_floatinput selectbox_initializr" style="width:50%;color:#555555; padding:2px 12px;"></select>
                   <?php 
               }
}else if (isset($item_subcategory_list)) {
    if (!empty($item_subcategory_list)) {
    ?>
        {!! Form::select('item_subcatgeory[]',$item_subcategory_list,$sub_category,['class' => 'form-control custom_floatinput selectbox_initializr','title' => 'Sub Category','id' => 'item_subcatgeory','style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
    <?php
    } else {
    ?>
        <select id="item_subcatgeory" name="item_subcatgeory" class="form-control custom_floatinput selectbox_initializr" style="width:50%;color:#555555; padding:2px 12px;"><option value="All">No Sub Category Found </option></select>
    <?php
    }
}  
else if (isset($requestNoDetails)) {
    if(!empty($requestNoDetails)){
        foreach ($requestNoDetails as $data) { ?>

            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillStockRequestValues("{{htmlentities($data->stock_request_num)}}","{{htmlentities($data->from_location)}}","{{htmlentities($data->to_location)}}","{{htmlentities($data->from_location_name)}}","{{htmlentities($data->to_location_name)}}")'>
                {!! $data->stock_request_num !!}
                <?php //echo htmlentities($data->stock_request_num); ?>
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}

/* For location search */
else if (isset($fromLocationDetails)){
    if(!empty($fromLocationDetails)){
        foreach ($fromLocationDetails as $location) {
           ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillFromLocationValues(this,"{{htmlentities($location->location_name)}}","{{htmlentities($location->location_code)}}")'>
                {!!$location->location_name!!} [ {!!$location->location_code!!} ]
                <?php //echo htmlentities($location->location_name) . '     [   ' . htmlentities($location->location_code) . ' ]'; ?>
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}

else if (isset($toLocationDetails)){
    if(!empty($toLocationDetails)){
        foreach ($toLocationDetails as $location) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillToLocationValues(this,"{{htmlentities($location->location_name)}}","{{htmlentities($location->location_code)}}")'>
                {!! $location->location_name !!} [ {!! $location->location_code !!} ]
                <?php //echo htmlentities($location->location_name) . '     [   ' . htmlentities($location->location_code) . '   ]'; ?>
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}


else if  (isset($itemCodeDetails)) {
    if (!empty($itemCodeDetails)) {
        foreach ($itemCodeDetails as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities(number_format($item->selling_price,2))}}","{{htmlentities(number_format($item->mrp,2))}}","{{htmlentities($item->stock)}}",
                "{{htmlentities($item->available_qty)}}","{{htmlentities($item->chemical_name)}}","{{htmlentities($item->batch_no)}}", "{{htmlentities($item->stock_id)}}","{{htmlentities($item->purchase_rate)}}","{{htmlentities($item->purchase_cost)}}","{{htmlentities($item->expiry_date)}}","{{htmlentities($item->tax)}}","{{htmlentities($item->tax_category)}}","{{htmlentities($item->tax_amount)}}","{{htmlentities($item->selling_uom)}}" @if(isset($item->rack)), "{{htmlentities($item->rack)}}" @endif)'>
                {!!$item->item_desc!!}@if(isset($item->chemical_name))[Chemical Name: {!! $item->chemical_name !!}]@endif
                <?php //echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]'; ?>
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}
else if  (isset($itemCodeChemicalDetails)) {
    if (!empty($itemCodeChemicalDetails)) {
        foreach ($itemCodeChemicalDetails as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->chemical_name)}}","{{htmlentities($item->rack)}}")'>
                {!!$item->item_desc!!}@if(isset($item->chemical_name))[Chemical Name: {!! $item->chemical_name !!}]@endif
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}

else if  (isset($itemCodePharmacyReturn)) {
    if (!empty($itemCodePharmacyReturn)) {
        foreach ($itemCodePharmacyReturn as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->chemical_name)}}","{{htmlentities($item->rack)}}")'>
                {!!$item->item_desc!!} @if(isset($item->chemical_name)) [Chemical Name: {!! $item->chemical_name !!}] @endif
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}

else if (isset($itemCodePoReturn)) {
    if (!empty($itemCodePoReturn)) {
        foreach ($itemCodePoReturn as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; <?php if($item->cnt > 0) echo 'background-color:#f4d442'; ?> "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}",
                                             "{{htmlentities($item->stock)}}" )'>
                {!!$item->item_desc!!} @if(isset($item->chemical_name)) [Chemical Name: {!! $item->chemical_name !!}] @endif
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}
// Department Stock Consumption
else if  (isset($itemCodePharmacyReturndpt)) {
    $type = isset($type)?$type:0;
    if (!empty($itemCodePharmacyReturndpt)) {
        foreach ($itemCodePharmacyReturndpt as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px;" 
            @if($type == '0') onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}",atob("<?= base64_encode($item->item_desc) ?>"),"{{htmlentities($item->chemical_name)}}")' 
            @elseif($type == '2') onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}",atob("<?= base64_encode($item->item_desc)?>"),"{{htmlentities($item->is_fractional)}}")' 
            @else onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}",atob("<?= base64_encode($item->item_desc) ?>"))' @endif >
            @if($type == '0') 
                {!!$item->item_desc!!} @if(isset($item->chemical_name)) [Chemical Name: {!! $item->chemical_name !!}] @endif
            @else
             {!!$item->item_desc!!} 
            @endif 

         </li>
         <?php
        }
    } else {
        echo 'No Result Found';
    }
}
else if  (isset($itemCodePharmacyReturndpt1)) {
    $type = isset($type)?$type:0;
    if (!empty($itemCodePharmacyReturndpt1)) {
        foreach ($itemCodePharmacyReturndpt1 as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px;" 
            @if($type == '0') onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}",atob("<?= base64_encode($item->item_desc) ?>"),"{{htmlentities($item->chemical_name)}}","{{htmlentities($item->stock)}}")' 
            @elseif($type == '2') onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}",atob("<?= base64_encode($item->item_desc)?>"),"{{htmlentities($item->is_fractional)}}","{{htmlentities($item->stock)}}")' 
            @else onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}",atob("<?= base64_encode($item->item_desc) ?>"),"{{htmlentities($item->stock)}}","{{htmlentities($item->is_fractional)}}")' @endif >
            @if($type == '0') 
                {!!$item->item_desc!!} @if(isset($item->chemical_name)) [Chemical Name: {!! $item->chemical_name !!}] @endif
            @else
             {!!$item->item_desc!!} 
            @endif 

         </li>
         <?php
        }
    } else {
        echo 'No Result Found';
    }
}


/* For stock request item selection */
else if  (isset($itemRequest)) {
    if (!empty($itemRequest)) {
        foreach ($itemRequest as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}", "{{htmlentities($item->available_qty)}}","{{htmlentities($item->chemical_name)}}",
                "{{htmlentities($item->reorder_level)}}","{{htmlentities($item->pharmacy_qty)}}")'>
                {!!$item->item_desc!!} [Chemical Name: {!! $item->chemical_name !!}] {{--[ Item Code: {!! $item->item_code !!}]--}} [Store Stock: {!! $item->available_qty !!}] [Pharmacy Stock: {!! $item->pharmacy_qty !!}]
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}

/* For Stock Request Auto Controller */
else if  (isset($itemRequeststockRequestAuto)) {
    if (!empty($itemRequeststockRequestAuto)) {
        foreach ($itemRequeststockRequestAuto as $item) {
           ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}", "{{htmlentities($item->available_qty)}}","{{htmlentities($item->chemical_name)}}",
                    "{{htmlentities($item->reorder_level)}}","{{htmlentities($item->pharmacy_qty)}}","{{htmlentities($item->rack)}}")'>
            {!!$item->item_desc!!} [Chemical Name: {!! $item->chemical_name !!}] {{--[ Item Code: {!! $item->item_code !!}]--}} [Store Stock: {!! $item->available_qty !!}] [Pharmacy Stock: {!! $item->pharmacy_qty !!}]
        </li>
        <?php
        }
    } else {
        echo 'No Result Found';
    }
}


/* For Pharmacy Indent Item Search */
else if  (isset($itemSearch)) {
    if (!empty($itemSearch)) {
        foreach ($itemSearch as $item) {
            ?>
            <li style="display: block; padding:2px 10px 2px 2px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}",
                "{{htmlentities($item->pharmacy_qty)}}")'>
                {!!$item->item_desc!!} [Chemical Name: {!! $item->chemical_name !!}] [ Item Code: {!! $item->item_code !!}] [Stock: {!! $item->pharmacy_qty !!}]
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}

else if (isset($vendorDetails)){
    if(!empty($vendorDetails)){
        foreach ($vendorDetails as $vendor) {
           ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillVendorValues(this,"{{htmlentities($vendor->vendor_name)}}","{{htmlentities($vendor->vendor_code)}}")'>
                {!!$vendor->vendor_name!!} [GST No: {!! $vendor->gst_vendor_code or '' !!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}



/* For Voucher Number details search */
else if  (isset($voucherNoSearch)) {
    if(!empty($voucherNoSearch)){
        foreach ($voucherNoSearch as $data) { ?>

            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillVoucherEntryValues("{{htmlentities($data->id)}}","{{htmlentities($data->purchase_voucher_no)}}")'>
                {!! $data->purchase_voucher_no !!}
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}


else if  (isset($itemDetails)) {
    if (!empty($itemDetails)) {
        foreach ($itemDetails as $item) {
        	$tax_rate = empty($item->tax_rate)?'0.00':$item->tax_rate;
            ?>
            <li style="display: block; padding:2px 10px 2px 2px; "
               onclick='fillItemValues(this,"{{htmlentities($item->hsn_code)}}","{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","@if(isset($item->selling_price)){{htmlentities($item->selling_price)}}@endif","@if(isset($item->mrp)){{htmlentities($item->mrp)}}@endif",
               "@if(isset($item->stock)){{htmlentities($item->stock)}}@endif","@if(isset($item->available_qty)){{htmlentities($item->available_qty)}}@endif","{{htmlentities($item->chemical_name)}}","{{htmlentities($item->item_id)}}","@if(isset($item->batch_no)){{htmlentities($item->batch_no)}}@endif",
               "{{htmlentities($tax_rate)}}","@if(isset($item->selling_uom)){{htmlentities($item->selling_uom)}}@endif","{{htmlentities($item->selling_uom_id)}}","{{htmlentities($item->selling_uom_desc)}}","{{htmlentities($item->tax_category)}}","{!! $item->pack_size or 1 !!}")'>
                {!! $item->item_desc !!} [HSN Code: {!! $item->hsn_code or '' !!}]
               <?php
                if(isset($item->chemical_name)) echo ' [ Chemical Name: ' . htmlentities($item->chemical_name). ' ]';
               if(isset($item->batch_no)) echo '  [ Batch:   ' . htmlentities($item->batch_no) . '   ]';
               if(isset($item->stock)) echo '  [ Stock:   ' . $item->stock . '   ]'; ?>
           </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}


else if  (isset($uomCodeDetails)) {
    if (!empty($uomCodeDetails)) {
        foreach ($uomCodeDetails as $uom) {
        	$uom_list_txt = htmlentities($uom->uom_name) . ' [ ' . htmlentities($uom->uom_code) . ' ]';
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillUomValues(this,"{{htmlentities($uom->conversion_factor)}}","{{htmlentities($uom->id)}}","{{htmlentities($uom->uom_code)}}","{{htmlentities($uom->uom_name)}}", "{!!$uom_list_txt!!}")'>
                {!! $uom_list_txt !!}
            </li>
            <?php
        }
    } else { ?>
        <li style="display: block; padding:5px 10px 5px 5px; " > No result found.  </li>
        <li class="getUomTxtID" data-toggle="modal" data-target="#addUom" style="display: block; padding:5px 10px 5px 5px;background-color: #4285F4; font-weight: bold;color: #fff">
            Add New UOM
        </li>
   <?php }
}

else if  (isset($uomDetails)) {
    if (!empty($uomDetails)) {
        foreach ($uomDetails as $uom) {
        	$uom_list_txt = htmlentities($uom->desc) . ' [ ' . htmlentities($uom->code) . ' ]';
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillUomValues(this,"{{htmlentities($uom->conversion_value)}}","{{htmlentities($uom->id)}}","{{htmlentities($uom->code)}}","{{htmlentities($uom->desc)}}", "{!!$uom_list_txt!!}")'>
                {!! $uom_list_txt !!}
            </li>
            <?php
        }
    } else { ?>
        <li style="display: block; padding:5px 10px 5px 5px; " > No result found.  </li>
        <li class="getUomTxtID" data-toggle="modal" data-target="#addUom" style="display: block; padding:5px 10px 5px 5px;background-color: #4285F4; font-weight: bold;color: #fff">
            Add New UOM
        </li>
   <?php }
}


else if  (isset($freeItemUomCodeDetails)) {
    if (!empty($freeItemUomCodeDetails)) {
        foreach ($freeItemUomCodeDetails as $uom) {
        	$uom_list_txt = htmlentities($uom->uom_name) . ' [ ' . htmlentities($uom->uom_code) . ' ]';
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillFreeItemUomValues(this,"{{htmlentities($uom->conversion_factor)}}","{{htmlentities($uom->uom_code)}}","{{htmlentities($uom->uom_name)}}", "{!!$uom_list_txt!!}")'>
                {!! $uom_list_txt !!}
            </li>
            <?php
        }
    } else { ?>
            <li style="display: block; padding:5px 10px 5px 5px; " > No result found.  </li>
<!--            <li class="getUomTxtID" data-toggle="modal" data-target="#addUom" style="display: block; padding:5px 10px 5px 5px;background-color: #4285F4; font-weight: bold;color: #fff">
                Add New UOM
            </li>-->
   <?php }
}
else if (isset($item_po_no)) {
    if (!empty($item_po_no)) {
        $type = isset($type)?$type:0;
        foreach ($item_po_no as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillitempo_no(this,"{{htmlentities($desc->request_no)}}","{{$type}}")'>
                {!! $desc->request_no !!}
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}
else if(isset($po_req_by)){
    if(!empty($po_req_by)){
        foreach ($po_req_by as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillporeq_by(this,"{{htmlentities($desc->user_name)}}","{{htmlentities($desc->user_id)}}")'>
                {!! $desc->user_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}
else if(isset($patient_list)){
    if(!empty($patient_list)){
        foreach ($patient_list as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_name(this,"{{htmlentities($data->full_name)}}","{{htmlentities($data->id)}}")'>
                {!! $data->full_name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($patient_detail_data)){
    if(!empty($patient_detail_data)){
        foreach ($patient_detail_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fill_patient_data(this,"{{htmlentities($data->patient_name)}}","{{htmlentities($data->patient_id)}}","{{htmlentities($data->id)}}")'>
                {!! $data->patient_name !!} [{!!$data->patient_id!!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($bill_no2)){
    if(!empty($bill_no2)){
        foreach ($bill_no2 as $data) {            
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_bill_no2(this,atob("<?= base64_encode($data->bill_no)?>"),atob("<?= base64_encode($data->id)?>"))'>
                {!! $data->bill_no !!} 
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($bill_no1)){
    if(!empty($bill_no1)){
        foreach ($bill_no1 as $data) {            
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_bill_no1(this,atob("<?= base64_encode($data->bill_no)?>"),atob("<?= base64_encode($data->id)?>"))'>
                {!! $data->bill_no !!} 
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($bill_no)){
    if(!empty($bill_no)){
        foreach ($bill_no as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_bill_no(this,atob("<?= base64_encode($data->bill_no)?>"))'>
                {!! $data->bill_no !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($patient_name)){
    if(!empty($patient_name)){
        foreach ($patient_name as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_name(this,"{{htmlentities($data->name)}}","{{htmlentities($data->id)}}")'>
                {!! $data->name !!}[{!!$data->patient_id!!}]
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($patient_id_data)){
    if(!empty($patient_id_data)){
        foreach ($patient_id_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_id(this,"{{htmlentities($data->patient_id)}}","{{htmlentities($data->id)}}")'>
                {!! $data->patient_id !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($speciality_name)){
    if(!empty($speciality_name)){
        foreach ($speciality_name as $data) {
            ?>
            <li id="li_{{$data->id}}" style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_speciality_name(this,"{{htmlentities($data->name)}}","{{htmlentities($data->id)}}")'>
                {!! $data->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($floor_location_name)){
    if(!empty($floor_location_name)){
        foreach ($floor_location_name as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_floor_location(this,"{{htmlentities($data->floor_location)}}","{{htmlentities($data->id)}}")'>
                {!! $data->floor_location !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($service_name_data)){
    if(!empty($service_name_data)){
        foreach ($service_name_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_service_desc(this,"{{htmlentities($data->service_desc)}}","{{htmlentities($data->service_code)}}")'>
                {!! $data->service_desc !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($filter_name_data)){
    if(!empty($filter_name_data)){
        foreach ($filter_name_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_filter_desc(this,"{{htmlentities($data->name)}}","{{htmlentities($data->id)}}")'>
                {!! $data->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}else if(isset($filter_name_data1)){
    if(!empty($filter_name_data1)){
        foreach ($filter_name_data1 as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_filter_desc1(this,"{{htmlentities($data->name)}}","{{htmlentities($data->id)}}")'>
                {!! $data->name !!}
            </li>
            <?php
        }
    }else {
        echo 'No Result Found';
    }
}elseif (isset($op_no_srch) ) {
    if(!empty($op_no_srch)){
        foreach($op_no_srch as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; " data-pid ="{{htmlentities(ucfirst($item->id))}}" data-op_id = "{{ htmlentities(ucfirst($item->patient_id))}}"
            onclick='fillOpDetials(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}")'>
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}elseif (isset($OpNoDetails) ) {
    if(!empty($OpNoDetails)){
        foreach($OpNoDetails as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}")'>
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}elseif (isset($OpIpNoDetails) ) {
    if(!empty($OpIpNoDetails)){
        foreach($OpIpNoDetails as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}","{{ htmlentities(ucfirst($item->ip_number))}}")'>
          @if(!empty($item->ip_number)) 
        <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']' . ' [' . htmlentities(ucfirst($item->ip_number)) . ']'; ?>
        @else
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        @endif
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}elseif(isset($bookingOpNoDetails)){
    if(!empty($bookingOpNoDetails)){
        foreach($bookingOpNoDetails as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}","{{htmlentities(ucfirst($item->gender))}}","{{htmlentities(ucfirst($item->mobile_no))}}","{{htmlentities(ucfirst($item->address))}}","{{htmlentities(ucfirst($item->age))}}")'>
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}elseif(isset($opPatientDetails)){   
    if(!empty($opPatientDetails)){
        foreach($opPatientDetails as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}","{{htmlentities(ucfirst($item->gender))}}","{{htmlentities(ucfirst($item->mobile_no))}}","{{htmlentities(ucfirst($item->address))}}","{{htmlentities(ucfirst($item->age))}}","{{htmlentities(ucfirst($item->email))}}","{{htmlentities(ucfirst($item->dob))}}","{{htmlentities(ucfirst($item->last_op_visit_id))}}","{{htmlentities(ucfirst($item->doctor_name))}}","{{htmlentities(ucfirst($item->doctor_id))}}")'>
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}elseif (isset($package_name) ) {
    if(!empty($package_name)){
        foreach($package_name as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillpackageDetials(this,"{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->package_name))}}")'>
            <?php echo htmlentities(ucfirst($item->package_name)); ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}elseif (isset($icd_details) ) {
    if(!empty($icd_details)){
        $html_id = isset($html_id)?$html_id:NULL;
        foreach($icd_details as $icd_name) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillicdname(this,"{{htmlentities(ucfirst($icd_name->icd10_code))}}",atob("<?= base64_encode($icd_name->title)?>"),
            "{{htmlentities(ucfirst($icd_name->id))}}","{{$html_id}}")'>
            <?php echo htmlentities(ucfirst($icd_name->title)); ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}
elseif (isset($transaction_no) ) {
    if(!empty($transaction_no)){
        foreach($transaction_no as $no) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='filltranscationno(this,"{{htmlentities($no->transaction_num)}}")'>
            <?php echo $no->transaction_num; ?>
            </li>
        <?php }
    } else {
        echo 'No Results Found';
    }
}elseif (isset($ip_no) ) {
    if(!empty($ip_no)){
        foreach($ip_no as $no) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fill_ip_no(this,"{{htmlentities($no->ip_no)}}")'>
            <?php echo $no->ip_no; ?>
            </li>
        <?php }
    } else {
        echo 'No Results Found';
    }
}else if(isset($doctor_name_data_box)){
    if(!empty($doctor_name_data_box)){
        if($edit==0){
        foreach ($doctor_name_data_box as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorValues("{{htmlentities($desc->doctor_name)}}","{{htmlentities($desc->id)}}","{{$doc_no}}")'>
                {!! $desc->doctor_name !!}
            </li>
            <?php
        }
        }else{
             foreach ($doctor_name_data_box as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorValues_edit("{{htmlentities($desc->doctor_name)}}","{{htmlentities($desc->id)}}","{{$doc_no}}")'>
                {!! $desc->doctor_name !!}
            </li>
            <?php
        }
        }
    }else {
        echo 'No Result Found';
    }
}elseif (isset($OpNoDetails_box) ) {
    if(!empty($OpNoDetails_box)){
        if($edit==0){
        foreach($OpNoDetails_box as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}","{{htmlentities($item->age).' '.htmlentities(ucfirst($item->gender))}}","{{htmlentities($op_no_box)}}")'>
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        </li>
        <?php
        }
    }else{
        foreach($OpNoDetails_box as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials_edit(this,"{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}","{{htmlentities($item->age).' '.htmlentities(ucfirst($item->gender))}}","{{htmlentities($op_no_box)}}")'>
            <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
        </li>
        <?php
        }
    }
    }else {
        echo 'No Results Found';
    }
}elseif (!empty($category_list)) {
?>
    {!! Form::select('from_category[]',$category_list,'',['class' => 'form-control custom_floatinput selectbox_initializr','id' => 'from_category','style' => 'width:50%;color:#555555; padding:2px 12px;','multiple'=>'multiple']) !!}
<?php
} elseif (!empty($sub_category_list)) {
?>
    {!! Form::select('sub_category[]',$sub_category_list,'',['multiple'=>'multiple','class' => 'form-control custom_floatinput selectbox_initializr','id' => 'sub_category','style' => 'color:#555555; padding:2px 12px;']) !!}
<?php
} 
elseif (!empty($category_list1)) {
?>
    {!! Form::select('category_name_hidden[]',$category_list1,'',['multiple'=>'multiple','class' => 'form-control custom_floatinput selectbox_initializr','id' => 'category_code','style' => 'color:#555555; padding:2px 12px;','onchange'=>'getSubCategory(this)',]) !!}
<?php
}  elseif (!empty($sub_dep_list)) {
   ?>
      {!! Form::select('sub_department[]',$sub_dep_list,'',['multiple'=>'multiple','class' => 'form-control custom_floatinput selectbox_initializr','id' => 'sub_department','style' => 'color:#555555; padding:2px 12px;']) !!}
<?php } ?>
 

