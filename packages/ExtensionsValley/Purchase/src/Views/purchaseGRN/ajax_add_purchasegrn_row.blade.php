<?php $row_count = ++$row_count; ?>
<tr style="background: #FFF;" class="row_class" id="row_data_{{ $row_count }}">
    <td class='row_count_class'></td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
            id="item_desc_{{ $row_count }}" onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
            onclick="item_history({{ $row_count }})" class="form-control popinput" name="item_desc[]"
            placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
    </td>
    <td>
        <button id="getGrnTaxRateBtn{{ $row_count }}" class="btn light_purple_bg grn_drop_btn"
            onclick="chargesModel({{ $row_count }})"><i id="getGrnTaxRateSpin{{ $row_count }}"
                class="fa fa-plus"></i></button>
    </td>
    <td><input name='batch[]' readonly id="batch_row{{ $row_count }}" class="form-control" type="text"
            value=""></td>
    <td><input name='expiry_date[]' id="expiry_date{{ $row_count }}" id="expiry_date{{ $row_count }}"
            class="form-control expiry_date" readonly type="text" value=""></td>
    <td><input class="form-control number_class" readonly name='grn_qty[]' id="grn_qty{{ $row_count }}"
            type="text" value="0.00"></td>
    <td><input class="form-control number_class" readonly name='free_qty[]' id="free_qty{{ $row_count }}"
            type="text" value="0.00"></td>
    <td title="Unit converion">
        <select name="uom_select[]" id="uom_select_id_{{ $row_count }}" disabled class="form-control">

        </select>
    </td>
    <td><input class="form-control number_class" id="tot_qty_{{ $row_count }}" readonly name="tot_qty[]"
            type="text" value="0.00"></td>
    <td><input class="form-control number_class" id="unit_rate{{ $row_count }}" readonly name="unit-rate[]"
            type="text" value="0"></td>
    <td><input class="form-control number_class" id="unit_cost{{ $row_count }}" readonly name="unit-cost[]"
            type="text" value="0"></td>
    <td>
        <input class="form-control number_class" id="unit-mrp_{{ $row_count }}" readonly name="unit-mrp[]"
            type="text" value="0">
    </td>
    <td>
        <input class="form-control number_class" id="sales_price_{{ $row_count }}" readonly name="sales_price[]"
            type="text" value="0">
    </td>
    <td><input class="form-control number_class" id="tot_tax_amt_{{ $row_count }}" readonly name="tot_tax_amt[]"
            type="text" value="0.00"></td>
    <td><input class="form-control number_class" id="tot_dic_amt_{{ $row_count }}" readonly name="tot_dic_amt[]"
            type="text" value="0.00"></td>
    <td><input class="form-control number_class" id="net_cost_{{ $row_count }}" readonly name="net_cost[]"
            type="text" value="0.00"></td>
    <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i></td>
</tr>
