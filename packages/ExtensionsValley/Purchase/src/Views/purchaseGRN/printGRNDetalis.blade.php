<?php
$total_cgst = 0.0;
$total_sgst = 0.0;
$total_igst = 0.0;
$total_cess = 0.0;
$total_discount1 = 0.0;
$total_discount2 = 0.0;
$total_othercharges = 0.0;
$total_freightcharges = 0.0;
$total_amt = 0.0;
$net_total = 0.0;
$tot_tax = 0.0;
$oth_charges = 0.0;
$cgst = 0.0;
$sgst = 0.0;
$igst = 0.0;
?>
<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" class="table no-margin table_sm table-striped no-border styled-table"
                        style="border: 1px solid #CCC;width: 100%">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:#4ea5f8;color:black;border-spacing: 0 1em;font-family:sans-serif">
                                <th style="text-align: center" colspan="21">
                                    GRN
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="10" width="60%">
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?></th>
                                <th class="common_td_rules" colspan="11" width="40%">GRN No :
                                    <?= @$grn_no ? $grn_no : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="10" width="60%">Address :
                                    <?= @$vendor_data['vendor_address'] ? $vendor_data['vendor_address'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="5" width="40%">Bill No :
                                    <?= @$bill_no ? $bill_no : '' ?></th>
                                <th class="common_td_rules" colspan="6" width="40%">Bill Date :
                                    <?= @$po_date ? $po_date : '' ?></th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="10" width="60%"> PO NO.:
                                    <?= $po_no ? $po_no : '-' ?>
                                </th>
                                <th class="common_td_rules" colspan="11" width="40%">Email:
                                    <?= @$vendor_data['vendor_email'] ? $vendor_data['vendor_email'] : '' ?></th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="10" width="60%">Telephone No. :
                                    <?= @$vendor_data['contact_no'] ? $vendor_data['contact_no'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="11" width="40%">Location :
                                    <?= @$location_name ? $location_name : '' ?></th>
                            </tr>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="5%">SL.No.</th>
                                <th width="40%"><b>Item Name</b></th>
                                <th width="40%"><b>HSN CODE</b></th>
                                <th width="40%"><b>Batch No.</b></th>
                                <th width="40%"><b>Expiry Date</b></th>
                                <th width="10%"><b>Unit</b></th>
                                <th width="5%"><b>Qty</b></th>
                                <th width="5%"><b>Free Qty</b></th>
                                <th width="5%"><b>Total Qty</b></th>
                                <th width="5%"><b>Disc.1<br> (%)</b></th>
                                <th width="5%"><b>Disc.2<br> (%)</b></th>
                                <th width="5%"><b>CGST<br> (%)</b></th>
                                <th width="5%"><b>SGST <br> (%)</b></th>
                                <th width="5%"><b>IGST<br> (%)</b></th>
                                <th width="5%"><b>Rate</b></th>
                                <th width="5%"><b>MRP</b></th>
                                <th width="5%"><b>Unit Rate</b></th>
                                <th width="5%"><b>Unit Cost</b></th>
                                <th width="5%"><b>Sale Price</b></th>
                                <th width="5%"><b>Total Rate</b></th>
                                <th width="5%"><b>Net Rate</b></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($item_string)!=0){
                                foreach ($item_string as $each) {
                                    $total_cgst += floatval($each['cgst_amt']);
                                    $total_sgst += floatval($each['sgst_amt']);
                                    $total_igst += floatval($each['igst_amt']);
                                    $total_discount1 += floatval($each['discount_amt']);
                                    $total_discount2 += floatval($each['special_discount_amt']);
                                    $total_othercharges += floatval($each['othercharge_amt']);
                                    $total_freightcharges += floatval($each['flightcharge_amt']);
                                    $total_amt += floatval($each['totalRate']);
                                    $net_total += floatval($each['netRate']);
                                    $cgst += floatval($each['cgst_amt']);
                                    $sgst += floatval($each['sgst_amt']);
                                    $igst += floatval($each['igst_amt']);
                                    ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td class="common_td_rules"><?= @$each['item_desc'] ? $each['item_desc'] : '' ?></td>
                                <td class="common_td_rules"><?= @$each['hsn_code'] ? $each['hsn_code'] : '' ?></td>
                                <td class="common_td_rules"><?= @$each['batch_no'] ? $each['batch_no'] : '' ?></td>
                                <td class="common_td_rules"><?= @$each['expiry_date'] ? $each['expiry_date'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$uom_array[$each['item_id']] ? $uom_array[$each['item_id']] : '' ?></td>
                                <td class="td_common_numeric_rules"><?= @$each['grn_qty'] ? $each['grn_qty'] : 0 ?></td>
                                <td class="td_common_numeric_rules"><?= @$each['free_qty'] ? $each['free_qty'] : 0 ?>
                                </td>
                                <td class="td_common_numeric_rules"><?= @$each['all_total_qty'] ? $each['all_total_qty'] : 0 ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['discount_per'], 2, '.', '') ?></td>
                                <td class="td_common_numeric_rules">
                                    <?= number_format($each['special_discount_per'], 2, '.', '') ?></td>
                                <td class="td_common_numeric_rules"><?= number_format($each['cgst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules"><?= number_format($each['sgst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules"><?= number_format($each['igst_per'], 2, '.', '') ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['item_rate'] ? number_format($each['item_rate'], 2, '.', '') : 0 ?></td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['grn_mrp'] ? number_format($each['grn_mrp'], 2, '.', '') : 0 ?></td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['unit_rate'] ? number_format($each['unit_rate'], 2, '.', '') : 0 ?></td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['unit_cost'] ? number_format($each['unit_cost'], 2, '.', '') : 0 ?></td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['selling_price'] ? number_format($each['selling_price'], 2, '.', '') : 0 ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['totalRate'] ? number_format($each['totalRate'], 2, '.', '') : 0 ?></td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['netRate'] ? number_format($each['netRate'], 2, '.', '') : 0 ?></td>
                            </tr>
                            <?php
                                       $i++;
                                        }
                                        $tot_tax = floatval($total_cgst) + floatval($total_sgst) + floatval($total_igst) + floatval($total_cess);
                                        $oth_charges = floatval($total_othercharges) + floatval($total_freightcharges);
                                        }else {
                                            ?>
                            <tr>
                                <td colspan="21" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                            <?php
                                        }
                                        $total_discount= floatval($total_discount1) + floatval($total_discount2);
                                        $total_net = (floatval($total_amt) + floatval($tot_tax) + floatval($oth_charges) + floatval($round_off)) - (floatval($total_discount)+floatval($bill_discount));

                                        ?>

                            <tr>
                                <th colspan="20" class="td_common_numeric_rules"> Gross Amount :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($total_amt, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="20" class="td_common_numeric_rules">Item Discount(-) : </th>
                                <th class="td_common_numeric_rules"> <?= number_format($total_discount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="20" class="td_common_numeric_rules">Bill Discount(-) : </th>
                                <th class="td_common_numeric_rules"> <?= number_format($bill_discount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="20" class="td_common_numeric_rules">Freight/Other :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($oth_charges, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="20" class="td_common_numeric_rules">CGST :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($cgst, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="20" class="td_common_numeric_rules">SGST :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($sgst, 2, '.', '') ?>
                                </th>

                            </tr>
                            <tr>
                                <th colspan="20" class="td_common_numeric_rules">IGST : </th>
                                <th class="td_common_numeric_rules"> <?= number_format($igst, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th colspan="20" class="td_common_numeric_rules">Total Tax :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($tot_tax, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th colspan="20" class="td_common_numeric_rules">RoundOff :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($round_off, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th colspan="20" class="td_common_numeric_rules">Total Amount :</th>
                                <th class="td_common_numeric_rules"> <?= number_format($total_net, 2, '.', '') ?>
                                </th>
                            </tr>
                            @if($enable_payment_mode_grn ==1)
                            <tr>
                                <td colspan="2" class="common_td_rules"><b>Payment Mode :</b></td>
                                <td colspan="2" class="common_td_rules">@php
                                    $payment_mode = \DB::table('payment_mode')->where('code',$payment_details['payment_mode'])->value('name');
                                @endphp
                                {{$payment_mode}}</td>

                                <td colspan="2" class="common_td_rules"><b>Bank :</b></td>
                                <td colspan="4" class="common_td_rules">
                                    @php
                                        $bank = \DB::table('bank_detail')->where('bank_code',$payment_details['bank'])->value('bank_name');
                                    @endphp
                                    {{$bank}}
                                </td>
                                <td class="common_td_rules"><b>Card No :</b></td>
                                <td colspan="2" class="common_td_rules">{{$payment_details['card_no']}}</td>

                                <td class="common_td_rules"><b>Phone No :</b></td>
                                <td colspan="2" class="common_td_rules">{{$payment_details['payment_phone_no']}}</td>

                                <td class="common_td_rules"><b>Exp Month :</b></td>
                                <td class="common_td_rules">{{$payment_details['exp_month']}}</td>

                                <td class="common_td_rules"><b>Exp Year :</b></td>
                                <td colspan="2" class="common_td_rules">{{$payment_details['exp_year']}}</td>

                            </tr>
                            @endif

                            <tr>
                                <td class="common_td_rules" colspan="21">
                                    <b>Remarks: </b> <?= @$full_remarks ? $full_remarks : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="4" class="common_td_rules" width="30%">
                                    GST NO. <?= @$hospital_header[0]->gst_no ? $hospital_header[0]->gst_no : '' ?>
                                </th>
                                <th colspan="5" class="common_td_rules" width="40%">
                                    Drug License No.
                                    <?= @$hospital_header[0]->drug_licence_no ? $hospital_header[0]->drug_licence_no : '' ?>
                                </th>
                                <th colspan="4" class="common_td_rules" width="30%">
                                    PAN No. <?= @$hospital_header[0]->pan_no ? $hospital_header[0]->pan_no : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="4" class="common_td_rules" width="40%">
                                    <b>Seller : </b>
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </td>
                                <td colspan="9" class="common_td_rules" width="60%" colspan="2">
                                    <b>Buyer : </b>
                                    <?= @$hospital_header[0]->address ? $hospital_header[0]->address : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="10" class="common_td_rules" width="30%">
                                    <b>GRN Status : </b>
                                    <?= $po_status ?>
                                </th>
                                <th colspan="11" class="common_td_rules" width="40%">
                                    <b>Approved By : </b>
                                    <?= @$users[0]->approved_by ? $users[0]->approved_by : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="4" class="common_td_rules" width="30%">
                                    <b>Prepared By : </b>
                                    <?= @$users[0]->created_by ? $users[0]->created_by : '' ?>
                                </td>
                                <td colspan="4" class="common_td_rules" width="30%">
                                    <b>Printed By : </b>
                                    <?= @$users[0]->printed_by ? $users[0]->printed_by : '' ?>
                                </td>
                                <td colspan="5" class="common_td_rules" width="40%">
                                    <b>Print Date Time : </b>
                                    <?= date('M-d-Y H:i:s') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
