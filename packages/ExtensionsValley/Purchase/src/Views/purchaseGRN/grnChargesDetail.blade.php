<div class="col-md-12 padding_sm" style="margin-top: 10px;min-height: 500px;">
    <input type="hidden" id="item_descpopup" value="{{ $item_desc }}">
    <input type="hidden" id="item_codepopup" value="{{ $item_code }}">
    <input type="hidden" id="row_idpopup" value="{{ $row_id }}">
    <input type="hidden" id="newBatchItem{{ $row_id }}" value="0">
    <ul id="popupBatchliid" class="nav nav-tabs sm_nav">
        <li class="active">
            <a data-toggle="tab" href="#batchli{{ $row_id }}"
                style="border-radius: 0px;cursor: pointer;padding: 6px;" aria-expanded="false"><span
                    id="batchname{{ $row_id }}">Batch</span>
            </a>
        </li>
        <button style="padding: 8px 8px;" class="btn btn-success" onclick="addNewItemDetails({{ $row_id }})"
            id="addNewBatchBtn"> <i id="addNewBatchSpin" class="fa fa-plus"></i>
        </button>
    </ul>

    </span>
    <div class="tab-content" id="popupBatchtab">
        <div id="batchli{{ $row_id }}" class="tab-pane active">
            <div class="row padding_sm" id="grnBillItemDetails{{ $row_id }}">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $is_checked = '';
                        $is_readonly = '';
                        $pois_checked = '';
                        $po_total_qty = 0;
                        $po_unit_rate = 0;
                        $div_size = 'col-md-6';

                        if (!empty($item_arr[$row_id]['is_free']) && $item_arr[$row_id]['is_free'] == 1) {
                            $is_checked = 'checked';
                            $is_readonly = 'readonly';
                        }
                        if (count($poDetailArr) != 0) {
                            if (!empty($poDetailArr[0]->is_free) && $poDetailArr[0]->is_free == 1) {
                                $pois_checked = 'checked';
                            }

                            $poqty = @$poDetailArr[0]->pur_qnty ? $poDetailArr[0]->pur_qnty : 0;
                            $pofreeqty = @$poDetailArr[0]->free_qty ? $poDetailArr[0]->free_qty : 0;
                            $uomval = @$poDetailArr[0]->conv_factor ? $poDetailArr[0]->conv_factor : 0;
                            $pur_rate = @$poDetailArr[0]->pur_rate ? $poDetailArr[0]->pur_rate : 0;
                            $po_total_qty = (floatval($poqty) + floatval($pofreeqty)) * $uomval;

                            if (floatval($uomval) != 0) {
                                $po_unit_rate = $pur_rate / $uomval;
                            }
                            $div_size = 'col-md-3';
                        }

                        ?>

                        <input type="hidden" class="calculationSelectRowID" value="{{ $row_id }}">
                        <input type="hidden" id="popupItemid{{ $row_id }}" value="{{ $item_id }}">
                        <input type="hidden" id="popPODetailID{{ $row_id }}" value="{{ $PODetailID }}">
                        <input type="hidden" id="batch_needed{{ $row_id }}" value="{{ $batch_needed }}">
                        <input id="stock_data" type="hidden" value="{{ $stock_data }}">
                        <table class="table no-border table_sm">
                            <tbody>
                                <tr>
                                    <td width="25%">
                                        <label for="" style="font-size: 12px;font-weight: 700">Batch
                                            No</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" blockSpecialChar(event);
                                            id='batch_name_model{{ $row_id }}' class="form-control validation"
                                            onchange="checkBtachExpiry({{ $row_id }})"
                                            value="{{ @$item_arr[$row_id]['batch_no'] ? $item_arr[$row_id]['batch_no'] : '' }}">
                                    </td>
                                    <td width="25%">
                                        <?php
                                            $expiry_date=  @$item_arr[$row_id]['expiry_date'] ? $item_arr[$row_id]['expiry_date'] : '';

                                        if ($expiry_date_format == 'DD-MM-YYYY') {
                                             $expiry_date=  $expiry_date ? date('d-m-Y',strtotime($expiry_date)) : '';
                                        } else if ($expiry_date_format == 'MM-YYYY') {
                                             $expiry_date=  $expiry_date ? date('m-Y',strtotime($expiry_date)) : '';
                                        } else if ($expiry_date_format == 'YYYY') {
                                            $expiry_date=  $expiry_date ? date('Y',strtotime($expiry_date)) : '';
                                        }
                                       ?>
                                        <label for="" style="font-size: 12px;font-weight: 700">Expiry</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" name="Expiry_pop[]"
                                            id='expiry_date_model{{ $row_id }}' value="{{ $expiry_date }}"
                                            class="form-control validation expiry_date_model" placeholder="{{ $expiry_date_format }}"
                                            onchange="checkBtachExpiry({{ $row_id }})">
                                    </td>
                                    <td width="50%">
                                        <label for="" style="font-size: 12px;font-weight: 700">HSN
                                            Code</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" name="hsn_code[]"
                                            class="form-control validation" id='hsn_code_model{{ $row_id }}'
                                            value="{{ @$item_arr[$row_id]['hsn_code'] ? $item_arr[$row_id]['hsn_code'] : '' }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12 padding_sm">
                        @if ($div_size == 'col-md-3')
                            <div class="col-md-3 padding_sm">
                                <table class="table no-border table-striped table_sm">
                                    <tbody>
                                        <tr class="table_header_bg">
                                            <th style="text-align: center" colspan="2">PO Details</th>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>Free Item</b></label></td>
                                            <td><input disabled type="checkbox" {{ $pois_checked }} name="free_item">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>PO Qty</b></label></td>
                                            <td>
                                                <label>{{ @$poDetailArr[0]->pur_qnty ? number_format($poDetailArr[0]->pur_qnty, 2, '.', '') : 0 }}</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>PO Free Qty</b></label></td>
                                            <td> <label>{{ @$poDetailArr[0]->free_qty ? number_format($poDetailArr[0]->free_qty, 2, '.', '') : 0 }}</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>UOM</b></label></td>
                                            <td> <label>{{ @$poDetailArr[0]->uom_name ? $poDetailArr[0]->uom_name : '' }}</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>UOM Value</b></label></td>
                                            <td> <label>{{ @$poDetailArr[0]->conv_factor ? $poDetailArr[0]->conv_factor : 0 }}</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>PO Total Qty</b></label></td>
                                            <td> <label>{{ $po_total_qty ? number_format($po_total_qty, 2, '.', '') : 0 }}</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>Rate(Per UOM)</b></label></td>
                                            <td> <label>{{ @$poDetailArr[0]->pur_rate ? number_format($poDetailArr[0]->pur_rate, 2, '.', '') : 0 }}</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for=""><b>Unit Rate</b></label></td>
                                            <td> <label>{{ $po_unit_rate ? number_format($po_unit_rate, 2, '.', '') : 0 }}</label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label for=""><b>MRP(Per UOM)</b></label></td>
                                            <td> <label>{{ @$poDetailArr[0]->mrp ? number_format($poDetailArr[0]->mrp, 2, '.', '') : 0 }}</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endif
                        <div class="{{ $div_size }} padding_sm">
                            <table class="table no-border table-striped table_sm">
                                <tbody>
                                    <tr class="table_header_bg">
                                        <th style="text-align: center" colspan="2">GRN Details</th>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>Free Item</b></label></td>
                                        <td><input type="checkbox" {{ $is_checked }}
                                                onchange="itemIsFree({{ $row_id }},{{ $item_id }});"
                                                id="free_item_id{{ $row_id }}" name="free_item">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>GRN Qty</b></label></td>
                                        <td>
                                            <input type="text" autocomplete="off"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                value="{{ @$item_arr[$row_id]['grn_qty'] ? $item_arr[$row_id]['grn_qty'] : 0 }}"
                                                id="grn_qty{{ $row_id }}" name="grn_qty"
                                                class="form-control number_class1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>GRN Free Qty</b></label></td>
                                        <td><input type="text" autocomplete="off"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                value="{{ @$item_arr[$row_id]['free_qty'] ? $item_arr[$row_id]['free_qty'] : 0 }}"
                                                id="free_qty{{ $row_id }}" name="free_qty"
                                                class="form-control number_class1"></td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>UOM</b></label></td>
                                        <td>
                                            <select name="uom_select_pop[]"
                                                id="uom_select_pop_id_{{ $row_id }}"
                                                onchange="fetchUOMValue({{ $row_id }},{{ $item_id }});"
                                                class="form-control">
                                                @php $pur_conv_factor=0; @endphp
                                                @foreach ($pur_list as $pur_each)
                                                    @php $pur_conv_factor=$pur_each->conv_factor; @endphp
                                                    <option value='{{ $pur_each->id }}'
                                                        @if (!empty($item_arr[$row_id]['item_unit']) && $item_arr[$row_id]['item_unit'] == $pur_each->id) selected @endif
                                                        data-uom_value='{{ $pur_each->conv_factor }}'>
                                                        {{ $pur_each->uom_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>UOM Value</b></label></td>
                                        <td><input type="text" autocomplete="off" readonly
                                                value="{{ @$item_arr[$row_id]['uom_val'] ? $item_arr[$row_id]['uom_val'] : $pur_conv_factor }}"
                                                id="uom_val_model{{ $row_id }}" name="uom_val"
                                                class="form-control number_class1"></td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>GRN Total Qty</b></label></td>
                                        <td><input type="text" autocomplete="off" readonly
                                                value="{{ @$item_arr[$row_id]['all_total_qty'] ? $item_arr[$row_id]['all_total_qty'] : 0 }}"
                                                id="all_total_qty_model{{ $row_id }}" name="all_total_qty"
                                                class="form-control number_class1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>Rate(Per UOM)</b></label></td>
                                        <td><input type="text" autocomplete="off"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                value="{{ @$item_arr[$row_id]['item_rate'] ? $item_arr[$row_id]['item_rate'] : 0 }}"
                                                id="rate{{ $row_id }}" {{ $is_readonly }} name="rate"
                                                class="form-control validation number_class1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>Unit Rate</b></label></td>
                                        <td><input type="text" autocomplete="off"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                value="{{ @$item_arr[$row_id]['unit_rate'] ? $item_arr[$row_id]['unit_rate'] : 0 }}"
                                                id="unit_rate{{ $row_id }}" readonly name="unit_rate"
                                                class="form-control number_class1"></td>
                                    </tr>

                                    <tr>
                                        <td><label for=""><b>MRP(Per UOM)</b></label></td>
                                        <td><input type="text" autocomplete="off"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                value="{{ @$item_arr[$row_id]['grn_mrp'] ? $item_arr[$row_id]['grn_mrp'] : 0 }}"
                                                id="mrp{{ $row_id }}" name="mrp"
                                                class="form-control validation number_class1"></td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>Unit MRP</b></label></td>
                                        <td><input type="text" autocomplete="off"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                value="{{ @$item_arr[$row_id]['unit_mrp'] ? $item_arr[$row_id]['unit_mrp'] : 0 }}"
                                                id="unit_mrp{{ $row_id }}" readonly name="unit_mrp"
                                                class="form-control number_class1"></td>
                                    </tr>
                                    <tr>
                                        <?php
                                        $checked = '';
                                        $readonly = 'readonly';
                                        if (!empty($item_arr[$row_id]['sales_rate_mrp_check']) && $item_arr[$row_id]['sales_rate_mrp_check'] == 1) {
                                            $readonly = '';
                                            $checked = 'checked';
                                        } ?>
                                        <td><label for=""><b>Edit Selling Price</b></label></td>
                                        <td><input type="checkbox" id="unit_check_box{{ $row_id }}"
                                                {{ $checked }}
                                                onclick="editSellingPriceCheck({{ $row_id }},{{ $item_id }})"
                                                value="1" name="unit_check_box"></td>
                                    </tr>
                                    <tr>
                                        <td><label for=""><b>Selling Price</b></label></td>
                                        <td>
                                            <input type="text" autocomplete="off"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                onchange="calculateAmount({{ $row_id }},{{ $item_id }})"
                                                value="{{ @$item_arr[$row_id]['selling_price'] ? $item_arr[$row_id]['selling_price'] : 0 }}"
                                                id="selling_price{{ $row_id }}" {{ $readonly }}
                                                name="selling_price" class="form-control number_class1">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <input type="hidden" id="dataentryType{{ $row_id }}">

                        <div class="col-md-6 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel ">GST</label>
                                    <select onchange="changeChargeType(1,{{ $row_id }})"
                                        id="gst_tax_type{{ $row_id }}" name="gst_tax_type"
                                        class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($tax_head as $each)
                                            <option attt-id="{{ $each->tax_division }}"
                                                value="{{ $each->tax_name }}">
                                                {{ $each->tax_val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel ">IGST</label>
                                    <select onchange="changeChargeType(2,{{ $row_id }})"
                                        id="igst_tax_type{{ $row_id }}" name="igst_tax_type"
                                        class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($tax_head as $each)
                                            <option attt-id="{{ $each->tax_division }}"
                                                value="{{ $each->tax_name }}">
                                                {{ $each->tax_val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <table class="table no-margin table_sm no-border" style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th class="common_td_rules" width="30%">Description</th>
                                        <th class="common_td_rules" width="20%">Type</th>
                                        <th class="common_td_rules" width="25%">Percentage</th>
                                        <th class="common_td_rules" width="25%">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                            foreach ($charges_head as $charge) {
                                                ?>
                                    <tr>
                                        <td class="common_td_rules">{{ $charge->name }}</td>
                                        <?php
                                        $val_per = 0;
                                        $class = '';
                                        $val_amt = 0;
                                        $read_only = '';
                                        $onblur_data = '';
                                        $charge_typevalue1 = '';
                                        $charge_typevalue2 = '';
                                        if ($charge->calc_code == 'GA') {
                                            $class = 'active_class' . $row_id;
                                            $onblur_data = "onchange='calculateAmount($row_id,$item_id,$charge->id,1)'";
                                        } elseif ($charge->calc_code == 'OTHCH') {
                                            $read_only = 'readonly';
                                            $class = '';
                                        } elseif ($charge->calc_code == 'DA') {
                                            $read_only = 'readonly';
                                            $onblur_data = "onchange='calculateAmount($row_id,$item_id,$charge->id,1)'";
                                            $class = 'active_class' . $row_id;
                                        } else {
                                            $onblur_data = "onchange='calculateAmount($row_id,$item_id,$charge->id,1)'";
                                            $class = 'active_class' . $row_id;
                                        }
                                        if ($charge->status_code == 'CGST') {
                                            $val_per = @$item_arr[$row_id]['cgst_per'] ? $item_arr[$row_id]['cgst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['cgst_amt'] ? $item_arr[$row_id]['cgst_amt'] : 0;
                                        } elseif ($charge->status_code == 'SGST') {
                                            $val_per = @$item_arr[$row_id]['sgst_per'] ? $item_arr[$row_id]['sgst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['sgst_amt'] ? $item_arr[$row_id]['sgst_amt'] : 0;
                                        } elseif ($charge->status_code == 'IGST') {
                                            $val_per = @$item_arr[$row_id]['igst_per'] ? $item_arr[$row_id]['igst_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['igst_amt'] ? $item_arr[$row_id]['igst_amt'] : 0;
                                        } elseif ($charge->status_code == 'CESS') {
                                            $val_per = @$item_arr[$row_id]['cess_per'] ? $item_arr[$row_id]['cess_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['cess_amt'] ? $item_arr[$row_id]['cess_amt'] : 0;
                                        } elseif ($charge->status_code == 'OTHCHG') {
                                            $val_per = @$item_arr[$row_id]['othercharge_per'] ? $item_arr[$row_id]['othercharge_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['othercharge_amt'] ? $item_arr[$row_id]['othercharge_amt'] : 0;
                                        } elseif ($charge->status_code == 'FRCHG') {
                                            $val_per = @$item_arr[$row_id]['flightcharge_per'] ? $item_arr[$row_id]['flightcharge_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['flightcharge_amt'] ? $item_arr[$row_id]['flightcharge_amt'] : 0;
                                        } elseif ($charge->status_code == 'ITEMDISC') {
                                            $val_per = @$item_arr[$row_id]['discount_per'] ? $item_arr[$row_id]['discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['discount_amt'] ? $item_arr[$row_id]['discount_amt'] : 0;
                                            $charge_typevalue1 = @$item_arr[$row_id]['discounttype1'] ? $item_arr[$row_id]['discounttype1'] : (@$charge_type[$item_id][$charge->id] ? $charge_type[$item_id][$charge->id] : 1);
                                            if (intval($charge_typevalue1) == 2) {
                                                $read_only = 'readonly';
                                            }
                                        } elseif ($charge->status_code == 'SCHEMADISC') {
                                            $val_per = @$item_arr[$row_id]['special_discount_per'] ? $item_arr[$row_id]['special_discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                            $val_amt = @$item_arr[$row_id]['special_discount_amt'] ? $item_arr[$row_id]['special_discount_amt'] : 0;
                                            $charge_typevalue2 = @$item_arr[$row_id]['discounttype2'] ? $item_arr[$row_id]['discounttype2'] : (@$charge_type[$item_id][$charge->id] ? $charge_type[$item_id][$charge->id] : 1);
                                            if (intval($charge_typevalue2) == 2) {
                                                $read_only = 'readonly';
                                            }
                                        }
                                        ?>
                                        @if ($charge->calc_code == 'GA')
                                            @if ($charge->status_code == 'ITEMDISC')
                                                <td>
                                                    <select
                                                        id="popdiscount_type{{ $charge->id }}{{ $row_id }}"
                                                        onchange="changeItemAmountType({{ $charge->id }},{{ $row_id }})"
                                                        class="form-control">
                                                        @foreach ($charge_data as $key => $each)
                                                            @php
                                                                $selected = '';
                                                            @endphp
                                                            @if ($key == $charge_typevalue1)
                                                                @php
                                                                    $selected = 'selected';
                                                                @endphp
                                                            @endif
                                                            <option {{ $selected }} value="{{ $key }}">
                                                                {{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            @elseif ($charge->status_code == 'SCHEMADISC')
                                                <td>
                                                    <select
                                                        id="popdiscount_type{{ $charge->id }}{{ $row_id }}"
                                                        onchange="changeItemAmountType({{ $charge->id }},{{ $row_id }})"
                                                        class="form-control">
                                                        @foreach ($charge_data as $key => $each)
                                                            @php
                                                                $selected = '';
                                                            @endphp
                                                            @if ($key == $charge_typevalue2)
                                                                @php
                                                                    $selected = 'selected';
                                                                @endphp
                                                            @endif
                                                            <option {{ $selected }}
                                                                value="{{ $key }}">
                                                                {{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            @endif
                                        @else
                                            <td style="text-align: center">--</td>
                                        @endif
                                        <td>
                                            <input <?= $read_only ?> <?= $onblur_data ?>
                                                data-code="{{ $charge->calc_code }}"
                                                data-id="{{ $charge->status_code }}"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                type="text" autocomplete="off"
                                                class="<?= $class ?> percentage_class{{ $row_id }} form-control td_common_numeric_rules check_selling_price tax_class"
                                                id="charge_percentage{{ $charge->id }}{{ $row_id }}"
                                                name="taxpercentage" value="<?= $val_per ?>"
                                                placeholder="{{ $charge->name }}">
                                        </td>
                                        <?php
                                        $read_only = '';
                                        $onblur_data = '';
                                        if ($charge->calc_code == 'GA') {
                                            $class = 'active_class' . $row_id;
                                            $onblur_data = "onchange='calculateAmount($row_id,$item_id,$charge->id,2)'";
                                            if ($charge->status_code == 'ITEMDISC') {
                                                $val_per = @$item_arr[$row_id]['discount_per'] ? $item_arr[$row_id]['discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                                $val_amt = @$item_arr[$row_id]['discount_amt'] ? $item_arr[$row_id]['discount_amt'] : 0;
                                                $charge_typevalue1 = @$item_arr[$row_id]['discounttype1'] ? $item_arr[$row_id]['discounttype1'] : (@$charge_type[$item_id][$charge->id] ? $charge_type[$item_id][$charge->id] : 1);
                                                if (intval($charge_typevalue1) == 1) {
                                                    $read_only = 'readonly';
                                                }
                                            } elseif ($charge->status_code == 'SCHEMADISC') {
                                                $val_per = @$item_arr[$row_id]['special_discount_per'] ? $item_arr[$row_id]['special_discount_per'] : (@$item_last_details[$charge->id] ? $item_last_details[$charge->id] : 0);
                                                $val_amt = @$item_arr[$row_id]['special_discount_amt'] ? $item_arr[$row_id]['special_discount_amt'] : 0;
                                                $charge_typevalue2 = @$item_arr[$row_id]['discounttype2'] ? $item_arr[$row_id]['discounttype2'] : (@$charge_type[$item_id][$charge->id] ? $charge_type[$item_id][$charge->id] : 1);
                                                if (intval($charge_typevalue2) == 1) {
                                                    $read_only = 'readonly';
                                                }
                                            }
                                        } elseif ($charge->calc_code != 'OTHCH') {
                                            $read_only = 'readonly';
                                            $class = '';
                                        } else {
                                            $onblur_data = "onchange='calculateAmount($row_id,$item_id,$charge->id,2)'";
                                            $class = 'active_class' . $row_id;
                                        }
                                        ?>
                                        <td>
                                            <input <?= $read_only ?> <?= $onblur_data ?>
                                                data-id="{{ $charge->status_code }}"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                type="text" autocomplete="off"
                                                data-code="{{ $charge->calc_code }}"
                                                class="<?= $class ?> amount_class{{ $row_id }} form-control td_common_numeric_rules check_selling_price nummeric_class tax_class"
                                                id="charge_amount{{ $charge->id }}{{ $row_id }}"
                                                name="taxamount" value="<?= $val_amt ?>"
                                                placeholder="{{ $charge->name }}">
                                        </td>
                                    </tr>
                                    <?php
                                            }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-4 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel ">Total Rate</label>
                                <input type="text" autocomplete="off"
                                    style="color: red;font-weight:700;font-size:14px !important;"
                                    class="form-control active_class{{ $row_id }}" readonly
                                    id='tot_rate_cal{{ $row_id }}'>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel ">Unit Cost</label>
                                <input type="text" autocomplete="off"
                                    onchange="calculateAmount(this,'{{ $row_id }}')"
                                    style="color: red;font-weight:700;font-size:14px !important;"
                                    value="{{ @$item_arr[$row_id]['unit_cost'] ? $item_arr[$row_id]['unit_cost'] : 0 }}"
                                    id="unit_cost{{ $row_id }}" readonly name="unit_cost"
                                    class="form-control active_class{{ $row_id }}">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel ">Net Rate</label>
                                <input type="text" autocomplete="off"
                                    style="color: red;font-weight:700;font-size:14px !important;"
                                    class="form-control active_class{{ $row_id }}" readonly
                                    id="charges_tot_pop{{ $row_id }}">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
