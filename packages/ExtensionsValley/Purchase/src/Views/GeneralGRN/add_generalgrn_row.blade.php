<?php $row_count = ++$row_count; ?>
<tr style="background: #FFF;" onclick="item_history({{ $row_count }})" class="row_class" id="row_data_{{ $row_count }}">
    <td class='row_count_class'>{{ $row_count }}</td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc_{{ $row_count }}"
            onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
            class="form-control popinput" name="item_desc" placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
        <input type='hidden' name='row_id_hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' name='item_code_hidden' value="" id="item_code_hidden{{ $row_count }}">
        <input type='hidden' name='item_id_hidden' value="" id="item_id_hidden{{ $row_count }}">
        <input type='hidden' name='po_detail_id_hidden' value="0" id="po_detail_id_hidden{{ $row_count }}">
        <input type='hidden' name='bill_detail_id_hidden' value="0" id="bill_detail_id_hidden{{ $row_count }}">
    </td>
    <td><input name='batch' id="batch_row{{ $row_count }}" onchange="calculateListAmount({{ $row_count }},1)"
            class="form-control" type="text" value=""></td>
    <td><input name='expiry_date' onblur="calculateListAmount({{ $row_count }},1)" id="expiry_date{{ $row_count }}"
            id="expiry_date{{ $row_count }}" class="form-control expiry_date" type="text" value=""></td>
    <td style="text-align: center">
        <div style="margin-top: 3px !important" class="checkbox checkbox-success inline no-margin">
            <input onclick="item_isfree({{ $row_count }})" title="Is Free" type="checkbox"
                id="is_freecheck{{ $row_count }}" value="1">
            <label for="is_freecheck{{ $row_count }}"></label>
        </div>
    </td>

    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='grn_qty'
            id="grn_qty{{ $row_count }}" type="text" value="0.00">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='free_qty'
            id="free_qty{{ $row_count }}" type="text" value="0.00">
    </td>
    <td title="Unit converion">
        <select onchange="getUomConvensionFactor({{ $row_count }})" name="uom_select"
            id="uom_select_id_{{ $row_count }}" class="form-control">

        </select>
    </td>
    <td><input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='uom_vlaue'
            id="uom_vlaue{{ $row_count }}" type="text" value="0.00">
    </td>
    <td><input class="form-control number_class input_readonly" readonly id="tot_qty_{{ $row_count }}" name="tot_qty"
            type="text" value="0.00">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="unit_rate{{ $row_count }}" name="unit_rate" type="text" value="0">
    </td>
    <td title="Discount Type">
        <select onchange="calculateListAmount({{ $row_count }},1)" name="discount_type"
            id="discount_type{{ $row_count }}" class="form-control">
            <option value="1">Percentage</option>
            <option value="2">Amount</option>
        </select>
    </td>

    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_dic_amt_{{ $row_count }}" name="tot_dic_amt" type="text" value="0.00">
    </td>
    <td title="Tax Type">
        <select onchange="calculateListAmount({{ $row_count }},1)" name="tax_type" id="tax_type{{ $row_count }}"
            class="form-control">
            <option value="">Select</option>
            @foreach ($tax_head as $each)
            <option attt-id="{{ $each->tax_division }}" value="{{ $each->tax_name }}">
                {{ $each->tax_val }}</option>
            @endforeach
        </select>
    </td>
    <td><input class="form-control number_class sgst_tax input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="sgst_tax{{ $row_count }}" name="sgst_tax" type="text" value="0.00">
    </td>
    <td><input class="form-control number_class cgst_tax input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="cgst_tax{{ $row_count }}" name="cgst_tax" type="text" value="0.00">
    </td>
    <td><input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_tax_amt_{{ $row_count }}" name="tot_tax_amt" type="text" value="0.00">
    </td>

    <td><input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="net_rate{{ $row_count }}" name="net_cost" type="text" value="0.00"></td>
    <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i></td>
</tr>
