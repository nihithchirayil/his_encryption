@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="col-md-12 padding_sm" style="text-align: right">
            <strong>{{ $title }}</strong>
            <div class="clearfix"></div>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text"  autocomplete="off" class="form-control datepicker" name="from_date"
                                        id="from_date" value="{{ $searchFields['from_date'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control datepicker" name="to_date"
                                        id="to_date" value="{{ $searchFields['to_date'] ?? '' }}">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Return No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="return_no">
                                    <div id="return_no_AjaxDiv" class="ajaxSearchBox" style="margin-top: 13px"></div>
                                    <input type="hidden" id="return_no_hidden">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Return From</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('from_location', ['All' => 'Select'] + $location->toArray(), $searchFields['from_location'] ?? $default_location, [
    'class' => 'form-control select2',
    'id' => 'from_location',
    'onchange' => 'searchReturnList()',
]) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Return To</label>
                                    <div class="clearfix"></div>
                                    <?php $dfltstore = \WebConf::getConfig('default_return_store'); ?>
                                    {!! Form::select('to_location', ['All' => 'Select'] + $to_location->toArray(), $searchFields['to_location'] ?? $dfltstore, [
    'class' => 'form-control select2',
    'id' => 'to_location',
    'onchange' => 'searchReturnList()',
]) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Select Status</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('status', ['All' => ' Select', '1' => ' Worksheet', '2' => ' Submitted', '3' => ' Approved', '4' => ' Received', '10' => 'Partially Received'], $searchFields['status'] ?? null, [
    'class' => 'form-control status select2',
    'id' => 'status',
    'onchange' => 'searchReturnList()',
]) !!}
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searchReturnList()" id="stock_return_receive_list_btn"
                                    class="btn btn-block btn-primary"><i id="stock_return_receive_list_spin"
                                        class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="resetFilter()" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                    Clear</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div id="stock_return_receive_list" class="col-md-12 padding_sm"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/receiveStockReturnList.js') }}">
    </script>
@endsection
