@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

@stop

@section('css_extra')
    <style>
        .close_btn_vendor_search {

            position: absolute;
            z-index: 99;
            color: #FFF !important;
            background: #000;
            right: -11px;
            top: -1px;
            border-radius: 100%;
            text-align: center;
            width: 20px;
            height: 20px;
            line-height: 20px;
            cursor: pointer;
        }

        .vendor-list-div {
            position: absolute;
            z-index: 99;
            background: #FFF;
            box-shadow: 0 0 6px #ccc;
            padding: 10px;
            /* width: 95%; */
            width: 300px;
        }

        #VendorTable>tbody>tr:hover {
            background: #87d7ca52;
        }

        #VendorTable>tbody>tr>td {
            padding: 8px;
            line-height: 2;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .ajaxSearchBox {
            display: block;
            z-index: 7000;
            margin-top: 15px !important;
        }
    </style>
@endsection
@section('content-area')
    <!-- page content -->

    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <input type="hidden" name="base_url" id="base_url" value="{{ url('/') }}" />
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        <input type="hidden" id="default_location" value="{{ $default_location }}">
        <div class="row codfox_container">
            <div class="col-md-9 padding_sm" style="margin-top: 10px;">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Request No</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="RequestNo_search">
                                <div id="RequestNo_search_AjaxDiv" style="margin-left:-3px" class="ajaxSearchBox"></div>
                                <input type="hidden" id="Requestid_hidden">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="from_date" id="from_date"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="to_date" id="to_date"
                                    value="">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Department</label>
                                <div class="clearfix"></div>
                                {!! Form::select(
                                    'from_location',
                                    ['All' => 'Department'] + $location->toArray(),
                                    $searchFields['from_location'] ?? $default_location,
                                    [
                                        'class' => 'form-control from_location select2',
                                        'id' => 'from_location',
                                    ],
                                ) !!}
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Store</label>
                                <div class="clearfix"></div>
                                <?php
                                $user_id = !empty(\Auth::user()->id) ? \Auth::user()->id : 0;
                                $group_id = \DB::table('user_group')
                                    ->where('user_id', '=', $user_id)
                                    ->pluck('group_id');
                                $group_name = \DB::table('groups')
                                    ->wherein('id', $group_id)
                                    ->pluck('name')
                                    ->toArray();

                                ?>
                                <select class="form-control to_location select2" id="to_location" name="to_location">
                                    <?php
                                 foreach ($to_location as $key=>$val) {
                                    $seleted='';
                                     if($key==$default_store){
                                         $seleted="selected=''";
                                     }
                                    ?>
                                    <option <?= $seleted ?> value="<?= $key ?>"><?= $val ?></option>
                                    <?php
                                 }
                                 ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Select Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select(
                                    'status',
                                    [
                                        'All' => ' Select Status',
                                        '1' => ' Worksheet',
                                        '2' => ' Submitted',
                                        '3' => ' Verified',
                                        '4' => ' Approved',
                                        '5' => ' Partially Issued',
                                        '6' => ' Partially Received',
                                        '7' => ' Received',
                                        '8' => ' Closed',
                                        '9' => ' Cancelled',
                                        '10' => ' Issued',
                                        '12' => ' Rejected',
                                    ],
                                    $searchFields['status'] ?? null,
                                    [
                                        'class' => 'form-control status select2',
                                        'id' => 'status',
                                    ],
                                ) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1 padding_sm" style="margin-top: 10px;">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                        <div class="col-md-12 padding_sm"> <strong>Total Count </strong></div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" style="margin-top: 5px"><strong><span
                                    id="total_cntspan">0</span> </strong>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-2 padding_sm" style="margin-top: 10px;">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                        <div class="col-md-4 padding_sm" style="margin-top:-10px">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button onclick="searchRequisitionData()" id="searchRequisitionBtn"
                                class="btn btn-block light_purple_bg"><i id="searchRequisitionSpin"
                                    class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top:-10px">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button onclick="resetFilter()" class="btn btn-block btn-warning"><i class="fa fa-recycle"></i>
                                Clear</button>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top:-10px">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <div onclick="addStockRequestNew();" class="btn btn-block btn-primary"><i
                                    class="fa fa-plus"></i>
                                Add</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-footer" id="searchRequisitionDataDiv"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 550px;">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.js')}}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/Purchase/default/javascript/stockRequisition.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        @include('Purchase::messagetemplate')
    </script>
@stop
