<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            @page { margin: 0; }
            body { margin: 15px; }
            thead {display: table-header-group;}
            @media print{
                table thead {display: table-header-group;}
            }
        </style>
    </head>

    <body>
    
        <section class="content" style="margin-top:15px;">
            <div >
                <div style="width: 100%; overflow-x: auto;">

                    @if(isset($result) && count($result) > 0)
                    <table   style="width: 100%; font-size: 14px;">
                        <tr>
                                <td colspan="10" align="center"><b style="font-size:15px">{!!$header!!}</b></td>
                            </tr>
                    </table>
                    <table   style="width: 100%; font-size: 14px;">
                        <thead>
                            <tr>
                                <td colspan="10" align="center"><strong style="font-size:20px">Stock Return</strong></td>
                            </tr>   
                            <tr>
                                <td colspan="3" style="border: none">Return From</td>
                                <td colspan="2" style="border: none">:{{$result[0]->from_location}}</td>
                                <td colspan="4" style="border: none">Doc.No </td>
                                <td colspan="2" style="border: none">:{{$result[0]->return_no}}</td>
                            </tr>
                            <tr >
                                <td colspan="3" style="border: none">Return To</td>
                                <td colspan="2" style="border: none">:{{$result[0]->to_location}}</td>
                                <td colspan="4" style="border: none">Status</td>
                                <td colspan="2" style="border: none">:{{$result[0]->status}}</td>
                            </tr>
                            <tr >
                                <td colspan="3" style="border: none">Manual Doc No</td>
                                <td colspan="2" style="border: none">:</td>
                                <td colspan="4" style="border: none">Issued Date</td>
                                <td colspan="2" style="border: none">:{{$result[0]->issued_date}}</td>
                            </tr>
                            <tr >
                                <td colspan="3" style="border: none">Printed Date</td>
                                <td colspan="2" style="border: none">:{{date("d-M-Y H:i:s")}}</td>
                                <td colspan="5" style="border: none"></td>
                            </tr>
                            <tr >
                                <td colspan="3" style="border: none">Issued By</td>
                                <td colspan="2" style="border: none">:{{$result[0]->issued_by}}</td>
                                <td colspan="5" style="border: none"></td>
                            </tr>
                        </thead>
                    </table>
                    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%; font-size: 12px; border-collapse: collapse; border: 1px solid #000; margin: 15px auto auto auto ;">

                        <tr colspan="10">
                            <th>SI.No</th>
                            <th>Item Name</th>
                            <th>Batch</th>
                            <th>Expiry Date</th>
                            <th>unit</th>
                            <th>Issued Qty</th>
                        </tr>
                        </thead>
                        <tbody >
                            <?php $i = 1;
                            $print_total = 0; ?>
                            @foreach($result as $p_val)
                            <tr>
                                <td>{{$i}}</td>
                                <td >{{$p_val->item_desc}}</td>
                                <td>{{$p_val->batch_no}}</td>
                                <td>{{$p_val->expiry}}</td>
                                <td>{{$p_val->uom_name}}</td>
                                <td >{{$p_val->issued_quantity}}</td>
                            </tr>
                            <?php
                            $i++;
                            ?>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- FOOTER SECTION-->
                    <div style="margin-top: 20px">
                        <table width="100%" style="border-collapse: collapse;" border="0">
                            <tbody>
                                <tr>
                                    <td>Remarks :</td> 
                                </tr>
                              
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>

        </section>

        <script type="text/javascript">
             window.onload = window.print();
            // window.onload = window.close();
        </script>
    </body>
</html>