@extends('Dashboard::dashboard.dashboard')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}

@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" >
    <div class="row" style="text-align: right; font-size: 12px;padding-right: 66px; font-weight: bold;"> {{$title}}
        <div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.purchase.listStockConsumption')}}" id="requestSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Transaction No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete='off' class="form-control" name="transcation_no" id="transcation_no"
                                           value="{{ $searchFields['transcation_no'] ?? '' }}">
                                </div>
                                <div id="transcation_no_searchCodeAjaxDiv" class="ajaxSearchBox" ></div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">User Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete='off' class="form-control" name="user_name" id="user_name"
                                           value="{{ $searchFields['user_name'] ?? '' }}">
                                    <input type="hidden" autocomplete='off' class="form-control" name="user_name_hidden" id="user_name_hidden"
                                           value="{{ $searchFields['user_name_hidden'] ?? '' }}">
                                </div>
                                <div id="user_name_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Department</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('from_location',$location,$searchFields['from_location']?$searchFields['from_location']:$default_location,['class' => 'form-control select2 abc','placeholder' => 'Department','title' => 'Department','id' => 'from_location','style' => 'color:#555555; padding:2px 12px;']) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Select Status</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('status', array(""=> " Select Status","2"=> " Approved","1"=> " Not Approved") , $searchFields['status'] ?? null , [
                                    'class'       => 'form-control status',
                                    'id'    => 'status'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning">
                                    <i class="fa fa-times"></i>  Clear</a>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="addStockRequestNew();" class="btn btn-block btn-primary" ><i class="fa fa-plus" ></i> Add</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Transaction No</th>
                                    <th>Patient Name</th>
                                    <th>Location</th>
                                    <th>User</th>
                                    <th>Last Updated</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($resultData) && count($resultData) > 0)
                                @foreach ($resultData as $res)
                                <tr style="cursor: pointer;" onclick="stockItemEditLoadData(this,'{{$res->head_id}}')" >
                                    <td class="common_td_rules">{{$res->transaction_no}}</td>
                                    <td class="common_td_rules">{{ $res->patient_name }}</td>
                                    <td class="common_td_rules">{{ $res->location_name }}</td>
                                    <td class="common_td_rules">{{ $res->user_name }}</td>
                                    <td class="common_td_rules">{{ $res->last_updated }}</td>
                                    <td class="common_td_rules"> {{ $res->dep_show_status }}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="12" class="location_code">No Records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="row">
                        <div style="padding:10px 30px;">
                            <div class="col-md-12" style="text-align:right;">
                            <?php echo $paginator->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
        </div>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">
                                    $(document).ready(function() {

                                    setTimeout(function() {
                                    // $('#menu_toggle').trigger('click');
                                    // $(body).addClass('sidebar-collapse');
                                    $('.theadscroll').perfectScrollbar("update");
                                    $(".theadfix_wrapper").floatThead('reflow');
                                    }, 300);
                                    $('.datepicker').datetimepicker({
                                    format: 'DD-MMM-YYYY'
                                    });
                                    $('.date_time_picker').datetimepicker();
                                    $('.theadscroll').perfectScrollbar({
                                    wheelPropagation: true,
                                            minScrollbarLength: 30
                                    });
                                    });</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
    function addStockRequestNew(){
    // var protocol = window.location.protocol;
    // var host_name = window.location.host;
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addStockConsumption";
    var action_url = main_uri + route_path;
    document.location.href = action_url;
    // alert(action_url);
    }
    function stockItemEditLoadData(list, stock_id){
    // alert(stock_id);
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addStockConsumption/" + stock_id;
    var action_url = main_uri + route_path;
    document.location.href = action_url;
    }
    $('#user_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var user_name = $(this).val();
    user_name = user_name.trim();
    if (user_name == "") {
    $("#user_name_searchCodeAjaxDiv").html("");
    $("#user_name_hidden").val("");
    } else {
    try {
    var param = {search_user_name: user_name};
    $.ajax({
    type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
            $("#user_name_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            if (html == 'No Result Found') {
            }
            $("#user_name_searchCodeAjaxDiv").html(html).show();
            $("#user_name_searchCodeAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });
    } catch (err) {
    document.getElementById("demo").innerHTML = err.message;
    }
    }
    } else {
    ajax_list_key_down('user_name_searchCodeAjaxDiv', event);
    }
    });
    $('#user_name').on('keydown', function (event) {
    if (event.keyCode === 13) {
    ajaxlistenter('user_name_searchCodeAjaxDiv');
    return false;
    }
    });
    function fill_user_details(e, user_name, user_id) {
    $('#user_name').val(user_name);
    $('#user_name_hidden').val(user_id);
    $('#user_name_searchCodeAjaxDiv').hide();
    }
//---------------------------------------------------------------------------------------------
    $('#transcation_no').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var transcation_no = $(this).val();
    transcation_no = transcation_no.trim();
    if (transcation_no == "") {
    $("#transcation_no_searchCodeAjaxDiv").html("");
    } else {
    try {
    var param = {transcation_no_search: transcation_no};
    $.ajax({
    type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
            $("#transcation_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            if (html == 'No Result Found') {
            }
            $("#transcation_no_searchCodeAjaxDiv").html(html).show();
            $("#transcation_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });
    } catch (err) {
    document.getElementById("demo").innerHTML = err.message;
    }
    }
    } else {
    ajax_list_key_down('transcation_no_searchCodeAjaxDiv', event);
    }
    });
    $('#transcation_no').on('keydown', function (event) {
    if (event.keyCode === 13) {
    ajaxlistenter('transcation_no_searchCodeAjaxDiv');
    return false;
    }
    });
    function fill_transaction_details(e, trans_no) {
    $('#transcation_no').val(trans_no);
    $('#transcation_no_searchCodeAjaxDiv').hide();
    }
    @include('Purchase::messagetemplate')
</script>

@endsection
