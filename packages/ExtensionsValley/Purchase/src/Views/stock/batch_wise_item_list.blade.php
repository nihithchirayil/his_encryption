<div class="row">
    <div class="col-md-12 item_label">
        <div>
            <h4 class="title" style=" text-align: center;padding-top: 8px;margin-top: 0px;">
                <span id="modal_desc" style="line-height: 28px;">{{$request_form['item_desc']}} </span></h4>
        </div>

        <div class="clearfix"></div>
        <div class="h10"></div>
        <div class="col-md-8 padding_sm" >
            <input type="hidden" id="item_code_hiddenbatch" value="{{$request_form['item_code']}}">
            <div class="theadscroll" >
                <table class="table  table_round_border no-margin theadfix_wrapper table_sm">
                    <thead>
                        <tr class="table_header_bg">
                            <th >Batch No</th>
                            <th >Exp.</th>
                            <th >Mrp.</th>
                            <th >Stock</th>
                            <th >Iss.Qty</th>
                        </tr>
                    </thead>
                    <tbody class="tbody_itemdata">

                        <?php
                        $requsted_qty = 0;
                        $balance_stock = 0;
                        $issued_qty_batch = 0;
                        $total_issued_qty = 0;
                        if (count($request_form['res']) != 0) {
                            $i = 1;
                            $total_batch_qnty = 0.00;
                            foreach ($request_form['res'] as $each) {
                                $key = trim($each->r_item_code . $each->r_batch_no);
                                if ($request_form['batch_data'] != '') {
                                    $batch_data = json_decode($request_form['batch_data'], TRUE);
                                    if (isset($batch_data[$each->r_item_code][base64_encode($each->r_batch_no)])) {
                                        $issued_qty_batch = $batch_data[$each->r_item_code][base64_encode($each->r_batch_no)];
                                    } else {
                                        $issued_qty_batch = 0;
                                    }
                                    $total_batch_qnty += $issued_qty_batch;
                                }
                                if (isset($request_form['nearly_expired'])) {
                                    $nearly_ecp_val = $request_form['nearly_expired'];
                                    $exp_date = strtotime(date(\WebConf::getConfig('datetime_format_web'), strtotime($each->r_expiry_date)));
                                    $curdate = strtotime(date(\WebConf::getConfig('datetime_format_web')));
                                    $nerlyExp = strtotime('+' . $nearly_ecp_val . 'day', strtotime(date(\WebConf::getConfig('datetime_format_web'))));
                                }
                                ?>
                                <tr @if(isset($nerlyExp) && isset($curdate) && $exp_date <= $nerlyExp) style="background: #2fe096;" @endif>
                                    <td width='8%'> <input type="hidden" class="batchdata" value="{{$i}}">
                                        <input type="hidden" class="itembatch" id="itembatchid{{$i}}" value="{{$each->r_batch_no}}">{{$each->r_batch_no}}
                                        <input type="hidden" class="decimal_qty_ind" id="decimal_qty_ind{{$i}}" value="{{$each->decimal_qty_ind}}">
                                    </td>
                                    <td width='25%'>{{$each->r_expiry_date}}</td>
                                    <td width='5%'>{{$each->r_mrp}}</td>
                                    <td width='5%'id="itemstocktrid{{$i}}">{{$each->r_stock}}</td>
                                    <td width='10%'><input autocomplete="off" type='text' name='item_issueqtydb[]' id='item_issueqtyid{{$i}}' value='{{$issued_qty_batch}}' onkeyup="check_issue_qty_count(this);batch_qty_validation(this,{{$i}})" class='form-control issueqtychnges @if(isset($each->is_fractional) && ($each->is_fractional == 0)) allownumericwithoutdecimal @endif'></td>
                                </tr>
                                <?php
                                $requsted_qty += (int) $each->r_qty;
                                $balance_stock += (int) $each->r_stock;
                                $total_issued_qty += (int) isset($request_form['issd_qty'][$key]) ? $request_form['issd_qty'][$key] : 0;
                                $i++;
                            }
                            ?>
                            <tr>

                                <td style="text-align: right" colspan="3">
                                    <input type="hidden" value="{{$request_form['qty']}}" id="requested_qtyspan">
                                    Total Stock
                                </td>
                                <td id="requested_qtytot" class="requested_qtytot">{{$total_batch_qnty}}</td>
                            </tr>
                            <?php
                        } else {
                            echo '<tr><td colspan="9" style="text-align: center">No Result Found</td> </tr>';
                        }
                        ?>
                    </tbody>
                </table>


            </div>
        </div>
        <div class="col-md-4  padding_sm" >
        <div class="theadscroll" >

        <table class="table  table_round_border no-margin theadfix_wrapper table_sm" style="border:1px solid;"><tr class="table_header_bg"><th>Appr.Date</th><th>Appr.Qty</th></tr>@if(count($request_form['app'])!=0)@foreach ($request_form['app'] as $item)<tr><td>{{$item->to_char}}</td><td>{{$item->approved_qty}}</td></tr>@endforeach @else <td colspan="2">No Result Found</td> @endif
        </table>
        </div>
        </div>
    </div>
</div>
<div class="modal-footer" style="padding: 2px 7px;">
    <span class="btn btn-warning"  onclick="hide_batch_div(this)" style="padding: 0px 2px;"><i class="fa fa-times"> Cancel</i></span>
    <span class="btn btn-success" id="add_batch_button" onclick="add_batch_list(this);" style="padding: 0px 2px;"><i class="fa fa-thumbs-up"> OK</i></span>
</div>

<script type="text/javascript">

    function check_issue_qty_count(list) {
    $('#requested_qtyspan').removeClass();
    var requsted_qty_get = 0;
    $(list).closest(".tbody_itemdata").find(".issueqtychnges").each(function (key, val) {
        var item_value = val.value;
        requsted_qty_get += Number(item_value);
    });
        console.log(requsted_qty_get);
    $('.requested_qtytot').html(requsted_qty_get);
    validate_requested_qty(requsted_qty_get);
    }

    function validate_requested_qty(qty) {
    var requested_qty = Number($('#requested_qtyspan').html().trim());
    if (requested_qty < qty) {
    $('#requested_qtyspan').addClass('text_boxalert');
    $("#add_batch_button").prop("disabled", true);
    $('#requested_qtytot').addClass('text_boxalert');
    } else if (requested_qty >= qty) {
    $('#requested_qtyspan').removeClass();
    $("#add_batch_button").prop("disabled", false);
    $('#requested_qtytot').removeClass('text_boxalert');
    }
    }
    function batch_qty_validation(obj, inc) {
    var num = obj.value;
    var location_type = $("#location_type").val();
    var decimal_qty_ind_hidden = $(obj).closest("tr").find(".decimal_qty_ind").val();
    if(decimal_qty_ind_hidden==1){
        var valid = /^\d{0,12}(\.\d{0,5})?$/.test(obj.value),
        val = obj.value;
        if (!valid) {
            obj.value = val.substring(0, val.length - 1);
            $('#requested_qtytot').html('0');
        }
    }else{
    var valid = /^\d{0,12}$/.test(obj.value),
    val = obj.value;
    if (!valid) {
        obj.value = val.substring(0, val.length - 1);
        $('#requested_qtytot').html('0');
        }
    }

    // var valid = /^\d+$/.test(obj.value),
    //         val = obj.value;
    // if (!valid) {
    //     obj.value = '';
    //     $('#requested_qtytot').html('0');
    // }
    if (num) {
        var itemstock = Number($('#itemstocktrid' + inc).html().trim());
        if (itemstock < num) {
            toastr.error('Less Quantity in Store');
            $("#add_batch_button").hide();
        } else {
            $("#add_batch_button").show();
        }

    } else {
        $('#requested_qtytot').html('0');
    }
}
</script>
