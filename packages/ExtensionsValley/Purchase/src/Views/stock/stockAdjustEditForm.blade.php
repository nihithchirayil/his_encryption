@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
       .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .search_box_bottom{
        bottom: -30px;
    }
    .ajax_search_box_bottomcls .ajaxSearchBox{
        bottom: 3px;
        height: 200px !important;
    }
    .ajax_search_box_bottomcls_reduce_box{
        height: 200px !important;
    }
    table td{
        position: relative !important;
    }

</style>
@endsection
@section('content-area')
     <!-- page content -->
     <div class="right_col"  role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                 <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                {!!Form::open(array('route' => 'extensionsvalley.purchase.updateStockAdjustment', 'method' => 'post', 'class' => 'formShortcuts', 'id' => 'requisitionForm'))!!}
                <div class="col-md-12 no-padding">
                <div class="box no-border no-margin" >
                    <div class="box-body clearfix">
                           <div class="col-md-2 padding_sm">
                            @if(isset($item[0]) && $item[0] !='')
                            <div class="mate-input-box">
                                <label for="">Stock Adjustment No.</label>
                                <div class="clearfix"></div>
                                <span>{{isset($item[0]->stock_adjustment_no) ? $item[0]->stock_adjustment_no : ''}}</span>
                                <input type="hidden" class="form-control" name="stock_adjustment_no" id="stock_adjustment_no" value="{{isset($item[0]->stock_adjustment_no) ? $item[0]->stock_adjustment_no : ''}}" readonly="">
                                 <input type="hidden" class="form-control" name="stock_head_id" id="stock_head_id" value="{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}" readonly="">
                             </div>
                            @endif
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Item Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('item_type', array("-1"=> " Select Type") + $item_type->toArray(),isset($item[0]->item_type) ? $item[0]->item_type :null, [
                            'class'       => 'form-control item_type',
                            'id'    => 'item_type'
                        ]) !!}
                    </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('from_location', array("-1"=> " Select Location") + $location->toArray(),isset($item[0]->location_code) ? $item[0]->location_code :$default_location, [
                            'class'       => 'form-control from_location select2',
                            'id'    => 'from_location'
                        ]) !!}
                    </div>
                            </div>
                            <!-- <div class="col-md-2 padding_sm">
                                <label for="">Financial Year</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control year_picker" name="financial_year" id="financial_year"
                                    value="">
                            </div> -->
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Reason</label>
                                <div class="clearfix"></div>
                               {!! Form::select('reason_head', array("-1"=> " Select") + $reason_data->toArray(),isset($item[0]->reason_head) ? $item[0]->reason_head :null, [
                                            'class'       => 'form-control reason_list',
                                            'id'    => 'reason_list'
                                        ]) !!}
                                    </div>
                            </div>
                            <!-- <div class="col-md-1 padding_sm">
                                <label for="">Sales Date</label>
                                <div class="clearfix"></div>
                                <?php
                                // if(isset($item[0]->sales_date)){
                                //     $sales_date = $item[0]->sales_date;
                                // }else{
                                //     $now = date('Y-m-d');
                                //     $sales_date = date('Y-m-d', strtotime($now));
                                // }
                                ?>
                                <label style="background-color: #e7e7e7; color: black;"></label>
                                <input type="hidden" class="form-control" name="sales_date" id="sales_date" value="">
                            </div> -->
<!--                             <div class="clearfix"></div>
 -->                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                     <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                       <textarea autocomplete="off" class="form-control" wrap="off" cols="35" rows="1" name="remarks" >{{isset($item[0]->comments) ? $item[0]->comments :''}}</textarea>
                                   </div>
                            </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm text-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div data-toggle="modal" data-target="#addNewReason"  class="btn btn-primary managereasonbtn" style="cursor: pointer;">
                                     Manage Reason</div>
                </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                <div class="theadscroll always-visible" style="position: relative; height: 400px;">

                <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="addNewItem" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg" style="cursor: pointer;">
                               <th width="2%">#</th>
                               <th width="15%">
                                    <input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Item Search.. " style="color: #000;display: none;width: 90%;" >
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Item Name</span>
                                </th>
                               <th width="7%">Batch</th>
                               <th width="1%"></th>
                               <th width="3%" title="Reference Batch">Ref.Batch</th>
                               <th width="3%" title="Expiry Date">Exp.Date</th>
                               <th width="3%">Stock</th>
                               <th width="3%">Reason</th>
                               <th width="3%" title="Adjustment Operation">Adj.Optn</th>
                               <th width="3%" title="Adjustment Quantity">Adj.Qty</th>
                               <th width="3%"><button type="button" tabindex="-1" id="add"  class="btn btn-success add_row_btn" onclick="addNewRowByDefault()"><i class="fa fa-plus"></i></button></th>
                           </tr>

                       </thead>
                       <tbody id="fillrequestitemdetails">

                       </tbody>

            </table>
                </div>


                 <div class="clearfix"></div>
                    <div class="ht5"></div>
            </div>
                <div class="col-md-12">
                <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                @if(isset($item[0]->status) && ($item[0]->status == '2'))
                <div></div>
                @else
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="saveItem();" class="btn btn-block light_purple_bg saveitembtn"><i class="fa fa-save"></i>
                                    Save</div>
                </div>
                @if(isset($approveAccess) && ($approveAccess== '1'))
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="approveItem();" class="btn btn-block btn-primary approveitembtn"><i class="fa fa-check"></i>
                                    Approve</div>
                </div>
                @endif
                @endif
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                               <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                </div>
            </div>
                </div>
            </div>
            </div>
                    <input type="hidden" id="approveitembtnhid" value="0" name="approveitembtnhid">
                    <input type="hidden" id="saveitembtnhid" value="0" name="saveitembtnhid">
            <div class="clearfix"></div>
            <div class="ht10"></div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Modal -->
<div id="addNewReason" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Reason</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Reason Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="reason_name" id="reason_name">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;"  onclick="manageReasonReset();"   class="btn btn-warning">
                <i class="fa fa-times"></i>&nbsp;&nbsp;Clear</button>
                <button style="margin: 0 5px 0 0;"  onclick="manageReason();" class="btn light_purple_bg">
                <i class="fa fa-save"></i>&nbsp;&nbsp;Save</button>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
</script>

@stop

@section('javascript_extra')
<script src="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.js')}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    var j=1;
    var flag  = 0;
    batch_array = {};
    item_array = {};
    var previous;
(function () {
    $("#from_location").on('focus', function () {
    previous = this.value;
}).change(function() {
    if(previous == '-1' || previous == -1){
        setTimeout(function(){
         addNewRowByDefault();
     },300);
    }
});
})();



function validate_adj_qty(e){

    var adj_op = $(e).parents("tr").find(".adj_optn").val();
    var total_stock = $(e).closest("tr").find("input[name='stock[]']").val();
    var adj_qty = $(e).val();
    if(adj_op == 2){
        if(parseFloat(adj_qty) > parseFloat(total_stock)){
            alert("Please check total stock");
            $(e).val('');
        }
    }
    var chk_btch = $(e).parents("tr").find("input[name='item_batch[]']").val();
//    var valid_refer_batch = 1;
//    $(".refer_batch_cls option").each(function(){
//        var fnd_batch = $(this).val();
//        if(chk_btch == fnd_batch){
//            valid_refer_batch = 0;
//        }
//    });
//    if(valid_refer_batch == 1){
//         toastr.warning("Please select reference batch !!");
//         $(e).val('');
//    }
    if(chk_btch == ''){
         toastr.warning("Please add batch !!");
         $(e).val('');
    }


    if(parseFloat(adj_qty) < 0){
        $(e).val('');
    }


}
$(document).ready(function() {
$('body').on('keydown', '.adj_qtyy', function(e) {
        var th = $(this).closest("tr").find("input[name='item_desc[]']").attr('id');
        var ths = th.split('_');
        var rowCounts =  ths[2];
        var rowCountss = parseFloat(rowCounts)+1;
    if (e.which == 9) {  setTimeout(function(){
    $('#item_code_'+rowCountss).focus();
            },300);
    }
});
    $("#item_search_btn").click(function(){
      $("#issue_search_box").toggle();
      $("#item_search_btn_text").toggle();
    });

    @if(isset($item[0]->stock_id))
        getStockRecords("{{$item[0]->stock_id}}");
    @else
    addNewRowByDefault();
    setTimeout(function(){
    document.getElementById("item_code_1").focus();
    },2000);
    @endif

    var $table = $('table.theadfix_wrapper');
    $('.year_picker').datetimepicker({
                    format: 'YYYY'
                });

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
    });
$('.newdatepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
});

/**
 *
 * Java script search in the search item box
 */

function addNewRowByDefault(){

            if(document.getElementById('from_location').value == "-1")
            {
                 alert('Please Enter From Location ..! ');return false;
            }

            var table = document.getElementById("addNewItem");
            var newrow = table.rows.length;
            var row = table.insertRow(newrow);
            row.id = "newRow" + newrow;
            <?php  $a = 1; $b = 0;
               foreach($incr_array as $inc) { ?>

                    var cell_no{{$a}} = row.insertCell("{{$b}}");

                    cell_no{{$a}}.innerHTML = "<?php  echo $inc; ?>";
            <?php
                    $a++; $b++;
                } ?>

            var reason_head = $("#reason_head").val();
            console.log(reason_head);
            cell_no2.firstChild.id = 'item_code_'+j;
            cell_no3.firstChild.id = 'item_batch_'+j;
            cell_no4.firstChild.id = 'add_btch_span_'+j;
            cell_no6.firstChild.id = 'return_reason_'+j;
            var text_input_id = cell_no2.firstChild.id;
            var text_input_id5 = cell_no6.firstChild.id;
            document.getElementById(text_input_id).nextSibling.id = 'ajaxSearchBox_'+j;
            $("#"+text_input_id5).val(reason_head);
            var text_input_id1 = cell_no3.firstChild.id;
            document.getElementById(text_input_id1).nextSibling.id = 'ajaxSearchBatchBox_'+j;
            if(j > 2){
                $('#ajaxSearchBox_'+j).addClass('ajax_search_box_bottomcls_reduce_box');
            }
            if(j > 6){
                $('#newRow'+j).addClass('ajax_search_box_bottomcls');
            }
            $("#add_btch_span_"+j).trigger("click");
            j++;

 }

function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("addNewItem");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function searchItemBatch(id,event,list) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        var ajax_div = $('#'+id).next().attr('id');
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var fromLocationPub = $('#from_location').val();
            var toLocationPub = $('#to_location').val();
            var item_batch = $('#'+id).val();
            var item_code = $(list).closest("tr").find("input[name='item_code_hidden[]']").val();
            if (item_batch == "") {
                $("#"+ajax_div).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxStockAdjustItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_batch=' +item_batch+'&item_code='+item_code+'&from_location='+fromLocationPub+'&to_location='+toLocationPub+'&batchData='+1,
                    beforeSend: function () {
                    $("#"+ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+ajax_div).html(html).show();
                        $("#"+ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            }
}

function searchItemCode(id,event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        var ajax_div = $('#'+id).next().attr('id');
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var fromLocationPub = $('#from_location').val();
            var toLocationPub = $('#to_location').val();
            var item_code = $('#'+id).val();
            if (item_code == "") {
                $("#"+ajax_div).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxStockAdjustItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_code=' +item_code+'&from_location='+fromLocationPub+'&to_location='+toLocationPub+'&itemSearchData='+1,
                    beforeSend: function () {
                    $("#"+ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+ajax_div).html(html).show();
                        $("#"+ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            }
}

function fillNoItem(list){
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#'+itemCodeListDivId).hide();
}
function fillItemValues(list,item_code,item_desc) {

   var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(item_desc);
    $('#'+itemCodeListDivId).hide();
   // alert(itemCodeListDivId+" -- "+itemCodeTextId ); return;

    $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    // $('#'+itemCodeTextId).closest("tr").find("label[for='stock[]']").text(dep_stock);
     $('#'+itemCodeTextId).closest("tr").find(".item_batch").focus();
    // addNewRowByDefault();
}

function fillItemBatchValues(list,batch_no,expiry_date,stock){
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(batch_no);
    $('#'+itemCodeListDivId).hide();

    $('#'+itemCodeTextId).closest("tr").find("input[name='item_batch_hidden[]']").val(batch_no);
    $('#'+itemCodeTextId).closest("tr").find("input[name='item_batch_exp_hidden[]']").val(expiry_date);
    $('#'+itemCodeTextId).closest("tr").find("label[for='stock[]']").text(stock);
    $('#'+itemCodeTextId).closest("tr").find("input[name='stock[]']").val(stock);

    $('#'+itemCodeTextId).closest("tr").find("label[for='expiry_date[]']").text(expiry_date);
    //$(".item_reason").focus();
     $('#'+itemCodeTextId).parents("tr").find("input[name='item_reason[]']").focus();

     var chk_new_row_create = 0;
    $('#addNewItem tr').each(function () {
        if ($(this).find('input[name="item_desc[]"]').val() ==''){
            chk_new_row_create = 1;
        }
    });
    if(chk_new_row_create == 0){
    addNewRowByDefault();
    }
}

function deleteRow(list){
    var item_name = $(list).closest("tr").find(".item_desc").val();
    if(item_name == ""){
        var id = $(list).closest("tr").attr('id');
        $("#"+id+"").remove();
    }else{
        if (confirm('Are you sure wanted to delete?')) {
                var id = $(list).closest("tr").attr('id');
                $("#"+id+"").remove();
        }
    }
}

function deleteEditRow(list,id){
     if (confirm('This item will be deleted immediately , are you sure to delete?')) {
            var url = '';
        $.ajax({
            type: "GET",
            url: url,
            data: 'stockrequestdelID='+id,
            beforeSend: function () {
            },
            success: function (data) { //alert(html);
                if(data==1){
                    var d = $(list).closest("tr").attr('id');
                    $("#"+d+"").remove();
                }
            }
        });

        }
}

function getStockRecords(stock_id){
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'stockItemDetails='+stock_id,
        beforeSend: function () {
        },
        success: function (html) { //alert(html);
        $("#fillrequestitemdetails").html(html);
        addNewRowByDefault();
            setTimeout(function(){
            document.getElementById("item_code_1").focus();
        },2000);
        }
    });
}

function saveItem(){
        var flag = 0;
        var f  = 0;
        $(".adj_qtyy").each(function () {
            var item = $(this).closest("tr").find(".item_desc").val();
            if(item != ""){
                if (isNaN(this.value) || this.value == '' || this.value == 0){
                        f  = 1;
                    }
            }else{
                if (this.value != '' || this.value != 0){
                            flag = 1;
                }
            }
        });
        if(f == 1){
                alert("Request quantity is zero"); return false;
        }
        if(document.getElementById('from_location').value == "-1")
        {
             alert('Please Enter From Location ..! ');
             return false;

        }

        var table = document.getElementById("addNewItem");
        var count_row = table.rows.length;

        // $(".item_desc" ).each(function() {
        //         if($(this).val() == ""){
        //             flag = 1;
        //         }
        // });
        if(flag == 1 || count_row == 1){
                 alert('Please Enter Item ..! ');
                 return false;
        }else{
                $(".saveitembtn").prop("disabled",true);
                $("#saveitembtnhid").val("1");
                $("#requisitionForm").submit();
        }
}

function approveItem(){
        var flag = 0;
        var f  = 0;
        $(".adj_qtyy").each(function () {
            var item = $(this).closest("tr").find(".item_desc").val();
            if(item != ""){
                if (isNaN(this.value) || this.value == '' || this.value == 0){
                        f  = 1;
                    }
            }else{
                if (this.value != '' || this.value != 0){
                            flag = 1;
                }
            }
        });
        if(f == 1){
                alert("Request quantity is zero"); return false;
        }
        if(document.getElementById('from_location').value == "-1")
        {
             alert('Please Enter From Location ..! ');
             return false;

        }

        var table = document.getElementById("addNewItem");
        var count_row = table.rows.length;
        // $(".item_desc" ).each(function() {
        //         if($(this).val() == ""){
        //             flag = 1;
        //         }
        // });
        if(flag == 1 || count_row == 1){
                 alert('Please Enter Item ..! ');
                 return false;
        }else{
                $(".approveitembtn").prop("disabled",true);
                $("#approveitembtnhid").val("2");
                $("#requisitionForm").submit();
        }
}

function manageReasonReset(){
    $("#reason_name").val('');
}

function manageReason(){
    var url = "{{route('extensionsvalley.purchase.addStockAdjustReason')}}";
    var reason_name = $("#reason_name").val();

    $.ajax({
        type: "GET",
        url: url,
        data: 'reason_name='+reason_name,
        beforeSend: function () {
        },
        success: function (data) { //alert(data);
            if(data){
                // alert("Reason Added Successfully..");
                $(".reason_list").empty();
                $(".reason_list").append('<option value="-1"> Select Reason</option>');
                $.each(data,function(key,value){
                  $(".reason_list").append('<option value="'+key+'">'+value+'</option>');
                });
            }else{
                // alert("Reason Not Added Successfully..");
            }
        },
        complete: function (){
            $("#addNewReason").modal('hide');
        }
    });
}
function addBatch(e){
    var ids = $(e).closest("tr").find("input[name='item_batch[]']").attr('id');
    var hidden_ids = $(e).closest("tr").find("input[name='item_batch_hidden[]']").attr('id');
    var batch_ids = "'"+ids+"'";
    var hidden_batch_ids = "'"+ids+"'";
    //***************************************************************
    var exp_hidden_ids = $(e).closest("tr").find("input[name='item_batch_exp_hidden[]']").attr('id');
    var hidden_exp_ids = "'"+exp_hidden_ids+"'";
    //*****************************************************************
    //***************************************************************
    var stock_ids = $(e).closest("tr").find("input[name='stock[]']").attr('id');
    var hidden_stock_ids = "'"+stock_ids+"'";
    //*****************************************************************
    //*****************************************************************
    var batch_data = "";
    var url = "{{route('extensionsvalley.purchase.addAjaxStockAdjustItem')}}";
    var from_location = $('#from_location').val();
    var item_code = $(e).closest("tr").find("input[name='item_code_hidden[]']").val();
    $.ajax({
        type: "GET",
        url: url,
        data: 'get_batch_data=1'+'&from_location='+from_location+'&item_code='+item_code,
        beforeSend: function () {},
        success: function (data) {
            if(data){
                var cnt = Object.keys(data).length;
                $(".refer_batch_cls").empty();
                $(".refer_batch_cls").append('<option value="">Select Batch</option>');
                // $.each(data,function(key,value){
                for(i=0;i<cnt;i++){
                  $(".refer_batch_cls").append('<option value="'+data[i].batch_no+'">'+data[i].batch_no+'</option>');
                }
                // });
            }else{
                data = "";
            }
        },
        complete: function (){}
    });
    //*****************************************************************
    $('[data-toggle="popover"]').popover({
        delay: {
            show: 100
        },
         html: true,
         sanitize: false,
         content: '<div class="col-md-12 no-padding">\n\
                    <div class="form-group clearfix">\n\
                    <div class="col-md-6 padding_sm">\n\
                    <div class="mate-input-box"><label for="">Reference Batch</label>\n\
                    <select required class="form-control refer_batch_cls" onchange="setReferBatch(this,'+batch_ids+')"><option value="">Select Batch</option></select></div></div>\n\
                    <div class="col-md-6 padding_sm">\n\
                    <div class="mate-input-box"><label for="">Batch</label>\n\
                    <input type="text" value=" " class="form-control" onkeyup="checkReferenceBatch(this,'+batch_ids+','+hidden_batch_ids+')" onblur="setbtch(this,'+batch_ids+','+hidden_batch_ids+')"></div></div></div></div>'
//                     +''+'<div class="col-md-4">\n\
//                    <div class="mate-input-box"><label for="">Stock</label>\n\
//                    <input type="text" value=" " class="form-control"  onblur="setstock(this,'+batch_ids+')">\n\

//                    +''+'<div class="col-md-4">\n\
//                    <div class="mate-input-box"><label for="">Exp.Date</label>\n\
//                    <input type="text" value=" " class="form-control newdatepicker"  onblur="setExpiryDate(this,'+batch_ids+')"></div></div>\n\
                    ,
                                showCallback: function () {
            $('.newdatepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
        }
    });
}
function setbtch(e,ids,hidden_batch_ids){
    var vls = e.value;
    $("#"+ids).closest("tr").find("input[name='item_batch[]']").val(vls);
    $("#"+hidden_batch_ids).closest("tr").find("input[name='item_batch_hidden[]']").val(vls);
    }
function setReferBatch(e,ids){
    var vls = e.value;
    $("#"+ids).closest("tr").find("label[for='refer_batch[]']").text(vls);
    $("#"+ids).closest("tr").find("input[name='refer_batch_hidden[]']").val(vls);
}
function checkReferenceBatch(e,ids,hidden_batch_ids){
    var vls = e.value;
        var chk_ref_btch = $("#"+ids).closest("tr").find("input[name='refer_batch_hidden[]']").val();
         if(chk_ref_btch == ''){
         toastr.warning("Please select reference batch !!");
         $(e).val('');
    }
    }
function setExpiryDate(e,hidden_batch_ids){
    var vls = e.value;
     $('#'+hidden_batch_ids).closest("tr").find("label[for='expiry_date[]']").text(vls);
     $('#'+hidden_batch_ids).closest("tr").find("input[name='item_batch_exp_hidden[]']").val(vls);
}
function setstock(e,hidden_batch_ids){
    var vls = e.value;
    $('#'+hidden_batch_ids).closest("tr").find("label[for='stock[]']").text(vls);
    $("#"+hidden_batch_ids).closest("tr").find("input[name='stock[]']").val(vls);
}
</script>

@endsection
