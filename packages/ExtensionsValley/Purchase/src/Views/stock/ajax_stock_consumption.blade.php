<?php
$i = 1;
?>
@if(isset($stockItemDetails) && !empty($stockItemDetails))
@foreach($stockItemDetails as $data)

<tr id="{{$data->id}}" >

    <td style="width: 2%;">
        <input type="hidden" id="save_update_{{$i}}" name="save_update[]"  value="1">
        <input name="del[]" class="deleteID" type="checkbox" value="{{$data->id}}" >
        <input type="hidden" id="save_update_id_{{$i}}" name="save_update_id[]" value="{{$data->id}}">
    </td>
    <td style="width: 15%;">
        <input type='text' required  autocomplete='off' value="{{$data->item_desc}}" id="field_item_code-{{$i}}" readonly name='item_desc[]' onkeyup='searchItemCode(this.id, event)' class='form-control item_desc' placeholder='Search Item Name'>
        <div class='ajaxSearchBox' style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width: 34%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;'> </div>
        <input type='hidden' class="item_code_hidden" name='item_code_hidden[]' value="{{$data->item_code}}" id="item_code_hidden{{$i}}">
    </td>
    <td style="width: 7%;">
        <label for='item_batch[]' id="field_batch-{{$i}}" class="batch_label">{{$data->batch_no}}</label>
        <input type='hidden' class='item_batch' value="{{$data->batch_no}}" name='item_batch[]'>
        <input type='hidden' required  readonly autocomplete='off' name='item_batch[]' onkeyup='searchItemBatch(this.id, event, this)' value="{{$data->batch_no}}" class='form-control item_batch' placeholder='Search Batch'>
        <div class='ajaxSearchBox' style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width: 23%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
        <input type='hidden' class='item_batch_hidden' value="{{$data->batch_no}}" name='item_batch_hidden[]'>

    </td>

    <td>
        <label for='stock[]' id="field_stock-{{$i}}" class="stock_label">{{$data->current_stock}}</label>
        <input type='hidden' class='form-control' name='stock[]' value="{{$data->current_stock}}">
    </td>
    <td>
        <label for='item_batch_exp_hidden' class='item_batch_exp_hidden'>{{$data->expiry_date ?  date(\WebConf::getConfig('date_format_web'), strtotime($data->expiry_date)) :''}}</label>
        <input type='hidden' class='item_batch_exp_hidden' value="{{$data->expiry_date ? date(\WebConf::getConfig('date_format_web'), strtotime($data->expiry_date)) :''}}" name='item_batch_exp_hidden[]'>
    </td>
    <td>
        <input type='text' class='form-control request_req_qty' required autocomplete='off'  name='req_qty[]' value="{{$data->consumed_quantity}}" id="field_req_qty-{{$i}}" onkeyup="number_validation(this);checkStock(this)">
    </td>
    <td>
        <input type='text' class='form-control' name='remarks[]' value="{{$data->remarks ? $data->remarks:''}}">
    </td>

</tr> <?php $i++; ?>
@endforeach
@endif