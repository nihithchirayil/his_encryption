<?php $i=1; ?>
@if(isset($item_req_history) && !empty($item_req_history))
@foreach($item_req_history as $item)
<tr style="cursor: pointer;">
    <td style="text-align: left;" class="common_td_rules"  title="{{ $item->request_no }}">{{ $item->request_no }}</td>
    <td style="text-align: left;" title="{{ $item->requested_date }}" class="common_td_rules">{{ $item->requested_date }}
    </td>
    <td style="text-align: left;" class="common_td_rules"  title="{{ $item->issued_by }}">{{ $item->issued_by }}</td>
    <td style="text-align: left;" class="common_td_rules"  title="{{ $item->received_by }}">{{ $item->received_by }}</td>
    <td style="text-align: left;" class="common_td_rules" title="{{ $item->received_date }}">{{ $item->received_date }}
    </td>
    <td style="text-align: left;" class="common_td_rules" title="{{ $item->issue_status }}">{{ $item->issue_status }}</td>
    <td style="text-align: left;" class="common_td_rules" title="{{ $item->issued_qty }}">{{ $item->issued_qty }}</td>
    
</tr>
 <?php $i++; ?>
@endforeach
@endif

<?php $i=1; ?>
@if(isset($item_stock_req_history) && !empty($item_stock_req_history))
@foreach($item_stock_req_history as $item)
<tr style="cursor: pointer;">
    <td style="text-align: left;" class="common_td_rules"  title="{{ $item->request_no }}">{{ $item->request_no }}</td>
    <td style="text-align: left;" title="{{ $item->requested_date }}" class="common_td_rules">{{ $item->requested_date }}
    </td>
    <td style="text-align: left;" class="common_td_rules"  title="{{ $item->issued_by }}">{{ $item->issued_by }}</td>
    <td style="text-align: left;" class="common_td_rules"  title="{{ $item->received_by }}">{{ $item->received_by }}</td>
    <td style="text-align: left;" class="common_td_rules" title="{{ $item->received_date }}">{{ $item->received_date }}
    </td>
    <td style="text-align: left;" class="common_td_rules" title="{{ $item->issue_status }}">{{ $item->issue_status }}</td>
    <td style="text-align: left;" class="common_td_rules" title="{{ $item->issued_qty }}">{{ $item->issued_qty }}</td>
    
</tr>
 <?php $i++; ?>
@endforeach
@endif