<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url != 'undefined') {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var return_no = $('#return_no_hidden').val();
                var from_location = $('#from_location').val();
                var to_location = $('#to_location').val();
                var status = $('#status').val();
                var param = {
                    _token: token,
                    from_date: from_date,
                    to_date: to_date,
                    return_no: return_no,
                    from_location: from_location,
                    to_location: to_location,
                    status: status
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#stock_return_receive_list").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        $('#stock_return_receive_list_btn').attr('disabled', true);
                        $('#stock_return_receive_list_spin').removeClass('fa fa-search');
                        $('#stock_return_receive_list_spin').addClass(
                            'fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#stock_return_receive_list').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function() {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    complete: function() {
                        $("#stock_return_receive_list").LoadingOverlay("hide");
                        $('#stock_return_receive_list_btn').attr('disabled', false);
                        $('#stock_return_receive_list_spin').removeClass(
                            'fa fa-spinner fa-spin');
                        $('#stock_return_receive_list_spin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>

<div class="theadscroll" style="position: relative; height: 450px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>Return No</th>
                <th>Return From</th>
                <th>Return To</th>
                <th>Returned Date</th>
                <th>Approved Date</th>
                <th>Approved By</th>
                <th>Received Date</th>
                <th>Received By</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @if (count($item_list) > 0)
                @foreach ($item_list as $item)
                    <tr style="cursor: pointer;" onclick="addWindowLoad('receiveStockReturn','{{ $item->stock_id }}')">
                        <td style="text-align: left;" class="common_td_rules">{{ $item->return_no }}</td>
                        <td style="text-align: left;" class="common_td_rules">{{ $item->from_location }}</td>
                        <td style="text-align: left;" class="common_td_rules">{{ $item->to_location }}</td>
                        <td style="text-align: left;" title="{{ $item->return_date }}" class="common_td_rules">
                            {{ $item->ret_date }}
                        </td>
                        <td style="text-align: left;" title="{{ $item->approved_date }}" class="common_td_rules">
                            {{ $item->apv_date }}</td>
                        <td style="text-align: left;" class="common_td_rules">{{ $item->approved_by }}</td>
                        <td style="text-align: left;" title="{{ $item->received_date }}" class="common_td_rules">
                            {{ $item->rec_date }}</td>
                        <td style="text-align: left;" class="common_td_rules">{{ $item->received_by }}</td>
                        <td style="text-align: left;" class="common_td_rules">{{ $item->status }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="12" class="location_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>
