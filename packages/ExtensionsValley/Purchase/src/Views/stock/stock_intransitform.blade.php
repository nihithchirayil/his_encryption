@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
     <!-- page content -->
<div class="right_col" >
<div class="row" style="text-align: right; padding-right: 66px; font-size: 12px; font-weight: bold;"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.purchase.listIntransitStock')}}" id="requestSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Request No.</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="req_no" id="item_code_search" 
                                    value="{{ $searchFields['req_no'] ?? '' }}">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control datepicker" name="from_date" id="from_date"
                                    value="{{ $searchFields['from_date'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control datepicker" name="to_date" id="to_date"
                                    value="{{ $searchFields['to_date'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Item Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="item_name" id="item_name_search" 
                                    value="{{ $searchFields['item_name'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Issued Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('issue_location', array("-1"=> " Issued Location") + $location->toArray(), $searchFields['issue_location'] ?? $default_location, [
                                'class'       => 'form-control issue_location select2',
                                'id'    => 'issue_location'
                            ]) !!}
                            </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Requested Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('request_location', array("-1"=> " Requested Location") + $location->toArray(), $searchFields['request_location'] ?? null, [
                                'class'       => 'form-control request_location select2',
                                'id'    => 'request_location'
                            ]) !!}
                            </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Rejected</label>
                                @if(isset($searchFields['is_rejected']) && $searchFields['is_rejected']==1 )
                                @php $checked='checked'; @endphp
                                @else
                                @php $checked='';@endphp
                                @endif
                                <input type="checkbox" autocomplete='off' class="" name="is_rejected" id="is_rejected" 
                                    value="1" {{$checked}}>
                                </div>
                            </div>
                         <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                           
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
            <form action="{{route('extensionsvalley.purchase.revertIntransitStock')}}" id="revertIntransitForm" method="POST">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Request No</th>
                                    <th>Requested Date</th>
                                    <th>Item</th>
                                    <th>Batch</th>
                                    <th>Intransit Qty</th>
                                    <th>Issued Location</th>
                                    <th>Issued Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($item) > 0)
                                @foreach ($item as $item)
                                <?php //dd($item); ?>
                                    <tr style="cursor: pointer;">
                                        <td>
                                        <input type="checkbox" typ_code="{{$item->typ_code}}" class="chkall" name="chkall[]" value="{{$item->id}}">
                                        <input type="hidden" class="chkall_type" name="chkall_type[]">
                                        </td>
                                        <td style="text-align: left;" class="common_td_rules"  title="{{ $item->typ }}" >{{$item->typ}}</td>
                                        <td style="text-align: left;" class="common_td_rules"  title="{{ $item->request_no }}" >{{ $item->request_no }}</td>
                                        <td style="text-align: left;" class="common_td_rules"  title="{{ $item->requested_date }}" >{{ $item->requested_date }}</td>
                                        <td style="text-align: left;" class="common_td_rules"  title="{{ $item->item_desc }}" >{{ $item->item_desc }}</td>
                                        <td style="text-align: left;" class="common_td_rules"  title="{{ $item->batch_no }}" >{{ $item->batch_no }}</td>
                                        <td style="text-align: left;" class="td_common_numeric_rules"  title="{{ $item->qnty_intransit }}" >{{ $item->qnty_intransit }}</td>
                                        <td style="text-align: left;" class="common_td_rules"  title="{{ $item->issue_location }}" >{{ $item->issue_location }}</td>
                                        <td style="text-align: left;" title="{{ $item->issued_date }}" class="common_td_rules"> {{ $item->issued_date }}</td>
                                    </tr>
                                @endforeach
                            @else
                                    <tr>
                                        <td colspan="12" class="location_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    
                    
                    </div>
                    <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="saveItem();" class="btn btn-block light_purple_bg saveitembtn"><i class="fa fa-save"></i>
                                    Save</div>
                    </div>
                    {!! Form::token() !!}
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                         <ul class="pagination purple_pagination" style="text-align:right !important;">
                            <?php echo $paginator->render(); ?>
                        </ul>
                        
                    </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
        </div>
    </div>
</div>
            {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
            {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}
            
            {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
            {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
             <script type="text/javascript">
                $(document).ready(function() {

                    setTimeout(function() {
                        // $('#menu_toggle').trigger('click');
                        // $(body).addClass('sidebar-collapse');
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 300);

                   $(document).on('click', '.grn_drop_btn', function(e) {
                    e.stopPropagation();
                    $(".grn_btn_dropdown").hide();
                    $(this).next().slideDown('');
                });
                   $(document).on('click', '.btn_group_box', function(e) {
                    e.stopPropagation();
                });

                   $(document).on('click', function() {
                    $(".grn_btn_dropdown").hide();
                });

                   $(".select_button li").click(function() {
                    $(this).toggleClass('active');
                });


                   $(document).on('click', '.notes_sec_list ul li', function() {
                    var disset = $(this).attr("id");
                    $('.notes_sec_list ul li').removeClass("active");
                    $(this).addClass("active");
                    $(this).closest('.notes_box').find(".note_content").css("display", "none");
                    $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
                });



                   $('.month_picker').datetimepicker({
                    format: 'MM'
                });
                   $('.year_picker').datetimepicker({
                    format: 'YYYY'
                });


                   var $table = $('table.theadfix_wrapper');

                   $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }

                });

                   $('.datepicker').datetimepicker({
                    format: 'DD-MMM-YYYY'
                });
                   $('.date_time_picker').datetimepicker();


                   $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });


    //       $('.fixed_header').floatThead({
    //     position: 'absolute',
    //     scrollContainer: true
    // });






    $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
        var $table = $('table.theadfix_wrapper');
        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }
        });
    })



});
</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on("click",".chkall", function(e){
            var typ_code = $(this).attr('typ_code');
            if($(this).prop('checked')==true){
                $(this).closest("td").find(".chkall_type").val(typ_code);
            }else{
                $(this).closest("td").find(".chkall_type").removeAttr("value");
            }
        });
    });
function saveItem(){
    var chkall_count = $(':checkbox[name="chkall[]"]:checked').length;
    // console.log(chkall_count);
                if(chkall_count == 0){
                    alert("Please select any of the records!");
                    return false;
                }else{
                    if(confirm("Are you sure to revert the intransit stock?") == true){
                        $(".saveitembtn").prop("disabled",true);
                        $("#revertIntransitForm").submit();
                    }else{
                        return false;
                    }
                }
        
}
@include('Purchase::messagetemplate')

</script>

@endsection
