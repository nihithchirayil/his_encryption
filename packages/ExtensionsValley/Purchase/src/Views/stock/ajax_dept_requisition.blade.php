<?php
if(isset($itemCodeDetails)){ ?>
<div class="medicine-list-div-row-listing" style="display:block">
    <div class="wrapper theadscroll" style="position: relative; height: 200px !important;">
        <table class="table theadfix_wrapper table-border-radius-top table-striped table_sm">
            <thead>
                <tr class="table_header_bg" style="background-color:#21a4f1 !important">
                    <th width="70%" style="text-align:center;border-left: solid 1px #bbd2bd !important;">Item</th>
                    <th width="15%" style="text-align:center;border-left: solid 1px #bbd2bd !important;">Dpt Stock</th>
                    <th width="15%" style="text-align:center;border-left: solid 1px #bbd2bd !important;">Store Stock</th>
                </tr>
            </thead>
            <tbody class="list-medicine-search-data-row-listing">
                <?php if (!empty($itemCodeDetails)) { $m=1;
                    foreach ($itemCodeDetails as $item) {
                        if($m==1){
                            $act_class = "active_row_item";
                        }else{
                            $act_class = "";
                        }
                        $location_stk = @$item->stk ? $item->stk :0;
                        $store_stk = @$item->store_stk ? $item->store_stk :0; ?>
            <tr onclick='fillItemValues(this,"{{ htmlentities($item->item_code) }}","{{ htmlentities($item->item_desc) }}","{{ htmlentities($item->uom_id) }}","{{ htmlentities($item->uom_name) }}","{{ htmlentities($item_code_id) }}")'
                class="list_hover {{ $act_class }}">
                <td style="border-left: solid 1px #bbd2bd !important;">{!! $item->item_desc !!} </td> <td style=" text-align:right;border-left: solid 1px #bbd2bd !important;">{!! $location_stk !!}</td> <td style=" text-align:right;border-left: solid 1px #bbd2bd !important;">{{ $store_stk }}</td>
            <?php
                   $m++; }
                } else {  ?>
            <tr onclick='fillNoItem(this);' style="display: block; padding:5px 10px 5px 5px; "> <td colspan="4"><?php
            echo 'No Result Found'; ?></td>
            </li> <?php
                } ?>
            </tbody>
        </table>
    </div>
</div>
<?php }
        //  <li style="display: block; padding:5px 10px 5px 5px; "
//                 onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->uom_id)}}","{{htmlentities($item->uom_name)}}")' class="list_hover">
//                 {!!$item->item_desc!!} &nbsp;({!! $location_stk !!})
//             </li> --}}
if (isset($grnitemCodeDetails)) {
    if (!empty($grnitemCodeDetails)) {
        foreach ($grnitemCodeDetails as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->item_id)}}","{{htmlentities($item->uom_name)}}","{{htmlentities($item->uom_id)}}","{{htmlentities($item->conv_factor)}}")'>
                {!!$item->item_desc!!}
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}

$i = 1;
?>
@if(isset($stockItemDetails) && !empty($stockItemDetails))
@foreach($stockItemDetails as $data)

<tr id="{{$data->id}}" >

    <td style="width: 2%;">
        <input type="hidden" id="save_update_{{$i}}" name="save_update[]"  value="1">
        <input name="del[]" class="deleteID" type="checkbox" value="{{$data->id}}" >
        <input type="hidden" id="save_update_id_{{$i}}" name="save_update_id[]" value="{{$data->id}}">
        <input type="hidden" name="editStkItmId[]" value="">
    </td>
    <td style="width: 20%;">
        <input type='text' required  autocomplete='off' value="{{$data->item_desc}}" id="field_item_code-{{$i}}" readonly name='item_desc[]' onkeyup='searchItemCode(this.id,event)' class='form-control item_desc' placeholder='Search Item Name'>
          <div class='ajaxSearchBox' style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
          <input type='hidden' class="item_code_hidden" name='item_code_hidden[]' value="{{$data->item_code}}" id="item_code_hidden{{$i}}">
    </td>
    {{-- <td>
        <button type='button' class='btn btn-info bttn_cls'  style='cursor:pointer; padding: 0px 2px;font-size: 13px;' onclick='showItemReqHistory(this)'><i class='fa fa-info' ></i></button><div  class='bttn_spin hide'><i class='fa fa-spinner fa-spin'></i></div>
    </td> --}}
    <td>
        <input type='checkbox' name='emergency[]' class="field_emergency" @if($data->is_urgent == 1) checked value='1' @else value='0' @endif id="field_emergency-{{$i}}">
        <input type='hidden' name='emerg[]' value="{{$data->is_urgent}}" id="emerg-{{$i}}">
    </td>
    <td class="common_td_rules">
        <label for='uom[]' id="field_uom-{{$i}}">{{$data->uom_name}}</label>
        <input type='hidden' name='uom[]' value="{{$data->uom_id}}" id="field_uom-{{$i}}">
    </td>
    <td class="td_common_numeric_rules">
        <label for='reorder_level[]'></label>
    </td>
    <!-- <td class="td_common_numeric_rules">
        <label for='min_order[]' ></label>
    </td> -->
    <td class="td_common_numeric_rules">
        <label for='stock[]' id="field_stock-{{$i}}">{{$data->r_depstock}}</label>
    </td>

    <td class="td_common_numeric_rules">
        <label for='st_stock[]'>{{$data->r_ststock}}</label>
    </td>
  {{--  <td class="td_common_numeric_rules">
        <label for='global_stock[]'>{{$data->r_globalstock}}</label>
    </td> --}}
    <td class="td_common_numeric_rules">
        <label for='lastRQty[]'>{{$data->last_requested_qty}}</label>
    </td>
    <td>
        <input type='text' readonly class='form-control request_req_qty' required autocomplete='off' onblur='chk_batch_item(this);' name='req_qty[]'  value="{{$data->requested_qty}}" onclick='get_product_batch(this,"{{$data->item_code}}", "{{$data->item_desc}}","{{$data->last_requested_qty}}","{{$data->r_depstock}}","{{$data->id}}");' id="field_req_qty-{{$i}}" >
        <div id='td_spin_{{$i}}' class="td_spin hide"><i class='fa fa-spinner fa-spin'></i></div><div class='issue_qty_pop hide' style="margin-left:-360px" id='issue_qty_batch_pop_{{$i}}'></div>
    </td>
    <?php if(isset($data->head_status) && ($data->head_status < 4)){ ?>
    <td>
        <i style='padding: 5px 8px; font-size: 15px;cursor: pointer;' class='fa fa-trash text-red deleteRow' onclick='deleteEditRow(this,"{{$data->id}}");'></i>
    </td>
    <?php } ?>
</tr> <?php $i++; ?>
@endforeach
@endif
<?php $i=1; ?>
@if(isset($pendingItemDetails) && !empty($pendingItemDetails))
@foreach($pendingItemDetails as $data)
<tr>
    <td width="3%" style="text-align: left;">{{$i}}</td>
    <td width="5%" style="text-align: left;"> {{$data->r_request_no}} </td>
    <td width="5%" style="text-align: left;"> {{$data->r_item_code}} </td>
    <td width="15%" style="text-align: left;"> {{$data->r_item_desc}} </td>
    <td width="3%" style="text-align: left;"> {{$data->r_issued_qty}} </td>
    <td width="3%" style="text-align: left;"> {{$data->r_approved_qty}} </td>
    <td width="3%" style="text-align: left;" style="text-align: left;"> {{$data->r_verified_qty}} </td>
    <td width="3%"style="text-align: left;" style="text-align: left;"> {{$data->r_requested_qty}} </td>

</tr> <?php $i++; ?>
@endforeach
@endif

<?php $i=1; ?>
@if(isset($grnItemDetails) && !empty($grnItemDetails))
<div class="theadscroll">
<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="grndata" style="border: 1px solid #CCC;">
    <thead>
    <tr class="table_header_bg ">
       <!-- <th width="3%">#</th> -->
       <th width="5%">GRN No.</th>
       <th width="5%">GRN Date</th>
       <th width="15%">Vendor Name </th>
       <th width="10%">Location</th>
    </tr>
    </thead>
    <tbody >
@foreach($grnItemDetails as $data)
<tr style="cursor: pointer;" onclick="list_grn_items(this,'{{$data->grn_head_id}}');">
    <!-- <td width="3%" style="text-align: left;"></td> -->
    <td width="5%" style="text-align: left;" class="common_td_rules"> {{$data->grn_no}} </td>
    <td width="5%" style="text-align: left;" title="{{$data->grn_date_time}}" class="common_td_rules"> {{$data->grn_date_dis}} </td>
    <td width="15%" style="text-align: left;" class="common_td_rules"> {{$data->vendor_name}} </td>
    <td width="15%" style="text-align: left;" class="common_td_rules"> {{$data->location_name}} </td>
</tr>
<?php $i++; ?>
@endforeach
</tbody>
</table>
    <div class="clearfix"></div>
    <div class="col-md-12 text-right"  style="padding-top:10px">
    <ul class="grn_pages pagination purple_pagination" style="text-align:right !important;">
                {!! $paginator->render() !!}
    </ul>
</div>
</div>

@endif

<?php $i=1; ?>
@if(isset($reorderLevelItemsList) && !empty($reorderLevelItemsList))
<div class="theadscroll">
<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="grndata" style="border: 1px solid #CCC;">
    <thead>
    <tr class="table_header_bg ">
       <th width="5%"><input type="checkbox" id="select_all_item" class=""><label for="select_all_item">All</label></th>
       <th width="5%">Item</th>
       <th width="5%">Store Stock</th>
       <th width="13%">Dept. Stock </th>
       <th width="10%">Re-order Level</th>
    </tr>
    </thead>
    <tbody >
@foreach($reorderLevelItemsList as $data)
<tr style="cursor: pointer;">
    <td width="3%" style="text-align: left;">
        <input type="checkbox" class="chk_reorder_items selected_item{{$data->item_code}}" onclick="list_reorder_level_items(this,'{{$data->item_code}}','{{$data->item_desc}}','{{$data->r_ststock}}','{{$data->r_depstock}}','{{$data->r_globalstock}}','{{$data->uom_id}}','{{$data->uom_name}}','{{$data->last_requested_qty}}','{{$data->reorder_level}}');">
    </td>
    <td width="5%" style="text-align: left;" class="common_td_rules"> {{$data->item_desc}} </td>
    <td width="5%" style="text-align: left;" class="common_td_rules"> {{$data->r_ststock}} </td>
    <td width="15%" style="text-align: left;" class="common_td_rules"> {{$data->r_depstock}} </td>
    <td width="15%" style="text-align: left;" class="common_td_rules"> {{$data->reorder_level}} </td>
</tr>
<?php $i++; ?>
@endforeach
</tbody>
</table>
    <div class="clearfix"></div>
    <div class="col-md-12 text-right"  style="max-height: 75px; height: 100%;">
    <ul class="reorder_level_pages pagination purple_pagination" style="text-align:right !important;">
                {!! $paginator->render() !!}
    </ul>
</div>
</div>

@endif
@if(isset($grn_detail_data) && !empty($grn_detail_data))
<tr class="<?= $fetch_with_grn_detail_id ?>">
    <td colspan="3">
<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="grndata" style="border: 1px solid #CCC;">
    <thead>
    <tr class="table_header_bg ">
            <th width="3%">
               <input type="checkbox" class="chk_all_grn_items" onclick="checkAllGrnDtls(this,'<?= $fetch_with_grn_detail_id ?>');" name="chk_all_grn_items">
        </th>
       <th width="5%">Item</th>
       <th width="5%">Store Stock</th>
       <th width="10%">Dept. Stock </th>
       <th width="10%">Grn.Qty </th>
    </tr>
    </thead>
    <tbody>
@foreach($grn_detail_data as $data)
<tr style="cursor: pointer;" class="">
    <td width="3%" style="text-align: left;">
        <input type="checkbox" class="chk_all_grn_items_dtls_<?= $fetch_with_grn_detail_id ?>" onclick="list_grn_level_items(this,'{{$data->batch}}','{{$data->item_code}}',('{{base64_encode($data->item_desc)}}'),'{{$data->r_ststock}}','{{$data->r_depstock}}',
               '{{$data->r_globalstock}}','{{$data->grn_unit}}','{{$data->uom_name}}','{{$data->last_requested_qty}}','{{$data->r_grn_qty}}','{{$fetch_with_grn_detail_id}}');">
    </td>
    <td width="5%"  class="common_td_rules"> {{$data->item_desc}} </td>
    <td width="5%"  class="td_common_numeric_rules"> {{$data->r_ststock}} </td>
    <td width="10%" class="td_common_numeric_rules"> {{$data->r_depstock}} </td>
    <td width="10%" class="td_common_numeric_rules"> {{$data->r_grn_qty}} </td>
</tr>
<?php $i++; ?>
@endforeach
    </tbody>
</table>
    </td>
</tr>
@endif

