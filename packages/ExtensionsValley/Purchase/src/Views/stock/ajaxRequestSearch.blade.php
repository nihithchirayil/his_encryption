<?php
$date_now = date("d-M-Y");
if(isset($itemCodeDetails)){ ?>
<div class="medicine-list-div-row-listing" style="display:block">
    <div class="wrapper theadscroll" style="position: relative; height: 200px !important;">
        <table class="table theadfix_wrapper table-border-radius-top table-striped table_sm">
            <thead>
                <tr class="table_header_bg" style="background-color:#21a4f1 !important">
                    <th width="70%" style="text-align:center;border-left: solid 1px #bbd2bd !important;">Item</th>
                    <th width="15%" style="text-align:center;border-left: solid 1px #bbd2bd !important;">Dpt Stock
                    </th>
                    <th width="15%" style="text-align:center;border-left: solid 1px #bbd2bd !important;">Store Stock
                    </th>
                </tr>
            </thead>
            <tbody class="list-medicine-search-data-row-listing">
                <?php if (!empty($itemCodeDetails)) { $m=1;
                    foreach ($itemCodeDetails as $item) {
                        if($m==1){
                            $act_class = "active_row_item";
                        }else{
                            $act_class = "";
                        }
                        $location_stk = @$item->stk ? $item->stk :0;
                        $store_stk = @$item->store_stk ? $item->store_stk :0; ?>
                <tr onclick='fillItemValues(this,"{{ htmlentities($item->item_code) }}","{{ htmlentities($item->item_desc) }}","{{ htmlentities($item->uom_id) }}","{{ htmlentities($item->uom_name) }}","{{ htmlentities($item_code_id) }}","{{ htmlentities($item->decimal_qty_ind) }}")'
                    class="list_hover {{ $act_class }}">
                    <td style="border-left: solid 1px #bbd2bd !important;">{!! $item->item_desc !!} </td>
                    <td style=" text-align:right;border-left: solid 1px #bbd2bd !important;">{!! $location_stk !!}
                    </td>
                    <td style=" text-align:right;border-left: solid 1px #bbd2bd !important;">{{ $store_stk }}</td>
                    <?php
                   $m++; }
                } else {  ?>
                <tr onclick='fillNoItem(this);' style="display: block; padding:5px 10px 5px 5px; ">
                    <td colspan="4"><?php
                    echo 'No Result Found'; ?></td>
                    </li> <?php
                } ?>
            </tbody>
        </table>
    </div>
</div>
<?php }
if (isset($ledger_name)) {
    if (!empty($ledger_name)) {
        foreach ($ledger_name as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillLedgerValues("{{ htmlentities($item->id) }}","{{ htmlentities($item->ledger_name) }}","{{ $search_key }}","{{ $type }}")'>
    {!! $item->ledger_name, ' - ', $item->ledger_group_name !!}
    <?php //echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]';
    ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($dept_name)) {
    if (!empty($dept_name)) {
        foreach ($dept_name as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillLedgerValues("{{ htmlentities($item->id) }}","{{ htmlentities($item->dept_name) }}","{{ $search_key }}","{{ $type }}")'>
    {!! $item->dept_name !!}
    <?php //echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]';
    ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($speciality)) {
    if (!empty($speciality)) {
        foreach ($speciality as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillLedgerValues("{{ htmlentities($item->id) }}","{{ htmlentities($item->speciality) }}","{{ $search_key }}","{{ $type }}")'>
    {!! $item->speciality !!}
    <?php //echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]';
    ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($grnitemCodeDetails)) {
    if (!empty($grnitemCodeDetails)) {
        foreach ($grnitemCodeDetails as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillItemValues(this,"{{ htmlentities($item->item_code) }}","{{ htmlentities($item->item_desc) }}","{{ htmlentities($item->item_id) }}","{{ htmlentities($item->uom_name) }}","{{ htmlentities($item->uom_id) }}","{{ htmlentities($item->conv_factor) }}","{{ $row_id }}")'>
    {!! $item->item_desc !!}
    <?php //echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]';
    ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($email_users)) {
    if (!empty($email_users)) {
        foreach ($email_users as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillEmailUsers("{{ htmlentities($item->name) }}","{{ htmlentities($item->email) }}","{{ htmlentities($email_count) }}")'>
    {!! $item->name !!}
</li>
<?php
        }
    } else {
        echo 0;
    }
}
if (isset($po_number_search)) {
    if (!empty($po_number_search)) {
        foreach ($po_number_search as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillPoNumber("{{ htmlentities($item->po_id) }}","{{ htmlentities($item->po_no) }}")'>
    {!! strtoupper($item->po_no) !!}
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($grn_vendor_name_details)) {
    if (!empty($grn_vendor_name_details)) {
        foreach ($grn_vendor_name_details as $vendor) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemValues1(this,"{{ htmlentities($vendor->ven_id) }}","{{ htmlentities($vendor->vendor_code) }}","{{ htmlentities($vendor->gst_vendor_code) }}","{{ htmlentities($vendor->vendor_name) }}","{{ htmlentities($vendor->email) }}","{{ $row_id }}")'>
    {!! $vendor->vendor_name !!}
    <?php echo '     [   ' . htmlentities($vendor->gst_vendor_code) . '   ]'; ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}

if (isset($kenya_vendor_name_details)) {
    if (!empty($kenya_vendor_name_details)) {
        foreach ($kenya_vendor_name_details as $vendor) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemValues1(this,"{{ htmlentities($vendor->ven_id) }}","{{ htmlentities($vendor->vendor_code) }}","{{ htmlentities($vendor->gst_vendor_code) }}","{{ htmlentities($vendor->vendor_name) }}","{{ htmlentities($vendor->email) }}","{{ $row_id }}")'>
    {!! $vendor->vendor_name !!}
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}

if (isset($productDataSearch)) {
    if (!empty($productDataSearch)) {
        foreach ($productDataSearch as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px;"
    onclick='fillitem_desc("{{ htmlentities($item->item_id) }}","{{ htmlentities($item->item_desc) }}")'>
    {!! htmlentities($item->item_desc) !!}
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($grn_manufacturer_name_details)) {
    if (!empty($grn_manufacturer_name_details)) {
        foreach ($grn_manufacturer_name_details as $manufacturer) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillManufacturerValues(this,"{{ htmlentities($manufacturer->manufacturer_id) }}","{{ htmlentities($manufacturer->manufacturer_code) }}","{{ htmlentities($manufacturer->manufacturer_name) }}")'>
    {!! $manufacturer->manufacturer_name !!}
    <?php echo '     [   ' . htmlentities($manufacturer->manufacturer_code) . '   ]'; ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}

$i = 1;
?>
@if (isset($stockItemDetails) && !empty($stockItemDetails))
    @foreach ($stockItemDetails as $data)
        <?php
        $reqQty = $data->requested_qty;
        if (isset($data->head_status) && $data->head_status == 4) {
            $reqQty = $data->approved_qty;
        } elseif (isset($data->head_status) && $data->head_status == 3) {
            $reqQty = $data->verified_qty;
        }
        ?>

        <tr id="{{ $data->id }}">

            <td style="width: 2%;">
                <input type="hidden" id="save_update_{{ $i }}" name="save_update[]" value="1">
                <input name="del[]" class="deleteID" type="checkbox" value="{{ $data->id }}">
                <input type="hidden" id="save_update_id_{{ $i }}" name="save_update_id[]"
                    value="{{ $data->id }}">
            </td>
            <td style="width: 20%;">
                <input type='text' required autocomplete='off' value="{{ $data->item_desc }}"
                    id="field_item_code-{{ $i }}" readonly name='item_desc[]'
                    onkeyup='searchItemCode(this.id,event)' class='form-control item_desc'
                    placeholder='Search Item Name'>
                <div class='ajaxSearchBox'
                    style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;width: 34%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
                </div>
                <input type='hidden' name='item_code_hidden[]' class="item_code_hidden" value="{{ $data->item_code }}"
                    id="item_code_hidden{{ $i }}">
                <input type='hidden' name='decimal_qty_ind_hidden[]' class='decimal_qty_ind_hidden' value="{{ $data->decimal_qty_ind }}">
            </td>
            <td>
                <button type='button' class='btn btn-info bttn_cls'
                    style='cursor:pointer; padding: 0px 2px;font-size: 13px;' onclick='showItemReqHistory(this)'><i
                        class='fa fa-info'></i></button>
                <div class='bttn_spin hide'><i class='fa fa-spinner fa-spin'></i></div>
            </td>
            <td>
                <input type='checkbox' name='emergency[]' class="field_emergency"
                    @if ($data->is_urgent == 1) checked value='1' @else value='0' @endif
                    id="field_emergency-{{ $i }}">
                <input type='hidden' name='emerg[]' value="{{ $data->is_urgent }}" id="emerg-{{ $i }}">
            </td>
            <td class="common_td_rules">
                <label for='uom[]' id="field_uom-{{ $i }}">{{ $data->uom_name }}</label>
                <input type='hidden' name='uom[]' value="{{ $data->uom_id }}" id="field_uom-{{ $i }}">
            </td>
            <td class="td_common_numeric_rules">
                <label for='stock[]' id="field_stock-{{ $i }}">{{ $data->r_depstock }}</label>
            </td>
            @if ($disable_show_stock_config == 0)
                <td class="td_common_numeric_rules">
                    <label for='st_stock[]'>{{ $data->r_ststock }}</label>
                </td>
                <td class="td_common_numeric_rules">
                    <label for='global_stock[]'>{{ $data->r_globalstock }}</label>
                </td>
            @endif
            <td class="common_td_rules">
                @if ($date_now == $data->last_app_date)
                    <label for='last_app_date[]' style="background-color: orange;">{{ $data->last_app_date }}</label>
                @else
                    <label for='last_app_date[]'>{{ $data->last_app_date }}</label>
                @endif

            </td>
            <td class="td_common_numeric_rules">
                <label for='lastRQty[]'>{{ $data->last_requested_qty }}</label>
            </td>
            @if ($data->head_status == 3 || $data->head_status == 4)
                <td class="td_common_numeric_rules">
                    <label for='ver_qty[]'>{{ $data->verified_qty }}</label>
                </td>
                <td class="td_common_numeric_rules">
                    <label class="wk_sub_qty" for='wk_sub_qty[]'>{{ $data->requested_qty }}</label>
                </td>
            @elseif($data->head_status == 1 || $data->head_status == 2)
                <td class="td_common_numeric_rules">
                    <label class="wk_sub_qty" for='wk_sub_qty[]'>{{ $data->requested_qty }}</label>
                </td>
            @endif
            <td>
                <input type='text' class='form-control request_req_qty' required autocomplete='off'
                    name='req_qty[]' value="{{ $reqQty }}" id="field_req_qty-{{ $i }}">
            </td>
            <?php if(isset($data->head_status) && ($data->head_status < 4)){ ?>
            <td>
                <i style='padding: 5px 8px; font-size: 15px;cursor: pointer;' class='fa fa-trash text-red deleteRow'
                    onclick='deleteEditRow(this,"{{ $data->id }}");'></i>
            </td>
            <?php } ?>

        </tr> <?php $i++; ?>
    @endforeach
@endif
<?php $i = 1; ?>
@if (isset($pendingItemDetails) && !empty($pendingItemDetails))
    @foreach ($pendingItemDetails as $data)
        <tr>
            <td width="3%" style="text-align: left;">{{ $i }}</td>
            <td width="5%" style="text-align: left;"> {{ $data->r_request_no }} </td>
            <td width="5%" style="text-align: left;"> {{ $data->r_item_code }} </td>
            <td width="15%" style="text-align: left;"> {{ $data->r_item_desc }} </td>
            <td width="3%" style="text-align: left;"> {{ $data->r_issued_qty }} </td>
            <td width="3%" style="text-align: left;"> {{ $data->r_approved_qty }} </td>
            <td width="3%" style="text-align: left;" style="text-align: left;"> {{ $data->r_verified_qty }}
            </td>
            <td width="3%" style="text-align: left;" style="text-align: left;"> {{ $data->r_requested_qty }}
            </td>

        </tr> <?php $i++; ?>
    @endforeach
@endif


<?php $i = 1; ?>
@if (isset($reorderLevelItemsList) && !empty($reorderLevelItemsList))
    <div class="theadscroll">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
            id="grndata" style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg ">
                    <th width="3%"><input type="checkbox" id="select_all_item" class=""><label
                            for="select_all_item">All</label></th>
                    <th width="5%">Item</th>
                    <th width="5%">Store Stock</th>
                    <th width="15%">Dept. Stock </th>
                    <th width="10%">Re-order Level Qty</th>
                    <th width="10%">Ordering Qty</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($reorderLevelItemsList as $data)
                    <tr style="cursor: pointer;">
                        <td width="3%" style="text-align: left;">
                            <input type="checkbox" class="chk_reorder_items selected_item{{ $data->item_code }}"
                                onclick="list_reorder_level_items(this,'{{ $data->item_code }}','{{ $data->item_desc }}','{{ $data->r_ststock }}','{{ $data->r_depstock }}','{{ $data->r_globalstock }}','{{ $data->uom_id }}','{{ $data->uom_name }}','{{ $data->last_requested_qty }}','{{ $data->ordering_qty }}','{{ $data->last_app_date }}');">
                        </td>
                        <td width="5%" style="text-align: left;" class="common_td_rules"> {{ $data->item_desc }}
                        </td>
                        <td width="5%" style="text-align: left;" class="common_td_rules"> {{ $data->r_ststock }}
                        </td>
                        <td width="15%" style="text-align: left;" class="common_td_rules">
                            {{ $data->r_depstock }}
                        </td>
                        <td width="15%" style="text-align: left;" class="common_td_rules">
                            {{ $data->reorder_level }}
                        </td>
                        <td width="15%" style="text-align: left;" class="common_td_rules">
                            {{ $data->ordering_qty }}
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix"></div>
        <div class="col-md-12 text-right" style="max-height: 75px; height: 100%;">
            <ul class="reorder_level_pages pagination purple_pagination" style="text-align:right !important;">
                {!! $paginator->render() !!}
            </ul>
        </div>
    </div>

@endif

<?php $i = 1; ?>
@if (isset($prevReqItems) && !empty($prevReqItems))
    <div class="theadscroll">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
            id="grndata" style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg ">
                    <th width="3%">#</th>
                    <th width="5%">Item</th>
                    <th width="5%" title="Previously Requested Quantity">Prev.Req.Qty</th>
                    <th width="5%" title="Previously Approved Quantity">Prev.Apvd.Qty </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($prevReqItems as $data)
                    <tr style="cursor: pointer;">
                        <td width="3%" style="text-align: left;">
                            <input type="checkbox" class="chk_reorder_items"
                                onclick="list_prev_req_items(this,'{{ $data->item_code }}','{{ $data->item_desc }}','{{ $data->r_ststock }}','{{ $data->r_depstock }}','{{ $data->r_globalstock }}','{{ $data->uom_id }}','{{ $data->uom_name }}','{{ $data->last_requested_qty }}','{{ $data->last_app_date }}');">
                        </td>
                        <td width="5%" style="text-align: left;" class="common_td_rules">
                            {{ $data->item_desc }}
                        </td>
                        <td width="5%" style="text-align: left;" class="common_td_rules">
                            {{ $data->r_ststock }}
                        </td>
                        <td width="15%" style="text-align: left;" class="common_td_rules">
                            {{ $data->r_depstock }}
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix"></div>
        <div class="col-md-12 text-right" style="padding-top:10px">
            <ul class="reorder_level_pages pagination purple_pagination" style="text-align:right !important;">
                {!! $paginator->render() !!}
            </ul>
        </div>
    </div>

@endif
