@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: hidden !important;
        width: 34%;
        z-index: 99999;
        position: relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);
        top:1px !important;

    }
    .search_box_bottom{
        bottom: -30px;
    }
    .ajax_search_box_bottomcls .ajaxSearchBox{
        bottom: 3px;
        height: 200px !important;
    }
    .ajax_search_box_bottomcls_reduce_box{
        height: 200px !important;
    }
    table td{
        position: relative !important;
    }


    /* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1;
}

/* Handle */
::-webkit-scrollbar-thumb {
  background: #888;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555;
}
.medicine-list-div-row-listing {
    z-index: 99999;
    background: #FFF;
    box-shadow: 0 0 6px #ccc;
    padding: 1px;
}
.active_row_item{
    background-color: #ccebbc !important;
    cursor: pointer;
}
</style>
@endsection
@section('content-area')
     <!-- page content -->
     <div class="right_col"  role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
                 <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                {!!Form::open(array('route' => 'extensionsvalley.purchase.updateStockRequests', 'method' => 'post', 'class' => 'formShortcuts', 'id' => 'requisitionForm'))!!}
                <div class="col-md-12 no-padding">
                <div class="box no-border no-margin" >
                    <div class="box-body clearfix">

                           <div class="col-md-2 padding_sm">
                               @if(isset($item[0]) && $item[0] !='')
                               <div class="mate-input-box">
                                <label for="">Request No.</label>
                                <div class="clearfix"></div>
                                <span>{{isset($item[0]->request_no) ? $item[0]->request_no : ''}}</span>
                                <input type="hidden" class="form-control" name="request_no" id="request_no" value="{{isset($item[0]->request_no) ? $item[0]->request_no : ''}}" readonly="">
                                 <input type="hidden" class="form-control" name="request_stock_head_id" id="request_stock_head_id" value="{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}" readonly="">
                             </div>
                            @endif
                           </div>


                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Department</label>
                                <div class="clearfix"></div>
                                {!! Form::select('from_location', array("-1"=> " Department") + $location->toArray(),isset($item[0]->from_location_code) ? $item[0]->from_location_code :$default_location, [
                            'class'       => 'form-control from_location select2',
                            'id'    => 'from_location',
                            'onchange' => 'changeLocationCheck(this)'
                        ]) !!}
                    </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box" id="append_to_loc">
                                <label for="">Store</label>
                                <div class="clearfix"></div>
                                <?php  $dfltstore = isset($item[0]->to_location_code) ? $item[0]->to_location_code : $default_store;
                                ?>
                                <select class="form-control to_location select2" id="to_location" name="to_location" >
                                    <?php
                                    foreach ($to_location as $key=>$val) {
                                    $seleted='';
                                        if($key==$dfltstore){
                                            $seleted="selected=''";
                                        }
                                    ?>
                                    <option <?=$seleted?> value="<?=$key?>"><?=$val?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <input type="hidden" value="{{$dfltstore}}" id="dflt_ret_store_chk" >
                            <input type="hidden" value="" id="purchase_type">
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                     <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                    <textarea id="requisition_comments" class="form-control" wrap="off" cols="35" rows="1" name="remarks" >{{isset($item[0]->comments) ? $item[0]->comments :''}}</textarea>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
             <?php
                $date_now = date("d-M-Y");
                $status_value = '';
                $worksheet = '';
                $submit = '';
                $verify = '';
                $approve = '';
                $disable_prop = '';
                $status_value = isset($item[0]->head_status) ? $item[0]->head_status : '';
                if($status_value >= 4){
                    $status_value = 4;
                }
                foreach ($access_control as $key => $value) {
                    foreach ($value as $k => $v) {
                        if(($k == 'worksheet') && ($v == 1)){$worksheet = '1';
                        }if(($k == 'submit') && ($v == 1)){ $submit = '1';
                        }if(($k == 'verify') && ($v == 1)){  $verify = '1';
                        }if(($k == 'approve') && ($v == 1)){  $approve = '1';
                        }
                    }
                }
                if($status_value == 4){
                $disable_prop = 'display:none';
                }
                $prev_req_gen_str_loc = isset($item[0]->to_location_code) ? $item[0]->to_location_code :$default_store;
                if($prev_req_gen_str_loc == 'LGST001'){
                    $disable_prop_str = '';
                }else{
                    $disable_prop_str = 'display:none';
                }
            ?>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm text-right">
                    @if(isset($item[0]->request_no))
                    <div class="btn light_purple_bg pendrequestbtn" style="cursor: pointer;" onclick="pendrequest();">
                        Pending Request
                    </div>
                        @if(($status_value == 4))
                            <div class="btn btn-dark" style="cursor: pointer;" onclick="printData('{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}');">
                            <i class="fa fa-print" aria-hidden="true"></i> Print</div>
                        @endif
                    @endif
                    <div class="col-md-6 padding_sm">
                        <label>Search By : </label>&nbsp;&nbsp;&nbsp;
                        <input type="radio" checked="checked" class="" value="1" id="search_items_by_starts_with" name="search_items_by">
                        <label for="search_items_by_starts_with" >Starts with</label>&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="search_items_by_contains" class="" value="2" name="search_items_by">
                        <label for="search_items_by_contains" >Contains</label>
                    </div>


                    <div class="btn btn-info" style="cursor: pointer;<?= $disable_prop ?>" onclick="reorderLevelItems(0);">
                        Re-order Level Items
                    </div>
                    <div class="btn btn-primary" title="Previously Requested Items" id="prev_req_items_div" style="cursor: pointer;<?= $disable_prop_str ?>" onclick="prevReqItems(0);">
                        Prev.Req.Items
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                <div class="" style="position: relative; min-height: 400px;overflow:scroll">
                <table class="table no-margin table-striped no-border table-condensed styled-table" id="addNewItem" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg " style="cursor: pointer;">
                               <th width="2%">#</th>
                               <th width="22%">
                                    <input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Item Search.. " style="color: #000;display: none;width: 90%;" >
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Item Name</span>
                                </th>
                                <th width="1%"></th>
                               <th width="2%"><i class="fa fa-exclamation-triangle text-red"></i></th>
                               <th width="5%">UOM</th>
                               <th width="3%">Stock</th>
                               @if($disable_show_stock_config == 0)
                               <th width="3%">S.Stock</th>
                               <th width="3%">G.Stock</th>
                               @endif
                               <th width="3%" title="Last Approved Date">Lt.App.Dt</th>
                               <th width="3%" title="Last Approved Quantity">Lt.App.Qty</th>
                            @if (!empty($item) && (isset($item[0])))
                               @if(($item[0]->head_status == 3) || ($item[0]->head_status == 4))
                               <th width="3%" title="Verified Quantity">Vd.Qty</th>
                               <th width="3%" title="Last Requested Quantity">Lt.Req.Qty</th>
                               @elseif(($item[0]->head_status == 1) || ($item[0]->head_status == 2))
                               <th width="3%" title="Last Requested Quantity">Lt.Req.Qty</th>
                               @endif
                            @endif
                               <th width="3%" title="Requested Quantity">Req.Qty</th>
                               <th width="3%"><button type="button" tabindex="-1" id="add"  class="btn btn-success add_row_btn" onclick="addNewRowByDefault(0)"><i class="fa fa-plus"></i></button></th>
                           </tr>

                       </thead>
                       <tbody id="fillrequestitemdetails">

                       </tbody>

            </table>
                </div>
            </div>

                 <div class="clearfix"></div>
                    <div class="ht5"></div>

                <div class="col-md-12">
                <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">

                         @if(isset($status_value) && $status_value != 4)
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="saveItem(event);" class="btn btn-block light_purple_bg saveitembtn" style="<?= $disable_prop ?>"><i id="saveitembtn_spin" class="fa fa-save"></i>
                                    Save</button>
                </div>
                         @endif
                <div class="col-md-2 padding_sm" style="float: right !important;">
                    <div class="mate-input-box">
                                <label for="">Select Status</label>
                                <div class="clearfix"></div>
                                <select name="status" id="status" class="form-control status chk_status_head">

                               @if($worksheet == 1)
                               <option @if($status_value == 1) selected @endif value="1">Worksheet</option>@endif
                               @if($submit == 1)
                               <option @if($status_value == 2) selected @endif value="2">Submitted</option>@endif
                               @if($verify == 1)
                               <option @if($status_value == 3) selected @endif value="3">Verified</option>@endif
                               @if($approve == 1)
                               <option @if($status_value == 4) selected @endif value="4">Approved</option>@endif
                               </select>
                           </div>
                </div>
                <!-- <div class="col-md-2 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div data-toggle="modal" data-target="#raiseNewItemRequest" class="btn btn-block light_purple_bg raiserequestbtn"><i class="fa fa-plus"></i>
                                    Add New Request</div>
                </div>-->
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                               <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                </div>
            </div>
                </div>
            </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Modal -->
<div id="prev_req_items_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List of Previously Requested Items</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-3 padding_sm">
                            <label for="">Item Name or Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="prev_req_item_search" id="prev_req_item_search">
                        </div>
                        <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="prevReqItems(0);" class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button onclick="clearprevReqItemsSearch();" style="margin: 0 5px 0 0;" class="btn btn-block btn-warning">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="box no-border no-margin">
                    <div class="box-body clearfix" style="position: relative; height: 350px;" id="fillPrevReqlist">


                    </div>
                    </div>
                </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>
</div>
<!-- Modal -->
<div id="reorder_level_items_modal" class="modal fade" role="dialog">
    <!-- <div class="modal-dialog modal-lg"> -->
        <div class="modal-dialog" style="max-width: 1150px; width: 100%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List of Re-order Level Reached Items</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Item Name or Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="reorder_item_search" id="reorder_item_search">
                        </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                                <label for="" style="font-size: 12px; font-weight: 700;">Show Zero Stock</label>
                                <input type="checkbox" name="chk_zero_stock"  id="chk_zero_stock" >
                        </div>
                        <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="reorderLevelItems(0);" class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button onclick="clearReorderLevelSearch();" style="margin: 0 5px 0 0;" class="btn btn-block btn-warning">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="box no-border no-margin">
                    <div class="box-body clearfix" style="position: relative; height: 550px;" id="fillreorderLevellist">


                    </div>
                    </div>
                </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>
</div>
<!-- Modal -->
<div id="item_hist_req_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title item_history_title">
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="theadscroll always-visible">
                             <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed" style="border: 1px solid #CCC;">
                            <thead>
                            <tr class="table_header_bg" style="cursor: pointer;">
                                    <th>Request No</th>
                                    <th>Requested Date</th>
                                    <th>Issued By</th>
                                    <th>Received By</th>
                                    <th>Received Date</th>
                                    <th>Issued Status</th>
                                    <th>Issued Qty</th>
                                </tr>
                       </thead>
                        <tbody id="item_req_history_div">

                        </tbody>

                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i class="fa fa-thumbs-up"> OK</i></button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="pend_req_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pending Request Item Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="theadscroll always-visible">
                             <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed" id="penddata" style="border: 1px solid #CCC;">
                            <thead>
                            <tr class="table_header_bg ">
                               <th width="3%">#</th>
                               <th width="5%">Request No.</th>
                               <th width="5%">Item Code</th>
                               <th width="15%">Item </th>
                               <th width="3%">Issued Qty</th>
                               <th width="3%">Approved Qty</th>
                               <th width="3%">Verified Qty</th>
                               <th width="3%">Requested Qty</th>
                           </tr>

                       </thead>
                        <tbody id="fillpendingitemdetails">

                        </tbody>

                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg">OK</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="raiseNewItemRequest" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Item Request</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Item Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="itm_name" id="itm_name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Quantity</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="itm_qty" id="itm_qty">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Description</label>
                                <div class="clearfix"></div>
                                <textarea cols="25" rows="4" style="width: 548px; height: 91px;" name="itm_desc" id="itm_desc">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;"  onclick="raiseRequestNewReset();"   class="btn light_purple_bg">Clear</button>
                <button style="margin: 0 5px 0 0;"  onclick="raiseRequestNewItem();" class="btn light_purple_bg">Save</button>
            </div>
        </div>

    </div>
</div>
<div id="prrint_div" style="display:none"></div>
<script type="text/javascript">
</script>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    var j=1;
    var dynamic_search_item_code = '';
    var previous;
(function () {
$("#from_location").on('focus', function () {
    previous = this.value;
}).change(function() {
    if(previous == '-1' || previous == -1){
        setTimeout(function(){
         addNewRowByDefault(0);
     },300);
    }
});
})();

$('body').on('click', '.reorder_level_pages a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var page = GetUrlParameter(url,'page');
            reorderLevelItems(page);
});

$(document).ready(function() {
    $('body').on('keydown', '.request_req_qty', function(e) {
        var rowCounts = $('#addNewItem tr').length;
        var th = $(this).closest("tr").find("input[name='item_desc[]']").attr('id');;
        var ths = th.split('_');
        var rowCounts =  ths[2];
        var rowCountss = parseFloat(rowCounts)+1;
    if (e.which == 9) {  setTimeout(function(){
    $('#item_code_'+rowCountss).focus();
            },300);
    }
});

    $("#item_search_btn").click(function(){
      $("#issue_search_box").toggle();
      $("#item_search_btn_text").toggle();
    });

    $("#to_location").change(function(){
        if($(this).val() == 'LGST001'){
            $("#prev_req_items_div").show();
        }else{
            $("#prev_req_items_div").hide();
        }
    });

    @if(isset($item[0]->stock_id))
    $("#from_location").trigger('change');
        getStockRecords("{{$item[0]->stock_id}}");
    @else
     @if(isset($status_value) && $status_value != 4)
    addNewRowByDefault(0);
    @endif
    setTimeout(function(){
    document.getElementById("item_code_1").focus();
    },2000);
    @endif

    @if(isset($item[0]->head_status) && ($item[0]->head_status == 3))
    $('.lb_ver_qty').show();
    $('.ver_qty').show();
    @endif

    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
    });

     $(document).on('click', '.field_emergency', function(e) {
                            // console.log($(this).prop("checked"));
                        if($(this).prop("checked") == true) {
                            $(this).closest("tr").find("input[name='emerg[]']").val('1');
                        }else{
                            $(this).closest("tr").find("input[name='emerg[]']").val('0');
                        };

        });
    $(document).on('keyup', '.request_req_qty', function(e) {
    var decimal_qty_ind_hidden = $(this).closest("tr").find("input[name='decimal_qty_ind_hidden[]']").val();
    var location_type = $("#purchase_type").val();
    var $input = $(this);
    if(decimal_qty_ind_hidden == 1){
    var $input = $(this);
    var value = $input.val();
    // Allow only numbers and a single decimal point
    value = value.replace(/[^0-9.]/g, '');
    // Ensure there's only one decimal point
    var parts = value.split('.');
    if (parts.length > 2) {
      value = parts[0] + '.' + parts[1];
    }
    $input.val(value);
    }else{
        $input.val($input.val().replace(/[^0-9]/g, ''));
    }

  });
});
$(document).on('keydown', function (event) {
if ($('.medicine-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-medicine-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                console.log("ffffffffff");
                if ($(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':first-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr').last().addClass('active_row_item');
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').prev().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').last().removeClass('active_row_item');
                }
            } else if (event.which == 40) { // down

                if ($(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':last-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr').first().addClass('active_row_item');
                    console.log("ppppp");
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').next().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').first().removeClass('active_row_item');
                    console.log("vvvv");
                }
            } else if (event.which == 13) { // enter
                event.preventDefault();
                $(".list-medicine-search-data-row-listing").find('tr.active_row_item').trigger('click');
            }
        }
    }
});
/**
 *
 * Java script search in the search item box
 */
 function addNewRowByDefault(is_val = 0){

            if(document.getElementById('from_location').value == "-1")
            {
                 alert('Please Enter From Location ..! ');return false;
            } else if(document.getElementById('to_location').value == "-1")
            {
                 alert('Please Enter To Location ..! ');return false;
            } else if(document.getElementById('to_location').value == document.getElementById('from_location').value)
            {
                alert('Error Please select location properly..!');return false;
            }

            var table = document.getElementById("addNewItem");
            var newrow = table.rows.length;
            var row = table.insertRow(newrow);
            row.id = "newRow" + newrow;
            row.classList.add('removeTr');
            <?php  $a = 1; $b = 0; ?>
             <?php   foreach($incr_array as $inc) { ?>//

                    var cell_no{{$a}} = row.insertCell("{{$b}}");

                    cell_no{{$a}}.innerHTML = "<?php  echo $inc; ?>";
            <?php
                    $a++; $b++;
                }

            ?>
            cell_no2.firstChild.id = 'item_code_'+j;
            cell_no4.firstChild.id = 'fld_emergency-'+j;
            var text_input_id = cell_no2.firstChild.id;
            document.getElementById(text_input_id).nextSibling.id = 'ajaxSearchBox_'+j;
            var text_input_id1 = cell_no4.firstChild.id;
            document.getElementById(text_input_id1).nextSibling.id = 'emg-'+j;
            if(j > 2){
                $('#ajaxSearchBox_'+j).addClass('ajax_search_box_bottomcls_reduce_box');

            }
            if(j > 6){
                $('#newRow'+j).addClass('ajax_search_box_bottomcls');

            }
            $('.cmm_stl_txt').closest('td').addClass('common_td_rules');
            $('.cmm_stl').closest('td').addClass('td_common_numeric_rules');
            j++;
            if(is_val == 1){
                return text_input_id;
            }
 }
function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("addNewItem");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


function searchItemCode(id,event) {
        var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var search_type = $('input[name="search_items_by"]:checked').val();
    var current;
        var div_id = $('#'+id).next().attr('id');
        if (event.keyCode == 13) {
        ajaxlistenter(div_id);
        return false;
        } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

            var from_location = $('#from_location').val();
            var to_location = $('#to_location').val();
            var item_code = $('#'+id).val();
            if (item_code == "") {
                $("#"+div_id).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxNewStockItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_code=' +item_code+'&from_location='+from_location+'&to_location='+to_location+'&search_type='+search_type+'&item_code_id='+id,
                    beforeSend: function () {
                    $("#"+div_id).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+div_id).html(html).show();
                        $("#"+div_id).find('li').first().addClass('liHover');
                        $('.theadscroll').perfectScrollbar({
                                    minScrollbarLength: 30
                                });
                                var $table = $('table.theadfix_wrapper');
                                $table.floatThead({
                                    scrollContainer: function($table){
                                        return $table.closest('.theadscroll');
                                    }
                                });
//                        $("#item_code_1").focus();
                    },
                    complete: function () {

//                        document.getElementById("item_code_1").focus();
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(div_id, event);
            }
}
function fillNoItem(list){
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#'+itemCodeListDivId).hide();
}

function fillItemValues(list,item_code,item_desc,uom_id,uom,item_code_id,decimal_qty_ind) {
    var item_code_exts = 0;
   $('#addNewItem tr').each(function () {
           item_code_temp = $(this).find(".item_code_hidden").val();
           if(item_code == item_code_temp){
               item_code_exts = 1;
           }
   });
   if(item_code_exts == 1){
       toastr.error("Selected Item Is Already Existing!");
       return false;
   }
    var url = "{{route('extensionsvalley.purchase.addAjaxNewStockItem')}}";
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    $.ajax({
    type: "GET",
        url: url,
        data: 'code=' +item_code+'&location='+from_location+'&store_location='+to_location+'&fillStockFunctions=3',
        beforeSend: function () {
        },
        success: function (data) {
        if(data){
            if(data.block_general_store_request == '1'){
                alert('Please recieve already issued item ! ');return false;
            }
            if(data.receiv_html != 'NA'){
                alert(data.receiv_html);
            }
            if(data.get_lastreq_det == "no item found"){
                 fillRequestItemDetails(list,item_code,item_desc,uom_id,uom,data.r_depstock,data.r_ststock,data.r_globalstock,data.last_requested_qty,data.last_apv_date,item_code_id,decimal_qty_ind);
            }else{
                if (confirm( data.get_lastreq_det  )) {
                    fillRequestItemDetails(list,item_code,item_desc,uom_id,uom,data.r_depstock,data.r_ststock,data.r_globalstock,data.last_requested_qty,data.last_apv_date,item_code_id,decimal_qty_ind);
                }else{
                        return;
                }
            }
        }else{
           alert('Please Enter Valid Item..! ');return false;
        }
        },
        complete: function () {
        }
    });
}

function fillRequestItemDetails(list,item_code,item_desc,uom_id,uom,dep_stock,st_stock,global_stock,lastRQty,app_date,item_code_id,decimal_qty_ind){
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = item_code_id;//$('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(item_desc);
    $('#'+itemCodeListDivId).hide();
   // alert(itemCodeListDivId+" -- "+itemCodeTextId ); return;

    $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    $('#'+itemCodeTextId).closest("tr").find("input[name='decimal_qty_ind_hidden[]']").val(decimal_qty_ind);
    $('#'+itemCodeTextId).closest("tr").find("label[for='uom[]']").text(uom);
    $('#'+itemCodeTextId).closest("tr").find("input[name='uom[]']").val(uom_id);
    $('#'+itemCodeTextId).closest("tr").find("label[for='stock[]']").text(dep_stock);
    $('#'+itemCodeTextId).closest("tr").find("label[for='st_stock[]']").text(st_stock);
    $('#'+itemCodeTextId).closest("tr").find("label[for='global_stock[]']").text(global_stock);
    $('#'+itemCodeTextId).closest("tr").find("label[for='lastRQty[]']").text(lastRQty);
    $('#'+itemCodeTextId).closest("tr").find("label[for='last_approved_date[]']").text(app_date);
    if('{{$date_now}}' == app_date){
                $('#'+itemCodeTextId).closest("tr").find("label[for='last_approved_date[]']").css( "background-color", "orange" );
    }
    //$(".request_req_qty").focus();
   // $('#'+itemCodeTextId).parents("tr").find("input[name='req_qty[]']")
     $('#'+itemCodeTextId).parents("tr").find("input[name='req_qty[]']").removeAttr('disabled').focus();
          var chk_new_row_create = 0;
    $('#addNewItem tr').each(function () {
        if ($(this).find('input[name="item_desc[]"]').val() ==''){
            chk_new_row_create = 1;
        }
    });
    if(chk_new_row_create == 0){
        addNewRowByDefault(0);
    }
    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
}

function deleteRow(list){
    var item_name = $(list).closest("tr").find(".item_desc").val();
    if(item_name == ""){
        var id = $(list).closest("tr").attr('id');
        $("#"+id+"").remove();
    }else{
        if (confirm('Are you sure wanted to delete?')) {
                var id = $(list).closest("tr").attr('id');
                $("#"+id+"").remove();
        }
    }
}

function deleteEditRow(list,id){
     if (confirm('This item will be deleted immediately , are you sure to delete?')) {
            var url = '';
        $.ajax({
            type: "GET",
            url: url,
            data: 'stockrequestdelID='+id,
            beforeSend: function () {
            },
            success: function (data) {
                if(data==1){
                    var d = $(list).closest("tr").attr('id');
                    $("#"+d+"").remove();
                }
            }
        });

        }
}

function getStockRecords(stock_id){
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'stockItemDetails='+stock_id,
        beforeSend: function () {
        },
        success: function (html) { //alert(html);
        $("#fillrequestitemdetails").html(html);
        @if(isset($status_value) && $status_value != 4)
              addNewRowByDefault(0);
        @endif
    setTimeout(function(){
    document.getElementById("item_code_1").focus();
},2000);
        }
    });
}

function saveItem(e){
    e.preventDefault();
        var flag  = 0, f = 0;
        if($(".chk_status_head").val() == ''){
            alert('Please Select Status ..! ');
                 return false;
        }

        $(".request_req_qty").each(function () {
            var item = $(this).closest("tr").find(".item_desc").val();
            if(item != ""){
                if (isNaN(this.value) || this.value == '' || this.value == 0){
                        f  = 1;
                    }
               }
            if (this.value != '' || this.value != 0){
                 if(item == ""){
                        flag = 1;
                 }
            }
            });
            if(f == 1){
                alert("Request quantity is zero"); return false;
            }

        if(document.getElementById('from_location').value == "-1")
        {
             alert('Please Select Department ..! ');
             return false;

        } else if(document.getElementById('to_location').value == "-1")
        {
             alert('Please Select Store ..! ');return false;
        } else if(document.getElementById('to_location').value == document.getElementById('from_location').value)
        {
            alert('Error Please select location properly..!');return false;
        }

        var table = document.getElementById("addNewItem");
        var count_row = table.rows.length;
        // $(".item_desc" ).each(function() {
        //         if($(this).val() == ""){
        //             flag = 1;
        //         }
        // });
        if(flag == 1 || count_row == 1){
                 alert('Please Enter Item ..! ');
                 return false;
        }else{
                $(".saveitembtn").prop("disabled",true);
                $('#saveitembtn_spin').removeClass('fa fa-save');
                $('#saveitembtn_spin').addClass('fa fa-spinner fa-spin');
                $("#requisitionForm").submit();
        }
}

function raiseRequestNewReset(){
    $("#itm_name").val('');
    $("#itm_qty").val('');
    $("#itm_desc").val('');
}

function raiseRequestNewItem(){
    var url = "{{route('extensionsvalley.purchase.addNewStockItemRequest')}}";
    // window.open(url,'_blank');
    var itm_name = $("#itm_name").val();
    var itm_qty = $("#itm_qty").val();
    var itm_desc = $("#itm_desc").val();
    var from_location = $("#from_location").val();
    if(from_location == "-1"){
                alert('Please Enter From Location ..! ');return false;
    }else if(itm_name == ""){
                alert('Please Enter Item ..! ');return false;
    }else if(itm_qty == ""){
                alert('Please Enter Item Quantity..! ');return false;
    }

    $.ajax({
        type: "GET",
        url: url,
        data: 'itm_name='+itm_name+'&itm_qty='+itm_qty+'&itm_desc='+itm_desc+'&from_location='+from_location,
        beforeSend: function () {
        },
        success: function (data) { //alert(data);
            if(data != 0){
                alert("New Item Requested Successfully..");
            }else{
                alert("New Item Not Requested Successfully..");
            }
        },
        complete: function (){
            $("#itm_name").val('');
            $("#itm_qty").val('');
            $("#itm_desc").val('');
            $("#raiseNewItemRequest").modal('hide');
        }
    });
}

function pendrequest(){
    var fromLocation = $('#from_location').val();
    @if(isset($item[0]->stock_id))
        var req_head_id = "{{$item[0]->stock_id}}";
    @else
        var req_head_id = 0;
    @endif
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'pendingItemDetails='+req_head_id+'&fromLocation='+fromLocation,
        beforeSend: function () {
            $("#fillpendingitemdetails").html('');
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (html) { //alert(html);
            $("#pend_req_modal").modal('show');
            // $(".theadfix_wrapper").floatThead('reflow');

            if(html){
                $("#fillpendingitemdetails").html(html);
            }else{
                $("#fillpendingitemdetails").append("<tr><td colspan='8'>No pending request found!</td></tr>");
            }
        },
        complete: function (){
            $.LoadingOverlay("hide");
            setTimeout(function(){
                $(".theadfix_wrapper").floatThead('reflow');
            },300);
        }
    });
}

function showItemReqHistory(list){
    var from_location = $('#from_location').val();
    var item_desc = $(list).closest("tr").find(".item_desc").val();
    var item_code = $(list).closest("tr").find(".item_code_hidden").val();
    if(from_location == ""){
        toastr.error('Select Department!');
    }else if(item_desc == ""){
        toastr.error('Select Any Item!');
    }else if(item_code == ""){
        toastr.error('Enter Valid Item!');
    }else{
        $.ajax({
            type: "GET",
            url: '',
            data: 'item_req_history=1'+'&item_code='+item_code+'&from_location='+from_location,
            beforeSend: function () {
                $(list).closest("tr").find(".bttn_spin").removeClass('hide');
                $(list).closest("tr").find(".bttn_cls").hide();
            },
            success: function (ret) {
                $(list).closest("tr").find(".bttn_spin").addClass('hide');
                $(list).closest("tr").find(".bttn_cls").show();
                $("#item_req_history_div").html('');
                $("#item_req_history_div").html(ret.html);
                $(".item_history_title").html(ret.item_desc+" - "+item_code);
                if(ret.status == 1)
                    $('#item_hist_req_modal').modal('show');
                else
                    toastr.warning('No history exists for this item!');
            },
            complete: function () {
                 setTimeout(function(){
                        $(".theadfix_wrapper").floatThead('reflow');
                    },300);
                $(list).closest("tr").find(".bttn_cls").removeClass('fa fa-spinner fa-spin');
            }
        });
    }
}

function GetUrlParameter(sPageURL, sParam){
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam){
            return sParameterName[1];
        }
    }
}

function clearReorderLevelSearch(){
    $('#reorder_item_search').val("");
    $('#chk_zero_stock').attr("checked",false);
    reorderLevelItems(0);
}

function reorderLevelItems(page){

  var fromLocation = $('#from_location').val();
  var toLocation = $('#to_location').val();
  var reorder_item_search = $('#reorder_item_search').val();
  if(fromLocation == "-1"){
              alert('Please Select Department ..! ');return false;
  }else if(toLocation == "-1"){
              alert('Please Select Store Name ..! ');return false;
  }
  var chk_zero_stock = 0;
  if($('#chk_zero_stock').prop("checked") == true) {
      chk_zero_stock = 1;
  }else{
      chk_zero_stock = 0;
  }
  var url = '';
  $.ajax({
      type: "GET",
      url: url,
      data: 'reorderLevelItems=1'+'&fromLocation='+fromLocation+'&toLocation='+toLocation+'&reorder_item_search='+reorder_item_search+'&chk_zero_stock='+chk_zero_stock+'&page_value='+page,
      beforeSend: function () {
          $("#fillreorderLevellist").html('');
          $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
      },
      success: function (html) { //alert(html);
          if($('#reorder_level_items_modal').hasClass('show')){
              console.log($('#reorder_level_items_modal').hasClass('show'));
          }else{
            $("#reorder_level_items_modal").modal('show');
          }
          if(html){
              $("#fillreorderLevellist").html(html);
          }else{
              $("#fillreorderLevellist").append("<tr><td colspan='8'>No Re-order Level Reached Items For this Department Found!</td></tr>");
          }
          $('.item_code_hidden').each(function(){
              var set=$(this).val();
              $('.selected_item'+set).prop('checked',true);
          })
          var count=0;
  $('.chk_reorder_items').each(function(){
      if($(this).is(':checked')){
    count +=1;
  }
  })
  console.log(count);
      if(count==30){
          $('#select_all_item').prop('checked',true)
      }
      },
      complete: function (){
          $.LoadingOverlay("hide");
          setTimeout(function(){
              $(".theadfix_wrapper").floatThead('reflow');
          },2000);
      }
  });
}


function list_reorder_level_items(obj,item_code,item_desc,r_ststock,r_depstock,r_globalstock,uom_id,uom_name,last_requested_qty,reorder_level,last_approved_date){

   if($(obj).prop("checked") == true) {
       var item_code_exts = 0;
   $('#addNewItem tr').each(function () {
           item_code_temp = $(this).find(".item_code_hidden").val();
           if(item_code == item_code_temp){
               item_code_exts = 1;
           }
   });
   if(item_code_exts == 1){
       toastr.error("Selected Item Is Already Existing!");
       return false;
   }

       $(".deleteID" ).each(function() {
               var item_val = $(this).closest("tr").find(".item_desc").val();
               if($(this).val() == 0){
                 if(item_val == ""){
                       var tr_id = $(this).closest("tr").attr('id');
                       $("#"+tr_id+"").remove();
                   }
               }
       });
       var input_itm_id = addNewRowByDefault(1);
       $('#'+input_itm_id).val(item_desc);
       $('#'+input_itm_id).closest("tr").addClass('added'+item_code);
       $('#'+input_itm_id).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
       val_id_itm = $('#'+input_itm_id).closest("tr").find("input[name='item_desc[]']").attr('id');
       $('#'+input_itm_id).closest("tr").find("label[for='uom[]']").text(uom_name);
       $('#'+input_itm_id).closest("tr").find("input[name='uom[]']").val(uom_id);
       $('#'+input_itm_id).closest("tr").find("label[for='stock[]']").text(r_depstock);
       $('#'+input_itm_id).closest("tr").find("label[for='st_stock[]']").text(r_ststock);
       // $('#'+input_itm_id).closest("tr").find("label[for='global_stock[]']").text(r_globalstock);
       $('#'+input_itm_id).closest("tr").find("label[for='lastRQty[]']").text(last_requested_qty);
       $('#'+input_itm_id).closest("tr").find("label[for='last_approved_date[]']").text(last_approved_date);
       if('{{$date_now}}' == last_approved_date){
                $('#'+input_itm_id).closest("tr").find("label[for='last_approved_date[]']").css( "background-color", "orange" );
    }
       $('#'+input_itm_id).closest("tr").find("input[name='req_qty[]']").val(reorder_level);
       $('#'+input_itm_id).closest("tr").find("label[for='reorder_level[]']").text(reorder_level);
   }else{
       console.log('asdsdsd');
       $('.added'+item_code).remove();
   }
   disableItemsQuantity();
}
function prevReqItems(page){
    var fromLocation = $('#from_location').val();
    var toLocation = $('#to_location').val();
    var prev_req_item_search = $('#prev_req_item_search').val();
    if(fromLocation == "-1"){
                alert('Please Select Department ..! ');return false;
    }else if(toLocation == "-1"){
                alert('Please Select Store Name ..! ');return false;
    }
    var url = '';
    if(toLocation == 'LGST001'){

        $.ajax({
            type: "GET",
            url: url,
            data: 'prevReqItems=1'+'&F_Location='+fromLocation+'&T_Location='+toLocation+'&prev_req_item_search='+prev_req_item_search+'&p_value='+page,
            beforeSend: function () {
                $("#fillPrevReqlist").html('');
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (html) {
                if($('#prev_req_items_modal').hasClass('show')){
                    console.log($('#prev_req_items_modal').hasClass('show'));
                }else{
                  $("#prev_req_items_modal").modal('show');
                }
                if(html){
                    $("#fillPrevReqlist").html(html);
                }else{
                    $("#fillPrevReqlist").append("<tr><td colspan='8'>No Previous Requested Items Found!</td></tr>");
                }
            },
            complete: function (){
                $.LoadingOverlay("hide");
                setTimeout(function(){
                    $(".theadfix_wrapper").floatThead('reflow');
                },2000);
            }

        });
    }else{
        $("#prev_req_items_div").hide();
    }
}

function list_prev_req_items(obj,item_code,item_desc,r_ststock,r_depstock,r_globalstock,uom_id,uom_name,last_requested_qty,last_approved_date){
    if($(obj).prop("checked") == true) {
        $(".deleteID" ).each(function() {
                var item_val = $(this).closest("tr").find(".item_desc").val();
                if($(this).val() == 0){
                  if(item_val == ""){
                        var tr_id = $(this).closest("tr").attr('id');
                        $("#"+tr_id+"").remove();
                    }
                }
        });
        var input_itm_id = addNewRowByDefault(1);
        $('#'+input_itm_id).val(item_desc);
        $('#'+input_itm_id).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
        val_id_itm = $('#'+input_itm_id).closest("tr").find("input[name='item_desc[]']").attr('id');
        $('#'+input_itm_id).closest("tr").find("label[for='uom[]']").text(uom_name);
        $('#'+input_itm_id).closest("tr").find("input[name='uom[]']").val(uom_id);
        $('#'+input_itm_id).closest("tr").find("label[for='stock[]']").text(r_depstock);
        $('#'+input_itm_id).closest("tr").find("label[for='st_stock[]']").text(r_ststock);
        $('#'+input_itm_id).closest("tr").find("label[for='global_stock[]']").text(r_globalstock);
        $('#'+input_itm_id).closest("tr").find("label[for='lastRQty[]']").text(last_requested_qty);
        $('#'+input_itm_id).closest("tr").find("label[for='last_approved_date[]']").text(last_approved_date);
        if('{{$date_now}}' == last_approved_date){
                $('#'+input_itm_id).closest("tr").find("label[for='last_approved_date[]']").css( "background-color", "orange" );
            }
        $('#'+input_itm_id).closest("tr").find("input[name='req_qty[]']").removeAttr('disabled');
    }
}
function clearprevReqItemsSearch(){
    $('#prev_req_item_search').val("");
    prevReqItems(0);
}
function printData(head_id) {
    url = "";
    url = $("#req_base_url").val();
    urls = url + "/purchase/stock_issue_print_out?stock_head_id=" + head_id;
    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    //window.open(urls, 'div', 'height=3508,width=2480');
    $.ajax({
            type: "GET",
            url: urls,
            data: '',
            beforeSend: function () {
                            },
            success: function (html) {
                $("#prrint_div").html(html)
                var msglist = document.getElementById('print_content');
    showw = msglist.innerHTML;
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.print()
    mywindow.close();
                },
            complete: function (){
                $("#prrint_div").html('');
            }

        });
}

function disableItemsQuantity(){
    $('.item_desc').each(function(){
     if($(this).val().trim() !=''){
        $(this).closest('tr').find('td .request_req_qty').attr('disabled',false);
     }
});
}
$(document).on('change','#select_all_item',function(){
    if($('#select_all_item').is(':checked')){
        $('.chk_reorder_items').prop('checked',false);
         $('.chk_reorder_items').click();
    }else{
        $('.chk_reorder_items').prop('checked',false);
        $('.removeTr').remove();
        addNewRowByDefault(0);
    }
})
function changeLocationCheck(e){
var selected_loc = $(e).val();
var dflt_ret_store_chk = $("#dflt_ret_store_chk").val();
// if(selected_loc == 'OTPIPS'){
//     $("#to_location").val('IPH001').select2();
//     $("#to_location option").each(function (index) {
//         if ( $(this).val() == 'IPH001' ) {
//             $(this).prop('disabled', false);
//          }
//          else {
//             $(this).prop('disabled', true);
//          }
//       });
// }else{

//     $("#to_location option").each(function (index) {
//             $(this).prop('disabled', false);
//       });
//       $("#to_location").val(dflt_ret_store_chk).select2();
// }
var url = "{{route('extensionsvalley.purchase.getToLocation')}}";
var loc_searchstring = '';
$.ajax({
            type: "POST",
            url: url,
            data: 'selected_loc='+selected_loc+'&selecting_store=1',
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                var datas = JSON.parse(data);
                $.each(datas, function (key, val) {
                        var sel = '';
                        if (dflt_ret_store_chk == val.location_code)
                        {
                            sel = 'selected';
                        } else {
                            sel = '';
                        }
                        loc_searchstring += "<option " + sel + " value='" + val.location_code + "'>" + val.location_name + "</option>";
                    });
            },
            complete: function (){
                $.LoadingOverlay("hide");
                var html_data = '<label for="">Store</label><div class="clearfix"></div>'+
                '<select class=""form-control to_location select2" id="to_location" name="to_location">' + loc_searchstring + '</select>';
                $("#append_to_loc").html(html_data);
                $("#to_location").select2();
            }

        });

}
function getStoreLocationType(){
    var url = "{{route('extensionsvalley.purchase.getStoreLocationType')}}";
    var to_location = $("#to_location").val();
var loc_searchstring = '';
$.ajax({
            type: "POST",
            url: url,
            data: 'selected_loc='+to_location+'&selecting_store=1',
            beforeSend: function () {
            },
            success: function (data) {
                $("#purchase_type").val(data);
            },
            complete: function (){

            }

        });
}
</script>

@endsection
