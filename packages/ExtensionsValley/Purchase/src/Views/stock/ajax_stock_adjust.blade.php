<?php

if (isset($itemCodeDetails)) {
    if (!empty($itemCodeDetails)) {
        foreach ($itemCodeDetails as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}")' class="list_hover">
                {!!$item->item_desc!!}
            </li>
            <?php
        }
   } else {  ?>
        <li onclick='fillNoItem(this);' style="display: block; padding:5px 10px 5px 5px; "> <?php 
        echo 'No Result Found';  ?>
        </li> <?php 
    }
}

if(isset($request_form)) {
    if(!empty($request_form['res'])) {
        foreach ($request_form['res'] as $batch) {
            
      ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemBatchValues(this,"{{htmlentities($batch->batch_no)}}","{{htmlentities($batch->expiry_date)}}","{{htmlentities($batch->stock)}}")' class="list_hover">
                <?php echo htmlentities($batch->batch_no) . '     [   ' . htmlentities($batch->expiry_date) . '   ]'; ?>
            </li>
            <?php
        }
    } else {  ?>
        <li onclick='fillNoItem(this);' style="display: block; padding:5px 10px 5px 5px; "> <?php 
        echo 'No Result Found';  ?>
        </li> <?php 
    }
}


$i = 1;
?>
@if(isset($stockItemDetails) && !empty($stockItemDetails))
@foreach($stockItemDetails as $data)

<tr id="{{$data->id}}" >

    <td style="width: 2%;">
        <input type="hidden" id="save_update_{{$i}}" name="save_update[]"  value="1">
        <input name="del[]" class="deleteID" type="checkbox" value="{{$data->id}}" >
        <input type="hidden" id="save_update_id_{{$i}}" name="save_update_id[]" value="{{$data->id}}">
    </td>
    <td style="width: 15%;">
        <input type='text' required  autocomplete='off' value="{{$data->item_desc}}" id="field_item_code-{{$i}}" readonly name='item_desc[]' onkeyup='searchItemCode(this.id,event)' class='form-control item_desc' placeholder='Search Item Name'>
          <div class='ajaxSearchBox' style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
          <input type='hidden' class="item_code_hidden" name='item_code_hidden[]' value="{{$data->item_code}}" id="item_code_hidden{{$i}}">
    </td>
    <td style="width: 7%;">
        <input type='text' required  readonly autocomplete='off' name='item_batch[]' onkeyup='searchItemBatch(this.id,event,this)' value="{{$data->batch_no}}" class='form-control item_batch' placeholder='Search Batch'>
            <div class='ajaxSearchBox' style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width: 23%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
        <input type='hidden' class='item_batch_hidden' value="{{$data->batch_no}}" name='item_batch_hidden[]'>
    </td>
    <td></td>
    <td>
        <label for='refer_batch[]'>
            {{$data->reference_batch}}
        </label>
        <input type='hidden' class='refer_batch_hidden' value="{{$data->reference_batch}}" name='refer_batch_hidden[]' >
    </td>
    <td>
        <label for='expiry_date[]' id="field_exp-{{$i}}">{{$data->expiry_date}}</label>
        <input type='hidden' class='item_batch_exp_hidden' value="{{$data->expiry_date}}" name='item_batch_exp_hidden[]'>
    </td>
    <td>
        <label for='stock[]' id="field_stock-{{$i}}">{{$data->previous_quantity}}</label>
        <input type='hidden' class='stock' value="{{$data->previous_quantity}}" name='stock[]'>
    </td>
    <td>
        <input type='text' class='form-control item_reason' autocomplete='off'  name='item_reason[]' value="{{$data->item_reason}}" id="field_item_reason-{{$i}}" >
    </td>
    <td>
         {!! Form::select('adj_operation[]', array("1"=> " In", "2" => " Out"),isset($data->adjustment_operation) ? $data->adjustment_operation :null, [
                        'class'       => 'form-control adj_optn',
                        'id'    => 'adj_operation'
                    ]) !!}

    </td>
    <td>
        <?php 
        $added_qty = 0;
        if($data->adjustment_operation == 2){
            $added_qty = $data->previous_quantity - $data->adj_quantity;
        }else if($data->adjustment_operation == 1){
            $added_qty = $data->adj_quantity - $data->previous_quantity;
        }

        ?>
        <input type='text' class='form-control adj_qtyy' required autocomplete='off'  name='adj_qty[]' value="{{$data->adj_quantity}}" id="field_adj_qty-{{$i}}" onkeyup="validate_adj_qty(this)" >
    </td>
    <?php if(isset($data->head_status) && ($data->head_status < 2)){ ?>
    <td>
        <i style='padding: 5px 8px; font-size: 15px;cursor: pointer;' class='fa fa-trash text-red deleteRow' onclick='deleteEditRow(this,"{{$data->id}}");'></i>
    </td>
    <?php } ?>

</tr> <?php $i++; ?>
@endforeach
@endif
<?php $i=1; ?>
@if(isset($pendingItemDetails) && !empty($pendingItemDetails))
@foreach($pendingItemDetails as $data)
<tr>
    <td width="3%" style="text-align: left;">{{$i}}</td>
    <td width="5%" style="text-align: left;"> {{$data->r_request_no}} </td>
    <td width="5%" style="text-align: left;"> {{$data->r_item_code}} </td>
    <td width="15%" style="text-align: left;"> {{$data->r_item_desc}} </td>
    <td width="3%" style="text-align: left;"> {{$data->r_issued_qty}} </td>
    <td width="3%" style="text-align: left;"> {{$data->r_approved_qty}} </td>
    <td width="3%" style="text-align: left;" style="text-align: left;"> {{$data->r_verified_qty}} </td>
    <td width="3%"style="text-align: left;" style="text-align: left;"> {{$data->r_requested_qty}} </td>

</tr> <?php $i++; ?>
@endforeach
@endif



