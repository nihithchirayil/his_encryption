@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset(" packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset(" packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display: none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position: absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }

    .search_box_bottom {
        bottom: -30px;
    }

    .ajax_search_box_bottomcls .ajaxSearchBox {
        bottom: 3px;
        height: 200px !important;
    }

    .ajax_search_box_bottomcls_reduce_box {
        height: 200px !important;
    }

    table td {
        position: relative !important;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" role="main">
    <div class="row codfox_container">
        <div class="col-md-12">
            <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
            {!!Form::open(array('route' => 'extensionsvalley.purchase.updateStockConsumption', 'method' => 'post',
            'class' => 'formShortcuts', 'id' => 'requisitionForm'))!!}
            <div class="col-md-12 no-padding">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <?php if(isset($item[0]->transaction_no)){ ?>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Transaction No</label>
                                <div class="clearfix"></div>
                                <input type="text" readonly="" autocomplete='off' class="form-control"
                                    name="transaction_no" id="transaction_no"
                                    value="<?= isset($item[0]->transaction_no) ? $item[0]->transaction_no :'' ?>">
                                <input type="hidden" class="form-control" name="request_stock_head_id"
                                    id="request_stock_head_id"
                                    value="{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}" readonly="">
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('from_location', array("-1"=> " Select Location") +
                                $location->toArray(),isset($item[0]->location_code) ? $item[0]->location_code
                                :$default_location, [
                                'class' => 'form-control from_location select2',
                                'id' => 'from_location'
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="patient_name"
                                    id="patient_name"
                                    value="<?= isset($item[0]->patient_name) ? $item[0]->patient_name :'' ?>">
                                <input type="hidden" autocomplete='off' class="form-control" name="patient_name_hidden"
                                    id="patient_name_hidden"
                                    value="<?= isset($item[0]->patient_id) ? $item[0]->patient_id :0 ?>">
                            </div>
                            <div id="patient_name_searchCodeAjaxDiv" class="ajaxSearchBox"
                                style=" width: 100%;z-index: 1100"></div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12 padding_sm">
                <!--                <div class="theadscroll always-visible" style="position: relative; height: 680px;">-->

                <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                    id="addNewItem" style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg ">
                            <th width="2%">#</th>
                            <th width="15%">
                                <input id="issue_search_box" onkeyup="searchProducts();" type="text"
                                    placeholder="Item Search.. " style="color: #000;display: none;width: 90%;">
                                <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                        class="fa fa-search"></i></span>
                                <span id="item_search_btn_text">Item Name</span>
                            </th>
                            <th width="7%">Batch</th>
                            <th width="3%">Stock</th>
                            <th width="3%">Expiry Date</th>
                            <th width="3%">Qty</th>
                            <th width="5%">Remarks</th>
                            <th width="1%"><button type="button" tabindex="-1" id="add"
                                    class="btn btn-success add_row_btn" onclick="addNewRowByDefault()"><i
                                        class="fa fa-plus"></i></button></th>
                        </tr>

                    </thead>
                    <tbody id="fillrequestitemdetails">

                    </tbody>

                </table>
                <!--                </div>-->


                <div class="clearfix"></div>
                <div class="ht5"></div>
            </div>
            <div class="col-md-12" style="padding-top:70px;">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="col-md-1 padding_sm" style="float: right;">
                                <?php
                                $status_value = '';
                                $worksheet = '';
                                $submit = '';
                                $approve = '';
                                $disable_prop = '';
                                $status_value = isset($item[0]->status) ? $item[0]->status : '';

                                foreach ($access_control as $key => $value) {
                                    foreach ($value as $k => $v) {
                                        if (($k == 'worksheet') && ($v == 1)) {
                                            $worksheet = '1';
                                        }if (($k == 'submit') && ($v == 1)) {
                                            $submit = '1';
                                        }if (($k == 'approve') && ($v == 1)) {
                                            $approve = '1';
                                        }
                                    }
                                }
                                if ($status_value == 2) {
                                    $disable_prop = 'display:none';
                                }
                                ?>
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="saveItem();" class="btn btn-block light_purple_bg saveitembtn"
                                    style="<?= $disable_prop ?>"><i class="fa fa-save"></i>
                                    Save</div>
                            </div>
                            <div class="col-md-2 padding_sm" style="float: right !important;">
                                <div class="mate-input-box">
                                    <label for="">Select Status</label>
                                    <div class="clearfix"></div>
                                    <select name="status" id="status" class="form-control status">
                                        @if($submit == 1)
                                        <option @if($status_value==1) selected @endif value="1">Submitted</option>@endif
                                        @if($approve == 1)
                                        <option @if($status_value==2) selected @endif value="2">Approved</option>@endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i>
                                    Clear</a>
                            </div>
                            <div class="col-md-1 padding_sm" style="float: left;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <span onclick="deleteConsumption()" class="btn btn-block btn-danger"
                                    style="<?= $disable_prop ?>"><i class="fa fa-trash"></i>
                                    Delete</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}" />
<script type="text/javascript">
</script>

@stop

@section('javascript_extra')
<script src="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.js')}}"></script>
<script src="{{asset(" packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset(" packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    var j = 1;
    batch_array = {};
    item_array = {};
    var previous;
    (function () {
    $("#from_location").on('focus', function () {
    previous = this.value;
    }).change(function() {
    if (previous == '-1' || previous == - 1){
    @if (isset($status_value) && $status_value != '3')
            setTimeout(function(){
            addNewRowByDefault();
            }, 300);
    @endif
    }

    });
    })();
    $(document).ready(function() {
    $('body').on('keydown', '.request_req_qty', function(e) {
    var rowCounts = $('#addNewItem tr').length;
    var th = $(this).closest("tr").find("input[name='item_desc[]']").attr('id'); ;
    var ths = th.split('_');
    var rowCounts = ths[2];
    var rowCountss = parseFloat(rowCounts) + 1;
    if (e.which == 9) {  setTimeout(function(){
    $('#item_code_' + rowCountss).focus();
    }, 300);
    }
    });
    $("#item_search_btn").click(function(){
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
    });
    @if(isset($item[0]->stock_id))
            getStockRecords("{{$item[0]->stock_id}}");
    @else if ($("#from_location").val() != '-1'){
        setTimeout(function(){
            addNewRowByDefault();
        }, 1000);
    }
    setTimeout(function(){
    document.getElementById("item_code_1").focus();
    }, 2000);
    @endif

            var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function($table) {
    return $table.closest('.theadscroll');
    }
    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });


    $('body').on('change', '.item_batch', function(e) {
        var from_location = $("#from_location").val();
        var batch_no = $(this).val();
        var exp_date = $(this).find('option:selected').attr('exp-date');
        var item_code_hidden = $(this).closest("tr").find("input[name='item_code_hidden[]']").val();
        $(this).closest("tr").find("[class='item_batch_exp_hidden']").html(exp_date);
        $(this).closest("tr").find("input[name='item_batch_exp_hidden[]']").val(exp_date);
        var stocks = getStockBasedBatch(item_code_hidden, batch_no, from_location);
        $(this).closest("tr").find("label[for='stock[]']").text(stocks);
        $(this).closest("tr").find("input[name='stock[]']").val(stocks);
        $(this).parents("tr").find("input[name='req_qty[]']").focus();
        var item_desc_chk = $(this).parents("tr").find("input[name='item_desc[]']").val();
        var batch_contain = 0;
        var item_desc_contain = 0;
        var chk_new_row_create = 0;
        $('#addNewItem tr').each(function () {
            if ($(this).find('input[name="item_desc[]"]').val() == item_desc_chk) {
            item_desc_contain = item_desc_contain + 1;
            }
            if ($(this).find('input[name="item_batch[]"]').val() == batch_no) {
            batch_contain = batch_contain + 1;
            }
            if (batch_contain > 1 && item_desc_contain > 1){
            toastr.error("This Item already selected");
            }
            if ($(this).find('input[name="item_desc[]"]').val() == ''){
            chk_new_row_create = 1;
            }
        });

    });


    });
    /**
     *
     * Java script search in the search item box
     */
    function addNewRowByDefault(){

    if (document.getElementById('from_location').value == "-1")
    {
    alert('Please Select Location  ..! '); return false;
}

    var table = document.getElementById("addNewItem");
    var newrow = table.rows.length;
    var row = table.insertRow(newrow);
    row.id = "newRow" + newrow;
    <?php  $a = 1; $b = 0;
               foreach($incr_array as $inc) { ?>

                    var cell_no{{$a}} = row.insertCell("{{$b}}");

                    cell_no{{$a}}.innerHTML = "<?php  echo $inc; ?>";
            <?php
                    $a++; $b++;
                } ?>

    var reason_head = $("#reason_head").val();
    cell_no2.firstChild.id = 'item_code_' + j;
    cell_no3.firstChild.id = 'item_batch_' + j;
    cell_no6.firstChild.id = 'return_reason_' + j;
    var text_input_id = cell_no2.firstChild.id;
    document.getElementById(text_input_id).nextSibling.id = 'ajaxSearchBox_' + j;
    var text_input_id1 = cell_no3.firstChild.id;
    document.getElementById(text_input_id1).nextSibling.id = 'ajaxSearchBatchBox_' + j;
    if (j > 2){
    $('#ajaxSearchBox_' + j).addClass('ajax_search_box_bottomcls_reduce_box');
    }
    if (j > 7){
    $('#newRow' + j).addClass('ajax_search_box_bottomcls');
    }
    j++;
    }

    function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("addNewItem");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
    if (td.innerHTML.toUpperCase().indexOf(filter) > - 1) {
    tr[i].style.display = "";
    } else {
    tr[i].style.display = "none";
    }
    }
    }
    }

    function searchItemBatch(id, event, list) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var from_location = $('#from_location').val();
    var item_batch = $('#'+id).val();
    var item_code = $(list).closest("tr").find("input[name='item_code_hidden[]']").val();
    if (item_batch == "") {
    $("#" + ajax_div).html("");
    } else {
    var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
    $.ajax({
    type: "GET",
            url: url,
            data:  'item_batch=' + item_batch + '&item_code=' + item_code + '&from_location=' + from_location + '&batchData=' + 1,
            beforeSend: function () {
            $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#" + ajax_div).html(html).show();
            $("#" + ajax_div).find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });
    }

    } else {
    ajaxProgressiveKeyUpDown(ajax_div, event);
    }
    }

    function searchItemCode(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var current;
    var ajax_div = $('#' + id).next().attr('id');
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var item_code = $('#' + id).val();
    if (item_code == "") {
    $("#" + ajax_div).html("");
    $("#" + ajax_div).hide();
    } else {
    var url = "";
    $.ajax({
    type: "GET",
            url: url,
            data:  'item_code=' + item_code + '&from_location=' + from_location + '&to_location=' + to_location + '&itemSearchData=' + 1,
            beforeSend: function () {
            $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            $("#" + ajax_div).html(html).show();
            $("#" + ajax_div).find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });
    }

    } else {
    ajaxProgressiveKeyUpDown(ajax_div, event);
    }
    }

    function fillNoItem(list){
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#' + itemCodeListDivId).hide();
    }

    /* For Purchase order item Search, filling values */
    function fillItemValues(list, item_code, item_desc, uom_name) {
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#' + itemCodeTextId).val(item_desc);
    $('#' + itemCodeListDivId).hide();
    // var consumable = chk_consumable_item(item_code);
    // if(consumable == 0){
    //     alert("This Item Is Not Consumable !!");
    //     $('#' + itemCodeTextId).val("");
    //     $('#' + itemCodeListDivId).hide();
    //     return false;
    // }
    $('#' + itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    $('#' + itemCodeTextId).closest("tr").find("label[for='uom[]']").text(uom_name);
    $('#' + itemCodeTextId).parents("tr").find("input[name='item_batch[]']").focus();
    //$(".item_batch").focus();
    var fromLocationPub = $('#from_location').val();
    var toLocationPub = $('#to_location').val();
    var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
        $.ajax({
        type: "GET",
            url: url,
            data:  'item_code='+item_code+'&from_location='+fromLocationPub+'&to_location='+toLocationPub+'&batchData='+1,
            beforeSend: function () {
            },
            success: function (res) {
                // console.log(res.length > 0);
                if (res.length > 0){
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").empty();
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").append('<option value=""> Select Batch</option>');
                    for(var i=0;i<res.length;i++){
                        $('#'+itemCodeTextId).parents("tr").find(".item_batch").append('<option exp-date="' + res[i].expiry_date + '" value="' + res[i].batch_no + '" >' + res[i].batch_no + " [ "+ res[i].expiry_date + " ]" + " [ "+ res[i].stock + " ]" + '</option>');
                     }
                    } else{
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").empty();
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").append('<option value=""> Select Batch</option>');
                    }
            },
            complete: function () {
            }
        });
    }
    function chk_consumable_item(item_code) {
        var consumable = 1;
        var url = "";
        $.ajax({
        type: "GET",
                url: url,
                async: false,
                data:  'chk_consumable=1' + '&item_code=' + item_code ,
                beforeSend: function () {
                },
                success: function (data) {
                    var data = JSON.parse(data);
                    if(data.item_type == 1) {
                        consumable = data.consumable;
                    }
                },
        });
        return consumable;
    }
    function fillItemBatchValues(list, batch_no, expiry_date, stock){
    var from_location = $("#from_location").val();
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#' + itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#' + itemCodeTextId).val(batch_no);
    $('#' + itemCodeListDivId).hide();
    $('#' + itemCodeTextId).closest("tr").find("input[name='item_batch_hidden[]']").val(batch_no);
    $('#' + itemCodeTextId).closest("tr").find("[class='item_batch_exp_hidden']").html(expiry_date);
    $('#' + itemCodeTextId).closest("tr").find("input[name='item_batch_exp_hidden[]']").val(expiry_date);
    var item_code_hidden = $('#' + itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val();
    var stocks = getStockBasedBatch(item_code_hidden, batch_no, from_location);
    $('#' + itemCodeTextId).closest("tr").find("label[for='stock[]']").text(stocks);
    $('#' + itemCodeTextId).closest("tr").find("input[name='stock[]']").val(stocks);
    $('#' + itemCodeTextId).parents("tr").find("input[name='req_qty[]']").focus();
    var item_desc_chk = $('#' + itemCodeTextId).parents("tr").find("input[name='item_desc[]']").val();
    var batch_contain = 0;
    var item_desc_contain = 0;
    var chk_new_row_create = 0;
    $('#addNewItem tr').each(function () {
    if ($(this).find('input[name="item_desc[]"]').val() == item_desc_chk) {
    item_desc_contain = item_desc_contain + 1;
    }
    if ($(this).find('input[name="item_batch[]"]').val() == batch_no) {
    batch_contain = batch_contain + 1;
    }
    if (batch_contain > 1 && item_desc_contain > 1){
    toastr.error("This Item already selected");
    }
    if ($(this).find('input[name="item_desc[]"]').val() == ''){
    chk_new_row_create = 1;
    }
    });
    }
    function getStockBasedBatch(item_code_hidden, batch_no, from_location){
    var ret_data = '';
    var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
    $.ajax({
    type: "GET",
            url: url,
            async: false,
            data:  'item_code_hidden_stock_only=' + item_code_hidden + '&batch_no_stock_only=' + batch_no + '&from_location_stock_only=' + from_location + '&stock_only=' + 1,
            beforeSend: function () {
            },
            success: function (data) {
            ret_data = data;
            },
    });
    return ret_data;
    }
    function deleteRow(list){
    var item_name = $(list).closest("tr").find(".item_desc").val();
    if (item_name == ""){
    var id = $(list).closest("tr").attr('id');
    $("#" + id + "").remove();
    } else{
    if (confirm('Are you sure wanted to delete?')) {
    var id = $(list).closest("tr").attr('id');
    $("#" + id + "").remove();
    }
    }
    }

    function deleteEditRow(list, id){
    if (confirm('Are you sure wanted to delete?')) {
    var url = '';
    $.ajax({
    type: "GET",
            url: url,
            data: 'stockrequestdelID=' + id,
            beforeSend: function () {
            },
            success: function (data) { //alert(html);
            if (data == 1){
            var d = $(list).closest("tr").attr('id');
            $("#" + d + "").remove();
            }
            }
    });
    }
    }

    function getStockRecords(stock_id){
    var url = '';
    $.ajax({
    type: "GET",
            url: url,
            data: 'stockItemDetails=' + stock_id,
            beforeSend: function () {
            },
            success: function (html) { //alert(html);
            $("#fillrequestitemdetails").html(html);
            @if (isset($status_value) && $status_value != '2')
                    addNewRowByDefault();
            @endif
                    setTimeout(function(){
                    document.getElementById("item_code_1").focus();
                    }, 2000);
            }
    });
    }

    function saveItem(){
        var status = $('#status').val();
        if(status == '' || status == '0'){
            toastr.error("Please Select Status");
            return false;
        }
        var flag = 0;
        var f = 0;
        $(".request_req_qty").each(function () {
            var item = $(this).closest("tr").find(".item_desc").val();
            if (item != ""){
                if (isNaN(this.value) || this.value == '' || this.value == 0){
                    f = 1;
                }
            } else{
                if (this.value != '' || this.value != 0){
                    flag = 1;
                }
            }
        });
        if (f == 1){
            alert("Request quantity is zero"); return false;
        }
        if (document.getElementById('from_location').value == "-1")
        {
            alert('Please Enter Return From ..! ');
            return false;
        }

        var table = document.getElementById("addNewItem");
        var count_row = table.rows.length;
        if (flag == 1 || count_row == 1){
            alert('Please Enter Item ..! ');
            return false;
        } else{
            $(".saveitembtn").prop("disabled", true);
            $("#requisitionForm").submit();
        }
    }

    function raiseRequestNewReset(){
    $("#itm_name").val('');
    $("#itm_qty").val('');
    $("#itm_desc").val('');
    }

    function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
    e.value = val.substring(0, val.length - 1);
    }
    }
    function checkStock(e){
    var chk_new_row_create = 0;
    var stock = $(e).parents("tr").find(".stock_label").html();
    var ret_val = $(e).val();
    if (/\D/.test(ret_val)){
    toastr.error("Only numeric allowed");
    $(e).val('');
    chk_new_row_create = 1;
    }
    if ((parseFloat(stock) < parseFloat(ret_val)) || stock == ''){
    toastr.error("Please check available stock");
    $(e).val('');
    chk_new_row_create = 1;
    }
    var chk_btch = $(e).parents("tr").find(".reason_all").val();
    if (chk_btch == '-1'){
    toastr.warning("Please add reason !!");
    $(e).val('');
    chk_new_row_create = 1;
    }
    if (chk_new_row_create == 0){
    var ck = 0;
    $('#addNewItem tr').each(function () {
    if ($(this).find('input[name="item_desc[]"]').val() == ''){
    ck = 1;
    }
    });
    if (ck == 0){
    addNewRowByDefault();
    }
    }
    }
$('#patient_name').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var patient_name = $(this).val();
    patient_name = patient_name.trim();
    if (patient_name == "") {
    $("#patient_name_searchCodeAjaxDiv").html("");
    $("#patient_name_hidden").val("");
    } else {
    try {
    var param = {patient_search: patient_name};
    $.ajax({
    type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
            $("#patient_name_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            if (html == 'No Result Found') {
            }
            $("#patient_name_searchCodeAjaxDiv").html(html).show();
            $("#patient_name_searchCodeAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });
    } catch (err) {
    document.getElementById("demo").innerHTML = err.message;
    }
    }
    } else {
    ajax_list_key_down('patient_name_searchCodeAjaxDiv', event);
    }
    });
    $('#patient_name').on('keydown', function (event) {
    if (event.keyCode === 13) {
    ajaxlistenter('patient_name_searchCodeAjaxDiv');
    return false;
    }
    });
    function fill_patient_details(e, user_name, user_id) {
    $('#patient_name').val(user_name);
    $('#patient_name_hidden').val(user_id);
    $('#patient_name_searchCodeAjaxDiv').hide();
    }
    function deleteConsumption(){

  if(confirm('If sure press OK !!')){
        var deleteArray = [];
        $('.deleteID').each(function () {
            if (this.checked) {
                var checked = $(this).val();
                 deleteArray.push(checked);
            }
        });

        if (deleteArray.length > 0){
            var url = '';
            $.ajax({
                type: "GET",
                data: "deleteArray=" + deleteArray + "&delete_consumption=1",
                url: url,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (data) {
                    if (data == 1){
                        toastr.success("Deleted Successfully");
                        $('.deleteID').each(function () {
                        if (this.checked) {
                            var checked_id = $(this).val();
                            $("#" + checked_id).remove();
                            }
                        });
                        deleteArray = [];
                        //location.reload();
                    } else if (msg == 0){
                        toastr.error("Error !!");
                    }

                },
                complete: function () {
                $.LoadingOverlay("hide");
                },
            });
        }
    }
}
function rejectConsumption(head_id){
    if(confirm('If sure press OK !!')){

        if (head_id > 0){
            var url = '';
            $.ajax({
                type: "GET",
                data: "reject_id=" + head_id ,
                url: url,
                beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (data) {
                    if (data == 1){
                        toastr.success("Rejected Successfully");
                        var main_uri = $("#domain_url").val() + "/purchase/";
                        var route_path = "listStockConsumption";
                        var action_url = main_uri + route_path;
                        document.location.href = action_url;
                    } else if (msg == 0){
                        toastr.error("Error !!");
                    }

                },
                complete: function () {
                $.LoadingOverlay("hide");
                },
            });
        }
    }
    }
</script>

@endsection
