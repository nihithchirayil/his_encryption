@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: hidden !important;
        width: 34%;
        z-index: 99999;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .search_box_bottom{
        bottom: -30px;
    }
    .ajax_search_box_bottomcls .ajaxSearchBox{
        bottom: 3px;
        height: 200px !important;
    }
    .ajax_search_box_bottomcls_reduce_box{
        height: 200px !important;
    }
    table td{
        position: relative !important;
    }
    .medicine-list-div-row-listing {
    z-index: 99999;
    background: #FFF;
    box-shadow: 0 0 6px #ccc;
    padding: 1px;
}
.active_row_item{
    background-color: #ccebbc !important;
    cursor: pointer;
}
</style>
@endsection
@section('content-area')
     <!-- page content -->
     <div class="right_col"  role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <input type="hidden" id="item_list" value='{{ json_encode($stock_itemdetaildata) }}'>
                <input type="hidden" id="head_id" value="{{$id}}">
                <input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
                 <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                {!!Form::open(array('route' => 'extensionsvalley.purchase.updateDeptStockRequests', 'method' => 'post', 'class' => 'formShortcuts', 'id' => 'requisitionForm'))!!}
                <div class="col-md-12 no-padding">
                <div class="box no-border no-margin" >
                    <div class="box-body clearfix">
                           <div class="col-md-2 padding_sm">
                             @if(isset($item[0]) && $item[0] !='')
                             <div class="mate-input-box">
                                <label for="">Request No.</label>
                                <div class="clearfix"></div>
                                <span>{{isset($item[0]->request_no) ? $item[0]->request_no : ''}}</span>
                                <input type="hidden" class="form-control" name="request_no" id="request_no" value="{{isset($item[0]->request_no) ? $item[0]->request_no : ''}}" readonly="">
                                 <input type="hidden" class="form-control" name="request_stock_head_id" id="request_stock_head_id" value="{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}" readonly="">
                             </div>
                                  @endif
                            </div>

                        <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('to_location', array("-1"=> " Select From Location") + $location->toArray(),isset($item[0]->to_location_code) ? $item[0]->to_location_code :$default_location, [
                            'class'       => 'form-control to_location select2',
                            'id'    => 'to_location',
                            'onchange' => 'changeLocationCheck(this)'
                        ]) !!}
                    </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box" id="append_to_loc">
                                    <label for="">To Location</label>
                                <div class="clearfix"></div>
                                <?php  $dfltstore = \WebConf::getConfig('default_store'); ?>
                               {!! Form::select('from_location', array("-1"=> " To Location") + $to_location->toArray(),isset($item[0]->from_location_code) ? $item[0]->from_location_code : '', [
                            'class'       => 'form-control from_location select2',
                            'id'    => 'from_location',

                        ]) !!}

                    </div>
                    <input type="hidden" value="{{isset($item[0]->from_location_code) ? $item[0]->from_location_code : $dfltstore}}" id="dflt_ret_store_chk" >

                            </div>

                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                       <textarea id="requisition_comments"  wrap="off" class="form-control" cols="35" rows="1" name="remarks" >{{isset($item[0]->comments) ? $item[0]->comments :''}}</textarea>
                                   </div>
                            </div>

                    </div>
                </div>
            </div>
            <input type="hidden" value="" id="location_type">
            <?php
                $status_value = '';
                $worksheet = '';
                $submit = '';
                $verify = '';
                $approve = '';
                $disable_prop = '';
                $status_value = isset($item[0]->head_status) ? $item[0]->head_status : '';
                if($status_value >= 4){
                    $status_value = 4;
                }
                foreach ($access_control as $key => $value) {
                    foreach ($value as $k => $v) {
                        if(($k == 'worksheet') && ($v == 1)){$worksheet = '1';
                        }if(($k == 'submit') && ($v == 1)){ $submit = '1';
                        }if(($k == 'verify') && ($v == 1)){  $verify = '1';
                        }if(($k == 'approve') && ($v == 1)){  $approve = '1';
                        }
                    }
                }
                if($status_value == 4){
                $disable_prop = 'display:none';
                }
            ?>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm text-right">
                    @if(isset($item[0]->request_no))
                    <div class="btn light_purple_bg pendrequestbtn" style="cursor: pointer;" onclick="pendrequest();">
                        Pending Request</div>
                    <div class="btn btn-dark" style="cursor: pointer;" onclick="printData('{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}');">
                        <i class="fa fa-print" aria-hidden="true"></i>Print</div>
                    @endif
                        <div class="btn btn-primary requestGrnbtn" style="cursor: pointer;<?= $disable_prop ?>" onclick="requestGrn(0);">
                        Against GRN</div>
                        <div class="btn btn-info" style="cursor: pointer;<?= $disable_prop ?>" onclick="reorderLevelItems(0);">
                        Re-order Level Items</div>
                </div>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                <div style="min-height:400px;">

                <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="addNewItem" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg" style="cursor: pointer;">
                               <th width="2%">#</th>
                               <th width="22%">
                                    <input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Item Search.. " style="color: #000;display: none;width: 90%;" >
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Item Name</span>
                                </th>
                                {{-- <th width="1%"></th> --}}
                               <th width="2%"><i class="fa fa-exclamation-triangle text-red"></i></th>
                               <th width="3%">UOM</th>
                               <th width="3%" title="Reorder Level">Re.Level</th>
                               <!-- <th width="4%" title="Min.Orderable/ Cons/ Req.Qty">Min.Ord/Req.Qty</th> -->
                               <th width="3%">Stock</th>
                               <th width="3%">S.Stock</th>
                               <!-- <th width="3%">G.Stock</th> -->
                               <th width="4%" title="Last Approved Qty">Lt.App.Qty</th>
                               <th width="4%" title="Issue Qty">Iss.Qty</th>
                               <th width="3%"><button type="button" tabindex="-1" id="add"  class="btn btn-success add_row_btn"onclick="addNewRowByDefault(0)" ><i class="fa fa-plus"></i></button></th>
                           </tr>

                       </thead>
                       <tbody id="fillrequestitemdetails">

                       </tbody>

            </table>
                </div>


                 <div class="clearfix"></div>
                    <div class="ht5"></div>
            </div>
                <div class="col-md-12">
                <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">

                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <input type="hidden" name="hid_item_batch_array" id="hid_item_batch_array">
                                <input type="hidden" name="hid_item_grn_array" id="hid_item_grn_array">
                                <span onclick="saveItem(event);" id="saveitembtn" class="btn btn-block light_purple_bg saveitembtn" style="<?= $disable_prop ?>"><i id="saveitembtn_spin" class="fa fa-save"></i>
                                    Save</span>
                </div>
                <div class="col-md-2 padding_sm" style="float: right !important;">
                    <div class="mate-input-box">
                                <label for="">Select Status</label>
                                <div class="clearfix"></div>
                                <select name="status" id="status" class="form-control status chk_status_head">

                               @if($worksheet == 1)
                               <option @if($status_value == 1) selected @endif value="1">Worksheet</option>@endif
                               @if($submit == 1)
                               <option @if($status_value == 2) selected @endif value="2">Submitted</option>@endif
                               @if($verify == 1)
                               <option @if($status_value == 3) selected @endif value="3">Verified</option>@endif
                               @if($approve == 1)
                               <option @if($status_value == 4) selected @endif value="4">Approved</option>@endif
                               </select>
                           </div>
                </div>
                <!-- <div class="col-md-2 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div data-toggle="modal" data-target="#raiseNewItemRequest" class="btn btn-block light_purple_bg raiserequestbtn"><i class="fa fa-plus"></i>
                                    Add New Request</div>
                </div> -->
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                               <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                </div>
            </div>
                </div>
            </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Modal -->
<div id="item_hist_req_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title item_history_title">
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="theadscroll always-visible">
                             <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed" style="border: 1px solid #CCC;">
                            <thead>
                            <tr class="table_header_bg" style="cursor: pointer;">
                                    <th>Request No</th>
                                    <th>Requested Date</th>
                                    <th>Issued By</th>
                                    <th>Received By</th>
                                    <th>Received Date</th>
                                    <th>Issued Status</th>
                                    <th>Issued Qty</th>
                                </tr>
                       </thead>
                        <tbody id="item_req_history_div">

                        </tbody>

                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i class="fa fa-thumbs-up"> OK</i></button>
            </div>
        </div>

    </div>
</div>
<!-- Modal -->
<div id="grnlist_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">GRN List</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-3 padding_sm">
                            <label for="">GRN No.</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="grn_no_search" id="grn_no_search">
                        </div>

                        <div class="col-md-3 padding_sm">
                            <label for="">Vendor Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="vendor_name" id="vendor_name">
                        </div>
                        <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="requestGrn(0);" class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button onclick="clearGrnSearch();" style="margin: 0 5px 0 0;" class="btn btn-block btn-warning">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="box no-border no-margin">
                    <div class="box-body clearfix" style="position: relative; height: 350px;" id="fillgrnlist">


                    </div>
                    </div>
                </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>
</div>
<!-- Modal -->
<div id="reorder_level_items_modal" class="modal fade" role="dialog">
    <!-- <div class="modal-dialog modal-lg"> -->
        <div class="modal-dialog" style="max-width: 1150px; width: 100%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List of Re-order Level Reached Items</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Item Name or Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="reorder_item_search" id="reorder_item_search">
                        </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                                <label for="" style="font-size: 12px; font-weight: 700;">Show Zero Stock</label>
                                <input type="checkbox" name="chk_zero_stock"  id="chk_zero_stock" >
                        </div>
                        <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="reorderLevelItems(0);" class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button onclick="clearReorderLevelSearch();" style="margin: 0 5px 0 0;" class="btn btn-block btn-warning">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="box no-border no-margin">
                    <div class="box-body clearfix" style="position: relative; height: 550px;" id="fillreorderLevellist">


                    </div>
                    </div>
                </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>
</div>
<!-- Modal -->
<div id="pend_req_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pending Request Item Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="theadscroll always-visible">
                             <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed" id="penddata" style="border: 1px solid #CCC;">
                            <thead>
                            <tr class="table_header_bg ">
                               <th width="3%">#</th>
                               <th width="5%">Request No.</th>
                               <th width="5%">Item Code</th>
                               <th width="15%">Item </th>
                               <th width="3%">Issued Qty</th>
                               <th width="3%">Approved Qty</th>
                               <th width="3%">Verified Qty</th>
                               <th width="3%">Requested Qty</th>
                           </tr>

                       </thead>
                        <tbody id="fillpendingitemdetails">

                        </tbody>

                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg">OK</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="raiseNewItemRequest" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Item Request</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Item Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="itm_name" id="itm_name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Quantity</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="itm_qty" id="itm_qty">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mate-input-box">
                                <label for="">Description</label>
                                <div class="clearfix"></div>
                                <textarea cols="25" rows="4" style="width: 548px; height: 91px;" name="itm_desc" id="itm_desc">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;"  onclick="raiseRequestNewReset();"   class="btn light_purple_bg">Clear</button>
                <button style="margin: 0 5px 0 0;"  onclick="raiseRequestNewItem();" class="btn light_purple_bg">Save</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
</script>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.js')}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    var j=1;
    batch_array = {};
    item_array = {};
    grn_array_data ={};
    grn_array ={};
    item_code_for_grn = '';
    var arr = new Array();
       var previous;
(function () {
    $("#from_location").on('focus', function () {
    previous = this.value;
}).change(function() {
    if(previous == '-1' || previous == -1){
        setTimeout(function(){
         addNewRowByDefault(0);
     },300);
    }
});

$('body').on('click', '.grn_pages a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var page = GetUrlParameter(url,'page');
            // console.log("   ---url---  "+page); return false;
            requestGrn(page);
});

$('body').on('click', '.reorder_level_pages a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var page = GetUrlParameter(url,'page');
            // console.log("   ---url---  "+page); return false;
            reorderLevelItems(page);
});

})();
$(document).ready(function() {
    $('body').on('keydown', '.request_req_qty', function(e) {
        var rowCounts = $('#addNewItem tr').length;
        var th = $(this).closest("tr").find("input[name='item_desc[]']").attr('id');;
        var ths = th.split('_');
        var rowCounts =  ths[2];
        var rowCountss = parseFloat(rowCounts)+1;
    if (e.which == 9) {  setTimeout(function(){
    $('#item_code_'+rowCountss).focus();
            },300);
    }
    });
    $("#item_search_btn").click(function(){
      $("#issue_search_box").toggle();
      $("#item_search_btn_text").toggle();
    });

    @if(isset($item[0]->stock_id))
        $("#to_location").trigger('change');
        getStockRecords("{{$item[0]->stock_id}}");
    @else
        @if(isset($status_value) && $status_value != 4)
            addNewRowByDefault(0);
        @endif
    setTimeout(function(){
    document.getElementById("item_code_1").focus();
    },2000);
    @endif

    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
    });

     $(document).on('click', '.field_emergency', function(e) {
                            // console.log($(this).prop("checked"));
                        if($(this).prop("checked") == true) {
                            $(this).closest("tr").find("input[name='emerg[]']").val('1');
                        }else{
                            $(this).closest("tr").find("input[name='emerg[]']").val('0');
                        };

        });

    $(".theadfix_wrapper").floatThead('reflow');
    $('.theadscroll').perfectScrollbar("update");
    var head_id=$('#head_id').val();
    if(parseInt(head_id)!=0){
        var item_list=$('#item_list').val();
        var obj=JSON.parse(item_list);
        $.each(obj, function (index, value) {
            batch_array[value.item_code]={};
            batch_array[value.item_code][btoa(value.batch_no)] = value.issued_qty;
        });
        console.log(batch_array);
    }
});
$(document).on('keydown', function (event) {
if ($('.medicine-list-div-row-listing').css('display') == 'block') {
        var row_length = $(".list-medicine-search-data-row-listing").find('tr').length;
        if (row_length > 0) {
            if (event.which == 38) { // up
                console.log("ffffffffff");
                if ($(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':first-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr').last().addClass('active_row_item');
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').prev().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').last().removeClass('active_row_item');
                }
            } else if (event.which == 40) { // down

                if ($(".list-medicine-search-data-row-listing").find('tr.active_row_item').length == 0 || $(".list-medicine-search-data-row-listing").find('tr.active_row_item').is(':last-child')) {
                    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr').first().addClass('active_row_item');
                    console.log("ppppp");
                } else {
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').next().addClass('active_row_item');
                    $(".list-medicine-search-data-row-listing").find('tr.active_row_item').first().removeClass('active_row_item');
                    console.log("vvvv");
                }
            } else if (event.which == 13) { // enter
                event.preventDefault();
                $(".list-medicine-search-data-row-listing").find('tr.active_row_item').trigger('click');
            }
        }
    }
});
/**
 *
 * Java script search in the search item box
 */
function GetUrlParameter(sPageURL, sParam){
    // var sPageURL = window.location.search.substring(1);
    // console.log(sPageURL);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] == sParam)

        {
            return sParameterName[1];
        }
    }
}

 function addNewRowByDefault(grn_val = 0){

//            if(document.getElementById('from_location').value == "-1")
//            {
//                 alert('Please Select Department ..! ');return false;
//            } else
            if(document.getElementById('to_location').value == "-1")
            {
                 alert('Please Select Store Name ..! ');return false;
            } else if(document.getElementById('to_location').value == document.getElementById('from_location').value)
            {
                alert('Error Please select location properly..!');return false;
            }

            var table = document.getElementById("addNewItem");
            var newrow = table.rows.length;
            var row = table.insertRow(newrow);
            row.id = "newRow" + newrow;
            row.classList.add('removeTr');
            <?php  $a = 1; $b = 0; ?>
             <?php   foreach($incr_array as $inc) { ?>

                    var cell_no{{$a}} = row.insertCell("{{$b}}");

                    cell_no{{$a}}.innerHTML = "<?php  echo $inc; ?>";
            <?php
                    $a++; $b++;
                }

            ?>
            cell_no2.firstChild.id = 'item_code_'+j;
            cell_no4.firstChild.id = 'fld_emergency-'+j;
            var text_input_id = cell_no2.firstChild.id;
            document.getElementById(text_input_id).nextSibling.id = 'ajaxSearchBox_'+j;
            var text_input_id1 = cell_no4.firstChild.id;
            document.getElementById(text_input_id1).nextSibling.id = 'emg-'+j;
            if(j > 2){
                $('#ajaxSearchBox_'+j).addClass('ajax_search_box_bottomcls_reduce_box');
            }
            if(j > 7){
                $('#newRow'+j).addClass('ajax_search_box_bottomcls');
            }
            $('.cmm_stl_txt').closest('td').addClass('common_td_rules');
            $('.cmm_stl').closest('td').addClass('td_common_numeric_rules');
            j++;
            if(grn_val == 1){
                return text_input_id;
            }

 }

function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("addNewItem");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


function searchItemCode(id,event) {
     if(document.getElementById('from_location').value == "-1"){
            alert('Please Select Department ..! ');return false;
        }
        var keycheck = /^[ A-Za-z0-9_@./#&+-]*$/;
        var value = event.key;
        var current;
        var div_id = $('#'+id).next().attr('id');
        if (event.keyCode == 13) {
        ajaxlistenter(div_id);
        return false;
        } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {

            var from_location = $('#from_location').val();
            var to_location = $('#to_location').val();
            var item_code = $('#'+id).val();
            if (item_code == "") {
                $("#"+div_id).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxNewDeptStockItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_code=' +encodeURIComponent(item_code)+'&from_location='+from_location+'&to_location='+to_location+'&item_code_id='+id,
                    beforeSend: function () {
                    $("#"+div_id).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+div_id).html(html).show();
                        $("#"+div_id).find('li').first().addClass('liHover');
                        $('.theadscroll').perfectScrollbar({
                                    minScrollbarLength: 30
                                });
                                var $table = $('table.theadfix_wrapper');
                                $table.floatThead({
                                    scrollContainer: function($table){
                                        return $table.closest('.theadscroll');
                                    }
                                });
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(div_id, event);
            }
}

function fillNoItem(list){
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#'+itemCodeListDivId).hide();
}

function fillItemValues(list,item_code,item_desc,uom_id,uom,item_code_id) {
 var item_code_exts11 = 0;
    $('#addNewItem tr').each(function () {
            item_code_temps = $(this).find(".item_code_hidden").val();
            if(item_code == item_code_temps){
                item_code_exts11 = 1;
            }
    });
    if(item_code_exts11 == 1){
        toastr.error("Selected Item Is Already Existing!");
        return false;
    }
    var url = "{{route('extensionsvalley.purchase.addAjaxNewDeptStockItem')}}";
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    $.ajax({
    type: "GET",
        url: url,
        data: 'code=' +item_code+'&location='+from_location+'&store_location='+to_location+'&fillStockFunctions=3',
        beforeSend: function () {
        },
        success: function (data) {
        if(data){
            if(data.get_lastreq_det == "no item found"){
                 fillRequestItemDetails(list,item_code,item_desc,uom_id,uom,data.r_depstock,data.r_ststock,data.r_globalstock,data.last_requested_qty,data.single_batch,data.single_stock,item_code_id);
            }else{
                if (confirm( data.get_lastreq_det  )) {
                  fillRequestItemDetails(list,item_code,item_desc,uom_id,uom,data.r_depstock,data.r_ststock,data.r_globalstock,data.last_requested_qty,data.single_batch,data.single_stock,item_code_id);
                }else{
                        return;
                }
            }
        }else{
           alert('Please Enter Valid Item..! ');return false;
        }
        },
        complete: function () {
        }
    });
}

function fillRequestItemDetails(list,item_code,item_desc,uom_id,uom,dep_stock,st_stock,global_stock,lastRQty,single_batch,single_stock,item_code_id){
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = item_code_id;//$('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(item_desc);
    $('#'+itemCodeListDivId).hide();
    var item_code_for_grn = $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val();
    $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    $('#'+itemCodeTextId).closest("tr").find("label[for='uom[]']").text(uom);
    $('#'+itemCodeTextId).closest("tr").find("input[name='uom[]']").val(uom_id);
    $('#'+itemCodeTextId).closest("tr").find("label[for='stock[]']").text(dep_stock);
    $('#'+itemCodeTextId).closest("tr").find("label[for='st_stock[]']").text(st_stock);
    // $('#'+itemCodeTextId).closest("tr").find("label[for='global_stock[]']").text(global_stock);
    $('#'+itemCodeTextId).closest("tr").find("label[for='lastRQty[]']").text(lastRQty);
    $('#'+itemCodeTextId).closest("tr").find("input[name='lastRQty[]']").val(lastRQty);
    //if(single_batch == 0){
    $('#'+itemCodeTextId).closest("tr").find(".request_req_qty").focus();
    $('#'+itemCodeTextId).closest("tr").find(".request_req_qty").click();
    delete grn_array_data[item_code_for_grn];
    grn_array_data[item_code_for_grn] = {};
//    }else{
//        $('#'+itemCodeTextId).closest("tr").find(".request_req_qty").focus();
//        $('#'+itemCodeTextId).closest("tr").find(".request_req_qty").removeAttr("onclick");
//        $('#'+itemCodeTextId).closest("tr").find(".request_req_qty").attr('batch-name', single_batch);
//        $('#'+itemCodeTextId).closest("tr").find("label[for='st_stock[]']").text(single_stock);
//    }
    var ck = 0;
    $('#addNewItem tr').each(function () {
        if ($(this).find('input[name="item_desc[]"]').val() ==''){
            ck = 1;
        }
    });
    if(ck == 0){
        addNewRowByDefault(0);
    }
    $(".list-medicine-search-data-row-listing").find('tr').removeClass('active_row_item');

}

function deleteRow(list){
    var item_name = $(list).closest("tr").find(".item_desc").val();
    if(item_name == ""){
        var id = $(list).closest("tr").attr('id');
        $("#"+id+"").remove();
    }else{
        if (confirm('Are you sure wanted to delete?')) {
                var id = $(list).closest("tr").attr('id');
                $("#"+id+"").remove();
        }
    }
}

function deleteEditRow(list,id){
     if (confirm('This item will be deleted immediately , are you sure to delete?')) {
            var url = '';
        $.ajax({
            type: "GET",
            url: url,
            data: 'stockrequestdelID='+id,
            beforeSend: function () {
            },
            success: function (data) {
                if(data==1){
                    var d = $(list).closest("tr").attr('id');
                    $("#"+d+"").remove();
                }
            }
        });

        }
}

function getStockRecords(stock_id){
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'stockItemDetails='+stock_id,
        beforeSend: function () {
        },
        success: function (html) { //alert(html);
        $("#fillrequestitemdetails").html(html);
        @if(isset($status_value) && $status_value != 4)
            addNewRowByDefault(0);
        @endif
            setTimeout(function(){
                document.getElementById("item_code_1").focus();
            },2000);
        }
    });
}

function saveItem(e){
    if (event.key == "Enter") {
        event.preventDefault();
    }
    e.preventDefault();
        var flag  = 0;
        var f  = 0;
        var table = document.getElementById("addNewItem");
        var count_row = table.rows.length;

        $(".request_req_qty").each(function () {
            var item = $(this).closest("tr").find(".item_desc").val();
            if(item != ""){
                if (isNaN(this.value) || this.value == '' || this.value == 0){
                        f  = 1;
                    }
            }else{
                  if(this.value > 0){
                        flag = 1;
                      }
                 }
        });

        var item_array = JSON.stringify(batch_array);
        var grn_array = JSON.stringify(grn_array_data);
        $("#hid_item_batch_array").val(item_array);
        $("#hid_item_grn_array").val(grn_array);

        if(f == 1){
                alert("Request quantity is zero");
                return false;
        } else if(document.getElementById('from_location').value == "-1")
        {
             alert('Please Select Department ..! ');
             return false;

        } else if(document.getElementById('to_location').value == "-1")
        {
             alert('Please Select Store Name ..! ');
             return false;
        } else if(document.getElementById('to_location').value == document.getElementById('from_location').value)
        {
            alert('Error Please select location properly..!');
            return false;
        } else if($(".chk_status_head").val() == ''){
            alert('Please Select Status ..! ');
            return false;
        } else if(flag == 1 || count_row == 1){
                 alert('Please Enter Item ..! ');
                 return false;
        }else{
                $("#saveitembtn").hide();
                $('#saveitembtn_spin').removeClass('fa fa-save');
                $('#saveitembtn_spin').addClass('fa fa-spinner fa-spin');
                $("#requisitionForm").submit();
        }
}

function raiseRequestNewReset(){
    $("#itm_name").val('');
    $("#itm_qty").val('');
    $("#itm_desc").val('');
}

function raiseRequestNewItem(){
    var url = "{{route('extensionsvalley.purchase.addNewDeptStockItemRequest')}}";
    // window.open(url,'_blank');
    var itm_name = $("#itm_name").val();
    var itm_qty = $("#itm_qty").val();
    var itm_desc = $("#itm_desc").val();
    var from_location = $("#from_location").val();
    if(from_location == "-1"){
                alert('Please Enter From Location ..! ');return false;
    }else if(itm_name == ""){
                alert('Please Enter Item ..! ');return false;
    }else if(itm_qty == ""){
                alert('Please Enter Item Quantity..! ');return false;
    }

    $.ajax({
        type: "GET",
        url: url,
        data: 'itm_name='+itm_name+'&itm_qty='+itm_qty+'&itm_desc='+itm_desc+'&from_location='+from_location,
        beforeSend: function () {
        },
        success: function (data) { //alert(data);
            if(data != 0){
                alert("New Item Requested Successfully..");
            }else{
                alert("New Item Not Requested Successfully..");
            }
        },
        complete: function (){
            $("#itm_name").val('');
            $("#itm_qty").val('');
            $("#itm_desc").val('');
            $("#raiseNewItemRequest").modal('hide');
        }
    });
}
function clearGrnSearch(){
    $('#grn_no_search').val("");
    $('#vendor_name').val("");
    requestGrn(0);
}

function requestGrn(page){
    // $("#grnlist_modal").modal('show');return false;
    var fromLocation = $('#from_location').val();
    var toLocation = $('#to_location').val();
    var grn_no_search = $('#grn_no_search').val();
    var vendor_name = $('#vendor_name').val();
    if(fromLocation == '-1'){
        alert('Please Select Department ..! ');return false;
    }
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'grnItemDetails=1'+'&fromLocation='+fromLocation+'&toLocation='+toLocation+'&grn_no_search='+grn_no_search+'&vendor_name='+vendor_name+'&page_value='+page,
        beforeSend: function () {
            $("#fillgrnlist").html('');
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (html) { //alert(html);
            if($('#grnlist_modal').hasClass('show')){
                console.log($('#grnlist_modal').hasClass('show'));
            }else{
              $("#grnlist_modal").modal('show');
            }
            if(html){
                $("#fillgrnlist").html(html);
            }else{
                $("#fillgrnlist").append("<tr><td colspan='8'>No GRN List Found!</td></tr>");
            }
        },
        complete: function (){
            $.LoadingOverlay("hide");
            setTimeout(function(){
                $(".theadfix_wrapper").floatThead('reflow');
            },2000);
        }
    });
}

function pendrequest(){
    var fromLocation = $('#from_location').val();
    @if(isset($item[0]->stock_id))
        var req_head_id = "{{$item[0]->stock_id}}";
    @else
        var req_head_id = 0;
    @endif
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'pendingItemDetails='+req_head_id+'&fromLocation='+fromLocation,
        beforeSend: function () {
            $("#fillpendingitemdetails").html('');
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (html) { //alert(html);
            $("#pend_req_modal").modal('show');
            // $(".theadfix_wrapper").floatThead('reflow');

            if(html){
                $("#fillpendingitemdetails").html(html);
            }else{
                $("#fillpendingitemdetails").append("<tr><td colspan='8'>No pending request found!</td></tr>");
            }
        },
        complete: function (){
            $.LoadingOverlay("hide");
            setTimeout(function(){
                $(".theadfix_wrapper").floatThead('reflow');
            },300);
        }
    });
}

function hide_batch_div(list) {
    $(list).closest("tr").find(".issue_qty_pop").addClass('hide');
}

function add_batch_list(list) {

        var item_code = $(list).closest("tr").find(".item_code_hidden").val();
        var issue_total = 0;
        var issue_qty_get = 0;
        var ij = 1;
        delete batch_array[item_code];
        batch_array[item_code] = {};
        $(".issueqtychnges").each(function (key, val) {
            var item_id = val.value;
            var bat_name = btoa($('#itembatchid' + ij).val().trim());
            issue_qty_get = $('#item_issueqtyid' + ij).val().trim();
            batch_array[item_code][bat_name] = issue_qty_get;
            issue_total += parseInt(issue_qty_get);
            ij++;
        });

        var req_qty = $('#requested_qtytot').html();
        if (req_qty) {
            $(list).closest("tr").find("input[name='req_qty[]']").val(req_qty);
        } else {
            $(list).closest("tr").find("input[name='req_qty[]']").val(0);
        }
        $(list).closest("tr").find(".issue_qty_pop").addClass('hide');

}
function chk_batch_item(list){
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var item_desc = $(list).closest("tr").find(".item_desc").val();
    var item_code = $(list).closest("tr").find("input[name='item_code_hidden[]']").val();
    var st_stock  = $(list).closest("tr").find("label[for='st_stock[]']").text();
    if(from_location == "-1"){
                alert('Please Select Department ..! ');return false;
    }else if(to_location == "-1"){
                alert('Please Select Store Name ..! ');return false;
    }else if(item_desc == ""){
                alert('Please Enter Item ..! ');return false;
    }
//    if(parseFloat(st_stock) < parseFloat($(list).val())){
//        toastr.error('Less Quantity in store');
//          $(list).val('');
//    }
//        var bch = btoa($(list).attr('batch-name').trim());
//        delete batch_array[item_code];
//        batch_array[item_code] = {};
//        batch_array[item_code][bch] = $(list).val();
//        console.log(batch_array);
}


function get_product_batch(list, item_code, item_desc, qty, stock, detail_id) {
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var qty = $(list).closest("tr").find("input[name='lastRQty[]']").val();
    var approved_qty = qty;
    var item_qty = $(list).val();
    var item_array = JSON.stringify(batch_array);
    var item_desc = $(list).closest("tr").find(".item_desc").val();
    var item_code = $(list).closest("tr").find(".item_code_hidden").val();
    // if(from_location == "-1"){
    //             alert('Please Select Department ..! ');return false;
    // }else if(to_location == "-1"){
    //             alert('Please Select Store Name ..! ');return false;
    // }else if(item_desc == ""){
    //             alert('Please Enter Item ..! ');return false;
    // }
    // console.log(item_code + " --- " + item_desc);

    if (item_qty == '0') {
        item_qty = qty;
    }
    if (to_location != '') {
        var file_token = $('#hidden_filetoken').val();
        var param = {_token: file_token, from_location: from_location, detail_id: detail_id, item_code: item_code,item_desc: item_desc, to_location: to_location, qty: item_qty, stock: stock,item_array: item_array, batchData: 1};
        $.ajax({
            type: "POST",
            url: '',
            data: param,
            beforeSend: function () {
                $(list).closest("tr").find(".td_spin").removeClass('hide');
                $(list).closest("tr").find("input[name='req_qty[]']").hide();
            },
            success: function (result) { //alert(result['request_form']);return false;
                 $(".issue_qty_pop").html('');
                $(list).closest("tr").find(".td_spin").addClass('hide');
                $(list).closest("tr").find("input[name='req_qty[]']").show();
                $(list).closest("tr").find(".issue_qty_pop").html('');
                $(list).closest("tr").find(".issue_qty_pop").html(result);
                $(list).closest("tr").find(".issue_qty_pop").removeClass('hide');
                 $("#item_issueqtyid1").focus();
                //setTimeout(function () {
//                    $('.theadfix_wrapper').floatThead({
//                        position: 'absolute',
//                        scrollContainer: true
//                    });
//                }, 400);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
            $("#item_issueqtyid1").focus();
            },
            complete: function () {
                $(list).closest("tr").find("input[name='req_qty[]']").removeClass('fa fa-spinner fa-spin');
                $('#modal_desc').html(item_desc);
                // if (balance_qty == 0 && approved_qty != 0) {
                //     $(".issueqtychnges").attr('readonly', true);
                //     $("#add_batch_button").hide();
                // }
            }
        });
    } else {
        toastr.error('Store Name Is not Added');
    }
}
function list_grn_items(e,grn_head_id){
  var added=false;
  var url = "{{route('extensionsvalley.purchase.addAjaxNewDeptStockItem')}}";
  var fromLocationPub = $('#from_location').val();
  $.ajax({
      type: "GET",
      url: url,
      data:  'fetch_with_grn_detail_id=' +grn_head_id+'&fromLocation='+fromLocationPub,
      beforeSend: function () {
          $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
      },
      success: function (data) {
        if(data != 0){
            $.map(arr, function(elementOfArray, indexInArray) {
            if (elementOfArray.id == grn_head_id) {
            added = true;
            $("."+grn_head_id).remove();
            arr = arr.filter(function( obj ) {
            return obj.id !== grn_head_id;
            });
            }
            });
    if(!added){
        arr.push({id: grn_head_id});
        $(e).closest('tr').after(data);
    }
        }else{
          alert("No Data Found!");return false;
        }
      },
      complete: function () {
        $.LoadingOverlay("hide");
      }

  });

}
//function list_grn_items(grn_head_id){
//
//  var url = "{{route('extensionsvalley.purchase.addAjaxNewDeptStockItem')}}";
//  var fromLocationPub = $('#from_location').val();
//  $.ajax({
//      type: "GET",
//      url: url,
//      data:  'grn_head_id=' +grn_head_id+'&fromLocation='+fromLocationPub,
//      beforeSend: function () {
//          $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
//      },
//      success: function (data) {
//        if(data != 0){
//          var deleteIDFlag = 0;
//          var confirm_flag = 0;
//              $(".deleteID" ).each(function() {
//                var item_val = $(this).closest("tr").find(".item_desc").val();
//                if($(this).val() == 0){
//                  if(item_val != "")
//                        deleteIDFlag = 1;
//                }
//              });
//          if(deleteIDFlag == 1){
//              if(confirm('Are you sure wanted to delete the items entered?')){
//                  confirm_flag = 1;
//              }else{
//                  confirm_flag = 0;
//              }
//          }
//          $(".item_desc" ).each(function() {
//                    if($(this).val() == ""){
//                      var id = $(this).closest("tr").attr('id');
//                      $("#"+id+"").remove();
//                      $("#grnlist_modal").modal('hide');
//                    }else{
//                      if(confirm_flag == 1){
//                        var deleteID = $(this).closest("tr").find(".deleteID").val();
//                        if(deleteID == 0){
//                                    var id = $(this).closest("tr").attr('id');
//                                    $("#"+id+"").remove();
//                                  $("#grnlist_modal").modal('hide');
//                          }
//                      }
//                    }
//          });
//              var rc = 0;
//              var rc_id = '';
//          $.each(data, function(key, value) {
//                var input_itm_id = addNewRowByDefault(1);
//                $.each(value, function(ky, val) {
//                    if(ky == 'item_desc')
//                        $('#'+input_itm_id).val(val);
//                    if(ky == 'item_code'){
//                        $('#'+input_itm_id).closest("tr").find("input[name='item_code_hidden[]']").val(val);
//                        val_id_itm = $('#'+input_itm_id).closest("tr").find("input[name='item_desc[]']").attr('id');
//                        $('#'+input_itm_id).closest("tr").find("input[name='item_desc[]']").addClass('grnItemClass');
//                    }
//                    if(ky == 'uom_name')
//                        $('#'+input_itm_id).closest("tr").find("label[for='uom[]']").text(val);
//                    if(ky == 'grn_unit')
//                        $('#'+input_itm_id).closest("tr").find("input[name='uom[]']").val(val);
//                    if(ky == 'r_depstock')
//                        $('#'+input_itm_id).closest("tr").find("label[for='stock[]']").text(val);
//                    if(ky == 'r_ststock')
//                        $('#'+input_itm_id).closest("tr").find("label[for='st_stock[]']").text(val);
//                    // if(ky == 'r_globalstock')
//                    //     $('#'+input_itm_id).closest("tr").find("label[for='global_stock[]']").text(val);
//                    if(ky == 'last_requested_qty')
//                        $('#'+input_itm_id).closest("tr").find("label[for='lastRQty[]']").text(val);
//                if(rc == 0){
//                          rc_id = val_id_itm;
//                        }
//                });
//                rc++;
//            });
//          if(rc_id != ''){
//           $('#'+rc_id).closest("tr").find(".request_req_qty").focus();
//           $('#'+rc_id).closest("tr").find(".request_req_qty").click();
//          }
//        }else{
//          alert("No Data Found!");return false;
//        }
//      },
//      complete: function () {
//        $.LoadingOverlay("hide");
//      }
//
//  });
//
//}

function showItemReqHistory(list){
    var from_location = $('#from_location').val();
    var item_desc = $(list).closest("tr").find(".item_desc").val();
    var item_code = $(list).closest("tr").find(".item_code_hidden").val();
    if(from_location == ""){
        toastr.error('Select Department!');
    }else if(item_desc == ""){
        toastr.error('Select Any Item!');
    }else if(item_code == ""){
        toastr.error('Enter Valid Item!');
    }else{
        $.ajax({
            type: "GET",
            url: '',
            data: 'item_req_history=1'+'&item_code='+item_code+'&from_location='+from_location,
            beforeSend: function () {
                $(list).closest("tr").find(".bttn_spin").removeClass('hide');
                $(list).closest("tr").find(".bttn_cls").hide();
            },
            success: function (ret) {
                $(list).closest("tr").find(".bttn_spin").addClass('hide');
                $(list).closest("tr").find(".bttn_cls").show();
                $("#item_req_history_div").html('');
                $("#item_req_history_div").html(ret.html);
                $(".item_history_title").html(ret.item_desc+" - "+item_code);
                if(ret.status == 1)
                    $('#item_hist_req_modal').modal('show');
                else
                    toastr.warning('No history exists for this item!');
            },
            complete: function () {
                 setTimeout(function(){
                        $(".theadfix_wrapper").floatThead('reflow');
                    },300);
                $(list).closest("tr").find(".bttn_cls").removeClass('fa fa-spinner fa-spin');
            }
        });
    }
}

function clearReorderLevelSearch(){
    $('#reorder_item_search').val("");
    $('#chk_zero_stock').attr("checked",false);
    reorderLevelItems(0);
}

function reorderLevelItems(page){

    var fromLocation = $('#from_location').val();
    var toLocation = $('#to_location').val();
    var reorder_item_search = $('#reorder_item_search').val();
    if(fromLocation == "-1"){
                alert('Please Select Department ..! ');return false;
    }else if(toLocation == "-1"){
                alert('Please Select Store Name ..! ');return false;
    }
    var chk_zero_stock = 0;
    if($('#chk_zero_stock').prop("checked") == true) {
        chk_zero_stock = 1;
    }else{
        chk_zero_stock = 0;
    }
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'reorderLevelItems=1'+'&fromLocation='+fromLocation+'&toLocation='+toLocation+'&reorder_item_search='+reorder_item_search+'&chk_zero_stock='+chk_zero_stock+'&page_value='+page,
        beforeSend: function () {
            $("#fillreorderLevellist").html('');
            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (html) { //alert(html);
            if($('#reorder_level_items_modal').hasClass('show')){
                console.log($('#reorder_level_items_modal').hasClass('show'));
            }else{
              $("#reorder_level_items_modal").modal('show');
            }
            if(html){
                $("#fillreorderLevellist").html(html);
            }else{
                $("#fillreorderLevellist").append("<tr><td colspan='8'>No Re-order Level Reached Items For this Department Found!</td></tr>");
            }
            $('.item_code_hidden').each(function(){
                var set=$(this).val();
                $('.selected_item'+set).prop('checked',true);
            })
            var count=0;
    $('.chk_reorder_items').each(function(){
        if($(this).is(':checked')){
      count +=1;
    }
    })
    console.log(count);
        if(count==30){
            $('#select_all_item').prop('checked',true)
        }
        },
        complete: function (){
            $.LoadingOverlay("hide");
            setTimeout(function(){
                $(".theadfix_wrapper").floatThead('reflow');
            },2000);
        }
    });
}

function list_reorder_level_items(obj,item_code,item_desc,r_ststock,r_depstock,r_globalstock,uom_id,uom_name,last_requested_qty,reorder_level){

    if($(obj).prop("checked") == true) {
        var item_code_exts = 0;
    $('#addNewItem tr').each(function () {
            item_code_temp = $(this).find(".item_code_hidden").val();
            if(item_code == item_code_temp){
                item_code_exts = 1;
            }
    });
    if(item_code_exts == 1){
        toastr.error("Selected Item Is Already Existing!");
        return false;
    }

        $(".deleteID" ).each(function() {
                var item_val = $(this).closest("tr").find(".item_desc").val();
                if($(this).val() == 0){
                  if(item_val == ""){
                        var tr_id = $(this).closest("tr").attr('id');
                        $("#"+tr_id+"").remove();
                    }
                }
        });
        var input_itm_id = addNewRowByDefault(1);
        $('#'+input_itm_id).val(item_desc);
        $('#'+input_itm_id).closest("tr").addClass('added'+item_code);
        $('#'+input_itm_id).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
        val_id_itm = $('#'+input_itm_id).closest("tr").find("input[name='item_desc[]']").attr('id');
        $('#'+input_itm_id).closest("tr").find("label[for='uom[]']").text(uom_name);
        $('#'+input_itm_id).closest("tr").find("input[name='uom[]']").val(uom_id);
        $('#'+input_itm_id).closest("tr").find("label[for='stock[]']").text(r_depstock);
        $('#'+input_itm_id).closest("tr").find("label[for='st_stock[]']").text(r_ststock);
        // $('#'+input_itm_id).closest("tr").find("label[for='global_stock[]']").text(r_globalstock);
        $('#'+input_itm_id).closest("tr").find("label[for='lastRQty[]']").text(last_requested_qty);
        $('#'+input_itm_id).closest("tr").find("label[for='reorder_level[]']").text(reorder_level);
    }else{
        console.log('asdsdsd');
        $('.added'+item_code).remove();
    }
}


function printData(head_id) {
    url = "";
    url = $("#req_base_url").val();
    urls = url + "/purchase/dept_issue_print_out?dept_head_id=" + head_id;
    window.open(urls, 'div', 'height=3508,width=2480');
}
function checkAllGrnDtls(e,head_id) {
   // var checkboxes = $('.chk_all_grn_items_dtls');
    if($(e).is(':checked')) {
       // checkboxes.each(function(){
           $(".chk_all_grn_items_dtls_"+head_id).trigger("click");
           $(".chk_all_grn_items_dtls_"+head_id).attr('checked', true);
           //$(".chk_all_grn_items_dtls").attr('checked', $(this).attr('checked'));
            //$(".chk_all_grn_items_dtls").attr("checked", true).trigger("click");
       // });
    }else{
           $(".chk_all_grn_items_dtls_"+head_id).trigger("click");
           $(".chk_all_grn_items_dtls_"+head_id).attr('checked', false);
           //$(".chk_all_grn_items_dtls").attr('checked', $(this).attr('checked'));
    }
}
function list_grn_level_items(obj,batch,item_code,item_desc,r_ststock,r_depstock,r_globalstock,uom_id,uom_name,last_requested_qty,r_grn_qty,grn_head_id,reorder_level,){
   var item_code_exts = 0;
    $('#addNewItem tr').each(function () {
            item_code_temp = $(this).find(".item_code_hidden").val();
            if(item_code == item_code_temp){
                item_code_exts = 1;
                var tr_id = $(this).closest("tr").attr('id');
                 $("#"+tr_id+"").remove();
            }
    });
    if(item_code_exts == 1){
                //if($(this).val() == 0){ alert("ggg");
                  //if(item_val == ""){
                        var tr_id = $(this).closest("tr").attr('id');
                        $("#"+tr_id+"").remove();
                    //}
                //}
    }

    if($(obj).prop("checked") == true) {
        $(".deleteID" ).each(function() {
                var item_val = $(this).closest("tr").find(".item_desc").val();
                if($(this).val() == 0){
                  if(item_val == ""){
                        var tr_id = $(this).closest("tr").attr('id');
                        $("#"+tr_id+"").remove();
                    }
                }
        });
        var input_itm_id = addNewRowByDefault(1);
        $('#'+input_itm_id).val(atob(item_desc));
        $('#'+input_itm_id).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
        val_id_itm = $('#'+input_itm_id).closest("tr").find("input[name='item_desc[]']").attr('id');
        $('#'+input_itm_id).closest("tr").find("label[for='uom[]']").text(uom_name);
        $('#'+input_itm_id).closest("tr").find("input[name='uom[]']").val(uom_id);
        $('#'+input_itm_id).closest("tr").find("label[for='stock[]']").text(r_depstock);
        $('#'+input_itm_id).closest("tr").find("label[for='st_stock[]']").text(r_ststock);
        // $('#'+input_itm_id).closest("tr").find("label[for='global_stock[]']").text(r_globalstock);
        $('#'+input_itm_id).closest("tr").find("label[for='lastRQty[]']").text(last_requested_qty);
        $('#'+input_itm_id).closest("tr").find("label[for='reorder_level[]']").text(reorder_level);
        $('#'+input_itm_id).closest("tr").find("input[name='req_qty[]']").val(r_grn_qty);
        var grn_batc_array = {};
        var grn_batc_array1 = [];
        grn_batc_array = batch.split(',');
        delete batch_array[item_code];
        delete grn_array_data[item_code];
        batch_array[item_code] = {};
        grn_array_data[item_code] = {};
        for (const [key, value] of Object.entries(grn_batc_array)) {
          grn_batc_array1.push(value);
        }
        for(var j = 0;j < grn_batc_array1.length;j++){
            var sp_arr = grn_batc_array1[j].split(':');
            var grn_bat_name = btoa((sp_arr[0]).trim());
            var grn_stock = (sp_arr[1]).trim();
            batch_array[item_code][grn_bat_name] = grn_stock;
          }
          grn_array_data[item_code] = grn_head_id;
    }else{
        grn_array_data[item_code] ={};
    }
}
$(document).on('change','#select_all_item',function(){
    if($('#select_all_item').is(':checked')){
        $('.chk_reorder_items').prop('checked',false);
         $('.chk_reorder_items').click();
    }else{
        $('.chk_reorder_items').prop('checked',false);
        $('.removeTr').remove();
        addNewRowByDefault(0);
    }
})
function changeLocationCheck(e){
var selected_loc = $(e).val();
var dflt_ret_store_chk = $("#dflt_ret_store_chk").val();
// if(selected_loc == 'IPH001'){
//     $("#from_location").val('OTPIPS').select2();
//     $("#from_location option").each(function (index) {
//         if ( $(this).val() == 'OTPIPS') {
//             $(this).prop('disabled', false);
//          }
//          else {
//             $(this).prop('disabled', true);
//          }
//       });
// }else{
//     $("#from_location option").prop('disabled', false);
//     $("#from_location").val(dflt_ret_store_chk).select2();
// }

var url = "{{route('extensionsvalley.purchase.getToLocation')}}";
var loc_searchstring = '';
$.ajax({
            type: "POST",
            url: url,
            data: 'selected_loc='+selected_loc+'&selecting_store=0',
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                var datas = JSON.parse(data);
                $.each(datas, function (key, val) {
                        var sel = '';
                        if (dflt_ret_store_chk == val.location_code)
                        {
                            sel = 'selected';
                        } else {
                            sel = '';
                        }
                        loc_searchstring += "<option " + sel + " value='" + val.location_code + "'>" + val.location_name + "</option>";
                    });
            },
            complete: function (){
                $.LoadingOverlay("hide");
                var html_data = '<label for="">To Location</label><div class="clearfix"></div>'+
                '<select class=""form-control from_location select2" id="from_location" name="from_location">' + loc_searchstring + '</select>';
                $("#append_to_loc").html(html_data);
                $("#from_location").select2();
               // getStoreLocationType();
            }

        });

}
function getStoreLocationType(){
    var url = "{{route('extensionsvalley.purchase.getStoreLocationType')}}";
    var to_location = $("#to_location").val();
var loc_searchstring = '';
$.ajax({
            type: "POST",
            url: url,
            data: 'selected_loc='+to_location+'&selecting_store=1',
            beforeSend: function () {
            },
            success: function (data) {
                $("#location_type").val(data);
            },
            complete: function (){

            }

        });
}
</script>

@endsection
