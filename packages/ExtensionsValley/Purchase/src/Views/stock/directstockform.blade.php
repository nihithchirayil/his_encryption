@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px;font-weight: bold;"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.purchase.listDirectStockItems')}}" id="requestSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Item Name</label>
                                   <input type="text" autocomplete='off' class="form-control" name="item_code" id="item_code"
                                       value="{{ $searchFields['item_code'] ?? '' }}">
                                </div>
                                 <div id="item_code_searchCodeAjaxDiv" class="ajaxSearchBox" ></div>
                            </div>

                               <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location</label>
                                <div class="clearfix"></div>
                               {!! Form::select('location', array("0"=> " Select location") + $location->toArray(),null, [
                            'class'       => 'form-control location',
                            'id'    => 'location'
                        ]) !!}

                            </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="from_date" id="from_date"
                                       value="{{ $searchFields['from_date'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="to_date" id="to_date"
                                       value="{{ $searchFields['to_date'] ?? '' }}">
                                   </div>
                            </div>



                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning">
                                <i class="fa fa-times"></i>  Clear</a>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="addDirectStockNew();" class="btn btn-block btn-primary" ><i class="fa fa-plus" ></i> Add</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg" style="cursor: pointer;">
                                    <th>Updated Date</th>
                                    <th>Item Name</th>
                                    <th>Batch</th>
                                    <th>Location</th>
                                    <th>Added Stock</th>

                                    <th>Selling Price</th>
                                    <th>MRP</th>
                                    <th>Expiry Date</th>
                                    <th>Remark</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if(count($item) > 0)
                                @foreach ($item as $item)
                                <?php //dd($item); ?>
                                <tr style="cursor: pointer;" >
                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->updated_at }}</td>
                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->item_desc }}</td>
                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->batch_no }}</td>
                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->location_name }}
                                    </td>
                                    <td style="text-align: left; width: 3%"  class="common_td_rules"> {{ $item->quantity }}</td>

                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->unit_selling_price }}</td>
                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->unit_mrp }}</td>
                                    <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->expiry_date }}</td>
                                    <td style="text-align: left; width: 3%"  class="common_td_rules">{{ $item->remark }}
                                    </td>

                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="12" class="location_code">No Records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
        </div>
    </div>
</div>

{!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
{!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

{!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
{!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function() {

    setTimeout(function() {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');

    }, 300);
    $(document).on('click', '.grn_drop_btn', function(e) {
    e.stopPropagation();
    $(".grn_btn_dropdown").hide();
    $(this).next().slideDown('');
    });
    $(document).on('click', '.btn_group_box', function(e) {
    e.stopPropagation();
    });
    $(document).on('click', function() {
    $(".grn_btn_dropdown").hide();
    });
    $(".select_button li").click(function() {
    $(this).toggleClass('active');
    });
    $(document).on('click', '.notes_sec_list ul li', function() {
    var disset = $(this).attr("id");
    $('.notes_sec_list ul li').removeClass("active");
    $(this).addClass("active");
    $(this).closest('.notes_box').find(".note_content").css("display", "none");
    $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
    });
    $('.month_picker').datetimepicker({
    format: 'MM'
    });
    $('.year_picker').datetimepicker({
    format: 'YYYY'
    });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.datepicker').datetimepicker({
    format: 'DD-MMM-YYYY'
    });
    $('.date_time_picker').datetimepicker();
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function($table) {
    return $table.closest('.theadscroll');
    }
    });
    })



            });</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function() {
    });
    function addDirectStockNew(){
    // var protocol = window.location.protocol;
    // var host_name = window.location.host;
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addDirectStockForm";
    var action_url = main_uri + route_path;
    document.location.href = action_url;
    // alert(action_url);
    }
    function stockItemEditLoadData(list, stock_id){
    // alert(stock_id);
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addDirectStockForm/" + stock_id;
    var action_url = main_uri + route_path;
    document.location.href = action_url;
    }


      $('#item_code').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
    var item_code = $(this).val();
    item_code = item_code.trim();
    if (item_code == "") {
    $("#item_code_searchCodeAjaxDiv").html("");
    } else {
    try {
    var param = {item_code_search: item_code};
    $.ajax({
    type: "GET",
            url: '',
            data: param,
            beforeSend: function () {
            $("#item_code_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
            if (html == 'No Result Found') {
            }
            $("#item_code_searchCodeAjaxDiv").html(html).show();
            $("#item_code_searchCodeAjaxDiv").find('li').first().addClass('liHover');
            },
            complete: function () {
            }
    });
    } catch (err) {
    document.getElementById("demo").innerHTML = err.message;
    }
    }
    } else {
    ajax_list_key_down('item_code_searchCodeAjaxDiv', event);
    }
    });
    $('#item_code').on('keydown', function (event) {
    if (event.keyCode === 13) {
    ajaxlistenter('item_code_searchCodeAjaxDiv');
    return false;
    }
    });
    function fill_desc_details(e, item_desc) {
    $('#item_code').val(item_desc);
    $('#item_code_searchCodeAjaxDiv').hide();
    }




@include('Purchase::messagetemplate')

</script>

@endsection
