@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .search_box_bottom{
        bottom: -30px;
    }
    .ajax_search_box_bottomcls .ajaxSearchBox{
        bottom: 3px;
        height: 200px !important;
    }
    .ajax_search_box_bottomcls_reduce_box{
        height: 200px !important;
    }
    table td{
        position: relative !important;
    }
</style>
@endsection
@section('content-area')
     <!-- page content -->
     <div class="right_col"  role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <input type="hidden" id="stock_return_base_url" value="{{URL::to('/')}}">
                <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                {!!Form::open(array('route' => 'extensionsvalley.purchase.updateStockReturn', 'method' => 'post', 'class' => 'formShortcuts', 'id' => 'requisitionForm'))!!}
                <div class="col-md-12 no-padding">
                <div class="box no-border no-margin" >
                    <div class="box-body clearfix">
                           <div class="col-md-2 padding_sm">
                            @if(isset($item[0]) && $item[0] !='')
                            <div class="mate-input-box">
                                <label for="">Return No.</label>
                                <div class="clearfix"></div>
                                <span>{{isset($item[0]->return_no) ? $item[0]->return_no : ''}}</span>
                                <input type="hidden" class="form-control" name="return_no" id="return_no" value="{{isset($item[0]->return_no) ? $item[0]->return_no : ''}}" readonly="">
                                 <input type="hidden" class="form-control" name="request_stock_head_id" id="request_stock_head_id" value="{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}" readonly="">
                             </div>
                            @endif
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Return From</label>
                                <div class="clearfix"></div>
                                {!! Form::select('from_location', array("-1"=> " Return From") + $location->toArray(),isset($item[0]->from_location_code) ? $item[0]->from_location_code :$default_location, [
                            'class'       => 'form-control from_location select2',
                            'id'    => 'from_location',
                            'onchange' => 'changeLocationCheck(this)'
                        ]) !!}
                    </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box" id="append_to_loc">
                                <label for="">Return To</label>
                                <div class="clearfix"></div>
                                <?php
                            $returnloc = \ExtensionsValley\Purchase\CommonInvController::getLocationByroleToReturn();
                            $dflt_ret_store = \WebConf::getConfig('default_return_store');
                            ?>
                               {!! Form::select('to_location', array("-1"=> " Return To") + $returnloc->toArray(),isset($item[0]->to_location_code) ? $item[0]->to_location_code :$dflt_ret_store, [
                            'class'       => 'form-control to_location select2',
                            'id'    => 'to_location'
                        ]) !!}
                        <input type="hidden" value="{{isset($item[0]->to_location_code) ? $item[0]->to_location_code :$dflt_ret_store}}" id="dflt_ret_store_chk" >
                    </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Reason</label>
                                <div class="clearfix"></div>
                               {!! Form::select('reason_head', array("-1"=> " Select") + $reason_data->toArray(),isset($item[0]->reason_head) ? $item[0]->reason_head :null, [
                                            'class'       => 'form-control reason_head',
                                            'id'    => 'reason_head'
                                        ]) !!}
                                    </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                     <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                       <textarea  id="requisition_comments" class="form-control" wrap="off" cols="35" rows="1" name="remarks" >{{isset($item[0]->comments) ? $item[0]->comments :''}}</textarea>
                                   </div>
                            </div>
                            <input type="hidden" value="" id="location_type">
                            @if(!empty($item[0]->stock_id))
                            <div class="col-md-12 padding_sm text-right">
                                <div class="btn btn-dark" style="cursor: pointer;margin-top:1px;" onclick="printData('{{isset($item[0]->stock_id) ? $item[0]->stock_id : ''}}');">
                                <i class="fa fa-print" aria-hidden="true"></i> Print</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                <div class="theadscroll always-visible" style="position: relative; height: 420px;">

                <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="addNewItem" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg ">
                               <th width="2%">#</th>
                               <th width="15%">
                                    <input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Item Search.. " style="color: #000;display: none;width: 90%;" >
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Item Name</span>
                                </th>
                               <th width="3%">Item Unit</th>
                               <th width="7%">Batch</th>
                               <th width="3%">Stock</th>
                               <th width="3%">Reason</th>
                               <th width="3%">Return Qty</th>
                               <th width="3%"><button type="button" tabindex="-1" id="add"  class="btn btn-success add_row_btn" onclick="addNewRowByDefault()"><i class="fa fa-plus"></i></button></th>
                           </tr>

                       </thead>
                       <tbody id="fillrequestitemdetails">

                       </tbody>

            </table>
                </div>


                 <div class="clearfix"></div>
                    <div class="ht5"></div>
            </div>
                <div class="col-md-12">
                <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                <div class="col-md-1 padding_sm" style="float: right;">
                     <?php
                                        $status_value = '';
                                        $worksheet = '';
                                        $submit = '';
                                        $approve = '';
                                        $disable_prop = '';
                                        $status_value = isset($item[0]->status) ? $item[0]->status : '';
                                        if($status_value == 4){
                                            $status_value = 3;
                                        }
                                        foreach ($access_control as $key => $value) {
                                            foreach ($value as $k => $v) {
                                                if(($k == 'worksheet') && ($v == 1)){$worksheet = '1';
                                                }if(($k == 'submit') && ($v == 1)){ $submit = '1';
                                                }if(($k == 'approve') && ($v == 1)){  $approve = '1';
                                                }
                                            }
                                        }
                                        if($status_value == 3){
                                        $disable_prop = 'display:none';
                                        }
                                ?>
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="saveItem();" class="btn btn-block light_purple_bg saveitembtn" style="<?= $disable_prop ?>"><i class="fa fa-save"></i>
                                    Save</div>
                </div>
                <div class="col-md-2 padding_sm" style="float: right !important;">
                    <div class="mate-input-box">
                                <label for="">Select Status</label>
                                <div class="clearfix"></div>
                                <select name="status" id="status" class="form-control status">

                               @if($worksheet == 1)
                               <option @if($status_value == 1) selected @endif value="1">Worksheet</option>@endif
                               @if($submit == 1)
                               <option @if($status_value == 2) selected @endif value="2">Submitted</option>@endif
                               @if($approve == 1)
                               <option @if($status_value == 3) selected @endif value="3">Approved</option>@endif
                               </select>
                           </div>
                </div>
                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                               <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                </div>
            </div>
                </div>
            </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script type="text/javascript">
</script>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.js')}}"></script>
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    var j=1;
    batch_array = {};
    item_array = {};
    var previous;
(function () {
    $("#from_location").on('focus', function () {
    previous = this.value;
}).change(function() {
    if(previous == '-1' || previous == -1){
        @if(isset($status_value) && $status_value != '3')
        setTimeout(function(){
         addNewRowByDefault();
     },300);
    @endif
    }

});
})();
$(document).ready(function() {
$('body').on('keydown', '.request_req_qty', function(e) {
        var rowCounts = $('#addNewItem tr').length;
        var th = $(this).closest("tr").find("input[name='item_desc[]']").attr('id');;
        var ths = th.split('_');
        var rowCounts =  ths[2];
        var rowCountss = parseFloat(rowCounts)+1;
    if (e.which == 9) {  setTimeout(function(){
    $('#item_code_'+rowCountss).focus();
            },300);
    }
});
    $("#item_search_btn").click(function(){
      $("#issue_search_box").toggle();
       $("#item_search_btn_text").toggle();
    });

    @if(isset($item[0]->stock_id))
    $("#from_location").trigger('change');
        getStockRecords("{{$item[0]->stock_id}}");
    @else
        if($("#from_location").val() != '-1'){
            @if(isset($status_value) && $status_value != '3')
                addNewRowByDefault();
            @endif
        }
    setTimeout(function(){
    document.getElementById("item_code_1").focus();
    },2000);
    @endif

    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
    });

    $('body').on('change', '.item_batch', function(e) {
        var from_location = $("#from_location").val();
        var batch_no = $(this).val();
        var item_code_hidden = $(this).closest("tr").find("input[name='item_code_hidden[]']").val();
        var stocks = getStockBasedBatch(item_code_hidden,batch_no,from_location);
        $(this).closest("tr").find("label[for='stock[]']").text(stocks);
        $(this).parents("tr").find("input[name='req_qty[]']").focus();
        var item_desc_chk = $(this).parents("tr").find("input[name='item_desc[]']").val();
        var batch_contain = 0;
        var item_desc_contain = 0;
        var chk_new_row_create = 0;
        $('#addNewItem tr').each(function () {
            if ($(this).find('input[name="item_desc[]"]').val() == item_desc_chk) {
                item_desc_contain = item_desc_contain + 1;
            }
            if ($(this).find('input[name="item_batch[]"]').val() == batch_no) {
                batch_contain = batch_contain + 1;
            }
            if(batch_contain > 1 && item_desc_contain > 1){
                toastr.error("Error occured Please check data");
            }
            if ($(this).find('input[name="item_desc[]"]').val() ==''){
                chk_new_row_create = 1;
            }
        });

    });
   // getStoreLocationType();
});

 function addNewRowByDefault(){

           if(document.getElementById('from_location').value == "-1")
            {
                 alert('Please Select Return From ..! ');return false;
            } else if(document.getElementById('to_location').value == "-1")
            {
                 alert('Please Select Return To ..! ');return false;
            } else if(document.getElementById('to_location').value == document.getElementById('from_location').value)
            {
                alert('Error Please select location properly..!');return false;
            }

            var table = document.getElementById("addNewItem");
            var newrow = table.rows.length;
            var row = table.insertRow(newrow);
            row.id = "newRow" + newrow;
            <?php  $a = 1; $b = 0;
               foreach($incr_array as $inc) { ?>

                    var cell_no{{$a}} = row.insertCell("{{$b}}");

                    cell_no{{$a}}.innerHTML = "<?php  echo $inc; ?>";
            <?php
                    $a++; $b++;
                } ?>

            var reason_head = $("#reason_head").val();
            console.log(reason_head);
            cell_no2.firstChild.id = 'item_code_'+j;
            cell_no4.firstChild.id = 'item_batch_'+j;
            cell_no6.firstChild.id = 'return_reason_'+j;
            var text_input_id = cell_no2.firstChild.id;
            var text_input_id6 = cell_no6.firstChild.id;
            document.getElementById(text_input_id).nextSibling.id = 'ajaxSearchBox_'+j;
            $("#"+text_input_id6).val(reason_head);
            var text_input_id1 = cell_no4.firstChild.id;
            document.getElementById(text_input_id1).nextSibling.id = 'ajaxSearchBatchBox_'+j;
            if(j > 2){
                $('#ajaxSearchBox_'+j).addClass('ajax_search_box_bottomcls_reduce_box');
            }
            if(j > 7){
                $('#newRow'+j).addClass('ajax_search_box_bottomcls');
            }
            $(".item_batch").addClass('select2');
            j++;

 }

function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("addNewItem");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function searchItemBatch(id,event,list) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        var ajax_div = $('#'+id).next().attr('id');
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var fromLocationPub = $('#from_location').val();
            var toLocationPub = $('#to_location').val();
            var item_batch = $('#'+id).val();
            var item_code = $(list).closest("tr").find("input[name='item_code_hidden[]']").val();
            if (item_batch == "") {
                $("#"+ajax_div).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_batch=' +item_batch+'&item_code='+item_code+'&from_location='+fromLocationPub+'&to_location='+toLocationPub+'&batchData='+1,
                    beforeSend: function () {
                    $("#"+ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+ajax_div).html(html).show();
                        $("#"+ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            }
}

function searchItemCode(id,event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var current;
        var ajax_div = $('#'+id).next().attr('id');
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var fromLocationPub = $('#from_location').val();
            var toLocationPub = $('#to_location').val();
            var item_code = $('#'+id).val();
            if (item_code == "") {
                $("#"+ajax_div).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_code=' +item_code+'&from_location='+fromLocationPub+'&to_location='+toLocationPub+'&itemSearchData='+1,
                    beforeSend: function () {
                    $("#"+ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+ajax_div).html(html).show();
                        $("#"+ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            }
}

function fillNoItem(list){
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#'+itemCodeListDivId).hide();
}

function fillItemValues(list,item_code,item_desc,uom_name,decimal_qty_ind) {

   var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(item_desc);
    $('#'+itemCodeListDivId).hide();
    $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    $('#'+itemCodeTextId).closest("tr").find("input[name='decimal_qty_ind_hidden[]']").val(decimal_qty_ind);
    $('#'+itemCodeTextId).closest("tr").find("label[for='uom[]']").text(uom_name);
    // $('#'+itemCodeTextId).parents("tr").find("input[name='item_batch[]']").focus();
    $('#'+itemCodeTextId).parents("tr").find(".item_batch").focus();
    //$(".item_batch").focus();
    var fromLocationPub = $('#from_location').val();
    var toLocationPub = $('#to_location').val();
    var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
        $.ajax({
        type: "GET",
            url: url,
            data:  'item_code='+item_code+'&from_location='+fromLocationPub+'&to_location='+toLocationPub+'&batchData='+1,
            beforeSend: function () {
            },
            success: function (res) {
                console.log(res.length > 0);
                if (res.length > 0){
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").empty();
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").append('<option value=""> Select Batch</option>');
                    for(var i=0;i<res.length;i++){
                        $('#'+itemCodeTextId).parents("tr").find(".item_batch").append('<option value="' + res[i].batch_no + '" >' + res[i].batch_no + " [ "+ res[i].expiry_date + " ]" + " [ "+ res[i].stock + " ]" + '</option>');
                     }
                    } else{
                    $('#'+itemCodeTextId).parents("tr").find(".item_batch").empty();
                    }
            },
            complete: function () {
            }
        });
}

function fillItemBatchValues(list,batch_no,expiry_date,stock){
    var from_location = $("#from_location").val();
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(batch_no);
    $('#'+itemCodeListDivId).hide();

    $('#'+itemCodeTextId).closest("tr").find("input[name='item_batch_hidden[]']").val(batch_no);
    $('#'+itemCodeTextId).closest("tr").find("input[name='item_batch_exp_hidden[]']").val(expiry_date);
    var item_code_hidden = $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val();
    var stocks = getStockBasedBatch(item_code_hidden,batch_no,from_location);
    $('#'+itemCodeTextId).closest("tr").find("label[for='stock[]']").text(stocks);
    //$(".request_req_qty").focus();
    $('#'+itemCodeTextId).parents("tr").find("input[name='req_qty[]']").focus();
    var item_desc_chk = $('#'+itemCodeTextId).parents("tr").find("input[name='item_desc[]']").val();
    var batch_contain = 0;
    var item_desc_contain = 0;
    var chk_new_row_create = 0;
    $('#addNewItem tr').each(function () {
        if ($(this).find('input[name="item_desc[]"]').val() == item_desc_chk) {
            item_desc_contain = item_desc_contain + 1;
        }
        if ($(this).find('input[name="item_batch[]"]').val() == batch_no) {
            batch_contain = batch_contain + 1;
        }
        if(batch_contain > 1 && item_desc_contain > 1){
            toastr.error("Error occured Please check data");
        }
        if ($(this).find('input[name="item_desc[]"]').val() ==''){
            chk_new_row_create = 1;
        }
    });

}
function getStockBasedBatch(item_code_hidden,batch_no,from_location){
    var ret_data = '';
    var url = "{{route('extensionsvalley.purchase.addAjaxNewStockReturnItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    async: false,
                    data:  'item_code_hidden_stock_only=' +item_code_hidden+'&batch_no_stock_only='+batch_no+'&from_location_stock_only='+from_location+'&stock_only='+1,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        ret_data = data;
                    },

                });
                return ret_data;
}
function deleteRow(list){
   var item_name = $(list).closest("tr").find(".item_desc").val();
    if(item_name == ""){
        var id = $(list).closest("tr").attr('id');
        $("#"+id+"").remove();
    }else{
        if (confirm('Are you sure wanted to delete?')) {
                var id = $(list).closest("tr").attr('id');
                $("#"+id+"").remove();
        }
    }
}

function deleteEditRow(list,id){
     if (confirm('This item will be deleted immediately , are you sure to delete?')) {
            var url = '';
        $.ajax({
            type: "GET",
            url: url,
            data: 'stockrequestdelID='+id,
            beforeSend: function () {
            },
            success: function (data) { //alert(html);
                if(data==1){
                    var d = $(list).closest("tr").attr('id');
                    $("#"+d+"").remove();
                }
            }
        });

        }
}

function getStockRecords(stock_id){
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'stockItemDetails='+stock_id,
        beforeSend: function () {
        },
        success: function (html) { //alert(html);
        $("#fillrequestitemdetails").html(html);
         @if(isset($status_value) && $status_value != '3')
        addNewRowByDefault();
        @endif
            setTimeout(function(){
            document.getElementById("item_code_1").focus();
        },2000);
        }
    });
}

function saveItem(){
        var status=$('#status').val();
        if(status==''||status== null){
         toastr.warning("Please select Any status");
         return;
        }
        var flag  = 0;
        var f  = 0;
        $(".request_req_qty").each(function () {
            var item = $(this).closest("tr").find(".item_desc").val();
            if(item != ""){
                if (isNaN(this.value) || this.value == '' || this.value == 0){
                        f  = 1;
                    }
            }else{
                if (this.value != '' || this.value != 0){
                            flag = 1;
                }
            }
        });
        if(f == 1){
                alert("Request quantity is zero"); return false;
        }
        if(document.getElementById('from_location').value == "-1")
        {
             alert('Please Enter Return From ..! ');
             return false;

        } else if(document.getElementById('to_location').value == "-1")
        {
             alert('Please Enter Return To ..! ');return false;
        } else if(document.getElementById('to_location').value == document.getElementById('from_location').value)
        {
            alert('Error Please select location properly..!');return false;
        }

        var table = document.getElementById("addNewItem");
        var count_row = table.rows.length;

        // $(".item_desc" ).each(function() {
        //         if($(this).val() == ""){
        //             f = 1;
        //         }
        // });

        if(flag == 1 || count_row == 1){
                 alert('Please Enter Item ..! ');
                 return false;
        }else{
                $(".saveitembtn").hide();
                $("#requisitionForm").submit();
        }
}

function raiseRequestNewReset(){
    $("#itm_name").val('');
    $("#itm_qty").val('');
    $("#itm_desc").val('');
}

function raiseRequestNewItem(){
    var url = "{{route('extensionsvalley.purchase.addNewDeptStockItemRequest')}}";
    // window.open(url,'_blank');
    var itm_name = $("#itm_name").val();
    var itm_qty = $("#itm_qty").val();
    var itm_desc = $("#itm_desc").val();
    var from_location = $("#from_location").val();
    if(from_location == "-1"){
                alert('Please Enter From Location ..! ');return false;
    }else if(itm_name == ""){
                alert('Please Enter Item ..! ');return false;
    }else if(itm_qty == ""){
                alert('Please Enter Item Quantity..! ');return false;
    }

    $.ajax({
        type: "GET",
        url: url,
        data: 'itm_name='+itm_name+'&itm_qty='+itm_qty+'&itm_desc='+itm_desc+'&from_location='+from_location,
        beforeSend: function () {
        },
        success: function (data) { //alert(data);
            if(data != 0){
                alert("New Item Requested Successfully..");
            }else{
                alert("New Item Not Requested Successfully..");
            }
        },
        complete: function (){
            $("#itm_name").val('');
            $("#itm_qty").val('');
            $("#itm_desc").val('');
            $("#raiseNewItemRequest").modal('hide');
        }
    });
}

function requestGrn(){
    // $("#grnlist_modal").modal('show');return false;
    var fromLocation = $('#from_location').val();
    @if(isset($item[0]->stock_id))
        var req_head_id = "{{$item[0]->stock_id}}";
    @else
        var req_head_id = 0;
    @endif
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'pendingItemDetails='+req_head_id+'&fromLocation='+fromLocation,
        beforeSend: function () {
            $("#fillgrnlist").html('');
        },
        success: function (html) { //alert(html);
            $("#grnlist_modal").modal('show');

            if(html){
                $("#fillgrnlist").html(html);
            }else{
                $("#fillgrnlist").append("<tr><td colspan='8'>No GRN List Found!</td></tr>");
            }
        },
        complete: function (){
            setTimeout(function(){
                $(".theadfix_wrapper").floatThead('reflow');
            },300);
        }
    });
}

function pendrequest(){
    var fromLocation = $('#from_location').val();
    @if(isset($item[0]->stock_id))
        var req_head_id = "{{$item[0]->stock_id}}";
    @else
        var req_head_id = 0;
    @endif
    var url = '';
    $.ajax({
        type: "GET",
        url: url,
        data: 'pendingItemDetails='+req_head_id+'&fromLocation='+fromLocation,
        beforeSend: function () {
            $("#fillpendingitemdetails").html('');
        },
        success: function (html) { //alert(html);
            $("#pend_req_modal").modal('show');
            // $(".theadfix_wrapper").floatThead('reflow');

            if(html){
                $("#fillpendingitemdetails").html(html);
            }else{
                $("#fillpendingitemdetails").append("<tr><td colspan='8'>No pending request found!</td></tr>");
            }
        },
        complete: function (){
            setTimeout(function(){
                $(".theadfix_wrapper").floatThead('reflow');
            },300);
        }
    });
}

function hide_batch_div(list) {
    $(list).closest("tr").find(".issue_qty_pop").addClass('hide');
}

function add_batch_list(list) {

        var item_code = $(list).closest("tr").find(".item_code_hidden").val();
        var issue_total = 0;
        var issue_qty_get = 0;
        batch_array[item_code] = {};
        $(".batchdata").each(function (key, val) {
            var item_id = val.value;
            var bat_name = btoa($(list).closest("tr").find('.itembatch').val().trim());
            issue_qty_get = $(list).closest("tr").find('.issueqtychnges').val().trim();
            batch_array[item_code][bat_name] = issue_qty_get;
            issue_total += parseInt(issue_qty_get);
        });

        var req_qty = $('#requested_qtytot').html();
        if (req_qty) {
            $(list).closest("tr").find("input[name='req_qty[]']").val(req_qty);
        } else {
            $(list).closest("tr").find("input[name='req_qty[]']").val(0);
        }
        $(list).closest("tr").find(".issue_qty_pop").addClass('hide');


}

function chk_batch_item(list){
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var item_desc = $(list).closest("tr").find(".item_desc").val();
    if(from_location == "-1"){
                alert('Please Enter From Location ..! ');return false;
    }else if(to_location == "-1"){
                alert('Please Enter To Location ..! ');return false;
    }else if(item_desc == ""){
                alert('Please Enter Item ..! ');return false;
    }
}

function get_product_batch(list, item_code, item_desc, qty, stock, detail_id) {
    var from_location = $('#from_location').val();
    var to_location = $('#to_location').val();
    var qty = $(list).closest("tr").find("input[name='lastRQty[]']").val();
    var approved_qty = qty;
    var item_qty = $(list).val();
    var item_array = JSON.stringify(batch_array);
    var item_desc = $(list).closest("tr").find(".item_desc").val();
    var item_code = $(list).closest("tr").find(".item_code_hidden").val();

    console.log(item_code + " --- " + item_desc);

    if (item_qty == '0') {
        item_qty = qty;
    }
    if (to_location != '') {
        var file_token = $('#hidden_filetoken').val();
        var param = {_token: file_token, from_location: from_location, detail_id: detail_id, item_code: item_code,item_desc: item_desc, to_location: to_location, qty: item_qty, stock: stock,item_array: item_array, batchData: 1};
        $.ajax({
            type: "POST",
            url: '',
            data: param,
            beforeSend: function () {
                $(list).closest("tr").find(".td_spin").removeClass('hide');
                $(list).closest("tr").find("input[name='req_qty[]']").hide();
            },
            success: function (result) { //alert(result['request_form']);return false;
                $(list).closest("tr").find(".td_spin").addClass('hide');
                $(list).closest("tr").find("input[name='req_qty[]']").show();
                $(list).closest("tr").find(".issue_qty_pop").html('');
                $(list).closest("tr").find(".issue_qty_pop").html(result);
                $(list).closest("tr").find(".issue_qty_pop").removeClass('hide');

                setTimeout(function () {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });

            },
            complete: function () {
                $(list).closest("tr").find("input[name='req_qty[]']").removeClass('fa fa-spinner fa-spin');
                $('#modal_desc').html(item_desc);
                // if (balance_qty == 0 && approved_qty != 0) {
                //     $(".issueqtychnges").attr('readonly', true);
                //     $("#add_batch_button").hide();
                // }
            }
        });
    } else {
        toastr.error('To Location Is not Added');
    }
}
function number_validation(e) {
    // var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
    //         val = e.value;
    // if (!valid) {
    //     e.value = val.substring(0, val.length - 1);
    // }
    var decimal_qty_ind_hidden = $(e).closest("tr").find("input[name='decimal_qty_ind_hidden[]']").val();
    if(decimal_qty_ind_hidden==1){
        var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
        val = e.value;
        if (!valid) {
            e.value = val.substring(0, val.length - 1);
            $(e).val('');
        }
    }else{
    var valid = /^\d{0,12}$/.test(e.value),
    val = e.value;
    if (!valid) {
    e.value = val.substring(0, val.length - 1);
    $(e).val('');
        }
    }
}
function checkStock(e){
   var chk_new_row_create = 0;
   var stock = $(e).parents("tr").find(".stock_label").html();
   var ret_val = $(e).val();
//    if(/\D/.test(ret_val)){
//        toastr.error("Only numeric allowed");
//        $(e).val('');
//        chk_new_row_create = 1;
//    }
   if((parseFloat(stock) < parseFloat(ret_val)) || stock == ''){
       toastr.error("Please check available stock");
       $(e).val('');
       chk_new_row_create = 1;
   }
       var chk_btch = $(e).parents("tr").find(".reason_all").val();
    if(chk_btch == '-1'){
         toastr.warning("Please add reason !!");
         $(e).val('');
         chk_new_row_create = 1;
    }
    if(chk_new_row_create == 0){
         var ck = 0;
            $('#addNewItem tr').each(function () {
        if ($(this).find('input[name="item_desc[]"]').val() ==''){
            ck = 1;
        }
    });
    if(ck == 0){
    addNewRowByDefault();
    }
    }
    }
    function printData(head_id) {
    url = "";
    url = $("#stock_return_base_url").val();
    urls = url + "/purchase/stock_return_print_out?stock_head_id=" + head_id;
    window.open(urls, 'div', 'height=3508,width=2480');
}
function changeLocationCheck(e){
 var selected_loc = $(e).val();
 var dflt_ret_store_chk = $("#dflt_ret_store_chk").val();
// if(selected_loc == 'OTPIPS' || selected_loc == 'IPH001'){
//     $("#to_location").val('IPR001').select2();
//     $("#to_location option").each(function (index) {
//         if ($(this).val() == 'IPR001' ) {
//             $(this).prop('disabled', false);
//          }
//          else {
//             $(this).prop('disabled', true);
//          }
//       });
// }else{

//     $("#to_location option").each(function (index) {
//             $(this).prop('disabled', false);
//       });
//       $("#to_location").val(dflt_ret_store_chk).select2();
// }
var url = "{{route('extensionsvalley.purchase.getToLocation')}}";
var loc_searchstring = '';
$.ajax({
            type: "POST",
            url: url,
            data: 'selected_loc='+selected_loc+'&selecting_store = 0&selecting_ret_store=1',
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                var datas = JSON.parse(data);
                $.each(datas, function (key, val) {
                        var sel = '';
                        if (dflt_ret_store_chk == val.location_code)
                        {
                            sel = 'selected';
                        } else {
                            sel = '';
                        }
                        loc_searchstring += "<option " + sel + " value='" + val.location_code + "'>" + val.location_name + "</option>";
                    });
            },
            complete: function (){
                $.LoadingOverlay("hide");
                var html_data = '<label for="">Store</label><div class="clearfix"></div>'+
                '<select  class=""form-control to_location select2" id="to_location" name="to_location">' + loc_searchstring + '</select>';
                $("#append_to_loc").html(html_data);
                $("#to_location").select2();
            }

        });
       // getStoreLocationType()
}
function getStoreLocationType(){
    var url = "{{route('extensionsvalley.purchase.getStoreLocationType')}}";
    var to_location = $("#from_location").val();
var loc_searchstring = '';
$.ajax({
            type: "POST",
            url: url,
            data: 'selected_loc='+to_location+'&selecting_store=1',
            beforeSend: function () {
            },
            success: function (data) {
                $("#location_type").val(data);
            },
            complete: function (){

            }

        });
}
</script>

@endsection
