@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position: relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .search_box_bottom{
        bottom: -30px;
    }
    .ajax_search_box_bottomcls .ajaxSearchBox{
        bottom: 3px;
        height: 200px !important;
    }
    .ajax_search_box_bottomcls_reduce_box{
        height: 200px !important;
    }
    table td{
        position: relative !important;
    }


    /* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1;
}

/* Handle */
::-webkit-scrollbar-thumb {
  background: #888;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555;
}
</style>
@endsection
@section('content-area')
     <!-- page content -->
     <div class="right_col"  role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <input type="hidden" id="req_base_url" value="{{URL::to('/')}}">
                 <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                {!!Form::open(array('route' => 'extensionsvalley.purchase.updateDirectStockForm', 'method' => 'post', 'class' => 'formShortcuts', 'id' => 'requisitionForm'))!!}
                <div class="col-md-12 no-padding">
                <div class="box no-border no-margin" >
                    <div class="box-body clearfix">


                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Store</label>
                                <div class="clearfix"></div>
                               {!! Form::select('location', array("0"=> " Select location") + $location->toArray(),null, [
                            'class'       => 'form-control location',
                            'id'    => 'location'
                        ]) !!}
                            </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                     <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                    <input type="text" id="remarks" class="form-control"  name="remarks" >
                                </div>
                            </div>

                    </div>
                </div>
            </div>


                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                <div class="" style="position: relative; min-height: 400px;overflow:scroll">
                <table class="table no-margin table-striped no-border table-condensed styled-table" id="addNewItem" style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg " style="cursor: pointer;">
                               <th width="2%">#</th>
                               <th width="22%"> Item Name </th>
                               <th width="4%">Batch</th>
                               <th width="3%">Quantiy</th>
                               <th width="3%">Tax</th>
                               <th width="3%">Selling Price</th>
                               <th width="3%">MRP</th>
                               <th width="3%">Expiry Date</th>

                               <th width="3%"><button type="button" tabindex="-1" id="add"  class="btn btn-success add_row_btn" onclick="addNewRowByDefault(0)"><i class="fa fa-plus"></i></button></th>
                           </tr>

                       </thead>
                       <tbody id="fillrequestitemdetails">

                       </tbody>

            </table>
                </div>
            </div>

                 <div class="clearfix"></div>
                    <div class="ht5"></div>

                <div class="col-md-12">
                <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">


                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button  class="btn btn-block light_purple_bg saveitembtn" ><i id="saveitembtn_spin" class="fa fa-save"></i>
                                    Save</button>
                </div>



                <div class="col-md-1 padding_sm" style="float: right;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                               <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                </div>
            </div>
                </div>
            </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>



<script type="text/javascript">
</script>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    var j=1;
    var dynamic_search_item_code = '';
    var previous;



$(document).ready(function() {

    $('.datepicker').datetimepicker({
    format: 'DD-MMM-YYYY'
    });

      addNewRowByDefault(0);


    var $table = $('table.theadfix_wrapper');

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
    $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
    });


});



 function addNewRowByDefault(is_val = 0){

            var table = document.getElementById("addNewItem");
            var newrow = table.rows.length;
            var row = table.insertRow(newrow);
            row.id = "newRow" + newrow;
            <?php  $m = 1; $n = 0; ?>
             <?php   foreach($addFieldsArray as $addFields) { ?>//

                    var cell{{$m}} = row.insertCell("{{$n}}");

                    cell{{$m}}.innerHTML = "<?php  echo $addFields; ?>";
            <?php
                    $m++; $n++;
                }

            ?>
            cell2.firstChild.id = 'item_code_'+j;
           // cell4.firstChild.id = 'fld_emergency-'+j;
            var txtFieldID = cell2.firstChild.id;
            document.getElementById(txtFieldID).nextSibling.id = 'ajaxSearchBox_'+j;
           // var txtFieldID1 = cell4.firstChild.id;
           // document.getElementById(txtFieldID1).nextSibling.id = 'emg-'+j;
            if(j > 2){
                $('#ajaxSearchBox_'+j).addClass('ajax_search_box_bottomcls_reduce_box');

            }
            if(j > 6){
                $('#newRow'+j).addClass('ajax_search_box_bottomcls');

            }
            $('.cmm_stl_txt').closest('td').addClass('common_td_rules');
            $('.cmm_stl').closest('td').addClass('td_common_numeric_rules');
            j++;
            if(is_val == 1){
                return txtFieldID;
            }
 }



function searchItemCode(id,event) {
     if(document.getElementById('location').value == "0")
            {
                 alert('Please Select Location ..! ');return false;
            }
        var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var search_type = $('input[name="search_items_by"]:checked').val();
    var current;
        var ajax_div = $('#'+id).next().attr('id');
        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {

            var item_code = $('#'+id).val();
            if (item_code == "") {
                $("#"+ajax_div).html("");
            } else {
            var url = "{{route('extensionsvalley.purchase.addAjaxDirectStockItem')}}";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_code=' +item_code,
                    beforeSend: function () {
                    $("#"+ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#"+ajax_div).html(html).show();
                        $("#"+ajax_div).find('li').first().addClass('liHover');

                    },
                    complete: function () {

                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
            }
}


function fillRequestItemDetails(list,item_code,item_desc,expiry_date,unit_tax_rate,unit_selling_price,unit_mrp){
    var itemCodeListDivId = $(list).parent().attr('id');
    var itemCodeTextId = $('#'+itemCodeListDivId).prev("input[type=text]").attr('id');
    $('#'+itemCodeTextId).val(item_desc);
    $('#'+itemCodeListDivId).hide();


    $('#'+itemCodeTextId).closest("tr").find("input[name='item_code_hidden[]']").val(item_code);
    $('#'+itemCodeTextId).closest("tr").find("label[for='expiry_date[]']").text(expiry_date);
    $('#'+itemCodeTextId).closest("tr").find("label[for='tax[]']").text(unit_tax_rate);
    $('#'+itemCodeTextId).closest("tr").find("label[for='selling_price[]']").text(unit_selling_price);
    $('#'+itemCodeTextId).closest("tr").find("label[for='mrp[]']").text(unit_mrp);
    $('#'+itemCodeTextId).closest("tr").find("input[name='expiry_date[]']").val(expiry_date);
    $('#'+itemCodeTextId).closest("tr").find("input[name='tax[]']").val(unit_tax_rate);
    $('#'+itemCodeTextId).closest("tr").find("input[name='selling_price[]']").val(unit_selling_price);
    $('#'+itemCodeTextId).closest("tr").find("input[name='mrp[]']").val(unit_mrp);



}



</script>

@endsection
