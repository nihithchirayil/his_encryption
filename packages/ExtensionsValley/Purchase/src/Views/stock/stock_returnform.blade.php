@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
     <!-- page content -->
<div class="right_col" >
<div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.purchase.listStockReturns')}}" id="requestSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Return No.</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control" name="return_no" id="item_code_search" 
                                    value="{{ $searchFields['return_no'] ?? '' }}">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control datepicker" name="from_date" id="from_date"
                                    value="{{ $searchFields['from_date'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete='off' class="form-control datepicker" name="to_date" id="to_date"
                                    value="{{ $searchFields['to_date'] ?? '' }}">
                                </div>
                            </div>
    
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Return From</label>
                                <div class="clearfix"></div>
                                {!! Form::select('from_location', array("-1"=> " Return From") + $location->toArray(), $searchFields['from_location'] ?? $default_location, [
                                'class'       => 'form-control from_location select2',
                                'id'    => 'from_location'
                            ]) !!}
                        </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Return To</label>
                                <div class="clearfix"></div>
                                <?php  $dfltstore = \WebConf::getConfig('default_return_store'); ?>
                                {!! Form::select('to_location', array("-1"=> " Return To") + $to_location->toArray(),$searchFields['to_location'] ?? $dfltstore, [
                                    'class'       => 'form-control to_location select2',
                                    'id'    => 'to_location'
                                ]) !!}
                            </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Status</label>
                                <div class="clearfix"></div>
                               {!! Form::select('status', array("-1"=> " Select Status","1"=> " Worksheet","2"=> " Submitted","3"=> " Approved","4"=> " Received") , $searchFields['status'] ?? null , [
                            'class'       => 'form-control status select2',
                            'id'    => 'status'
                        ]) !!}
                    </div>
                            </div>
                             <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning">
                                <i class="fa fa-times"></i>  Clear</a>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="addStockRequestNew();" class="btn btn-block btn-primary" ><i class="fa fa-plus" ></i> Add</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Return No</th>
                                    <th>Return From</th>
                                    <th>Return To</th>
                                    <th>Returned Date</th>
                                    <th>Approved Date</th>
                                    <th>Approved By</th>
                                    <th>Received Date</th>
                                    <th>Received By</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($item) > 0)
                                @foreach ($item as $item)
                                <?php //dd($item); ?>
                                    <tr style="cursor: pointer;" onclick="stockItemEditLoadData(this,'{{$item->stock_id}}')" >
                                        <td style="text-align: left;" class="common_td_rules">{{ $item->return_no }}</td>
                                        <td style="text-align: left;" class="common_td_rules">{{ $item->from_location }}</td>
                                        <td style="text-align: left;" class="common_td_rules">{{ $item->to_location }}</td>
                                        <td style="text-align: left;" title="{{ $item->return_date }}">{{ $item->ret_date }}
                                        </td>
                                        <td style="text-align: left;" title="{{ $item->approved_date }}"  class="common_td_rules"> {{ $item->apv_date }}</td>
                                        <td style="text-align: left;" class="common_td_rules">{{ $item->approved_by }}</td>
                                         <td style="text-align: left;" title="{{ $item->received_date }}" class="common_td_rules"> {{ $item->rec_date }}</td>
                                        <td style="text-align: left;" class="common_td_rules">{{ $item->received_by }}</td>
                                        <td style="text-align: left;" class="common_td_rules">{{ $item->status }}</td>
                                    </tr>
                                @endforeach
                            @else
                                    <tr>
                                        <td colspan="12" class="location_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
        </div>
    </div>
</div>
            {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
            {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}
            
            {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
            {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
            {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
            <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
             <script type="text/javascript">
                $(document).ready(function() {

                    setTimeout(function() {
                        // $('#menu_toggle').trigger('click');
                        // $(body).addClass('sidebar-collapse');
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 300);

                   $(document).on('click', '.grn_drop_btn', function(e) {
                    e.stopPropagation();
                    $(".grn_btn_dropdown").hide();
                    $(this).next().slideDown('');
                });
                   $(document).on('click', '.btn_group_box', function(e) {
                    e.stopPropagation();
                });

                   $(document).on('click', function() {
                    $(".grn_btn_dropdown").hide();
                });

                   $(".select_button li").click(function() {
                    $(this).toggleClass('active');
                });


                   $(document).on('click', '.notes_sec_list ul li', function() {
                    var disset = $(this).attr("id");
                    $('.notes_sec_list ul li').removeClass("active");
                    $(this).addClass("active");
                    $(this).closest('.notes_box').find(".note_content").css("display", "none");
                    $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
                });



                   $('.month_picker').datetimepicker({
                    format: 'MM'
                });
                   $('.year_picker').datetimepicker({
                    format: 'YYYY'
                });


                   var $table = $('table.theadfix_wrapper');

                   $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }

                });

                   $('.datepicker').datetimepicker({
                    format: 'DD-MMM-YYYY'
                });
                   $('.date_time_picker').datetimepicker();


                   $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });


    //       $('.fixed_header').floatThead({
    //     position: 'absolute',
    //     scrollContainer: true
    // });






    $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
        var $table = $('table.theadfix_wrapper');
        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }
        });
    })

});
</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function() {
    });

function addStockRequestNew(){
    // var protocol = window.location.protocol;
    // var host_name = window.location.host;
    var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addStockReturn";
    var action_url = main_uri + route_path;
    document.location.href = action_url;
    // alert(action_url);
}
function stockItemEditLoadData(list,stock_id){
    // alert(stock_id);
     var main_uri = $("#domain_url").val() + "/purchase/";
    var route_path = "addStockReturn/"+stock_id;
    var action_url = main_uri + route_path;
    document.location.href = action_url;

}
@include('Purchase::messagetemplate')
</script>

@endsection
