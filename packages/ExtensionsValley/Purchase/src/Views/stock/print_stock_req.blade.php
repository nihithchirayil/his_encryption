<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            @page { margin: 0; }
            body { margin: 15px; }
            thead {display: table-header-group;}
            @media print{
                table thead {display: table-header-group;}
            }
        </style>
    </head>

    <body>
        <section class="content" style="margin-top:15px;" id="print_content">
            <div >
                <div style="width: 100%; overflow-x: auto;">

                    @if(isset($result) && count($result) > 0)
                    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%; font-size: 12px; border-collapse: collapse; border: 1px solid #000; margin: 15px auto auto auto ;">
                        <thead>
                            <tr>
                                <td colspan="10" align="center"><b style="font-size:15px">{{$title}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="3">Location :</td>
                                <td colspan="2">{{$result[0]->issue_location}}</td>
                                <td colspan="3">Doc.No :  </td>
                                <td colspan="2">{{$result[0]->request_no}}</td>
                            </tr>
                            <tr >
                                <td colspan="3">Issued To:</td>
                                <td colspan="2">{{$result[0]->requested_location}}</td>
                                <td colspan="3">Status :</td>
                                <td colspan="2">{{$result[0]->status}}</td>
                            </tr>
                            <tr >
                                <td colspan="3">Manual Doc No :</td>
                                <td colspan="2"></td>
                                <td colspan="3">Approved Date</td>
                                <td colspan="2">{{$result[0]->approved_date}}</td>
                            </tr>
                            <tr >
                                <td colspan="3">Printed Date  </td>
                                <td colspan="2">{{date("d-M-Y H:i:s")}}</td>
                                <td colspan="5"></td>
                            </tr>
                            <tr >
                                <td colspan="3">Approved By  </td>
                                <td colspan="2">{{$result[0]->approved_by}}</td>
                                <td colspan="5"></td>
                            </tr>

                        <tr colspan="10">
                            <th>SI.No</th>
                            <th colspan="3">Product</th>
                            <th>Qty</th>
                            <th>Unit</th>
                            <th colspan="3">Remarks</th>
                        </tr>
                        </thead>
                        <tbody >
                            <?php $i = 1;
                            $print_total = 0; ?>
                            @foreach($result as $p_val)
                            <tr>
                                <td>{{$i}}</td>
                                <td colspan="3">{{$p_val->item_desc}}</td>
                                <td>{{$p_val->approved_qty}}</td>
                                <td>{{$p_val->unit_name}}</td>
                                <td colspan="3">{{$p_val->comments}}</td>
                            </tr>
                            <?php
                            $i++;
                            ?>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- FOOTER SECTION-->
                    <div style="margin-top: 20px">
                        <table width="100%" style="border-collapse: collapse;" border="0">
                            <tbody>
                                <tr>
                                    <td>Remarks :</td>
                                </tr>
                                <tr>
                                    <td>Created By :
                                        @if(isset($created_by))
                                        {{$created_by}}
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>

        </section>

        <script type="text/javascript">
            // window.onload = window.print();
            // window.onload = window.close();
        </script>
    </body>
</html>

