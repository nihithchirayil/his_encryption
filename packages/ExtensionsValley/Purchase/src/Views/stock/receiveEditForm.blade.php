@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
        <div class="row codfox_container">
            <div class="col-md-12">
                <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                <input type="hidden" id="c_token" value="{{ csrf_token() }}">
                <input type="hidden" value='<?= $id ?>' id="stock_id">
                {!! Form::open(['route' => 'extensionsvalley.purchase.receiveReturnStock', 'method' => 'post', 'class' => 'formShortcuts', 'id' => 'requisitionForm']) !!}
                <div class="col-md-12 no-padding">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Return No.</label>
                                    <div class="clearfix"></div>
                                    <span>{{ isset($item[0]->return_no) ? $item[0]->return_no : '' }}</span>
                                    <input type="hidden" class="form-control" name="return_no" id="return_no"
                                        value="{{ isset($item[0]->return_no) ? $item[0]->return_no : '' }}" readonly="">
                                    <input type="hidden" class="form-control" name="request_stock_head_id"
                                        id="request_stock_head_id"
                                        value="{{ isset($item[0]->stock_id) ? $item[0]->stock_id : '' }}" readonly="">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Return From</label>
                                    <div class="clearfix"></div>
                                    <?php
                                    $default_location_name = '';
                                    if ($default_location != '') {
                                        $default_location_name = \DB::table('location')
                                            ->where('location_code', $default_location)
                                            ->pluck('location_name');
                                        $default_location_name = isset($default_location_name[0]) ? $default_location_name[0] : '';
                                    }
                                    ?>
                                    <span>{{ isset($item[0]->frm_loc) ? $item[0]->frm_loc : $default_location_name }}</span>
                                    <input type="hidden" class="form-control from_location" name="from_location"
                                        id="from_location"
                                        value="{{ isset($item[0]->from_location_code) ? $item[0]->from_location_code : $default_location }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Return To</label>
                                    <div class="clearfix"></div>
                                    <?php
                                    $dfltstore = \WebConf::getConfig('default_return_store');
                                    $default_store_name = '';
                                    if ($dfltstore != '') {
                                        $default_store_name = \DB::table('location')
                                            ->where('location_code', $dfltstore)
                                            ->pluck('location_name');
                                        $default_store_name = isset($default_store_name[0]) ? $default_store_name[0] : '';
                                    }
                                    ?>
                                    <span>{{ isset($item[0]->to_loc) ? $item[0]->to_loc : $default_store_name }}</span>
                                    <input type="hidden" class="form-control to_location" name="to_location"
                                        id="to_location"
                                        value="{{ isset($item[0]->to_location_code) ? $item[0]->to_location_code : $dfltstore }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Reason</label>
                                    <div class="clearfix"></div>
                                    <span>{{ $reason }}</span>
                                    <input type="hidden" class="form-control reason_head" name="reason_head"
                                        id="reason_head"
                                        value="{{ isset($item[0]->reason_head) ? $item[0]->reason_head : '' }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Remarks</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="{{ isset($item[0]->comments) ? $item[0]->comments : '' }}"
                                        autocomplete="off" id="requisition_comments" class="form-control" name="remarks">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="radio radio-primary inline">
                                    <input id="fully_receive" onclick="makeFullyRequestRejectItem()" type="radio" name="receive_reject"  value="1">
                                    <label for="fully_receive">
                                        Fully Receive
                                    </label>
                                </div>
                                <div class="radio radio-primary inline">
                                    <input id="fully_reject" onclick="makeFullyRequestRejectItem()"  type="radio" name="receive_reject"  value="2">
                                    <label for="fully_reject">
                                        Fully Reject
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                    <div class="theadscroll always-visible" style="position: relative; height: 400px;">

                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            id="addNewItem" style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th class="common_td_rules" width="35">
                                        <input id="issue_search_box" onkeyup="searchProducts();" type="text"
                                            placeholder="Item Search.. " style="color: #000;display: none;width: 90%;">
                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                                class="fa fa-search"></i></span>
                                        <span id="item_search_btn_text">Item Name</span>
                                    </th>
                                    <th class="common_td_rules" width="8%">Item Unit</th>
                                    <th class="common_td_rules" width="9%">Batch</th>
                                    <th class="common_td_rules" width="8%">Exp date</th>
                                    <th class="common_td_rules" width="15%">Reason</th>
                                    <th class="common_td_rules" width="7%">Return Qty</th>
                                    <th class="common_td_rules" width="7%">Receive Qty</th>
                                    <th class="common_td_rules" width="7%">Reject Qty</th>
                                    <th width="5%">
                                        <div class="checkbox checkbox-primary inline">
                                            <input onclick="makeFullyRequest()" id="select_all_records" type="checkbox"
                                                value="">
                                            <label for="select_all_records">
                                            </label>
                                        </div>
                                    </th>
                                </tr>

                            </thead>
                            <tbody id="fillrequestitemdetails">

                            </tbody>

                        </table>
                    </div>


                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border">
                            <div class="box-body clearfix">
                                @if (isset($approveAccess) && $approveAccess == '1')
                                    @if (isset($item[0]->status) && $item[0]->status != '4')
                                        <div class="col-md-1 padding_sm" style="float: right;">
                                            <label for="">&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <button type="button" id="saveitembtn" onclick="saveItem();"
                                                class="btn btn-block btn-success">
                                                Save <i id="saveitemspin" class="fa fa-save"></i> </button>
                                        </div>
                                    @endif
                                @endif
                                <div class="col-md-1 padding_sm" style="float: right;">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i>
                                        Clear</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>
                {!! Form::token() !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript"></script>

@stop

@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/receiveStockReturn.js') }}"></script>
@endsection
