<?php

if (isset($itemCodeDetails)) {
    if (!empty($itemCodeDetails)) {
        foreach ($itemCodeDetails as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemValues(this,"{{ htmlentities($item->item_code) }}","{{ htmlentities($item->item_desc) }}","{{ htmlentities($item->uom_name) }}","{{ htmlentities($item->decimal_qty_ind) }}")'
    class="list_hover">
    {!! $item->item_desc !!}
</li>
<?php
        }
    } else {  ?>
<li onclick='fillNoItem(this);' style="display: block; padding:5px 10px 5px 5px; ">
    <?php
echo 'No Result Found'; ?>
</li>
<?php
    }
}

if(isset($request_form)) {
    if(!empty($request_form['res'])) {
        foreach ($request_form['res'] as $batch) {

      ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemBatchValues(this,"{{ htmlentities($batch->batch_no) }}","{{ htmlentities($batch->expiry_date) }}","{{ htmlentities($batch->stock) }}")'
    class="list_hover">
    <?php echo htmlentities($batch->batch_no) . '     [   ' . htmlentities($batch->expiry_date) . '   ]'; ?>
</li>
<?php
        }
    } else {  ?>
<li onclick='fillNoItem(this);' style="display: block; padding:5px 10px 5px 5px; ">
    <?php
echo 'No Result Found'; ?>
</li>
<?php
    }
}


$i = 1;
?>
@if (isset($stockItemDetails) && !empty($stockItemDetails))
@foreach ($stockItemDetails as $data)
<?php
        $reason_data = [];
        $reason_data = \DB::table('config_head as h')
            ->leftJoin('config_detail as d', 'h.id', '=', 'd.head_id')
            ->where('h.name', 'stock_return_reason')
            ->where('h.status', 1)
            ->where('d.status', 1)
            ->orderBy('d.name')
            ->pluck('d.name', 'd.value');
        ?>

<tr id="{{ $data->id }}">

    <td style="width: 2%;">
        <input name="del[]" id="deleteID{{ $data->id }}" type="checkbox" class="deleteID" value="{{ $data->id }}">
        <input type="hidden" id="save_update_{{ $i }}" name="save_update[]" value="1">
        <input type="hidden" id="save_update_id_{{ $i }}" name="save_update_id[]" value="{{ $data->id }}">
    </td>
    <td class="common_td_rules" style="width: 15%;">
        <input type='text' required autocomplete='off' value="{{ $data->item_desc }}" id="field_item_code-{{ $i }}"
            readonly name='item_desc[]' onkeyup='searchItemCode(this.id,event)' class='form-control item_desc'
            placeholder='Search Item Name'>
        <div class='ajaxSearchBox'
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 99999;
            position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
        <input type='hidden' class="item_code_hidden" name='item_code_hidden[]' value="{{ $data->item_code }}"
            id="item_code_hidden{{ $i }}">
        <input type='hidden' class="decimal_qty_ind_hidden" name='decimal_qty_ind_hidden[]'
            value="{{ $data->decimal_qty_ind }}" id="decimal_qty_ind_hidden{{ $i }}">
    </td>
    <td class="common_td_rules">
        <label for='uom[]' id="field_uom-{{ $i }}">{{ $data->uom_name }}</label>
    </td>
    <td style="width: 7%;" class="common_td_rules">
        <label for='item_batch[]'>{{ $data->batch_no }} [ {{ $data->expiry }} ] </label>
        <input type='hidden' required readonly autocomplete='off' name='item_batch[]'
            onkeyup='searchItemBatch(this.id,event,this)' value="{{ $data->batch_no }}" class='form-control item_batch'
            placeholder='Search Batch'>
        <div class='ajaxSearchBox'
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 99999;
            position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
        <input type='hidden' class='item_batch_hidden' value="{{ $data->batch_no }}" name='item_batch_hidden[]'>
        <input type='hidden' class='item_batch_exp_hidden' value="{{ $data->expiry }}" name='item_batch_exp_hidden[]'>
    </td>

    <td class="common_td_rules">
        <label for='stock[]' id="field_stock-{{ $i }}" class="stock_label">{{ $data->ps_stock }}</label>
    </td>
    <td>
        {!! Form::select('reason[]', ['-1' => ' Select'] + $reason_data->toArray(), isset($data->reason) ? $data->reason
        : null, [
        'class' => 'form-control reason_all',
        ]) !!}

    </td>
    <td class="common_td_rules">
        <input type='text' class='form-control request_req_qty' required autocomplete='off' name='req_qty[]'
            value="{{ $data->return_quantity }}" id="field_req_qty-{{ $i }}"
            onkeyup="number_validation(this);checkStock(this)">
    </td>
    <td class="common_td_rules">
        <?php if(isset($data->head_status) && ($data->head_status < 3)){ ?>
        <i style='padding: 5px 8px; font-size: 15px;cursor: pointer;' class='fa fa-trash text-red deleteRow'
            onclick='deleteEditRow(this,"{{ $data->id }}");'></i>
        <?php } ?>
    </td>

</tr>
<?php $i++; ?>
@endforeach
@endif
<?php
$i = 1;
?>
@if (isset($stockItemReceiveDetails) && !empty($stockItemReceiveDetails))
@foreach ($stockItemReceiveDetails as $data)
<?php
        if ($data->status == 1) {
            $receive_qty = $data->return_received_qty ? $data->return_received_qty : 0;
            $reject_qty = floatval($data->return_quantity) - floatval($receive_qty);
        } else {
            $receive_qty = 0;
            $reject_qty = 0;
        }

        ?>

<tr id="{{ $data->id }}">

    <td class="common_td_rules">
        <label for='item_desc_lab[]' id="field_item_code_lab-{{ $i }}">{{ $data->item_desc }}</label>
        <input type='hidden' required autocomplete='off' value="{{ $data->item_desc }}" id="field_item_code-{{ $i }}"
            readonly name='item_desc[]' onkeyup='searchItemCode(this.id,event)' class='form-control item_desc'
            placeholder='Search Item Name'>
        <div class='ajaxSearchBox'
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 99999;
            position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
        <input type='hidden' class="item_code_hidden" name='item_code_hidden' value="{{ $data->item_code }}"
            id="item_code_hidden{{ $i }}">
        <input type='hidden' name='decimal_qty_ind_hidden[]' class='decimal_qty_ind_hidden'
            value="{{ $data->decimal_qty_ind }}">
    </td>
    <td class="common_td_rules">
        <label for='uom[]' id="field_uom-{{ $i }}">{{ $data->uom_name }}</label>
    </td>
    <td class="common_td_rules">
        <label for='item_batch_lab[]' id=" ">{{ $data->batch_no }}</label>
        <input type='hidden' required readonly autocomplete='off' name='item_batch'
            onkeyup='searchItemBatch(this.id,event,this)' value="{{ $data->batch_no }}" class='form-control item_batch'
            placeholder='Search Batch'>
        <div class='ajaxSearchBox'
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 99999;
            position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
        <input type='hidden' class='item_batch_hidden' value="{{ $data->batch_no }}" name='item_batch_hidden'>
    </td>
    <td class="common_td_rules">
        <label for='item_batch_exp_lab[]' id=" ">{{ $data->expiry }}</label>
        <input type='hidden' class='item_batch_exp_hidden' value="{{ $data->expiry }}" name='item_batch_exp_hidden'>
    </td>

    <td class="common_td_rules">
        <input type='text' class='form-control' autocomplete='off'
            value="{{ isset($data->comments) ? $data->comments : '' }}" name='reason_data'>
    </td>
    <td class="common_td_rules">
        <label for='req_qty_lab[]' id="field_req_qt_lab-{{ $i }} ">{{ $data->return_quantity }}</label>
        <input type='hidden' class='form-control ret_req_qty' readonly="" autocomplete='off' name='return_qty'
            value="{{ $data->return_quantity }}" id="return_qty{{ $data->id }}">
    </td>
    <td class="common_td_rules">
        <input type='text' class='form-control ret_rec_qty' required autocomplete='off' name='request_qty'
            value="{{ $receive_qty }}" id="request_qty{{ $data->id }}"
            onkeyup="number_validation(this);checkStock(this)">
    </td>
    <td class="common_td_rules">
        <input type='text' class='form-control ret_reject_qty' required autocomplete='off' value={{ $reject_qty ?
            $reject_qty : 0 }} name='reject_qty' id="reject_qty{{ $data->id }}"
            onkeyup="number_validation(this);checkStock(this)">
    </td>
    <td>
        <?php
                    if($data->status==0){
                ?>
        <div class="checkbox checkbox-primary inline">
            <input id="makePatiallyRequest{{ $data->id }}" type="checkbox" class="makePatiallyRequest"
                value="{{ $data->id }}">
            <label for="makePatiallyRequest{{ $data->id }}">
            </label>
        </div>
        <?php
                    }else {
                        echo "-";
                    }
                ?>
        <input type="hidden" id="save_update_{{ $i }}" name="save_update[]" value="1">
        <input type="hidden" id="save_update_id_{{ $i }}" name="save_update_id" value="{{ $data->id }}">
    </td>
</tr>
<?php $i++; ?>
@endforeach
@endif
