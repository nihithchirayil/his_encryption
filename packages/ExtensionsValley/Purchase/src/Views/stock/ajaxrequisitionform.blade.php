<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var request_id = $('#Requestid_hidden').val();
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var from_location = $('#from_location').val();
                var to_location = $('#to_location').val();
                var status = $('#status').val();
                var param = {
                    _token: token,
                    request_id: request_id,
                    from_date: from_date,
                    to_date: to_date,
                    from_location: from_location,
                    to_location: to_location,
                    status: status
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#searchRequisitionDataDiv").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        $('#searchRequisitionBtn').attr('disabled', true);
                        $('#searchRequisitionSpin').removeClass('fa fa-search');
                        $('#searchRequisitionSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#searchRequisitionDataDiv').html(data.html);
                        $('#total_cntspan').html(data.total_records);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function() {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    complete: function() {
                        $("#searchRequisitionDataDiv").LoadingOverlay("hide");
                        $('#searchRequisitionBtn').attr('disabled', false);
                        $('#searchRequisitionSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchRequisitionSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 450px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg" style="cursor: pointer;">
                <th>Request No</th>
                <th>Department</th>
                <th>Store</th>
                <th title="Requested Date">Req.Date</th>
                <th title="Approved Date">App.Date</th>
                <th>Approved By</th>
                <th>Issued By</th>
                <th>Rej.By</th>
                <th>Received By</th>
                <th>Requested By</th>
                <th title="Received Date">Rece.Date</th>
                <th>Status</th>
                <th title="Issued Status">Iss.Status</th>
            </tr>
        </thead>
        <tbody>
            @if (count($requisition_list) > 0)
                @foreach ($requisition_list as $item)
                    <tr style="cursor: pointer;" onclick="stockItemEditLoadData(this,'{{ $item->stock_id }}')">
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->request_no }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->from_location }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->to_location }}</td>
                        <td style="text-align: left; width: 3%" title="{{ $item->requested_date }}"
                            class="common_td_rules">{{ $item->req_date }}
                        </td>
                        <td style="text-align: left; width: 3%" title="{{ $item->approved_date }}"
                            class="common_td_rules"> {{ $item->apv_date }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->approved_by }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->issued_by }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->rejected_by }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->received_by }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">{{ $item->requested_by }}</td>
                        <td style="text-align: left; width: 3%" title="{{ $item->received_date }}"
                            class="common_td_rules">
                            {{ $item->rec_date }}
                        </td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->status }}</td>
                        <td style="text-align: left; width: 3%" class="common_td_rules">
                            {{ $item->issue_status }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="12" class="location_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>
