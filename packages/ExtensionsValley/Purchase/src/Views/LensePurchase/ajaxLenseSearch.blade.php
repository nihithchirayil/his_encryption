<?php
$date_now = date("d-M-Y");


if (isset($grnitemCodeDetails)) {
    if (!empty($grnitemCodeDetails)) {
        foreach ($grnitemCodeDetails as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillItemValues(this,"{{ htmlentities($item->item_code) }}","{{ htmlentities($item->item_desc) }}","{{ htmlentities($item->item_id) }}","{{ htmlentities($item->uom_name) }}","{{ htmlentities($item->uom_id) }}","{{ htmlentities($item->conv_factor) }}","{{ $row_id }}")'>
    {!! $item->item_desc !!}
    <?php //echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]';
    ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($patientNameDetails)) {
    if (!empty($patientNameDetails)) {
        foreach ($patientNameDetails as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillPatientValues(this,"{{ htmlentities($item->patient_name) }}","{{ htmlentities($item->id) }}","{{ $row_id }}")'>
    {!! $item->patient_name !!} [ {{ $item->uhid }} ]
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($email_users)) {
    if (!empty($email_users)) {
        foreach ($email_users as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillEmailUsers("{{ htmlentities($item->name) }}","{{ htmlentities($item->email) }}","{{ htmlentities($email_count) }}")'>
    {!! $item->name !!}
</li>
<?php
        }
    } else {
        echo 0;
    }
}
if (isset($po_number_search)) {
    if (!empty($po_number_search)) {
        foreach ($po_number_search as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; " class="list_hover"
    onclick='fillPoNumber("{{ htmlentities($item->po_id) }}","{{ htmlentities($item->po_no) }}")'>
    {!! strtoupper($item->po_no) !!}
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($grn_vendor_name_details)) {
    if (!empty($grn_vendor_name_details)) {
        foreach ($grn_vendor_name_details as $vendor) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemValues1(this,"{{ htmlentities($vendor->ven_id) }}","{{ htmlentities($vendor->vendor_code) }}","{{ htmlentities($vendor->gst_vendor_code) }}","{{ htmlentities($vendor->vendor_name) }}","{{ htmlentities($vendor->email) }}","{{ $row_id }}")'>
    {!! $vendor->vendor_name !!}
    <?php echo '     [   ' . htmlentities($vendor->gst_vendor_code) . '   ]'; ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}


if (isset($productDataSearch)) {
    if (!empty($productDataSearch)) {
        foreach ($productDataSearch as $item) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px;"
    onclick='fillitem_desc("{{ htmlentities($item->item_id) }}","{{ htmlentities($item->item_desc) }}")'>
    {!! htmlentities($item->item_desc) !!}
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}
if (isset($grn_manufacturer_name_details)) {
    if (!empty($grn_manufacturer_name_details)) {
        foreach ($grn_manufacturer_name_details as $manufacturer) {
            ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillManufacturerValues(this,"{{ htmlentities($manufacturer->manufacturer_id) }}","{{ htmlentities($manufacturer->manufacturer_code) }}","{{ htmlentities($manufacturer->manufacturer_name) }}")'>
    {!! $manufacturer->manufacturer_name !!}
    <?php echo '     [   ' . htmlentities($manufacturer->manufacturer_code) . '   ]'; ?>
</li>
<?php
        }
    } else {
        echo 'No Result Found';
    }
}

$i = 1;
?>
