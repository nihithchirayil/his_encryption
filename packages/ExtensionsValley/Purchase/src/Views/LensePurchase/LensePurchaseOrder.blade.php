@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/po_new.css') }}" rel="stylesheet">
@endsection
@section('content-area')


    <div class="modal fade" id="sendEmailCCModel" data-keyboard="false" data-backdrop="static" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 800px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Email CC Users</h4>
                </div>
                <div class="modal-body" style="height: 440px" id="">
                    <div class="col-md-12 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 410px;">
                                <table class="table no-margin table_sm table-striped no-border styled-table"
                                    style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th width="40%">Name</th>
                                            <th width="55%">Email</th>
                                            <th width="5%" style="text-align: center">
                                                <button type="button" onclick="manageEmailCCUsers('','')"
                                                    class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="addUsersToEmail">

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger"> Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="addUserEmailCC()" type="button" class="btn btn-primary"> Add
                        to
                        Email <i class="fa fa-save"></i></button>
                </div>

            </div>

        </div>
    </div>
    <div class="modal fade" id="print_po_model" data-keyboard="false" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Print Purchase Order</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div id="print_po_modelDiv"></div>

            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button onclick="exceller();" style="padding: 3px 3px" type="button" class="btn btn-warning">Excel
                    <i class="fa fa-file-excel-o"></i></button>
                {{-- <button onclick="DownloadPdfSendMail(0)" style="padding: 3px 3px" type="button" id="file_pdf_btn"
                    class="btn btn-success">PDF <i id="file_pdf_spin" class="fa fa-file"></i>
                </button> --}}
                <button onclick="printReportData();" style="padding: 3px 3px" type="button"
                    class="btn btn-primary">Print <i class="fa fa-print"></i>
                </button>
            </div>
        </div>
    </div>
</div>
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="prescription">
        <div class="modal-dialog" role="document" style="width: 90% !important;">
            <div class="modal-content" style="margin-left: 10%;">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Prescription</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:500px;" id="prescription_modal_body">


                </div>
            </div>
        </div>
    </div>
    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-2 padding_sm">
                <strong><?= $title ?></strong>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
            <input type="hidden" id="row_count_id" name="row_count" value="2">
            <input type="hidden" id="po_id" value="<?= $po_id ?>">
            <input type="hidden" id="calculationSelectRowID" value="0">
            <input type="hidden" id="decimal_configuration" value="<?= $decimal_configuration ?>">
            <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
            <input type="hidden" id="lastClickedRowID" value="0">
            <input type="hidden" id="default_item_type" value="{{ $item_type }}">
        </div>
        <div class="row padding_sm">
            <div id="poplast_item_box_div" class="poplast_item_box col-md-12 padding_sm"
                style="z-index: 999999; display: block;">
            </div>
            <div class="col-md-12 padding_sm">
                <div class="col-md-8 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 140px;">
                            <div class="col-md-6 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Supplier <span style="color: red">*</span></label>
                                    <input onkeyup="searchNewVendor(this.id,event)" type="text" name="searchnewvendor"
                                        value="" class="form-control" autocomplete="off" id="searchnewvendor"
                                        placeholder="">
                                    <div id="ajaxVendorSearchBox" class="ajaxSearchBox"
                                        style="margin-top: -16px; width: 100%; z-index: 9999;">
                                    </div>
                                    <input type="hidden" name="vendoritem_id_hidden" value="0"
                                        id="vendoritem_id_hidden">
                                    <input type="hidden" name="vendoritem_code_hidden" value=""
                                        id="vendoritem_code_hidden">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Manufacturer</label>
                                    {!! Form::select('manufacturer_list', $manufacture_list, '', [
                                        'class' => 'form-control custom_floatinput select2',
                                        'placeholder' => 'Select Manufacturer',
                                        'title' => 'Select Manufacturer',
                                        'id' => 'manufacturer_list',
                                        'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Location</label>
                                    {!! Form::select('deprt_list', $listdept, $default_optical_store, [
                                        'class' => 'form-control custom_floatinput select2',
                                        'placeholder' => 'Select Location',
                                        'title' => 'Select Location',
                                        'id' => 'deprt_list',
                                        'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <div class="checkbox checkbox-info inline no-margin">
                                        <input title="Is IGST" type="checkbox" id="is_igstcheck" value="1">
                                        <label for="is_igstcheck">IGST</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Remarks</label>
                                    <input name="payment_remarks" id="payment_remarks" class="form-control"
                                        autocomplete="off" type="text">
                                </div>
                            </div>
                            {{-- <div class="col-md-6 padding_sm">
                                <div class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Patient Name</label>
                                    <input onkeyup="searchPateint(this.id,event)" type="text" name="searchpatient"
                                        value="" class="form-control" autocomplete="off" id="searchpatient"
                                        placeholder="">
                                    <div id="ajaxVendorPatientSearchBox" class="ajaxSearchBox"
                                        style="margin-top: -16px; width: 100%; z-index: 9999;">
                                    </div>
                                    <input type="hidden" name="patient_id_hidden" value="0"
                                        id="patient_id_hidden">
                                </div>
                            </div> --}}
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 140px;">
                            <table class="table no-margin table_sm no-border grn_total_table">
                                <tbody>
                                    <tr>
                                        <td width="50%"><label class="text-bold">+ Gross Amount</label></td>
                                        <td><input readonly="" type="text" id="item_value" Value="0.00"
                                                name="item_value" class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td><label class="text-bold">+ Total Tax</label></td>
                                        <td><input readonly="" id="payment_othercharges" type="text"
                                                Value="0.00" name="payment_othercharges"
                                                class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td><label class="text-bold">RoundOff</label></td>
                                        <td><input value="0.00" autocomplete="off"
                                                onblur="updateRoundAmt('round_off','')" onkeyup="getRoundOffAmt()"
                                                name="round_off" id="round_off" type="text"
                                                class="form-control text-right">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label class="text-bold">Total Amount</label></td>
                                        <td><input readonly="" name="po_amount"
                                                style="font-size: 16px !important;font-weight: 700;" id="po_amount"
                                                type="text" Value="0.00" class="form-control text-right">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div id="po_approve_btn_id" class="col-md-4 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="savePurchaseOrder(3)" id="po_approve_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-buysellads"
                                        id="po_approve_spin"></i>
                                    Approve</button>
                            </div>
                            <div id="po_post_btn_id" class="col-md-4 padding_sm" style="margin-top: 10px" >
                                <button type="button" onclick="savePurchaseOrder(1)" id="po_post_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-paper-plane-o"
                                        id="po_post_spin"></i>
                                    Save
                                </button>
                            </div>
                            @if($po_id && $po_id > 0)
                            <div id="po_printpreview_btn_id" class="col-md-4 padding_sm" style="margin-top: 10px">
                                <button type="button" onclick="getPrintPreview()" id="print_preview_btn"
                                    class="btn btn-success btn-block"><i class="fa fa-eye" id="print_preview_spin"></i>
                                    Preview
                                </button>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 140px;">

                            <div id="po_email_cc_btn_id" class="col-md-2 pull-left padding_sm" style="display: none">
                                <button type="button" onclick="createPoSendEmailUsers()" id="addEmail_cc_btn"
                                    class="btn bg-green btn-block savepobtn"><i class="fa fa-cc"
                                        id="addEmail_cc_Spin"></i>
                                    Email CC</button>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="row padding_sm" style="margin-top: 10px;">
            <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 36px;">
                            <div id="po_prescription_btn_id" class="col-md-2 padding_sm">
                                <button type="button" onclick="showPrescription()" id="po_presc_btn"
                                    class="btn btn-info btn-block infopobtn"><i class="fa fa-info"
                                        id="po_presc_spin"></i>
                                    Prescription</button>
                            </div>
                            <div class="col-md-2 padding_sm pull-right" >
                                    <label class="custom_floatlabel red" style="display: inline-flex"> PO Number :
                                    <h5 class="red" style="margin-top: 1px !important"><span id="po_number_show"><?=  $po_no ?></span></h5> </label>
                            </div>
                            <div class="col-md-2 padding_sm pull-right" >
                                <label class="custom_floatlabel red" style="display: inline-flex"> PO Date :</label>
                                <input style="border: none" value="<?= date('M-d-Y') ?>" readonly
                                id="po_date" placeholder="PO Date" name="po_date" type="text">
                            </div>
                        </div>
                    </div>
            </div>
        </div>
<?php $row_count = 1 ?>
        <div class="row padding_sm" style="margin-top: 10px">
            <div class="col-md-12 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #01987a;min-height: 400px;">

                        <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
                            style="border: 1px solid #CCC;" id="main_row_tbl">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th rowspan="2" width="2%">SL.No.</th>

                                    <th rowspan="2" width="15%">
                                        <input id="issue_search_box"
                                            onkeyup="searchProducts('issue_search_box','added_new_list_table_product');"
                                            type="text" placeholder="Item Search.. "
                                            style="color: #000;display: none;width: 90%;">
                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                                class="fa fa-search"></i></span>
                                        <span id="item_search_btn_text">Product Name</span>
                                    </th>
                                    <th rowspan="2" width="15%">Patient Name</th>
                                    <th rowspan="2" width="5%">Rate</th>
                                    <th rowspan="2" width="5%">Tax %</th>
                                    <th rowspan="2" width="5%">Net Amt</th>
                                    <th colspan='6' width="12%" style="text-align: center;border: 1px solid;">Right Eye</th>
                                    <th colspan='6' width="12%" style="text-align: center;border: 1px solid">Left Eye</th>

                                    <th rowspan="2" width="2%"><button type="button" style="margin-top: 5px;"
                                            id="addQuotationRowbtn" onclick="getNewRowInserted();"
                                            class="btn btn-primary add_row_btn"><i id="addQuotationRowSpin"
                                                class="fa fa-plus"></i></button>
                                    </th>
                                </tr>
                                <tr class="table_header_bg ">
                                    <th width="2%" title="Sphere-DV" style="border-left: 1px solid;">SPHDV</th>
                                    <th width="2%" title="Sphere-NV">SPHNV</th>
                                    <th width="2%" title="Cylinder_DV">CYLDV</th>
                                    <th width="2%" title="Cylinder-NV">CYLNV</th>
                                    <th width="2%" title="AXIS-DV">AXDV</th>
                                    <th width="2%" style="border-right: 1px solid;" title="AXIS-NV">AXNV</th>

                                    <th width="2%" title="Sphere-DV">SPHDV</th>
                                    <th width="2%" title="Sphere-NV">SPHNV</th>
                                    <th width="2%" title="Cylinder-DV">CYLDV</th>
                                    <th width="2%" title="Cylinder-NV">CYLNV</th>
                                    <th width="2%" title="AXIS-NV">AXDV</th>
                                    <th width="2%" style="border-right: 1px solid;" title="AXIS-NV">AXNV</th>

                                </tr>
                            </thead>
                            <tbody id="added_new_list_table_product">
                                <tr onclick="lastClicKItemRow(<?= $row_count ?>)" style="background: #FFF;" class="row_class"
                                    id="row_data_{{ $row_count }}">
                                    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
                                    </td>
                                    <td style="position: relative;">
                                        <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
                                            id="item_desc_{{ $row_count }}" onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
                                            class="form-control popinput" name="item_desc[]" placeholder="Search Item" value="{{ @$defaultItems[0]->item_desc ? $defaultItems[0]->item_desc:'' }}">
                                        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}"
                                            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
                                           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
                                        </div>
                                    </td>
                                    <td style="position: relative;">
                                        <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
                                            id="patient_name_{{ $row_count }}" onkeyup='searchPatientName(this.id,event,{{ $row_count }})'
                                            class="form-control popinput" name="patient_name[]" placeholder="Search Patient">
                                        <div class='ajaxSearchBox' id="ajaxPatientSearchBox_{{ $row_count }}"
                                            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
                                           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
                                        </div>
                                    </td>
                                    <td>
                                        <input class="form-control number_class"
                                            oninput='validateNumber(this)' autocomplete="off" id="request_rate{{ $row_count }}" type="text"
                                            value='' name="request_rate[]" onblur="changeRate(this,'{{ $row_count }}')">
                                            <input type='hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
                                            <input type='hidden' id="item_id_hidden_{{ $row_count }}" name="item_id_hidden[]" value="{{ @$defaultItems[0]->id ? $defaultItems[0]->id:'' }}">
                                            <input type='hidden' id="patient_id_hidden_{{ $row_count }}" name="patient_id_hidden[]" value="">
                                    </td>

                                    <td>
                                        <input class="form-control number_class"  id="request_tax{{ $row_count }}" type="text"
                                            value='' name="request_tax[]" onblur="changeRate(this,'{{ $row_count }}')">
                                    </td>
                                    <td>
                                        <input class="form-control number_class" readonly autocomplete="off" id="request_netamt{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="request_netamt[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="rsphdv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="rsphdv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="rsphnv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="rsphnv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="rcyldv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="rcyldv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="rcylnv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="rcylnv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="raxdv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="raxdv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="raxnv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="raxnv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="lsphdv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="lsphdv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="lsphnv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="lsphnv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="lscydv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="lscydv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="lscynv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="lscynv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="laxdv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="laxdv[]">
                                    </td>
                                    <td>
                                        <input class="form-control number_class"  autocomplete="off" id="laxnv{{ $row_count }}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
                                            value=''  name="laxnv[]">
                                    </td>
                                    <td style="text-align: center">
                                        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
                                            class="fa fa-trash text-red deleteRow"></i>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/lense/LensePurchaseOrder.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
@endsection
