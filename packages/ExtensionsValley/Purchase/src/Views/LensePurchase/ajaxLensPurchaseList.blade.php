<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var po_id = $('#po_id_hidden').val();
                var vendor = $('#supplier_id_hidden').val();
                var location = $('#deprt_list').val();
                var po_dated = $('#po_dated').val();
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var po_status = $('#po_status').val();
                var token = $('#token_hiddendata').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        po_id: po_id,
                        vendor: vendor,
                        location: location,
                        po_dated: po_dated,
                        from_date: from_date,
                        to_date: to_date,
                        po_status: po_status
                    },
                    beforeSend: function() {
                        $('#searchQuotationBtn').attr('disabled', true);
                        $('#searchQuotationSpin').removeClass('fa fa-search');
                        $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#searchDataDiv').html(data);
                    },
                    complete: function() {
                        $('#searchQuotationBtn').attr('disabled', false);
                        $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchQuotationSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
            return false;
        });


    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 580px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="12%">PO No.</th>
                    <th class="common_td_rules" width="8%">Created At</th>
                    <th class="common_td_rules" width="17%">Location</th>
                    <th class="common_td_rules" width="17%">Supplier</th>
                    <th class="common_td_rules" width="8%">Created By</th>
                    <th class="common_td_rules" width="8%">Approved By</th>

                    {{-- <th class="common_td_rules" width="8%">Status</th> --}}
                    <th class="common_td_rules" width="8%">Net Total</th>
                    <th style="text-align: center" width="3%"><i class="fa fa-edit"></i></th>

                    {{-- <th class="common_td_rules" width="12%">Mail Status</th>
                    <th style="text-align: center" width="3%"><i class="fa fa-copy"></i></th> --}}
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($purchase_list) != 0) {
                foreach ($purchase_list as $list) {
                    ?>
                <tr>
                    <td id="purchase_no_data<?= $list->po_id ?>" class="common_td_rules">{{ $list->po_no }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->po_date)) }}</td>
                    <td class="common_td_rules">{{ $list->location_name }}</td>
                    <td class="common_td_rules">{{ $list->vendor_name }}</td>
                    <td class="common_td_rules">{{ $list->created_by }}</td>
                    <td class="common_td_rules">{{ $list->approved_by }}</td>

                    <td class="td_common_numeric_rules">
                        <?= number_format($list->net_total, 2, '.', '') ?></td>
                    <td style="text-align: center">
                        <button title="Edit PO" type="button" class="btn bg-green"
                            onclick="addWindowLoad('lensePurchaseOrder',<?= $list->po_id ?>,0)"><i
                                class="fa fa-edit"></i>
                        </button>
                    </td>

                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="10" style="text-align: center">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>


</div>
