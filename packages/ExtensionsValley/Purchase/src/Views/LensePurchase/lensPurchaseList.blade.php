@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" role="main">
    <div class="row padding_sm">
        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
            <thead>
                <tr class="table_header_bg">
                    <th colspan="11">
                        <?= strtoupper($title) ?>
                    </th>
                </tr>
            </thead>
        </table>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
    </div>
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm">
            <div class="x_panel">
                <div class="row codfox_container">
                    <div class="col-md-10 padding_sm">
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">PO Number</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="po_number"
                                    name="po_number" value="">
                                <input type="hidden" name="po_id_hidden" value="" id="po_id_hidden">
                                <div class="ajaxSearchBox" id="po_idAjaxDiv"
                                    style="margin-top: -13px;width: 100%;z-index: 9999;display: none;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Supplier</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="supplier_number"
                                    name="po_number" value="">
                                <input type="hidden" value="" id="supplier_id_hidden">
                                <div class="ajaxSearchBox" id="supplier_idAjaxDiv"
                                    style="margin-top: -13px;width: 100%;z-index: 9999;display: none;width:400px !important">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Location</label>
                                {!! Form::select('deprt_list', $location, $default_location, [
                                'class' => 'form-control custom_floatinput select2',
                                'onchange' => 'searchQuotation()',
                                'id' => 'deprt_list',
                                'style' => 'width:100%;color:#555555; padding:2px 12px;',
                                ]) !!}
                            </div>
                        </div>
                        {{-- <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -4px;">Dates</label>
                                <div class="clearfix"></div>
                                {!! Form::select(
                                'po_dated',
                                ['1' => 'Created Date', '2' => 'Verified Date', '3' => 'Approved Date'],
                                $searchFields['status'] ?? null,
                                [
                                'class' => 'form-control status select2',
                                'id' => 'po_dated',
                                'onchange' => 'searchQuotation()',
                                ],
                                ) !!}
                            </div>
                        </div> --}}


                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="from_date" id="from_date"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="to_date" id="to_date" value="">
                            </div>
                        </div>


                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label style="margin-top: -4px;">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select(
                                'po_status',
                                [
                                '' => ' Status',
                                // '0' => 'Cancelled',
                                // '2' => 'Closed',
                                '3' => 'Approved',
                                ],
                                $searchFields['status'] ?? null,
                                [
                                'class' => 'form-control status select2',
                                'id' => 'po_status',
                                'onchange' => 'searchQuotation()',
                                ],
                                ) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <div onclick="addWindowLoadNew('lensePurchaseOrder')"
                                class="btn btn-block btn btn-success"><i class="fa fa-plus"></i>
                                Add New PO
                            </div>
                        </div>

                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <button type="button" id="searchQuotationBtn" onclick="searchLensePo()"
                                class="btn btn-block btn btn-primary"><i id="searchQuotationSpin"
                                    class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="clearfix"></div>
                        {{-- <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <a style="display: none" id="downloadfile" href="" download> </a>
                            <button type="button" id="downloadPoListExcelBtn" onclick="downloadPoListExcel()"
                                class="btn btn-block btn btn-warning"><i id="downloadPoListExcelSpin"
                                    class="fa fa-file-excel-o"></i>
                                Excel
                            </button>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div id="searchDataDiv">

            </div>
        </div>
    </div>

</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/lense/lensPurchaseList.js') }}"></script>
@endsection
