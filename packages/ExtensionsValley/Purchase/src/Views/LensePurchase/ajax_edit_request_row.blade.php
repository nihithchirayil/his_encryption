<?php
$row_count=1;
if(count($res)!=0){
    foreach ($res as $each) {
        $string=$each->item_desc;
        $readonly_status='';
        ?>
<tr onclick="lastClicKItemRow(<?= $row_count ?>)" style="background: #FFF;" class="row_class"
    id="row_data_{{ $row_count }}">
    <td style="text-align: center" class='row_count_class'>{{ $row_count }}
    </td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
            id="item_desc_{{ $row_count }}" onkeyup='searchItemCode(this.id,event,{{ $row_count }})'
            class="form-control popinput" name="item_desc[]" placeholder="Search Item" value="{{ $each->item_desc }}">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off"
            id="patient_name_{{ $row_count }}" onkeyup='searchPatientName(this.id,event,{{ $row_count }})'
            class="form-control popinput" name="patient_name[]" placeholder="Search Patient" value="{{ $each->patient_name }}">
        <div class='ajaxSearchBox' id="ajaxPatientSearchBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3);'>
        </div>
    </td>
    <td>
        <input class="form-control number_class"
            oninput='validateNumber(this)' autocomplete="off" id="request_rate{{ $row_count }}" type="text"
            value='{{ $each->unit_rate }}' name="request_rate[]" onblur="changeRate(this,'{{ $row_count }}')">
            <input type='hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
            <input type='hidden' id="item_id_hidden_{{ $row_count }}" name="item_id_hidden[]" value="{{ $each->item_id }}">
            <input type='hidden' id="patient_id_hidden_{{ $row_count }}" name="patient_id_hidden[]" value="{{ $each->patient_id }}">
    </td>

    <td>
        <input class="form-control number_class"  id="request_tax{{ $row_count }}" type="text"
            value='{{ $each->tax_percentage }}' name="request_tax[]"  onblur="changeRate(this,'{{ $row_count }}')">
    </td>
    <td>
        <input class="form-control number_class" readonly autocomplete="off" id="request_netamt{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->net_amount }}'  name="request_netamt[]" >
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="rsphdv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->r_sphere_dv }}'  name="rsphdv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="rsphnv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->r_sphere_nv }}'  name="rsphnv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="rcyldv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->r_cylinder_dv }}'  name="rcyldv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="rcylnv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->r_cylinder_nv }}'  name="rcylnv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="raxdv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->r_axis_dv }}'  name="raxdv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="raxnv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->r_axis_nv }}'  name="raxnv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="lsphdv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->l_sphere_dv }}'  name="lsphdv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="lsphnv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->l_sphere_nv }}'  name="lsphnv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="lscydv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->l_cylinder_dv }}'  name="lscydv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="lscynv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->l_cylinder_nv }}'  name="lscynv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="laxdv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->l_axis_dv }}'  name="laxdv[]">
    </td>
    <td>
        <input class="form-control number_class"  autocomplete="off" id="laxnv{{ $row_count }}"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text"
            value='{{ $each->l_axis_nv }}'  name="laxnv[]">
    </td>
    <td style="text-align: center">
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
<?php
 $row_count++;
    }

}
?>
<input type="hidden" id="edit_row_count" value="{{ $row_count }}" >
