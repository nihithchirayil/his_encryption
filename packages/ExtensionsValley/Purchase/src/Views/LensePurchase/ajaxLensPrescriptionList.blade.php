<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var token = $('#token_hiddendata').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                    },
                    beforeSend: function() {
                        $('#searchQuotationBtn').attr('disabled', true);
                        $('#searchQuotationSpin').removeClass('fa fa-search');
                        $('#searchQuotationSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#searchDataDiv').html(data);
                    },
                    complete: function() {
                        $('#searchQuotationBtn').attr('disabled', false);
                        $('#searchQuotationSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchQuotationSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
            return false;
        });


    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="2%">
                            {{-- <input type="checkbox" class="chk_all_presc_items" onclick="checkAllGrnDtls(this);" name="chk_all_presc_items"> --}}
                    </th>
                    <th class="common_td_rules" width="12%">Patient Name</th>
                    <th colspan='6' width="12%" style="text-align: center;border: 1px solid;">Right Eye</th>
                    <th colspan='6' width="12%" style="text-align: center;border: 1px solid">Left Eye</th>
                </tr>
                <tr>
                    <tr class="table_header_bg ">
                        <th></th>
                        <th></th>
                        <th width="2%" title="Sphere-DV" style="border-left: 1px solid;">SPHDV</th>
                        <th width="2%" title="Sphere-NV">SPHNV</th>
                        <th width="2%" title="Cylinder_DV">CYLDV</th>
                        <th width="2%" title="Cylinder-NV">CYLNV</th>
                        <th width="2%" title="AXIS-DV">AXDV</th>
                        <th width="2%" style="border-right: 1px solid;" title="AXIS-NV">AXNV</th>

                        <th width="2%" title="Sphere-DV">SPHDV</th>
                        <th width="2%" title="Sphere-NV">SPHNV</th>
                        <th width="2%" title="Cylinder-DV">CYLDV</th>
                        <th width="2%" title="Cylinder-NV">CYLNV</th>
                        <th width="2%" title="AXIS-NV">AXDV</th>
                        <th width="2%" style="border-right: 1px solid;" title="AXIS-NV">AXNV</th>

                    </tr>
            </thead>
            <tbody>
                <?php
            if (count($purchase_list) != 0) {
                foreach ($purchase_list as $list) {
                  $data  = json_decode($list->prescription_json,true);
                    ?>
                <tr>
                    <td class="common_td_rules">
                            <input type="checkbox" data-id="{{ $list->id }}" class="chk_all_prescription_dtls_<?= $list->id ?>"
                            onclick="
                            list_prescription_items(this,'{{  $list->id }}','{{ $default_item_id }}','{{ $default_item_name }}','{{ $list->patient_name }}','{{ $list->patient_id }}',
                            '{{ $data['glass_prescription_right']['DvSPH']}}',
                            '{{ $data['glass_prescription_right']['NvSPH']}}',
                            '{{ $data['glass_prescription_right']['DvCyl']}}',
                            '{{ $data['glass_prescription_right']['NvCyl']}}',
                            '{{ $data['glass_prescription_right']['DvAxix']}}',
                            '{{ $data['glass_prescription_right']['NvAxix']}}',
                            '{{ $data['glass_prescription_left']['DvSPH'] }}',
                            '{{ $data['glass_prescription_left']['NvSPH'] }}',
                            '{{ $data['glass_prescription_left']['DvCyl'] }}',
                            '{{ $data['glass_prescription_left']['NvCyl'] }}',
                            '{{ $data['glass_prescription_left']['DvAxix']}}',
                            '{{ $data['glass_prescription_left']['NvAxix']}}')";>
                    <td class="common_td_rules">{{ $list->patient_name }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_right']['DvSPH'] ? $data['glass_prescription_right']['DvSPH']: '' }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_right']['NvSPH'] ? $data['glass_prescription_right']['NvSPH']: '' }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_right']['DvCyl'] ? $data['glass_prescription_right']['DvCyl']: ''}}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_right']['NvCyl'] ? $data['glass_prescription_right']['NvCyl']: ''}}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_right']['DvAxix'] ? $data['glass_prescription_right']['DvAxix']: '' }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_right']['NvAxix'] ? $data['glass_prescription_right']['NvAxix']: '' }}</td>

                    <td class="common_td_rules">{{ @$data['glass_prescription_left']['DvSPH'] ? $data['glass_prescription_left']['DvSPH']: '' }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_left']['NvSPH'] ? $data['glass_prescription_left']['NvSPH']: '' }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_left']['DvCyl'] ? $data['glass_prescription_left']['DvCyl']: ''}}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_left']['NvCyl'] ? $data['glass_prescription_left']['NvCyl']: ''}}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_left']['DvAxix'] ? $data['glass_prescription_left']['DvAxix']: '' }}</td>
                    <td class="common_td_rules">{{ @$data['glass_prescription_left']['NvAxix'] ? $data['glass_prescription_left']['NvAxix']: '' }}</td>

                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="10" style="text-align: center">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>


</div>
