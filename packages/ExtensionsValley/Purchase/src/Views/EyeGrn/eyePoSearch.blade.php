@if (!empty($view) && $view == 2)
@foreach ($po_result as $po_each)
<tr class="table_header_light_purple_bg ">
    <td class="common_td_rules"><input type="checkbox" class="po_dl_id" value="{{ $po_each->p_dl_id }}"></td>
    <td class="common_td_rules">{{ $po_each->item_code }}</td>
    <td class="common_td_rules">{{ $po_each->item_desc }}</td>
    <td class="td_common_numeric_rules">{{ $po_each->pur_qnty }}</td>
    <td class="td_common_numeric_rules">{{ $po_each->free_qty }}</td>
    <td class="td_common_numeric_rules">{{ $po_each->pur_rate }}</td>
    <td class="td_common_numeric_rules">{{ $po_each->total }}</td>
</tr>
@endforeach
@else
<?php $i = 1; ?>
@if (count($po_result) != 0)
@foreach ($po_result as $po_each)
<tr class="table_header_light_purple_bg ">
    <td class="common_td_rules">{{ $i++ }}</td>
    <td class="common_td_rules" id="poNumber{{ $po_each->po_id }}">{{ $po_each->po_no }}</td>
    <td class="common_td_rules">{{ $po_each->po_date }}</td>
    <td class="common_td_rules">{{ $po_each->vendor_name }}</td>
    <td class="common_td_rules">{{ $po_each->location_name }}</td>
    <td class="common_td_rules">
        <button type="button" tabindex="-1" id="po_add_btn{{ $po_each->po_id }}" onclick="AddPo({{ $po_each->po_id }});"
            class="btn addPoSaveBtn btn-success add_row_btn">Add <i class="fa fa-plus" id="po_add_spin{{ $po_each->po_id }}"></i>
        </button>
        <button style="display: none" type="button" tabindex="-1" id="po_list_btn{{ $po_each->po_id }}"
            onclick="ViewPo({{ $po_each->po_id }});" class="btn btn-success add_row_btn">View <i class="fa fa-list"
                id="po_list_spin{{ $po_each->po_id }}"></i>
        </button>
    </td>
</tr>
@endforeach
@else
<tr>
    <td colspan="7" style="text-align: center">No Result Found</td>
</tr>
@endif
@endif
