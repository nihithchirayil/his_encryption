<?php
$total_discount1 = 0.0;
$total_discount2 = 0.0;
$total_othercharges = 0.0;
$total_freightcharges = 0.0;
$total_amt = 0.0;
$net_total = 0.0;
$tot_tax = 0.0;
$oth_charges = 0.0;
$cgst = 0.0;
$sgst = 0.0;
$igst = 0.0;
?>
<div id="ResultDataContainer">
    <div class="row">
        <div class="col-md-12" id="result_container_div">
            <div class="print_data" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 450px;margin-top:15px; ">
                    <table id="result_data_table" class="table no-margin table_sm table-striped no-border styled-table"
                        style="border: 1px solid #CCC;width: 100%">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:#4ea5f8;color:black;border-spacing: 0 1em;font-family:sans-serif">
                                <th style="text-align: center" colspan="24">
                                    GRN
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="12" width="50%">
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="12" width="50%">GRN No :
                                    <?= @$grn_no ? $grn_no : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="10" width="50%">Address :
                                    <?= @$vendor_data['vendor_address'] ? $vendor_data['vendor_address'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="7" width="50%">Bill No :
                                    <?= @$bill_no ? $bill_no : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="7" width="50%">Bill Date :
                                    <?= @$po_date ? $po_date : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="12" width="50%"> PO NO.:
                                    <?= $po_no ? $po_no : '-' ?>
                                </th>
                                <th class="common_td_rules" colspan="12" width="50%">Email:
                                    <?= @$vendor_data['vendor_email'] ? $vendor_data['vendor_email'] : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <th class="common_td_rules" colspan="12" width="50%">Telephone No. :
                                    <?= @$vendor_data['contact_no'] ? $vendor_data['contact_no'] : '' ?>
                                </th>
                                <th class="common_td_rules" colspan="12" width="50%">Location :
                                    <?= @$location_name ? $location_name : '' ?>
                                </th>
                            </tr>
                            <tr class="headerclass"
                                style="background-color:rgb(91 100 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="2%">#</th>
                                <th width="10%">Item Name</th>
                                <th width="10%">Patient Name</th>
                                <th width="6%">Refe. No.</th>
                                <th title="RE Sphere DV" width="3%">RSDV</th>
                                <th title="RE Sphere NV" width="3%">RSNV</th>
                                <th title="RE Cylinder DV" width="3%">RCDV</th>
                                <th title="RE Cylinder NV" width="3%">RCNV</th>
                                <th title="RE Axis DV" width="3%">RADV</th>
                                <th title="RE Axis NV" width="3%">RANV</th>
                                <th title="LE Sphere DV" width="3%">LSDV</th>
                                <th title="LE Sphere NV" width="3%">LSNV</th>
                                <th title="LE Cylinder DV" width="3%">LCDV</th>
                                <th title="LE Cylinder NV" width="3%">LCNV</th>
                                <th title="LE Axis DV" width="3%">LADV</th>
                                <th title="LE Axis NV" width="3%">LANV</th>
                                <th width="3%">GRN Qty</th>
                                <th width="3%">Unit Rate</th>
                                <th width="3%">Sell. Price</th>
                                <th width="5%">Dis. Type</th>
                                <th width="3%">Dis. Amt</th>
                                <th width="5%">Tax Per.</th>
                                <th width="5%">Tot. Tax</th>
                                <th width="5">Net Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($item_string)!=0){
                                foreach ($item_string as $each) {
                                    $total_discount1 += floatval($each['discount_amt']);
                                    $tot_tax += floatval($each['tot_tax_amt']);
                                    $total_amt += floatval($each['totalRate']);
                                    $net_total += floatval($each['netRate']);
                                    if($is_igst=='false'){
                                    $cgst += floatval($each['cgst_amt']);
                                    $sgst += floatval($each['sgst_amt']);
                                    }else{
                                    $igst += floatval($each['total_tax_amt']);
                                    }
                                    ?>
                            <tr>
                                <td>
                                    <?= $i ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['item_desc'] ? $each['item_desc'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['patientname'] ? $each['patientname'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['refe_no'] ? $each['refe_no'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['resdv'] ? $each['resdv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['resnv'] ? $each['resnv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['recdv'] ? $each['recdv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['recnv'] ? $each['recnv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['readv'] ? $each['readv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['reanv'] ? $each['reanv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['lesdv'] ? $each['lesdv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['lesnv'] ? $each['lesnv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['lecdv'] ? $each['lecdv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['lecnv'] ? $each['lecnv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['leanv'] ? $each['leanv'] : '' ?>
                                </td>
                                <td class="common_td_rules">
                                    <?= @$each['leadv'] ? $each['leadv'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['grn_qty'] ? $each['grn_qty'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['unit_rate'] ? $each['unit_rate'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['selling_rate'] ? $each['selling_rate'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['tot_dis_amt'] ? $each['tot_dis_amt'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['tax_type'] ? $each['tax_type'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['total_tax_amt'] ? $each['total_tax_amt'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['totalRate'] ? $each['totalRate'] : '' ?>
                                </td>
                                <td class="td_common_numeric_rules">
                                    <?= @$each['netRate'] ? $each['netRate'] : '' ?>
                                </td>
                            </tr>
                            <?php
                                       $i++;
                                        }
                                        $oth_charges = floatval($total_othercharges) + floatval($total_freightcharges);
                                        }else {
                                            ?>
                            <tr>
                                <td colspan="24" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                            <?php
                                        }
                                        $total_discount= floatval($total_discount1) + floatval($total_discount2);
                                        $total_net = (floatval($total_amt) + floatval($tot_tax) + floatval($other_charges) + floatval($freight_charges) + floatval($round_off)) - (floatval($total_discount)+floatval($bill_discount));

                                        ?>

                            <tr>
                                <th colspan="23" class="td_common_numeric_rules"> Gross Amount :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($total_amt, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="23" class="td_common_numeric_rules">Item Discount(-) : </th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($total_discount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="23" class="td_common_numeric_rules">Bill Discount(-) : </th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($bill_discount, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="23" class="td_common_numeric_rules">Freight :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($freight_charges, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="23" class="td_common_numeric_rules">Other :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($other_charges, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th colspan="23" class="td_common_numeric_rules">Total Tax :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($tot_tax, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th colspan="23" class="td_common_numeric_rules">RoundOff :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($round_off, 2, '.', '') ?>
                                </th>
                            </tr>
                            <tr class="bg-blue">
                                <th colspan="23" class="td_common_numeric_rules">Total Amount :</th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format($total_net, 2, '.', '') ?>
                                </th>
                            </tr>

                            <tr>
                                <td class="common_td_rules" colspan="24">
                                    <b>Remarks: </b>
                                    <?= @$full_remarks ? $full_remarks : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="7" class="common_td_rules" width="30%">
                                    GST NO.
                                    <?= @$hospital_header[0]->gst_no ? $hospital_header[0]->gst_no : '' ?>
                                </th>
                                <th colspan="10" class="common_td_rules" width="50%">
                                    Drug License No.
                                    <?= @$hospital_header[0]->drug_licence_no ? $hospital_header[0]->drug_licence_no : '' ?>
                                </th>
                                <th colspan="7" class="common_td_rules" width="30%">
                                    PAN No.
                                    <?= @$hospital_header[0]->pan_no ? $hospital_header[0]->pan_no : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="12" class="common_td_rules" width="50%">
                                    <b>Seller : </b>
                                    <?= @$vendor_data['vendor_name'] ? $vendor_data['vendor_name'] : '' ?>
                                </td>
                                <td colspan="12" class="common_td_rules" width="50%" colspan="2">
                                    <b>Buyer : </b>
                                    <?= @$hospital_header[0]->address ? $hospital_header[0]->address : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="12" class="common_td_rules" width="30%">
                                    <b>GRN Status : </b>
                                    <?= $po_status ?>
                                </th>
                                <th colspan="12" class="common_td_rules" width="50%">
                                    <b>Approved By : </b>
                                    <?= @$users[0]->approved_by ? $users[0]->approved_by : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="7" class="common_td_rules" width="30%">
                                    <b>Prepared By : </b>
                                    <?= @$users[0]->created_by ? $users[0]->created_by : '' ?>
                                </td>
                                <td colspan="7" class="common_td_rules" width="30%">
                                    <b>Printed By : </b>
                                    <?= @$users[0]->printed_by ? $users[0]->printed_by : '' ?>
                                </td>
                                <td colspan="10" class="common_td_rules" width="50%">
                                    <b>Print Date Time : </b>
                                    <?= date('M-d-Y H:i:s') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
