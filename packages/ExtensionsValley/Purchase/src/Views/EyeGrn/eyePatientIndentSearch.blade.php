<?php $i = 1; ?>
@if (count($po_result) != 0)
@foreach ($po_result as $po_each)
@php
$prescription_data = @$po_each->prescription_json ? json_decode($po_each->prescription_json,
true) : [];
@endphp
<tr class="table_header_light_purple_bg ">
    <td class="common_td_rules">{{ $i++ }}</td>
    <td class="common_td_rules">{{ $po_each->patient_name }}</td>
    <td class="common_td_rules">{{ $po_each->doctor_name }}</td>
    <td class="common_td_rules">{{ $po_each->prescription_date }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_right']['DvSPH'] ?
        $prescription_data['glass_prescription_right']['DvSPH'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_right']['NvSPH'] ?
        $prescription_data['glass_prescription_right']['NvSPH'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_right']['DvCyl'] ?
        $prescription_data['glass_prescription_right']['DvCyl'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_right']['NvCyl'] ?
        $prescription_data['glass_prescription_right']['NvCyl'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_right']['DvAxix'] ?
        $prescription_data['glass_prescription_right']['DvAxix'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_right']['NvAxix'] ?
        $prescription_data['glass_prescription_right']['NvAxix'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_left']['DvSPH'] ?
        $prescription_data['glass_prescription_left']['DvSPH'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_left']['NvSPH'] ?
        $prescription_data['glass_prescription_left']['NvSPH'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_left']['DvCyl'] ?
        $prescription_data['glass_prescription_left']['DvCyl'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_left']['NvCyl'] ?
        $prescription_data['glass_prescription_left']['NvCyl'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_left']['DvAxix'] ?
        $prescription_data['glass_prescription_left']['DvAxix'] : '' }}</td>
    <td class="common_td_rules">{{@$prescription_data['glass_prescription_left']['NvAxix'] ?
        $prescription_data['glass_prescription_left']['NvAxix'] : '' }}</td>
    <td class="common_td_rules">
        <button type="button" id="addIndentBtn{{ $po_each->indent_id }}" onclick="addIndent({{ $po_each->indent_id }});"
            class="btn btn-success addIndentSaveBtn add_row_btn">Add <i class="fa fa-plus"
                id="addIndentSpin{{ $po_each->indent_id }}"></i>
        </button>
    </td>
</tr>
@endforeach
@else
<tr>
    <td colspan="7" style="text-align: center">No Result Found</td>
</tr>
@endif
