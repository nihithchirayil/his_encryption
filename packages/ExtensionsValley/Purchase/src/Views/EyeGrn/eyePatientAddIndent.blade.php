@if(count($po_result)!=0)
@foreach ($po_result as $each)
@php
$prescription_data = @$each->prescription_json ? json_decode($each->prescription_json,
true) : [];
@endphp
<tr style="background: #FFF;" onclick="item_history({{ $row_count }})" class="row_class" id="row_data_{{ $row_count }}">
    <td class='row_count_class'>{{ $row_count }}</td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc{{ $row_count }}"
            onkeyup='searchItemCode(this.id,event,{{ $row_count }})' class="form-control popinput" name="item_desc"
            placeholder="Search Item" value="{{ @$defult_item[0]->item_desc ? $defult_item[0]->item_desc : '' }}">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{ $row_count }}" style='text-align: left; list-style: none;
        cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
        <input type='hidden' name='row_id_hidden' value="{{ $row_count }}" id="row_id_hidden{{ $row_count }}">
        <input type='hidden' name='item_code_hidden'
            value="{{ @$defult_item[0]->item_code ? $defult_item[0]->item_code : '' }}"
            id="item_code_hidden{{ $row_count }}">
        <input type='hidden' name='item_id_hidden'
            value="{{ @$defult_item[0]->item_id ? $defult_item[0]->item_id : '' }}" id="item_id_hidden{{ $row_count }}">
        <input type='hidden' name='po_head_id_hidden' value="" id="po_head_id_hidden{{ $row_count }}">
        <input type='hidden' name='po_detail_id_hidden' value="" id="po_detail_id_hidden{{ $row_count }}">
        <input type='hidden' name='bill_detail_id_hidden' value="0" id="bill_detail_id_hidden{{ $row_count }}">
        <input type='hidden' name='glass_prescription_id' value="{{ $indent_id }}"
            id="glass_prescription_id{{ $row_count }}">
    </td>
    <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="patientname{{ $row_count }}"
            onkeyup='searchEyePatient(this.id,event,{{ $row_count }})' class="form-control" name="patient_name"
            placeholder="Search Patient" value="{{ $each->patient_name }}">
        <div class='ajaxSearchBox' id="ajaxpatientnBox_{{ $row_count }}"
            style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
                   position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '>
        </div>
        <input type='hidden' name='patientid_hidden' value="{{ $each->patient_id }}"
            id="patientid_hidden{{ $row_count }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='Refe No'
            id="refe_no{{ $row_count }}" type="text" autocomplete="off" value="">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='resdv'
            id="resdv{{ $row_count }}" type="text" autocomplete="off" value="{{ @$prescription_data['glass_prescription_right']['DvSPH'] ?
            $prescription_data['glass_prescription_right']['DvSPH'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='resnv'
            id="resnv{{ $row_count }}" type="text" autocomplete="off" value="{{ @$prescription_data['glass_prescription_right']['NvSPH'] ?
            $prescription_data['glass_prescription_right']['NvSPH'] : ''  }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='recdv'
            id="recdv{{ $row_count }}" type="text" autocomplete="off" value="{{ @$prescription_data['glass_prescription_right']['DvCyl'] ?
            $prescription_data['glass_prescription_right']['DvCyl'] : ''  }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='recnv'
            id="recnv{{ $row_count }}" type="text" autocomplete="off" value="{{ @$prescription_data['glass_prescription_right']['NvCyl'] ?
            $prescription_data['glass_prescription_right']['NvCyl'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='reanv'
            id="reanv{{ $row_count }}" type="text" autocomplete="off" value="{{ @$prescription_data['glass_prescription_right']['DvAxix'] ?
            $prescription_data['glass_prescription_right']['DvAxix'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='readv'
            id="readv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_right']['NvAxix'] ?
            $prescription_data['glass_prescription_right']['NvAxix'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='lesdv'
            id="lesdv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_left']['DvSPH'] ?
            $prescription_data['glass_prescription_left']['DvSPH'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='lesnv'
            id="lesnv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_left']['NvSPH'] ?
            $prescription_data['glass_prescription_left']['NvSPH'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='lecdv'
            id="lecdv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_left']['DvCyl'] ?
            $prescription_data['glass_prescription_left']['DvCyl'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='lecnv'
            id="lecnv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_left']['NvCyl'] ?
            $prescription_data['glass_prescription_left']['NvCyl'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='leanv'
            id="leanv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_left']['DvAxix'] ?
            $prescription_data['glass_prescription_left']['DvAxix'] : '' }}">
    </td>
    <td>
        <input class="form-control" onchange="calculateListAmount({{ $row_count }},1)" name='leadv'
            id="leadv{{ $row_count }}" type="text" autocomplete="off" value="{{@$prescription_data['glass_prescription_left']['NvAxix'] ?
            $prescription_data['glass_prescription_left']['NvAxix'] : '' }}">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='grn_qty'
            id="grn_qty{{ $row_count }}" type="text" autocomplete="off" value="1">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='unit_rate'
            id="unit_rate{{ $row_count }}" type="text" autocomplete="off" value="">
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name='selling_rate'
            id="selling_rate{{ $row_count }}" type="text" autocomplete="off" value="">
    </td>
    <td title="Discount Type">
        <select onchange="calculateListAmount({{ $row_count }},1)" name="discount_type"
            id="discount_type{{ $row_count }}" class="form-control">
            @foreach ($discount_type_array as $dis=>$type)
            <option value="{{ $dis }}">{{ $type }}</option>
            @endforeach

        </select>
    </td>
    <td><input class="form-control number_class" onchange="calculateListAmount({{ $row_count }},1)"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_dis_amt{{ $row_count }}" name="tot_dis_amt" type="text">
    </td>
    <td title="Tax Type">
        <select onchange="calculateListAmount({{ $row_count }},1)" name="tax_type" id="tax_type{{ $row_count }}"
            class="form-control">
            <option value="">Select</option>
            @foreach ($tax_head as $tax)
            <option attt-id="{{ $tax->tax_division }}" value="{{ $tax->tax_name }}">
                {{ $tax->tax_val }}</option>
            @endforeach
        </select>
    </td>
    <td><input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="tot_tax_amt{{ $row_count }}" name="tot_tax_amt" type="text" autocomplete="off" value="">
    </td>
    <td>
        <input class="form-control number_class input_readonly" readonly
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            id="net_rate{{ $row_count }}" name="net_cost" type="text" autocomplete="off" value="">
    </td>
    <td>
        <i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{ $row_count }})"
            class="fa fa-trash text-red deleteRow"></i>
    </td>
</tr>
@php
$row_count++;
@endphp
@endforeach
@endif
