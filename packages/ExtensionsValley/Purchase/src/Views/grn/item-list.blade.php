@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
         <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            <form id="itemSearchForm" method="POST" action="{{route('extensionsvalley.item.listItem')}}">
                                {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <label for="">Product Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="item_code" value="{{ $searchFields['item_code'] ?? '' }}">
                            </div>

                            <div class="col-md-4 padding_sm">
                                <label for="">Product Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="item_desc" value="{{ $searchFields['item_desc'] ?? '' }}">
                            </div>
                            <div class="col-md-4 padding_sm">
                                <label for="">Generic Name</label>
                                <div class="clearfix"></div>
                                {!! Form::select('generic_name', array("0"=> " Select Generic Name") + $GenericName->toArray(),$searchFields['generic_name'] ?? '', [
                                            'class'       => 'form-control',
                                        ]) !!}
                            </div>
                            <div class="col-md-4 padding_sm">
                                <label for="">Item Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('item_type', array("0"=> " Select Item Type") + $itemType->toArray(),$searchFields['item_type'] ?? '', [
                                    'class'       => 'form-control item_type',
                                ]) !!}
                            </div>
                            <div class="col-md-4 padding_sm">
                                <label for="">Category</label>
                                <div class="clearfix"></div>
                                {!! Form::select('category', array("0"=> " Select Category") + $Category->toArray(),$searchFields['category'] ?? '', [
                                    'class'       => 'form-control category',
                                ]) !!}
                            </div>
                            <div class="col-md-4 padding_sm">
                                <label for="">Subcategory</label>
                                <div class="clearfix"></div>
                                {!! Form::select('subcategory', array("0"=> " Select SubCategory") + $SubCategory->toArray(),$searchFields['subcategory'] ?? '', [
                                    'class'       => 'form-control subcategory',
                                ]) !!}
                            </div>
                            <div class="col-md-4 padding_sm">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status', array("-1"=> " Select Status","1"=> " Active","0"=> " Inactive"),$searchFields['status'] ?? '', [
                                    'class'       => 'form-control',
                                ]) !!}
                            </div>
                            <div class="col-md-4 padding_sm">
                                <label for="">HSN Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="hsn_code" value="{{ $searchFields['hsn_code'] ?? '' }}">
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i> Search</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="addProductNew();" class="btn btn-block light_purple_bg" ><i class="fa fa-plus" ></i> Add</div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll always-visible" style="position: relative; height: 350px;">
                            <table class="table no-margin theadfix_wrapper table-striped table-col-bordered table_sm table-condensed" style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th>Item Type</th>
                                        <th>Category</th>
                                        <th>Subcategory</th>
                                        <th>Product Code</th>
                                        <th>Product Name</th>
                                        <th>Product By</th>
                                        <th>HSN Code</th>
                                        <th>Generic Name</th>
                                        <th>Status</th>
                                        <th class="text-center">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($item))
                                    @foreach($item as $itm)
                                    <tr>
                                        <td>{{$itm->item_type_name}}</td>
                                        <td>{{$itm->category_name}}</td>
                                        <td>{{$itm->subcategory_name}}</td>
                                        <td>{{$itm->item_code}}</td>
                                        <td>{{$itm->item_desc}}</td>
                                        <td>{{$itm->username}}</td>
                                        <td>{{$itm->hsn_code}}</td>
                                        <td>{{$itm->generic_name}}</td>
                                        <td>{{$itm->status_name}}</td>
                                        <td class="text-center" style="cursor: pointer;" onclick="editProduct('{{$itm->id}}');"><i class="fa fa-edit"></i></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination">
                            {{ $item->links() }}
                        </ul>
                        </div>
                    </div>
                </div>
         </div>
   
</div>


<form action="{{route('extensionsvalley.item.saveItem')}}" method="POST" id="itemForm">
{!! Form::token() !!}
<!-- Modal -->
<div id="add_list_modal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 1000px; width: 100%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title titleName">Manage Product Details</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

            <div class="col-md-12 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                       
                        <div class="col-md-4 padding_sm">
                            <label for="">Item Type</label>
                            <div class="clearfix"></div>
                            {!! Form::select('item_type', array("0"=> " Select Item Type") + $itemType->toArray(),null, [
                            'class'       => 'form-control item_type_list',
                            'id'    => 'item_type'
                        ]) !!}
                        </div>

                        <div class="col-md-4 padding_sm">
                            <label for="">Category</label>
                            <div class="clearfix"></div>
                            {!! Form::select('category', array("0"=> " Select Category") + $Category->toArray(),null, [
                            'class'       => 'form-control category_list',
                            'id'    => 'category'
                        ]) !!}
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">Sub Category</label>
                            <div class="clearfix"></div>
                            {!! Form::select('subcategory', array("0"=> " Select SubCategory") + $SubCategory->toArray(),null, [
                            'class'       => 'form-control subcategory_list',
                            'id'    => 'subcategory'
                        ]) !!}
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">Product Name</label>
                            <div class="clearfix"></div>
                            <input type="text" name="item_desc" id="item_desc" class="form-control">
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">Product Code</label>
                            <div class="clearfix"></div>
                            <input type="text" name="item_code" id="item_code" class="form-control">
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">Barcode</label>
                            <div class="clearfix"></div>
                            <input type="text" name="barcode" id="barcode" class="form-control">
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">HSN Code</label>
                            <div class="clearfix"></div>
                            <input type="text" name="hsn_code" id="hsn_code" class="form-control">
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">Manufacturer</label>
                            <div class="clearfix"></div>
                            {!! Form::select('manufacturer', array("0"=> " Select Manufacturer") + $Manufacturer->toArray(),null, [
                            'class'       => 'form-control',
                            'id'    => 'manufacturer'
                        ]) !!}
                        </div>
                        <div class="col-md-4 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block light_purple_bg">Taxes and Expenses</button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-4 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-body">

                        <div class="col-md-4 padding_xs">
                            <h5 class="text-bold" style="margin: 3px 0 0 0;"><b>UOM</b></h5>
                        </div>
                        <div class="col-md-4 padding_xs">
                            <label for="">Standard UOM</label>
                        </div>
                        <div class="col-md-4 padding_xs">
                            {!! Form::select('std_uom', array("0"=> " Select UOM") + $Uom->toArray(),null, [
                            'class'       => 'form-control',
                            'id'    => 'std_uom'
                        ]) !!}
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="theadscroll always-visible" style="position: relative; height: 150px;">
                            <table class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin uom_table" style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th width="55%">Purchase UOM</th>
                                        <th>Conv Factor</th>
                                        <th>Default</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i=0 ; $i < 5 ; $i++ ) {  ?>
                                    <tr>
                                        <td>
                                            {!! Form::select('purchase_uom[]', array("0"=> " Select UOM") + $Uom->toArray(),null, [
                                                    'class'       => 'form-control',
                                                    'id'          => 'pur_uom'.$i
                                                ]) !!}
                                        </td>
                                        <td>
                                            <input type="text" id="pur_con_fact{{$i}}" name="pur_con_fact[]" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" id="pur_default{{$i}}" name="pur_default[]" class="form-control">
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="theadscroll always-visible" style="position: relative; height: 150px;">
                            <table class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin uom_table" style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th width="55%">Selling UOM</th>
                                        <th>Conv Factor</th>
                                        <th>Default</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i=0 ; $i < 5 ; $i++ ) {  ?>
                                    <tr>
                                        <td>
                                            {!! Form::select('selling_uom[]', array("0"=> " Select UOM") + $Uom->toArray(),null, [
                                                    'class'       => 'form-control',
                                                    'id'          => 'sel_uom'.$i
                                                ]) !!}
                                        </td>
                                        <td>
                                            <input type="text" id="sel_con_fact{{$i}}" name="sel_con_fact[]" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" id="sel_default{{$i}}" name="sel_default[]" class="form-control">
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4 padding_sm">
                <div class="theadscroll always-visible" style="position: relative; height: 295px; border: 1px solid #CCC;">
                    <table class="table no-border no-margin table_sm no-margin">
                    <tbody>
                        <tr>
                            <td width="3%">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox1" class="styled" type="checkbox" checked="" name="status">
                                    <label for="checkbox1"> The item is active</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox2" class="styled" type="checkbox" name="purchasable_ind" >
                                    <label for="checkbox2">The item is purchasable</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox3" class="styled" type="checkbox" name="sellable_ind" >
                                    <label for="checkbox3">The item is Sellable</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox5" class="styled" type="checkbox" name="inventory_tracked_ind" >
                                    <label for="checkbox5">Inventory is tracked for this item</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox6" class="styled" type="checkbox" name="fixed_asset_ind" >
                                    <label for="checkbox6"> This is a fixed asset</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox7" class="styled" type="checkbox" name="consumable_ind">
                                    <label for="checkbox7">Consumable item</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox8" class="styled" type="checkbox" name="chemo_ind" >
                                    <label for="checkbox8">This is a chemo item</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox9" class="styled" type="checkbox" name="narcotic_ind" >
                                    <label for="checkbox9"> This is a narcotic item</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox10" class="styled" type="checkbox" name="consignment_ind">
                                    <label for="checkbox10">This item is a consignment item</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox11" class="styled" type="checkbox" name="returnable_ind" >
                                    <label for="checkbox11">This item is returnable</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox12" class="styled" type="checkbox" name="perishable_ind">
                                    <label for="checkbox12">This item is perishable</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox13" class="styled" type="checkbox" name="price_editable_ind" >
                                    <label for="checkbox13"> The price of the item is editable</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox14" class="styled" type="checkbox" name="disposable_ind" >
                                    <label for="checkbox14">The item is disposable</label>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox15" class="styled" type="checkbox" name="decimal_qty_ind" >
                                    <label for="checkbox15">The item can be sold in decimal qty</label>
                                </div>
                            </td>

                        </tr>
                    </tbody>
                </table>
                </div>
                <div class="clearfix"></div>
                <div class="ht5"></div>
                <div class="col-md-12 no-padding min_sellable_div" style="display: none;">
                    <label for="">Min Sellable Qty and Price</label>
                    <div class="clearfix"></div>
                    <div class="col-md-6 padding_xs">
                        <input type="text" class="form-control" placeholder="Qty" id="min_sellable_qty" name="min_sellable_qty" >
                    </div>
                    <div class="col-md-6 padding_xs">
                        <input type="text" class="form-control" placeholder="Price" id="min_sellable_price" name="min_sellable_price" >
                    </div>
                </div>
            </div>




            <div class="col-md-4 padding_sm">
                <div class="box no-margin no-border">
                    <div class="box-body">
                        <h5 class="no-margin"><b>Clinical Information</b></h5>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="theadscroll always-visible" style="position: relative; height: 130px;">
                            <table class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin" style="border: 1px solid #CCC;" id="tableid">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Chemical Composition</th>
                                    <th style="text-align: center;">Dose</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php for ($i=0 ; $i < 5 ; $i++ ) {  ?>
                                <tr>
                                    <td>  
                                        {!! Form::select('chemical_name[]', array("0"=> "Select Chemical Name") + $ChemicalComposition->toArray(),null, [
                                                    'class'       => 'form-control',
                                                    'id'          => 'chemical_id'.$i
                                                ]) !!}
                                    </td>
                                    <td align="center">
                                        <input type="text" id="dose{{$i}}" name="dose[]" class="form-control">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <table class="table no-margin table_sm no-margin" style="border: 1px solid #CCC;">
                            <tbody>
                                <tr>
                                    <td>VED Type</td>
                                    <td><input type="text" name="ved_type" id="ved_type" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Generic Name</td>
                                    <td>
                                        {!! Form::select('generic_name', array("0"=> " Select Generic Name") + $GenericName->toArray(),null, [
                                            'class'       => 'form-control',
                                            'id'    =>  'generic_name'
                                        ]) !!}
                                                    </td>
                                </tr>
                                <tr>
                                    <td>Schedule</td>
                                    <td><input type="text" name="schedule" id="schedule" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Strength</td>
                                    <td><input type="text" name="strength" id="strength" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Consume Type</td>
                                    <td><input type="text" name="consume_type" id="consume_type" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Route</td>
                                    <td><input type="text" name="route" id="route" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td><input type="text" name="frequency" id="frequency" class="form-control"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>




            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="hidden_edit_id" id="hidden_edit_id">
        <button type="submit" class="btn btn-success">Submit</button>
    </div>
</form>
</div>

</div>
</div>


@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function() {
        
        $(document).on('click', '.grn_drop_btn', function(e) {
            e.stopPropagation();
            $(".grn_btn_dropdown").hide();
            $(this).next().slideDown('');
        });

        $(document).on('click', '.btn_group_box', function(e) {
            e.stopPropagation();
        });

        $(document).on('click', function() {
            $(".grn_btn_dropdown").hide();
        });

        $(".select_button li").click(function() {
            $(this).toggleClass('active');
        });


        $(document).on('click', '.notes_sec_list ul li', function() {
            var disset = $(this).attr("id");
            $('.notes_sec_list ul li').removeClass("active");
            $(this).addClass("active");
            $(this).closest('.notes_box').find(".note_content").css("display", "none");
            $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
        });


        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }

        });



        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.modal').on('shown.bs.modal', function(e) {
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });

            $('.theadscroll').perfectScrollbar('update');
            $('table.theadfix_wrapper').floatThead('reflow');
        });

        $(document).on('click', '#checkbox15', function(e) {
                if($("#checkbox15").prop('checked') == true){
                        $('.min_sellable_div').show();
                }else{
                        $('.min_sellable_div').hide();
                }
         });

        $(".item_type_list").on('change', function() {

        var item_type = $('.item_type_list').val();
        if (item_type) {
            var url = "{{route('extensionsvalley.item.ajaxCategoryByItemType')}}";
            $.ajax({
                        type: "GET",
                        url: url,
                        data: 'item_type=' + item_type,
                        beforeSend: function () {
                        },
                        success: function (res) {
                         // console.log(res);
                                    if(res){
                                            $(".category_list").empty();
                                            $(".subcategory_list").empty();
                                            $(".category_list").append('<option value="0"> Select Category</option>');
                                            $.each(res,function(key,value){
                                              $(".category_list").append('<option value="'+key+'">'+value+'</option>');
                                            });
                                          
                                    }else{
                                            $(".category_list").empty();
                                            $(".subcategory_list").empty();
                                    }

                        },
                        error: function () {
                          alert('Please check your internet connection and try again');
                        },
                        complete: function () {
                        }
                    });
            
            } else {
                    $('.item_type_list').focus();
                    $(".category_list").empty();
                    $(".subcategory_list").empty();
                }
        });
        $(".category_list").on('change', function() {

            var category = $('.category_list').val();
            if (category) {
                var url = "{{route('extensionsvalley.item.ajaxSubcategoryByCategory')}}";
                $.ajax({
                            type: "GET",
                            url: url,
                            data: 'category=' + category,
                            beforeSend: function () {
                            },
                            success: function (res) {
                             // console.log(res);
                                        if(res){
                                                $(".subcategory_list").empty();
                                                $(".subcategory_list").append('<option value="0">Select Subcategory</option>');
                                                $.each(res,function(key,value){
                                                  $(".subcategory_list").append('<option value="'+key+'">'+value+'</option>');
                                                });
                                              
                                        }else{
                                                $(".subcategory_list").empty();
                                        }

                            },
                            error: function () {
                              alert('Please check your internet connection and try again');
                            },
                            complete: function () {
                            }
                        });
                
                } else {
                        $('.category_list').focus();
                        $(".subcategory_list").empty();
                    }
            });

        $(".item_type").on('change', function() {

        var item_type = $('.item_type').val();
        if (item_type) {
            var url = "{{route('extensionsvalley.item.ajaxCategoryByItemType')}}";
            $.ajax({
                        type: "GET",
                        url: url,
                        data: 'item_type=' + item_type,
                        beforeSend: function () {
                        },
                        success: function (res) {
                         // console.log(res);
                                    if(res){
                                            $(".category").empty();
                                            $(".subcategory").empty();
                                            $(".category").append('<option value="0"> Select Category</option>');
                                            $.each(res,function(key,value){
                                              $(".category").append('<option value="'+key+'">'+value+'</option>');
                                            });
                                          
                                    }else{
                                            $(".category").empty();
                                            $(".subcategory").empty();
                                    }

                        },
                        error: function () {
                          alert('Please check your internet connection and try again');
                        },
                        complete: function () {
                        }
                    });
            
            } else {
                    $('.item_type').focus();
                    $(".category").empty();
                    $(".subcategory").empty();
                }
        });
        $(".category").on('change', function() {

            var category = $('.category').val();
            if (category) {
                var url = "{{route('extensionsvalley.item.ajaxSubcategoryByCategory')}}";
                $.ajax({
                            type: "GET",
                            url: url,
                            data: 'category=' + category,
                            beforeSend: function () {
                            },
                            success: function (res) {
                             // console.log(res);
                                        if(res){
                                                $(".subcategory").empty();
                                                $(".subcategory").append('<option value="0"> Select Subcategory</option>');
                                                $.each(res,function(key,value){
                                                  $(".subcategory").append('<option value="'+key+'">'+value+'</option>');
                                                });
                                              
                                        }else{
                                                $(".subcategory").empty();
                                        }

                            },
                            error: function () {
                              alert('Please check your internet connection and try again');
                            },
                            complete: function () {
                            }
                        });
                
                } else {
                        $('.category').focus();
                        $(".subcategory").empty();
                    }
            });

        });

function editProduct(id){
    // alert("id="+id);
    $('#hidden_edit_id').val(id);
    $('.titleName').html('');
    $('.titleName').html('Edit Product Details');
     if (id != '') {
            var url = "{{route('extensionsvalley.item.ajaxGetItem')}}";
            $.ajax({
                        type: "GET",
                        url: url,
                        data: 'item_id='+ id,
                        beforeSend: function () {  },
                        success: function (data) {
                            console.log(data['itm_comp']);
                            var item = data['item'];
                            for (var i = 0; i < data['sel_uom'].length; i++) {
                                var s_uom = data['sel_uom'][i];
                                $("#sel_uom"+i).val(s_uom.uom_id);
                                $("#sel_con_fact"+i).val(s_uom.conv_factor);
                                $("#sel_default"+i).val(s_uom.default);
                            }
                            for (var i = 0; i < data['pur_uom'].length; i++) {
                                var p_uom = data['pur_uom'][i];
                                $("#pur_uom"+i).val(p_uom.uom_id);
                                $("#pur_con_fact"+i).val(p_uom.conv_factor);
                                $("#pur_default"+i).val(p_uom.default);
                            }
                            for (var i = 0; i < data['itm_comp'].length; i++) {
                                var itm_comp = data['itm_comp'][i];
                                $("#chemical_id"+i).val(itm_comp.chemical_id);
                                $("#dose"+i).val(itm_comp.dose);
                            }
                            $("#item_type").val(item.item_type_id);
                            $("#category").val(item.category_id);
                            $("#subcategory").val(item.subcategory_id);
                            $("#item_desc").val(item.item_desc);
                            $("#item_code").val(item.item_code);
                            $("#barcode").val(item.barcode);
                            $("#hsn_code").val(item.hsn_code);
                            $("#manufacturer").val(item.manufacturer_id);
                            $("#std_uom").val(item.uom_id);
                            $("#min_sellable_qty").val(item.min_sellable_qty);
                            $("#min_sellable_price").val(item.min_sellable_price);
                            $("#ved_type").val(item.ved_type);
                            $("#generic_name").val(item.generic_name_id);
                            $("#schedule").val(item.schedule);
                            $("#strength").val(item.strength);
                            $("#consume_type").val(item.consume_type);
                            $("#route").val(item.route);
                            $("#frequency").val(item.frequency);
                            if(item.status == 0){
                                $("#checkbox1").removeAttr('checked');
                            }else{
                                    $("#checkbox1").prop('checked', 'checked');
                            }if(item.purchasable_ind == 1){ 
                                $("#checkbox2").prop('checked', 'checked');
                            }else{
                                $("#checkbox2").removeAttr('checked');
                            }if(item.sellable_ind == 1){ 
                                $("#checkbox3").prop('checked', 'checked');
                            }else{
                                $("#checkbox3").removeAttr('checked');
                            }if(item.inventory_tracked_ind == 1){ 
                                $("#checkbox5").prop('checked', 'checked');
                            }else{
                                $("#checkbox5").removeAttr('checked');
                            }if(item.fixed_asset_ind == 1){ 
                                $("#checkbox6").prop('checked', 'checked');
                            }else{
                                $("#checkbox6").removeAttr('checked');
                            }if(item.consumable_ind == 1){ 
                                $("#checkbox7").prop('checked', 'checked');
                            }else{
                                $("#checkbox7").removeAttr('checked');
                            }if(item.chemo_ind == 1){ 
                                $("#checkbox8").prop('checked', 'checked');
                            }else{
                                $("#checkbox8").removeAttr('checked');
                            }if(item.narcotic_ind == 1){ 
                                $("#checkbox9").prop('checked', 'checked');
                            }else{
                                $("#checkbox9").removeAttr('checked');
                            }if(item.consignment_ind == 1){ 
                                $("#checkbox10").prop('checked', 'checked');
                            }else{
                                $("#checkbox10").removeAttr('checked');
                            }if(item.returnable_ind == 1){ 
                                $("#checkbox11").prop('checked', 'checked');
                            }if(item.perishable_ind == 1){ 
                                $("#checkbox12").prop('checked', 'checked');
                            }else{
                                $("#checkbox12").removeAttr('checked');
                            }if(item.price_editable_ind == 1){ 
                                $("#checkbox13").prop('checked', 'checked');
                            }else{
                                $("#checkbox13").removeAttr('checked');
                            }if(item.disposable_ind == 1){ 
                                $("#checkbox14").prop('checked', 'checked');
                            }else{
                                $("#checkbox14").removeAttr('checked');
                            }if(item.decimal_qty_ind == 1){ 
                                $("#checkbox15").prop('checked', 'checked');
                                $('.min_sellable_div').show();
                            }else{
                                $("#checkbox15").removeAttr('checked');
                            }
                            $("#add_list_modal").modal('show');
                            $("#itemForm").attr("action","{{route('extensionsvalley.item.editItem')}}");

                        },
                        error: function () {
                          alert('Please check your internet connection and try again');
                        },
                        complete: function () {    }
                    });
            
            } else {
                   alert("Please Select Any Record!");
                }

}
function addProductNew(){
    $('.titleName').html('Add New Product');
    $("#add_list_modal").modal('show');
}
</script>

@endsection
