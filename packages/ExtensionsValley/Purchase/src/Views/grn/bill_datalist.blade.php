<div class="col-md-12 wrapper" id="result_container_div">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="headerclass"
                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='3%'>Sn. No.</th>
                <th width='15%'>Item Description</th>
                <th width='13%'>Sub Category</th>
                <th width='6%'>HSN CODE</th>
                <th width='5%'>Unit</th>
                <th width='6%'>Discount</th>
                <th width='5%'>Qty</th>
                <th width='5%'>Rate</th>
                <th width='6%'>Taxable Amount</th>
                <th width='5%'>Tax Rate</th>
                <th width='5%'>CGST</th>
                <th width='5%'>SGST</th>
                <th width='5%'>IGST</th>
                <th width='5%'>Margin</th>
                <th width='6%'>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
        if (count($grn_array) != 0){
            $i=1;
            $netamttotal=0;
            foreach ($grn_array as $data){
                $tax_rate=intval($data->grn_tax_total_rate);
                $amount=0.0;
                $CGST=0.0;
                $SGST=0.0;
                $IGST=0.0;
                $CESS=0.0;
                If($tax_rate==0){
                    $amount=$data->exemptedsales;
                    $IGST=floatval($data->exemptedsalesigst);
                }else If($tax_rate==3){
                    $amount=$data->salesthree;
                    $CGST= floatval($data->salesthreetax) / 2;
                    $SGST= floatval($data->salesthreetax) / 2;
                    $IGST=floatval($data->salesthreeigst);
                }else If($tax_rate==5){
                    $amount=$data->salesfive;
                    $CGST= floatval($data->salesfivetax) / 2;
                    $SGST= floatval($data->salesfivetax) / 2;
                    $IGST=floatval($data->salesfivetaxigst);
                }else If($tax_rate==12){
                    $amount=$data->salestwelve;
                    $CGST= floatval($data->salestwelvetax) / 2;
                    $SGST= floatval($data->salestwelvetax) / 2;
                    $IGST=floatval($data->salestwelvetaxigst);
                }else If($tax_rate==18){
                    $amount=$data->saleseighteen;
                    $CGST= floatval($data->saleseighteentax) / 2;
                    $SGST= floatval($data->saleseighteentax) / 2;
                    $IGST=floatval($data->saleseighteentaxigst);
                }else If($tax_rate==28){
                    $amount=$data->salestwentyeight;
                    $CGST= floatval($data->salestwentyeighttax) / 2;
                    $SGST= floatval($data->salestwentyeighttax) / 2;
                    $IGST=floatval($data->salestwentyeighttaxigst);
                }
                $total=floatval($amount)+floatval($CGST)+floatval($SGST)+floatval($IGST);
                $netamttotal+=floatval($total);
                ?>
            <tr>
                <td class="common_td_rules">{{ $i }}.</td>
                <td class="common_td_rules">{{ $data->item_desc }}</td>
                <td class="common_td_rules">{{ $data->subcategory_name }}</td>
                <td class="common_td_rules">{{ $data->hsn_code }}</td>
                <td class="common_td_rules">{{ $data->uom_name }}</td>
                <td class="td_common_numeric_rules">{{ $data->bill_discount_amount }}</td>
                <td class="td_common_numeric_rules">{{ $data->qty }}</td>
                <td class="td_common_numeric_rules">{{ number_format($data->unit_rate, 2, '.', '') }}</td>
                <td class="td_common_numeric_rules">{{ number_format($amount, 2, '.', '') }}</td>
                <td class="td_common_numeric_rules">{{ $tax_rate }}</td>
                <td class="td_common_numeric_rules">{{ number_format($CGST, 2, '.', '') }}</td>
                <td class="td_common_numeric_rules">{{ number_format($SGST, 2, '.', '') }}</td>
                <td class="td_common_numeric_rules">{{ number_format($IGST, 2, '.', '') }}</td>
                <td class="td_common_numeric_rules">{{ number_format($data->margin, 2, '.', '') }}</td>
                <td class="td_common_numeric_rules">{{ number_format($total, 2, '.', '') }}</td>
            </tr>
            <?php
        $i++;
            }
            ?>
            <tr class="bg-green">
                <th colspan="14" class="common_td_rules">Total</th>
                <th class="td_common_numeric_rules">{{ number_format($netamttotal, 2, '.', '') }}</th>
            </tr>
            <?php
        }else{
            ?>
            <tr>
                <td colspan="15" style="text-align: center">
                    No Result Found
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
