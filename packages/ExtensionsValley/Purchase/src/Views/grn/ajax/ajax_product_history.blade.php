@section('tscripts')
<table>
    @endsection
    @if(isset($resultData) && count($resultData))
    @foreach($resultData as $pre_detail)
    <tr class="fixtr">
        <td>{{@$pre_detail->r_grn_no ? $pre_detail->r_grn_no : ''}}</td>
        <td>{{@$pre_detail->r_grn_date ? date('M-d-Y',strtotime($pre_detail->r_grn_date)) : ''}}</td>
        <td>{{$pre_detail->r_vendor_name}}</td>
        <td>{{@$pre_detail->r_batch ? $pre_detail->r_batch : ''}}</td>
        <td>{{@$pre_detail->r_expiry ? date('M-d-Y',strtotime($pre_detail->r_expiry)) : ''}}</td>
        <td class="number_class">{{$pre_detail->r_grn_qty}}</td>
        <td class="number_class">{{@$pre_detail->r_grn_unitname ? $pre_detail->r_grn_unitname : ''}}</td>
        <td class="number_class">{{@$pre_detail->r_grn_rate ? $pre_detail->r_grn_rate : 0}}</td>
        <td class="number_class">{{@$pre_detail->r_unit_cost ? $pre_detail->r_unit_cost : 0}}</td>
        <td class="number_class">{{@$pre_detail->r_free_qty ? $pre_detail->r_free_qty : 0}}</td>
        <td class="number_class">{{@$pre_detail->r_mrp ? $pre_detail->r_mrp : 0 }}</td>
    </tr>
    @endforeach
    @else
    <tr>
        <td colspan="9" style="text-align: center">No Result Found</td>
    </tr>
    @endif

    @section('tscripts')
</table>
@endsection
