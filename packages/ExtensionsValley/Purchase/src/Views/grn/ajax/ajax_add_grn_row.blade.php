@if(!empty($po_id))
<?php $row_count = 0; ?>
    <input type='hidden' name='po_id' value="{{$po_id}}" id="po_id_hidden">
    @foreach($po_result as $each)
    <?php $row_count++;?>
    <tr style="background: #FFF;" class="row_class" id="row_data_{{$row_count}}">
        <td class='row_count_class'>{{$row_count}}</td>
        <td style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" value="{{$each->item_desc}}" autocomplete="off" id="item_desc_{{$row_count}}" onclick="item_history({{$row_count}})"
        onkeyup='searchItemCode(this.id,event,{{$row_count}})' readonly class="form-control popinput" name="item_desc[]" placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{$row_count}}" style='text-align: left; list-style: none;
          cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
        position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
            <input type='hidden' name='row_id_hidden[]' value="{{$row_count}}" id="row_id_hidden{{$row_count}}">
            <input type='hidden' name='item_code_hidden[]' value="{{$each->item_code}}" id="item_code_hidden{{$row_count}}">
            <input type='hidden' name='item_id_hidden[]' value="{{$each->item_id}}" id="item_id_hidden{{$row_count}}">
            <input type='hidden' name='uom_name_hidden[]' value="" id="uom_name_hidden{{$row_count}}">
            <input type='hidden' name='uom_id_hidden[]' value="{{$each->pur_unit}}" id="uom_id_hidden{{$row_count}}">
            <input type='hidden' name='uom_val_hidden[]' value="" id="uom_val_hidden{{$row_count}}">
            <input type='hidden' name='unit_cost_hidden[]' value="" id="unit_cost_hidden{{$row_count}}">
            <input type='hidden' name='unit_mrp_hidden[]' value="" id="unit_mrp_hidden{{$row_count}}">
            <input type='hidden' name='mrp_hidden[]' value="{{$each->mrp}}" id="mrp_hidden{{$row_count}}">
            <input type='hidden' name='rate_hidden[]' value="{{$each->pur_rate}}" id="rate_hidden{{$row_count}}">
            <input type='hidden' name='unit_rate_hidden[]' value="" id="unit_rate_hidden{{$row_count}}">
            <input type='hidden' name='unit_mrp_hidden[]' value="" id="unit_mrp_hidden{{$row_count}}">
            <input type='hidden' name='free_qty_hidden[]' value="{{$each->free_qty}}" id="free_qty_hidden{{$row_count}}">
            <input type='hidden' name='qty_hidden[]' value="{{$each->pur_qnty}}" id="qty_hidden{{$row_count}}">
            <input type='hidden' name='total_tax_amount_hidden[]' value="{{$each->total_tax_amount}}" id="total_tax_amount_hidden{{$row_count}}">
            <input type='hidden' name='total_tax_perc_hidden[]' value="{{$each->total_tax_perc}}" id="total_tax_perc_hidden{{$row_count}}">
            <input type='hidden' name='po_balance_qty_hidden[]' value="{{$each->po_balance_qty}}" id="po_balance_qty_hidden{{$row_count}}">
            <input type='hidden' name='total_free_tax_amount_hidden[]' value="{{$each->total_free_tax_amount}}" id="total_free_tax_amount_hidden{{$row_count}}">
            <input type='hidden' name='total_free_tax_perc_hidden[]' value="{{$each->total_free_tax_perc}}" id="total_free_tax_perc_hidden{{$row_count}}">
            <input type='hidden' name='total_other_charge_hidden[]' value="{{$each->total_other_charge}}" id="total_other_charge_hidden{{$row_count}}">
            <input type='hidden' name='total_disc_amount_hidden[]' value="{{$each->total_disc_amount}}" id="total_disc_amount_hidden{{$row_count}}">
            <input type='hidden' name='po_detail_id_hidden[]' value="{{$each->po_detail_id}}" id="po_detail_id_hidden{{$row_count}}">
            <input type='hidden' name='is_free_hidden[]' value="0" id="is_free_hidden{{$row_count}}">
            <input type='hidden' name='tot_mrp[]' value="0" id="tot_mrp_{{$row_count}}">
            <input type='hidden' name='tot_rate[]' value="0" id="tot_rate_{{$row_count}}">

        </td>


        <td>
            <button id="getGrnTaxRateBtn{{$row_count}}" class="btn light_purple_bg grn_drop_btn" onclick="chargesModel({{$row_count}})" ><i id="getGrnTaxRateSpin{{$row_count}}" class="fa fa-plus"></i></button>
        </td>
        <td><input name='batch[]' readonly id="batch_row{{$row_count}}" class="form-control" type="text" value=""></td>
        <td><input name='expiry_date[]' id="expiry_date{{$row_count}}" class="form-control expiry_date number_class" readonly type="text" value=""></td>
        <td><input class="form-control number_class" readonly name='grn_qty[]' type="text" value="0"></td>
        <td><input class="form-control number_class" readonly name='free_qty[]' type="text" value="0"></td>
        <td title="Unit converion">
            <?php
                $pur_unit = !empty($each->pur_unit)?$each->pur_unit:0;
                $item_id = !empty($each->item_id)?$each->item_id:0;
                $pur_list = \DB::table('item_uom as iu')->select('u.uom_name','u.id','iu.conv_factor')
                    ->join('uom as u','u.id','=','iu.uom_id')->where('iu.uom_type',1)
                            ->whereNotNull('iu.conv_factor')->where('iu.item_id',$item_id)->get();
            ?>
             <select name="uom_select[]" id="uom_select_id_{{$row_count}}" disabled class="form-control">
                @foreach($pur_list as $pur_each)
                <?php
                    $selected = '';
                    if($pur_unit == $pur_each->id){$selected = 'selected';}
                ?>
                    <option value='{{$pur_each->id}}'  {{$selected}} data-uom_value='{{$pur_each->conv_factor}}'>{{$pur_each->uom_name}}</option>
                @endforeach
            </select>

        </td>
        <td><input class="form-control number_class" id="tot_qty_{{$row_count}}" readonly name="tot_qty[]" type="text" value="0"></td>
        <td><input class="form-control number_class" id="unit-cost_{{$row_count}}" readonly name="unit-cost[]" type="text" value="0"></td>
        <td>
            <input class="form-control number_class" id="unit-mrp_{{$row_count}}" readonly name="unit-mrp[]" type="text" value="0">
        </td>
        <td>
            <input class="form-control number_class" id="sales_price_{{$row_count}}" readonly name="sales_price[]" type="text" value="0">
        </td>
        <td><input class="form-control number_class" id="tot_tax_amt_{{$row_count}}" readonly name="tot_tax_amt[]" type="text" value="0"></td>
        <td><input class="form-control number_class" id="tot_dic_amt_{{$row_count}}" readonly name="tot_dic_amt[]" type="text" value="0"></td>
        <td><input class="form-control number_class" id="net_cost_{{$row_count}}" readonly name="net_cost[]" type="text" value="0"></td>
        <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{$row_count}})" class="fa fa-trash text-red deleteRow"></i></td>
    </tr>
    @endforeach
@elseif(!empty($bill_id))
<?php $row_count = 0;?>
    <input type='hidden' name='bill_id' value="{{$bill_id}}" id="bill_id_hidden">
    <input type='hidden' name='grn_no_hidden' value="{{$grn_no}}" id="grn_no_hidden">
    @foreach($po_result as $each)
    <?php $row_count++;?>
    <tr style="background: #FFF;" class="row_class" id="row_data_{{$row_count}}">
        <td class='row_count_class'>{{$row_count}}</td>
        <td  style="position: relative;">
        <input style="border-radius: 4px;" type="text" required="" value="{{$each->item_desc}}" autocomplete="off" id="item_desc_{{$row_count}}" onclick="item_history({{$row_count}})"
        readonly class="form-control popinput" name="item_desc[]" placeholder="Search Item">
        <div class='ajaxSearchBox' id="ajaxSearchBox_{{$row_count}}" style='text-align: left; list-style: none;
          cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
        position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
            <input type='hidden' name='row_id_hidden[]' value="{{$row_count}}" id="row_id_hidden{{$row_count}}">
            <input type='hidden' name='item_code_hidden[]' value="{{$each->item_code}}" id="item_code_hidden{{$row_count}}">
            <input type='hidden' name='item_id_hidden[]' value="{{$each->item_id}}" id="item_id_hidden{{$row_count}}">
            <input type='hidden' name='uom_name_hidden[]' value="" id="uom_name_hidden{{$row_count}}">
            <input type='hidden' name='uom_id_hidden[]' value="{{$each->grn_unit}}" id="uom_id_hidden{{$row_count}}">
            <input type='hidden' name='uom_val_hidden[]' value="" id="uom_val_hidden{{$row_count}}">
            <input type='hidden' name='unit_cost_hidden[]' value="" id="unit_cost_hidden{{$row_count}}">
            <input type='hidden' name='unit_mrp_hidden[]' value="" id="unit_mrp_hidden{{$row_count}}">
            <input type='hidden' name='mrp_hidden[]' value="{{$each->grn_mrp}}" id="mrp_hidden{{$row_count}}">
            <input type='hidden' name='rate_hidden[]' value="{{$each->grn_rate}}" id="rate_hidden{{$row_count}}">
            <input type='hidden' name='unit_rate_hidden[]' value="" id="unit_rate_hidden{{$row_count}}">
            <input type='hidden' name='unit_mrp_hidden[]' value="" id="unit_mrp_hidden{{$row_count}}">
            <input type='hidden' name='free_qty_hidden[]' value="{{$each->grn_free_qty}}" id="free_qty_hidden{{$row_count}}">
            <input type='hidden' name='qty_hidden[]' value="{{$each->grn_qty}}" id="qty_hidden{{$row_count}}">

            <input type='hidden' name='po_detail_id_hidden[]' value="" id="po_detail_id_hidden{{$row_count}}">
            <input type='hidden' name='bill_detail_id_hidden[]' value="{{$each->bill_detail_id}}" id="bill_detail_id_hidden{{$row_count}}">
            <input type='hidden' name='is_free_hidden[]' value="0" id="is_free_hidden{{$row_count}}">
            <input type='hidden' name='tot_mrp[]' value="0" id="tot_mrp_{{$row_count}}">
            <input type='hidden' name='tot_rate[]' value="0" id="tot_rate_{{$row_count}}">

        </td>


        <td>
            <button id="getGrnTaxRateBtn{{$row_count}}" class="btn light_purple_bg grn_drop_btn" onclick="chargesModel({{$row_count}})" ><i id="getGrnTaxRateSpin{{$row_count}}" class="fa fa-plus"></i></button>
        </td>
        <td><input name='batch[]' readonly id="batch_row{{$row_count}}" class="form-control" type="text" value="{{$each->batch}}"></td>
        <td><input name='expiry_date[]' id="expiry_date{{$row_count}}" class="form-control expiry_date number_class" readonly type="text" value="{{$each->expiry}}"></td>
        <td><input class="form-control number_class" readonly name='grn_qty[]' type="text" value="0"></td>
        <td><input class="form-control number_class" readonly name='free_qty[]' type="text" value="0"></td>
        <td title="Unit converion">
            <?php
                $grn_unit = !empty($each->grn_unit)?$each->grn_unit:0;
                $item_id = !empty($each->item_id)?$each->item_id:0;
                $pur_list = \DB::table('item_uom as iu')->select('u.uom_name','u.id','iu.conv_factor')
                    ->join('uom as u','u.id','=','iu.uom_id')->where('iu.uom_type',1)
                            ->whereNotNull('iu.conv_factor')->where('iu.item_id',$item_id)->get();
            ?>
             <select name="uom_select[]" id="uom_select_id_{{$row_count}}" disabled class="form-control">
                @foreach($pur_list as $pur_each)
                <?php
                    $selected = '';
                    if($grn_unit == $pur_each->id){$selected = 'selected';}
                ?>
                    <option value='{{$pur_each->id}}'  {{$selected}} data-uom_value='{{$pur_each->conv_factor}}' {{$selected}}>{{$pur_each->uom_name}}</option>
                @endforeach
            </select>

        </td>
        <td><input class="form-control number_class" id="tot_qty_{{$row_count}}" readonly name="tot_qty[]" type="text" value="0"></td>
        <td><input class="form-control number_class" id="unit-cost_{{$row_count}}" readonly name="unit-cost[]" type="text" value="0"></td>
        <td>
            <input class="form-control number_class" id="unit-mrp_{{$row_count}}" readonly name="unit-mrp[]" type="text" value="0">
        </td>
        <td>
            <input class="form-control number_class" id="sales_price_{{$row_count}}" readonly name="sales_price[]" type="text" value="0">
        </td>
        <td>
            <input class="form-control number_class" id="tot_tax_amt_{{$row_count}}" readonly name="tot_tax_amt[]" type="text" value="0">
        </td>
        <td><input class="form-control number_class" id="tot_dic_amt_{{$row_count}}" readonly name="tot_dic_amt[]" type="text" value="0"></td>
        <td><input class="form-control number_class" id="net_cost_{{$row_count}}" readonly name="net_cost[]" type="text" value="0"></td>
        <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{$row_count}})" class="fa fa-trash text-red deleteRow"></i></td>
    </tr>
    @endforeach
@else
<?php $row_count = ++$row_count;?>
<tr style="background: #FFF;" class="row_class" id="row_data_{{$row_count}}">
        <td class='row_count_class'>{{$row_count}}</td>
        <td style="position: relative;">
         <input style="border-radius: 4px;" type="text" required="" autocomplete="off" id="item_desc_{{$row_count}}" onkeyup='searchItemCode(this.id,event,{{$row_count}})' onclick="item_history({{$row_count}})"  class="form-control popinput" name="item_desc[]" placeholder="Search Item">
         <div class='ajaxSearchBox' id="ajaxSearchBox_{{$row_count}}" style='text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px; " "overflow-y: auto; width: 34%; z-index: 599;
           position:absolute; background: #ffffff;  border-radius: 3px;  "border: 1px solid rgba(0, 0, 0, 0.3); '> </div>
          <input type='hidden' name='row_id_hidden[]' value="{{$row_count}}" id="row_id_hidden{{$row_count}}">
          <input type='hidden' name='item_code_hidden[]' value="" id="item_code_hidden{{$row_count}}">
          <input type='hidden' name='item_id_hidden[]' value="" id="item_id_hidden{{$row_count}}">
          <input type='hidden' name='uom_name_hidden[]' value="" id="uom_name_hidden{{$row_count}}">
          <input type='hidden' name='uom_id_hidden[]' value="" id="uom_id_hidden{{$row_count}}">
          <input type='hidden' name='uom_val_hidden[]' value="" id="uom_val_hidden{{$row_count}}">
          <input type='hidden' name='unit_cost_hidden[]' value="" id="unit_cost_hidden{{$row_count}}">
          <input type='hidden' name='unit_mrp_hidden[]' value="" id="unit_mrp_hidden{{$row_count}}">
          <input type='hidden' name='mrp_hidden[]' value="" id="mrp_hidden{{$row_count}}">
          <input type='hidden' name='rate_hidden[]' value="" id="rate_hidden{{$row_count}}">
          <input type='hidden' name='unit_rate_hidden[]' value="" id="unit_rate_hidden{{$row_count}}">
          <input type='hidden' name='unit_mrp_hidden[]' value="" id="unit_mrp_hidden{{$row_count}}">
          <input type='hidden' name='free_qty_hidden[]' value="" id="free_qty_hidden{{$row_count}}">
          <input type='hidden' name='qty_hidden[]' value="" id="qty_hidden{{$row_count}}">
          <input type='hidden' name='is_free_hidden[]' value="0" id="is_free_hidden{{$row_count}}">
          <input type='hidden' name='tot_mrp[]' value="0" id="tot_mrp_{{$row_count}}">
          <input type='hidden' name='tot_rate[]' value="0" id="tot_rate_{{$row_count}}">
        </td>
        <td>
            <button id="getGrnTaxRateBtn{{$row_count}}" class="btn light_purple_bg grn_drop_btn" onclick="chargesModel({{$row_count}})" ><i id="getGrnTaxRateSpin{{$row_count}}" class="fa fa-plus"></i></button>
        </td>
        <td><input name='batch[]' readonly id="batch_row{{$row_count}}" class="form-control" type="text" value=""></td>
        <td><input name='expiry_date[]' id="expiry_date{{$row_count}}" id="expiry_date{{$row_count}}" class="form-control expiry_date number_class" readonly type="text" value=""></td>
     <td><input class="form-control number_class" readonly name='grn_qty[]' type="text" value="0.00"></td>
     <td><input class="form-control number_class" readonly name='free_qty[]' type="text" value="0.00"></td>
     <td title="Unit converion">
         <select name="uom_select[]" id="uom_select_id_{{$row_count}}" disabled class="form-control">

         </select>
     </td>
     <td><input class="form-control number_class" id="tot_qty_{{$row_count}}" readonly name="tot_qty[]" type="text" value="0.00"></td>
     <td><input class="form-control number_class" id="unit-cost_{{$row_count}}" readonly name="unit-cost[]" type="text" value="0"></td>
    <td>
        <input class="form-control number_class" id="unit-mrp_{{$row_count}}" readonly name="unit-mrp[]" type="text" value="0">
    </td>
    <td>
        <input class="form-control number_class" id="sales_price_{{$row_count}}" readonly name="sales_price[]" type="text" value="0">
    </td>
     <td><input class="form-control number_class" id="tot_tax_amt_{{$row_count}}" readonly name="tot_tax_amt[]" type="text" value="0.00"></td>
     <td><input class="form-control number_class" id="tot_dic_amt_{{$row_count}}" readonly name="tot_dic_amt[]" type="text" value="0.00"></td>
     <td><input class="form-control number_class" id="net_cost_{{$row_count}}" readonly name="net_cost[]" type="text" value="0.00"></td>
     <td><i style="padding: 5px 8px; font-size: 15px;" onclick="removeRow({{$row_count}})" class="fa fa-trash text-red deleteRow"></i></td>
 </tr>
 @endif
