<div class="row">
    <div class="col-md-12">
        <?php
        $is_checked = '';
        $is_readonly = '';
        $pur_list = \DB::table('item_uom as iu')
            ->select('u.uom_name', 'u.id', 'iu.conv_factor')
            ->join('uom as u', 'u.id', '=', 'iu.uom_id')
            ->where('iu.uom_type', 1)
            ->whereNotNull('iu.conv_factor')
            ->where('iu.item_id', $item_id)
            ->get();

        if (!empty($item_arr[$row_id]['is_free']) && $item_arr[$row_id]['is_free'] == 1) {
            $is_checked = 'checked';
            $is_readonly = 'readonly';
        } ?>
        <table class="table no-border table_sm">
            <input id="batch_needed" type="hidden" value="{{ $batch_needed }}">
            <input id="batch_item_id" type="hidden" value="{{ $item_id }}">
            <input id="batch_popup_itemcode" type="hidden" value="{{ $item_code }}">
            <input id="popup_rowid" type="hidden" value="{{ $row_id }}">
            <input id="stock_data" type="hidden" value="{{ $stock_data }}">


            <input id="selling_price_data{{ $row_id }}" type="hidden"
                value="{{ @$selling_price ? $selling_price : 0 }}">
            <input id="selling_price_edited{{ $row_id }}" type="hidden" value="0">
            <tbody>
                <tr>
                    <td colspan="3">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" style="font-size: 13px !important;font-weight: 700"
                            class="form-control validation" readonly value="{{ $param['item_desc'] }}"
                            placeholder="Item Name">
                    </td>
                </tr>
                <tr>
                    <td width="25%">
                        <label for="" style="font-size: 12px;font-weight: 700">Batch No</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" name="batch_pop[]" onblur="checkbatchStatus()"
                            blockSpecialChar(event);batchEntry(this,'{{ $row_id }}')" id='batch_pop'
                            class="form-control validation" value="<?= $batch_row ?>">
                    </td>
                    <td width="25%">
                        <label for="" style="font-size: 12px;font-weight: 700">Expiry</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" name="Expiry_pop[]"
                            onblur="ExpiryEntry(this,'{{ $row_id }}')" id='Expiry_date_pop'
                            class="form-control validation"
                            value="@if (!empty($item_arr[$row_id]['Expiry'])) {{ $item_arr[$row_id]['Expiry'] }} @endif">
                    </td>
                    <td width="50%">
                        <label for="" style="font-size: 12px;font-weight: 700">HSN Code</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" name="hsn_code[]"
                            onblur="updateHsnCode({{ $row_id }})" class="form-control validation" id='hsn_code'
                            value="@if (!empty($item_arr[$row_id]['hsn_code'])) {{ $item_arr[$row_id]['hsn_code'] }}@else{{ $hsn_code }} @endif">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="ht10"></div>
    <div class="col-md-12">
        <div class="col-md-6 padding_xs">
            <table class="table no-border table-striped table_sm">
                <tbody>
                    <tr>

                        <td><label for=""><b>Free Item</b></label></td>
                        <td><input type="checkbox" {{ $is_checked }}
                                onchange="freeItem(this,'{{ $row_id }}');" id="free_item_id" name="free_item">
                        </td>
                    </tr>
                    <tr>
                        <td><label for=""><b>GRN Qty</b></label></td>
                        <td><input type="text" autocomplete="off" index='2'
                                onblur="priceCalculator(this,'{{ $row_id }}')"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="@if (!empty($item_arr[$row_id]['grn_qty'])) {{ trim($item_arr[$row_id]['grn_qty']) }}@else{{ 0 }} @endif"
                                id="grn_qty" name="grn_qty" class="form-control number_class1"></td>
                    </tr>
                    <tr>
                        <td><label for=""><b>GRN Free Qty</b></label></td>
                        <td><input type="text" autocomplete="off"
                                onblur="priceCalculator(this,'{{ $row_id }}')"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="@if (!empty($item_arr[$row_id]['free_qty'])) {{ trim($item_arr[$row_id]['free_qty']) }}@else{{ 0 }} @endif"
                                id="free_qty" name="free_qty" class="form-control number_class1"></td>
                    </tr>
                    <tr>
                        <td><label for=""><b>UOM</b></label></td>
                        <td>
                            <select name="uom_select_pop[]" id="uom_select_pop_id_{{ $row_id }}"
                                onchange="fetchUOMValue({{ $row_id }});" class="form-control">
                                @foreach ($pur_list as $pur_each)
                                    <option value='{{ $pur_each->id }}'
                                        @if (!empty($item_arr[$row_id]['grn_unit']) && $item_arr[$row_id]['grn_unit'] == $pur_each->id) selected @endif
                                        data-uom_value='{{ $pur_each->conv_factor }}'>{{ $pur_each->uom_name }}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for=""><b>UOM Value</b></label></td>
                        <td><input type="text" autocomplete="off" readonly
                                value="@if (!empty($item_arr[$row_id]['uom_val'])) {{ number_format(trim($item_arr[$row_id]['uom_val']), $decimalConfiguration, '.', '') }}@else{{ 0 }} @endif"
                                id="uom_val" name="uom_val" class="form-control number_class1"></td>
                    </tr>
                    <tr>
                        <td><label for=""><b>Rate(Per UOM)</b></label></td>
                        <td><input type="text" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onblur="priceCalculator(this,'{{ $row_id }}');mrpCheck();"
                                value="@if (!empty($item_arr[$row_id]['rate'])) {{ number_format(trim($item_arr[$row_id]['rate']), $decimalConfiguration, '.', '') }}@else{{ 0 }} @endif"
                                id="rate" {{ $is_readonly }} name="rate"
                                class="form-control validation number_class1"></td>
                    </tr>
                    <tr>
                        <td><label for=""><b>Unit Rate</b></label></td>
                        <td><input type="text" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onblur="priceCalculator(this,'{{ $row_id }}')"
                                value="@if (!empty($item_arr[$row_id]['unit_rate'])) {{ number_format(trim($item_arr[$row_id]['unit_rate']), $decimalConfiguration, '.', '') }}@else{{ 0 }} @endif"
                                id="unit_rate" readonly name="unit_rate" class="form-control number_class1"></td>
                    </tr>

                    <tr>
                        <td><label for=""><b>MRP(Per UOM)</b></label></td>
                        <td><input type="text" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onblur="priceCalculator(this,'{{ $row_id }}');mrpCheck();"
                                value="@if (!empty($item_arr[$row_id]['mrp'])) {{ number_format(trim($item_arr[$row_id]['mrp']), $decimalConfiguration, '.', '') }}@else{{ 0 }} @endif"
                                id="mrp" name="mrp" class="form-control validation number_class1"></td>
                    </tr>
                    <tr>
                        <td><label for=""><b>Unit MRP</b></label></td>
                        <td><input type="text" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onblur="priceCalculator(this,'{{ $row_id }}')"
                                value="@if (!empty($item_arr[$row_id]['unit_mrp'])) {{ number_format(trim($item_arr[$row_id]['unit_mrp']), $decimalConfiguration, '.', '') }}@else{{ 0 }} @endif"
                                id="unit_mrp" readonly name="unit_mrp" class="form-control number_class1"></td>
                    </tr>
                    <tr>
                        <?php
                        $checked = '';
                        $readonly = 'readonly';
                        if (!empty($item_arr[$row_id]['unit_check']) && $item_arr[$row_id]['unit_check'] == 1) {
                            $readonly = '';
                            $checked = 'checked';
                        } ?>
                        <td><label for=""><b>Edit Selling Price</b></label></td>
                        <td><input type="checkbox" id="unit_check_box" {{ $checked }}
                                onchange="unitMRPCh({{ $row_id }});priceCalculator(this,{{ $row_id }})"
                                value="1" name="unit_check_box"></td>
                    </tr>
                    <tr>
                        <td><label for=""><b>Selling Price</b></label></td>
                        <td>
                            <input type="text" autocomplete="off"
                                onkeyup="checkSellingPrice({{ $row_id }})"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                onblur="priceCalculator(this,'{{ $row_id }}')"
                                value="@if (!empty($item_arr[$row_id]['selling_price'])) {{ number_format(trim($item_arr[$row_id]['selling_price']), $decimalConfiguration, '.', '') }}@else{{ 0 }} @endif"
                                id="selling_price" {{ $readonly }} name="selling_price"
                                class="form-control number_class1">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <input type="hidden" id="dataentryType">
        <input type="hidden" id="row_id_hidden" value="<?= $row_id ?>">
        <div class="col-md-6 padding_xs">
            <table class="table table-bordered table-striped table_sm">
                <thead>
                    <tr class="table_header_light_purple_bg">
                        <th style="width: 108px;">Description</th>
                        <th>%</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody id='charges_table'>
                    @foreach ($charges_head as $charge)
                        <tr>
                            <td>{{ $charge->name }}</td>
                            <?php
                            $val = 0;
                            if (!empty($charge_item[$charge->code][$row_id][$charge->status_code])) {
                                if ($charge->code == 'TAX') {
                                    $val = $charge_item[$charge->code][$row_id][$charge->status_code]['tax'];
                                }
                                if ($charge->code == 'DISCOUNT') {
                                    if ($charge->status_code == 'DAH') {
                                        $val = $charge_item[$charge->code][$row_id][$charge->status_code]['discount'];
                                    } else {
                                        $val = $charge_item[$charge->code][$row_id][$charge->status_code]['discount'];
                                    }
                                }
                                if ($charge->code == 'FREE') {
                                    $val = $charge_item[$charge->code][$row_id][$charge->status_code]['free'];
                                }
                                if ($charge->code == 'OTHCH') {
                                    $val = $charge_item[$charge->code][$row_id][$charge->status_code]['other'];
                                }
                                if ($charge->code == 'GA') {
                                    $val = $charge_item[$charge->code][$row_id][$charge->status_code]['discount'];
                                }
                            }
                            ?>
                            <td><input class="form-control number_class calcu_percent valid_number_chk"
                                    @if ($charge->code != 'OTHCH') onblur="calcPercentage(this,'{{ $row_id }}','{{ $charge->code }}','{{ $charge->calc_code }}','{{ $charge->charge_code }}','{{ $charge->id }}','0','0.00','','{{ $charge->name }}','{{ $charge->status_code }}',1);" @endif
                                    type="text" autocomplete="off" data-id="{{ $charge->id }}"
                                    data-code="{{ $charge->code }}"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    id="charge_percentage_{{ $charge->id }}" data-calc="{{ $charge->calc_code }}"
                                    data-status_code="{{ $charge->status_code }}"
                                    data-charge="{{ $charge->calc_code }}"
                                    @if ($charge->code == 'OTHCH') readonly="" value="0.00" @else value="{{ $val }}" @endif
                                    @if ($charge->status_code == 'DAH') readonly @endif
                                    @if ($charge->code == 'DISCOUNT') name="discount_{{ $charge->status_code }}_percentage[]"  @else name="charge_{{ $charge->status_code }}_percentage[]" @endif
                                    placeholder="{{ $charge->name }}">
                            </td>
                            <td><input class="form-control number_class calcu_amount valid_number_chk"
                                    data-code="{{ $charge->code }}" status_code="{{ $charge->calc_code }}"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    @if ($charge->code == 'OTHCH' || $charge->code == 'DISCOUNT') onblur="calcPercentage(this,'{{ $row_id }}','{{ $charge->code }}','{{ $charge->calc_code }}','{{ $charge->charge_code }}','{{ $charge->id }}','0','0.00','','{{ $charge->name }}','{{ $charge->status_code }}',2);" @endif
                                    data-calc="{{ $charge->id }}" data-charge="{{ $charge->calc_code }}"
                                    id="charge_amount_{{ $charge->id }}"
                                    @if ($charge->code == 'DISCOUNT') name="discount_{{ $charge->status_code }}_amount[]" @else name="charge_amount[]" @endif
                                    data-status_code="{{ $charge->status_code }}" type="text" autocomplete="off"
                                    @if ($charge->code == 'DISCOUNT') @elseif($charge->code != 'OTHCH') readonly=""  value="0.00" @else value="{{ $val }}" @endif
                                    placeholder="{{ $charge->name }}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="ht10"></div>
            <div class="ht10"></div>
            <div class="ht5"></div>
            <table class="table no-border table-striped table_sm">
                <tbody>
                    <tr>
                        <td><label for=""><b>Unit Cost</b></label></td>
                        <td><input type="text" autocomplete="off"
                                onblur="priceCalculator(this,'{{ $row_id }}')"
                                value="@if (!empty($item_arr[$row_id]['unit_rate'])) {{ $item_arr[$row_id]['unit_rate'] }}@else{{ 0 }} @endif"
                                id="unit_cost" readonly name="unit_cost" class="form-control number_class1"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-3"><b>Total Rate</b></div>
        <div class="col-md-3"><input type="text" autocomplete="off"
                style="color: red;font-weight:700;font-size:14px !important;" class="form-control number_class"
                readonly id='tot_rate_cal'></div>
        <div class="col-md-3"><b>Net Rate</b></div>
        <div class="col-md-3"><input type="text" autocomplete="off"
                style="color: red;font-weight:700;font-size:14px !important;" class="form-control number_class"
                readonly id='charges_tot_pop'></div>
    </div>
</div>
