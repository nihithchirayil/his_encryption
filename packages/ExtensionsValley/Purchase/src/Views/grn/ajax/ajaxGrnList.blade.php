<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                $('#re_edit_modal').modal('hide');
                $('#grn_edit_details').html('');
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var grn_id = $('#grn_id_hidden').val();
                var bill_no = $('#bill_no_search').val();
                var vendor_id = $('#vendoritem_id_hidden').val();
                var item_desc = $('#item_desc').val();
                var item_code = $('#item_desc_hidden').val();
                var location = $('#to_location').val();
                var status = $('#status').val();
                var grn_no = $('#grn_no_search').val();
                var bill_id = $('#bill_id_hidden').val();
                var vendor_name = $('#vendor_name_search').val();
                var insert_from = $('#insert_from').val();

                localStorage.setItem("from_date", from_date);
                localStorage.setItem("to_date", to_date);
                localStorage.setItem("grn_no", grn_no);
                localStorage.setItem("grn_id", grn_id);
                localStorage.setItem("bill_no", bill_no);
                localStorage.setItem("bill_id", bill_id);
                localStorage.setItem("vendor_id", vendor_id);
                localStorage.setItem("vendor_name", vendor_name);
                localStorage.setItem("item_desc", item_desc);
                localStorage.setItem("item_code", item_code);
                localStorage.setItem("store_location", location);
                localStorage.setItem("status", status);

                var param = {
                    _token: token,
                    grn_id: grn_id,
                    from_date: from_date,
                    to_date: to_date,
                    vendor_id: vendor_id,
                    item_code: item_code,
                    bill_no: bill_no,
                    location: location,
                    insert_from: insert_from,
                    status: status
                };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#searchGrnListData").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    $('#searchgrnlistBtn').attr('disabled', true);
                    $('#searchgrnlistSpin').removeClass('fa fa-search');
                    $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    $('#searchGrnListData').html(data);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function () {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function () {
                    $("#searchGrnListData").LoadingOverlay("hide");
                    $('#searchgrnlistBtn').attr('disabled', false);
                    $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchgrnlistSpin').addClass('fa fa-search');
                },
                error: function () {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
            }
            return false;
        });

    });
</script>
<table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
    style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th width="10%">GRN No</th>
            <th width="8%">Bill No.</th>
            <th width="7%">Invoice Date</th>
            <th width="9%">Location</th>
            <th width="8%">Created Date</th>
            <th width="8%">Created By</th>
            <th width="8%">Approved Date</th>
            <th width="10%">Approved By</th>
            <th width="10%">PO No.</th>
            <th width="12%">Vendor Name</th>
            <th width="10%">Net Amount</th>
            <th width="12%">Status</th>
            <th width="3%" title="Accounts Receive Status"><i class="fa fa-list"></i></th>
            <th width="3%"><i class="fa fa-pencil-square"></i></th>
            <th width="3%"><i class="fa fa-info"></i></th>
            <th width="3%"><i class="fa fa-edit"></i></th>
        </tr>
    </thead>
    <tbody>
        @if (count($item) > 0)
        @foreach ($item as $each)
        <?php

                $status = '';
                if ($each->bill_partially_receive_status == 1) {
                    $status = 'Partially Approved';
                } elseif ($each->approve_status == 1) {
                    $status = 'Not Approved';
                } elseif ($each->approve_status == 2) {
                    $status = 'Approved';
                } elseif ($each->approve_status == 3) {
                    $status = 'Cancelled';
                } elseif ($each->approve_status == 4) {
                    $status = 'Closed';
                }
                $display = '';
                $accounts_receive_status = 'fa fa-times-circle red';
                if (intval($each->accounts_receive_status) == 1) {
                    $accounts_receive_status = 'fa fa-check-circle green';
                }

                if ($status == 'Approved') {
                    $display = 'none';
                }
                if (isset($is_editable[$each->grn_no][0])) {
                    if ($is_editable[$each->grn_no][0]['request_status'] == 1) {
                        $req_type = 'info';
                        $display = 'none';
                    } elseif ($is_editable[$each->grn_no][0]['request_status'] == 2) {
                        $req_type = 'primary';
                        $display = 'block';
                    } elseif ($is_editable[$each->grn_no][0]['request_status'] == 3) {
                        $req_type = 'danger';
                        $display = 'none';
                    } elseif ($is_editable[$each->grn_no][0]['request_status'] == 4) {
                        $req_type = 'success';
                        $display = 'none';
                    } else {
                        $req_type = 'default';
                        $display = 'none';
                    }
                } else {
                    $req_type = 'default';
                }

                if ($edit_access_type == 1) {
                    $display = '';
                }

                ?>
        <tr>
            <td class="common_td_rules">{{ $each->grn_no }}</td>
            <td class="common_td_rules">{{ $each->bill_no }}</td>
            <td class="common_td_rules">{{ $each->bill_date }}</td>
            <td class="common_td_rules">{{ $each->location }}</td>
            <td class="common_td_rules" title="{{ $each->created_at }}">
                {{ $each->created_at }}
            </td>
            <td class="common_td_rules" title="{{ $each->created_by }}">
                {{ $each->created_by }}
            </td>
            <td class="common_td_rules" title="{{ $each->approved_at }}">
                {{ $each->approved_at }}
            </td>
            <td class="common_td_rules" title="{{ $each->approved_by }}">
                {{ $each->approved_by }}
            </td>
            <td class="common_td_rules" title="{{ $each->po_no }}">
                @if (!empty($each->po_no))
                {{ $each->po_no }}
                @else
                --
                @endif
            </td>
            <td class="common_td_rules" title="{{ $each->vendor_name }}">
                {{ $each->vendor_name }}
            </td>
            <td class="common_td_rules" title="{{ $each->net_amount }}">
                {{ $each->net_amount }}
            </td>
            <td class="common_td_rules" title="{{ $status }}">
                {{ $status }}
            </td>
            <td style="text-align: center;">
                <i class="{{ $accounts_receive_status }}"></i>
            </td>
            <td style="text-align: center;">
                @if ($status == 'Approved')
                <button title="GRN Edit Request" onclick="fetchGrnRequest({{ $each->bill_id }},'{{ $each->grn_no }}',0)"
                    id="fetch_req_btn{{ $each->bill_id }}" type="button" class="btn btn-{{ $req_type }}"><i
                        id="fetch_req_spin{{ $each->bill_id }}" class="fa fa-pencil"></i>
                </button>
                @endif
            </td>
            <td style="text-align: center;">
                <button title="GRN View" onclick="grn_listview(1,'{{ $each->bill_id }}')"
                    id="grn_listviewbtn{{ $each->bill_id }}" type="button" class="btn btn-info"><i
                        id="grn_listviewspin{{ $each->bill_id }}" class="fa fa-info"></i>
                </button>
            </td>
            <td style="text-align: center;">
                @if ($status != 'Cancelled')
                <button title="GRN Edit" id="provision_to_edit{{ $each->bill_id }}" type="button"
                    style="display: {{ $display }}"
                    onclick="billEditLoadData(this,'{{ $each->bill_id }}','{{ $each->insert_from }}')"
                    class="btn btn-warning"><i class="fa fa-edit"></i></button>
                @endif
            </td>

        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="16" class="location_code">No Records found</td>
        </tr>
        @endif
    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination">
        {!! $page_links !!}
    </ul>
</div>
