<div class="row">
    <div class="col-md-12" id="result_container_div theadscroll_it">
            <table id="result_data_table" class="table no-margin table-striped table-col-bordered table-condensed theadfix_wrapper_it"
            style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th width='3%'  style="white-space: pre">SL.No.</th>
                        <th width='10%' style="white-space: pre">Requested By</th>
                        <th width='8%' style="white-space: pre">Requested At</th>
                        <th width='10%' style="white-space: pre">Approved By</th>
                        <th width='8%' style="white-space: pre">Approved At</th>
                        <th width='50%' style="white-space: pre">Remark</th>
                        <th width='10%' style="white-space: pre">Request Status</th>
                        <th width='3%' style="white-space: pre"><i class="fa fa-edit"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr>
                                <td class="common_td_rules">{{ $loop->iteration }}.</td>
                                <td class="common_td_rules">{{ @$data->requested_by ? $data->requested_by : '--' }}</td>
                                <td class="common_td_rules">{{ @$data->requested_at ? date('d-M-Y',strtotime($data->requested_at)) : '--' }}</td>
                                <td class="common_td_rules">{{ @$data->approved_by ? $data->approved_by : '--' }}</td>
                                <td class="common_td_rules">{{ @$data->approved_at ? date('d-M-Y',strtotime($data->approved_at)) : '--'  }}</td>
                                <td class="common_td_rules">{{ @$data->remarks ? $data->remarks : '--' }}</td>
                                @if($data->request_status==1)
                               @php
                                   $set_data='Send';
                               @endphp
                                @elseif($data->request_status==2)
                               @php
                                   $set_data='Approved';
                               @endphp
                                @elseif($data->request_status==3)
                                @php
                                $set_data='Rejected';
                            @endphp
                            @elseif($data->request_status==4)
                            @php
                            $set_data='Completed';
                            @endphp
                            @else
                            @php
                            $set_data='- - ';
                        @endphp
                                @endif
                                <td class="common_td_rules">{{ $set_data }}</td>
                                <td class="common_td_rules">
                                    @if ($data->requester_id==$user_id)
                                        @php
                                            $has_save=1;
                                        @endphp
                                    @else  
                                        @php
                                            $has_save=0;
                                        @endphp
                                    @endif
                                    @if(($data->requester_id==$user_id || $edit_access_type==1) && $data->request_status !=4)
                                    <button type="button" id="editRequestBtn" class="btn btn-primary" onclick="editRequest({{ $data->id }},'{{ base64_encode($data->remarks) }}',{{ $data->request_status }},{{ $has_save }})"><i id="editRequestSpin" class="fa fa-edit"></i></button>
                                    @else
                                      --
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
    </div>
