@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/jquery-ui.css') }}" rel="stylesheet">

    <style>
        .ajaxSearchBox {
            display: none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 160px;
            margin: -2px 0px 0px 0px;
            overflow-y: scroll;
            width: 100% !important;
            z-index: 9999;
            position: absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);
            top: 30px !important;
        }

        .medicine-list-div {
            position: absolute;
            z-index: 99;
            background: #FFF;
            box-shadow: 0 0 6px #ccc;
            padding: 10px;
            width: 30%;
        }

        .close_btn_med_search {
            position: absolute;
            z-index: 99;
            color: #FFF !important;
            background: #000;
            right: -11px;
            top: -1px;
            border-radius: 100%;
            text-align: center;
            width: 20px;
            height: 20px;
            line-height: 20px;
            cursor: pointer;
        }

        .errorClass {
            background: #ff000030 !important;
        }

        .floatThead-container {
            width: 100% !important;
        }

        .row_sub .table {
            margin-bottom: 0px;
        }

        .number_class {
            text-align: right;

        }

        .table.no-margin.table-striped.theadfix_wrapper#main_row_tbl td {
            padding: 1px 1px !important;
        }

        table.table.no-margin.table_sm.no-border.grn_total_table td {
            padding: 0px !important;
        }
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row codfox_container">
            <div class="col-md-12 row_sub">
                <div class="col-md-6 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix" style="/*! padding: 16px 2px; */">
                            <div class="box no-border box-widget" style="margin-bottom: 5px;">
                                <div class="box-body clearfix">
                                    <input type="hidden" id="grn_listrow_id" value="0">
                                    <input type="hidden" id="grn_delete_check" value="">
                                    <div class="col-md-2">
                                        <b>Bill Entry</b>
                                    </div>
                                    <div class="col-md-2">

                                        <label class="switch" style="margin-left: -34px;">
                                            <input type="checkbox" name="poCheckBox" id='poCheckBox' value="1">
                                            <span class="slider round"></span>
                                        </label>

                                        <label for="">With PO</label>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <b>Bill Duplication</b>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="switch" style="margin-left: -50px;">
                                            <input onchange="checkGrnBillNo()" type="checkbox" name="poCheckBox"
                                                id='bill_duplication_check'>
                                            <span class="slider round"></span>
                                        </label>
                                        <label for="">Enable</label>

                                    </div>
                                    <div class="col-md-1 padding_sm text-right">
                                    </div>
                                    <div class="col-md-1 padding_sm text-right">
                                        <label id=" " style="font-weight: 700;color: #ebad29;">Status:</label>
                                    </div>
                                    <div class="col-md-1 padding_sm ">
                                        <label id="grn_status_head" style="font-weight: 700;color: #238823;">NEW</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding headerInputCheck">
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label>GRN No</label>
                                        <input class="form-control" type="text" id="grn_no" readonly=""
                                            tabindex="-1" name="GRNill No">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label>PO</label>
                                        <input class="form-control" type="text" id="po_no" readonly=""
                                            tabindex="-1" name="po_no">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label>P.O Date</label>
                                        <input class="form-control" type="text" readonly="" tabindex="-1"
                                            name="po_date" id="po_date">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label>Supplier <span style="color: red">*</span></label>
                                        <input style=position: relative;" type="text" required="" autocomplete="off"
                                            onkeyup='searchvendorCode(this.id,event)' id="inspected_by"
                                            class="form-control popinput auto_focus_cls" name="inspected_by[]"
                                            placeholder="Search Supplier">
                                        <div class='ajaxSearchBox' id="ajaxSearchBox_supplier" index='1'
                                            style="top: 48px !important;width: 470px !important;max-height: 290px;"> </div>
                                        <input type='hidden' name='vendor_id' value="" id="vendor_id_js">
                                        <input type='hidden' name='vendor_code' value="" id="vendor_code_id">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-12 no-padding headerInputCheck">
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label>Bill No <span style="color: red">*</span></label>
                                        <input class="form-control" onblur="checkGrnBillNo();" type="text"
                                            index='2' name="bill_no" id="bill_no">

                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label>Bill Date <span style="color: red">*</span></label>
                                        <input class="form-control" type="text" index='3' name="bill_date"
                                            id="bill_date">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label>Gate Pass</label>
                                        <input class="form-control" type="text" name="gatepass" id="gatepass">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label>Manufacturer </label>
                                        <div class="clearfix"></div>
                                        <input type="text" style="position: relative;" class="form-control"
                                            maxlength="100" onkeyup="searchManufacturerCode(this.id,event);"
                                            id="manufacturer_name" name="manufacturer_name" placeholder=""
                                            autocomplete="off">
                                        <div class='ajaxSearchBox' id="ajaxSearchBox_manufacturer"
                                            style="top: 48px !important; width: 470px !important;max-height: 290px;">
                                        </div>
                                        <input type='hidden' name='manufacturer_id' value=""
                                            id="manufacturer_id_js">
                                        <input type='hidden' name='manufacturer_code' value=""
                                            id="manufacturer_code_id">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm ">
                                    <div class="mate-input-box">
                                        <label>Supply Location</label>
                                        <div class="clearfix"></div>
                                        <?php

                                        ?>
                                        {!! Form::select('location', $location, $default_location, [
                                            'onchange' => 'getPurchaseCategory()',
                                            'class' => 'form-control',
                                            'id' => 'location',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm ">
                                    <div class="mate-input-box">
                                        <label>Purchase Category <span style="color: red">*</span></label>
                                        <div class="clearfix"></div>
                                        <div id="purchase_category_changediv">
                                            <?php
                                            $pur_category = \DB::table('purchase_category')
                                                ->where('active_status', 1)
                                                ->orderBy('name')
                                                ->pluck('name', 'id');
                                            ?>
                                            {!! Form::select('pur_category', $pur_category, null, [
                                                'class' => 'form-control',
                                                'index' => '4',
                                                'placeholder' => '--Select--',
                                                'title' => 'Purchase Category',
                                                'id' => 'pur_category',
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-12 padding_sm">

                                    <div class="col-md-4"><a href="{{ route('extensionsvalley.item.listItem') }}"
                                            target="_blank" id="item_master_id"
                                            class="btn btn-block light_purple_bg"><span></span> Item Master</a></div>
                                    <?php
                                           $user_id = (!empty(\Auth::user()->id)) ? \Auth::user()->id : 0;
                                           $group_id = \DB::table('user_group')->where('user_id', '=', $user_id)->pluck('group_id');
                                            $group_name = \DB::table('groups')->wherein('id', $group_id)->pluck('name')->toArray();
                                           if (in_array("Vijayan p", $group_name)) { ?>
                                    <div class="col-md-4"><a href="{{ route('extensionsvalley.accounts.listSupplier') }}"
                                            target="_blank" id="item_vendor_id"
                                            class="btn btn-block light_purple_bg"><span></span>
                                            Supplier</a></div>
                                    <?php }else{ ?>
                                    <div class="col-md-4"><a href="{{ route('extensionsvalley.item.listVendor') }}"
                                            target="_blank" id="item_vendor_id"
                                            class="btn btn-block light_purple_bg"><span></span> Supplier</a></div>
                                    <?php } ?>

                                    <div class="col-md-4"><button class="btn btn-block light_purple_bg"
                                            id="grn_purchase_history">Purchase History <span><b>F2</b></span></button>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                @php
                                    $barcode_search_enabled = \DB::table('config_detail')
                                        ->where('name', 'enable_bar_code_search_in_grn')
                                        ->where('status', 1)
                                        ->value('value');
                                @endphp

                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-4 padding_sm">
                                        @if (intval($grn_list_all_check) == '1')
                                            <div class="checkbox checkbox-warning inline no-margin">
                                                <input type="checkbox" id="getallgrnitems" value="1">
                                                <label style="padding-left: 2px;" for="getallgrnitems">
                                                    Get All Items</label><br>
                                            </div>
                                        @endif
                                    </div>
                                    @if ((int) $barcode_search_enabled > 0)
                                        <div class="col-md-8 padding_sm">
                                            <table class="table no-margin table_sm table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td>Search Item by :</td>
                                                        <td>
                                                            <div class="radio radio-primary radio-inline">
                                                                <input type="radio" id="search_type_item_desc"
                                                                    value="item_desc" name="search_type_item">
                                                                <label for="search_type_item_desc"> Item Description
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="radio radio-primary radio-inline">
                                                                <input type="radio" id="search_type_barcode"
                                                                    value="barcode" name="search_type_item"
                                                                    checked="">
                                                                <label for="search_type_barcode"> Barcode </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-body clearfix">
                            {{-- <div class="col-md-2"><b>Totals</b></div>
                            <div class="clearfix"></div>
                            <div class="ht5"></div> --}}
                            <div class="col-md-6 padding_xs">
                                <?php
                                $charges = \DB::table('purchase_charges_head')
                                    ->where('status', 1)
                                    ->where('is_grn', 1)
                                    ->orderBy('display_order', 'asc')
                                    ->get();
                                ?>
                                <table class="table table-bordered table-striped table_sm">
                                    <thead>
                                        <tr class="table_header_light_purple_bg ">
                                            <th>Totals </th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($charges as $charge)
                                            <tr>
                                                <td>{{ $charge->name }}</td>
                                                <td class="charges_class" style="text-align: right"
                                                    id="{{ $charge->status_code }}_amot"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-6 padding_xs">
                                <div class="box no-border no-margin">
                                    <div class="box-body clearfix">
                                        <div class="col-md-12 padding_sm">
                                            <div class="custom-float-label-control mate-input-box">
                                                <label class="custom_floatlabel">Disc.Mode</label>
                                                <select class="form-control" id="disc_mode" name="disc_mode"
                                                    style="color:#555555; padding:4px 12px;">
                                                    {{-- <option value="1">Item Wise</option> --}}
                                                    <option value="2">On Net Amt</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12 padding_sm">
                                            <div class="custom-float-label-control mate-input-box">
                                                <label class="custom_floatlabel">Disc.Type</label>
                                                {!! Form::select('bill_discount_type', ['1' => 'Percentage', '2' => 'Amount'], '', [
                                                    'class' => 'form-control',
                                                    'id' => 'bill_discount_type',
                                                    'style' => 'color:#555555; padding:4px 12px;',
                                                ]) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12 padding_sm">
                                            <div class="custom-float-label-control mate-input-box">
                                                <label class="custom_floatlabel ">Disc. Val</label>
                                                <input class="form-control number_class" value="0.00" type="text"
                                                    maxlength="8" id="bill_discount_value" name="bill_discount_value">
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-3  pull-left" >Dis Amount
                                                <input type="checkbox" style="display: block;margin: 0 auto;height: 30px;" name="from_discounted" id="from_discounted">
                                            </div> --}}
                                        <div class="col-md-12 padding_sm" style="padding-top:4px !important">
                                            <input class="btn btn-success btn-block" type="button"
                                                onclick="BillDiscountcalculate();" value="Apply Discount"
                                                data-original-title="Generate Bill Discount" id="bill_discount_generate">
                                        </div>
                                        <div class="col-md-6 padding_sm" style="    padding-top: 2px !important;">
                                            <div class="custom-float-label-control " style="padding-left: 4px;">
                                                <label class="pull-left" style="font-weight:700;font-size: 12px;">Disc.
                                                    Amnt
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding_sm" style="    padding-top: 2px !important;">
                                            <label class="pull-right" style="font-weight:700;font-size: 14px;">
                                                <span class="label  total_discount"
                                                    style="font-size: 14px;color: rgb(179, 33, 33);font-weight:700">0.00
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding_xs">
                                <div class="mate-input-box">
                                    <label for=""><b>Remarks</b></label>
                                    <div class="clearfix"></div>
                                    <textarea name="" class="form-control" id="remark_txt" name="remark_txt"
                                        style="resize: none; height: 22px !important;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 padding_sm">
                    <div class="box no-border no-margin box-widget">
                        <div class="box-body  clearfix" style="background: #FFF;">
                            <!--<button class="btn btn-primary" type="button" id="calculateTotal" value="Get Total">Get Total</button>-->
                            <div class="col-md-12 no-padding">
                                <table class="table no-margin table_sm no-border grn_total_table">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><label class="text-bold">Amount</label></td>
                                            <td><input type="text" class="form-control text-right" readonly
                                                    value="0.00" style="font-weight: 700;" readonly=""
                                                    id="gross_amount_hd" name="gross_amount_hh"></td>
                                        </tr>
                                        <tr>
                                            <td width="35%"><label class="text-bold">-Disc</label></td>
                                            <td><input type="text" class="form-control text-right" readonly
                                                    value="0.00" style="font-weight: 700;" autocomplete="off"
                                                    id="discount_hd" name="discount_hd"></td>
                                        </tr>
                                        <tr>
                                            <td width="35%"><label class="text-bold">+Tax</label></td>
                                            <td><input type="text" class="form-control text-right" readonly
                                                    value="0.00" style="font-weight: 700;" autocomplete="off"
                                                    id="tot_tax_amt_hd" name="tot_tax_amt_hd"></td>
                                        </tr>
                                        <tr>
                                            <td width="35%"><label class="text-bold">RoundOff</label></td>
                                            <td><input type="text" value="0.00" class="form-control text-right"
                                                    style="font-weight: 700;" onchange="calRoundOff()" autocomplete="off"
                                                    id="round_off" name="round_off"></td>
                                        </tr>
                                        <tr style="background: #F1E9FF;">
                                            <td width="35%"><label class="text-bold"
                                                    style=" margin: 3px 0 0 0; font-weight: 700; font-size: 15px !important;">Tot.
                                                    Amt</label></td>
                                            <td><input
                                                    style="height: 36px !important; font-weight: 700; font-size: 16px !important;"
                                                    type="text" readonly="" class="form-control text-right "
                                                    value="0.00" id="net_amount_hd" name="net_amount_hd"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>



                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>

                    <div class="col-md-12 no-padding">
                        <a type="reset" href="{{ route('extensionsvalley.purchase.grn') }}">
                            <button id="cancel_btn" class="btn light_purple_bg btn-block disable_on_save">Reload</button>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 no-padding">

                        <div class="col-md-6 padding_xs">
                            <button type="button" id="grn_save_btn" onclick="grn_savedata(1,0);" name="save"
                                value="save" class="btn btn-success btn-block saveForm disable_on_save">Save</button>
                        </div>
                        <div class="col-md-6 padding_xs">
                            <button type="button" id="grn_save_print_btn" onclick="grn_savedata(1,1);" name="save"
                                value="save" class="btn btn-success btn-block disable_on_save">Save
                                &amp; Print</button>
                        </div>

                        <div class="clearfix"></div>
                        @if (!empty($accessPerm['approve']) && $accessPerm['approve'] == 1)
                            <div class="col-md-6 padding_xs">
                                <button type="button" id="grn_approve_btn" onclick="checkGrnBillNo(2,0);"
                                    name="approve" value="approve"
                                    class="btn btn-success btn-block disable_on_save">Approve</button>
                            </div>
                            <div class="col-md-6 padding_xs">
                                <button type="button" id="grn_approve_print_btn" onclick="checkGrnBillNo(2,1);"
                                    name="approve" value="approve"
                                    class="btn btn-success btn-block disable_on_save">Approve &amp; Print</button>
                            </div>
                        @endif
                        @if (!empty($accessPerm['submit']) && $accessPerm['submit'] == 1)
                            <div class="col-md-12 padding_xs">
                                <button type="button" id="grn_reject_btn" onclick="grn_savedata(3,0);" name="reject"
                                    value="reject" class="btn btn-success btn-block disable_on_save"
                                    style="display: none;">Cancel</button>
                            </div>
                        @endif
                        <div class="clearfix"></div>
                        @if (!empty($accessPerm['approve']) && $accessPerm['approve'] == 1)
                            <div class="col-md-12 padding_sm">
                                <button value="close_bill" type="button" id="grn_close_btn"
                                    onclick="checkGrnBillNo(4,0);" class="btn btn-warning btn-block disable_on_save"
                                    style="display: none">Close Bill</button>
                            </div>
                    </div>
                    @endif
                    <div class="col-md-12 padding_sm">
                        <button value="print_bill" type="button" id="grn_print_btn" onclick="grnPrint();"
                            class="btn btn-primary btn-block" style="display: none">Print Bill</button>
                    </div>
                    <input type="hidden" id="dataentryType_main">
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12 padding_sm">
                <div class="theadscroll always-visible" style="position: relative; height: 360px;">
                    <input type="hidden" name="row_count" id="row_count_id" value="0">
                    {{-- search product list html --}}
                    <div class="medicine-list-div" style="display: none;">
                        <a style="float: left;" class="close_btn_med_search">X</a>
                        <div class=" presc_theadscroll" style="position: relative;">
                            <table id="MedicationTable"
                                class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                <thead>
                                    <tr class="light_purple_bg">
                                        <th>Medicine</th>
                                    </tr>
                                </thead>
                                <tbody id="ListMedicineSearchData">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="grn_list_data_div">
                        {{-- search product list html end --}}
                        <table
                            class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed"
                            id="main_row_tbl" style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="2%">#</th>
                                    <th width="23%">
                                        <input id="issue_search_box" onkeyup="searchProducts();" type="text"
                                            placeholder="Item Search.. " style="color: #000;display: none;width: 90%;">
                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                                class="fa fa-search"></i></span>
                                        <span id="item_search_btn_text">Item Name</span>
                                    </th>
                                    <th width="2%">&nbsp;</th>
                                    <th width="9%">Batch</th>
                                    <th width="7%">Expiry</th>
                                    <th width="5%">GRN Qty</th>
                                    <th width="3%">Free Qty</th>
                                    <th width="6%">UOM</th>
                                    <th width="4%">Tot Qty</th>
                                    <th width="6%">Unit Rate</th>
                                    <th width="6%">Unit MRP</th>
                                    <th width="6%">Sales Price</th>
                                    <th width="6%">Tax Amt</th>
                                    <th width="6%">Dis.Amt</th>
                                    <th width="7%">Net Rate</th>
                                    <th width="2%"><button type="button" tabindex="-1" id="add"
                                            onclick="getNewRowInserted();" class="btn btn-success add_row_btn"><i
                                                class="fa fa-plus"></i></button></th>
                                </tr>
                            </thead>
                            <tbody id="row_body_data">

                            </tbody>

                        </table>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ht5"></div>


                <div class="clearfix"></div>
                <div class="ht5"></div>
                <table class="table no-margin table_sm no-border table-condensed">
                    <thead>
                        <tr>
                            <th width="55%" colspan="11" style="visibility: hidden;">
                            </th>
                            <th width="1%" style="text-align: right;">
                            </th>
                            <th width="8%" style="border: 1px solid #CCC !important;">
                                Tax Total
                            </th>
                            <th width="8%" style="border: 1px solid #CCC !important;">
                                Discount Total
                            </th>
                            <th width="7%" style="border: 1px solid #CCC !important;">
                                Total Net Amount
                            </th>
                            <th width="3%" align="center" style="visibility: hidden;">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="itemParentClass" id="footer_tr">

                            <td width="55%" colspan="11" style="visibility: hidden;">
                                0.00
                            </td>
                            <td width="1%" style="text-align: right;">
                                <b>Totals</b>
                            </td>
                            <td width="8%" style="border: 1px solid #CCC !important;" id="tot_tax_footer">
                                0.00
                            </td>
                            <td width="8%" style="border: 1px solid #CCC !important;" id="tot_dicount_footer">
                                0.00
                            </td>
                            <td width="7%" style="border: 1px solid #CCC !important;" id="tot_net_amt_footer">
                                0.00
                            </td>
                            <td width="3%" align="center" style="visibility: hidden;">
                                <i style="font-size: 15px;color: #714FA6;" class="fa fa-edit"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>

        </div>
    </div>


    <div id="desc_amount_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                    <h4 class="modal-title">Product Extra Info</h4>

                </div>
                <div class="modal-body">
                    <div id='model_charges_div'>

                    </div>
                    <div class="modal-footer">
                        <label style="float: left;"><b style="color: #a71313;">Press Esc Key to Close the
                                Popop</b></label>
                        <button class="btn light_purple_bg" id='charges_pop_btn_id'
                            onclick="checkPOPDataValidation();"><i class="fa fa-save"></i> OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="po_list_model" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 70%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" onclick="closePoPopup();"
                        data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Purchase Order List</h4>

                </div>
                <div class="modal-body">
                    <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;width: 100%">

                        <div class="clearfix panel-body"
                            style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">
                            <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                                <div class="date custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">PO NO. </label>
                                    <div class="clearfix"></div>
                                    <div class="ht5"></div>
                                    <input type="text" autocomplete="off" class="form-control custom_floatinput"
                                        id="po_number_search" placeholder="PO Number">
                                    <div id="po_number_searchCodeAjaxDiv"
                                        style="margin: 0; z-index: 999; font-size: 12px; width: 100%"
                                        class="ajaxSearchBox"></div>
                                    <input type="hidden" id="po_number_search_hidden" />
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div style="padding-top: 3px;" class="custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Location</label>
                                    <div class="clearfix"></div>
                                    <div class="ht5"></div>
                                    {!! Form::select('po_department', $location, $default_location, [
                                        'class' => 'form-control custom_floatinput',
                                        'id' => 'po_department',
                                        'style' => 'color:#555555; padding:2px 12px;',
                                    ]) !!}
                                </div>
                            </div>


                            <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                                <div class="input-group date custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Created From Date</label>
                                    <div class="clearfix"></div>
                                    <div class="ht5"></div>
                                    <input type="text" autocomplete="off" id="po_created_date_from"
                                        placeholder="Created Date From"
                                        class="form-control date_format custom_floatinput po_date_picker">
                                </div>
                            </div>

                            <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                                <div class="input-group date custom-float-label-control mate-input-box">
                                    <label class="custom_floatlabel">Created To Date</label>
                                    <div class="clearfix"></div>
                                    <div class="ht5"></div>
                                    <input type="text" autocomplete="off" id="po_created_date_to"
                                        placeholder="Created Date To"
                                        class="form-control date_format custom_floatinput po_date_picker">
                                </div>
                            </div>


                            <div class="col-xs-1 padding_sm" style="padding-bottom: 10px;">

                                <button style="padding: 0px 2px;" type="button" id="searchpobtn" onclick="serach_po();"
                                    class="padding_sm btn btn-block btn-success"> <i id="posearchdataspin"
                                        class="fa fa-search"></i>
                                    Search </button>
                            </div>
                        </div>


                        <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                            <table
                                class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align "
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_light_purple_bg "
                                        style="    background: #539a7a;color: white;">
                                        <th>#</th>
                                        <th>PO No </th>
                                        <th>PO Date</th>
                                        <th>Name</th>
                                        <th>Vendor</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="row_po_list">


                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="po_list_detail" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 70%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Purchase Order Detail</h4>

                </div>
                <div class="modal-body">
                    <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;width: 100%">

                        <div class="clearfix panel-body"
                            style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">

                            <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                                <table
                                    class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed"
                                    style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_light_purple_bg ">
                                            <th><input type="checkbox" id='po_select_all' onclick="checkAllPo();"></th>
                                            <th>Item Code</th>
                                            <th>Item Desc</th>
                                            <th>Qty</th>
                                            <th>Free Qty</th>
                                            <th>Rate</th>
                                            <th>Net Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody id="row_po_dl_list">


                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" value="" name="pop_po_no" id='pop_po_no'>
                            <button type="button" class="btn btn-success" onclick="addToGRN();"> Add To GRN</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id='hdn_item_history' value="" name="hdn_item_history">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="decimal_configuration" value="<?= @$decimal_configuration ? $decimal_configuration : 2 ?>">
    <input type="hidden" id="purchaseCategoryIdhidden" value="0">
    <input type="hidden" id="grn_id_hidden" value="{{ $bill_id ? $bill_id : 0 }}">
    <input type="hidden" id="grn_status_hidden" value="{{ $grnStatus ? $grnStatus : 0 }}">
    <div id="purchase_history" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 70%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Purchase History</h4>

                </div>
                <div class="modal-body">
                    <div class="panel panel-primary no-margin" style="border:1px solid #eeeedd;width: 100%">

                        <div class="clearfix panel-body"
                            style="border-bottom:1px solid #F4F6F5; background:#FBFDFC;  padding:10px 15px;">

                            <div class="theadscroll always-visible" style="position: relative; height: 260px;">
                                <table
                                    class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align "
                                    style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_light_purple_bg ">
                                            <th style="text-align: center;">Grn No</th>
                                            <th style="text-align: center;">Grn Date</th>
                                            <th style="text-align: center;">Vendor Name </th>
                                            <th style="text-align: center;">Grn Qty </th>
                                            <th style="text-align: center;">Grn Unit</th>
                                            <th style="text-align: center;">Grn Rate</th>
                                            <th style="text-align: center;">Unit Cost</th>
                                            <th style="text-align: center;">Free Qty</th>
                                            <th style="text-align: center;">MRP</th>
                                        </tr>
                                    </thead>
                                    <tbody id="history_dl_list">


                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- grm Model --}}


    {{-- <img src="{{url('/packages/loader.gif')}}"> --}}
    <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}" />

    <div id="dialog" title="Basic dialog">
        </p>
    </div>

    {{-- include('core::dashboard.partials.bootstrap_datetimepicker_js') --}}
    {!! Html::script('packages/extensionsvalley/emr/toastr/toastr.min.js') !!}
    {!! Html::script('packages/extensionsvalley/purchase/default/javascript/jquery-ui.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {

            $('.month_picker').datetimepicker({
                format: 'MM'
            });
            $('.year_picker').datetimepicker({
                format: 'YYYY'
            });
            $("#item_search_btn").click(function() {
                $("#issue_search_box").toggle();
                $("#item_search_btn_text").toggle();
            });
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.datepicker,.po_date_picker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $('.date_time_picker').datetimepicker();


            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });





            $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }
                });
            })



        });
    </script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    {!! Html::script('packages/Purchase/default/javascript/grn_script.js?v=1') !!}
    {!! Html::script('packages/extensionsvalley/master/canvas/js/jsonh.js') !!}
@stop

@section('javascript_extra')
    <script type="text/javascript">
        var item_arr = {};
        var charge_item = {};
        var item_array = {};
        var delete_array = new Array();;

        charge_item['TAX'] = {};
        charge_item['DISCOUNT'] = {};
        charge_item['OTHCH'] = {};
        charge_item['FREE'] = {};
        var is_igst = 0;
        var is_sgst = 0;
        var is_gst = 0;
        @if (!empty($bill_id))
            var bill_id = <?php echo $bill_id; ?>;
            if (bill_id) {
                setTimeout(function() {
                    loadGRN(bill_id);
                }, 1000);
            }
        @else
            getNewRowInserted();
        @endif
        $(document).ready(function() {
            $("#bill_date").datetimepicker({
                format: 'DD-MM-YYYY'
            });
            document.getElementById("inspected_by").focus();

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            document.onkeyup = KeyCheck;
            setTimeout(function() {
                //$('#menu_toggle').trigger('click');
                // $(body).addClass('sidebar-collapse');
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');
                getPurchaseCategory();
            }, 500);

        });

        function KeyCheck(e) {
            var KeyID = (window.event) ? event.keyCode : e.keyCode;
            if (KeyID == 113) {
                $("#grn_purchase_history").trigger("click");
            }
            if (KeyID == 115) {}
            if (KeyID == 27) {
                checkbatchStatus(1);
                setTimeout(function() {
                    $('#desc_amount_modal').modal('hide');
                    BillDiscountcalculate();
                }, 1500);
            }
        }

        function getNewRowInserted() {
            var url = "{{ route('extensionsvalley.purchase.grnaddrow') }}";
            var row_count = $('#row_count_id').val();
            $('#row_count_id').val(parseInt(row_count) + 1);
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    row_count: row_count,
                    add_row: 1,
                    _token: $('#_token').val()
                },
                // data: 'add_row=' +1,
                beforeSend: function() {},
                success: function(html) {
                    // console.log(html);
                    $('#row_body_data').append(html);
                    row_ct = 1;
                    $(".row_class").each(function(i) {
                        $(this).find('.row_count_class').text(row_ct); // row Count Re-arenge
                        row_ct++;
                    });
                    // $(".expiry_date").datetimepicker({format: 'DD-MM-YYYY',minDate: new Date() });

                },
                // error: function () {
                //     alert('Please check your internet connection and try again');
                // },
                complete: function() {
                    $("#row_data_" + parseInt(row_count) + 1 + ' .expiry_date ').val("");
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

        function closePoPopup() {
            $('#poCheckBox').prop('checked', false); // Unchecks it
        }
        /* remove Roe data */

        function chargesModel(row_id) {

            var bill_detail_id = $('#bill_detail_id_hidden' + row_id).val();
            if (!bill_detail_id && bill_detail_id != 'undefined') {
                bill_detail_id = 0;
            }
            var item_code = $('#item_code_hidden' + row_id).val();
            var item_desc = $('#item_desc_' + row_id).val();
            var item_id = $('#item_id_hidden' + row_id).val();
            var location = $('#location').val();
            var batch_row = $('#batch_row' + row_id).val();
            var selling_price_row = $('#sales_price_' + row_id).val();
            var decimalConfiguration = $('#decimal_configuration').val();
            var _token = $('#_token').val();
            var grn_id = $('#grn_id_hidden').val();
            var grn_status = $('#grn_status_hidden').val();
            var location_code = $('#location').val();

            var url = "{{ route('extensionsvalley.purchase.charges_popup') }}";
            if (item_code != '') {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: _token,
                        row_id: row_id,
                        batch_row: batch_row,
                        decimalConfiguration: decimalConfiguration,
                        item_id: item_id,
                        item_desc: item_desc,
                        item_code: item_code,
                        selling_price_row: selling_price_row,
                        location: location,
                        charge_item: charge_item,
                        location_code: location_code,
                        grn_id: grn_id,
                        grn_status: grn_status,
                        bill_detail_id: bill_detail_id,
                        item_arr: item_arr
                    },
                    beforeSend: function() {
                        $('#getGrnTaxRateBtn' + row_id).prop('disabled', true);
                        $('#getGrnTaxRateSpin' + row_id).removeClass('fa fa-plus');
                        $('#getGrnTaxRateSpin' + row_id).addClass('fa fa-spinner fa-spin');
                    },
                    success: function(html) {
                        $("#model_charges_div").html(html);
                        $('#desc_amount_modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        })
                        calculateAll(row_id, '', 2);
                        $("#Expiry_date_pop").datetimepicker({
                            format: 'DD-MM-YYYY',
                            useCurrent: false
                        });
                        $('#uom_val').val($('#uom_select_id_' + row_id + ' option:selected').data("uom_value"));
                    },

                    complete: function() {
                        $('#getGrnTaxRateBtn' + row_id).prop('disabled', false);
                        $('#getGrnTaxRateSpin' + row_id).removeClass('fa fa-spinner fa-spin');
                        $('#getGrnTaxRateSpin' + row_id).addClass('fa fa-plus');
                        $('#charge_percentage_1').focus();
                        $('#grn_listrow_id').val(row_id);
                        setTimeout(function() {
                            $('#charge_percentage_1').blur();
                        }, 400);
                        setTimeout(function() {
                            $('#model_charges_div').find('#batch_pop').focus();
                            removeWhiteSpace();
                            var batch_status = $('#batch_needed').val();
                            batch_validate[item_id] = batch_status;
                        }, 500);
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            } else {
                Command: toastr["warning"]("Please Select an Item !!");
            }
        }
        /**
         *
         * @param {type} ajax_div
         * @returns {undefined}
         * FOR KEY DOWN KY UP IN PROGRESSIVE  SEARCH
         */


        function grn_savedata(post_type, is_print) {
            var url = "{{ route('extensionsvalley.purchase.grn') }}";
            var item_head = new Array();
            var item_array = new Array();
            bill_date = $('#bill_date').val();
            var _token = $('#_token').val();
            bill_no = $('#bill_no').val();
            po_date = $('#po_date').val();
            gatepass = $('#gatepass').val();
            vendor_code = $('#vendor_code_id').val();
            location_code = $('#location').val();
            bill_discount_type = $('#bill_discount_type').val();
            net_tax_amount = $('#tot_tax_amt_hd').val();
            gross_amount = $('#gross_amount_hd').val();
            net_amount = $('#net_amount_hd').val();
            purchase_category = $('#pur_category').val();
            round_off = $('#round_off').val();
            po_id = $('#po_id_hidden').val();
            po_no = $('#po_no').val();
            bill_id = $('#bill_id_hidden').val();
            grn_no = $('#grn_no_hidden').val();
            disc_mode = $('#disc_mode').val();
            manufacturer_code = $('#manufacturer_code_id').val();
            bill_discount_value = $('#bill_discount_value').val();
            total_discount = $('.total_discount').text();
            bill_discount_amount = $('#bill_discount_amount').val();
            remark_txt = $('#remark_txt').val();
            total_net_cost_without_tax = net_amount - net_tax_amount;
            item_head.push({
                'bill_date': bill_date,
                'bill_no': bill_no,
                'po_date': po_date,
                'gatepass': gatepass,
                'vendor_code': vendor_code,
                'location_code': location_code,
                'bill_discount_type': bill_discount_type,
                'net_tax_amount': net_tax_amount,
                'gross_amount': gross_amount,
                'net_amount': net_amount,
                'manufacturer_code': manufacturer_code,
                'round_off': round_off,
                'po_id': po_id,
                'bill_id': bill_id,
                'grn_no': grn_no,
                'po_no': po_no,
                'disc_mode': disc_mode,
                'bill_discount_type': bill_discount_type,
                'bill_discount_value': bill_discount_value,
                'bill_discount_amount': bill_discount_amount,
                'total_discount': total_discount,
                'remark_txt': remark_txt,
                'total_net_cost_without_tax': total_net_cost_without_tax ? total_net_cost_without_tax : 0,
                'purchase_category': purchase_category ? purchase_category : 0

            });
            // item_array = {};
            var flag = 1;
            var batch_flag = mrp_flag = exp_flag = 0;
            $('#row_body_data tr').each(function() {
                var item_code = $(this).find("input[name='item_code_hidden[]']").val();
                if (item_code != '') {

                    var row_id = $(this).find("input[name='row_id_hidden[]']").val();
                    var sales_rate_mrp_check = item_arr[row_id]['unit_check'];
                    var is_free = item_arr[row_id]['is_free'];
                    var item_desc = $(this).find("input[name='item_desc[]']").val();
                    var item_id = $(this).find("input[name='item_id_hidden[]']").val();
                    var uom_name = $(this).find("input[name='uom_name_hidden[]']").val();
                    var uom_id = $('#uom_select_id_' + row_id + ' option:selected').val();
                    var uom_val = $('#uom_select_id_' + row_id + ' option:selected').data("uom_value");
                    var unit_mrp = $(this).find("input[name='unit_mrp_hidden[]']").val();
                    var free_qty = $(this).find("input[name='free_qty_hidden[]']").val();
                    var batch = $(this).find("input[name='batch[]']").val();
                    var expiry_date = $(this).find("input[name='expiry_date[]']").val();

                    var free_qty = parseFloat($(this).find("input[name='free_qty[]']").val());
                    var tot_qty = parseFloat($(this).find("input[name='tot_qty[]']").val());
                    var tot_rate = parseFloat($(this).find("input[name='tot_rate[]']").val());
                    var tot_mrp = parseFloat($(this).find("input[name='tot_mrp[]']").val());
                    var tot_tax_amt = parseFloat($(this).find("input[name='tot_tax_amt[]']").val());
                    var tot_dic_amt = parseFloat($(this).find("input[name='tot_dic_amt[]']").val());
                    var net_cost = parseFloat($(this).find("input[name='net_cost[]']").val());
                    var unit_mrp = parseFloat($(this).find("input[name='unit_mrp_hidden[]']").val());
                    var unit_cost = parseFloat($(this).find("input[name='unit_cost_hidden[]']").val());
                    var unit_rate = parseFloat($(this).find("input[name='unit_rate_hidden[]']").val());
                    var rate = parseFloat($(this).find("input[name='rate_hidden[]']").val());
                    var mrp = parseFloat($(this).find("input[name='mrp_hidden[]']").val());
                    var grn_qty = parseFloat($(this).find("input[name='qty_hidden[]']").val());
                    var free_qty = parseFloat($(this).find("input[name='free_qty_hidden[]']").val());
                    var po_detail_id = $(this).find("input[name='po_detail_id_hidden[]']").val();
                    var bill_detail_id = $(this).find("input[name='bill_detail_id_hidden[]']").val();
                    var free_tax_applied = parseFloat($(this).find("input[name='free_tax_applied_hidden[]']")
                        .val());
                    if (isNaN(grn_qty)) {
                        grn_qty = 0;
                    }
                    if (isNaN(free_qty)) {
                        free_qty = 0;
                    }
                    if (isNaN(tot_qty)) {
                        tot_qty = 0;
                    }
                    if (isNaN(tot_mrp)) {
                        tot_mrp = 0;
                    }
                    if (isNaN(tot_rate)) {
                        tot_rate = 0;
                    }
                    if (isNaN(tot_dic_amt)) {
                        tot_dic_amt = 0;
                    }
                    if (isNaN(tot_tax_amt)) {
                        tot_tax_amt = 0;
                    }
                    if (isNaN(net_cost)) {
                        net_cost = 0;
                    }
                    if (isNaN(unit_mrp)) {
                        unit_mrp = 0;
                    }
                    if (isNaN(unit_cost)) {
                        unit_cost = 0;
                    }
                    if (isNaN(unit_rate)) {
                        unit_rate = 0;
                    }
                    if (isNaN(rate)) {
                        rate = 0;
                    }
                    if (isNaN(mrp)) {
                        mrp = 0;
                    }
                    if (isNaN(free_qty)) {
                        free_qty = 0;
                    }
                    if (isNaN(grn_qty)) {
                        grn_qty = 0;
                    }

                    item_array.push({
                        'item_code': item_code,
                        'item_id': item_id,
                        'row_id': row_id,
                        'item_desc': item_desc,
                        'uom_name': uom_name,
                        'uom_id': uom_id,
                        'uom_val': uom_val,
                        'unit_cost': unit_cost,
                        'grn_qty': grn_qty,
                        'free_qty': free_qty,
                        'unit_mrp': unit_mrp,
                        'batch': batch,
                        'expiry_date': expiry_date,
                        'tot_qty': tot_qty,
                        'tot_rate': tot_rate,
                        'tot_mrp': tot_mrp ? tot_mrp : 0,
                        'tot_tax_amt': tot_tax_amt ? tot_tax_amt : 0,
                        'net_cost': net_cost,
                        'tot_dic_amt': tot_dic_amt,
                        'unit_rate': unit_rate,
                        'rate': rate,
                        'mrp': mrp,
                        'po_detail_id': po_detail_id,
                        'bill_detail_id': bill_detail_id,
                        'sales_rate_mrp_check': sales_rate_mrp_check,
                        'is_free': is_free

                    });
                    var batch_status = batch_validate[item_id] ? batch_validate[item_id] : 0;
                    if (tot_mrp == 0 && parseInt(batch_status) == 1) {
                        $(this).find("input[name='tot_mrp[]']").addClass('errorClass');
                        flag = 0;
                        mrp_flag = 1;
                    } else {
                        $(this).find("input[name='tot_mrp[]']").removeClass('errorClass');
                    }
                    if (batch == '' && parseInt(batch_status) == 1) {
                        $(this).find("input[name='batch[]']").addClass('errorClass');
                        flag = 0;
                        batch_flag = 1;
                    } else {
                        $(this).find("input[name='batch[]']").removeClass('errorClass');

                    }
                    if (expiry_date == '' && parseInt(batch_status) == 1) {

                        $(this).find("input[name='expiry_date[]']").addClass('errorClass');
                        flag = 0;
                        exp_flag = 1;
                    } else {
                        $(this).find("input[name='expiry_date[]']").removeClass('errorClass');
                    }
                }
            });
            if (mrp_flag) {
                Command: toastr["warning"]("Please Enter MRP !!");
            }
            if (batch_flag) {
                Command: toastr["warning"]("Please Enter Batch !!");
            }
            if (exp_flag) {
                Command: toastr["warning"]("Please Select a Expiry Date !!");
            }

            if (vendor_code == '') {
                Command: toastr["warning"]("Please Select a Supplier !!");
                flag = 0;
            }
            if (post_type == 2) {

                if (bill_no == '') {
                    Command: toastr["warning"]("Please Enter Bill No !!");
                    flag = 0;
                    $('#bill_no').css('background', '#f9b4b442');
                }
                else {
                    $('#bill_no').css('background', 'fff');
                }
                if (bill_date == '') {
                    Command: toastr["warning"]("Please Enter Bill Date !!");
                    flag = 0;
                    $('#bill_date').css('background', '#f9b4b442');
                }
                else {
                    $('#bill_date').css('background', 'fff');
                }
                if (purchase_category == '') {
                    Command: toastr["warning"]("Please Select Purchase Catagory !!");
                    flag = 0;
                }
            }
            var item_head_arr = JSONH.pack(item_head);
            item_head_arr = JSON.stringify(item_head_arr);
            if (item_array.length != 0) {
                var item_detail = JSONH.pack(item_array);
                item_detail = JSON.stringify(item_detail);
                if (flag) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            item_head: item_head_arr,
                            item_array: item_detail,
                            item_arr: item_arr,
                            charge_item: charge_item,
                            post_type: post_type,
                            is_print: is_print,
                            delete_array: delete_array,
                            grn_save: 1,
                            _token: _token
                        },
                        beforeSend: function() {
                            $('.disable_on_save').prop('disabled', true);
                        },
                        success: function(json_array) {
                            var data = JSON.parse(json_array);
                            if (parseInt(data.status) == 1) {
                                var post_type = data.post_type;
                                var html = data.print_html;
                                if (html && is_print == 1) {
                                    var popupWin = window.open('', 'my div');
                                    popupWin.document.write(html);
                                    popupWin.document.close();
                                    popupWin.focus(); // necessary for IE >= 10
                                    popupWin.print();
                                    popupWin.close();
                                    window.location.href = "{{ route('extensionsvalley.purchase.grnList') }}";
                                    return true;
                                }
                                showBillNo(post_type, data.grn_no);
                            } else if (parseInt(data.status) == 2) {
                                toastr.warning(data.message);
                            } else if (parseInt(data.status) == 3) {
                                toastr.error(data.message);
                            }

                        },
                        complete: function() {
                            $('.disable_on_save').prop('disabled', false);
                        },
                        error: function() {
                            toastr.error("Error Please Check Your Internet Connection");
                        }
                    });
                }
            } else {
                toastr.warning('Please Add Item !');
            }
        }




        function loadGRN(bill_id) {
            var url = "{{ route('extensionsvalley.purchase.loadGRN') }}";
            var _token = $('#_token').val();
            var vendor_code = $('#vendor_list_grn_hidden').val();
            var po_id = $('#pop_po_no').val();
            $('#dataentryType_main').val("EditSection");

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: _token,
                    bill_id: bill_id,
                    loadGRN: 1
                },
                beforeSend: function() {
                    $("#grn_list_data_div").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(json_array) {
                    $('#po_list_detail').modal('hide');
                    var data = JSON.parse(json_array);
                    item_arr = data.item_c_array;
                    batch_validate = data.batch_validate;
                    rowCount = data.rowCount;
                    charges_dts = data.po_charges_dts;
                    $('#row_body_data').html(data.html);
                    $(".expiry_date").datetimepicker({
                        format: 'DD-MM-YYYY'
                    });
                    pre_bill_detail_id = '';
                    var row_id = 0
                    $.each(charges_dts, function(charge_status, charge_val) {
                        if (charge_val.bill_detail_id != pre_bill_detail_id) {
                            row_id++;
                            pre_bill_detail_id = charge_val.bill_detail_id;
                        }
                        if (charge_val.code == 'DISCOUNT') {
                            if (typeof charge_item['DISCOUNT'] == 'undefined')
                                charge_item['DISCOUNT'] = {}
                            if (typeof charge_item['DISCOUNT'][row_id] == 'undefined')
                                charge_item['DISCOUNT'][row_id] = {}
                            if (typeof charge_item['DISCOUNT'][row_id][charge_val.status_code] ==
                                'undefined')
                                charge_item['DISCOUNT'][row_id][charge_val.status_code] = {}
                        }
                        if (charge_val.code == 'TAX') {
                            if (typeof charge_item['TAX'] == 'undefined')
                                charge_item['TAX'] = {}
                            if (typeof charge_item['TAX'][row_id] == 'undefined')
                                charge_item['TAX'][row_id] = {}
                            if (typeof charge_item['TAX'][row_id][charge_val.status_code] ==
                                'undefined')
                                charge_item['TAX'][row_id][charge_val.status_code] = {}
                        }
                        if (charge_val.code == 'OTHCH') {
                            if (typeof charge_item['OTHCH'] == 'undefined')
                                charge_item['OTHCH'] = {}
                            if (typeof charge_item['OTHCH'][row_id] == 'undefined')
                                charge_item['OTHCH'][row_id] = {}
                            if (typeof charge_item['OTHCH'][row_id][charge_val.status_code] ==
                                'undefined')
                                charge_item['OTHCH'][row_id][charge_val.status_code] = {}
                        }
                        if (charge_val.code == 'FREE') {
                            if (typeof charge_item['FREE'] == 'undefined')
                                charge_item['FREE'] = {}
                            if (typeof charge_item['FREE'][row_id] == 'undefined')
                                charge_item['FREE'][row_id] = {}
                            if (typeof charge_item['FREE'][row_id][charge_val.status_code] ==
                                'undefined')
                                charge_item['FREE'][row_id][charge_val.status_code] = {}
                        }
                        if (charge_val.code == 'TAX') {
                            charge_item[charge_val.code][row_id][charge_val.status_code]['table_id'] =
                                charge_val.head_id;
                            charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                                charge_val.value;
                            charge_item[charge_val.code][row_id][charge_val.status_code]['tax'] =
                                charge_val.perc;
                        }
                        if (charge_val.code == 'DISCOUNT') {
                            if (charge_val.status_code == 'DAH') {

                                charge_item[charge_val.code][row_id][charge_val.status_code][
                                    'table_id'
                                ] = charge_val.head_id;
                                charge_item[charge_val.code][row_id][charge_val.status_code][
                                    'bill_dis_amt'
                                ] = charge_val.value;
                                charge_item[charge_val.code][row_id][charge_val.status_code][
                                    'discount'
                                ] = charge_val.perc;
                            } else {

                                charge_item[charge_val.code][row_id][charge_val.status_code][
                                    'table_id'
                                ] = charge_val.head_id;
                                charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                                    charge_val.value;
                                charge_item[charge_val.code][row_id][charge_val.status_code][
                                    'discount'
                                ] = charge_val.perc;
                            }
                        }
                        if (charge_val.code == 'FREE') {
                            charge_item[charge_val.code][row_id][charge_val.status_code]['table_id'] =
                                charge_val.head_id;
                            charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                                charge_val.value;
                            charge_item[charge_val.code][row_id][charge_val.status_code]['free'] =
                                charge_val.perc;
                        }
                        if (charge_val.code == 'OTHCH') {
                            charge_item[charge_val.code][row_id][charge_val.status_code]['table_id'] =
                                charge_val.head_id;
                            charge_item[charge_val.code][row_id][charge_val.status_code]['amount'] =
                                charge_val.value;
                            charge_item[charge_val.code][row_id][charge_val.status_code]['other'] =
                                charge_val.perc;
                        }

                    });

                    $('#grn_print_btn').show();
                    if (data.head.approve_status == 1) {
                        $('#grn_approve_print_btn').prop('disabled', false);
                        $('#grn_approve_btn').prop('disabled', false);
                        $('#grn_reject_btn').show();
                        $('#grn_close_btn').hide();
                        $('#grn_status_head').text('GRN Created');
                    }
                    if (data.head.approve_status == 3) {
                        $('#grn_approve_print_btn').hide();
                        $('#grn_approve_btn').hide();

                        $('#grn_close_btn').hide();
                        $('#grn_status_head').text('GRN Rejected');
                    }
                    if (data.head.approve_status == 2) {
                        $('#grn_approve_print_btn').hide();
                        $('#grn_approve_btn').hide();

                        $('#grn_close_btn').show();
                        $('#grn_status_head').text('GRN Approved');

                        if (data.grn_reedit == 1) {
                            $('#grn_approve_print_btn').show();
                            $('#grn_approve_btn').show();
                        }
                    }
                    if (data.head.approve_status == 4) {
                        $('#grn_approve_print_btn').hide();
                        $('#grn_approve_btn').hide();
                        $('#grn_close_btn').hide();
                        $('#grn_status_head').text('GRN Closed');
                        $('.disable_on_save').attr('disabled', true);
                    }

                    $('#grn_no').val(data.head.grn_no);
                    $('#po_no').val(data.head.po_no);
                    if (data.head.po_no) {
                        $('#poCheckBox').prop('checked', true);
                    }
                    $('#po_date').val(data.head.po_date);
                    $('#round_off').val(data.head.round_amount);
                    $('#inspected_by').val(data.vendor.vendor_name);
                    $('#inspected_by').attr('title', data.vendor.gst_vendor_code);
                    // $('#inspected_by').prop('disabled',true);
                    $('#vendor_code_id').val(data.vendor.vendor_code);
                    $('#vendor_id_js').val(data.vendor.id);
                    if (typeof data.manufacturer != 'undefined' && data.manufacturer != null) {
                        $('#manufacturer_name').val(data.manufacturer.manufacturer_name);
                        $('#manufacturer_id_js').val(data.manufacturer.manufacturer_code);
                        $('#manufacturer_id_js').val(data.manufacturer.id);
                    }
                    $('#location').prop('disabled', true);
                    $('#location option').filter(function() {
                        return ($(this).val() == data.head.location); //To select Blue
                    }).prop('selected', true);
                    $('#pur_category option').filter(function() {
                        $('#purchaseCategoryIdhidden').val(data.head.purchase_category);
                        return ($(this).val() == data.head.purchase_category);
                    }).prop('selected', true);
                    $('#bill_discount_type option').filter(function() {
                        return ($(this).val() == data.head.bill_discount_type);
                    }).prop('selected', true);

                    $('#bill_discount_value').val(data.head.bill_discount_value);
                    $('.total_discount').text(data.head.bill_discount_amount);
                    $('#remark_txt').text(data.head.remark);
                    $('#bill_no').val(data.head.bill_no);
                    $('#bill_date').val(data.bill_date);
                    $('#gatepass').val(data.head.gatepass);
                    $('#row_count_id').val(data.rowCount);
                    BillDiscountcalculate();
                },
                complete: function() {
                    $("#grn_list_data_div").LoadingOverlay("hide");
                    getPurchaseCategory();
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

        function ViewPo(po_id) {
            var url = "{{ route('extensionsvalley.purchase.view_po') }}";
            var _token = $('#_token').val();
            $('#pop_po_no').val(po_id);
            var vendor_code = $('#vendor_list_grn_hidden').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: _token,
                    po_id: po_id
                },
                beforeSend: function() {},
                success: function(html) {
                    $('#po_list_model').modal('hide');

                    $('#po_list_detail').modal('show');

                    // priceCalculator()
                    $('#row_po_dl_list').html(html);

                },
                complete: function() {
                    setTimeout(function() {
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 500);

                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

        function searchProducts() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("issue_search_box");
            filter = input.value.toUpperCase();
            table = document.getElementById("main_row_tbl");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }


        function grnPrint() {
            var url = "{{ route('extensionsvalley.purchase.grnPrint') }}";
            var _token = $('#_token').val();
            var bill_id = $('#bill_id_hidden').val();
            var other_amt = $('#OTHCHG_amot').html().trim();
            var feight_amt = $('#FRCHG_amot').html().trim();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: _token,
                    bill_id: bill_id,
                    other_amt: other_amt,
                    feight_amt: feight_amt
                },
                beforeSend: function() {},
                success: function(result) {

                    if (result) {
                        var popupWin = window.open('', 'my div');
                        //                    popupWin.document.write('<style>@page{size:landscape;}</style>');
                        popupWin.document.write(result);
                        popupWin.document.close();
                        popupWin.focus(); // necessary for IE >= 10
                        popupWin.print();
                        popupWin.close();
                        return true;
                    }

                },
                complete: function() {
                    setTimeout(function() {
                        $(".theadfix_wrapper").floatThead('reflow');
                    }, 500);

                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }

        function checkAllPo() {


        }
        $('#po_select_all').click(function() {

            if ($("#po_select_all").is(":checked") == true) {
                $('.po_dl_id').prop('checked', true);
            } else {
                $('.po_dl_id').prop('checked', false);
            }
        });

        function item_history(row_id) {
            item_code = $('#item_code_hidden' + row_id).val();
            $('#hdn_item_history').val(item_code);
            $('.row_class').css('background', '#FFF');
            $('#row_data_' + row_id).css('background-color', '#40f54066');
        }

        function mrpCheck() {
            // rate = $('#rate').val();
            // mrp = $('#mrp').val();
            // if(mrp < rate){
            //     $('#rate').val(0);
            //     $('#charges_pop_btn_id').prop('disabled',true);
            //     Command: toastr["warning"]("Rate Should be Less than MRP !!");
            // }else{
            //     $('#charges_pop_btn_id').prop('disabled',false);

            // }
        }
        /* show item detail of previous purchase details popup */
        $(document).on('click', '#grn_purchase_history', function() {
            var item_code = $('#hdn_item_history').val();
            if (item_code != "") {
                try {
                    var url = "{{ route('extensionsvalley.purchase.lastGRNHistory') }}";
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {
                            item_code: item_code,
                            history: 1
                        },
                        beforeSend: function() {
                            $('#item_pre_grn_datail').html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            );
                        },
                        success: function(data) {
                            $('#purchase_history').modal("show");
                            if (data != 1) {
                                $('#history_dl_list').html(data);
                                setTimeout(function() {
                                    $(".theadfix_wrapper").floatThead('reflow');
                                }, 500);
                            }
                        },
                        complete: function() {
                            // $(this).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').hide();
                        },
                        error: function() {
                            toastr.error("Error Please Check Your Internet Connection");
                        }
                    });
                } catch (err) {
                    console.log(err.message);
                    bootbox.alert('Item previous purchase details fetching failed!!');
                }
            } else {
                Command: toastr["warning"]("Please Select A Item !!");
            }

        });
    </script>

@endsection
