@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

@stop

@section('css_extra')
<style>
    .close_btn_vendor_search {

        position: absolute;
        z-index: 99;
        color: #FFF !important;
        background: #000;
        right: -11px;
        top: -1px;
        border-radius: 100%;
        text-align: center;
        width: 20px;
        height: 20px;
        line-height: 20px;
        cursor: pointer;
    }

    .vendor-list-div {
        position: absolute;
        z-index: 99;
        background: #FFF;
        box-shadow: 0 0 6px #ccc;
        padding: 10px;
        /* width: 95%; */
        width: 300px;
    }

    #VendorTable>tbody>tr:hover {
        background: #87d7ca52;
    }

    #VendorTable>tbody>tr>td {
        padding: 8px;
        line-height: 2;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .ajaxSearchBox {
        display: block;
        z-index: 7000;
        margin-top: 15px !important;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="modal fade" id="grnlist_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Grn View</h4>
            </div>
            <div class="modal-body" style="min-height: 400px" id="grnlist_div">

            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" type="button" class="btn light_purple_bg" onclick="exceller()" ;>Excel
                    <i class="fa fa-file-excel-o"></i></button>
                <button style="padding: 3px 3px" type="button" class="btn btn-primary"
                    onclick="printReportData();">Print <i class="fa fa-print"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                        value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                        value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                        style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="right_col">

    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" id="grnviewtype" value="{{ $grnviewtype }}">
    <input type="hidden" id="default_location" value="{{ $default_location }}">
    <input type="hidden" id="insert_from" value="{{ @$list_type ? $list_type : 1 }}">
    <div class="row" style="font-size: 12px; font-weight: bold;">
        <div class="col-md-12 pull-right"> {{ $title }}</div>
    </div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <form action="{{ route('extensionsvalley.purchase.grnList') }}" id="requestSearchForm"
                            method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="from_date" id="from_date">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="to_date" id="to_date">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">GRN No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="grn_no_search">
                                    <div id="ajaxGrnNoSearchBox" class="ajaxSearchBox"></div>
                                    <input type="hidden" id="grn_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="bill_no_search">
                                    <div id="ajaxBillNoSearchBox" class="ajaxSearchBox"></div>
                                    <input type="hidden" id="bill_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Vendor Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="vendor_name_search">
                                    <div id="ajaxVendorSearchBox" class="ajaxSearchBox"></div>
                                    <input type="hidden" id="vendoritem_id_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Product Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="item_desc">
                                    <div id="item_desc_AjaxDiv" class="ajaxSearchBox"></div>
                                    <input type="hidden" name="item_desc_hidden" id="item_desc_hidden">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill Location</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('location', ['0' => ' Bill Location'] + $location->toArray(),
                                    $default_location, [
                                    'class' => 'form-control to_location select2',
                                    'onchange' => 'searchGrnList()',
                                    'id' => 'to_location',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Select Status</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select(
                                    'status',
                                    [
                                    '' => ' Select Status',
                                    '1' => ' Not Approved',
                                    '2' => ' Approved',
                                    '5' => ' Partially Approved',
                                    '3' => ' Cancelled',
                                    '4' => ' Closed',
                                    ],
                                    $grnListStatus,
                                    [
                                    'class' => 'form-control status',
                                    'onchange' => 'searchGrnList()',
                                    'id' => 'status',
                                    ],
                                    ) !!}
                                </div>
                            </div>


                            <div class="col-md-1 padding_sm">
                                <button id="grnlistpreviewBtn" type="button" onclick="getGrnListPreview()"
                                    class="btn btn-block light_purple_bg"><i id="grnlistpreviewSpin"
                                        class="fa fa-eye"></i>
                                    Preview</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <button type="button" onclick="searchGrnList()" id="searchgrnlistBtn"
                                    class="btn btn-block light_purple_bg"><i id="searchgrnlistSpin"
                                        class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <button type="button" onclick="clearFilter()" class="btn btn-block light_purple_bg"><i
                                        class="fa fa-times"></i> Clear</button>

                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <button id="Vendordatabtn" onclick="grn_listview(2,'')" type="button"
                                    title="Vendor Purchase" class="btn btn-block light_purple_bg">
                                    <i id="Vendordataspin" class="fa fa-industry"></i>
                                    Vendor</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <div onclick="addBillNew();" class="btn btn-block light_purple_bg"><i
                                        class="fa fa-plus"></i> Add</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix" id="searchGrnListData" style="min-height: 400px;">


                </div>
            </div>
            <input type="hidden" id="base_url" value="{{ url('/') }}" />
            <input type="hidden" id="token" value="{{ csrf_token() }}" />
        </div>
    </div>
</div>
{{-- ------------------------------------- --}}
<div id="re_edit_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width:85%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="current_grn_no">NA</h4>
            </div>
            <div class="modal-body" style="min-height:450px;">
                <div class="col-md-12" style="padding:0px !important;" id="request_setup_body">
                    <div class="col-md-8 box-body" id="grn_edit_details" style="height: 400px;">

                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <input type="hidden" id="request_id" value="0">
                            <input type="hidden" id="req_id_modal" value="0">

                            <div class="col-md-12 box-body" id="grn_edit_details" style="height: 400px;">
                                <h4 class="modal-title blue" id="editable_grn" style="text-align: center">Add Request
                                </h4>
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Remark</label>
                                        <div class="clearfix"></div>
                                        <textarea style="resize:none;margin-top:1px;height: 315px !important;"
                                            name="grn_edit_remark" id="grn_edit_remark" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3 padding_sm pull-right">
                                    <button type="button" class="btn btn-primary btn-block" id="add_this_req_btn1"
                                        onclick="setEditDetails(1)">
                                        <i id="add_this_req_spin1" class="fa fa-save"></i> Save</button>
                                </div>
                                @if ($edit_access_type == 1)
                                <div class="col-md-3 padding_sm pull-right" id="app_btn" style="display: none">
                                    <button type="button" class="btn btn-success btn-block" id="add_this_req_btn2"
                                        onclick="setEditDetails(2)">
                                        <i id="add_this_req_spin2" class="fa fa-thumbs-up"></i> Approve</button>
                                </div>
                                <div class="col-md-3 padding_sm pull-right" id="app_rej_btn" style="display: none">
                                    <button type="button" class="btn btn-danger btn-block" id="add_this_req_btn3"
                                        onclick="setEditDetails(3)">
                                        <i id="add_this_req_spin3" class="fa fa-thumbs-down"></i> Reject</button>
                                </div>
                                @endif
                                <div class="col-md-3 padding_sm pull-right">
                                    <button type="button" class="btn btn-warning btn-block" id=""
                                        onclick="reset_editables()">
                                        <i id="" class="fa fa-refresh"></i> Refresh</button>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>

    </div>
</div>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/Purchase/default/javascript/grn_list.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>

@stop
