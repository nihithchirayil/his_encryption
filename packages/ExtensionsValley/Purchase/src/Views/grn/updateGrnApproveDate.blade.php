@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

@stop

@section('css_extra')
    <style>
        .close_btn_vendor_search {

            position: absolute;
            z-index: 99;
            color: #FFF !important;
            background: #000;
            right: -11px;
            top: -1px;
            border-radius: 100%;
            text-align: center;
            width: 20px;
            height: 20px;
            line-height: 20px;
            cursor: pointer;
        }

        .vendor-list-div {
            position: absolute;
            z-index: 99;
            background: #FFF;
            box-shadow: 0 0 6px #ccc;
            padding: 10px;
            /* width: 95%; */
            width: 300px;
        }

        #VendorTable>tbody>tr:hover {
            background: #87d7ca52;
        }

        #VendorTable>tbody>tr>td {
            padding: 8px;
            line-height: 2;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .ajaxSearchBox {
            display: block;
            z-index: 7000;
            margin-top: 15px !important;
        }
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row codfox_container">
            <input type="hidden" id="base_url" value="{{ url('/') }}" />
            <input type="hidden" id="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="default_location" value="{{ $default_location }}">
            <div class="row" style="text-align: left; font-size: 12px; font-weight: bold;">
                <div class="col-md-12 padding_sm"> {{ $title }}</div>
            </div>
            <div class="col-md-12 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <form action="{{ route('extensionsvalley.purchase.grnList') }}" id="requestSearchForm"
                            method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="from_date" id="from_date">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" name="to_date" id="to_date">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">GRN No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="grn_no_search">
                                    <div id="ajaxGrnNoSearchBox" class="ajaxSearchBox"></div>
                                    <input type="hidden" id="grn_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="bill_no_search">
                                    <div id="ajaxBillNoSearchBox" class="ajaxSearchBox"></div>
                                    <input type="hidden" id="bill_id_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill Location</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('location', ['0' => ' Bill Location'] + $location->toArray(), $default_location, [
                                        'class' => 'form-control to_location select2',
                                        'onchange' => 'searchApproveGrn()',
                                        'id' => 'to_location',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top:13px;">
                                <button type="button" class="btn btn-warning btn-block" onclick="resetSearch()">Reset
                                    <i class="fa fa-recycle"></i></button>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top:13px;">
                                <button id="searchgrnlistBtn" type="button" class="btn btn-primary btn-block"
                                    onclick="searchApproveGrn()">Search <i id="searchgrnlistSpin"
                                        class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" id="searchGrnListData" style="min-height: 575px;">


                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- ------------------------------------- --}}
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/Purchase/default/javascript/updateApproveDate.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>

@stop
