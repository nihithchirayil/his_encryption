<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var grn_id = $('#grn_id_hidden').val();
                var bill_no = $('#bill_id_hidden').val();
                var location = $('#to_location').val();

                var param = {
                    _token: token,
                    from_date: from_date,
                    to_date: to_date,
                    grn_id: grn_id,
                    bill_no: bill_no,
                    location: location,
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#searchGrnListData").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        $('#searchgrnlistBtn').attr('disabled', true);
                        $('#searchgrnlistSpin').removeClass('fa fa-search');
                        $('#searchgrnlistSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#searchGrnListData').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function() {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    complete: function() {
                        $("#searchGrnListData").LoadingOverlay("hide");
                        $('#searchgrnlistBtn').attr('disabled', false);
                        $('#searchgrnlistSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchgrnlistSpin').addClass('fa fa-search');
                        $('.datetime_picker').datetimepicker({
                            format: 'MMM-DD-YYYY',
                        });
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>
<table class="table no-margin table-striped table-col-bordered table-condensed" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th>GRN No</th>
            <th>Bill No.</th>
            <th>Location</th>
            <th>Created Date</th>
            <th>Approved Status</th>
            <th>Approved By</th>
            <th>Vendor Name</th>
            <th>Net Amount</th>
            <th>Approved Date</th>
            <th>Date</th>
            <th>Time</th>
            <th><i class="fa fa-save"></i></th>
        </tr>
    </thead>
    <tbody>
        @if (count($item) > 0)
            @foreach ($item as $each)
                <?php

                $status = '';
                if ($each->approve_status == 1) {
                    $status = 'Not Approved';
                } elseif ($each->approve_status == 2) {
                    $status = 'Approved';
                } elseif ($each->approve_status == 3) {
                    $status = 'Cancelled';
                } elseif ($each->approve_status == 4) {
                    $status = 'Closed';
                }
                ?>
                <tr>
                    <td style="text-align: left;">{{ $each->grn_no }}</td>
                    <td style="text-align: left;">{{ $each->bill_no }}</td>
                    <td style="text-align: left;">{{ $each->location }}</td>
                    <td style="text-align: left;" title="{{ $each->created_at }}">
                        {{ $each->created_at }}
                    </td>
                    <td style="text-align: left;">
                        {{ $status }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->approved_by }}">
                        {{ $each->approved_by }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->vendor_name }}">
                        {{ $each->vendor_name }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->vendor_name }}">
                        {{ $each->net_amount }}
                    </td>
                    <td style="text-align: left;" title="{{ $each->approved_at }}">
                        {{ $each->approved_at }}
                    </td>
                    <td style="text-align: left;">
                        <input id="grnApproveDate{{ $each->bill_id }}" type="text"
                            class="form-control datetime_picker" value="{{ $each->approved_date }}">
                    </td>
                    <td style="text-align: left;">
                        <input id="grnApproveTime{{ $each->bill_id }}" type="time" class="form-control"
                            value="{{ $each->approved_time }}">
                    </td>
                    <td style="text-align: center;" title="{{ $each->approved_at }}">
                        <button id="updateGrnApproveDateBtn{{ $each->bill_id }}"
                            onclick="updateGrnApproveDate({{ $each->bill_id }})" type="button"
                            style="padding: 1px 6px" class="btn btn-primary"><i
                                id="updateGrnApproveDateSpin{{ $each->bill_id }}" class="fa fa-save"></i></button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="12" class="location_code">No Records found</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination">
        {!! $page_links !!}
    </ul>
</div>
