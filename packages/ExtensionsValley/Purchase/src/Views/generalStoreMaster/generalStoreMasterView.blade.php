@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->

<div class="right_col" role="main">
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm" style="font-weight: 600; color: #337ab7!important; margin-top: 1px;">{{
            $title }}</div>
        <div class="col-md-12 padding_sm">
            <div class="col-md-8 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #337ab7;min-height: 70px;">
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Item Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" name="search_item_type" id="search_item_type">
                                    <option value="">Select</option>
                                    @foreach ($item_type as $each)
                                    <option value="{{ $each->id }}">{{ $each->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label class="custom_floatlabel">Item Description</label>
                                <input onkeyup="searchItemDesc(event)" type="text" autocomplete="off"
                                    name="search_item_description" id="search_item_description" class="form-control">
                                <div class='ajaxSearchBox' id="ajaxSearchBox_description" index='1'
                                    style="max-height: 350px; margin-top: 20px !important;margin-left: -5px;"> </div>
                                <input type='hidden' value="" id="item_id_hidden">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Category</label>
                                <div class="clearfix"></div>
                                <select
                                    onchange="getSubCategory('search_category_master','search_subcategory_master',0)"
                                    class="form-control select2" name="search_category_master"
                                    id="search_category_master">
                                    <option value="">Select</option>
                                    @foreach ($category as $each)
                                    <option value="{{ $each->id }}">{{ $each->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Sub Category</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" name="search_subcategory_master"
                                    id="search_subcategory_master">
                                    <option value="">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 15px">
                            <button onclick="resetfilter()" type="button" class="btn btn-warning btn-block"><i
                                    class="fa fa-recycle"></i> Reset </button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 15px">
                            <button id="searchGeneralStoreBtn" onclick="searchGeneralStore()" type="button"
                                class="btn btn-primary btn-block"><i id="searchGeneralStoreSpin"
                                    class="fa fa-search"></i>
                                Search </button>
                        </div>
                    </div>
                </div>
                <div class="box no-border" style="margin-top: 10px">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #337ab7;min-height: 520px;">
                        <div id="searchGeneralStoreData"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc;border-radius:10px; border: 2px solid #337ab7;min-height: 610px;">
                        <div class="col-md-12 blue text-center padding_sm">
                            <h5 id="item_title"> Add General Store Item Master </h5>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Item Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" name="item_type" id="item_type">
                                    <option value="">Select</option>
                                    @foreach ($item_type as $each)
                                    <option value="{{ $each->id }}">{{ $each->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Item Code</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly style="background-color: #FFF"
                                    name="item_code" id="item_code" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Item Description</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" name="item_description" id="item_description"
                                    placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Category</label>
                                <div class="clearfix"></div>
                                <select onchange="getSubCategory('category_master','subcategory_master',0)"
                                    class="form-control select2" name="category_master" id="category_master">
                                    <option value="">Select</option>
                                    @foreach ($category as $each)
                                    <option value="{{ $each->id }}">{{ $each->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Sub Category</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" name="subcategory_master" id="subcategory_master">
                                    <option value="">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="custom-float-label-control mate-input-box">
                                <label class="custom_floatlabel">Standard UOM</label>
                                <div class="clearfix"></div>
                                {!! Form::select('std_uom', ['0' => ' Select UOM'] + $Uom->toArray(), null, [
                                'class' => 'form-control select2',
                                'id' => 'std_uom',
                                'disabled' => true,
                                ]) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" style="margin-top: 5px">
                            <div class="theadscroll always-visible" style="position: relative; height: 150px;">
                                <table
                                    class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin uom_table"
                                    style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_blue">
                                            <th width="55%">Purchase UOM</th>
                                            <th>Conv Factor</th>
                                            <th>Default</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < 5; $i++) { ?>
                                        <tr>
                                            <td>
                                                {!! Form::select('purchase_uom', ['' => ' Select UOM'] +
                                                $Uom->toArray(),
                                                null, [
                                                'class' => 'form-control select2 purchase_uom',
                                                'id' => 'pur_uom' . $i,
                                                'data-id' => $i ,
                                                'onchange' => 'setenabledefault(' . $i . ')',
                                                ]) !!}
                                            </td>
                                            <td>
                                                <input type="text" id="pur_con_fact{{ $i }}" name="pur_con_fact"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                    class="form-control pur_con_fact">
                                            </td>
                                            <td>
                                                <div class="col-md-1 padding_sm">
                                                    <div class="radio radio-primary radio-inline">
                                                        <input disabled onchange="setStdUom({{ $i }})" type="radio"
                                                            class="pur_default_cls" id="pur_default{{ $i }}"
                                                            value="{{ $i }}" name="pur_default">
                                                        <label for="pur_default{{ $i }}">
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 padding_sm" style="margin-top: 10px">
                            <div class="checkbox checkbox-success checkbox-inline">
                                <input type="checkbox" id="is_purchasable" value="1" name="is_purchasable">
                                <label for="is_purchasable"> Is Purchasable </label>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top: 10px">
                            <div class="checkbox checkbox-success checkbox-inline">
                                <input type="checkbox" id="is_sellable" value="1" name="is_sellable">
                                <label for="is_sellable"> Is Sellable </label>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm" style="margin-top: 10px">
                            <div class="checkbox checkbox-success checkbox-inline">
                                <input type="checkbox" id="is_active" value="1" name="is_active" checked="">
                                <label for="is_active"> Is Active </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm" style="margin-top: 20px">
                            <button onclick="resetSave()" type="button" class="btn btn-warning btn-block"><i
                                    class="fa fa-recycle"></i> Reset </button>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 20px">
                            <button id="saveGeneralStoreBtn" onclick="saveGeneralStore()" type="button"
                                class="btn btn-success btn-block"><i id="saveGeneralStoreSpin" class="fa fa-save"></i>
                                Save </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
<input type="hidden" id="general_item_id" value="0">
<input type="hidden" id="editsubcategory_item_id" value="0">
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/generalStoreMaster.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
