<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if(url && url!='undefined'){
                var item_type = $('#search_item_type').val();
                var item_description = $('#search_item_description').val();
                var category_master = $('#search_category_master').val();
                var subcategory_master = $('#search_subcategory_master').val();
                var param = {
                    item_type: item_type,
                    item_description: item_description,
                    category_master: category_master,
                    subcategory_master: subcategory_master,
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $('#searchGeneralStoreBtn').attr('disabled', true);
                        $('#searchGeneralStoreSpin').removeClass('fa fa-search');
                        $('#searchGeneralStoreSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function (data) {
                        $('#searchGeneralStoreData').html(data);
                    },
                    complete: function () {
                        $('#searchGeneralStoreBtn').attr('disabled', false);
                        $('#searchGeneralStoreSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchGeneralStoreSpin').addClass('fa fa-search');
                    },
                    error: function () {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
             }
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper no-border"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_blue">
                    <th class="common_td_rules" width="20%">Item Code</th>
                    <th class="common_td_rules" width="30%">Item Description</th>
                    <th class="common_td_rules" width="10%">Item Type</th>
                    <th class="common_td_rules" width="20%">Category Name</th>
                    <th class="common_td_rules" width="20%">Sub Category Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($general_storelist) != 0) {
                foreach ($general_storelist as $list) {
                    ?>
                <tr class="editGeneralStore" id="editGeneralStore<?= $list->product_id ?>" onclick="editGeneralStore(<?= $list->product_id ?>)"
                    id="generalstore_datarow<?= $list->product_id ?>" style="cursor: pointer">
                    <td title="{{ $list->item_code }}" class="common_td_rules">{{ $list->item_code }}</td>
                    <td title="{{ $list->item_desc }}" class="common_td_rules">{{ $list->item_desc }}</td>
                    <td title="{{ $list->item_type }}" class="common_td_rules">{{ $list->item_type }}</td>
                    <td title="{{ $list->category_name }}" class="common_td_rules">{{ $list->category_name }}</td>
                    <td title="{{ $list->subcategory_name }}" class="common_td_rules">{{ $list->subcategory_name }}</td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="5" class="re-records-found">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
