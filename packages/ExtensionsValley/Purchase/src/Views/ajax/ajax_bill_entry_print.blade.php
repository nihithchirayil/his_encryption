<style type="text/css">
    @page {
        margin: 0;
    }

    .body {
        margin: 15px;
    }

    #tblhead td {
        border: 0px !important
    }

</style>
@if (isset($headerResults) && !empty($headerResults))
    <?php $row_cnt = 0; ?>
    <div name="printData" id="printData" class="body">

        <table border="1" width="100%" style="font-size: 12px; border-collapse: collapse;"
            class="table  table-bordered table-condensed table_sm">
            <thead id="printHead">
                <tr>
                    <td colspan="13" style="">
                        <table border="0" style="width: 91%; font-size: 12px;" id="tblhead">
                        </table>
                    </td>
                </tr>
            </thead>
            <tbody>
                <th colspan="13" style="text-align: center;border-bottom:1px solid black;">{{ $title }}</th>

                <tr class=" font-normal" style="border: none;">
                    <td style="width: 9%;"><b>Location</b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td colspan="6">{{ $headerResults[0]->location_name }}</td>
                    <td>&nbsp;</td>
                    <td style="width: 9%;"><b>Status </b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td style="text-align: left">{{ $headerResults[0]->process_status }}</td>
                </tr>

                <tr class=" font-normal" style="border: none;">
                    <td><b>Sup Name</b></td>
                    <td style="text-align: right"><b>:</b></td>
                    <td colspan="6" style="font-size: 14px"><b>{{ $headerResults[0]->vendor_name }}</b></td>
                    <td>&nbsp;</td>
                    <td><b>GRN Date </b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td>{{ date('d-m-Y', strtotime($headerResults[0]->grn_date)) }}</td>
                </tr>

                <tr class=" font-normal">
                    <td><b>Sup GST No.</b></td>
                    <td style="text-align: right"><b>:</b></td>
                    <td colspan="6">{{ $headerResults[0]->gst_vendor_code }}</td>
                    <td>&nbsp;</td>
                    <td><b>Grn No</b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td>{{ $headerResults[0]->grn_no }}</td>
                </tr>

                <tr class="font-normal" style="border: none;">
                    <td colspan=""> <b> Address </b></td>
                    <td style="text-align: right"><b>:</b></td>
                    <td colspan="10">
                        {{ $headerResults[0]->address_1 . ' , ' . $headerResults[0]->address_2 . ' , ' . $headerResults[0]->address_3 }}
                    </td>
                </tr>

                <!-- head end -->

                <tr class=" font-normal">
                    <td><b>BILL No</b></td>
                    <td style="text-align: right"><b>:</b></td>
                    <td colspan="6"><b>{{ $headerResults[0]->bill_no }}</b></td>
                    <td>&nbsp;</td>
                    <td><b>Bill Date</b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td colspan="3"><b>{{ date('d-m-Y', strtotime($headerResults[0]->bill_date)) }}</b></td>
                </tr>

                <tr class=" font-normal">
                    <td><b>PO No</b></td>
                    <td style="text-align: right"><b>:</b></td>
                    <td colspan="6">{{ $headerResults[0]->po_no }}</td>
                    <td>&nbsp;</td>
                    <td><b>Po Date</b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td colspan="3">
                        @if ($headerResults[0]->grn_with_po == 1)
                            {{ date('d-m-Y', strtotime($headerResults[0]->po_date)) }}
                        @endif
                    </td>
                </tr>

                <tr>
                    <td><b>Remark</b></td>
                    <td style="text-align: right"><b>:</b></td>
                    <td colspan="6">{{ $headerResults[0]->narration }}</td>
                    <td>&nbsp;</td>
                    <td><b>GRN Against</b></td>
                    <td style="text-align: right"><b>: </b></td>
                    <td colspan="3">
                        @if ($headerResults[0]->grn_with_po == 0)
                        @endif
                        @if ($headerResults[0]->grn_with_po == 1)
                            With PO
                        @endif
                        @if ($headerResults[0]->grn_with_po == 2)
                            With Out PO
                        @endif
                    </td>
                </tr>


            </tbody>
        </table>
        @if (isset($detailResults) && !empty($detailResults))
            <table class="table" id="tblQPVE" border="0"
                style="border-collpase: collapse;font-size: 12px;margin-top: 30px;;font-family: sans-serif !important;"
                class="table  table-bordered table-condensed table_sm">
                <tbody>
                    <tr class="font-normal td_border" style="border:1px solid #000;padding:0.5em;">
                        <td colspan="4" style="text-align: center;"><b>Item Name</b></td>
                        <td style="min-width: 20px;"><b>Rec</b></td>
                        <td style="min-width: 20px;"><b>Free</b></td>
                        <td style="min-width: 20px;"><b>Total <br> Calc. Qty</b></td>
                        <td style="min-width: 20px;"><b>Disc</b></td>
                        <td colspan="3" style="text-align: center;"><b>GST</b></td>
                        <td><b>Rate</b></td>
                        <td><b>MRP</b></td>
                        <td><b>Unit Rate</b></td>
                        <td><b>Unit MRP</b></td>
                        <td><b>Sale Price</b></td>
                        <td><b>Amount</b></td>

                    </tr>
                    <tr class="font-normal td_border" style="border:1px solid #000;padding:0.5em;">
                        <td><b>HSN Code</b></td>
                        <td><b>Batch No.</b></td>
                        <td><b>Expiry</b></td>
                        <td><b>Unit Name /<br>Value Calc.</b></td>
                        <td><b>Qty</b></td>
                        <td><b>Qty</b></td>
                        <td></td>
                        <td><b>Charges</b></td>
                        <td><b>CGST(%)</b></td>
                        <td><b>SGST(%)</b></td>
                        <td><b>IGST(%)</b></td>
                        <td colspan="6"></td>
                        <td colspan="2"></td>
                    </tr>
                    <?php
                    $total_rate = 0;
                    $total_mrp = 0;
                    $total_rec = 0;
                    $total_free_qty = 0;
                    $total_qty = 0;
                    $all_total_qty = 0;
                    $free_qty = 0;
                    $other_charges_total = 0;
                    $freight_charges_total = 0;
                    $rate_wt_tx = 0;

                    ?>
                    @foreach ($detailResults as $detail)
                        <?php
                        ?>
                        <tr class="td_border" style="border:1px solid #000;padding:0.5em;">
                            <td colspan="4"> <b style="word-break: break-all;">{{ $detail->item_desc }}</b> </td>
                            <td style="text-align: center">{{ $detail->rec_qty }}</td>
                            <td style="text-align: center">{{ $detail->grn_free_qty }}</td>
                            <td style="text-align: center">{{ $detail->all_total_quantity }}</td>
                            <td style="text-align: right">{{ $detail->discount_perc + $detail->bill_discount_perc }}</td>
                            <td style="text-align: right">{{ $detail->cgst }}</td>
                            <td style="text-align: right">{{ $detail->sgst }}</td>
                            <td style="text-align: right">{{ $detail->igst }}</td>
                            <td style="text-align: right">{{ $detail->grn_rate }}</td>

                            <td style="text-align: right">{{ $detail->grn_mrp }}</td>
                            <td style="text-align: right">{{ $detail->unit_rate }}</td>
                            <td style="text-align: right">{{ $detail->unit_mrp }}</td>
                            <td style="text-align: right">{{ $detail->selling_price }}</td>
                            <td style="text-align: right">{{ $detail->amount }}</td>
                        </tr>
                        <tr class="td_border">
                            <td>{{ $detail->hsn_code }}</td>
                            <td>{{ $detail->batch_no }}</td>
                            <td>{{ date('d/m/Y', strtotime($detail->expiry_date)) }}</td>
                            <td>{{ $detail->unit }} / {{ $detail->conv_factor }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td colspan="6"></td>

                        </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table pull-right"
                style="width: 30%;border-collpase: collapse;font-size: 12px;font-family: sans-serif !important;float:right;margin: 20px 0 20px 0;">
                <tr>
                    <td>
                        <b>Other Charges</b>
                    </td>
                    <td width="10%">:</td>
                    <td style="text-align: right">
                        <b>{{ $other_amt }} </b>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <b>Freight</b>
                    </td>
                    <td width="10%">:</td>
                    <td style="text-align: right">
                        <b>{{ $feight_amt }}</b>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <b> Round Off</b>
                    </td>
                    <td>:</td>
                    <td style="text-align: right">
                        <b>{{ $detail->round_amnt }}</b>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <b> CGST Amt </b>
                    </td>
                    <td>:</td>
                    <td style="text-align: right">
                        <b>{{ floatval($detail->totalsgst / 2) }}</b>

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <b> SGST Amt </b>
                    </td>
                    <td>:</td>
                    <td style="text-align: right">
                        <b>{{ floatval($detail->totalsgst / 2) }}</b>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <b> IGST Amt </b>
                    </td>
                    <td>:</td>
                    <td style="text-align: right">
                        <b>{{ $detail->totaligst }}</b>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <b> Net GRN Value </b>
                    </td>
                    <td>:</td>
                    <td style="text-align: right;font-size: 18px;">
                        <b>{{ $detail->net_amount }}</b>
                    </td>
                    <td></td>
                </tr>

            </table>


            <div style="clear: both;"></div>
            <table style="width: 100%">
                <tr>
                    <td width="30%">
                        <b> Approved By :
                            <?php
                $createdby = '';
                $createdby_id = '';

                $createdby_id = $detail->created_by;
                $createdby = \DB::table('users')->where('id', $createdby_id)->pluck('name');

                $approvedby = '';
                $approvedby_id = '';

                $approvedby_id = $detail->approved_by;
                if($approvedby_id!='')
                {
                $approvedby = \DB::table('users')->where('id', $approvedby_id)->pluck('name');
                ?>
                            {{ $approvedby[0] }} </b>
                        <?php } ?>
                    </td>

                    <td>
                        <b> Quality Verified </b>
                    </td>
                    <td>
                        <b> Manager </b>
                    </td>
                    <td>
                        <b> AGM </b>
                    </td>
                </tr>
            </table>

            <br>
            <?php
      if(empty($signature_name)){
    ?>
            <br>
            <br>
            <br>
            <?php
      }
    ?>

            <div style="clear: both;"></div>
            <table class="table" style="border-collapse: collapse;">
                <tr>
                    <td width="25%" style="font-size: 11px">
                        <b> Prepared By: </b>
                    </td>
                    <td width="32%" style="font-size: 11px">
                        {{ $createdby[0] }}
                    </td>

                    <td style="font-size: 11px">
                        <b> Printed By: </b>
                    </td>
                    <td style="font-size: 11px">
                        {{ \Auth::user()->name }}
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px">
                        <b> Prepared at: </b>
                    </td>

                    <td style="font-size: 11px">{{ date('d-m-Y h:i A', strtotime($headerResults[0]->created_at)) }}
                    </td>

                    <td style="font-size: 11px">
                        <b> Printed at: </b>
                    </td>
                    <td style="font-size: 11px">
                        <?php
                        $currentDateTime = '';
                        $newDateTime = '';
                        $currentDateTime = date('d-m-Y H:i:s');
                        ?>
                        {{ $newDateTime = date('d-m-Y h:i A', strtotime($currentDateTime)) }}
                    </td>

                </tr>
                <?php
                $company_arr = \DB::table('company')
                    ->select('drug_licence_no', 'gst_no')
                    ->first();
                ?>
                <td style="font-size: 11px">
                    <b>Pharmacy GST no: </b>
                </td>
                <td style="font-size: 11px">{{ $company_arr->gst_no }}</td>
                <td style="font-size: 11px">
                    <b>Pharmacy Drug Licence no: </b>
                </td>
                <td style="font-size: 11px">
                    {{ $company_arr->drug_licence_no }}
                </td>
                </tr>
                <?php ?>
            </table>
        @else
            <div style="width: 100%; text-align: center;color: red;">Bill details not found</div>
        @endif
@endif
<style type="text/css">
    table {
        border-collapse: collapse;
        padding-top: 2px;
    }

    .td_border th,
    .td_border td {
        border: 1px solid #000;
    }

</style>
</div>
