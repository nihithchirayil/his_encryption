
<?php
$count = 1;
$total_rate = 0;
$Item_value = 0;
?>
<html>
<head>
<style>
 /*  @page { margin: 5px;  }
body { margin: 5px;  }*/
.total_table{
                width: 100%;
    border: none !important;
    font-size: 12px;
            }
.total_table td{
    border: none;
}
.total_table td:last-child{
    width: 10%;
}
table tr,table td{
    page-break-inside: avoid !important;
    page-break-before: always !important;
    page-break-after: always !important;
}   
.classtable tr {
    page-break-before: always !important;
    page-break-inside: avoid !important;
}
@page {
    margin:20px;
}
</style>
</head>
<body>
<br>
<?php
$hospital_details = \DB::table('company')->first();
$vendor_details = \DB::table('vendor')->Where('vendor_code', $po_head_details->vendor_code)->first();
$location_details = \DB::table('location')->Where('location_code', 'ST001')->first();
       $single_tax_status =0;
       
    $var_currency_symbol_rs = '';

  $colspam = '2';

    $colspam1 = '9';
    $colspam2 = '7';
    $colspam0 = '16';

?>

    <table border="1" width="100%" style="font-size: 12px; border-collapse: collapse;" class="table table-striped  table-condensed  sorted_table">
        <thead>
            <tr class="header">

                <th colspan="{{$colspam0}}" style="padding: 0px;">
                <table style="width: 100%; font-size: 12px;">    
                <tr>
                {{-- include('inpatient::bills.partials.ExtensionsValley.report_header')                                                            --}}
                </tr>  
                </table>
                </th>
            </tr>
            <tr class="header">
                <th colspan="{{$colspam0}}" align="center"> 
                     <strong style="font-size:23px">Purchase Order</strong>
                </th>
            </tr>
            <tr class="header">
                <th colspan="{{$colspam2}}" align="left"> 
                    <b> M/s. {{$vendor_details->vendor_name}}</b>
                </th>
                <th colspan="{{$colspam1}}" align="left"> 
                    <b> P.O.No: {{$po_head_details->po_no}}
                    <!-- /Fin Year : 
                    @if(isset($finance_year->financial_year))
                        {{$finance_year->financial_year}}
                    @endif -->
                    </b>
                </th>
            </tr>
            <tr>
                <th colspan="{{$colspam2}}" align="left">Address:  @if($vendor_details->address_1){{$vendor_details->address_1}},@endif
                @if($vendor_details->address_2){{$vendor_details->address_2}},@endif
                @if($vendor_details->address_3){{$vendor_details->address_3}}@endif</th>
                <th colspan="{{$colspam1}}" align="left"> P.O.DATE: {{date('d-m-Y'),strtotime($po_head_details->po_date)}}</th>
            </tr>
            <tr>
                <th colspan="{{$colspam2}}" align="left"> Email :{{$vendor_details->email}}</th>
                <th colspan="{{$colspam1}}" align="left"> Department :{{$po_head_details->dep_name}}</th>
            </tr>
            <tr>
                <th colspan="{{$colspam2}}" align="left"> Telephone No: {{$vendor_details->contact_no}}</th>
                <th colspan="{{$colspam1}}" align="left"> Location : {{$po_head_details->location_name}}</th>
            </tr>
            <tr>
                <th colspan="{{$colspam2}}" align="left"> 
<!--                    GSTIN: {{$vendor_details->gst_vendor_code}}-->
                </th>
                <th colspan="{{$colspam1}}" align="left">
                 
                </th>
            </tr>
        </thead>

        
        <thead>
            <tr>
            <td width="1%">SL. No</strong><br> </td>
            <td width="7%">HSN</strong><br> </td>
            <td width="15%" colspan="{{$colspam}}">Item Name</strong><br> </td>
            <td width="5%">Qty</th>
            
            <td width="5%">Free Qty</td>
            <td width="5%">Unit</td>
            <td width="5%">Rate</td>
            <!--<th width="7%">MRP</th>-->
            <td width="5%">Bill Disc.%</td>
            <td width="5%">Disc.%</td>
            <?php   
            $per = '5%';
            $per1 = '7%';
            if ($single_tax_status == '1') { 
                $per = '7%';
            $per1 = '10%';
                ?>
            <td width="5%">TAX%</td>
             <?php   }else{ ?>
            <td width="5%">CGST%</td>
            <td width="5%">SGST%</td>
            <td width="5%">IGST%</td>
            <td width="5%">TCS%</td>
            <?php } ?>
            <td width="{{$per}}">Cost<br>(Per Qty)</td>
            <td width="{{$per1}}" >Item Value</td>
            </tr>
        </thead>
        <tbody>
            <?php
                $t_sgst = 0;
                $t_cgst = 0;
                $t_igst = 0;
                $t_tcs = 0;
                $t_dis = 0;
                $bill_dis=0;
                $t_other_chr = 0;
                $t_free_tax = 0;
                $dis_mode = 0;
                
            ?>
             @foreach($po_items as $item)
             <?php 
             $cost_per =0;
             $cgst_per =!empty($item->cgst_per)?$item->cgst_per:0;
             $sgst_perc =!empty($item->sgst_perc)?$item->sgst_perc:0;
             $igst_perc =!empty($item->igst_perc)?$item->igst_perc:0;
             $cost_per = $item->total/($item->pur_qnty+$item->free_qty);
             if($item->pur_qnty< 1){
                $cost_per = $item->total;
            }
             
            ?>
                <tr>
                    <td>{{$count++}}</strong><br> </td>
                    <td>{{$item->hsn_code}}</td>
                    <td  colspan="{{$colspam}}">{{$item->item_desc}}</td>
                    <td align="right">{{intval($item->pur_qnty)}}</td>
                    <td align="right">{{intval($item->free_qty)}}</td>f
                    <td align="right">{{$item->unit_name}}</td>
                   
                    
                        <td align="right">{{number_format((float)$item->pur_rate, 2, '.', '')}}</td>
                        <td align="right">
                            @if($item->discount_cal_mode == 2)
                                {{number_format((float)$item->bill_discount_value, 2, '.', '')}}
                            @else
                                {{number_format((float)$item->item_dis_prc, 2, '.', '')}}
                            @endif
                        </td>    
                        
                        <td align="right">{{str_replace('-', '', number_format((float)$item->total_disc_perc-$item->item_dis_prc, 1, '.', ''))}}</td>
                       <?php   if ($single_tax_status == '1') { ?>
                        <td align="right">{{number_format((float)$item->cgst_per, 2, '.', '')}}</td>
                        <?php   }else{ ?>
                        <td align="right">{{number_format((float)$item->cgst_per, 2, '.', '')}}</td>
                        <td align="right">{{number_format((float)$item->sgst_perc, 2, '.', '')}}</td>
                        <td align="right">{{number_format((float)$item->igst_perc, 2, '.', '')}}</td>
                        <td align="right">{{number_format((float)$item->tcs_perc, 2, '.', '')}}</td>
                          <?php } ?>
                        <td align="right">{{number_format((float)$cost_per, 2, '.', '')}}</td>
                        <td align="right">{{$item->pur_qnty*$item->pur_rate}}</td>
                </tr>
                 <?php
                    $t_sgst += $item->sgst_value;
                    $t_cgst += $item->cgst_value;
                    $t_igst += $item->igst_value;
                    $t_tcs += $item->tcs_value;
                    $t_dis += $item->total_disc_amount-$item->item_dis_amt;
                    $t_other_chr += $item->total_other_charge;
                    $t_free_tax += $item->total_free_tax_amount;
                    if($item->discount_cal_mode == 2){
                        $bill_dis = $item->bill_discount_amount;
                    } else {
                        $bill_dis += $item->item_dis_amt;
                    }
                ?>
            @endforeach
               
                <tr>
                 <td colspan="{{$colspam0}}" align="right">
                     <!-- Total Value(Rs)
                   {{$po_head_details->total_purchase_cost}}<br>
                    Other Charge(Rs)
                    {{$t_other_chr}}<br>
                    Item Discount(-)
                     {{str_replace('-', '', $t_dis)}}<br>
                    Bill Discount(-)
                    {{$bill_dis}}<br>
                     Free Tax 
                     {{$t_free_tax}}<br>
                    CGST 
                     {{$t_cgst}}<br>
                    SGST
                    {{$t_sgst}}<br>
                    IGST
                     {{$t_igst}}<br>
                    Grand Total(RS)
                    {{$po_head_details->net_total}}<br> -->
                        <table class="total_table">
                            <tbody>
                            
                                <tr>
                                    <td style="text-align: right;" colspan="2"> Other Charge {{ $var_currency_symbol_rs }} : </td>
                                    <td style="text-align: right;"><b>{{$t_other_chr}}</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="2">Item Discount(-) : </td>
                                    <td style="text-align: right;"><b>{{str_replace('-', '', $t_dis)}}</b></td>
                                </tr>
                                
                                <tr>
                                    <td style="text-align: right;" colspan="2"> Bill Discount(-) :</td>
                                    <td style="text-align: right;"><b>{{$bill_dis}}</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="2">Free Tax  :</td>
                                    <td style="text-align: right;"><b>{{$t_free_tax}}</b></td>
                                </tr>
                                <?php   if ($single_tax_status == '1') { ?>
                                    <tr>
                                        <td style="text-align: right;" colspan="2">TAX  : </td>
                                        <td style="text-align: right;"><b>{{$t_cgst}}</b></td>
                                </tr>
                                <?php   }else{ ?>
                                    <tr>
                                        <td style="text-align: right;" colspan="2">CGST  : </td>
                                    <td style="text-align: right;"><b>{{$t_cgst}}</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="2">SGST  : </td>
                                    <td style="text-align: right;"><b>{{$t_sgst}}</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="2">IGST  : </td>
                                    <td style="text-align: right;"><b>{{$t_igst}}</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="2">Total Value {{ $var_currency_symbol_rs }} :</td>
                                    <td style="text-align: right;"><b>{{$po_head_details->total_purchase_cost}} </b></td>
                                </tr>
                                {{-- <tr>
                                    <td style="text-align: right;" colspan="2">TCS  : </td>
                                    <td style="text-align: right;"><b>{{number_format((float)$t_tcs,2, '.', '')}}</b></td>
                                </tr> --}}
                                     <?php } ?>
                                <tr>
                                    <td width="60%" style="text-align: left;">
                                    <b style="font-size: 12px;">Total Amount In Words :<b><br/>
                                    <?php 
                                        if(isset($po_items[0])){
                                            $number_to_word =  \ExtensionsValley\Master\GridController::number_to_word($po_items[0]->net_total);
                                            echo '<b style="font-size: 11px;">('.$number_to_word.')</b>';
                                        }
                                    ?>
                                    </td>
                                    <td style="text-align: right;"><b>Grand Total {{ $var_currency_symbol_rs }}  : </b></td>
                                    <td style="text-align: right;"><b>{{$po_head_details->net_total}}</b></td>
                                </tr>
                            </tbody>
                        </table>
                </td>
            </tr>

                    <!-- <tr>
                        <td colspan="15" align="left"><b>Total Amount In Words:</b></td>
                    </tr>
                    <tr>
                       <td  colspan="15" align="left">
                       <?php   
                           if(isset($po_items[0])){
                               //echo  $number_to_word =  \ExtensionsValley\Master\GridController::number_to_word($po_items[0]->net_total);
                            } 
                        ?>  
                        </td>
                    </tr> -->
                   
                        <?php  $span = '15'; ?>
                   
                        
                        <tr>
                            <td align="left"><b>Remarks </b></td>
                            <td colspan="{{$span}}" align="left">
                                <p style=" word-break: break-all;margin-bottom: 0px;margin-top: 0px;">{{$po_head_details->remarks}}</p>
                            </td>
                        </tr>
                        
                        
                        <tr>
                           <td colspan="{{$colspam0}}" align="left" height="80">Remarks : </td>
                        </tr>
                        <tr>
                           <td colspan="{{$colspam0}}" align="left">
                           @if(isset($hospital_details->gst_no))
                            <!--<b>GST No :</b>{{$hospital_details->gst_no}}-->  
                            <b>Drug License No : </b>{{$hospital_details->drug_licence_no}},
                            <b>PAN No : </b> {{$hospital_details->pan_no}}
                            @endif
                           </td>
                        </tr>
                        
        </tbody>
        <tfoot>
            </tfoot>
    </table>
<table width="100%" border="1" style="table-layout: fixed; white-space: normal; word-wrap: break-word; word-break: break-all; border-collapse: collapse;font-size: 12px;"><tbody>
               
                        <?php  $span = '5';
                        $span1 = '10'; ?>
                    
                    <tr class="footer">
                        <th colspan="{{$span}}" align="left">
                            <p><b>Seller Details</b></p>
                            <p>
                             <?php 
                                    if(count($seller_acceptance) > 0) {
                                        foreach($seller_acceptance as $k => $v) { 
                                              if($v->type == 1) 
                                            echo html_entity_decode($v->value);
                                         } 
                                    } ?>
                            </p>
                            <p><b>Vendor: {{$vendor_details->vendor_name}}</b></p>
                        </th>
                       
                        <th colspan="{{$span1}}" align="left" style="word-break: break-all;white-space: normal;">
                            <p><b>Buyer</b></p>
                            
                                <p>{{$hospital_details->city}} {{$hospital_details->address}}  {{$hospital_details->name}}</p>
                          
                                <?php 
                                    if(count($seller_acceptance) > 0) {
                                        foreach($seller_acceptance as $k => $v) { 
                                              if($v->type == 2) 
                                            echo html_entity_decode($v->value);
                                         } 
                                    } ?>
                        </th>
                    </tr>
                    @if(isset($po_items[0]->approved_by))
                    <tr class="footer">
                        <?php
                            $user_id = !empty($po_head_details->id) ? $po_head_details->id : 0;
                             
                        ?>
                        <th colspan="3">Approved By: <br/> {{$po_items[0]->approved_by}}
                                
                                        @if(!empty($po_head_details->signature_name))
                                        <br>
                                            <img id="sign" alt="" width="100" height="50" style="margin:0 100px" 
                                            src="{{URL('packages/visits/uploads/signature/').'/'.$po_head_details->signature_name}}" /> 
                                        @endif
                            <br/>
                            @if(isset($po_head_details->approved_at) && !empty($po_head_details->approved_at))({{ 
                                date('d-m-Y'), strtotime($po_head_details->approved_at) }}) @endif
                        </th>
                        <th colspan="4">Verified By: <br/>  {{$po_items[0]->verified_by}}
                        
                            <br/>
                            @if(isset($po_head_details->verified_at) && !empty($po_head_details->verified_at))({{ 
                                date('d-m-Y'), strtotime($po_head_details->verified_at) }}) @endif
                        </th>
                        <th colspan="4"> Prepared By: <br/> {{$po_items[0]->created_by}}
                        <br/>
                        @if(isset($po_head_details->created_at) && !empty($po_head_details->created_at))({{ 
                            date('d-m-Y'), strtotime($po_head_details->created_at) }}) @endif
                        @endif
                        </th>
                        <th colspan="4"> Print Date Time: <br/> {{date('d-m-Y')}}</th>
                    </tr>
        </tbody></table>



    </body>

</html>