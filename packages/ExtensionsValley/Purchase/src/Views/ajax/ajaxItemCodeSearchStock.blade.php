<?php

/* For Item details search for purchase order */
if (isset($itemCodeStockTransferDetails)) {    //print_r($itemCodePODetails); exit;
    if(!empty($itemCodeStockTransferDetails)){
        foreach ($itemCodeStockTransferDetails as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->chemical_name)}}","{{htmlentities($item->batch_no)}}","{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->stock)}}","{{htmlentities($item->selling_price)}}","{{htmlentities($item->mrp)}}","{{htmlentities($item->selling_uom)}}")'>
                    <?php echo htmlentities($item->item_desc) . '     [ Chemical Name:   ' . htmlentities($item->chemical_name). ' ]    [ Item Code:   ' . htmlentities($item->item_code) . '   ]'. '     [ Batch:   ' . htmlentities($item->batch_no) . '   ]'; ?>
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

elseif (isset($itemCodeInsuranceReturn)) {   // print_r($itemCodeStockAdjustment); exit;
    if(!empty($itemCodeInsuranceReturn)){
        foreach ($itemCodeInsuranceReturn as $item) { 
            $selling_price = (($item->item_selling_price * $item->item_tax)/100) + $item->item_selling_price ;
            $selling_price = number_format($selling_price,2);
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->chemical_name)}}",atob("<?= base64_encode($item->item_code) ?>"),atob("<?= base64_encode($item->item_desc) ?>"),"{{htmlentities($item->item_tax)}}","{{$selling_price}}","{{htmlentities($item->item_mrp)}}")'
            >
                    <?php echo htmlentities($item->item_desc); ?>
                    @if(isset($item->chemical_name))[ Chemical Name:  {{$item->chemical_name}} ]@endif
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}
elseif (isset($itemWithDetails)) {  
    if(!empty($itemWithDetails)){
        foreach ($itemWithDetails as $item) { 
//            $selling_price = (($item->item_selling_price * $item->item_tax)/100) + $item->item_selling_price ;
//            $selling_price = number_format($selling_price,2);
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,atob("<?= base64_encode($item->item_code) ?>"),atob("<?= base64_encode($item->item_desc) ?>"),"{{htmlentities($item->item_tax)}}","{{$item->item_selling_price}}","{{htmlentities($item->item_mrp)}}","{{htmlentities($item->purchase_rate)}}")' >
                <?php echo $item->item_desc; ?>
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

elseif (isset($itemCodeStockAdjustment)) {   // print_r($itemCodeStockAdjustment); exit;
    if(!empty($itemCodeStockAdjustment)){
        foreach ($itemCodeStockAdjustment as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->chemical_name)}}","{{htmlentities($item->item_code)}}","<?php echo base64_encode($item->item_desc);?>","{{htmlentities($item->is_fractional)}}")'>
                    <?php echo htmlentities($item->item_desc); ?>
                    @if(isset($item->chemical_name))[ Chemical Name:  {{$item->chemical_name}} ]@endif
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

elseif (isset($itemCodeStockAdjustmentNew)) {   // print_r($itemCodeStockAdjustment); exit;
    if(!empty($itemCodeStockAdjustmentNew)){
        foreach ($itemCodeStockAdjustmentNew as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{$item->item_code}}","<?php echo base64_encode($item->item_desc);?>","{{$item->is_fractional}}")'>
                    <?php echo htmlentities($item->item_desc); ?>
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

elseif (isset($batchCodeStockAdjustment)) {    			
    if(!empty($batchCodeStockAdjustment)){
        foreach ($batchCodeStockAdjustment as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillBatchValues(this,"{{htmlentities($item->batch_no)}}","{{htmlentities($item->stock)}}")'>
                    {!! $item->batch_no  . '     [ Stock :   '.$item->stock.' ]'; !!}
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

elseif (isset($batchCodePharmacyReturn)) {    			
    if(!empty($batchCodePharmacyReturn)){
        foreach ($batchCodePharmacyReturn as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; " 
                onclick='fillBatchValues(this,"{{htmlentities($item->batch_no)}}","{{htmlentities($item->stock)}}","{{htmlentities($item->rack)}}",
                "{{htmlentities(number_format($item->selling_price,2))}}","{{htmlentities(number_format($item->mrp,2))}}")'>
                    {!! $item->batch_no  . ' [ Stock :   '.$item->stock.' ]'; !!}
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

elseif (isset($batchCodeApproveTransfer)) {    			
    if(!empty($batchCodeApproveTransfer)){
        foreach ($batchCodeApproveTransfer as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillBatchValues(this,"{{htmlentities($item->batch_no)}}")'>
                    {!! $item->batch_no  or '' !!}
            </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}



/* For Item details search for purchase order */
elseif (isset($itemCodeDetails)) {    //print_r($itemCodePODetails); exit;
    if(!empty($itemCodeDetails)){
        foreach ($itemCodeDetails as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}")'>{{trim(htmlentities($item->item_desc))}} [ Item Code:{{trim(htmlentities($item->item_code))}} ]
            </li>
    <?php  }
    } else {
    	echo 'No Results Found';
    }
}

/* For Item details search for pharmacy Return */
elseif (isset($itemCodePharmacyReturn)) {    //print_r($itemCodePODetails); exit;
    if(!empty($itemCodePharmacyReturn)){
        foreach ($itemCodePharmacyReturn as $item) { ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->chemical_name)}}")'>
                {{trim(htmlentities($item->item_desc))}} [ Item Code:{{trim(htmlentities($item->item_code))}} ]
            </li>
    <?php  }
    } else {
    	echo 'No Results Found';
    }
}

?>