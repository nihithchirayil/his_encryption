<style>
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 25px 0px 0px 0px;
        overflow-y: auto;
        width: auto;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }

    .liHover{
        background: #cde3eb;
    }
    .box_border {
        padding-top: 25px;
        padding-bottom: 10px;
        border: 1px solid #ccc;
        background: #f3f3f3;
    }
</style>

<section class="content" style="margin-top:15px;">
    <div name="printDepartIssue" id="printDepartIssue">
        <table  border="1" width="100%" style="font-size: 12px; border-collapse: collapse;"
                class="table  table-bordered table-condensed table_sm">           
            <thead>
                <tr>
                    <td colspan="28" style="padding: 0px;">
                        <table style="width: 91%; font-size: 12px;">      
                            @include('inpatient::bills.partials.ExtensionsValley.report_header')                                                                
                        </table>
                    </td>
                </tr>
            </thead>           
           
            <tr class="headergroupbg font-normal">
                <th class="newtborder" align="left" style="background-color: #e6e7e7;padding-left:10px; margin:0;">S#</th>
                <th class="newtborder" align="left" style="background-color: #e6e7e7;padding-left:10px; margin:0;">Name</th>
                <th class="newtborder" align="left" style="background-color: #e6e7e7;padding-left:10px; margin:0;">Description</th> 
                <th class="newtborder" align="left" style="background-color: #e6e7e7;padding-left:10px; margin:0;">User</th> 
                <th class="newtborder" align="left" style="background-color: #e6e7e7;padding-left:10px; margin:0;">Status</th> 
            </tr> 
               <tbody>
                <?php
                ?>
                @if(sizeof($itemprint))
                 <?php $i = 1;?>
                    @foreach($itemprint as $item)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->created_by}}</td>
                            <td @if($item->status == 1) onclick="getData('{{$item->id}}')" @endif >
                            <?php    
                            if($item->status == 1)
                            {   echo 'Active'; }
                            elseif($item->status == 0)
                            {    echo 'Created';  }
                            else 
                            {    echo 'Rejected'; }
                            ?>
                            </td>                            
                        </tr>
                         <?php $i++; ?>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" align="center">No Data Found!</td>
                    </tr>
                @endif
                </tbody>
          
        </table>
    </div>
</section>


