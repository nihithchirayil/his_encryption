<?php
use ExtensionsValley\core\CommonController;
/* For Vendor details search */
if (isset($vendorCodeDetails) ) {
    if(!empty($vendorCodeDetails)){

        foreach ($vendorCodeDetails as $vendor) {
            ?>

            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillVendorValues(this,"{{htmlentities(ucfirst($vendor->vendor_name))}}","{{htmlentities(ucfirst($vendor->vendor_code))}}")'>
                    <?php echo htmlentities(ucfirst($vendor->vendor_name)) . '     [   ' . htmlentities(ucfirst($vendor->vendor_code)) . '   ]'; ?>
            </li>

            <?php
        }
    } else {
        echo 'No Results Found';
    }
}

/* For Location details search */
if (isset($locationCodeDetails)) {
    if(!empty($locationCodeDetails)){
        foreach ($locationCodeDetails as $location) {
            ?>

            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLocationValues(this,"{{htmlentities(ucfirst($location->location_name))}}","{{htmlentities(ucfirst($location->location_code))}}")'>
                    <?php echo htmlentities(ucfirst($location->location_name)) . '     [   ' . htmlentities(ucfirst($location->location_code)) . '   ]'; ?>
            </li>

            <?php
        }
    } else {
        echo 'No Results Found';
    }
}


/* For Item details search for purchase order */
if (isset($itemCodePODetails) ) {    //print_r($itemCodePODetails); exit;

    if(!empty($itemCodePODetails)){
        foreach ($itemCodePODetails as $item) { ?>

            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->uom_code)}}","{{htmlentities($item->cost)}}","{{htmlentities($item->tax_rate)}}","{{htmlentities($item->mrp)}}")'>
                    <?php echo htmlentities($item->item_desc) . '     [   ' . htmlentities($item->item_code) . '   ]'; ?>
            </li>

        <?php  }
    } else {
        echo 'No Results Found';
    }
}

/* Item details search for billing */
if (isset($itemCodeForBillingDetails)) {

    if(!empty($itemCodeForBillingDetails)){
        foreach ($itemCodeForBillingDetails as $item) { ?>

    <li style="display: block; padding:5px 10px 5px 5px; "
        onclick='fillItemValues(this,"{{htmlentities($item->chemical_name)}}", "{{htmlentities($item->batch_no)}}",
       "{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->selling_price)}}")'>
        <?php echo htmlentities($item->item_desc) . '     [ chemical Name :  ' . htmlentities($item->chemical_name) . '   ]'. '     [ item Code:  ' . htmlentities($item->item_code) . '   ]';  ?>
    </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
}

/* Service List details search for billing */
if (isset($serviceForBillingDetails)) {

    if(!empty($serviceForBillingDetails)){
        foreach ($serviceForBillingDetails as $item) { ?>

    <li style="display: block; padding:5px 10px 5px 5px; "
        onclick='fillItemValues(this,"{{htmlentities($item->id)}}","{{htmlentities($item->name)}}","{{$package}}")'>
        <?php echo htmlentities($item->name) ; ?>
    </li>
    <?php  }
    } else {
        echo 'No Results Found';
    }
} ?>
