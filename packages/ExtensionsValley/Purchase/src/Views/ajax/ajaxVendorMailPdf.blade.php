<?php
$count = 1;
$total_rate = 0;
$Item_value = 0;
?>
<html>
<head>
</head>
<body>
<br>
<?php
$hospital_details = \DB::table('company')->first();
$vendor_details = \DB::table('vendor')->Where('vendor_code', $po_head_details->vendor_code)->first();
$location_details = \DB::table('location')->Where('location_code', 'ST001')->first();
?>
<div>
    <table width="100%" style="border-collapse: collapse; margin: auto;">
        <tr>
            <td>
                <center><h4>{{$hospital_details->name}}</h4></center>
            </td>
        </tr>
        <tr>
            <td>
                <center>Address:{{$hospital_details->address}}, {{$hospital_details->city}}</center>
            </td>
        </tr> <tr>
            <td>
                <center>Phone:{{$hospital_details->phone}}</center>
            </td>
        </tr>
        <tr>
            <td>
                <center><b style="color: red;">GST ID No : {{$hospital_details->gst_no}}</b></center>
            </td>
        </tr>
        <tr></tr>
    </table>
</div>

<hr><h3 style="text-align: center">Purchase Order</h3>
<center>Purchase Order No : {{$po_head_details->po_no}}</center>
<center>To Vendor : {{$vendor_details->vendor_name}},{{$vendor_details->address_1}}
    ,{{$vendor_details->address_2}}</center>
<br>
<table width="100%" border="1" style="border-collapse: collapse; margin: auto; font-size: 14px;" cellspacing="0" cellpadding="0">
  <tr>
    <td rowspan="2" style="font-weight: bold;">Sl</td>
    <td rowspan="2" style="font-weight: bold;" width="25%">Item</td>
    <td rowspan="2" style="font-weight: bold;">Purchase. Qty.</td>
    <td rowspan="2" style="font-weight: bold;">Free. Qty</td>
    <td rowspan="2" style="font-weight: bold;">Purchase Rate</td>
    <td rowspan="2" style="font-weight: bold;">Discount (Rs)</td>
    <td rowspan="2" style="font-weight: bold;">T.Amount(Rs)</td>
    <td colspan="4" style="font-weight: bold;">CGST / SGST</td>
    <td rowspan="2" style="font-weight: bold;">IGST</td>
    <td rowspan="2" style="font-weight: bold;">Other Charge</td>
  </tr>
  <tr>
    <td>5% </td>
    <td>10%</td>
    <td>18%</td>
    <td>26%</td>
  </tr>
  @foreach($po_items as $item)
        <tr>
            <td>{{$count++}}</td>
            <td>{{$item->item_desc}}</td>
            <td>{{$item->pur_qnty}}</td>
            <td>{{$item->free_qty}}</td>
            <td>{{$item->pur_rate}}</td>
            <td>{{$item->total_disc_amount}}</td>
            <td>{{$item->total}}</td>
            <td colspan="4">@if($item->sgst_value){{$item->sgst_value}}@else{{$item->sgst_value}}}@endif</td>
            <td>{{$item->igst_value}}</td>
            <td>{{$item->total_other_charge}}</td>
            <?php $net_amount = $item->net_total ?>
        </tr>
  @endforeach
  <tr>
      <td colspan="12" style="text-align: right">Net Amount</td>
      <td>{{$net_amount}}</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" valign="middle"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" valign="middle"></td>
  </tr>
</table>
<table width="100%" cellpadding="4" cellspacing="4" border="0" style="border-collapse: collapse; margin-top: 20px;">
    <tbody>

    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <!-- <tr>
        <td style="text-align: left;"><?php echo \DB::table('users')->where('id',$po_head_details->created_by)->value('name'); ?></td>
        <td>&nbsp;</td>
    </tr> -->
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    
        <tr>
            <td colspan="2" style="font-style: italic;">
                Note : Credit note will issued against expiry or damage goods.
                Goods will be delivered with in _______ days delivered period.
                You are requested to delivered b/w 8 to 4 pm in work days..
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>