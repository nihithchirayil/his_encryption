
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
  <style type="text/css">
    @page { margin: 0; }
  body { margin: 15px; }
  </style>
</head>
<body>
<?php
$count = 1;
$total_rate = 0;
$Item_value = 0;
?>
<html>
<head>
<style>
   
</style>
</head>
<body>
<br>
<?php
$hospital_details = \DB::table('company')->first();


?>

    <table border="1" width="100%" style="font-size: 12px; border-collapse: collapse;" class="table table-striped  table-condensed  sorted_table">
        <thead>
            <tr class="header">

                <th colspan="14" style="padding: 0px;">
                <table style="width: 100%; font-size: 12px;">      
                @include('inpatient::bills.partials.ExtensionsValley.report_header')                                                           
                </table>
                </th>
            </tr>
            <tr class="header">
                <th colspan="14" align="center"> 
                     <strong> {{$title}}</strong>
                </th>
            </tr>
            @if(isset($search_values['req_no']) && $search_values['req_no'] !='')
            <tr class="header">
                <th colspan="14" align="center"> 
                Return Number : @if(isset($search_values['req_no'])) {{$search_values['req_no']}} @endif
                </th>
            </tr>
            @endif
            @if(isset($search_values['from_date']) && $search_values['from_date'] !='' || isset($search_values['from_date']) && $search_values['from_date'] !='')
            <tr class="header">
                <th colspan="14" align="center"> 
                    <b> @if(isset($search_values['from_date'])&& $search_values['from_date'] !='') {{'From Date : '.$search_values['from_date']}} @endif 
                       @if(isset($search_values['to_date']) && $search_values['to_date'] !='') {{ 'To Date : '.$search_values['to_date']}}@endif
                    </b>
                </th>
            </tr>
            @endif
            @if(isset($search_values['to_location']) && $search_values['to_location'] !='')
            <tr class="header">
                <th colspan="14" align="center"> 
                <?php $location_name  = DB::table('location')->where('location_code', $search_values['to_location'])->pluck('location_name'); ?> 
                    <b> Return Store : @if(isset($location_name)) {{$location_name}} @endif</b>
                </th>
            </tr>
            @endif
            @if(isset($search_values['vendor_list']) && $search_values['vendor_list'] !='')
            <tr class="header">
                <th colspan="14" align="center">
                <?php $vendor_name  = DB::table('vendor')->where('vendor_code', $search_values['vendor_list'])->pluck('vendor_name'); ?> 
                Vendor Name : @if(isset($vendor_name)) {{$vendor_name}} @endif
                </th>
            </tr>
            @endif
            @if(isset($search_values['show_approved']) && $search_values['show_approved'] !='')
            <tr class="header">
                <th colspan="14" align="center"> 
                Status : @if($search_values['show_approved']==1) {{'Approved'}} 
                         @elseif($search_values['show_approved']==2) {{'Credit & Approved'}}
                         @elseif($search_values['show_approved']==3) {{'Close'}}
                         @elseif($search_values['show_approved']==4) {{'Not Approved'}}@endif 
                </th>
            </tr>
            @endif
       </thead>

        <tbody>
            <tr>
            <th>Return No</th>
            <th>Date</th>
            <th>Location</th>
            <th>Vendor</th>
            <th>Status</th>
            </tr>
            <?php
            //print_r($search_values);die
            ?>
             @foreach($res as $item)
                <tr>
                    <td>{{$item->purchase_return_no}}</td>
                    <td>{{$item->date}}</td>
                    <td>{{$item->location_name}}</td>
                    <td>{{$item->vendor_name}}</td>
                    <td>{{$item->status}}</td>
                </tr>
            @endforeach
          
        </tbody>
    </table>



    </body>

</html>
</body>
</html>