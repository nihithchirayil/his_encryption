<?php
namespace ExtensionsValley\Purchase\Events;


\Event::listen('admin.menu.groups', function ($collection) {

    $collection->put('extensionsvalley.purchase', [
        'menu_text' => 'Purchase'
        , 'menu_icon' => '<i class="fa fa-cubes"></i>'
        , 'acl_key' => 'extensionsvalley.purchase.purchases'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/purchase/listStockRequests'
                , 'menu_text' => 'Stock Requisition'
                , 'acl_key' => 'extensionsvalley.purchase.listStockRequests'
            ],
            '1' => [
                'link' => url('/').'/purchase/issue_requisition_list/1'
                , 'menu_text' => 'Issue Requisition'
                , 'acl_key' => 'extensionsvalley.purchase.issue_requisition_list'
            ],
            '2' => [
                'link' => url('/').'/purchase/issue_requisition_list/2'
                , 'menu_text' => 'Receive Stock'
                , 'acl_key' => 'extensionsvalley.purchase.issue_requisition_list'
            ],
            '3' => [
                'link' => url('/').'/purchase/listDepartmentRequisition'
                , 'menu_text' => 'Departmental Issue'
                , 'acl_key' => 'extensionsvalley.purchase.listDeptStockRequests'
            ],
            '4' => [
                'link' => url('/').'/purchase/listStockReturns'
                , 'menu_text' => 'Stock Return'
                , 'acl_key' => 'extensionsvalley.purchase.listStockReturns'
            ],
            '5' => [
                'link' => url('/').'/purchase/listStockAdjustment'
                , 'menu_text' => 'Stock Adjustment'
                , 'acl_key' => 'extensionsvalley.purchase.listStockAdjustment'
            ],
            '6' => [
                'link' => url('/').'/purchase/listStockReturnReceive'
                , 'menu_text' => 'Receive Stock Return'
                , 'acl_key' => 'extensionsvalley.purchase.listStockReturnReceive'
            ],
            '7' => [
                'link' => url('/').'/purchase/listIntransitStock'
                , 'menu_text' => 'Revert Intransit Stock'
                , 'acl_key' => 'extensionsvalley.purchase.listIntransitStock'
            ],
        ],
    ]);


});
