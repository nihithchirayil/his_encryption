<?php
namespace ExtensionsValley\Master\Validators;

class CategoryValidation
{

    public function getRules()
    {
        return [
            'name' => 'required|max:255',
            'category_code' => 'required|max:50|unique:category',
           // 'item_type' => 'required',
            'status' => 'required',
        ];
    }

    public function getUpdateRules($category)
    {
        return [
            'name' => 'required|max:255',
            'category_code' => 'required|max:50|unique:category,category_code,' . $category->id,
          //  'item_type' => 'required',
            'status' => 'required',
            
        ];
    }

    public function getSubcategoryRules()
    {
        return [
            'name' => 'required|max:255',
            'category_code' => 'required|max:50',
            'subcategory_code' => 'required|max:50|unique:subcategory',
           // 'item_type' => 'required',
            'status' => 'required',
        ];
    }

    public function getSubcategoryUpdateRules($subcategory)
    {
        return [
            'name' => 'required|max:255',
            'subcategory_code' => 'required|max:50|unique:subcategory,subcategory_code,' . $subcategory->id,
          //  'item_type' => 'required',
            'status' => 'required',
        ];
    }

}
