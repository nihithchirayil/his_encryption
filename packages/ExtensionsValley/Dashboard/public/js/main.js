

   $(document).on('click', '.sign_in_btn', function() {
        $( ".sign_btn_list li:first-child" ).trigger( "click" );
   });
   $(document).on('click', '.sign_up_btn', function() {
        $( ".sign_btn_list li:last-child" ).trigger( "click" );
   });

$(document).on('click', '.top_sec_three_btn li', function() {
	var disset = $(this).attr("id");
	$('.top_sec_three_btn li').removeClass("active");
	$(this).addClass("active");
	$(this).closest('.booking_status_wrapper').find(".booking_status_div").css("display", "none");
	$(this).closest('.booking_status_wrapper').find("div#" + disset).css("display", "block");
});

$(document).on('click', '.sign_btn_list li', function() {
    var disset = $(this).attr("id");
    $('.sign_btn_list li').removeClass("active");
    $(this).addClass("active");
    $(this).closest('.right_sign_box').find(".signin_content").css("display", "none");
    $(this).closest('.right_sign_box').find("div#" + disset).css("display", "block");
});

$(document).on('click', '.search_tab_btns li', function() {
    var disset = $(this).attr("id");
    $('.search_tab_btns li').removeClass("active");
    $(this).addClass("active");
    $(this).closest('.left_adv_search_box').find(".search_tab_content").css("display", "none");
    $(this).closest('.left_adv_search_box').find("div#" + disset).css("display", "block");
});

$(document).on('click', '.ft_btns li', function() {
	var disset = $(this).attr("id");
	$('.ft_btns li').removeClass("active");
	$(this).addClass("active");
	$(this).closest('.appfeature_block').find(".ft_tab_content").css("display", "none");
	$(this).closest('.appfeature_block').find("div#" + disset).css("display", "block");
});


$('.home_banner').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        autoplaySpeed: 3000,
        arrows: false,
        fade: true,
        pauseOnHover: false,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1
        // responsive: [
        // {
        // 	breakpoint: 1024,
        // 	settings: {
        // 		slidesToShow: 3,
        // 		slidesToScroll: 3,
        // 		infinite: true,
        // 		dots: true
        // 	}
        // },
        // {
        // 	breakpoint: 600,
        // 	settings: {
        // 		slidesToShow: 2,
        // 		slidesToScroll: 2
        // 	}
        // },
        // {
        // 	breakpoint: 480,
        // 	settings: {
        // 		slidesToShow: 1,
        // 		slidesToScroll: 1
        // 	}
        // }

        // ]
    });

    $('.bottom_slider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        autoplaySpeed: 3000,
        arrows: true,
        pauseOnHover: false,
        variableWidth: true,
        autoplay: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
        {
        	breakpoint: 1024,
        	settings: {
        		slidesToShow: 3,
        		slidesToScroll: 3,
        		infinite: true,
        		dots: false
        	}
        },
        {
        	breakpoint: 600,
        	settings: {
        		slidesToShow: 2,
        		slidesToScroll: 2
        	}
        },
        {
        	breakpoint: 480,
        	settings: {
        		slidesToShow: 1,
        		slidesToScroll: 1
        	}
        }

        ]
    });


