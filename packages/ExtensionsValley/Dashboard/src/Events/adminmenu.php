<?php
namespace ExtensionsValley\Dashboard\Events;


\Event::listen('admin.menu.groups', function ($collection) {

    $collection->put('extensionsvalley.users', [
        'menu_text' => 'User Panel'
        , 'menu_icon' => '<i class="fa fa-user"></i>'
        , 'acl_key' => 'extensionsvalley.admin.userpanel'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/admin/listUsers'
                , 'menu_text' => 'Manage Users'
                , 'acl_key' => 'extensionsvalley.admin.adduser'
            ],
            '1' => [
                'link' => url('/').'/admin/listGroups'
                , 'menu_text' => 'Manage Groups'
                , 'acl_key' => 'extensionsvalley.admin.groups'
            ],
            '2' => [
                'link' => url('/').'/admin/list_user_group'
                , 'menu_text' => 'Assign User Groups'
                , 'acl_key' => 'extensionsvalley.admin.usergroup'
            ]
        ],
    ]);

     $collection->put('extensionsvalley.admin', [
        'menu_text' => 'Admin'
        , 'menu_icon' => '<i class="fa fa-user"></i>'
        , 'acl_key' => 'extensionsvalley.admin.adminpanel'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/purchase/listPendingBills'
                , 'menu_text' => 'Pending Bill List'
                , 'acl_key' => 'extensionsvalley.purchase.listPendingBills'
            ],
            '1' => [
                'link' => url('/').'/purchase/cancelBills'
                , 'menu_text' => 'Cancel Bill'
                , 'acl_key' => 'extensionsvalley.purchase.cancelBills'
            ],
        ],
    ]);


    $collection->put('extensionsvalley.nursing', [
        'menu_text' => 'Nursing'
        , 'menu_icon' => '<i class="fa fa-heartbeat"></i>'
        , 'acl_key' => 'extensionsvalley.admin.nursepanel'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/nursing/MedicationChartReport'
                , 'menu_text' => 'Medication Chart Report'
                , 'acl_key' => 'extensionsvalley.nursing.MedicationChartReport'
            ],

        ],
    ]);
    $collection->put('extensionsvalley.report', [
        'menu_text' => 'Report'
        , 'menu_icon' => '<i class="fa fa-list"></i>'
        , 'acl_key' => 'extensionsvalley'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/report/cashierCollectionReport'
                , 'menu_text' => 'Cashier Collection Report'
                , 'acl_key' => 'extensionsvalley.report.cashierCollectionReport'
            ],

        ],
    ]);


    $collection->put('extensionsvalley.forms', [
        'menu_text' => 'Form Builder'
        , 'menu_icon' => '<i class="fa fa-folder"></i>'
        , 'acl_key' => 'extensionsvalley.forms.categorylistview'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/forms/category-list'
                , 'menu_text' => 'Form Category'
                , 'acl_key' => 'extensionsvalley.forms.categorylistview'
            ],
            '1' => [
                'link' => url('/').'/forms/form-fields-list'
                , 'menu_text' => 'Form Fields'
                , 'acl_key' => 'extensionsvalley.forms.formfieldslist'
            ],
            '2' => [
                'link' => url('/').'/forms/forms-list'
                , 'menu_text' => 'Forms List'
                , 'acl_key' => 'extensionsvalley.forms.formslistview'
            ]
        ],
    ]);


    $collection->put('extensionsvalley.frequency', [
        'menu_text' => 'Frequency'
        , 'menu_icon' => '<i class="fa fa-folder"></i>'
        , 'acl_key' => 'extensionsvalley.frequency.addfrequency'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/').'/frequency/add_frequency'
                , 'menu_text' => 'Manage Frequency'
                , 'acl_key' => 'extensionsvalley.frequency.addfrequency'
            ],
           
           
        ],
    ]);

});
