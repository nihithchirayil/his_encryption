@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
   
      <!-- Custom Theme Scripts -->
      <link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

    
       <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">



@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <input type="hidden" name="edit_id" id="edit_id" value=''>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-9 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height:70px">
                        <div class="col-md-12 padding_sm">
                            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">

                            <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                    <div class="mate-input-box">
                                        <label for="">Generic Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="generic_name" autocomplete="off" id="generic_name" class="form-control" onkeyup="genericNameSearch(this,event)">
                                        <div class="ajaxSearchBox" id="generic_namediv" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                        </div>
                                        <input type="hidden" name="generic_name_id" class="generic_name_id" id="generic_name_id">
                                    </div>
                                   
                            </div>
                            <div class="col-md-1 pull-right  padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searcIntraction()" id="searchBtn"
                                    class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 padding_sm">
                <div class="box no-border">
                    <input type="hidden" name="hidden_id" id="hidden_id" value="">
                    <div class="box-body clearfix" style="height: 630px">
                        <h5 class="blue text-center"><strong id="box_head"> Add Interaction </strong></h5>
                        <hr>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Generic Name 1</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="generic_name1" autocomplete="off" id="generic_name1" class="form-control" onkeyup="genericName1Search(this,event)">
                                    <div class="ajaxSearchBox" id="generic_name1div" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                    </div>
                                    <input type="hidden" name="generic_name_id1" class="generic_name_id1" id="generic_name_id1">
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Generic Name 2</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="generic_name2" autocomplete="off" id="generic_name2" class="form-control" onkeyup="genericName2Search(this,event)">
                                    <div class="ajaxSearchBox" id="generic_name2div" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                    </div>
                                    <input type="hidden" name="generic_name_id2" class="generic_name_id2" id="generic_name_id2">
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px" >
                                <div class="mate-input-box">
                                    <label for="">Severity</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" id="severity_id" name="parent_id">
                                        <option value="">Select Severity</option>
                                        @foreach ($severity as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>

                                        @endforeach



                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm"  id="">
                                <div class="mate-input-box" style="height: 99px !important">
                                    <label for="">Action Recommended</label>
                                    <div class="clearfix"></div>
                                   <textarea class="form-control" name="action_recommender" id="action_recommender" style="resize: none"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm"  id="">
                                <div class="mate-input-box"  style="height: 99px !important">
                                    <label for="">Interaction</label>
                                    <div class="clearfix"></div>
                                   <textarea class="form-control" name="intraction" id="intraction" style="resize: none"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px;">
                                <div class="col-md-4 padding_sm pull-right">
                                    <label for="">&nbsp;</label>
                                    <button type="button" id="savebtnBtn" class="btn btn-block btn-success"
                                        onclick="saveIntractions()"><i id="savebtnSpin" class="fa fa-save"></i> Save
                                    </button>
                                </div>
                                <div class="col-md-4 padding_sm pull-right">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn btn-block btn-info clearFormButton"
                                        onclick="formReset()"><i class="fa fa-times"></i> Clear </button>
                                </div>
                            </div>

                            
                    </div>


                </div>
            </div>
        </div>



    </div>
    </div>
    </div>

@stop
@section('javascript_extra')
 
    <script src="{{asset("packages/extensionsvalley/emr/js/drugintraction.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version='.env('APP_JS_VERSION', '0.0.1')) }}"> </script>
@endsection
