<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var gen_id = $("#generic_name_id").val();
            var token = $('#token_hiddendata').val();
            var param = {
                _token: token,
                gen_id: gen_id,
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {

                    $('#searchBtn').attr('disabled', true);
                    $('#searchSpin').removeClass('fa fa-search');
                    $('#searchSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function() {
                    $('#searchBtn').attr('disabled', false);
                    $('#searchSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchSpin').addClass('fa fa-search');
                },
            });
            return false;
        });

    });
</script>
<?php
$div_height = '515px';
if ($page_links) {
    $div_height = '450px';
}
?>

<div class="theadscroll" style="position: relative; height: <?= $div_height ?>;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">Sl.No.</th>
                <th width="18%">Generic Name1</th>
                <th width="18%">Generic Name2</th>
                <th width="5%">Severity</th>
                <th width="22%">Action Recommended</th>
                <th width="22%">Interaction</th>
                <th width="10%">Action</th>


            </tr>
        </thead>
        <tbody>
            @if (sizeof($search)>0)
            @foreach ($search as $item)

            <tr style="cursor:pointer;" >
                <td class="td_common_numeric_rules">{{($search->currentPage() - 1) * $search->perPage() + $loop->iteration}}</td>
                <td title="{{$item->gn1}}" class="common_td_rules">{{$item->gn1}}</td>
                <td title="{{$item->gn2}}" class="common_td_rules">{{$item->gn2}}</td>
                <td title="{{$item->severity_name}}" class="common_td_rules">{{$item->severity_name}}</td>
                <td title="{{$item->action_recommended}}" class="common_td_rules">{{$item->action_recommended}}</td>
                <td title="{{$item->interaction}}" class="common_td_rules">{{$item->interaction}}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-warning "  onclick="editItem({{$item->id}},{{$item->gn1_id}},'{{$item->gn1}}',{{$item->gn2_id}},'{{$item->gn2}}',{{$item->severity}},'{{base64_encode($item->action_recommended)}}','{{base64_encode($item->interaction)}}');"><i id = "editButton_{{$item->id}}" class="fa fa-edit editButton" title="Edit"></i> </button>
                    <button type="button" class="btn btn-sm btn-danger " onclick="deleteIntraction({{$item->id}});"><i id = "deleteButton_{{$item->id}}" class="fa fa-trash" title="Delete"></i> </button>
                </td>


            </tr>

            @endforeach
            @else
            <tr>
                <td colspan="11" style="text-align: center">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>