<div class="modal fade" id="doctor_documentdataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header" style="background: #f0ad4e; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Documents</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 padding_sm">
                    <form id="saveDoctorDocumentsForm">
                        @php
                            $doc_list = \DB::table('doctor_master')
                                ->where('status', 1)
                                ->orderBy('doctor_name')
                                ->pluck('doctor_name', 'id');
                            $user_id = \Auth::user()->id;
                            $doctor_id = \DB::table('doctor_master')
                                ->where('user_id', $user_id)
                                ->value('id');
                        @endphp
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label class="filter_label">Document Title</label>
                                <input type="text" autocomplete="off" class="form-control" value=""
                                    placeholder="Document Title" name="doctor_document_title"
                                    id="doctor_document_title">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label class="filter_label">Doctor Name</label>
                                {!! Form::select('doctordocument_uploadid', $doc_list, $doctor_id, ['class' => 'form-control custom_floatinput', 'placeholder' => 'Select Doctor', 'title' => 'Select Doctor', 'id' => 'doctordocument_uploadid', 'style' => 'width:100%;color:#555555;']) !!}
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm" style="margin-top: 10px">
                            <div class="fileUpload">
                                <input type="file" class="btn-success" name="upload_document" id="upload_document"
                                    required>
                            </div>
                        </div>
                        <div class="col-md-2 pull-right" style="padding-top: 10px;">
                            <button id="uploadDocumentBtn" class="btn btn-primary btn-block">Upload <i
                                    id="uploadDocumentSpin" class="fa fa-upload"></i></button>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm" id="doctor_documentuploadlist">

                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div id="doctor_document_print_previewmodel" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width: 85%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="doctordocument_modaldata" style="height:550px">

                <iframe src="" width="100%" height="100%" id="doctor_documentdataiframe"></iframe>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>

<!-----------select default doctor modal------------------------------>
<div class="modal fade practice_location_modal" id="practice_location_modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                {{-- <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button> --}}
                <h4 class="modal-title">Select Practice Location</h4>
            </div>
            <div class="modal-body" style="min-height:350px;overflow:hidden;">
                <div class="col-md-12 padding_sm" id="practice_location_modal_data">

                </div>
            </div>
        </div>

    </div>
</div>
<!-----------select default doctor modal ends------------------------->



<input type="hidden" id="base_doumenturl" value="{{ URL::to('/') }}">
<li role="presentation" class="dropdown" style="padding-top: 10px;margin-left: 10px;">
    @php
        $user_id = \Auth::user()->id;
        $status = 0;
        $sql = "select g.name from user_group ug join groups g on g.id=ug.group_id where ug.user_id=$user_id";
        $res_users = \DB::select($sql);
        if (count($res_users) != 0) {
            foreach ($res_users as $each) {
                if ($each->name == 'SUPER ADMIN') {
                    $status = 1;
                } elseif ($each->name == 'DOCTOR') {
                    $status = 1;
                }
            }
        }
    @endphp
    <?php
    if($status == 1){
        ?>
    <button type="button" onclick="doctorDocuments()" class="btn btn-warning" id="doctor_documentsBtn"><i
            id="doctor_documentSpin" class="fa fa-file"></i> Documents </button>
    <?php } ?>
</li>
<!--
<li role="presentation" class="dropdown">



    <a href="#" onclick="showAllNotifications();" style="width:125px;" class="dropdown-toggle info-number"
        data-toggle="dropdown" aria-expanded="false">{{ $title }}

        <span id="notification_count" class="badge bg-blue">0</span>
    </a>
</li> -->

@php
$user_id = \Auth::user()->id;
$dr_status = 0;
$sql = "select g.name from user_group ug join groups g on g.id=ug.group_id where ug.user_id=$user_id and ug.status = 1 and g.status = 1 and g.deleted_at is null and g.name = 'DOCTOR'";
$res_users = \DB::select($sql);
if (count($res_users) > 0) {
    $dr_status = 1;
    $show_queue_management = 0;
    if (\Schema::hasColumn('doctor_master', 'show_queue_management')) {
        $show_queue_management = \DB::table('doctor_master')->where('user_id', $user_id)->value('show_queue_management');
    }
}
@endphp
@if (env('PRACTICELOCATION_ENABLED', 0) == 1)

    <li role="presentation" class="dropdown change_default_practice_btn"
        style="margin-top: 10px; margin-right: 15px; cursor:pointer;">
        <h5 style="float:left;">Location : <b><span style="color:darkturquoise;" id="current_practice_location">Daya
                    Hospital </span></b></h5>
        <i class="fa fa-map-marker" style="font-size: 25px; color: #6db5f5;"></i>
    </li>
@endif
@if (env('QUEUE_MANAGEMENT_ENABLED', '') == 'TRUE' && $dr_status == 1)
    @if($show_queue_management == '1')
    <li role="presentation" class="dropdown" style="margin-top: 10px;">
        <span style="color: #000000;">In/Out </span>
        <!-- Rounded switch -->
        <label class="switch">
            <input type="checkbox" name="check_in_box" class="check_in_box">
            <span class="slider round"></span>
        </label>

    </li>
    @endif
    <li role="presentation" class="dropdown change_default_dr_btn"
        style="margin-top: 10px; margin-right: 15px; cursor:pointer;">
        <i class="fa fa-user-md" style="font-size: 25px; color: #6db5f5;"></i>
    </li>
@endif
@if (env('CLINIC_MODE', 0) == 1)
    <li role="presentation" class="dropdown change_default_location_btn"
        style="margin-top: 10px; margin-right: 15px; cursor:pointer;">
        <h5 style="float:left;">Location : <b><span id="current_location" class="green">Select Location</span></b></h5> &nbsp;&nbsp; 
        <i class="fa fa-sitemap" style="font-size: 25px; color: #6db5f5;"></i>
    </li>
@endif

<!--
<li role="presentation" class="dropdown queue_management_div" style="margin-right: 15px;width: 482px; display:none;">
    <div style="padding:5px;border: 1px solid #e74c3c;font-size: 21px;width: auto;float: left; text-align:center; min-width:35px;">
        <span style="color: #e74c3c; font-weight: 600;" class="current_token blink_me">
            ---
        </span>
    </div>
    <div style="padding:5px;float: left;font-size: 11px;margin-top: 5px;margin-left: 15px;">
        <button type="button" class="next_general_token btn btn-sm btn-primary"><i class="fa fa-forward"></i> Next Token</button>
    </div>
    <div style="padding:5px;float: left;font-size: 11px;margin-top: 5px;">
        <button type="button" class="next_special_token btn btn-primary"><i class="fa fa-forward"></i> Next Special Token</button>
    </div>
    <div style="padding:5px;float: left;font-size: 11px;margin-top: 5px;">
        <button type="button" class="recall_token btn btn-primary btn-warning"><i class="fa fa-refresh"></i> Recall</button>
    </div>
    <div style="padding:5px;float: left;font-size: 11px;margin-top: 5px;">
        <button type="button" class="emergency_btn btn btn-primary btn-danger"><i class="fa fa-bell"></i> Emergency</button>
    </div>
</li> -->

<ul id="notification_contianer" class="dropdown-menu list-unstyled msg_list" role="menu" style="width:450px;">
    <script>
        $(document).ready(function() {

            @if (env('QUEUE_MANAGEMENT_ENABLED', '') == 'TRUE' && $dr_status == 1)
            $(".change_default_dr_btn").on("click", function() {
                showGroupedDoctorsList();
            })
            @endif
            $(".change_default_practice_btn").on("click", function() {
                selectPracticeLocation();
            })
            $(".change_default_location_btn").on("click", function() {
                showLocationListModal();
            })


            $("#saveDoctorDocumentsForm").on('submit', function(e) {
                e.preventDefault();
                var image_vai = dr_image_Validate('upload_document');
                if (image_vai) {
                    var url = $('#base_doumenturl').val() + "/emr/saveDoctorDocumentsView";
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $('#uploadDocumentBtn').attr('disabled', true);
                            $('#uploadDocumentSpin').removeClass("fa fa-upload");
                            $('#uploadDocumentSpin').addClass("fa fa-spinner fa-spin");
                        },
                        success: function(data) {
                            if (data) {
                                toastr.success("Successfully Added");
                                document.getElementById('saveDoctorDocumentsForm').reset();
                                doctorDocuments(1);
                            }
                        },
                        complete: function() {
                            $('#uploadDocumentBtn').attr('disabled', false);
                            $('#uploadDocumentSpin').removeClass("fa fa-spinner fa-spin");
                            $('#uploadDocumentSpin').addClass("fa fa-upload");
                        },
                        error: function() {
                            toastr.error(
                                "Error Please Check Your Internet Connection and Try Again"
                            );
                        }
                    });
                } else {
                    bootbox.alert(
                        '<strong>File criteria.</strong><br>Extensions: jpeg,jpg,png,gif,Pdf,Docx,Doc<br>Size : Maximum 5 MB'
                    );
                }

            });
        });



        function dr_image_Validate(input_id, status) {
            var fileInput = document.getElementById(input_id);
            var filePath = fileInput.value;
            if (filePath || status == '1') {
                var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.pdf|\.docx|\.doc|\.xls|\.xlsx)$/i;
                var file_size = fileInput.size;
                if (!allowedExtensions.exec(filePath) || file_size > 5150) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }

        }

        function doctorDocuments(fromtype = 0) {
            var url = $('#base_doumenturl').val() + "/emr/DoctorDocumentsView";
            $.ajax({
                url: url,
                type: "GET",
                data: "notification=1",
                beforeSend: function() {
                    $('#doctor_documentsBtn').attr('disabled', true);
                    $('#doctor_documentSpin').removeClass('fa fa-file');
                    $('#doctor_documentSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $("#doctor_documentuploadlist").html(data);
                    if (fromtype != '1') {
                        $("#doctor_documentdataModal").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                },
                complete: function() {
                    $('#doctor_documentsBtn').attr('disabled', false);
                    $('#doctor_documentSpin').removeClass('fa fa-spinner fa-spin');
                    $('#doctor_documentSpin').addClass('fa fa-file');
                }
            });
        }

        function docDocumentDataPreview(docID) {
            var url = $('#base_doumenturl').val() + "/emr/previewDoctorDocument";
            var id = docID;
            $.ajax({
                type: "GET",
                url: url,
                data: "id=" + id,
                beforeSend: function() {
                    $('#doctordocument_modaldata').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#print_doctor_documentbtn' + docID).attr('disabled', true);
                    $('#print_doctor_documentspin' + docID).removeClass('fa fa-print');
                    $('#print_doctor_documentspin' + docID).addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#doctor_document_print_previewmodel').modal('show');

                    $('#doctor_documentdataiframe').attr("src", data);
                },
                complete: function() {
                    $('#doctordocument_modaldata').LoadingOverlay("hide");
                    $('#print_doctor_documentbtn' + docID).attr('disabled', false);
                    $('#print_doctor_documentspin' + docID).removeClass('fa fa-spinner fa-spin');
                    $('#print_doctor_documentspin' + docID).addClass('fa fa-print');
                }
            });
        }

        function docDocumentDataDelete(docID) {
            var message_show = 'Are you sure you want to delete this Document';
            bootbox.confirm({
                message: message_show,
                buttons: {
                    'confirm': {
                        label: 'Delete',
                        className: 'btn-danger',
                        default: 'true'
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn-success'
                    }
                },
                callback: function(result) {
                    if (result) {
                        var url = $('#base_doumenturl').val() + "/emr/DeleteDoctorDocument";
                        var id = docID;
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: "id=" + id,
                            beforeSend: function() {
                                $('#delete_doctor_documentbtn' + docID).attr('disabled', true);
                                $('#print_doctor_documentspin' + docID).removeClass('fa fa-trash');
                                $('#print_doctor_documentspin' + docID).addClass(
                                    'fa fa-spinner fa-spin');
                            },
                            success: function(data) {
                                if (data == '1') {
                                    toastr.success("Successfully Deleted");
                                    doctorDocuments(1);
                                } else {
                                    toastr.error("Error");
                                }
                            },
                            complete: function() {
                                $('#delete_doctor_documentbtn' + docID).attr('disabled', false);
                                $('#print_doctor_documentspin' + docID).removeClass(
                                    'fa fa-spinner fa-spin');
                                $('#print_doctor_documentspin' + docID).addClass('fa fa-trash');
                            }
                        });
                    }
                }
            });

        }
    </script>
