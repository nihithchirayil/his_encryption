<!DOCTYPE html>
<html lang="en">
@php  
$license_expired_status = 0;
$date_difference = 0;
$current_date = date('Y-m-d');
$license_expiry_date = (env("LICENCE_EXPIRY_DATE")) ? base64_decode(env("LICENCE_EXPIRY_DATE")) : '';
if($license_expiry_date != ''){
    $date1=date_create($current_date);
    $date2=date_create($license_expiry_date);
    $diff=date_diff($date1,$date2);
    $date_difference = $diff->format("%a");
    if($current_date > date('Y-m-d', strtotime($license_expiry_date)) ){
        $license_expired_status = 1;
    }
    
}
@endphp

@if($license_expired_status == 1)
<script type="text/javascript">
    localStorage.clear();
    window.location.href = "{{ route('extensionsvalley.admin.logout') }}";
</script>
@else 

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }} | {{ \WebConf::get('site_name') }}</title>
    @if (\WebConf::get('fav_icon') != '')
        <link rel="shortcut icon" href="{{ URL::to('/') }}/{{ \WebConf::get('fav_icon') }}" />
    @endif

    <!-- Bootstrap -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/custom.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/core-admin.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/green.css') }}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/datatables/dataTables.bootstrap.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/datatables/buttons.bootstrap.min.css') }}"
        rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/select2.min.css') }}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap-progressbar-3.3.4.min.css') }}"
        rel="stylesheet">


    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/plugins/font-awesome/css/font-awesome.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/plugins/custom-input-icons/custom-input-icons.css') }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}"
        rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"> -->
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    @yield('css_extra')

    <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        if (localStorage.getItem('loginstatus') == 0) {
            document.location.href = "{{ route('extensionsvalley.admin.login') }}";
        }
    </script>
</head>

<body class="nav-sm">
    <div class="container body">
        <div class="main_container">
            @yield('content-header')
            @yield('content-area')
            <!-- Footer Starts-->
            @include('Dashboard::dashboard.partials.footer')
            <!-- Footer Ends-->
        </div>
    </div>
    <div id="ledger_company_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="width:70%">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <h4 class="modal-title">Ledger Company</h4>
                </div>
                <div class="modal-body" style="height: 130px" id="ledger_company">
                    <?php
                    $ledger_company_list = \DB::table('ledger_company')
                        ->where('status', 1)
                        ->pluck('company', 'id');
                    $ledger_company = ['0' => ' Select Company'] + $ledger_company_list->toArray();
                    ?>
                    {!! Form::select('ledger_company', $ledger_company, '', ['class' => 'form-control', 'title' => 'Company', 'id' => 'ledger_company', 'style' => 'color:#555555;', 'onchange' => 'setLedgerCompany(this)']) !!}
                </div>

            </div>

        </div>
    </div>
    <!-- iCheck -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/icheck.min.js') }}"></script>
    <!-- Datatables -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/responsive.bootstrap.js') }}"></script>
    <!-- select 2 JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/select2.full.min.js') }}"></script>
    <!-- Custom Core JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/core-admin.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/custom.js') }}"></script>

    {{-- new design integrated start --}}
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/momentjs/moment.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}">
    </script>

    <script
        src="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/floathead/jquery.floatThead.js') }}"></script>

    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.locales.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>

    {{-- new design integrated end --}}

    <!-- Tiny Mce Editor JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
            selector: 'textarea.texteditor',
            theme: "modern",
            subfolder: "",
            height: "400",
            relative_urls: false,
            remove_script_host: false,
            convert_urls: true,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor filemanager"
            ],
            image_advtab: true,
            content_css: '{{ asset('packages/extensionsvalley/dashboard/js/tinymce/skins/lightgray/content.min.css') }}',
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
            style_formats: [{
                    title: 'Bold text',
                    inline: 'b'
                },
                {
                    title: 'Red text',
                    inline: 'span',
                    styles: {
                        color: '#ff0000'
                    }
                },
                {
                    title: 'Red header',
                    block: 'h1',
                    styles: {
                        color: '#ff0000'
                    }
                },
                {
                    title: 'Example 1',
                    inline: 'span',
                    classes: 'example1'
                },
                {
                    title: 'Example 2',
                    inline: 'span',
                    classes: 'example2'
                },
                {
                    title: 'Table styles'
                },
                {
                    title: 'Table row 1',
                    selector: 'tr',
                    classes: 'tablerow1'
                }
            ],
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery(".select2").select2();
        });

        function redirectToDashboard() {
            var assesment_draft_mode = $('#assesment_draft_mode').val();
            var investigation_draft_mode = $('#investigation_draft_mode').val();
            var prescription_changes_draft_mode = $('#prescription_changes_draft_mode').val();
            if (assesment_draft_mode == 1) {
                checkAssesmentSaveValidation();
            } else if (investigation_draft_mode == 1) {
                checkInvestigationSaveValidation();
            } else if (prescription_changes_draft_mode == 1) {
                checkPrescriptionSaveValidation();
            } else {
                window.location.href = "{{ route('extensionsvalley.admin.dashboard') }}";
            }
        }

        function redirectToBackBtn(){
            history.back();return false;
        }

        $(window).load(function(){
            var date_difference = '<?php echo $date_difference ?>';
            var license_expiry_date = '<?php echo $license_expiry_date ?>';
            if((!localStorage.getItem('licence_exp_status') || localStorage.getItem('licence_exp_status') == 0 ) && license_expiry_date != ''){
                if(parseInt(date_difference) == 0 ){
                    bootbox.alert('Your licence is going to expired on today.');
                    localStorage.setItem('licence_exp_status', 1);
                } else if(parseInt(date_difference) <= 10 && parseInt(date_difference) != 0 ){
                    bootbox.alert('Your licence is about to expire in '+date_difference+ ' days');
                    localStorage.setItem('licence_exp_status', 1);
                } else {
                    bootbox.alert('Your licence has expired. Please contact S/W team for licence renewal.');
                }
            }

            if(parseInt(date_difference) <= 10  && license_expiry_date != ''){
                $(".licence_warning_indication").show();
            }


            $(document).on('click', '.treeview', function(){ 
                $('body').addClass('nav-md'); 
                $('body').removeClass('nav-sm');  
            });
        })
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            @if (Session::has('accounts_group') && !Session::has('ledger_company_id'))
                $("#ledger_company_modal").modal({backdrop: 'static', keyboard: false});
            @endif
        });

        function setLedgerCompany(e) {
            if ($(e).val() != 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('extensionsvalley.master.set_ledger_company') }}",
                    data: 'set_ledger_company=' + $(e).val(),
                    beforeSend: function() {
                        $("#ledger_company").html(
                            '<li style="width:200px;text-align: center;" id="ledger_company_bdy"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(data) {
                        $("#ledger_company").removeClass('ledger_company_bdy');
                        $("#ledger_company_modal").modal('hide');
                    },
                    complete: function() {}
                });
            }
        }
    </script>
    @yield('javascript_extra')

</body>
@endif
</html>
