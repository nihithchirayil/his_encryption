<?php
use ExtensionsValley\Emr\CommonController;

$user_id = \Auth::user()->id;
$menu_level_one = [];
$menu_level_two = [];
if ($user_id) {
	list($menu_level_one, $menu_level_two) = CommonController::getMenuList($user_id);
}
?>

<div class="theadscroll sidebar_menu_parent_div" style="position: relative; height: 550px; margin-top:20px;">
<aside id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<section class="sidebar ">
		<div class="user-panel">
			<div class="pull-left image hide"></div>
		</div>
		<ul id="nav_side_menu" class="nav side-menu">

			@foreach($menu_level_one as $menu_id=>$menu_item)
			<li class="treeview">
				<?php //dd($menu_item); ?>
				<a @if($menu_item['url']) href="{{$menu_item['url']}}" @elseif($menu_item['route_name']) href="{{route($menu_item['route_name'])}}" @endif> @if(isset($menu_item['class_name'])){!! $menu_item['class_name'] !!}@else{!! '<i class="fa fa-share"></i>' !!}@endif <span>{{$menu_item['child_name']}}</span>
				@if(isset($menu_level_two[$menu_item['child_id']]) && count($menu_level_two[$menu_item['child_id']]))
				<span class="fa fa-chevron-down"></span></a>
				@endif
				</a>
                 <ul class="nav child_menu" style="z-index:99999">
                    @if( isset($menu_level_two[$menu_item['child_id']]) )
                	@foreach($menu_level_two[$menu_item['child_id']] as $level_two_id=>$level_two_item)
                	<li @if(isset($menu_level_two[$level_two_item['child_id']]) && count($menu_level_two[$level_two_item['child_id']])) class="treeview" @endif>
                        <a @if(isset($menu_level_two[$level_two_item['child_id']]) && count($menu_level_two[$level_two_item['child_id']])) @else href="@if(isset($level_two_item['url'])){{url($level_two_item['url'])}} @elseif(isset($level_two_item['route_name']) && Route::has($level_two_item['route_name'])){{route($level_two_item['route_name'])}} @else{{url('#')}} @endif" @if( (Route::currentRouteName()==$level_two_item['route_name'] && $level_two_item['route_name']!='') || (Request::url()==url($level_two_item['url']) && $level_two_item['url']!='') || ($level_two_item['submenu_routes_colln']->search(Route::currentRouteName())!==false) )id="current_menu_item"@endif @endif>{{$level_two_item['child_name'] }}
                        @if(isset($menu_level_two[$level_two_item['child_id']]) && count($menu_level_two[$level_two_item['child_id']]))
                        <span class="fa fa-chevron-down"></span></a>
                        @endif
                        </a>
                        @if(isset($menu_level_two[$level_two_item['child_id']]) && count($menu_level_two[$level_two_item['child_id']]))
                        <ul class="nav child_menu">
		                	@foreach($menu_level_two[$level_two_item['child_id']] as $level_three_id=>$level_three_item)
		                	<li>
		                        <a <?php if($level_three_item['child_name']=='Radiology' || $level_three_item['child_name']=='Rounding List'){ echo "target='_blank'"; } ?> href="@if(isset($level_three_item['url'])){{url($level_three_item['url'])}}@elseif(isset($level_three_item['route_name']) && Route::has($level_three_item['route_name'])){{route($level_three_item['route_name'])}} @else{{url('#')}}@endif" @if( (Route::currentRouteName()==$level_three_item['route_name'] && $level_three_item['route_name']!='') || (Request::url()==url($level_three_item['url']) && url($level_three_item['url']!='')) || $level_three_item['submenu_routes_colln']->search(Route::currentRouteName())!==false )) id="current_menu_item_level3"@endif>{{$level_three_item['child_name'] }}
		                        </a>
		                    </li>
		                	@endforeach
		                </ul>
		                @endif
                    </li>
                	@endforeach
	                @endif
                </ul>
			</li>
			@endforeach

		</ul>
	</section>
</aside>
</div>

	@section('tscripts')
	<script type="text/javascript">
	$(document).ready(function{
	@endsection

	@section('js_scripts_ready_partial1')

        //$('.slimScrollDiv').attr('style', '');
        $('.sidebar').attr('style', '');
        //$('.slimScrollBar').attr('style', '');

        $(window).on('resize', function(){
            //$('.slimScrollDiv').attr('style', '');
            $('.sidebar').attr('style', '');
            //$('.slimScrollBar').attr('style', '');
        });

	if($('#current_menu_item').length) {
		//Menu Display 2nd level
		var menu_obj = $('#current_menu_item');
		menu_obj.closest('ul').closest('li.treeview').find('span').trigger('click');
		menu_obj.closest('li').find('a').trigger('click');

	} else if($('#current_menu_item_level3').length) {
		//Menu Display 3rd level
		var menu_obj = $('#current_menu_item_level3');
		menu_obj.closest('ul').closest('ul').closest('li.treeview').find('span').trigger('click');
		menu_obj.closest('ul').closest('li').find('a').trigger('click');
	}

	if(typeof(menu_obj)!='undefined') {
		setTimeout(function() {
			menu_obj.closest('li').addClass('active');
		}, 1000);
	}


// 	$('#current_menu_item').closest('ul').attr('style', 'display: block;');
// 	$('#current_menu_item').closest('li').addClass('active');
// 	$('#current_menu_item').closest('ul').closest('li.treeview').addClass('active');
// 	$('#current_menu_item').closest('ul').closest('li.treeview').trigger('click');
	//Menu Display 3rd level
// 	$('#current_menu_item_level3').closest('ul').closest('li').addClass('active');
// 	$('#current_menu_item_level3').closest('li').addClass('active');
// 	$('#current_menu_item_level3').closest('ul').closest('ul').attr('style', 'display: block;');
// 	$('#current_menu_item_level3').closest('ul').closest('ul').closest('li.treeview').addClass('active');

// 	$('#current_menu_item_level3').closest('ul').closest('li').addClass('active');
	@stop

	@section('tscripts')
		});
	</script>
	@endsection
