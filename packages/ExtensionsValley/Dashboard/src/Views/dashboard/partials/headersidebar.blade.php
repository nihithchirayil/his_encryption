<?php
$is_menu_color = env('MENU_COLOR');
if (isset($is_menu_color) && $is_menu_color == 1) {
    $is_menu_color_bg = env('MENU_COLOR_BG');
    if(isset($is_menu_color_bg)){
        $background = "background: $is_menu_color_bg !important";
    }else{
        $background = 'background:#416e53 !important';
    }

} else {
    $background = '';
}
$clinic_mode = env('CLINIC_MODE', 0);
$koyili_mode = env('KOYILI_MODE', 0);
?>
@php
$user_id = \Auth::user()->id;
$dr_status = 0;
$sql = "select g.name from user_group ug join groups g on g.id=ug.group_id where ug.user_id=$user_id and ug.status = 1 and g.status = 1 and g.deleted_at is null and g.name in ('DOCTOR','OP ASSISTANT')";
$res_users = \DB::select($sql);
if (count($res_users) > 0) {
    $dr_status = 1;
}
@endphp
<div class="col-md-3 left_col" style="<?= $background ?>">
    <input type="hidden" id="exceller_templatedata"
        value="<?= base64_encode('<html xmlns:o="urn:schemas-microsoft-com:office:office"
        xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">

    <head>
        <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->
    </head>

    <body>
        <table>{table}</table>
    </body>

    </html>') ?>">
    <div class="left_col scroll-view" style="<?= $background ?>">
        <div class="navbar nav_title" style="border: 0;<?= $background ?>">
            <a href="{{ route('extensionsvalley.admin.dashboard') }}" class="site_title">
                @if (\WebConf::get('site_name') != '')
                    <i class="fa fa-paw"></i>
                    <span>{{ \WebConf::get('site_name') }}</span>
                @else
                    <img style="width: 40px; height: 35px;"
                        src="{{ asset('packages/extensionsvalley/dashboard/images/codfoxxlogo_white.png') }}">
                    <span>GRANDIS</span>
                @endif
            </a>
                <i id="btn-toggle-search" class="fa fa-search" style="display:none; cursor: pointer;"></i>
                <input type="text" name="menu_search" id="menu_search" placeholder="search.."
                    style="padding-left: 3px;position: relative;z-index: 9999 !important;display:none;" />
        </div>
        <div class="clearfix"></div>

        @include('Dashboard::dashboard.partials.avatarsection')

        <br />
        <!-- sidebar menu -->
        @include('Dashboard::dashboard.partials.sidemenu')
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        @include('Dashboard::dashboard.partials.sidemenufooter')
        <!-- /menu footer buttons -->
    </div>
</div>


<!-----------select default location modal------------------------------>
<div class="modal fade select_default_location_modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Select Default Location</h4>
            </div>
            @php
                $user_id = \Auth::user()->id;
                $user_details = \DB::table('users')
                    ->where('id', $user_id)
                    ->select(['unit_id', 'default_location'])
                    ->get();
                $unit_id = $user_details[0]->unit_id;
                $default_location = $user_details[0]->default_location;

                $locations = \DB::table('location')
                    ->whereNull('location.deleted_at')
                    ->where('location.status', 1)
                    ->where('location.unit_id', $unit_id)
                    ->orderBy('location.location_name')
                    ->pluck('location.location_name', 'location.location_code');
            @endphp
            <div class="modal-body locationListModalBody" data-default-location="{{ $default_location }}"
                style="min-height:350px; height:auto;">
                <div class="col-md-12 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Location</label>
                        <div class="clearfix"></div>
                        {!! Form::select('location', ['0' => ' Select Location'] + $locations->toArray(), null, [
                            'class' => 'form-control select_default_location_modal_data',
                        ]) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-----------select default location modal ends------------------------->
<!-----------select default branch modal------------------------------>
@if ($koyili_mode == 1)
    <div class="modal fade select_default_branch_modal" role="dialog" tabindex="-1" data-keyboard="false"
        data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close close_white">&times;</button>
                    <h4 class="modal-title">Select Default Branch</h4>
                </div>
                @php
                    $branchs = \DB::table('branch')
                        ->whereNull('deleted_at')
                        ->where('status', 1)
                        ->orderBy('name')
                        ->pluck('name', 'id');
                @endphp
                <div class="modal-body branchListModalBody" style="min-height:350px; height:auto;">
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Branch</label>
                            <div class="clearfix"></div>
                            {!! Form::select('Branch', ['0' => ' Select Branch'] + $branchs->toArray(), null, [
                                'class' => 'form-control select_default_branch_modal_data',
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveDefaultBranch()">Save <i
                            class="fa fa-save"></i></button>
                </div>
            </div>

        </div>
    </div>
@endif
<!-----------select default branch modal ends------------------------->

<!-- top navigation -->
@include('Dashboard::dashboard.partials.topnavigation')
<!-- /top navigation -->
<!-- Message Starts-->
@include('Dashboard::dashboard.partials.statusmessage')
<!-- Message Ends-->
<!-- <script src="{{ asset('packages/socketjs/socket.io.js') }}"></script> -->

<script type="text/javascript">
    $(document).ready(function() {
        @if ($clinic_mode == 1)
            showLocationSelectionPopup();
        @endif
        @if ($koyili_mode == 1)
            if (localStorage.getItem('def_branch') == null) {
                $('.select_default_branch_modal').modal('show');
            }
        @endif

        setTimeout(function() {
            $('#menu_toggle').trigger('click');
            $(".theadfix_wrapper").floatThead('reflow');
        }, 1000);

        $('.theadscroll').perfectScrollbar({
            minScrollbarLength: 30
        });
        // setTimeout(function() {
        //     $('#btn-toggle-search').hide();
        // }, 2000);

        if(!$('li.current-page').parents('.treeview').hasClass('active')){
            $('li.current-page').parents('.treeview').addClass('active')
        }
    });

    $('#btn-toggle-search').on('click', function() {
        $('#menu_search').toggle();
        $('#menu_search').focus();
    });

    var $div = $("body");
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if($(mutation.target).hasClass('nav-md')){
                $('#btn-toggle-search').show();
                if(!$('li.current-page').parents('.treeview').hasClass('active')){
                    $('li.current-page').parents('.treeview').addClass('active')
                }
            }
            if($(mutation.target).hasClass('nav-sm')){
                $('#btn-toggle-search').hide();
            }
        });
    });

    

    observer.observe($div[0], {
        attributes: true,
        attributeFilter: ['class']
    });

    $('#menu_search').on('keyup', function() {


        var filter = $(this).val();
        $(".child_menu li").each(function() {

            if (filter == "") {
                $(this).show();
                $(this).fadeIn();
                $('.child_menu').hide();
            } else if ($(this).text().trim().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
                $(this).parent('.child_menu').hide();
                 $(this).fadeOut();
            } else {
                $(this).parent("ul").show();
                $(this).show();
                $(this).fadeIn();

            }
        });

        $("li.treeview").each(function(){
            if($(this).find('li:not([style*="display:none"]):not([style*="display: none"])').length == 0 && $(this).find('span').text().trim().search(new RegExp(filter, "i")) < 0 ){
                $(this).hide();
            } else {
                $(this).show();
            }
        });


        $(".sidebar_menu_parent_div").scrollTop(0);
        $('.nav .child_menu').show()

    });

    function selectPracticeLocation() {
        doctor_id = localStorage.getItem('selected_dr_id');
        $('#practice_location_modal').modal({
            show: true,
            keyboard: false,
            backdrop: 'static'
        });

        $.ajax({
            type: "GET",
            url: $('#base_url').val() + "/patient_register/getPracticeLocation",
            data: {
                doctor_id: doctor_id
            },
            beforeSend: function() {
                $('#practice_location_modal').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function(data) {
                $('#practice_location_modal').LoadingOverlay("hide");
                $('#practice_location_modal_data').html(data);
            }
        });
    }

    function set_practice_location(practice_location_id, name) {
        $.ajax({
            type: "GET",
            url: $('#base_url').val() + "/patient_register/setPracticeLocation",
            data: {
                practice_location_id: practice_location_id
            },
            beforeSend: function() {
                $('#practice_location_modal').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function(data) {
                $('#practice_location_modal').LoadingOverlay("hide");
                $('#practice_location_modal').modal('hide');
                $('#practice_location').val(data);
                $('#practice_location_hidden').val(data);
                localStorage.setItem('practice_location', data);
                localStorage.setItem('practice_location_name', name);
                $('#practice_location').select2("val", data);
                $('#current_practice_location').html(name);
                searchPatient();
            }

        });
    }


    function ajax_list_key_down(ajax_div, event) {
        var list_items = $("#" + ajax_div).find('li');
        var selected = list_items.filter('.liHover');

        if (event.keyCode != 40 && event.keyCode != 38) return;
        list_items.removeClass('liHover');
        if (event.keyCode === 40) {
            if (!selected.length || selected.is(':last-child')) {
                current = list_items.eq(0);
            } else {
                current = selected.next();
            }
        } else if (event.keyCode === 38) {
            if (!selected.length || selected.is(':first-child')) {
                current = list_items.last();
            } else {
                current = selected.prev();
            }
        }
        current.addClass('liHover');
    }

    function ajaxlistenter(ajax_div) {
        $("#" + ajax_div).find('li.liHover').trigger('click');
    }
    $('#menu_toggle').on('click', function() {
        $(".theadfix_wrapper").floatThead('reflow');
    });


    function showLocationSelectionPopup() {

        var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : '';
        var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : '';
        if (location_name) {
            if (location_name == 'undefined') {
                location_name = 'Select Location';
            }
            $("#current_location").html(location_name);
        }
        else {
            $("#current_location").html('Select Location');
        }

        if (location_id != '') {
            $(".select_default_location_modal_data").val(location_id);
        } else {
            var default_location = $('.locationListModalBody').attr('data-default-location');
            $(".select_default_location_modal_data").val(default_location).trigger('change');

            if (!default_location){
                @if($dr_status == 0)
                showLocationListModal();
                @endif
            }
        }


    }

    function showLocationListModal() {
        $(".select_default_location_modal").modal('show');
    }

    $(document).on("change", '.select_default_location_modal_data', function() {
        var location_id = $(this).val();

        var location_name = $('.select_default_location_modal_data option:selected').html();
        localStorage.setItem('location_id', location_id);
        localStorage.setItem('location_name', location_name);
        $("#current_location").html(location_name);
        $(".select_default_location_modal").modal('hide');
    });

    $(document).on("click", ".right_col", function() {
        if (window.screen.width < 1000 && $('body').hasClass('nav-md')) {
            $('body').removeClass('nav-md').addClass('nav-sm');
            $(".nav .child_menu").hide()
        }
    });

    function saveDefaultBranch() {
        if ($('.select_default_branch_modal_data').val() != 0) {
            localStorage.setItem('def_branch', $('.select_default_branch_modal_data').val())
            $('.select_default_branch_modal').modal('hide');
            toastr.success('Success.');

        } else {
            toastr.warning('Please select branch.');
        }
    }
</script>
