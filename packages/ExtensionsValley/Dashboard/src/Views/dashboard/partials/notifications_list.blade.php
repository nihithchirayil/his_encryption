@foreach($resultData as $res)
<li @if($res->is_viewed == 0) style="background-color:#6f9fc7;color:white;height:75px" @endif style="height:75px;">
    <a @if($res->is_viewed == 0) style="background-color:#6f9fc7;color:white" @endif
         onclick="goToNotification('{{base64_encode($res->target_url)}}','{{$res->id}}');">

      <span class="image">
        @if($res->patient_image !='')
            <img src="data:image/jpg;base64,{!!$res->patient_image!!}" width="64"/>
        @else
            <div class="img-circle user_img" style="color: darkgray;
            font-size: 37px;" alt="">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
        @endif
      </span>
      <span>
        <span>{{$res->patient_name}}{{$res->is_viewed}}</span>
         @php

             $created_at = strtotime($res->created_at);
             $current_time = strtotime(date('Y-m-d H:i:s'));
             $interval = round(abs($created_at - $current_time) / 60,2);
             if($interval<60){
                $interval = (int)$interval.'minutes';
             }else if($interval < 1440){
                $interval = (int)($interval/60) .'hr ago';
             }else{
                $interval = (int)($interval/1440) .'days ago';
             }

         @endphp
        <span class="time">{{$interval}}</span>
      </span>
      <span class="message">
        {{$res->content}}
      </span>
    </a>
</li>
@endforeach
  <li>
    <div class="text-center">
      <a>
        <strong>See All Alerts</strong>
        <i class="fa fa-angle-right"></i>
      </a>
    </div>
  </li>
</ul>
</li>
