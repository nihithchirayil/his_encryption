<style>
    .top_nav .toggle {
        padding-top: 8px !important;
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .top_nav .toggle a {
        padding: 0 15px 0;
    }

    .top_nav .toggle a:hover {
        color: #009869;
    }

    .top_nav .nav>li>a {
        padding: 5px 15px 5px !important;
    }

    .top_nav .nav_menu {
        background: #f8fffd !important;
        box-shadow: 0 0 6px #ccc;
    }

    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 22px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #a5a9ac;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 15px;
        width: 15px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #399f62;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .blink_me {
        animation: blinker 0.8s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    /*
.blink_me {
  animation: animateColors 1s infinite;
}
@keyframes animateColors {
  0% { color: #e74c3c; }
  25% { color: #399f62; }
  50% { color: #f0ad4e; }
  75% { color: #399f62; }
  100% { color: #f0ad4e; }
} */

</style>
<div class="top_nav">
    @php
        $user_id = \Auth::user()->id;
        $status = 0;
        $sql = "select g.name from user_group ug join groups g on g.id=ug.group_id where ug.user_id=$user_id and ug.status = 1 and g.status = 1 and g.deleted_at is null and g.name = 'DOCTOR'";
        $res_users = \DB::select($sql);
        if (count($res_users) > 0 && env('QUEUE_MANAGEMENT_ENABLED', 'FALSE') == 'TRUE') {
            $status = 1;
        }
        $enable_emr_lite_version = 0;
        try{
            $enable_emr_lite_version = \DB::table('config_detail')->where('name', 'enable_emr_lite_version')->value('value');
        } catch(\Exception $e){
            $enable_emr_lite_version = 0;
        }
        
    @endphp

    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle" style="display:none;"><i style="font-size: 20px;" class="fa fa-bars"></i></a>
                @if($enable_emr_lite_version == 0)
                {{-- <a class="action-redirection"><i style="font-size: 20px;" onclick="redirectToBackBtn();"
                        class="fa fa-arrow-circle-left"></i></a> --}}
                <a id="btn_back_to_dashboard" class="action-redirection" onclick="redirectToDashboard();"><span
                        class="badge bg-default">
                        <i style="font-size:14px;" class="fa fa-arrow-circle-left"></i>&nbsp;
                        Dashboard
                    </span>
                </a>
                @endif
                @if($enable_emr_lite_version == 1)
                <div style="white-space: nowrap;">Refreshing in <span class="countdown_timer_oplist_refresh" style="font-size: 17px; color:red; font-weight: 600;"></span> seconds.</div>
                @endif
                <button type="button" class="btn btn-sm licence_warning_indication" style="background: #f8fefd; border: 1px solid #f3f3f3; display:none;" title="Your licence is going to expired soon."><i class="fa fa-exclamation-triangle blink-icon" style="color:#dc1313; font-size: 20px;" > </i></button>


            </div>

            <ul class="nav navbar-nav navbar-right" style="width:80%;">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false">
                        <?php
                        $profile_image = 'packages/extensionsvalley/dashboard/images/profile/user.png';
                        $icon = \DB::table('user_profile')
                            ->Where('user_id', \Auth::guard('admin')->user()->id)
                            ->value('media');
                        if (!empty($icon)) {
                            $profile_image = $icon;
                        }
                        ?>
                        <img src="{{ URL::to('/') }}/{{ $profile_image }}" alt="">
                        {{ ucfirst(\Auth::guard('admin')->user()->name) }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ route('extensionsvalley.admin.editusersprofile') }}"> Profile</a></li>
                        @if ($status == 1)
                            <li><a href="javascript:;" onclick="logoutDoctor()"><i
                                        class="logout_btn fa fa-sign-out pull-right"></i> Log Out</a>
                            </li>
                        @else
                            <li><a href="javascript:;" onclick="logoutMe()"><i
                                        class="logout_btn fa fa-sign-out pull-right"></i> Log Out</a>
                            </li>
                        @endif
                    </ul>
                </li>
                @include('Dashboard::dashboard.partials.topnavbuilder')

            </ul>
        </nav>
    </div>
</div>
<script type="text/javascript">
    function logoutMe() {
        localStorage.setItem('loginstatus', 0);
        localStorage.clear();
        document.location.href = "{{ route('extensionsvalley.admin.logout') }}";
    }

    function logoutDoctor() {
        updateCheckInCheckout("FALSE");
    }

    function updateCheckInCheckout(check_in_status) {

        var url = $('#base_doumenturl').val() + "/emr/updateCheckInStatus";
        var noise_image = $('#base_doumenturl').val() + '/packages/extensionsvalley/emr/images/noise.svg';
        var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) :
            0;
        localStorage.removeItem('practice_location');

        $.ajax({
            url: url,
            type: "POST",
            data: {
                check_in_status: check_in_status,
                selected_dr_id: selected_dr_id
            },
            success: function(data) {
                if (data) {
                    // alert('ddd' + check_in_status);
                    // return;
                    if (data.status == 1) {
                        toastr.success("Check-In Successful");
                        $(".queue_management_div").show();
                        $(".call_token_btn").show();
                        $("#monitorscreen").css("background", "white");
                    } else if (data.status == 2) {
                        toastr.success("Check-Out Successful");
                        $(".queue_management_div").hide();
                        $("#monitorscreen").css("background",
                            "linear-gradient(to right, #353535, transparent), url(" + noise_image + ") "
                        );
                        $(".call_token_btn").hide();
                    }

                    localStorage.removeItem('selected_dr_id');
                    localStorage.removeItem('current_booking_id');
                    localStorage.removeItem('doctor_emergency_status');
                    localStorage.removeItem('current_special_booking_id');
                    localStorage.removeItem('current_normal_booking_id');
                    localStorage.setItem('loginstatus', 0);
                    localStorage.removeItem('practice_location');
                    localStorage.removeItem('shift_id');
                    document.location.href = "{{ route('extensionsvalley.admin.logout') }}";
                }
            },
            error: function(e) {
                localStorage.removeItem('selected_dr_id');
                localStorage.removeItem('current_booking_id');
                localStorage.removeItem('current_special_booking_id');
                localStorage.removeItem('current_normal_booking_id');
                localStorage.removeItem('doctor_emergency_status');
                localStorage.setItem('loginstatus', 0);
                localStorage.removeItem('shift_id');
                document.location.href = "{{ route('extensionsvalley.admin.logout') }}";

            },
            complete: function(e) {
                localStorage.clear();
            }
        });
    }

    $(window).load(function(){
        $("#menu_toggle").show();
    })
</script>
