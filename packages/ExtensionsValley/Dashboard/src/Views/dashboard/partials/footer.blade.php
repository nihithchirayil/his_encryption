<footer style="height:38px;">
    <div class="pull-right" style="margin:7px 15px 0px 0px !important;">
        <span><b> {{ env('APP_VERSION', '') }}</b></span> | Powered by {!! \WebConf::getConfig('copyright') !!}  <b>
            {{--  <a href="http://www.codfoxx.in" target="_blank">Codfoxx Software Labs</a>  --}}
        </b>
    </div>
    <div class="clearfix"></div>
</footer>
