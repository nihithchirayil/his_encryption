<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }} | {{ \WebConf::get('site_name') }}</title>
    @if (\WebConf::get('fav_icon') != '')
        <link rel="shortcut icon" href="{{ URL::to('/') }}/{{ \WebConf::get('fav_icon') }}" />
    @endif

    <!-- Bootstrap -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/custom.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/core-admin.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/green.css') }}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/datatables/dataTables.bootstrap.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/datatables/buttons.bootstrap.min.css') }}"
        rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/select2.min.css') }}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap-progressbar-3.3.4.min.css') }}"
        rel="stylesheet">


    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/plugins/font-awesome/css/font-awesome.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/plugins/custom-input-icons/custom-input-icons.css') }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}"
        rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"> -->
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    @yield('css_extra')

    <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        if (localStorage.getItem('loginstatus') == 0) {
            document.location.href = "{{ route('extensionsvalley.admin.login') }}";
        }
    </script>
</head>

<body class="nav-sm" style="background: #fff">
    <div class="container body">
        <div class="main_container">
            @yield('content-header')
            @yield('content-area')
            @include('Dashboard::dashboard.partials.footer')
        </div>
    </div>
    <!-- iCheck -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/datatables/responsive.bootstrap.js') }}"></script>
    <!-- select 2 JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/select2.full.min.js') }}"></script>
    <!-- Custom Core JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/core-admin.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/custom.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/momentjs/moment.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}">
    </script>

    <script
        src="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/floathead/jquery.floatThead.js') }}"></script>


    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.locales.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>

    @yield('javascript_extra')

</body>

</html>
