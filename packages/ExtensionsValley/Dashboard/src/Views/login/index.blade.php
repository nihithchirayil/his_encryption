<!DOCTYPE html>
<html lang="en">

<head>
    <title>GRANDIS</title>
    <link rel="icon" href="{{ asset('packages/extensionsvalley/dashboard/images/codfoxxlogo_white.png') }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }} </title>
    @if (\WebConf::get('fav_icon') != '')
        <link rel="shortcut icon" href="{{ URL::to('/') }}/{{ \WebConf::get('fav_icon') }}" />
    @endif

    <!-- Bootstrap -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/custom.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/core-admin.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/green.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/default/css/select2.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet"> -->
    <link href="{{ asset('packages/extensionsvalley/default/css/login-style.css') }}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}"></script>

    <!-- iCheck -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/icheck.min.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script
        src="{{ asset('packages/extensionsvalley/dashboard/js/custom.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/login.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/range_slider.js') }}"></script>

</head>

<body class="login" style="background-color: #86c2ff">
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_main_container">
        <div class="row" style="z-index: 999;">
            <div class="col-md-12 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="message alert alert-success  col-md-12 text-center">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="message alert alert-danger  col-md-12 text-center">{{ Session::get('error') }}</div>
                @endif
                @if (Session::has('warning'))
                    <div class="message alert alert-warning  col-md-12 text-center">{{ Session::get('warning') }}</div>
                @endif
            </div>
        </div>

        {{-- <section class="login_content"> --}}
        <div class="codfox_login_wrapper">
            <div class="col-md-7 no-padding">
                <div class="login_left_box">
                    <div class="login_img">
                        <img class="img-responsive"
                            src="{{ asset('packages/extensionsvalley/default/img/login_img.png') }}" alt="">
                    </div>
                </div>
            </div>
            {!! Form::open(['route' => 'extensionsvalley.admin.auth', 'method' => 'post']) !!}
            <div class="col-md-5 no-padding">
                <div class="login_right_box">
                    <div class="login_form_box">
                        <div class="codfox_login_logo" style="height:110px;">
                            <img class=""
                                src="{{ asset('packages/extensionsvalley/dashboard/images/grandis_new.gif') }}">

                        </div>
                        <div class="clearfix"></div>
                        @if ($errors->has('license_expired'))
                            <span class="error_red"><b>{{ $errors->first('license_expired') }}</b></span>
                        @endif
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="blue" for="">Username</label>
                            <div class="clearfix"></div>
                            @if ($errors->has('email'))
                                <span class="error_red"><b>{{ $errors->first('email') }}</b></span>
                            @endif
                            <input type="text" style="font-weight: 600;color:#5ea2c4 !important;" autocomplete="off"
                                id="email" name="email" class="form-control" required="" placeholder="Email" />
                        </div>



                        <div class="form-group">
                            <label class="blue" for="">Password</label>
                            <div class="clearfix"></div>
                            @if ($errors->has('password'))
                                <span class="error_red"><b>{{ $errors->first('password') }}</b></span>
                            @endif
                            <input type="password" style="font-weight: 600;color:#5ea2c4 !important;" name="password"
                                class="form-control" required="" placeholder="Password" />
                        </div>


                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm">
                            <input name="remember" id="remember_me" type="checkbox" class="flat" value="Remember Me">
                            <label id="remember_me">Remember Me</label>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <a style="color:#87888c;color:#5ea2c4 !important;" class=""
                                href="{{ route('extensionsvalley.admin.reset') }}">Forgot Password ?
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="ht5"></div>
                        <div class="form-group no-margin">
                            <button class="btn btn-block signin_btn" onclick="return SetLocalStorage()"
                                type="submit">SIGN IN</button>
                        </div>
                    </div>
                </div>


            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
            <div style="text-align: center;color: #1caff4">
                <b style="border-right: 1px solid;padding-left:4px;"> Powered by Grandis Business solutions Pvt Ltd &nbsp;</b>
                <b>{{ env('APP_VERSION', '') }}</b>
            </div>
        </div>
        {{-- </section> --}}
    </div>
    <script type="text/javascript">
        function SetLocalStorage() {
            window.localStorage.setItem('loginstatus', 1);
            localStorage.removeItem('selected_dr_id')
            localStorage.removeItem('current_booking_id')
            localStorage.removeItem('current_normal_booking_id')
            localStorage.removeItem('current_special_booking_id')
            localStorage.removeItem('doctor_emergency_status');
            localStorage.removeItem('shift_id');
            return true;
        }
    </script>
</body>

</html>
