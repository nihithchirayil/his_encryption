<?php
$div_height = '400px';
if ($page_links) {
    $div_height = '370px';
}
?>
<div class="theadscroll" style="position: relative; height: <?= $div_height ?>;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th style = "width:25%">User Name</th>
                <th style = "width:25%">Branch Name</th>
                <th style = "width:25%">Actions</th>
            </tr>
        </thead>
        <tbody>
           
                    @if(count($user_branches) > 0)
            @foreach ($user_branches as $user_branche)
                <tr style="cursor: pointer;"   data-id="{{ $user_branche->emp_br_id }}" data-emp-name="{{ $user_branche->user_name }}" data-emp-user_id="{{  $user_branche->user_id }}" data-emp-branch_id="{{ $user_branche->br_id }}">
                    <td class = "td_common_numeric_rules">{{($user_branches->currentPage() - 1) * $user_branches->perPage() + $loop->iteration}}.</td>
                    <td class = "common_td_rules" >{{ $user_branche->user_name }}</td>
                    <td class = "common_td_rules" >{{ $user_branche->branch_name}}</td>
                        <td>
                            <button type="button" id='button' class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>