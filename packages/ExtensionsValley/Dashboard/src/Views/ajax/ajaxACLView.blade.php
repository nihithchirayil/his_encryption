
<?php $i=0; ?>
@if(isset($grid[0]))
<ul class="menu_container" id="parient_0">
    @foreach($grid[0] as $parient_id => $level_data)

    <li class="mainmenu" id="check_0">
        <div class="col-md-2"><span style="">{{++$i}}</span> <input type="hidden" name="menu_id[]" value="{{$parient_id}}"></div>

        <div class="col-xs-8 no-padding"><span>{{$level_data['name']}}</span> <span style="float:right;">@if(isset($grid[1][$parient_id]) && !empty($grid[1][$parient_id]))<i class="fa fa-chevron-down red minusbtn" style="display: none;" id="minus_{{$parient_id}}" onclick="menudrop('{{$parient_id}}','minus')"></i> <i class="fa fa-chevron-circle-right blue plusbtn" id="plus_{{$parient_id}}" onclick="menudrop('{{$parient_id}}','plus')"></i>@endif</span></div>

        <div class="col-xs-2" style="padding-left : 30px">
            <input class="view" data-parentid="0" data-menuid="{{$parient_id}}" name="view_update[{{$parient_id}}]" data-actiontype="has_view" id="view_{{$parient_id}}" type="checkbox" onchange="showHideControls2(this, 'checkbox');" @if(isset($level_data['view1']) && ($level_data['view1'] == '1')) {{ "checked value='1'" }} @else  {{ "value='0'" }} @endif>
        </div>


        {{-- <div class="col-xs-1" style="padding-left : 30px">
            <input class="add" data-parentid="0" data-menuid="{{$parient_id}}" name="add_update[{{$parient_id}}]" data-actiontype="has_add" id="add_{{$parient_id}}" type="checkbox" onchange="showHideControls2(this, 'checkbox');" @if(isset($level_data['add1']) && ($level_data['add1'] == '1')) {{ "checked value='1'" }} @else  {{ "value='0'" }} @endif> Add
        </div>
        <div class="col-xs-1" style="padding-left : 30px">
            <input class="edit" data-parentid="0" data-menuid="{{$parient_id}}" name="edit_update[{{$parient_id}}]" data-actiontype="has_edit" id="edit_{{$parient_id}}" type="checkbox" onchange="showHideControls2(this, 'checkbox');" @if(isset($level_data['edit1']) && ($level_data['edit1'] == '1')) {{ "checked value='1'" }} @else  {{ "value='0'" }} @endif> Edit
        </div> --}}

        @if($max_level != 0)
            <?php sub_category($max_level,$i,0,1,$parient_id,$grid); ?>
        @endif
    </li>
    @endforeach
</ul>
@endif

<?php function sub_category($level,$count_parent,$count_child,$sub_level,$parient,$grid_data) { ?>
    @if(isset($grid_data[$sub_level][$parient]) && !empty($grid_data[$sub_level][$parient]) )
    <ul class="subheading" id="parient_{{$parient}}">
        <?php $j = 0; $next_level = $sub_level + 1; ?>

        <?php
        foreach ($grid_data[$sub_level][$parient] as $next_parent_id => $next_level_data) {

            ?>

        <li id="check_<?php echo $parient ?>" @if(!isset($grid_data[$next_level][$next_parent_id]) && empty($grid_data[$next_level][$next_parent_id])) style="width:100%; position:relative; display:inline-block;margin-left:5px;" @endif>

            @if($count_child != 0)
            <div class="col-md-2"><span style="">{{$count_parent.".".$count_child.".".++$j}}</span> <input type="hidden" name="menu_id[]" value="{{$next_parent_id}}"></div>
            @else
            <div class="col-md-2"><span style="">{{$count_parent.".".++$j}}</span> <input type="hidden" name="menu_id[]" value="{{$next_parent_id}}"></div>
            @endif

            <div class="col-md-8 no-padding"><span>{{$next_level_data['name']}}</span> <span style="float:right;">@if(isset($grid_data[$next_level][$next_parent_id]) && !empty($grid_data[$next_level][$next_parent_id]))<i class="fa fa-chevron-down red minusbtn" style="display: none;" id="minus_{{$next_parent_id}}" onclick="menudrop('{{$next_parent_id}}','minus')"></i> <i class="fa fa-chevron-circle-right blue plusbtn" id="plus_{{$next_parent_id}}" onclick="menudrop('{{$next_parent_id}}','plus')"></i>@endif</span></div>

            <div class="col-md-2" style="padding-left : 30px">
                <input class="view" data-parentid="{{$parient}}" data-menuid="{{$next_parent_id}}" name="view_update[{{$next_parent_id}}]" data-actiontype="has_view" id="view_{{$next_parent_id}}" type="checkbox" onchange="showHideControls2(this, 'checkbox');" @if(isset($next_level_data['view1']) && ($next_level_data['view1'] == '1')) {{ "checked value='1'" }} @else  {{ "value='0'" }} @endif>
            </div>
            {{-- <div class="col-xs-1" style="padding-left : 30px">
                <input class="add" data-parentid="{{$parient}}" data-menuid="{{$next_parent_id}}" name="add_update[{{$next_parent_id}}]" data-actiontype="has_add" id="add_{{$next_parent_id}}" type="checkbox" onchange="showHideControls2(this, 'checkbox');" @if(isset($next_level_data['add1']) && ($next_level_data['add1'] == '1')) {{ "checked value='1'" }} @else  {{ "value='0'" }} @endif> Add
            </div>
            <div class="col-xs-1" style="padding-left : 30px">
                <input class="edit" data-parentid="{{$parient}}" data-menuid="{{$next_parent_id}}" name="edit_update[{{$next_parent_id}}]" data-actiontype="has_edit" id="edit_{{$next_parent_id}}" type="checkbox" onchange="showHideControls2(this, 'checkbox');" @if(isset($next_level_data['edit1']) && ($next_level_data['edit1'] == '1')) {{ "checked value='1'" }} @else  {{ "value='0'" }} @endif> Edit
            </div> --}}

            @if($level != $sub_level)
                <?php sub_category($level,$count_parent,$j,$next_level,$next_parent_id,$grid_data); ?>
            @endif
        </li>
        <?php } ?>
    </ul>
    @endif
<?php }

?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".menu_container").children().find('.subheading').hide();
    });
</script>
