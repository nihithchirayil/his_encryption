<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var menu = $('#search_menu').val();
            var parent_menu = $('#parent_menu').val();
            var token = $('#token_hiddendata').val();
            var param = {
                _token: token,
                menu: menu,
                parent_menu: parent_menu
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {

                    $('#searchBtn').attr('disabled', true);
                    $('#searchSpin').removeClass('fa fa-search');
                    $('#searchSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function() {
                    $('#searchBtn').attr('disabled', false);
                    $('#searchSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchSpin').addClass('fa fa-search');
                },
            });
            return false;
        });

    });
</script>
<?php
$div_height = '515px';
if ($page_links) {
    $div_height = '450px';
}
?>

<div class="theadscroll" style="position: relative; height: <?= $div_height ?>;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">Sl.No.</th>
                <th width="8%">Menu Name</th>
                <th width="8%">Parent Menu </th>
                <th width="5%">Route </th>
                <th width="5%">Level</th>
                <th width="5%">Ordering</th>
                <th width="6%">Status</th>
                <th width="10%">Action</th>


            </tr>
        </thead>
        <tbody>
            @if (sizeof($searchMenu)>0)
            @foreach ($searchMenu as $item)

            <tr style="cursor:pointer;" data-id="{{$item->id}}" data-name="{{$item->name}}" data-parent-id="{{$item->parent_name}}" data-menu-level="{{$item->menu_level}}" data-display-order="{{$item->display_order}}" data-status="{{$item->status_id}}" data-url="{{$item->url}}" data-rout="{{$item->route}}" data-menu_icon="{{$item ->menu_icon}}">
                <td class="td_common_numeric_rules">{{($searchMenu->currentPage() - 1) * $searchMenu->perPage() + $loop->iteration}}</td>
                <td class="common_td_rules">{{$item->name}}</td>
                <td class="common_td_rules">{{$item->parent_id}}</td>
                <td title="{{$item->route}}"class="common_td_rules">{{$item->route}}</td>
                <td class="td_common_numeric_rules">{{$item->menu_level}}</td>
                <td class="td_common_numeric_rules">{{$item->display_order}}</td>
                <td class="common_td_rules">{{$item->status}}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-warning "  onclick="editItem({{$item->id}});"><i id = "editButton_{{$item->id}}" class="fa fa-edit editButton" title="Edit"></i>Edit </button>
                    {{-- <button type="button" class="btn btn-sm btn-danger " onclick="delete_menu({{$item->id}});"><i id = "deleteButton_{{$item->id}}" class="fa fa-trash" title="Delete"></i> </button> --}}
                </td>


            </tr>

            @endforeach
            @else
            <tr>
                <td colspan="11" style="text-align: center">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>