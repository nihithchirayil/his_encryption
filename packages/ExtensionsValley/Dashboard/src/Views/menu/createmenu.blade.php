@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
   
      <!-- Custom Theme Scripts -->
      <link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

    
       <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">



@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <input type="hidden" name="employee_id" id="employee_id" value="<?= $user_id ?>">
        {{-- <input type="hidden" id="employee_name_hidden" value=""> --}}
        <input type="hidden" name="leave_id" id="leave_id" value="0">
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-9 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height:70px">
                        <div class="col-md-12 padding_sm">
                            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">

                            <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box ">
                                    <label for="">Menu</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="search_menu">
                                        <option value="">Select Menu</option>
                                        <?php
                                        foreach ($menu as $each) {
                                            ?>
                                        <option value="<?= $each->id ?>"><?= $each->name ?></option>
                                        <?php
                                    }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            {{-- <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box">
                                    <label for="">Parent Menu</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="parent_menu">
                                        <option value="">Select Parent Menu</option>

                                    </select>
                                </div>
                            </div> --}}
                            <div class="col-md-1 pull-right  padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searchMenu()" id="searchBtn"
                                    class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 padding_sm">
                <div class="box no-border">
                    <input type="hidden" name="hidden_id" id="hidden_id" value="">
                    <div class="box-body clearfix" style="height: 630px">
                        <h5 class="blue text-center"><strong> Add Menu </strong></h5>
                        <hr>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Menu Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="menu_name"
                                        name="menu_name" value="" required>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px" >
                                <div class="mate-input-box">
                                    <label for="">Parent Menu</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="parent_id" onchange = "getSubMenu()" name="parent_id">
                                        <option value="">Select Parent Menu</option>
                                        @foreach ($parent_menu as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>

                                        @endforeach



                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px; display:none "  id="sub_menu_id">
                                <div class="mate-input-box">
                                    <label for="">Sub Menu</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="sub_id" style="width: 271px;" name="sub_id">
                                        <option value="">Select Sub Menu</option>
                                        @foreach ($menu as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>

                                        @endforeach


                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">URL</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="menu_url"
                                        name="menu_url" value="">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Route Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="route" name="route"
                                        value="" required>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Menu Icon</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="icon" name="icon"
                                        value="">
                                </div>
                            </div>
                            <!-- <div class="clearfix"></div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Menu Level</label>
                                    <div class="clearfix"></div>
                                    <input type="number" autocomplete="off" class="form-control" id="level" name="level"
                                        value="0">
                                </div>
                            </div> -->
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Display Order</label>
                                    <div class="clearfix"></div>
                                    <input type="" autocomplete="off" class="form-control" id="order" name="order"
                                        value="">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" selected>{{__('Active')}}</option>
                                        <option value="0">{{__('Inactive')}}</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:20px">
                                <div class="" > 
                                    <div class="clearfix"></div>
                                   <input type="checkbox" name="" id="desktop_menu" class="" style="padding:20px"><Label><strong style="padding: 10px">Is Desktop Menu</strong></Label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <button type="button" id="savebtnBtn" class="btn btn-block btn-success"
                                    onclick="savemenu()"><i id="savebtnSpin" class="fa fa-save"></i> Save
                                </button>
                            </div>
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-info clearFormButton"
                                    onclick="formReset()"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>



    </div>
    </div>
    </div>





    <!-- /page content -->

@stop
@section('javascript_extra')
 
    <script src="{{asset("packages/extensionsvalley/emr/js/createmenu.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version='.env('APP_JS_VERSION', '0.0.1')) }}"> </script>
@endsection
