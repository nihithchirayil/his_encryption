@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

    <style>
    </style>
@endsection
@section('content-area')
    <div class="right_col" role="main">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <h2>{{ $title }}</h2>
                </div>
            </div>
        </div>
        <?php
        $action = 'extensionsvalley.admin.updateusergroup';
        ?>
        <div class="x_panel">
            {!! Form::open(['route' => $action, 'method' => 'post']) !!}
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <div class="row-fluid"
                        style="border-bottom:1px solid #F4F6F5; background:#FBFDFC; padding:10px 15px 10px 15px;">
                        <div class="mate-input-box">
                            <div id="userDiv">
                                {!! Form::select(
                                    'user_id',
                                    ['0' => ' Choose User'] + $groupCategory->toArray(),
                                    request()->has('id') ? request()->get('id') : null,
                                    ['class' => 'select2', 'id' => 'user_id', 'style' => ' color:#555555; padding:4px 12px;'],
                                ) !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id="parent" class="table-responsive theadscroll" style="position: relative; height: 550px;">
                    <table id="fixTable"
                        class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                        style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg">
                                <th style="width:15%" class="common_td_rules">#</th>
                                <th width="30%">
                                    <input id="issue_search_box" onkeyup="searchbyName();" type="text"
                                        placeholder="Name Search.. " style="color: #000;display: none;width: 90%;">
                                    <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i
                                            class="fa fa-search"></i></span>
                                    <span id="item_search_btn_text">Name</span>
                                </th>
                                <th style="width:4%">Active</th>
                            </tr>
                        </thead>
                        <tbody class="assignListData">
                            @if (isset($resultData) && count($resultData))
                                <?php $i = 1; ?>
                                @foreach ($resultData as $data)
                                    <tr>

                                        <td class="common_td_rules">{{ $i }}</td>
                                        <td class="common_td_rules">
                                            <span id="child_id-{{ $i }}"
                                                style="border: none;">{{ $data->name }}</span>
                                            <input type="hidden" class="ug_id" name="ug_id[]"
                                                value="{{ $data->user_group_id }}">
                                            <input type="hidden" class="ass_val" name="ass_val[]"
                                                value="{{ $ass_val }}">
                                        </td>
                                        <td title="">
                                            <input style="" type="checkbox" name="ass_ug_chk[]" class="ass_ug_chk"
                                                <?php if ($data->id != 0) {
                                                    echo 'checked';
                                                } else {
                                                } ?>>
                                            <input type="hidden" name="hidden_ass_ug_id[]"
                                                id="field_view-{{ $i }}" value="{{ $data->id }}"
                                                class="hidden_ass_ug_id">
                                        </td>

                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">No Items Found.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12" style="padding:10px 30px;">
                    <!-- <input type="submit" style="float: right; margin-right: 10px;" value='Submit' class="btn btn-primary"> -->
                </div>
            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // $('#menu_toggle').trigger('click');
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });


            $(document).off('change', '#user_id').on('change', '#user_id', function() {
                var user_id = jQuery('#user_id :selected').val();
                if (!isNaN(parseInt(user_id))) {
                    document.location.href = "?id=" + user_id + "&is_ug=u";
                }
            });


            $(document).on('keyup change', '.ass_ug_chk', function() {
                var status = ($(this).prop('checked')) ? 1 : 0;

                var ug_id = $(this).closest("tr").find('.ug_id').val().trim();
                var ass_val = $(this).closest("tr").find('.ass_val').val().trim();
                var table_ug_id = $(this).closest("tr").find('.hidden_ass_ug_id').val().trim();
                var ur_gp_id = '';
                if (ass_val == 'g')
                    ur_gp_id = $('#user_id').val().trim();
                if (ass_val == 'u')
                    ur_gp_id = $('#group_id').val().trim();
                var iD_input = $(this).closest("tr").find('.hidden_ass_ug_id').attr('id');
                var url = "";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: "ur_gp_id=" + ur_gp_id + "&ug_id=" + ug_id + "&status=" + status +
                        "&ass_val=" + ass_val + "&table_ug_id=" + table_ug_id,
                    beforeSend: function() {},
                    success: function(data) {
                        toastr.success(data.message);
                        $("#" + iD_input).val(data.table_ug_id);
                    },
                    complete: function() {}
                });
            });




        });


        $("#item_search_btn").click(function() {
            $("#issue_search_box").toggle();
            $("#item_search_btn_text").toggle();
        });

        function searchbyName() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("issue_search_box");
            filter = input.value.toUpperCase();
            table = document.getElementById("fixTable");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
        $("#user_id").on('change', function() {
            var id = $("#user_id").val();
            if (id != 0) {
                $('.floatThead-wrapper').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            }
        });

    </script>
@stop
