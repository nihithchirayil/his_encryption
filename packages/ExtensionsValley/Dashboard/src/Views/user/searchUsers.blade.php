<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var user_name = $('#search_username').val();
            var status = $('#search_status').val();
            var param = {
                user_name: user_name,
                status: status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $('#searchUserBtn').attr('disabled', true);
                    $('#searchUserSpin').removeClass('fa fa-search');
                    $('#searchUserSpin').addClass('fa fa-spinner');
                    $('#sarchUsersDataDiv').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(msg) {
                    $('#sarchUsersDataDiv').html(msg);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function() {
                    $('#searchUserBtn').attr('disabled', false);
                    $('#searchUserSpin').removeClass('fa fa-spinner');
                    $('#searchUserSpin').addClass('fa fa-search');
                    $('#sarchUsersDataDiv').LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>
<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; height: 400px;">
            <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th>User Name </th>
                        <th>Email ID</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($user_list) > 0)
                        @foreach ($user_list as $user)
                        <?php $img_url= "" ?>
                        @php
                            $img_url=URL::to('/').'/packages/extensionsvalley/signature/'.$user->signature_name;
                        @endphp
                         <input type="hidden" name="sign_img" id="sign_img_{{$user->id}}" value="{{$img_url}}">
                            <tr class="userdataform" id="userformrow<?= $user->id ?>" style="cursor: pointer;" onclick="userEditLoadData('{{ $user->username }}',
                                        '{{ $user->id }}',
                                        '{{ $user->name }}',
                                        '{{ $user->email }}',
                                        '{{ $user->status }}',
                                        '{{ $user->groups }}',
                                        '{{ $user->is_cashier }}',
                                        '{{ $user->is_nurse }}',
                                        '{{ $user->default_store }}',
                                        '{{ $user->default_location }}',
                                        '{{ $user->station_id }}',
                                        '{{ $user->default_landing_page }}',
                                        '{{ $user->default_procedure_type }}',
                                        '{{ $user->signature_name }}',
                                        '{{ $user->is_casuality_user }}')"
                                        >
                                <td class="common_td_rules name">{{ $user->name }}</td>
                                <td class="common_td_rules email">{{ $user->email }}</td>
                                <td class="common_td_rules status">{{ $user->status_name }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="user_name">No Records found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-12 text-right">
            <ul class="pagination purple_pagination" style="text-align:right !important;">
                {!! $page_links !!}
            </ul>
        </div>
    </div>
</div>
