@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('content-area')
    <div class="right_col" role="main">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <h2>{{$title}}</h2>
                </div>
            </div>
        </div>
        <?php
        $action = 'extensionsvalley.admin.updatedoctorprofile';
        ?>

<input type="hidden" id="base_url" value="{{URL::to('/')}}">

        <div class="x_panel">
            <div class="x_title">
                <h2>Doctor Profile</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {!!Form::open(array('route' => $action, 'method' => 'post','files'=>true))!!}

                <div class="row">

                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group  control-required">
                            {!! Form::label('doctor_id', 'Doctor') !!}
                        {!! Form::select('doctor_id', array("0"=> " Select Doctor") + $doctor_list->toArray(), null, [
                            'class'       => 'form-control',
                            'required'    => 'required'
                        ]) !!}

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group  control-required">
                            {!! Form::label('prescription_department_head', 'Prescription Department Head') !!}
                            {!! Form::text('prescription_department_head', request()->old('prescription_department_head'), [
                                'class'       => 'form-control',
                                'placeholder' => 'Prescription Department Head',
                                'required'    => 'required',
                                'tabindex'    => 4
                            ]) !!}

                        </div>
                    </div>
                     <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group  control-required">
                             {!! Form::label('medication_note', 'Medication Note') !!}
                     <textarea class="tiny_editor" id="medication_note" name="medication_note"></textarea>
                 </div>
             </div>
                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group  control-required">
                             {!! Form::label('medication_footer_text', 'Medication Footer') !!}
                     <textarea class="tiny_editor" id="medication_footer_text" name="medication_footer_text"></textarea>
                 </div>
             </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <a href="javascript:;" onclick="history.go(-1);" class="btn btn-success">Clear</a>
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::token() !!}
                {!! Form::close() !!}
            </div>
        </div>


    </div>
@stop
@section('javascript_extra')

{!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
<script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>

<script type="text/javascript">


    $("#doctor_id").on('change', function() {
    var doctor_id = $("#doctor_id").val();

    if (doctor_id) {
        var url = $('#base_url').val() + "/admin/getdoctordetails";
        var param = { doctor_id: doctor_id};
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {

            },
            success: function(response) {

                $("#prescription_department_head").val(response.prescription_department_head);
                if(response.medication_note){
                    tinymce.get('medication_note').setContent(response.medication_note);
                }else{
                    tinyMCE.get('medication_note').setContent('');
                }
                if(response.medication_footer_text){
                    tinymce.get('medication_footer_text').setContent(response.medication_footer_text);
                }else{
                    tinymce.get('medication_footer_text').setContent('');
                }
            },
            complete: function() {
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    }
 });


tinymce.init({
    selector: '#medication_note',
    max_height: 300,
    autoresize_min_height: '90',

    plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
    imagetools_cors_hosts: ['picsum.photos'],
    menubar: 'file edit view insert format tools table help',
    toolbar: 'undo redo | bold italic underline strikethrough | customInsertButton | fontselect fontsizeselect formatselect | FetchInvestigation | fetchMedication | Consultation | AssesmentData | Vitals | fetchTemplate |Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
    browser_spellcheck: true,
    toolbar_sticky: true,
    autosave_ask_before_unload: true,
    autosave_interval: '30s',
    autosave_prefix: '{path}{query}-{id}-',
    autosave_restore_when_empty: false,
    autosave_retention: '2m',
    paste_enable_default_filters: false,
    image_advtab: true,
    contextmenu: false,

    importcss_append: true,

    height: 600,
    image_caption: true,
    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
    noneditable_noneditable_class: 'mceNonEditable',
    toolbar_mode: 'sliding',

    //contextmenu: "paste | link image inserttable | cell row column deletetable",
    //content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px;line-height: 1;}',
    branding: false,
    statusbar: false,
    forced_root_block :'',


 });


tinymce.init({
    selector: '#medication_footer_text',
    max_height: 300,
    autoresize_min_height: '90',

    plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
    imagetools_cors_hosts: ['picsum.photos'],
    menubar: 'file edit view insert format tools table help',
    toolbar: 'undo redo | bold italic underline strikethrough | customInsertButton | fontselect fontsizeselect formatselect | FetchInvestigation | fetchMedication | Consultation | AssesmentData | Vitals | fetchTemplate |Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
    browser_spellcheck: true,
    toolbar_sticky: true,
    autosave_ask_before_unload: true,
    autosave_interval: '30s',
    autosave_prefix: '{path}{query}-{id}-',
    autosave_restore_when_empty: false,
    autosave_retention: '2m',
    paste_enable_default_filters: false,
    image_advtab: true,
    contextmenu: false,

    importcss_append: true,

    height: 600,
    image_caption: true,
    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
    noneditable_noneditable_class: 'mceNonEditable',
    toolbar_mode: 'sliding',

    //contextmenu: "paste | link image inserttable | cell row column deletetable",
    //content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px;line-height: 1;}',
    branding: false,
    statusbar: false,
    forced_root_block :'',


 });

</script>

@endsection


