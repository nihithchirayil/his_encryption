@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop


@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
@stop

@section('content-area')
    <div class="right_col" role="main">
        <div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{$title}}</div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <h4>{{$title}}</h4>
                </div>
            </div>
        </div>
        <?php
        $action = 'extensionsvalley.admin.menuaccessmanager';
        ?>
        <div class="x_panel">
            {!!Form::open(array('route' => $action, 'method' => 'post'))!!}
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }} control-required">
                        {!! Form::label('menutypes', 'Menu Type') !!}
                        {!! Form::select('is_desktop', array("0"=> "Non Desktop", "1" => "Desktop"), (request()->has('is_desktop')) ?  request()->get('is_desktop') : null, [
                        'class'       => 'form-control js-menu-type-is-desktop select2',
                        'required'    => 'required', 'id'=>'is_desktop'
                        ]) !!}
                    </div>

                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        {!! Form::label('groups', 'User Group') !!}
                        {!! Form::select('groups_id', array("0"=> " Choose Group","1" => "Super Admin") + ExtensionsValley\Dashboard\Models\Group::getGroups()->toArray(), (request()->has('id')) ?  request()->get('id') : null, [
                        'class'       => 'form-control js-menu-permission-user-groups select2',
                        'required'    => 'required', 'id'=>'groups_id'
                        ]) !!}

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group control-required">
                        <table class="table table-responsive">
                            <tr>
                                <th></th>
                                <th>#</th>
                                <th>Menus</th>
                                <th>Order</th>
                                <th>
                                    <!-- <input type="checkbox" id="view_all_permissions" class="flat ch_box"> -->
                                    Access
                                </th>
                            </tr>
                            @if( isset($menu_items) && count($menu_items) )
                            <?php
                            $menu_items_level1 = $menu_items->where('parent_id', 0)->sortBy('display_order')->toArray();
                            // dump($menu_items, $menu_items_level1);
                            ?>
                            @if(sizeof($menu_items_level1))
                                <?php $count = 1;
                                    $maincounter = 0;
                                ?>
                                @foreach($menu_items_level1 as $main_menu)
                                <?php
                                    $subcounter = 0;
                                    $maincounter++;
                                ?>

                                    <tr class="accordion-toggle collapsed">
                                        <td style="width: 2%;">
                                            <i id="main_menu_accordion1" data-toggle="collapse" data-parent="#main_menu_accordion{!! $main_menu->id !!}" href="#main_menu_collapse{!! $main_menu->id !!}" style="font-weight: bold; font-size: 14px; cursor: pointer;">+</i>
                                        </td>
                                        <td style="width: 5%;">
                                            {!! $maincounter !!}
                                        </td>
                                        <td>
                                            {!! $main_menu->menu_icon !!} &nbsp;&nbsp;
                                            {!! $main_menu->name !!}
                                        </td>
                                        <td><input type="number" name="main_menu_order[{!! $main_menu->id !!}]" class="ordering_menu_permissions"
                                                   value="@if(!empty($main_menu->menu_permission_id)){{intval($main_menu->ordering)}}@else{{intval($main_menu->display_order)}}@endif"  />
                                        </td>
                                        <td>
                                            <input type="checkbox" name="has_view[{!! $main_menu->id !!}]" class="view_chk_menu_permissions flat ch_box"
                                                   @if(!empty($main_menu->has_view)) checked="checked" @endif />

                                        </td>
                                    </tr>
                                    <?php
                                    $menu_items_level2 = $menu_items->where('parent_id', $main_menu->id)->sortBy('display_order');
                                    ?>
                                    @if(sizeof($menu_items_level2))
                                        <tbody id="main_menu_collapse{!! $main_menu->id !!}"  class="collapse in p-3 hide-table-padding">
                                        @foreach($menu_items_level2 as $submenu)

                                            <tr>
                                                <td></td>
                                                <td>{!! $maincounter !!}.{!! ++$subcounter !!}</td>
                                                <td>
                                                    {!! str_repeat('&nbsp;', 0) !!}
                                                    <i class="{!! $submenu->menu_icon !!}"></i>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $submenu->name !!}
                                                </td>
                                                <td><input type="number" name="main_menu_order[{!! $submenu->id !!}]" class="ordering_menu_permissions"
                                                    value="@if(!empty($submenu->menu_permission_id)){{$submenu->ordering}}@else{{$submenu->display_order}}@endif"  />
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="has_view[{!! $submenu->id !!}]" class="view_chk_menu_permissions flat ch_box"
                                                    @if(!empty($submenu->has_view)) checked="checked" @endif />

                                                </td>
                                            </tr>
                                            <?php   $count++;
                                                    $subcounter++;
                                            ?>
                                        @endforeach
                                        </tbody>
                                    @endif
                                @endforeach
                            @endif
                            @else
                            <tr>
                                <td colspan="5">No items</td>
                            </tr>
                            @endif
                        </table>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                </div>
            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">
    $(document).off('change', '.js-menu-permission-user-groups').on('change', '.js-menu-permission-user-groups', function(){
        // document.location.href = "?id=" + jQuery('.js-menu-permission-user-groups').val();
        var group_id = jQuery('#groups_id :selected').val();
        if(!isNaN(parseInt(group_id)))
        {
            document.location.href = "?id=" + group_id + '&is_desktop=' + jQuery('#is_desktop :selected').val();
        }
    });
    $(document).off('change', '#is_desktop').on('change', '#is_desktop', function(){
        console.log('is_desktop');
        var group_id = jQuery('#groups_id :selected').val();
        if(!isNaN(parseInt(group_id)))
        {
            document.location.href = "?id=" + group_id + '&is_desktop=' + jQuery('#is_desktop :selected').val();
        }
    });
    // $(document).on('change keydown', '.ordering_menu_permissions', function(){
    //     console.log('ordering_menu_permissions');
    // });
    $(document).on('ifChanged', '.view_chk_menu_permissions', function(ev){
        $(ev.target).click();
        console.log('view_chk_menu_permissions');
        var item_val = ($(this).prop('checked'))?1:0;
        var groups_id = $('#groups_id').val().trim();
        var item_index = parseInt($(this).attr('name').match(/[0-9]+/));
        var ordering = $("input[name='main_menu_order["+item_index+"]']").val();
        // console.log($(this), item_val, $(this).prop('checked'));
        console.log('view_chk_menu_permissions'+item_val);
        if ( !isNaN(parseInt(item_val)) ) {
            var url = "{!! route('extensionsvalley.admin.menuaccessmanager') !!}";
            $.ajax({
                type: "GET",
                url: url,
                data: "edit_permissions=update_has_view&groups_id="+groups_id+"&menu_id="+item_index+"&has_view=" + item_val+"&ordering=" + ordering,
                beforeSend: function () { },
                success: function (data) {
                    toastr.success(data.message);
                },
                complete: function () { }
            });
        }
    });
    $(document).on('change', '.ordering_menu_permissions', function(){
        var groups_id = $('#groups_id').val().trim();
        var item_val = $(this).val();
        var item_index = parseInt($(this).attr('name').match(/[0-9]+/));
        console.log('ordering_menu_permissions'+item_val);
        if ( !isNaN(parseInt(item_val)) ) {
            var url = "{!! route('extensionsvalley.admin.menuaccessmanager') !!}";
            $.ajax({
                type: "GET",
                url: url,
                data: "edit_permissions=update_ordering&groups_id="+groups_id+"&menu_id="+item_index+"&ordering=" + item_val,
                beforeSend: function () { },
                success: function (data) {
                    toastr.success(data.message);
                },
                complete: function () { }
            });
        }
    });
</script>
@stop
