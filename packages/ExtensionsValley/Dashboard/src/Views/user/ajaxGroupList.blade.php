@if(isset($resultData) && count($resultData))
    <?php $i = 1;  ?>
    @foreach($resultData as $data)
    <tr>

        <td >{{$i}}</td>
        <td>
            <span id="child_id-{{$i}}" class="form-control" style="border: none;">{{$data->name}}</span>
            <input type="hidden" name="group_id[]" class="group_id" value="{{$data->id}}">
            <input type="hidden" class="group_id_hidden" name="group_id_hidden[]"  value="{{$data->group_id}}">
            <input type="hidden" name="group_edit" >
            <?php $chkID[$i] = $data->id; ?>
        </td>
        <td title="">
            <input style="" type="checkbox" name="assigned_groups[]" class="assigned_groups" <?php if($data->id != 0){ 
            echo 'checked';
            }else{
            } ?> id="field_view-{{$i}}" >
            <input type="hidden" name="hidden_group_chk_status[]"  class="hidden_group_edit" >
            <input type="hidden" name="hidden_group_id[]"  value="{{$data->id}}" class="hidden_group_id" >
        </td> 
        
    </tr> 
    <?php $i++; ?>
    @endforeach
    @endif
    
    @if(isset($userResultData) && count($userResultData))
    <?php $i = 1; ?>
       @foreach($userResultData as $data)
    <tr>

        <td>{{$i}}</td>
        <td>
            <span id="child_id-{{$i}}" class="form-control" style="border: none;">{{$data->name}}</span>
            <input type="hidden" name="user_group_id[]" class="user_group_id" value="{{$data->id}}">
            <input type="hidden" class="user_id_hidden" name="user_id_hidden[]"  value="{{$data->user_id}}">
            <input type="hidden" name="user_edit" >
        </td>
        <td title="">
            <input style="" type="checkbox" name="assigned_users[]" class="assigned_users" <?php if($data->id != 0){ 
            echo 'checked';
            }else{
            } ?> id="field_view-{{$i}}" >
            <input type="hidden" name="hidden_user_chk_status[]"  class="hidden_user_edit" >
            <input type="hidden" name="hidden_user_id[]"  value="{{$data->id}}" class="hidden_user_id" >
        </td> 
        
    </tr>  
    <?php $i++; ?>
    @endforeach
    @endif

@if(isset($groupData) && count($groupData))
    <?php $i = 1;  ?>
    @foreach($groupData as $data)
    <tr>

        <td >{{$i}}</td>
        <td>
            <span id="child_id-{{$i}}" class="form-control" style="border: none;">{{$data->name}}</span>
            <input type="hidden" name="group_id[]" class="group_id" value="{{$data->id}}">
            <input type="hidden" name="group_id_hidden[]"  value="{{$data->group_id}}">
            <input type="hidden" name="group_edit" >
            <?php $chkID[$i] = $data->id; ?>
        </td>
        <td title="">
            <input style="" type="checkbox" name="assigned_groups[]" class="assigned_groups" <?php if($data->id != 0){ 
            echo 'checked';
            }else{
            } ?> id="field_view-{{$i}}" >
            <input type="hidden" name="hidden_group_chk_status[]"  class="hidden_group_edit" >
            <input type="hidden" name="hidden_group_id[]"  value="{{$data->id}}" class="hidden_group_id" >
        </td> 
        
    </tr> 
    <?php $i++; ?>
    @endforeach
    @endif
    
    @if(isset($billTagData) && count($billTagData))
    <?php $i = 1; ?>
       @foreach($billTagData as $data)
    <tr>

        <td>{{$i}}</td>
        <td>
            <span id="child_id-{{$i}}" class="form-control" style="border: none;">{{$data->name}}</span>
            <input type="hidden" name="user_group_id[]" class="user_group_id" value="{{$data->id}}">
            <input type="hidden" name="user_id_hidden[]"  value="{{$data->user_id}}">
            <input type="hidden" name="user_edit" >
        </td>
        <td title="">
            <input style="" type="checkbox" name="assigned_users[]" class="assigned_users" <?php if($data->id != 0){ 
            echo 'checked';
            }else{
            } ?> id="field_view-{{$i}}" >
            <input type="hidden" name="hidden_user_chk_status[]"  class="hidden_user_edit" >
            <input type="hidden" name="hidden_user_id[]"  value="{{$data->id}}" class="hidden_user_id" >
        </td> 
        
    </tr>  
    <?php $i++; ?>
    @endforeach
    @endif