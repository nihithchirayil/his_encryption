@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

<style>
</style>
@endsection
@section('content-area')
    <div class="right_col" role="main">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <h2>{{$title}}</h2>
                </div>
            </div>
        </div>
        <?php
        $action = 'extensionsvalley.admin.updateusergroup';
        ?>
        <div class="x_panel">
            {!!Form::open(array('route' => $action, 'method' => 'post'))!!}
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                   <div class="row-fluid" style="border-bottom:1px solid #F4F6F5; background:#FBFDFC; padding:10px 15px 10px 15px;">
                    <div class="mate-input-box" style="height:86px !important;">
                                        {!! Form::radio('assign', 1, true,['id' => 'assign_user']) !!} User  {!! Form::radio('assign', 2,'',['id' => 'assign_group']) !!} Group
                                        <span class="clearfix"></span><br>
                                    <div id="userDiv">
                                        {!! Form::select('user_id',array("0"=> " Choose User") + ExtensionsValley\Dashboard\Models\User::getUsers()->toArray(), (request()->has('id')) ?  request()->get('id') : null, ['class' => 'select2','id' => 'user_id', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                                    </div>
                                    <div id="groupDiv">
                                        {!! Form::select('group_id',array("0"=> " Choose Group") +  ExtensionsValley\Dashboard\Models\Group::getGroups()->toArray(), (request()->has('id')) ?  request()->get('id') : null, ['class' => 'select2','id' => 'group_id', 'style' => ' color:#555555; padding:4px 12px; display:none;']) !!}
                                    </div>
                                        <span class="clearfix"></span>
                    </div>
                </div>
                </div>
            </div>

            <div class="row">
                    <div id="parent" class="table-responsive theadscroll" style="position: relative; height: 550px;">
                    <table id="fixTable" class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                                        <thead>
                                           <tr class="table_header_bg">
                                                    <th style="width:15%" class="common_td_rules">#</th>
                                                    <th width="30%">
                                                        <input id="issue_search_box" onkeyup="searchbyName();" type="text" placeholder="Name Search.. " style="color: #000;display: none;width: 90%;" >
                                                        <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                                        <span id="item_search_btn_text">Name</span>
                                                    </th>
                                                    <th style="width:4%">Active</th>
                                            </tr>
                                        </thead>
                                        <tbody class="assignListData">
    @if(isset($resultData) && count($resultData))
    <?php $i = 1;  ?>
    @foreach($resultData as $data)
    <tr>

        <td class="common_td_rules">{{$i}}</td>
        <td class="common_td_rules">
            <span id="child_id-{{$i}}" style="border: none;">{{$data->name}}</span>
            <input type="hidden" class="ug_id" name="ug_id[]"  value="{{$data->user_group_id}}">
            <input type="hidden" class="ass_val" name="ass_val[]"  value="{{$ass_val}}">
        </td>
        <td title="">
            <input style="" type="checkbox" name="ass_ug_chk[]" class="ass_ug_chk" <?php if($data->id != 0){
            echo 'checked';
            }else{
            } ?>  >
            <input type="hidden" name="hidden_ass_ug_id[]" id="field_view-{{$i}}" value="{{$data->id}}" class="hidden_ass_ug_id" >
        </td>

    </tr>
    <?php $i++; ?>
    @endforeach
    @else
    <tr>
        <td colspan="3">No Items Found.</td>
    </tr>
    @endif
            </tbody>
            </table>
            </div>
                    <div class="col-md-12" style="padding:10px 30px;">
                            <!-- <input type="submit" style="float: right; margin-right: 10px;" value='Submit' class="btn btn-primary"> -->
                        </div>
                    </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
        @php
            $is_ug = (request()->has('is_ug')) ?  request()->get('is_ug') : '';
        @endphp
        @if($is_ug == 'u')
            $('#groupDiv').hide();
            $('#group_id').val('0');
            $('#group_id').select2().trigger('change');
            $('#assign_user').prop('checked',true) ;
            $('#assign_group').prop('checked',false);
        @elseif($is_ug == 'g')
            $('#userDiv').hide();
            $('#user_id').val('0');
            $('#user_id').select2().trigger('change');
            $('#assign_group').prop('checked',true);
            $('#assign_user').prop('checked',false) ;
        @else
            $('#groupDiv').hide();
            $('#group_id').val('0');
            $('#group_id').select2().trigger('change');
            $('#assign_user').prop('checked',true) ;
            $('#assign_group').prop('checked',false);
        @endif
        $('#assign_group').change(function () {
                if ($(this).prop('checked')) {
                    $('#userDiv').hide();
                    $('#user_id').val('0');
                    $('#user_id').select2().trigger('change');
                    $('#groupDiv').show();
                    // document.getElementById('group_id').selectedIndex = 0;
                    $('#group_id').val('0');
                    $('#group_id').select2().trigger('change');
                    $('.assignListData').hide();
                }else {
                    $('#userDiv').show();
                    $('#groupDiv').hide();
                    $('#group_id').val('0');
                    $('#group_id').select2().trigger('change');
                    $('.assignListData').show();
                }
        });

        $('#assign_user').change(function () {
                if ($(this).prop('checked')) {
                    $('#groupDiv').hide();
                    $('#group_id').val('0');
                    $('#group_id').select2().trigger('change');
                    $('#userDiv').show();
                    // document.getElementById('user_id').selectedIndex = 0;
                    $('#user_id').val('0');
                    $('#user_id').select2().trigger('change');
                    $('.assignListData').hide();
                }else {
                    $('#groupDiv').show();
                    $('#userDiv').hide();
                    $('#user_id').val('0');
                    $('#user_id').select2().trigger('change');
                    $('.assignListData').show();
                }
        });
    $(document).off('change', '#user_id').on('change', '#user_id', function(){
        console.log('user_id');
        var user_id = jQuery('#user_id :selected').val();
        if(!isNaN(parseInt(user_id)))
        {
            document.location.href = "?id=" + user_id + "&is_ug=u";
        }
    });
    $(document).off('change', '#group_id').on('change', '#group_id', function(){
        console.log('group_id');
        var group_id = jQuery('#group_id :selected').val();
        if(!isNaN(parseInt(group_id)))
        {
            document.location.href = "?id=" + group_id + "&is_ug=g";

        }
    });

    $(document).on('keyup change', '.ass_ug_chk', function() {
            var status = ($(this).prop('checked'))?1:0;

            var ug_id = $(this).closest("tr").find('.ug_id').val().trim();
            var ass_val = $(this).closest("tr").find('.ass_val').val().trim();
            var table_ug_id = $(this).closest("tr").find('.hidden_ass_ug_id').val().trim();
            var ur_gp_id = '';
            if(ass_val == 'g')
                 ur_gp_id = $('#user_id').val().trim();
            if(ass_val == 'u')
                 ur_gp_id = $('#group_id').val().trim();
            var iD_input = $(this).closest("tr").find('.hidden_ass_ug_id').attr('id');
            // alert(iD_input);return false;
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: "ur_gp_id="+ur_gp_id+"&ug_id="+ug_id+"&status="+status +"&ass_val="+ ass_val+"&table_ug_id=" + table_ug_id,
                beforeSend: function () { },
                success: function (data) {
                    toastr.success(data.message);
                    $("#"+iD_input).val(data.table_ug_id);
                },
                complete: function () { }
            });
        });


        // $(document).on('keyup change', '.assigned_groups', function() {
        //     var status = '';
        //     if ($(this).prop('checked')) {
        //         $(this).closest("tr").find("input[name='hidden_group_chk_status[]']").val('1');
        //         status = '1';
        //     }else {
        //         $(this).closest('tr').find('input[name="hidden_group_chk_status[]"]').val('0');
        //         status = '0';
        //     }
        //     var user_group_id = $(this).closest("tr").find('.hidden_group_id').val().trim();
        //     var group_id = $(this).closest("tr").find('.group_id_hidden').val().trim();
        //     var user_id = $('#user_id').val().trim();

        //     // alert("ug--"+user_group_id+" -g- "+group_id+" -u- "+user_id);return false;
        //     var url = "";
        //     $.ajax({
        //         type: "GET",
        //         url: url,
        //         data: "assign_group=assign_group&user_group_id="+user_group_id+"&hidden_group_chk_status="+status +"&user_id="+ user_id+"&group_id=" + group_id,
        //         beforeSend: function () { },
        //         success: function (data) {
        //             toastr.success(data.message);
        //             $(this).closest("tr").find('.hidden_group_id').val(data.ass_id);
        //             console.log($(this).closest("tr").find('.hidden_group_id').val(data.ass_id));
        //         },
        //         complete: function () { }
        //     });
        // });
        // $(document).on('keyup change', '.assigned_users', function() {
        //     var status = '';
        //     if ($(this).prop('checked')) {
        //         $(this).closest("tr").find("input[name='hidden_user_chk_status[]']").val('1');
        //         status = '1';
        //     }else {
        //         $(this).closest('tr').find('input[name="hidden_user_chk_status[]"]').val('0');
        //          status = '0';
        //     }
        //     var user_group_id = $(this).closest("tr").find('.hidden_user_id').val().trim();
        //     var user_id = $(this).closest("tr").find('.user_id_hidden').val().trim();
        //     var group_id = $('#group_id').val().trim();

        //     // alert("ug--"+user_group_id+" -g- "+group_id+" -u- "+user_id);return false;
        //     var url = "";
        //     $.ajax({
        //         type: "GET",
        //         url: url,
        //         data: "assign_user=assign_user&user_group_id="+user_group_id+"&hidden_user_chk_status="+status +"&user_id="+ user_id+"&group_id=" + group_id,
        //         beforeSend: function () { },
        //         success: function (data) {
        //             toastr.success(data.message);
        //             $(this).closest("tr").find('.hidden_user_id').val(data.ass_id);
        //             console.log($(this).closest("tr").find('.hidden_user_id').val(data.ass_id));
        //         },
        //         complete: function () { }
        //     });

        // });

});

// function ajaxGroupList() {
//         var user_id = $('#user_id').val();
//         if (user_id == "") {
//             $("#ajaxSearchBox").html("");
//         } else {
//             var url = "{{route('extensionsvalley.admin.ajaxGroupList')}}";
//             $.ajax({
//                 type: "GET",
//                 url: url,
//                 data: "user_id=" + user_id,
//                 beforeSend: function () { },
//                 success: function (aclData) {
//                     // console.log(aclData);
//                     $(".assignListData").html(aclData);
//                                 $(".assignListData").show();
//                                 $(".theadscroll").scrollTop( $( ".theadscroll" ));
//                                 $(".theadscroll").perfectScrollbar('update');
//                 },
//                 complete: function () { }
//             });
//         }

//     }

//     function ajaxUserList() {
//         var group_id = $('#group_id').val();
//         if (group_id == "") {
//             $("#ajaxSearchBox").html("");
//         } else {
//             var url = "{{route('extensionsvalley.admin.ajaxGroupList')}}";
//             $.ajax({
//                 type: "GET",
//                 url: url,
//                 data: "group_id=" + group_id,
//                 beforeSend: function () { },
//                 success: function (aclData) {
//                     // console.log(aclData);
//                     $(".assignListData").html(aclData);
//                                 $(".assignListData").show();
//                                 $(".theadscroll").scrollTop( $( ".theadscroll" ));
//                                 $(".theadscroll").perfectScrollbar('update');
//                 },
//                 complete: function () { }
//             });
//         }

//     }
//------------------------------------------name search on table head---------------------------------------------------------------
$("#item_search_btn").click(function(){
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
    });

    function searchbyName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("fixTable");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
$("#user_id").on('change', function(){
    var id=$("#user_id").val();
    if(id!=0){
    $('.floatThead-wrapper').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
    }
});
$("#group_id").on('change', function(){
    var id=$("#group_id").val();
    if(id!=0){
    $('.floatThead-wrapper').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
    }
});


</script>
@stop
