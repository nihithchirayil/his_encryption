@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    <style>

    </style>
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="right_col">
        <div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{ $title }}</div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        <input type="hidden" id="updateUrlHidden" value="<?= $updateUrl ?>">
        <div class="row codfox_container">
            <div class="col-md-8 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Name</label>
                                    <div class="clearfix"></div>
                                    {!! Form::text('name', $searchFields['name'] ?? '', [
    'class' => 'form-control',
    'placeholder' => 'User Name',
    'id' => 'search_username',
    'autocomplete' => 'off',
]) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('status', ['' => 'Select Status', '1' => 'Active', '0' => 'Inactive'], $searchFields['status'] ?? '', [
    'class' => 'form-control select2',
    'id' => 'search_status',
]) !!}
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="resetFilter()" class="btn btn-block bg-orange"><i
                                        class="fa fa-recycle"></i>
                                    Reset</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searchUser()" id="searchUserBtn"
                                    class="btn btn-block light_purple_bg"><i id="searchUserSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-2 pull-right padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="resetUserForm(1)" type="button" class="btn btn-block bg-blue"><i
                                        class="fa fa-plus"></i>
                                    Add New User</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 no-padding" id="sarchUsersDataDiv">

                </div>

            </div>
            <div class="col-md-4 padding_sm" id="user_add_form_wraper">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height: 595px">
                        <strong>
                            <h5 class="green text-center" id="userHeaderString"> Add New User</h5>
                        </strong>
                        <hr>
                        <input type="hidden" name="user_id" id="user_id" value="0">
                        <input type="hidden" name="group_id" id="group_id" value="0">

                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Name<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" required name="name"
                                    value="" id="name">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Email ID<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="email" onblur="checkEmailUserName(1)" class="form-control" value=""
                                    name="email" id="email">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">User Name/Login Id<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" onblur="checkEmailUserName(2)"
                                    class="form-control" required name="user_name" id="user_name">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Password<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="password" autocomplete="off" class="form-control" name="password"
                                    id="password">
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Confirm Password<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="password" autocomplete="off" class="form-control"
                                    onblur="passwordConfirm()" name="password_confirmation" id="password_confirmation">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3 padding_sm" style="margin-top: 15px">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" class="" name="is_cashier" id="is_cashier">
                                <label for="is_cashier">
                                    Is Cashier</label><br>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm" style="margin-top: 15px">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" class="" name="is_nurse" id="is_nurse">
                                <label for="is_nurse">
                                    Is Nurse</label><br>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Default Location</label>
                                <div class="clearfix"></div>
                                <select class="form-control default_location select2" name="default_location"
                                    id="default_location">
                                    <option value=''>Select Location</option>
                                    <?php
                                           foreach ($location as $key=>$val) {
                                               ?>
                                    <option value="<?= $key ?>">
                                        <?= $val ?>(<?= $key ?>)</option>
                                    <?php
                                           }
                                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Default Nursing Station</label>
                                <div class="clearfix"></div>
                                {!! Form::select('default_station_id', ['0' => ' Select Nursing Station'] + $default_station_id->toArray(), null, [
    'class' => 'form-control select2 default_station_id',
    'id' => 'default_station_id',
]) !!}
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Default Store</label>
                                <div class="clearfix"></div>
                                {!! Form::select('default_store', ['0' => ' Select Store'] + $store->toArray(), null, [
    'class' => 'form-control select2 default_store',
    'id' => 'default_store',
]) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm">
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Status<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" name="status" id="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Default Procedure Type</label>
                                {{-- <div class="clearfix"></div>
                                <input type="text" autocomplete="off"
                                    class="form-control" name="default_landing_page" id="default_landing_page"> --}}
                                    <div class="clearfix"></div>
                                    {!! Form::select('default_procedure_type',$procedure_type_list, null, [
                                        'class' => 'form-control select2',
                                        'id' => 'default_procedure_type',
                                        'placeholder' => 'Select Procedure Type',
                                    ]) !!}
                            </div>
                        </div>
                        <div class="col-md-5 padding_sm" style="margin-top: 15px">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" class="" name="is_casuality_user" id="is_casuality_user">
                                <label for="is_casuality_user">
                                    Is Casuality User</label><br>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Signature</label>
                                <div class="clearfix"></div>
                                 <input type="file" autocomplete="off" id="uploadSignature" class="form-control" name="uploadSignature" placeholder="" onchange="validateImage(this);">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 15px">
                            <div id="popupSign" onclick="showImage();" class="hidden" style="margin-top: 15px">
                                  <!-- <img id="previewImage" src="" alt="Preview"> -->
                                  <i title="Preview" class="fa fa-info-circle" style="font-size:20px"></i>
                            </div>
                        </div>
                        {{-- default landing page --}}
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label for="">Default Landing Page</label>
                                {{-- <div class="clearfix"></div>
                                <input type="text" autocomplete="off"
                                    class="form-control" name="default_landing_page" id="default_landing_page"> --}}
                                    <div class="clearfix"></div>
                                    {!! Form::select('default_landing_page',$menu_list, null, [
                                        'class' => 'form-control select2',
                                        'id' => 'default_landing_page',
                                        'placeholder' => 'Select Landing Page',
                                    ]) !!}
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <button onclick="resetUserForm()" type="button" class="btn btn-block bg-orange"><i
                                    class="fa fa-recycle"></i>
                                Reset</button>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button onclick="saveUserForm()" id="saveUserFormBtn" type="button"
                                class="btn btn-block bg-blue"><i id="saveUserFormSpin" class="fa fa-save"></i>
                                Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="imageModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 50%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>

            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow: auto">
                <div id="imageViewSign">
                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/addNewUser.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection
